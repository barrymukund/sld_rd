<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#165" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1878" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.752755859375" Y="-4.21956640625" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140625" />
                  <Point X="25.542365234375" Y="-3.467376953125" />
                  <Point X="25.464931640625" Y="-3.355809814453" />
                  <Point X="25.37863671875" Y="-3.2314765625" />
                  <Point X="25.356751953125" Y="-3.209020263672" />
                  <Point X="25.33049609375" Y="-3.189776611328" />
                  <Point X="25.30249609375" Y="-3.175669189453" />
                  <Point X="25.182671875" Y="-3.138480224609" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097036132812" />
                  <Point X="24.843353515625" Y="-3.134224853516" />
                  <Point X="24.70981640625" Y="-3.175669433594" />
                  <Point X="24.68181640625" Y="-3.189776855469" />
                  <Point X="24.655560546875" Y="-3.209020751953" />
                  <Point X="24.63367578125" Y="-3.231477539062" />
                  <Point X="24.556244140625" Y="-3.343044433594" />
                  <Point X="24.46994921875" Y="-3.467377929688" />
                  <Point X="24.4618125" Y="-3.481572021484" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.23787890625" Y="-4.300472167969" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="24.074361328125" Y="-4.875184082031" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.7850390625" Y="-4.810456054688" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.762232421875" Y="-4.736663085938" />
                  <Point X="23.7850390625" Y="-4.563438476562" />
                  <Point X="23.78580078125" Y="-4.5479296875" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.754865234375" Y="-4.372311035156" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.182964355469" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.566037109375" Y="-4.034457275391" />
                  <Point X="23.443095703125" Y="-3.926639648438" />
                  <Point X="23.4168125" Y="-3.910295898438" />
                  <Point X="23.387115234375" Y="-3.897994628906" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.2105546875" Y="-3.881369628906" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708496094" />
                  <Point X="22.985533203125" Y="-3.882028076172" />
                  <Point X="22.957341796875" Y="-3.894801513672" />
                  <Point X="22.83533984375" Y="-3.976321289062" />
                  <Point X="22.699376953125" Y="-4.067169677734" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461914062" />
                  <Point X="22.546365234375" Y="-4.253937011719" />
                  <Point X="22.5198515625" Y="-4.288489746094" />
                  <Point X="22.42358203125" Y="-4.228881835938" />
                  <Point X="22.19828515625" Y="-4.0893828125" />
                  <Point X="22.01049609375" Y="-3.944791748047" />
                  <Point X="21.89527734375" Y="-3.856077392578" />
                  <Point X="22.2241640625" Y="-3.286429199219" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647655517578" />
                  <Point X="22.593412109375" Y="-2.616130859375" />
                  <Point X="22.59442578125" Y="-2.585198486328" />
                  <Point X="22.585443359375" Y="-2.555581787109" />
                  <Point X="22.571228515625" Y="-2.526753417969" />
                  <Point X="22.553201171875" Y="-2.501593261719" />
                  <Point X="22.535853515625" Y="-2.484244628906" />
                  <Point X="22.510697265625" Y="-2.466216796875" />
                  <Point X="22.4818671875" Y="-2.451997802734" />
                  <Point X="22.452248046875" Y="-2.443011962891" />
                  <Point X="22.4213125" Y="-2.444024169922" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.681228515625" Y="-2.85355859375" />
                  <Point X="21.1819765625" Y="-3.141801513672" />
                  <Point X="21.095130859375" Y="-3.027705078125" />
                  <Point X="20.917140625" Y="-2.793861083984" />
                  <Point X="20.7825078125" Y="-2.568102539062" />
                  <Point X="20.693857421875" Y="-2.419449951172" />
                  <Point X="21.27803125" Y="-1.971198364258" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.915423828125" Y="-1.475592285156" />
                  <Point X="21.933388671875" Y="-1.448460449219" />
                  <Point X="21.946142578125" Y="-1.419833496094" />
                  <Point X="21.95384765625" Y="-1.390086303711" />
                  <Point X="21.95665234375" Y="-1.359655273438" />
                  <Point X="21.954443359375" Y="-1.327985839844" />
                  <Point X="21.947443359375" Y="-1.298243164062" />
                  <Point X="21.93136328125" Y="-1.272261230469" />
                  <Point X="21.910533203125" Y="-1.248305053711" />
                  <Point X="21.88703125" Y="-1.228768066406" />
                  <Point X="21.860548828125" Y="-1.213181396484" />
                  <Point X="21.831287109375" Y="-1.201957641602" />
                  <Point X="21.7993984375" Y="-1.195475097656" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="20.910154296875" Y="-1.307330810547" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.2355390625" Y="-1.265191894531" />
                  <Point X="20.165923828125" Y="-0.992650817871" />
                  <Point X="20.130302734375" Y="-0.743601379395" />
                  <Point X="20.107576171875" Y="-0.584698425293" />
                  <Point X="20.765392578125" Y="-0.408437225342" />
                  <Point X="21.467125" Y="-0.220408432007" />
                  <Point X="21.489525390625" Y="-0.211210540771" />
                  <Point X="21.518216796875" Y="-0.194881835938" />
                  <Point X="21.5314375" Y="-0.185800216675" />
                  <Point X="21.55788671875" Y="-0.164128967285" />
                  <Point X="21.574080078125" Y="-0.147104141235" />
                  <Point X="21.585599609375" Y="-0.126625663757" />
                  <Point X="21.597400390625" Y="-0.097788986206" />
                  <Point X="21.601994140625" Y="-0.083388938904" />
                  <Point X="21.609115234375" Y="-0.052860404968" />
                  <Point X="21.611599609375" Y="-0.031473009109" />
                  <Point X="21.609203125" Y="-0.010075713158" />
                  <Point X="21.60258203125" Y="0.018839895248" />
                  <Point X="21.5980625" Y="0.033220058441" />
                  <Point X="21.58576171875" Y="0.063668598175" />
                  <Point X="21.574333984375" Y="0.084197692871" />
                  <Point X="21.55821875" Y="0.101294540405" />
                  <Point X="21.5332734375" Y="0.121922851562" />
                  <Point X="21.520109375" Y="0.131055770874" />
                  <Point X="21.4899140625" Y="0.148428787231" />
                  <Point X="21.467125" Y="0.157848297119" />
                  <Point X="20.68509375" Y="0.367393066406" />
                  <Point X="20.108185546875" Y="0.521975463867" />
                  <Point X="20.1306484375" Y="0.673783874512" />
                  <Point X="20.17551171875" Y="0.976968078613" />
                  <Point X="20.247216796875" Y="1.241581176758" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="20.731314453125" Y="1.366016845703" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255017578125" Y="1.299341796875" />
                  <Point X="21.276578125" Y="1.301228271484" />
                  <Point X="21.29686328125" Y="1.305263427734" />
                  <Point X="21.3259140625" Y="1.314423217773" />
                  <Point X="21.3582890625" Y="1.324631225586" />
                  <Point X="21.37722265625" Y="1.332962402344" />
                  <Point X="21.395966796875" Y="1.343784423828" />
                  <Point X="21.4126484375" Y="1.356015991211" />
                  <Point X="21.426287109375" Y="1.371568359375" />
                  <Point X="21.438701171875" Y="1.389298217773" />
                  <Point X="21.448650390625" Y="1.407432983398" />
                  <Point X="21.460306640625" Y="1.435575073242" />
                  <Point X="21.473296875" Y="1.466937011719" />
                  <Point X="21.479083984375" Y="1.486788818359" />
                  <Point X="21.48284375" Y="1.508104614258" />
                  <Point X="21.484197265625" Y="1.528750854492" />
                  <Point X="21.481048828125" Y="1.549200561523" />
                  <Point X="21.4754453125" Y="1.570107299805" />
                  <Point X="21.467951171875" Y="1.589377441406" />
                  <Point X="21.45388671875" Y="1.616396362305" />
                  <Point X="21.4382109375" Y="1.646507080078" />
                  <Point X="21.426716796875" Y="1.663709716797" />
                  <Point X="21.4128046875" Y="1.680288574219" />
                  <Point X="21.39786328125" Y="1.694590087891" />
                  <Point X="20.9492890625" Y="2.038794433594" />
                  <Point X="20.648140625" Y="2.269873535156" />
                  <Point X="20.744521484375" Y="2.434996582031" />
                  <Point X="20.918853515625" Y="2.733665771484" />
                  <Point X="21.108783203125" Y="2.977796875" />
                  <Point X="21.24949609375" Y="3.158662353516" />
                  <Point X="21.48440234375" Y="3.023038330078" />
                  <Point X="21.79334375" Y="2.844671142578" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.89366796875" Y="2.822256591797" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.95943359375" Y="2.818762939453" />
                  <Point X="21.980892578125" Y="2.821587890625" />
                  <Point X="22.000984375" Y="2.826504150391" />
                  <Point X="22.019537109375" Y="2.835652832031" />
                  <Point X="22.037791015625" Y="2.847281738281" />
                  <Point X="22.053923828125" Y="2.860228515625" />
                  <Point X="22.082642578125" Y="2.888947021484" />
                  <Point X="22.1146484375" Y="2.920951904297" />
                  <Point X="22.127595703125" Y="2.937084228516" />
                  <Point X="22.139224609375" Y="2.955338134766" />
                  <Point X="22.14837109375" Y="2.973887207031" />
                  <Point X="22.1532890625" Y="2.9939765625" />
                  <Point X="22.156115234375" Y="3.015434814453" />
                  <Point X="22.15656640625" Y="3.036122314453" />
                  <Point X="22.153025390625" Y="3.076582275391" />
                  <Point X="22.14908203125" Y="3.121671875" />
                  <Point X="22.145044921875" Y="3.141959472656" />
                  <Point X="22.13853515625" Y="3.162603271484" />
                  <Point X="22.130205078125" Y="3.181532958984" />
                  <Point X="21.931427734375" Y="3.525825439453" />
                  <Point X="21.81666796875" Y="3.724595947266" />
                  <Point X="21.9957578125" Y="3.861903320312" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.598517578125" Y="4.260880859375" />
                  <Point X="22.83296484375" Y="4.391133789062" />
                  <Point X="22.862228515625" Y="4.352995117187" />
                  <Point X="22.9568046875" Y="4.229741210938" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.00488671875" Y="4.18939453125" />
                  <Point X="23.04991796875" Y="4.165952636719" />
                  <Point X="23.100103515625" Y="4.139827636719" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.222548828125" Y="4.134482421875" />
                  <Point X="23.269451171875" Y="4.153910644531" />
                  <Point X="23.32172265625" Y="4.175562011719" />
                  <Point X="23.339853515625" Y="4.185508789062" />
                  <Point X="23.357583984375" Y="4.197922851562" />
                  <Point X="23.373140625" Y="4.211563964844" />
                  <Point X="23.385373046875" Y="4.228249511719" />
                  <Point X="23.396193359375" Y="4.246993652344" />
                  <Point X="23.404521484375" Y="4.265921875" />
                  <Point X="23.419787109375" Y="4.31433984375" />
                  <Point X="23.43680078125" Y="4.368298339844" />
                  <Point X="23.4408359375" Y="4.388583496094" />
                  <Point X="23.44272265625" Y="4.410145019531" />
                  <Point X="23.442271484375" Y="4.430826660156" />
                  <Point X="23.419671875" Y="4.602481445312" />
                  <Point X="23.415798828125" Y="4.631897460938" />
                  <Point X="23.65731640625" Y="4.699610351562" />
                  <Point X="24.050369140625" Y="4.80980859375" />
                  <Point X="24.41301171875" Y="4.852250488281" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.774703125" Y="4.627399902344" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.248068359375" Y="4.6664296875" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.500845703125" Y="4.867680664062" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.14407421875" Y="4.759301757813" />
                  <Point X="26.4810234375" Y="4.677952148438" />
                  <Point X="26.675537109375" Y="4.607400390625" />
                  <Point X="26.894650390625" Y="4.527926269531" />
                  <Point X="27.083486328125" Y="4.439613769531" />
                  <Point X="27.294572265625" Y="4.340895996094" />
                  <Point X="27.477033203125" Y="4.234594726563" />
                  <Point X="27.680982421875" Y="4.1157734375" />
                  <Point X="27.85303515625" Y="3.993418212891" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.55508203125" Y="3.256906982422" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142080078125" Y="2.539935546875" />
                  <Point X="27.133078125" Y="2.516058349609" />
                  <Point X="27.122923828125" Y="2.478087646484" />
                  <Point X="27.111607421875" Y="2.435771972656" />
                  <Point X="27.108619140625" Y="2.417936035156" />
                  <Point X="27.107728515625" Y="2.380953613281" />
                  <Point X="27.1116875" Y="2.348119873047" />
                  <Point X="27.116099609375" Y="2.311528808594" />
                  <Point X="27.121443359375" Y="2.289601318359" />
                  <Point X="27.1297109375" Y="2.267511474609" />
                  <Point X="27.140072265625" Y="2.247470214844" />
                  <Point X="27.160388671875" Y="2.217529052734" />
                  <Point X="27.18303125" Y="2.184161621094" />
                  <Point X="27.194466796875" Y="2.170327636719" />
                  <Point X="27.221599609375" Y="2.145592529297" />
                  <Point X="27.251541015625" Y="2.125276123047" />
                  <Point X="27.284908203125" Y="2.102635009766" />
                  <Point X="27.304953125" Y="2.092272705078" />
                  <Point X="27.327041015625" Y="2.084006591797" />
                  <Point X="27.34896484375" Y="2.078663330078" />
                  <Point X="27.381798828125" Y="2.074704101562" />
                  <Point X="27.418388671875" Y="2.070291748047" />
                  <Point X="27.436466796875" Y="2.069845703125" />
                  <Point X="27.47320703125" Y="2.074171142578" />
                  <Point X="27.511177734375" Y="2.084325195313" />
                  <Point X="27.553494140625" Y="2.095640869141" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.375109375" Y="2.564273681641" />
                  <Point X="28.967328125" Y="2.90619140625" />
                  <Point X="29.002298828125" Y="2.85758984375" />
                  <Point X="29.123271484375" Y="2.689465087891" />
                  <Point X="29.21919140625" Y="2.530955810547" />
                  <Point X="29.26219921875" Y="2.459883544922" />
                  <Point X="28.767296875" Y="2.080130371094" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.22142578125" Y="1.660241577148" />
                  <Point X="28.203974609375" Y="1.641627685547" />
                  <Point X="28.176646484375" Y="1.605976928711" />
                  <Point X="28.146193359375" Y="1.566246337891" />
                  <Point X="28.136607421875" Y="1.550912231445" />
                  <Point X="28.121630859375" Y="1.517087890625" />
                  <Point X="28.111451171875" Y="1.480688232422" />
                  <Point X="28.10010546875" Y="1.440123413086" />
                  <Point X="28.09665234375" Y="1.417825317383" />
                  <Point X="28.0958359375" Y="1.39425378418" />
                  <Point X="28.097740234375" Y="1.371765869141" />
                  <Point X="28.10609765625" Y="1.331266479492" />
                  <Point X="28.11541015625" Y="1.286133178711" />
                  <Point X="28.1206796875" Y="1.268979980469" />
                  <Point X="28.136283203125" Y="1.235740966797" />
                  <Point X="28.15901171875" Y="1.201194824219" />
                  <Point X="28.18433984375" Y="1.162695556641" />
                  <Point X="28.198892578125" Y="1.145451416016" />
                  <Point X="28.21613671875" Y="1.129361206055" />
                  <Point X="28.23434765625" Y="1.116034790039" />
                  <Point X="28.26728515625" Y="1.097494262695" />
                  <Point X="28.303990234375" Y="1.07683215332" />
                  <Point X="28.320521484375" Y="1.069501342773" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.40065234375" Y="1.053552978516" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.235396484375" Y="1.145354370117" />
                  <Point X="29.77683984375" Y="1.21663659668" />
                  <Point X="29.793978515625" Y="1.146236938477" />
                  <Point X="29.845939453125" Y="0.932788391113" />
                  <Point X="29.8761640625" Y="0.738667114258" />
                  <Point X="29.890865234375" Y="0.644238708496" />
                  <Point X="29.33143359375" Y="0.494339141846" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.7047890625" Y="0.325585296631" />
                  <Point X="28.681546875" Y="0.315068023682" />
                  <Point X="28.637794921875" Y="0.289778839111" />
                  <Point X="28.589037109375" Y="0.26159564209" />
                  <Point X="28.5743125" Y="0.251095840454" />
                  <Point X="28.547533203125" Y="0.225577148438" />
                  <Point X="28.52128125" Y="0.192127105713" />
                  <Point X="28.49202734375" Y="0.154849517822" />
                  <Point X="28.48030078125" Y="0.135568344116" />
                  <Point X="28.47052734375" Y="0.114105499268" />
                  <Point X="28.463681640625" Y="0.092603630066" />
                  <Point X="28.454931640625" Y="0.0469126091" />
                  <Point X="28.4451796875" Y="-0.004006982803" />
                  <Point X="28.443484375" Y="-0.021875137329" />
                  <Point X="28.4451796875" Y="-0.058553001404" />
                  <Point X="28.4539296875" Y="-0.104244171143" />
                  <Point X="28.463681640625" Y="-0.155163619995" />
                  <Point X="28.47052734375" Y="-0.176663360596" />
                  <Point X="28.48030078125" Y="-0.198127578735" />
                  <Point X="28.492025390625" Y="-0.217408447266" />
                  <Point X="28.518275390625" Y="-0.250858337402" />
                  <Point X="28.54753125" Y="-0.288136077881" />
                  <Point X="28.56000390625" Y="-0.301239685059" />
                  <Point X="28.589037109375" Y="-0.324155609131" />
                  <Point X="28.6327890625" Y="-0.349444946289" />
                  <Point X="28.681546875" Y="-0.37762802124" />
                  <Point X="28.692708984375" Y="-0.383137695312" />
                  <Point X="28.716580078125" Y="-0.392149810791" />
                  <Point X="29.4017890625" Y="-0.575751342773" />
                  <Point X="29.891474609375" Y="-0.706961853027" />
                  <Point X="29.88403515625" Y="-0.756301269531" />
                  <Point X="29.855025390625" Y="-0.948724182129" />
                  <Point X="29.816298828125" Y="-1.118426879883" />
                  <Point X="29.80117578125" Y="-1.18469909668" />
                  <Point X="29.137439453125" Y="-1.09731652832" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710266113" />
                  <Point X="28.37466015625" Y="-1.005508666992" />
                  <Point X="28.288791015625" Y="-1.024172729492" />
                  <Point X="28.193095703125" Y="-1.044972412109" />
                  <Point X="28.163974609375" Y="-1.056597167969" />
                  <Point X="28.1361484375" Y="-1.073489379883" />
                  <Point X="28.1123984375" Y="-1.093960083008" />
                  <Point X="28.06049609375" Y="-1.15638269043" />
                  <Point X="28.002654296875" Y="-1.225947998047" />
                  <Point X="27.987935546875" Y="-1.250329956055" />
                  <Point X="27.976591796875" Y="-1.277716064453" />
                  <Point X="27.969759765625" Y="-1.305365356445" />
                  <Point X="27.9623203125" Y="-1.386205078125" />
                  <Point X="27.95403125" Y="-1.476295532227" />
                  <Point X="27.956349609375" Y="-1.50756237793" />
                  <Point X="27.964080078125" Y="-1.539182739258" />
                  <Point X="27.976451171875" Y="-1.567996826172" />
                  <Point X="28.02397265625" Y="-1.641913085938" />
                  <Point X="28.076931640625" Y="-1.724287353516" />
                  <Point X="28.0869375" Y="-1.737241943359" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.746509765625" Y="-2.248836425781" />
                  <Point X="29.213123046875" Y="-2.606882568359" />
                  <Point X="29.20658984375" Y="-2.617453857422" />
                  <Point X="29.124802734375" Y="-2.749797851562" />
                  <Point X="29.044724609375" Y="-2.863579101562" />
                  <Point X="29.028982421875" Y="-2.885945556641" />
                  <Point X="28.436029296875" Y="-2.543602783203" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.7542265625" Y="-2.159825195312" />
                  <Point X="27.652029296875" Y="-2.141368408203" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395507812" />
                  <Point X="27.474609375" Y="-2.125353515625" />
                  <Point X="27.444833984375" Y="-2.135176757812" />
                  <Point X="27.359931640625" Y="-2.179859619141" />
                  <Point X="27.26531640625" Y="-2.229655761719" />
                  <Point X="27.242384765625" Y="-2.246549316406" />
                  <Point X="27.22142578125" Y="-2.267508544922" />
                  <Point X="27.204533203125" Y="-2.290438476562" />
                  <Point X="27.159849609375" Y="-2.37533984375" />
                  <Point X="27.110052734375" Y="-2.469956542969" />
                  <Point X="27.100228515625" Y="-2.499735351563" />
                  <Point X="27.095271484375" Y="-2.531908935547" />
                  <Point X="27.09567578125" Y="-2.563259033203" />
                  <Point X="27.1141328125" Y="-2.66545703125" />
                  <Point X="27.134703125" Y="-2.779349365234" />
                  <Point X="27.13898828125" Y="-2.795140869141" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.5604375" Y="-3.533822265625" />
                  <Point X="27.86128515625" Y="-4.054903808594" />
                  <Point X="27.7818359375" Y="-4.111653808594" />
                  <Point X="27.701767578125" Y="-4.163480957031" />
                  <Point X="27.24316796875" Y="-3.565822998047" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.621130859375" Y="-2.835755371094" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.30686328125" Y="-2.751260742188" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="26.0194765625" Y="-2.866237060547" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968861572266" />
                  <Point X="25.88725" Y="-2.996688476562" />
                  <Point X="25.875625" Y="-3.02580859375" />
                  <Point X="25.850173828125" Y="-3.142903076172" />
                  <Point X="25.821810546875" Y="-3.273396484375" />
                  <Point X="25.819724609375" Y="-3.289627685547" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.935541015625" Y="-4.202698242188" />
                  <Point X="26.022064453125" Y="-4.859915527344" />
                  <Point X="25.975681640625" Y="-4.870083007812" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.941568359375" Y="-4.752634765625" />
                  <Point X="23.85875390625" Y="-4.731328125" />
                  <Point X="23.8792265625" Y="-4.575836914062" />
                  <Point X="23.879923828125" Y="-4.568098632812" />
                  <Point X="23.88055078125" Y="-4.541030761719" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497688964844" />
                  <Point X="23.8480390625" Y="-4.353776855469" />
                  <Point X="23.81613671875" Y="-4.193397460938" />
                  <Point X="23.811875" Y="-4.178468261719" />
                  <Point X="23.80097265625" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135462890625" />
                  <Point X="23.778259765625" Y="-4.107625976562" />
                  <Point X="23.76942578125" Y="-4.094861083984" />
                  <Point X="23.74979296875" Y="-4.070937744141" />
                  <Point X="23.738994140625" Y="-4.059779296875" />
                  <Point X="23.62867578125" Y="-3.963032470703" />
                  <Point X="23.505734375" Y="-3.85521484375" />
                  <Point X="23.49326171875" Y="-3.845965087891" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822527587891" />
                  <Point X="23.423470703125" Y="-3.810226318359" />
                  <Point X="23.4086875" Y="-3.805476318359" />
                  <Point X="23.378544921875" Y="-3.798447998047" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.216767578125" Y="-3.786572998047" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166015625" />
                  <Point X="22.991994140625" Y="-3.781945556641" />
                  <Point X="22.9609453125" Y="-3.790265136719" />
                  <Point X="22.946326171875" Y="-3.79549609375" />
                  <Point X="22.918134765625" Y="-3.80826953125" />
                  <Point X="22.9045625" Y="-3.815812011719" />
                  <Point X="22.782560546875" Y="-3.897331787109" />
                  <Point X="22.64659765625" Y="-3.988180175781" />
                  <Point X="22.64031640625" Y="-3.992759765625" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032770507812" />
                  <Point X="22.589529296875" Y="-4.041629394531" />
                  <Point X="22.496798828125" Y="-4.162479003906" />
                  <Point X="22.47359375" Y="-4.148111328125" />
                  <Point X="22.25240625" Y="-4.01115625" />
                  <Point X="22.068453125" Y="-3.869519287109" />
                  <Point X="22.01913671875" Y="-3.831546875" />
                  <Point X="22.306435546875" Y="-3.333929199219" />
                  <Point X="22.658509765625" Y="-2.724119628906" />
                  <Point X="22.6651484375" Y="-2.710086181641" />
                  <Point X="22.67605078125" Y="-2.681122070312" />
                  <Point X="22.680314453125" Y="-2.66619140625" />
                  <Point X="22.6865859375" Y="-2.634666748047" />
                  <Point X="22.688361328125" Y="-2.619242431641" />
                  <Point X="22.689375" Y="-2.588310058594" />
                  <Point X="22.6853359375" Y="-2.557626220703" />
                  <Point X="22.676353515625" Y="-2.528009521484" />
                  <Point X="22.6706484375" Y="-2.513568603516" />
                  <Point X="22.65643359375" Y="-2.484740234375" />
                  <Point X="22.648451171875" Y="-2.471422363281" />
                  <Point X="22.630423828125" Y="-2.446262207031" />
                  <Point X="22.62037890625" Y="-2.434419921875" />
                  <Point X="22.60303125" Y="-2.417071289062" />
                  <Point X="22.59119140625" Y="-2.407025878906" />
                  <Point X="22.56603515625" Y="-2.388998046875" />
                  <Point X="22.55271875" Y="-2.381015625" />
                  <Point X="22.523888671875" Y="-2.366796630859" />
                  <Point X="22.509447265625" Y="-2.361089355469" />
                  <Point X="22.479828125" Y="-2.352103515625" />
                  <Point X="22.449140625" Y="-2.348062744141" />
                  <Point X="22.418205078125" Y="-2.349074951172" />
                  <Point X="22.402779296875" Y="-2.350849365234" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.633728515625" Y="-2.771286132812" />
                  <Point X="21.206912109375" Y="-3.017708740234" />
                  <Point X="21.170724609375" Y="-2.970166503906" />
                  <Point X="20.995978515625" Y="-2.740584472656" />
                  <Point X="20.864099609375" Y="-2.519444091797" />
                  <Point X="20.818734375" Y="-2.443373535156" />
                  <Point X="21.33586328125" Y="-2.046567016602" />
                  <Point X="21.951876953125" Y="-1.573882080078" />
                  <Point X="21.963515625" Y="-1.563310668945" />
                  <Point X="21.98489453125" Y="-1.540389526367" />
                  <Point X="21.994634765625" Y="-1.528039794922" />
                  <Point X="22.012599609375" Y="-1.500907958984" />
                  <Point X="22.020166015625" Y="-1.487121582031" />
                  <Point X="22.032919921875" Y="-1.458494628906" />
                  <Point X="22.038107421875" Y="-1.443654174805" />
                  <Point X="22.0458125" Y="-1.413906982422" />
                  <Point X="22.048447265625" Y="-1.398805053711" />
                  <Point X="22.051251953125" Y="-1.368373901367" />
                  <Point X="22.051421875" Y="-1.353045043945" />
                  <Point X="22.049212890625" Y="-1.321375488281" />
                  <Point X="22.046916015625" Y="-1.306222045898" />
                  <Point X="22.039916015625" Y="-1.276479370117" />
                  <Point X="22.028224609375" Y="-1.248248413086" />
                  <Point X="22.01214453125" Y="-1.222266479492" />
                  <Point X="22.003052734375" Y="-1.209926635742" />
                  <Point X="21.98222265625" Y="-1.185970458984" />
                  <Point X="21.97126171875" Y="-1.175250732422" />
                  <Point X="21.947759765625" Y="-1.155713745117" />
                  <Point X="21.93521875" Y="-1.146896118164" />
                  <Point X="21.908736328125" Y="-1.131309448242" />
                  <Point X="21.8945703125" Y="-1.124482421875" />
                  <Point X="21.86530859375" Y="-1.113258789062" />
                  <Point X="21.850212890625" Y="-1.108861816406" />
                  <Point X="21.81832421875" Y="-1.102379272461" />
                  <Point X="21.802705078125" Y="-1.100532714844" />
                  <Point X="21.77137890625" Y="-1.09944152832" />
                  <Point X="21.755671875" Y="-1.100196655273" />
                  <Point X="20.89775390625" Y="-1.213143554688" />
                  <Point X="20.33908203125" Y="-1.286694213867" />
                  <Point X="20.327583984375" Y="-1.241680908203" />
                  <Point X="20.259240234375" Y="-0.974114868164" />
                  <Point X="20.224345703125" Y="-0.730150512695" />
                  <Point X="20.213548828125" Y="-0.654654602051" />
                  <Point X="20.78998046875" Y="-0.500200256348" />
                  <Point X="21.491712890625" Y="-0.312171325684" />
                  <Point X="21.503208984375" Y="-0.308288421631" />
                  <Point X="21.525609375" Y="-0.299090576172" />
                  <Point X="21.536513671875" Y="-0.293775848389" />
                  <Point X="21.565205078125" Y="-0.27744708252" />
                  <Point X="21.572005859375" Y="-0.273186798096" />
                  <Point X="21.591646484375" Y="-0.2592840271" />
                  <Point X="21.618095703125" Y="-0.237612731934" />
                  <Point X="21.62672265625" Y="-0.229602096558" />
                  <Point X="21.642916015625" Y="-0.212577346802" />
                  <Point X="21.65687890625" Y="-0.193680099487" />
                  <Point X="21.6683984375" Y="-0.173201644897" />
                  <Point X="21.673521484375" Y="-0.162606170654" />
                  <Point X="21.685322265625" Y="-0.133769454956" />
                  <Point X="21.68790625" Y="-0.126661270142" />
                  <Point X="21.694509765625" Y="-0.104969314575" />
                  <Point X="21.701630859375" Y="-0.074440834045" />
                  <Point X="21.70348046875" Y="-0.063822021484" />
                  <Point X="21.70596484375" Y="-0.042434593201" />
                  <Point X="21.706009765625" Y="-0.020899133682" />
                  <Point X="21.70361328125" Y="0.000498104453" />
                  <Point X="21.701806640625" Y="0.01112865448" />
                  <Point X="21.695185546875" Y="0.040044132233" />
                  <Point X="21.6932109375" Y="0.047323680878" />
                  <Point X="21.686146484375" Y="0.068804595947" />
                  <Point X="21.673845703125" Y="0.099253120422" />
                  <Point X="21.668767578125" Y="0.10987474823" />
                  <Point X="21.65733984375" Y="0.130403884888" />
                  <Point X="21.64346484375" Y="0.149358947754" />
                  <Point X="21.627349609375" Y="0.166455780029" />
                  <Point X="21.618759765625" Y="0.174505203247" />
                  <Point X="21.593814453125" Y="0.195133468628" />
                  <Point X="21.58742578125" Y="0.199977386475" />
                  <Point X="21.567486328125" Y="0.213399215698" />
                  <Point X="21.537291015625" Y="0.230772201538" />
                  <Point X="21.526203125" Y="0.236224578857" />
                  <Point X="21.5034140625" Y="0.24564414978" />
                  <Point X="21.491712890625" Y="0.249611190796" />
                  <Point X="20.709681640625" Y="0.459155944824" />
                  <Point X="20.2145546875" Y="0.591824890137" />
                  <Point X="20.224625" Y="0.659878173828" />
                  <Point X="20.26866796875" Y="0.957521057129" />
                  <Point X="20.33891015625" Y="1.216734130859" />
                  <Point X="20.3664140625" Y="1.318237060547" />
                  <Point X="20.7189140625" Y="1.271829589844" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.2529453125" Y="1.204364379883" />
                  <Point X="21.263298828125" Y="1.204703369141" />
                  <Point X="21.284859375" Y="1.20658984375" />
                  <Point X="21.29511328125" Y="1.208053833008" />
                  <Point X="21.3153984375" Y="1.212088989258" />
                  <Point X="21.3254296875" Y="1.214660400391" />
                  <Point X="21.35448046875" Y="1.22382019043" />
                  <Point X="21.38685546875" Y="1.234028198242" />
                  <Point X="21.39655078125" Y="1.237677001953" />
                  <Point X="21.415484375" Y="1.246008300781" />
                  <Point X="21.42472265625" Y="1.250690185547" />
                  <Point X="21.443466796875" Y="1.261512207031" />
                  <Point X="21.452140625" Y="1.267172241211" />
                  <Point X="21.468822265625" Y="1.279403930664" />
                  <Point X="21.48407421875" Y="1.293379150391" />
                  <Point X="21.497712890625" Y="1.308931518555" />
                  <Point X="21.504107421875" Y="1.317080078125" />
                  <Point X="21.516521484375" Y="1.334809936523" />
                  <Point X="21.521990234375" Y="1.343603759766" />
                  <Point X="21.531939453125" Y="1.361738525391" />
                  <Point X="21.536419921875" Y="1.371079589844" />
                  <Point X="21.548076171875" Y="1.399221679688" />
                  <Point X="21.56106640625" Y="1.430583618164" />
                  <Point X="21.5645" Y="1.440349731445" />
                  <Point X="21.570287109375" Y="1.460201538086" />
                  <Point X="21.572640625" Y="1.470286987305" />
                  <Point X="21.576400390625" Y="1.491602783203" />
                  <Point X="21.577640625" Y="1.501890014648" />
                  <Point X="21.578994140625" Y="1.522536376953" />
                  <Point X="21.578091796875" Y="1.543206787109" />
                  <Point X="21.574943359375" Y="1.563656616211" />
                  <Point X="21.572810546875" Y="1.573794921875" />
                  <Point X="21.56720703125" Y="1.594701538086" />
                  <Point X="21.563986328125" Y="1.604540527344" />
                  <Point X="21.5564921875" Y="1.623810668945" />
                  <Point X="21.55221875" Y="1.633241821289" />
                  <Point X="21.538154296875" Y="1.660260742188" />
                  <Point X="21.522478515625" Y="1.690371459961" />
                  <Point X="21.517201171875" Y="1.69928515625" />
                  <Point X="21.50570703125" Y="1.716487915039" />
                  <Point X="21.499490234375" Y="1.724776611328" />
                  <Point X="21.485578125" Y="1.74135546875" />
                  <Point X="21.478494140625" Y="1.748917236328" />
                  <Point X="21.463552734375" Y="1.76321875" />
                  <Point X="21.4556953125" Y="1.769958496094" />
                  <Point X="21.00712109375" Y="2.114162841797" />
                  <Point X="20.77238671875" Y="2.294280761719" />
                  <Point X="20.826568359375" Y="2.387106933594" />
                  <Point X="20.99771875" Y="2.680324951172" />
                  <Point X="21.183763671875" Y="2.919462890625" />
                  <Point X="21.273662109375" Y="3.035013671875" />
                  <Point X="21.43690234375" Y="2.940766113281" />
                  <Point X="21.74584375" Y="2.762398925781" />
                  <Point X="21.75508203125" Y="2.757716796875" />
                  <Point X="21.774015625" Y="2.749385742188" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.885388671875" Y="2.727618164062" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.961505859375" Y="2.723785644531" />
                  <Point X="21.97183203125" Y="2.724575683594" />
                  <Point X="21.993291015625" Y="2.727400634766" />
                  <Point X="22.00347265625" Y="2.729310302734" />
                  <Point X="22.023564453125" Y="2.7342265625" />
                  <Point X="22.043" Y="2.741300292969" />
                  <Point X="22.061552734375" Y="2.750448974609" />
                  <Point X="22.070580078125" Y="2.755530517578" />
                  <Point X="22.088833984375" Y="2.767159423828" />
                  <Point X="22.09725" Y="2.773190185547" />
                  <Point X="22.1133828125" Y="2.786136962891" />
                  <Point X="22.121099609375" Y="2.793052978516" />
                  <Point X="22.149818359375" Y="2.821771484375" />
                  <Point X="22.18182421875" Y="2.853776367188" />
                  <Point X="22.18873828125" Y="2.861489990234" />
                  <Point X="22.201685546875" Y="2.877622314453" />
                  <Point X="22.20771875" Y="2.886041015625" />
                  <Point X="22.21934765625" Y="2.904294921875" />
                  <Point X="22.2244296875" Y="2.913323974609" />
                  <Point X="22.233576171875" Y="2.931873046875" />
                  <Point X="22.240646484375" Y="2.951297851562" />
                  <Point X="22.245564453125" Y="2.971387207031" />
                  <Point X="22.2474765625" Y="2.981571777344" />
                  <Point X="22.250302734375" Y="3.003030029297" />
                  <Point X="22.251091796875" Y="3.013363525391" />
                  <Point X="22.25154296875" Y="3.034051025391" />
                  <Point X="22.251205078125" Y="3.044405029297" />
                  <Point X="22.2476640625" Y="3.084864990234" />
                  <Point X="22.243720703125" Y="3.129954589844" />
                  <Point X="22.242255859375" Y="3.140212646484" />
                  <Point X="22.23821875" Y="3.160500244141" />
                  <Point X="22.235646484375" Y="3.170529785156" />
                  <Point X="22.22913671875" Y="3.191173583984" />
                  <Point X="22.22548828125" Y="3.200867431641" />
                  <Point X="22.217158203125" Y="3.219797119141" />
                  <Point X="22.2124765625" Y="3.229032958984" />
                  <Point X="22.01369921875" Y="3.573325439453" />
                  <Point X="21.94061328125" Y="3.699915039062" />
                  <Point X="22.053560546875" Y="3.78651171875" />
                  <Point X="22.3516328125" Y="4.015041015625" />
                  <Point X="22.644654296875" Y="4.177836914063" />
                  <Point X="22.8074765625" Y="4.268296386719" />
                  <Point X="22.881435546875" Y="4.17191015625" />
                  <Point X="22.8881796875" Y="4.164048828125" />
                  <Point X="22.902482421875" Y="4.149107421875" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.93490625" Y="4.121898925781" />
                  <Point X="22.95210546875" Y="4.11040625" />
                  <Point X="22.96101953125" Y="4.105128417969" />
                  <Point X="23.00605078125" Y="4.081686767578" />
                  <Point X="23.056236328125" Y="4.055561767578" />
                  <Point X="23.06567578125" Y="4.051285400391" />
                  <Point X="23.08495703125" Y="4.043788330078" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031378173828" />
                  <Point X="23.219181640625" Y="4.035136474609" />
                  <Point X="23.229271484375" Y="4.037488525391" />
                  <Point X="23.2491328125" Y="4.043277587891" />
                  <Point X="23.258904296875" Y="4.046714355469" />
                  <Point X="23.305806640625" Y="4.066142578125" />
                  <Point X="23.358078125" Y="4.087793945312" />
                  <Point X="23.367416015625" Y="4.092272705078" />
                  <Point X="23.385546875" Y="4.102219238281" />
                  <Point X="23.39433984375" Y="4.1076875" />
                  <Point X="23.4120703125" Y="4.1201015625" />
                  <Point X="23.420216796875" Y="4.126494140625" />
                  <Point X="23.4357734375" Y="4.140135253906" />
                  <Point X="23.4497578125" Y="4.155395507812" />
                  <Point X="23.461990234375" Y="4.172081054688" />
                  <Point X="23.4676484375" Y="4.180754882812" />
                  <Point X="23.47846875" Y="4.199499023438" />
                  <Point X="23.4831484375" Y="4.208734375" />
                  <Point X="23.4914765625" Y="4.227662597656" />
                  <Point X="23.495125" Y="4.23735546875" />
                  <Point X="23.510390625" Y="4.2857734375" />
                  <Point X="23.527404296875" Y="4.339731933594" />
                  <Point X="23.529974609375" Y="4.349764160156" />
                  <Point X="23.534009765625" Y="4.370049316406" />
                  <Point X="23.535474609375" Y="4.380302246094" />
                  <Point X="23.537361328125" Y="4.401863769531" />
                  <Point X="23.53769921875" Y="4.412216796875" />
                  <Point X="23.537248046875" Y="4.4328984375" />
                  <Point X="23.536458984375" Y="4.443227050781" />
                  <Point X="23.520736328125" Y="4.562654296875" />
                  <Point X="23.682962890625" Y="4.608137207031" />
                  <Point X="24.068830078125" Y="4.716320800781" />
                  <Point X="24.4240546875" Y="4.75789453125" />
                  <Point X="24.63477734375" Y="4.782556152344" />
                  <Point X="24.682939453125" Y="4.602812011719" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.33983203125" Y="4.641841796875" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.490951171875" Y="4.773197265625" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.121779296875" Y="4.666955078125" />
                  <Point X="26.45359375" Y="4.586845214844" />
                  <Point X="26.64314453125" Y="4.518093261719" />
                  <Point X="26.858265625" Y="4.440067382812" />
                  <Point X="27.0432421875" Y="4.353559570312" />
                  <Point X="27.2504453125" Y="4.256657226562" />
                  <Point X="27.4292109375" Y="4.152509277344" />
                  <Point X="27.629435546875" Y="4.035858398438" />
                  <Point X="27.797978515625" Y="3.915998779297" />
                  <Point X="27.81778125" Y="3.901915527344" />
                  <Point X="27.472810546875" Y="3.304406982422" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.06237890625" Y="2.593110595703" />
                  <Point X="27.0531875" Y="2.573448974609" />
                  <Point X="27.044185546875" Y="2.549571777344" />
                  <Point X="27.041302734375" Y="2.540601318359" />
                  <Point X="27.0311484375" Y="2.502630615234" />
                  <Point X="27.01983203125" Y="2.460314941406" />
                  <Point X="27.0179140625" Y="2.451469726562" />
                  <Point X="27.013646484375" Y="2.420223144531" />
                  <Point X="27.012755859375" Y="2.383240722656" />
                  <Point X="27.013412109375" Y="2.369581298828" />
                  <Point X="27.01737109375" Y="2.336747558594" />
                  <Point X="27.021783203125" Y="2.300156494141" />
                  <Point X="27.02380078125" Y="2.289035644531" />
                  <Point X="27.02914453125" Y="2.267108154297" />
                  <Point X="27.032470703125" Y="2.256301513672" />
                  <Point X="27.04073828125" Y="2.234211669922" />
                  <Point X="27.045322265625" Y="2.223882324219" />
                  <Point X="27.05568359375" Y="2.203841064453" />
                  <Point X="27.0614609375" Y="2.194129150391" />
                  <Point X="27.08177734375" Y="2.164187988281" />
                  <Point X="27.104419921875" Y="2.130820556641" />
                  <Point X="27.10980859375" Y="2.123634277344" />
                  <Point X="27.13046484375" Y="2.100122070312" />
                  <Point X="27.15759765625" Y="2.075386962891" />
                  <Point X="27.1682578125" Y="2.066981201172" />
                  <Point X="27.19819921875" Y="2.046664672852" />
                  <Point X="27.23156640625" Y="2.02402355957" />
                  <Point X="27.24128125" Y="2.018244384766" />
                  <Point X="27.261326171875" Y="2.007882080078" />
                  <Point X="27.27165625" Y="2.003299194336" />
                  <Point X="27.293744140625" Y="1.995033081055" />
                  <Point X="27.304546875" Y="1.991708251953" />
                  <Point X="27.326470703125" Y="1.986365112305" />
                  <Point X="27.337591796875" Y="1.984346557617" />
                  <Point X="27.37042578125" Y="1.980387329102" />
                  <Point X="27.407015625" Y="1.975974975586" />
                  <Point X="27.416044921875" Y="1.975320800781" />
                  <Point X="27.44757421875" Y="1.975497314453" />
                  <Point X="27.484314453125" Y="1.979822875977" />
                  <Point X="27.49775" Y="1.982395996094" />
                  <Point X="27.535720703125" Y="1.992550048828" />
                  <Point X="27.578037109375" Y="2.003865722656" />
                  <Point X="27.584" Y="2.005671875" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572753906" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.422609375" Y="2.482001220703" />
                  <Point X="28.94040625" Y="2.780951416016" />
                  <Point X="29.043953125" Y="2.637044921875" />
                  <Point X="29.136884765625" Y="2.483471435547" />
                  <Point X="28.70946484375" Y="2.155498779297" />
                  <Point X="28.172953125" Y="1.743819946289" />
                  <Point X="28.168138671875" Y="1.739869018555" />
                  <Point X="28.15212109375" Y="1.725217285156" />
                  <Point X="28.134669921875" Y="1.706603393555" />
                  <Point X="28.128578125" Y="1.699423339844" />
                  <Point X="28.10125" Y="1.663772460938" />
                  <Point X="28.070796875" Y="1.624041870117" />
                  <Point X="28.065638671875" Y="1.616604248047" />
                  <Point X="28.0497421875" Y="1.589374267578" />
                  <Point X="28.034765625" Y="1.555549926758" />
                  <Point X="28.030140625" Y="1.542674316406" />
                  <Point X="28.0199609375" Y="1.506274536133" />
                  <Point X="28.008615234375" Y="1.465709960938" />
                  <Point X="28.006224609375" Y="1.454661987305" />
                  <Point X="28.002771484375" Y="1.432363891602" />
                  <Point X="28.001708984375" Y="1.421113647461" />
                  <Point X="28.000892578125" Y="1.397542114258" />
                  <Point X="28.001173828125" Y="1.386237792969" />
                  <Point X="28.003078125" Y="1.36374987793" />
                  <Point X="28.004701171875" Y="1.35256628418" />
                  <Point X="28.01305859375" Y="1.312066772461" />
                  <Point X="28.02237109375" Y="1.26693359375" />
                  <Point X="28.024599609375" Y="1.258235473633" />
                  <Point X="28.03468359375" Y="1.228610595703" />
                  <Point X="28.050287109375" Y="1.195371582031" />
                  <Point X="28.056919921875" Y="1.183526123047" />
                  <Point X="28.0796484375" Y="1.148979980469" />
                  <Point X="28.1049765625" Y="1.110480712891" />
                  <Point X="28.11173828125" Y="1.101425415039" />
                  <Point X="28.126291015625" Y="1.084181274414" />
                  <Point X="28.13408203125" Y="1.075992431641" />
                  <Point X="28.151326171875" Y="1.05990222168" />
                  <Point X="28.16003515625" Y="1.052696044922" />
                  <Point X="28.17824609375" Y="1.039369628906" />
                  <Point X="28.187748046875" Y="1.033249267578" />
                  <Point X="28.220685546875" Y="1.01470880127" />
                  <Point X="28.257390625" Y="0.994046630859" />
                  <Point X="28.265478515625" Y="0.989988037109" />
                  <Point X="28.2946796875" Y="0.978083496094" />
                  <Point X="28.33027734375" Y="0.968020996094" />
                  <Point X="28.343671875" Y="0.965257629395" />
                  <Point X="28.388205078125" Y="0.959371887207" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.444033203125" Y="0.952199829102" />
                  <Point X="28.46571875" Y="0.951222900391" />
                  <Point X="28.49121875" Y="0.952032287598" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="29.247796875" Y="1.051166992188" />
                  <Point X="29.704703125" Y="1.111319946289" />
                  <Point X="29.7526875" Y="0.914206542969" />
                  <Point X="29.782294921875" Y="0.724051757812" />
                  <Point X="29.783873046875" Y="0.713921142578" />
                  <Point X="29.306845703125" Y="0.586102050781" />
                  <Point X="28.6919921875" Y="0.42135269165" />
                  <Point X="28.686029296875" Y="0.419543487549" />
                  <Point X="28.665623046875" Y="0.412136413574" />
                  <Point X="28.642380859375" Y="0.401619140625" />
                  <Point X="28.634005859375" Y="0.397316802979" />
                  <Point X="28.59025390625" Y="0.372027709961" />
                  <Point X="28.54149609375" Y="0.343844512939" />
                  <Point X="28.533880859375" Y="0.338944396973" />
                  <Point X="28.508775390625" Y="0.319870147705" />
                  <Point X="28.48199609375" Y="0.294351593018" />
                  <Point X="28.47280078125" Y="0.284228424072" />
                  <Point X="28.446548828125" Y="0.250778320313" />
                  <Point X="28.417294921875" Y="0.21350088501" />
                  <Point X="28.410859375" Y="0.204214324951" />
                  <Point X="28.3991328125" Y="0.184933181763" />
                  <Point X="28.393841796875" Y="0.174938446045" />
                  <Point X="28.384068359375" Y="0.153475509644" />
                  <Point X="28.38000390625" Y="0.142925964355" />
                  <Point X="28.373158203125" Y="0.121424087524" />
                  <Point X="28.370376953125" Y="0.11047177124" />
                  <Point X="28.361626953125" Y="0.064780776978" />
                  <Point X="28.351875" Y="0.013861235619" />
                  <Point X="28.350603515625" Y="0.004966154575" />
                  <Point X="28.3485859375" Y="-0.026261449814" />
                  <Point X="28.35028125" Y="-0.062939350128" />
                  <Point X="28.351875" Y="-0.076421081543" />
                  <Point X="28.360625" Y="-0.122112220764" />
                  <Point X="28.370376953125" Y="-0.173031768799" />
                  <Point X="28.37316015625" Y="-0.183986618042" />
                  <Point X="28.380005859375" Y="-0.205486404419" />
                  <Point X="28.384068359375" Y="-0.216031341553" />
                  <Point X="28.393841796875" Y="-0.23749546814" />
                  <Point X="28.399130859375" Y="-0.247486938477" />
                  <Point X="28.41085546875" Y="-0.266767791748" />
                  <Point X="28.417291015625" Y="-0.276057159424" />
                  <Point X="28.443541015625" Y="-0.309507110596" />
                  <Point X="28.472796875" Y="-0.346784881592" />
                  <Point X="28.478720703125" Y="-0.353634155273" />
                  <Point X="28.501146484375" Y="-0.375809875488" />
                  <Point X="28.5301796875" Y="-0.398725738525" />
                  <Point X="28.54149609375" Y="-0.40640435791" />
                  <Point X="28.585248046875" Y="-0.431693572998" />
                  <Point X="28.634005859375" Y="-0.459876647949" />
                  <Point X="28.639498046875" Y="-0.462815368652" />
                  <Point X="28.659154296875" Y="-0.472014678955" />
                  <Point X="28.683025390625" Y="-0.481026855469" />
                  <Point X="28.6919921875" Y="-0.483912689209" />
                  <Point X="29.377201171875" Y="-0.667514282227" />
                  <Point X="29.78487890625" Y="-0.776751037598" />
                  <Point X="29.761619140625" Y="-0.93103894043" />
                  <Point X="29.727802734375" Y="-1.079219726563" />
                  <Point X="29.14983984375" Y="-1.003129333496" />
                  <Point X="28.436783203125" Y="-0.909253601074" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042480469" />
                  <Point X="28.36672265625" Y="-0.910840881348" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.26861328125" Y="-0.931340270996" />
                  <Point X="28.17291796875" Y="-0.952139892578" />
                  <Point X="28.157875" Y="-0.956742492676" />
                  <Point X="28.12875390625" Y="-0.9683671875" />
                  <Point X="28.11467578125" Y="-0.975389465332" />
                  <Point X="28.086849609375" Y="-0.992281616211" />
                  <Point X="28.074125" Y="-1.001530273438" />
                  <Point X="28.050375" Y="-1.022000976562" />
                  <Point X="28.039349609375" Y="-1.033223022461" />
                  <Point X="27.987447265625" Y="-1.095645629883" />
                  <Point X="27.92960546875" Y="-1.1652109375" />
                  <Point X="27.92132421875" Y="-1.176851318359" />
                  <Point X="27.90660546875" Y="-1.201233276367" />
                  <Point X="27.90016796875" Y="-1.213974853516" />
                  <Point X="27.88882421875" Y="-1.241360961914" />
                  <Point X="27.884365234375" Y="-1.254927368164" />
                  <Point X="27.877533203125" Y="-1.282576660156" />
                  <Point X="27.87516015625" Y="-1.296659545898" />
                  <Point X="27.867720703125" Y="-1.377499267578" />
                  <Point X="27.859431640625" Y="-1.46758972168" />
                  <Point X="27.859291015625" Y="-1.48332019043" />
                  <Point X="27.861609375" Y="-1.514587036133" />
                  <Point X="27.864068359375" Y="-1.530123291016" />
                  <Point X="27.871798828125" Y="-1.561743652344" />
                  <Point X="27.87678515625" Y="-1.576661865234" />
                  <Point X="27.88915625" Y="-1.605475952148" />
                  <Point X="27.896541015625" Y="-1.619371826172" />
                  <Point X="27.9440625" Y="-1.693288085938" />
                  <Point X="27.997021484375" Y="-1.775662353516" />
                  <Point X="28.00174609375" Y="-1.782358642578" />
                  <Point X="28.019798828125" Y="-1.804454101562" />
                  <Point X="28.0434921875" Y="-1.828121337891" />
                  <Point X="28.052798828125" Y="-1.836277587891" />
                  <Point X="28.688677734375" Y="-2.324205078125" />
                  <Point X="29.087171875" Y="-2.629980957031" />
                  <Point X="29.045474609375" Y="-2.697453369141" />
                  <Point X="29.00127734375" Y="-2.760252929688" />
                  <Point X="28.483529296875" Y="-2.461330322266" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.815029296875" Y="-2.079513916016" />
                  <Point X="27.783123046875" Y="-2.069326416016" />
                  <Point X="27.771111328125" Y="-2.066337646484" />
                  <Point X="27.6689140625" Y="-2.047880737305" />
                  <Point X="27.555021484375" Y="-2.027311889648" />
                  <Point X="27.539361328125" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403442383" />
                  <Point X="27.492314453125" Y="-2.02650378418" />
                  <Point X="27.460140625" Y="-2.031461914062" />
                  <Point X="27.444845703125" Y="-2.035136474609" />
                  <Point X="27.4150703125" Y="-2.044959594727" />
                  <Point X="27.40058984375" Y="-2.051108398438" />
                  <Point X="27.3156875" Y="-2.095791259766" />
                  <Point X="27.221072265625" Y="-2.145587402344" />
                  <Point X="27.208970703125" Y="-2.153169921875" />
                  <Point X="27.1860390625" Y="-2.170063476562" />
                  <Point X="27.175208984375" Y="-2.179374511719" />
                  <Point X="27.15425" Y="-2.200333740234" />
                  <Point X="27.14494140625" Y="-2.211161376953" />
                  <Point X="27.128048828125" Y="-2.234091308594" />
                  <Point X="27.12046484375" Y="-2.246193603516" />
                  <Point X="27.07578125" Y="-2.331094970703" />
                  <Point X="27.025984375" Y="-2.425711669922" />
                  <Point X="27.0198359375" Y="-2.440193359375" />
                  <Point X="27.01001171875" Y="-2.469972167969" />
                  <Point X="27.0063359375" Y="-2.485269287109" />
                  <Point X="27.00137890625" Y="-2.517442871094" />
                  <Point X="27.000279296875" Y="-2.533134033203" />
                  <Point X="27.00068359375" Y="-2.564484130859" />
                  <Point X="27.0021875" Y="-2.580143066406" />
                  <Point X="27.02064453125" Y="-2.682341064453" />
                  <Point X="27.04121484375" Y="-2.796233398438" />
                  <Point X="27.04301953125" Y="-2.804228759766" />
                  <Point X="27.05123828125" Y="-2.831542236328" />
                  <Point X="27.064072265625" Y="-2.862479980469" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.478166015625" Y="-3.581322265625" />
                  <Point X="27.735896484375" Y="-4.027721679687" />
                  <Point X="27.7237578125" Y="-4.036082763672" />
                  <Point X="27.318537109375" Y="-3.507990722656" />
                  <Point X="26.83391796875" Y="-2.876422851562" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849625732422" />
                  <Point X="26.78325390625" Y="-2.828004638672" />
                  <Point X="26.77330078125" Y="-2.820646972656" />
                  <Point X="26.672505859375" Y="-2.755845214844" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.546283203125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663873535156" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.298158203125" Y="-2.656660400391" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079875" Y="-2.699414794922" />
                  <Point X="26.055494140625" Y="-2.714135742188" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="25.958740234375" Y="-2.793189208984" />
                  <Point X="25.863876953125" Y="-2.872063720703" />
                  <Point X="25.852654296875" Y="-2.883088378906" />
                  <Point X="25.83218359375" Y="-2.906838378906" />
                  <Point X="25.822935546875" Y="-2.919563720703" />
                  <Point X="25.80604296875" Y="-2.947390625" />
                  <Point X="25.799021484375" Y="-2.961466552734" />
                  <Point X="25.787396484375" Y="-2.990586669922" />
                  <Point X="25.78279296875" Y="-3.005630859375" />
                  <Point X="25.757341796875" Y="-3.122725341797" />
                  <Point X="25.728978515625" Y="-3.25321875" />
                  <Point X="25.7275859375" Y="-3.261287353516" />
                  <Point X="25.724724609375" Y="-3.289677490234" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520019531" />
                  <Point X="25.833087890625" Y="-4.152315429688" />
                  <Point X="25.655068359375" Y="-3.487937011719" />
                  <Point X="25.652607421875" Y="-3.480121337891" />
                  <Point X="25.642146484375" Y="-3.453579589844" />
                  <Point X="25.6267890625" Y="-3.423815917969" />
                  <Point X="25.62041015625" Y="-3.413209960938" />
                  <Point X="25.5429765625" Y="-3.301642822266" />
                  <Point X="25.456681640625" Y="-3.177309570312" />
                  <Point X="25.446671875" Y="-3.165172851562" />
                  <Point X="25.424787109375" Y="-3.142716552734" />
                  <Point X="25.412912109375" Y="-3.132396972656" />
                  <Point X="25.38665625" Y="-3.113153320312" />
                  <Point X="25.3732421875" Y="-3.104936523438" />
                  <Point X="25.3452421875" Y="-3.090829101562" />
                  <Point X="25.33065625" Y="-3.084938476562" />
                  <Point X="25.21083203125" Y="-3.047749511719" />
                  <Point X="25.077296875" Y="-3.006305175781" />
                  <Point X="25.063376953125" Y="-3.003109130859" />
                  <Point X="25.035216796875" Y="-2.99883984375" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.998840087891" />
                  <Point X="24.948935546875" Y="-3.003109619141" />
                  <Point X="24.935015625" Y="-3.006305664062" />
                  <Point X="24.815193359375" Y="-3.043494384766" />
                  <Point X="24.68165625" Y="-3.084938964844" />
                  <Point X="24.6670703125" Y="-3.090829345703" />
                  <Point X="24.6390703125" Y="-3.104936767578" />
                  <Point X="24.62565625" Y="-3.113153808594" />
                  <Point X="24.599400390625" Y="-3.132397705078" />
                  <Point X="24.587525390625" Y="-3.142717773438" />
                  <Point X="24.565640625" Y="-3.165174560547" />
                  <Point X="24.555630859375" Y="-3.177311279297" />
                  <Point X="24.47819921875" Y="-3.288878173828" />
                  <Point X="24.391904296875" Y="-3.413211669922" />
                  <Point X="24.38753125" Y="-3.420131835938" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213378906" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.146115234375" Y="-4.275884277344" />
                  <Point X="24.014572265625" Y="-4.766805664062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.635580695148" Y="-1.067078443224" />
                  <Point X="29.702011351607" Y="-0.754546776654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.541102192172" Y="-1.05464009027" />
                  <Point X="29.610122460139" Y="-0.729925259431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.446623689197" Y="-1.042201737317" />
                  <Point X="29.518233568671" Y="-0.705303742207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.986172003519" Y="-2.751531838966" />
                  <Point X="29.022548549979" Y="-2.580393643212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.352145186221" Y="-1.029763384363" />
                  <Point X="29.426344677203" Y="-0.680682224984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.71908692166" Y="0.696561752609" />
                  <Point X="29.758040748776" Y="0.879825100535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.899665688804" Y="-2.701587289084" />
                  <Point X="28.939045584747" Y="-2.516319444919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.257666683246" Y="-1.017325031409" />
                  <Point X="29.334455794205" Y="-0.656060667908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.616098951294" Y="0.668966209061" />
                  <Point X="29.707597182736" Y="1.099431543665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.813159374088" Y="-2.651642739203" />
                  <Point X="28.855542619516" Y="-2.452245246627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.16318818027" Y="-1.004886678456" />
                  <Point X="29.242566920946" Y="-0.631439065017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.513110980928" Y="0.641370665513" />
                  <Point X="29.610361768605" Y="1.098899649388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.726653059373" Y="-2.601698189321" />
                  <Point X="28.772039654285" Y="-2.388171048334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.068709674429" Y="-0.992448338982" />
                  <Point X="29.150678047688" Y="-0.606817462126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.410123010562" Y="0.613775121964" />
                  <Point X="29.510443329347" Y="1.085745114315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.640146744658" Y="-2.55175363944" />
                  <Point X="28.688536689034" Y="-2.32409685013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.974231168117" Y="-0.980010001727" />
                  <Point X="29.058789174429" Y="-0.582195859234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.307135040195" Y="0.586179578416" />
                  <Point X="29.41052489009" Y="1.072590579242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.553640429943" Y="-2.501809089559" />
                  <Point X="28.60503371263" Y="-2.260022704403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.879752661805" Y="-0.967571664472" />
                  <Point X="28.96690030117" Y="-0.557574256343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.204147075258" Y="0.558584060407" />
                  <Point X="29.310606450832" Y="1.05943604417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.467134108483" Y="-2.451864571406" />
                  <Point X="28.521530736226" Y="-2.195948558675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.785274155492" Y="-0.955133327217" />
                  <Point X="28.875011427911" Y="-0.532952653451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.101159110335" Y="0.53098854247" />
                  <Point X="29.210688011963" Y="1.046281510923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.380627758184" Y="-2.401920188936" />
                  <Point X="28.438027759821" Y="-2.131874412948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.69079564918" Y="-0.942694989961" />
                  <Point X="28.783122554653" Y="-0.50833105056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.998171145413" Y="0.503393024532" />
                  <Point X="29.11076957375" Y="1.033126980769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.294121407884" Y="-2.351975806466" />
                  <Point X="28.354524783417" Y="-2.06780026722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.596317142868" Y="-0.930256652706" />
                  <Point X="28.691241812886" Y="-0.483671192005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.895183180491" Y="0.475797506595" />
                  <Point X="29.010851135538" Y="1.019972450614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.207615057584" Y="-2.302031423996" />
                  <Point X="28.271021807012" Y="-2.003726121493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.501838636555" Y="-0.917818315451" />
                  <Point X="28.602988061202" Y="-0.441947686703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.792195215568" Y="0.448201988658" />
                  <Point X="28.910932697326" Y="1.006817920459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.121108707285" Y="-2.252087041526" />
                  <Point X="28.187518830608" Y="-1.939651975765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.406769684223" Y="-0.908157808318" />
                  <Point X="28.517226052173" Y="-0.388501453878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.68918481178" Y="0.420500904155" />
                  <Point X="28.811014259114" Y="0.993663390304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.125889371865" Y="2.475034326481" />
                  <Point X="29.130074796147" Y="2.494725199577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.661916813436" Y="-3.955490288603" />
                  <Point X="27.670602102605" Y="-3.914629215669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.034602356985" Y="-2.202142659056" />
                  <Point X="28.104015854204" Y="-1.875577830038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.306468668505" Y="-0.923112224127" />
                  <Point X="28.438311039396" Y="-0.302842636329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.580569278932" Y="0.366429760714" />
                  <Point X="28.711095820901" Y="0.980508860149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.009839059901" Y="2.385985297351" />
                  <Point X="29.058199327231" Y="2.613502467133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.585862202376" Y="-3.856374339013" />
                  <Point X="27.599614344426" Y="-3.791675597454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.948096006685" Y="-2.152198276586" />
                  <Point X="28.021623825415" Y="-1.806277086707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.204641928452" Y="-0.945244608581" />
                  <Point X="28.369620447946" Y="-0.169081698353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.463438942877" Y="0.27229961773" />
                  <Point X="28.611177382689" Y="0.967354329995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.893788747937" Y="2.296936268222" />
                  <Point X="28.983839069432" Y="2.720589722096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.509807591315" Y="-3.757258389423" />
                  <Point X="27.528626586246" Y="-3.66872197924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.861589656386" Y="-2.102253894116" />
                  <Point X="27.947410961065" Y="-1.698496400088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.099102607386" Y="-0.984843313449" />
                  <Point X="28.511258944477" Y="0.95419979984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.777738435973" Y="2.207887239093" />
                  <Point X="28.893831331666" Y="2.754061371667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.433752980254" Y="-3.658142439834" />
                  <Point X="27.457638873553" Y="-3.54576814703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.772051794083" Y="-2.066571654294" />
                  <Point X="27.876418501651" Y="-1.575564899444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.975331152181" Y="-1.110217465552" />
                  <Point X="28.414497366364" Y="0.955897128762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.661688200148" Y="2.118838568167" />
                  <Point X="28.783122877944" Y="2.690143809663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.357698369194" Y="-3.559026490244" />
                  <Point X="27.386651272674" Y="-3.422813788773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.678533061565" Y="-2.049617936349" />
                  <Point X="28.320537241911" Y="0.970774260917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.545638073125" Y="2.029790409117" />
                  <Point X="28.672414424222" Y="2.62622624766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.281643785204" Y="-3.459910413294" />
                  <Point X="27.315663671795" Y="-3.299859430515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.585001158941" Y="-2.032726178877" />
                  <Point X="28.231463806969" Y="1.008641459687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.429587946102" Y="1.940742250068" />
                  <Point X="28.561705970501" Y="2.562308685657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.20558922995" Y="-3.360794201155" />
                  <Point X="27.244676070917" Y="-3.176905072257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.489095989546" Y="-2.026999763719" />
                  <Point X="28.146245020961" Y="1.06464335589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.313537819079" Y="1.851694091018" />
                  <Point X="28.450997516779" Y="2.498391123654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.129534674696" Y="-3.261677989016" />
                  <Point X="27.173688470038" Y="-3.053950713999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.385118473362" Y="-2.059250754314" />
                  <Point X="28.070125566476" Y="1.163454241154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.197487692057" Y="1.762645931969" />
                  <Point X="28.340289068651" Y="2.434473587969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.053480119442" Y="-3.162561776877" />
                  <Point X="27.10270086916" Y="-2.930996355742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.275762870521" Y="-2.11680365333" />
                  <Point X="28.008887820758" Y="1.332278061564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.070945289849" Y="1.624235499166" />
                  <Point X="28.229580622452" Y="2.370556061361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.977425564187" Y="-3.063445564738" />
                  <Point X="27.038003083976" Y="-2.778450741182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.162676848815" Y="-2.191906793259" />
                  <Point X="28.118872176254" Y="2.306638534753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.901371008933" Y="-2.964329352598" />
                  <Point X="28.008163730055" Y="2.242721008144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.825059315148" Y="-2.866422882136" />
                  <Point X="27.897455283857" Y="2.178803481536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.741951004001" Y="-2.800491982355" />
                  <Point X="27.786746837659" Y="2.114885954928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.656505150579" Y="-2.745558354345" />
                  <Point X="27.67603839146" Y="2.05096842832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.571059210548" Y="-2.690625133797" />
                  <Point X="27.568353647859" Y="2.001276303995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.481763742025" Y="-2.653802520898" />
                  <Point X="27.4662183689" Y="1.977692358113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.3857471324" Y="-2.648600390798" />
                  <Point X="27.369687762146" Y="1.980476321843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.754982175274" Y="3.793144018859" />
                  <Point X="27.783312652092" Y="3.926428433114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.286687195135" Y="-2.657715991547" />
                  <Point X="27.276992107604" Y="2.001302317217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.601268682524" Y="3.526903655385" />
                  <Point X="27.698943528921" Y="3.986427678681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.187627226409" Y="-2.66683174031" />
                  <Point X="27.19060685458" Y="2.051816417576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.447555196407" Y="3.260663323116" />
                  <Point X="27.614213077924" Y="4.044727010482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.800559467123" Y="-4.030917612305" />
                  <Point X="25.810777035315" Y="-3.982847733344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.0840249781" Y="-2.697317234366" />
                  <Point X="27.108983686386" Y="2.124734365612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.293841744027" Y="2.994423149569" />
                  <Point X="27.527792637728" Y="4.095075568213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.746400180975" Y="-3.828792257874" />
                  <Point X="25.773629830905" Y="-3.700686826942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.968195476653" Y="-2.785327431685" />
                  <Point X="27.037162318133" Y="2.243766156775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.140128291647" Y="2.728182976022" />
                  <Point X="27.441372197532" Y="4.145424125944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.692240894827" Y="-3.626666903444" />
                  <Point X="25.736482626495" Y="-3.418525920541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.849520597082" Y="-2.886724080603" />
                  <Point X="27.354951684812" Y="4.195772342477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.634896146775" Y="-3.439527969" />
                  <Point X="27.268531160215" Y="4.246120503132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.561430573225" Y="-3.328231555578" />
                  <Point X="27.180592263923" Y="4.289325286589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.487078596983" Y="-3.221105338956" />
                  <Point X="27.092251506664" Y="4.330639462845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.409364468019" Y="-3.129796807264" />
                  <Point X="27.00391075469" Y="4.371953663969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.322326551134" Y="-3.082353248956" />
                  <Point X="26.915570009304" Y="4.413267896079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.231214802313" Y="-3.054075563033" />
                  <Point X="26.826586392345" Y="4.451557655235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.140103054325" Y="-3.025797873195" />
                  <Point X="26.736415801194" Y="4.484263139869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.048289533101" Y="-3.000821766853" />
                  <Point X="26.646245210043" Y="4.516968624502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.950738998374" Y="-3.002836186974" />
                  <Point X="26.556074673272" Y="4.549674364971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.847077907202" Y="-3.033598514931" />
                  <Point X="26.465904138438" Y="4.582380114551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.743095918281" Y="-3.065870548102" />
                  <Point X="26.373824427813" Y="4.606103898226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.637460125068" Y="-3.105923118743" />
                  <Point X="26.281442859349" Y="4.628407552421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.511713617318" Y="-3.240589162512" />
                  <Point X="26.189061290885" Y="4.650711206616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.367980587592" Y="-3.459875139137" />
                  <Point X="26.096679735348" Y="4.673014921625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.993913928631" Y="-4.762795643086" />
                  <Point X="26.004298214461" Y="4.695318799651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.901161082288" Y="-4.742238713981" />
                  <Point X="25.911916693574" Y="4.717622677678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.866684825973" Y="-4.447511984755" />
                  <Point X="25.819298001874" Y="4.738810754754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.819733155962" Y="-4.211477462427" />
                  <Point X="25.724290534186" Y="4.748760524394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.751929944206" Y="-4.07354173122" />
                  <Point X="25.629283066497" Y="4.758710294034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.670500595826" Y="-3.999711932656" />
                  <Point X="25.534275598809" Y="4.768660063674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.588638075183" Y="-3.92792004936" />
                  <Point X="25.439268167956" Y="4.778610006608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.506775594793" Y="-3.856127976686" />
                  <Point X="25.22601075087" Y="4.232237503865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.419669543524" Y="-3.809004965453" />
                  <Point X="25.102093912886" Y="4.106179379564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.325796303786" Y="-3.793719072848" />
                  <Point X="25.000493818948" Y="4.08511328125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.230008426106" Y="-3.787440843558" />
                  <Point X="24.908049102305" Y="4.107119846618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.13422055293" Y="-3.781162593072" />
                  <Point X="24.824352800975" Y="4.17028447008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.038167752115" Y="-3.776130729134" />
                  <Point X="24.758905109777" Y="4.319302054227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.935927696545" Y="-3.800207610215" />
                  <Point X="24.704745997819" Y="4.521428228156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.824054171106" Y="-3.869606403697" />
                  <Point X="24.65058682726" Y="4.723554126392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.71085438034" Y="-3.945244784969" />
                  <Point X="24.564251315435" Y="4.774302240693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.594444941281" Y="-4.035983374244" />
                  <Point X="24.464651283677" Y="4.762645695128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.473501229179" Y="-4.148054040999" />
                  <Point X="24.365051232641" Y="4.750989058869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.387674598435" Y="-4.094911829442" />
                  <Point X="24.265451168341" Y="4.73933236021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.301847967691" Y="-4.041769617885" />
                  <Point X="22.537298013061" Y="-2.934064245158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.638567616759" Y="-2.457628218427" />
                  <Point X="24.165851104041" Y="4.72767566155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.217023451014" Y="-3.983912830314" />
                  <Point X="22.383584911117" Y="-3.200302770038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.557163446586" Y="-2.383679965708" />
                  <Point X="24.066155829853" Y="4.715571035582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.133560659202" Y="-3.919649630944" />
                  <Point X="22.22987161068" Y="-3.466542228748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.467108862894" Y="-2.350428708892" />
                  <Point X="23.962878848182" Y="4.686615800748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.050097910952" Y="-3.85538622663" />
                  <Point X="22.076158110234" Y="-3.732782628431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.368390506266" Y="-2.357937299094" />
                  <Point X="23.859601866511" Y="4.657660565914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.260300829096" Y="-2.409534486079" />
                  <Point X="23.756324884841" Y="4.62870533108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.149592378953" Y="-2.473452031246" />
                  <Point X="23.653047891278" Y="4.599750040302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.03888392881" Y="-2.537369576413" />
                  <Point X="23.466486881111" Y="4.178974257364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.531179518326" Y="4.483329186266" />
                  <Point X="23.549770868555" Y="4.570794612329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.928175478666" Y="-2.60128712158" />
                  <Point X="23.349202076744" Y="4.084117398104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.817467028523" Y="-2.665204666747" />
                  <Point X="23.243020248331" Y="4.041495933825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.70675857838" Y="-2.729122211914" />
                  <Point X="21.952416644305" Y="-1.573391878326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.023239972447" Y="-1.240194316294" />
                  <Point X="23.143479783986" Y="4.030119630908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.596050126687" Y="-2.793039764373" />
                  <Point X="21.836347085447" Y="-1.662531456972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.944553664803" Y="-1.153459525693" />
                  <Point X="23.052210751387" Y="4.05765735483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.485341671989" Y="-2.856957330968" />
                  <Point X="21.720296849786" Y="-1.751580127126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.856520337305" Y="-1.110699006125" />
                  <Point X="22.9647643223" Y="4.103179014331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.374633217291" Y="-2.920874897563" />
                  <Point X="21.604246614124" Y="-1.84062879728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.761691841279" Y="-1.099907241015" />
                  <Point X="22.882089253845" Y="4.171148160724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.263924762593" Y="-2.984792464158" />
                  <Point X="21.488196378463" Y="-1.929677467434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.66188343275" Y="-1.11254412221" />
                  <Point X="22.805367307332" Y="4.267124543854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.170088803499" Y="-2.969331179914" />
                  <Point X="21.372146142802" Y="-2.018726137588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.561965002184" Y="-1.125698616389" />
                  <Point X="22.6952399633" Y="4.205940887995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.094167925796" Y="-2.86958606434" />
                  <Point X="21.256095958254" Y="-2.107774567272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.462046571619" Y="-1.138853110569" />
                  <Point X="21.666636262239" Y="-0.176334291989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.703823737649" Y="-0.001380975477" />
                  <Point X="22.58511255067" Y="4.144756909404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.018247048094" Y="-2.769840948766" />
                  <Point X="21.140045796955" Y="-2.196822887577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.362128141053" Y="-1.152007604749" />
                  <Point X="21.545656996058" Y="-0.288572227536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.639611160496" Y="0.153447363392" />
                  <Point X="22.227613452468" Y="2.919780650644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.251501876974" Y="3.032166851844" />
                  <Point X="22.474985079758" Y="4.083572656626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.945378440793" Y="-2.655736029959" />
                  <Point X="21.023995635656" Y="-2.285871207882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.262209710488" Y="-1.165162098929" />
                  <Point X="21.440607836201" Y="-0.325864905224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.556567307514" Y="0.219681514994" />
                  <Point X="22.099760584201" Y="2.775204959763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.200597927254" Y="3.249607360051" />
                  <Point X="22.364857608847" Y="4.022388403847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.873776685523" Y="-2.535671040942" />
                  <Point X="20.907945474358" Y="-2.374919528187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.162291279922" Y="-1.178316593109" />
                  <Point X="21.337619856092" Y="-0.353460494607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.467202664924" Y="0.256178689493" />
                  <Point X="21.992453674384" Y="2.727290403635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.12961029234" Y="3.372561558182" />
                  <Point X="22.249535152881" Y="3.936763667961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.062372849357" Y="-1.191471087289" />
                  <Point X="21.234631875983" Y="-0.381056083991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.375313783554" Y="0.280800254226" />
                  <Point X="21.89521818629" Y="2.726758161388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.058622657425" Y="3.495515756313" />
                  <Point X="22.133503711124" Y="3.847803416176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.962454418791" Y="-1.204625581468" />
                  <Point X="21.131643895875" Y="-0.408651673374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.283424902184" Y="0.305421818958" />
                  <Point X="21.496412213932" Y="1.307448338742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.561124996509" Y="1.611898044124" />
                  <Point X="21.800972991075" Y="2.740294141056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.98763507983" Y="3.618470224112" />
                  <Point X="22.01747218517" Y="3.758842768274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.862535985397" Y="-1.217780088957" />
                  <Point X="21.028655915766" Y="-0.436247262758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.191536020815" Y="0.33004338369" />
                  <Point X="21.383456098303" Y="1.232956358858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.490318839482" Y="1.735706028586" />
                  <Point X="21.712625657304" Y="2.781577377256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.762617546805" Y="-1.230934620895" />
                  <Point X="20.925667935657" Y="-0.463842852141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.099647139445" Y="0.354664948423" />
                  <Point X="21.280651100606" Y="1.206221634036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.40822029183" Y="1.806387492108" />
                  <Point X="21.626119311079" Y="2.831521778896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.662699108214" Y="-1.244089152833" />
                  <Point X="20.822679955548" Y="-0.491438441524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.007758258075" Y="0.379286513155" />
                  <Point X="21.184446891143" Y="1.210541176288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.324717346997" Y="1.87046178637" />
                  <Point X="21.539612964854" Y="2.881466180536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.562780669623" Y="-1.257243684771" />
                  <Point X="20.719691983386" Y="-0.51903399352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.915869376705" Y="0.403908077887" />
                  <Point X="21.089968377476" Y="1.222979478942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.241214402165" Y="1.934536080632" />
                  <Point X="21.453106618629" Y="2.931410582176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.462862231031" Y="-1.270398216709" />
                  <Point X="20.616704014922" Y="-0.546629528122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.823980495336" Y="0.428529642619" />
                  <Point X="20.995489863809" Y="1.235417781595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.157711457332" Y="1.998610374893" />
                  <Point X="21.366600344136" Y="2.981355321287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.36294379244" Y="-1.283552748647" />
                  <Point X="20.513716046457" Y="-0.574225062724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.732091613966" Y="0.453151207352" />
                  <Point X="20.901011350141" Y="1.247856084248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.0742085125" Y="2.062684669155" />
                  <Point X="21.280094086176" Y="3.031300138182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.298731470728" Y="-1.128723208023" />
                  <Point X="20.410728077993" Y="-0.601820597326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.640202732796" Y="0.477772773024" />
                  <Point X="20.806532836474" Y="1.260294386902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.990705558073" Y="2.126758918279" />
                  <Point X="21.149965923415" Y="2.876020028312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.249277539098" Y="-0.904460901052" />
                  <Point X="20.307740109528" Y="-0.629416131927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.54831385169" Y="0.502394338998" />
                  <Point X="20.712054323112" Y="1.272732690992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.907202564436" Y="2.190832982937" />
                  <Point X="21.016332991706" Y="2.704251276928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.456424970584" Y="0.527015904973" />
                  <Point X="20.617575813651" Y="1.285171013431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.8236995708" Y="2.254907047594" />
                  <Point X="20.866249724854" Y="2.455089783519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.364536089479" Y="0.551637470948" />
                  <Point X="20.523097304189" Y="1.297609335871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.272647208373" Y="0.576259036922" />
                  <Point X="20.428618794728" Y="1.31004765831" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.6609921875" Y="-4.244154296875" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521543945312" />
                  <Point X="25.38688671875" Y="-3.409976806641" />
                  <Point X="25.300591796875" Y="-3.285643554688" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.15451171875" Y="-3.2292109375" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.871513671875" Y="-3.224955322266" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643798828" />
                  <Point X="24.6342890625" Y="-3.397210693359" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.329642578125" Y="-4.325060058594" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.056259765625" Y="-4.968443359375" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.7613671875" Y="-4.902459472656" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.668044921875" Y="-4.724263671875" />
                  <Point X="23.6908515625" Y="-4.5510390625" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.66169140625" Y="-4.390845214844" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.5033984375" Y="-4.105881835938" />
                  <Point X="23.38045703125" Y="-3.998064208984" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.204341796875" Y="-3.976166259766" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791015625" />
                  <Point X="22.888119140625" Y="-4.055310791016" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.621734375" Y="-4.31176953125" />
                  <Point X="22.54290625" Y="-4.414500488281" />
                  <Point X="22.3735703125" Y="-4.30965234375" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.9525390625" Y="-4.020064208984" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.141892578125" Y="-3.238929199219" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597594970703" />
                  <Point X="22.4860234375" Y="-2.568766601562" />
                  <Point X="22.46867578125" Y="-2.55141796875" />
                  <Point X="22.439845703125" Y="-2.537198974609" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.728728515625" Y="-2.935831054688" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="21.019537109375" Y="-3.085243408203" />
                  <Point X="20.838302734375" Y="-2.847136962891" />
                  <Point X="20.700916015625" Y="-2.616760986328" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="21.22019921875" Y="-1.895829711914" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.396012939453" />
                  <Point X="21.8618828125" Y="-1.366265625" />
                  <Point X="21.859673828125" Y="-1.334596069336" />
                  <Point X="21.83884375" Y="-1.310640014648" />
                  <Point X="21.812361328125" Y="-1.295053466797" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.9225546875" Y="-1.401518066406" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.143494140625" Y="-1.288702758789" />
                  <Point X="20.072607421875" Y="-1.0111875" />
                  <Point X="20.036259765625" Y="-0.757051818848" />
                  <Point X="20.00160546875" Y="-0.514742248535" />
                  <Point X="20.7408046875" Y="-0.316674346924" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.471228515625" Y="-0.112316635132" />
                  <Point X="21.497677734375" Y="-0.090645294189" />
                  <Point X="21.509478515625" Y="-0.061808555603" />
                  <Point X="21.516599609375" Y="-0.031280040741" />
                  <Point X="21.509978515625" Y="-0.00236456275" />
                  <Point X="21.497677734375" Y="0.028083959579" />
                  <Point X="21.472732421875" Y="0.048712276459" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.660505859375" Y="0.275630187988" />
                  <Point X="20.001814453125" Y="0.452126037598" />
                  <Point X="20.036671875" Y="0.687690002441" />
                  <Point X="20.08235546875" Y="0.996414855957" />
                  <Point X="20.1555234375" Y="1.266428100586" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.74371484375" Y="1.460204101562" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866699219" />
                  <Point X="21.29734765625" Y="1.405026367188" />
                  <Point X="21.32972265625" Y="1.41523425293" />
                  <Point X="21.348466796875" Y="1.426056396484" />
                  <Point X="21.360880859375" Y="1.443786376953" />
                  <Point X="21.372537109375" Y="1.471928466797" />
                  <Point X="21.38552734375" Y="1.503290527344" />
                  <Point X="21.389287109375" Y="1.524606323242" />
                  <Point X="21.38368359375" Y="1.545513061523" />
                  <Point X="21.369619140625" Y="1.572532104492" />
                  <Point X="21.353943359375" Y="1.602642822266" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="20.89145703125" Y="1.963426025391" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.662474609375" Y="2.482886230469" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="21.033802734375" Y="3.036130859375" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.53190234375" Y="3.105310546875" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.901947265625" Y="2.916895019531" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775146484" />
                  <Point X="21.986748046875" Y="2.927404052734" />
                  <Point X="22.015466796875" Y="2.956122558594" />
                  <Point X="22.04747265625" Y="2.988127441406" />
                  <Point X="22.0591015625" Y="3.006381347656" />
                  <Point X="22.061927734375" Y="3.027839599609" />
                  <Point X="22.05838671875" Y="3.068299560547" />
                  <Point X="22.054443359375" Y="3.113389160156" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.84915625" Y="3.478325439453" />
                  <Point X="21.69272265625" Y="3.749276611328" />
                  <Point X="21.937955078125" Y="3.937294921875" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.552380859375" Y="4.343924804688" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.93759765625" Y="4.410827148438" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.09378515625" Y="4.25021875" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.233095703125" Y="4.241678710938" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.30309765625" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.29448828125" />
                  <Point X="23.32918359375" Y="4.34290625" />
                  <Point X="23.346197265625" Y="4.396864746094" />
                  <Point X="23.348083984375" Y="4.418426269531" />
                  <Point X="23.325484375" Y="4.590081054688" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.631669921875" Y="4.791083496094" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.40196875" Y="4.946606445312" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.866466796875" Y="4.651987792969" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.1563046875" Y="4.691017578125" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.510740234375" Y="4.9621640625" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.166369140625" Y="4.8516484375" />
                  <Point X="26.508455078125" Y="4.76905859375" />
                  <Point X="26.7079296875" Y="4.696707519531" />
                  <Point X="26.93103515625" Y="4.615785644531" />
                  <Point X="27.12373046875" Y="4.52566796875" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.52485546875" Y="4.316680175781" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="27.908091796875" Y="4.070837890625" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.637353515625" Y="3.209406982422" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491515380859" />
                  <Point X="27.21469921875" Y="2.453544677734" />
                  <Point X="27.2033828125" Y="2.411229003906" />
                  <Point X="27.202044921875" Y="2.392325927734" />
                  <Point X="27.20600390625" Y="2.3594921875" />
                  <Point X="27.210416015625" Y="2.322901123047" />
                  <Point X="27.21868359375" Y="2.300811279297" />
                  <Point X="27.239" Y="2.270870117188" />
                  <Point X="27.261642578125" Y="2.237502685547" />
                  <Point X="27.27494140625" Y="2.224203857422" />
                  <Point X="27.3048828125" Y="2.203887451172" />
                  <Point X="27.33825" Y="2.181246337891" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.393171875" Y="2.169020996094" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4486640625" Y="2.165946289062" />
                  <Point X="27.486634765625" Y="2.176100341797" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.327609375" Y="2.646546142578" />
                  <Point X="28.994248046875" Y="3.031430664062" />
                  <Point X="29.07941015625" Y="2.913075927734" />
                  <Point X="29.20259765625" Y="2.741874267578" />
                  <Point X="29.30046875" Y="2.580139404297" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.82512890625" Y="2.004761962891" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583832275391" />
                  <Point X="28.25204296875" Y="1.548181396484" />
                  <Point X="28.22158984375" Y="1.508450927734" />
                  <Point X="28.21312109375" Y="1.491501464844" />
                  <Point X="28.20294140625" Y="1.455101806641" />
                  <Point X="28.191595703125" Y="1.414536987305" />
                  <Point X="28.190779296875" Y="1.390965454102" />
                  <Point X="28.19913671875" Y="1.350466186523" />
                  <Point X="28.20844921875" Y="1.305332763672" />
                  <Point X="28.215646484375" Y="1.287955810547" />
                  <Point X="28.238375" Y="1.253409667969" />
                  <Point X="28.263703125" Y="1.214910400391" />
                  <Point X="28.280947265625" Y="1.19882019043" />
                  <Point X="28.313884765625" Y="1.180279541016" />
                  <Point X="28.35058984375" Y="1.159617675781" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.413099609375" Y="1.147734008789" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.22299609375" Y="1.239541625977" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.886283203125" Y="1.168707763672" />
                  <Point X="29.93919140625" Y="0.951367858887" />
                  <Point X="29.970033203125" Y="0.753282409668" />
                  <Point X="29.997859375" Y="0.574556396484" />
                  <Point X="29.356021484375" Y="0.40257623291" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819198608" />
                  <Point X="28.6853359375" Y="0.207530014038" />
                  <Point X="28.636578125" Y="0.179346832275" />
                  <Point X="28.622265625" Y="0.166925933838" />
                  <Point X="28.596013671875" Y="0.133475875854" />
                  <Point X="28.566759765625" Y="0.096198257446" />
                  <Point X="28.556986328125" Y="0.074735412598" />
                  <Point X="28.548236328125" Y="0.029044403076" />
                  <Point X="28.538484375" Y="-0.021875146866" />
                  <Point X="28.538484375" Y="-0.040684932709" />
                  <Point X="28.547234375" Y="-0.086376091003" />
                  <Point X="28.556986328125" Y="-0.13729548645" />
                  <Point X="28.566759765625" Y="-0.15875970459" />
                  <Point X="28.593009765625" Y="-0.192209609985" />
                  <Point X="28.622265625" Y="-0.229487380981" />
                  <Point X="28.636578125" Y="-0.241906906128" />
                  <Point X="28.680330078125" Y="-0.267196228027" />
                  <Point X="28.729087890625" Y="-0.295379272461" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.426376953125" Y="-0.483988372803" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.97797265625" Y="-0.770463928223" />
                  <Point X="29.948431640625" Y="-0.966412719727" />
                  <Point X="29.90891796875" Y="-1.139562988281" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="29.1250390625" Y="-1.19150378418" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.30896875" Y="-1.117005249023" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.185447265625" Y="-1.154697143555" />
                  <Point X="28.133544921875" Y="-1.217119750977" />
                  <Point X="28.075703125" Y="-1.286685180664" />
                  <Point X="28.064359375" Y="-1.314071166992" />
                  <Point X="28.056919921875" Y="-1.394911010742" />
                  <Point X="28.048630859375" Y="-1.485001464844" />
                  <Point X="28.056361328125" Y="-1.516621826172" />
                  <Point X="28.1038828125" Y="-1.590538085938" />
                  <Point X="28.156841796875" Y="-1.672912353516" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="28.804341796875" Y="-2.173467773437" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.287404296875" Y="-2.667395019531" />
                  <Point X="29.204134765625" Y="-2.802138183594" />
                  <Point X="29.122412109375" Y="-2.918255615234" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.388529296875" Y="-2.625875244141" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.63514453125" Y="-2.234855957031" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219245117188" />
                  <Point X="27.40417578125" Y="-2.263927978516" />
                  <Point X="27.309560546875" Y="-2.313724121094" />
                  <Point X="27.2886015625" Y="-2.334683349609" />
                  <Point X="27.24391796875" Y="-2.419584716797" />
                  <Point X="27.19412109375" Y="-2.514201416016" />
                  <Point X="27.1891640625" Y="-2.546375" />
                  <Point X="27.20762109375" Y="-2.648572998047" />
                  <Point X="27.22819140625" Y="-2.762465332031" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.642708984375" Y="-3.486322265625" />
                  <Point X="27.98667578125" Y="-4.082087646484" />
                  <Point X="27.934107421875" Y="-4.119634765625" />
                  <Point X="27.83529296875" Y="-4.190215820312" />
                  <Point X="27.7439296875" Y="-4.249354003906" />
                  <Point X="27.67977734375" Y="-4.29087890625" />
                  <Point X="27.167798828125" Y="-3.623655273438" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.569755859375" Y="-2.915665527344" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.315568359375" Y="-2.845861083984" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.080212890625" Y="-2.939284912109" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045986328125" />
                  <Point X="25.943005859375" Y="-3.163080810547" />
                  <Point X="25.914642578125" Y="-3.29357421875" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="26.029728515625" Y="-4.190298339844" />
                  <Point X="26.127642578125" Y="-4.934028320312" />
                  <Point X="26.087828125" Y="-4.942755859375" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.9099375" Y="-4.978581054688" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#164" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.091393266905" Y="4.696208806539" Z="1.2" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.2" />
                  <Point X="-0.610000441139" Y="5.027547843578" Z="1.2" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.2" />
                  <Point X="-1.387985882063" Y="4.870507317709" Z="1.2" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.2" />
                  <Point X="-1.730244645039" Y="4.614835297266" Z="1.2" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.2" />
                  <Point X="-1.724658965457" Y="4.389222120976" Z="1.2" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.2" />
                  <Point X="-1.792195649252" Y="4.319152766711" Z="1.2" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.2" />
                  <Point X="-1.889283585606" Y="4.32584918069" Z="1.2" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.2" />
                  <Point X="-2.028891352471" Y="4.472545417174" Z="1.2" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.2" />
                  <Point X="-2.478059383287" Y="4.418912438217" Z="1.2" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.2" />
                  <Point X="-3.098622815166" Y="4.008254975894" Z="1.2" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.2" />
                  <Point X="-3.200302147543" Y="3.484605551556" Z="1.2" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.2" />
                  <Point X="-2.997579821421" Y="3.09522334871" Z="1.2" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.2" />
                  <Point X="-3.026044905214" Y="3.022758662673" Z="1.2" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.2" />
                  <Point X="-3.099853116897" Y="2.997984877752" Z="1.2" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.2" />
                  <Point X="-3.449253796741" Y="3.179891801203" Z="1.2" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.2" />
                  <Point X="-4.011816884616" Y="3.09811334553" Z="1.2" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.2" />
                  <Point X="-4.386864304291" Y="2.539371323255" Z="1.2" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.2" />
                  <Point X="-4.145137956612" Y="1.955038298069" Z="1.2" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.2" />
                  <Point X="-3.68088762962" Y="1.580723374949" Z="1.2" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.2" />
                  <Point X="-3.679813107807" Y="1.522342116798" Z="1.2" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.2" />
                  <Point X="-3.723845122607" Y="1.483992254453" Z="1.2" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.2" />
                  <Point X="-4.255915984194" Y="1.541056382777" Z="1.2" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.2" />
                  <Point X="-4.898893606838" Y="1.310785342457" Z="1.2" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.2" />
                  <Point X="-5.018946478795" Y="0.726288997638" Z="1.2" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.2" />
                  <Point X="-4.358593198422" Y="0.258614002137" Z="1.2" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.2" />
                  <Point X="-3.56193333115" Y="0.038916777992" Z="1.2" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.2" />
                  <Point X="-3.543931974129" Y="0.01409692781" Z="1.2" />
                  <Point X="-3.539556741714" Y="0" Z="1.2" />
                  <Point X="-3.544432627006" Y="-0.015710023251" Z="1.2" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.2" />
                  <Point X="-3.563435263974" Y="-0.039959205142" Z="1.2" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.2" />
                  <Point X="-4.27829459035" Y="-0.237098054619" Z="1.2" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.2" />
                  <Point X="-5.019392838714" Y="-0.732850733887" Z="1.2" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.2" />
                  <Point X="-4.911109031045" Y="-1.269796938582" Z="1.2" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.2" />
                  <Point X="-4.077076320865" Y="-1.419810259065" Z="1.2" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.2" />
                  <Point X="-3.205201003356" Y="-1.31507831138" Z="1.2" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.2" />
                  <Point X="-3.1967370303" Y="-1.338128422235" Z="1.2" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.2" />
                  <Point X="-3.816395755522" Y="-1.824882049022" Z="1.2" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.2" />
                  <Point X="-4.348184671138" Y="-2.611090718426" Z="1.2" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.2" />
                  <Point X="-4.026404252384" Y="-3.084246546519" Z="1.2" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.2" />
                  <Point X="-3.252429111138" Y="-2.947852210946" Z="1.2" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.2" />
                  <Point X="-2.563695799916" Y="-2.564634669216" Z="1.2" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.2" />
                  <Point X="-2.907564802223" Y="-3.182649271355" Z="1.2" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.2" />
                  <Point X="-3.084121435472" Y="-4.028401243637" Z="1.2" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.2" />
                  <Point X="-2.658908030106" Y="-4.320882989583" Z="1.2" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.2" />
                  <Point X="-2.344755472855" Y="-4.310927593729" Z="1.2" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.2" />
                  <Point X="-2.090258814738" Y="-4.065604164473" Z="1.2" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.2" />
                  <Point X="-1.805084235309" Y="-3.994779221658" Z="1.2" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.2" />
                  <Point X="-1.535724597064" Y="-4.112193542852" Z="1.2" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.2" />
                  <Point X="-1.393504442203" Y="-4.369320212965" Z="1.2" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.2" />
                  <Point X="-1.387683991658" Y="-4.686456795539" Z="1.2" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.2" />
                  <Point X="-1.257249159347" Y="-4.919602072349" Z="1.2" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.2" />
                  <Point X="-0.959424449165" Y="-4.986247540239" Z="1.2" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.2" />
                  <Point X="-0.628216778841" Y="-4.306721017647" Z="1.2" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.2" />
                  <Point X="-0.330792614" Y="-3.394439637576" Z="1.2" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.2" />
                  <Point X="-0.119823884185" Y="-3.241428280176" Z="1.2" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.2" />
                  <Point X="0.133535195176" Y="-3.245683765113" Z="1.2" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.2" />
                  <Point X="0.339653245432" Y="-3.407206118192" Z="1.2" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.2" />
                  <Point X="0.60653808156" Y="-4.225815012168" Z="1.2" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.2" />
                  <Point X="0.912719004389" Y="-4.996495667003" Z="1.2" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.2" />
                  <Point X="1.092376294781" Y="-4.960316374327" Z="1.2" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.2" />
                  <Point X="1.073144446638" Y="-4.152491759599" Z="1.2" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.2" />
                  <Point X="0.985709061783" Y="-3.142420367028" Z="1.2" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.2" />
                  <Point X="1.106022072133" Y="-2.946451324639" Z="1.2" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.2" />
                  <Point X="1.313994208098" Y="-2.864370629039" Z="1.2" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.2" />
                  <Point X="1.536559094141" Y="-2.926443524547" Z="1.2" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.2" />
                  <Point X="2.121973073029" Y="-3.622813147153" Z="1.2" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.2" />
                  <Point X="2.76494277905" Y="-4.260047951547" Z="1.2" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.2" />
                  <Point X="2.95701371907" Y="-4.12904194434" Z="1.2" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.2" />
                  <Point X="2.679852972731" Y="-3.430042130731" Z="1.2" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.2" />
                  <Point X="2.250668004195" Y="-2.608406200143" Z="1.2" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.2" />
                  <Point X="2.282007169055" Y="-2.411591603426" Z="1.2" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.2" />
                  <Point X="2.421306774947" Y="-2.276894140912" Z="1.2" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.2" />
                  <Point X="2.62010060337" Y="-2.252780005296" Z="1.2" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.2" />
                  <Point X="3.357371466376" Y="-2.637896527544" Z="1.2" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.2" />
                  <Point X="4.157143320125" Y="-2.915753001448" Z="1.2" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.2" />
                  <Point X="4.323780946071" Y="-2.662400081705" Z="1.2" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.2" />
                  <Point X="3.828621499006" Y="-2.102519962348" Z="1.2" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.2" />
                  <Point X="3.139783763768" Y="-1.532218690455" Z="1.2" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.2" />
                  <Point X="3.100552592162" Y="-1.368212154387" Z="1.2" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.2" />
                  <Point X="3.165833063079" Y="-1.217806736262" Z="1.2" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.2" />
                  <Point X="3.313430652426" Y="-1.13458445196" Z="1.2" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.2" />
                  <Point X="4.1123561874" Y="-1.209796063798" Z="1.2" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.2" />
                  <Point X="4.951507204722" Y="-1.119406679343" Z="1.2" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.2" />
                  <Point X="5.021257665657" Y="-0.74663782012" Z="1.2" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.2" />
                  <Point X="4.433162360054" Y="-0.404412003595" Z="1.2" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.2" />
                  <Point X="3.69919447505" Y="-0.192627509083" Z="1.2" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.2" />
                  <Point X="3.626187779925" Y="-0.130060545702" Z="1.2" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.2" />
                  <Point X="3.590185078787" Y="-0.045691076622" Z="1.2" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.2" />
                  <Point X="3.591186371637" Y="0.050919454591" Z="1.2" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.2" />
                  <Point X="3.629191658476" Y="0.133888192873" Z="1.2" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.2" />
                  <Point X="3.704200939302" Y="0.195521335159" Z="1.2" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.2" />
                  <Point X="4.362805457252" Y="0.385559929173" Z="1.2" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.2" />
                  <Point X="5.013280749392" Y="0.792254473118" Z="1.2" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.2" />
                  <Point X="4.928707351377" Y="1.211814454058" Z="1.2" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.2" />
                  <Point X="4.21031407935" Y="1.320393893965" Z="1.2" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.2" />
                  <Point X="3.413493115099" Y="1.228583067434" Z="1.2" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.2" />
                  <Point X="3.332255137461" Y="1.255130586169" Z="1.2" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.2" />
                  <Point X="3.273989382221" Y="1.312170255735" Z="1.2" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.2" />
                  <Point X="3.241948409321" Y="1.391849946565" Z="1.2" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.2" />
                  <Point X="3.244936427292" Y="1.472914039836" Z="1.2" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.2" />
                  <Point X="3.285570603047" Y="1.549044150558" Z="1.2" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.2" />
                  <Point X="3.849408863311" Y="1.996374229352" Z="1.2" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.2" />
                  <Point X="4.337089124742" Y="2.637305593656" Z="1.2" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.2" />
                  <Point X="4.113838721774" Y="2.973559088544" Z="1.2" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.2" />
                  <Point X="3.296452010782" Y="2.721127504912" Z="1.2" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.2" />
                  <Point X="2.467562318056" Y="2.255683024579" Z="1.2" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.2" />
                  <Point X="2.393000651089" Y="2.249941372226" Z="1.2" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.2" />
                  <Point X="2.326799356609" Y="2.276541690372" Z="1.2" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.2" />
                  <Point X="2.274216909863" Y="2.330225503773" Z="1.2" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.2" />
                  <Point X="2.249488330533" Y="2.396757792743" Z="1.2" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.2" />
                  <Point X="2.256844918078" Y="2.471907257825" Z="1.2" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.2" />
                  <Point X="2.674497847948" Y="3.215687071173" Z="1.2" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.2" />
                  <Point X="2.930911585237" Y="4.142865146612" Z="1.2" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.2" />
                  <Point X="2.543867945859" Y="4.391162862626" Z="1.2" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.2" />
                  <Point X="2.1387559174" Y="4.60224022644" Z="1.2" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.2" />
                  <Point X="1.718822249809" Y="4.774991410211" Z="1.2" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.2" />
                  <Point X="1.171946662854" Y="4.931532169344" Z="1.2" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.2" />
                  <Point X="0.509790609812" Y="5.043172206907" Z="1.2" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.2" />
                  <Point X="0.101851294648" Y="4.735238699412" Z="1.2" />
                  <Point X="0" Y="4.355124473572" Z="1.2" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>