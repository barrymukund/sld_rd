<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#215" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3553" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004715820312" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.563302734375" Y="-3.512517578125" />
                  <Point X="25.55771875" Y="-3.497130615234" />
                  <Point X="25.542361328125" Y="-3.467373291016" />
                  <Point X="25.384353515625" Y="-3.239713623047" />
                  <Point X="25.3786328125" Y="-3.231472900391" />
                  <Point X="25.356744140625" Y="-3.209014160156" />
                  <Point X="25.330490234375" Y="-3.189773681641" />
                  <Point X="25.302494140625" Y="-3.175668701172" />
                  <Point X="25.057986328125" Y="-3.099782226562" />
                  <Point X="25.049134765625" Y="-3.097035400391" />
                  <Point X="25.020974609375" Y="-3.092766113281" />
                  <Point X="24.9913359375" Y="-3.092766357422" />
                  <Point X="24.96317578125" Y="-3.097035644531" />
                  <Point X="24.71866796875" Y="-3.172921875" />
                  <Point X="24.70981640625" Y="-3.175668945312" />
                  <Point X="24.681814453125" Y="-3.189778808594" />
                  <Point X="24.655556640625" Y="-3.209025634766" />
                  <Point X="24.633673828125" Y="-3.231479492188" />
                  <Point X="24.475666015625" Y="-3.459139160156" />
                  <Point X="24.470240234375" Y="-3.467944091797" />
                  <Point X="24.457302734375" Y="-3.491750244141" />
                  <Point X="24.44901171875" Y="-3.5125234375" />
                  <Point X="24.435017578125" Y="-3.564747802734" />
                  <Point X="24.083416015625" Y="-4.876941894531" />
                  <Point X="23.92685546875" Y="-4.846552734375" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.7850390625" Y="-4.563438476562" />
                  <Point X="23.78580078125" Y="-4.5479296875" />
                  <Point X="23.7834921875" Y="-4.516220214844" />
                  <Point X="23.725078125" Y="-4.22255859375" />
                  <Point X="23.722962890625" Y="-4.211928710938" />
                  <Point X="23.71205859375" Y="-4.182956542969" />
                  <Point X="23.695986328125" Y="-4.155123046875" />
                  <Point X="23.67635546875" Y="-4.131204589844" />
                  <Point X="23.451244140625" Y="-3.933785400391" />
                  <Point X="23.443095703125" Y="-3.926639404297" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.058197265625" Y="-3.871383544922" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708496094" />
                  <Point X="22.985533203125" Y="-3.882028076172" />
                  <Point X="22.957341796875" Y="-3.894801513672" />
                  <Point X="22.70838671875" Y="-4.061148193359" />
                  <Point X="22.699373046875" Y="-4.067172119141" />
                  <Point X="22.68237109375" Y="-4.081698486328" />
                  <Point X="22.66262890625" Y="-4.103069824219" />
                  <Point X="22.657041015625" Y="-4.109700683594" />
                  <Point X="22.5198515625" Y="-4.288489257813" />
                  <Point X="22.20736328125" Y="-4.09500390625" />
                  <Point X="22.198287109375" Y="-4.089383789063" />
                  <Point X="21.895279296875" Y="-3.856077148438" />
                  <Point X="22.57623828125" Y="-2.676620117188" />
                  <Point X="22.587140625" Y="-2.647655029297" />
                  <Point X="22.593412109375" Y="-2.616129150391" />
                  <Point X="22.59442578125" Y="-2.585194824219" />
                  <Point X="22.58544140625" Y="-2.555576416016" />
                  <Point X="22.571224609375" Y="-2.526747558594" />
                  <Point X="22.553197265625" Y="-2.501590820312" />
                  <Point X="22.53649609375" Y="-2.484888427734" />
                  <Point X="22.535890625" Y="-2.484281982422" />
                  <Point X="22.510732421875" Y="-2.466240234375" />
                  <Point X="22.481900390625" Y="-2.452010498047" />
                  <Point X="22.452275390625" Y="-2.443015625" />
                  <Point X="22.42133203125" Y="-2.444023681641" />
                  <Point X="22.389794921875" Y="-2.450293212891" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="22.315775390625" Y="-2.487202636719" />
                  <Point X="21.1819765625" Y="-3.141801025391" />
                  <Point X="20.92430859375" Y="-2.803277587891" />
                  <Point X="20.917138671875" Y="-2.793855957031" />
                  <Point X="20.693857421875" Y="-2.419450927734" />
                  <Point X="21.894041015625" Y="-1.498516723633" />
                  <Point X="21.915421875" Y="-1.475593505859" />
                  <Point X="21.93338671875" Y="-1.448462524414" />
                  <Point X="21.946142578125" Y="-1.419834350586" />
                  <Point X="21.95357421875" Y="-1.391142700195" />
                  <Point X="21.9538359375" Y="-1.390133789062" />
                  <Point X="21.956650390625" Y="-1.359697875977" />
                  <Point X="21.95444921875" Y="-1.328018798828" />
                  <Point X="21.947453125" Y="-1.298264038086" />
                  <Point X="21.931369140625" Y="-1.272271362305" />
                  <Point X="21.910533203125" Y="-1.248307006836" />
                  <Point X="21.887029296875" Y="-1.228767944336" />
                  <Point X="21.86147265625" Y="-1.213725952148" />
                  <Point X="21.860509765625" Y="-1.213159545898" />
                  <Point X="21.831259765625" Y="-1.201947631836" />
                  <Point X="21.799384765625" Y="-1.19547253418" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="21.7112109375" Y="-1.201869873047" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.168728515625" Y="-1.003632568359" />
                  <Point X="20.165921875" Y="-0.992649963379" />
                  <Point X="20.107576171875" Y="-0.584698669434" />
                  <Point X="21.467123046875" Y="-0.220408920288" />
                  <Point X="21.4825078125" Y="-0.21482774353" />
                  <Point X="21.5122734375" Y="-0.19946913147" />
                  <Point X="21.539056640625" Y="-0.180879821777" />
                  <Point X="21.540001953125" Y="-0.180223632812" />
                  <Point X="21.5624375" Y="-0.158365661621" />
                  <Point X="21.58170703125" Y="-0.132093933105" />
                  <Point X="21.595833984375" Y="-0.104066749573" />
                  <Point X="21.60476171875" Y="-0.075300956726" />
                  <Point X="21.605158203125" Y="-0.074011802673" />
                  <Point X="21.60936328125" Y="-0.046031822205" />
                  <Point X="21.609353515625" Y="-0.016428142548" />
                  <Point X="21.605083984375" Y="0.011699830055" />
                  <Point X="21.59615625" Y="0.040465473175" />
                  <Point X="21.5958203125" Y="0.041548042297" />
                  <Point X="21.581759765625" Y="0.069437705994" />
                  <Point X="21.562505859375" Y="0.095730384827" />
                  <Point X="21.54002734375" Y="0.117646789551" />
                  <Point X="21.513244140625" Y="0.136236099243" />
                  <Point X="21.499994140625" Y="0.143928390503" />
                  <Point X="21.467125" Y="0.157848175049" />
                  <Point X="21.41529296875" Y="0.171736679077" />
                  <Point X="20.108185546875" Y="0.521975402832" />
                  <Point X="20.17370703125" Y="0.9647734375" />
                  <Point X="20.17551171875" Y="0.97697052002" />
                  <Point X="20.29644921875" Y="1.423268066406" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.255017578125" Y="1.299341796875" />
                  <Point X="21.276578125" Y="1.301228271484" />
                  <Point X="21.296865234375" Y="1.305263793945" />
                  <Point X="21.35614453125" Y="1.323954956055" />
                  <Point X="21.35826953125" Y="1.324624633789" />
                  <Point X="21.3772109375" Y="1.332956176758" />
                  <Point X="21.395958984375" Y="1.343778442383" />
                  <Point X="21.412640625" Y="1.356008544922" />
                  <Point X="21.42628125" Y="1.371559570312" />
                  <Point X="21.43869921875" Y="1.389291748047" />
                  <Point X="21.44865234375" Y="1.40743359375" />
                  <Point X="21.4724375" Y="1.464859130859" />
                  <Point X="21.473302734375" Y="1.466948120117" />
                  <Point X="21.47908203125" Y="1.486784790039" />
                  <Point X="21.482841796875" Y="1.508104370117" />
                  <Point X="21.4841953125" Y="1.528742675781" />
                  <Point X="21.481048828125" Y="1.549184814453" />
                  <Point X="21.475447265625" Y="1.570095703125" />
                  <Point X="21.46794921875" Y="1.589380615234" />
                  <Point X="21.439248046875" Y="1.644514770508" />
                  <Point X="21.438208984375" Y="1.646510498047" />
                  <Point X="21.426712890625" Y="1.663713500977" />
                  <Point X="21.41280078125" Y="1.680291381836" />
                  <Point X="21.397865234375" Y="1.694589477539" />
                  <Point X="21.368134765625" Y="1.717403198242" />
                  <Point X="20.648140625" Y="2.269873779297" />
                  <Point X="20.911833984375" Y="2.721642089844" />
                  <Point X="20.9188515625" Y="2.733665039062" />
                  <Point X="21.24949609375" Y="3.158661621094" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.83633984375" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796630859" />
                  <Point X="21.935767578125" Y="2.818573486328" />
                  <Point X="21.938755859375" Y="2.818312011719" />
                  <Point X="21.95943359375" Y="2.818763183594" />
                  <Point X="21.980892578125" Y="2.821588134766" />
                  <Point X="22.000986328125" Y="2.826504638672" />
                  <Point X="22.0195390625" Y="2.835654052734" />
                  <Point X="22.03779296875" Y="2.847283447266" />
                  <Point X="22.053921875" Y="2.860229003906" />
                  <Point X="22.112525390625" Y="2.918831298828" />
                  <Point X="22.114646484375" Y="2.920952392578" />
                  <Point X="22.12759765625" Y="2.937089355469" />
                  <Point X="22.1392265625" Y="2.955345458984" />
                  <Point X="22.148375" Y="2.973897460938" />
                  <Point X="22.1532890625" Y="2.993989501953" />
                  <Point X="22.15611328125" Y="3.015450683594" />
                  <Point X="22.156564453125" Y="3.036124755859" />
                  <Point X="22.149341796875" Y="3.118685791016" />
                  <Point X="22.149080078125" Y="3.121674316406" />
                  <Point X="22.14504296875" Y="3.14196484375" />
                  <Point X="22.13853515625" Y="3.162602783203" />
                  <Point X="22.130205078125" Y="3.181535644531" />
                  <Point X="22.117029296875" Y="3.204354980469" />
                  <Point X="21.816669921875" Y="3.724596191406" />
                  <Point X="22.28716796875" Y="4.085322509766" />
                  <Point X="22.299384765625" Y="4.094690185547" />
                  <Point X="22.83296484375" Y="4.391134765625" />
                  <Point X="22.9568046875" Y="4.229741210938" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.004888671875" Y="4.18939453125" />
                  <Point X="23.096779296875" Y="4.141559570313" />
                  <Point X="23.1001015625" Y="4.139830078125" />
                  <Point X="23.119373046875" Y="4.132334960938" />
                  <Point X="23.14028125" Y="4.126730957031" />
                  <Point X="23.1607265625" Y="4.123583007812" />
                  <Point X="23.1813671875" Y="4.124935058594" />
                  <Point X="23.20268359375" Y="4.128692871094" />
                  <Point X="23.222546875" Y="4.134481445312" />
                  <Point X="23.318255859375" Y="4.174125976563" />
                  <Point X="23.321720703125" Y="4.175561035156" />
                  <Point X="23.339853515625" Y="4.185509277344" />
                  <Point X="23.357583984375" Y="4.197923828125" />
                  <Point X="23.37313671875" Y="4.211562988281" />
                  <Point X="23.385369140625" Y="4.228245117188" />
                  <Point X="23.39619140625" Y="4.246990234375" />
                  <Point X="23.404521484375" Y="4.265923828125" />
                  <Point X="23.435640625" Y="4.364626464844" />
                  <Point X="23.436771484375" Y="4.368202148438" />
                  <Point X="23.440830078125" Y="4.388556640625" />
                  <Point X="23.442720703125" Y="4.410134765625" />
                  <Point X="23.442271484375" Y="4.430828125" />
                  <Point X="23.4407734375" Y="4.442205566406" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="24.03455859375" Y="4.805376464844" />
                  <Point X="24.0503671875" Y="4.80980859375" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.14621484375" Y="4.286311035156" />
                  <Point X="25.152966796875" Y="4.311504882812" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.83023046875" Y="4.833185058594" />
                  <Point X="25.844041015625" Y="4.831738769531" />
                  <Point X="26.46601171875" Y="4.681576171875" />
                  <Point X="26.48103125" Y="4.677949707031" />
                  <Point X="26.8858203125" Y="4.531129882812" />
                  <Point X="26.894642578125" Y="4.5279296875" />
                  <Point X="27.286111328125" Y="4.344853515625" />
                  <Point X="27.294572265625" Y="4.340896484375" />
                  <Point X="27.672767578125" Y="4.12055859375" />
                  <Point X="27.68097265625" Y="4.115777832031" />
                  <Point X="27.94326171875" Y="3.929254638672" />
                  <Point X="27.1475859375" Y="2.551102539062" />
                  <Point X="27.142078125" Y="2.53993359375" />
                  <Point X="27.133078125" Y="2.516057861328" />
                  <Point X="27.112357421875" Y="2.438576171875" />
                  <Point X="27.109568359375" Y="2.423128173828" />
                  <Point X="27.10748046875" Y="2.401420166016" />
                  <Point X="27.107728515625" Y="2.380950927734" />
                  <Point X="27.115798828125" Y="2.314037353516" />
                  <Point X="27.116087890625" Y="2.311611816406" />
                  <Point X="27.121427734375" Y="2.289659667969" />
                  <Point X="27.129697265625" Y="2.267542236328" />
                  <Point X="27.1400703125" Y="2.247471435547" />
                  <Point X="27.18152734375" Y="2.186374267578" />
                  <Point X="27.19143359375" Y="2.174101806641" />
                  <Point X="27.20622265625" Y="2.158608642578" />
                  <Point X="27.221599609375" Y="2.145593505859" />
                  <Point X="27.2826953125" Y="2.104136474609" />
                  <Point X="27.28494140625" Y="2.102612548828" />
                  <Point X="27.304978515625" Y="2.092259277344" />
                  <Point X="27.327048828125" Y="2.084002929688" />
                  <Point X="27.3489609375" Y="2.078663818359" />
                  <Point X="27.4159609375" Y="2.070584716797" />
                  <Point X="27.43198046875" Y="2.070015380859" />
                  <Point X="27.453310546875" Y="2.071060058594" />
                  <Point X="27.473205078125" Y="2.074171142578" />
                  <Point X="27.5506875" Y="2.094890869141" />
                  <Point X="27.56037890625" Y="2.098048339844" />
                  <Point X="27.58853515625" Y="2.110145507813" />
                  <Point X="27.64066796875" Y="2.140244873047" />
                  <Point X="28.967328125" Y="2.90619140625" />
                  <Point X="29.11840234375" Y="2.696230957031" />
                  <Point X="29.123271484375" Y="2.689464599609" />
                  <Point X="29.26219921875" Y="2.459883789063" />
                  <Point X="28.23078125" Y="1.668448852539" />
                  <Point X="28.2214296875" Y="1.660244995117" />
                  <Point X="28.20397265625" Y="1.641627685547" />
                  <Point X="28.148208984375" Y="1.568879882813" />
                  <Point X="28.139857421875" Y="1.555928833008" />
                  <Point X="28.12937109375" Y="1.536344604492" />
                  <Point X="28.1216328125" Y="1.517088623047" />
                  <Point X="28.100859375" Y="1.442812744141" />
                  <Point X="28.100107421875" Y="1.440124389648" />
                  <Point X="28.09665234375" Y="1.417825073242" />
                  <Point X="28.0958359375" Y="1.394254394531" />
                  <Point X="28.097740234375" Y="1.371767333984" />
                  <Point X="28.11479296875" Y="1.289125976562" />
                  <Point X="28.119138671875" Y="1.274289794922" />
                  <Point X="28.126955078125" Y="1.253920288086" />
                  <Point X="28.13628515625" Y="1.235739501953" />
                  <Point X="28.18260546875" Y="1.165335449219" />
                  <Point X="28.18428125" Y="1.162782592773" />
                  <Point X="28.198876953125" Y="1.145469726562" />
                  <Point X="28.216130859375" Y="1.1293671875" />
                  <Point X="28.234349609375" Y="1.116034301758" />
                  <Point X="28.30155078125" Y="1.078206542969" />
                  <Point X="28.315888671875" Y="1.071637817383" />
                  <Point X="28.33630859375" Y="1.064265014648" />
                  <Point X="28.356123046875" Y="1.059438110352" />
                  <Point X="28.446994140625" Y="1.047428222656" />
                  <Point X="28.45690234375" Y="1.046643066406" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="28.5377265625" Y="1.053504150391" />
                  <Point X="29.77683984375" Y="1.21663659668" />
                  <Point X="29.84384375" Y="0.941404541016" />
                  <Point X="29.845935546875" Y="0.932807067871" />
                  <Point X="29.890865234375" Y="0.64423828125" />
                  <Point X="28.71658203125" Y="0.329590332031" />
                  <Point X="28.7047890625" Y="0.325585357666" />
                  <Point X="28.681546875" Y="0.315067657471" />
                  <Point X="28.5922734375" Y="0.26346572876" />
                  <Point X="28.579826171875" Y="0.254881393433" />
                  <Point X="28.56227734375" Y="0.240590621948" />
                  <Point X="28.54753125" Y="0.225576126099" />
                  <Point X="28.49396484375" Y="0.15731918335" />
                  <Point X="28.492029296875" Y="0.15485319519" />
                  <Point X="28.48030859375" Y="0.135581436157" />
                  <Point X="28.47053125" Y="0.114115386963" />
                  <Point X="28.463681640625" Y="0.092605926514" />
                  <Point X="28.445826171875" Y="-0.000629799545" />
                  <Point X="28.444171875" Y="-0.01573157692" />
                  <Point X="28.443525390625" Y="-0.037920143127" />
                  <Point X="28.4451796875" Y="-0.058555438995" />
                  <Point X="28.46303515625" Y="-0.151791015625" />
                  <Point X="28.463681640625" Y="-0.15516607666" />
                  <Point X="28.470529296875" Y="-0.176669464111" />
                  <Point X="28.480302734375" Y="-0.198130950928" />
                  <Point X="28.492025390625" Y="-0.217408630371" />
                  <Point X="28.545591796875" Y="-0.285665435791" />
                  <Point X="28.556146484375" Y="-0.297058654785" />
                  <Point X="28.5723984375" Y="-0.311950073242" />
                  <Point X="28.589037109375" Y="-0.324155975342" />
                  <Point X="28.67830859375" Y="-0.37575592041" />
                  <Point X="28.686927734375" Y="-0.380168151855" />
                  <Point X="28.716580078125" Y="-0.392149871826" />
                  <Point X="28.76199609375" Y="-0.404318908691" />
                  <Point X="29.891474609375" Y="-0.706961975098" />
                  <Point X="29.856193359375" Y="-0.94097668457" />
                  <Point X="29.8550234375" Y="-0.94872644043" />
                  <Point X="29.80117578125" Y="-1.18469921875" />
                  <Point X="28.4243828125" Y="-1.003441040039" />
                  <Point X="28.40803515625" Y="-1.002710327148" />
                  <Point X="28.374658203125" Y="-1.005508850098" />
                  <Point X="28.199435546875" Y="-1.043593994141" />
                  <Point X="28.19309375" Y="-1.04497265625" />
                  <Point X="28.163974609375" Y="-1.056596801758" />
                  <Point X="28.136150390625" Y="-1.073487304688" />
                  <Point X="28.1123984375" Y="-1.093958618164" />
                  <Point X="28.00648828125" Y="-1.2213359375" />
                  <Point X="28.002654296875" Y="-1.225946655273" />
                  <Point X="27.98793359375" Y="-1.250329467773" />
                  <Point X="27.97658984375" Y="-1.277716308594" />
                  <Point X="27.969759765625" Y="-1.305365356445" />
                  <Point X="27.95458203125" Y="-1.470311645508" />
                  <Point X="27.954029296875" Y="-1.476287841797" />
                  <Point X="27.956349609375" Y="-1.507576660156" />
                  <Point X="27.96408203125" Y="-1.539190673828" />
                  <Point X="27.976451171875" Y="-1.567995239258" />
                  <Point X="28.073421875" Y="-1.718825805664" />
                  <Point X="28.0804609375" Y="-1.728401245117" />
                  <Point X="28.095591796875" Y="-1.746491088867" />
                  <Point X="28.11062890625" Y="-1.760908569336" />
                  <Point X="28.1527734375" Y="-1.793248046875" />
                  <Point X="29.213123046875" Y="-2.606883056641" />
                  <Point X="29.12810546875" Y="-2.744454589844" />
                  <Point X="29.12480078125" Y="-2.749799560547" />
                  <Point X="29.028982421875" Y="-2.8859453125" />
                  <Point X="27.800953125" Y="-2.176941894531" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.7542265625" Y="-2.159825195312" />
                  <Point X="27.545685546875" Y="-2.122162841797" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474611328125" Y="-2.125352783203" />
                  <Point X="27.4448359375" Y="-2.135176757812" />
                  <Point X="27.27158984375" Y="-2.226355224609" />
                  <Point X="27.265318359375" Y="-2.229655761719" />
                  <Point X="27.242390625" Y="-2.246544677734" />
                  <Point X="27.2214296875" Y="-2.267503173828" />
                  <Point X="27.204533203125" Y="-2.290437255859" />
                  <Point X="27.113353515625" Y="-2.463684082031" />
                  <Point X="27.110052734375" Y="-2.469955322266" />
                  <Point X="27.100228515625" Y="-2.499734375" />
                  <Point X="27.095271484375" Y="-2.531908691406" />
                  <Point X="27.09567578125" Y="-2.563259033203" />
                  <Point X="27.13333984375" Y="-2.77180078125" />
                  <Point X="27.136013671875" Y="-2.782801513672" />
                  <Point X="27.143279296875" Y="-2.806463867188" />
                  <Point X="27.1518203125" Y="-2.826077636719" />
                  <Point X="27.17890234375" Y="-2.872986328125" />
                  <Point X="27.86128515625" Y="-4.054906738281" />
                  <Point X="27.785765625" Y="-4.108848632812" />
                  <Point X="27.78185546875" Y="-4.111641113281" />
                  <Point X="27.701765625" Y="-4.163481445312" />
                  <Point X="26.758548828125" Y="-2.934256103516" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.51624609375" Y="-2.768324951172" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.19215625" Y="-2.76181640625" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397705078" />
                  <Point X="26.128978515625" Y="-2.780740966797" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="25.930900390625" Y="-2.939884033203" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968861328125" />
                  <Point X="25.88725" Y="-2.996687988281" />
                  <Point X="25.875625" Y="-3.025809082031" />
                  <Point X="25.82369140625" Y="-3.264747802734" />
                  <Point X="25.822" Y="-3.275420654297" />
                  <Point X="25.81940625" Y="-3.301215820312" />
                  <Point X="25.8197421875" Y="-3.323121337891" />
                  <Point X="25.82741796875" Y="-3.381418945312" />
                  <Point X="26.022064453125" Y="-4.859915527344" />
                  <Point X="25.979376953125" Y="-4.869272949219" />
                  <Point X="25.975673828125" Y="-4.870084472656" />
                  <Point X="25.929318359375" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.94495703125" Y="-4.753293457031" />
                  <Point X="23.941576171875" Y="-4.75263671875" />
                  <Point X="23.858755859375" Y="-4.731328613281" />
                  <Point X="23.8792265625" Y="-4.575838378906" />
                  <Point X="23.879923828125" Y="-4.568098632812" />
                  <Point X="23.88055078125" Y="-4.541031738281" />
                  <Point X="23.8782421875" Y="-4.509322265625" />
                  <Point X="23.876666015625" Y="-4.497686523438" />
                  <Point X="23.818251953125" Y="-4.204024902344" />
                  <Point X="23.811873046875" Y="-4.178465332031" />
                  <Point X="23.80096875" Y="-4.149493164062" />
                  <Point X="23.794328125" Y="-4.135450683594" />
                  <Point X="23.778255859375" Y="-4.1076171875" />
                  <Point X="23.769419921875" Y="-4.094853027344" />
                  <Point X="23.7497890625" Y="-4.070934570312" />
                  <Point X="23.738994140625" Y="-4.059780029297" />
                  <Point X="23.5138828125" Y="-3.862360839844" />
                  <Point X="23.49326171875" Y="-3.845965332031" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.06441015625" Y="-3.776586914062" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166015625" />
                  <Point X="22.991994140625" Y="-3.781945556641" />
                  <Point X="22.9609453125" Y="-3.790265136719" />
                  <Point X="22.946326171875" Y="-3.79549609375" />
                  <Point X="22.918134765625" Y="-3.80826953125" />
                  <Point X="22.9045625" Y="-3.815812011719" />
                  <Point X="22.655607421875" Y="-3.982158691406" />
                  <Point X="22.637662109375" Y="-3.994944824219" />
                  <Point X="22.62066015625" Y="-4.009471191406" />
                  <Point X="22.612587890625" Y="-4.017235839844" />
                  <Point X="22.592845703125" Y="-4.038607177734" />
                  <Point X="22.581671875" Y="-4.051868408203" />
                  <Point X="22.496796875" Y="-4.162478027344" />
                  <Point X="22.257375" Y="-4.014233398438" />
                  <Point X="22.252408203125" Y="-4.011157958984" />
                  <Point X="22.01913671875" Y="-3.831546630859" />
                  <Point X="22.658509765625" Y="-2.724120117188" />
                  <Point X="22.6651484375" Y="-2.710085693359" />
                  <Point X="22.67605078125" Y="-2.681120605469" />
                  <Point X="22.680314453125" Y="-2.666190429688" />
                  <Point X="22.6865859375" Y="-2.634664550781" />
                  <Point X="22.688361328125" Y="-2.619240478516" />
                  <Point X="22.689375" Y="-2.588306152344" />
                  <Point X="22.6853359375" Y="-2.557618652344" />
                  <Point X="22.6763515625" Y="-2.528000244141" />
                  <Point X="22.67064453125" Y="-2.513559082031" />
                  <Point X="22.656427734375" Y="-2.484730224609" />
                  <Point X="22.6484453125" Y="-2.471411621094" />
                  <Point X="22.63041796875" Y="-2.446254882812" />
                  <Point X="22.620375" Y="-2.434418212891" />
                  <Point X="22.603673828125" Y="-2.417715820312" />
                  <Point X="22.59125390625" Y="-2.407081298828" />
                  <Point X="22.566095703125" Y="-2.389039550781" />
                  <Point X="22.55277734375" Y="-2.381050537109" />
                  <Point X="22.5239453125" Y="-2.366820800781" />
                  <Point X="22.5095" Y="-2.361108154297" />
                  <Point X="22.479875" Y="-2.35211328125" />
                  <Point X="22.449181640625" Y="-2.348065917969" />
                  <Point X="22.41823828125" Y="-2.349073974609" />
                  <Point X="22.40280859375" Y="-2.350847167969" />
                  <Point X="22.371271484375" Y="-2.357116699219" />
                  <Point X="22.3563359375" Y="-2.361380126953" />
                  <Point X="22.327359375" Y="-2.372284179688" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="22.268275390625" Y="-2.404930175781" />
                  <Point X="21.206912109375" Y="-3.017708007813" />
                  <Point X="20.99990234375" Y="-2.745739501953" />
                  <Point X="20.99598046875" Y="-2.740585693359" />
                  <Point X="20.818734375" Y="-2.443374267578" />
                  <Point X="21.951873046875" Y="-1.573885131836" />
                  <Point X="21.96351171875" Y="-1.563314086914" />
                  <Point X="21.984892578125" Y="-1.540390991211" />
                  <Point X="21.994630859375" Y="-1.528042358398" />
                  <Point X="22.012595703125" Y="-1.500911254883" />
                  <Point X="22.020162109375" Y="-1.487127197266" />
                  <Point X="22.03291796875" Y="-1.458499023438" />
                  <Point X="22.038107421875" Y="-1.443654907227" />
                  <Point X="22.04553125" Y="-1.414996582031" />
                  <Point X="22.048431640625" Y="-1.398881225586" />
                  <Point X="22.05124609375" Y="-1.36844519043" />
                  <Point X="22.051421875" Y="-1.353112915039" />
                  <Point X="22.049220703125" Y="-1.32143371582" />
                  <Point X="22.046927734375" Y="-1.306274902344" />
                  <Point X="22.039931640625" Y="-1.276520141602" />
                  <Point X="22.02823828125" Y="-1.248275390625" />
                  <Point X="22.012154296875" Y="-1.222282592773" />
                  <Point X="22.003060546875" Y="-1.209938964844" />
                  <Point X="21.982224609375" Y="-1.185974487305" />
                  <Point X="21.971263671875" Y="-1.175253417969" />
                  <Point X="21.947759765625" Y="-1.155714355469" />
                  <Point X="21.935216796875" Y="-1.146896362305" />
                  <Point X="21.90966015625" Y="-1.131854370117" />
                  <Point X="21.89451171875" Y="-1.124453125" />
                  <Point X="21.86526171875" Y="-1.113241088867" />
                  <Point X="21.850171875" Y="-1.108849243164" />
                  <Point X="21.818296875" Y="-1.102374023438" />
                  <Point X="21.802685546875" Y="-1.100529907227" />
                  <Point X="21.771373046875" Y="-1.099441162109" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="21.698810546875" Y="-1.107682617188" />
                  <Point X="20.33908203125" Y="-1.286694335938" />
                  <Point X="20.2607734375" Y="-0.980121398926" />
                  <Point X="20.259236328125" Y="-0.974101135254" />
                  <Point X="20.213548828125" Y="-0.654654846191" />
                  <Point X="21.4917109375" Y="-0.312171875" />
                  <Point X="21.499521484375" Y="-0.30971395874" />
                  <Point X="21.526068359375" Y="-0.299251647949" />
                  <Point X="21.555833984375" Y="-0.283893096924" />
                  <Point X="21.56644140625" Y="-0.277513153076" />
                  <Point X="21.593224609375" Y="-0.258923858643" />
                  <Point X="21.606294921875" Y="-0.248268936157" />
                  <Point X="21.62873046875" Y="-0.226410964966" />
                  <Point X="21.639041015625" Y="-0.214552047729" />
                  <Point X="21.658310546875" Y="-0.188280273438" />
                  <Point X="21.6665390625" Y="-0.174853530884" />
                  <Point X="21.680666015625" Y="-0.146826370239" />
                  <Point X="21.686564453125" Y="-0.132225814819" />
                  <Point X="21.6954921875" Y="-0.103460144043" />
                  <Point X="21.699103515625" Y="-0.088130729675" />
                  <Point X="21.70330859375" Y="-0.060150688171" />
                  <Point X="21.70436328125" Y="-0.046000450134" />
                  <Point X="21.704353515625" Y="-0.016396848679" />
                  <Point X="21.70327734375" Y="-0.002171406984" />
                  <Point X="21.6990078125" Y="0.025956518173" />
                  <Point X="21.695814453125" Y="0.039859149933" />
                  <Point X="21.68688671875" Y="0.068624816895" />
                  <Point X="21.680650390625" Y="0.08431463623" />
                  <Point X="21.66658984375" Y="0.112204322815" />
                  <Point X="21.65840625" Y="0.125565368652" />
                  <Point X="21.63915234375" Y="0.151857955933" />
                  <Point X="21.62882421875" Y="0.163750610352" />
                  <Point X="21.606345703125" Y="0.185666976929" />
                  <Point X="21.5941953125" Y="0.195690704346" />
                  <Point X="21.567412109375" Y="0.214280014038" />
                  <Point X="21.56094140625" Y="0.218394485474" />
                  <Point X="21.537041015625" Y="0.231407318115" />
                  <Point X="21.504171875" Y="0.245327041626" />
                  <Point X="21.491712890625" Y="0.249611099243" />
                  <Point X="21.439880859375" Y="0.263499603271" />
                  <Point X="20.2145546875" Y="0.591824768066" />
                  <Point X="20.26768359375" Y="0.950867614746" />
                  <Point X="20.26866796875" Y="0.957524658203" />
                  <Point X="20.3664140625" Y="1.318237182617" />
                  <Point X="21.221935546875" Y="1.20560559082" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.2529453125" Y="1.204364257812" />
                  <Point X="21.263298828125" Y="1.204703369141" />
                  <Point X="21.284859375" Y="1.20658984375" />
                  <Point X="21.29511328125" Y="1.208053833008" />
                  <Point X="21.315400390625" Y="1.212089233398" />
                  <Point X="21.32543359375" Y="1.214660888672" />
                  <Point X="21.38470703125" Y="1.233350097656" />
                  <Point X="21.39651953125" Y="1.237665161133" />
                  <Point X="21.4154609375" Y="1.245996704102" />
                  <Point X="21.424705078125" Y="1.250680175781" />
                  <Point X="21.443453125" Y="1.261502441406" />
                  <Point X="21.45212890625" Y="1.267163085938" />
                  <Point X="21.468810546875" Y="1.279393310547" />
                  <Point X="21.48405859375" Y="1.293363525391" />
                  <Point X="21.49769921875" Y="1.308914550781" />
                  <Point X="21.50409765625" Y="1.317064575195" />
                  <Point X="21.516515625" Y="1.334796875" />
                  <Point X="21.52198828125" Y="1.343597167969" />
                  <Point X="21.53194140625" Y="1.361739013672" />
                  <Point X="21.536421875" Y="1.371080322266" />
                  <Point X="21.56020703125" Y="1.428505981445" />
                  <Point X="21.564509765625" Y="1.440375244141" />
                  <Point X="21.5702890625" Y="1.460211914062" />
                  <Point X="21.572638671875" Y="1.470285888672" />
                  <Point X="21.5763984375" Y="1.49160546875" />
                  <Point X="21.577638671875" Y="1.501887329102" />
                  <Point X="21.5789921875" Y="1.522525634766" />
                  <Point X="21.57808984375" Y="1.543195068359" />
                  <Point X="21.574943359375" Y="1.563637084961" />
                  <Point X="21.5728125" Y="1.573766479492" />
                  <Point X="21.5672109375" Y="1.594677368164" />
                  <Point X="21.563990234375" Y="1.604521728516" />
                  <Point X="21.5564921875" Y="1.623806518555" />
                  <Point X="21.55221484375" Y="1.633246948242" />
                  <Point X="21.523513671875" Y="1.688381103516" />
                  <Point X="21.5171953125" Y="1.699294189453" />
                  <Point X="21.50569921875" Y="1.716497192383" />
                  <Point X="21.499484375" Y="1.724782470703" />
                  <Point X="21.485572265625" Y="1.741360351562" />
                  <Point X="21.47849609375" Y="1.748915039062" />
                  <Point X="21.463560546875" Y="1.763213134766" />
                  <Point X="21.45569921875" Y="1.76995703125" />
                  <Point X="21.42596875" Y="1.792770874023" />
                  <Point X="20.77238671875" Y="2.294281494141" />
                  <Point X="20.993880859375" Y="2.673752441406" />
                  <Point X="20.99771484375" Y="2.680321777344" />
                  <Point X="21.273662109375" Y="3.035012695312" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.75508203125" Y="2.757716552734" />
                  <Point X="21.774015625" Y="2.749385498047" />
                  <Point X="21.7837109375" Y="2.745736328125" />
                  <Point X="21.804353515625" Y="2.739228027344" />
                  <Point X="21.814388671875" Y="2.736656494141" />
                  <Point X="21.83467578125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731158203125" />
                  <Point X="21.92748828125" Y="2.723935058594" />
                  <Point X="21.940828125" Y="2.723334716797" />
                  <Point X="21.961505859375" Y="2.723785888672" />
                  <Point X="21.97183203125" Y="2.724575927734" />
                  <Point X="21.993291015625" Y="2.727400878906" />
                  <Point X="22.003470703125" Y="2.729310302734" />
                  <Point X="22.023564453125" Y="2.734226806641" />
                  <Point X="22.04300390625" Y="2.741302246094" />
                  <Point X="22.061556640625" Y="2.750451660156" />
                  <Point X="22.070583984375" Y="2.755532714844" />
                  <Point X="22.088837890625" Y="2.767162109375" />
                  <Point X="22.0972578125" Y="2.773196044922" />
                  <Point X="22.11338671875" Y="2.786141601562" />
                  <Point X="22.121095703125" Y="2.793053222656" />
                  <Point X="22.17969921875" Y="2.851655517578" />
                  <Point X="22.188736328125" Y="2.861489990234" />
                  <Point X="22.2016875" Y="2.877626953125" />
                  <Point X="22.20772265625" Y="2.886050537109" />
                  <Point X="22.2193515625" Y="2.904306640625" />
                  <Point X="22.2244296875" Y="2.913329589844" />
                  <Point X="22.233578125" Y="2.931881591797" />
                  <Point X="22.240654296875" Y="2.951327880859" />
                  <Point X="22.245568359375" Y="2.971419921875" />
                  <Point X="22.2474765625" Y="2.981594726562" />
                  <Point X="22.25030078125" Y="3.003055908203" />
                  <Point X="22.25108984375" Y="3.013377929688" />
                  <Point X="22.251541015625" Y="3.034052001953" />
                  <Point X="22.251203125" Y="3.044404052734" />
                  <Point X="22.24398046875" Y="3.126965087891" />
                  <Point X="22.24225390625" Y="3.140212646484" />
                  <Point X="22.238216796875" Y="3.160503173828" />
                  <Point X="22.23564453125" Y="3.170534667969" />
                  <Point X="22.22913671875" Y="3.191172607422" />
                  <Point X="22.225490234375" Y="3.200861572266" />
                  <Point X="22.21716015625" Y="3.219794433594" />
                  <Point X="22.2124765625" Y="3.229038330078" />
                  <Point X="22.19930078125" Y="3.251857666016" />
                  <Point X="21.940615234375" Y="3.699915771484" />
                  <Point X="22.344970703125" Y="4.009930664062" />
                  <Point X="22.351642578125" Y="4.015046875" />
                  <Point X="22.8074765625" Y="4.268296875" />
                  <Point X="22.881435546875" Y="4.171909179688" />
                  <Point X="22.8881796875" Y="4.164048828125" />
                  <Point X="22.902482421875" Y="4.149107421875" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.93491015625" Y="4.121895996094" />
                  <Point X="22.952111328125" Y="4.110403320312" />
                  <Point X="22.9610234375" Y="4.105128417969" />
                  <Point X="23.0529140625" Y="4.057293457031" />
                  <Point X="23.065666015625" Y="4.051290527344" />
                  <Point X="23.0849375" Y="4.043795410156" />
                  <Point X="23.094779296875" Y="4.040573730469" />
                  <Point X="23.1156875" Y="4.034969726562" />
                  <Point X="23.12582421875" Y="4.032837402344" />
                  <Point X="23.14626953125" Y="4.029689453125" />
                  <Point X="23.166935546875" Y="4.028786132812" />
                  <Point X="23.187576171875" Y="4.030138183594" />
                  <Point X="23.197859375" Y="4.031377685547" />
                  <Point X="23.21917578125" Y="4.035135498047" />
                  <Point X="23.229263671875" Y="4.037486816406" />
                  <Point X="23.249126953125" Y="4.043275390625" />
                  <Point X="23.25890234375" Y="4.046713134766" />
                  <Point X="23.354611328125" Y="4.086357666016" />
                  <Point X="23.367416015625" Y="4.092272460938" />
                  <Point X="23.385548828125" Y="4.102220703125" />
                  <Point X="23.394341796875" Y="4.107688964844" />
                  <Point X="23.412072265625" Y="4.120103515625" />
                  <Point X="23.420220703125" Y="4.126498535156" />
                  <Point X="23.4357734375" Y="4.140137695312" />
                  <Point X="23.449748046875" Y="4.15538671875" />
                  <Point X="23.46198046875" Y="4.172068847656" />
                  <Point X="23.467642578125" Y="4.18074609375" />
                  <Point X="23.47846484375" Y="4.199491210938" />
                  <Point X="23.4831484375" Y="4.208732910156" />
                  <Point X="23.491478515625" Y="4.227666503906" />
                  <Point X="23.495125" Y="4.237358398438" />
                  <Point X="23.52621875" Y="4.335979980469" />
                  <Point X="23.5299375" Y="4.349625488281" />
                  <Point X="23.53399609375" Y="4.369979980469" />
                  <Point X="23.535466796875" Y="4.380264648438" />
                  <Point X="23.537357421875" Y="4.401842773438" />
                  <Point X="23.53769921875" Y="4.412196777344" />
                  <Point X="23.53725" Y="4.432890136719" />
                  <Point X="23.5349609375" Y="4.454606933594" />
                  <Point X="23.520736328125" Y="4.562654785156" />
                  <Point X="24.060205078125" Y="4.713903320312" />
                  <Point X="24.068828125" Y="4.716320800781" />
                  <Point X="24.63477734375" Y="4.782556152344" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.23274609375" Y="4.2471015625" />
                  <Point X="25.2379765625" Y="4.26171875" />
                  <Point X="25.244728515625" Y="4.286912597656" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.8203359375" Y="4.738701660156" />
                  <Point X="25.827876953125" Y="4.737912109375" />
                  <Point X="26.443716796875" Y="4.589229492188" />
                  <Point X="26.45359765625" Y="4.58684375" />
                  <Point X="26.853427734375" Y="4.441822753906" />
                  <Point X="26.858251953125" Y="4.440072753906" />
                  <Point X="27.2458671875" Y="4.258798828125" />
                  <Point X="27.250453125" Y="4.256654296875" />
                  <Point X="27.6249453125" Y="4.038473632812" />
                  <Point X="27.62941796875" Y="4.035867431641" />
                  <Point X="27.81778125" Y="3.901915283203" />
                  <Point X="27.065314453125" Y="2.598602539062" />
                  <Point X="27.0623828125" Y="2.593119384766" />
                  <Point X="27.05318359375" Y="2.573442382812" />
                  <Point X="27.04418359375" Y="2.549566650391" />
                  <Point X="27.041302734375" Y="2.540601074219" />
                  <Point X="27.02058203125" Y="2.463119384766" />
                  <Point X="27.018869140625" Y="2.455455078125" />
                  <Point X="27.01500390625" Y="2.432223388672" />
                  <Point X="27.012916015625" Y="2.410515380859" />
                  <Point X="27.01248828125" Y="2.400269042969" />
                  <Point X="27.012736328125" Y="2.379799804688" />
                  <Point X="27.013412109375" Y="2.369575683594" />
                  <Point X="27.021482421875" Y="2.302662109375" />
                  <Point X="27.023779296875" Y="2.289157958984" />
                  <Point X="27.029119140625" Y="2.267205810547" />
                  <Point X="27.032443359375" Y="2.256389404297" />
                  <Point X="27.040712890625" Y="2.234271972656" />
                  <Point X="27.045302734375" Y="2.223925048828" />
                  <Point X="27.05567578125" Y="2.203854248047" />
                  <Point X="27.061458984375" Y="2.194130371094" />
                  <Point X="27.102916015625" Y="2.133033203125" />
                  <Point X="27.10760546875" Y="2.126704589844" />
                  <Point X="27.12271484375" Y="2.108506347656" />
                  <Point X="27.13750390625" Y="2.093013183594" />
                  <Point X="27.14484765625" Y="2.086095947266" />
                  <Point X="27.160224609375" Y="2.073080810547" />
                  <Point X="27.1682578125" Y="2.066982910156" />
                  <Point X="27.229353515625" Y="2.025525756836" />
                  <Point X="27.24133203125" Y="2.018213378906" />
                  <Point X="27.261369140625" Y="2.007860107422" />
                  <Point X="27.271693359375" Y="2.003281494141" />
                  <Point X="27.293763671875" Y="1.995025268555" />
                  <Point X="27.30455859375" Y="1.991703369141" />
                  <Point X="27.326470703125" Y="1.986364257812" />
                  <Point X="27.337587890625" Y="1.984347167969" />
                  <Point X="27.404587890625" Y="1.976267944336" />
                  <Point X="27.4125859375" Y="1.97564465332" />
                  <Point X="27.436626953125" Y="1.975129272461" />
                  <Point X="27.45795703125" Y="1.976173950195" />
                  <Point X="27.46798828125" Y="1.977200927734" />
                  <Point X="27.4878828125" Y="1.980311889648" />
                  <Point X="27.49774609375" Y="1.982395874023" />
                  <Point X="27.575228515625" Y="2.003115600586" />
                  <Point X="27.597880859375" Y="2.010763549805" />
                  <Point X="27.626037109375" Y="2.022860595703" />
                  <Point X="27.63603515625" Y="2.027873413086" />
                  <Point X="27.68816796875" Y="2.057972900391" />
                  <Point X="28.94040625" Y="2.780951660156" />
                  <Point X="29.0412890625" Y="2.640745605469" />
                  <Point X="29.043953125" Y="2.637044189453" />
                  <Point X="29.136884765625" Y="2.483471679688" />
                  <Point X="28.17294921875" Y="1.743817382812" />
                  <Point X="28.168130859375" Y="1.73986315918" />
                  <Point X="28.15212890625" Y="1.725225952148" />
                  <Point X="28.134671875" Y="1.706608642578" />
                  <Point X="28.128576171875" Y="1.699422241211" />
                  <Point X="28.0728125" Y="1.626674560547" />
                  <Point X="28.068369140625" Y="1.620364746094" />
                  <Point X="28.056107421875" Y="1.600772460938" />
                  <Point X="28.04562109375" Y="1.581188110352" />
                  <Point X="28.04122265625" Y="1.571768310547" />
                  <Point X="28.033484375" Y="1.552512329102" />
                  <Point X="28.03014453125" Y="1.542676269531" />
                  <Point X="28.00937109375" Y="1.468400390625" />
                  <Point X="28.006228515625" Y="1.454670043945" />
                  <Point X="28.0027734375" Y="1.432370849609" />
                  <Point X="28.001708984375" Y="1.421113525391" />
                  <Point X="28.000892578125" Y="1.39754284668" />
                  <Point X="28.001173828125" Y="1.386238037109" />
                  <Point X="28.003078125" Y="1.363750976562" />
                  <Point X="28.004701171875" Y="1.352568847656" />
                  <Point X="28.02175390625" Y="1.269927490234" />
                  <Point X="28.023623046875" Y="1.262421264648" />
                  <Point X="28.0304453125" Y="1.240255126953" />
                  <Point X="28.03826171875" Y="1.219885620117" />
                  <Point X="28.042435546875" Y="1.210545898438" />
                  <Point X="28.051765625" Y="1.192365112305" />
                  <Point X="28.056921875" Y="1.183524414062" />
                  <Point X="28.1031875" Y="1.113203125" />
                  <Point X="28.1116484375" Y="1.101549316406" />
                  <Point X="28.126244140625" Y="1.084236450195" />
                  <Point X="28.13405859375" Y="1.076017089844" />
                  <Point X="28.1513125" Y="1.059914794922" />
                  <Point X="28.16002734375" Y="1.052703613281" />
                  <Point X="28.17824609375" Y="1.039370605469" />
                  <Point X="28.18775" Y="1.033249023438" />
                  <Point X="28.254951171875" Y="0.995421203613" />
                  <Point X="28.261982421875" Y="0.991838928223" />
                  <Point X="28.283626953125" Y="0.982283630371" />
                  <Point X="28.304046875" Y="0.974910888672" />
                  <Point X="28.31382421875" Y="0.971964294434" />
                  <Point X="28.333638671875" Y="0.967137329102" />
                  <Point X="28.34367578125" Y="0.965257141113" />
                  <Point X="28.434546875" Y="0.953247192383" />
                  <Point X="28.457939453125" Y="0.951648742676" />
                  <Point X="28.4892421875" Y="0.95199017334" />
                  <Point X="28.50060546875" Y="0.952797180176" />
                  <Point X="28.550126953125" Y="0.959316955566" />
                  <Point X="29.704703125" Y="1.111319824219" />
                  <Point X="29.7515390625" Y="0.91893359375" />
                  <Point X="29.752681640625" Y="0.914245239258" />
                  <Point X="29.78387109375" Y="0.713920532227" />
                  <Point X="28.691994140625" Y="0.42135333252" />
                  <Point X="28.686033203125" Y="0.419544586182" />
                  <Point X="28.665623046875" Y="0.412135894775" />
                  <Point X="28.642380859375" Y="0.401618133545" />
                  <Point X="28.634005859375" Y="0.397316101074" />
                  <Point X="28.544732421875" Y="0.345714233398" />
                  <Point X="28.538337890625" Y="0.341670959473" />
                  <Point X="28.519837890625" Y="0.32854574585" />
                  <Point X="28.5022890625" Y="0.31425491333" />
                  <Point X="28.4945" Y="0.307157287598" />
                  <Point X="28.47975390625" Y="0.292142669678" />
                  <Point X="28.472796875" Y="0.284225830078" />
                  <Point X="28.41923046875" Y="0.215968811035" />
                  <Point X="28.410861328125" Y="0.204217651367" />
                  <Point X="28.399140625" Y="0.184945861816" />
                  <Point X="28.393853515625" Y="0.174959594727" />
                  <Point X="28.384076171875" Y="0.153493545532" />
                  <Point X="28.380009765625" Y="0.142941467285" />
                  <Point X="28.37316015625" Y="0.121432014465" />
                  <Point X="28.370376953125" Y="0.110474494934" />
                  <Point X="28.352521484375" Y="0.017238891602" />
                  <Point X="28.351390625" Y="0.009714858055" />
                  <Point X="28.349212890625" Y="-0.012964847565" />
                  <Point X="28.34856640625" Y="-0.035153354645" />
                  <Point X="28.348830078125" Y="-0.04551177597" />
                  <Point X="28.350484375" Y="-0.066147026062" />
                  <Point X="28.351875" Y="-0.07642414856" />
                  <Point X="28.36973046875" Y="-0.169659606934" />
                  <Point X="28.37316015625" Y="-0.183992050171" />
                  <Point X="28.3800078125" Y="-0.205495407104" />
                  <Point X="28.384072265625" Y="-0.216041549683" />
                  <Point X="28.393845703125" Y="-0.237502990723" />
                  <Point X="28.3991328125" Y="-0.24749029541" />
                  <Point X="28.41085546875" Y="-0.266768035889" />
                  <Point X="28.417291015625" Y="-0.276058441162" />
                  <Point X="28.470857421875" Y="-0.344315155029" />
                  <Point X="28.475900390625" Y="-0.350227081299" />
                  <Point X="28.491966796875" Y="-0.3671015625" />
                  <Point X="28.50821875" Y="-0.381992980957" />
                  <Point X="28.51620703125" Y="-0.388549346924" />
                  <Point X="28.532845703125" Y="-0.400755279541" />
                  <Point X="28.54149609375" Y="-0.406404754639" />
                  <Point X="28.630767578125" Y="-0.458004821777" />
                  <Point X="28.6513359375" Y="-0.46824911499" />
                  <Point X="28.68098828125" Y="-0.480230926514" />
                  <Point X="28.6919921875" Y="-0.48391293335" />
                  <Point X="28.737408203125" Y="-0.496081878662" />
                  <Point X="29.784880859375" Y="-0.776751281738" />
                  <Point X="29.7622578125" Y="-0.926796020508" />
                  <Point X="29.7616171875" Y="-0.931041503906" />
                  <Point X="29.727802734375" Y="-1.079219726563" />
                  <Point X="28.436783203125" Y="-0.909253845215" />
                  <Point X="28.428625" Y="-0.908535827637" />
                  <Point X="28.40009765625" Y="-0.908042541504" />
                  <Point X="28.366720703125" Y="-0.910841003418" />
                  <Point X="28.35448046875" Y="-0.912676330566" />
                  <Point X="28.1792578125" Y="-0.950761535645" />
                  <Point X="28.157873046875" Y="-0.956742858887" />
                  <Point X="28.12875390625" Y="-0.968366943359" />
                  <Point X="28.114677734375" Y="-0.97538848877" />
                  <Point X="28.086853515625" Y="-0.992278869629" />
                  <Point X="28.07412890625" Y="-1.001526672363" />
                  <Point X="28.050376953125" Y="-1.02199798584" />
                  <Point X="28.039349609375" Y="-1.033221557617" />
                  <Point X="27.933439453125" Y="-1.160598876953" />
                  <Point X="27.921326171875" Y="-1.176846557617" />
                  <Point X="27.90660546875" Y="-1.201229370117" />
                  <Point X="27.9001640625" Y="-1.213975219727" />
                  <Point X="27.8888203125" Y="-1.241362060547" />
                  <Point X="27.884361328125" Y="-1.25493347168" />
                  <Point X="27.87753125" Y="-1.282582519531" />
                  <Point X="27.87516015625" Y="-1.296660644531" />
                  <Point X="27.859986328125" Y="-1.4615625" />
                  <Point X="27.8592890625" Y="-1.483313598633" />
                  <Point X="27.861609375" Y="-1.514602539062" />
                  <Point X="27.8640703125" Y="-1.530147216797" />
                  <Point X="27.871802734375" Y="-1.561761230469" />
                  <Point X="27.8767890625" Y="-1.576675292969" />
                  <Point X="27.889158203125" Y="-1.605479858398" />
                  <Point X="27.896541015625" Y="-1.619370239258" />
                  <Point X="27.99351171875" Y="-1.770200683594" />
                  <Point X="28.007591796875" Y="-1.789351806641" />
                  <Point X="28.02272265625" Y="-1.807441650391" />
                  <Point X="28.02984375" Y="-1.815064208984" />
                  <Point X="28.044880859375" Y="-1.829481689453" />
                  <Point X="28.052794921875" Y="-1.836276367188" />
                  <Point X="28.094939453125" Y="-1.868615844727" />
                  <Point X="29.087169921875" Y="-2.629981689453" />
                  <Point X="29.047291015625" Y="-2.694512695312" />
                  <Point X="29.04548046875" Y="-2.697442626953" />
                  <Point X="29.001275390625" Y="-2.760252197266" />
                  <Point X="27.848453125" Y="-2.094669433594" />
                  <Point X="27.841189453125" Y="-2.090883544922" />
                  <Point X="27.815029296875" Y="-2.079513916016" />
                  <Point X="27.783123046875" Y="-2.069326416016" />
                  <Point X="27.771111328125" Y="-2.066337646484" />
                  <Point X="27.5625703125" Y="-2.028675170898" />
                  <Point X="27.539361328125" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460142578125" Y="-2.0314609375" />
                  <Point X="27.444845703125" Y="-2.035136352539" />
                  <Point X="27.4150703125" Y="-2.044960327148" />
                  <Point X="27.400591796875" Y="-2.051108886719" />
                  <Point X="27.227345703125" Y="-2.142287353516" />
                  <Point X="27.2089765625" Y="-2.153167236328" />
                  <Point X="27.186048828125" Y="-2.170056152344" />
                  <Point X="27.17521875" Y="-2.179365722656" />
                  <Point X="27.1542578125" Y="-2.20032421875" />
                  <Point X="27.1449453125" Y="-2.211154296875" />
                  <Point X="27.128048828125" Y="-2.234088378906" />
                  <Point X="27.12046484375" Y="-2.246192382812" />
                  <Point X="27.02928515625" Y="-2.419439208984" />
                  <Point X="27.0198359375" Y="-2.440192382812" />
                  <Point X="27.01001171875" Y="-2.469971435547" />
                  <Point X="27.0063359375" Y="-2.485268554688" />
                  <Point X="27.00137890625" Y="-2.517442871094" />
                  <Point X="27.000279296875" Y="-2.533133789062" />
                  <Point X="27.00068359375" Y="-2.564484130859" />
                  <Point X="27.0021875" Y="-2.580143554688" />
                  <Point X="27.0398515625" Y="-2.788685302734" />
                  <Point X="27.04519921875" Y="-2.810686767578" />
                  <Point X="27.05246484375" Y="-2.834349121094" />
                  <Point X="27.0561796875" Y="-2.844392578125" />
                  <Point X="27.064720703125" Y="-2.864006347656" />
                  <Point X="27.069546875" Y="-2.873576660156" />
                  <Point X="27.09662890625" Y="-2.920485351562" />
                  <Point X="27.73589453125" Y="-4.027725097656" />
                  <Point X="27.723755859375" Y="-4.036083740234" />
                  <Point X="26.83391796875" Y="-2.876423828125" />
                  <Point X="26.828654296875" Y="-2.870145751953" />
                  <Point X="26.80883203125" Y="-2.849625732422" />
                  <Point X="26.78325390625" Y="-2.828004638672" />
                  <Point X="26.77330078125" Y="-2.820646972656" />
                  <Point X="26.56762109375" Y="-2.688414794922" />
                  <Point X="26.546283203125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663873535156" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.183451171875" Y="-2.667216064453" />
                  <Point X="26.1612265625" Y="-2.670339355469" />
                  <Point X="26.133576171875" Y="-2.677171386719" />
                  <Point X="26.1200078125" Y="-2.681629394531" />
                  <Point X="26.092623046875" Y="-2.69297265625" />
                  <Point X="26.079876953125" Y="-2.699414306641" />
                  <Point X="26.05549609375" Y="-2.714134521484" />
                  <Point X="26.043861328125" Y="-2.722413085938" />
                  <Point X="25.8701640625" Y="-2.8668359375" />
                  <Point X="25.852654296875" Y="-2.883087890625" />
                  <Point X="25.83218359375" Y="-2.906837646484" />
                  <Point X="25.822935546875" Y="-2.919562988281" />
                  <Point X="25.80604296875" Y="-2.947389648438" />
                  <Point X="25.79901953125" Y="-2.961467041016" />
                  <Point X="25.78739453125" Y="-2.990588134766" />
                  <Point X="25.78279296875" Y="-3.005631835938" />
                  <Point X="25.730859375" Y="-3.244570556641" />
                  <Point X="25.7274765625" Y="-3.265916259766" />
                  <Point X="25.7248828125" Y="-3.291711425781" />
                  <Point X="25.72441796875" Y="-3.302672607422" />
                  <Point X="25.72475390625" Y="-3.324578125" />
                  <Point X="25.7255546875" Y="-3.335522460938" />
                  <Point X="25.73323046875" Y="-3.393820068359" />
                  <Point X="25.833087890625" Y="-4.152311523437" />
                  <Point X="25.65506640625" Y="-3.4879296875" />
                  <Point X="25.652603515625" Y="-3.480109863281" />
                  <Point X="25.642138671875" Y="-3.453562255859" />
                  <Point X="25.62678125" Y="-3.423804931641" />
                  <Point X="25.62040625" Y="-3.413206298828" />
                  <Point X="25.4623984375" Y="-3.185546630859" />
                  <Point X="25.446666015625" Y="-3.165166748047" />
                  <Point X="25.42477734375" Y="-3.142708007813" />
                  <Point X="25.412900390625" Y="-3.132388427734" />
                  <Point X="25.386646484375" Y="-3.113147949219" />
                  <Point X="25.373234375" Y="-3.104933105469" />
                  <Point X="25.34523828125" Y="-3.090828125" />
                  <Point X="25.330654296875" Y="-3.084937988281" />
                  <Point X="25.086146484375" Y="-3.009051513672" />
                  <Point X="25.063375" Y="-3.003108642578" />
                  <Point X="25.03521484375" Y="-2.998839355469" />
                  <Point X="25.020974609375" Y="-2.997766113281" />
                  <Point X="24.9913359375" Y="-2.997766357422" />
                  <Point X="24.977095703125" Y="-2.998839599609" />
                  <Point X="24.948935546875" Y="-3.003108886719" />
                  <Point X="24.935015625" Y="-3.006304931641" />
                  <Point X="24.6905078125" Y="-3.082191162109" />
                  <Point X="24.667068359375" Y="-3.090830810547" />
                  <Point X="24.63906640625" Y="-3.104940673828" />
                  <Point X="24.62565234375" Y="-3.113157958984" />
                  <Point X="24.59939453125" Y="-3.132404785156" />
                  <Point X="24.587521484375" Y="-3.142721191406" />
                  <Point X="24.565638671875" Y="-3.165175048828" />
                  <Point X="24.55562890625" Y="-3.1773125" />
                  <Point X="24.39762109375" Y="-3.404972167969" />
                  <Point X="24.38676953125" Y="-3.42258203125" />
                  <Point X="24.37383203125" Y="-3.446388183594" />
                  <Point X="24.3690703125" Y="-3.456534912109" />
                  <Point X="24.360779296875" Y="-3.477308105469" />
                  <Point X="24.35725" Y="-3.487934570312" />
                  <Point X="24.343255859375" Y="-3.540158935547" />
                  <Point X="24.014576171875" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.849782824987" Y="-2.495437215166" />
                  <Point X="21.291348651403" Y="-2.96895859083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.048422955212" Y="-3.780821385399" />
                  <Point X="22.277980822272" Y="-4.026992059174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.884250041026" Y="-2.393102256531" />
                  <Point X="21.37578519343" Y="-2.920209173847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.098093316097" Y="-3.694789803592" />
                  <Point X="22.513690050563" Y="-4.140462737622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.959967149647" Y="-2.335002391995" />
                  <Point X="21.460221735458" Y="-2.871459756864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.147763676982" Y="-3.608758221784" />
                  <Point X="22.572327025951" Y="-4.064046672642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.035684258269" Y="-2.276902527459" />
                  <Point X="21.544658277485" Y="-2.822710339881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.197434037867" Y="-3.522726639977" />
                  <Point X="22.63773571405" Y="-3.994892380487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.111401366891" Y="-2.218802662923" />
                  <Point X="21.629094819513" Y="-2.773960922899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.247104398751" Y="-3.43669505817" />
                  <Point X="22.717310839599" Y="-3.940929732587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.326628740664" Y="-1.237940531437" />
                  <Point X="20.368482912015" Y="-1.282823635178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.187118475513" Y="-2.160702798387" />
                  <Point X="21.71353136154" Y="-2.725211505916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.296774759636" Y="-3.350663476363" />
                  <Point X="22.797341097775" Y="-3.887455154674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.277624924418" Y="-1.046093849588" />
                  <Point X="20.484175655914" Y="-1.267592391077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.262835584135" Y="-2.102602933851" />
                  <Point X="21.797967903568" Y="-2.676462088933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.346445120521" Y="-3.264631894555" />
                  <Point X="22.87737135595" Y="-3.833980576761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.244535515172" Y="-0.871313279843" />
                  <Point X="20.599868399814" Y="-1.252361146976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.338552692757" Y="-2.044503069315" />
                  <Point X="21.882404445595" Y="-2.62771267195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.396115481406" Y="-3.178600312748" />
                  <Point X="22.965391255022" Y="-3.78907383975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.221004169725" Y="-0.706782478645" />
                  <Point X="20.715561143714" Y="-1.237129902875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.414269801379" Y="-1.986403204779" />
                  <Point X="21.966840987623" Y="-2.578963254968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.445785842291" Y="-3.092568730941" />
                  <Point X="23.084895165878" Y="-3.777929571841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.873007524327" Y="-4.623076605026" />
                  <Point X="24.005371965246" Y="-4.765020089787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.284549699758" Y="-0.635630194079" />
                  <Point X="20.831253887614" Y="-1.221898658774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.489986910001" Y="-1.928303340242" />
                  <Point X="22.05127752965" Y="-2.530213837985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.495456203176" Y="-3.006537149134" />
                  <Point X="23.223247395697" Y="-3.786997651426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.87414174012" Y="-4.484996379917" />
                  <Point X="24.041887047156" Y="-4.664881198435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.388477644371" Y="-0.607782747343" />
                  <Point X="20.946946631513" Y="-1.206667414673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.565704018623" Y="-1.870203475706" />
                  <Point X="22.135714071678" Y="-2.481464421002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.545126564061" Y="-2.920505567327" />
                  <Point X="23.361599625517" Y="-3.79606573101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.838920250879" Y="-4.307929434298" />
                  <Point X="24.070880490009" Y="-4.556676336711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.492405588984" Y="-0.579935300607" />
                  <Point X="21.062639375413" Y="-1.191436170573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.641421127245" Y="-1.81210361117" />
                  <Point X="22.220150613705" Y="-2.432715004019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.594796924945" Y="-2.834473985519" />
                  <Point X="23.730317645697" Y="-4.052170876038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.764283784179" Y="-4.088595100147" />
                  <Point X="24.099873932862" Y="-4.448471474986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.596333533596" Y="-0.552087853871" />
                  <Point X="21.178332119313" Y="-1.176204926472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.717138235866" Y="-1.754003746634" />
                  <Point X="22.304587241713" Y="-2.383965679239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.64446728583" Y="-2.748442403712" />
                  <Point X="24.128867375714" Y="-4.340266613262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.700261478209" Y="-0.524240407135" />
                  <Point X="21.294024863212" Y="-1.160973682371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.792855344488" Y="-1.695903882098" />
                  <Point X="22.403523252404" Y="-2.350765038763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.683364538067" Y="-2.650858077281" />
                  <Point X="24.157860818567" Y="-4.232061751537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.804189422822" Y="-0.496392960399" />
                  <Point X="21.409717607112" Y="-1.14574243827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.86857245311" Y="-1.637804017562" />
                  <Point X="22.575197869872" Y="-2.395567004205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.652980700938" Y="-2.478978878418" />
                  <Point X="24.18685426142" Y="-4.123856889812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.908117367434" Y="-0.468545513663" />
                  <Point X="21.525410351012" Y="-1.130511194169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.944289561732" Y="-1.579704153026" />
                  <Point X="24.215847704272" Y="-4.015652028088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.012045312047" Y="-0.440698066927" />
                  <Point X="21.641103094911" Y="-1.115279950068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.00766046933" Y="-1.508364608824" />
                  <Point X="24.244841147125" Y="-3.907447166363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.11597325666" Y="-0.412850620191" />
                  <Point X="21.756879512227" Y="-1.100138434721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.046333383745" Y="-1.410539709532" />
                  <Point X="24.273834589978" Y="-3.799242304639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.227488275517" Y="0.679229395549" />
                  <Point X="20.340451935193" Y="0.558090701542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.219901201272" Y="-0.385003173455" />
                  <Point X="21.924491418715" Y="-1.14058367603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.030891261871" Y="-1.254683538583" />
                  <Point X="24.302828032831" Y="-3.691037442914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.245277641318" Y="0.799449158928" />
                  <Point X="20.513615962816" Y="0.511691539253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.323829145885" Y="-0.357155726719" />
                  <Point X="24.331821475683" Y="-3.582832581189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.26306700712" Y="0.919668922307" />
                  <Point X="20.68677999044" Y="0.465292376964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.427757090498" Y="-0.329308279983" />
                  <Point X="24.361539129294" Y="-3.475404340421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.288705992188" Y="1.031470999599" />
                  <Point X="20.859944018063" Y="0.418893214675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.528469567137" Y="-0.298012666005" />
                  <Point X="24.409626267751" Y="-3.387674960421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.317953562576" Y="1.139403342907" />
                  <Point X="21.033108045686" Y="0.372494052386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.609273150772" Y="-0.245367378117" />
                  <Point X="24.465052637021" Y="-3.307815941901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.347201132964" Y="1.247335686214" />
                  <Point X="21.206272073309" Y="0.326094890097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.668951279805" Y="-0.170067813729" />
                  <Point X="24.520479006291" Y="-3.227956923381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.417217571789" Y="1.311548770667" />
                  <Point X="21.379436100932" Y="0.279695727807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.702342494717" Y="-0.066578985155" />
                  <Point X="24.579023143436" Y="-3.151441301574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.565292548845" Y="1.292054321169" />
                  <Point X="21.57569290233" Y="0.208532597494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.667848398002" Y="0.109707927479" />
                  <Point X="24.657088450937" Y="-3.095859572041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.713367525902" Y="1.272559871672" />
                  <Point X="24.755444647697" Y="-3.062037157248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.861442502959" Y="1.253065422174" />
                  <Point X="24.856184726264" Y="-3.030771142713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.805287378758" Y="-4.048559129849" />
                  <Point X="25.821753269132" Y="-4.066216635468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.009517480016" Y="1.233570972677" />
                  <Point X="24.95887952293" Y="-3.001601306704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.752913630902" Y="-3.853098638785" />
                  <Point X="25.800399827476" Y="-3.90402135015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.157592457072" Y="1.214076523179" />
                  <Point X="25.099623599484" Y="-3.013234327886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.700539883045" Y="-3.657638147721" />
                  <Point X="25.779046385821" Y="-3.741826064831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.293340801081" Y="1.207800769262" />
                  <Point X="25.282426355948" Y="-3.069969761388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.643607967666" Y="-3.457289620431" />
                  <Point X="25.757692944165" Y="-3.579630779513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.395675446618" Y="1.237356820073" />
                  <Point X="25.73633950251" Y="-3.417435494194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.478280809246" Y="1.288069936546" />
                  <Point X="25.727229830121" Y="-3.26837004393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.534501692654" Y="1.367076942965" />
                  <Point X="25.750531595181" Y="-3.154061605034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.782926714005" Y="2.312338955942" />
                  <Point X="20.868636331491" Y="2.220426644001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.57174138927" Y="1.466438780179" />
                  <Point X="25.775084941008" Y="-3.041095322188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.832932624783" Y="2.398010704542" />
                  <Point X="21.325282604178" Y="1.870029992258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.557638120788" Y="1.620859206644" />
                  <Point X="25.810599611646" Y="-2.939883621093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.882938535562" Y="2.483682453142" />
                  <Point X="25.871410908895" Y="-2.865799230842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.932944446341" Y="2.569354201743" />
                  <Point X="25.944577313815" Y="-2.804964071468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.98295035712" Y="2.655025950343" />
                  <Point X="26.017743718735" Y="-2.744128912093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.039351449873" Y="2.733839705899" />
                  <Point X="26.097898230914" Y="-2.690787580285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.098432320041" Y="2.809779752005" />
                  <Point X="26.204046245744" Y="-2.665320867384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.15751319021" Y="2.885719798112" />
                  <Point X="26.323676656575" Y="-2.654312254091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.216594060378" Y="2.961659844218" />
                  <Point X="26.44781059327" Y="-2.64813308102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.638565235442" Y="-3.925061100601" />
                  <Point X="27.734924100557" Y="-4.028393332484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.283248989966" Y="3.029477705994" />
                  <Point X="26.686599758006" Y="-2.76490658694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.035177326903" Y="-3.138710264842" />
                  <Point X="27.527327573782" Y="-3.666476790226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.564645696017" Y="2.867013205957" />
                  <Point X="27.316170129822" Y="-3.300741631797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.816491393438" Y="2.736238282924" />
                  <Point X="27.105012685861" Y="-2.935006473369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.958069467136" Y="2.7237109093" />
                  <Point X="27.025773432661" Y="-2.710736254996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.062522482996" Y="2.75099528606" />
                  <Point X="27.000422709419" Y="-2.54425440998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.137708018971" Y="2.80966519247" />
                  <Point X="27.02424642575" Y="-2.430505695294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.202791646956" Y="2.879168068919" />
                  <Point X="27.07060715063" Y="-2.340924963393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.245751396352" Y="2.972395900513" />
                  <Point X="27.117470023507" Y="-2.251882719293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.245271544985" Y="3.112207000741" />
                  <Point X="27.177729965884" Y="-2.17720707333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.125108971788" Y="3.380362106989" />
                  <Point X="27.259353969947" Y="-2.125441578638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.950178919723" Y="3.707248143902" />
                  <Point X="27.346487095615" Y="-2.079583893575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.025922340601" Y="3.765319791998" />
                  <Point X="27.437266957192" Y="-2.037636853995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.101665761479" Y="3.823391440094" />
                  <Point X="27.558315974312" Y="-2.028149509698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.177409182357" Y="3.88146308819" />
                  <Point X="27.714246346887" Y="-2.056067839554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.253152603235" Y="3.939534736286" />
                  <Point X="27.917095008233" Y="-2.134299874216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.328896024113" Y="3.997606384382" />
                  <Point X="28.198491897026" Y="-2.29676457022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.411511561402" Y="4.048308589867" />
                  <Point X="28.479888785818" Y="-2.459229266224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.497077490599" Y="4.095846887387" />
                  <Point X="28.761285674611" Y="-2.621693962228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.582643419796" Y="4.143385184908" />
                  <Point X="27.862183378355" Y="-1.518228269975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.425344918525" Y="-2.122145084343" />
                  <Point X="29.009496550448" Y="-2.748571016327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.668209348993" Y="4.190923482428" />
                  <Point X="27.867104217952" Y="-1.38420870175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.881993309479" Y="-2.472544007649" />
                  <Point X="29.063907960971" Y="-2.667623587804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.753775278191" Y="4.238461779949" />
                  <Point X="27.882704323213" Y="-1.26164124387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.052330111783" Y="4.057597440813" />
                  <Point X="27.926747343886" Y="-1.169575078499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.205431716203" Y="4.032712593415" />
                  <Point X="27.987567196138" Y="-1.095499862366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.304614903033" Y="4.065648169933" />
                  <Point X="28.049630326341" Y="-1.022757898607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.394925935008" Y="4.108097967709" />
                  <Point X="28.128791904088" Y="-0.968351774983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.463292797511" Y="4.174080006194" />
                  <Point X="28.231663080379" Y="-0.939371082965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.504993309897" Y="4.268658204155" />
                  <Point X="28.339668163905" Y="-0.915895832426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.534848261195" Y="4.375939211179" />
                  <Point X="28.467091391933" Y="-0.913243992457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.525685511818" Y="4.525061579546" />
                  <Point X="28.615166351639" Y="-0.932738423349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.599843258321" Y="4.584833655226" />
                  <Point X="28.763241311346" Y="-0.952232854241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.702817294709" Y="4.613704043293" />
                  <Point X="28.911316271053" Y="-0.971727285133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.805791331098" Y="4.642574431361" />
                  <Point X="29.05939123076" Y="-0.991221716025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.908765367486" Y="4.671444819429" />
                  <Point X="28.355919718597" Y="-0.097544355352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.724282313819" Y="-0.492564876412" />
                  <Point X="29.207466190466" Y="-1.010716146917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.011739403875" Y="4.700315207497" />
                  <Point X="28.356995142298" Y="0.040598916556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.897446244919" Y="-0.538963935193" />
                  <Point X="29.355541150173" Y="-1.030210577809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.121014626614" Y="4.722428400486" />
                  <Point X="28.383356582756" Y="0.151626255294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.070610261264" Y="-0.585363085387" />
                  <Point X="29.50361610988" Y="-1.049705008701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.23812930188" Y="4.736134809882" />
                  <Point X="28.434777914264" Y="0.235780150993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.243774277608" Y="-0.631762235581" />
                  <Point X="29.651691069587" Y="-1.069199439593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.355243977146" Y="4.749841219277" />
                  <Point X="24.759782771102" Y="4.316026474648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.972279818902" Y="4.088151289615" />
                  <Point X="28.496453666533" Y="0.308937526728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.416938293953" Y="-0.678161385776" />
                  <Point X="29.74021403955" Y="-1.024832180064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.472358652412" Y="4.763547628673" />
                  <Point X="24.707409881923" Y="4.511486044892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.089681727665" Y="4.101549678796" />
                  <Point X="28.575481043033" Y="0.36348756357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.590102310297" Y="-0.72456053597" />
                  <Point X="29.76454488269" Y="-0.911627292301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.589473327678" Y="4.777254038069" />
                  <Point X="24.655036992745" Y="4.706945615137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.171958103721" Y="4.152615590175" />
                  <Point X="28.661677029147" Y="0.410350207767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.763266326641" Y="-0.770959686164" />
                  <Point X="29.782624180517" Y="-0.791718442953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.226532297426" Y="4.233388455107" />
                  <Point X="27.020668177136" Y="2.309413276173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.320577144007" Y="1.987800284245" />
                  <Point X="28.039727255889" Y="1.216606206451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.236079612534" Y="1.006044083046" />
                  <Point X="28.763456514413" Y="0.440501595081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.258519925879" Y="4.338382445881" />
                  <Point X="27.01818917575" Y="2.451368202327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.461022342887" Y="1.976487770128" />
                  <Point X="28.000892744294" Y="1.397547644191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.412491376194" Y="0.956162150253" />
                  <Point X="28.867384512028" Y="0.468348984979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.287513512657" Y="4.446587153265" />
                  <Point X="27.047733034091" Y="2.558982815705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.567912068872" Y="2.001159095195" />
                  <Point X="28.022140727467" Y="1.514058494521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.540613549037" Y="0.958064463671" />
                  <Point X="28.971312509644" Y="0.496196374876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.316507099434" Y="4.554791860649" />
                  <Point X="27.094134306117" Y="2.648520066114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.659996124699" Y="2.041707557669" />
                  <Point X="28.062158531585" Y="1.610441176177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.656306271332" Y="0.973295730941" />
                  <Point X="29.07524050726" Y="0.524043764773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.345500686212" Y="4.662996568033" />
                  <Point X="27.143804616981" Y="2.734551701562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.744432482666" Y="2.090457172033" />
                  <Point X="28.119779676221" Y="1.687946586268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.771999017067" Y="0.988526973073" />
                  <Point X="29.179168504875" Y="0.55189115467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.37449427299" Y="4.771201275418" />
                  <Point X="27.193474927846" Y="2.820583337009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.828869053592" Y="2.139206558026" />
                  <Point X="28.187303964694" Y="1.754832174779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.887691762802" Y="1.003758215205" />
                  <Point X="29.283096502491" Y="0.579738544568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.503781978004" Y="4.771853708605" />
                  <Point X="27.24314523871" Y="2.906614972457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.913305624518" Y="2.187955944018" />
                  <Point X="28.263021095847" Y="1.812932015154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.003384508538" Y="1.018989457337" />
                  <Point X="29.387024500106" Y="0.607585934465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.647736768809" Y="4.756777617923" />
                  <Point X="27.292815549575" Y="2.992646607904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.997742195445" Y="2.236705330011" />
                  <Point X="28.338738227001" Y="1.871031855528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.119077254273" Y="1.03422069947" />
                  <Point X="29.490952497722" Y="0.635433324362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.791691559615" Y="4.74170152724" />
                  <Point X="27.342485860439" Y="3.078678243352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.082178766371" Y="2.285454716003" />
                  <Point X="28.414455358154" Y="1.929131695902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.234770000008" Y="1.049451941602" />
                  <Point X="29.594880495337" Y="0.66328071426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.953375907204" Y="4.707612814622" />
                  <Point X="27.392156171304" Y="3.164709878799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.166615337298" Y="2.334204101996" />
                  <Point X="28.490172489307" Y="1.987231536276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.350462745744" Y="1.064683183734" />
                  <Point X="29.698808492953" Y="0.691128104157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.121013581137" Y="4.66713994111" />
                  <Point X="27.441826482168" Y="3.250741514247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.251051908224" Y="2.382953487988" />
                  <Point X="28.56588962046" Y="2.045331376651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.466155491479" Y="1.079914425867" />
                  <Point X="29.779145220838" Y="0.744274033543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.288651255071" Y="4.626667067598" />
                  <Point X="27.491496793033" Y="3.336773149694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.33548847915" Y="2.431702873981" />
                  <Point X="28.641606751613" Y="2.103431217025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.581848237214" Y="1.095145667999" />
                  <Point X="29.753110756077" Y="0.911489101571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.456749128832" Y="4.58570069019" />
                  <Point X="27.541167103897" Y="3.422804785142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.419925050077" Y="2.480452259973" />
                  <Point X="28.717323882767" Y="2.161531057399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.69754098295" Y="1.110376910131" />
                  <Point X="29.7075441583" Y="1.099649817885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.653034818483" Y="4.514506581019" />
                  <Point X="27.590837414762" Y="3.508836420589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.504361621003" Y="2.529201645966" />
                  <Point X="28.79304101392" Y="2.219630897773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.849320508134" Y="4.443312471847" />
                  <Point X="27.640507725626" Y="3.594868056037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.58879819193" Y="2.577951031958" />
                  <Point X="28.868758145073" Y="2.277730738148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.078125503678" Y="4.337245676564" />
                  <Point X="27.690178036491" Y="3.680899691484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.673234762856" Y="2.626700417951" />
                  <Point X="28.944475276226" Y="2.335830578522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.322098051647" Y="4.214913672653" />
                  <Point X="27.739848347355" Y="3.766931326932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.757671333783" Y="2.675449803943" />
                  <Point X="29.020192407379" Y="2.393930418896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.606513268166" Y="4.049212216439" />
                  <Point X="27.78951865822" Y="3.852962962379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.842107904709" Y="2.724199189936" />
                  <Point X="29.095909538532" Y="2.45203025927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.926544475635" Y="2.772948575928" />
                  <Point X="29.012449067246" Y="2.680827179837" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.9998359375" Y="0.001626220703" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.85812890625" Y="-4.979877441406" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.46431640625" Y="-3.521540283203" />
                  <Point X="25.30630859375" Y="-3.293880615234" />
                  <Point X="25.300587890625" Y="-3.285639892578" />
                  <Point X="25.274333984375" Y="-3.266399414062" />
                  <Point X="25.029826171875" Y="-3.190512939453" />
                  <Point X="25.020974609375" Y="-3.187766113281" />
                  <Point X="24.9913359375" Y="-3.187766357422" />
                  <Point X="24.746828125" Y="-3.263652587891" />
                  <Point X="24.7379765625" Y="-3.266399658203" />
                  <Point X="24.71171875" Y="-3.285646484375" />
                  <Point X="24.5537109375" Y="-3.513306152344" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.526779296875" Y="-3.589336669922" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="23.90875390625" Y="-4.939812011719" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.53475390625" />
                  <Point X="23.631904296875" Y="-4.241092285156" />
                  <Point X="23.6297890625" Y="-4.230462402344" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.38860546875" Y="-4.005209960938" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.051984375" Y="-3.966180175781" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791015625" />
                  <Point X="22.761166015625" Y="-4.140137695312" />
                  <Point X="22.75215234375" Y="-4.146161621094" />
                  <Point X="22.73241015625" Y="-4.167532714844" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.1573515625" Y="-4.175774414063" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.771419921875" Y="-3.880607421875" />
                  <Point X="21.775537109375" Y="-3.873476318359" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.59759375" />
                  <Point X="22.486021484375" Y="-2.568764892578" />
                  <Point X="22.46929296875" Y="-2.552035888672" />
                  <Point X="22.4686875" Y="-2.551429931641" />
                  <Point X="22.43985546875" Y="-2.537200195312" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="22.363275390625" Y="-2.569475097656" />
                  <Point X="21.157041015625" Y="-3.265894042969" />
                  <Point X="20.84871484375" Y="-2.860815673828" />
                  <Point X="20.838296875" Y="-2.847126464844" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="20.5791953125" Y="-2.387689453125" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.396013793945" />
                  <Point X="21.861609375" Y="-1.367322143555" />
                  <Point X="21.86187890625" Y="-1.366282958984" />
                  <Point X="21.859677734375" Y="-1.334603881836" />
                  <Point X="21.838841796875" Y="-1.310639526367" />
                  <Point X="21.81328515625" Y="-1.29559753418" />
                  <Point X="21.81234765625" Y="-1.295046142578" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.723611328125" Y="-1.296057128906" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.07668359375" Y="-1.027143676758" />
                  <Point X="20.072607421875" Y="-1.011188598633" />
                  <Point X="20.001603515625" Y="-0.5147421875" />
                  <Point X="20.010603515625" Y="-0.512330871582" />
                  <Point X="21.442537109375" Y="-0.128645339966" />
                  <Point X="21.45810546875" Y="-0.121424980164" />
                  <Point X="21.484888671875" Y="-0.102835762024" />
                  <Point X="21.485833984375" Y="-0.102179534912" />
                  <Point X="21.505103515625" Y="-0.075907676697" />
                  <Point X="21.51403125" Y="-0.047141880035" />
                  <Point X="21.51436328125" Y="-0.046063220978" />
                  <Point X="21.514353515625" Y="-0.016459415436" />
                  <Point X="21.50542578125" Y="0.012306232452" />
                  <Point X="21.50511328125" Y="0.013310048103" />
                  <Point X="21.485859375" Y="0.039602741241" />
                  <Point X="21.459076171875" Y="0.058191959381" />
                  <Point X="21.442537109375" Y="0.066085273743" />
                  <Point X="21.390705078125" Y="0.07997379303" />
                  <Point X="20.001814453125" Y="0.452126037598" />
                  <Point X="20.07973046875" Y="0.97867956543" />
                  <Point X="20.08235546875" Y="0.996417663574" />
                  <Point X="20.226484375" Y="1.528299072266" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866699219" />
                  <Point X="21.327576171875" Y="1.414557739258" />
                  <Point X="21.329716796875" Y="1.415232299805" />
                  <Point X="21.34846484375" Y="1.42605456543" />
                  <Point X="21.3608828125" Y="1.443786743164" />
                  <Point X="21.38466796875" Y="1.501212402344" />
                  <Point X="21.385525390625" Y="1.503283691406" />
                  <Point X="21.38928515625" Y="1.524603271484" />
                  <Point X="21.38368359375" Y="1.545514282227" />
                  <Point X="21.354982421875" Y="1.6006484375" />
                  <Point X="21.353943359375" Y="1.602644165039" />
                  <Point X="21.34003125" Y="1.619221801758" />
                  <Point X="21.31030078125" Y="1.642035522461" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.829787109375" Y="2.769531738281" />
                  <Point X="20.83998828125" Y="2.787008300781" />
                  <Point X="21.225330078125" Y="3.282310546875" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920435058594" />
                  <Point X="21.944046875" Y="2.913211914062" />
                  <Point X="21.94703515625" Y="2.912950439453" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927404785156" />
                  <Point X="22.0453515625" Y="2.986007080078" />
                  <Point X="22.04747265625" Y="2.988128173828" />
                  <Point X="22.0591015625" Y="3.006384277344" />
                  <Point X="22.06192578125" Y="3.027845458984" />
                  <Point X="22.054703125" Y="3.110406494141" />
                  <Point X="22.05444140625" Y="3.113395019531" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="22.0347578125" Y="3.156852294922" />
                  <Point X="21.69272265625" Y="3.749276611328" />
                  <Point X="22.229365234375" Y="4.160714355469" />
                  <Point X="22.24712890625" Y="4.174334472656" />
                  <Point X="22.858453125" Y="4.513972167969" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.14064453125" Y="4.225825683594" />
                  <Point X="23.143966796875" Y="4.224096191406" />
                  <Point X="23.164875" Y="4.2184921875" />
                  <Point X="23.18619140625" Y="4.22225" />
                  <Point X="23.281900390625" Y="4.26189453125" />
                  <Point X="23.285365234375" Y="4.263329589844" />
                  <Point X="23.303095703125" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.294489257812" />
                  <Point X="23.3450625" Y="4.393272949219" />
                  <Point X="23.346193359375" Y="4.396848632813" />
                  <Point X="23.348083984375" Y="4.418426757812" />
                  <Point X="23.3465859375" Y="4.429804199219" />
                  <Point X="23.31086328125" Y="4.701141113281" />
                  <Point X="24.008912109375" Y="4.896849609375" />
                  <Point X="24.03190625" Y="4.903296386719" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.061205078125" Y="4.336097167969" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.840125" Y="4.927668457031" />
                  <Point X="25.860205078125" Y="4.925565429688" />
                  <Point X="26.488306640625" Y="4.773922851562" />
                  <Point X="26.508462890625" Y="4.769056152344" />
                  <Point X="26.918212890625" Y="4.620437011719" />
                  <Point X="26.931033203125" Y="4.615786621094" />
                  <Point X="27.32635546875" Y="4.430908203125" />
                  <Point X="27.33869140625" Y="4.425138671875" />
                  <Point X="27.72058984375" Y="4.202643554688" />
                  <Point X="27.73252734375" Y="4.195688476562" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="28.0613828125" Y="3.94384765625" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491514648438" />
                  <Point X="27.2041328125" Y="2.414032958984" />
                  <Point X="27.202044921875" Y="2.392324951172" />
                  <Point X="27.21012109375" Y="2.325355224609" />
                  <Point X="27.210412109375" Y="2.322929931641" />
                  <Point X="27.218681640625" Y="2.3008125" />
                  <Point X="27.260138671875" Y="2.239715332031" />
                  <Point X="27.26015234375" Y="2.239697265625" />
                  <Point X="27.27494140625" Y="2.224204101562" />
                  <Point X="27.336037109375" Y="2.182747070313" />
                  <Point X="27.338263671875" Y="2.181237060547" />
                  <Point X="27.360333984375" Y="2.172980712891" />
                  <Point X="27.427333984375" Y="2.164901611328" />
                  <Point X="27.4486640625" Y="2.165946289062" />
                  <Point X="27.526146484375" Y="2.186666015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="27.59316796875" Y="2.222516845703" />
                  <Point X="28.99425" Y="3.031431152344" />
                  <Point X="29.195515625" Y="2.751716552734" />
                  <Point X="29.202587890625" Y="2.741889160156" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="29.38340234375" Y="2.433140869141" />
                  <Point X="28.2886171875" Y="1.593082763672" />
                  <Point X="28.27937109375" Y="1.583833129883" />
                  <Point X="28.223607421875" Y="1.511085205078" />
                  <Point X="28.2236015625" Y="1.511076416016" />
                  <Point X="28.21312109375" Y="1.491500976562" />
                  <Point X="28.19234765625" Y="1.417225097656" />
                  <Point X="28.191595703125" Y="1.414536621094" />
                  <Point X="28.190779296875" Y="1.390965820312" />
                  <Point X="28.20783203125" Y="1.308324462891" />
                  <Point X="28.2156484375" Y="1.287954956055" />
                  <Point X="28.26201953125" Y="1.217474609375" />
                  <Point X="28.2636953125" Y="1.214922241211" />
                  <Point X="28.28094921875" Y="1.198819702148" />
                  <Point X="28.348150390625" Y="1.160991943359" />
                  <Point X="28.348150390625" Y="1.160991210938" />
                  <Point X="28.3685703125" Y="1.153619140625" />
                  <Point X="28.45944140625" Y="1.14160925293" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="28.525326171875" Y="1.14769152832" />
                  <Point X="29.8489765625" Y="1.321953369141" />
                  <Point X="29.9361484375" Y="0.963875488281" />
                  <Point X="29.939189453125" Y="0.951378173828" />
                  <Point X="29.997859375" Y="0.574555908203" />
                  <Point X="29.99581640625" Y="0.574008544922" />
                  <Point X="28.74116796875" Y="0.237826843262" />
                  <Point X="28.729087890625" Y="0.23281918335" />
                  <Point X="28.639814453125" Y="0.181217193604" />
                  <Point X="28.639814453125" Y="0.181217529297" />
                  <Point X="28.622265625" Y="0.166926361084" />
                  <Point X="28.56869921875" Y="0.098669494629" />
                  <Point X="28.566763671875" Y="0.096203536987" />
                  <Point X="28.556986328125" Y="0.074737342834" />
                  <Point X="28.539130859375" Y="-0.018498380661" />
                  <Point X="28.538484375" Y="-0.040686824799" />
                  <Point X="28.55633984375" Y="-0.133922393799" />
                  <Point X="28.556986328125" Y="-0.137297409058" />
                  <Point X="28.566759765625" Y="-0.158758880615" />
                  <Point X="28.620326171875" Y="-0.22701574707" />
                  <Point X="28.620330078125" Y="-0.227020309448" />
                  <Point X="28.636578125" Y="-0.241907165527" />
                  <Point X="28.725849609375" Y="-0.293507110596" />
                  <Point X="28.74116796875" Y="-0.300386901855" />
                  <Point X="28.786583984375" Y="-0.312555908203" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.950130859375" Y="-0.955139099121" />
                  <Point X="29.9484296875" Y="-0.966416381836" />
                  <Point X="29.874548828125" Y="-1.290178833008" />
                  <Point X="29.86701953125" Y="-1.28918762207" />
                  <Point X="28.411982421875" Y="-1.097628295898" />
                  <Point X="28.3948359375" Y="-1.098341308594" />
                  <Point X="28.21961328125" Y="-1.136426513672" />
                  <Point X="28.213271484375" Y="-1.137805175781" />
                  <Point X="28.185447265625" Y="-1.154695678711" />
                  <Point X="28.079537109375" Y="-1.282072998047" />
                  <Point X="28.075703125" Y="-1.28668371582" />
                  <Point X="28.064359375" Y="-1.314070556641" />
                  <Point X="28.0491796875" Y="-1.479029785156" />
                  <Point X="28.04862890625" Y="-1.485006103516" />
                  <Point X="28.056361328125" Y="-1.516620117188" />
                  <Point X="28.15333203125" Y="-1.667450805664" />
                  <Point X="28.168462890625" Y="-1.685540771484" />
                  <Point X="28.210607421875" Y="-1.717880249023" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.208919921875" Y="-2.794395996094" />
                  <Point X="29.2041328125" Y="-2.802140869141" />
                  <Point X="29.056689453125" Y="-3.011638183594" />
                  <Point X="29.04936328125" Y="-3.007408691406" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.52880078125" Y="-2.215650390625" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489080078125" Y="-2.219244628906" />
                  <Point X="27.315833984375" Y="-2.310423095703" />
                  <Point X="27.3095625" Y="-2.313723632812" />
                  <Point X="27.2886015625" Y="-2.334682128906" />
                  <Point X="27.197421875" Y="-2.507928955078" />
                  <Point X="27.19412109375" Y="-2.514200195312" />
                  <Point X="27.1891640625" Y="-2.546374511719" />
                  <Point X="27.226828125" Y="-2.754916259766" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.26117578125" Y="-2.825487304688" />
                  <Point X="27.98667578125" Y="-4.082088623047" />
                  <Point X="27.840982421875" Y="-4.186153808594" />
                  <Point X="27.835302734375" Y="-4.190209960937" />
                  <Point X="27.67977734375" Y="-4.290879394531" />
                  <Point X="27.672076171875" Y="-4.280842773438" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.46487109375" Y="-2.848235107422" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.200861328125" Y="-2.856416748047" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509277344" />
                  <Point X="25.99163671875" Y="-3.012932128906" />
                  <Point X="25.985349609375" Y="-3.018159667969" />
                  <Point X="25.96845703125" Y="-3.045986328125" />
                  <Point X="25.9165234375" Y="-3.284925048828" />
                  <Point X="25.9165234375" Y="-3.284925292969" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="25.92160546875" Y="-3.369017822266" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="25.99971875" Y="-4.962069824219" />
                  <Point X="25.994345703125" Y="-4.963247558594" />
                  <Point X="25.860203125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#214" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.186493938389" Y="5.051129362463" Z="2.45" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.45" />
                  <Point X="-0.220866811266" Y="5.073090251485" Z="2.45" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.45" />
                  <Point X="-1.010742330347" Y="4.976273180148" Z="2.45" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.45" />
                  <Point X="-1.709143720482" Y="4.454557701065" Z="2.45" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.45" />
                  <Point X="-1.708773506062" Y="4.439604238381" Z="2.45" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.45" />
                  <Point X="-1.743389593664" Y="4.339369066807" Z="2.45" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.45" />
                  <Point X="-1.842425240881" Y="4.301456096782" Z="2.45" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.45" />
                  <Point X="-2.127304019744" Y="4.600799361499" Z="2.45" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.45" />
                  <Point X="-2.157074521177" Y="4.59724461012" Z="2.45" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.45" />
                  <Point X="-2.80721617181" Y="4.231673251636" Z="2.45" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.45" />
                  <Point X="-3.014699499519" Y="3.163132353748" Z="2.45" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.45" />
                  <Point X="-3.001263224259" Y="3.137324409654" Z="2.45" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.45" />
                  <Point X="-2.996161271317" Y="3.05264229657" Z="2.45" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.45" />
                  <Point X="-3.057752055953" Y="2.994301474914" Z="2.45" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.45" />
                  <Point X="-3.77072699455" Y="3.365494449226" Z="2.45" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.45" />
                  <Point X="-3.808013230579" Y="3.360074237936" Z="2.45" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.45" />
                  <Point X="-4.219551951918" Y="2.826017456199" Z="2.45" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.45" />
                  <Point X="-3.726293530182" Y="1.633647701974" Z="2.45" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.45" />
                  <Point X="-3.6955233861" Y="1.608838405251" Z="2.45" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.45" />
                  <Point X="-3.667683445081" Y="1.551625714951" Z="2.45" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.45" />
                  <Point X="-3.693615711366" Y="1.493523541824" Z="2.45" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.45" />
                  <Point X="-4.779341102737" Y="1.609966627755" Z="2.45" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.45" />
                  <Point X="-4.821957150459" Y="1.59470444553" Z="2.45" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.45" />
                  <Point X="-4.975887284906" Y="1.017278921801" Z="2.45" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.45" />
                  <Point X="-3.628393228122" Y="0.062957506772" Z="2.45" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.45" />
                  <Point X="-3.575591242911" Y="0.048396148711" Z="2.45" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.45" />
                  <Point X="-3.548484689604" Y="0.028765703159" Z="2.45" />
                  <Point X="-3.539556741714" Y="0" Z="2.45" />
                  <Point X="-3.539879911531" Y="-0.001041247902" Z="2.45" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.45" />
                  <Point X="-3.549777352213" Y="-0.030479834423" Z="2.45" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.45" />
                  <Point X="-5.008494560651" Y="-0.432754549984" Z="2.45" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.45" />
                  <Point X="-5.057613965754" Y="-0.465612645031" Z="2.45" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.45" />
                  <Point X="-4.977919268251" Y="-1.008237350879" Z="2.45" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.45" />
                  <Point X="-3.276020976027" Y="-1.314349342442" Z="2.45" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.45" />
                  <Point X="-3.21823377033" Y="-1.307407791875" Z="2.45" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.45" />
                  <Point X="-3.192945447377" Y="-1.323488969798" Z="2.45" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.45" />
                  <Point X="-4.457399602884" Y="-2.316741579739" Z="2.45" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.45" />
                  <Point X="-4.492646151143" Y="-2.36885086783" Z="2.45" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.45" />
                  <Point X="-4.197229054901" Y="-2.8598179251" Z="2.45" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.45" />
                  <Point X="-2.617882159948" Y="-2.581496362951" Z="2.45" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.45" />
                  <Point X="-2.572233461416" Y="-2.556097007715" Z="2.45" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.45" />
                  <Point X="-3.273920650217" Y="-3.817196222544" Z="2.45" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.45" />
                  <Point X="-3.285622684072" Y="-3.873251993604" Z="2.45" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.45" />
                  <Point X="-2.875127402235" Y="-4.187005323472" Z="2.45" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.45" />
                  <Point X="-2.234078563076" Y="-4.166690687133" Z="2.45" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.45" />
                  <Point X="-2.217210726085" Y="-4.150430844488" Z="2.45" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.45" />
                  <Point X="-1.957441101558" Y="-3.984793214201" Z="2.45" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.45" />
                  <Point X="-1.650518334014" Y="-4.011522000306" Z="2.45" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.45" />
                  <Point X="-1.423291570547" Y="-4.219570345972" Z="2.45" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.45" />
                  <Point X="-1.411414560544" Y="-4.866708283966" Z="2.45" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.45" />
                  <Point X="-1.402769443136" Y="-4.882160968309" Z="2.45" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.45" />
                  <Point X="-1.106931222036" Y="-4.957615955897" Z="2.45" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.45" />
                  <Point X="-0.431080309218" Y="-3.570997644404" Z="2.45" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.45" />
                  <Point X="-0.411367271071" Y="-3.510532356873" Z="2.45" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.45" />
                  <Point X="-0.244508472357" Y="-3.280125775053" Z="2.45" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.45" />
                  <Point X="0.008850607003" Y="-3.206986270235" Z="2.45" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.45" />
                  <Point X="0.259078588361" Y="-3.291113398895" Z="2.45" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.45" />
                  <Point X="0.803674551183" Y="-4.961538385412" Z="2.45" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.45" />
                  <Point X="0.823967980316" Y="-5.012618488993" Z="2.45" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.45" />
                  <Point X="1.00426467133" Y="-4.9796303915" Z="2.45" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.45" />
                  <Point X="0.9650208274" Y="-3.331211432098" Z="2.45" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.45" />
                  <Point X="0.959225679323" Y="-3.264264699969" Z="2.45" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.45" />
                  <Point X="1.017447398025" Y="-3.020098261636" Z="2.45" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.45" />
                  <Point X="1.199286182159" Y="-2.874926165926" Z="2.45" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.45" />
                  <Point X="1.431675509524" Y="-2.859013070496" Z="2.45" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.45" />
                  <Point X="2.626251020474" Y="-4.280000829135" Z="2.45" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.45" />
                  <Point X="2.668866543488" Y="-4.32223624741" Z="2.45" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.45" />
                  <Point X="2.863884276177" Y="-4.195562136892" Z="2.45" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.45" />
                  <Point X="2.298319646506" Y="-2.769207301493" Z="2.45" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.45" />
                  <Point X="2.269873606618" Y="-2.714749922581" Z="2.45" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.45" />
                  <Point X="2.235511556145" Y="-2.499937069511" Z="2.45" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.45" />
                  <Point X="2.332961308861" Y="-2.323389753822" Z="2.45" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.45" />
                  <Point X="2.513756880931" Y="-2.233574402873" Z="2.45" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.45" />
                  <Point X="4.018206295614" Y="-3.019429853769" Z="2.45" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.45" />
                  <Point X="4.071214540824" Y="-3.037845960853" Z="2.45" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.45" />
                  <Point X="4.245293595585" Y="-2.789404440447" Z="2.45" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.45" />
                  <Point X="3.234888359611" Y="-1.646932439825" Z="2.45" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.45" />
                  <Point X="3.189232739938" Y="-1.609133323086" Z="2.45" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.45" />
                  <Point X="3.0928119668" Y="-1.452331381231" Z="2.45" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.45" />
                  <Point X="3.111825152839" Y="-1.282761485542" Z="2.45" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.45" />
                  <Point X="3.224078125959" Y="-1.15400559323" Z="2.45" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.45" />
                  <Point X="4.854337931107" Y="-1.307479806147" Z="2.45" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.45" />
                  <Point X="4.909956196099" Y="-1.301488869315" Z="2.45" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.45" />
                  <Point X="4.993414786882" Y="-0.931313750835" Z="2.45" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.45" />
                  <Point X="3.793367853406" Y="-0.232979590089" Z="2.45" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.45" />
                  <Point X="3.744721043141" Y="-0.218942681483" Z="2.45" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.45" />
                  <Point X="3.653503720779" Y="-0.164867473465" Z="2.45" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.45" />
                  <Point X="3.599290392405" Y="-0.093235630079" Z="2.45" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.45" />
                  <Point X="3.582081058019" Y="0.003374901096" Z="2.45" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.45" />
                  <Point X="3.601875717621" Y="0.09908126511" Z="2.45" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.45" />
                  <Point X="3.658674371212" Y="0.169206162759" Z="2.45" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.45" />
                  <Point X="5.002599963901" Y="0.55699234268" Z="2.45" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.45" />
                  <Point X="5.04571295122" Y="0.583947735033" Z="2.45" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.45" />
                  <Point X="4.978573208071" Y="1.006980507907" Z="2.45" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.45" />
                  <Point X="3.512644752408" Y="1.22854395631" Z="2.45" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.45" />
                  <Point X="3.459832089817" Y="1.222458807423" Z="2.45" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.45" />
                  <Point X="3.366527808721" Y="1.235838040748" Z="2.45" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.45" />
                  <Point X="3.297639754699" Y="1.27622272913" Z="2.45" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.45" />
                  <Point X="3.250643765002" Y="1.349707819428" Z="2.45" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.45" />
                  <Point X="3.234343930832" Y="1.43503781015" Z="2.45" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.45" />
                  <Point X="3.257134493783" Y="1.511947005217" Z="2.45" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.45" />
                  <Point X="4.407683339794" Y="2.424753287114" Z="2.45" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.45" />
                  <Point X="4.440006401379" Y="2.467233710454" Z="2.45" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.45" />
                  <Point X="4.229942470277" Y="2.812201216807" Z="2.45" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.45" />
                  <Point X="2.562011428072" Y="2.297098060968" Z="2.45" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.45" />
                  <Point X="2.507073275549" Y="2.266248769348" Z="2.45" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.45" />
                  <Point X="2.427166432916" Y="2.245821611756" Z="2.45" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.45" />
                  <Point X="2.357955221162" Y="2.255401166272" Z="2.45" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.45" />
                  <Point X="2.295357433963" Y="2.29906963922" Z="2.45" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.45" />
                  <Point X="2.253608091003" Y="2.362592010916" Z="2.45" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.45" />
                  <Point X="2.246279173309" Y="2.432396300333" Z="2.45" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.45" />
                  <Point X="3.098527291892" Y="3.950127653883" Z="2.45" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.45" />
                  <Point X="3.115522191372" Y="4.011580282588" Z="2.45" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.45" />
                  <Point X="2.739603735642" Y="4.277126738268" Z="2.45" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.45" />
                  <Point X="2.34137968105" Y="4.50747986903" Z="2.45" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.45" />
                  <Point X="1.929104565101" Y="4.698720460821" Z="2.45" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.45" />
                  <Point X="1.493885111295" Y="4.853806303406" Z="2.45" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.45" />
                  <Point X="0.839177475654" Y="5.008676604885" Z="2.45" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.45" />
                  <Point X="0.006750623164" Y="4.380318143488" Z="2.45" />
                  <Point X="0" Y="4.355124473572" Z="2.45" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>