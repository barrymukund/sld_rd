<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#135" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="873" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999526367188" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442382812" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.634474609375" Y="-3.778132568359" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140136719" />
                  <Point X="25.54236328125" Y="-3.467375732422" />
                  <Point X="25.5132734375" Y="-3.425464355469" />
                  <Point X="25.378634765625" Y="-3.231475341797" />
                  <Point X="25.356751953125" Y="-3.209020751953" />
                  <Point X="25.33049609375" Y="-3.189776855469" />
                  <Point X="25.30249609375" Y="-3.175669189453" />
                  <Point X="25.257482421875" Y="-3.161698730469" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097036132812" />
                  <Point X="24.9181640625" Y="-3.111006347656" />
                  <Point X="24.70981640625" Y="-3.175669433594" />
                  <Point X="24.68181640625" Y="-3.189776855469" />
                  <Point X="24.655560546875" Y="-3.209020751953" />
                  <Point X="24.63367578125" Y="-3.231477539062" />
                  <Point X="24.604587890625" Y="-3.273388671875" />
                  <Point X="24.46994921875" Y="-3.467377929688" />
                  <Point X="24.4618125" Y="-3.481572021484" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.11959765625" Y="-4.74190625" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.9206640625" Y="-4.845351074219" />
                  <Point X="23.8723515625" Y="-4.832920410156" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.776470703125" Y="-4.628512695312" />
                  <Point X="23.7850390625" Y="-4.563438964844" />
                  <Point X="23.78580078125" Y="-4.547930175781" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.77273828125" Y="-4.462161132812" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.182964355469" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131205078125" />
                  <Point X="23.6349140625" Y="-4.094860839844" />
                  <Point X="23.443095703125" Y="-3.926640136719" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.30196875" Y="-3.887361083984" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708740234" />
                  <Point X="22.985533203125" Y="-3.882028564453" />
                  <Point X="22.957341796875" Y="-3.894801513672" />
                  <Point X="22.911509765625" Y="-3.925425292969" />
                  <Point X="22.699376953125" Y="-4.067169433594" />
                  <Point X="22.68721484375" Y="-4.076821533203" />
                  <Point X="22.6648984375" Y="-4.099461425781" />
                  <Point X="22.5198515625" Y="-4.288489746094" />
                  <Point X="22.19829296875" Y="-4.089388427734" />
                  <Point X="22.131396484375" Y="-4.037881103516" />
                  <Point X="21.89527734375" Y="-3.856077148438" />
                  <Point X="22.443978515625" Y="-2.905701171875" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.588720703125" Y="-2.646968994141" />
                  <Point X="22.594134765625" Y="-2.624443603516" />
                  <Point X="22.593935546875" Y="-2.601277587891" />
                  <Point X="22.58979296875" Y="-2.569796386719" />
                  <Point X="22.583375" Y="-2.545839355469" />
                  <Point X="22.570974609375" Y="-2.524360107422" />
                  <Point X="22.55830859375" Y="-2.507852539062" />
                  <Point X="22.5501171875" Y="-2.498509521484" />
                  <Point X="22.5358515625" Y="-2.484243164062" />
                  <Point X="22.5106953125" Y="-2.466215087891" />
                  <Point X="22.4818671875" Y="-2.451997314453" />
                  <Point X="22.452248046875" Y="-2.44301171875" />
                  <Point X="22.4213125" Y="-2.444023925781" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.3005" Y="-3.073372070313" />
                  <Point X="21.1819765625" Y="-3.141801513672" />
                  <Point X="20.917140625" Y="-2.793862548828" />
                  <Point X="20.86918359375" Y="-2.713446533203" />
                  <Point X="20.693857421875" Y="-2.419450439453" />
                  <Point X="21.6626328125" Y="-1.676082519531" />
                  <Point X="21.894044921875" Y="-1.498513305664" />
                  <Point X="21.916935546875" Y="-1.475880615234" />
                  <Point X="21.9357578125" Y="-1.446142700195" />
                  <Point X="21.94383984375" Y="-1.425789916992" />
                  <Point X="21.94751171875" Y="-1.414548706055" />
                  <Point X="21.95384765625" Y="-1.390086425781" />
                  <Point X="21.957962890625" Y="-1.358610107422" />
                  <Point X="21.957265625" Y="-1.335737060547" />
                  <Point X="21.9511171875" Y="-1.313694458008" />
                  <Point X="21.939111328125" Y="-1.284711791992" />
                  <Point X="21.92670703125" Y="-1.263228881836" />
                  <Point X="21.909162109375" Y="-1.245690063477" />
                  <Point X="21.891955078125" Y="-1.232491455078" />
                  <Point X="21.88232421875" Y="-1.225998291016" />
                  <Point X="21.860546875" Y="-1.213181030273" />
                  <Point X="21.831283203125" Y="-1.201956787109" />
                  <Point X="21.799396484375" Y="-1.195474975586" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="20.429521484375" Y="-1.370607421875" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.165923828125" Y="-0.992651916504" />
                  <Point X="20.153236328125" Y="-0.903944763184" />
                  <Point X="20.107578125" Y="-0.584698425293" />
                  <Point X="21.20351171875" Y="-0.291043426514" />
                  <Point X="21.467125" Y="-0.220408355713" />
                  <Point X="21.48630859375" Y="-0.212960464478" />
                  <Point X="21.50680859375" Y="-0.202317977905" />
                  <Point X="21.517205078125" Y="-0.196047363281" />
                  <Point X="21.54002734375" Y="-0.180207427979" />
                  <Point X="21.56398828125" Y="-0.158672317505" />
                  <Point X="21.5842265625" Y="-0.129816955566" />
                  <Point X="21.59329296875" Y="-0.109787147522" />
                  <Point X="21.5974765625" Y="-0.098771713257" />
                  <Point X="21.605083984375" Y="-0.074260444641" />
                  <Point X="21.61052734375" Y="-0.045523181915" />
                  <Point X="21.609873046875" Y="-0.013257699966" />
                  <Point X="21.605984375" Y="0.006863182068" />
                  <Point X="21.60344140625" Y="0.016995950699" />
                  <Point X="21.595833984375" Y="0.041507221222" />
                  <Point X="21.582517578125" Y="0.070832824707" />
                  <Point X="21.560912109375" Y="0.098981903076" />
                  <Point X="21.544162109375" Y="0.113921875" />
                  <Point X="21.535095703125" Y="0.121069534302" />
                  <Point X="21.5122734375" Y="0.136909469604" />
                  <Point X="21.498076171875" Y="0.145047409058" />
                  <Point X="21.467125" Y="0.157848358154" />
                  <Point X="20.24697265625" Y="0.484786987305" />
                  <Point X="20.10818359375" Y="0.521975524902" />
                  <Point X="20.17551171875" Y="0.976967041016" />
                  <Point X="20.2010546875" Y="1.071229980469" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="21.04537109375" Y="1.324670532227" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255015625" Y="1.299341674805" />
                  <Point X="21.276576171875" Y="1.301228149414" />
                  <Point X="21.2968671875" Y="1.305264282227" />
                  <Point X="21.307779296875" Y="1.308705200195" />
                  <Point X="21.35829296875" Y="1.324631835938" />
                  <Point X="21.377224609375" Y="1.332962402344" />
                  <Point X="21.39596875" Y="1.343784545898" />
                  <Point X="21.412650390625" Y="1.356016479492" />
                  <Point X="21.426287109375" Y="1.371568969727" />
                  <Point X="21.438701171875" Y="1.389299316406" />
                  <Point X="21.448650390625" Y="1.407432495117" />
                  <Point X="21.453029296875" Y="1.418004394531" />
                  <Point X="21.473296875" Y="1.466936767578" />
                  <Point X="21.479083984375" Y="1.486788574219" />
                  <Point X="21.48284375" Y="1.508104003906" />
                  <Point X="21.484197265625" Y="1.528750610352" />
                  <Point X="21.481048828125" Y="1.549200805664" />
                  <Point X="21.4754453125" Y="1.570106933594" />
                  <Point X="21.467951171875" Y="1.589374755859" />
                  <Point X="21.46266796875" Y="1.599524902344" />
                  <Point X="21.4382109375" Y="1.646504638672" />
                  <Point X="21.426716796875" Y="1.663707763672" />
                  <Point X="21.4128046875" Y="1.680287231445" />
                  <Point X="21.39786328125" Y="1.694590209961" />
                  <Point X="20.697982421875" Y="2.231628662109" />
                  <Point X="20.648140625" Y="2.269874267578" />
                  <Point X="20.9188515625" Y="2.733664306641" />
                  <Point X="20.986501953125" Y="2.820620361328" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.677287109375" Y="2.911676513672" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.86840625" Y="2.824466552734" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.000986328125" Y="2.826504638672" />
                  <Point X="22.019537109375" Y="2.835653564453" />
                  <Point X="22.037791015625" Y="2.847282714844" />
                  <Point X="22.053921875" Y="2.860227050781" />
                  <Point X="22.0647109375" Y="2.871015380859" />
                  <Point X="22.114646484375" Y="2.920950439453" />
                  <Point X="22.127595703125" Y="2.937085205078" />
                  <Point X="22.139224609375" Y="2.955339355469" />
                  <Point X="22.14837109375" Y="2.973888427734" />
                  <Point X="22.1532890625" Y="2.993977539062" />
                  <Point X="22.156115234375" Y="3.015436279297" />
                  <Point X="22.15656640625" Y="3.036122558594" />
                  <Point X="22.155236328125" Y="3.051321777344" />
                  <Point X="22.14908203125" Y="3.121672119141" />
                  <Point X="22.145044921875" Y="3.141962158203" />
                  <Point X="22.13853515625" Y="3.162604736328" />
                  <Point X="22.130205078125" Y="3.181532958984" />
                  <Point X="21.82006640625" Y="3.718709472656" />
                  <Point X="21.8209140625" Y="3.727852783203" />
                  <Point X="22.299373046875" Y="4.094681396484" />
                  <Point X="22.405927734375" Y="4.153881835938" />
                  <Point X="22.832962890625" Y="4.391133300781" />
                  <Point X="22.921275390625" Y="4.276041992188" />
                  <Point X="22.9568046875" Y="4.229740234375" />
                  <Point X="22.971109375" Y="4.214797851563" />
                  <Point X="22.987689453125" Y="4.200885742188" />
                  <Point X="23.00488671875" Y="4.189395019531" />
                  <Point X="23.021802734375" Y="4.180588867188" />
                  <Point X="23.100103515625" Y="4.139828125" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.22255078125" Y="4.134483398438" />
                  <Point X="23.240169921875" Y="4.141782226563" />
                  <Point X="23.321724609375" Y="4.175562988281" />
                  <Point X="23.339853515625" Y="4.185508789062" />
                  <Point X="23.357583984375" Y="4.197922851562" />
                  <Point X="23.373140625" Y="4.211563964844" />
                  <Point X="23.385373046875" Y="4.228249511719" />
                  <Point X="23.396193359375" Y="4.246993652344" />
                  <Point X="23.404521484375" Y="4.265923828125" />
                  <Point X="23.410255859375" Y="4.284112792969" />
                  <Point X="23.43680078125" Y="4.368300292969" />
                  <Point X="23.4408359375" Y="4.388583496094" />
                  <Point X="23.44272265625" Y="4.410145019531" />
                  <Point X="23.442271484375" Y="4.430826171875" />
                  <Point X="23.415798828125" Y="4.6318984375" />
                  <Point X="23.43096875" Y="4.636151367188" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.17953125" Y="4.824925292969" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.831763671875" Y="4.414447753906" />
                  <Point X="24.866095703125" Y="4.286315917969" />
                  <Point X="24.87887109375" Y="4.258122558594" />
                  <Point X="24.89673046875" Y="4.231395507813" />
                  <Point X="24.91788671875" Y="4.208808105469" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183885742188" />
                  <Point X="25.067130859375" Y="4.194217773438" />
                  <Point X="25.094423828125" Y="4.208806640625" />
                  <Point X="25.11558203125" Y="4.23139453125" />
                  <Point X="25.13344140625" Y="4.25812109375" />
                  <Point X="25.146216796875" Y="4.286314941406" />
                  <Point X="25.30512890625" Y="4.879381835938" />
                  <Point X="25.307421875" Y="4.887937011719" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="25.950912109375" Y="4.8059375" />
                  <Point X="26.48103125" Y="4.677949707031" />
                  <Point X="26.54937109375" Y="4.653163085938" />
                  <Point X="26.894642578125" Y="4.527930175781" />
                  <Point X="26.961912109375" Y="4.496470214844" />
                  <Point X="27.294568359375" Y="4.340897949219" />
                  <Point X="27.359591796875" Y="4.303016113281" />
                  <Point X="27.680990234375" Y="4.115767578125" />
                  <Point X="27.742271484375" Y="4.072188720703" />
                  <Point X="27.943263671875" Y="3.929254638672" />
                  <Point X="27.3006640625" Y="2.816242431641" />
                  <Point X="27.147583984375" Y="2.55109765625" />
                  <Point X="27.139765625" Y="2.533744628906" />
                  <Point X="27.13094921875" Y="2.507398193359" />
                  <Point X="27.129263671875" Y="2.501794433594" />
                  <Point X="27.111607421875" Y="2.435772216797" />
                  <Point X="27.108384765625" Y="2.410771728516" />
                  <Point X="27.10853515625" Y="2.379530273438" />
                  <Point X="27.109216796875" Y="2.368615722656" />
                  <Point X="27.116099609375" Y="2.311530273438" />
                  <Point X="27.12144140625" Y="2.289609619141" />
                  <Point X="27.12970703125" Y="2.26751953125" />
                  <Point X="27.140072265625" Y="2.247467529297" />
                  <Point X="27.147705078125" Y="2.236219726562" />
                  <Point X="27.18303125" Y="2.184158935547" />
                  <Point X="27.200001953125" Y="2.165213867188" />
                  <Point X="27.22455078125" Y="2.144282226562" />
                  <Point X="27.23284765625" Y="2.137960693359" />
                  <Point X="27.284908203125" Y="2.102635253906" />
                  <Point X="27.304953125" Y="2.092272705078" />
                  <Point X="27.327041015625" Y="2.084006591797" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.361298828125" Y="2.077176269531" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.44434375" Y="2.070734619141" />
                  <Point X="27.477509765625" Y="2.07588671875" />
                  <Point X="27.48746875" Y="2.077985351562" />
                  <Point X="27.5534921875" Y="2.095640625" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.8157734375" Y="2.81869140625" />
                  <Point X="28.967326171875" Y="2.906191162109" />
                  <Point X="29.12328125" Y="2.689451171875" />
                  <Point X="29.15744140625" Y="2.632999267578" />
                  <Point X="29.26219921875" Y="2.459884033203" />
                  <Point X="28.43233203125" Y="1.823103271484" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.216650390625" Y="1.655098144531" />
                  <Point X="28.197138671875" Y="1.632455566406" />
                  <Point X="28.19370703125" Y="1.62823449707" />
                  <Point X="28.14619140625" Y="1.566245849609" />
                  <Point X="28.133427734375" Y="1.543840698242" />
                  <Point X="28.121134765625" Y="1.513215942383" />
                  <Point X="28.11780859375" Y="1.503414794922" />
                  <Point X="28.100107421875" Y="1.440124267578" />
                  <Point X="28.09665234375" Y="1.417824951172" />
                  <Point X="28.0958359375" Y="1.394254150391" />
                  <Point X="28.097740234375" Y="1.371759765625" />
                  <Point X="28.100880859375" Y="1.356545776367" />
                  <Point X="28.11541015625" Y="1.286126953125" />
                  <Point X="28.124109375" Y="1.26160949707" />
                  <Point X="28.139845703125" Y="1.231254882812" />
                  <Point X="28.1448203125" Y="1.222765380859" />
                  <Point X="28.184337890625" Y="1.162697509766" />
                  <Point X="28.198890625" Y="1.145452514648" />
                  <Point X="28.216134765625" Y="1.129361816406" />
                  <Point X="28.234345703125" Y="1.116034790039" />
                  <Point X="28.24671875" Y="1.109069824219" />
                  <Point X="28.30398828125" Y="1.076832519531" />
                  <Point X="28.32872265625" Y="1.067168579102" />
                  <Point X="28.363427734375" Y="1.058959594727" />
                  <Point X="28.37284765625" Y="1.057227539063" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.653998046875" Y="1.200464355469" />
                  <Point X="29.77683984375" Y="1.21663659668" />
                  <Point X="29.84594140625" Y="0.932789001465" />
                  <Point X="29.856705078125" Y="0.86365222168" />
                  <Point X="29.890865234375" Y="0.644238830566" />
                  <Point X="28.947556640625" Y="0.391479614258" />
                  <Point X="28.716580078125" Y="0.329589782715" />
                  <Point X="28.69808984375" Y="0.322498443604" />
                  <Point X="28.66957421875" Y="0.307990631104" />
                  <Point X="28.665111328125" Y="0.305567749023" />
                  <Point X="28.589037109375" Y="0.261595733643" />
                  <Point X="28.568302734375" Y="0.245403030396" />
                  <Point X="28.54412890625" Y="0.22041696167" />
                  <Point X="28.537669921875" Y="0.213010955811" />
                  <Point X="28.492025390625" Y="0.154849121094" />
                  <Point X="28.48030078125" Y="0.135567489624" />
                  <Point X="28.47052734375" Y="0.114103569031" />
                  <Point X="28.463681640625" Y="0.092603668213" />
                  <Point X="28.46039453125" Y="0.075439361572" />
                  <Point X="28.4451796875" Y="-0.004006833076" />
                  <Point X="28.44387890625" Y="-0.030520065308" />
                  <Point X="28.447166015625" Y="-0.066493637085" />
                  <Point X="28.448466796875" Y="-0.075717788696" />
                  <Point X="28.463681640625" Y="-0.155163986206" />
                  <Point X="28.47052734375" Y="-0.176663574219" />
                  <Point X="28.48030078125" Y="-0.198127502441" />
                  <Point X="28.492025390625" Y="-0.21740852356" />
                  <Point X="28.50188671875" Y="-0.229974334717" />
                  <Point X="28.54753125" Y="-0.288136169434" />
                  <Point X="28.567119140625" Y="-0.306841918945" />
                  <Point X="28.5978671875" Y="-0.328762420654" />
                  <Point X="28.60547265625" Y="-0.333655853271" />
                  <Point X="28.681546875" Y="-0.37762802124" />
                  <Point X="28.692708984375" Y="-0.383138305664" />
                  <Point X="28.716580078125" Y="-0.392149810791" />
                  <Point X="29.785666015625" Y="-0.678610778809" />
                  <Point X="29.891474609375" Y="-0.706961914062" />
                  <Point X="29.8550234375" Y="-0.948732421875" />
                  <Point X="29.84123046875" Y="-1.00917767334" />
                  <Point X="29.801177734375" Y="-1.18469921875" />
                  <Point X="28.69225" Y="-1.038706298828" />
                  <Point X="28.4243828125" Y="-1.003440917969" />
                  <Point X="28.40803515625" Y="-1.002710205078" />
                  <Point X="28.37466015625" Y="-1.005508789063" />
                  <Point X="28.34240234375" Y="-1.012520141602" />
                  <Point X="28.193095703125" Y="-1.044972412109" />
                  <Point X="28.163978515625" Y="-1.056595825195" />
                  <Point X="28.136150390625" Y="-1.07348815918" />
                  <Point X="28.1123984375" Y="-1.093959472656" />
                  <Point X="28.092900390625" Y="-1.117409179688" />
                  <Point X="28.002654296875" Y="-1.225947631836" />
                  <Point X="27.987931640625" Y="-1.250334838867" />
                  <Point X="27.97658984375" Y="-1.277720092773" />
                  <Point X="27.969759765625" Y="-1.305364379883" />
                  <Point X="27.96696484375" Y="-1.335732910156" />
                  <Point X="27.95403125" Y="-1.476294799805" />
                  <Point X="27.95634765625" Y="-1.507561889648" />
                  <Point X="27.964078125" Y="-1.539182617188" />
                  <Point X="27.976451171875" Y="-1.56799597168" />
                  <Point X="27.994302734375" Y="-1.595763427734" />
                  <Point X="28.076931640625" Y="-1.724286621094" />
                  <Point X="28.0869375" Y="-1.73724206543" />
                  <Point X="28.110630859375" Y="-1.760909301758" />
                  <Point X="29.102748046875" Y="-2.522188720703" />
                  <Point X="29.213123046875" Y="-2.606882568359" />
                  <Point X="29.12481640625" Y="-2.749775634766" />
                  <Point X="29.096279296875" Y="-2.790323730469" />
                  <Point X="29.028982421875" Y="-2.885944824219" />
                  <Point X="28.03952734375" Y="-2.314682861328" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.7542265625" Y="-2.159825195312" />
                  <Point X="27.7158359375" Y="-2.152891845703" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135176513672" />
                  <Point X="27.412939453125" Y="-2.151962158203" />
                  <Point X="27.26531640625" Y="-2.229655517578" />
                  <Point X="27.242384765625" Y="-2.246549804688" />
                  <Point X="27.22142578125" Y="-2.267509521484" />
                  <Point X="27.20453515625" Y="-2.290436035156" />
                  <Point X="27.187748046875" Y="-2.322330078125" />
                  <Point X="27.1100546875" Y="-2.469954101562" />
                  <Point X="27.100228515625" Y="-2.499735351563" />
                  <Point X="27.095271484375" Y="-2.531908691406" />
                  <Point X="27.09567578125" Y="-2.563259033203" />
                  <Point X="27.102609375" Y="-2.601650878906" />
                  <Point X="27.134703125" Y="-2.779349365234" />
                  <Point X="27.13898828125" Y="-2.795140869141" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.789357421875" Y="-3.930322998047" />
                  <Point X="27.86128515625" Y="-4.054906005859" />
                  <Point X="27.781859375" Y="-4.111637695312" />
                  <Point X="27.749953125" Y="-4.132291503906" />
                  <Point X="27.701767578125" Y="-4.163481445312" />
                  <Point X="26.940599609375" Y="-3.171510253906" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900556884766" />
                  <Point X="26.684060546875" Y="-2.876213378906" />
                  <Point X="26.50880078125" Y="-2.763538085938" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.375689453125" Y="-2.744927490234" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="26.07262109375" Y="-2.822048828125" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968861083984" />
                  <Point X="25.88725" Y="-2.996687744141" />
                  <Point X="25.875625" Y="-3.025809326172" />
                  <Point X="25.866064453125" Y="-3.069797119141" />
                  <Point X="25.821810546875" Y="-3.273397216797" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="26.000416015625" Y="-4.695466308594" />
                  <Point X="26.02206640625" Y="-4.859915527344" />
                  <Point X="25.9756796875" Y="-4.870083007812" />
                  <Point X="25.946208984375" Y="-4.875437011719" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.941572265625" Y="-4.75263671875" />
                  <Point X="23.8960234375" Y="-4.740916992188" />
                  <Point X="23.858755859375" Y="-4.731328125" />
                  <Point X="23.870658203125" Y="-4.640912109375" />
                  <Point X="23.8792265625" Y="-4.575838378906" />
                  <Point X="23.879923828125" Y="-4.568099121094" />
                  <Point X="23.88055078125" Y="-4.54103125" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497688964844" />
                  <Point X="23.865912109375" Y="-4.443626953125" />
                  <Point X="23.81613671875" Y="-4.193397460938" />
                  <Point X="23.811875" Y="-4.178468261719" />
                  <Point X="23.80097265625" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135462890625" />
                  <Point X="23.778259765625" Y="-4.107625976562" />
                  <Point X="23.769423828125" Y="-4.094859619141" />
                  <Point X="23.749791015625" Y="-4.070937255859" />
                  <Point X="23.738994140625" Y="-4.05978125" />
                  <Point X="23.697552734375" Y="-4.023437011719" />
                  <Point X="23.505734375" Y="-3.855216308594" />
                  <Point X="23.493263671875" Y="-3.845967041016" />
                  <Point X="23.46698046875" Y="-3.829622314453" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.308181640625" Y="-3.792564453125" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166259766" />
                  <Point X="22.991994140625" Y="-3.781946044922" />
                  <Point X="22.9609453125" Y="-3.790265869141" />
                  <Point X="22.946326171875" Y="-3.79549609375" />
                  <Point X="22.918134765625" Y="-3.808269042969" />
                  <Point X="22.9045625" Y="-3.815811767578" />
                  <Point X="22.85873046875" Y="-3.846435546875" />
                  <Point X="22.64659765625" Y="-3.9881796875" />
                  <Point X="22.6403203125" Y="-3.992755859375" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032771484375" />
                  <Point X="22.589529296875" Y="-4.04162890625" />
                  <Point X="22.496796875" Y="-4.162479003906" />
                  <Point X="22.252416015625" Y="-4.0111640625" />
                  <Point X="22.189353515625" Y="-3.962608154297" />
                  <Point X="22.01913671875" Y="-3.831546142578" />
                  <Point X="22.52625" Y="-2.953201171875" />
                  <Point X="22.658509765625" Y="-2.724119628906" />
                  <Point X="22.663794921875" Y="-2.713479980469" />
                  <Point X="22.67627734375" Y="-2.683829345703" />
                  <Point X="22.68108984375" Y="-2.669170410156" />
                  <Point X="22.68650390625" Y="-2.646645019531" />
                  <Point X="22.689130859375" Y="-2.623626708984" />
                  <Point X="22.688931640625" Y="-2.600460693359" />
                  <Point X="22.688123046875" Y="-2.588883544922" />
                  <Point X="22.68398046875" Y="-2.55740234375" />
                  <Point X="22.681556640625" Y="-2.545213134766" />
                  <Point X="22.675138671875" Y="-2.521256103516" />
                  <Point X="22.6656484375" Y="-2.498341308594" />
                  <Point X="22.653248046875" Y="-2.476862060547" />
                  <Point X="22.64634375" Y="-2.466529785156" />
                  <Point X="22.633677734375" Y="-2.450022216797" />
                  <Point X="22.617294921875" Y="-2.431336181641" />
                  <Point X="22.603029296875" Y="-2.417069824219" />
                  <Point X="22.591189453125" Y="-2.407024658203" />
                  <Point X="22.566033203125" Y="-2.388996582031" />
                  <Point X="22.552716796875" Y="-2.381013671875" />
                  <Point X="22.523888671875" Y="-2.366795898438" />
                  <Point X="22.5094453125" Y="-2.361088623047" />
                  <Point X="22.479826171875" Y="-2.352103027344" />
                  <Point X="22.449140625" Y="-2.3480625" />
                  <Point X="22.418205078125" Y="-2.349074707031" />
                  <Point X="22.402779296875" Y="-2.350849121094" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.253" Y="-2.991099609375" />
                  <Point X="21.206912109375" Y="-3.017708496094" />
                  <Point X="20.995978515625" Y="-2.740587158203" />
                  <Point X="20.950775390625" Y="-2.664787841797" />
                  <Point X="20.818734375" Y="-2.443374023438" />
                  <Point X="21.72046484375" Y="-1.751451049805" />
                  <Point X="21.951876953125" Y="-1.573881958008" />
                  <Point X="21.960837890625" Y="-1.566068115234" />
                  <Point X="21.983728515625" Y="-1.543435302734" />
                  <Point X="21.99720703125" Y="-1.526688110352" />
                  <Point X="22.016029296875" Y="-1.496950073242" />
                  <Point X="22.02405078125" Y="-1.481203857422" />
                  <Point X="22.0321328125" Y="-1.460850952148" />
                  <Point X="22.0394765625" Y="-1.438368408203" />
                  <Point X="22.0458125" Y="-1.41390612793" />
                  <Point X="22.048046875" Y="-1.402401977539" />
                  <Point X="22.052162109375" Y="-1.37092565918" />
                  <Point X="22.05291796875" Y="-1.355715454102" />
                  <Point X="22.052220703125" Y="-1.332842407227" />
                  <Point X="22.0487734375" Y="-1.310212646484" />
                  <Point X="22.042625" Y="-1.288170043945" />
                  <Point X="22.038884765625" Y="-1.277337402344" />
                  <Point X="22.02687890625" Y="-1.248354736328" />
                  <Point X="22.0213828125" Y="-1.237208496094" />
                  <Point X="22.008978515625" Y="-1.215725585938" />
                  <Point X="21.99387109375" Y="-1.196041992188" />
                  <Point X="21.976326171875" Y="-1.178503173828" />
                  <Point X="21.96698046875" Y="-1.170311157227" />
                  <Point X="21.9497734375" Y="-1.157112670898" />
                  <Point X="21.93051171875" Y="-1.144126098633" />
                  <Point X="21.908734375" Y="-1.131308837891" />
                  <Point X="21.894568359375" Y="-1.124481689453" />
                  <Point X="21.8653046875" Y="-1.113257446289" />
                  <Point X="21.85020703125" Y="-1.108860717773" />
                  <Point X="21.8183203125" Y="-1.10237902832" />
                  <Point X="21.802703125" Y="-1.100532592773" />
                  <Point X="21.77137890625" Y="-1.09944152832" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="20.41712109375" Y="-1.276420166016" />
                  <Point X="20.33908203125" Y="-1.286694213867" />
                  <Point X="20.25923828125" Y="-0.974114501953" />
                  <Point X="20.247279296875" Y="-0.890494018555" />
                  <Point X="20.21355078125" Y="-0.654654541016" />
                  <Point X="21.228099609375" Y="-0.382806335449" />
                  <Point X="21.491712890625" Y="-0.312171234131" />
                  <Point X="21.5015078125" Y="-0.308968109131" />
                  <Point X="21.52069140625" Y="-0.301520172119" />
                  <Point X="21.530080078125" Y="-0.297275482178" />
                  <Point X="21.550580078125" Y="-0.286633056641" />
                  <Point X="21.571373046875" Y="-0.274091674805" />
                  <Point X="21.5941953125" Y="-0.258251739502" />
                  <Point X="21.60353125" Y="-0.250863983154" />
                  <Point X="21.6274921875" Y="-0.229328826904" />
                  <Point X="21.641765625" Y="-0.213222717285" />
                  <Point X="21.66200390625" Y="-0.184367294312" />
                  <Point X="21.6707734375" Y="-0.168991958618" />
                  <Point X="21.67983984375" Y="-0.148962051392" />
                  <Point X="21.68820703125" Y="-0.126931243896" />
                  <Point X="21.695814453125" Y="-0.102419914246" />
                  <Point X="21.698423828125" Y="-0.091940811157" />
                  <Point X="21.7038671875" Y="-0.063203536987" />
                  <Point X="21.7055078125" Y="-0.043597057343" />
                  <Point X="21.704853515625" Y="-0.011331627846" />
                  <Point X="21.703146484375" Y="0.004768985271" />
                  <Point X="21.6992578125" Y="0.024889846802" />
                  <Point X="21.694171875" Y="0.045155467987" />
                  <Point X="21.686564453125" Y="0.069666793823" />
                  <Point X="21.682333984375" Y="0.080785720825" />
                  <Point X="21.669017578125" Y="0.110111244202" />
                  <Point X="21.65787890625" Y="0.128675125122" />
                  <Point X="21.6362734375" Y="0.156824157715" />
                  <Point X="21.624146484375" Y="0.169878295898" />
                  <Point X="21.607396484375" Y="0.184818313599" />
                  <Point X="21.589263671875" Y="0.19911390686" />
                  <Point X="21.56644140625" Y="0.214953826904" />
                  <Point X="21.559517578125" Y="0.219329437256" />
                  <Point X="21.534384765625" Y="0.232835388184" />
                  <Point X="21.50343359375" Y="0.245636428833" />
                  <Point X="21.491712890625" Y="0.249611343384" />
                  <Point X="20.271560546875" Y="0.576549865723" />
                  <Point X="20.2145546875" Y="0.591824768066" />
                  <Point X="20.26866796875" Y="0.957520812988" />
                  <Point X="20.292748046875" Y="1.046383300781" />
                  <Point X="20.3664140625" Y="1.318237060547" />
                  <Point X="21.032970703125" Y="1.230483154297" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815551758" />
                  <Point X="21.252943359375" Y="1.204364257812" />
                  <Point X="21.263296875" Y="1.20470324707" />
                  <Point X="21.284857421875" Y="1.20658972168" />
                  <Point X="21.295109375" Y="1.208053588867" />
                  <Point X="21.315400390625" Y="1.21208984375" />
                  <Point X="21.336349609375" Y="1.218102905273" />
                  <Point X="21.38686328125" Y="1.234029541016" />
                  <Point X="21.3965546875" Y="1.237678100586" />
                  <Point X="21.415486328125" Y="1.246008544922" />
                  <Point X="21.424724609375" Y="1.250690429688" />
                  <Point X="21.44346875" Y="1.261512573242" />
                  <Point X="21.45214453125" Y="1.267173339844" />
                  <Point X="21.468826171875" Y="1.279405273438" />
                  <Point X="21.484080078125" Y="1.293385009766" />
                  <Point X="21.497716796875" Y="1.3089375" />
                  <Point X="21.504107421875" Y="1.317081787109" />
                  <Point X="21.516521484375" Y="1.334812133789" />
                  <Point X="21.52198828125" Y="1.343601806641" />
                  <Point X="21.5319375" Y="1.361734985352" />
                  <Point X="21.540798828125" Y="1.381650268555" />
                  <Point X="21.56106640625" Y="1.430582641602" />
                  <Point X="21.5645" Y="1.440349609375" />
                  <Point X="21.570287109375" Y="1.460201293945" />
                  <Point X="21.572640625" Y="1.470286499023" />
                  <Point X="21.576400390625" Y="1.491601806641" />
                  <Point X="21.577640625" Y="1.501889526367" />
                  <Point X="21.578994140625" Y="1.522536132812" />
                  <Point X="21.578091796875" Y="1.543206298828" />
                  <Point X="21.574943359375" Y="1.56365625" />
                  <Point X="21.572810546875" Y="1.573795776367" />
                  <Point X="21.56720703125" Y="1.594701782227" />
                  <Point X="21.563984375" Y="1.604543701172" />
                  <Point X="21.556490234375" Y="1.623811523438" />
                  <Point X="21.546935546875" Y="1.643386962891" />
                  <Point X="21.522478515625" Y="1.690366699219" />
                  <Point X="21.517201171875" Y="1.699281860352" />
                  <Point X="21.50570703125" Y="1.716485107422" />
                  <Point X="21.499490234375" Y="1.72477331543" />
                  <Point X="21.485578125" Y="1.741352783203" />
                  <Point X="21.478498046875" Y="1.748912475586" />
                  <Point X="21.463556640625" Y="1.763215454102" />
                  <Point X="21.4556953125" Y="1.769958618164" />
                  <Point X="20.77238671875" Y="2.294281005859" />
                  <Point X="20.997716796875" Y="2.680323486328" />
                  <Point X="21.061482421875" Y="2.762286376953" />
                  <Point X="21.273662109375" Y="3.035013183594" />
                  <Point X="21.629787109375" Y="2.829404052734" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.860126953125" Y="2.729828125" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310791016" />
                  <Point X="22.02356640625" Y="2.734227294922" />
                  <Point X="22.043005859375" Y="2.741302978516" />
                  <Point X="22.061556640625" Y="2.750451904297" />
                  <Point X="22.070580078125" Y="2.755531738281" />
                  <Point X="22.088833984375" Y="2.767160888672" />
                  <Point X="22.097248046875" Y="2.773189208984" />
                  <Point X="22.11337890625" Y="2.786133544922" />
                  <Point X="22.131884765625" Y="2.803837890625" />
                  <Point X="22.1818203125" Y="2.853772949219" />
                  <Point X="22.188736328125" Y="2.861488525391" />
                  <Point X="22.201685546875" Y="2.877623291016" />
                  <Point X="22.20771875" Y="2.886042480469" />
                  <Point X="22.21934765625" Y="2.904296630859" />
                  <Point X="22.2244296875" Y="2.913325195312" />
                  <Point X="22.233576171875" Y="2.931874267578" />
                  <Point X="22.240646484375" Y="2.951298828125" />
                  <Point X="22.245564453125" Y="2.971387939453" />
                  <Point X="22.2474765625" Y="2.981572998047" />
                  <Point X="22.250302734375" Y="3.003031738281" />
                  <Point X="22.251091796875" Y="3.013364746094" />
                  <Point X="22.25154296875" Y="3.034051025391" />
                  <Point X="22.249875" Y="3.059603515625" />
                  <Point X="22.243720703125" Y="3.129953857422" />
                  <Point X="22.242255859375" Y="3.1402109375" />
                  <Point X="22.23821875" Y="3.160500976562" />
                  <Point X="22.235646484375" Y="3.170533935547" />
                  <Point X="22.22913671875" Y="3.191176513672" />
                  <Point X="22.225486328125" Y="3.200871337891" />
                  <Point X="22.21715625" Y="3.219799560547" />
                  <Point X="22.2124765625" Y="3.229032958984" />
                  <Point X="21.94061328125" Y="3.699915771484" />
                  <Point X="22.351626953125" Y="4.01503515625" />
                  <Point X="22.45206640625" Y="4.070838134766" />
                  <Point X="22.807474609375" Y="4.268294921875" />
                  <Point X="22.84590625" Y="4.218209960938" />
                  <Point X="22.881435546875" Y="4.171908203125" />
                  <Point X="22.888181640625" Y="4.164045410156" />
                  <Point X="22.902486328125" Y="4.149103027344" />
                  <Point X="22.910044921875" Y="4.142022949219" />
                  <Point X="22.926625" Y="4.128110839844" />
                  <Point X="22.93491015625" Y="4.121895996094" />
                  <Point X="22.952107421875" Y="4.110405273438" />
                  <Point X="22.977935546875" Y="4.096323242188" />
                  <Point X="23.056236328125" Y="4.0555625" />
                  <Point X="23.065673828125" Y="4.051286621094" />
                  <Point X="23.084955078125" Y="4.0437890625" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031378173828" />
                  <Point X="23.219181640625" Y="4.035136474609" />
                  <Point X="23.2292734375" Y="4.037489257812" />
                  <Point X="23.24913671875" Y="4.043279296875" />
                  <Point X="23.27652734375" Y="4.054014892578" />
                  <Point X="23.35808203125" Y="4.087795654297" />
                  <Point X="23.36741796875" Y="4.092273925781" />
                  <Point X="23.385546875" Y="4.102219726562" />
                  <Point X="23.39433984375" Y="4.1076875" />
                  <Point X="23.4120703125" Y="4.1201015625" />
                  <Point X="23.420216796875" Y="4.126494140625" />
                  <Point X="23.4357734375" Y="4.140135253906" />
                  <Point X="23.4497578125" Y="4.155395507812" />
                  <Point X="23.461990234375" Y="4.172081054688" />
                  <Point X="23.4676484375" Y="4.180754882812" />
                  <Point X="23.47846875" Y="4.199499023438" />
                  <Point X="23.483150390625" Y="4.208737792969" />
                  <Point X="23.491478515625" Y="4.22766796875" />
                  <Point X="23.500859375" Y="4.255548339844" />
                  <Point X="23.527404296875" Y="4.339735839844" />
                  <Point X="23.529974609375" Y="4.349764160156" />
                  <Point X="23.534009765625" Y="4.370047363281" />
                  <Point X="23.535474609375" Y="4.380302246094" />
                  <Point X="23.537361328125" Y="4.401863769531" />
                  <Point X="23.53769921875" Y="4.412216796875" />
                  <Point X="23.537248046875" Y="4.432897949219" />
                  <Point X="23.536458984375" Y="4.4432265625" />
                  <Point X="23.520734375" Y="4.562655761719" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.19057421875" Y="4.730569335938" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.74" Y="4.389859863281" />
                  <Point X="24.77433203125" Y="4.261728027344" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912597656" />
                  <Point X="24.7998828125" Y="4.205341308594" />
                  <Point X="24.8177421875" Y="4.178614257812" />
                  <Point X="24.82739453125" Y="4.166452636719" />
                  <Point X="24.84855078125" Y="4.143865234375" />
                  <Point X="24.873103515625" Y="4.125025390625" />
                  <Point X="24.9003984375" Y="4.110436035156" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.021630859375" Y="4.085113037109" />
                  <Point X="25.05216796875" Y="4.090154541016" />
                  <Point X="25.06723046875" Y="4.093927246094" />
                  <Point X="25.09766796875" Y="4.104259277344" />
                  <Point X="25.1119140625" Y="4.110436035156" />
                  <Point X="25.13920703125" Y="4.125024902344" />
                  <Point X="25.1637578125" Y="4.143861328125" />
                  <Point X="25.184916015625" Y="4.16644921875" />
                  <Point X="25.1945703125" Y="4.178612792969" />
                  <Point X="25.2124296875" Y="4.205339355469" />
                  <Point X="25.21997265625" Y="4.218911621094" />
                  <Point X="25.232748046875" Y="4.24710546875" />
                  <Point X="25.23798046875" Y="4.261727050781" />
                  <Point X="25.378193359375" Y="4.785005859375" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="25.9286171875" Y="4.713590820313" />
                  <Point X="26.45360546875" Y="4.586841796875" />
                  <Point X="26.51698046875" Y="4.563855957031" />
                  <Point X="26.85825" Y="4.44007421875" />
                  <Point X="26.92166796875" Y="4.410416015625" />
                  <Point X="27.25044140625" Y="4.256659667969" />
                  <Point X="27.31176953125" Y="4.220930664062" />
                  <Point X="27.629439453125" Y="4.035854003906" />
                  <Point X="27.68721484375" Y="3.994768554688" />
                  <Point X="27.817783203125" Y="3.901915527344" />
                  <Point X="27.218392578125" Y="2.863742431641" />
                  <Point X="27.0653125" Y="2.59859765625" />
                  <Point X="27.06096875" Y="2.590121826172" />
                  <Point X="27.04967578125" Y="2.563891601562" />
                  <Point X="27.040859375" Y="2.537545166016" />
                  <Point X="27.03748828125" Y="2.526337646484" />
                  <Point X="27.01983203125" Y="2.460315429688" />
                  <Point X="27.01738671875" Y="2.447917480469" />
                  <Point X="27.0141640625" Y="2.422916992188" />
                  <Point X="27.01338671875" Y="2.410314453125" />
                  <Point X="27.013537109375" Y="2.379072998047" />
                  <Point X="27.014900390625" Y="2.357243896484" />
                  <Point X="27.021783203125" Y="2.300158447266" />
                  <Point X="27.02380078125" Y="2.289038085938" />
                  <Point X="27.029142578125" Y="2.267117431641" />
                  <Point X="27.032466796875" Y="2.256317138672" />
                  <Point X="27.040732421875" Y="2.234227050781" />
                  <Point X="27.045314453125" Y="2.223895996094" />
                  <Point X="27.0556796875" Y="2.203843994141" />
                  <Point X="27.069095703125" Y="2.182875244141" />
                  <Point X="27.104421875" Y="2.130814453125" />
                  <Point X="27.11226953125" Y="2.120772216797" />
                  <Point X="27.129240234375" Y="2.101827148438" />
                  <Point X="27.13836328125" Y="2.092924316406" />
                  <Point X="27.162912109375" Y="2.071992675781" />
                  <Point X="27.179505859375" Y="2.059349609375" />
                  <Point X="27.23156640625" Y="2.024023925781" />
                  <Point X="27.24128125" Y="2.018245117188" />
                  <Point X="27.261326171875" Y="2.00788269043" />
                  <Point X="27.27165625" Y="2.003299316406" />
                  <Point X="27.293744140625" Y="1.995033203125" />
                  <Point X="27.304546875" Y="1.991708007812" />
                  <Point X="27.326470703125" Y="1.986364990234" />
                  <Point X="27.34992578125" Y="1.98285949707" />
                  <Point X="27.407015625" Y="1.975975219727" />
                  <Point X="27.4200078125" Y="1.975305786133" />
                  <Point X="27.445962890625" Y="1.975748413086" />
                  <Point X="27.45892578125" Y="1.976860473633" />
                  <Point X="27.492091796875" Y="1.982012573242" />
                  <Point X="27.512009765625" Y="1.986209960938" />
                  <Point X="27.578033203125" Y="2.003865234375" />
                  <Point X="27.583994140625" Y="2.005670654297" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572875977" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.8632734375" Y="2.736418945312" />
                  <Point X="28.94040625" Y="2.780951660156" />
                  <Point X="29.043958984375" Y="2.637038818359" />
                  <Point X="29.0761640625" Y="2.583816650391" />
                  <Point X="29.13688671875" Y="2.483472167969" />
                  <Point X="28.3745" Y="1.898471801758" />
                  <Point X="28.172953125" Y="1.743819946289" />
                  <Point X="28.165546875" Y="1.737508300781" />
                  <Point X="28.14468359375" Y="1.71711328125" />
                  <Point X="28.125171875" Y="1.694470703125" />
                  <Point X="28.11830859375" Y="1.686028564453" />
                  <Point X="28.07079296875" Y="1.624039794922" />
                  <Point X="28.063646484375" Y="1.613269897461" />
                  <Point X="28.0508828125" Y="1.590864746094" />
                  <Point X="28.045265625" Y="1.579229614258" />
                  <Point X="28.03297265625" Y="1.548604858398" />
                  <Point X="28.0263203125" Y="1.529002563477" />
                  <Point X="28.008619140625" Y="1.465712036133" />
                  <Point X="28.006228515625" Y="1.454670166016" />
                  <Point X="28.0027734375" Y="1.432370727539" />
                  <Point X="28.001708984375" Y="1.42111340332" />
                  <Point X="28.000892578125" Y="1.397542602539" />
                  <Point X="28.001173828125" Y="1.386240478516" />
                  <Point X="28.003078125" Y="1.36374609375" />
                  <Point X="28.007841796875" Y="1.33733984375" />
                  <Point X="28.02237109375" Y="1.266921142578" />
                  <Point X="28.02587890625" Y="1.254359741211" />
                  <Point X="28.034578125" Y="1.229842407227" />
                  <Point X="28.03976953125" Y="1.217886108398" />
                  <Point X="28.055505859375" Y="1.187531494141" />
                  <Point X="28.065455078125" Y="1.170552490234" />
                  <Point X="28.10497265625" Y="1.110484619141" />
                  <Point X="28.111734375" Y="1.101429199219" />
                  <Point X="28.126287109375" Y="1.084184204102" />
                  <Point X="28.134078125" Y="1.075994506836" />
                  <Point X="28.151322265625" Y="1.059903808594" />
                  <Point X="28.16003125" Y="1.052697875977" />
                  <Point X="28.1782421875" Y="1.039370849609" />
                  <Point X="28.2001171875" Y="1.026284790039" />
                  <Point X="28.25738671875" Y="0.994047546387" />
                  <Point X="28.269416015625" Y="0.988346557617" />
                  <Point X="28.294150390625" Y="0.978682617188" />
                  <Point X="28.30685546875" Y="0.974719604492" />
                  <Point X="28.341560546875" Y="0.966510559082" />
                  <Point X="28.360400390625" Y="0.963046447754" />
                  <Point X="28.437833984375" Y="0.952812866211" />
                  <Point X="28.444033203125" Y="0.952199829102" />
                  <Point X="28.46571875" Y="0.951222900391" />
                  <Point X="28.49121875" Y="0.952032287598" />
                  <Point X="28.50060546875" Y="0.952797302246" />
                  <Point X="29.6663984375" Y="1.106277099609" />
                  <Point X="29.704703125" Y="1.111320068359" />
                  <Point X="29.752689453125" Y="0.914207275391" />
                  <Point X="29.7628359375" Y="0.849038024902" />
                  <Point X="29.78387109375" Y="0.713921386719" />
                  <Point X="28.92296875" Y="0.483242492676" />
                  <Point X="28.6919921875" Y="0.421352661133" />
                  <Point X="28.6825625" Y="0.418290130615" />
                  <Point X="28.65501171875" Y="0.407170013428" />
                  <Point X="28.62649609375" Y="0.392662200928" />
                  <Point X="28.6175703125" Y="0.387816497803" />
                  <Point X="28.54149609375" Y="0.343844482422" />
                  <Point X="28.530564453125" Y="0.33646862793" />
                  <Point X="28.509830078125" Y="0.320275878906" />
                  <Point X="28.50002734375" Y="0.311459259033" />
                  <Point X="28.475853515625" Y="0.286473236084" />
                  <Point X="28.462935546875" Y="0.27166104126" />
                  <Point X="28.417291015625" Y="0.213499252319" />
                  <Point X="28.410853515625" Y="0.204207061768" />
                  <Point X="28.39912890625" Y="0.184925476074" />
                  <Point X="28.393841796875" Y="0.174935928345" />
                  <Point X="28.384068359375" Y="0.153471969604" />
                  <Point X="28.380005859375" Y="0.142926422119" />
                  <Point X="28.37316015625" Y="0.121426498413" />
                  <Point X="28.36708984375" Y="0.093307937622" />
                  <Point X="28.351875" Y="0.013861732483" />
                  <Point X="28.35029296875" Y="0.000648416102" />
                  <Point X="28.3489921875" Y="-0.025864864349" />
                  <Point X="28.3492734375" Y="-0.039164680481" />
                  <Point X="28.352560546875" Y="-0.075138252258" />
                  <Point X="28.355162109375" Y="-0.093586662292" />
                  <Point X="28.370376953125" Y="-0.173032867432" />
                  <Point X="28.37316015625" Y="-0.183987121582" />
                  <Point X="28.380005859375" Y="-0.205486755371" />
                  <Point X="28.384068359375" Y="-0.216031997681" />
                  <Point X="28.393841796875" Y="-0.237495803833" />
                  <Point X="28.399130859375" Y="-0.247486679077" />
                  <Point X="28.41085546875" Y="-0.266767669678" />
                  <Point X="28.42715234375" Y="-0.28862387085" />
                  <Point X="28.472796875" Y="-0.346785797119" />
                  <Point X="28.481921875" Y="-0.356840576172" />
                  <Point X="28.501509765625" Y="-0.375546234131" />
                  <Point X="28.51197265625" Y="-0.384196990967" />
                  <Point X="28.542720703125" Y="-0.406117523193" />
                  <Point X="28.557931640625" Y="-0.41590447998" />
                  <Point X="28.634005859375" Y="-0.459876647949" />
                  <Point X="28.639494140625" Y="-0.462813568115" />
                  <Point X="28.65915625" Y="-0.472016174316" />
                  <Point X="28.68302734375" Y="-0.481027618408" />
                  <Point X="28.6919921875" Y="-0.483912689209" />
                  <Point X="29.761078125" Y="-0.770373779297" />
                  <Point X="29.78487890625" Y="-0.776751342773" />
                  <Point X="29.7616171875" Y="-0.931050231934" />
                  <Point X="29.748611328125" Y="-0.98804296875" />
                  <Point X="29.7278046875" Y="-1.079219848633" />
                  <Point X="28.704650390625" Y="-0.944519042969" />
                  <Point X="28.436783203125" Y="-0.909253662109" />
                  <Point X="28.428625" Y="-0.908535644531" />
                  <Point X="28.40009765625" Y="-0.908042358398" />
                  <Point X="28.36672265625" Y="-0.910840942383" />
                  <Point X="28.354482421875" Y="-0.912676269531" />
                  <Point X="28.322224609375" Y="-0.919687744141" />
                  <Point X="28.17291796875" Y="-0.952139953613" />
                  <Point X="28.157875" Y="-0.956742675781" />
                  <Point X="28.1287578125" Y="-0.968366027832" />
                  <Point X="28.11468359375" Y="-0.975386657715" />
                  <Point X="28.08685546875" Y="-0.992278991699" />
                  <Point X="28.07412890625" Y="-1.001527526855" />
                  <Point X="28.050376953125" Y="-1.021998840332" />
                  <Point X="28.0393515625" Y="-1.033221679688" />
                  <Point X="28.019853515625" Y="-1.056671386719" />
                  <Point X="27.929607421875" Y="-1.165209838867" />
                  <Point X="27.921326171875" Y="-1.176849243164" />
                  <Point X="27.906603515625" Y="-1.201236572266" />
                  <Point X="27.900162109375" Y="-1.21398425293" />
                  <Point X="27.8888203125" Y="-1.241369384766" />
                  <Point X="27.88436328125" Y="-1.25493371582" />
                  <Point X="27.877533203125" Y="-1.282577758789" />
                  <Point X="27.87516015625" Y="-1.296657836914" />
                  <Point X="27.872365234375" Y="-1.327026489258" />
                  <Point X="27.859431640625" Y="-1.467588256836" />
                  <Point X="27.859291015625" Y="-1.483313598633" />
                  <Point X="27.861607421875" Y="-1.514580688477" />
                  <Point X="27.864064453125" Y="-1.530122558594" />
                  <Point X="27.871794921875" Y="-1.561743286133" />
                  <Point X="27.876787109375" Y="-1.576667602539" />
                  <Point X="27.88916015625" Y="-1.605480957031" />
                  <Point X="27.896541015625" Y="-1.619370117188" />
                  <Point X="27.914392578125" Y="-1.647137573242" />
                  <Point X="27.997021484375" Y="-1.775660766602" />
                  <Point X="28.001744140625" Y="-1.78235534668" />
                  <Point X="28.019798828125" Y="-1.804454223633" />
                  <Point X="28.0434921875" Y="-1.828121337891" />
                  <Point X="28.052798828125" Y="-1.836277832031" />
                  <Point X="29.044916015625" Y="-2.597557128906" />
                  <Point X="29.087171875" Y="-2.629981445312" />
                  <Point X="29.0455078125" Y="-2.697400878906" />
                  <Point X="29.01858984375" Y="-2.735647705078" />
                  <Point X="29.0012734375" Y="-2.760250976562" />
                  <Point X="28.08702734375" Y="-2.232410400391" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.815029296875" Y="-2.079513916016" />
                  <Point X="27.783123046875" Y="-2.069326416016" />
                  <Point X="27.771111328125" Y="-2.066337646484" />
                  <Point X="27.732720703125" Y="-2.059404296875" />
                  <Point X="27.555021484375" Y="-2.027311889648" />
                  <Point X="27.539361328125" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503295898" />
                  <Point X="27.460140625" Y="-2.03146105957" />
                  <Point X="27.444845703125" Y="-2.035136108398" />
                  <Point X="27.4150703125" Y="-2.044959594727" />
                  <Point X="27.40058984375" Y="-2.051108154297" />
                  <Point X="27.3686953125" Y="-2.067893798828" />
                  <Point X="27.221072265625" Y="-2.145587158203" />
                  <Point X="27.20896875" Y="-2.153170898438" />
                  <Point X="27.186037109375" Y="-2.170065185547" />
                  <Point X="27.175208984375" Y="-2.179375732422" />
                  <Point X="27.15425" Y="-2.200335449219" />
                  <Point X="27.14494140625" Y="-2.211161132813" />
                  <Point X="27.12805078125" Y="-2.234087646484" />
                  <Point X="27.12046875" Y="-2.246188476562" />
                  <Point X="27.103681640625" Y="-2.278082519531" />
                  <Point X="27.02598828125" Y="-2.425706542969" />
                  <Point X="27.019837890625" Y="-2.440187744141" />
                  <Point X="27.01001171875" Y="-2.469968994141" />
                  <Point X="27.0063359375" Y="-2.485269042969" />
                  <Point X="27.00137890625" Y="-2.517442382812" />
                  <Point X="27.000279296875" Y="-2.533133789062" />
                  <Point X="27.00068359375" Y="-2.564484130859" />
                  <Point X="27.0021875" Y="-2.580143066406" />
                  <Point X="27.00912109375" Y="-2.618534912109" />
                  <Point X="27.04121484375" Y="-2.796233398438" />
                  <Point X="27.04301953125" Y="-2.804228759766" />
                  <Point X="27.05123828125" Y="-2.831542236328" />
                  <Point X="27.064072265625" Y="-2.862479980469" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.7070859375" Y="-3.977822998047" />
                  <Point X="27.735896484375" Y="-4.027723144531" />
                  <Point X="27.723755859375" Y="-4.036082763672" />
                  <Point X="27.01596875" Y="-3.113677734375" />
                  <Point X="26.83391796875" Y="-2.876422607422" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849626220703" />
                  <Point X="26.78325390625" Y="-2.828004882812" />
                  <Point X="26.77330078125" Y="-2.820646484375" />
                  <Point X="26.735435546875" Y="-2.796302978516" />
                  <Point X="26.56017578125" Y="-2.683627685547" />
                  <Point X="26.546283203125" Y="-2.676244628906" />
                  <Point X="26.517470703125" Y="-2.663873291016" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.366984375" Y="-2.650327148438" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079875" Y="-2.699414794922" />
                  <Point X="26.055494140625" Y="-2.714135742188" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="26.011884765625" Y="-2.749000976562" />
                  <Point X="25.863876953125" Y="-2.872063720703" />
                  <Point X="25.852654296875" Y="-2.883087646484" />
                  <Point X="25.83218359375" Y="-2.906837158203" />
                  <Point X="25.822935546875" Y="-2.919562744141" />
                  <Point X="25.80604296875" Y="-2.947389404297" />
                  <Point X="25.79901953125" Y="-2.961467285156" />
                  <Point X="25.78739453125" Y="-2.990588867188" />
                  <Point X="25.78279296875" Y="-3.005632568359" />
                  <Point X="25.773232421875" Y="-3.049620361328" />
                  <Point X="25.728978515625" Y="-3.253220458984" />
                  <Point X="25.7275859375" Y="-3.261287353516" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520263672" />
                  <Point X="25.83308984375" Y="-4.152321777344" />
                  <Point X="25.72623828125" Y="-3.753544921875" />
                  <Point X="25.655068359375" Y="-3.487937011719" />
                  <Point X="25.652607421875" Y="-3.480122314453" />
                  <Point X="25.64214453125" Y="-3.453575439453" />
                  <Point X="25.62678515625" Y="-3.423811035156" />
                  <Point X="25.62040625" Y="-3.413207275391" />
                  <Point X="25.59131640625" Y="-3.371295898438" />
                  <Point X="25.456677734375" Y="-3.177306884766" />
                  <Point X="25.446669921875" Y="-3.165172119141" />
                  <Point X="25.424787109375" Y="-3.142717529297" />
                  <Point X="25.412912109375" Y="-3.132397705078" />
                  <Point X="25.38665625" Y="-3.113153808594" />
                  <Point X="25.3732421875" Y="-3.104937255859" />
                  <Point X="25.3452421875" Y="-3.090829589844" />
                  <Point X="25.33065625" Y="-3.084938476562" />
                  <Point X="25.285642578125" Y="-3.070968017578" />
                  <Point X="25.077296875" Y="-3.006305175781" />
                  <Point X="25.063376953125" Y="-3.003109130859" />
                  <Point X="25.035216796875" Y="-2.99883984375" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.998840087891" />
                  <Point X="24.948935546875" Y="-3.003109619141" />
                  <Point X="24.935015625" Y="-3.006305664062" />
                  <Point X="24.89000390625" Y="-3.020275878906" />
                  <Point X="24.68165625" Y="-3.084938964844" />
                  <Point X="24.6670703125" Y="-3.090829345703" />
                  <Point X="24.6390703125" Y="-3.104936767578" />
                  <Point X="24.62565625" Y="-3.113153808594" />
                  <Point X="24.599400390625" Y="-3.132397705078" />
                  <Point X="24.587525390625" Y="-3.142717773438" />
                  <Point X="24.565640625" Y="-3.165174560547" />
                  <Point X="24.555630859375" Y="-3.177311279297" />
                  <Point X="24.52654296875" Y="-3.219222412109" />
                  <Point X="24.391904296875" Y="-3.413211669922" />
                  <Point X="24.38753125" Y="-3.420131835938" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213378906" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.027833984375" Y="-4.717318359375" />
                  <Point X="24.01457421875" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.799513918543" Y="3.870272256762" />
                  <Point X="26.338739015107" Y="4.614574246046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.751948664123" Y="3.787886971826" />
                  <Point X="25.941038016769" Y="4.710592033413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.704383409703" Y="3.705501686889" />
                  <Point X="25.639430986684" Y="4.757647498412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.656818155283" Y="3.623116401952" />
                  <Point X="25.377987742843" Y="4.784238492172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.609252900863" Y="3.540731117016" />
                  <Point X="25.352850507949" Y="4.690425560506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.968919344854" Y="2.741325468404" />
                  <Point X="28.917314350481" Y="2.767619526358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.561687646444" Y="3.458345832079" />
                  <Point X="25.327713273054" Y="4.596612628839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.079450287613" Y="2.578386147536" />
                  <Point X="28.819215787684" Y="2.710982248086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.514122392024" Y="3.375960547142" />
                  <Point X="25.30257603816" Y="4.502799697173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.104798189357" Y="2.458849753931" />
                  <Point X="28.721117184532" Y="2.654344990377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.466557137604" Y="3.293575262206" />
                  <Point X="25.277438803265" Y="4.408986765506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.649088306717" Y="4.7291473347" />
                  <Point X="24.561173101003" Y="4.773942369409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.021295241887" Y="2.394775638199" />
                  <Point X="28.623018581379" Y="2.597707732668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.418991883184" Y="3.211189977269" />
                  <Point X="25.252301568371" Y="4.31517383384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.682174420988" Y="4.605668124879" />
                  <Point X="24.391004371483" Y="4.754026675232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.937792294418" Y="2.330701522468" />
                  <Point X="28.524919978227" Y="2.541070474959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.371426628764" Y="3.128804692332" />
                  <Point X="25.2222232802" Y="4.223878494565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.715260535259" Y="4.482188915057" />
                  <Point X="24.220835641964" Y="4.734110981055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.854289346948" Y="2.266627406736" />
                  <Point X="28.426821375074" Y="2.484433217249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.323861374344" Y="3.046419407396" />
                  <Point X="25.16577786457" Y="4.14601787776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.748346438675" Y="4.358709812672" />
                  <Point X="24.054421322335" Y="4.712282319491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.770786399478" Y="2.202553291004" />
                  <Point X="28.328722771922" Y="2.42779595954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.276296119924" Y="2.964034122459" />
                  <Point X="25.061548915663" Y="4.092504187227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.786000179004" Y="4.232903281131" />
                  <Point X="23.919439135687" Y="4.674438186241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.687283452009" Y="2.138479175273" />
                  <Point X="28.230624168769" Y="2.371158701831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.228730865504" Y="2.881648837522" />
                  <Point X="23.78445694904" Y="4.636594052991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.603780504539" Y="2.074405059541" />
                  <Point X="28.132525565617" Y="2.314521444122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.181165914082" Y="2.799263398201" />
                  <Point X="23.649474762392" Y="4.598749919741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.520277557069" Y="2.010330943809" />
                  <Point X="28.034426962465" Y="2.257884186412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.133601046804" Y="2.716877916006" />
                  <Point X="23.521430206417" Y="4.557370887105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.4367746096" Y="1.946256828078" />
                  <Point X="27.936328359312" Y="2.201246928703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.086036179526" Y="2.63449243381" />
                  <Point X="23.536469652949" Y="4.443086913775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.35327161073" Y="1.882182738535" />
                  <Point X="27.83822975616" Y="2.144609670994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.044674558883" Y="2.548946239585" />
                  <Point X="23.527710139976" Y="4.340929115985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.711469657048" Y="1.083525275907" />
                  <Point X="29.666730808833" Y="1.106320857654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.269768461078" Y="1.818108725821" />
                  <Point X="27.740131153007" Y="2.087972413285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.018876358818" Y="2.455470086494" />
                  <Point X="23.498688211045" Y="4.249095534793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.74110173594" Y="0.961805985015" />
                  <Point X="29.500441624725" Y="1.084428436357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.186265311425" Y="1.754034713107" />
                  <Point X="27.642032549855" Y="2.031335155575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.015718559737" Y="2.350458072915" />
                  <Point X="23.456168210295" Y="4.164139564712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.763669094238" Y="0.843686349059" />
                  <Point X="29.334152439901" Y="1.062536015426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.116109643221" Y="1.683159818908" />
                  <Point X="27.518128838514" Y="1.987846257215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.042528912577" Y="2.230176523257" />
                  <Point X="23.377308099254" Y="4.097699805663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.781698136385" Y="0.72787910068" />
                  <Point X="29.167863255076" Y="1.040643594495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.059231405233" Y="1.60551973611" />
                  <Point X="27.296330838468" Y="1.994236990311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.212562118797" Y="2.036919284854" />
                  <Point X="23.263675307998" Y="4.048977612129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.663262217679" Y="0.68160422282" />
                  <Point X="29.001574070251" Y="1.018751173564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.023056004081" Y="1.517331031067" />
                  <Point X="22.71969676615" Y="4.219527530604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.526124800726" Y="0.64485823426" />
                  <Point X="28.835284885426" Y="0.996858752632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.001751697579" Y="1.421565124839" />
                  <Point X="22.619592831098" Y="4.163912040533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.388987383773" Y="0.6081122457" />
                  <Point X="28.668995700601" Y="0.974966331701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.01372095867" Y="1.308845489126" />
                  <Point X="22.519488896046" Y="4.108296550461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.251849966821" Y="0.57136625714" />
                  <Point X="28.502706515776" Y="0.95307391077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.061017339229" Y="1.178125786987" />
                  <Point X="22.419385291588" Y="4.052680891943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.114712549868" Y="0.534620268581" />
                  <Point X="22.324632526616" Y="3.994338844531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.977575132915" Y="0.497874280021" />
                  <Point X="22.241087751847" Y="3.930286040873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.840437546758" Y="0.461128377675" />
                  <Point X="22.157542977078" Y="3.866233237215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.703299848648" Y="0.424382532372" />
                  <Point X="22.073998202309" Y="3.802180433556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.59331964184" Y="0.373799254106" />
                  <Point X="21.990453427541" Y="3.738127629898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.502336758388" Y="0.313536356118" />
                  <Point X="21.975797891869" Y="3.638974005723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.437893060754" Y="0.239751067546" />
                  <Point X="22.063011147205" Y="3.487915640021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.386686246938" Y="0.159221249798" />
                  <Point X="22.150224402542" Y="3.336857274319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.361729172152" Y="0.065316521971" />
                  <Point X="22.229568080357" Y="3.18980865864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.349183509332" Y="-0.034912136117" />
                  <Point X="22.248662970898" Y="3.073458333379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.771395879345" Y="-0.8661865258" />
                  <Point X="29.386224564499" Y="-0.66993193847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.365983764799" Y="-0.150093286411" />
                  <Point X="22.244917611779" Y="2.968745696592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.754099250378" Y="-0.963994445726" />
                  <Point X="28.944868492086" Y="-0.551670779862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.426259316353" Y="-0.287426206485" />
                  <Point X="22.20511297246" Y="2.882406180758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.732302607038" Y="-1.059509493806" />
                  <Point X="22.138018476415" Y="2.809971541438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.503870081926" Y="-1.049738301344" />
                  <Point X="22.053436527533" Y="2.746447204386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.221709137436" Y="-1.012591111848" />
                  <Point X="21.880253711158" Y="2.72806726417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.939548192945" Y="-0.975443922352" />
                  <Point X="21.215444921082" Y="2.960183269186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.657387248459" Y="-0.938296732858" />
                  <Point X="21.156042174236" Y="2.883829487898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.390357203414" Y="-0.908859121704" />
                  <Point X="21.09663942739" Y="2.807475706611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.238198043062" Y="-0.937951149706" />
                  <Point X="21.037236945359" Y="2.731121790393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.109084441447" Y="-0.978785476382" />
                  <Point X="20.981661881496" Y="2.652817707214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.029624800712" Y="-1.044919759795" />
                  <Point X="20.933693966949" Y="2.570637587859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.967354031664" Y="-1.119812210781" />
                  <Point X="20.885726052402" Y="2.488457468505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.909261581571" Y="-1.19683362161" />
                  <Point X="20.837758137856" Y="2.40627734915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.876802093028" Y="-1.286915678696" />
                  <Point X="20.789790223309" Y="2.324097229795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.866718161028" Y="-1.388398651286" />
                  <Point X="21.035910178021" Y="2.092071856666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.859900915029" Y="-1.491546083529" />
                  <Point X="21.449485011644" Y="1.774723961089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.894694437157" Y="-1.615895261106" />
                  <Point X="21.561452790449" Y="1.61105253569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.996059368354" Y="-1.774164265812" />
                  <Point X="21.576992536728" Y="1.496513646907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.337581213187" Y="-2.054799329888" />
                  <Point X="21.549925173095" Y="1.403684164953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.751157388141" Y="-2.372147908907" />
                  <Point X="21.505824184682" Y="1.319533748322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.077774189946" Y="-2.645188474235" />
                  <Point X="21.427985872104" Y="1.252573356951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.025839238158" Y="-2.725347287155" />
                  <Point X="28.302114571526" Y="-2.35659115108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.711238088102" Y="-2.055524545267" />
                  <Point X="21.303017881798" Y="1.209626735809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.456481096906" Y="-2.032340367392" />
                  <Point X="21.059748124587" Y="1.226957875625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.343267686748" Y="-2.081276246267" />
                  <Point X="20.777587204683" Y="1.264105052594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.240333890333" Y="-2.135449849956" />
                  <Point X="20.49542618384" Y="1.301252280993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.155658714491" Y="-2.1989266855" />
                  <Point X="20.352634905757" Y="1.267387078567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.103274306557" Y="-2.278856489076" />
                  <Point X="20.327248233403" Y="1.173701241634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.059026087223" Y="-2.362931887805" />
                  <Point X="20.301861561049" Y="1.080015404701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.017183408985" Y="-2.448232970944" />
                  <Point X="20.276474638897" Y="0.986329695047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.00044944525" Y="-2.546327583125" />
                  <Point X="20.258508431316" Y="0.888862942464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.016845289605" Y="-2.661302675665" />
                  <Point X="21.692114947696" Y="0.051782945233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.095488735464" Y="0.3557791842" />
                  <Point X="20.243837495159" Y="0.78971716523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.038053559521" Y="-2.778729821502" />
                  <Point X="21.704057989856" Y="-0.060923331267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.654133774837" Y="0.474039776325" />
                  <Point X="20.229166559001" Y="0.690571387995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.092307607779" Y="-2.912994632403" />
                  <Point X="21.677543764634" Y="-0.154034651318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.1795212112" Y="-3.064053175463" />
                  <Point X="26.848453394766" Y="-2.895365697482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.367453445263" Y="-2.650283982004" />
                  <Point X="21.623356477299" Y="-0.233045841958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.266734814621" Y="-3.215111718524" />
                  <Point X="26.982785735689" Y="-3.070432436447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.190208607442" Y="-2.666594218919" />
                  <Point X="21.535072306978" Y="-0.294683802967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.353948418042" Y="-3.366170261584" />
                  <Point X="27.11711962374" Y="-3.245499963714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.064081976201" Y="-2.708950483018" />
                  <Point X="21.405483980551" Y="-0.33527624527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.441162021463" Y="-3.517228804645" />
                  <Point X="27.251454019341" Y="-3.420567749591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.982074853851" Y="-2.773786759715" />
                  <Point X="21.268346489064" Y="-0.372022195853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.528375624884" Y="-3.668287347705" />
                  <Point X="27.385788414943" Y="-3.595635535467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.902565488426" Y="-2.839895707134" />
                  <Point X="21.131209015378" Y="-0.408768155506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.615589228305" Y="-3.819345890765" />
                  <Point X="27.520122810544" Y="-3.770703321344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.830158632955" Y="-2.909623564129" />
                  <Point X="20.994071549087" Y="-0.445514118927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.702802831726" Y="-3.970404433826" />
                  <Point X="27.654457206145" Y="-3.945771107221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.786371373915" Y="-2.993933833859" />
                  <Point X="20.856934082797" Y="-0.482260082348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.764575345998" Y="-3.089449195513" />
                  <Point X="20.719796616506" Y="-0.519006045769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.743711207831" Y="-3.185439378711" />
                  <Point X="22.046354975791" Y="-1.301542282639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.672644523283" Y="-1.111127296344" />
                  <Point X="20.582659150215" Y="-0.55575200919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.725423556049" Y="-3.282742347291" />
                  <Point X="22.046876304658" Y="-1.40842890554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.506355300132" Y="-1.133019697747" />
                  <Point X="20.445521683925" Y="-0.59249797261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.733162476745" Y="-3.393306516913" />
                  <Point X="25.53734422712" Y="-3.293532135253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.963133235855" Y="-3.000957021824" />
                  <Point X="22.014889057792" Y="-1.498751581777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.34006607698" Y="-1.15491209915" />
                  <Point X="20.308384217634" Y="-0.629243936031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.748208881506" Y="-3.507594035637" />
                  <Point X="25.642127805853" Y="-3.453543027882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.828970025295" Y="-3.039218444233" />
                  <Point X="21.95232259046" Y="-1.573493366962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.173776853829" Y="-1.176804500553" />
                  <Point X="20.218624288211" Y="-0.690129960221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.763255286266" Y="-3.621881554361" />
                  <Point X="25.679527806111" Y="-3.5792202724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.698926492009" Y="-3.079578947058" />
                  <Point X="21.868856225986" Y="-1.637586122661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.007487630677" Y="-1.198696901956" />
                  <Point X="20.235071134797" Y="-0.805131039696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.778301691026" Y="-3.736169073085" />
                  <Point X="25.712614234535" Y="-3.70269964229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.597158123651" Y="-3.134346366001" />
                  <Point X="21.785353333568" Y="-1.701660266443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.841198407526" Y="-1.220589303359" />
                  <Point X="20.251517990859" Y="-0.920132123999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.793348095786" Y="-3.850456591809" />
                  <Point X="25.74570042674" Y="-3.826178891821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.533841722854" Y="-3.208706040999" />
                  <Point X="21.701850433778" Y="-1.765734406469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.674909184374" Y="-1.242481704762" />
                  <Point X="20.275850968026" Y="-1.039151387703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.808394500546" Y="-3.964744110533" />
                  <Point X="25.778786453587" Y="-3.949658057097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.47917391742" Y="-3.287472395438" />
                  <Point X="21.618347508288" Y="-1.8298085334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.508619961223" Y="-1.264374106165" />
                  <Point X="20.307160658158" Y="-1.161725464216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.823440905306" Y="-4.079031629257" />
                  <Point X="25.811872480434" Y="-4.073137222374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.424505996416" Y="-3.366238690991" />
                  <Point X="22.63839433502" Y="-2.456169343871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.427605607891" Y="-2.348767122932" />
                  <Point X="21.534844582798" Y="-1.893882660331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.342330746651" Y="-1.286266511939" />
                  <Point X="20.33847034829" Y="-1.284299540729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.373383969966" Y="-3.44681171006" />
                  <Point X="22.688016534155" Y="-2.588074109765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.296544543058" Y="-2.388609167537" />
                  <Point X="21.451341657308" Y="-1.957956787261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.343743199143" Y="-3.538329975559" />
                  <Point X="22.674584282561" Y="-2.68785102831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.19844591305" Y="-2.445246411563" />
                  <Point X="21.367838731818" Y="-2.022030914192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.318606067509" Y="-3.632142959839" />
                  <Point X="22.630778464627" Y="-2.772151841811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.100347283042" Y="-2.501883655589" />
                  <Point X="21.284335806328" Y="-2.086105041123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.293468935875" Y="-3.725955944119" />
                  <Point X="22.583213424047" Y="-2.854537235705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.002248653035" Y="-2.558520899614" />
                  <Point X="21.200832880838" Y="-2.150179168054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.268331804241" Y="-3.8197689284" />
                  <Point X="22.535648383468" Y="-2.936922629599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.904150023027" Y="-2.61515814364" />
                  <Point X="21.117329955348" Y="-2.214253294985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.243194672607" Y="-3.91358191268" />
                  <Point X="22.488083192357" Y="-3.019307946793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.80605139302" Y="-2.671795387666" />
                  <Point X="21.033827029858" Y="-2.278327421916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.218057540973" Y="-4.007394896961" />
                  <Point X="22.440517964178" Y="-3.1016932451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.707952763012" Y="-2.728432631692" />
                  <Point X="20.950324104368" Y="-2.342401548846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.192920409339" Y="-4.101207881241" />
                  <Point X="22.392952735999" Y="-3.184078543407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.609854133004" Y="-2.785069875718" />
                  <Point X="20.866821178878" Y="-2.406475675777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.167783277705" Y="-4.195020865521" />
                  <Point X="23.512466630352" Y="-3.861120356218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.398677460942" Y="-3.803141878527" />
                  <Point X="22.34538750782" Y="-3.266463841714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.511755502997" Y="-2.841707119744" />
                  <Point X="20.857473342416" Y="-2.508333707778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.14264614607" Y="-4.288833849802" />
                  <Point X="23.773495222488" Y="-4.100742059532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.148066156113" Y="-3.78207003336" />
                  <Point X="22.297822279641" Y="-3.348849140021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.413656872989" Y="-2.89834436377" />
                  <Point X="20.948810760589" Y="-2.661493439403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.117509014436" Y="-4.382646834082" />
                  <Point X="23.824037405896" Y="-4.233115580827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.957391343979" Y="-3.791537356576" />
                  <Point X="22.250257051462" Y="-3.431234438328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.315558242982" Y="-2.954981607795" />
                  <Point X="21.060087747044" Y="-2.82481288852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.092371882802" Y="-4.476459818362" />
                  <Point X="23.847638405088" Y="-4.351761883124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.857497080316" Y="-3.847259679556" />
                  <Point X="22.202691823283" Y="-3.513619736635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.217459643533" Y="-3.011618867392" />
                  <Point X="21.192658166854" Y="-2.998981883839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.067234751168" Y="-4.570272802643" />
                  <Point X="23.871239375088" Y="-4.470408170547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.766964711386" Y="-3.907752126159" />
                  <Point X="22.155126595104" Y="-3.596005034943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.042097619534" Y="-4.664085786923" />
                  <Point X="23.878577458038" Y="-4.580768103136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.676432342456" Y="-3.968244572761" />
                  <Point X="22.107561366925" Y="-3.67839033325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.01696081847" Y="-4.757898939638" />
                  <Point X="23.865422346702" Y="-4.680686231694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.596189854246" Y="-4.033979975463" />
                  <Point X="22.059996138747" Y="-3.760775631557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.536803357092" Y="-4.110342036382" />
                  <Point X="22.076851516712" Y="-3.875984868166" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999837890625" Y="0.001626220703" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.5427109375" Y="-3.802720214844" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.43523046875" Y="-3.4796328125" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.229322265625" Y="-3.252429443359" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.94632421875" Y="-3.201736816406" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643798828" />
                  <Point X="24.6826328125" Y="-3.327554931641" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.211361328125" Y="-4.766494140625" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.144763671875" Y="-4.985622558594" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.8486796875" Y="-4.924923828125" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.682283203125" Y="-4.61611328125" />
                  <Point X="23.6908515625" Y="-4.551039550781" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.679564453125" Y="-4.4806953125" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.572275390625" Y="-4.166284667969" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.295755859375" Y="-3.982157714844" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791259766" />
                  <Point X="22.9642890625" Y="-4.004415039062" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157293945313" />
                  <Point X="22.555328125" Y="-4.398311523438" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.503302734375" Y="-4.389979003906" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="22.073439453125" Y="-4.113153808594" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.36170703125" Y="-2.858201171875" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.499748046875" Y="-2.613671630859" />
                  <Point X="22.49560546875" Y="-2.582190429688" />
                  <Point X="22.482939453125" Y="-2.565682861328" />
                  <Point X="22.468673828125" Y="-2.551416503906" />
                  <Point X="22.439845703125" Y="-2.537198730469" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.348" Y="-3.15564453125" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="21.122033203125" Y="-3.219900634766" />
                  <Point X="20.83830078125" Y="-2.847135742188" />
                  <Point X="20.787591796875" Y="-2.762104980469" />
                  <Point X="20.56898046875" Y="-2.395526855469" />
                  <Point X="21.60480078125" Y="-1.600713989258" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.84746484375" Y="-1.411081665039" />
                  <Point X="21.855546875" Y="-1.390728881836" />
                  <Point X="21.8618828125" Y="-1.366266601562" />
                  <Point X="21.863349609375" Y="-1.350051513672" />
                  <Point X="21.85134375" Y="-1.321068847656" />
                  <Point X="21.83413671875" Y="-1.307870239258" />
                  <Point X="21.812359375" Y="-1.295052856445" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.441921875" Y="-1.464794555664" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.183580078125" Y="-1.445638549805" />
                  <Point X="20.072607421875" Y="-1.011187744141" />
                  <Point X="20.059193359375" Y="-0.91739453125" />
                  <Point X="20.00160546875" Y="-0.5147421875" />
                  <Point X="21.178923828125" Y="-0.199280441284" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.463037109375" Y="-0.118002891541" />
                  <Point X="21.485859375" Y="-0.102163024902" />
                  <Point X="21.4976796875" Y="-0.090641860962" />
                  <Point X="21.50674609375" Y="-0.070612129211" />
                  <Point X="21.514353515625" Y="-0.046100879669" />
                  <Point X="21.516599609375" Y="-0.031284379959" />
                  <Point X="21.5127109375" Y="-0.011163576126" />
                  <Point X="21.505103515625" Y="0.013347678185" />
                  <Point X="21.497677734375" Y="0.028085262299" />
                  <Point X="21.480927734375" Y="0.043025104523" />
                  <Point X="21.45810546875" Y="0.058864971161" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.222384765625" Y="0.393024078369" />
                  <Point X="20.001814453125" Y="0.45212600708" />
                  <Point X="20.0108359375" Y="0.513096008301" />
                  <Point X="20.08235546875" Y="0.996414855957" />
                  <Point X="20.109361328125" Y="1.096076538086" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="21.057771484375" Y="1.418857788086" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.279208984375" Y="1.399307495117" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.348466796875" Y="1.426056396484" />
                  <Point X="21.360880859375" Y="1.443786621094" />
                  <Point X="21.365259765625" Y="1.454358520508" />
                  <Point X="21.38552734375" Y="1.503290893555" />
                  <Point X="21.389287109375" Y="1.524606079102" />
                  <Point X="21.38368359375" Y="1.545512451172" />
                  <Point X="21.378400390625" Y="1.555662475586" />
                  <Point X="21.353943359375" Y="1.602642211914" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="20.640150390625" Y="2.156260253906" />
                  <Point X="20.52389453125" Y="2.245466308594" />
                  <Point X="20.5620859375" Y="2.3108984375" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="20.911521484375" Y="2.878954345703" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.724787109375" Y="2.993948974609" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.876685546875" Y="2.919104980469" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927404541016" />
                  <Point X="21.997537109375" Y="2.938192871094" />
                  <Point X="22.04747265625" Y="2.988127929688" />
                  <Point X="22.0591015625" Y="3.006382080078" />
                  <Point X="22.061927734375" Y="3.027840820312" />
                  <Point X="22.06059765625" Y="3.043040039062" />
                  <Point X="22.054443359375" Y="3.113390380859" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.737794921875" Y="3.671209472656" />
                  <Point X="21.69272265625" Y="3.749276855469" />
                  <Point X="21.763111328125" Y="3.803244140625" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.3597890625" Y="4.23692578125" />
                  <Point X="22.858451171875" Y="4.513971679688" />
                  <Point X="22.99664453125" Y="4.333874511719" />
                  <Point X="23.032173828125" Y="4.287572753906" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.065669921875" Y="4.264854492188" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.2038125" Y="4.229549316406" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.30309765625" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.29448828125" />
                  <Point X="23.31965234375" Y="4.312677246094" />
                  <Point X="23.346197265625" Y="4.396864746094" />
                  <Point X="23.348083984375" Y="4.418426269531" />
                  <Point X="23.31282421875" Y="4.686247558594" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.405322265625" Y="4.727624023438" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.16848828125" Y="4.91928125" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.92352734375" Y="4.439035644531" />
                  <Point X="24.957859375" Y="4.310903808594" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176269531" />
                  <Point X="25.054453125" Y="4.310902832031" />
                  <Point X="25.213365234375" Y="4.903969726562" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.313107421875" Y="4.982861328125" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="25.97320703125" Y="4.898284179688" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.58176171875" Y="4.742470703125" />
                  <Point X="26.931029296875" Y="4.615788085938" />
                  <Point X="27.00215625" Y="4.582524414062" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.4074140625" Y="4.385102050781" />
                  <Point X="27.73252734375" Y="4.195688964844" />
                  <Point X="27.797326171875" Y="4.149608886719" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.382935546875" Y="2.768742431641" />
                  <Point X="27.22985546875" Y="2.50359765625" />
                  <Point X="27.2210390625" Y="2.477251220703" />
                  <Point X="27.2033828125" Y="2.411229003906" />
                  <Point X="27.203533203125" Y="2.379987548828" />
                  <Point X="27.210416015625" Y="2.322902099609" />
                  <Point X="27.218681640625" Y="2.300812011719" />
                  <Point X="27.226314453125" Y="2.289564208984" />
                  <Point X="27.261640625" Y="2.237503417969" />
                  <Point X="27.286189453125" Y="2.216571777344" />
                  <Point X="27.33825" Y="2.181246337891" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.372671875" Y="2.171492919922" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.462927734375" Y="2.169760742188" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.7682734375" Y="2.900963867188" />
                  <Point X="28.994248046875" Y="3.031430664062" />
                  <Point X="29.009748046875" Y="3.009890625" />
                  <Point X="29.20259765625" Y="2.741874267578" />
                  <Point X="29.23871875" Y="2.682182373047" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.4901640625" Y="1.747734619141" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.26910546875" Y="1.570440307617" />
                  <Point X="28.22158984375" Y="1.508451782227" />
                  <Point X="28.209296875" Y="1.477827026367" />
                  <Point X="28.191595703125" Y="1.414536499023" />
                  <Point X="28.190779296875" Y="1.390965698242" />
                  <Point X="28.193919921875" Y="1.375751708984" />
                  <Point X="28.20844921875" Y="1.305332885742" />
                  <Point X="28.224185546875" Y="1.274978271484" />
                  <Point X="28.263703125" Y="1.214910522461" />
                  <Point X="28.280947265625" Y="1.198819824219" />
                  <Point X="28.2933203125" Y="1.191854858398" />
                  <Point X="28.35058984375" Y="1.159617553711" />
                  <Point X="28.385294921875" Y="1.151408569336" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.64159765625" Y="1.294651611328" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.85636328125" Y="1.291608032227" />
                  <Point X="29.939193359375" Y="0.951367248535" />
                  <Point X="29.95057421875" Y="0.878266479492" />
                  <Point X="29.997859375" Y="0.574556396484" />
                  <Point X="28.97214453125" Y="0.29971673584" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.71265234375" Y="0.223319076538" />
                  <Point X="28.636578125" Y="0.179346984863" />
                  <Point X="28.612404296875" Y="0.154360824585" />
                  <Point X="28.566759765625" Y="0.096199020386" />
                  <Point X="28.556986328125" Y="0.074735107422" />
                  <Point X="28.55369921875" Y="0.057570766449" />
                  <Point X="28.538484375" Y="-0.021875450134" />
                  <Point X="28.541771484375" Y="-0.057848968506" />
                  <Point X="28.556986328125" Y="-0.137295181274" />
                  <Point X="28.566759765625" Y="-0.158759094238" />
                  <Point X="28.57662109375" Y="-0.171324966431" />
                  <Point X="28.622265625" Y="-0.22948677063" />
                  <Point X="28.653013671875" Y="-0.251407180786" />
                  <Point X="28.729087890625" Y="-0.295379425049" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.81025390625" Y="-0.586847900391" />
                  <Point X="29.9980703125" Y="-0.637172668457" />
                  <Point X="29.9946796875" Y="-0.659658203125" />
                  <Point X="29.948431640625" Y="-0.966413024902" />
                  <Point X="29.933849609375" Y="-1.030313354492" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="28.679849609375" Y="-1.132893554688" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.362580078125" Y="-1.105352539062" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.1854453125" Y="-1.154697265625" />
                  <Point X="28.165947265625" Y="-1.178146972656" />
                  <Point X="28.075701171875" Y="-1.286685424805" />
                  <Point X="28.064359375" Y="-1.314070800781" />
                  <Point X="28.061564453125" Y="-1.344439331055" />
                  <Point X="28.048630859375" Y="-1.485001220703" />
                  <Point X="28.056361328125" Y="-1.516621826172" />
                  <Point X="28.074212890625" Y="-1.544389282227" />
                  <Point X="28.156841796875" Y="-1.672912475586" />
                  <Point X="28.168462890625" Y="-1.685540771484" />
                  <Point X="29.160580078125" Y="-2.4468203125" />
                  <Point X="29.339076171875" Y="-2.583784423828" />
                  <Point X="29.334498046875" Y="-2.591192138672" />
                  <Point X="29.2041328125" Y="-2.802140380859" />
                  <Point X="29.17396875" Y="-2.845" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="27.99202734375" Y="-2.396955322266" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.698951171875" Y="-2.246379394531" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.45718359375" Y="-2.236030517578" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.33468359375" />
                  <Point X="27.271814453125" Y="-2.366577636719" />
                  <Point X="27.19412109375" Y="-2.514201660156" />
                  <Point X="27.1891640625" Y="-2.546375" />
                  <Point X="27.19609765625" Y="-2.584766845703" />
                  <Point X="27.22819140625" Y="-2.762465332031" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.87162890625" Y="-3.882822998047" />
                  <Point X="27.98667578125" Y="-4.082088867188" />
                  <Point X="27.8353125" Y="-4.190202636719" />
                  <Point X="27.801576171875" Y="-4.212041015625" />
                  <Point X="27.67977734375" Y="-4.29087890625" />
                  <Point X="26.86523046875" Y="-3.229342529297" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.632685546875" Y="-2.956123779297" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.38439453125" Y="-2.839527832031" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.133357421875" Y="-2.895096679688" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045986083984" />
                  <Point X="25.958896484375" Y="-3.089973876953" />
                  <Point X="25.914642578125" Y="-3.293573974609" />
                  <Point X="25.9139296875" Y="-3.310719970703" />
                  <Point X="26.094603515625" Y="-4.68306640625" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.963189453125" Y="-4.968907226562" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#134" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.034332864015" Y="4.483256472984" Z="0.45" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.45" />
                  <Point X="-0.843480619063" Y="5.000222398833" Z="0.45" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.45" />
                  <Point X="-1.614332013093" Y="4.807047800245" Z="0.45" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.45" />
                  <Point X="-1.742905199774" Y="4.711001854986" Z="0.45" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.45" />
                  <Point X="-1.734190241094" Y="4.358992850533" Z="0.45" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.45" />
                  <Point X="-1.821479282605" Y="4.307022986653" Z="0.45" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.45" />
                  <Point X="-1.917398592442" Y="4.340485031035" Z="0.45" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.45" />
                  <Point X="-1.969843752107" Y="4.395593050578" Z="0.45" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.45" />
                  <Point X="-2.670650300553" Y="4.311913135075" Z="0.45" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.45" />
                  <Point X="-3.273466801179" Y="3.874204010449" Z="0.45" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.45" />
                  <Point X="-3.311663736356" Y="3.677489470242" Z="0.45" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.45" />
                  <Point X="-2.995369779718" Y="3.069962712144" Z="0.45" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.45" />
                  <Point X="-3.043975085552" Y="3.004828482334" Z="0.45" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.45" />
                  <Point X="-3.125113753463" Y="3.000194919455" Z="0.45" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.45" />
                  <Point X="-3.256369878055" Y="3.068530212389" Z="0.45" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.45" />
                  <Point X="-4.134099077038" Y="2.940936810086" Z="0.45" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.45" />
                  <Point X="-4.487251715715" Y="2.367383643488" Z="0.45" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.45" />
                  <Point X="-4.396444612471" Y="2.147872655727" Z="0.45" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.45" />
                  <Point X="-3.672106175733" Y="1.563854356768" Z="0.45" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.45" />
                  <Point X="-3.687090905442" Y="1.504771957906" Z="0.45" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.45" />
                  <Point X="-3.741982769352" Y="1.478273482031" Z="0.45" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.45" />
                  <Point X="-3.941860913068" Y="1.49971023579" Z="0.45" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.45" />
                  <Point X="-4.945055480665" Y="1.140433880613" Z="0.45" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.45" />
                  <Point X="-5.044781995129" Y="0.55169504314" Z="0.45" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.45" />
                  <Point X="-4.796713180603" Y="0.376007899355" Z="0.45" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.45" />
                  <Point X="-3.553738584093" Y="0.03322915556" Z="0.45" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.45" />
                  <Point X="-3.541200344844" Y="0.005295662601" Z="0.45" />
                  <Point X="-3.539556741714" Y="0" Z="0.45" />
                  <Point X="-3.547164256291" Y="-0.02451128846" Z="0.45" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.45" />
                  <Point X="-3.57163001103" Y="-0.045646827574" Z="0.45" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.45" />
                  <Point X="-3.84017460817" Y="-0.119704157401" Z="0.45" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.45" />
                  <Point X="-4.99646016249" Y="-0.893193587201" Z="0.45" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.45" />
                  <Point X="-4.871022888721" Y="-1.426732691204" Z="0.45" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.45" />
                  <Point X="-4.557709527767" Y="-1.48308680904" Z="0.45" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.45" />
                  <Point X="-3.197381343172" Y="-1.319680623083" Z="0.45" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.45" />
                  <Point X="-3.199011980054" Y="-1.346912093697" Z="0.45" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.45" />
                  <Point X="-3.431793447104" Y="-1.529766330591" Z="0.45" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.45" />
                  <Point X="-4.261507783135" Y="-2.756434628784" Z="0.45" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.45" />
                  <Point X="-3.923909370875" Y="-3.21890371937" Z="0.45" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.45" />
                  <Point X="-3.633157281851" Y="-3.167665719743" Z="0.45" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.45" />
                  <Point X="-2.558573203016" Y="-2.569757266116" Z="0.45" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.45" />
                  <Point X="-2.687751293426" Y="-2.801921100641" Z="0.45" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.45" />
                  <Point X="-2.963220686312" Y="-4.121490793656" Z="0.45" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.45" />
                  <Point X="-2.529176406828" Y="-4.401209589249" Z="0.45" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.45" />
                  <Point X="-2.411161618723" Y="-4.397469737687" Z="0.45" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.45" />
                  <Point X="-2.014087667929" Y="-4.014708156463" Z="0.45" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.45" />
                  <Point X="-1.71367011556" Y="-4.000770826133" Z="0.45" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.45" />
                  <Point X="-1.466848354894" Y="-4.172596468379" Z="0.45" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.45" />
                  <Point X="-1.375632165196" Y="-4.45917013316" Z="0.45" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.45" />
                  <Point X="-1.373445650327" Y="-4.578305902483" Z="0.45" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.45" />
                  <Point X="-1.169936989073" Y="-4.942066734772" Z="0.45" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.45" />
                  <Point X="-0.870920385443" Y="-5.003426490845" Z="0.45" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.45" />
                  <Point X="-0.746498660615" Y="-4.748155041593" Z="0.45" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.45" />
                  <Point X="-0.282447819757" Y="-3.324784005997" Z="0.45" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.45" />
                  <Point X="-0.045013131282" Y="-3.218209783249" Z="0.45" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.45" />
                  <Point X="0.208345948079" Y="-3.268902262039" Z="0.45" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.45" />
                  <Point X="0.387998039675" Y="-3.47686174977" Z="0.45" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.45" />
                  <Point X="0.488256199786" Y="-3.784380988222" Z="0.45" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.45" />
                  <Point X="0.965969618833" Y="-4.986821973809" Z="0.45" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.45" />
                  <Point X="1.145243268851" Y="-4.948727964023" Z="0.45" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.45" />
                  <Point X="1.13801861818" Y="-4.645259956099" Z="0.45" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.45" />
                  <Point X="1.001599091258" Y="-3.069313767264" Z="0.45" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.45" />
                  <Point X="1.159166876598" Y="-2.90226316244" Z="0.45" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.45" />
                  <Point X="1.382819023662" Y="-2.858037306907" Z="0.45" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.45" />
                  <Point X="1.599489244912" Y="-2.966901796977" Z="0.45" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.45" />
                  <Point X="1.819406304562" Y="-3.228500537964" Z="0.45" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.45" />
                  <Point X="2.822588520388" Y="-4.22273497403" Z="0.45" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.45" />
                  <Point X="3.012891384806" Y="-4.089129828809" Z="0.45" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.45" />
                  <Point X="2.908772968465" Y="-3.826543028274" Z="0.45" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.45" />
                  <Point X="2.23914464274" Y="-2.54459996668" Z="0.45" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.45" />
                  <Point X="2.309904536801" Y="-2.358584323774" Z="0.45" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.45" />
                  <Point X="2.474314054598" Y="-2.248996773166" Z="0.45" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.45" />
                  <Point X="2.683906836833" Y="-2.264303366751" Z="0.45" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.45" />
                  <Point X="2.960870568833" Y="-2.408976531809" Z="0.45" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.45" />
                  <Point X="4.208700587705" Y="-2.842497225805" Z="0.45" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.45" />
                  <Point X="4.370873356363" Y="-2.586197466459" Z="0.45" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.45" />
                  <Point X="4.184861382642" Y="-2.375872475861" Z="0.45" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.45" />
                  <Point X="3.110114378065" Y="-1.486069910876" Z="0.45" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.45" />
                  <Point X="3.10519696738" Y="-1.317740618281" Z="0.45" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.45" />
                  <Point X="3.198237809223" Y="-1.178833886695" Z="0.45" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.45" />
                  <Point X="3.367042168307" Y="-1.122931767198" Z="0.45" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.45" />
                  <Point X="3.667167141175" Y="-1.151185818388" Z="0.45" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.45" />
                  <Point X="4.976437809896" Y="-1.01015736536" Z="0.45" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.45" />
                  <Point X="5.037963392922" Y="-0.635832261691" Z="0.45" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.45" />
                  <Point X="4.817039064043" Y="-0.507271451699" Z="0.45" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.45" />
                  <Point X="3.671878534196" Y="-0.176838405643" Z="0.45" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.45" />
                  <Point X="3.609798215412" Y="-0.109176389044" Z="0.45" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.45" />
                  <Point X="3.584721890616" Y="-0.017164344549" Z="0.45" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.45" />
                  <Point X="3.596649559808" Y="0.079446186687" Z="0.45" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.45" />
                  <Point X="3.645581222988" Y="0.154772349531" Z="0.45" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.45" />
                  <Point X="3.731516880156" Y="0.211310438599" Z="0.45" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.45" />
                  <Point X="3.978928753264" Y="0.28270048107" Z="0.45" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.45" />
                  <Point X="4.993821428296" Y="0.917238515969" Z="0.45" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.45" />
                  <Point X="4.89878783736" Y="1.334714821748" Z="0.45" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.45" />
                  <Point X="4.628915675516" Y="1.375503856558" Z="0.45" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.45" />
                  <Point X="3.385689730269" Y="1.23225762344" Z="0.45" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.45" />
                  <Point X="3.311691534704" Y="1.266706113421" Z="0.45" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.45" />
                  <Point X="3.259799158735" Y="1.333738771697" Z="0.45" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.45" />
                  <Point X="3.236731195913" Y="1.417135222847" Z="0.45" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.45" />
                  <Point X="3.251291925168" Y="1.495639777648" Z="0.45" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.45" />
                  <Point X="3.302632268605" Y="1.571302437762" Z="0.45" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.45" />
                  <Point X="3.514444177421" Y="1.739346794695" Z="0.45" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.45" />
                  <Point X="4.27533875876" Y="2.739348723578" Z="0.45" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.45" />
                  <Point X="4.044176472672" Y="3.070373811586" Z="0.45" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.45" />
                  <Point X="3.737116360408" Y="2.975545171278" Z="0.45" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.45" />
                  <Point X="2.443855743561" Y="2.249343577718" Z="0.45" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.45" />
                  <Point X="2.372501181993" Y="2.252413228508" Z="0.45" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.45" />
                  <Point X="2.308105837878" Y="2.289226004833" Z="0.45" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.45" />
                  <Point X="2.261532595402" Y="2.348919022504" Z="0.45" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.45" />
                  <Point X="2.24701647425" Y="2.41725726184" Z="0.45" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.45" />
                  <Point X="2.263184364939" Y="2.495613832321" Z="0.45" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.45" />
                  <Point X="2.420080181581" Y="2.775022721547" Z="0.45" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.45" />
                  <Point X="2.820145221556" Y="4.221636065027" Z="0.45" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.45" />
                  <Point X="2.426426471989" Y="4.459584537241" Z="0.45" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.45" />
                  <Point X="2.01718165921" Y="4.659096440886" Z="0.45" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.45" />
                  <Point X="1.592652860634" Y="4.820753979845" Z="0.45" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.45" />
                  <Point X="0.978783593789" Y="4.978167688907" Z="0.45" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.45" />
                  <Point X="0.312158490307" Y="5.06386956812" Z="0.45" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.45" />
                  <Point X="0.158911697538" Y="4.948191032967" Z="0.45" />
                  <Point X="0" Y="4.355124473572" Z="0.45" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>