<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#171" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2079" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999524414062" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.776412109375" Y="-4.307853027344" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140136719" />
                  <Point X="25.542365234375" Y="-3.467376708984" />
                  <Point X="25.45526171875" Y="-3.341878417969" />
                  <Point X="25.37863671875" Y="-3.231476318359" />
                  <Point X="25.356751953125" Y="-3.209020751953" />
                  <Point X="25.33049609375" Y="-3.189776855469" />
                  <Point X="25.30249609375" Y="-3.175669433594" />
                  <Point X="25.1677109375" Y="-3.133836669922" />
                  <Point X="25.04913671875" Y="-3.097036132812" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097036132812" />
                  <Point X="24.828390625" Y="-3.138868652344" />
                  <Point X="24.70981640625" Y="-3.175669433594" />
                  <Point X="24.68181640625" Y="-3.189776855469" />
                  <Point X="24.655560546875" Y="-3.209020751953" />
                  <Point X="24.63367578125" Y="-3.231477050781" />
                  <Point X="24.54657421875" Y="-3.356975097656" />
                  <Point X="24.46994921875" Y="-3.467377441406" />
                  <Point X="24.4618125" Y="-3.481572021484" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.26153515625" Y="-4.212185546875" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="24.05666015625" Y="-4.871748535156" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.767576171875" Y="-4.805962890625" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.759384765625" Y="-4.75829296875" />
                  <Point X="23.7850390625" Y="-4.563437988281" />
                  <Point X="23.78580078125" Y="-4.547929199219" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.751291015625" Y="-4.354341308594" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.182964355469" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.55226171875" Y="-4.022376464844" />
                  <Point X="23.443095703125" Y="-3.926639404297" />
                  <Point X="23.4168125" Y="-3.910295898438" />
                  <Point X="23.387115234375" Y="-3.897994628906" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.192271484375" Y="-3.880171142578" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708496094" />
                  <Point X="22.985533203125" Y="-3.882028076172" />
                  <Point X="22.957341796875" Y="-3.894801513672" />
                  <Point X="22.82010546875" Y="-3.986500488281" />
                  <Point X="22.699376953125" Y="-4.067169677734" />
                  <Point X="22.68721484375" Y="-4.076821533203" />
                  <Point X="22.6648984375" Y="-4.099461425781" />
                  <Point X="22.559646484375" Y="-4.236628417969" />
                  <Point X="22.5198515625" Y="-4.288489257813" />
                  <Point X="22.39763671875" Y="-4.21281640625" />
                  <Point X="22.19828515625" Y="-4.089383544922" />
                  <Point X="21.98631640625" Y="-3.926173828125" />
                  <Point X="21.895279296875" Y="-3.856078369141" />
                  <Point X="22.180203125" Y="-3.362574951172" />
                  <Point X="22.576240234375" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647654541016" />
                  <Point X="22.593412109375" Y="-2.616128173828" />
                  <Point X="22.59442578125" Y="-2.585194091797" />
                  <Point X="22.58544140625" Y="-2.555575927734" />
                  <Point X="22.571224609375" Y="-2.526747070312" />
                  <Point X="22.55319921875" Y="-2.501591064453" />
                  <Point X="22.5358515625" Y="-2.484242431641" />
                  <Point X="22.510689453125" Y="-2.466212402344" />
                  <Point X="22.481859375" Y="-2.451995361328" />
                  <Point X="22.4522421875" Y="-2.443011230469" />
                  <Point X="22.421310546875" Y="-2.444024169922" />
                  <Point X="22.38978515625" Y="-2.450295166016" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.757373046875" Y="-2.809595703125" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="21.0746328125" Y="-3.000773925781" />
                  <Point X="20.917142578125" Y="-2.793862548828" />
                  <Point X="20.765171875" Y="-2.539033691406" />
                  <Point X="20.693857421875" Y="-2.419449951172" />
                  <Point X="21.201109375" Y="-2.030221557617" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.915423828125" Y="-1.475591918945" />
                  <Point X="21.933388671875" Y="-1.448459472656" />
                  <Point X="21.946142578125" Y="-1.419833129883" />
                  <Point X="21.95384765625" Y="-1.39008581543" />
                  <Point X="21.95665234375" Y="-1.359655151367" />
                  <Point X="21.954443359375" Y="-1.327984619141" />
                  <Point X="21.94744140625" Y="-1.298239990234" />
                  <Point X="21.931359375" Y="-1.272256713867" />
                  <Point X="21.91052734375" Y="-1.248300537109" />
                  <Point X="21.887029296875" Y="-1.228766601562" />
                  <Point X="21.860546875" Y="-1.213179931641" />
                  <Point X="21.83128125" Y="-1.201955688477" />
                  <Point X="21.79939453125" Y="-1.195474609375" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="21.00628125" Y="-1.294675537109" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.227521484375" Y="-1.2338046875" />
                  <Point X="20.165921875" Y="-0.992650390625" />
                  <Point X="20.125716796875" Y="-0.711532958984" />
                  <Point X="20.107576171875" Y="-0.584698425293" />
                  <Point X="20.677767578125" Y="-0.43191607666" />
                  <Point X="21.467125" Y="-0.220408309937" />
                  <Point X="21.4899453125" Y="-0.210971054077" />
                  <Point X="21.520279296875" Y="-0.193503158569" />
                  <Point X="21.533447265625" Y="-0.184359313965" />
                  <Point X="21.558251953125" Y="-0.163827545166" />
                  <Point X="21.57435546875" Y="-0.146728149414" />
                  <Point X="21.585771484375" Y="-0.126200218201" />
                  <Point X="21.59812109375" Y="-0.095600944519" />
                  <Point X="21.602638671875" Y="-0.081209854126" />
                  <Point X="21.609212890625" Y="-0.05244071579" />
                  <Point X="21.611599609375" Y="-0.031043210983" />
                  <Point X="21.609107421875" Y="-0.009657684326" />
                  <Point X="21.601939453125" Y="0.021013080597" />
                  <Point X="21.597341796875" Y="0.035403873444" />
                  <Point X="21.58558984375" Y="0.064093467712" />
                  <Point X="21.5740625" Y="0.084569282532" />
                  <Point X="21.557861328125" Y="0.10158934021" />
                  <Point X="21.531275390625" Y="0.123356079102" />
                  <Point X="21.518046875" Y="0.132436187744" />
                  <Point X="21.489490234375" Y="0.14867137146" />
                  <Point X="21.467125" Y="0.157848327637" />
                  <Point X="20.772716796875" Y="0.343914489746" />
                  <Point X="20.10818359375" Y="0.521975646973" />
                  <Point X="20.135814453125" Y="0.708702941895" />
                  <Point X="20.17551171875" Y="0.976967712402" />
                  <Point X="20.25644921875" Y="1.275651611328" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="20.66850390625" Y="1.374286010742" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228027344" />
                  <Point X="21.29686328125" Y="1.305263549805" />
                  <Point X="21.329541015625" Y="1.315567016602" />
                  <Point X="21.3582890625" Y="1.324631103516" />
                  <Point X="21.37722265625" Y="1.332961791992" />
                  <Point X="21.395966796875" Y="1.343783935547" />
                  <Point X="21.4126484375" Y="1.35601550293" />
                  <Point X="21.426287109375" Y="1.371567626953" />
                  <Point X="21.438701171875" Y="1.389297363281" />
                  <Point X="21.448650390625" Y="1.407433349609" />
                  <Point X="21.46176171875" Y="1.439089477539" />
                  <Point X="21.473296875" Y="1.46693762207" />
                  <Point X="21.479083984375" Y="1.486788818359" />
                  <Point X="21.48284375" Y="1.508105224609" />
                  <Point X="21.484197265625" Y="1.528750854492" />
                  <Point X="21.481048828125" Y="1.549200195312" />
                  <Point X="21.4754453125" Y="1.570107421875" />
                  <Point X="21.467951171875" Y="1.589377441406" />
                  <Point X="21.452130859375" Y="1.619770019531" />
                  <Point X="21.4382109375" Y="1.646506835938" />
                  <Point X="21.42671484375" Y="1.663711669922" />
                  <Point X="21.412802734375" Y="1.680289916992" />
                  <Point X="21.39786328125" Y="1.694590087891" />
                  <Point X="20.99955078125" Y="2.000227661133" />
                  <Point X="20.648140625" Y="2.269873779297" />
                  <Point X="20.76459765625" Y="2.469394042969" />
                  <Point X="20.9188515625" Y="2.733665039062" />
                  <Point X="21.133240234375" Y="3.009231933594" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.445826171875" Y="3.045310546875" />
                  <Point X="21.79334375" Y="2.844671142578" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.89871875" Y="2.821814697266" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.95943359375" Y="2.818762939453" />
                  <Point X="21.980892578125" Y="2.821587890625" />
                  <Point X="22.000984375" Y="2.826504150391" />
                  <Point X="22.019537109375" Y="2.83565234375" />
                  <Point X="22.037791015625" Y="2.847281005859" />
                  <Point X="22.053923828125" Y="2.860228759766" />
                  <Point X="22.086228515625" Y="2.892533447266" />
                  <Point X="22.1146484375" Y="2.920952148438" />
                  <Point X="22.127595703125" Y="2.937084472656" />
                  <Point X="22.139224609375" Y="2.955338623047" />
                  <Point X="22.14837109375" Y="2.973887695312" />
                  <Point X="22.1532890625" Y="2.993976806641" />
                  <Point X="22.156115234375" Y="3.015435546875" />
                  <Point X="22.15656640625" Y="3.03612109375" />
                  <Point X="22.152583984375" Y="3.081633056641" />
                  <Point X="22.14908203125" Y="3.121670654297" />
                  <Point X="22.145044921875" Y="3.141960449219" />
                  <Point X="22.13853515625" Y="3.162603759766" />
                  <Point X="22.130205078125" Y="3.181533203125" />
                  <Point X="21.95369921875" Y="3.487249023438" />
                  <Point X="21.81666796875" Y="3.724595947266" />
                  <Point X="22.0307265625" Y="3.888713623047" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.63703515625" Y="4.282280761719" />
                  <Point X="22.832962890625" Y="4.391133789062" />
                  <Point X="22.85041796875" Y="4.368385253906" />
                  <Point X="22.9568046875" Y="4.229740722656" />
                  <Point X="22.971109375" Y="4.214797851563" />
                  <Point X="22.987689453125" Y="4.200885742188" />
                  <Point X="23.00488671875" Y="4.18939453125" />
                  <Point X="23.055541015625" Y="4.163025390625" />
                  <Point X="23.100103515625" Y="4.139827636719" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.222548828125" Y="4.134482421875" />
                  <Point X="23.27530859375" Y="4.156336914062" />
                  <Point X="23.32172265625" Y="4.175562011719" />
                  <Point X="23.339857421875" Y="4.18551171875" />
                  <Point X="23.3575859375" Y="4.19792578125" />
                  <Point X="23.37313671875" Y="4.211563476563" />
                  <Point X="23.3853671875" Y="4.228243164063" />
                  <Point X="23.396189453125" Y="4.246987304688" />
                  <Point X="23.404521484375" Y="4.265921875" />
                  <Point X="23.421693359375" Y="4.320385742188" />
                  <Point X="23.43680078125" Y="4.368298339844" />
                  <Point X="23.4408359375" Y="4.388583496094" />
                  <Point X="23.44272265625" Y="4.410145507812" />
                  <Point X="23.442271484375" Y="4.430826660156" />
                  <Point X="23.422205078125" Y="4.583248046875" />
                  <Point X="23.41580078125" Y="4.631897949219" />
                  <Point X="23.702583984375" Y="4.712302246094" />
                  <Point X="24.050369140625" Y="4.80980859375" />
                  <Point X="24.45970703125" Y="4.857715820312" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.763291015625" Y="4.669990234375" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.23665625" Y="4.623839355469" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.540373046875" Y="4.863541503906" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.18270703125" Y="4.749974609375" />
                  <Point X="26.48102734375" Y="4.677950195312" />
                  <Point X="26.700771484375" Y="4.598248535156" />
                  <Point X="26.894640625" Y="4.527930664062" />
                  <Point X="27.107802734375" Y="4.4282421875" />
                  <Point X="27.294578125" Y="4.340893066406" />
                  <Point X="27.500521484375" Y="4.220910644531" />
                  <Point X="27.6809765625" Y="4.115775878906" />
                  <Point X="27.875189453125" Y="3.9776640625" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.605966796875" Y="3.345039794922" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142080078125" Y="2.539936767578" />
                  <Point X="27.133078125" Y="2.516058105469" />
                  <Point X="27.12165625" Y="2.473346191406" />
                  <Point X="27.111607421875" Y="2.435771728516" />
                  <Point X="27.108619140625" Y="2.417936279297" />
                  <Point X="27.107728515625" Y="2.380953857422" />
                  <Point X="27.112181640625" Y="2.344020263672" />
                  <Point X="27.116099609375" Y="2.311529052734" />
                  <Point X="27.121443359375" Y="2.289600830078" />
                  <Point X="27.1297109375" Y="2.267511230469" />
                  <Point X="27.140072265625" Y="2.247469726562" />
                  <Point X="27.16292578125" Y="2.213789794922" />
                  <Point X="27.18303125" Y="2.184161132813" />
                  <Point X="27.194466796875" Y="2.170328125" />
                  <Point X="27.221599609375" Y="2.145592529297" />
                  <Point X="27.255279296875" Y="2.122739257812" />
                  <Point X="27.284908203125" Y="2.102635009766" />
                  <Point X="27.304953125" Y="2.092272216797" />
                  <Point X="27.327041015625" Y="2.084006347656" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.3858984375" Y="2.074209960938" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.436466796875" Y="2.069845703125" />
                  <Point X="27.473205078125" Y="2.074170898438" />
                  <Point X="27.51591796875" Y="2.085592773438" />
                  <Point X="27.5534921875" Y="2.095640625" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.2869765625" Y="2.513390136719" />
                  <Point X="28.967326171875" Y="2.906190429688" />
                  <Point X="29.016228515625" Y="2.838227050781" />
                  <Point X="29.12328125" Y="2.68944921875" />
                  <Point X="29.231541015625" Y="2.510547363281" />
                  <Point X="29.26219921875" Y="2.459883544922" />
                  <Point X="28.8342890625" Y="2.131535644531" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.22142578125" Y="1.660241699219" />
                  <Point X="28.203974609375" Y="1.641627319336" />
                  <Point X="28.173234375" Y="1.601524780273" />
                  <Point X="28.146193359375" Y="1.566245849609" />
                  <Point X="28.136607421875" Y="1.550911743164" />
                  <Point X="28.121630859375" Y="1.517088378906" />
                  <Point X="28.1101796875" Y="1.476143554688" />
                  <Point X="28.10010546875" Y="1.440123779297" />
                  <Point X="28.09665234375" Y="1.417824951172" />
                  <Point X="28.0958359375" Y="1.394253173828" />
                  <Point X="28.097740234375" Y="1.371766235352" />
                  <Point X="28.107140625" Y="1.326209838867" />
                  <Point X="28.11541015625" Y="1.286133544922" />
                  <Point X="28.1206796875" Y="1.268978271484" />
                  <Point X="28.136283203125" Y="1.235741210938" />
                  <Point X="28.161849609375" Y="1.196881347656" />
                  <Point X="28.18433984375" Y="1.162695800781" />
                  <Point X="28.198892578125" Y="1.145451416016" />
                  <Point X="28.21613671875" Y="1.129361083984" />
                  <Point X="28.23434765625" Y="1.116034423828" />
                  <Point X="28.2713984375" Y="1.095178833008" />
                  <Point X="28.303990234375" Y="1.07683215332" />
                  <Point X="28.320521484375" Y="1.069501708984" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.406212890625" Y="1.052818115234" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.15167578125" Y="1.134332397461" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.7999609375" Y="1.121657348633" />
                  <Point X="29.84594140625" Y="0.932787841797" />
                  <Point X="29.880056640625" Y="0.713670898438" />
                  <Point X="29.8908671875" Y="0.644238952637" />
                  <Point X="29.408208984375" Y="0.514911071777" />
                  <Point X="28.716580078125" Y="0.329589874268" />
                  <Point X="28.7047890625" Y="0.325585662842" />
                  <Point X="28.681546875" Y="0.315067932129" />
                  <Point X="28.63233203125" Y="0.286620758057" />
                  <Point X="28.589037109375" Y="0.261595367432" />
                  <Point X="28.5743125" Y="0.251096496582" />
                  <Point X="28.54753125" Y="0.22557673645" />
                  <Point X="28.518001953125" Y="0.187949874878" />
                  <Point X="28.492025390625" Y="0.154848937988" />
                  <Point X="28.48030078125" Y="0.135566558838" />
                  <Point X="28.47052734375" Y="0.114102333069" />
                  <Point X="28.463681640625" Y="0.092604560852" />
                  <Point X="28.453837890625" Y="0.041208034515" />
                  <Point X="28.4451796875" Y="-0.004006072998" />
                  <Point X="28.443484375" Y="-0.021876203537" />
                  <Point X="28.4451796875" Y="-0.058554073334" />
                  <Point X="28.4550234375" Y="-0.109950454712" />
                  <Point X="28.463681640625" Y="-0.155164550781" />
                  <Point X="28.47052734375" Y="-0.176662322998" />
                  <Point X="28.48030078125" Y="-0.198126556396" />
                  <Point X="28.492025390625" Y="-0.217409088135" />
                  <Point X="28.5215546875" Y="-0.255035949707" />
                  <Point X="28.54753125" Y="-0.28813671875" />
                  <Point X="28.56" Y="-0.301235931396" />
                  <Point X="28.589037109375" Y="-0.324155517578" />
                  <Point X="28.638251953125" Y="-0.35260256958" />
                  <Point X="28.681546875" Y="-0.377627929688" />
                  <Point X="28.692708984375" Y="-0.383138244629" />
                  <Point X="28.716580078125" Y="-0.392149871826" />
                  <Point X="29.325013671875" Y="-0.555179443359" />
                  <Point X="29.89147265625" Y="-0.706961608887" />
                  <Point X="29.880693359375" Y="-0.77846295166" />
                  <Point X="29.85501953125" Y="-0.948748352051" />
                  <Point X="29.8113125" Y="-1.140276733398" />
                  <Point X="29.80117578125" Y="-1.18469909668" />
                  <Point X="29.2264765625" Y="-1.109038574219" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710144043" />
                  <Point X="28.37466015625" Y="-1.005508728027" />
                  <Point X="28.278068359375" Y="-1.026503295898" />
                  <Point X="28.193095703125" Y="-1.044972412109" />
                  <Point X="28.163974609375" Y="-1.056596923828" />
                  <Point X="28.1361484375" Y="-1.073489135742" />
                  <Point X="28.1123984375" Y="-1.093960327148" />
                  <Point X="28.054015625" Y="-1.164177490234" />
                  <Point X="28.002654296875" Y="-1.225948242188" />
                  <Point X="27.987935546875" Y="-1.250329345703" />
                  <Point X="27.976591796875" Y="-1.27771484375" />
                  <Point X="27.969759765625" Y="-1.305366088867" />
                  <Point X="27.961392578125" Y="-1.396300415039" />
                  <Point X="27.95403125" Y="-1.476296386719" />
                  <Point X="27.95634765625" Y="-1.507561401367" />
                  <Point X="27.964078125" Y="-1.539182373047" />
                  <Point X="27.976451171875" Y="-1.567996337891" />
                  <Point X="28.02990625" Y="-1.651142822266" />
                  <Point X="28.076931640625" Y="-1.724287109375" />
                  <Point X="28.0869375" Y="-1.737242431641" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.67526171875" Y="-2.194165771484" />
                  <Point X="29.213123046875" Y="-2.606882568359" />
                  <Point X="29.197171875" Y="-2.632694091797" />
                  <Point X="29.124798828125" Y="-2.749801513672" />
                  <Point X="29.034412109375" Y="-2.878230224609" />
                  <Point X="29.028982421875" Y="-2.8859453125" />
                  <Point X="28.515328125" Y="-2.58938671875" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.754224609375" Y="-2.159825195312" />
                  <Point X="27.639265625" Y="-2.139063720703" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395507812" />
                  <Point X="27.474609375" Y="-2.125353515625" />
                  <Point X="27.444833984375" Y="-2.135177001953" />
                  <Point X="27.349330078125" Y="-2.185439453125" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.242384765625" Y="-2.246549804688" />
                  <Point X="27.22142578125" Y="-2.267509277344" />
                  <Point X="27.204533203125" Y="-2.290438476562" />
                  <Point X="27.15426953125" Y="-2.38594140625" />
                  <Point X="27.110052734375" Y="-2.469956542969" />
                  <Point X="27.100228515625" Y="-2.499735595703" />
                  <Point X="27.095271484375" Y="-2.531909179688" />
                  <Point X="27.09567578125" Y="-2.563259277344" />
                  <Point X="27.1164375" Y="-2.678218505859" />
                  <Point X="27.134703125" Y="-2.779349609375" />
                  <Point X="27.13898828125" Y="-2.795141601562" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.514654296875" Y="-3.454521972656" />
                  <Point X="27.86128515625" Y="-4.054903808594" />
                  <Point X="27.781833984375" Y="-4.111655273438" />
                  <Point X="27.701767578125" Y="-4.163480957031" />
                  <Point X="27.3036796875" Y="-3.644685302734" />
                  <Point X="26.758548828125" Y="-2.934254882812" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.608544921875" Y="-2.827663574219" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.293099609375" Y="-2.75252734375" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="26.00884765625" Y="-2.875074707031" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968861083984" />
                  <Point X="25.88725" Y="-2.996687744141" />
                  <Point X="25.875625" Y="-3.02580859375" />
                  <Point X="25.84699609375" Y="-3.157524414063" />
                  <Point X="25.821810546875" Y="-3.273396484375" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323120361328" />
                  <Point X="25.92256640625" Y="-4.10414453125" />
                  <Point X="26.02206640625" Y="-4.859915527344" />
                  <Point X="25.975689453125" Y="-4.870081054688" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.94157421875" Y="-4.752636230469" />
                  <Point X="23.85875390625" Y="-4.731328125" />
                  <Point X="23.8792265625" Y="-4.575835449219" />
                  <Point X="23.879923828125" Y="-4.568098144531" />
                  <Point X="23.88055078125" Y="-4.541030273438" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497688964844" />
                  <Point X="23.84446484375" Y="-4.335807128906" />
                  <Point X="23.81613671875" Y="-4.193397460938" />
                  <Point X="23.811875" Y="-4.178468261719" />
                  <Point X="23.80097265625" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135462890625" />
                  <Point X="23.778259765625" Y="-4.107625976562" />
                  <Point X="23.76942578125" Y="-4.094861083984" />
                  <Point X="23.74979296875" Y="-4.070937744141" />
                  <Point X="23.738994140625" Y="-4.059779296875" />
                  <Point X="23.614900390625" Y="-3.950951660156" />
                  <Point X="23.505734375" Y="-3.855214599609" />
                  <Point X="23.49326171875" Y="-3.845964599609" />
                  <Point X="23.466978515625" Y="-3.82962109375" />
                  <Point X="23.45316796875" Y="-3.822527587891" />
                  <Point X="23.423470703125" Y="-3.810226318359" />
                  <Point X="23.4086875" Y="-3.805476318359" />
                  <Point X="23.378544921875" Y="-3.798447998047" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.198484375" Y="-3.785374511719" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166015625" />
                  <Point X="22.991994140625" Y="-3.781945556641" />
                  <Point X="22.9609453125" Y="-3.790265136719" />
                  <Point X="22.946326171875" Y="-3.79549609375" />
                  <Point X="22.918134765625" Y="-3.80826953125" />
                  <Point X="22.9045625" Y="-3.815812011719" />
                  <Point X="22.767326171875" Y="-3.907510986328" />
                  <Point X="22.64659765625" Y="-3.988180175781" />
                  <Point X="22.640322265625" Y="-3.992755371094" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032771484375" />
                  <Point X="22.589529296875" Y="-4.041629150391" />
                  <Point X="22.496798828125" Y="-4.162478515625" />
                  <Point X="22.4476484375" Y="-4.132045898438" />
                  <Point X="22.25240625" Y="-4.011156738281" />
                  <Point X="22.0442734375" Y="-3.850901367188" />
                  <Point X="22.01913671875" Y="-3.831547607422" />
                  <Point X="22.262474609375" Y="-3.410074951172" />
                  <Point X="22.65851171875" Y="-2.724119628906" />
                  <Point X="22.66515234375" Y="-2.710079833984" />
                  <Point X="22.676052734375" Y="-2.681114746094" />
                  <Point X="22.680314453125" Y="-2.666189453125" />
                  <Point X="22.6865859375" Y="-2.634663085938" />
                  <Point X="22.688361328125" Y="-2.619239501953" />
                  <Point X="22.689375" Y="-2.588305419922" />
                  <Point X="22.6853359375" Y="-2.557617675781" />
                  <Point X="22.6763515625" Y="-2.527999511719" />
                  <Point X="22.67064453125" Y="-2.51355859375" />
                  <Point X="22.656427734375" Y="-2.484729736328" />
                  <Point X="22.648447265625" Y="-2.4714140625" />
                  <Point X="22.630421875" Y="-2.446258056641" />
                  <Point X="22.620376953125" Y="-2.434417724609" />
                  <Point X="22.603029296875" Y="-2.417069091797" />
                  <Point X="22.591185546875" Y="-2.407020751953" />
                  <Point X="22.5660234375" Y="-2.388990722656" />
                  <Point X="22.552705078125" Y="-2.381009033203" />
                  <Point X="22.523875" Y="-2.366791992188" />
                  <Point X="22.509435546875" Y="-2.3610859375" />
                  <Point X="22.479818359375" Y="-2.352101806641" />
                  <Point X="22.4491328125" Y="-2.348062011719" />
                  <Point X="22.418201171875" Y="-2.349074951172" />
                  <Point X="22.40277734375" Y="-2.350849609375" />
                  <Point X="22.371251953125" Y="-2.357120605469" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372285888672" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.709873046875" Y="-2.727323242188" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="21.1502265625" Y="-2.943235595703" />
                  <Point X="20.99598046875" Y="-2.740587402344" />
                  <Point X="20.846763671875" Y="-2.490375" />
                  <Point X="20.818734375" Y="-2.443373535156" />
                  <Point X="21.25894140625" Y="-2.105590087891" />
                  <Point X="21.951876953125" Y="-1.573881958008" />
                  <Point X="21.963517578125" Y="-1.563310180664" />
                  <Point X="21.984896484375" Y="-1.540388671875" />
                  <Point X="21.994634765625" Y="-1.528038696289" />
                  <Point X="22.012599609375" Y="-1.50090625" />
                  <Point X="22.020166015625" Y="-1.48712121582" />
                  <Point X="22.032919921875" Y="-1.458494995117" />
                  <Point X="22.038107421875" Y="-1.443653686523" />
                  <Point X="22.0458125" Y="-1.41390637207" />
                  <Point X="22.048447265625" Y="-1.3988046875" />
                  <Point X="22.051251953125" Y="-1.368373901367" />
                  <Point X="22.051421875" Y="-1.353045043945" />
                  <Point X="22.049212890625" Y="-1.321374511719" />
                  <Point X="22.046916015625" Y="-1.306216430664" />
                  <Point X="22.0399140625" Y="-1.276471801758" />
                  <Point X="22.028220703125" Y="-1.248242675781" />
                  <Point X="22.012138671875" Y="-1.222259399414" />
                  <Point X="22.003046875" Y="-1.209918701172" />
                  <Point X="21.98221484375" Y="-1.185962524414" />
                  <Point X="21.9712578125" Y="-1.175246459961" />
                  <Point X="21.947759765625" Y="-1.155712768555" />
                  <Point X="21.935216796875" Y="-1.14689465332" />
                  <Point X="21.908734375" Y="-1.131307983398" />
                  <Point X="21.89456640625" Y="-1.124479858398" />
                  <Point X="21.86530078125" Y="-1.113255615234" />
                  <Point X="21.850203125" Y="-1.10885925293" />
                  <Point X="21.81831640625" Y="-1.102378173828" />
                  <Point X="21.802701171875" Y="-1.100532104492" />
                  <Point X="21.77137890625" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="20.993880859375" Y="-1.20048828125" />
                  <Point X="20.33908203125" Y="-1.286694213867" />
                  <Point X="20.31956640625" Y="-1.210293457031" />
                  <Point X="20.259236328125" Y="-0.974113098145" />
                  <Point X="20.219759765625" Y="-0.698083007812" />
                  <Point X="20.213548828125" Y="-0.65465447998" />
                  <Point X="20.70235546875" Y="-0.523678955078" />
                  <Point X="21.491712890625" Y="-0.312171325684" />
                  <Point X="21.5034296875" Y="-0.308197601318" />
                  <Point X="21.52625" Y="-0.298760345459" />
                  <Point X="21.537353515625" Y="-0.293296813965" />
                  <Point X="21.5676875" Y="-0.275828887939" />
                  <Point X="21.57446484375" Y="-0.271534851074" />
                  <Point X="21.5940234375" Y="-0.257541412354" />
                  <Point X="21.618828125" Y="-0.237009613037" />
                  <Point X="21.62741015625" Y="-0.228958557129" />
                  <Point X="21.643513671875" Y="-0.211859207153" />
                  <Point X="21.657380859375" Y="-0.192900115967" />
                  <Point X="21.668796875" Y="-0.172372177124" />
                  <Point X="21.6738671875" Y="-0.161754852295" />
                  <Point X="21.686216796875" Y="-0.13115562439" />
                  <Point X="21.688759765625" Y="-0.124053833008" />
                  <Point X="21.695251953125" Y="-0.102373313904" />
                  <Point X="21.701826171875" Y="-0.073604232788" />
                  <Point X="21.703626953125" Y="-0.06297190094" />
                  <Point X="21.706013671875" Y="-0.041574363708" />
                  <Point X="21.7059609375" Y="-0.020046632767" />
                  <Point X="21.70346875" Y="0.001338864207" />
                  <Point X="21.701615234375" Y="0.011961834908" />
                  <Point X="21.694447265625" Y="0.042632698059" />
                  <Point X="21.69243359375" Y="0.049924583435" />
                  <Point X="21.685251953125" Y="0.071414115906" />
                  <Point X="21.6735" Y="0.100103683472" />
                  <Point X="21.668373046875" Y="0.110698120117" />
                  <Point X="21.656845703125" Y="0.131173904419" />
                  <Point X="21.642873046875" Y="0.150068618774" />
                  <Point X="21.626671875" Y="0.167088775635" />
                  <Point X="21.61804296875" Y="0.175095535278" />
                  <Point X="21.59145703125" Y="0.196862243652" />
                  <Point X="21.585037109375" Y="0.201680160522" />
                  <Point X="21.565" Y="0.215022338867" />
                  <Point X="21.536443359375" Y="0.231257598877" />
                  <Point X="21.525552734375" Y="0.236560317993" />
                  <Point X="21.5031875" Y="0.245737335205" />
                  <Point X="21.491712890625" Y="0.249611343384" />
                  <Point X="20.7973046875" Y="0.435677398682" />
                  <Point X="20.2145546875" Y="0.591825073242" />
                  <Point X="20.229791015625" Y="0.694796813965" />
                  <Point X="20.26866796875" Y="0.95752130127" />
                  <Point X="20.348142578125" Y="1.25080456543" />
                  <Point X="20.3664140625" Y="1.318237060547" />
                  <Point X="20.656103515625" Y="1.280098754883" />
                  <Point X="21.221935546875" Y="1.20560559082" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.20470324707" />
                  <Point X="21.28485546875" Y="1.206589477539" />
                  <Point X="21.295111328125" Y="1.208053588867" />
                  <Point X="21.3153984375" Y="1.212089111328" />
                  <Point X="21.325431640625" Y="1.214660766602" />
                  <Point X="21.358109375" Y="1.224964111328" />
                  <Point X="21.386857421875" Y="1.234028198242" />
                  <Point X="21.396548828125" Y="1.237676025391" />
                  <Point X="21.415482421875" Y="1.246006713867" />
                  <Point X="21.42472265625" Y="1.250689819336" />
                  <Point X="21.443466796875" Y="1.261511962891" />
                  <Point X="21.452140625" Y="1.267171875" />
                  <Point X="21.468822265625" Y="1.279403320312" />
                  <Point X="21.48407421875" Y="1.293378051758" />
                  <Point X="21.497712890625" Y="1.308930175781" />
                  <Point X="21.504107421875" Y="1.317079101562" />
                  <Point X="21.516521484375" Y="1.334808837891" />
                  <Point X="21.5219921875" Y="1.343605224609" />
                  <Point X="21.53194140625" Y="1.361741210938" />
                  <Point X="21.536419921875" Y="1.371080932617" />
                  <Point X="21.54953125" Y="1.402737060547" />
                  <Point X="21.56106640625" Y="1.430585205078" />
                  <Point X="21.5645" Y="1.440349609375" />
                  <Point X="21.570287109375" Y="1.460200805664" />
                  <Point X="21.572640625" Y="1.470287475586" />
                  <Point X="21.576400390625" Y="1.491603881836" />
                  <Point X="21.577640625" Y="1.501890380859" />
                  <Point X="21.578994140625" Y="1.522536010742" />
                  <Point X="21.578091796875" Y="1.54320703125" />
                  <Point X="21.574943359375" Y="1.56365637207" />
                  <Point X="21.572810546875" Y="1.573793945312" />
                  <Point X="21.56720703125" Y="1.594701171875" />
                  <Point X="21.563986328125" Y="1.604540771484" />
                  <Point X="21.5564921875" Y="1.623810791016" />
                  <Point X="21.55221875" Y="1.633241210938" />
                  <Point X="21.5363984375" Y="1.663633789062" />
                  <Point X="21.522478515625" Y="1.690370605469" />
                  <Point X="21.51719921875" Y="1.699286499023" />
                  <Point X="21.505703125" Y="1.716491455078" />
                  <Point X="21.499486328125" Y="1.724779785156" />
                  <Point X="21.48557421875" Y="1.741358154297" />
                  <Point X="21.478494140625" Y="1.748917358398" />
                  <Point X="21.4635546875" Y="1.763217529297" />
                  <Point X="21.4556953125" Y="1.769958496094" />
                  <Point X="21.0573828125" Y="2.075596191406" />
                  <Point X="20.77238671875" Y="2.294281494141" />
                  <Point X="20.84664453125" Y="2.421504638672" />
                  <Point X="20.99771484375" Y="2.680322021484" />
                  <Point X="21.208220703125" Y="2.950897705078" />
                  <Point X="21.273662109375" Y="3.035013183594" />
                  <Point X="21.398326171875" Y="2.963038085938" />
                  <Point X="21.74584375" Y="2.762398681641" />
                  <Point X="21.75508203125" Y="2.757716796875" />
                  <Point X="21.774015625" Y="2.749385742188" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.890439453125" Y="2.727176269531" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.961505859375" Y="2.723785644531" />
                  <Point X="21.97183203125" Y="2.724575683594" />
                  <Point X="21.993291015625" Y="2.727400634766" />
                  <Point X="22.00347265625" Y="2.729310302734" />
                  <Point X="22.023564453125" Y="2.7342265625" />
                  <Point X="22.042998046875" Y="2.741299316406" />
                  <Point X="22.06155078125" Y="2.750447509766" />
                  <Point X="22.070580078125" Y="2.755529541016" />
                  <Point X="22.088833984375" Y="2.767158203125" />
                  <Point X="22.09725390625" Y="2.773191650391" />
                  <Point X="22.11338671875" Y="2.786139404297" />
                  <Point X="22.121099609375" Y="2.793053710938" />
                  <Point X="22.153404296875" Y="2.825358398438" />
                  <Point X="22.18182421875" Y="2.853777099609" />
                  <Point X="22.18873828125" Y="2.861490234375" />
                  <Point X="22.201685546875" Y="2.877622558594" />
                  <Point X="22.20771875" Y="2.886041748047" />
                  <Point X="22.21934765625" Y="2.904295898438" />
                  <Point X="22.2244296875" Y="2.913324462891" />
                  <Point X="22.233576171875" Y="2.931873535156" />
                  <Point X="22.240646484375" Y="2.951298095703" />
                  <Point X="22.245564453125" Y="2.971387207031" />
                  <Point X="22.2474765625" Y="2.981572265625" />
                  <Point X="22.250302734375" Y="3.003031005859" />
                  <Point X="22.251091796875" Y="3.013364013672" />
                  <Point X="22.25154296875" Y="3.034049560547" />
                  <Point X="22.251205078125" Y="3.044402099609" />
                  <Point X="22.24722265625" Y="3.0899140625" />
                  <Point X="22.243720703125" Y="3.129951660156" />
                  <Point X="22.242255859375" Y="3.140209716797" />
                  <Point X="22.23821875" Y="3.160499511719" />
                  <Point X="22.235646484375" Y="3.17053125" />
                  <Point X="22.22913671875" Y="3.191174560547" />
                  <Point X="22.22548828125" Y="3.200868164062" />
                  <Point X="22.217158203125" Y="3.219797607422" />
                  <Point X="22.2124765625" Y="3.229033447266" />
                  <Point X="22.035970703125" Y="3.534749267578" />
                  <Point X="21.94061328125" Y="3.699915039062" />
                  <Point X="22.088529296875" Y="3.813322021484" />
                  <Point X="22.3516328125" Y="4.015041015625" />
                  <Point X="22.683171875" Y="4.199236816406" />
                  <Point X="22.807474609375" Y="4.268296386719" />
                  <Point X="22.881435546875" Y="4.171909179688" />
                  <Point X="22.8881796875" Y="4.164046875" />
                  <Point X="22.902484375" Y="4.149104003906" />
                  <Point X="22.910044921875" Y="4.142022949219" />
                  <Point X="22.926625" Y="4.128110839844" />
                  <Point X="22.934908203125" Y="4.121896972656" />
                  <Point X="22.95210546875" Y="4.110405761719" />
                  <Point X="22.96101953125" Y="4.105128417969" />
                  <Point X="23.011673828125" Y="4.078759521484" />
                  <Point X="23.056236328125" Y="4.055561767578" />
                  <Point X="23.06567578125" Y="4.051285400391" />
                  <Point X="23.08495703125" Y="4.043788330078" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031378173828" />
                  <Point X="23.219181640625" Y="4.035136474609" />
                  <Point X="23.229271484375" Y="4.037488525391" />
                  <Point X="23.2491328125" Y="4.043277587891" />
                  <Point X="23.258904296875" Y="4.046714355469" />
                  <Point X="23.3116640625" Y="4.068568847656" />
                  <Point X="23.358078125" Y="4.087793945312" />
                  <Point X="23.36741796875" Y="4.092274169922" />
                  <Point X="23.385552734375" Y="4.102224121094" />
                  <Point X="23.39434765625" Y="4.107693359375" />
                  <Point X="23.412076171875" Y="4.120107421875" />
                  <Point X="23.420224609375" Y="4.126500976563" />
                  <Point X="23.435775390625" Y="4.140138671875" />
                  <Point X="23.449748046875" Y="4.155387695312" />
                  <Point X="23.461978515625" Y="4.172067382812" />
                  <Point X="23.467638671875" Y="4.1807421875" />
                  <Point X="23.4784609375" Y="4.199486328125" />
                  <Point X="23.483142578125" Y="4.208724121094" />
                  <Point X="23.491474609375" Y="4.227658691406" />
                  <Point X="23.495125" Y="4.23735546875" />
                  <Point X="23.512296875" Y="4.291819335938" />
                  <Point X="23.527404296875" Y="4.339731933594" />
                  <Point X="23.529974609375" Y="4.349764160156" />
                  <Point X="23.534009765625" Y="4.370049316406" />
                  <Point X="23.535474609375" Y="4.380302246094" />
                  <Point X="23.537361328125" Y="4.401864257812" />
                  <Point X="23.53769921875" Y="4.412217285156" />
                  <Point X="23.537248046875" Y="4.4328984375" />
                  <Point X="23.536458984375" Y="4.4432265625" />
                  <Point X="23.520736328125" Y="4.562655273438" />
                  <Point X="23.72823046875" Y="4.620829589844" />
                  <Point X="24.068826171875" Y="4.7163203125" />
                  <Point X="24.47075" Y="4.763359863281" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.67152734375" Y="4.64540234375" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.328419921875" Y="4.599251464844" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.530478515625" Y="4.769058105469" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.160412109375" Y="4.657627929688" />
                  <Point X="26.45359765625" Y="4.586843261719" />
                  <Point X="26.66837890625" Y="4.50894140625" />
                  <Point X="26.85825" Y="4.440073730469" />
                  <Point X="27.06755859375" Y="4.3421875" />
                  <Point X="27.250455078125" Y="4.25665234375" />
                  <Point X="27.45269921875" Y="4.138825195312" />
                  <Point X="27.629423828125" Y="4.035863525391" />
                  <Point X="27.81778125" Y="3.901916503906" />
                  <Point X="27.523693359375" Y="3.392539550781" />
                  <Point X="27.065310546875" Y="2.59859765625" />
                  <Point X="27.062380859375" Y="2.593114257812" />
                  <Point X="27.0531875" Y="2.573448242188" />
                  <Point X="27.044185546875" Y="2.549569580078" />
                  <Point X="27.041302734375" Y="2.540600341797" />
                  <Point X="27.029880859375" Y="2.497888427734" />
                  <Point X="27.01983203125" Y="2.460313964844" />
                  <Point X="27.0179140625" Y="2.451469970703" />
                  <Point X="27.013646484375" Y="2.420223388672" />
                  <Point X="27.012755859375" Y="2.383240966797" />
                  <Point X="27.013412109375" Y="2.36958203125" />
                  <Point X="27.017865234375" Y="2.3326484375" />
                  <Point X="27.021783203125" Y="2.300157226562" />
                  <Point X="27.02380078125" Y="2.289036376953" />
                  <Point X="27.02914453125" Y="2.267108154297" />
                  <Point X="27.032470703125" Y="2.25630078125" />
                  <Point X="27.04073828125" Y="2.234211181641" />
                  <Point X="27.045322265625" Y="2.223882568359" />
                  <Point X="27.05568359375" Y="2.203841064453" />
                  <Point X="27.0614609375" Y="2.194128173828" />
                  <Point X="27.084314453125" Y="2.160448242188" />
                  <Point X="27.104419921875" Y="2.130819580078" />
                  <Point X="27.109810546875" Y="2.123631347656" />
                  <Point X="27.13046484375" Y="2.100123291016" />
                  <Point X="27.15759765625" Y="2.075387695312" />
                  <Point X="27.1682578125" Y="2.066981445312" />
                  <Point X="27.2019375" Y="2.044128295898" />
                  <Point X="27.23156640625" Y="2.024023925781" />
                  <Point X="27.24128125" Y="2.018245239258" />
                  <Point X="27.261326171875" Y="2.007882324219" />
                  <Point X="27.27165625" Y="2.003298461914" />
                  <Point X="27.293744140625" Y="1.995032470703" />
                  <Point X="27.304548828125" Y="1.991707397461" />
                  <Point X="27.32647265625" Y="1.986364746094" />
                  <Point X="27.337591796875" Y="1.984346801758" />
                  <Point X="27.374525390625" Y="1.979893066406" />
                  <Point X="27.407015625" Y="1.975975219727" />
                  <Point X="27.416044921875" Y="1.975320922852" />
                  <Point X="27.44757421875" Y="1.975497314453" />
                  <Point X="27.4843125" Y="1.979822509766" />
                  <Point X="27.49774609375" Y="1.982395629883" />
                  <Point X="27.540458984375" Y="1.993817504883" />
                  <Point X="27.578033203125" Y="2.003865356445" />
                  <Point X="27.583994140625" Y="2.005670776367" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572753906" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.3344765625" Y="2.431117675781" />
                  <Point X="28.940404296875" Y="2.780950195312" />
                  <Point X="29.04396484375" Y="2.637026123047" />
                  <Point X="29.136884765625" Y="2.483471435547" />
                  <Point X="28.77645703125" Y="2.206904052734" />
                  <Point X="28.172953125" Y="1.743819946289" />
                  <Point X="28.168140625" Y="1.739869506836" />
                  <Point X="28.152119140625" Y="1.725216430664" />
                  <Point X="28.13466796875" Y="1.706602050781" />
                  <Point X="28.128578125" Y="1.699422363281" />
                  <Point X="28.097837890625" Y="1.659319824219" />
                  <Point X="28.070796875" Y="1.624040893555" />
                  <Point X="28.065638671875" Y="1.616603759766" />
                  <Point X="28.0497421875" Y="1.589374511719" />
                  <Point X="28.034765625" Y="1.555551269531" />
                  <Point X="28.030140625" Y="1.542675415039" />
                  <Point X="28.018689453125" Y="1.501730712891" />
                  <Point X="28.008615234375" Y="1.4657109375" />
                  <Point X="28.006224609375" Y="1.454661865234" />
                  <Point X="28.002771484375" Y="1.432363037109" />
                  <Point X="28.001708984375" Y="1.42111328125" />
                  <Point X="28.000892578125" Y="1.397541503906" />
                  <Point X="28.001173828125" Y="1.386236816406" />
                  <Point X="28.003078125" Y="1.36374987793" />
                  <Point X="28.004701171875" Y="1.352567749023" />
                  <Point X="28.0141015625" Y="1.307011352539" />
                  <Point X="28.02237109375" Y="1.266935058594" />
                  <Point X="28.02459765625" Y="1.258239013672" />
                  <Point X="28.034685546875" Y="1.228606933594" />
                  <Point X="28.0502890625" Y="1.195369873047" />
                  <Point X="28.056919921875" Y="1.183526611328" />
                  <Point X="28.082486328125" Y="1.144666748047" />
                  <Point X="28.1049765625" Y="1.110481201172" />
                  <Point X="28.11173828125" Y="1.101426269531" />
                  <Point X="28.126291015625" Y="1.084181884766" />
                  <Point X="28.13408203125" Y="1.075992675781" />
                  <Point X="28.151326171875" Y="1.05990234375" />
                  <Point X="28.160033203125" Y="1.052696411133" />
                  <Point X="28.178244140625" Y="1.039369750977" />
                  <Point X="28.187748046875" Y="1.033248657227" />
                  <Point X="28.224798828125" Y="1.012392944336" />
                  <Point X="28.257390625" Y="0.994046386719" />
                  <Point X="28.26548046875" Y="0.989987304688" />
                  <Point X="28.2946796875" Y="0.978084106445" />
                  <Point X="28.33027734375" Y="0.968021179199" />
                  <Point X="28.343671875" Y="0.965257629395" />
                  <Point X="28.393765625" Y="0.958637084961" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.444033203125" Y="0.952199829102" />
                  <Point X="28.46571875" Y="0.951222900391" />
                  <Point X="28.49121875" Y="0.952032287598" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="29.164076171875" Y="1.040145141602" />
                  <Point X="29.704703125" Y="1.111319702148" />
                  <Point X="29.70765625" Y="1.09918737793" />
                  <Point X="29.752689453125" Y="0.914208190918" />
                  <Point X="29.783873046875" Y="0.713921203613" />
                  <Point X="29.38362109375" Y="0.60667401123" />
                  <Point X="28.6919921875" Y="0.421352844238" />
                  <Point X="28.68603125" Y="0.419544250488" />
                  <Point X="28.665623046875" Y="0.412136138916" />
                  <Point X="28.642380859375" Y="0.401618377686" />
                  <Point X="28.634005859375" Y="0.397316497803" />
                  <Point X="28.584791015625" Y="0.36886932373" />
                  <Point X="28.54149609375" Y="0.343843902588" />
                  <Point X="28.533884765625" Y="0.338946472168" />
                  <Point X="28.50877734375" Y="0.319871917725" />
                  <Point X="28.48199609375" Y="0.294352172852" />
                  <Point X="28.472796875" Y="0.284227233887" />
                  <Point X="28.443267578125" Y="0.246600387573" />
                  <Point X="28.417291015625" Y="0.213499389648" />
                  <Point X="28.410853515625" Y="0.204205551147" />
                  <Point X="28.39912890625" Y="0.184923080444" />
                  <Point X="28.393841796875" Y="0.174934432983" />
                  <Point X="28.384068359375" Y="0.153470306396" />
                  <Point X="28.380005859375" Y="0.142927734375" />
                  <Point X="28.37316015625" Y="0.121429885864" />
                  <Point X="28.370376953125" Y="0.110474739075" />
                  <Point X="28.360533203125" Y="0.059078117371" />
                  <Point X="28.351875" Y="0.013864059448" />
                  <Point X="28.350603515625" Y="0.004966154575" />
                  <Point X="28.3485859375" Y="-0.026262638092" />
                  <Point X="28.35028125" Y="-0.062940391541" />
                  <Point X="28.351875" Y="-0.076424201965" />
                  <Point X="28.36171875" Y="-0.127820678711" />
                  <Point X="28.370376953125" Y="-0.173034744263" />
                  <Point X="28.37316015625" Y="-0.183989883423" />
                  <Point X="28.380005859375" Y="-0.205487731934" />
                  <Point X="28.384068359375" Y="-0.216030303955" />
                  <Point X="28.393841796875" Y="-0.237494415283" />
                  <Point X="28.39912890625" Y="-0.247482925415" />
                  <Point X="28.410853515625" Y="-0.266765411377" />
                  <Point X="28.417291015625" Y="-0.276059539795" />
                  <Point X="28.4468203125" Y="-0.313686401367" />
                  <Point X="28.472796875" Y="-0.346787109375" />
                  <Point X="28.478720703125" Y="-0.353635498047" />
                  <Point X="28.501140625" Y="-0.375805419922" />
                  <Point X="28.530177734375" Y="-0.398725006104" />
                  <Point X="28.54149609375" Y="-0.406404205322" />
                  <Point X="28.5907109375" Y="-0.434851226807" />
                  <Point X="28.634005859375" Y="-0.459876495361" />
                  <Point X="28.639494140625" Y="-0.462813415527" />
                  <Point X="28.65915625" Y="-0.472015869141" />
                  <Point X="28.68302734375" Y="-0.48102746582" />
                  <Point X="28.6919921875" Y="-0.483912841797" />
                  <Point X="29.30042578125" Y="-0.646942321777" />
                  <Point X="29.784876953125" Y="-0.776750549316" />
                  <Point X="29.761607421875" Y="-0.931085266113" />
                  <Point X="29.727802734375" Y="-1.079219726563" />
                  <Point X="29.238876953125" Y="-1.014851318359" />
                  <Point X="28.436783203125" Y="-0.909253601074" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042297363" />
                  <Point X="28.36672265625" Y="-0.910840881348" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.257890625" Y="-0.933670837402" />
                  <Point X="28.17291796875" Y="-0.952139892578" />
                  <Point X="28.157876953125" Y="-0.9567421875" />
                  <Point X="28.128755859375" Y="-0.968366699219" />
                  <Point X="28.11467578125" Y="-0.975389282227" />
                  <Point X="28.086849609375" Y="-0.992281494141" />
                  <Point X="28.074125" Y="-1.001530700684" />
                  <Point X="28.050375" Y="-1.02200189209" />
                  <Point X="28.039349609375" Y="-1.033223632812" />
                  <Point X="27.980966796875" Y="-1.103440795898" />
                  <Point X="27.92960546875" Y="-1.165211547852" />
                  <Point X="27.921326171875" Y="-1.176850341797" />
                  <Point X="27.906607421875" Y="-1.201231445312" />
                  <Point X="27.90016796875" Y="-1.213973632812" />
                  <Point X="27.88882421875" Y="-1.241359130859" />
                  <Point X="27.884365234375" Y="-1.254927612305" />
                  <Point X="27.877533203125" Y="-1.282578857422" />
                  <Point X="27.87516015625" Y="-1.296661621094" />
                  <Point X="27.86679296875" Y="-1.387595947266" />
                  <Point X="27.859431640625" Y="-1.467591918945" />
                  <Point X="27.859291015625" Y="-1.483315673828" />
                  <Point X="27.861607421875" Y="-1.514580688477" />
                  <Point X="27.864064453125" Y="-1.530121948242" />
                  <Point X="27.871794921875" Y="-1.561742919922" />
                  <Point X="27.87678515625" Y="-1.576666625977" />
                  <Point X="27.889158203125" Y="-1.60548059082" />
                  <Point X="27.896541015625" Y="-1.619370849609" />
                  <Point X="27.94999609375" Y="-1.702517333984" />
                  <Point X="27.997021484375" Y="-1.775661621094" />
                  <Point X="28.00174609375" Y="-1.782356201172" />
                  <Point X="28.01980078125" Y="-1.804455444336" />
                  <Point X="28.043494140625" Y="-1.828122070312" />
                  <Point X="28.052798828125" Y="-1.836277587891" />
                  <Point X="28.6174296875" Y="-2.269534423828" />
                  <Point X="29.087169921875" Y="-2.629980224609" />
                  <Point X="29.0454765625" Y="-2.6974453125" />
                  <Point X="29.0012734375" Y="-2.760251464844" />
                  <Point X="28.562828125" Y="-2.507114257812" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.81502734375" Y="-2.079513427734" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771107421875" Y="-2.066337646484" />
                  <Point X="27.6561484375" Y="-2.045575927734" />
                  <Point X="27.555017578125" Y="-2.027311889648" />
                  <Point X="27.539359375" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403442383" />
                  <Point X="27.492314453125" Y="-2.02650378418" />
                  <Point X="27.460140625" Y="-2.031461914062" />
                  <Point X="27.444845703125" Y="-2.03513659668" />
                  <Point X="27.4150703125" Y="-2.044960083008" />
                  <Point X="27.40058984375" Y="-2.051108886719" />
                  <Point X="27.3050859375" Y="-2.101371337891" />
                  <Point X="27.221072265625" Y="-2.145587890625" />
                  <Point X="27.20896875" Y="-2.153170654297" />
                  <Point X="27.186037109375" Y="-2.170064453125" />
                  <Point X="27.175208984375" Y="-2.179375488281" />
                  <Point X="27.15425" Y="-2.200334960938" />
                  <Point X="27.14494140625" Y="-2.211161132813" />
                  <Point X="27.128048828125" Y="-2.234090332031" />
                  <Point X="27.12046484375" Y="-2.246193359375" />
                  <Point X="27.070201171875" Y="-2.341696289062" />
                  <Point X="27.025984375" Y="-2.425711425781" />
                  <Point X="27.0198359375" Y="-2.440193603516" />
                  <Point X="27.01001171875" Y="-2.46997265625" />
                  <Point X="27.0063359375" Y="-2.48526953125" />
                  <Point X="27.00137890625" Y="-2.517443115234" />
                  <Point X="27.000279296875" Y="-2.533134277344" />
                  <Point X="27.00068359375" Y="-2.564484375" />
                  <Point X="27.0021875" Y="-2.580143310547" />
                  <Point X="27.02294921875" Y="-2.695102539062" />
                  <Point X="27.04121484375" Y="-2.796233642578" />
                  <Point X="27.04301953125" Y="-2.804228271484" />
                  <Point X="27.05123828125" Y="-2.831543701172" />
                  <Point X="27.064072265625" Y="-2.862480712891" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.4323828125" Y="-3.502021972656" />
                  <Point X="27.735896484375" Y="-4.027721679687" />
                  <Point X="27.7237578125" Y="-4.036083007813" />
                  <Point X="27.379048828125" Y="-3.586852783203" />
                  <Point X="26.83391796875" Y="-2.876422363281" />
                  <Point X="26.82865234375" Y="-2.870141113281" />
                  <Point X="26.80883203125" Y="-2.849625732422" />
                  <Point X="26.78325390625" Y="-2.828004638672" />
                  <Point X="26.77330078125" Y="-2.820646972656" />
                  <Point X="26.659919921875" Y="-2.747753417969" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.546283203125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663873535156" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.28439453125" Y="-2.657927001953" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079875" Y="-2.699414794922" />
                  <Point X="26.055494140625" Y="-2.714135742188" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="25.948111328125" Y="-2.802026855469" />
                  <Point X="25.863876953125" Y="-2.872063720703" />
                  <Point X="25.852654296875" Y="-2.883087646484" />
                  <Point X="25.83218359375" Y="-2.906837158203" />
                  <Point X="25.822935546875" Y="-2.919562744141" />
                  <Point X="25.80604296875" Y="-2.947389404297" />
                  <Point X="25.79901953125" Y="-2.961466552734" />
                  <Point X="25.78739453125" Y="-2.990587402344" />
                  <Point X="25.78279296875" Y="-3.005631103516" />
                  <Point X="25.7541640625" Y="-3.137346923828" />
                  <Point X="25.728978515625" Y="-3.253218994141" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323170166016" />
                  <Point X="25.7255546875" Y="-3.335520507812" />
                  <Point X="25.82837890625" Y="-4.116544433594" />
                  <Point X="25.83308984375" Y="-4.152319824219" />
                  <Point X="25.655068359375" Y="-3.487937011719" />
                  <Point X="25.652607421875" Y="-3.480122314453" />
                  <Point X="25.642146484375" Y="-3.453578857422" />
                  <Point X="25.6267890625" Y="-3.423815429688" />
                  <Point X="25.62041015625" Y="-3.413209228516" />
                  <Point X="25.533306640625" Y="-3.2877109375" />
                  <Point X="25.456681640625" Y="-3.177308837891" />
                  <Point X="25.446671875" Y="-3.165171386719" />
                  <Point X="25.424787109375" Y="-3.142715820312" />
                  <Point X="25.412912109375" Y="-3.132397705078" />
                  <Point X="25.38665625" Y="-3.113153808594" />
                  <Point X="25.3732421875" Y="-3.104936767578" />
                  <Point X="25.3452421875" Y="-3.090829345703" />
                  <Point X="25.33065625" Y="-3.084938964844" />
                  <Point X="25.19587109375" Y="-3.043106201172" />
                  <Point X="25.077296875" Y="-3.006305664062" />
                  <Point X="25.063376953125" Y="-3.003109619141" />
                  <Point X="25.035216796875" Y="-2.998840087891" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.998840087891" />
                  <Point X="24.948935546875" Y="-3.003109619141" />
                  <Point X="24.935015625" Y="-3.006305664062" />
                  <Point X="24.80023046875" Y="-3.048138183594" />
                  <Point X="24.68165625" Y="-3.084938964844" />
                  <Point X="24.6670703125" Y="-3.090829345703" />
                  <Point X="24.6390703125" Y="-3.104936767578" />
                  <Point X="24.62565625" Y="-3.113153808594" />
                  <Point X="24.599400390625" Y="-3.132397705078" />
                  <Point X="24.587525390625" Y="-3.142717041016" />
                  <Point X="24.565640625" Y="-3.165173339844" />
                  <Point X="24.555630859375" Y="-3.177310302734" />
                  <Point X="24.468529296875" Y="-3.302808349609" />
                  <Point X="24.391904296875" Y="-3.413210693359" />
                  <Point X="24.387529296875" Y="-3.420132568359" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213378906" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.169771484375" Y="-4.18759765625" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.301107341275" Y="-0.631193214796" />
                  <Point X="20.403583533224" Y="-1.278202426934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.393375745603" Y="-0.606469936337" />
                  <Point X="20.497803081038" Y="-1.265798183613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.255713602767" Y="0.86997768255" />
                  <Point X="20.303545327188" Y="0.567980060038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.485644149931" Y="-0.581746657878" />
                  <Point X="20.592022628852" Y="-1.253393940292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.312445215214" Y="1.119071434574" />
                  <Point X="20.403992393168" Y="0.541065301103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.577912554259" Y="-0.557023379419" />
                  <Point X="20.686242176665" Y="-1.240989696972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.870401778006" Y="-2.403727658876" />
                  <Point X="20.897636406764" Y="-2.575680337448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.377311904364" Y="1.31680233374" />
                  <Point X="20.504439459148" Y="0.514150542168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.670180958587" Y="-0.53230010096" />
                  <Point X="20.780461724479" Y="-1.228585453651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.956163137257" Y="-2.337920514706" />
                  <Point X="21.026234779517" Y="-2.780335452159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.4755444048" Y="1.303869791366" />
                  <Point X="20.604886525128" Y="0.487235783233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.762449366912" Y="-0.507576847732" />
                  <Point X="20.874681272293" Y="-1.216181210331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.041924496508" Y="-2.272113370537" />
                  <Point X="21.147692517387" Y="-2.939906372567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.573776905236" Y="1.290937248992" />
                  <Point X="20.705333591108" Y="0.460321024299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.854717777375" Y="-0.482853608013" />
                  <Point X="20.968900820107" Y="-1.20377696701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.127685855759" Y="-2.206306226367" />
                  <Point X="21.252069926328" Y="-2.991636340321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.672009405813" Y="1.278004705727" />
                  <Point X="20.805780656284" Y="0.433406270447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.946986187839" Y="-0.458130368294" />
                  <Point X="21.063120367617" Y="-1.191372721768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.21344721501" Y="-2.140499082197" />
                  <Point X="21.340195600424" Y="-2.940756892585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.770241907121" Y="1.265072157847" />
                  <Point X="20.906227712723" Y="0.406491571753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.039254598303" Y="-0.433407128574" />
                  <Point X="21.157339915016" Y="-1.178968475833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.29920858359" Y="-2.074691996927" />
                  <Point X="21.42832127452" Y="-2.889877444848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.868474408428" Y="1.252139609967" />
                  <Point X="21.006674769162" Y="0.379576873059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.131523008767" Y="-0.408683888855" />
                  <Point X="21.251559462416" Y="-1.166564229898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.38496996271" Y="-2.008884978203" />
                  <Point X="21.516446948616" Y="-2.838997997112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.793791311082" Y="2.330953185" />
                  <Point X="20.803364329477" Y="2.27051152561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.966706909736" Y="1.239207062087" />
                  <Point X="21.107121825601" Y="0.352662174365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.223791419231" Y="-0.383960649136" />
                  <Point X="21.345779009816" Y="-1.154159983963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.47073134183" Y="-1.943077959478" />
                  <Point X="21.604572622712" Y="-2.788118549375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.869446331055" Y="2.460569244095" />
                  <Point X="20.912855244717" Y="2.186496149712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.064939411044" Y="1.226274514207" />
                  <Point X="21.20756888204" Y="0.32574747567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.316059829695" Y="-0.359237409417" />
                  <Point X="21.439998557216" Y="-1.141755738028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.55649272095" Y="-1.877270940754" />
                  <Point X="21.692698296808" Y="-2.737239101639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.945101601958" Y="2.590183718884" />
                  <Point X="21.022346159957" Y="2.102480773813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.163171912352" Y="1.213341966327" />
                  <Point X="21.308015938479" Y="0.298832776976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.408328240159" Y="-0.334514169697" />
                  <Point X="21.534218104616" Y="-1.129351492093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.64225410007" Y="-1.81146392203" />
                  <Point X="21.780823993573" Y="-2.686359797023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.022054246166" Y="2.711606900996" />
                  <Point X="21.131837129549" Y="2.018465054751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.260737597358" Y="1.204619530881" />
                  <Point X="21.408462994918" Y="0.271918078282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.500501577665" Y="-0.309190662948" />
                  <Point X="21.628437652015" Y="-1.116947246158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.728015479189" Y="-1.745656903305" />
                  <Point X="21.868949695824" Y="-2.635480527051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.049938327483" Y="-3.778197774331" />
                  <Point X="22.063839547587" Y="-3.865966623815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.101969260361" Y="2.81432641512" />
                  <Point X="21.241328124718" Y="1.934449174203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.353909267965" Y="1.223639810501" />
                  <Point X="21.509183477462" Y="0.243277035105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.589066983039" Y="-0.261087569233" />
                  <Point X="21.722657199415" Y="-1.104543000223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.813776858309" Y="-1.679849884581" />
                  <Point X="21.957075398075" Y="-2.584601257079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.12541661061" Y="-3.647465842706" />
                  <Point X="22.173382593618" Y="-3.950311140576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.181884274556" Y="2.917045929244" />
                  <Point X="21.350819119887" Y="1.850433293654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.444036242887" Y="1.261883542121" />
                  <Point X="21.615886976746" Y="0.176860710917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.670601507972" Y="-0.168593243485" />
                  <Point X="21.818504568909" Y="-1.102416418499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.899538237429" Y="-1.614042865857" />
                  <Point X="22.045201100326" Y="-2.533721987107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.200894893737" Y="-3.516733911082" />
                  <Point X="22.282117491058" Y="-4.029553207941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.261799315437" Y="3.01976527488" />
                  <Point X="21.460386547938" Y="1.765934834884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.526093025952" Y="1.351080459798" />
                  <Point X="21.920347283135" Y="-1.138142953658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.984178644443" Y="-1.541158307801" />
                  <Point X="22.133326802578" Y="-2.482842717135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.276373174916" Y="-3.386001967154" />
                  <Point X="22.388759854624" Y="-4.095583536395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.363812115542" Y="2.982964859743" />
                  <Point X="22.037528447191" Y="-1.270712649667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.051366062942" Y="-1.358079917074" />
                  <Point X="22.221452504829" Y="-2.431963447163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.351851447459" Y="-3.255269968709" />
                  <Point X="22.495402201856" Y="-4.161613761712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.469676944682" Y="2.92184369045" />
                  <Point X="22.30957820708" Y="-2.381084177192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.427329720003" Y="-3.124537970265" />
                  <Point X="22.575482182804" Y="-4.059935806679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.575541761548" Y="2.860722598658" />
                  <Point X="22.401028798014" Y="-2.351197428173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.502807992547" Y="-2.99380597182" />
                  <Point X="22.658989916708" Y="-3.979899832058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.681406578413" Y="2.799601506866" />
                  <Point X="22.498241350038" Y="-2.357690269723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.578286265091" Y="-2.863073973376" />
                  <Point X="22.745969067289" Y="-3.92178151974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.78624883198" Y="2.744936625649" />
                  <Point X="22.603980954779" Y="-2.418020803273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.653764537634" Y="-2.732341974931" />
                  <Point X="22.832948253191" Y="-3.863663430432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.885173007972" Y="2.72763701568" />
                  <Point X="22.920209983538" Y="-3.807329256541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.981637612054" Y="2.72586653159" />
                  <Point X="23.011861910618" Y="-3.778713693922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.072890518914" Y="2.757001408729" />
                  <Point X="23.108163456699" Y="-3.779454670314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.011164593195" Y="3.754006621774" />
                  <Point X="22.049641967067" Y="3.511070044205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.157585770408" Y="2.829539692366" />
                  <Point X="23.205356595921" Y="-3.78582494425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.096933594271" Y="3.819765517358" />
                  <Point X="22.182187089867" Y="3.281496130405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.236353397734" Y="2.939503522069" />
                  <Point X="23.302549758941" Y="-3.792195368446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.182702681228" Y="3.885523870713" />
                  <Point X="23.400536419279" Y="-3.803575737528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.268471768185" Y="3.951282224067" />
                  <Point X="23.504788267337" Y="-3.854512945069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.354321007179" Y="4.01653451738" />
                  <Point X="23.616464036375" Y="-3.952322944944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.44272600464" Y="4.065650386797" />
                  <Point X="23.728163224809" Y="-4.050280809064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.531131002101" Y="4.114766256214" />
                  <Point X="23.935335352457" Y="-4.751031087757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.619535999563" Y="4.163882125631" />
                  <Point X="24.02679390432" Y="-4.72119460207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.707941005467" Y="4.21299794174" />
                  <Point X="24.087245439782" Y="-4.495587519615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.796346033063" Y="4.262113620892" />
                  <Point X="24.147696975244" Y="-4.26998043716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.911781502132" Y="4.140565809253" />
                  <Point X="24.208148514034" Y="-4.04437337572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.018301274089" Y="4.075309493759" />
                  <Point X="24.268600054739" Y="-3.818766326366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.121053415463" Y="4.033841061569" />
                  <Point X="24.329051595444" Y="-3.593159277013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.217090815028" Y="4.034767840639" />
                  <Point X="24.395837223221" Y="-3.407544079507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.308151872737" Y="4.067114005641" />
                  <Point X="24.47415018925" Y="-3.294709631349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.397553387433" Y="4.109938112862" />
                  <Point X="24.55246305528" Y="-3.181874551819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.479294136246" Y="4.201130392275" />
                  <Point X="24.63669222023" Y="-3.106393513565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.533134852364" Y="4.468476545377" />
                  <Point X="24.727237723418" Y="-3.070792265418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.610420129139" Y="4.58779956812" />
                  <Point X="24.818915364767" Y="-3.042339056306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.702514795089" Y="4.613619787525" />
                  <Point X="24.91059296525" Y="-3.013885589177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.794609483369" Y="4.639439865948" />
                  <Point X="25.004224155368" Y="-2.997766601562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.886704180299" Y="4.665259889754" />
                  <Point X="25.103025513512" Y="-3.014290770153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.978798877229" Y="4.691079913559" />
                  <Point X="25.204182165256" Y="-3.045685677279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.070946081633" Y="4.716568418225" />
                  <Point X="25.305338934212" Y="-3.07708132445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.165379781417" Y="4.727620559217" />
                  <Point X="25.409939270634" Y="-3.130218800926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.259813481202" Y="4.738672700209" />
                  <Point X="25.530405779256" Y="-3.283531346169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.354247180986" Y="4.749724841201" />
                  <Point X="25.664596976838" Y="-3.523498167112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.448680880771" Y="4.760776982193" />
                  <Point X="25.724727857715" Y="-3.295866551284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.543114602801" Y="4.771828982733" />
                  <Point X="25.778252142442" Y="-3.026522529013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.64167973066" Y="4.756796313457" />
                  <Point X="25.851863742532" Y="-2.884004824537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.832173337401" Y="4.161350071403" />
                  <Point X="25.936582160529" Y="-2.811612808439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.938692558101" Y="4.096097236409" />
                  <Point X="26.021573654415" Y="-2.740944925658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.036234583095" Y="4.087524184403" />
                  <Point X="26.109079944283" Y="-2.686154839811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.127472687176" Y="4.118752522609" />
                  <Point X="26.201994255697" Y="-2.665509658192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.210419297224" Y="4.202331293822" />
                  <Point X="26.296796709283" Y="-2.656785737075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.275197278499" Y="4.400622272472" />
                  <Point X="26.391599174351" Y="-2.648061888449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.335648774609" Y="4.626229603384" />
                  <Point X="26.488971720257" Y="-2.65556489161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.407165760915" Y="4.781972178812" />
                  <Point X="26.592936558563" Y="-2.704689990896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.504972264028" Y="4.771729277681" />
                  <Point X="26.700025102684" Y="-2.773537392501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.602778782419" Y="4.761486280084" />
                  <Point X="26.808172153109" Y="-2.849067939888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.700585306201" Y="4.751243248455" />
                  <Point X="26.928136647251" Y="-2.999210890441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.798391829983" Y="4.741000216826" />
                  <Point X="27.049338192795" Y="-3.157164276156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.89773641209" Y="4.721046267111" />
                  <Point X="27.028818670818" Y="-2.420326057154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.112347141519" Y="-2.947704065556" />
                  <Point X="27.170539738338" Y="-3.31511766187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.997744829238" Y="4.696901027908" />
                  <Point X="27.102753069002" Y="-2.279846419632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.244892464498" Y="-3.177279243237" />
                  <Point X="27.291741283882" Y="-3.473071047585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.097753246386" Y="4.672755788704" />
                  <Point X="27.182087423246" Y="-2.173460762865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.377437787477" Y="-3.406854420918" />
                  <Point X="27.412942739785" Y="-3.631023867329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.19776166929" Y="4.648610513155" />
                  <Point X="27.26979550759" Y="-2.119944757196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.509983039857" Y="-3.63642915285" />
                  <Point X="27.534143964782" Y="-3.788975229192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.297770101851" Y="4.624465176633" />
                  <Point X="27.358579004537" Y="-2.073218639477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.642528242248" Y="-3.866003569169" />
                  <Point X="27.655345189779" Y="-3.946926591054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.397778534413" Y="4.60031984011" />
                  <Point X="27.448589140674" Y="-2.034237216807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.498687455062" Y="4.570489045556" />
                  <Point X="27.543501217772" Y="-2.026205431302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.600733855648" Y="4.533476485337" />
                  <Point X="27.642358956692" Y="-2.043085574105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.702780281037" Y="4.496463768509" />
                  <Point X="27.023250318375" Y="2.473095584861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.069253271352" Y="2.182644370821" />
                  <Point X="27.74137544463" Y="-2.060968018762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.804826755201" Y="4.459450743733" />
                  <Point X="27.092184582499" Y="2.645144826377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.185622906762" Y="2.055198465029" />
                  <Point X="27.842397506231" Y="-2.091513157167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.907746181512" Y="4.416926116011" />
                  <Point X="27.167662838348" Y="2.775876930224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.291184749555" Y="1.995990276247" />
                  <Point X="27.948203081007" Y="-2.152260209127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.011624736555" Y="4.36834578781" />
                  <Point X="27.243141094198" Y="2.90660903407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.390218198585" Y="1.978000743475" />
                  <Point X="27.896991745698" Y="-1.221641507209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.980725264013" Y="-1.750314135296" />
                  <Point X="28.054067899332" Y="-2.213381310135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.115503333555" Y="4.319765194702" />
                  <Point X="27.318619350048" Y="3.037341137917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.486060806773" Y="1.980157386896" />
                  <Point X="27.975496739182" Y="-1.110019472683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.095743964891" Y="-1.86923057614" />
                  <Point X="28.159932717657" Y="-2.274502411143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.219381979504" Y="4.271184292545" />
                  <Point X="27.394097605897" Y="3.168073241763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.578469109495" Y="2.003997381659" />
                  <Point X="28.056855589492" Y="-1.01641598102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.205234884476" Y="-1.95324597947" />
                  <Point X="28.265797535982" Y="-2.335623512151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.324720620494" Y="4.213385344481" />
                  <Point X="27.469575861747" Y="3.29880534561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.667953154419" Y="2.046300413525" />
                  <Point X="28.144438033573" Y="-0.962106713955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.314725804061" Y="-2.037261382801" />
                  <Point X="28.371662354307" Y="-2.396744613159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.430682381172" Y="4.151652173545" />
                  <Point X="27.545054099018" Y="3.429537566758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.756078842072" Y="2.097179775668" />
                  <Point X="28.236842991664" Y="-0.938245602021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.424216723646" Y="-2.121276786131" />
                  <Point X="28.477527172632" Y="-2.457865714167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.53664432121" Y="4.089917870172" />
                  <Point X="27.62053228922" Y="3.56027008509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.844204529724" Y="2.148059137812" />
                  <Point X="28.329826188126" Y="-0.91803534348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.533707643231" Y="-2.205292189462" />
                  <Point X="28.583391995916" Y="-2.518986846482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.642908707931" Y="4.0262739936" />
                  <Point X="27.696010479421" Y="3.691002603422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.932330217377" Y="2.198938499956" />
                  <Point X="28.034395809311" Y="1.554521714288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.104645124796" Y="1.110984992239" />
                  <Point X="28.42449444842" Y="-0.908464159257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.643198574282" Y="-2.28930766519" />
                  <Point X="28.689256839767" Y="-2.580108108659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.751301411111" Y="3.94919245576" />
                  <Point X="27.771488669623" Y="3.821735121753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.02045590503" Y="2.249817862099" />
                  <Point X="28.111217152327" Y="1.676773899503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.215627059429" Y="1.017555690388" />
                  <Point X="28.364310940906" Y="0.078802608507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.421330900451" Y="-0.281207247434" />
                  <Point X="28.522592951633" Y="-0.920550676464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.752689542588" Y="-2.373323376132" />
                  <Point X="28.795121683619" Y="-2.641229370835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.108581592682" Y="2.300697224243" />
                  <Point X="28.194200092374" Y="1.760123292132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.319158938726" Y="0.971164186707" />
                  <Point X="28.435488559896" Y="0.236687864842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.536844476909" Y="-0.40324820972" />
                  <Point X="28.620825455376" Y="-0.933483239719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.862180510894" Y="-2.457339087075" />
                  <Point X="28.900986527471" Y="-2.702350633012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.196707280335" Y="2.351576586387" />
                  <Point X="28.279961485824" Y="1.825930220376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.417830963375" Y="0.955456597661" />
                  <Point X="28.517453684971" Y="0.32646348829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.642700552609" Y="-0.464314111968" />
                  <Point X="28.719057959119" Y="-0.946415802974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.9716714792" Y="-2.541354798018" />
                  <Point X="29.00541033381" Y="-2.754373542414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.284832967988" Y="2.40245594853" />
                  <Point X="28.365722879275" Y="1.89173714862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.514153843219" Y="0.95458092519" />
                  <Point X="28.605065344618" Y="0.380588295531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.74420472289" Y="-0.497903164785" />
                  <Point X="28.817290462862" Y="-0.959348366229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.081162447506" Y="-2.62537050896" />
                  <Point X="29.082969152663" Y="-2.636777596381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.372958654101" Y="2.453335320391" />
                  <Point X="28.451484272725" Y="1.957544076864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.60837338457" Y="0.966985209319" />
                  <Point X="28.694679034433" Y="0.422072781772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.844651785997" Y="-0.524817905575" />
                  <Point X="28.915522966605" Y="-0.972280929485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.46108433823" Y="2.504214704788" />
                  <Point X="28.537245666176" Y="2.023351005107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.70259292592" Y="0.979389493448" />
                  <Point X="28.786947448794" Y="0.446795996883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.945098849103" Y="-0.551732646365" />
                  <Point X="29.013755470348" Y="-0.98521349274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.549210022358" Y="2.555094089185" />
                  <Point X="28.623007059626" Y="2.089157933351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.796812467271" Y="0.991793777576" />
                  <Point X="28.879215863156" Y="0.471519211994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.04554591221" Y="-0.578647387155" />
                  <Point X="29.111987974091" Y="-0.998146055995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.637335706486" Y="2.605973473582" />
                  <Point X="28.708768453077" Y="2.154964861595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.891032008621" Y="1.004198061705" />
                  <Point X="28.971484277517" Y="0.496242427104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.145992975316" Y="-0.605562127945" />
                  <Point X="29.210220477834" Y="-1.01107861925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.725461390614" Y="2.656852857979" />
                  <Point X="28.794529833145" Y="2.220771874335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.985251549972" Y="1.016602345834" />
                  <Point X="29.063752691879" Y="0.520965642215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.246440038422" Y="-0.632476868735" />
                  <Point X="29.308452983054" Y="-1.024011191836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.813587074742" Y="2.707732242375" />
                  <Point X="28.88029116309" Y="2.286579203536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.079471091322" Y="1.029006629963" />
                  <Point X="29.15602110624" Y="0.545688857326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.346887097976" Y="-0.659391587098" />
                  <Point X="29.406685488884" Y="-1.036943768265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.90171275887" Y="2.758611626772" />
                  <Point X="28.966052493035" Y="2.352386532738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.173690634309" Y="1.041410903759" />
                  <Point X="29.248289520602" Y="0.570412072436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.447334153403" Y="-0.6863062794" />
                  <Point X="29.504917994713" Y="-1.049876344694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.009587107997" Y="2.684802847622" />
                  <Point X="29.05181382298" Y="2.418193861939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.267910191697" Y="1.053815086631" />
                  <Point X="29.340557934964" Y="0.595135287547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.54778120883" Y="-0.713220971702" />
                  <Point X="29.603150500543" Y="-1.062808921122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.362129749085" Y="1.066219269503" />
                  <Point X="29.432826346643" Y="0.619858519593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.648228264256" Y="-0.740135664004" />
                  <Point X="29.701383006372" Y="-1.075741497551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.456349306473" Y="1.078623452375" />
                  <Point X="29.525094755974" Y="0.644581766461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.748675319683" Y="-0.767050356306" />
                  <Point X="29.767971036297" Y="-0.888878716307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.550568863861" Y="1.091027635247" />
                  <Point X="29.617363165306" Y="0.669305013329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.644788421249" Y="1.103431818118" />
                  <Point X="29.709631574638" Y="0.694028260197" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.6846484375" Y="-4.332440917969" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.377216796875" Y="-3.396045898438" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.13955078125" Y="-3.224567138672" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.85655078125" Y="-3.229599121094" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643798828" />
                  <Point X="24.624619140625" Y="-3.411141845703" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.353298828125" Y="-4.2367734375" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.03855859375" Y="-4.9650078125" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.743904296875" Y="-4.897966308594" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.665197265625" Y="-4.745893554688" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.6581171875" Y="-4.372875488281" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.489623046875" Y="-4.093801269531" />
                  <Point X="23.38045703125" Y="-3.998064208984" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.18605859375" Y="-3.974967773438" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791015625" />
                  <Point X="22.872884765625" Y="-4.065489990234" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157293945313" />
                  <Point X="22.635015625" Y="-4.2944609375" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.347625" Y="-4.293586914062" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.928359375" Y="-4.001446289062" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.0979296875" Y="-3.315074951172" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597593261719" />
                  <Point X="22.486021484375" Y="-2.568764404297" />
                  <Point X="22.468673828125" Y="-2.551415771484" />
                  <Point X="22.43984375" Y="-2.537198730469" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.804873046875" Y="-2.891868164062" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="20.9990390625" Y="-3.058312011719" />
                  <Point X="20.838302734375" Y="-2.847135742188" />
                  <Point X="20.683580078125" Y="-2.587692382812" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="21.14327734375" Y="-1.954852905273" />
                  <Point X="21.836212890625" Y="-1.423144897461" />
                  <Point X="21.854177734375" Y="-1.396012573242" />
                  <Point X="21.8618828125" Y="-1.366265258789" />
                  <Point X="21.859673828125" Y="-1.334594726562" />
                  <Point X="21.838841796875" Y="-1.310638671875" />
                  <Point X="21.812359375" Y="-1.295052124023" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.018681640625" Y="-1.388862670898" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.1354765625" Y="-1.257315673828" />
                  <Point X="20.072607421875" Y="-1.011187805176" />
                  <Point X="20.031673828125" Y="-0.724983154297" />
                  <Point X="20.00160546875" Y="-0.514742248535" />
                  <Point X="20.6531796875" Y="-0.340153167725" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.47287109375" Y="-0.111177429199" />
                  <Point X="21.49767578125" Y="-0.090645599365" />
                  <Point X="21.510025390625" Y="-0.060046287537" />
                  <Point X="21.516599609375" Y="-0.03127722168" />
                  <Point X="21.509431640625" Y="-0.000606403053" />
                  <Point X="21.4976796875" Y="0.028083265305" />
                  <Point X="21.47109375" Y="0.049850105286" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.74812890625" Y="0.252151504517" />
                  <Point X="20.001814453125" Y="0.452126068115" />
                  <Point X="20.041837890625" Y="0.722608642578" />
                  <Point X="20.08235546875" Y="0.996414978027" />
                  <Point X="20.164755859375" Y="1.300498413086" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.680904296875" Y="1.468473266602" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.300974609375" Y="1.406169921875" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.348466796875" Y="1.426056152344" />
                  <Point X="21.360880859375" Y="1.443785888672" />
                  <Point X="21.3739921875" Y="1.475441894531" />
                  <Point X="21.38552734375" Y="1.503290161133" />
                  <Point X="21.389287109375" Y="1.524606445312" />
                  <Point X="21.38368359375" Y="1.545513793945" />
                  <Point X="21.36786328125" Y="1.575906616211" />
                  <Point X="21.353943359375" Y="1.602643432617" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="20.94171875" Y="1.92485925293" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.68255078125" Y="2.517283447266" />
                  <Point X="20.83998828125" Y="2.787007568359" />
                  <Point X="21.058259765625" Y="3.067566162109" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.493326171875" Y="3.127582763672" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.906998046875" Y="2.916453125" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775146484" />
                  <Point X="21.986748046875" Y="2.927403808594" />
                  <Point X="22.019052734375" Y="2.959708496094" />
                  <Point X="22.04747265625" Y="2.988127197266" />
                  <Point X="22.0591015625" Y="3.006381347656" />
                  <Point X="22.061927734375" Y="3.027840087891" />
                  <Point X="22.0579453125" Y="3.073352050781" />
                  <Point X="22.054443359375" Y="3.113389648438" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.871427734375" Y="3.439748779297" />
                  <Point X="21.69272265625" Y="3.749276611328" />
                  <Point X="21.972923828125" Y="3.964105224609" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.5908984375" Y="4.365324707031" />
                  <Point X="22.858451171875" Y="4.513971679688" />
                  <Point X="22.925787109375" Y="4.426217285156" />
                  <Point X="23.032173828125" Y="4.287572753906" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.099408203125" Y="4.247291503906" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.238953125" Y="4.244104980469" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.303095703125" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.29448828125" />
                  <Point X="23.33108984375" Y="4.348952148438" />
                  <Point X="23.346197265625" Y="4.396864746094" />
                  <Point X="23.348083984375" Y="4.418426757812" />
                  <Point X="23.328017578125" Y="4.570848144531" />
                  <Point X="23.31086328125" Y="4.701141113281" />
                  <Point X="23.6769375" Y="4.803775390625" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.4486640625" Y="4.952071777344" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.8550546875" Y="4.694578125" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.144892578125" Y="4.648427246094" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.550267578125" Y="4.958024902344" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.205001953125" Y="4.842321289062" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.7331640625" Y="4.687555664062" />
                  <Point X="26.931029296875" Y="4.615788085938" />
                  <Point X="27.148046875" Y="4.514296875" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.54834375" Y="4.30299609375" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="27.93024609375" Y="4.055083740234" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.68823828125" Y="3.297539794922" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491515869141" />
                  <Point X="27.213431640625" Y="2.448803955078" />
                  <Point X="27.2033828125" Y="2.411229492188" />
                  <Point X="27.202044921875" Y="2.392325683594" />
                  <Point X="27.206498046875" Y="2.355392089844" />
                  <Point X="27.210416015625" Y="2.322900878906" />
                  <Point X="27.21868359375" Y="2.300811279297" />
                  <Point X="27.241537109375" Y="2.267131347656" />
                  <Point X="27.261642578125" Y="2.237502685547" />
                  <Point X="27.27494140625" Y="2.224203613281" />
                  <Point X="27.30862109375" Y="2.201350341797" />
                  <Point X="27.33825" Y="2.18124609375" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.397271484375" Y="2.168526611328" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4486640625" Y="2.165946289062" />
                  <Point X="27.491376953125" Y="2.177368164062" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.2394765625" Y="2.595662597656" />
                  <Point X="28.994248046875" Y="3.031430419922" />
                  <Point X="29.093341796875" Y="2.893712890625" />
                  <Point X="29.20259765625" Y="2.741874267578" />
                  <Point X="29.312818359375" Y="2.559730712891" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.89212109375" Y="2.056167236328" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583832275391" />
                  <Point X="28.248630859375" Y="1.543729736328" />
                  <Point X="28.22158984375" Y="1.508450927734" />
                  <Point X="28.21312109375" Y="1.491501220703" />
                  <Point X="28.201669921875" Y="1.450556396484" />
                  <Point X="28.191595703125" Y="1.414536621094" />
                  <Point X="28.190779296875" Y="1.39096484375" />
                  <Point X="28.2001796875" Y="1.345408447266" />
                  <Point X="28.20844921875" Y="1.30533215332" />
                  <Point X="28.215646484375" Y="1.287955810547" />
                  <Point X="28.241212890625" Y="1.249095947266" />
                  <Point X="28.263703125" Y="1.214910400391" />
                  <Point X="28.280947265625" Y="1.19882019043" />
                  <Point X="28.317998046875" Y="1.177964599609" />
                  <Point X="28.35058984375" Y="1.159617797852" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.41866015625" Y="1.146999145508" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.139275390625" Y="1.22851965332" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.892265625" Y="1.144127441406" />
                  <Point X="29.939193359375" Y="0.951366516113" />
                  <Point X="29.97392578125" Y="0.728285766602" />
                  <Point X="29.997859375" Y="0.57455645752" />
                  <Point X="29.432796875" Y="0.423148132324" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819351196" />
                  <Point X="28.679873046875" Y="0.204372238159" />
                  <Point X="28.636578125" Y="0.179346832275" />
                  <Point X="28.622265625" Y="0.166926239014" />
                  <Point X="28.592736328125" Y="0.129299362183" />
                  <Point X="28.566759765625" Y="0.096198562622" />
                  <Point X="28.556986328125" Y="0.074734344482" />
                  <Point X="28.547142578125" Y="0.023337913513" />
                  <Point X="28.538484375" Y="-0.021876211166" />
                  <Point X="28.538484375" Y="-0.040683864594" />
                  <Point X="28.548328125" Y="-0.092080299377" />
                  <Point X="28.556986328125" Y="-0.137294433594" />
                  <Point X="28.566759765625" Y="-0.158758636475" />
                  <Point X="28.5962890625" Y="-0.196385513306" />
                  <Point X="28.622265625" Y="-0.229486312866" />
                  <Point X="28.636578125" Y="-0.241906906128" />
                  <Point X="28.68579296875" Y="-0.270354034424" />
                  <Point X="28.729087890625" Y="-0.295379425049" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.3496015625" Y="-0.463416473389" />
                  <Point X="29.998068359375" Y="-0.637172546387" />
                  <Point X="29.9746328125" Y="-0.792624816895" />
                  <Point X="29.948431640625" Y="-0.966413574219" />
                  <Point X="29.903931640625" Y="-1.161412963867" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="29.214076171875" Y="-1.203225830078" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.29824609375" Y="-1.11933581543" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.185447265625" Y="-1.154697021484" />
                  <Point X="28.127064453125" Y="-1.22491418457" />
                  <Point X="28.075703125" Y="-1.286685058594" />
                  <Point X="28.064359375" Y="-1.314070556641" />
                  <Point X="28.0559921875" Y="-1.405004882813" />
                  <Point X="28.048630859375" Y="-1.485000854492" />
                  <Point X="28.056361328125" Y="-1.516621826172" />
                  <Point X="28.10981640625" Y="-1.599768066406" />
                  <Point X="28.156841796875" Y="-1.672912475586" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="28.73309375" Y="-2.118797119141" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.277986328125" Y="-2.682635253906" />
                  <Point X="29.204130859375" Y="-2.802142822266" />
                  <Point X="29.112099609375" Y="-2.932906982422" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.467828125" Y="-2.671659179688" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.6223828125" Y="-2.232551269531" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219245117188" />
                  <Point X="27.39357421875" Y="-2.269507568359" />
                  <Point X="27.309560546875" Y="-2.313724121094" />
                  <Point X="27.2886015625" Y="-2.33468359375" />
                  <Point X="27.238337890625" Y="-2.430186523438" />
                  <Point X="27.19412109375" Y="-2.514201660156" />
                  <Point X="27.1891640625" Y="-2.546375244141" />
                  <Point X="27.20992578125" Y="-2.661334472656" />
                  <Point X="27.22819140625" Y="-2.762465576172" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.59692578125" Y="-3.407021972656" />
                  <Point X="27.98667578125" Y="-4.082087646484" />
                  <Point X="27.922931640625" Y="-4.1276171875" />
                  <Point X="27.83529296875" Y="-4.190215820312" />
                  <Point X="27.732400390625" Y="-4.25681640625" />
                  <Point X="27.67977734375" Y="-4.29087890625" />
                  <Point X="27.228310546875" Y="-3.702517822266" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.557169921875" Y="-2.907573730469" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.3018046875" Y="-2.847127685547" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.069583984375" Y="-2.948122558594" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045986083984" />
                  <Point X="25.939828125" Y="-3.177701904297" />
                  <Point X="25.914642578125" Y="-3.293573974609" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="26.01675390625" Y="-4.091744628906" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.07725390625" Y="-4.945073730469" />
                  <Point X="25.994349609375" Y="-4.96324609375" />
                  <Point X="25.8992890625" Y="-4.980515625" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#170" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.102805347483" Y="4.73879927325" Z="1.35" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.35" />
                  <Point X="-0.563304405554" Y="5.033012932527" Z="1.35" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.35" />
                  <Point X="-1.342716655857" Y="4.883199221201" Z="1.35" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.35" />
                  <Point X="-1.727712534092" Y="4.595601985722" Z="1.35" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.35" />
                  <Point X="-1.72275271033" Y="4.395267975064" Z="1.35" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.35" />
                  <Point X="-1.786338922581" Y="4.321578722722" Z="1.35" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.35" />
                  <Point X="-1.883660584239" Y="4.322922010621" Z="1.35" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.35" />
                  <Point X="-2.040700872544" Y="4.487935890493" Z="1.35" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.35" />
                  <Point X="-2.439541199834" Y="4.440312298846" Z="1.35" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.35" />
                  <Point X="-3.063654017963" Y="4.035065168983" Z="1.35" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.35" />
                  <Point X="-3.17802982978" Y="3.446028767819" Z="1.35" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.35" />
                  <Point X="-2.998021829762" Y="3.100275476023" Z="1.35" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.35" />
                  <Point X="-3.022458869146" Y="3.02634469874" Z="1.35" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.35" />
                  <Point X="-3.094800989583" Y="2.997542869411" Z="1.35" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.35" />
                  <Point X="-3.487830580478" Y="3.202164118966" Z="1.35" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.35" />
                  <Point X="-3.987360446131" Y="3.129548652619" Z="1.35" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.35" />
                  <Point X="-4.366786822006" Y="2.573768859208" Z="1.35" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.35" />
                  <Point X="-4.094876625441" Y="1.916471426538" Z="1.35" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.35" />
                  <Point X="-3.682643920398" Y="1.584097178585" Z="1.35" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.35" />
                  <Point X="-3.678357548279" Y="1.525856148576" Z="1.35" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.35" />
                  <Point X="-3.720217593258" Y="1.485136008938" Z="1.35" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.35" />
                  <Point X="-4.318726998419" Y="1.549325612174" Z="1.35" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.35" />
                  <Point X="-4.889661232072" Y="1.344855634826" Z="1.35" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.35" />
                  <Point X="-5.013779375529" Y="0.761207788538" Z="1.35" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.35" />
                  <Point X="-4.270969201986" Y="0.235135222693" Z="1.35" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.35" />
                  <Point X="-3.563572280561" Y="0.040054302478" Z="1.35" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.35" />
                  <Point X="-3.544478299986" Y="0.015857180852" Z="1.35" />
                  <Point X="-3.539556741714" Y="0" Z="1.35" />
                  <Point X="-3.543886301149" Y="-0.013949770209" Z="1.35" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.35" />
                  <Point X="-3.561796314562" Y="-0.038821680656" Z="1.35" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.35" />
                  <Point X="-4.365918586786" Y="-0.260576834063" Z="1.35" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.35" />
                  <Point X="-5.023979373959" Y="-0.700782163225" Z="1.35" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.35" />
                  <Point X="-4.919126259509" Y="-1.238409788058" Z="1.35" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.35" />
                  <Point X="-3.980949679484" Y="-1.407154949071" Z="1.35" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.35" />
                  <Point X="-3.206764935393" Y="-1.31415784904" Z="1.35" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.35" />
                  <Point X="-3.19628204035" Y="-1.336371687942" Z="1.35" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.35" />
                  <Point X="-3.893316217205" Y="-1.883905192708" Z="1.35" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.35" />
                  <Point X="-4.365520048739" Y="-2.582021936355" Z="1.35" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.35" />
                  <Point X="-4.046903228686" Y="-3.057315111949" Z="1.35" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.35" />
                  <Point X="-3.176283476995" Y="-2.903889509186" Z="1.35" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.35" />
                  <Point X="-2.564720319296" Y="-2.563610149835" Z="1.35" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.35" />
                  <Point X="-2.951527503982" Y="-3.258794905497" Z="1.35" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.35" />
                  <Point X="-3.108301585304" Y="-4.009783333633" Z="1.35" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.35" />
                  <Point X="-2.684854354761" Y="-4.304817669649" Z="1.35" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.35" />
                  <Point X="-2.331474243682" Y="-4.293619164938" Z="1.35" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.35" />
                  <Point X="-2.105493044099" Y="-4.075783366075" Z="1.35" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.35" />
                  <Point X="-1.823367059259" Y="-3.993580900763" Z="1.35" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.35" />
                  <Point X="-1.549499845498" Y="-4.100112957746" Z="1.35" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.35" />
                  <Point X="-1.397078897604" Y="-4.351350228926" Z="1.35" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.35" />
                  <Point X="-1.390531659925" Y="-4.70808697415" Z="1.35" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.35" />
                  <Point X="-1.274711593401" Y="-4.915109139864" Z="1.35" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.35" />
                  <Point X="-0.97712526191" Y="-4.982811750118" Z="1.35" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.35" />
                  <Point X="-0.604560402486" Y="-4.218434212858" Z="1.35" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.35" />
                  <Point X="-0.340461572849" Y="-3.408370763891" Z="1.35" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.35" />
                  <Point X="-0.134786034766" Y="-3.246071979561" Z="1.35" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.35" />
                  <Point X="0.118573044595" Y="-3.241040065727" Z="1.35" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.35" />
                  <Point X="0.329984286584" Y="-3.393274991876" Z="1.35" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.35" />
                  <Point X="0.630194457915" Y="-4.314101816958" Z="1.35" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.35" />
                  <Point X="0.9020688815" Y="-4.998430405642" Z="1.35" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.35" />
                  <Point X="1.081802899966" Y="-4.962634056388" Z="1.35" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.35" />
                  <Point X="1.060169612329" Y="-4.053938120299" Z="1.35" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.35" />
                  <Point X="0.982531055887" Y="-3.157041686981" Z="1.35" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.35" />
                  <Point X="1.09539311124" Y="-2.955288957078" Z="1.35" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.35" />
                  <Point X="1.300229244985" Y="-2.865637293466" Z="1.35" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.35" />
                  <Point X="1.523973063987" Y="-2.918351870061" Z="1.35" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.35" />
                  <Point X="2.182486426722" Y="-3.701675668991" Z="1.35" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.35" />
                  <Point X="2.753413630783" Y="-4.267510547051" Z="1.35" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.35" />
                  <Point X="2.945838185923" Y="-4.137024367446" Z="1.35" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.35" />
                  <Point X="2.634068973584" Y="-3.350741951222" Z="1.35" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.35" />
                  <Point X="2.252972676485" Y="-2.621167446835" Z="1.35" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.35" />
                  <Point X="2.276427695506" Y="-2.422193059356" Z="1.35" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.35" />
                  <Point X="2.410705319016" Y="-2.282473614461" Z="1.35" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.35" />
                  <Point X="2.607339356677" Y="-2.250475333006" Z="1.35" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.35" />
                  <Point X="3.436671645885" Y="-2.683680526691" Z="1.35" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.35" />
                  <Point X="4.146831866609" Y="-2.930404156577" Z="1.35" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.35" />
                  <Point X="4.314362464013" Y="-2.677640604754" Z="1.35" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.35" />
                  <Point X="3.757373522278" Y="-2.047849459645" Z="1.35" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.35" />
                  <Point X="3.145717640908" Y="-1.541448446371" Z="1.35" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.35" />
                  <Point X="3.099623717119" Y="-1.378306461608" Z="1.35" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.35" />
                  <Point X="3.15935211385" Y="-1.225601306176" Z="1.35" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.35" />
                  <Point X="3.30270834925" Y="-1.136914988913" Z="1.35" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.35" />
                  <Point X="4.201393996645" Y="-1.22151811288" Z="1.35" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.35" />
                  <Point X="4.946521083687" Y="-1.14125654214" Z="1.35" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.35" />
                  <Point X="5.017916520204" Y="-0.768798931806" Z="1.35" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.35" />
                  <Point X="4.356387019256" Y="-0.383840113975" Z="1.35" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.35" />
                  <Point X="3.704657663221" Y="-0.195785329771" Z="1.35" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.35" />
                  <Point X="3.629465692827" Y="-0.134237377034" Z="1.35" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.35" />
                  <Point X="3.591277716421" Y="-0.051396423037" Z="1.35" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.35" />
                  <Point X="3.590093734003" Y="0.045214108171" Z="1.35" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.35" />
                  <Point X="3.625913745573" Y="0.129711361541" Z="1.35" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.35" />
                  <Point X="3.698737751131" Y="0.192363514471" Z="1.35" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.35" />
                  <Point X="4.43958079805" Y="0.406131818794" Z="1.35" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.35" />
                  <Point X="5.017172613612" Y="0.767257664548" Z="1.35" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.35" />
                  <Point X="4.93469125418" Y="1.18723438052" Z="1.35" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.35" />
                  <Point X="4.126593760117" Y="1.309371901446" Z="1.35" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.35" />
                  <Point X="3.419053792065" Y="1.227848156233" Z="1.35" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.35" />
                  <Point X="3.336367858012" Y="1.252815480718" Z="1.35" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.35" />
                  <Point X="3.276827426919" Y="1.307856552542" Z="1.35" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.35" />
                  <Point X="3.242991852003" Y="1.386792891309" Z="1.35" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.35" />
                  <Point X="3.243665327717" Y="1.468368892274" Z="1.35" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.35" />
                  <Point X="3.282158269935" Y="1.544592493117" Z="1.35" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.35" />
                  <Point X="3.916401800489" Y="2.047779716284" Z="1.35" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.35" />
                  <Point X="4.349439197939" Y="2.616896967672" Z="1.35" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.35" />
                  <Point X="4.127771171594" Y="2.954196143936" Z="1.35" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.35" />
                  <Point X="3.208319140857" Y="2.670243971639" Z="1.35" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.35" />
                  <Point X="2.472303632956" Y="2.256950913952" Z="1.35" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.35" />
                  <Point X="2.397100544908" Y="2.24944700097" Z="1.35" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.35" />
                  <Point X="2.330538060356" Y="2.27400482748" Z="1.35" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.35" />
                  <Point X="2.276753772755" Y="2.326486800026" Z="1.35" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.35" />
                  <Point X="2.249982701789" Y="2.392657898924" Z="1.35" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.35" />
                  <Point X="2.255577028706" Y="2.467165942926" Z="1.35" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.35" />
                  <Point X="2.725381381221" Y="3.303819941098" Z="1.35" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.35" />
                  <Point X="2.953064857973" Y="4.127110962929" Z="1.35" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.35" />
                  <Point X="2.567356240633" Y="4.377478527703" Z="1.35" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.35" />
                  <Point X="2.163070769038" Y="4.590868983551" Z="1.35" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.35" />
                  <Point X="1.744056127644" Y="4.765838896284" Z="1.35" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.35" />
                  <Point X="1.210579276667" Y="4.922205065431" Z="1.35" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.35" />
                  <Point X="0.549317033713" Y="5.039032734664" Z="1.35" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.35" />
                  <Point X="0.09043921407" Y="4.692648232701" Z="1.35" />
                  <Point X="0" Y="4.355124473572" Z="1.35" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>