<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#187" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2615" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.83949609375" Y="-4.54328515625" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140625" />
                  <Point X="25.542365234375" Y="-3.467376708984" />
                  <Point X="25.429478515625" Y="-3.304728759766" />
                  <Point X="25.37863671875" Y="-3.231476318359" />
                  <Point X="25.356751953125" Y="-3.209020263672" />
                  <Point X="25.33049609375" Y="-3.189776611328" />
                  <Point X="25.30249609375" Y="-3.175669189453" />
                  <Point X="25.127810546875" Y="-3.121453369141" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097036132812" />
                  <Point X="24.7884921875" Y="-3.151251708984" />
                  <Point X="24.70981640625" Y="-3.175669433594" />
                  <Point X="24.68181640625" Y="-3.189776855469" />
                  <Point X="24.655560546875" Y="-3.209020751953" />
                  <Point X="24.63367578125" Y="-3.231477294922" />
                  <Point X="24.520791015625" Y="-3.394125" />
                  <Point X="24.46994921875" Y="-3.467377685547" />
                  <Point X="24.4618125" Y="-3.481572021484" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.324619140625" Y="-3.97675390625" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="24.009458984375" Y="-4.862586425781" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.75358203125" Y="-4.802362792969" />
                  <Point X="23.7850390625" Y="-4.563437011719" />
                  <Point X="23.78580078125" Y="-4.547929199219" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.741759765625" Y="-4.306421386719" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.182963867188" />
                  <Point X="23.69598828125" Y="-4.155126953125" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.515529296875" Y="-3.990161621094" />
                  <Point X="23.443095703125" Y="-3.926639404297" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.143517578125" Y="-3.876975830078" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708740234" />
                  <Point X="22.985533203125" Y="-3.882028564453" />
                  <Point X="22.957341796875" Y="-3.894801757812" />
                  <Point X="22.77948046875" Y="-4.013645263672" />
                  <Point X="22.699376953125" Y="-4.067169677734" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461425781" />
                  <Point X="22.5950625" Y="-4.190472167969" />
                  <Point X="22.5198515625" Y="-4.288488769531" />
                  <Point X="22.328447265625" Y="-4.169975585938" />
                  <Point X="22.198287109375" Y="-4.089384033203" />
                  <Point X="21.9218359375" Y="-3.876525878906" />
                  <Point X="21.895279296875" Y="-3.856077392578" />
                  <Point X="22.06296875" Y="-3.565629882812" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647655273438" />
                  <Point X="22.593412109375" Y="-2.616130126953" />
                  <Point X="22.59442578125" Y="-2.585197998047" />
                  <Point X="22.585443359375" Y="-2.555581542969" />
                  <Point X="22.571228515625" Y="-2.526752929688" />
                  <Point X="22.553201171875" Y="-2.501592529297" />
                  <Point X="22.535853515625" Y="-2.484243896484" />
                  <Point X="22.5106953125" Y="-2.466215576172" />
                  <Point X="22.481865234375" Y="-2.451997070313" />
                  <Point X="22.452248046875" Y="-2.44301171875" />
                  <Point X="22.4213125" Y="-2.444023925781" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.960427734375" Y="-2.692362060547" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="21.01996875" Y="-2.928956787109" />
                  <Point X="20.917140625" Y="-2.793861328125" />
                  <Point X="20.7189453125" Y="-2.461517089844" />
                  <Point X="20.693857421875" Y="-2.419449462891" />
                  <Point X="20.99598828125" Y="-2.187616699219" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.915421875" Y="-1.47559375" />
                  <Point X="21.93338671875" Y="-1.448462890625" />
                  <Point X="21.946142578125" Y="-1.419834838867" />
                  <Point X="21.951455078125" Y="-1.399324829102" />
                  <Point X="21.95384765625" Y="-1.390087524414" />
                  <Point X="21.95665234375" Y="-1.35965637207" />
                  <Point X="21.954443359375" Y="-1.327986572266" />
                  <Point X="21.947443359375" Y="-1.298244262695" />
                  <Point X="21.93136328125" Y="-1.272262573242" />
                  <Point X="21.910533203125" Y="-1.248306152344" />
                  <Point X="21.887029296875" Y="-1.228767700195" />
                  <Point X="21.86876953125" Y="-1.218020996094" />
                  <Point X="21.860546875" Y="-1.213180908203" />
                  <Point X="21.831287109375" Y="-1.201957885742" />
                  <Point X="21.7993984375" Y="-1.195475219727" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="21.262619140625" Y="-1.260927856445" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.206142578125" Y="-1.15010559082" />
                  <Point X="20.165921875" Y="-0.99265057373" />
                  <Point X="20.113486328125" Y="-0.626016723633" />
                  <Point X="20.107576171875" Y="-0.584698364258" />
                  <Point X="20.444103515625" Y="-0.494526184082" />
                  <Point X="21.467125" Y="-0.220408279419" />
                  <Point X="21.4825078125" Y="-0.214827850342" />
                  <Point X="21.51226953125" Y="-0.199471069336" />
                  <Point X="21.531404296875" Y="-0.186190475464" />
                  <Point X="21.5400234375" Y="-0.180209030151" />
                  <Point X="21.5624765625" Y="-0.158326782227" />
                  <Point X="21.581724609375" Y="-0.132066894531" />
                  <Point X="21.5958359375" Y="-0.104058685303" />
                  <Point X="21.602212890625" Y="-0.083507484436" />
                  <Point X="21.605083984375" Y="-0.074256622314" />
                  <Point X="21.6093515625" Y="-0.046108921051" />
                  <Point X="21.609353515625" Y="-0.016468515396" />
                  <Point X="21.6050859375" Y="0.011691631317" />
                  <Point X="21.598708984375" Y="0.032242832184" />
                  <Point X="21.595830078125" Y="0.041516765594" />
                  <Point X="21.581720703125" Y="0.069514343262" />
                  <Point X="21.562474609375" Y="0.095769523621" />
                  <Point X="21.5400234375" Y="0.117649040222" />
                  <Point X="21.520888671875" Y="0.130929641724" />
                  <Point X="21.512234375" Y="0.136272659302" />
                  <Point X="21.48805078125" Y="0.14947265625" />
                  <Point X="21.467125" Y="0.157848297119" />
                  <Point X="21.006380859375" Y="0.281304260254" />
                  <Point X="20.10818359375" Y="0.521975463867" />
                  <Point X="20.14959375" Y="0.801819335938" />
                  <Point X="20.17551171875" Y="0.976968200684" />
                  <Point X="20.281068359375" Y="1.366505371094" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="20.5010078125" Y="1.396337280273" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228149414" />
                  <Point X="21.29686328125" Y="1.305263427734" />
                  <Point X="21.33921484375" Y="1.31861706543" />
                  <Point X="21.3582890625" Y="1.324631225586" />
                  <Point X="21.3772265625" Y="1.332963745117" />
                  <Point X="21.395970703125" Y="1.343786865234" />
                  <Point X="21.4126484375" Y="1.356016845703" />
                  <Point X="21.42628515625" Y="1.37156640625" />
                  <Point X="21.438701171875" Y="1.389297851562" />
                  <Point X="21.448650390625" Y="1.407433227539" />
                  <Point X="21.46564453125" Y="1.448460083008" />
                  <Point X="21.473296875" Y="1.4669375" />
                  <Point X="21.479087890625" Y="1.486807495117" />
                  <Point X="21.48284375" Y="1.508122924805" />
                  <Point X="21.484193359375" Y="1.528757202148" />
                  <Point X="21.481048828125" Y="1.549194946289" />
                  <Point X="21.475447265625" Y="1.57010144043" />
                  <Point X="21.467951171875" Y="1.58937890625" />
                  <Point X="21.447447265625" Y="1.628768554688" />
                  <Point X="21.4382109375" Y="1.646508666992" />
                  <Point X="21.426712890625" Y="1.663714233398" />
                  <Point X="21.41280078125" Y="1.680291748047" />
                  <Point X="21.39786328125" Y="1.694590087891" />
                  <Point X="21.133580078125" Y="1.897382446289" />
                  <Point X="20.648140625" Y="2.269873291016" />
                  <Point X="20.818138671875" Y="2.561120849609" />
                  <Point X="20.9188515625" Y="2.733664794922" />
                  <Point X="21.19845703125" Y="3.093059570312" />
                  <Point X="21.24949609375" Y="3.158662353516" />
                  <Point X="21.342955078125" Y="3.104703369141" />
                  <Point X="21.79334375" Y="2.844671142578" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.91219140625" Y="2.820635986328" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.00098828125" Y="2.826504882812" />
                  <Point X="22.0195390625" Y="2.835654541016" />
                  <Point X="22.03779296875" Y="2.847284179688" />
                  <Point X="22.053921875" Y="2.860228515625" />
                  <Point X="22.095791015625" Y="2.902095947266" />
                  <Point X="22.114646484375" Y="2.920951904297" />
                  <Point X="22.127595703125" Y="2.937086181641" />
                  <Point X="22.139224609375" Y="2.955340576172" />
                  <Point X="22.14837109375" Y="2.973889404297" />
                  <Point X="22.1532890625" Y="2.993978027344" />
                  <Point X="22.156115234375" Y="3.015436523438" />
                  <Point X="22.15656640625" Y="3.036120605469" />
                  <Point X="22.15140625" Y="3.095104980469" />
                  <Point X="22.14908203125" Y="3.121670166016" />
                  <Point X="22.145044921875" Y="3.141963378906" />
                  <Point X="22.13853515625" Y="3.16260546875" />
                  <Point X="22.130205078125" Y="3.181533203125" />
                  <Point X="22.013091796875" Y="3.384377441406" />
                  <Point X="21.81666796875" Y="3.724595703125" />
                  <Point X="22.123978515625" Y="3.960207519531" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.73975" Y="4.339347167969" />
                  <Point X="22.83296484375" Y="4.391134765625" />
                  <Point X="22.9568046875" Y="4.229741210938" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.00488671875" Y="4.189395019531" />
                  <Point X="23.07053515625" Y="4.155220214844" />
                  <Point X="23.100103515625" Y="4.139828125" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.202685546875" Y="4.128692871094" />
                  <Point X="23.222546875" Y="4.134481933594" />
                  <Point X="23.290923828125" Y="4.162805664062" />
                  <Point X="23.321720703125" Y="4.175561523437" />
                  <Point X="23.339853515625" Y="4.185509277344" />
                  <Point X="23.357583984375" Y="4.197923828125" />
                  <Point X="23.37313671875" Y="4.2115625" />
                  <Point X="23.3853671875" Y="4.228244140625" />
                  <Point X="23.396189453125" Y="4.246988769531" />
                  <Point X="23.404521484375" Y="4.265921875" />
                  <Point X="23.42677734375" Y="4.336508300781" />
                  <Point X="23.43680078125" Y="4.368298339844" />
                  <Point X="23.4408359375" Y="4.388583984375" />
                  <Point X="23.44272265625" Y="4.410145507812" />
                  <Point X="23.442271484375" Y="4.430826660156" />
                  <Point X="23.42895703125" Y="4.531959472656" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.823302734375" Y="4.746147949219" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.58423046875" Y="4.872289550781" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.732859375" Y="4.783564941406" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.206224609375" Y="4.510264648438" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.645775390625" Y="4.852502441406" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.2857265625" Y="4.725102539062" />
                  <Point X="26.48102734375" Y="4.677950195312" />
                  <Point X="26.7680625" Y="4.573841796875" />
                  <Point X="26.894646484375" Y="4.527928710938" />
                  <Point X="27.172640625" Y="4.397919433594" />
                  <Point X="27.294576171875" Y="4.340893554688" />
                  <Point X="27.56315625" Y="4.184419433594" />
                  <Point X="27.680978515625" Y="4.115775878906" />
                  <Point X="27.934263671875" Y="3.935653076172" />
                  <Point X="27.94326171875" Y="3.929253662109" />
                  <Point X="27.74165625" Y="3.580060791016" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142080078125" Y="2.539938720703" />
                  <Point X="27.133078125" Y="2.516058349609" />
                  <Point X="27.118275390625" Y="2.460702880859" />
                  <Point X="27.111607421875" Y="2.435771972656" />
                  <Point X="27.108619140625" Y="2.417936767578" />
                  <Point X="27.107728515625" Y="2.380953613281" />
                  <Point X="27.1135" Y="2.333086914062" />
                  <Point X="27.116099609375" Y="2.311528808594" />
                  <Point X="27.121443359375" Y="2.289600341797" />
                  <Point X="27.1297109375" Y="2.267510986328" />
                  <Point X="27.140072265625" Y="2.247469482422" />
                  <Point X="27.16969140625" Y="2.203819580078" />
                  <Point X="27.18303125" Y="2.184160888672" />
                  <Point X="27.19446875" Y="2.170326660156" />
                  <Point X="27.2216015625" Y="2.145592285156" />
                  <Point X="27.26525" Y="2.115973876953" />
                  <Point X="27.28491015625" Y="2.102634765625" />
                  <Point X="27.30495703125" Y="2.092270996094" />
                  <Point X="27.32704296875" Y="2.084006103516" />
                  <Point X="27.34896484375" Y="2.078663330078" />
                  <Point X="27.39683203125" Y="2.072891357422" />
                  <Point X="27.418388671875" Y="2.070291748047" />
                  <Point X="27.436466796875" Y="2.069845458984" />
                  <Point X="27.47320703125" Y="2.074170898438" />
                  <Point X="27.5285625" Y="2.088973876953" />
                  <Point X="27.553494140625" Y="2.095640625" />
                  <Point X="27.565283203125" Y="2.099637939453" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.051955078125" Y="2.377700683594" />
                  <Point X="28.967328125" Y="2.90619140625" />
                  <Point X="29.053384765625" Y="2.786592285156" />
                  <Point X="29.123267578125" Y="2.689469238281" />
                  <Point X="29.26219921875" Y="2.459883056641" />
                  <Point X="29.0129375" Y="2.2686171875" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.221423828125" Y="1.660239379883" />
                  <Point X="28.203974609375" Y="1.641626708984" />
                  <Point X="28.164134765625" Y="1.589653198242" />
                  <Point X="28.146193359375" Y="1.566245605469" />
                  <Point X="28.136609375" Y="1.550916748047" />
                  <Point X="28.121630859375" Y="1.517089111328" />
                  <Point X="28.106791015625" Y="1.464023803711" />
                  <Point X="28.10010546875" Y="1.440124511719" />
                  <Point X="28.09665234375" Y="1.417827148438" />
                  <Point X="28.0958359375" Y="1.39425390625" />
                  <Point X="28.097740234375" Y="1.371766601562" />
                  <Point X="28.109923828125" Y="1.312724731445" />
                  <Point X="28.11541015625" Y="1.286133789062" />
                  <Point X="28.1206796875" Y="1.268979003906" />
                  <Point X="28.136283203125" Y="1.235741821289" />
                  <Point X="28.16941796875" Y="1.185378662109" />
                  <Point X="28.18433984375" Y="1.162696411133" />
                  <Point X="28.198890625" Y="1.145453613281" />
                  <Point X="28.216134765625" Y="1.129362792969" />
                  <Point X="28.23434765625" Y="1.116034790039" />
                  <Point X="28.282365234375" Y="1.089005493164" />
                  <Point X="28.303990234375" Y="1.07683215332" />
                  <Point X="28.320521484375" Y="1.069501342773" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.421041015625" Y="1.050858398438" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="28.928421875" Y="1.104940307617" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.81591796875" Y="1.056110229492" />
                  <Point X="29.84594140625" Y="0.932784851074" />
                  <Point X="29.89043359375" Y="0.647012756348" />
                  <Point X="29.890865234375" Y="0.644238586426" />
                  <Point X="29.612943359375" Y="0.569769348145" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.7047890625" Y="0.325585754395" />
                  <Point X="28.681546875" Y="0.315067901611" />
                  <Point X="28.617763671875" Y="0.278199981689" />
                  <Point X="28.589037109375" Y="0.261595489502" />
                  <Point X="28.574314453125" Y="0.251098571777" />
                  <Point X="28.54753125" Y="0.225576690674" />
                  <Point X="28.50926171875" Y="0.176811462402" />
                  <Point X="28.492025390625" Y="0.154848907471" />
                  <Point X="28.48030078125" Y="0.135569107056" />
                  <Point X="28.47052734375" Y="0.114105949402" />
                  <Point X="28.463681640625" Y="0.09260408783" />
                  <Point X="28.45092578125" Y="0.025993318558" />
                  <Point X="28.4451796875" Y="-0.00400637579" />
                  <Point X="28.443484375" Y="-0.021874074936" />
                  <Point X="28.4451796875" Y="-0.058553760529" />
                  <Point X="28.457935546875" Y="-0.125164375305" />
                  <Point X="28.463681640625" Y="-0.155164077759" />
                  <Point X="28.47052734375" Y="-0.176665939331" />
                  <Point X="28.48030078125" Y="-0.198129089355" />
                  <Point X="28.492025390625" Y="-0.217409194946" />
                  <Point X="28.530294921875" Y="-0.266174133301" />
                  <Point X="28.54753125" Y="-0.28813684082" />
                  <Point X="28.560001953125" Y="-0.301238769531" />
                  <Point X="28.589037109375" Y="-0.324155609131" />
                  <Point X="28.6528203125" Y="-0.361023529053" />
                  <Point X="28.681546875" Y="-0.37762802124" />
                  <Point X="28.692708984375" Y="-0.383138305664" />
                  <Point X="28.716580078125" Y="-0.392149963379" />
                  <Point X="29.12028125" Y="-0.500321075439" />
                  <Point X="29.891474609375" Y="-0.706961914062" />
                  <Point X="29.87178515625" Y="-0.837559020996" />
                  <Point X="29.855025390625" Y="-0.94872454834" />
                  <Point X="29.80117578125" Y="-1.18469921875" />
                  <Point X="29.46391015625" Y="-1.140297485352" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710266113" />
                  <Point X="28.37466015625" Y="-1.005508666992" />
                  <Point X="28.2494765625" Y="-1.032718139648" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.163974609375" Y="-1.056597412109" />
                  <Point X="28.1361484375" Y="-1.073489868164" />
                  <Point X="28.1123984375" Y="-1.093960083008" />
                  <Point X="28.036732421875" Y="-1.184962890625" />
                  <Point X="28.002654296875" Y="-1.225948242188" />
                  <Point X="27.98793359375" Y="-1.250330688477" />
                  <Point X="27.97658984375" Y="-1.277717163086" />
                  <Point X="27.969759765625" Y="-1.305366088867" />
                  <Point X="27.9589140625" Y="-1.423218505859" />
                  <Point X="27.95403125" Y="-1.476296386719" />
                  <Point X="27.956349609375" Y="-1.507568237305" />
                  <Point X="27.96408203125" Y="-1.539188964844" />
                  <Point X="27.976453125" Y="-1.567996948242" />
                  <Point X="28.04573046875" Y="-1.675755615234" />
                  <Point X="28.07693359375" Y="-1.724287475586" />
                  <Point X="28.086935546875" Y="-1.737239868164" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.485265625" Y="-2.048377685547" />
                  <Point X="29.213123046875" Y="-2.606882568359" />
                  <Point X="29.172056640625" Y="-2.673335205078" />
                  <Point X="29.12480078125" Y="-2.749799316406" />
                  <Point X="29.028982421875" Y="-2.885945556641" />
                  <Point X="28.726794921875" Y="-2.711477539062" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.7542265625" Y="-2.159825195312" />
                  <Point X="27.60523828125" Y="-2.13291796875" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135176757812" />
                  <Point X="27.321060546875" Y="-2.200317871094" />
                  <Point X="27.26531640625" Y="-2.229655761719" />
                  <Point X="27.242384765625" Y="-2.246550292969" />
                  <Point X="27.22142578125" Y="-2.267510253906" />
                  <Point X="27.204533203125" Y="-2.290438720703" />
                  <Point X="27.139390625" Y="-2.414212158203" />
                  <Point X="27.110052734375" Y="-2.469956787109" />
                  <Point X="27.100228515625" Y="-2.499735595703" />
                  <Point X="27.095271484375" Y="-2.531908691406" />
                  <Point X="27.09567578125" Y="-2.563259277344" />
                  <Point X="27.122583984375" Y="-2.712248535156" />
                  <Point X="27.134703125" Y="-2.779349609375" />
                  <Point X="27.13898828125" Y="-2.795140869141" />
                  <Point X="27.1518203125" Y="-2.826078369141" />
                  <Point X="27.392560546875" Y="-3.2430546875" />
                  <Point X="27.86128515625" Y="-4.054907226563" />
                  <Point X="27.837916015625" Y="-4.071598632812" />
                  <Point X="27.781849609375" Y="-4.111644042969" />
                  <Point X="27.701767578125" Y="-4.163481445312" />
                  <Point X="27.465048828125" Y="-3.854985351562" />
                  <Point X="26.758548828125" Y="-2.934254882812" />
                  <Point X="26.74750390625" Y="-2.922177490234" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.57498046875" Y="-2.8060859375" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.479990234375" Y="-2.751167236328" />
                  <Point X="26.448369140625" Y="-2.743435546875" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.256392578125" Y="-2.755905273438" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.156365234375" Y="-2.769396972656" />
                  <Point X="26.12898046875" Y="-2.780739746094" />
                  <Point X="26.10459765625" Y="-2.7954609375" />
                  <Point X="25.98050390625" Y="-2.898641357422" />
                  <Point X="25.92461328125" Y="-2.945111328125" />
                  <Point X="25.904142578125" Y="-2.968861328125" />
                  <Point X="25.88725" Y="-2.996688232422" />
                  <Point X="25.875625" Y="-3.02580859375" />
                  <Point X="25.838521484375" Y="-3.196514648438" />
                  <Point X="25.821810546875" Y="-3.273396484375" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323120361328" />
                  <Point X="25.887966796875" Y="-3.841334960938" />
                  <Point X="26.022064453125" Y="-4.859915527344" />
                  <Point X="25.975681640625" Y="-4.870083007812" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.941580078125" Y="-4.752637695312" />
                  <Point X="23.85875390625" Y="-4.731327636719" />
                  <Point X="23.8792265625" Y="-4.575837890625" />
                  <Point X="23.879923828125" Y="-4.56809765625" />
                  <Point X="23.88055078125" Y="-4.541030273438" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497689453125" />
                  <Point X="23.83493359375" Y="-4.287887695313" />
                  <Point X="23.81613671875" Y="-4.193397949219" />
                  <Point X="23.811875" Y="-4.17846875" />
                  <Point X="23.80097265625" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135462402344" />
                  <Point X="23.778259765625" Y="-4.107625488281" />
                  <Point X="23.769423828125" Y="-4.094859863281" />
                  <Point X="23.749791015625" Y="-4.070937011719" />
                  <Point X="23.738994140625" Y="-4.059779785156" />
                  <Point X="23.57816796875" Y="-3.918737304688" />
                  <Point X="23.505734375" Y="-3.855215087891" />
                  <Point X="23.49326171875" Y="-3.845965332031" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.14973046875" Y="-3.782179199219" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166259766" />
                  <Point X="22.991994140625" Y="-3.781946044922" />
                  <Point X="22.9609453125" Y="-3.790265869141" />
                  <Point X="22.946326171875" Y="-3.795496337891" />
                  <Point X="22.918134765625" Y="-3.80826953125" />
                  <Point X="22.9045625" Y="-3.815812255859" />
                  <Point X="22.726701171875" Y="-3.934655761719" />
                  <Point X="22.64659765625" Y="-3.988180175781" />
                  <Point X="22.64031640625" Y="-3.992759765625" />
                  <Point X="22.619560546875" Y="-4.010130859375" />
                  <Point X="22.597244140625" Y="-4.032769287109" />
                  <Point X="22.589529296875" Y="-4.041628662109" />
                  <Point X="22.519693359375" Y="-4.132639160156" />
                  <Point X="22.496796875" Y="-4.162477539063" />
                  <Point X="22.378458984375" Y="-4.089205078125" />
                  <Point X="22.252408203125" Y="-4.011157958984" />
                  <Point X="22.019138671875" Y="-3.831548339844" />
                  <Point X="22.1452421875" Y="-3.613129882813" />
                  <Point X="22.65851171875" Y="-2.724119628906" />
                  <Point X="22.6651484375" Y="-2.7100859375" />
                  <Point X="22.67605078125" Y="-2.681121582031" />
                  <Point X="22.680314453125" Y="-2.666190917969" />
                  <Point X="22.6865859375" Y="-2.634665771484" />
                  <Point X="22.688361328125" Y="-2.619241699219" />
                  <Point X="22.689375" Y="-2.588309570312" />
                  <Point X="22.6853359375" Y="-2.557625488281" />
                  <Point X="22.676353515625" Y="-2.528009033203" />
                  <Point X="22.6706484375" Y="-2.513568603516" />
                  <Point X="22.65643359375" Y="-2.484739990234" />
                  <Point X="22.648453125" Y="-2.471422363281" />
                  <Point X="22.63042578125" Y="-2.446261962891" />
                  <Point X="22.62037890625" Y="-2.434419189453" />
                  <Point X="22.60303125" Y="-2.417070556641" />
                  <Point X="22.591189453125" Y="-2.407023681641" />
                  <Point X="22.56603125" Y="-2.388995361328" />
                  <Point X="22.55271484375" Y="-2.381013916016" />
                  <Point X="22.523884765625" Y="-2.366795410156" />
                  <Point X="22.5094453125" Y="-2.361088623047" />
                  <Point X="22.479828125" Y="-2.352103271484" />
                  <Point X="22.449140625" Y="-2.3480625" />
                  <Point X="22.418205078125" Y="-2.349074707031" />
                  <Point X="22.402779296875" Y="-2.350849121094" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.912927734375" Y="-2.610089599609" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="21.0955625" Y="-2.871418457031" />
                  <Point X="20.995978515625" Y="-2.740585449219" />
                  <Point X="20.818734375" Y="-2.443372802734" />
                  <Point X="21.0538203125" Y="-2.262985351562" />
                  <Point X="21.951876953125" Y="-1.573882080078" />
                  <Point X="21.963517578125" Y="-1.563309814453" />
                  <Point X="21.98489453125" Y="-1.540390136719" />
                  <Point X="21.994630859375" Y="-1.528042602539" />
                  <Point X="22.012595703125" Y="-1.500911743164" />
                  <Point X="22.020162109375" Y="-1.487127685547" />
                  <Point X="22.03291796875" Y="-1.458499633789" />
                  <Point X="22.038107421875" Y="-1.443655639648" />
                  <Point X="22.043419921875" Y="-1.423145629883" />
                  <Point X="22.048447265625" Y="-1.398806274414" />
                  <Point X="22.051251953125" Y="-1.36837512207" />
                  <Point X="22.051421875" Y="-1.353046142578" />
                  <Point X="22.049212890625" Y="-1.321376220703" />
                  <Point X="22.046916015625" Y="-1.30622253418" />
                  <Point X="22.039916015625" Y="-1.276480224609" />
                  <Point X="22.028224609375" Y="-1.248249145508" />
                  <Point X="22.01214453125" Y="-1.222267333984" />
                  <Point X="22.003052734375" Y="-1.209928466797" />
                  <Point X="21.98222265625" Y="-1.185971923828" />
                  <Point X="21.97126171875" Y="-1.175251586914" />
                  <Point X="21.9477578125" Y="-1.155713134766" />
                  <Point X="21.93521484375" Y="-1.146895019531" />
                  <Point X="21.916955078125" Y="-1.13614831543" />
                  <Point X="21.894568359375" Y="-1.124481933594" />
                  <Point X="21.86530859375" Y="-1.113258911133" />
                  <Point X="21.850212890625" Y="-1.108862060547" />
                  <Point X="21.81832421875" Y="-1.102379394531" />
                  <Point X="21.802705078125" Y="-1.100532836914" />
                  <Point X="21.77137890625" Y="-1.09944152832" />
                  <Point X="21.755671875" Y="-1.100196655273" />
                  <Point X="21.25021875" Y="-1.166740478516" />
                  <Point X="20.33908203125" Y="-1.286694213867" />
                  <Point X="20.2981875" Y="-1.126594604492" />
                  <Point X="20.259236328125" Y="-0.974113342285" />
                  <Point X="20.213548828125" Y="-0.654654418945" />
                  <Point X="20.46869140625" Y="-0.58628918457" />
                  <Point X="21.491712890625" Y="-0.312171173096" />
                  <Point X="21.499521484375" Y="-0.309713409424" />
                  <Point X="21.5260703125" Y="-0.299251556396" />
                  <Point X="21.55583203125" Y="-0.283894775391" />
                  <Point X="21.5664375" Y="-0.277515441895" />
                  <Point X="21.585572265625" Y="-0.264234802246" />
                  <Point X="21.606328125" Y="-0.24824357605" />
                  <Point X="21.62878125" Y="-0.226361236572" />
                  <Point X="21.63909765625" Y="-0.214488937378" />
                  <Point X="21.658345703125" Y="-0.188228897095" />
                  <Point X="21.666564453125" Y="-0.174811828613" />
                  <Point X="21.68067578125" Y="-0.14680368042" />
                  <Point X="21.686568359375" Y="-0.132212478638" />
                  <Point X="21.6929453125" Y="-0.111661354065" />
                  <Point X="21.699009765625" Y="-0.088497138977" />
                  <Point X="21.70327734375" Y="-0.060349449158" />
                  <Point X="21.7043515625" Y="-0.04611523819" />
                  <Point X="21.704353515625" Y="-0.016474779129" />
                  <Point X="21.70328125" Y="-0.00223417902" />
                  <Point X="21.699013671875" Y="0.02592599678" />
                  <Point X="21.695818359375" Y="0.039845573425" />
                  <Point X="21.68944140625" Y="0.060396701813" />
                  <Point X="21.6894375" Y="0.060407848358" />
                  <Point X="21.680666015625" Y="0.084269851685" />
                  <Point X="21.666556640625" Y="0.112267433167" />
                  <Point X="21.65833984375" Y="0.12567931366" />
                  <Point X="21.63909375" Y="0.151934432983" />
                  <Point X="21.62877734375" Y="0.163805252075" />
                  <Point X="21.606326171875" Y="0.185684768677" />
                  <Point X="21.59419140625" Y="0.195693328857" />
                  <Point X="21.575056640625" Y="0.208973968506" />
                  <Point X="21.55775" Y="0.219659805298" />
                  <Point X="21.53356640625" Y="0.23285975647" />
                  <Point X="21.5233515625" Y="0.237670227051" />
                  <Point X="21.50242578125" Y="0.246045883179" />
                  <Point X="21.491712890625" Y="0.249611190796" />
                  <Point X="21.03096875" Y="0.373067169189" />
                  <Point X="20.2145546875" Y="0.591824768066" />
                  <Point X="20.2435703125" Y="0.787913085938" />
                  <Point X="20.26866796875" Y="0.957521972656" />
                  <Point X="20.3664140625" Y="1.318236938477" />
                  <Point X="20.488607421875" Y="1.302149902344" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.204703369141" />
                  <Point X="21.28485546875" Y="1.20658972168" />
                  <Point X="21.295109375" Y="1.208053588867" />
                  <Point X="21.315396484375" Y="1.212088745117" />
                  <Point X="21.325431640625" Y="1.214660522461" />
                  <Point X="21.367783203125" Y="1.228014160156" />
                  <Point X="21.396548828125" Y="1.237676269531" />
                  <Point X="21.415486328125" Y="1.246008789062" />
                  <Point X="21.42473046875" Y="1.250693603516" />
                  <Point X="21.443474609375" Y="1.261516723633" />
                  <Point X="21.4521484375" Y="1.267177612305" />
                  <Point X="21.468826171875" Y="1.279407592773" />
                  <Point X="21.484072265625" Y="1.293378662109" />
                  <Point X="21.497708984375" Y="1.308928222656" />
                  <Point X="21.504103515625" Y="1.317075683594" />
                  <Point X="21.51651953125" Y="1.334807128906" />
                  <Point X="21.521990234375" Y="1.343604614258" />
                  <Point X="21.531939453125" Y="1.361739990234" />
                  <Point X="21.53641796875" Y="1.371077880859" />
                  <Point X="21.553412109375" Y="1.412104736328" />
                  <Point X="21.564501953125" Y="1.440356079102" />
                  <Point X="21.57029296875" Y="1.460226074219" />
                  <Point X="21.572646484375" Y="1.470322143555" />
                  <Point X="21.57640234375" Y="1.491637573242" />
                  <Point X="21.577640625" Y="1.501922607422" />
                  <Point X="21.578990234375" Y="1.522556884766" />
                  <Point X="21.578087890625" Y="1.543203857422" />
                  <Point X="21.574943359375" Y="1.563641601562" />
                  <Point X="21.5728125" Y="1.573781494141" />
                  <Point X="21.5672109375" Y="1.594687988281" />
                  <Point X="21.56398828125" Y="1.604531005859" />
                  <Point X="21.5564921875" Y="1.623808349609" />
                  <Point X="21.55221875" Y="1.633243286133" />
                  <Point X="21.53171484375" Y="1.67263293457" />
                  <Point X="21.522478515625" Y="1.690373046875" />
                  <Point X="21.517197265625" Y="1.69929309082" />
                  <Point X="21.50569921875" Y="1.716498657227" />
                  <Point X="21.499482421875" Y="1.724784057617" />
                  <Point X="21.4855703125" Y="1.741361572266" />
                  <Point X="21.4784921875" Y="1.748919067383" />
                  <Point X="21.4635546875" Y="1.763217407227" />
                  <Point X="21.4556953125" Y="1.769958496094" />
                  <Point X="21.191412109375" Y="1.972750854492" />
                  <Point X="20.77238671875" Y="2.294280517578" />
                  <Point X="20.900185546875" Y="2.513231201172" />
                  <Point X="20.997716796875" Y="2.680322753906" />
                  <Point X="21.2734375" Y="3.034725341797" />
                  <Point X="21.273662109375" Y="3.035013671875" />
                  <Point X="21.295455078125" Y="3.022431152344" />
                  <Point X="21.74584375" Y="2.762398925781" />
                  <Point X="21.75508203125" Y="2.757716796875" />
                  <Point X="21.774015625" Y="2.749385742188" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.903912109375" Y="2.725997558594" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310546875" />
                  <Point X="22.023568359375" Y="2.734227294922" />
                  <Point X="22.04301171875" Y="2.7413046875" />
                  <Point X="22.0615625" Y="2.750454345703" />
                  <Point X="22.070583984375" Y="2.755533691406" />
                  <Point X="22.088837890625" Y="2.767163330078" />
                  <Point X="22.09725390625" Y="2.773194091797" />
                  <Point X="22.1133828125" Y="2.786138427734" />
                  <Point X="22.121095703125" Y="2.793052001953" />
                  <Point X="22.16296484375" Y="2.834919433594" />
                  <Point X="22.1818203125" Y="2.853775390625" />
                  <Point X="22.188734375" Y="2.861489013672" />
                  <Point X="22.20168359375" Y="2.877623291016" />
                  <Point X="22.20771875" Y="2.886043945312" />
                  <Point X="22.21934765625" Y="2.904298339844" />
                  <Point X="22.2244296875" Y="2.913326171875" />
                  <Point X="22.233576171875" Y="2.931875" />
                  <Point X="22.240646484375" Y="2.951299316406" />
                  <Point X="22.245564453125" Y="2.971387939453" />
                  <Point X="22.2474765625" Y="2.981573242188" />
                  <Point X="22.250302734375" Y="3.003031738281" />
                  <Point X="22.251091796875" Y="3.013364746094" />
                  <Point X="22.25154296875" Y="3.034048828125" />
                  <Point X="22.251205078125" Y="3.044399902344" />
                  <Point X="22.246044921875" Y="3.103384277344" />
                  <Point X="22.243720703125" Y="3.129949462891" />
                  <Point X="22.242255859375" Y="3.140206054688" />
                  <Point X="22.23821875" Y="3.160499267578" />
                  <Point X="22.235646484375" Y="3.170535888672" />
                  <Point X="22.22913671875" Y="3.191177978516" />
                  <Point X="22.225486328125" Y="3.200872802734" />
                  <Point X="22.21715625" Y="3.219800537109" />
                  <Point X="22.2124765625" Y="3.229033447266" />
                  <Point X="22.09536328125" Y="3.431877685547" />
                  <Point X="21.94061328125" Y="3.699914550781" />
                  <Point X="22.18178125" Y="3.884815673828" />
                  <Point X="22.351634765625" Y="4.015041992188" />
                  <Point X="22.78588671875" Y="4.256303222656" />
                  <Point X="22.807474609375" Y="4.268297363281" />
                  <Point X="22.881435546875" Y="4.171909179688" />
                  <Point X="22.8881796875" Y="4.164048828125" />
                  <Point X="22.902482421875" Y="4.149107421875" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.934908203125" Y="4.121897460938" />
                  <Point X="22.952107421875" Y="4.110405273438" />
                  <Point X="22.96101953125" Y="4.105129394531" />
                  <Point X="23.02666796875" Y="4.070954345703" />
                  <Point X="23.056236328125" Y="4.055562255859" />
                  <Point X="23.065673828125" Y="4.051286621094" />
                  <Point X="23.084955078125" Y="4.0437890625" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031377929688" />
                  <Point X="23.2191796875" Y="4.035135742188" />
                  <Point X="23.22926953125" Y="4.037488037109" />
                  <Point X="23.249130859375" Y="4.043277099609" />
                  <Point X="23.25890234375" Y="4.046713867188" />
                  <Point X="23.327279296875" Y="4.075037597656" />
                  <Point X="23.358076171875" Y="4.087793457031" />
                  <Point X="23.3674140625" Y="4.092271972656" />
                  <Point X="23.385546875" Y="4.102219726562" />
                  <Point X="23.394341796875" Y="4.107688964844" />
                  <Point X="23.412072265625" Y="4.120103515625" />
                  <Point X="23.420220703125" Y="4.126497558594" />
                  <Point X="23.4357734375" Y="4.140136230469" />
                  <Point X="23.449751953125" Y="4.155391113281" />
                  <Point X="23.461982421875" Y="4.172072753906" />
                  <Point X="23.467638671875" Y="4.180744140625" />
                  <Point X="23.4784609375" Y="4.199488769531" />
                  <Point X="23.483142578125" Y="4.208723144531" />
                  <Point X="23.491474609375" Y="4.22765625" />
                  <Point X="23.495125" Y="4.237354980469" />
                  <Point X="23.517380859375" Y="4.30794140625" />
                  <Point X="23.527404296875" Y="4.339731445312" />
                  <Point X="23.529974609375" Y="4.349764160156" />
                  <Point X="23.534009765625" Y="4.370049804688" />
                  <Point X="23.535474609375" Y="4.380302734375" />
                  <Point X="23.537361328125" Y="4.401864257812" />
                  <Point X="23.53769921875" Y="4.412217285156" />
                  <Point X="23.537248046875" Y="4.4328984375" />
                  <Point X="23.536458984375" Y="4.4432265625" />
                  <Point X="23.52314453125" Y="4.544359375" />
                  <Point X="23.520736328125" Y="4.562655273438" />
                  <Point X="23.84894921875" Y="4.654675292969" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.5952734375" Y="4.77793359375" />
                  <Point X="24.63477734375" Y="4.782557128906" />
                  <Point X="24.641095703125" Y="4.758977050781" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.29798828125" Y="4.485676757813" />
                  <Point X="25.378193359375" Y="4.785005859375" />
                  <Point X="25.635880859375" Y="4.758019042969" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.263431640625" Y="4.632755859375" />
                  <Point X="26.45359765625" Y="4.586843261719" />
                  <Point X="26.735669921875" Y="4.484534667969" />
                  <Point X="26.858263671875" Y="4.440069335938" />
                  <Point X="27.132396484375" Y="4.311865234375" />
                  <Point X="27.25044921875" Y="4.256654785156" />
                  <Point X="27.515333984375" Y="4.102333984375" />
                  <Point X="27.6294296875" Y="4.035861572266" />
                  <Point X="27.81778125" Y="3.901916259766" />
                  <Point X="27.6593828125" Y="3.627560546875" />
                  <Point X="27.065310546875" Y="2.59859765625" />
                  <Point X="27.0623828125" Y="2.593120361328" />
                  <Point X="27.053185546875" Y="2.573448242188" />
                  <Point X="27.04418359375" Y="2.549567871094" />
                  <Point X="27.041302734375" Y="2.540600097656" />
                  <Point X="27.0265" Y="2.485244628906" />
                  <Point X="27.01983203125" Y="2.460313720703" />
                  <Point X="27.0179140625" Y="2.451470458984" />
                  <Point X="27.013646484375" Y="2.420223876953" />
                  <Point X="27.012755859375" Y="2.383240722656" />
                  <Point X="27.013412109375" Y="2.369581542969" />
                  <Point X="27.01918359375" Y="2.32171484375" />
                  <Point X="27.021783203125" Y="2.300156738281" />
                  <Point X="27.02380078125" Y="2.289036376953" />
                  <Point X="27.02914453125" Y="2.267107910156" />
                  <Point X="27.032470703125" Y="2.256299804688" />
                  <Point X="27.04073828125" Y="2.234210449219" />
                  <Point X="27.045322265625" Y="2.223882324219" />
                  <Point X="27.05568359375" Y="2.203840820312" />
                  <Point X="27.0614609375" Y="2.194127441406" />
                  <Point X="27.091080078125" Y="2.150477539062" />
                  <Point X="27.104419921875" Y="2.130818847656" />
                  <Point X="27.109814453125" Y="2.123628173828" />
                  <Point X="27.13046875" Y="2.100120117188" />
                  <Point X="27.1576015625" Y="2.075385742188" />
                  <Point X="27.168259765625" Y="2.066981933594" />
                  <Point X="27.211908203125" Y="2.037363525391" />
                  <Point X="27.231568359375" Y="2.024024414062" />
                  <Point X="27.241283203125" Y="2.018244995117" />
                  <Point X="27.261330078125" Y="2.007881225586" />
                  <Point X="27.271662109375" Y="2.00329675293" />
                  <Point X="27.293748046875" Y="1.995031860352" />
                  <Point X="27.304548828125" Y="1.991707763672" />
                  <Point X="27.326470703125" Y="1.986365112305" />
                  <Point X="27.337591796875" Y="1.984346557617" />
                  <Point X="27.385458984375" Y="1.978574584961" />
                  <Point X="27.407015625" Y="1.975974975586" />
                  <Point X="27.416044921875" Y="1.975320800781" />
                  <Point X="27.44757421875" Y="1.975496948242" />
                  <Point X="27.484314453125" Y="1.979822509766" />
                  <Point X="27.49775" Y="1.982395751953" />
                  <Point X="27.55310546875" Y="1.997198730469" />
                  <Point X="27.578037109375" Y="2.003865356445" />
                  <Point X="27.584" Y="2.00567175293" />
                  <Point X="27.60440234375" Y="2.01306640625" />
                  <Point X="27.627654296875" Y="2.023573486328" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.099455078125" Y="2.295428222656" />
                  <Point X="28.94040625" Y="2.780951416016" />
                  <Point X="28.976271484375" Y="2.731106689453" />
                  <Point X="29.04394921875" Y="2.637048095703" />
                  <Point X="29.136884765625" Y="2.483470458984" />
                  <Point X="28.95510546875" Y="2.343985595703" />
                  <Point X="28.172953125" Y="1.743819946289" />
                  <Point X="28.16813671875" Y="1.7398671875" />
                  <Point X="28.1521171875" Y="1.725213500977" />
                  <Point X="28.13466796875" Y="1.706600830078" />
                  <Point X="28.128578125" Y="1.69942175293" />
                  <Point X="28.08873828125" Y="1.647448242188" />
                  <Point X="28.070796875" Y="1.624040649414" />
                  <Point X="28.065642578125" Y="1.616608520508" />
                  <Point X="28.049744140625" Y="1.589379760742" />
                  <Point X="28.034765625" Y="1.555552124023" />
                  <Point X="28.030140625" Y="1.542674438477" />
                  <Point X="28.01530078125" Y="1.489609008789" />
                  <Point X="28.008615234375" Y="1.465709960938" />
                  <Point X="28.006224609375" Y="1.454663452148" />
                  <Point X="28.002771484375" Y="1.432366210938" />
                  <Point X="28.001708984375" Y="1.421115356445" />
                  <Point X="28.000892578125" Y="1.397541992188" />
                  <Point X="28.001173828125" Y="1.386237670898" />
                  <Point X="28.003078125" Y="1.363750488281" />
                  <Point X="28.004701171875" Y="1.352567504883" />
                  <Point X="28.016884765625" Y="1.293525512695" />
                  <Point X="28.02237109375" Y="1.266934448242" />
                  <Point X="28.02459765625" Y="1.258238525391" />
                  <Point X="28.03468359375" Y="1.228607788086" />
                  <Point X="28.050287109375" Y="1.195370605469" />
                  <Point X="28.056919921875" Y="1.183526977539" />
                  <Point X="28.0900546875" Y="1.133163818359" />
                  <Point X="28.1049765625" Y="1.110481567383" />
                  <Point X="28.111736328125" Y="1.101428344727" />
                  <Point X="28.126287109375" Y="1.084185546875" />
                  <Point X="28.134078125" Y="1.075995849609" />
                  <Point X="28.151322265625" Y="1.059905029297" />
                  <Point X="28.16003125" Y="1.052697875977" />
                  <Point X="28.178244140625" Y="1.039369750977" />
                  <Point X="28.187748046875" Y="1.033249389648" />
                  <Point X="28.235765625" Y="1.006220031738" />
                  <Point X="28.257390625" Y="0.994046813965" />
                  <Point X="28.265478515625" Y="0.989988037109" />
                  <Point X="28.2946796875" Y="0.978083496094" />
                  <Point X="28.33027734375" Y="0.968020996094" />
                  <Point X="28.343671875" Y="0.965257629395" />
                  <Point X="28.40859375" Y="0.956677368164" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.444033203125" Y="0.952199829102" />
                  <Point X="28.46571875" Y="0.951222900391" />
                  <Point X="28.49121875" Y="0.952032287598" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="28.940822265625" Y="1.010753051758" />
                  <Point X="29.704703125" Y="1.111319702148" />
                  <Point X="29.72361328125" Y="1.033639892578" />
                  <Point X="29.752689453125" Y="0.914203674316" />
                  <Point X="29.783873046875" Y="0.713921020508" />
                  <Point X="29.58835546875" Y="0.661532348633" />
                  <Point X="28.6919921875" Y="0.42135269165" />
                  <Point X="28.686033203125" Y="0.419544525146" />
                  <Point X="28.665623046875" Y="0.412136138916" />
                  <Point X="28.642380859375" Y="0.401618225098" />
                  <Point X="28.634005859375" Y="0.397316497803" />
                  <Point X="28.57022265625" Y="0.360448516846" />
                  <Point X="28.54149609375" Y="0.343844055176" />
                  <Point X="28.53388671875" Y="0.338947967529" />
                  <Point X="28.50877734375" Y="0.319873565674" />
                  <Point X="28.481994140625" Y="0.294351745605" />
                  <Point X="28.472796875" Y="0.284226196289" />
                  <Point X="28.43452734375" Y="0.235460952759" />
                  <Point X="28.417291015625" Y="0.213498352051" />
                  <Point X="28.41085546875" Y="0.20421031189" />
                  <Point X="28.399130859375" Y="0.184930511475" />
                  <Point X="28.393841796875" Y="0.174938583374" />
                  <Point X="28.384068359375" Y="0.153475509644" />
                  <Point X="28.38000390625" Y="0.14292640686" />
                  <Point X="28.373158203125" Y="0.121424530029" />
                  <Point X="28.370376953125" Y="0.11047177124" />
                  <Point X="28.35762109375" Y="0.043861064911" />
                  <Point X="28.351875" Y="0.013861235619" />
                  <Point X="28.350603515625" Y="0.004967046261" />
                  <Point X="28.3485859375" Y="-0.026260259628" />
                  <Point X="28.35028125" Y="-0.062939945221" />
                  <Point X="28.351875" Y="-0.076421531677" />
                  <Point X="28.364630859375" Y="-0.143032089233" />
                  <Point X="28.370376953125" Y="-0.173031768799" />
                  <Point X="28.373158203125" Y="-0.183984527588" />
                  <Point X="28.38000390625" Y="-0.205486404419" />
                  <Point X="28.384068359375" Y="-0.216035507202" />
                  <Point X="28.393841796875" Y="-0.237498580933" />
                  <Point X="28.399130859375" Y="-0.24748991394" />
                  <Point X="28.41085546875" Y="-0.266770019531" />
                  <Point X="28.417291015625" Y="-0.276058807373" />
                  <Point X="28.455560546875" Y="-0.324823760986" />
                  <Point X="28.472796875" Y="-0.346786499023" />
                  <Point X="28.47871875" Y="-0.353633850098" />
                  <Point X="28.50114453125" Y="-0.3758097229" />
                  <Point X="28.5301796875" Y="-0.398726501465" />
                  <Point X="28.54149609375" Y="-0.406404205322" />
                  <Point X="28.605279296875" Y="-0.443272033691" />
                  <Point X="28.634005859375" Y="-0.459876647949" />
                  <Point X="28.639494140625" Y="-0.462813568115" />
                  <Point X="28.65915625" Y="-0.472015869141" />
                  <Point X="28.68302734375" Y="-0.481027618408" />
                  <Point X="28.6919921875" Y="-0.483912963867" />
                  <Point X="29.095693359375" Y="-0.592083984375" />
                  <Point X="29.784880859375" Y="-0.776751281738" />
                  <Point X="29.77784765625" Y="-0.823396362305" />
                  <Point X="29.761619140625" Y="-0.931036376953" />
                  <Point X="29.7278046875" Y="-1.079219848633" />
                  <Point X="29.476310546875" Y="-1.046110107422" />
                  <Point X="28.436783203125" Y="-0.909253601074" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042480469" />
                  <Point X="28.36672265625" Y="-0.910840881348" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.229298828125" Y="-0.939885803223" />
                  <Point X="28.17291796875" Y="-0.952140075684" />
                  <Point X="28.157875" Y="-0.956742614746" />
                  <Point X="28.12875390625" Y="-0.968367614746" />
                  <Point X="28.11467578125" Y="-0.975390014648" />
                  <Point X="28.086849609375" Y="-0.992282531738" />
                  <Point X="28.074126953125" Y="-1.001529968262" />
                  <Point X="28.050376953125" Y="-1.022000244141" />
                  <Point X="28.039349609375" Y="-1.033223022461" />
                  <Point X="27.96368359375" Y="-1.124225708008" />
                  <Point X="27.92960546875" Y="-1.165211181641" />
                  <Point X="27.921326171875" Y="-1.17684753418" />
                  <Point X="27.90660546875" Y="-1.201229980469" />
                  <Point X="27.9001640625" Y="-1.213976074219" />
                  <Point X="27.8888203125" Y="-1.241362548828" />
                  <Point X="27.884361328125" Y="-1.254934204102" />
                  <Point X="27.87753125" Y="-1.282583374023" />
                  <Point X="27.87516015625" Y="-1.29666027832" />
                  <Point X="27.864314453125" Y="-1.414512695312" />
                  <Point X="27.859431640625" Y="-1.467590576172" />
                  <Point X="27.859291015625" Y="-1.483319946289" />
                  <Point X="27.861609375" Y="-1.514591796875" />
                  <Point X="27.864068359375" Y="-1.530134277344" />
                  <Point X="27.87180078125" Y="-1.561754882812" />
                  <Point X="27.876791015625" Y="-1.576674804688" />
                  <Point X="27.889162109375" Y="-1.605482788086" />
                  <Point X="27.89654296875" Y="-1.61937097168" />
                  <Point X="27.9658203125" Y="-1.727129516602" />
                  <Point X="27.9970234375" Y="-1.775661499023" />
                  <Point X="28.0017421875" Y="-1.782350585938" />
                  <Point X="28.019796875" Y="-1.804452026367" />
                  <Point X="28.0434921875" Y="-1.82812121582" />
                  <Point X="28.052798828125" Y="-1.836277587891" />
                  <Point X="28.42743359375" Y="-2.12374609375" />
                  <Point X="29.087171875" Y="-2.629980712891" />
                  <Point X="29.04546484375" Y="-2.697464355469" />
                  <Point X="29.001275390625" Y="-2.760252685547" />
                  <Point X="28.774294921875" Y="-2.629205078125" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.815029296875" Y="-2.079513916016" />
                  <Point X="27.783123046875" Y="-2.069326416016" />
                  <Point X="27.771111328125" Y="-2.066337646484" />
                  <Point X="27.622123046875" Y="-2.039430297852" />
                  <Point X="27.555021484375" Y="-2.027311889648" />
                  <Point X="27.539361328125" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.44484375" Y="-2.035136474609" />
                  <Point X="27.415068359375" Y="-2.044960083008" />
                  <Point X="27.40058984375" Y="-2.051108642578" />
                  <Point X="27.27681640625" Y="-2.116249755859" />
                  <Point X="27.221072265625" Y="-2.145587646484" />
                  <Point X="27.20896875" Y="-2.153171630859" />
                  <Point X="27.186037109375" Y="-2.170066162109" />
                  <Point X="27.175208984375" Y="-2.179376708984" />
                  <Point X="27.15425" Y="-2.200336669922" />
                  <Point X="27.14494140625" Y="-2.211160888672" />
                  <Point X="27.128048828125" Y="-2.234089355469" />
                  <Point X="27.12046484375" Y="-2.246193603516" />
                  <Point X="27.055322265625" Y="-2.369967041016" />
                  <Point X="27.025984375" Y="-2.425711669922" />
                  <Point X="27.0198359375" Y="-2.440193603516" />
                  <Point X="27.01001171875" Y="-2.469972412109" />
                  <Point X="27.0063359375" Y="-2.485269287109" />
                  <Point X="27.00137890625" Y="-2.517442382812" />
                  <Point X="27.000279296875" Y="-2.533133789062" />
                  <Point X="27.00068359375" Y="-2.564484375" />
                  <Point X="27.0021875" Y="-2.580143554688" />
                  <Point X="27.029095703125" Y="-2.7291328125" />
                  <Point X="27.04121484375" Y="-2.796233886719" />
                  <Point X="27.04301953125" Y="-2.804229248047" />
                  <Point X="27.051236328125" Y="-2.831537597656" />
                  <Point X="27.064068359375" Y="-2.862475097656" />
                  <Point X="27.069546875" Y="-2.873578125" />
                  <Point X="27.310287109375" Y="-3.290554443359" />
                  <Point X="27.73589453125" Y="-4.027724853516" />
                  <Point X="27.723755859375" Y="-4.036082275391" />
                  <Point X="27.54041796875" Y="-3.797152832031" />
                  <Point X="26.83391796875" Y="-2.876422363281" />
                  <Point X="26.828654296875" Y="-2.870143066406" />
                  <Point X="26.808830078125" Y="-2.849624023438" />
                  <Point X="26.783251953125" Y="-2.828003662109" />
                  <Point X="26.77330078125" Y="-2.820646728516" />
                  <Point X="26.62635546875" Y="-2.726175537109" />
                  <Point X="26.56017578125" Y="-2.683627929688" />
                  <Point X="26.546283203125" Y="-2.676245605469" />
                  <Point X="26.51747265625" Y="-2.663874511719" />
                  <Point X="26.5025546875" Y="-2.658885742188" />
                  <Point X="26.47093359375" Y="-2.651154052734" />
                  <Point X="26.45539453125" Y="-2.648695800781" />
                  <Point X="26.424125" Y="-2.646376953125" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.2476875" Y="-2.661304931641" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670339111328" />
                  <Point X="26.133578125" Y="-2.677170410156" />
                  <Point X="26.12001171875" Y="-2.681627929688" />
                  <Point X="26.092626953125" Y="-2.692970703125" />
                  <Point X="26.07987890625" Y="-2.699412841797" />
                  <Point X="26.05549609375" Y="-2.714134033203" />
                  <Point X="26.043861328125" Y="-2.722413085938" />
                  <Point X="25.919767578125" Y="-2.825593505859" />
                  <Point X="25.863876953125" Y="-2.872063476562" />
                  <Point X="25.852654296875" Y="-2.883088134766" />
                  <Point X="25.83218359375" Y="-2.906838134766" />
                  <Point X="25.822935546875" Y="-2.919563476563" />
                  <Point X="25.80604296875" Y="-2.947390380859" />
                  <Point X="25.799021484375" Y="-2.961466552734" />
                  <Point X="25.787396484375" Y="-2.990586914062" />
                  <Point X="25.78279296875" Y="-3.005631103516" />
                  <Point X="25.745689453125" Y="-3.176337158203" />
                  <Point X="25.728978515625" Y="-3.253218994141" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323170166016" />
                  <Point X="25.7255546875" Y="-3.335520507812" />
                  <Point X="25.793779296875" Y="-3.853735107422" />
                  <Point X="25.833087890625" Y="-4.152314941406" />
                  <Point X="25.655068359375" Y="-3.487937255859" />
                  <Point X="25.652607421875" Y="-3.480121337891" />
                  <Point X="25.642146484375" Y="-3.453579833984" />
                  <Point X="25.6267890625" Y="-3.423815917969" />
                  <Point X="25.62041015625" Y="-3.413209472656" />
                  <Point X="25.5075234375" Y="-3.250561523438" />
                  <Point X="25.456681640625" Y="-3.177309082031" />
                  <Point X="25.446671875" Y="-3.165172119141" />
                  <Point X="25.424787109375" Y="-3.142716064453" />
                  <Point X="25.412912109375" Y="-3.132396972656" />
                  <Point X="25.38665625" Y="-3.113153320312" />
                  <Point X="25.3732421875" Y="-3.104936523438" />
                  <Point X="25.3452421875" Y="-3.090829101562" />
                  <Point X="25.33065625" Y="-3.084938476562" />
                  <Point X="25.155970703125" Y="-3.03072265625" />
                  <Point X="25.077296875" Y="-3.006305175781" />
                  <Point X="25.063376953125" Y="-3.003109130859" />
                  <Point X="25.035216796875" Y="-2.99883984375" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.998840087891" />
                  <Point X="24.948935546875" Y="-3.003109619141" />
                  <Point X="24.935015625" Y="-3.006305664062" />
                  <Point X="24.76033203125" Y="-3.060521240234" />
                  <Point X="24.68165625" Y="-3.084938964844" />
                  <Point X="24.6670703125" Y="-3.090829345703" />
                  <Point X="24.6390703125" Y="-3.104936767578" />
                  <Point X="24.62565625" Y="-3.113153808594" />
                  <Point X="24.599400390625" Y="-3.132397705078" />
                  <Point X="24.587525390625" Y="-3.142717285156" />
                  <Point X="24.565640625" Y="-3.165173828125" />
                  <Point X="24.555630859375" Y="-3.177310791016" />
                  <Point X="24.44274609375" Y="-3.339958496094" />
                  <Point X="24.391904296875" Y="-3.413211181641" />
                  <Point X="24.38753125" Y="-3.420132080078" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213378906" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.23285546875" Y="-3.952166015625" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.690694497619" Y="0.688953953661" />
                  <Point X="29.654322570092" Y="1.104686987649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.743868103672" Y="-1.008825902986" />
                  <Point X="29.72209228245" Y="-0.75992712749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.597515948363" Y="0.663986886814" />
                  <Point X="29.560045575612" Y="1.092275207163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.653811634194" Y="-1.069478504996" />
                  <Point X="29.624440186198" Y="-0.73376131819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.50433740169" Y="0.639019790445" />
                  <Point X="29.465768581132" Y="1.079863426678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.557337554466" Y="-1.056777486176" />
                  <Point X="29.526788089946" Y="-0.70759550889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.411158855298" Y="0.614052690856" />
                  <Point X="29.371491586652" Y="1.067451646192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.460863474067" Y="-1.044076459697" />
                  <Point X="29.429135993693" Y="-0.681429699591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.317980308907" Y="0.589085591268" />
                  <Point X="29.277214592172" Y="1.055039865706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.364389390154" Y="-1.031375393047" />
                  <Point X="29.331483897441" Y="-0.655263890291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.224801762515" Y="0.564118491679" />
                  <Point X="29.182937597692" Y="1.042628085221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.061913267007" Y="2.425942514854" />
                  <Point X="29.043373977193" Y="2.637847567086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.267915306241" Y="-1.018674326397" />
                  <Point X="29.233831801189" Y="-0.629098080991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.131623216124" Y="0.539151392091" />
                  <Point X="29.088660603212" Y="1.030216304735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.972549610537" Y="2.357371023934" />
                  <Point X="28.935727461171" Y="2.778250117094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.171441222327" Y="-1.005973259747" />
                  <Point X="29.136179704936" Y="-0.602932271691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.038444669732" Y="0.514184292502" />
                  <Point X="28.994383608732" Y="1.017804524249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.883185924085" Y="2.288799875716" />
                  <Point X="28.844949895203" Y="2.725839685683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.074967138414" Y="-0.993272193097" />
                  <Point X="29.038527614007" Y="-0.576766523229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.945266123341" Y="0.489217192914" />
                  <Point X="28.900106616406" Y="1.005392719146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.79382223036" Y="2.22022881062" />
                  <Point X="28.754172329235" Y="2.673429254271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.978493054501" Y="-0.980571126447" />
                  <Point X="28.940875526846" Y="-0.550600817854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.852087576949" Y="0.464250093325" />
                  <Point X="28.805829626913" Y="0.992980881659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.704458536635" Y="2.151657745523" />
                  <Point X="28.663394763268" Y="2.62101882286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.034679596619" Y="-2.712788999902" />
                  <Point X="29.023135925614" Y="-2.580844236548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.882018970588" Y="-0.967870059797" />
                  <Point X="28.843223439686" Y="-0.524435112479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.758909030558" Y="0.439282993737" />
                  <Point X="28.71155263742" Y="0.980569044172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.61509484291" Y="2.083086680427" />
                  <Point X="28.5726171973" Y="2.568608391448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.94039402219" Y="-2.725102711129" />
                  <Point X="28.920910399871" Y="-2.502403888972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.785544886675" Y="-0.955168993147" />
                  <Point X="28.745571352526" Y="-0.498269407104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.665912011605" Y="0.412241026027" />
                  <Point X="28.617275647927" Y="0.968157206684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.525731149185" Y="2.014515615331" />
                  <Point X="28.481839631333" Y="2.516197960037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.83995793541" Y="-2.66711574448" />
                  <Point X="28.818684874128" Y="-2.423963541395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.689070802762" Y="-0.942467926497" />
                  <Point X="28.647431491731" Y="-0.466528423568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.574846552989" Y="0.363121222646" />
                  <Point X="28.522998658434" Y="0.955745369197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.436367455461" Y="1.945944550235" />
                  <Point X="28.391062065365" Y="2.463787528625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.739521852018" Y="-2.609128816571" />
                  <Point X="28.716459348386" Y="-2.345523193819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.592596718848" Y="-0.929766859847" />
                  <Point X="28.547091372974" Y="-0.409638376452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.485230415359" Y="0.297435604585" />
                  <Point X="28.427776029115" Y="0.954142244383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.347003761736" Y="1.877373485139" />
                  <Point X="28.300284499397" Y="2.411377097214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.639085775027" Y="-2.551141961816" />
                  <Point X="28.614233822643" Y="-2.267082846242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.496122634935" Y="-0.917065793197" />
                  <Point X="28.442896333702" Y="-0.30868638622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.399637555115" Y="0.185763715586" />
                  <Point X="28.331215850596" Y="0.967827376883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.257640068011" Y="1.808802420043" />
                  <Point X="28.20950693343" Y="2.358966665802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.538649698036" Y="-2.49315510706" />
                  <Point X="28.512008296901" Y="-2.188642498666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.399971240146" Y="-0.908053080107" />
                  <Point X="28.2323245799" Y="1.008157014886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.168296744818" Y="1.739998518707" />
                  <Point X="28.118729367462" Y="2.306556234391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.438213621045" Y="-2.435168252305" />
                  <Point X="28.409782765197" Y="-2.110202082951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.305936001098" Y="-0.923228137828" />
                  <Point X="28.13071750479" Y="1.079528439386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.081820573239" Y="1.638422924451" />
                  <Point X="28.027951803464" Y="2.254145780466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.337777544054" Y="-2.37718139755" />
                  <Point X="28.307557204929" Y="-2.031761340747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.212352712188" Y="-0.943569009264" />
                  <Point X="28.016446482365" Y="1.295649444054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.003866636558" Y="1.43943773959" />
                  <Point X="27.937174239997" Y="2.201735320473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.792290447857" Y="3.857764642461" />
                  <Point X="27.786480221237" Y="3.92417583661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.237341467062" Y="-2.319194542795" />
                  <Point X="28.205331644661" Y="-1.953320598544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.119560626393" Y="-0.972953373685" />
                  <Point X="27.84639667653" Y="2.14932486048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.709476787479" Y="3.714326353621" />
                  <Point X="27.684790492102" Y="3.996492000948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.136905390071" Y="-2.26120768804" />
                  <Point X="28.103106084393" Y="-1.87487985634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.030411170205" Y="-1.043973185032" />
                  <Point X="27.755619113063" Y="2.096914400487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.62666310053" Y="3.570888368494" />
                  <Point X="27.583649763682" Y="4.062533058387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.03646931308" Y="-2.203220833285" />
                  <Point X="27.999351436767" Y="-1.778961565658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.94412722904" Y="-1.147745982969" />
                  <Point X="27.664841549596" Y="2.044503940494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.5438493729" Y="3.427450848353" />
                  <Point X="27.483165082883" Y="4.121075457207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.936033236089" Y="-2.14523397853" />
                  <Point X="27.888721324344" Y="-1.604456352743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.868307404386" Y="-1.371124179917" />
                  <Point X="27.573148446481" Y="2.002558146562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.46103564527" Y="3.284013328212" />
                  <Point X="27.382680441475" Y="4.179617405785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.835706851987" Y="-2.088500919244" />
                  <Point X="27.479820957014" Y="1.979293474113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.37822191764" Y="3.140575808072" />
                  <Point X="27.282195800067" Y="4.238159354363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.737879858781" Y="-2.060336028614" />
                  <Point X="27.384510965833" Y="1.978688899952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.29540819001" Y="2.997138287931" />
                  <Point X="27.182431747503" Y="4.288464934766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.640986003747" Y="-2.042836956097" />
                  <Point X="27.287514162729" Y="1.997364674288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.21259446238" Y="2.85370076779" />
                  <Point X="27.083000518956" Y="4.334966319245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.719527368872" Y="-4.030571625744" />
                  <Point X="27.716310687718" Y="-3.993804791912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.54417368033" Y="-2.02626979423" />
                  <Point X="27.187184033395" Y="2.054140541785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.12978073475" Y="2.710263247649" />
                  <Point X="26.983569311703" Y="4.381467460342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.611892263405" Y="-3.890299498976" />
                  <Point X="27.603916201524" Y="-3.799132694508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.449488887128" Y="-2.034020413988" />
                  <Point X="27.082254901013" Y="2.163483254656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.047668145011" Y="2.558811684747" />
                  <Point X="26.884138104449" Y="4.427968601438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.504257126779" Y="-3.750027016067" />
                  <Point X="27.491521715329" Y="-3.604460597104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.357600461411" Y="-2.073733660364" />
                  <Point X="26.785404536537" Y="4.466495688377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.396621928567" Y="-3.609753829226" />
                  <Point X="27.379127229135" Y="-3.4097884997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.266435252312" Y="-2.121713310511" />
                  <Point X="26.686916376652" Y="4.502217748725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.288986730355" Y="-3.469480642384" />
                  <Point X="27.266732671121" Y="-3.21511558139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.176053714875" Y="-2.178650368744" />
                  <Point X="26.588428222327" Y="4.537939745525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.181351532143" Y="-3.329207455542" />
                  <Point X="27.154337999591" Y="-3.020441365594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.091427096669" Y="-2.301366454774" />
                  <Point X="26.489940068001" Y="4.573661742325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.073716333931" Y="-3.188934268701" />
                  <Point X="27.037616390181" Y="-2.776310023513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.010646881608" Y="-2.468047129941" />
                  <Point X="26.392125487834" Y="4.601684751279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.966081135719" Y="-3.048661081859" />
                  <Point X="26.294704809297" Y="4.625205443987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.858445937507" Y="-2.908387895017" />
                  <Point X="26.197284145918" Y="4.648725963432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.754340231125" Y="-2.808456984389" />
                  <Point X="26.099863489707" Y="4.672246400962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.653293841667" Y="-2.743494226224" />
                  <Point X="26.002442833495" Y="4.695766838491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.552328513234" Y="-2.679457999801" />
                  <Point X="25.905022177283" Y="4.719287276021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.454266969506" Y="-2.648612184436" />
                  <Point X="25.807846287637" Y="4.740010018904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.35911744759" Y="-2.651050930697" />
                  <Point X="25.711601587755" Y="4.750089214088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.264516186174" Y="-2.659756323141" />
                  <Point X="25.615356883821" Y="4.760168455572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.169950516597" Y="-2.668868532173" />
                  <Point X="25.519112164943" Y="4.770247867877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.077391311413" Y="-2.700914734154" />
                  <Point X="25.422867446065" Y="4.780327280183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.98797472273" Y="-2.768881207117" />
                  <Point X="25.339672690973" Y="4.641244923859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.899078487162" Y="-2.842795343393" />
                  <Point X="25.267782635712" Y="4.372949257203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.812006959273" Y="-2.937565983879" />
                  <Point X="25.189933098124" Y="4.172770785234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.817991774102" Y="-4.095975488729" />
                  <Point X="25.802779594583" Y="-3.922099481191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.739874437147" Y="-3.203090241588" />
                  <Point X="25.100458241365" Y="4.105470319436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.67639624956" Y="-3.567533995718" />
                  <Point X="25.006876366863" Y="4.08511328125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.559904755218" Y="-3.326032880906" />
                  <Point X="24.909648828769" Y="4.106426368596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.450918907331" Y="-3.170321697635" />
                  <Point X="24.806498278389" Y="4.195439796153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.348756220274" Y="-3.092599599521" />
                  <Point X="24.671885665272" Y="4.644066246358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.250547876497" Y="-3.060075851915" />
                  <Point X="24.565119659819" Y="4.774404514497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.152523312086" Y="-3.029652712085" />
                  <Point X="24.470723330256" Y="4.763356740259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.054723384784" Y="-3.001797186159" />
                  <Point X="24.376327000692" Y="4.75230896602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.959337346635" Y="-3.001532539499" />
                  <Point X="24.281930671129" Y="4.741261191782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.86625902965" Y="-3.02764526644" />
                  <Point X="24.187534341565" Y="4.730213417544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.773417115943" Y="-3.05646009522" />
                  <Point X="24.093138012001" Y="4.719165643306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.680583704161" Y="-3.085372101451" />
                  <Point X="23.999719114077" Y="4.696945774304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.590046216783" Y="-3.140526643691" />
                  <Point X="23.906639354026" Y="4.670849541688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.504364032143" Y="-3.251177550182" />
                  <Point X="23.813559602025" Y="4.644753217038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.419676476726" Y="-3.373197120718" />
                  <Point X="23.720479863151" Y="4.61865674236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.339987196528" Y="-3.552347238418" />
                  <Point X="23.627400124277" Y="4.592560267682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.26809715091" Y="-3.820643015295" />
                  <Point X="23.534320385403" Y="4.566463793004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.196207113768" Y="-4.088938889056" />
                  <Point X="23.472037087642" Y="4.188362385657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.124317084777" Y="-4.357234855981" />
                  <Point X="23.384271903647" Y="4.101520270741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.052427055786" Y="-4.625530822906" />
                  <Point X="23.292486843745" Y="4.060625547693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.968644194084" Y="-4.757891089918" />
                  <Point X="23.199655184994" Y="4.031693504231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.871238328536" Y="-4.734539710444" />
                  <Point X="23.866085584068" Y="-4.67564357167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.829984948697" Y="-4.263011421223" />
                  <Point X="23.103725183536" Y="4.038175679961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.715000666259" Y="-4.038737817296" />
                  <Point X="23.004484186668" Y="4.082502706406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.611712878405" Y="-3.948155758224" />
                  <Point X="22.903366614085" Y="4.148279091421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.508425191972" Y="-3.857574858409" />
                  <Point X="22.797965703365" Y="4.263014255364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.408500416744" Y="-3.805432209543" />
                  <Point X="22.707023262811" Y="4.212488349098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.312033848185" Y="-3.792817043785" />
                  <Point X="22.616080805279" Y="4.161962636897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.216120973651" Y="-3.786530629696" />
                  <Point X="22.525138347746" Y="4.111436924696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.120208097283" Y="-3.780244194634" />
                  <Point X="22.434195890214" Y="4.060911212496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.024601541838" Y="-3.777459023751" />
                  <Point X="22.343398508845" Y="4.008727272156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.931408072443" Y="-3.802255552628" />
                  <Point X="22.254030214151" Y="3.940208796379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.840949924666" Y="-3.858316950664" />
                  <Point X="22.243990612507" Y="2.964959209928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.222820998356" Y="3.206929006906" />
                  <Point X="22.164661908805" Y="3.871690442359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.750853895061" Y="-3.918517378351" />
                  <Point X="22.647957036449" Y="-2.742400902625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.621084939354" Y="-2.435251427347" />
                  <Point X="22.1602428666" Y="2.832197567547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.109994821237" Y="3.406535354157" />
                  <Point X="22.075293558502" Y="3.803172602193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.660757919302" Y="-3.978718421489" />
                  <Point X="22.565143330179" Y="-2.88583866691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.519584227788" Y="-2.365095743716" />
                  <Point X="22.071534235673" Y="2.756139100412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.997600140244" Y="3.60120967812" />
                  <Point X="21.985925208199" Y="3.734654762028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.572805716756" Y="-4.06342290459" />
                  <Point X="22.482329623909" Y="-3.029276431195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.422806511988" Y="-2.34892414872" />
                  <Point X="21.978851978886" Y="2.725499384683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.485496853664" Y="-4.155480791296" />
                  <Point X="22.399515917639" Y="-3.17271419548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.329419568596" Y="-2.371509259686" />
                  <Point X="21.883287646571" Y="2.727801942991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.384672171551" Y="-4.093052159666" />
                  <Point X="22.316702211368" Y="-3.316151959765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.238485380181" Y="-2.422129488336" />
                  <Point X="21.786430688002" Y="2.744879286973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.283847559545" Y="-4.030624329378" />
                  <Point X="22.233888505098" Y="-3.45958972405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.147707800076" Y="-2.474539758153" />
                  <Point X="22.05108726575" Y="-1.370161997291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.044651496168" Y="-1.296600814364" />
                  <Point X="21.686539477382" Y="2.796638290616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.182041451621" Y="-3.956977949405" />
                  <Point X="22.151074798828" Y="-3.603027488336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.05693021997" Y="-2.52695002797" />
                  <Point X="21.971841793253" Y="-1.554384860228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.936255030872" Y="-1.14762630493" />
                  <Point X="21.586103401571" Y="2.854625131873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.079790613091" Y="-3.878248275337" />
                  <Point X="22.068261112493" Y="-3.746465480479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.966152639865" Y="-2.579360297787" />
                  <Point X="21.882820614208" Y="-1.626870886031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.837270526406" Y="-1.106231000056" />
                  <Point X="21.485667325761" Y="2.912611973131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.875375067834" Y="-2.631770659894" />
                  <Point X="21.793456930829" Y="-1.695442069386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.741542448403" Y="-1.102056819984" />
                  <Point X="21.661187049926" Y="-0.18359041258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.631019597229" Y="0.16122514959" />
                  <Point X="21.526806698247" Y="1.352384035576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.493614147367" Y="1.731776628189" />
                  <Point X="21.385231249951" Y="2.970598814388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.784597507247" Y="-2.684181152805" />
                  <Point X="21.704093247451" Y="-1.764013252742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.647265450231" Y="-1.114468558268" />
                  <Point X="21.573606238917" Y="-0.272539920367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.529210008769" Y="0.234911312268" />
                  <Point X="21.439589916901" Y="1.259273649698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.390536498174" Y="1.819956791374" />
                  <Point X="21.284795159122" Y="3.028585827303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.69381994666" Y="-2.736591645717" />
                  <Point X="21.614729564072" Y="-1.832584436097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.552988452059" Y="-1.126880296551" />
                  <Point X="21.481939746962" Y="-0.314789881253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.431141096665" Y="0.265841348549" />
                  <Point X="21.347520832309" Y="1.221625343717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.288310940798" Y="1.898397500522" />
                  <Point X="21.19744129958" Y="2.937042252376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.603042386073" Y="-2.789002138629" />
                  <Point X="21.525365880694" Y="-1.901155619453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.458711453887" Y="-1.139292034835" />
                  <Point X="21.388761203793" Y="-0.339757017678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.333489002014" Y="0.292007139548" />
                  <Point X="21.253666015954" Y="1.204388045181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.186085384586" Y="1.97683819637" />
                  <Point X="21.111718395702" Y="2.826856768908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.512264825486" Y="-2.84141263154" />
                  <Point X="21.436002197316" Y="-1.969726802809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.364434455715" Y="-1.151703773119" />
                  <Point X="21.295582660625" Y="-0.364724154102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.235836907363" Y="0.318172930546" />
                  <Point X="21.157453895316" Y="1.214094857889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.08385984954" Y="2.055278650284" />
                  <Point X="21.025995491824" Y="2.71667128544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.421487264899" Y="-2.893823124452" />
                  <Point X="21.346638513937" Y="-2.038297986164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.270157457542" Y="-1.164115511402" />
                  <Point X="21.202404117456" Y="-0.389691290527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.138184812712" Y="0.344338721545" />
                  <Point X="21.060979815888" Y="1.226795873279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.981634314494" Y="2.133719104197" />
                  <Point X="20.9421425243" Y="2.585112331641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.330709704312" Y="-2.946233617363" />
                  <Point X="21.257274830559" Y="-2.10686916952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.175880465927" Y="-1.176527324628" />
                  <Point X="21.109225574287" Y="-0.414658426951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.040532718061" Y="0.370504512543" />
                  <Point X="20.964505736459" Y="1.239496888669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.879408779448" Y="2.212159558111" />
                  <Point X="20.85921019835" Y="2.443030396504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.239932143725" Y="-2.998644110275" />
                  <Point X="21.16791114718" Y="-2.175440352875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.08160347607" Y="-1.188939157955" />
                  <Point X="21.016047031118" Y="-0.439625563376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.942880621625" Y="0.396670323934" />
                  <Point X="20.868031657031" Y="1.252197904059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.777183244402" Y="2.290600012025" />
                  <Point X="20.776277982331" Y="2.300947204852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.13835736574" Y="-2.927641843606" />
                  <Point X="21.078547463802" Y="-2.244011536231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.987326486213" Y="-1.201350991282" />
                  <Point X="20.92286848795" Y="-0.4645926998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.845228524996" Y="0.422836137539" />
                  <Point X="20.771557577602" Y="1.264898919449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.030609759546" Y="-2.786083827646" />
                  <Point X="20.989183771884" Y="-2.312582621983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.893049496356" Y="-1.213762824609" />
                  <Point X="20.829689944781" Y="-0.489559836225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.747576428367" Y="0.449001951144" />
                  <Point X="20.675083498174" Y="1.27759993484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.920140392847" Y="-2.61341594677" />
                  <Point X="20.8998200767" Y="-2.381153670396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.798772506499" Y="-1.226174657936" />
                  <Point X="20.736511401612" Y="-0.514526972649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.649924331738" Y="0.475167764749" />
                  <Point X="20.578609418745" Y="1.29030095023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.704495516642" Y="-1.238586491262" />
                  <Point X="20.643332858443" Y="-0.539494109073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.55227223511" Y="0.501333578354" />
                  <Point X="20.482135339207" Y="1.303001966877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.610218526785" Y="-1.250998324589" />
                  <Point X="20.550154315275" Y="-0.564461245498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.454620138481" Y="0.527499391959" />
                  <Point X="20.385661258138" Y="1.315703001008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.515941536928" Y="-1.263410157916" />
                  <Point X="20.456975771827" Y="-0.589428378738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.356968041852" Y="0.553665205564" />
                  <Point X="20.308707818856" Y="1.105282078543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.421664547071" Y="-1.275821991243" />
                  <Point X="20.363797226443" Y="-0.614395489836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.259315945223" Y="0.579831019169" />
                  <Point X="20.242024868152" Y="0.777468934462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.321090543785" Y="-1.21625863172" />
                  <Point X="20.270618681058" Y="-0.639362600934" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.747732421875" Y="-4.567872558594" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521543945312" />
                  <Point X="25.35143359375" Y="-3.358895996094" />
                  <Point X="25.300591796875" Y="-3.285643554688" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.099650390625" Y="-3.212184082031" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.81665234375" Y="-3.241982177734" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643798828" />
                  <Point X="24.5988359375" Y="-3.448291503906" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.4163828125" Y="-4.001341796875" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="23.991357421875" Y="-4.955845703125" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.697337890625" Y="-4.885985351562" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.657603515625" Y="-4.803573730469" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.534756835938" />
                  <Point X="23.6485859375" Y="-4.324955078125" />
                  <Point X="23.6297890625" Y="-4.230465332031" />
                  <Point X="23.613716796875" Y="-4.202628417969" />
                  <Point X="23.452890625" Y="-4.061586181641" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.1373046875" Y="-3.971772460938" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791259766" />
                  <Point X="22.832259765625" Y="-4.092634765625" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.670431640625" Y="-4.248305175781" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.278435546875" Y="-4.25074609375" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.86387890625" Y="-3.951798339844" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="21.980697265625" Y="-3.518129882812" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597594482422" />
                  <Point X="22.4860234375" Y="-2.568765869141" />
                  <Point X="22.46867578125" Y="-2.551417236328" />
                  <Point X="22.439845703125" Y="-2.537198730469" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="22.007927734375" Y="-2.774634521484" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="20.944375" Y="-2.986494873047" />
                  <Point X="20.838302734375" Y="-2.847136962891" />
                  <Point X="20.637353515625" Y="-2.510175292969" />
                  <Point X="20.56898046875" Y="-2.395526367188" />
                  <Point X="20.93815625" Y="-2.112248046875" />
                  <Point X="21.836212890625" Y="-1.423144897461" />
                  <Point X="21.854177734375" Y="-1.396014038086" />
                  <Point X="21.859490234375" Y="-1.37550402832" />
                  <Point X="21.8618828125" Y="-1.366266601562" />
                  <Point X="21.859673828125" Y="-1.334596801758" />
                  <Point X="21.83884375" Y="-1.310640380859" />
                  <Point X="21.820583984375" Y="-1.299893676758" />
                  <Point X="21.812361328125" Y="-1.295053710938" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.27501953125" Y="-1.355115112305" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.11409765625" Y="-1.173616455078" />
                  <Point X="20.072607421875" Y="-1.011187744141" />
                  <Point X="20.019443359375" Y="-0.63946685791" />
                  <Point X="20.00160546875" Y="-0.5147421875" />
                  <Point X="20.419515625" Y="-0.402763305664" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.4581015625" Y="-0.121426872253" />
                  <Point X="21.477236328125" Y="-0.108146209717" />
                  <Point X="21.48585546875" Y="-0.102164848328" />
                  <Point X="21.505103515625" Y="-0.07590486908" />
                  <Point X="21.51148046875" Y="-0.055353652954" />
                  <Point X="21.5143515625" Y="-0.046102703094" />
                  <Point X="21.514353515625" Y="-0.016462242126" />
                  <Point X="21.5079765625" Y="0.004088969707" />
                  <Point X="21.5051015625" Y="0.013349503517" />
                  <Point X="21.48585546875" Y="0.03960477066" />
                  <Point X="21.466720703125" Y="0.052885433197" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.98179296875" Y="0.189541366577" />
                  <Point X="20.001814453125" Y="0.452126037598" />
                  <Point X="20.0556171875" Y="0.815725341797" />
                  <Point X="20.08235546875" Y="0.996415039063" />
                  <Point X="20.189375" Y="1.391352416992" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.513408203125" Y="1.490524536133" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.3106484375" Y="1.409220092773" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.348466796875" Y="1.426057128906" />
                  <Point X="21.3608828125" Y="1.443788574219" />
                  <Point X="21.377876953125" Y="1.484815429688" />
                  <Point X="21.385529296875" Y="1.50329284668" />
                  <Point X="21.38928515625" Y="1.524608276367" />
                  <Point X="21.38368359375" Y="1.545514526367" />
                  <Point X="21.3631796875" Y="1.584904174805" />
                  <Point X="21.353943359375" Y="1.602644287109" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="21.075748046875" Y="1.822014160156" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.736091796875" Y="2.609010498047" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="21.1234765625" Y="3.151393798828" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.390455078125" Y="3.186975585938" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.920470703125" Y="2.915274414062" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927405029297" />
                  <Point X="22.0286171875" Y="2.969272460938" />
                  <Point X="22.04747265625" Y="2.988128417969" />
                  <Point X="22.0591015625" Y="3.0063828125" />
                  <Point X="22.061927734375" Y="3.027841308594" />
                  <Point X="22.056767578125" Y="3.086825683594" />
                  <Point X="22.054443359375" Y="3.113390869141" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.9308203125" Y="3.336877197266" />
                  <Point X="21.69272265625" Y="3.749276855469" />
                  <Point X="22.06617578125" Y="4.035599365234" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.69361328125" Y="4.422391113281" />
                  <Point X="22.858453125" Y="4.513972167969" />
                  <Point X="22.894296875" Y="4.467259277344" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.11440234375" Y="4.239485839844" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.18619140625" Y="4.22225" />
                  <Point X="23.254568359375" Y="4.250573730469" />
                  <Point X="23.285365234375" Y="4.263329589844" />
                  <Point X="23.303095703125" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.294488769531" />
                  <Point X="23.336173828125" Y="4.365075195313" />
                  <Point X="23.346197265625" Y="4.396865234375" />
                  <Point X="23.348083984375" Y="4.418426757812" />
                  <Point X="23.33476953125" Y="4.519559570312" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.79765625" Y="4.837620605469" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.5731875" Y="4.966645507812" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.824623046875" Y="4.808152832031" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.1144609375" Y="4.534852539062" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.655669921875" Y="4.946985839844" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.308021484375" Y="4.81744921875" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.800455078125" Y="4.663148925781" />
                  <Point X="26.931029296875" Y="4.615788085938" />
                  <Point X="27.212884765625" Y="4.483973632812" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.610978515625" Y="4.266504394531" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="27.9893203125" Y="4.013072753906" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.823927734375" Y="3.532560791016" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491516601562" />
                  <Point X="27.21005078125" Y="2.436161132812" />
                  <Point X="27.2033828125" Y="2.411230224609" />
                  <Point X="27.202044921875" Y="2.392325683594" />
                  <Point X="27.20781640625" Y="2.344458984375" />
                  <Point X="27.210416015625" Y="2.322900878906" />
                  <Point X="27.21868359375" Y="2.300811523438" />
                  <Point X="27.248302734375" Y="2.257161621094" />
                  <Point X="27.261642578125" Y="2.237502929688" />
                  <Point X="27.274943359375" Y="2.224202636719" />
                  <Point X="27.318591796875" Y="2.194584228516" />
                  <Point X="27.338251953125" Y="2.181245117188" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.408205078125" Y="2.167208251953" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4486640625" Y="2.165946044922" />
                  <Point X="27.50401953125" Y="2.180749023438" />
                  <Point X="27.528951171875" Y="2.187415771484" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.004455078125" Y="2.459973144531" />
                  <Point X="28.994248046875" Y="3.031430664062" />
                  <Point X="29.13049609375" Y="2.842078369141" />
                  <Point X="29.202595703125" Y="2.741875244141" />
                  <Point X="29.345751953125" Y="2.505307373047" />
                  <Point X="29.387513671875" Y="2.436295654297" />
                  <Point X="29.07076953125" Y="2.193248779297" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583831665039" />
                  <Point X="28.23953125" Y="1.531858154297" />
                  <Point X="28.22158984375" Y="1.508450561523" />
                  <Point X="28.21312109375" Y="1.491503540039" />
                  <Point X="28.19828125" Y="1.438438354492" />
                  <Point X="28.191595703125" Y="1.4145390625" />
                  <Point X="28.190779296875" Y="1.390965820312" />
                  <Point X="28.202962890625" Y="1.331923950195" />
                  <Point X="28.20844921875" Y="1.305333007812" />
                  <Point X="28.215646484375" Y="1.287956665039" />
                  <Point X="28.24878125" Y="1.237593505859" />
                  <Point X="28.263703125" Y="1.214911132812" />
                  <Point X="28.280947265625" Y="1.19882019043" />
                  <Point X="28.32896484375" Y="1.171790893555" />
                  <Point X="28.35058984375" Y="1.159617675781" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.43348828125" Y="1.145039428711" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="28.916021484375" Y="1.199127563477" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.90822265625" Y="1.078580566406" />
                  <Point X="29.939193359375" Y="0.951366760254" />
                  <Point X="29.984302734375" Y="0.661627197266" />
                  <Point X="29.997859375" Y="0.574556274414" />
                  <Point X="29.63753125" Y="0.478006439209" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819351196" />
                  <Point X="28.6653046875" Y="0.195951385498" />
                  <Point X="28.636578125" Y="0.179346984863" />
                  <Point X="28.622265625" Y="0.166927154541" />
                  <Point X="28.58399609375" Y="0.118162017822" />
                  <Point X="28.566759765625" Y="0.09619947052" />
                  <Point X="28.556986328125" Y="0.074736328125" />
                  <Point X="28.54423046875" Y="0.008125679016" />
                  <Point X="28.538484375" Y="-0.021874082565" />
                  <Point X="28.538484375" Y="-0.040685997009" />
                  <Point X="28.551240234375" Y="-0.107296646118" />
                  <Point X="28.556986328125" Y="-0.137296401978" />
                  <Point X="28.566759765625" Y="-0.158759552002" />
                  <Point X="28.605029296875" Y="-0.207524520874" />
                  <Point X="28.622265625" Y="-0.229487228394" />
                  <Point X="28.636578125" Y="-0.241907058716" />
                  <Point X="28.700361328125" Y="-0.278775024414" />
                  <Point X="28.729087890625" Y="-0.295379425049" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.144869140625" Y="-0.408558044434" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.96572265625" Y="-0.851721252441" />
                  <Point X="29.948431640625" Y="-0.96641418457" />
                  <Point X="29.890634765625" Y="-1.219679321289" />
                  <Point X="29.874546875" Y="-1.290178588867" />
                  <Point X="29.451509765625" Y="-1.234484741211" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.269654296875" Y="-1.125550537109" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.185447265625" Y="-1.154697265625" />
                  <Point X="28.10978125" Y="-1.245699951172" />
                  <Point X="28.075703125" Y="-1.286685424805" />
                  <Point X="28.064359375" Y="-1.314071899414" />
                  <Point X="28.053513671875" Y="-1.431924316406" />
                  <Point X="28.048630859375" Y="-1.485002197266" />
                  <Point X="28.05636328125" Y="-1.516622924805" />
                  <Point X="28.125640625" Y="-1.624381591797" />
                  <Point X="28.15684375" Y="-1.672913452148" />
                  <Point X="28.168462890625" Y="-1.685540771484" />
                  <Point X="28.54309765625" Y="-1.973009277344" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.25287109375" Y="-2.723276367188" />
                  <Point X="29.2041328125" Y="-2.802140136719" />
                  <Point X="29.084603515625" Y="-2.971976318359" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.679294921875" Y="-2.79375" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.588353515625" Y="-2.226405517578" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.3653046875" Y="-2.284385986328" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.334683837891" />
                  <Point X="27.223458984375" Y="-2.458457275391" />
                  <Point X="27.19412109375" Y="-2.514201904297" />
                  <Point X="27.1891640625" Y="-2.546375" />
                  <Point X="27.216072265625" Y="-2.695364257812" />
                  <Point X="27.22819140625" Y="-2.762465332031" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.474833984375" Y="-3.195554931641" />
                  <Point X="27.98667578125" Y="-4.082088867188" />
                  <Point X="27.8931328125" Y="-4.148903808594" />
                  <Point X="27.83530078125" Y="-4.1902109375" />
                  <Point X="27.701658203125" Y="-4.276716308594" />
                  <Point X="27.67977734375" Y="-4.290879394531" />
                  <Point X="27.3896796875" Y="-3.912817871094" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467529297" />
                  <Point X="26.52360546875" Y="-2.885996337891" />
                  <Point X="26.45742578125" Y="-2.843448730469" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.26509765625" Y="-2.850505615234" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868508789062" />
                  <Point X="26.041240234375" Y="-2.971689208984" />
                  <Point X="25.985349609375" Y="-3.018159179688" />
                  <Point X="25.96845703125" Y="-3.045986083984" />
                  <Point X="25.931353515625" Y="-3.216692138672" />
                  <Point X="25.914642578125" Y="-3.293573974609" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="25.982154296875" Y="-3.828934814453" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.04905859375" Y="-4.951254394531" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.870888671875" Y="-4.985674804688" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#186" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.133237562358" Y="4.852373851146" Z="1.75" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.75" />
                  <Point X="-0.438781643995" Y="5.047586503057" Z="1.75" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.75" />
                  <Point X="-1.221998719308" Y="4.917044297182" Z="1.75" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.75" />
                  <Point X="-1.720960238234" Y="4.544313154938" Z="1.75" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.75" />
                  <Point X="-1.717669363323" Y="4.411390252634" Z="1.75" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.75" />
                  <Point X="-1.770720984793" Y="4.328047938753" Z="1.75" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.75" />
                  <Point X="-1.868665913927" Y="4.31511622377" Z="1.75" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.75" />
                  <Point X="-2.072192926071" Y="4.528977152677" Z="1.75" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.75" />
                  <Point X="-2.336826043959" Y="4.497378593855" Z="1.75" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.75" />
                  <Point X="-2.97040389209" Y="4.10655901722" Z="1.75" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.75" />
                  <Point X="-3.118636982412" Y="3.343157344521" Z="1.75" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.75" />
                  <Point X="-2.99920051867" Y="3.113747815525" Z="1.75" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.75" />
                  <Point X="-3.012896106299" Y="3.035907461587" Z="1.75" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.75" />
                  <Point X="-3.081328650081" Y="2.996364180503" Z="1.75" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.75" />
                  <Point X="-3.590702003777" Y="3.261556966333" Z="1.75" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.75" />
                  <Point X="-3.92214327684" Y="3.213376138189" Z="1.75" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.75" />
                  <Point X="-4.313246869247" Y="2.665495621751" Z="1.75" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.75" />
                  <Point X="-3.960846408983" Y="1.813626435788" Z="1.75" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.75" />
                  <Point X="-3.687327362472" Y="1.593093988282" Z="1.75" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.75" />
                  <Point X="-3.674476056207" Y="1.535226899985" Z="1.75" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.75" />
                  <Point X="-3.710544181661" Y="1.488186020896" Z="1.75" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.75" />
                  <Point X="-4.486223036353" Y="1.571376890567" Z="1.75" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.75" />
                  <Point X="-4.865041566031" Y="1.435709747809" Z="1.75" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.75" />
                  <Point X="-5.000000433484" Y="0.85432456427" Z="1.75" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.75" />
                  <Point X="-4.03730521149" Y="0.172525144176" Z="1.75" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.75" />
                  <Point X="-3.567942812324" Y="0.043087701108" Z="1.75" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.75" />
                  <Point X="-3.545935168938" Y="0.020551188964" Z="1.75" />
                  <Point X="-3.539556741714" Y="0" Z="1.75" />
                  <Point X="-3.542429432197" Y="-0.009255762097" Z="1.75" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.75" />
                  <Point X="-3.557425782799" Y="-0.035788282026" Z="1.75" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.75" />
                  <Point X="-4.599582577282" Y="-0.32318691258" Z="1.75" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.75" />
                  <Point X="-5.036210134611" Y="-0.615265974791" Z="1.75" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.75" />
                  <Point X="-4.940505535415" Y="-1.154710719993" Z="1.75" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.75" />
                  <Point X="-3.724611969136" Y="-1.373407455751" Z="1.75" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.75" />
                  <Point X="-3.210935420825" Y="-1.311703282798" Z="1.75" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.75" />
                  <Point X="-3.195068733814" Y="-1.331687063162" Z="1.75" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.75" />
                  <Point X="-4.098437448361" Y="-2.041300242537" Z="1.75" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.75" />
                  <Point X="-4.41174772234" Y="-2.504505184164" Z="1.75" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.75" />
                  <Point X="-4.101567165492" Y="-2.985497953095" Z="1.75" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.75" />
                  <Point X="-2.973228452614" Y="-2.786655637828" Z="1.75" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.75" />
                  <Point X="-2.567452370976" Y="-2.560878098155" Z="1.75" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.75" />
                  <Point X="-3.06876137534" Y="-3.461849929878" Z="1.75" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.75" />
                  <Point X="-3.172781984856" Y="-3.960135573622" Z="1.75" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.75" />
                  <Point X="-2.754044553842" Y="-4.261976816494" Z="1.75" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.75" />
                  <Point X="-2.296057632552" Y="-4.247463354827" Z="1.75" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.75" />
                  <Point X="-2.14611765573" Y="-4.10292790368" Z="1.75" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.75" />
                  <Point X="-1.872121256458" Y="-3.990385378377" Z="1.75" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.75" />
                  <Point X="-1.586233841322" Y="-4.067898064132" Z="1.75" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.75" />
                  <Point X="-1.406610778674" Y="-4.303430271488" Z="1.75" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.75" />
                  <Point X="-1.398125441968" Y="-4.765767450447" Z="1.75" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.75" />
                  <Point X="-1.321278084214" Y="-4.903127986571" Z="1.75" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.75" />
                  <Point X="-1.024327429229" Y="-4.973649643129" Z="1.75" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.75" />
                  <Point X="-0.541476732207" Y="-3.98300273342" Z="1.75" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.75" />
                  <Point X="-0.366245463111" Y="-3.445520434067" Z="1.75" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.75" />
                  <Point X="-0.174685102981" Y="-3.258455177922" Z="1.75" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.75" />
                  <Point X="0.07867397638" Y="-3.228656867366" Z="1.75" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.75" />
                  <Point X="0.304200396321" Y="-3.356125321701" Z="1.75" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.75" />
                  <Point X="0.693278128194" Y="-4.549533296396" Z="1.75" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.75" />
                  <Point X="0.873668553797" Y="-5.003589708679" Z="1.75" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.75" />
                  <Point X="1.053607180462" Y="-4.968814541883" Z="1.75" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.75" />
                  <Point X="1.025570054173" Y="-3.791128415499" Z="1.75" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.75" />
                  <Point X="0.9740563735" Y="-3.196031873522" Z="1.75" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.75" />
                  <Point X="1.067049215526" Y="-2.978855976917" Z="1.75" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.75" />
                  <Point X="1.263522676685" Y="-2.86901506527" Z="1.75" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.75" />
                  <Point X="1.490410316909" Y="-2.896774124765" Z="1.75" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.75" />
                  <Point X="2.343855369905" Y="-3.911975727225" Z="1.75" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.75" />
                  <Point X="2.722669235403" Y="-4.287410801727" Z="1.75" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.75" />
                  <Point X="2.916036764197" Y="-4.158310829063" Z="1.75" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.75" />
                  <Point X="2.511978309192" Y="-3.139274805866" Z="1.75" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.75" />
                  <Point X="2.259118469261" Y="-2.655197438016" Z="1.75" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.75" />
                  <Point X="2.261549099374" Y="-2.450463608503" Z="1.75" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.75" />
                  <Point X="2.382434769869" Y="-2.297352210593" Z="1.75" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.75" />
                  <Point X="2.573309365497" Y="-2.24432954023" Z="1.75" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.75" />
                  <Point X="3.648138791241" Y="-2.805771191083" Z="1.75" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.75" />
                  <Point X="4.119334657233" Y="-2.969473903586" Z="1.75" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.75" />
                  <Point X="4.289246511857" Y="-2.718281999551" Z="1.75" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.75" />
                  <Point X="3.567378917672" Y="-1.902061452437" Z="1.75" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.75" />
                  <Point X="3.161541313283" Y="-1.566061128812" Z="1.75" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.75" />
                  <Point X="3.097146717003" Y="-1.405224614199" Z="1.75" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.75" />
                  <Point X="3.142069582573" Y="-1.246386825945" Z="1.75" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.75" />
                  <Point X="3.274115540781" Y="-1.143129754119" Z="1.75" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.75" />
                  <Point X="4.438828154631" Y="-1.252776910431" Z="1.75" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.75" />
                  <Point X="4.933224760928" Y="-1.199522842931" Z="1.75" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.75" />
                  <Point X="5.009006798996" Y="-0.827895229634" Z="1.75" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.75" />
                  <Point X="4.151652777129" Y="-0.328981741653" Z="1.75" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.75" />
                  <Point X="3.71922616501" Y="-0.204206184939" Z="1.75" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.75" />
                  <Point X="3.6382067939" Y="-0.145375593918" Z="1.75" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.75" />
                  <Point X="3.594191416779" Y="-0.066610680143" Z="1.75" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.75" />
                  <Point X="3.587180033645" Y="0.029999851053" Z="1.75" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.75" />
                  <Point X="3.6171726445" Y="0.118573144657" Z="1.75" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.75" />
                  <Point X="3.684169249342" Y="0.183942659303" Z="1.75" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.75" />
                  <Point X="4.644315040178" Y="0.460990191116" Z="1.75" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.75" />
                  <Point X="5.027550918197" Y="0.700599508361" Z="1.75" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.75" />
                  <Point X="4.950648328322" Y="1.121687517752" Z="1.75" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.75" />
                  <Point X="3.903339575496" Y="1.279979921397" Z="1.75" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.75" />
                  <Point X="3.433882263975" Y="1.225888393029" Z="1.75" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.75" />
                  <Point X="3.347335112815" Y="1.246641866184" Z="1.75" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.75" />
                  <Point X="3.284395546111" Y="1.296353344029" Z="1.75" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.75" />
                  <Point X="3.245774365821" Y="1.373307410625" Z="1.75" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.75" />
                  <Point X="3.24027572885" Y="1.456248498774" Z="1.75" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.75" />
                  <Point X="3.273058714971" Y="1.532721406608" Z="1.75" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.75" />
                  <Point X="4.095049632964" Y="2.184861014768" Z="1.75" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.75" />
                  <Point X="4.382372726462" Y="2.562473965047" Z="1.75" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.75" />
                  <Point X="4.164924371116" Y="2.90256162498" Z="1.75" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.75" />
                  <Point X="2.973298154389" Y="2.534554549576" Z="1.75" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.75" />
                  <Point X="2.484947139353" Y="2.260331952278" Z="1.75" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.75" />
                  <Point X="2.408033595093" Y="2.248128677619" Z="1.75" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.75" />
                  <Point X="2.340507937013" Y="2.267239859768" Z="1.75" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.75" />
                  <Point X="2.283518740467" Y="2.316516923369" Z="1.75" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.75" />
                  <Point X="2.251301025139" Y="2.381724848739" Z="1.75" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.75" />
                  <Point X="2.25219599038" Y="2.454522436529" Z="1.75" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.75" />
                  <Point X="2.861070803283" Y="3.538840927566" Z="1.75" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.75" />
                  <Point X="3.012140251936" Y="4.085099806441" Z="1.75" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.75" />
                  <Point X="2.629991693364" Y="4.340986967909" Z="1.75" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.75" />
                  <Point X="2.227910373406" Y="4.56054566918" Z="1.75" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.75" />
                  <Point X="1.811346468538" Y="4.741432192479" Z="1.75" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.75" />
                  <Point X="1.313599580168" Y="4.897332788331" Z="1.75" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.75" />
                  <Point X="0.654720830782" Y="5.027994142017" Z="1.75" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.75" />
                  <Point X="0.060006999195" Y="4.579073654805" Z="1.75" />
                  <Point X="0" Y="4.355124473572" Z="1.75" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>