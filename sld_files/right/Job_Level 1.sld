<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#117" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="270" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999526367188" Y="0.004717285156" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766446289062" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.56350390625" Y="-3.513271728516" />
                  <Point X="25.558162109375" Y="-3.498408203125" />
                  <Point X="25.542283203125" Y="-3.467258789062" />
                  <Point X="25.37863671875" Y="-3.2314765625" />
                  <Point X="25.356650390625" Y="-3.208946777344" />
                  <Point X="25.330267578125" Y="-3.189663330078" />
                  <Point X="25.302369140625" Y="-3.175629638672" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0208515625" Y="-3.092766601562" />
                  <Point X="24.991083984375" Y="-3.092805908203" />
                  <Point X="24.963048828125" Y="-3.097075195312" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.687126953125" Y="-3.185170654297" />
                  <Point X="24.66102734375" Y="-3.202791503906" />
                  <Point X="24.646166015625" Y="-3.216927246094" />
                  <Point X="24.633595703125" Y="-3.231594482422" />
                  <Point X="24.46994921875" Y="-3.467376953125" />
                  <Point X="24.4618125" Y="-3.481572021484" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.753583984375" Y="-4.802362304688" />
                  <Point X="23.785013671875" Y="-4.563621582031" />
                  <Point X="23.785767578125" Y="-4.547866699219" />
                  <Point X="23.78340234375" Y="-4.515775878906" />
                  <Point X="23.722962890625" Y="-4.211932128906" />
                  <Point X="23.71198828125" Y="-4.182840820313" />
                  <Point X="23.69580078125" Y="-4.154901367187" />
                  <Point X="23.676240234375" Y="-4.1311015625" />
                  <Point X="23.443095703125" Y="-3.926639160156" />
                  <Point X="23.416677734375" Y="-3.910239746094" />
                  <Point X="23.386826171875" Y="-3.897928466797" />
                  <Point X="23.356818359375" Y="-3.890956054688" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.016439453125" Y="-3.873746582031" />
                  <Point X="22.98526171875" Y="-3.88215234375" />
                  <Point X="22.957212890625" Y="-3.894887695312" />
                  <Point X="22.699376953125" Y="-4.067169677734" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461914062" />
                  <Point X="22.5198515625" Y="-4.288489257813" />
                  <Point X="22.198283203125" Y="-4.089381835938" />
                  <Point X="21.89527734375" Y="-3.856077392578" />
                  <Point X="22.575865234375" Y="-2.677264160156" />
                  <Point X="22.577068359375" Y="-2.675142089844" />
                  <Point X="22.587662109375" Y="-2.646074707031" />
                  <Point X="22.593544921875" Y="-2.614807128906" />
                  <Point X="22.594283203125" Y="-2.584191162109" />
                  <Point X="22.5852421875" Y="-2.554931152344" />
                  <Point X="22.571072265625" Y="-2.526445068359" />
                  <Point X="22.553189453125" Y="-2.501580566406" />
                  <Point X="22.535849609375" Y="-2.484240722656" />
                  <Point X="22.51069140625" Y="-2.466213378906" />
                  <Point X="22.48186328125" Y="-2.451996582031" />
                  <Point X="22.452248046875" Y="-2.443011962891" />
                  <Point X="22.42131640625" Y="-2.4440234375" />
                  <Point X="22.389791015625" Y="-2.450292724609" />
                  <Point X="22.3608203125" Y="-2.461195800781" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="20.9171328125" Y="-2.793851074219" />
                  <Point X="20.693857421875" Y="-2.419450195312" />
                  <Point X="21.89339453125" Y="-1.499013305664" />
                  <Point X="21.894095703125" Y="-1.498474487305" />
                  <Point X="21.9154765625" Y="-1.475533569336" />
                  <Point X="21.93341796875" Y="-1.448408813477" />
                  <Point X="21.946146484375" Y="-1.419819702148" />
                  <Point X="21.95384765625" Y="-1.390087158203" />
                  <Point X="21.957888671875" Y="-1.362234985352" />
                  <Point X="21.957931640625" Y="-1.341990234375" />
                  <Point X="21.95368359375" Y="-1.322196166992" />
                  <Point X="21.944982421875" Y="-1.295777709961" />
                  <Point X="21.93478515625" Y="-1.274315917969" />
                  <Point X="21.919580078125" Y="-1.256056030273" />
                  <Point X="21.903658203125" Y="-1.241191162109" />
                  <Point X="21.887015625" Y="-1.228760009766" />
                  <Point X="21.860546875" Y="-1.213181274414" />
                  <Point X="21.83128515625" Y="-1.201957275391" />
                  <Point X="21.799400390625" Y="-1.195475097656" />
                  <Point X="21.76807421875" Y="-1.194383422852" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.165921875" Y="-0.99264831543" />
                  <Point X="20.107578125" Y="-0.584698364258" />
                  <Point X="21.4663828125" Y="-0.220607131958" />
                  <Point X="21.48114453125" Y="-0.215311157227" />
                  <Point X="21.512283203125" Y="-0.199461517334" />
                  <Point X="21.5400234375" Y="-0.180209030151" />
                  <Point X="21.56248828125" Y="-0.158313720703" />
                  <Point X="21.58173828125" Y="-0.132042755127" />
                  <Point X="21.595837890625" Y="-0.104051086426" />
                  <Point X="21.605083984375" Y="-0.074259033203" />
                  <Point X="21.609330078125" Y="-0.048218631744" />
                  <Point X="21.609943359375" Y="-0.020721805573" />
                  <Point X="21.606306640625" Y="0.007509840965" />
                  <Point X="21.605080078125" Y="0.011714131355" />
                  <Point X="21.595833984375" Y="0.041506031036" />
                  <Point X="21.58171484375" Y="0.069523498535" />
                  <Point X="21.562455078125" Y="0.095789154053" />
                  <Point X="21.540009765625" Y="0.117658508301" />
                  <Point X="21.51226953125" Y="0.136910995483" />
                  <Point X="21.498068359375" Y="0.145049987793" />
                  <Point X="21.467119140625" Y="0.157849563599" />
                  <Point X="20.10818359375" Y="0.521975341797" />
                  <Point X="20.17551171875" Y="0.976969482422" />
                  <Point X="20.29644921875" Y="1.423267822266" />
                  <Point X="21.233802734375" Y="1.299863037109" />
                  <Point X="21.23436328125" Y="1.299789428711" />
                  <Point X="21.255634765625" Y="1.299395141602" />
                  <Point X="21.275109375" Y="1.301224853516" />
                  <Point X="21.296892578125" Y="1.305272827148" />
                  <Point X="21.3582890625" Y="1.324630859375" />
                  <Point X="21.39230859375" Y="1.341182739258" />
                  <Point X="21.40928125" Y="1.353035766602" />
                  <Point X="21.423283203125" Y="1.368283935547" />
                  <Point X="21.437451171875" Y="1.387567504883" />
                  <Point X="21.448662109375" Y="1.407461303711" />
                  <Point X="21.473296875" Y="1.466935913086" />
                  <Point X="21.47894140625" Y="1.486004638672" />
                  <Point X="21.48273046875" Y="1.506481323242" />
                  <Point X="21.48166796875" Y="1.546042236328" />
                  <Point X="21.47678515625" Y="1.566286010742" />
                  <Point X="21.469439453125" Y="1.586425537109" />
                  <Point X="21.467935546875" Y="1.589409179688" />
                  <Point X="21.438208984375" Y="1.646510375977" />
                  <Point X="21.426716796875" Y="1.663708251953" />
                  <Point X="21.412802734375" Y="1.680289794922" />
                  <Point X="21.397861328125" Y="1.694592651367" />
                  <Point X="20.648140625" Y="2.269874023438" />
                  <Point X="20.918853515625" Y="2.733665771484" />
                  <Point X="21.24949609375" Y="3.158661621094" />
                  <Point X="21.793017578125" Y="2.844859375" />
                  <Point X="21.79334765625" Y="2.844669433594" />
                  <Point X="21.81234765625" Y="2.836322509766" />
                  <Point X="21.83305078125" Y="2.82981640625" />
                  <Point X="21.833064453125" Y="2.829859375" />
                  <Point X="21.85325" Y="2.825792724609" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959060546875" Y="2.818714355469" />
                  <Point X="21.980146484375" Y="2.821405273438" />
                  <Point X="21.99990625" Y="2.826115722656" />
                  <Point X="22.0182109375" Y="2.834918945312" />
                  <Point X="22.036271484375" Y="2.846125732422" />
                  <Point X="22.052744140625" Y="2.85906640625" />
                  <Point X="22.053955078125" Y="2.860259521484" />
                  <Point X="22.1146484375" Y="2.920952636719" />
                  <Point X="22.136888671875" Y="2.95080078125" />
                  <Point X="22.146529296875" Y="2.969122070312" />
                  <Point X="22.151970703125" Y="2.989096435547" />
                  <Point X="22.1557578125" Y="3.013046630859" />
                  <Point X="22.1565625" Y="3.036163330078" />
                  <Point X="22.14908203125" Y="3.121669921875" />
                  <Point X="22.145044921875" Y="3.141962646484" />
                  <Point X="22.13853515625" Y="3.162604980469" />
                  <Point X="22.130205078125" Y="3.181533203125" />
                  <Point X="21.816666015625" Y="3.724595703125" />
                  <Point X="22.29937890625" Y="4.094685546875" />
                  <Point X="22.83296484375" Y="4.391134277344" />
                  <Point X="22.956705078125" Y="4.229871582031" />
                  <Point X="22.956755859375" Y="4.229806152344" />
                  <Point X="22.970669921875" Y="4.21519921875" />
                  <Point X="22.986943359375" Y="4.201399414062" />
                  <Point X="23.004017578125" Y="4.189852539062" />
                  <Point X="23.004935546875" Y="4.189369628906" />
                  <Point X="23.10010546875" Y="4.139827636719" />
                  <Point X="23.135326171875" Y="4.127578613281" />
                  <Point X="23.15570703125" Y="4.123900878906" />
                  <Point X="23.1763984375" Y="4.1247265625" />
                  <Point X="23.200353515625" Y="4.128324707031" />
                  <Point X="23.22259765625" Y="4.134502441406" />
                  <Point X="23.32172265625" Y="4.175561523437" />
                  <Point X="23.353896484375" Y="4.194792480469" />
                  <Point X="23.369802734375" Y="4.208057617188" />
                  <Point X="23.382455078125" Y="4.224455078125" />
                  <Point X="23.3950234375" Y="4.245047363281" />
                  <Point X="23.404537109375" Y="4.265972167969" />
                  <Point X="23.43680078125" Y="4.368297363281" />
                  <Point X="23.4408359375" Y="4.388583496094" />
                  <Point X="23.44272265625" Y="4.410145507812" />
                  <Point X="23.442271484375" Y="4.430827148438" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.866" Y="4.286676269531" />
                  <Point X="24.878771484375" Y="4.258487792969" />
                  <Point X="24.896626953125" Y="4.231763183594" />
                  <Point X="24.91778125" Y="4.209174804687" />
                  <Point X="24.94507421875" Y="4.194583984375" />
                  <Point X="24.975509765625" Y="4.184249511719" />
                  <Point X="25.00604296875" Y="4.179205078125" />
                  <Point X="25.036576171875" Y="4.184241699219" />
                  <Point X="25.067013671875" Y="4.194568359375" />
                  <Point X="25.094310546875" Y="4.209152832031" />
                  <Point X="25.115470703125" Y="4.231737304688" />
                  <Point X="25.13333203125" Y="4.258457519531" />
                  <Point X="25.146109375" Y="4.286641113281" />
                  <Point X="25.307388671875" Y="4.887940429688" />
                  <Point X="25.844046875" Y="4.83173828125" />
                  <Point X="26.481025390625" Y="4.677950683594" />
                  <Point X="26.89465625" Y="4.527923828125" />
                  <Point X="27.294580078125" Y="4.340893554688" />
                  <Point X="27.6809765625" Y="4.115776367188" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.148013671875" Y="2.551843994141" />
                  <Point X="27.1427890625" Y="2.541351318359" />
                  <Point X="27.13306640625" Y="2.516016357422" />
                  <Point X="27.111607421875" Y="2.435770263672" />
                  <Point X="27.1086171875" Y="2.417903808594" />
                  <Point X="27.107732421875" Y="2.380918212891" />
                  <Point X="27.116099609375" Y="2.311528076172" />
                  <Point X="27.121423828125" Y="2.289655761719" />
                  <Point X="27.129658203125" Y="2.26761328125" />
                  <Point X="27.14020703125" Y="2.247515136719" />
                  <Point X="27.14009375" Y="2.247437744141" />
                  <Point X="27.18303125" Y="2.184160888672" />
                  <Point X="27.194490234375" Y="2.170305175781" />
                  <Point X="27.2216328125" Y="2.145570800781" />
                  <Point X="27.28491015625" Y="2.102634765625" />
                  <Point X="27.30476171875" Y="2.092344482422" />
                  <Point X="27.326634765625" Y="2.084104980469" />
                  <Point X="27.348521484375" Y="2.078717773438" />
                  <Point X="27.349" Y="2.078659179688" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.436505859375" Y="2.069848388672" />
                  <Point X="27.473248046875" Y="2.074181884766" />
                  <Point X="27.553494140625" Y="2.095640869141" />
                  <Point X="27.56527734375" Y="2.099635253906" />
                  <Point X="27.58853125" Y="2.110142578125" />
                  <Point X="28.967326171875" Y="2.906190429688" />
                  <Point X="29.1232734375" Y="2.689460449219" />
                  <Point X="29.26219921875" Y="2.459883789063" />
                  <Point X="28.231388671875" Y="1.668913818359" />
                  <Point X="28.222486328125" Y="1.661159301758" />
                  <Point X="28.203943359375" Y="1.641588745117" />
                  <Point X="28.14619140625" Y="1.566245361328" />
                  <Point X="28.13658984375" Y="1.550879394531" />
                  <Point X="28.121619140625" Y="1.517048339844" />
                  <Point X="28.10010546875" Y="1.440122192383" />
                  <Point X="28.096623046875" Y="1.416842163086" />
                  <Point X="28.0961875" Y="1.399624511719" />
                  <Point X="28.097748046875" Y="1.371725097656" />
                  <Point X="28.115408203125" Y="1.286135131836" />
                  <Point X="28.120693359375" Y="1.268945068359" />
                  <Point X="28.136306640625" Y="1.235704101562" />
                  <Point X="28.18433984375" Y="1.1626953125" />
                  <Point X="28.198998046875" Y="1.145352539062" />
                  <Point X="28.216291015625" Y="1.12926574707" />
                  <Point X="28.21634765625" Y="1.129325561523" />
                  <Point X="28.234380859375" Y="1.116015014648" />
                  <Point X="28.30398828125" Y="1.076832275391" />
                  <Point X="28.320564453125" Y="1.069486816406" />
                  <Point X="28.356166015625" Y="1.059432373047" />
                  <Point X="28.45028125" Y="1.046993896484" />
                  <Point X="28.462705078125" Y="1.046174926758" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.776837890625" Y="1.216636352539" />
                  <Point X="29.84594140625" Y="0.932782104492" />
                  <Point X="29.890865234375" Y="0.644238769531" />
                  <Point X="28.71723046875" Y="0.329764007568" />
                  <Point X="28.70595703125" Y="0.325972595215" />
                  <Point X="28.6815" Y="0.31504095459" />
                  <Point X="28.589037109375" Y="0.261595428467" />
                  <Point X="28.57428125" Y="0.251068756104" />
                  <Point X="28.54750390625" Y="0.225541549683" />
                  <Point X="28.492025390625" Y="0.15484942627" />
                  <Point X="28.48050390625" Y="0.136009353638" />
                  <Point X="28.470830078125" Y="0.115049232483" />
                  <Point X="28.46389453125" Y="0.093688072205" />
                  <Point X="28.463671875" Y="0.09255480957" />
                  <Point X="28.4451796875" Y="-0.004007406235" />
                  <Point X="28.443484375" Y="-0.02192489624" />
                  <Point X="28.445189453125" Y="-0.058601097107" />
                  <Point X="28.463681640625" Y="-0.155163314819" />
                  <Point X="28.47038671875" Y="-0.17635218811" />
                  <Point X="28.479939453125" Y="-0.19753302002" />
                  <Point X="28.491572265625" Y="-0.21682723999" />
                  <Point X="28.492052734375" Y="-0.217444732666" />
                  <Point X="28.54753125" Y="-0.288137145996" />
                  <Point X="28.560041015625" Y="-0.301272491455" />
                  <Point X="28.589083984375" Y="-0.324182067871" />
                  <Point X="28.681546875" Y="-0.377627746582" />
                  <Point X="28.692708984375" Y="-0.383137756348" />
                  <Point X="28.716580078125" Y="-0.392149993896" />
                  <Point X="29.891474609375" Y="-0.706961914062" />
                  <Point X="29.855025390625" Y="-0.948720214844" />
                  <Point X="29.80117578125" Y="-1.18469909668" />
                  <Point X="28.42513671875" Y="-1.003539978027" />
                  <Point X="28.409390625" Y="-1.002786193848" />
                  <Point X="28.374568359375" Y="-1.005528564453" />
                  <Point X="28.193095703125" Y="-1.04497265625" />
                  <Point X="28.169939453125" Y="-1.0523515625" />
                  <Point X="28.14236328125" Y="-1.067618041992" />
                  <Point X="28.1261640625" Y="-1.080512329102" />
                  <Point X="28.112279296875" Y="-1.094103271484" />
                  <Point X="28.002654296875" Y="-1.225948120117" />
                  <Point X="27.987892578125" Y="-1.250434204102" />
                  <Point X="27.976537109375" Y="-1.277940673828" />
                  <Point X="27.969748046875" Y="-1.305487182617" />
                  <Point X="27.95403125" Y="-1.476296508789" />
                  <Point X="27.956369140625" Y="-1.507642944336" />
                  <Point X="27.9641484375" Y="-1.539342285156" />
                  <Point X="27.9765" Y="-1.568075805664" />
                  <Point X="28.076931640625" Y="-1.724288085938" />
                  <Point X="28.0869375" Y="-1.737243286133" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="29.213123046875" Y="-2.606882080078" />
                  <Point X="29.124802734375" Y="-2.749797119141" />
                  <Point X="29.028982421875" Y="-2.8859453125" />
                  <Point X="27.801626953125" Y="-2.177330810547" />
                  <Point X="27.787369140625" Y="-2.170609375" />
                  <Point X="27.7541171875" Y="-2.159805664062" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.506689453125" Y="-2.120409912109" />
                  <Point X="27.47442578125" Y="-2.125414794922" />
                  <Point X="27.444744140625" Y="-2.135224121094" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.24754296875" Y="-2.241760498047" />
                  <Point X="27.2305078125" Y="-2.25644140625" />
                  <Point X="27.211828125" Y="-2.278275146484" />
                  <Point X="27.20440234375" Y="-2.290687011719" />
                  <Point X="27.110052734375" Y="-2.469956542969" />
                  <Point X="27.1002109375" Y="-2.499856933594" />
                  <Point X="27.09527734375" Y="-2.532156494141" />
                  <Point X="27.09569921875" Y="-2.563385253906" />
                  <Point X="27.134703125" Y="-2.779349365234" />
                  <Point X="27.13898828125" Y="-2.795140136719" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.86128515625" Y="-4.054905273438" />
                  <Point X="27.781837890625" Y="-4.111651367188" />
                  <Point X="27.701765625" Y="-4.163480957031" />
                  <Point X="26.759060546875" Y="-2.934922851562" />
                  <Point X="26.74840625" Y="-2.923206787109" />
                  <Point X="26.721716796875" Y="-2.900423095703" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.479828125" Y="-2.751127685547" />
                  <Point X="26.448033203125" Y="-2.743412353516" />
                  <Point X="26.41692578125" Y="-2.7411328125" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15626171875" Y="-2.769439941406" />
                  <Point X="26.128787109375" Y="-2.780857910156" />
                  <Point X="26.1045078125" Y="-2.795535888672" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904080078125" Y="-2.968962890625" />
                  <Point X="25.88716015625" Y="-2.996913330078" />
                  <Point X="25.87559765625" Y="-3.025932617188" />
                  <Point X="25.821810546875" Y="-3.273396728516" />
                  <Point X="25.819724609375" Y="-3.289627685547" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="26.02206640625" Y="-4.859916015625" />
                  <Point X="25.97568359375" Y="-4.870083007812" />
                  <Point X="25.92931640625" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.94157421875" Y="-4.75263671875" />
                  <Point X="23.858755859375" Y="-4.731328125" />
                  <Point X="23.879201171875" Y="-4.576020996094" />
                  <Point X="23.879904296875" Y="-4.568162109375" />
                  <Point X="23.880509765625" Y="-4.540883789062" />
                  <Point X="23.87814453125" Y="-4.50879296875" />
                  <Point X="23.876576171875" Y="-4.4972421875" />
                  <Point X="23.81613671875" Y="-4.1933984375" />
                  <Point X="23.81184765625" Y="-4.178400390625" />
                  <Point X="23.800873046875" Y="-4.149309082031" />
                  <Point X="23.7941875" Y="-4.135215820312" />
                  <Point X="23.778" Y="-4.107276367187" />
                  <Point X="23.769193359375" Y="-4.094581298828" />
                  <Point X="23.7496328125" Y="-4.070781494141" />
                  <Point X="23.73887890625" Y="-4.059676757813" />
                  <Point X="23.505734375" Y="-3.855214355469" />
                  <Point X="23.49319921875" Y="-3.845926269531" />
                  <Point X="23.46678125" Y="-3.829526855469" />
                  <Point X="23.4528984375" Y="-3.822415527344" />
                  <Point X="23.423046875" Y="-3.810104248047" />
                  <Point X="23.408326171875" Y="-3.805393554688" />
                  <Point X="23.378318359375" Y="-3.798421142578" />
                  <Point X="23.36303125" Y="-3.796159423828" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.037998046875" Y="-3.776139404297" />
                  <Point X="23.0070546875" Y="-3.779211181641" />
                  <Point X="22.991708984375" Y="-3.782021728516" />
                  <Point X="22.96053125" Y="-3.790427490234" />
                  <Point X="22.945986328125" Y="-3.795651123047" />
                  <Point X="22.9179375" Y="-3.808386474609" />
                  <Point X="22.90443359375" Y="-3.815898193359" />
                  <Point X="22.64659765625" Y="-3.988180175781" />
                  <Point X="22.64031640625" Y="-3.992759765625" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032770507812" />
                  <Point X="22.589529296875" Y="-4.041629394531" />
                  <Point X="22.496798828125" Y="-4.162478515625" />
                  <Point X="22.25240234375" Y="-4.011154785156" />
                  <Point X="22.019134765625" Y="-3.831546875" />
                  <Point X="22.65813671875" Y="-2.724764160156" />
                  <Point X="22.666326171875" Y="-2.707672119141" />
                  <Point X="22.676919921875" Y="-2.678604736328" />
                  <Point X="22.6810234375" Y="-2.663640136719" />
                  <Point X="22.68690625" Y="-2.632372558594" />
                  <Point X="22.688517578125" Y="-2.617097412109" />
                  <Point X="22.689255859375" Y="-2.586481445312" />
                  <Point X="22.685048828125" Y="-2.556145507812" />
                  <Point X="22.6760078125" Y="-2.526885498047" />
                  <Point X="22.67030078125" Y="-2.512620605469" />
                  <Point X="22.656130859375" Y="-2.484134521484" />
                  <Point X="22.648197265625" Y="-2.470976318359" />
                  <Point X="22.630314453125" Y="-2.446111816406" />
                  <Point X="22.620365234375" Y="-2.434405517578" />
                  <Point X="22.603025390625" Y="-2.417065673828" />
                  <Point X="22.59118359375" Y="-2.407019287109" />
                  <Point X="22.566025390625" Y="-2.388991943359" />
                  <Point X="22.552708984375" Y="-2.381010986328" />
                  <Point X="22.523880859375" Y="-2.366794189453" />
                  <Point X="22.509443359375" Y="-2.361088134766" />
                  <Point X="22.479828125" Y="-2.352103515625" />
                  <Point X="22.449142578125" Y="-2.348062744141" />
                  <Point X="22.4182109375" Y="-2.34907421875" />
                  <Point X="22.402787109375" Y="-2.350847900391" />
                  <Point X="22.37126171875" Y="-2.3571171875" />
                  <Point X="22.356330078125" Y="-2.361380859375" />
                  <Point X="22.327359375" Y="-2.372283935547" />
                  <Point X="22.3133203125" Y="-2.378923339844" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="20.99598046875" Y="-2.740587646484" />
                  <Point X="20.818734375" Y="-2.443372802734" />
                  <Point X="21.9512265625" Y="-1.574381958008" />
                  <Point X="21.963591796875" Y="-1.563245117188" />
                  <Point X="21.98497265625" Y="-1.540304199219" />
                  <Point X="21.994712890625" Y="-1.527942993164" />
                  <Point X="22.012654296875" Y="-1.500818237305" />
                  <Point X="22.020205078125" Y="-1.487048217773" />
                  <Point X="22.03293359375" Y="-1.458459228516" />
                  <Point X="22.038111328125" Y="-1.443640014648" />
                  <Point X="22.0458125" Y="-1.413907592773" />
                  <Point X="22.04786328125" Y="-1.403727661133" />
                  <Point X="22.051904296875" Y="-1.375875488281" />
                  <Point X="22.052888671875" Y="-1.362436645508" />
                  <Point X="22.052931640625" Y="-1.342191894531" />
                  <Point X="22.05081640625" Y="-1.322056030273" />
                  <Point X="22.046568359375" Y="-1.30226184082" />
                  <Point X="22.043916015625" Y="-1.292477416992" />
                  <Point X="22.03521484375" Y="-1.266058959961" />
                  <Point X="22.0307890625" Y="-1.2550078125" />
                  <Point X="22.020591796875" Y="-1.233545898438" />
                  <Point X="22.0077890625" Y="-1.213525512695" />
                  <Point X="21.992583984375" Y="-1.195265625" />
                  <Point X="21.98441015625" Y="-1.186615478516" />
                  <Point X="21.96848828125" Y="-1.171750610352" />
                  <Point X="21.960509765625" Y="-1.165079833984" />
                  <Point X="21.9438671875" Y="-1.152648803711" />
                  <Point X="21.935203125" Y="-1.146888305664" />
                  <Point X="21.908734375" Y="-1.131309570312" />
                  <Point X="21.894568359375" Y="-1.124482421875" />
                  <Point X="21.865306640625" Y="-1.113258422852" />
                  <Point X="21.8502109375" Y="-1.108861572266" />
                  <Point X="21.818326171875" Y="-1.102379638672" />
                  <Point X="21.802708984375" Y="-1.100532592773" />
                  <Point X="21.7713828125" Y="-1.099441040039" />
                  <Point X="21.755673828125" Y="-1.100196166992" />
                  <Point X="20.33908203125" Y="-1.286694213867" />
                  <Point X="20.259236328125" Y="-0.974110473633" />
                  <Point X="20.213548828125" Y="-0.654654663086" />
                  <Point X="21.490970703125" Y="-0.31237008667" />
                  <Point X="21.498462890625" Y="-0.310026611328" />
                  <Point X="21.52423828125" Y="-0.29997467041" />
                  <Point X="21.555376953125" Y="-0.284125061035" />
                  <Point X="21.56644921875" Y="-0.277506896973" />
                  <Point X="21.594189453125" Y="-0.258254425049" />
                  <Point X="21.606330078125" Y="-0.248240966797" />
                  <Point X="21.628794921875" Y="-0.226345703125" />
                  <Point X="21.6391171875" Y="-0.214464035034" />
                  <Point X="21.6583671875" Y="-0.188193161011" />
                  <Point X="21.66658203125" Y="-0.174779510498" />
                  <Point X="21.680681640625" Y="-0.146787872314" />
                  <Point X="21.686568359375" Y="-0.132209747314" />
                  <Point X="21.695814453125" Y="-0.102417701721" />
                  <Point X="21.698845703125" Y="-0.089547554016" />
                  <Point X="21.703091796875" Y="-0.063507194519" />
                  <Point X="21.704306640625" Y="-0.050336978912" />
                  <Point X="21.704919921875" Y="-0.022840112686" />
                  <Point X="21.7041640625" Y="-0.008584504128" />
                  <Point X="21.70052734375" Y="0.019647153854" />
                  <Point X="21.695810546875" Y="0.039872943878" />
                  <Point X="21.686564453125" Y="0.069664840698" />
                  <Point X="21.680669921875" Y="0.08425856781" />
                  <Point X="21.66655078125" Y="0.112276062012" />
                  <Point X="21.658326171875" Y="0.125699829102" />
                  <Point X="21.63906640625" Y="0.151965499878" />
                  <Point X="21.628751953125" Y="0.163831695557" />
                  <Point X="21.606306640625" Y="0.185700958252" />
                  <Point X="21.59417578125" Y="0.195703872681" />
                  <Point X="21.566435546875" Y="0.214956329346" />
                  <Point X="21.5595078125" Y="0.219333877563" />
                  <Point X="21.534375" Y="0.232838638306" />
                  <Point X="21.50342578125" Y="0.245638183594" />
                  <Point X="21.49170703125" Y="0.249612518311" />
                  <Point X="20.2145546875" Y="0.591824707031" />
                  <Point X="20.26866796875" Y="0.957522216797" />
                  <Point X="20.3664140625" Y="1.318236938477" />
                  <Point X="21.22140234375" Y="1.20567578125" />
                  <Point X="21.232603515625" Y="1.204805786133" />
                  <Point X="21.253875" Y="1.204411499023" />
                  <Point X="21.264521484375" Y="1.204811767578" />
                  <Point X="21.292466796875" Y="1.207823852539" />
                  <Point X="21.31425" Y="1.211871948242" />
                  <Point X="21.325458984375" Y="1.214669677734" />
                  <Point X="21.38685546875" Y="1.234027709961" />
                  <Point X="21.3998515625" Y="1.239205322266" />
                  <Point X="21.43387109375" Y="1.255757202148" />
                  <Point X="21.446701171875" Y="1.263295654297" />
                  <Point X="21.463673828125" Y="1.275148803711" />
                  <Point X="21.479255859375" Y="1.288780761719" />
                  <Point X="21.4932578125" Y="1.304029052734" />
                  <Point X="21.499841796875" Y="1.312035522461" />
                  <Point X="21.514009765625" Y="1.331319091797" />
                  <Point X="21.52021484375" Y="1.340927368164" />
                  <Point X="21.53142578125" Y="1.360821166992" />
                  <Point X="21.536431640625" Y="1.371106933594" />
                  <Point X="21.56106640625" Y="1.430581542969" />
                  <Point X="21.564390625" Y="1.439971557617" />
                  <Point X="21.57235546875" Y="1.468718994141" />
                  <Point X="21.57614453125" Y="1.489195678711" />
                  <Point X="21.5776953125" Y="1.509031860352" />
                  <Point X="21.5766328125" Y="1.548592773438" />
                  <Point X="21.57401953125" Y="1.568317504883" />
                  <Point X="21.56913671875" Y="1.588561279297" />
                  <Point X="21.566033203125" Y="1.598838623047" />
                  <Point X="21.5586875" Y="1.618978149414" />
                  <Point X="21.552201171875" Y="1.633276977539" />
                  <Point X="21.522474609375" Y="1.690378295898" />
                  <Point X="21.517197265625" Y="1.699292480469" />
                  <Point X="21.505705078125" Y="1.716490356445" />
                  <Point X="21.499490234375" Y="1.724774291992" />
                  <Point X="21.485576171875" Y="1.741355834961" />
                  <Point X="21.47849609375" Y="1.748915405273" />
                  <Point X="21.4635546875" Y="1.763218261719" />
                  <Point X="21.455693359375" Y="1.769961181641" />
                  <Point X="20.77238671875" Y="2.29428125" />
                  <Point X="20.99771875" Y="2.680324707031" />
                  <Point X="21.273662109375" Y="3.035012695312" />
                  <Point X="21.745517578125" Y="2.762586914062" />
                  <Point X="21.75513671875" Y="2.757692382812" />
                  <Point X="21.77413671875" Y="2.749345458984" />
                  <Point X="21.7838671875" Y="2.745692382812" />
                  <Point X="21.8045703125" Y="2.739186279297" />
                  <Point X="21.81508203125" Y="2.736573730469" />
                  <Point X="21.844970703125" Y="2.731154296875" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940638671875" Y="2.723330322266" />
                  <Point X="21.960943359375" Y="2.723732910156" />
                  <Point X="21.9710859375" Y="2.724478515625" />
                  <Point X="21.992171875" Y="2.727169433594" />
                  <Point X="22.00217578125" Y="2.728994628906" />
                  <Point X="22.021935546875" Y="2.733705078125" />
                  <Point X="22.041080078125" Y="2.740501953125" />
                  <Point X="22.059384765625" Y="2.749305175781" />
                  <Point X="22.06830078125" Y="2.754196777344" />
                  <Point X="22.086361328125" Y="2.765403564453" />
                  <Point X="22.094958984375" Y="2.771420898438" />
                  <Point X="22.111431640625" Y="2.784361572266" />
                  <Point X="22.121130859375" Y="2.793084228516" />
                  <Point X="22.18182421875" Y="2.85377734375" />
                  <Point X="22.190826171875" Y="2.864191162109" />
                  <Point X="22.21306640625" Y="2.894039306641" />
                  <Point X="22.2209609375" Y="2.906562744141" />
                  <Point X="22.2306015625" Y="2.924884033203" />
                  <Point X="22.238189453125" Y="2.944152099609" />
                  <Point X="22.243630859375" Y="2.964126464844" />
                  <Point X="22.2458046875" Y="2.974259033203" />
                  <Point X="22.249591796875" Y="2.998209228516" />
                  <Point X="22.250701171875" Y="3.009741699219" />
                  <Point X="22.251505859375" Y="3.032858398438" />
                  <Point X="22.251201171875" Y="3.044442626953" />
                  <Point X="22.243720703125" Y="3.12994921875" />
                  <Point X="22.242255859375" Y="3.140206298828" />
                  <Point X="22.23821875" Y="3.160499023438" />
                  <Point X="22.235646484375" Y="3.170534667969" />
                  <Point X="22.22913671875" Y="3.191177001953" />
                  <Point X="22.225486328125" Y="3.200871582031" />
                  <Point X="22.21715625" Y="3.219799804688" />
                  <Point X="22.2124765625" Y="3.229033447266" />
                  <Point X="21.940611328125" Y="3.699914794922" />
                  <Point X="22.351634765625" Y="4.015040771484" />
                  <Point X="22.8074765625" Y="4.268296386719" />
                  <Point X="22.8813359375" Y="4.172039550781" />
                  <Point X="22.88796875" Y="4.164282226563" />
                  <Point X="22.9018828125" Y="4.149675292969" />
                  <Point X="22.909228515625" Y="4.142743164062" />
                  <Point X="22.925501953125" Y="4.128943359375" />
                  <Point X="22.933724609375" Y="4.122705566406" />
                  <Point X="22.950798828125" Y="4.111158691406" />
                  <Point X="22.9610703125" Y="4.105103515625" />
                  <Point X="23.056240234375" Y="4.055561523438" />
                  <Point X="23.068900390625" Y="4.050099121094" />
                  <Point X="23.10412109375" Y="4.037850097656" />
                  <Point X="23.118455078125" Y="4.034088623047" />
                  <Point X="23.1388359375" Y="4.030410888672" />
                  <Point X="23.159494140625" Y="4.028976318359" />
                  <Point X="23.180185546875" Y="4.029802001953" />
                  <Point X="23.190509765625" Y="4.030780517578" />
                  <Point X="23.21446484375" Y="4.034378662109" />
                  <Point X="23.225775390625" Y="4.036789306641" />
                  <Point X="23.24801953125" Y="4.042967041016" />
                  <Point X="23.258953125" Y="4.046733886719" />
                  <Point X="23.358078125" Y="4.08779296875" />
                  <Point X="23.370462890625" Y="4.094017822266" />
                  <Point X="23.40263671875" Y="4.113248535156" />
                  <Point X="23.414740234375" Y="4.121833984375" />
                  <Point X="23.430646484375" Y="4.135099121094" />
                  <Point X="23.445015625" Y="4.150022949219" />
                  <Point X="23.45766796875" Y="4.166420410156" />
                  <Point X="23.463544921875" Y="4.174962890625" />
                  <Point X="23.47611328125" Y="4.195555175781" />
                  <Point X="23.48150390625" Y="4.205728027344" />
                  <Point X="23.491017578125" Y="4.226652832031" />
                  <Point X="23.495140625" Y="4.237404785156" />
                  <Point X="23.527404296875" Y="4.339729980469" />
                  <Point X="23.529974609375" Y="4.349763671875" />
                  <Point X="23.534009765625" Y="4.370049804688" />
                  <Point X="23.535474609375" Y="4.380302246094" />
                  <Point X="23.537361328125" Y="4.401864257812" />
                  <Point X="23.53769921875" Y="4.412217285156" />
                  <Point X="23.537248046875" Y="4.432898925781" />
                  <Point X="23.536458984375" Y="4.443227539062" />
                  <Point X="23.520736328125" Y="4.562654785156" />
                  <Point X="24.068826171875" Y="4.7163203125" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.774236328125" Y="4.262088378906" />
                  <Point X="24.779466796875" Y="4.247470703125" />
                  <Point X="24.79223828125" Y="4.219282226562" />
                  <Point X="24.799779296875" Y="4.205711425781" />
                  <Point X="24.817634765625" Y="4.178986816406" />
                  <Point X="24.827287109375" Y="4.166825195312" />
                  <Point X="24.84844140625" Y="4.144236816406" />
                  <Point X="24.8729921875" Y="4.125395507812" />
                  <Point X="24.90028515625" Y="4.1108046875" />
                  <Point X="24.914529296875" Y="4.104628417969" />
                  <Point X="24.94496484375" Y="4.094293945312" />
                  <Point X="24.960025390625" Y="4.090520019531" />
                  <Point X="24.99055859375" Y="4.085475585938" />
                  <Point X="25.02150390625" Y="4.085471679688" />
                  <Point X="25.052037109375" Y="4.090508300781" />
                  <Point X="25.06709765625" Y="4.094278320312" />
                  <Point X="25.09753515625" Y="4.104604980469" />
                  <Point X="25.11178125" Y="4.110778320312" />
                  <Point X="25.139078125" Y="4.125362792969" />
                  <Point X="25.16363671875" Y="4.14419921875" />
                  <Point X="25.184796875" Y="4.166783691406" />
                  <Point X="25.19444921875" Y="4.178942871094" />
                  <Point X="25.212310546875" Y="4.205663085938" />
                  <Point X="25.21985546875" Y="4.219230957031" />
                  <Point X="25.2326328125" Y="4.247414550781" />
                  <Point X="25.237865234375" Y="4.262030273438" />
                  <Point X="25.378138671875" Y="4.78501171875" />
                  <Point X="25.8278828125" Y="4.737911621094" />
                  <Point X="26.45359375" Y="4.58684375" />
                  <Point X="26.858267578125" Y="4.44006640625" />
                  <Point X="27.2504609375" Y="4.256650878906" />
                  <Point X="27.629423828125" Y="4.035864257813" />
                  <Point X="27.81778125" Y="3.901915283203" />
                  <Point X="27.0657421875" Y="2.599343994141" />
                  <Point X="27.06297265625" Y="2.594188232422" />
                  <Point X="27.054095703125" Y="2.575388671875" />
                  <Point X="27.044373046875" Y="2.550053710938" />
                  <Point X="27.041291015625" Y="2.540558349609" />
                  <Point X="27.01983203125" Y="2.460312255859" />
                  <Point X="27.01791015625" Y="2.451451904297" />
                  <Point X="27.01364453125" Y="2.42017578125" />
                  <Point X="27.012759765625" Y="2.383190185547" />
                  <Point X="27.013416015625" Y="2.369545410156" />
                  <Point X="27.021783203125" Y="2.300155273438" />
                  <Point X="27.023794921875" Y="2.289059082031" />
                  <Point X="27.029119140625" Y="2.267186767578" />
                  <Point X="27.032431640625" Y="2.256410644531" />
                  <Point X="27.040666015625" Y="2.234368164062" />
                  <Point X="27.045541015625" Y="2.223462890625" />
                  <Point X="27.0548828125" Y="2.205663818359" />
                  <Point X="27.061482421875" Y="2.194095458984" />
                  <Point X="27.104419921875" Y="2.130818603516" />
                  <Point X="27.10982421875" Y="2.123616455078" />
                  <Point X="27.130501953125" Y="2.100087402344" />
                  <Point X="27.15764453125" Y="2.075353027344" />
                  <Point X="27.16829296875" Y="2.066959472656" />
                  <Point X="27.2315703125" Y="2.02402331543" />
                  <Point X="27.24119140625" Y="2.018292724609" />
                  <Point X="27.26104296875" Y="2.008002441406" />
                  <Point X="27.2712734375" Y="2.003442749023" />
                  <Point X="27.293146484375" Y="1.99520324707" />
                  <Point X="27.3039296875" Y="1.991858276367" />
                  <Point X="27.32581640625" Y="1.986471069336" />
                  <Point X="27.337626953125" Y="1.984342407227" />
                  <Point X="27.407015625" Y="1.975975219727" />
                  <Point X="27.4160625" Y="1.97532043457" />
                  <Point X="27.4476328125" Y="1.975502319336" />
                  <Point X="27.484375" Y="1.97983581543" />
                  <Point X="27.497791015625" Y="1.982406738281" />
                  <Point X="27.578037109375" Y="2.003865844727" />
                  <Point X="27.583994140625" Y="2.005669799805" />
                  <Point X="27.60439453125" Y="2.013062866211" />
                  <Point X="27.6276484375" Y="2.02357019043" />
                  <Point X="27.63603125" Y="2.027870117188" />
                  <Point X="28.940404296875" Y="2.780950439453" />
                  <Point X="29.043955078125" Y="2.637038085938" />
                  <Point X="29.136884765625" Y="2.483471191406" />
                  <Point X="28.173556640625" Y="1.744282470703" />
                  <Point X="28.168990234375" Y="1.740548095703" />
                  <Point X="28.153525390625" Y="1.726499633789" />
                  <Point X="28.134982421875" Y="1.706929077148" />
                  <Point X="28.128544921875" Y="1.699382568359" />
                  <Point X="28.07079296875" Y="1.62403918457" />
                  <Point X="28.065626953125" Y="1.616587036133" />
                  <Point X="28.04971484375" Y="1.589322387695" />
                  <Point X="28.034744140625" Y="1.555491333008" />
                  <Point X="28.03012890625" Y="1.542634887695" />
                  <Point X="28.008615234375" Y="1.465708740234" />
                  <Point X="28.006150390625" Y="1.454176757812" />
                  <Point X="28.00266796875" Y="1.430896728516" />
                  <Point X="28.001654296875" Y="1.419244506836" />
                  <Point X="28.0013359375" Y="1.394318969727" />
                  <Point X="28.002896484375" Y="1.366419555664" />
                  <Point X="28.00470703125" Y="1.352527709961" />
                  <Point X="28.0223671875" Y="1.266937744141" />
                  <Point X="28.024603515625" Y="1.258216674805" />
                  <Point X="28.03470703125" Y="1.228556884766" />
                  <Point X="28.0503203125" Y="1.195315917969" />
                  <Point X="28.056943359375" Y="1.183489624023" />
                  <Point X="28.1049765625" Y="1.110480957031" />
                  <Point X="28.11178515625" Y="1.10137097168" />
                  <Point X="28.126443359375" Y="1.084028076172" />
                  <Point X="28.13429296875" Y="1.075795410156" />
                  <Point X="28.1515859375" Y="1.059708618164" />
                  <Point X="28.160828125" Y="1.052230712891" />
                  <Point X="28.17796484375" Y="1.039581176758" />
                  <Point X="28.187779296875" Y="1.033229858398" />
                  <Point X="28.25738671875" Y="0.994047119141" />
                  <Point X="28.2655" Y="0.989977966309" />
                  <Point X="28.294744140625" Y="0.978062866211" />
                  <Point X="28.330345703125" Y="0.968008361816" />
                  <Point X="28.34371875" Y="0.965251403809" />
                  <Point X="28.437833984375" Y="0.952812866211" />
                  <Point X="28.444033203125" Y="0.952199645996" />
                  <Point X="28.46571875" Y="0.951222717285" />
                  <Point X="28.49121875" Y="0.952032287598" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="29.704701171875" Y="1.111319580078" />
                  <Point X="29.752689453125" Y="0.914200500488" />
                  <Point X="29.78387109375" Y="0.713921142578" />
                  <Point X="28.692642578125" Y="0.421526977539" />
                  <Point X="28.686947265625" Y="0.419808135986" />
                  <Point X="28.66719140625" Y="0.412703063965" />
                  <Point X="28.642734375" Y="0.40177142334" />
                  <Point X="28.633958984375" Y="0.397289398193" />
                  <Point X="28.54149609375" Y="0.34384387207" />
                  <Point X="28.533865234375" Y="0.338932922363" />
                  <Point X="28.50873046875" Y="0.319829833984" />
                  <Point X="28.481953125" Y="0.29430267334" />
                  <Point X="28.47276953125" Y="0.284191864014" />
                  <Point X="28.417291015625" Y="0.213499832153" />
                  <Point X="28.410978515625" Y="0.204412582397" />
                  <Point X="28.39945703125" Y="0.185572555542" />
                  <Point X="28.394248046875" Y="0.175819625854" />
                  <Point X="28.38457421875" Y="0.154859481812" />
                  <Point X="28.38047265625" Y="0.14438633728" />
                  <Point X="28.373537109375" Y="0.123025062561" />
                  <Point X="28.3703671875" Y="0.110423179626" />
                  <Point X="28.351875" Y="0.013860967636" />
                  <Point X="28.3506015625" Y="0.004941366196" />
                  <Point X="28.3485859375" Y="-0.026336614609" />
                  <Point X="28.350291015625" Y="-0.063012874603" />
                  <Point X="28.351884765625" Y="-0.076469337463" />
                  <Point X="28.370376953125" Y="-0.173031692505" />
                  <Point X="28.373107421875" Y="-0.183824691772" />
                  <Point X="28.3798125" Y="-0.205013565063" />
                  <Point X="28.383787109375" Y="-0.215409423828" />
                  <Point X="28.39333984375" Y="-0.236590270996" />
                  <Point X="28.39858203125" Y="-0.246584564209" />
                  <Point X="28.41021484375" Y="-0.26587878418" />
                  <Point X="28.417318359375" Y="-0.276094970703" />
                  <Point X="28.472796875" Y="-0.34678729248" />
                  <Point X="28.47873828125" Y="-0.35365411377" />
                  <Point X="28.501205078125" Y="-0.375860168457" />
                  <Point X="28.530248046875" Y="-0.398769775391" />
                  <Point X="28.54154296875" Y="-0.406430541992" />
                  <Point X="28.634005859375" Y="-0.459876220703" />
                  <Point X="28.63949609375" Y="-0.462814056396" />
                  <Point X="28.659154296875" Y="-0.472014709473" />
                  <Point X="28.683025390625" Y="-0.481026885986" />
                  <Point X="28.6919921875" Y="-0.483912994385" />
                  <Point X="29.784880859375" Y="-0.776751281738" />
                  <Point X="29.761619140625" Y="-0.931033935547" />
                  <Point X="29.727802734375" Y="-1.079219726563" />
                  <Point X="28.437537109375" Y="-0.909352661133" />
                  <Point X="28.4296796875" Y="-0.908648620605" />
                  <Point X="28.401931640625" Y="-0.908079406738" />
                  <Point X="28.367109375" Y="-0.910821777344" />
                  <Point X="28.354390625" Y="-0.912696105957" />
                  <Point X="28.17291796875" Y="-0.952140136719" />
                  <Point X="28.164251953125" Y="-0.95445715332" />
                  <Point X="28.141095703125" Y="-0.961835998535" />
                  <Point X="28.123927734375" Y="-0.96923815918" />
                  <Point X="28.0963515625" Y="-0.984504577637" />
                  <Point X="28.08319921875" Y="-0.993290100098" />
                  <Point X="28.067" Y="-1.006184326172" />
                  <Point X="28.0597109375" Y="-1.012622680664" />
                  <Point X="28.03923046875" Y="-1.033366088867" />
                  <Point X="27.92960546875" Y="-1.1652109375" />
                  <Point X="27.921294921875" Y="-1.176899780273" />
                  <Point X="27.906533203125" Y="-1.201386108398" />
                  <Point X="27.90008203125" Y="-1.214183105469" />
                  <Point X="27.8887265625" Y="-1.241689575195" />
                  <Point X="27.884296875" Y="-1.255207397461" />
                  <Point X="27.8775078125" Y="-1.28275390625" />
                  <Point X="27.8751484375" Y="-1.296782592773" />
                  <Point X="27.859431640625" Y="-1.467591918945" />
                  <Point X="27.859294921875" Y="-1.483362182617" />
                  <Point X="27.8616328125" Y="-1.514708618164" />
                  <Point X="27.864107421875" Y="-1.530284912109" />
                  <Point X="27.87188671875" Y="-1.56198425293" />
                  <Point X="27.87687109375" Y="-1.576859985352" />
                  <Point X="27.88922265625" Y="-1.60559362793" />
                  <Point X="27.89658984375" Y="-1.619451171875" />
                  <Point X="27.997021484375" Y="-1.775663452148" />
                  <Point X="28.00174609375" Y="-1.782357666016" />
                  <Point X="28.01980078125" Y="-1.804457397461" />
                  <Point X="28.043494140625" Y="-1.828123291016" />
                  <Point X="28.052798828125" Y="-1.836277709961" />
                  <Point X="29.087171875" Y="-2.629980957031" />
                  <Point X="29.04548046875" Y="-2.697442138672" />
                  <Point X="29.001275390625" Y="-2.760252441406" />
                  <Point X="27.849126953125" Y="-2.095058349609" />
                  <Point X="27.84213671875" Y="-2.091400634766" />
                  <Point X="27.816724609375" Y="-2.080258544922" />
                  <Point X="27.78347265625" Y="-2.069454833984" />
                  <Point X="27.771" Y="-2.066318115234" />
                  <Point X="27.555017578125" Y="-2.027311889648" />
                  <Point X="27.5393125" Y="-2.025806884766" />
                  <Point X="27.5078671875" Y="-2.025417236328" />
                  <Point X="27.492126953125" Y="-2.026532714844" />
                  <Point X="27.45986328125" Y="-2.031537475586" />
                  <Point X="27.444615234375" Y="-2.035213134766" />
                  <Point X="27.41493359375" Y="-2.045022583008" />
                  <Point X="27.4005" Y="-2.05115625" />
                  <Point X="27.221072265625" Y="-2.145588134766" />
                  <Point X="27.211841796875" Y="-2.151136230469" />
                  <Point X="27.194068359375" Y="-2.163240722656" />
                  <Point X="27.185525390625" Y="-2.169797119141" />
                  <Point X="27.168490234375" Y="-2.184478027344" />
                  <Point X="27.158322265625" Y="-2.194682861328" />
                  <Point X="27.139642578125" Y="-2.216516601562" />
                  <Point X="27.120333984375" Y="-2.246442138672" />
                  <Point X="27.025984375" Y="-2.425711669922" />
                  <Point X="27.019814453125" Y="-2.440254638672" />
                  <Point X="27.00997265625" Y="-2.470155029297" />
                  <Point X="27.00630078125" Y="-2.485512451172" />
                  <Point X="27.0013671875" Y="-2.517812011719" />
                  <Point X="27.00028515625" Y="-2.533439697266" />
                  <Point X="27.00070703125" Y="-2.564668457031" />
                  <Point X="27.0022109375" Y="-2.58026953125" />
                  <Point X="27.04121484375" Y="-2.796233642578" />
                  <Point X="27.04301953125" Y="-2.804229736328" />
                  <Point X="27.05123828125" Y="-2.831540771484" />
                  <Point X="27.064072265625" Y="-2.862479248047" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.73589453125" Y="-4.027721679687" />
                  <Point X="27.723755859375" Y="-4.036082763672" />
                  <Point X="26.8344296875" Y="-2.877090576172" />
                  <Point X="26.829345703125" Y="-2.871008056641" />
                  <Point X="26.8100859375" Y="-2.850953125" />
                  <Point X="26.783396484375" Y="-2.828169433594" />
                  <Point X="26.773091796875" Y="-2.820512939453" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.54620703125" Y="-2.676212890625" />
                  <Point X="26.517234375" Y="-2.663802246094" />
                  <Point X="26.50223046875" Y="-2.658806884766" />
                  <Point X="26.470435546875" Y="-2.651091552734" />
                  <Point X="26.4549765625" Y="-2.648666503906" />
                  <Point X="26.423869140625" Y="-2.646386962891" />
                  <Point X="26.408220703125" Y="-2.646532470703" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.161171875" Y="-2.670352539063" />
                  <Point X="26.133419921875" Y="-2.677226806641" />
                  <Point X="26.1198046875" Y="-2.681713867188" />
                  <Point X="26.092330078125" Y="-2.693131835938" />
                  <Point X="26.079638671875" Y="-2.699559570312" />
                  <Point X="26.055359375" Y="-2.714237548828" />
                  <Point X="26.043771484375" Y="-2.722487792969" />
                  <Point X="25.863876953125" Y="-2.872063476562" />
                  <Point X="25.8526171875" Y="-2.883131347656" />
                  <Point X="25.832083984375" Y="-2.906982666016" />
                  <Point X="25.822810546875" Y="-2.919766113281" />
                  <Point X="25.805890625" Y="-2.947716552734" />
                  <Point X="25.798908203125" Y="-2.961749755859" />
                  <Point X="25.787345703125" Y="-2.990769042969" />
                  <Point X="25.782765625" Y="-3.005755126953" />
                  <Point X="25.728978515625" Y="-3.253219238281" />
                  <Point X="25.7275859375" Y="-3.261287353516" />
                  <Point X="25.724724609375" Y="-3.289677490234" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520263672" />
                  <Point X="25.83308984375" Y="-4.152329101562" />
                  <Point X="25.655267578125" Y="-3.488684082031" />
                  <Point X="25.65290625" Y="-3.481141601562" />
                  <Point X="25.642798828125" Y="-3.455262939453" />
                  <Point X="25.626919921875" Y="-3.424113525391" />
                  <Point X="25.620328125" Y="-3.413091552734" />
                  <Point X="25.456681640625" Y="-3.177309326172" />
                  <Point X="25.446626953125" Y="-3.165126464844" />
                  <Point X="25.424640625" Y="-3.142596679688" />
                  <Point X="25.412708984375" Y="-3.132249755859" />
                  <Point X="25.386326171875" Y="-3.112966308594" />
                  <Point X="25.372958984375" Y="-3.104795898438" />
                  <Point X="25.345060546875" Y="-3.090762207031" />
                  <Point X="25.330529296875" Y="-3.084898925781" />
                  <Point X="25.077296875" Y="-3.006305175781" />
                  <Point X="25.063314453125" Y="-3.003099853516" />
                  <Point X="25.035029296875" Y="-2.998830566406" />
                  <Point X="25.0207265625" Y="-2.997766601562" />
                  <Point X="24.990958984375" Y="-2.997805908203" />
                  <Point X="24.97678125" Y="-2.998888671875" />
                  <Point X="24.94874609375" Y="-3.003157958984" />
                  <Point X="24.934888671875" Y="-3.006344482422" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.67312109375" Y="-3.088042236328" />
                  <Point X="24.650431640625" Y="-3.097543701172" />
                  <Point X="24.63396875" Y="-3.106435058594" />
                  <Point X="24.607869140625" Y="-3.124055908203" />
                  <Point X="24.595552734375" Y="-3.13395703125" />
                  <Point X="24.58069140625" Y="-3.148092773438" />
                  <Point X="24.574033203125" Y="-3.155106689453" />
                  <Point X="24.55555078125" Y="-3.177427246094" />
                  <Point X="24.391904296875" Y="-3.413209716797" />
                  <Point X="24.387529296875" Y="-3.420133300781" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213378906" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.308548828125" Y="1.10469549542" />
                  <Point X="20.308548828125" Y="0.566639034588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.308548828125" Y="-0.629199458822" />
                  <Point X="20.308548828125" Y="-1.167161384164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.403548828125" Y="1.313348061888" />
                  <Point X="20.403548828125" Y="0.54118384274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.403548828125" Y="-0.603744254558" />
                  <Point X="20.403548828125" Y="-1.278206990002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.498548828125" Y="1.300841095218" />
                  <Point X="20.498548828125" Y="0.515728650893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.498548828125" Y="-0.578289050294" />
                  <Point X="20.498548828125" Y="-1.265699989402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.593548828125" Y="1.288334128548" />
                  <Point X="20.593548828125" Y="0.490273459046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.593548828125" Y="-0.55283384603" />
                  <Point X="20.593548828125" Y="-1.253192988803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.688548828125" Y="1.275827161879" />
                  <Point X="20.688548828125" Y="0.464818267198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.688548828125" Y="-0.527378641766" />
                  <Point X="20.688548828125" Y="-1.240685988203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.783548828125" Y="2.313404404693" />
                  <Point X="20.783548828125" Y="2.285716255014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.783548828125" Y="1.263320195209" />
                  <Point X="20.783548828125" Y="0.439363075351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.783548828125" Y="-0.501923437501" />
                  <Point X="20.783548828125" Y="-1.228178987603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.878548828125" Y="2.476160352946" />
                  <Point X="20.878548828125" Y="2.212820129713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.878548828125" Y="1.250813228539" />
                  <Point X="20.878548828125" Y="0.413907883504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.878548828125" Y="-0.476468233237" />
                  <Point X="20.878548828125" Y="-1.215671987003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.878548828125" Y="-2.397475610046" />
                  <Point X="20.878548828125" Y="-2.543672577829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.973548828125" Y="2.6389163012" />
                  <Point X="20.973548828125" Y="2.139924004412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.973548828125" Y="1.23830626187" />
                  <Point X="20.973548828125" Y="0.388452691656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.973548828125" Y="-0.451013028973" />
                  <Point X="20.973548828125" Y="-1.203164986404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.973548828125" Y="-2.324579627928" />
                  <Point X="20.973548828125" Y="-2.70297318389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.068548828125" Y="2.771367223789" />
                  <Point X="21.068548828125" Y="2.067027879111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.068548828125" Y="1.2257992952" />
                  <Point X="21.068548828125" Y="0.362997499809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.068548828125" Y="-0.425557824709" />
                  <Point X="21.068548828125" Y="-1.190657985804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.068548828125" Y="-2.251683645809" />
                  <Point X="21.068548828125" Y="-2.835927480889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.163548828125" Y="2.893476916745" />
                  <Point X="21.163548828125" Y="1.99413175381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.163548828125" Y="1.21329232853" />
                  <Point X="21.163548828125" Y="0.337542307961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.163548828125" Y="-0.400102620445" />
                  <Point X="21.163548828125" Y="-1.178150985204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.163548828125" Y="-2.178787663691" />
                  <Point X="21.163548828125" Y="-2.960737857566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.258548828125" Y="3.015586609702" />
                  <Point X="21.258548828125" Y="1.921235628508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.258548828125" Y="1.204587217727" />
                  <Point X="21.258548828125" Y="0.312087116114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.258548828125" Y="-0.374647416181" />
                  <Point X="21.258548828125" Y="-1.165643984604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.258548828125" Y="-2.105891681573" />
                  <Point X="21.258548828125" Y="-2.987895786034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.353548828125" Y="2.988890093778" />
                  <Point X="21.353548828125" Y="1.848339503207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.353548828125" Y="1.223526277686" />
                  <Point X="21.353548828125" Y="0.286631924267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.353548828125" Y="-0.349192211917" />
                  <Point X="21.353548828125" Y="-1.153136984005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.353548828125" Y="-2.032995699455" />
                  <Point X="21.353548828125" Y="-2.933047524354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.448548828125" Y="2.93404183847" />
                  <Point X="21.448548828125" Y="1.775443377906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.448548828125" Y="1.264585997144" />
                  <Point X="21.448548828125" Y="0.261176732419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.448548828125" Y="-0.323737007653" />
                  <Point X="21.448548828125" Y="-1.140629983405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.448548828125" Y="-1.960099717337" />
                  <Point X="21.448548828125" Y="-2.878199262673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.543548828125" Y="2.879193583161" />
                  <Point X="21.543548828125" Y="1.649897137876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.543548828125" Y="1.388289640052" />
                  <Point X="21.543548828125" Y="0.227909211728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.543548828125" Y="-0.290145586305" />
                  <Point X="21.543548828125" Y="-1.128122982805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.543548828125" Y="-1.887203735218" />
                  <Point X="21.543548828125" Y="-2.823351000993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.638548828125" Y="2.824345327852" />
                  <Point X="21.638548828125" Y="0.152560944274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.638548828125" Y="-0.21511825744" />
                  <Point X="21.638548828125" Y="-1.115615982205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.638548828125" Y="-1.8143077531" />
                  <Point X="21.638548828125" Y="-2.768502739312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.733548828125" Y="2.769497072544" />
                  <Point X="21.733548828125" Y="-1.103108981606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.733548828125" Y="-1.741411770982" />
                  <Point X="21.733548828125" Y="-2.713654477632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.828548828125" Y="2.734131922024" />
                  <Point X="21.828548828125" Y="-1.104457827978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.828548828125" Y="-1.668515788864" />
                  <Point X="21.828548828125" Y="-2.658806215951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.923548828125" Y="2.724279451326" />
                  <Point X="21.923548828125" Y="-1.140028925953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.923548828125" Y="-1.595619806745" />
                  <Point X="21.923548828125" Y="-2.603957954271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.018548828125" Y="3.759668396682" />
                  <Point X="22.018548828125" Y="3.564923959785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.018548828125" Y="2.732897732178" />
                  <Point X="22.018548828125" Y="-1.230351188523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.018548828125" Y="-1.490068646269" />
                  <Point X="22.018548828125" Y="-2.54910969259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.113548828125" Y="3.832503580864" />
                  <Point X="22.113548828125" Y="3.400380199233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.113548828125" Y="2.786265591471" />
                  <Point X="22.113548828125" Y="-2.49426143091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.113548828125" Y="-3.668017089368" />
                  <Point X="22.113548828125" Y="-3.904242414566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.208548828125" Y="3.905338765046" />
                  <Point X="22.208548828125" Y="3.235836438682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.208548828125" Y="2.887976361326" />
                  <Point X="22.208548828125" Y="-2.43941316923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.208548828125" Y="-3.503472431255" />
                  <Point X="22.208548828125" Y="-3.977389105598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.303548828125" Y="3.978173949228" />
                  <Point X="22.303548828125" Y="-2.384564907549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.303548828125" Y="-3.338927773143" />
                  <Point X="22.303548828125" Y="-4.042823312778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.398548828125" Y="4.04110518647" />
                  <Point X="22.398548828125" Y="-2.351690744888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.398548828125" Y="-3.17438311503" />
                  <Point X="22.398548828125" Y="-4.101644757903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.493548828125" Y="4.09388508426" />
                  <Point X="22.493548828125" Y="-2.356266079066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.493548828125" Y="-3.009838456918" />
                  <Point X="22.493548828125" Y="-4.160466203029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.588548828125" Y="4.146664982049" />
                  <Point X="22.588548828125" Y="-2.405131321368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.588548828125" Y="-2.845293798805" />
                  <Point X="22.588548828125" Y="-4.042907170739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.683548828125" Y="4.199444879839" />
                  <Point X="22.683548828125" Y="-2.551290963094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.683548828125" Y="-2.650217501089" />
                  <Point X="22.683548828125" Y="-3.963489974698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.778548828125" Y="4.252224777628" />
                  <Point X="22.778548828125" Y="-3.900012445193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.873548828125" Y="4.182188059478" />
                  <Point X="22.873548828125" Y="-3.836534915688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.968548828125" Y="4.101210472998" />
                  <Point X="22.968548828125" Y="-3.788265888289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.063548828125" Y="4.052408127947" />
                  <Point X="23.063548828125" Y="-3.776530528797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.158548828125" Y="4.029041963819" />
                  <Point X="23.158548828125" Y="-3.782757087993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.253548828125" Y="4.04487199639" />
                  <Point X="23.253548828125" Y="-3.788983647189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.348548828125" Y="4.083845789107" />
                  <Point X="23.348548828125" Y="-3.795210206385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.443548828125" Y="4.148499530037" />
                  <Point X="23.443548828125" Y="-3.818559593426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.538548828125" Y="4.567648797063" />
                  <Point X="23.538548828125" Y="-3.883991877642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.633548828125" Y="4.594283527234" />
                  <Point X="23.633548828125" Y="-3.967304697027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.728548828125" Y="4.620918257405" />
                  <Point X="23.728548828125" Y="-4.050617516412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.823548828125" Y="4.647552987576" />
                  <Point X="23.823548828125" Y="-4.230660904175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.918548828125" Y="4.674187717747" />
                  <Point X="23.918548828125" Y="-4.746712444248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.013548828125" Y="4.700822447917" />
                  <Point X="24.013548828125" Y="-4.766607495937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.108548828125" Y="4.720969269909" />
                  <Point X="24.108548828125" Y="-4.416082631181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.203548828125" Y="4.732087633973" />
                  <Point X="24.203548828125" Y="-4.061539608707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.298548828125" Y="4.743205998038" />
                  <Point X="24.298548828125" Y="-3.706996586234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.393548828125" Y="4.754324362103" />
                  <Point X="24.393548828125" Y="-3.410840269981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.488548828125" Y="4.765442726168" />
                  <Point X="24.488548828125" Y="-3.273963912372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.583548828125" Y="4.776561090233" />
                  <Point X="24.583548828125" Y="-3.145374861646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.678548828125" Y="4.61919915966" />
                  <Point X="24.678548828125" Y="-3.086068472395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.773548828125" Y="4.264654165051" />
                  <Point X="24.773548828125" Y="-3.056418413084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.868548828125" Y="4.128805529853" />
                  <Point X="24.868548828125" Y="-3.026933920585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.963548828125" Y="4.089937907448" />
                  <Point X="24.963548828125" Y="-3.000903749217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.058548828125" Y="4.092138341647" />
                  <Point X="25.058548828125" Y="-3.002380542609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.153548828125" Y="4.136461813059" />
                  <Point X="25.153548828125" Y="-3.029970893214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.248548828125" Y="4.301861915123" />
                  <Point X="25.248548828125" Y="-3.059455294124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.343548828125" Y="4.656050407187" />
                  <Point X="25.343548828125" Y="-3.090152236643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.438548828125" Y="4.778685178831" />
                  <Point X="25.438548828125" Y="-3.156848665127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.533548828125" Y="4.768736168109" />
                  <Point X="25.533548828125" Y="-3.288059744597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.628548828125" Y="4.758787157387" />
                  <Point X="25.628548828125" Y="-3.427308926538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.723548828125" Y="4.748838146665" />
                  <Point X="25.723548828125" Y="-3.743514439358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.818548828125" Y="4.738889135942" />
                  <Point X="25.818548828125" Y="-2.9268061524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.818548828125" Y="-4.041879361323" />
                  <Point X="25.818548828125" Y="-4.098061023465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.913548828125" Y="4.71722893548" />
                  <Point X="25.913548828125" Y="-2.830763140155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.008548828125" Y="4.694292709031" />
                  <Point X="26.008548828125" Y="-2.751774135638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.103548828125" Y="4.671356482582" />
                  <Point X="26.103548828125" Y="-2.68846951899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.198548828125" Y="4.648420256134" />
                  <Point X="26.198548828125" Y="-2.665826736825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.293548828125" Y="4.625484029685" />
                  <Point X="26.293548828125" Y="-2.657084718938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.388548828125" Y="4.602547803236" />
                  <Point X="26.388548828125" Y="-2.648342701052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.483548828125" Y="4.57597888429" />
                  <Point X="26.483548828125" Y="-2.654273611783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.578548828125" Y="4.541521880433" />
                  <Point X="26.578548828125" Y="-2.695440297063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.673548828125" Y="4.507064876577" />
                  <Point X="26.673548828125" Y="-2.756516269654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.768548828125" Y="4.472607872721" />
                  <Point X="26.768548828125" Y="-2.817592242244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.863548828125" Y="4.437596544814" />
                  <Point X="26.863548828125" Y="-2.915039371067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.958548828125" Y="4.393168268095" />
                  <Point X="26.958548828125" Y="-3.038845755377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.053548828125" Y="4.348739991376" />
                  <Point X="27.053548828125" Y="2.57396364394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.053548828125" Y="2.208205479735" />
                  <Point X="27.053548828125" Y="-2.373337668338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.053548828125" Y="-2.8371107331" />
                  <Point X="27.053548828125" Y="-3.162652139688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.148548828125" Y="4.304311714657" />
                  <Point X="27.148548828125" Y="2.742769459652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.148548828125" Y="2.083641721621" />
                  <Point X="27.148548828125" Y="-2.20610653721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.148548828125" Y="-3.010407461845" />
                  <Point X="27.148548828125" Y="-3.286458523998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.243548828125" Y="4.259883437938" />
                  <Point X="27.243548828125" Y="2.907314466197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.243548828125" Y="2.017070728168" />
                  <Point X="27.243548828125" Y="-2.133758836651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.243548828125" Y="-3.174952550336" />
                  <Point X="27.243548828125" Y="-3.410264908309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.338548828125" Y="4.205330213449" />
                  <Point X="27.338548828125" Y="3.071859472741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.338548828125" Y="1.984231243534" />
                  <Point X="27.338548828125" Y="-2.083760830022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.338548828125" Y="-3.339497638826" />
                  <Point X="27.338548828125" Y="-3.534071292619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.433548828125" Y="4.149982501252" />
                  <Point X="27.433548828125" Y="3.236404479286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.433548828125" Y="1.975421177846" />
                  <Point X="27.433548828125" Y="-2.03887045751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.433548828125" Y="-3.504042727316" />
                  <Point X="27.433548828125" Y="-3.657877676929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.528548828125" Y="4.094634789054" />
                  <Point X="27.528548828125" Y="3.400949485831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.528548828125" Y="1.990631876" />
                  <Point X="27.528548828125" Y="-2.025673508832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.528548828125" Y="-3.668587815806" />
                  <Point X="27.528548828125" Y="-3.78168406124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.623548828125" Y="4.039287076856" />
                  <Point X="27.623548828125" Y="3.565494492376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.623548828125" Y="2.021717773704" />
                  <Point X="27.623548828125" Y="-2.03968857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.623548828125" Y="-3.833132904296" />
                  <Point X="27.623548828125" Y="-3.90549044555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.718548828125" Y="3.972483679319" />
                  <Point X="27.718548828125" Y="3.73003949892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.718548828125" Y="2.075511675311" />
                  <Point X="27.718548828125" Y="-2.056845482116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.718548828125" Y="-3.997677992786" />
                  <Point X="27.718548828125" Y="-4.029296829861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.813548828125" Y="3.904925138427" />
                  <Point X="27.813548828125" Y="3.894584505465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.813548828125" Y="2.130359964906" />
                  <Point X="27.813548828125" Y="-2.079226719137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.908548828125" Y="2.185208254501" />
                  <Point X="27.908548828125" Y="-1.19804264576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.908548828125" Y="-1.638052284108" />
                  <Point X="27.908548828125" Y="-2.129365630587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.003548828125" Y="2.240056544096" />
                  <Point X="28.003548828125" Y="1.436785283318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.003548828125" Y="1.361414295184" />
                  <Point X="28.003548828125" Y="-1.076280027359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.003548828125" Y="-1.784564292165" />
                  <Point X="28.003548828125" Y="-2.184213978824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.098548828125" Y="2.294904833692" />
                  <Point X="28.098548828125" Y="1.660249567979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.098548828125" Y="1.120250872173" />
                  <Point X="28.098548828125" Y="-0.98328815149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.098548828125" Y="-1.871382959139" />
                  <Point X="28.098548828125" Y="-2.239062327061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.193548828125" Y="2.349753123287" />
                  <Point X="28.193548828125" Y="1.759623037555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.193548828125" Y="1.029982129456" />
                  <Point X="28.193548828125" Y="-0.947655911554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.193548828125" Y="-1.944279104973" />
                  <Point X="28.193548828125" Y="-2.293910675298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.288548828125" Y="2.404601412882" />
                  <Point X="28.288548828125" Y="1.832519205169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.288548828125" Y="0.980587056254" />
                  <Point X="28.288548828125" Y="-0.927007164796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.288548828125" Y="-2.017175250807" />
                  <Point X="28.288548828125" Y="-2.348759023535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.383548828125" Y="2.459449702477" />
                  <Point X="28.383548828125" Y="1.905415372782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.383548828125" Y="0.95998734717" />
                  <Point X="28.383548828125" Y="0.152241195679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.383548828125" Y="-0.214786183155" />
                  <Point X="28.383548828125" Y="-0.909527115086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.383548828125" Y="-2.090071396641" />
                  <Point X="28.383548828125" Y="-2.403607371772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.478548828125" Y="2.514297992073" />
                  <Point X="28.478548828125" Y="1.978311540396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.478548828125" Y="0.95163004475" />
                  <Point X="28.478548828125" Y="0.290554663843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.478548828125" Y="-0.353435152012" />
                  <Point X="28.478548828125" Y="-0.914751968204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.478548828125" Y="-2.162967542475" />
                  <Point X="28.478548828125" Y="-2.458455720009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.573548828125" Y="2.569146281668" />
                  <Point X="28.573548828125" Y="2.051207708009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.573548828125" Y="0.962400424665" />
                  <Point X="28.573548828125" Y="0.362371037301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.573548828125" Y="-0.42493066529" />
                  <Point X="28.573548828125" Y="-0.927258982963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.573548828125" Y="-2.235863688308" />
                  <Point X="28.573548828125" Y="-2.513304068246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.668548828125" Y="2.623994571263" />
                  <Point X="28.668548828125" Y="2.124103875623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.668548828125" Y="0.974907422367" />
                  <Point X="28.668548828125" Y="0.413191252304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.668548828125" Y="-0.475561475062" />
                  <Point X="28.668548828125" Y="-0.939765997723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.668548828125" Y="-2.308759834142" />
                  <Point X="28.668548828125" Y="-2.568152416483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.763548828125" Y="2.678842860858" />
                  <Point X="28.763548828125" Y="2.197000043236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.763548828125" Y="0.987414420069" />
                  <Point X="28.763548828125" Y="0.440526273716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.763548828125" Y="-0.503086515557" />
                  <Point X="28.763548828125" Y="-0.952273012483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.763548828125" Y="-2.381655979976" />
                  <Point X="28.763548828125" Y="-2.62300076472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.858548828125" Y="2.733691150454" />
                  <Point X="28.858548828125" Y="2.26989621085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.858548828125" Y="0.999921417772" />
                  <Point X="28.858548828125" Y="0.465981478818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.858548828125" Y="-0.528541658441" />
                  <Point X="28.858548828125" Y="-0.964780027242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.858548828125" Y="-2.45455212581" />
                  <Point X="28.858548828125" Y="-2.677849112957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.953548828125" Y="2.762682490093" />
                  <Point X="28.953548828125" Y="2.342792378463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.953548828125" Y="1.012428415474" />
                  <Point X="28.953548828125" Y="0.49143668392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.953548828125" Y="-0.553996801325" />
                  <Point X="28.953548828125" Y="-0.977287042002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.953548828125" Y="-2.527448271644" />
                  <Point X="28.953548828125" Y="-2.732697461194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.048548828125" Y="2.629446885098" />
                  <Point X="29.048548828125" Y="2.415688546077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.048548828125" Y="1.024935413176" />
                  <Point X="29.048548828125" Y="0.516891889023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.048548828125" Y="-0.579451944208" />
                  <Point X="29.048548828125" Y="-0.989794056762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.048548828125" Y="-2.600344417478" />
                  <Point X="29.048548828125" Y="-2.692477203023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.143548828125" Y="1.037442410879" />
                  <Point X="29.143548828125" Y="0.542347094125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.143548828125" Y="-0.604907087092" />
                  <Point X="29.143548828125" Y="-1.002301071522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.238548828125" Y="1.049949408581" />
                  <Point X="29.238548828125" Y="0.567802299227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.238548828125" Y="-0.630362229976" />
                  <Point X="29.238548828125" Y="-1.014808086281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.333548828125" Y="1.062456406283" />
                  <Point X="29.333548828125" Y="0.59325750433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.333548828125" Y="-0.65581737286" />
                  <Point X="29.333548828125" Y="-1.027315101041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.428548828125" Y="1.074963403986" />
                  <Point X="29.428548828125" Y="0.618712709432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.428548828125" Y="-0.681272515743" />
                  <Point X="29.428548828125" Y="-1.039822115801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.523548828125" Y="1.087470401688" />
                  <Point X="29.523548828125" Y="0.644167914534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.523548828125" Y="-0.706727658627" />
                  <Point X="29.523548828125" Y="-1.05232913056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.618548828125" Y="1.09997739939" />
                  <Point X="29.618548828125" Y="0.669623119636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.618548828125" Y="-0.732182801511" />
                  <Point X="29.618548828125" Y="-1.06483614532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.713548828125" Y="1.074976501912" />
                  <Point X="29.713548828125" Y="0.695078324739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.713548828125" Y="-0.757637944395" />
                  <Point X="29.713548828125" Y="-1.07734316008" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999837890625" Y="0.001626708984" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978485839844" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.471740234375" Y="-3.537859619141" />
                  <Point X="25.46423828125" Y="-3.521426025391" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.274208984375" Y="-3.266360351562" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.991208984375" Y="-3.187805908203" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.726501953125" Y="-3.271625976562" />
                  <Point X="24.711640625" Y="-3.28576171875" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.648412109375" Y="-4.873396484375" />
                  <Point X="23.690826171875" Y="-4.551221679688" />
                  <Point X="23.690228515625" Y="-4.534309570312" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.6136015625" Y="-4.202526367187" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.35060546875" Y="-3.985752685547" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.0099921875" Y="-3.973877197266" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.1441640625" Y="-4.167609375" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.49359375" Y="-2.629764160156" />
                  <Point X="22.49430078125" Y="-2.628509277344" />
                  <Point X="22.50018359375" Y="-2.597241699219" />
                  <Point X="22.486013671875" Y="-2.568755615234" />
                  <Point X="22.468673828125" Y="-2.551415771484" />
                  <Point X="22.439845703125" Y="-2.537198974609" />
                  <Point X="22.4083203125" Y="-2.543468261719" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="20.838294921875" Y="-2.847126220703" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="21.8355625" Y="-1.42364465332" />
                  <Point X="21.836240234375" Y="-1.423124267578" />
                  <Point X="21.854181640625" Y="-1.395999389648" />
                  <Point X="21.8618828125" Y="-1.366266845703" />
                  <Point X="21.863451171875" Y="-1.351914916992" />
                  <Point X="21.85475" Y="-1.325496582031" />
                  <Point X="21.838828125" Y="-1.310631713867" />
                  <Point X="21.812359375" Y="-1.295052978516" />
                  <Point X="21.780474609375" Y="-1.288570678711" />
                  <Point X="20.19671875" Y="-1.497076171875" />
                  <Point X="20.072607421875" Y="-1.011185791016" />
                  <Point X="20.00160546875" Y="-0.5147421875" />
                  <Point X="21.441794921875" Y="-0.128844161987" />
                  <Point X="21.4581171875" Y="-0.121416015625" />
                  <Point X="21.485857421875" Y="-0.102163574219" />
                  <Point X="21.505107421875" Y="-0.075892486572" />
                  <Point X="21.514353515625" Y="-0.04610036087" />
                  <Point X="21.514966796875" Y="-0.018603521347" />
                  <Point X="21.514349609375" Y="-0.016444688797" />
                  <Point X="21.505103515625" Y="0.013347281456" />
                  <Point X="21.48584375" Y="0.039613044739" />
                  <Point X="21.458103515625" Y="0.058865486145" />
                  <Point X="21.44253125" Y="0.066086601257" />
                  <Point X="20.0018125" Y="0.45212600708" />
                  <Point X="20.08235546875" Y="0.996415771484" />
                  <Point X="20.226484375" Y="1.528298706055" />
                  <Point X="21.246203125" Y="1.394050292969" />
                  <Point X="21.246748046875" Y="1.393978637695" />
                  <Point X="21.268287109375" Y="1.396002197266" />
                  <Point X="21.268326171875" Y="1.395876220703" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.346724609375" Y="1.424532348633" />
                  <Point X="21.360892578125" Y="1.443815917969" />
                  <Point X="21.38552734375" Y="1.503290283203" />
                  <Point X="21.38931640625" Y="1.523766967773" />
                  <Point X="21.38443359375" Y="1.544010742188" />
                  <Point X="21.383669921875" Y="1.545541259766" />
                  <Point X="21.353943359375" Y="1.602642456055" />
                  <Point X="21.340029296875" Y="1.619224121094" />
                  <Point X="20.52389453125" Y="2.245466796875" />
                  <Point X="20.83998828125" Y="2.787007080078" />
                  <Point X="21.225330078125" Y="3.282310791016" />
                  <Point X="21.840517578125" Y="2.927131835938" />
                  <Point X="21.840828125" Y="2.926952636719" />
                  <Point X="21.86153125" Y="2.920446533203" />
                  <Point X="21.861529296875" Y="2.920431152344" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.96812109375" Y="2.915641113281" />
                  <Point X="21.986181640625" Y="2.926847900391" />
                  <Point X="21.986779296875" Y="2.927434814453" />
                  <Point X="22.04747265625" Y="2.988127929688" />
                  <Point X="22.05813671875" Y="3.003933837891" />
                  <Point X="22.061923828125" Y="3.027884033203" />
                  <Point X="22.054443359375" Y="3.113390625" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.692720703125" Y="3.749276611328" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="23.03207421875" Y="4.287703613281" />
                  <Point X="23.032111328125" Y="4.287655273438" />
                  <Point X="23.048384765625" Y="4.27385546875" />
                  <Point X="23.04880078125" Y="4.273635742188" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.162287109375" Y="4.218672851563" />
                  <Point X="23.1862421875" Y="4.222270996094" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.301365234375" Y="4.273947265625" />
                  <Point X="23.31393359375" Y="4.294539550781" />
                  <Point X="23.346197265625" Y="4.396864746094" />
                  <Point X="23.348083984375" Y="4.418426757812" />
                  <Point X="23.31086328125" Y="4.701141113281" />
                  <Point X="24.031904296875" Y="4.903295898438" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.957763671875" Y="4.311264160156" />
                  <Point X="24.975619140625" Y="4.284539550781" />
                  <Point X="25.0060546875" Y="4.274205078125" />
                  <Point X="25.0364921875" Y="4.284531738281" />
                  <Point X="25.054353515625" Y="4.311251953125" />
                  <Point X="25.236638671875" Y="4.990869628906" />
                  <Point X="25.8602109375" Y="4.925564941406" />
                  <Point X="26.50845703125" Y="4.769057617188" />
                  <Point X="26.931041015625" Y="4.615783203125" />
                  <Point X="27.338703125" Y="4.425133300781" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.23028515625" Y="2.504343994141" />
                  <Point X="27.224841796875" Y="2.491474365234" />
                  <Point X="27.2033828125" Y="2.411228271484" />
                  <Point X="27.202048828125" Y="2.392291015625" />
                  <Point X="27.210416015625" Y="2.322900878906" />
                  <Point X="27.218650390625" Y="2.300858398438" />
                  <Point X="27.218705078125" Y="2.300780029297" />
                  <Point X="27.261642578125" Y="2.237503173828" />
                  <Point X="27.27497265625" Y="2.224182128906" />
                  <Point X="27.33825" Y="2.18124609375" />
                  <Point X="27.360123046875" Y="2.173006591797" />
                  <Point X="27.360373046875" Y="2.172975830078" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.448705078125" Y="2.16595703125" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103125" Y="2.192415039062" />
                  <Point X="28.994248046875" Y="3.031430419922" />
                  <Point X="29.202591796875" Y="2.741882080078" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.289220703125" Y="1.593545166016" />
                  <Point X="28.279341796875" Y="1.583794921875" />
                  <Point X="28.22158984375" Y="1.508451538086" />
                  <Point X="28.213109375" Y="1.491461914062" />
                  <Point X="28.191595703125" Y="1.414535766602" />
                  <Point X="28.1910234375" Y="1.390970703125" />
                  <Point X="28.1907890625" Y="1.390922485352" />
                  <Point X="28.20844921875" Y="1.305332519531" />
                  <Point X="28.215669921875" Y="1.287918457031" />
                  <Point X="28.263703125" Y="1.214909667969" />
                  <Point X="28.28099609375" Y="1.198822875977" />
                  <Point X="28.280982421875" Y="1.198800170898" />
                  <Point X="28.35058984375" Y="1.159617431641" />
                  <Point X="28.36861328125" Y="1.15361340332" />
                  <Point X="28.462728515625" Y="1.141174926758" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.848974609375" Y="1.321953125" />
                  <Point X="29.939193359375" Y="0.951364013672" />
                  <Point X="29.997859375" Y="0.574556335449" />
                  <Point X="28.741818359375" Y="0.238001098633" />
                  <Point X="28.729041015625" Y="0.232792480469" />
                  <Point X="28.636578125" Y="0.179346893311" />
                  <Point X="28.62223828125" Y="0.166891311646" />
                  <Point X="28.566759765625" Y="0.096199081421" />
                  <Point X="28.5570859375" Y="0.075238975525" />
                  <Point X="28.5569765625" Y="0.07468649292" />
                  <Point X="28.538484375" Y="-0.021875694275" />
                  <Point X="28.538494140625" Y="-0.040732788086" />
                  <Point X="28.556986328125" Y="-0.137294967651" />
                  <Point X="28.5665390625" Y="-0.158475799561" />
                  <Point X="28.566787109375" Y="-0.15879447937" />
                  <Point X="28.622265625" Y="-0.229486862183" />
                  <Point X="28.636625" Y="-0.241933624268" />
                  <Point X="28.729087890625" Y="-0.295379364014" />
                  <Point X="28.74116796875" Y="-0.300387023926" />
                  <Point X="29.9980703125" Y="-0.637172729492" />
                  <Point X="29.948431640625" Y="-0.96640447998" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="28.412736328125" Y="-1.097727294922" />
                  <Point X="28.39474609375" Y="-1.098361083984" />
                  <Point X="28.2132734375" Y="-1.137805053711" />
                  <Point X="28.20152734375" Y="-1.141946289062" />
                  <Point X="28.185328125" Y="-1.154840454102" />
                  <Point X="28.075703125" Y="-1.286685302734" />
                  <Point X="28.06434765625" Y="-1.314191772461" />
                  <Point X="28.048630859375" Y="-1.485000976562" />
                  <Point X="28.05641015625" Y="-1.516700317383" />
                  <Point X="28.156841796875" Y="-1.672912719727" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.20413671875" Y="-2.802134033203" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="27.754126953125" Y="-2.259603271484" />
                  <Point X="27.737234375" Y="-2.253293212891" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.48898828125" Y="-2.219291992188" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.292525390625" Y="-2.328404785156" />
                  <Point X="27.288470703125" Y="-2.334931884766" />
                  <Point X="27.19412109375" Y="-2.514201416016" />
                  <Point X="27.1891875" Y="-2.546500976562" />
                  <Point X="27.22819140625" Y="-2.762465087891" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.986673828125" Y="-4.082087890625" />
                  <Point X="27.8352890625" Y="-4.190218261719" />
                  <Point X="27.679775390625" Y="-4.29087890625" />
                  <Point X="26.68369140625" Y="-2.992755126953" />
                  <Point X="26.670341796875" Y="-2.980333251953" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.425630859375" Y="-2.835733154297" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165244140625" Y="-2.868583984375" />
                  <Point X="25.985349609375" Y="-3.018159667969" />
                  <Point X="25.9684296875" Y="-3.046110107422" />
                  <Point X="25.914642578125" Y="-3.29357421875" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="26.127642578125" Y="-4.934029296875" />
                  <Point X="25.994353515625" Y="-4.96324609375" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#116" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-9.6622281E-05" Y="4.355485072851" Z="0" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0" />
                  <Point X="-0.983568725817" Y="4.983827131987" Z="0" />
                  <Point X="-0.983964085579" Y="4.983780860901" />
                  <Point X="-1.75013969171" Y="4.768972089767" Z="0" />
                  <Point X="-1.750522971153" Y="4.768864631653" />
                  <Point X="-1.750501532614" Y="4.768701789618" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0" />
                  <Point X="-1.739909006476" Y="4.340855288267" Z="0" />
                  <Point X="-1.739925146103" Y="4.340804100037" />
                  <Point X="-1.839049462616" Y="4.299745118618" Z="0" />
                  <Point X="-1.839099049568" Y="4.299724578857" />
                  <Point X="-1.934267596543" Y="4.349266541243" Z="0" />
                  <Point X="-1.93431520462" Y="4.349291324615" />
                  <Point X="-1.934415191889" Y="4.349421630621" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0" />
                  <Point X="-2.786204850912" Y="4.24771355319" Z="0" />
                  <Point X="-2.786530971527" Y="4.247532367706" />
                  <Point X="-3.378373192787" Y="3.793773431182" Z="0" />
                  <Point X="-3.378669261932" Y="3.793546438217" />
                  <Point X="-3.378480689645" Y="3.793219821453" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0" />
                  <Point X="-2.994043754697" Y="3.054806330204" Z="0" />
                  <Point X="-2.99404001236" Y="3.054763555527" />
                  <Point X="-3.054733193755" Y="2.994070374131" Z="0" />
                  <Point X="-3.054763555527" Y="2.99404001236" />
                  <Point X="-3.140270135403" Y="3.001520944476" Z="0" />
                  <Point X="-3.14031291008" Y="3.001524686813" />
                  <Point X="-3.140639526844" Y="3.001713259101" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0" />
                  <Point X="-4.207468392491" Y="2.84663088882" Z="0" />
                  <Point X="-4.207675457001" Y="2.846364736557" />
                  <Point X="-4.547484162569" Y="2.264191035628" Z="0" />
                  <Point X="-4.547654151917" Y="2.263899803162" />
                  <Point X="-4.547228605986" Y="2.263573270321" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0" />
                  <Point X="-3.6668373034" Y="1.553732945859" Z="0" />
                  <Point X="-3.666822433472" Y="1.553704380989" />
                  <Point X="-3.691457584023" Y="1.494229862571" Z="0" />
                  <Point X="-3.691469907761" Y="1.494200110435" />
                  <Point X="-3.752865357399" Y="1.474842218578" Z="0" />
                  <Point X="-3.75289607048" Y="1.47483253479" />
                  <Point X="-3.753427870393" Y="1.474902547598" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0" />
                  <Point X="-4.972752604961" Y="1.038223003507" Z="0" />
                  <Point X="-4.9728307724" Y="1.037934541702" />
                  <Point X="-5.06028330493" Y="0.446938670442" Z="0" />
                  <Point X="-5.06032705307" Y="0.446643024683" />
                  <Point X="-5.059585169911" Y="0.446444237687" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0" />
                  <Point X="-3.548821735859" Y="0.029816582101" Z="0" />
                  <Point X="-3.548807859421" Y="0.029806951061" />
                  <Point X="-3.539561367273" Y="1.4903476E-05" Z="0" />
                  <Point X="-3.539556741714" Y="0" />
                  <Point X="-3.548803233862" Y="-0.029792047585" Z="0" />
                  <Point X="-3.548807859421" Y="-0.029806951061" />
                  <Point X="-3.576546859264" Y="-0.049059401033" Z="0" />
                  <Point X="-3.576560735703" Y="-0.049069032073" />
                  <Point X="-3.577302618861" Y="-0.049267819069" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0" />
                  <Point X="-4.982700556755" Y="-0.989399299189" Z="0" />
                  <Point X="-4.982661724091" Y="-0.989670813084" />
                  <Point X="-4.846971203327" Y="-1.520894142777" Z="0" />
                  <Point X="-4.846903324127" Y="-1.521159887314" />
                  <Point X="-4.846089451909" Y="-1.521052739024" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0" />
                  <Point X="-3.192689547062" Y="-1.322442010105" Z="0" />
                  <Point X="-3.192676305771" Y="-1.322449803352" />
                  <Point X="-3.200376949906" Y="-1.352182296574" Z="0" />
                  <Point X="-3.200380802155" Y="-1.352197170258" />
                  <Point X="-3.201032062054" Y="-1.352696899533" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0" />
                  <Point X="-4.209501650333" Y="-2.843640974998" Z="0" />
                  <Point X="-4.209354877472" Y="-2.843887090683" />
                  <Point X="-3.862412441969" Y="-3.299698023081" Z="0" />
                  <Point X="-3.862238883972" Y="-3.299926042557" />
                  <Point X="-3.861594184279" Y="-3.299553825021" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0" />
                  <Point X="-2.555499644876" Y="-2.572830824256" Z="0" />
                  <Point X="-2.555490970612" Y="-2.57283949852" />
                  <Point X="-2.555863188148" Y="-2.573484198213" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0" />
                  <Point X="-2.890680236816" Y="-4.177344523668" Z="0" />
                  <Point X="-2.890475511551" Y="-4.177502155304" />
                  <Point X="-2.451337432861" Y="-4.449405549049" Z="0" />
                  <Point X="-2.451117753983" Y="-4.449541568756" />
                  <Point X="-2.451005306244" Y="-4.449395024061" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0" />
                  <Point X="-1.968384979844" Y="-3.984170551658" Z="0" />
                  <Point X="-1.968255996704" Y="-3.984084367752" />
                  <Point X="-1.65882164371" Y="-4.004365788817" Z="0" />
                  <Point X="-1.658666849136" Y="-4.004375934601" />
                  <Point X="-1.425522609591" Y="-4.208838223696" Z="0" />
                  <Point X="-1.425405979156" Y="-4.208940505981" />
                  <Point X="-1.364908798993" Y="-4.513080085278" Z="0" />
                  <Point X="-1.364878535271" Y="-4.51323223114" />
                  <Point X="-1.364902645528" Y="-4.51341536665" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0" />
                  <Point X="-1.117549686909" Y="-4.955545532227" Z="0" />
                  <Point X="-1.117401838303" Y="-4.955583572388" />
                  <Point X="-0.817817947209" Y="-5.013733861208" Z="0" />
                  <Point X="-0.81766808033" Y="-5.013762950897" />
                  <Point X="-0.81746778968" Y="-5.013015455961" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0" />
                  <Point X="-0.253440943211" Y="-3.28299062705" Z="0" />
                  <Point X="-0.253359079361" Y="-3.282872676849" />
                  <Point X="-0.00012667954" Y="-3.204278685093" Z="0" />
                  <Point X="-1E-12" Y="-3.204239368439" />
                  <Point X="0.253232399821" Y="-3.282833360195" Z="0" />
                  <Point X="0.253359079361" Y="-3.282872676849" />
                  <Point X="0.417004916221" Y="-3.518655128717" Z="0" />
                  <Point X="0.417086780071" Y="-3.518773078918" />
                  <Point X="0.417287070721" Y="-3.519520573854" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0" />
                  <Point X="0.9979199875" Y="-4.981017757893" Z="0" />
                  <Point X="0.998010158539" Y="-4.981001377106" />
                  <Point X="1.176963453293" Y="-4.941774917841" Z="0" />
                  <Point X="1.177052974701" Y="-4.9417552948" />
                  <Point X="1.176943121105" Y="-4.940920874" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0" />
                  <Point X="1.011133108944" Y="-3.025449807405" Z="0" />
                  <Point X="1.01116001606" Y="-3.025326013565" />
                  <Point X="1.191053759277" Y="-2.875750265121" Z="0" />
                  <Point X="1.191143751144" Y="-2.875675439835" />
                  <Point X="1.424113913" Y="-2.854237313628" Z="0" />
                  <Point X="1.424230456352" Y="-2.854226589203" />
                  <Point X="1.637247335374" Y="-2.991176760435" Z="0" />
                  <Point X="1.637353897095" Y="-2.991245269775" />
                  <Point X="1.637866243482" Y="-2.99191297245" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0" />
                  <Point X="2.85717596519" Y="-4.200347187519" Z="0" />
                  <Point X="2.857273578644" Y="-4.200284004211" />
                  <Point X="3.046417984247" Y="-4.06518255949" Z="0" />
                  <Point X="3.04651260376" Y="-4.065114974976" />
                  <Point X="3.046124965906" Y="-4.064443566799" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0" />
                  <Point X="2.232230625868" Y="-2.506316226602" Z="0" />
                  <Point X="2.232211112976" Y="-2.506208181381" />
                  <Point X="2.326642957449" Y="-2.326779955983" Z="0" />
                  <Point X="2.326690196991" Y="-2.326690196991" />
                  <Point X="2.506118422389" Y="-2.232258352518" Z="0" />
                  <Point X="2.506208181381" Y="-2.232211112976" />
                  <Point X="2.722190576911" Y="-2.271217383623" Z="0" />
                  <Point X="2.722298622131" Y="-2.271236896515" />
                  <Point X="2.722970030308" Y="-2.271624534369" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0" />
                  <Point X="4.239634948254" Y="-2.798543760419" Z="0" />
                  <Point X="4.239722251892" Y="-2.798419713974" />
                  <Point X="4.399128802538" Y="-2.540475897312" Z="0" />
                  <Point X="4.399208545685" Y="-2.540346860886" />
                  <Point X="4.398605312824" Y="-2.53988398397" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0" />
                  <Point X="3.092312746644" Y="-1.458380643129" Z="0" />
                  <Point X="3.092262506485" Y="-1.458302497864" />
                  <Point X="3.10798359251" Y="-1.287457696617" Z="0" />
                  <Point X="3.107991456985" Y="-1.287372231483" />
                  <Point X="3.21768065691" Y="-1.155450176954" Z="0" />
                  <Point X="3.217735528946" Y="-1.15538418293" />
                  <Point X="3.399209077835" Y="-1.115940156341" Z="0" />
                  <Point X="3.399299860001" Y="-1.115920424461" />
                  <Point X="3.400053713441" Y="-1.116019671142" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0" />
                  <Point X="4.991396173" Y="-0.94460777697" Z="0" />
                  <Point X="4.991438388824" Y="-0.944422781467" />
                  <Point X="5.047986829281" Y="-0.569348926634" Z="0" />
                  <Point X="5.048015117645" Y="-0.569161295891" />
                  <Point X="5.047365086436" Y="-0.568987120561" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0" />
                  <Point X="3.655488969684" Y="-0.167364943579" Z="0" />
                  <Point X="3.655442714691" Y="-0.167338207364" />
                  <Point X="3.599964476705" Y="-0.096645895049" Z="0" />
                  <Point X="3.599936723709" Y="-0.096610531211" />
                  <Point X="3.581443977714" Y="-4.8305305E-05" Z="0" />
                  <Point X="3.581434726715" Y="-3.9E-11" />
                  <Point X="3.599927472711" Y="0.096562225945" Z="0" />
                  <Point X="3.599936723709" Y="0.096610531211" />
                  <Point X="3.655414961696" Y="0.167302843526" Z="0" />
                  <Point X="3.655442714691" Y="0.167338207364" />
                  <Point X="3.747906444669" Y="0.220783900663" Z="0" />
                  <Point X="3.747952699661" Y="0.220810636878" />
                  <Point X="3.74860273087" Y="0.220984812208" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0" />
                  <Point X="4.982145835638" Y="0.992228941679" Z="0" />
                  <Point X="4.982112884521" Y="0.992440581322" />
                  <Point X="4.88083612895" Y="1.408455042362" Z="0" />
                  <Point X="4.88078546524" Y="1.408663153648" />
                  <Point X="4.880076633215" Y="1.408569834113" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0" />
                  <Point X="3.36900769937" Y="1.234462357044" Z="0" />
                  <Point X="3.368960618973" Y="1.234468579292" />
                  <Point X="3.299353373051" Y="1.273651429772" Z="0" />
                  <Point X="3.299318552017" Y="1.273671030998" />
                  <Point X="3.251285024643" Y="1.346679881275" Z="0" />
                  <Point X="3.251260995865" Y="1.346716403961" />
                  <Point X="3.233600867867" Y="1.432306388617" Z="0" />
                  <Point X="3.233592033386" Y="1.432349205017" />
                  <Point X="3.255105223894" Y="1.509275220335" Z="0" />
                  <Point X="3.25511598587" Y="1.509313702583" />
                  <Point X="3.312869267941" Y="1.584657410085" Z="0" />
                  <Point X="3.312898159027" Y="1.584695100784" />
                  <Point X="3.313465365887" Y="1.5851303339" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0" />
                  <Point X="4.238288539171" Y="2.800574601531" Z="0" />
                  <Point X="4.23818397522" Y="2.800747394562" />
                  <Point X="4.002379123211" Y="3.128462645411" Z="0" />
                  <Point X="4.002261161804" Y="3.128626585007" />
                  <Point X="4.001514970183" Y="3.128195771098" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0" />
                  <Point X="2.429631798863" Y="2.245539909601" Z="0" />
                  <Point X="2.429591655731" Y="2.245529174805" />
                  <Point X="2.360201500535" Y="2.253896342278" Z="0" />
                  <Point X="2.360166788101" Y="2.253900527954" />
                  <Point X="2.296889726639" Y="2.296836593509" Z="0" />
                  <Point X="2.296858072281" Y="2.296858072281" />
                  <Point X="2.253922006726" Y="2.360135133743" Z="0" />
                  <Point X="2.253900527954" Y="2.360166788101" />
                  <Point X="2.245533360481" Y="2.429556943297" Z="0" />
                  <Point X="2.245529174805" Y="2.429591655731" />
                  <Point X="2.266988033056" Y="2.509837777019" Z="0" />
                  <Point X="2.266998767853" Y="2.509877920151" />
                  <Point X="2.267429581761" Y="2.510624111772" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0" />
                  <Point X="2.753685403347" Y="4.268898616076" Z="0" />
                  <Point X="2.753497838974" Y="4.269032001495" />
                  <Point X="2.355961587667" Y="4.500637542009" Z="0" />
                  <Point X="2.355762720108" Y="4.50075340271" />
                  <Point X="1.944237104297" Y="4.693210169554" Z="0" />
                  <Point X="1.944031238556" Y="4.693306446075" />
                  <Point X="1.516951227129" Y="4.848211521626" Z="0" />
                  <Point X="1.516737580299" Y="4.848289012909" />
                  <Point X="0.86288575235" Y="5.006149000645" Z="0" />
                  <Point X="0.862558662891" Y="5.006227970123" />
                  <Point X="0.193579218604" Y="5.076287984848" Z="0" />
                  <Point X="0.193244561553" Y="5.076323032379" />
                  <Point X="0.193147939272" Y="5.0759624331" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>