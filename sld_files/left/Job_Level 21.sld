<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#157" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1610" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004715820312" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.278787109375" Y="-4.101850585938" />
                  <Point X="-24.4366953125" Y="-3.512524414063" />
                  <Point X="-24.44227734375" Y="-3.497139648438" />
                  <Point X="-24.45763671875" Y="-3.467376708984" />
                  <Point X="-24.5221796875" Y="-3.374384521484" />
                  <Point X="-24.621365234375" Y="-3.231476318359" />
                  <Point X="-24.643248046875" Y="-3.209021484375" />
                  <Point X="-24.66950390625" Y="-3.18977734375" />
                  <Point X="-24.69750390625" Y="-3.175669433594" />
                  <Point X="-24.797376953125" Y="-3.144672119141" />
                  <Point X="-24.95086328125" Y="-3.097036132812" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097036132812" />
                  <Point X="-25.136697265625" Y="-3.128033203125" />
                  <Point X="-25.29018359375" Y="-3.175669433594" />
                  <Point X="-25.31818359375" Y="-3.18977734375" />
                  <Point X="-25.344439453125" Y="-3.209021484375" />
                  <Point X="-25.36632421875" Y="-3.231477294922" />
                  <Point X="-25.430865234375" Y="-3.324469238281" />
                  <Point X="-25.53005078125" Y="-3.467377685547" />
                  <Point X="-25.5381875" Y="-3.481571777344" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.793662109375" Y="-4.418187988281" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.191677734375" Y="-4.816446289062" />
                  <Point X="-26.246416015625" Y="-4.802362304688" />
                  <Point X="-26.233970703125" Y="-4.707822753906" />
                  <Point X="-26.2149609375" Y="-4.563438476562" />
                  <Point X="-26.21419921875" Y="-4.5479296875" />
                  <Point X="-26.2165078125" Y="-4.51622265625" />
                  <Point X="-26.240369140625" Y="-4.396270507812" />
                  <Point X="-26.277037109375" Y="-4.211931152344" />
                  <Point X="-26.287939453125" Y="-4.182964355469" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131204589844" />
                  <Point X="-26.415595703125" Y="-4.050564697266" />
                  <Point X="-26.556904296875" Y="-3.926639648438" />
                  <Point X="-26.5831875" Y="-3.910295898438" />
                  <Point X="-26.612884765625" Y="-3.897994628906" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.765068359375" Y="-3.882967285156" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708496094" />
                  <Point X="-27.014466796875" Y="-3.882028076172" />
                  <Point X="-27.042658203125" Y="-3.894801757812" />
                  <Point X="-27.14434765625" Y="-3.962749267578" />
                  <Point X="-27.300623046875" Y="-4.067169921875" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.3351015625" Y="-4.099461914062" />
                  <Point X="-27.47134375" Y="-4.277015136719" />
                  <Point X="-27.4801484375" Y="-4.288489257813" />
                  <Point X="-27.541822265625" Y="-4.250302246094" />
                  <Point X="-27.80171484375" Y="-4.089382568359" />
                  <Point X="-27.957263671875" Y="-3.969615478516" />
                  <Point X="-28.10472265625" Y="-3.856077392578" />
                  <Point X="-27.71721875" Y="-3.184901855469" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647653564453" />
                  <Point X="-27.406587890625" Y="-2.616125488281" />
                  <Point X="-27.405576171875" Y="-2.585189941406" />
                  <Point X="-27.414560546875" Y="-2.555570800781" />
                  <Point X="-27.428779296875" Y="-2.526740966797" />
                  <Point X="-27.446802734375" Y="-2.501588378906" />
                  <Point X="-27.46414453125" Y="-2.484245605469" />
                  <Point X="-27.489298828125" Y="-2.46621875" />
                  <Point X="-27.51812890625" Y="-2.451999023438" />
                  <Point X="-27.547748046875" Y="-2.443012451172" />
                  <Point X="-27.57868359375" Y="-2.444023925781" />
                  <Point X="-27.610212890625" Y="-2.450294433594" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.420298828125" Y="-2.912175537109" />
                  <Point X="-28.8180234375" Y="-3.141801513672" />
                  <Point X="-28.877537109375" Y="-3.063613525391" />
                  <Point X="-29.082857421875" Y="-2.793861816406" />
                  <Point X="-29.19437890625" Y="-2.606860595703" />
                  <Point X="-29.306142578125" Y="-2.419450195312" />
                  <Point X="-28.619408203125" Y="-1.892500976562" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.084578125" Y="-1.475594726562" />
                  <Point X="-28.06661328125" Y="-1.44846484375" />
                  <Point X="-28.053857421875" Y="-1.419835449219" />
                  <Point X="-28.04615234375" Y="-1.390088256836" />
                  <Point X="-28.04334765625" Y="-1.359663574219" />
                  <Point X="-28.0455546875" Y="-1.32799230957" />
                  <Point X="-28.0525546875" Y="-1.298243164062" />
                  <Point X="-28.068638671875" Y="-1.272256469727" />
                  <Point X="-28.08947265625" Y="-1.248299438477" />
                  <Point X="-28.112970703125" Y="-1.228767211914" />
                  <Point X="-28.139453125" Y="-1.213180786133" />
                  <Point X="-28.168716796875" Y="-1.201956787109" />
                  <Point X="-28.200603515625" Y="-1.195474975586" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-29.218013671875" Y="-1.324204589844" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.753771484375" Y="-1.307041503906" />
                  <Point X="-29.834076171875" Y="-0.992650634766" />
                  <Point X="-29.86358203125" Y="-0.786359375" />
                  <Point X="-29.892423828125" Y="-0.584698425293" />
                  <Point X="-29.11777734375" Y="-0.377132415771" />
                  <Point X="-28.532875" Y="-0.220408279419" />
                  <Point X="-28.51112109375" Y="-0.211575897217" />
                  <Point X="-28.484611328125" Y="-0.196762069702" />
                  <Point X="-28.47678515625" Y="-0.191875930786" />
                  <Point X="-28.45997265625" Y="-0.180207199097" />
                  <Point X="-28.436015625" Y="-0.15867741394" />
                  <Point X="-28.414671875" Y="-0.12728276062" />
                  <Point X="-28.403599609375" Y="-0.100794281006" />
                  <Point X="-28.39884765625" Y="-0.086218460083" />
                  <Point X="-28.390998046875" Y="-0.05334249115" />
                  <Point X="-28.38840234375" Y="-0.032045944214" />
                  <Point X="-28.390654296875" Y="-0.010710348129" />
                  <Point X="-28.396546875" Y="0.015858580589" />
                  <Point X="-28.401005859375" Y="0.030364578247" />
                  <Point X="-28.41403515625" Y="0.06316009903" />
                  <Point X="-28.425294921875" Y="0.083687049866" />
                  <Point X="-28.441232421875" Y="0.100837043762" />
                  <Point X="-28.4639921875" Y="0.119948516846" />
                  <Point X="-28.4709140625" Y="0.125239776611" />
                  <Point X="-28.4877265625" Y="0.136908813477" />
                  <Point X="-28.501923828125" Y="0.145046905518" />
                  <Point X="-28.532875" Y="0.157848297119" />
                  <Point X="-29.43173828125" Y="0.398698150635" />
                  <Point X="-29.89181640625" Y="0.521975585938" />
                  <Point X="-29.8762421875" Y="0.627225830078" />
                  <Point X="-29.82448828125" Y="0.976968200684" />
                  <Point X="-29.76509375" Y="1.196154296875" />
                  <Point X="-29.70355078125" Y="1.423268066406" />
                  <Point X="-29.1849375" Y="1.354991210938" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228149414" />
                  <Point X="-28.703134765625" Y="1.305263793945" />
                  <Point X="-28.678921875" Y="1.3128984375" />
                  <Point X="-28.641708984375" Y="1.324631225586" />
                  <Point X="-28.62277734375" Y="1.332961547852" />
                  <Point X="-28.604033203125" Y="1.343783447266" />
                  <Point X="-28.5873515625" Y="1.356014892578" />
                  <Point X="-28.573712890625" Y="1.371567138672" />
                  <Point X="-28.561298828125" Y="1.38929675293" />
                  <Point X="-28.551349609375" Y="1.407434692383" />
                  <Point X="-28.541634765625" Y="1.430891357422" />
                  <Point X="-28.526703125" Y="1.466939086914" />
                  <Point X="-28.520916015625" Y="1.486788208008" />
                  <Point X="-28.51715625" Y="1.508104125977" />
                  <Point X="-28.515802734375" Y="1.528750244141" />
                  <Point X="-28.518951171875" Y="1.549199829102" />
                  <Point X="-28.5245546875" Y="1.570106811523" />
                  <Point X="-28.532048828125" Y="1.589376220703" />
                  <Point X="-28.543771484375" Y="1.611896972656" />
                  <Point X="-28.5617890625" Y="1.646506103516" />
                  <Point X="-28.573283203125" Y="1.663709228516" />
                  <Point X="-28.5871953125" Y="1.680288330078" />
                  <Point X="-28.60213671875" Y="1.694590332031" />
                  <Point X="-29.1177265625" Y="2.090216796875" />
                  <Point X="-29.351859375" Y="2.269873046875" />
                  <Point X="-29.282248046875" Y="2.3891328125" />
                  <Point X="-29.081146484375" Y="2.733666015625" />
                  <Point X="-28.92382421875" Y="2.935883056641" />
                  <Point X="-28.75050390625" Y="3.158661865234" />
                  <Point X="-28.464162109375" Y="2.993341796875" />
                  <Point X="-28.20665625" Y="2.844671142578" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.113068359375" Y="2.822845947266" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.04056640625" Y="2.818762939453" />
                  <Point X="-28.019107421875" Y="2.821587890625" />
                  <Point X="-27.999015625" Y="2.826504394531" />
                  <Point X="-27.980462890625" Y="2.835653320313" />
                  <Point X="-27.962208984375" Y="2.847282470703" />
                  <Point X="-27.946078125" Y="2.860228759766" />
                  <Point X="-27.922140625" Y="2.884166015625" />
                  <Point X="-27.885353515625" Y="2.920952148438" />
                  <Point X="-27.872404296875" Y="2.937084472656" />
                  <Point X="-27.860775390625" Y="2.955338378906" />
                  <Point X="-27.85162890625" Y="2.973887695312" />
                  <Point X="-27.8467109375" Y="2.993977050781" />
                  <Point X="-27.843884765625" Y="3.015435791016" />
                  <Point X="-27.84343359375" Y="3.036122070312" />
                  <Point X="-27.846384765625" Y="3.069845703125" />
                  <Point X="-27.85091796875" Y="3.121671630859" />
                  <Point X="-27.854955078125" Y="3.141961181641" />
                  <Point X="-27.86146484375" Y="3.162604248047" />
                  <Point X="-27.869794921875" Y="3.181533203125" />
                  <Point X="-28.09826953125" Y="3.577261474609" />
                  <Point X="-28.18333203125" Y="3.724595947266" />
                  <Point X="-28.0508671875" Y="3.826156494141" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.45283984375" Y="4.232347167969" />
                  <Point X="-27.16703515625" Y="4.391133789062" />
                  <Point X="-27.122025390625" Y="4.332474609375" />
                  <Point X="-27.0431953125" Y="4.229741210938" />
                  <Point X="-27.028890625" Y="4.214798339844" />
                  <Point X="-27.012310546875" Y="4.200886230469" />
                  <Point X="-26.995111328125" Y="4.18939453125" />
                  <Point X="-26.957576171875" Y="4.16985546875" />
                  <Point X="-26.89989453125" Y="4.139827636719" />
                  <Point X="-26.8806171875" Y="4.132331542969" />
                  <Point X="-26.8597109375" Y="4.126729492187" />
                  <Point X="-26.839267578125" Y="4.123582519531" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.777451171875" Y="4.134482421875" />
                  <Point X="-26.738357421875" Y="4.150676269531" />
                  <Point X="-26.67827734375" Y="4.175562011719" />
                  <Point X="-26.66014453125" Y="4.185509765625" />
                  <Point X="-26.6424140625" Y="4.197924316406" />
                  <Point X="-26.626859375" Y="4.211565429688" />
                  <Point X="-26.614626953125" Y="4.22825" />
                  <Point X="-26.603806640625" Y="4.246994140625" />
                  <Point X="-26.595478515625" Y="4.265921386719" />
                  <Point X="-26.58275390625" Y="4.306278320312" />
                  <Point X="-26.56319921875" Y="4.368297851562" />
                  <Point X="-26.5591640625" Y="4.388583496094" />
                  <Point X="-26.55727734375" Y="4.410144042969" />
                  <Point X="-26.557728515625" Y="4.430826660156" />
                  <Point X="-26.583705078125" Y="4.628125976562" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.40304296875" Y="4.682688476562" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.64925" Y="4.844963867188" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.210080078125" Y="4.570612792969" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.73671484375" Y="4.723217285156" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.55185546875" Y="4.873200195312" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.907435546875" Y="4.77173828125" />
                  <Point X="-23.5189765625" Y="4.677952148438" />
                  <Point X="-23.358107421875" Y="4.619603515625" />
                  <Point X="-23.105349609375" Y="4.527926269531" />
                  <Point X="-22.94893359375" Y="4.454775878906" />
                  <Point X="-22.705423828125" Y="4.340893554688" />
                  <Point X="-22.55428515625" Y="4.252840820312" />
                  <Point X="-22.31902734375" Y="4.115777832031" />
                  <Point X="-22.176501953125" Y="4.014423828125" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.51276171875" Y="3.139396484375" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857921875" Y="2.539933105469" />
                  <Point X="-22.866919921875" Y="2.516060302734" />
                  <Point X="-22.875384765625" Y="2.484411376953" />
                  <Point X="-22.888390625" Y="2.435773925781" />
                  <Point X="-22.891380859375" Y="2.417935791016" />
                  <Point X="-22.892271484375" Y="2.380951660156" />
                  <Point X="-22.888970703125" Y="2.353584472656" />
                  <Point X="-22.883900390625" Y="2.311526855469" />
                  <Point X="-22.87855859375" Y="2.289609130859" />
                  <Point X="-22.87029296875" Y="2.26751953125" />
                  <Point X="-22.859927734375" Y="2.247468017578" />
                  <Point X="-22.8429921875" Y="2.22251171875" />
                  <Point X="-22.81696875" Y="2.184159423828" />
                  <Point X="-22.80553515625" Y="2.170329101562" />
                  <Point X="-22.77840234375" Y="2.145593505859" />
                  <Point X="-22.753447265625" Y="2.128659423828" />
                  <Point X="-22.71509375" Y="2.102635986328" />
                  <Point X="-22.695044921875" Y="2.092271728516" />
                  <Point X="-22.67295703125" Y="2.084006103516" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.62366796875" Y="2.075363525391" />
                  <Point X="-22.581611328125" Y="2.070291992188" />
                  <Point X="-22.563533203125" Y="2.069845458984" />
                  <Point X="-22.52679296875" Y="2.074170898438" />
                  <Point X="-22.495142578125" Y="2.082634277344" />
                  <Point X="-22.446505859375" Y="2.095640625" />
                  <Point X="-22.4347109375" Y="2.099639648438" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.507380859375" Y="2.632118408203" />
                  <Point X="-21.032671875" Y="2.906191650391" />
                  <Point X="-21.01627734375" Y="2.883406738281" />
                  <Point X="-20.87673046875" Y="2.689467285156" />
                  <Point X="-20.797275390625" Y="2.558166992188" />
                  <Point X="-20.73780078125" Y="2.459883544922" />
                  <Point X="-21.32202734375" Y="2.01158984375" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.778572265625" Y="1.660243896484" />
                  <Point X="-21.79602734375" Y="1.641626708984" />
                  <Point X="-21.8188046875" Y="1.611911254883" />
                  <Point X="-21.85380859375" Y="1.566245361328" />
                  <Point X="-21.863392578125" Y="1.550912353516" />
                  <Point X="-21.878369140625" Y="1.517086669922" />
                  <Point X="-21.886853515625" Y="1.486747070312" />
                  <Point X="-21.89989453125" Y="1.440122436523" />
                  <Point X="-21.90334765625" Y="1.417825805664" />
                  <Point X="-21.9041640625" Y="1.394254272461" />
                  <Point X="-21.902259765625" Y="1.371764160156" />
                  <Point X="-21.89529296875" Y="1.338007568359" />
                  <Point X="-21.88458984375" Y="1.286131347656" />
                  <Point X="-21.879318359375" Y="1.268973999023" />
                  <Point X="-21.863716796875" Y="1.2357421875" />
                  <Point X="-21.8447734375" Y="1.206947631836" />
                  <Point X="-21.81566015625" Y="1.162696655273" />
                  <Point X="-21.801109375" Y="1.145452880859" />
                  <Point X="-21.7838671875" Y="1.12936340332" />
                  <Point X="-21.76565234375" Y="1.116034667969" />
                  <Point X="-21.73819921875" Y="1.100581054688" />
                  <Point X="-21.696009765625" Y="1.076832397461" />
                  <Point X="-21.6794765625" Y="1.069500976563" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.60676171875" Y="1.054532958984" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.6529765625" Y="1.160050415039" />
                  <Point X="-20.22316015625" Y="1.216636352539" />
                  <Point X="-20.214001953125" Y="1.179011352539" />
                  <Point X="-20.15405859375" Y="0.932787475586" />
                  <Point X="-20.129025390625" Y="0.771997070312" />
                  <Point X="-20.109134765625" Y="0.644238647461" />
                  <Point X="-20.77093359375" Y="0.466909851074" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.295208984375" Y="0.32558605957" />
                  <Point X="-21.318453125" Y="0.315067749023" />
                  <Point X="-21.354919921875" Y="0.293988769531" />
                  <Point X="-21.410962890625" Y="0.261595184326" />
                  <Point X="-21.4256875" Y="0.251096755981" />
                  <Point X="-21.45246875" Y="0.22557699585" />
                  <Point X="-21.474349609375" Y="0.197696136475" />
                  <Point X="-21.507974609375" Y="0.154849365234" />
                  <Point X="-21.51969921875" Y="0.135568344116" />
                  <Point X="-21.52947265625" Y="0.114105194092" />
                  <Point X="-21.536318359375" Y="0.092603179932" />
                  <Point X="-21.543611328125" Y="0.054519130707" />
                  <Point X="-21.5548203125" Y="-0.004007438183" />
                  <Point X="-21.556515625" Y="-0.021874984741" />
                  <Point X="-21.5548203125" Y="-0.058552696228" />
                  <Point X="-21.54752734375" Y="-0.09663659668" />
                  <Point X="-21.536318359375" Y="-0.155163162231" />
                  <Point X="-21.52947265625" Y="-0.176665176392" />
                  <Point X="-21.51969921875" Y="-0.198128326416" />
                  <Point X="-21.507974609375" Y="-0.217409347534" />
                  <Point X="-21.48609375" Y="-0.245290359497" />
                  <Point X="-21.45246875" Y="-0.288137145996" />
                  <Point X="-21.44" Y="-0.301236175537" />
                  <Point X="-21.410962890625" Y="-0.324155303955" />
                  <Point X="-21.37449609375" Y="-0.345234130859" />
                  <Point X="-21.318453125" Y="-0.377627716064" />
                  <Point X="-21.3072890625" Y="-0.383138763428" />
                  <Point X="-21.283419921875" Y="-0.392149810791" />
                  <Point X="-20.49584375" Y="-0.603180419922" />
                  <Point X="-20.108525390625" Y="-0.706961914062" />
                  <Point X="-20.111509765625" Y="-0.726753295898" />
                  <Point X="-20.144974609375" Y="-0.948723815918" />
                  <Point X="-20.177052734375" Y="-1.089293457031" />
                  <Point X="-20.19882421875" Y="-1.18469909668" />
                  <Point X="-20.98127734375" Y="-1.081687133789" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710266113" />
                  <Point X="-21.62533984375" Y="-1.005508666992" />
                  <Point X="-21.696912109375" Y="-1.021065368652" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.8360234375" Y="-1.056596069336" />
                  <Point X="-21.8638515625" Y="-1.07348840332" />
                  <Point X="-21.8876015625" Y="-1.093959838867" />
                  <Point X="-21.93086328125" Y="-1.145989624023" />
                  <Point X="-21.997345703125" Y="-1.225947875977" />
                  <Point X="-22.0120703125" Y="-1.250335327148" />
                  <Point X="-22.023412109375" Y="-1.277721069336" />
                  <Point X="-22.030240234375" Y="-1.305364868164" />
                  <Point X="-22.03644140625" Y="-1.372745849609" />
                  <Point X="-22.04596875" Y="-1.476295288086" />
                  <Point X="-22.043650390625" Y="-1.50756237793" />
                  <Point X="-22.035919921875" Y="-1.539182739258" />
                  <Point X="-22.023548828125" Y="-1.567996337891" />
                  <Point X="-21.983939453125" Y="-1.629606445312" />
                  <Point X="-21.923068359375" Y="-1.724286987305" />
                  <Point X="-21.9130625" Y="-1.73724206543" />
                  <Point X="-21.889369140625" Y="-1.760909301758" />
                  <Point X="-21.158494140625" Y="-2.321730224609" />
                  <Point X="-20.786876953125" Y="-2.6068828125" />
                  <Point X="-20.875197265625" Y="-2.749796630859" />
                  <Point X="-20.94152734375" Y="-2.844044189453" />
                  <Point X="-20.971017578125" Y="-2.8859453125" />
                  <Point X="-21.669705078125" Y="-2.482557617188" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.2457734375" Y="-2.159825195312" />
                  <Point X="-22.330955078125" Y="-2.14444140625" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135177001953" />
                  <Point X="-22.625931640625" Y="-2.172420654297" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.757615234375" Y="-2.246549560547" />
                  <Point X="-22.77857421875" Y="-2.267509033203" />
                  <Point X="-22.795466796875" Y="-2.290438476562" />
                  <Point X="-22.8327109375" Y="-2.361204589844" />
                  <Point X="-22.889947265625" Y="-2.469956542969" />
                  <Point X="-22.899771484375" Y="-2.499735107422" />
                  <Point X="-22.904728515625" Y="-2.531908447266" />
                  <Point X="-22.90432421875" Y="-2.563259521484" />
                  <Point X="-22.888939453125" Y="-2.648442382812" />
                  <Point X="-22.865296875" Y="-2.779349853516" />
                  <Point X="-22.86101171875" Y="-2.795140136719" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.378517578125" Y="-3.639555664062" />
                  <Point X="-22.138712890625" Y="-4.054906494141" />
                  <Point X="-22.21813671875" Y="-4.111635742188" />
                  <Point X="-22.292318359375" Y="-4.159652832031" />
                  <Point X="-22.298232421875" Y="-4.163481445312" />
                  <Point X="-22.837517578125" Y="-3.460672851563" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.362087890625" Y="-2.846544189453" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.674783203125" Y="-2.749571777344" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-23.9663515625" Y="-2.854453369141" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968860839844" />
                  <Point X="-24.11275" Y="-2.996687255859" />
                  <Point X="-24.124375" Y="-3.025809082031" />
                  <Point X="-24.145587890625" Y="-3.123408447266" />
                  <Point X="-24.178189453125" Y="-3.273396972656" />
                  <Point X="-24.180275390625" Y="-3.289627197266" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.047158203125" Y="-4.334103027344" />
                  <Point X="-23.97793359375" Y="-4.859915527344" />
                  <Point X="-24.024318359375" Y="-4.870083007812" />
                  <Point X="-24.07068359375" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.05842578125" Y="-4.75263671875" />
                  <Point X="-26.14124609375" Y="-4.731328125" />
                  <Point X="-26.139783203125" Y="-4.720221679688" />
                  <Point X="-26.1207734375" Y="-4.575837402344" />
                  <Point X="-26.120076171875" Y="-4.568098632812" />
                  <Point X="-26.11944921875" Y="-4.541030761719" />
                  <Point X="-26.1217578125" Y="-4.509323730469" />
                  <Point X="-26.123333984375" Y="-4.497687988281" />
                  <Point X="-26.1471953125" Y="-4.377735839844" />
                  <Point X="-26.18386328125" Y="-4.193396484375" />
                  <Point X="-26.188126953125" Y="-4.178467285156" />
                  <Point X="-26.199029296875" Y="-4.149500488281" />
                  <Point X="-26.20566796875" Y="-4.135462890625" />
                  <Point X="-26.221740234375" Y="-4.107625976562" />
                  <Point X="-26.230576171875" Y="-4.094860351562" />
                  <Point X="-26.250208984375" Y="-4.0709375" />
                  <Point X="-26.261005859375" Y="-4.059780029297" />
                  <Point X="-26.35295703125" Y="-3.979140136719" />
                  <Point X="-26.494265625" Y="-3.855215087891" />
                  <Point X="-26.50673828125" Y="-3.845965087891" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822527587891" />
                  <Point X="-26.576529296875" Y="-3.810226318359" />
                  <Point X="-26.5913125" Y="-3.805476318359" />
                  <Point X="-26.621455078125" Y="-3.798447998047" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.75885546875" Y="-3.788170654297" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166015625" />
                  <Point X="-27.008005859375" Y="-3.781945556641" />
                  <Point X="-27.0390546875" Y="-3.790265136719" />
                  <Point X="-27.053673828125" Y="-3.795496337891" />
                  <Point X="-27.081865234375" Y="-3.808270019531" />
                  <Point X="-27.0954375" Y="-3.8158125" />
                  <Point X="-27.197126953125" Y="-3.883760009766" />
                  <Point X="-27.35340234375" Y="-3.988180664062" />
                  <Point X="-27.35968359375" Y="-3.992759277344" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032770507812" />
                  <Point X="-27.410470703125" Y="-4.041629394531" />
                  <Point X="-27.503201171875" Y="-4.162478515625" />
                  <Point X="-27.74759375" Y="-4.011156494141" />
                  <Point X="-27.899306640625" Y="-3.894343017578" />
                  <Point X="-27.98086328125" Y="-3.831546630859" />
                  <Point X="-27.634947265625" Y="-3.232401855469" />
                  <Point X="-27.341490234375" Y="-2.724119628906" />
                  <Point X="-27.3348515625" Y="-2.710084228516" />
                  <Point X="-27.32394921875" Y="-2.681118164062" />
                  <Point X="-27.319685546875" Y="-2.6661875" />
                  <Point X="-27.3134140625" Y="-2.634659423828" />
                  <Point X="-27.311638671875" Y="-2.619230712891" />
                  <Point X="-27.310626953125" Y="-2.588295166016" />
                  <Point X="-27.314666015625" Y="-2.557614257812" />
                  <Point X="-27.323650390625" Y="-2.527995117188" />
                  <Point X="-27.329359375" Y="-2.513550048828" />
                  <Point X="-27.343578125" Y="-2.484720214844" />
                  <Point X="-27.35155859375" Y="-2.471406982422" />
                  <Point X="-27.36958203125" Y="-2.446254394531" />
                  <Point X="-27.379625" Y="-2.434415039062" />
                  <Point X="-27.396966796875" Y="-2.417072265625" />
                  <Point X="-27.408806640625" Y="-2.40702734375" />
                  <Point X="-27.4339609375" Y="-2.389000488281" />
                  <Point X="-27.447275390625" Y="-2.381018554688" />
                  <Point X="-27.47610546875" Y="-2.366798828125" />
                  <Point X="-27.490546875" Y="-2.361091064453" />
                  <Point X="-27.520166015625" Y="-2.352104492188" />
                  <Point X="-27.5508515625" Y="-2.348063232422" />
                  <Point X="-27.581787109375" Y="-2.349074707031" />
                  <Point X="-27.59721484375" Y="-2.350848632812" />
                  <Point X="-27.628744140625" Y="-2.357119140625" />
                  <Point X="-27.64367578125" Y="-2.361383056641" />
                  <Point X="-27.67264453125" Y="-2.372285888672" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.467798828125" Y="-2.829903076172" />
                  <Point X="-28.793087890625" Y="-3.017708740234" />
                  <Point X="-28.801943359375" Y="-3.006074951172" />
                  <Point X="-29.004017578125" Y="-2.740587158203" />
                  <Point X="-29.112787109375" Y="-2.558201660156" />
                  <Point X="-29.181265625" Y="-2.443373535156" />
                  <Point X="-28.561576171875" Y="-1.967869628906" />
                  <Point X="-28.048123046875" Y="-1.573882080078" />
                  <Point X="-28.036484375" Y="-1.563311279297" />
                  <Point X="-28.015107421875" Y="-1.540392578125" />
                  <Point X="-28.005369140625" Y="-1.528045043945" />
                  <Point X="-27.987404296875" Y="-1.500915039063" />
                  <Point X="-27.9798359375" Y="-1.487128173828" />
                  <Point X="-27.967080078125" Y="-1.458498779297" />
                  <Point X="-27.961892578125" Y="-1.44365612793" />
                  <Point X="-27.9541875" Y="-1.413908935547" />
                  <Point X="-27.951552734375" Y="-1.398808837891" />
                  <Point X="-27.948748046875" Y="-1.368384155273" />
                  <Point X="-27.948578125" Y="-1.353059448242" />
                  <Point X="-27.95078515625" Y="-1.321388183594" />
                  <Point X="-27.953080078125" Y="-1.306233032227" />
                  <Point X="-27.960080078125" Y="-1.276483886719" />
                  <Point X="-27.971775390625" Y="-1.24824621582" />
                  <Point X="-27.987859375" Y="-1.222259521484" />
                  <Point X="-27.996953125" Y="-1.209916503906" />
                  <Point X="-28.017787109375" Y="-1.185959472656" />
                  <Point X="-28.02874609375" Y="-1.175242797852" />
                  <Point X="-28.052244140625" Y="-1.155710571289" />
                  <Point X="-28.06478515625" Y="-1.146895141602" />
                  <Point X="-28.091267578125" Y="-1.13130859375" />
                  <Point X="-28.10543359375" Y="-1.124481323242" />
                  <Point X="-28.134697265625" Y="-1.113257202148" />
                  <Point X="-28.14979296875" Y="-1.108860717773" />
                  <Point X="-28.1816796875" Y="-1.10237890625" />
                  <Point X="-28.197296875" Y="-1.100532592773" />
                  <Point X="-28.22862109375" Y="-1.09944152832" />
                  <Point X="-28.244328125" Y="-1.100196777344" />
                  <Point X="-29.2304140625" Y="-1.230017333984" />
                  <Point X="-29.66091796875" Y="-1.286694335938" />
                  <Point X="-29.6617265625" Y="-1.283530273438" />
                  <Point X="-29.740759765625" Y="-0.974114135742" />
                  <Point X="-29.7695390625" Y="-0.77290838623" />
                  <Point X="-29.786451171875" Y="-0.654654541016" />
                  <Point X="-29.093189453125" Y="-0.46889541626" />
                  <Point X="-28.508287109375" Y="-0.312171203613" />
                  <Point X="-28.49713671875" Y="-0.308429901123" />
                  <Point X="-28.4753828125" Y="-0.299597412109" />
                  <Point X="-28.464779296875" Y="-0.294506164551" />
                  <Point X="-28.43826953125" Y="-0.279692321777" />
                  <Point X="-28.422619140625" Y="-0.269920532227" />
                  <Point X="-28.405806640625" Y="-0.258251678467" />
                  <Point X="-28.39647265625" Y="-0.250866317749" />
                  <Point X="-28.372515625" Y="-0.229336654663" />
                  <Point X="-28.357453125" Y="-0.21208895874" />
                  <Point X="-28.336109375" Y="-0.18069430542" />
                  <Point X="-28.327021484375" Y="-0.163921020508" />
                  <Point X="-28.31594921875" Y="-0.137432540894" />
                  <Point X="-28.313279296875" Y="-0.130240524292" />
                  <Point X="-28.3064453125" Y="-0.108280899048" />
                  <Point X="-28.298595703125" Y="-0.075404914856" />
                  <Point X="-28.2966953125" Y="-0.064836334229" />
                  <Point X="-28.294099609375" Y="-0.043539855957" />
                  <Point X="-28.293927734375" Y="-0.022074098587" />
                  <Point X="-28.2961796875" Y="-0.000738534689" />
                  <Point X="-28.297908203125" Y="0.009859320641" />
                  <Point X="-28.30380078125" Y="0.036428344727" />
                  <Point X="-28.305740234375" Y="0.043771507263" />
                  <Point X="-28.31271875" Y="0.065440284729" />
                  <Point X="-28.325748046875" Y="0.098235717773" />
                  <Point X="-28.330744140625" Y="0.108848731995" />
                  <Point X="-28.34200390625" Y="0.129375640869" />
                  <Point X="-28.355705078125" Y="0.148357162476" />
                  <Point X="-28.371642578125" Y="0.165507217407" />
                  <Point X="-28.380142578125" Y="0.173589630127" />
                  <Point X="-28.40290234375" Y="0.192701049805" />
                  <Point X="-28.41674609375" Y="0.203283599854" />
                  <Point X="-28.43355859375" Y="0.214952728271" />
                  <Point X="-28.440482421875" Y="0.219328323364" />
                  <Point X="-28.465615234375" Y="0.232834442139" />
                  <Point X="-28.49656640625" Y="0.245635925293" />
                  <Point X="-28.508287109375" Y="0.249611297607" />
                  <Point X="-29.407150390625" Y="0.490461151123" />
                  <Point X="-29.7854453125" Y="0.591825073242" />
                  <Point X="-29.782265625" Y="0.613319702148" />
                  <Point X="-29.73133203125" Y="0.957521179199" />
                  <Point X="-29.673400390625" Y="1.171307495117" />
                  <Point X="-29.6335859375" Y="1.318237060547" />
                  <Point X="-29.197337890625" Y="1.260803955078" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815673828" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.204703369141" />
                  <Point X="-28.71514453125" Y="1.20658972168" />
                  <Point X="-28.704890625" Y="1.208053466797" />
                  <Point X="-28.6846015625" Y="1.212089111328" />
                  <Point X="-28.67456640625" Y="1.214661010742" />
                  <Point X="-28.650353515625" Y="1.222295654297" />
                  <Point X="-28.613140625" Y="1.234028442383" />
                  <Point X="-28.603447265625" Y="1.237677001953" />
                  <Point X="-28.584515625" Y="1.246007324219" />
                  <Point X="-28.57527734375" Y="1.250689086914" />
                  <Point X="-28.556533203125" Y="1.261510986328" />
                  <Point X="-28.547859375" Y="1.267171142578" />
                  <Point X="-28.531177734375" Y="1.279402587891" />
                  <Point X="-28.51592578125" Y="1.293377807617" />
                  <Point X="-28.502287109375" Y="1.308930053711" />
                  <Point X="-28.495892578125" Y="1.317078369141" />
                  <Point X="-28.483478515625" Y="1.334807983398" />
                  <Point X="-28.478005859375" Y="1.343608276367" />
                  <Point X="-28.468056640625" Y="1.36174621582" />
                  <Point X="-28.463580078125" Y="1.371083618164" />
                  <Point X="-28.453865234375" Y="1.394540283203" />
                  <Point X="-28.43893359375" Y="1.430588012695" />
                  <Point X="-28.4355" Y="1.440348510742" />
                  <Point X="-28.429712890625" Y="1.460197631836" />
                  <Point X="-28.427359375" Y="1.470286499023" />
                  <Point X="-28.423599609375" Y="1.491602416992" />
                  <Point X="-28.422359375" Y="1.501889526367" />
                  <Point X="-28.421005859375" Y="1.522535644531" />
                  <Point X="-28.421908203125" Y="1.543206054688" />
                  <Point X="-28.425056640625" Y="1.563655761719" />
                  <Point X="-28.427189453125" Y="1.573793945312" />
                  <Point X="-28.43279296875" Y="1.594700805664" />
                  <Point X="-28.436015625" Y="1.604541137695" />
                  <Point X="-28.443509765625" Y="1.623810546875" />
                  <Point X="-28.44778125" Y="1.633239624023" />
                  <Point X="-28.45950390625" Y="1.655760375977" />
                  <Point X="-28.477521484375" Y="1.690369506836" />
                  <Point X="-28.482798828125" Y="1.699283325195" />
                  <Point X="-28.49429296875" Y="1.716486450195" />
                  <Point X="-28.500509765625" Y="1.724775634766" />
                  <Point X="-28.514421875" Y="1.741354858398" />
                  <Point X="-28.52150390625" Y="1.748915893555" />
                  <Point X="-28.5364453125" Y="1.763217773438" />
                  <Point X="-28.5443046875" Y="1.769958862305" />
                  <Point X="-29.05989453125" Y="2.165585449219" />
                  <Point X="-29.22761328125" Y="2.294280273438" />
                  <Point X="-29.200201171875" Y="2.341242919922" />
                  <Point X="-29.00228125" Y="2.680324951172" />
                  <Point X="-28.84884375" Y="2.877548828125" />
                  <Point X="-28.726337890625" Y="3.035012939453" />
                  <Point X="-28.511662109375" Y="2.911069335938" />
                  <Point X="-28.25415625" Y="2.762398681641" />
                  <Point X="-28.24491796875" Y="2.757716796875" />
                  <Point X="-28.225984375" Y="2.749385742188" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.12134765625" Y="2.728207519531" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.038494140625" Y="2.723785644531" />
                  <Point X="-28.02816796875" Y="2.724575683594" />
                  <Point X="-28.006708984375" Y="2.727400634766" />
                  <Point X="-27.99652734375" Y="2.729310546875" />
                  <Point X="-27.976435546875" Y="2.734227050781" />
                  <Point X="-27.957" Y="2.741301025391" />
                  <Point X="-27.938447265625" Y="2.750449951172" />
                  <Point X="-27.929419921875" Y="2.755531494141" />
                  <Point X="-27.911166015625" Y="2.767160644531" />
                  <Point X="-27.90274609375" Y="2.773193115234" />
                  <Point X="-27.886615234375" Y="2.786139404297" />
                  <Point X="-27.878904296875" Y="2.793053222656" />
                  <Point X="-27.854966796875" Y="2.816990478516" />
                  <Point X="-27.8181796875" Y="2.853776611328" />
                  <Point X="-27.811267578125" Y="2.861484863281" />
                  <Point X="-27.798318359375" Y="2.8776171875" />
                  <Point X="-27.79228125" Y="2.886041259766" />
                  <Point X="-27.78065234375" Y="2.904295166016" />
                  <Point X="-27.7755703125" Y="2.913324707031" />
                  <Point X="-27.766423828125" Y="2.931874023438" />
                  <Point X="-27.759353515625" Y="2.951298339844" />
                  <Point X="-27.754435546875" Y="2.971387695312" />
                  <Point X="-27.7525234375" Y="2.981572509766" />
                  <Point X="-27.749697265625" Y="3.00303125" />
                  <Point X="-27.748908203125" Y="3.013364257812" />
                  <Point X="-27.74845703125" Y="3.034050537109" />
                  <Point X="-27.748794921875" Y="3.044403808594" />
                  <Point X="-27.75174609375" Y="3.078127441406" />
                  <Point X="-27.756279296875" Y="3.129953369141" />
                  <Point X="-27.757744140625" Y="3.140210693359" />
                  <Point X="-27.76178125" Y="3.160500244141" />
                  <Point X="-27.764353515625" Y="3.170532470703" />
                  <Point X="-27.77086328125" Y="3.191175537109" />
                  <Point X="-27.77451171875" Y="3.200869628906" />
                  <Point X="-27.782841796875" Y="3.219798583984" />
                  <Point X="-27.7875234375" Y="3.229033447266" />
                  <Point X="-28.015998046875" Y="3.62476171875" />
                  <Point X="-28.05938671875" Y="3.699915527344" />
                  <Point X="-27.993064453125" Y="3.750765136719" />
                  <Point X="-27.6483671875" Y="4.015040039062" />
                  <Point X="-27.406703125" Y="4.149302734375" />
                  <Point X="-27.1925234375" Y="4.268295898437" />
                  <Point X="-27.118564453125" Y="4.171909667969" />
                  <Point X="-27.1118203125" Y="4.164047363281" />
                  <Point X="-27.097515625" Y="4.149104492188" />
                  <Point X="-27.089955078125" Y="4.1420234375" />
                  <Point X="-27.073375" Y="4.128111328125" />
                  <Point X="-27.065087890625" Y="4.121895507812" />
                  <Point X="-27.047888671875" Y="4.110403808594" />
                  <Point X="-27.0389765625" Y="4.105127929688" />
                  <Point X="-27.00144140625" Y="4.085588867188" />
                  <Point X="-26.943759765625" Y="4.055561035156" />
                  <Point X="-26.93432421875" Y="4.051286132812" />
                  <Point X="-26.915046875" Y="4.043790039063" />
                  <Point X="-26.905205078125" Y="4.040568847656" />
                  <Point X="-26.884298828125" Y="4.034966796875" />
                  <Point X="-26.8741640625" Y="4.032835449219" />
                  <Point X="-26.853720703125" Y="4.029688476562" />
                  <Point X="-26.8330546875" Y="4.028785888672" />
                  <Point X="-26.812416015625" Y="4.030138427734" />
                  <Point X="-26.802134765625" Y="4.031378173828" />
                  <Point X="-26.780818359375" Y="4.035136474609" />
                  <Point X="-26.770728515625" Y="4.037488525391" />
                  <Point X="-26.7508671875" Y="4.043277587891" />
                  <Point X="-26.741095703125" Y="4.046714355469" />
                  <Point X="-26.702001953125" Y="4.062908203125" />
                  <Point X="-26.641921875" Y="4.087793945312" />
                  <Point X="-26.632583984375" Y="4.092272460938" />
                  <Point X="-26.614451171875" Y="4.102220214844" />
                  <Point X="-26.60565625" Y="4.107689453125" />
                  <Point X="-26.58792578125" Y="4.120104003906" />
                  <Point X="-26.579775390625" Y="4.126499511719" />
                  <Point X="-26.564220703125" Y="4.140140625" />
                  <Point X="-26.550244140625" Y="4.15539453125" />
                  <Point X="-26.53801171875" Y="4.172079101562" />
                  <Point X="-26.5323515625" Y="4.180755371094" />
                  <Point X="-26.52153125" Y="4.199499511719" />
                  <Point X="-26.5168515625" Y="4.208733398438" />
                  <Point X="-26.5085234375" Y="4.227660644531" />
                  <Point X="-26.504875" Y="4.237354003906" />
                  <Point X="-26.492150390625" Y="4.2777109375" />
                  <Point X="-26.472595703125" Y="4.33973046875" />
                  <Point X="-26.470025390625" Y="4.349763671875" />
                  <Point X="-26.465990234375" Y="4.370049316406" />
                  <Point X="-26.464525390625" Y="4.380301757812" />
                  <Point X="-26.462638671875" Y="4.401862304688" />
                  <Point X="-26.46230078125" Y="4.412215820312" />
                  <Point X="-26.462751953125" Y="4.4328984375" />
                  <Point X="-26.463541015625" Y="4.443227539062" />
                  <Point X="-26.479265625" Y="4.562655273438" />
                  <Point X="-26.377396484375" Y="4.591215820312" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.63820703125" Y="4.750607910156" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.30184375" Y="4.546024902344" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.644951171875" Y="4.698629394531" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.56175" Y="4.778716796875" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.92973046875" Y="4.679391601562" />
                  <Point X="-23.546408203125" Y="4.586845703125" />
                  <Point X="-23.3905" Y="4.530296386719" />
                  <Point X="-23.141734375" Y="4.440067382812" />
                  <Point X="-22.989177734375" Y="4.368721679688" />
                  <Point X="-22.74955078125" Y="4.256654785156" />
                  <Point X="-22.602107421875" Y="4.170755371094" />
                  <Point X="-22.370578125" Y="4.035865234375" />
                  <Point X="-22.23155859375" Y="3.937003662109" />
                  <Point X="-22.18221875" Y="3.901915039062" />
                  <Point X="-22.595033203125" Y="3.186896484375" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.937619140625" Y="2.593115234375" />
                  <Point X="-22.94681640625" Y="2.573439208984" />
                  <Point X="-22.955814453125" Y="2.54956640625" />
                  <Point X="-22.958693359375" Y="2.540606201172" />
                  <Point X="-22.967158203125" Y="2.508957275391" />
                  <Point X="-22.9801640625" Y="2.460319824219" />
                  <Point X="-22.982083984375" Y="2.451479736328" />
                  <Point X="-22.986353515625" Y="2.420222900391" />
                  <Point X="-22.987244140625" Y="2.383238769531" />
                  <Point X="-22.986587890625" Y="2.369576171875" />
                  <Point X="-22.983287109375" Y="2.342208984375" />
                  <Point X="-22.978216796875" Y="2.300151367188" />
                  <Point X="-22.97619921875" Y="2.289031982422" />
                  <Point X="-22.970857421875" Y="2.267114257812" />
                  <Point X="-22.967533203125" Y="2.256315917969" />
                  <Point X="-22.959267578125" Y="2.234226318359" />
                  <Point X="-22.95468359375" Y="2.223895019531" />
                  <Point X="-22.944318359375" Y="2.203843505859" />
                  <Point X="-22.938537109375" Y="2.194123291016" />
                  <Point X="-22.9216015625" Y="2.169166992187" />
                  <Point X="-22.895578125" Y="2.130814697266" />
                  <Point X="-22.8901875" Y="2.12362890625" />
                  <Point X="-22.869537109375" Y="2.100124267578" />
                  <Point X="-22.842404296875" Y="2.075388671875" />
                  <Point X="-22.83174609375" Y="2.066983642578" />
                  <Point X="-22.806791015625" Y="2.050049560547" />
                  <Point X="-22.7684375" Y="2.024026245117" />
                  <Point X="-22.75871875" Y="2.018245361328" />
                  <Point X="-22.738669921875" Y="2.007881103516" />
                  <Point X="-22.72833984375" Y="2.003297485352" />
                  <Point X="-22.706251953125" Y="1.995031860352" />
                  <Point X="-22.695451171875" Y="1.991707397461" />
                  <Point X="-22.673529296875" Y="1.986365112305" />
                  <Point X="-22.662408203125" Y="1.984346801758" />
                  <Point X="-22.635041015625" Y="1.98104675293" />
                  <Point X="-22.592984375" Y="1.975975219727" />
                  <Point X="-22.58395703125" Y="1.975320922852" />
                  <Point X="-22.55242578125" Y="1.975496948242" />
                  <Point X="-22.515685546875" Y="1.979822509766" />
                  <Point X="-22.502251953125" Y="1.982395385742" />
                  <Point X="-22.4706015625" Y="1.990858764648" />
                  <Point X="-22.42196484375" Y="2.003865112305" />
                  <Point X="-22.416001953125" Y="2.005671142578" />
                  <Point X="-22.395587890625" Y="2.013069458008" />
                  <Point X="-22.372341796875" Y="2.023574829102" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.459880859375" Y="2.549845947266" />
                  <Point X="-21.05959375" Y="2.780951660156" />
                  <Point X="-20.95605078125" Y="2.637048095703" />
                  <Point X="-20.878552734375" Y="2.508983154297" />
                  <Point X="-20.863115234375" Y="2.483471191406" />
                  <Point X="-21.379859375" Y="2.086958251953" />
                  <Point X="-21.827046875" Y="1.743820068359" />
                  <Point X="-21.831857421875" Y="1.739871337891" />
                  <Point X="-21.847875" Y="1.725221191406" />
                  <Point X="-21.865330078125" Y="1.706603881836" />
                  <Point X="-21.87142578125" Y="1.699420532227" />
                  <Point X="-21.894203125" Y="1.669705078125" />
                  <Point X="-21.92920703125" Y="1.62403918457" />
                  <Point X="-21.9343671875" Y="1.616598510742" />
                  <Point X="-21.950259765625" Y="1.589373046875" />
                  <Point X="-21.965236328125" Y="1.555547363281" />
                  <Point X="-21.969859375" Y="1.542671508789" />
                  <Point X="-21.97834375" Y="1.51233190918" />
                  <Point X="-21.991384765625" Y="1.465707275391" />
                  <Point X="-21.993775390625" Y="1.454661865234" />
                  <Point X="-21.997228515625" Y="1.432365356445" />
                  <Point X="-21.998291015625" Y="1.421114135742" />
                  <Point X="-21.999107421875" Y="1.397542602539" />
                  <Point X="-21.998826171875" Y="1.386239013672" />
                  <Point X="-21.996921875" Y="1.363749023438" />
                  <Point X="-21.995298828125" Y="1.35256237793" />
                  <Point X="-21.98833203125" Y="1.318805786133" />
                  <Point X="-21.97762890625" Y="1.26692956543" />
                  <Point X="-21.975400390625" Y="1.25823034668" />
                  <Point X="-21.9653125" Y="1.2286015625" />
                  <Point X="-21.9497109375" Y="1.195369750977" />
                  <Point X="-21.94308203125" Y="1.183529418945" />
                  <Point X="-21.924138671875" Y="1.154734863281" />
                  <Point X="-21.895025390625" Y="1.110483886719" />
                  <Point X="-21.888265625" Y="1.101430664062" />
                  <Point X="-21.87371484375" Y="1.084186889648" />
                  <Point X="-21.865921875" Y="1.07599609375" />
                  <Point X="-21.8486796875" Y="1.059906616211" />
                  <Point X="-21.83996875" Y="1.052697021484" />
                  <Point X="-21.82175390625" Y="1.039368408203" />
                  <Point X="-21.812251953125" Y="1.033249511719" />
                  <Point X="-21.784798828125" Y="1.017795837402" />
                  <Point X="-21.742609375" Y="0.994047180176" />
                  <Point X="-21.73451953125" Y="0.989987854004" />
                  <Point X="-21.705318359375" Y="0.978083312988" />
                  <Point X="-21.66972265625" Y="0.968021118164" />
                  <Point X="-21.656328125" Y="0.965257751465" />
                  <Point X="-21.619208984375" Y="0.960351867676" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.555966796875" Y="0.952199768066" />
                  <Point X="-21.53428125" Y="0.951222839355" />
                  <Point X="-21.50878125" Y="0.952032409668" />
                  <Point X="-21.49939453125" Y="0.952797180176" />
                  <Point X="-20.640576171875" Y="1.06586315918" />
                  <Point X="-20.295296875" Y="1.111319702148" />
                  <Point X="-20.247310546875" Y="0.914213500977" />
                  <Point X="-20.22289453125" Y="0.757382751465" />
                  <Point X="-20.21612890625" Y="0.713921020508" />
                  <Point X="-20.795521484375" Y="0.558672851562" />
                  <Point X="-21.3080078125" Y="0.421352813721" />
                  <Point X="-21.31396875" Y="0.419543792725" />
                  <Point X="-21.334375" Y="0.412136993408" />
                  <Point X="-21.357619140625" Y="0.401618652344" />
                  <Point X="-21.36599609375" Y="0.397315887451" />
                  <Point X="-21.402462890625" Y="0.376236846924" />
                  <Point X="-21.458505859375" Y="0.343843292236" />
                  <Point X="-21.46611328125" Y="0.338947357178" />
                  <Point X="-21.49122265625" Y="0.319872192383" />
                  <Point X="-21.51800390625" Y="0.29435244751" />
                  <Point X="-21.527203125" Y="0.284227661133" />
                  <Point X="-21.549083984375" Y="0.256346893311" />
                  <Point X="-21.582708984375" Y="0.213500076294" />
                  <Point X="-21.58914453125" Y="0.204208480835" />
                  <Point X="-21.600869140625" Y="0.184927474976" />
                  <Point X="-21.606158203125" Y="0.174937927246" />
                  <Point X="-21.615931640625" Y="0.153474700928" />
                  <Point X="-21.61999609375" Y="0.142925445557" />
                  <Point X="-21.626841796875" Y="0.121423423767" />
                  <Point X="-21.629623046875" Y="0.110470657349" />
                  <Point X="-21.636916015625" Y="0.072386619568" />
                  <Point X="-21.648125" Y="0.0138601017" />
                  <Point X="-21.649396484375" Y="0.004966059208" />
                  <Point X="-21.6514140625" Y="-0.02626140213" />
                  <Point X="-21.64971875" Y="-0.062939163208" />
                  <Point X="-21.648125" Y="-0.076420303345" />
                  <Point X="-21.64083203125" Y="-0.114504188538" />
                  <Point X="-21.629623046875" Y="-0.173030700684" />
                  <Point X="-21.626841796875" Y="-0.183983322144" />
                  <Point X="-21.61999609375" Y="-0.205485351562" />
                  <Point X="-21.615931640625" Y="-0.216034744263" />
                  <Point X="-21.606158203125" Y="-0.237497833252" />
                  <Point X="-21.600869140625" Y="-0.247487518311" />
                  <Point X="-21.58914453125" Y="-0.26676852417" />
                  <Point X="-21.582708984375" Y="-0.276059844971" />
                  <Point X="-21.560828125" Y="-0.303940917969" />
                  <Point X="-21.527203125" Y="-0.346787719727" />
                  <Point X="-21.521279296875" Y="-0.353636413574" />
                  <Point X="-21.498859375" Y="-0.375806182861" />
                  <Point X="-21.469822265625" Y="-0.398725341797" />
                  <Point X="-21.45850390625" Y="-0.406403625488" />
                  <Point X="-21.422037109375" Y="-0.427482391357" />
                  <Point X="-21.365994140625" Y="-0.459875946045" />
                  <Point X="-21.36050390625" Y="-0.462813903809" />
                  <Point X="-21.340841796875" Y="-0.472016204834" />
                  <Point X="-21.31697265625" Y="-0.48102734375" />
                  <Point X="-21.3080078125" Y="-0.483912719727" />
                  <Point X="-20.520431640625" Y="-0.69494329834" />
                  <Point X="-20.21512109375" Y="-0.776751098633" />
                  <Point X="-20.238380859375" Y="-0.931040771484" />
                  <Point X="-20.269671875" Y="-1.068157836914" />
                  <Point X="-20.2721953125" Y="-1.079219726563" />
                  <Point X="-20.968876953125" Y="-0.987499816895" />
                  <Point X="-21.563216796875" Y="-0.909253540039" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042419434" />
                  <Point X="-21.63327734375" Y="-0.910840881348" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.71708984375" Y="-0.928233032227" />
                  <Point X="-21.82708203125" Y="-0.952140197754" />
                  <Point X="-21.842123046875" Y="-0.956742004395" />
                  <Point X="-21.8712421875" Y="-0.968365661621" />
                  <Point X="-21.885318359375" Y="-0.975386901855" />
                  <Point X="-21.913146484375" Y="-0.99227923584" />
                  <Point X="-21.925876953125" Y="-1.001530395508" />
                  <Point X="-21.949626953125" Y="-1.02200189209" />
                  <Point X="-21.9606484375" Y="-1.033222290039" />
                  <Point X="-22.00391015625" Y="-1.085252075195" />
                  <Point X="-22.070392578125" Y="-1.165210327148" />
                  <Point X="-22.078671875" Y="-1.176845092773" />
                  <Point X="-22.093396484375" Y="-1.201232543945" />
                  <Point X="-22.099841796875" Y="-1.213985229492" />
                  <Point X="-22.11118359375" Y="-1.24137097168" />
                  <Point X="-22.115640625" Y="-1.254940185547" />
                  <Point X="-22.12246875" Y="-1.282584106445" />
                  <Point X="-22.12483984375" Y="-1.296658691406" />
                  <Point X="-22.131041015625" Y="-1.364039672852" />
                  <Point X="-22.140568359375" Y="-1.467588989258" />
                  <Point X="-22.140708984375" Y="-1.483319946289" />
                  <Point X="-22.138390625" Y="-1.514587036133" />
                  <Point X="-22.135931640625" Y="-1.530123291016" />
                  <Point X="-22.128201171875" Y="-1.561743652344" />
                  <Point X="-22.12321484375" Y="-1.576662475586" />
                  <Point X="-22.11084375" Y="-1.605476074219" />
                  <Point X="-22.103458984375" Y="-1.61937097168" />
                  <Point X="-22.063849609375" Y="-1.680981079102" />
                  <Point X="-22.002978515625" Y="-1.775661621094" />
                  <Point X="-21.99825390625" Y="-1.782356811523" />
                  <Point X="-21.980201171875" Y="-1.804454223633" />
                  <Point X="-21.9565078125" Y="-1.828121459961" />
                  <Point X="-21.947201171875" Y="-1.836277832031" />
                  <Point X="-21.216326171875" Y="-2.397098632812" />
                  <Point X="-20.912828125" Y="-2.629981201172" />
                  <Point X="-20.9545078125" Y="-2.697424316406" />
                  <Point X="-20.998724609375" Y="-2.760251708984" />
                  <Point X="-21.622205078125" Y="-2.40028515625" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.184970703125" Y="-2.079513916016" />
                  <Point X="-22.216876953125" Y="-2.069326416016" />
                  <Point X="-22.228888671875" Y="-2.066337646484" />
                  <Point X="-22.3140703125" Y="-2.050953857422" />
                  <Point X="-22.444978515625" Y="-2.027311889648" />
                  <Point X="-22.460638671875" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.55515625" Y="-2.03513659668" />
                  <Point X="-22.584931640625" Y="-2.044960571289" />
                  <Point X="-22.59941015625" Y="-2.051109130859" />
                  <Point X="-22.67017578125" Y="-2.088352783203" />
                  <Point X="-22.778927734375" Y="-2.145588134766" />
                  <Point X="-22.791029296875" Y="-2.153170166016" />
                  <Point X="-22.8139609375" Y="-2.170063720703" />
                  <Point X="-22.824791015625" Y="-2.179375244141" />
                  <Point X="-22.84575" Y="-2.200334716797" />
                  <Point X="-22.85505859375" Y="-2.211161132813" />
                  <Point X="-22.871951171875" Y="-2.234090576172" />
                  <Point X="-22.87953515625" Y="-2.246193603516" />
                  <Point X="-22.916779296875" Y="-2.316959716797" />
                  <Point X="-22.974015625" Y="-2.425711669922" />
                  <Point X="-22.9801640625" Y="-2.440193115234" />
                  <Point X="-22.98998828125" Y="-2.469971679688" />
                  <Point X="-22.9936640625" Y="-2.485268798828" />
                  <Point X="-22.99862109375" Y="-2.517442138672" />
                  <Point X="-22.999720703125" Y="-2.533133544922" />
                  <Point X="-22.99931640625" Y="-2.564484619141" />
                  <Point X="-22.9978125" Y="-2.580144287109" />
                  <Point X="-22.982427734375" Y="-2.665327148438" />
                  <Point X="-22.95878515625" Y="-2.796234619141" />
                  <Point X="-22.95698046875" Y="-2.804230957031" />
                  <Point X="-22.94876171875" Y="-2.831540771484" />
                  <Point X="-22.935927734375" Y="-2.862479248047" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.4607890625" Y="-3.687055664062" />
                  <Point X="-22.264103515625" Y="-4.027722900391" />
                  <Point X="-22.276244140625" Y="-4.036083007813" />
                  <Point X="-22.7621484375" Y="-3.402840332031" />
                  <Point X="-23.16608203125" Y="-2.876422607422" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849625732422" />
                  <Point X="-23.21674609375" Y="-2.828004638672" />
                  <Point X="-23.22669921875" Y="-2.820646972656" />
                  <Point X="-23.310712890625" Y="-2.766634033203" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.453716796875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663873535156" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.68348828125" Y="-2.654971435547" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920125" Y="-2.699414794922" />
                  <Point X="-23.944505859375" Y="-2.714135742188" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-24.027087890625" Y="-2.781405517578" />
                  <Point X="-24.136123046875" Y="-2.872063720703" />
                  <Point X="-24.147345703125" Y="-2.883087158203" />
                  <Point X="-24.16781640625" Y="-2.906836425781" />
                  <Point X="-24.177064453125" Y="-2.919562255859" />
                  <Point X="-24.19395703125" Y="-2.947388671875" />
                  <Point X="-24.20098046875" Y="-2.961467041016" />
                  <Point X="-24.21260546875" Y="-2.990588867188" />
                  <Point X="-24.21720703125" Y="-3.005632324219" />
                  <Point X="-24.238419921875" Y="-3.103231689453" />
                  <Point X="-24.271021484375" Y="-3.253220214844" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677001953" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520263672" />
                  <Point X="-24.16691015625" Y="-4.152330566406" />
                  <Point X="-24.1870234375" Y="-4.077262939453" />
                  <Point X="-24.344931640625" Y="-3.487936767578" />
                  <Point X="-24.347392578125" Y="-3.480122558594" />
                  <Point X="-24.35785546875" Y="-3.453573486328" />
                  <Point X="-24.37321484375" Y="-3.423810546875" />
                  <Point X="-24.37959375" Y="-3.413208984375" />
                  <Point X="-24.44413671875" Y="-3.320216796875" />
                  <Point X="-24.543322265625" Y="-3.17730859375" />
                  <Point X="-24.553328125" Y="-3.165173339844" />
                  <Point X="-24.5752109375" Y="-3.142718505859" />
                  <Point X="-24.587087890625" Y="-3.132398925781" />
                  <Point X="-24.61334375" Y="-3.113154785156" />
                  <Point X="-24.6267578125" Y="-3.104937988281" />
                  <Point X="-24.6547578125" Y="-3.090830078125" />
                  <Point X="-24.66934375" Y="-3.084938964844" />
                  <Point X="-24.769216796875" Y="-3.053941650391" />
                  <Point X="-24.922703125" Y="-3.006305664062" />
                  <Point X="-24.936623046875" Y="-3.003109619141" />
                  <Point X="-24.964783203125" Y="-2.998840087891" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.998840087891" />
                  <Point X="-25.051064453125" Y="-3.003109619141" />
                  <Point X="-25.064984375" Y="-3.006305664062" />
                  <Point X="-25.164857421875" Y="-3.037302734375" />
                  <Point X="-25.31834375" Y="-3.084938964844" />
                  <Point X="-25.3329296875" Y="-3.090830078125" />
                  <Point X="-25.3609296875" Y="-3.104937988281" />
                  <Point X="-25.37434375" Y="-3.113154785156" />
                  <Point X="-25.400599609375" Y="-3.132398925781" />
                  <Point X="-25.412474609375" Y="-3.142717041016" />
                  <Point X="-25.434359375" Y="-3.165172851562" />
                  <Point X="-25.444369140625" Y="-3.177310546875" />
                  <Point X="-25.50891015625" Y="-3.270302490234" />
                  <Point X="-25.608095703125" Y="-3.4132109375" />
                  <Point X="-25.61246875" Y="-3.420131591797" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213623047" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.88542578125" Y="-4.393600097656" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.247401452508" Y="4.478393558904" />
                  <Point X="-24.638250239603" Y="4.723637726154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.878225361169" Y="4.316832325239" />
                  <Point X="-24.662932067823" Y="4.631524270274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.357573080265" Y="4.754008222674" />
                  <Point X="-25.457939476071" Y="4.771705526211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.604382739687" Y="4.172080954475" />
                  <Point X="-24.687613971687" Y="4.539410827731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.330443246626" Y="4.652758972892" />
                  <Point X="-25.786766659044" Y="4.73322110243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.367818496518" Y="4.033902767604" />
                  <Point X="-24.712295875551" Y="4.447297385188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.303313412988" Y="4.55150972311" />
                  <Point X="-26.049639879734" Y="4.683107215614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.187446450642" Y="3.905632781221" />
                  <Point X="-24.736977779416" Y="4.355183942646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.276184275862" Y="4.450260596142" />
                  <Point X="-26.260866673909" Y="4.623886670347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.231301914103" Y="3.816900154552" />
                  <Point X="-24.76165968328" Y="4.263070500103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.249055178628" Y="4.349011476209" />
                  <Point X="-26.472093403495" Y="4.56466611369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.281850169157" Y="3.729347647616" />
                  <Point X="-24.808416981147" Y="4.174849545133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.22058587685" Y="4.247526042054" />
                  <Point X="-26.466704002489" Y="4.467250288754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.33239842421" Y="3.641795140681" />
                  <Point X="-24.920395391091" Y="4.098128831934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.14299696238" Y="4.1373794949" />
                  <Point X="-26.465905278296" Y="4.370643924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.382946679264" Y="3.554242633745" />
                  <Point X="-26.491823268281" Y="4.278748436791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.433494934317" Y="3.46669012681" />
                  <Point X="-26.527807080089" Y="4.188627825552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.484043189371" Y="3.379137619875" />
                  <Point X="-26.607911660627" Y="4.10628689625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.14023348642" Y="4.200149596557" />
                  <Point X="-27.273034308373" Y="4.223565964528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.534591444424" Y="3.291585112939" />
                  <Point X="-26.768501399762" Y="4.038137671955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.98291546342" Y="4.075944656422" />
                  <Point X="-27.404835226795" Y="4.150340494399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.585139699478" Y="3.204032606004" />
                  <Point X="-27.536636245753" Y="4.077115041997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.635688101333" Y="3.116480124953" />
                  <Point X="-27.663944205809" Y="4.003097342084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.027582192094" Y="2.736462137189" />
                  <Point X="-21.111134435213" Y="2.751194651949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.686236538914" Y="3.028927650202" />
                  <Point X="-27.766239116716" Y="3.924669166737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.94950381602" Y="2.626229284748" />
                  <Point X="-21.239127655773" Y="2.677297781952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.736784976494" Y="2.941375175451" />
                  <Point X="-27.868534027623" Y="3.84624099139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.88415509549" Y="2.518241014034" />
                  <Point X="-21.367120876333" Y="2.603400911956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.787333414075" Y="2.8538227007" />
                  <Point X="-27.970828938529" Y="3.767812816043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.932426062241" Y="2.430286959728" />
                  <Point X="-21.49511406522" Y="2.529504036374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.837881851655" Y="2.766270225949" />
                  <Point X="-28.051060916972" Y="3.685494350429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.034651338748" Y="2.351846505958" />
                  <Point X="-21.623107170715" Y="2.455607146089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.888430289236" Y="2.678717751198" />
                  <Point X="-27.989055226982" Y="3.578095546197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.136876615254" Y="2.273406052187" />
                  <Point X="-21.751100276211" Y="2.381710255803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.938564740795" Y="2.591092279542" />
                  <Point X="-27.927048143454" Y="3.470696496247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.239101891761" Y="2.194965598416" />
                  <Point X="-21.879093381707" Y="2.307813365518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.969530206046" Y="2.500086798407" />
                  <Point X="-27.865041059925" Y="3.363297446297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.341327168267" Y="2.116525144646" />
                  <Point X="-22.007086487203" Y="2.233916475232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.986680480907" Y="2.406645326462" />
                  <Point X="-27.803033976396" Y="3.255898396346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.443552732281" Y="2.03808474157" />
                  <Point X="-22.135079592699" Y="2.160019584946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.979268222998" Y="2.308872817276" />
                  <Point X="-27.76006118475" Y="3.151855605613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.545778470227" Y="1.959644369164" />
                  <Point X="-22.263072698195" Y="2.086122694661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.945684085943" Y="2.20648549966" />
                  <Point X="-27.74959483512" Y="3.053544577655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.648004208174" Y="1.881203996757" />
                  <Point X="-22.39567380911" Y="2.013038320058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.864735045207" Y="2.095746471586" />
                  <Point X="-27.757593119359" Y="2.958489362837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.75022994612" Y="1.802763624351" />
                  <Point X="-27.804233659939" Y="2.870247820407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.720918624437" Y="3.031884112457" />
                  <Point X="-28.727824709912" Y="3.033101841658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.84924729253" Y="1.723757525952" />
                  <Point X="-27.884593724967" Y="2.787951939913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.480371227133" Y="2.893003588044" />
                  <Point X="-28.793820740676" Y="2.948273194372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.917490085998" Y="1.63932504355" />
                  <Point X="-28.06659031907" Y="2.723577321722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.234695643062" Y="2.753218825942" />
                  <Point X="-28.859816699171" Y="2.863444534343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.96667780642" Y="1.551532637651" />
                  <Point X="-28.925812295285" Y="2.778615810417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.992694781888" Y="1.459654604253" />
                  <Point X="-28.991807891399" Y="2.693787086491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.283232516387" Y="1.061764756213" />
                  <Point X="-20.44929358085" Y="1.091045802323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.996937811601" Y="1.363937236743" />
                  <Point X="-29.045231384144" Y="2.606741561536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.258694211493" Y="0.960972462871" />
                  <Point X="-20.762514643508" Y="1.049809598467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.976860906992" Y="1.263931608642" />
                  <Point X="-29.096283355984" Y="2.519277873461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.239032292508" Y="0.861040007932" />
                  <Point X="-21.075734809559" Y="1.008573236515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.926694931482" Y="1.158620465517" />
                  <Point X="-29.147335327824" Y="2.431814185385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.223590264039" Y="0.761851633547" />
                  <Point X="-21.388954975611" Y="0.967336874563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.829464491978" Y="1.045010587557" />
                  <Point X="-29.198387299664" Y="2.34435049731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.328334962291" Y="0.683855421806" />
                  <Point X="-29.15782969566" Y="2.240733569322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.545464274491" Y="0.62567564972" />
                  <Point X="-28.994605283689" Y="2.115487173452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.762593586691" Y="0.567495877634" />
                  <Point X="-28.831381355228" Y="1.990240862838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.979723392208" Y="0.509316192534" />
                  <Point X="-28.668157426767" Y="1.864994552224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.19685328591" Y="0.451136522983" />
                  <Point X="-28.514496417034" Y="1.741434442196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.382991214136" Y="0.387492133733" />
                  <Point X="-28.447764316525" Y="1.633202244268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.499867063868" Y="0.311634971305" />
                  <Point X="-28.421423033059" Y="1.532092037157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.571488222451" Y="0.227798185824" />
                  <Point X="-28.436241924901" Y="1.438239479484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.620912369352" Y="0.140047468292" />
                  <Point X="-28.475240108342" Y="1.348650383294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.641729678169" Y="0.047252593373" />
                  <Point X="-28.550491293065" Y="1.265453669362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.650424061671" Y="-0.047679880365" />
                  <Point X="-28.751693832832" Y="1.204465577581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.63462149727" Y="-0.146931826962" />
                  <Point X="-29.647741641007" Y="1.265997454037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.599601374327" Y="-0.249572347634" />
                  <Point X="-29.672689507526" Y="1.173930907887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.513634201774" Y="-0.361196207739" />
                  <Point X="-29.697637606477" Y="1.08186440272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.198793338704" Y="-0.513176674657" />
                  <Point X="-29.722585712245" Y="0.989797898755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.216051986784" Y="-0.782926018188" />
                  <Point X="-29.740366501547" Y="0.896467603518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.230217978641" Y="-0.876893699744" />
                  <Point X="-29.754278110423" Y="0.802455067379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.247348879385" Y="-0.970338587868" />
                  <Point X="-29.768189719298" Y="0.70844253124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.268511389261" Y="-1.063072594527" />
                  <Point X="-29.782101328174" Y="0.6144299951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.655826753966" Y="-0.914916993107" />
                  <Point X="-28.985739756175" Y="0.377544435428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.8786974997" Y="-0.972084395552" />
                  <Point X="-28.381207066942" Y="0.174483483467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.975787990133" Y="-1.051430250648" />
                  <Point X="-28.312932928177" Y="0.065979382589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.045740451398" Y="-1.135561272489" />
                  <Point X="-28.294021788608" Y="-0.033820689682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.103123282899" Y="-1.221908659195" />
                  <Point X="-28.312291009016" Y="-0.127064861337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.126459658944" Y="-1.314259354596" />
                  <Point X="-28.360085276737" Y="-0.215102970543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.135194705958" Y="-1.409184658258" />
                  <Point X="-28.462877539683" Y="-0.293443449307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.139104380668" Y="-1.50496080525" />
                  <Point X="-28.665240865512" Y="-0.354226863187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.110296394005" Y="-1.606505958688" />
                  <Point X="-28.882370426134" Y="-0.412406591469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.041965606142" Y="-1.715020048331" />
                  <Point X="-29.099499984991" Y="-0.470586320062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.958413264766" Y="-1.826218108546" />
                  <Point X="-29.316629484852" Y="-0.528766059058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.797653528596" Y="-1.951029915573" />
                  <Point X="-29.533758984712" Y="-0.586945798054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.634429546879" Y="-2.076276235578" />
                  <Point X="-29.750888484573" Y="-0.64512553705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.471205565161" Y="-2.201522555582" />
                  <Point X="-22.211881737523" Y="-2.070921362427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.468295107914" Y="-2.025708767013" />
                  <Point X="-29.774616273097" Y="-0.73740721587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.307981583444" Y="-2.326768875587" />
                  <Point X="-21.943688553133" Y="-2.214676585006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.667648824165" Y="-2.087022856262" />
                  <Point X="-28.093283338205" Y="-1.130337103974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.255715431799" Y="-1.10169594334" />
                  <Point X="-29.760462096936" Y="-0.836368507146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.144757617813" Y="-2.452015192755" />
                  <Point X="-21.703140109701" Y="-2.35355729388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.800407976773" Y="-2.160079363851" />
                  <Point X="-27.971788211726" Y="-1.248225500926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.568936191629" Y="-1.142932200594" />
                  <Point X="-29.746307233696" Y="-0.935329919572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.981533672783" Y="-2.577261506291" />
                  <Point X="-21.462591625079" Y="-2.492438010017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.877503229135" Y="-2.242950918904" />
                  <Point X="-27.948879797305" Y="-1.348730400603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.882156951459" Y="-1.184168457847" />
                  <Point X="-29.725069906641" Y="-1.035540161459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.943957599629" Y="-2.680352709946" />
                  <Point X="-21.22204311957" Y="-2.631318729836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.924259292595" Y="-2.331172091534" />
                  <Point X="-27.961705633929" Y="-1.442934387685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.195377711288" Y="-1.2254047151" />
                  <Point X="-29.699267930106" Y="-1.136555274207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.970717847055" Y="-2.419445723027" />
                  <Point X="-28.007900406223" Y="-1.531254531091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.508598177787" Y="-1.266641024075" />
                  <Point X="-29.673465953571" Y="-1.237570386955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.997653461656" Y="-2.51116177556" />
                  <Point X="-28.097659780595" Y="-1.611893059747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.992690785878" Y="-2.608502357325" />
                  <Point X="-28.199885077732" Y="-1.69033350988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.97469533301" Y="-2.708140969325" />
                  <Point X="-28.302110374869" Y="-1.768773960013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.955868406372" Y="-2.807926192584" />
                  <Point X="-23.356314394333" Y="-2.73731676059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.775367425436" Y="-2.663426404859" />
                  <Point X="-28.404335672006" Y="-1.847214410146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.90776195988" Y="-2.912874185176" />
                  <Point X="-23.175708642382" Y="-2.865627955659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.96169906921" Y="-2.727036636831" />
                  <Point X="-28.506560969142" Y="-1.925654860279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.84575526621" Y="-3.020273166383" />
                  <Point X="-23.08855234907" Y="-2.977461489838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.057418586712" Y="-2.806624231444" />
                  <Point X="-28.608786417548" Y="-2.004095283739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.783748572541" Y="-3.127672147591" />
                  <Point X="-23.002949892768" Y="-3.089021040628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.150447886722" Y="-2.886686183985" />
                  <Point X="-28.711012042231" Y="-2.082535676117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.721741878871" Y="-3.235071128799" />
                  <Point X="-22.917347436465" Y="-3.200580591418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.205744498398" Y="-2.973401427534" />
                  <Point X="-27.406444088507" Y="-2.409031732655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.654203027966" Y="-2.365345146917" />
                  <Point X="-28.813237666913" Y="-2.160976068495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.659735185201" Y="-3.342470110006" />
                  <Point X="-22.831744980162" Y="-3.312140142208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.230229832604" Y="-3.065549530611" />
                  <Point X="-27.327006127449" Y="-2.519504316611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.789058364499" Y="-2.438032040723" />
                  <Point X="-28.915463291596" Y="-2.239416460873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.597728491532" Y="-3.449869091214" />
                  <Point X="-22.746142477213" Y="-3.423699701224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.250423157756" Y="-3.158454430686" />
                  <Point X="-24.671966376537" Y="-3.08412498768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.082489597623" Y="-3.011738667595" />
                  <Point X="-27.311620749679" Y="-2.61868270195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.91705150673" Y="-2.511928924531" />
                  <Point X="-29.017688916279" Y="-2.31785685325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.535721797862" Y="-3.557268072421" />
                  <Point X="-22.660539771435" Y="-3.535259296003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.270616982751" Y="-3.251359242624" />
                  <Point X="-24.522788016044" Y="-3.206894685702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.280697032473" Y="-3.073254877183" />
                  <Point X="-27.33527407992" Y="-2.710977509774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.045044648961" Y="-2.585825808339" />
                  <Point X="-29.119914540962" Y="-2.396297245628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.473715104193" Y="-3.664667053629" />
                  <Point X="-22.574937065657" Y="-3.646818890783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.272877876974" Y="-3.347426114101" />
                  <Point X="-24.446499914718" Y="-3.316811864402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.415607808487" Y="-3.145931995513" />
                  <Point X="-27.384577809663" Y="-2.7987494601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.173037791192" Y="-2.659722692147" />
                  <Point X="-29.15556031129" Y="-2.4864774627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.411707949066" Y="-3.772066116204" />
                  <Point X="-22.489334359879" Y="-3.758378485562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.2598761232" Y="-3.446184202217" />
                  <Point X="-24.371858580375" Y="-3.426438673651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.481481167683" Y="-3.230782273106" />
                  <Point X="-27.43512627745" Y="-2.886301929524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.301030933423" Y="-2.733619575955" />
                  <Point X="-29.091271726001" Y="-2.594278802968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.349700672408" Y="-3.879465200208" />
                  <Point X="-22.403731654101" Y="-3.869938080342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.246874369426" Y="-3.544942290334" />
                  <Point X="-24.333762312744" Y="-3.529621601628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.541132869707" Y="-3.316729596723" />
                  <Point X="-27.485674745237" Y="-2.973854398949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.429024075654" Y="-2.807516459764" />
                  <Point X="-29.026981942216" Y="-2.702080354562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.28769339575" Y="-3.986864284212" />
                  <Point X="-22.318128948323" Y="-3.981497675121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.233872615651" Y="-3.64370037845" />
                  <Point X="-24.306632895683" Y="-3.630870777956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.600784572868" Y="-3.402676920139" />
                  <Point X="-27.536223213024" Y="-3.061406868374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.557017249114" Y="-2.881413338065" />
                  <Point X="-28.949504428132" Y="-2.812207258822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.220870861877" Y="-3.742458466566" />
                  <Point X="-24.279503478622" Y="-3.732119954284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.643727609401" Y="-3.491570432294" />
                  <Point X="-27.586771680811" Y="-3.148959337799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.685010436146" Y="-2.955310213974" />
                  <Point X="-28.864698547093" Y="-2.923626351901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.207869108103" Y="-3.841216554682" />
                  <Point X="-24.252374061561" Y="-3.833369130612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.668409345141" Y="-3.583683904482" />
                  <Point X="-27.637320145113" Y="-3.236511807838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.194867354328" Y="-3.939974642798" />
                  <Point X="-24.2252446445" Y="-3.93461830694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.693091080881" Y="-3.675797376669" />
                  <Point X="-27.687868538678" Y="-3.32406429035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.181865600554" Y="-4.038732730914" />
                  <Point X="-24.198115227439" Y="-4.035867483268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.717772816622" Y="-3.767910848856" />
                  <Point X="-27.738416932243" Y="-3.411616772863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.168863846779" Y="-4.13749081903" />
                  <Point X="-24.170986536079" Y="-4.137116531635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.742454552362" Y="-3.860024321044" />
                  <Point X="-27.788965325808" Y="-3.499169255375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.767136288102" Y="-3.952137793231" />
                  <Point X="-26.582090963255" Y="-3.808439295947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.660467762315" Y="-3.794619351611" />
                  <Point X="-27.839513719372" Y="-3.586721737887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.791818023843" Y="-4.044251265419" />
                  <Point X="-26.401244465797" Y="-3.936792940945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.093491578004" Y="-3.814731097745" />
                  <Point X="-27.890062112937" Y="-3.674274220399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.816499759583" Y="-4.136364737606" />
                  <Point X="-26.263565892236" Y="-4.057534916258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.207976909828" Y="-3.891009772979" />
                  <Point X="-27.940610506502" Y="-3.761826702912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.841181495323" Y="-4.228478209794" />
                  <Point X="-26.192627837314" Y="-4.166508737429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.32220345649" Y="-3.967334079018" />
                  <Point X="-27.947766184222" Y="-3.857030491993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.865863231064" Y="-4.320591681981" />
                  <Point X="-26.169201517947" Y="-4.267104957721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.414935766639" Y="-4.047448398884" />
                  <Point X="-27.785267731328" Y="-3.982148881691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.890545048479" Y="-4.412705139767" />
                  <Point X="-26.149315537515" Y="-4.367076920739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.480134697669" Y="-4.13241759636" />
                  <Point X="-27.580266173112" Y="-4.114761715621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.915227178012" Y="-4.504818542518" />
                  <Point X="-26.129428802701" Y="-4.467049016774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.939909307544" Y="-4.596931945269" />
                  <Point X="-26.120008467918" Y="-4.565175604092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.964591437077" Y="-4.68904534802" />
                  <Point X="-26.131796765129" Y="-4.659562537367" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.37055078125" Y="-4.126438476562" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544433594" />
                  <Point X="-24.60022265625" Y="-3.428552246094" />
                  <Point X="-24.699408203125" Y="-3.285644042969" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.825537109375" Y="-3.235402587891" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.108537109375" Y="-3.218763671875" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285644042969" />
                  <Point X="-25.3528203125" Y="-3.378635986328" />
                  <Point X="-25.452005859375" Y="-3.521544433594" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.7018984375" Y="-4.442775878906" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.920138671875" Y="-4.973024414062" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.215349609375" Y="-4.908449707031" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.328158203125" Y="-4.695423339844" />
                  <Point X="-26.3091484375" Y="-4.5510390625" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.33354296875" Y="-4.414805175781" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.478234375" Y="-4.121989257813" />
                  <Point X="-26.61954296875" Y="-3.998064208984" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.77128125" Y="-3.977763916016" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791015625" />
                  <Point X="-27.091568359375" Y="-4.041738525391" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.395974609375" Y="-4.33484765625" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.591833984375" Y="-4.331072753906" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.015220703125" Y="-4.044887939453" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.799490234375" Y="-3.137401855469" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597591552734" />
                  <Point X="-27.51398046875" Y="-2.56876171875" />
                  <Point X="-27.531322265625" Y="-2.551418945312" />
                  <Point X="-27.56015234375" Y="-2.53719921875" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.372798828125" Y="-2.994447998047" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-28.953130859375" Y="-3.121151855469" />
                  <Point X="-29.161697265625" Y="-2.847135742188" />
                  <Point X="-29.275970703125" Y="-2.65551953125" />
                  <Point X="-29.43101953125" Y="-2.395526855469" />
                  <Point X="-28.677240234375" Y="-1.817132324219" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.396014770508" />
                  <Point X="-28.1381171875" Y="-1.366267578125" />
                  <Point X="-28.14032421875" Y="-1.334596435547" />
                  <Point X="-28.161158203125" Y="-1.310639404297" />
                  <Point X="-28.187640625" Y="-1.295052856445" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.20561328125" Y="-1.418391723633" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.84581640625" Y="-1.330552368164" />
                  <Point X="-29.927392578125" Y="-1.011187438965" />
                  <Point X="-29.957625" Y="-0.799809875488" />
                  <Point X="-29.99839453125" Y="-0.514742248535" />
                  <Point X="-29.142365234375" Y="-0.285369415283" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.530953125" Y="-0.113831710815" />
                  <Point X="-28.514140625" Y="-0.102162872314" />
                  <Point X="-28.502322265625" Y="-0.090644470215" />
                  <Point X="-28.49125" Y="-0.064156013489" />
                  <Point X="-28.483400390625" Y="-0.031280040741" />
                  <Point X="-28.48929296875" Y="-0.004711108208" />
                  <Point X="-28.502322265625" Y="0.028084388733" />
                  <Point X="-28.52508203125" Y="0.047195827484" />
                  <Point X="-28.54189453125" Y="0.058864818573" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.456326171875" Y="0.306935272217" />
                  <Point X="-29.998185546875" Y="0.452126068115" />
                  <Point X="-29.97021875" Y="0.641131347656" />
                  <Point X="-29.91764453125" Y="0.996414733887" />
                  <Point X="-29.856787109375" Y="1.221000976562" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.172537109375" Y="1.449178466797" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.707490234375" Y="1.403501220703" />
                  <Point X="-28.67027734375" Y="1.415234008789" />
                  <Point X="-28.651533203125" Y="1.426055908203" />
                  <Point X="-28.639119140625" Y="1.443785522461" />
                  <Point X="-28.629404296875" Y="1.46724230957" />
                  <Point X="-28.61447265625" Y="1.503289916992" />
                  <Point X="-28.610712890625" Y="1.524605834961" />
                  <Point X="-28.61631640625" Y="1.545512817383" />
                  <Point X="-28.6280390625" Y="1.568033569336" />
                  <Point X="-28.646056640625" Y="1.602642700195" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-29.17555859375" Y="2.014848388672" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.364294921875" Y="2.437022705078" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-28.9988046875" Y="2.994217285156" />
                  <Point X="-28.774669921875" Y="3.282310791016" />
                  <Point X="-28.416662109375" Y="3.075614013672" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.1047890625" Y="2.917484375" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775146484" />
                  <Point X="-28.013251953125" Y="2.927404296875" />
                  <Point X="-27.989314453125" Y="2.951341552734" />
                  <Point X="-27.95252734375" Y="2.988127685547" />
                  <Point X="-27.9408984375" Y="3.006381591797" />
                  <Point X="-27.938072265625" Y="3.027840332031" />
                  <Point X="-27.9410234375" Y="3.061563964844" />
                  <Point X="-27.945556640625" Y="3.113389892578" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.180541015625" Y="3.529761230469" />
                  <Point X="-28.30727734375" Y="3.749276611328" />
                  <Point X="-28.108669921875" Y="3.901548095703" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.4989765625" Y="4.315391601562" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.04665625" Y="4.390306640625" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273661132813" />
                  <Point X="-26.9137109375" Y="4.254122070313" />
                  <Point X="-26.856029296875" Y="4.224094238281" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.774712890625" Y="4.238444335938" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.69690234375" Y="4.275744628906" />
                  <Point X="-26.68608203125" Y="4.294488769531" />
                  <Point X="-26.673357421875" Y="4.334845703125" />
                  <Point X="-26.653802734375" Y="4.396865234375" />
                  <Point X="-26.651916015625" Y="4.41842578125" />
                  <Point X="-26.677892578125" Y="4.615725097656" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.428689453125" Y="4.774161132812" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.66029296875" Y="4.939319824219" />
                  <Point X="-25.22419921875" Y="4.990358398438" />
                  <Point X="-25.11831640625" Y="4.595200683594" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.828478515625" Y="4.747805175781" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.5419609375" Y="4.96768359375" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.885140625" Y="4.864084960938" />
                  <Point X="-23.491544921875" Y="4.76905859375" />
                  <Point X="-23.32571484375" Y="4.708910644531" />
                  <Point X="-23.06896484375" Y="4.615785644531" />
                  <Point X="-22.908689453125" Y="4.540830078125" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.506462890625" Y="4.33492578125" />
                  <Point X="-22.26747265625" Y="4.195688964844" />
                  <Point X="-22.1214453125" Y="4.09184375" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.430490234375" Y="3.091896484375" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491514404297" />
                  <Point X="-22.783611328125" Y="2.459865478516" />
                  <Point X="-22.7966171875" Y="2.411228027344" />
                  <Point X="-22.797955078125" Y="2.392327148438" />
                  <Point X="-22.794654296875" Y="2.364959960938" />
                  <Point X="-22.789583984375" Y="2.32290234375" />
                  <Point X="-22.781318359375" Y="2.300812744141" />
                  <Point X="-22.7643828125" Y="2.275856445312" />
                  <Point X="-22.738359375" Y="2.237504150391" />
                  <Point X="-22.72505859375" Y="2.224203369141" />
                  <Point X="-22.700103515625" Y="2.207269287109" />
                  <Point X="-22.66175" Y="2.181245849609" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.612294921875" Y="2.169680175781" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.551333984375" Y="2.165946289062" />
                  <Point X="-22.51968359375" Y="2.174409667969" />
                  <Point X="-22.471046875" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.554880859375" Y="2.714390869141" />
                  <Point X="-21.005751953125" Y="3.031430664062" />
                  <Point X="-20.939166015625" Y="2.938893310547" />
                  <Point X="-20.797404296875" Y="2.741876220703" />
                  <Point X="-20.715998046875" Y="2.607350830078" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.2641953125" Y="1.936221313477" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583832885742" />
                  <Point X="-21.74340625" Y="1.554117431641" />
                  <Point X="-21.77841015625" Y="1.508451538086" />
                  <Point X="-21.78687890625" Y="1.491501831055" />
                  <Point X="-21.79536328125" Y="1.461162231445" />
                  <Point X="-21.808404296875" Y="1.414537475586" />
                  <Point X="-21.809220703125" Y="1.390965942383" />
                  <Point X="-21.80225390625" Y="1.357209350586" />
                  <Point X="-21.79155078125" Y="1.305333129883" />
                  <Point X="-21.7843515625" Y="1.287954956055" />
                  <Point X="-21.765408203125" Y="1.259160400391" />
                  <Point X="-21.736294921875" Y="1.214909423828" />
                  <Point X="-21.719052734375" Y="1.198819824219" />
                  <Point X="-21.691599609375" Y="1.183366210938" />
                  <Point X="-21.64941015625" Y="1.159617553711" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.594314453125" Y="1.148713989258" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.665376953125" Y="1.254237670898" />
                  <Point X="-20.1510234375" Y="1.321953125" />
                  <Point X="-20.121697265625" Y="1.201480834961" />
                  <Point X="-20.060806640625" Y="0.951366455078" />
                  <Point X="-20.03515625" Y="0.78661138916" />
                  <Point X="-20.002140625" Y="0.574556396484" />
                  <Point X="-20.746345703125" Y="0.375146942139" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819503784" />
                  <Point X="-21.30737890625" Y="0.211740600586" />
                  <Point X="-21.363421875" Y="0.179346984863" />
                  <Point X="-21.377734375" Y="0.166926391602" />
                  <Point X="-21.399615234375" Y="0.139045455933" />
                  <Point X="-21.433240234375" Y="0.09619871521" />
                  <Point X="-21.443013671875" Y="0.074735565186" />
                  <Point X="-21.450306640625" Y="0.036651584625" />
                  <Point X="-21.461515625" Y="-0.021874994278" />
                  <Point X="-21.461515625" Y="-0.040685081482" />
                  <Point X="-21.45422265625" Y="-0.078769065857" />
                  <Point X="-21.443013671875" Y="-0.137295639038" />
                  <Point X="-21.433240234375" Y="-0.158758789063" />
                  <Point X="-21.411359375" Y="-0.186639724731" />
                  <Point X="-21.377734375" Y="-0.229486465454" />
                  <Point X="-21.363421875" Y="-0.241907058716" />
                  <Point X="-21.326955078125" Y="-0.262985961914" />
                  <Point X="-21.270912109375" Y="-0.295379577637" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.471255859375" Y="-0.511417541504" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.017572265625" Y="-0.740915771484" />
                  <Point X="-20.051568359375" Y="-0.966412963867" />
                  <Point X="-20.08443359375" Y="-1.11042956543" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-20.993677734375" Y="-1.175874389648" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.676734375" Y="-1.113897827148" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.8145546875" Y="-1.154697387695" />
                  <Point X="-21.85781640625" Y="-1.206727172852" />
                  <Point X="-21.924298828125" Y="-1.286685424805" />
                  <Point X="-21.935640625" Y="-1.314071166992" />
                  <Point X="-21.941841796875" Y="-1.381452026367" />
                  <Point X="-21.951369140625" Y="-1.485001464844" />
                  <Point X="-21.943638671875" Y="-1.516621826172" />
                  <Point X="-21.904029296875" Y="-1.578231811523" />
                  <Point X="-21.843158203125" Y="-1.672912475586" />
                  <Point X="-21.831537109375" Y="-1.685540771484" />
                  <Point X="-21.100662109375" Y="-2.246361816406" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.700037109375" Y="-2.647073974609" />
                  <Point X="-20.7958671875" Y="-2.802140380859" />
                  <Point X="-20.86383984375" Y="-2.898720703125" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.717205078125" Y="-2.564830078125" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.34783984375" Y="-2.237928955078" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.5816875" Y="-2.256488525391" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.334683349609" />
                  <Point X="-22.748642578125" Y="-2.405449462891" />
                  <Point X="-22.80587890625" Y="-2.514201416016" />
                  <Point X="-22.8108359375" Y="-2.546374755859" />
                  <Point X="-22.795451171875" Y="-2.631557617188" />
                  <Point X="-22.77180859375" Y="-2.762465087891" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.29624609375" Y="-3.592055664062" />
                  <Point X="-22.01332421875" Y="-4.082089111328" />
                  <Point X="-22.05098828125" Y="-4.108991699219" />
                  <Point X="-22.16469921875" Y="-4.1902109375" />
                  <Point X="-22.2406953125" Y="-4.239403320313" />
                  <Point X="-22.32022265625" Y="-4.290879394531" />
                  <Point X="-22.91288671875" Y="-3.518505126953" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.413462890625" Y="-2.926454345703" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.666078125" Y="-2.844172119141" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.905615234375" Y="-2.927501220703" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045985839844" />
                  <Point X="-24.052755859375" Y="-3.143585205078" />
                  <Point X="-24.085357421875" Y="-3.293573730469" />
                  <Point X="-24.0860703125" Y="-3.310719970703" />
                  <Point X="-23.952970703125" Y="-4.321703125" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.89807421875" Y="-4.939666015625" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.075861328125" Y="-4.976001464844" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#156" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.076177159468" Y="4.639421517591" Z="1" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1" />
                  <Point X="-0.672261821919" Y="5.020261058313" Z="1" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1" />
                  <Point X="-1.448344850338" Y="4.853584779719" Z="1" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1" />
                  <Point X="-1.733620792968" Y="4.640479712658" Z="1" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1" />
                  <Point X="-1.72720063896" Y="4.381160982191" Z="1" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1" />
                  <Point X="-1.800004618146" Y="4.315918158695" Z="1" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1" />
                  <Point X="-1.896780920762" Y="4.329752074116" Z="1" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1" />
                  <Point X="-2.013145325707" Y="4.452024786082" Z="1" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1" />
                  <Point X="-2.529416961224" Y="4.390379290713" Z="1" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1" />
                  <Point X="-3.145247878103" Y="3.972508051775" Z="1" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1" />
                  <Point X="-3.229998571226" Y="3.536041263206" Z="1" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1" />
                  <Point X="-2.996990476967" Y="3.088487178959" Z="1" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1" />
                  <Point X="-3.030826286637" Y="3.017977281249" Z="1" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1" />
                  <Point X="-3.106589286648" Y="2.998574222206" Z="1" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1" />
                  <Point X="-3.397818085091" Y="3.150195377519" Z="1" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1" />
                  <Point X="-4.044425469262" Y="3.056199602745" Z="1" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1" />
                  <Point X="-4.413634280671" Y="2.493507941984" Z="1" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1" />
                  <Point X="-4.212153064841" Y="2.006460793445" Z="1" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1" />
                  <Point X="-3.678545908584" Y="1.576224970101" Z="1" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1" />
                  <Point X="-3.681753853843" Y="1.517656741093" Z="1" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1" />
                  <Point X="-3.728681828406" Y="1.482467248474" Z="1" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1" />
                  <Point X="-4.172167965227" Y="1.53003074358" Z="1" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1" />
                  <Point X="-4.911203439858" Y="1.265358285965" Z="1" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1" />
                  <Point X="-5.025835949818" Y="0.679730609772" Z="1" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1" />
                  <Point X="-4.475425193671" Y="0.289919041395" Z="1" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1" />
                  <Point X="-3.559748065268" Y="0.037400078677" Z="1" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1" />
                  <Point X="-3.543203539653" Y="0.011749923755" Z="1" />
                  <Point X="-3.539556741714" Y="0" Z="1" />
                  <Point X="-3.545161061482" Y="-0.018057027306" Z="1" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1" />
                  <Point X="-3.565620529855" Y="-0.041475904457" Z="1" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1" />
                  <Point X="-4.161462595102" Y="-0.205793015361" Z="1" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1" />
                  <Point X="-5.013277458387" Y="-0.775608828104" Z="1" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1" />
                  <Point X="-4.900419393092" Y="-1.311646472614" Z="1" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1" />
                  <Point X="-4.205245176039" Y="-1.436684005725" Z="1" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1" />
                  <Point X="-3.203115760641" Y="-1.316305594501" Z="1" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1" />
                  <Point X="-3.197343683568" Y="-1.340470734625" Z="1" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1" />
                  <Point X="-3.713835139944" Y="-1.746184524107" Z="1" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1" />
                  <Point X="-4.325070834337" Y="-2.649849094522" Z="1" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1" />
                  <Point X="-3.999072283982" Y="-3.120155125946" Z="1" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1" />
                  <Point X="-3.353956623328" Y="-3.006469146625" Z="1" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1" />
                  <Point X="-2.562329774076" Y="-2.566000695056" Z="1" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1" />
                  <Point X="-2.848947866543" Y="-3.081121759164" Z="1" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1" />
                  <Point X="-3.051881235696" Y="-4.053225123642" Z="1" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1" />
                  <Point X="-2.624312930565" Y="-4.34230341616" Z="1" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1" />
                  <Point X="-2.36246377842" Y="-4.334005498785" Z="1" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1" />
                  <Point X="-2.069946508922" Y="-4.05203189567" Z="1" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1" />
                  <Point X="-1.780707136709" Y="-3.996376982851" Z="1" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1" />
                  <Point X="-1.517357599152" Y="-4.128300989659" Z="1" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1" />
                  <Point X="-1.388738501668" Y="-4.393280191684" Z="1" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1" />
                  <Point X="-1.383887100637" Y="-4.657616557391" Z="1" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1" />
                  <Point X="-1.23396591394" Y="-4.925592648995" Z="1" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1" />
                  <Point X="-0.935823365506" Y="-4.990828593734" Z="1" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1" />
                  <Point X="-0.659758613981" Y="-4.424436757366" Z="1" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1" />
                  <Point X="-0.317900668868" Y="-3.375864802488" Z="1" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1" />
                  <Point X="-0.099874350078" Y="-3.235236680995" Z="1" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1" />
                  <Point X="0.153484729283" Y="-3.251875364293" Z="1" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1" />
                  <Point X="0.352545190564" Y="-3.42578095328" Z="1" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1" />
                  <Point X="0.57499624642" Y="-4.108099272449" Z="1" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1" />
                  <Point X="0.926919168241" Y="-4.993916015485" Z="1" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1" />
                  <Point X="1.106474154533" Y="-4.957226131579" Z="1" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1" />
                  <Point X="1.090444225716" Y="-4.283896611999" Z="1" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1" />
                  <Point X="0.989946402976" Y="-3.122925273758" Z="1" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1" />
                  <Point X="1.12019401999" Y="-2.934667814719" Z="1" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1" />
                  <Point X="1.332347492248" Y="-2.862681743138" Z="1" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1" />
                  <Point X="1.55334046768" Y="-2.937232397195" Z="1" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1" />
                  <Point X="2.041288601438" Y="-3.517663118036" Z="1" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1" />
                  <Point X="2.78031497674" Y="-4.250097824209" Z="1" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1" />
                  <Point X="2.971914429933" Y="-4.118398713532" Z="1" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1" />
                  <Point X="2.740898304926" Y="-3.535775703409" Z="1" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1" />
                  <Point X="2.247595107807" Y="-2.591391204553" Z="1" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1" />
                  <Point X="2.289446467121" Y="-2.397456328852" Z="1" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1" />
                  <Point X="2.43544204952" Y="-2.269454842846" Z="1" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1" />
                  <Point X="2.63711559896" Y="-2.255852901684" Z="1" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1" />
                  <Point X="3.251637893698" Y="-2.576851195348" Z="1" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1" />
                  <Point X="4.170891924813" Y="-2.896218127943" Z="1" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1" />
                  <Point X="4.336338922149" Y="-2.642079384306" Z="1" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1" />
                  <Point X="3.923618801309" Y="-2.175413965951" Z="1" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1" />
                  <Point X="3.13187192758" Y="-1.519912349234" Z="1" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1" />
                  <Point X="3.10179109222" Y="-1.354753078092" Z="1" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1" />
                  <Point X="3.174474328718" Y="-1.207413976378" Z="1" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1" />
                  <Point X="3.327727056661" Y="-1.131477069357" Z="1" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1" />
                  <Point X="3.993639108407" Y="-1.194166665022" Z="1" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1" />
                  <Point X="4.958155366102" Y="-1.090273528948" Z="1" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1" />
                  <Point X="5.025712526261" Y="-0.717089671205" Z="1" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1" />
                  <Point X="4.535529481118" Y="-0.431841189756" Z="1" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1" />
                  <Point X="3.691910224156" Y="-0.188417081499" Z="1" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1" />
                  <Point X="3.621817229388" Y="-0.12449143726" Z="1" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1" />
                  <Point X="3.588728228608" Y="-0.038083948069" Z="1" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1" />
                  <Point X="3.592643221816" Y="0.05852658315" Z="1" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1" />
                  <Point X="3.633562209012" Y="0.139457301315" Z="1" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1" />
                  <Point X="3.711485190197" Y="0.199731762743" Z="1" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1" />
                  <Point X="4.260438336189" Y="0.358130743012" Z="1" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1" />
                  <Point X="5.0080915971" Y="0.825583551211" Z="1" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1" />
                  <Point X="4.920728814306" Y="1.244587885442" Z="1" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1" />
                  <Point X="4.321941171661" Y="1.33508988399" Z="1" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1" />
                  <Point X="3.406078879144" Y="1.229562949036" Z="1" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1" />
                  <Point X="3.326771510059" Y="1.258217393436" Z="1" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1" />
                  <Point X="3.270205322625" Y="1.317921859991" Z="1" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1" />
                  <Point X="3.240557152412" Y="1.398592686907" Z="1" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1" />
                  <Point X="3.246631226726" Y="1.478974236586" Z="1" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1" />
                  <Point X="3.290120380529" Y="1.554979693812" Z="1" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1" />
                  <Point X="3.760084947073" Y="1.92783358011" Z="1" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1" />
                  <Point X="4.32062236048" Y="2.664517094969" Z="1" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1" />
                  <Point X="4.095262122014" Y="2.999376348022" Z="1" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1" />
                  <Point X="3.413962504015" Y="2.788972215943" Z="1" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1" />
                  <Point X="2.461240564858" Y="2.253992505416" Z="1" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1" />
                  <Point X="2.387534125997" Y="2.250600533901" Z="1" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1" />
                  <Point X="2.321814418281" Y="2.279924174228" Z="1" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1" />
                  <Point X="2.270834426007" Y="2.335210442101" Z="1" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1" />
                  <Point X="2.248829168857" Y="2.402224317836" Z="1" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1" />
                  <Point X="2.258535437241" Y="2.478229011024" Z="1" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1" />
                  <Point X="2.606653136917" Y="3.09817657794" Z="1" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1" />
                  <Point X="2.901373888255" Y="4.163870724856" Z="1" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1" />
                  <Point X="2.512550219494" Y="4.409408642523" Z="1" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1" />
                  <Point X="2.106336115216" Y="4.617401883626" Z="1" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1" />
                  <Point X="1.685177079362" Y="4.787194762113" Z="1" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1" />
                  <Point X="1.120436511103" Y="4.943968307894" Z="1" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1" />
                  <Point X="0.457088711277" Y="5.04869150323" Z="1" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1" />
                  <Point X="0.117067402085" Y="4.79202598836" Z="1" />
                  <Point X="0" Y="4.355124473572" Z="1" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>