<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#123" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="471" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000473632812" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.41283984375" Y="-3.601559082031" />
                  <Point X="-24.4366953125" Y="-3.512524902344" />
                  <Point X="-24.446033203125" Y="-3.489878417969" />
                  <Point X="-24.463005859375" Y="-3.460260742188" />
                  <Point X="-24.46738671875" Y="-3.453328125" />
                  <Point X="-24.62136328125" Y="-3.231476806641" />
                  <Point X="-24.642890625" Y="-3.207521972656" />
                  <Point X="-24.67519140625" Y="-3.185802246094" />
                  <Point X="-24.705017578125" Y="-3.173693847656" />
                  <Point X="-24.712591796875" Y="-3.170986083984" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9796015625" Y="-3.091593017578" />
                  <Point X="-25.015287109375" Y="-3.092971435547" />
                  <Point X="-25.0451953125" Y="-3.099901367188" />
                  <Point X="-25.051912109375" Y="-3.10171875" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.319505859375" Y="-3.188985107422" />
                  <Point X="-25.35014453125" Y="-3.213492431641" />
                  <Point X="-25.371412109375" Y="-3.239359863281" />
                  <Point X="-25.37607421875" Y="-3.245525878906" />
                  <Point X="-25.53005078125" Y="-3.467377197266" />
                  <Point X="-25.5381875" Y="-3.481571044922" />
                  <Point X="-25.550990234375" Y="-3.512523925781" />
                  <Point X="-25.916583984375" Y="-4.87694140625" />
                  <Point X="-26.079328125" Y="-4.845352539063" />
                  <Point X="-26.092724609375" Y="-4.841905761719" />
                  <Point X="-26.246416015625" Y="-4.802362304688" />
                  <Point X="-26.217833984375" Y="-4.585254882812" />
                  <Point X="-26.2149609375" Y="-4.56344140625" />
                  <Point X="-26.214828125" Y="-4.539695800781" />
                  <Point X="-26.218966796875" Y="-4.505289550781" />
                  <Point X="-26.22011328125" Y="-4.498101074219" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.2870390625" Y="-4.181754882812" />
                  <Point X="-26.3080234375" Y="-4.148804199219" />
                  <Point X="-26.332248046875" Y="-4.124029296875" />
                  <Point X="-26.33753515625" Y="-4.119020996094" />
                  <Point X="-26.556904296875" Y="-3.926639160156" />
                  <Point X="-26.583205078125" Y="-3.908789550781" />
                  <Point X="-26.619921875" Y="-3.895419433594" />
                  <Point X="-26.654224609375" Y="-3.890511962891" />
                  <Point X="-26.66146484375" Y="-3.8897578125" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.984353515625" Y="-3.872525634766" />
                  <Point X="-27.021615234375" Y="-3.884268554688" />
                  <Point X="-27.05181640625" Y="-3.901256835938" />
                  <Point X="-27.058021484375" Y="-3.905067138672" />
                  <Point X="-27.300623046875" Y="-4.067169921875" />
                  <Point X="-27.3127890625" Y="-4.076825439453" />
                  <Point X="-27.335099609375" Y="-4.0994609375" />
                  <Point X="-27.4801484375" Y="-4.288490234375" />
                  <Point X="-27.80171484375" Y="-4.089383056641" />
                  <Point X="-27.8202421875" Y="-4.075117431641" />
                  <Point X="-28.10472265625" Y="-3.856077636719" />
                  <Point X="-27.46809765625" Y="-2.753410400391" />
                  <Point X="-27.42376171875" Y="-2.676620117188" />
                  <Point X="-27.412859375" Y="-2.647655517578" />
                  <Point X="-27.406587890625" Y="-2.616129638672" />
                  <Point X="-27.4056640625" Y="-2.584535644531" />
                  <Point X="-27.41515625" Y="-2.554387451172" />
                  <Point X="-27.43040625" Y="-2.524525390625" />
                  <Point X="-27.447833984375" Y="-2.500559082031" />
                  <Point X="-27.4641484375" Y="-2.484243652344" />
                  <Point X="-27.4893046875" Y="-2.466215576172" />
                  <Point X="-27.5181328125" Y="-2.451997558594" />
                  <Point X="-27.547751953125" Y="-2.44301171875" />
                  <Point X="-27.5786875" Y="-2.444023925781" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.8180234375" Y="-3.141801757812" />
                  <Point X="-29.08285546875" Y="-2.7938671875" />
                  <Point X="-29.096142578125" Y="-2.7715859375" />
                  <Point X="-29.306142578125" Y="-2.419449707031" />
                  <Point X="-28.183525390625" Y="-1.558036010742" />
                  <Point X="-28.105955078125" Y="-1.498513183594" />
                  <Point X="-28.0836796875" Y="-1.474210327148" />
                  <Point X="-28.065255859375" Y="-1.445308227539" />
                  <Point X="-28.0533984375" Y="-1.418062744141" />
                  <Point X="-28.04615234375" Y="-1.390087036133" />
                  <Point X="-28.04334765625" Y="-1.359667236328" />
                  <Point X="-28.045552734375" Y="-1.328002319336" />
                  <Point X="-28.052869140625" Y="-1.297495117188" />
                  <Point X="-28.06972265625" Y="-1.271034790039" />
                  <Point X="-28.092134765625" Y="-1.246143432617" />
                  <Point X="-28.114548828125" Y="-1.227837890625" />
                  <Point X="-28.139455078125" Y="-1.21317956543" />
                  <Point X="-28.16871875" Y="-1.201955810547" />
                  <Point X="-28.200607421875" Y="-1.195474609375" />
                  <Point X="-28.2319296875" Y="-1.194383789062" />
                  <Point X="-29.7321015625" Y="-1.391885131836" />
                  <Point X="-29.834076171875" Y="-0.992652526855" />
                  <Point X="-29.837591796875" Y="-0.968078918457" />
                  <Point X="-29.892421875" Y="-0.584698242188" />
                  <Point X="-28.621240234375" Y="-0.244085800171" />
                  <Point X="-28.532875" Y="-0.220408309937" />
                  <Point X="-28.5159375" Y="-0.214089279175" />
                  <Point X="-28.4860703125" Y="-0.198318862915" />
                  <Point X="-28.4599765625" Y="-0.180209213257" />
                  <Point X="-28.43654296875" Y="-0.156965698242" />
                  <Point X="-28.41674609375" Y="-0.128933349609" />
                  <Point X="-28.403615234375" Y="-0.102288375854" />
                  <Point X="-28.394916015625" Y="-0.074256790161" />
                  <Point X="-28.39066015625" Y="-0.04443422699" />
                  <Point X="-28.3912109375" Y="-0.013020145416" />
                  <Point X="-28.395466796875" Y="0.0134715271" />
                  <Point X="-28.404166015625" Y="0.041503417969" />
                  <Point X="-28.419357421875" Y="0.070954696655" />
                  <Point X="-28.440255859375" Y="0.098358177185" />
                  <Point X="-28.461626953125" Y="0.118793716431" />
                  <Point X="-28.4877265625" Y="0.136908691406" />
                  <Point X="-28.501923828125" Y="0.145046768188" />
                  <Point X="-28.532875" Y="0.157848175049" />
                  <Point X="-29.891814453125" Y="0.521975158691" />
                  <Point X="-29.824486328125" Y="0.976972595215" />
                  <Point X="-29.81741015625" Y="1.003090637207" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-28.8290078125" Y="1.308132324219" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.744982421875" Y="1.299341796875" />
                  <Point X="-28.723421875" Y="1.301228271484" />
                  <Point X="-28.703134765625" Y="1.305263793945" />
                  <Point X="-28.6994765625" Y="1.306416992188" />
                  <Point X="-28.641708984375" Y="1.324631103516" />
                  <Point X="-28.6227734375" Y="1.332963989258" />
                  <Point X="-28.6040234375" Y="1.343790527344" />
                  <Point X="-28.587337890625" Y="1.356027832031" />
                  <Point X="-28.5736953125" Y="1.371587890625" />
                  <Point X="-28.56128125" Y="1.389325439453" />
                  <Point X="-28.551333984375" Y="1.40746862793" />
                  <Point X="-28.549876953125" Y="1.410987915039" />
                  <Point X="-28.526703125" Y="1.466935791016" />
                  <Point X="-28.520916015625" Y="1.486787841797" />
                  <Point X="-28.51715625" Y="1.508103027344" />
                  <Point X="-28.515802734375" Y="1.528749633789" />
                  <Point X="-28.518951171875" Y="1.549199707031" />
                  <Point X="-28.5245546875" Y="1.570106201172" />
                  <Point X="-28.5320546875" Y="1.589384399414" />
                  <Point X="-28.533826171875" Y="1.592786743164" />
                  <Point X="-28.561794921875" Y="1.646514038086" />
                  <Point X="-28.57328125" Y="1.663704589844" />
                  <Point X="-28.587193359375" Y="1.68028515625" />
                  <Point X="-28.60213671875" Y="1.694590454102" />
                  <Point X="-29.351859375" Y="2.269873291016" />
                  <Point X="-29.081146484375" Y="2.733667724609" />
                  <Point X="-29.06241015625" Y="2.757752197266" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.24555859375" Y="2.867132080078" />
                  <Point X="-28.20665625" Y="2.844671142578" />
                  <Point X="-28.187724609375" Y="2.836340820312" />
                  <Point X="-28.16708203125" Y="2.82983203125" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.141697265625" Y="2.825350585938" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.999013671875" Y="2.826504638672" />
                  <Point X="-27.980462890625" Y="2.835653564453" />
                  <Point X="-27.962208984375" Y="2.847282714844" />
                  <Point X="-27.946083984375" Y="2.86022265625" />
                  <Point X="-27.942466796875" Y="2.863839111328" />
                  <Point X="-27.885359375" Y="2.920946044922" />
                  <Point X="-27.872404296875" Y="2.937085693359" />
                  <Point X="-27.860775390625" Y="2.955340087891" />
                  <Point X="-27.85162890625" Y="2.973888916016" />
                  <Point X="-27.8467109375" Y="2.993977783203" />
                  <Point X="-27.843884765625" Y="3.015436767578" />
                  <Point X="-27.84343359375" Y="3.036113037109" />
                  <Point X="-27.84387890625" Y="3.041208007812" />
                  <Point X="-27.85091796875" Y="3.121662597656" />
                  <Point X="-27.854955078125" Y="3.141963378906" />
                  <Point X="-27.86146484375" Y="3.16260546875" />
                  <Point X="-27.869794921875" Y="3.181532958984" />
                  <Point X="-28.18333203125" Y="3.724596191406" />
                  <Point X="-27.700626953125" Y="4.094682861328" />
                  <Point X="-27.671109375" Y="4.111081054688" />
                  <Point X="-27.167037109375" Y="4.391133300781" />
                  <Point X="-27.05510546875" Y="4.245260742188" />
                  <Point X="-27.0431953125" Y="4.229739746094" />
                  <Point X="-27.028888671875" Y="4.214796386719" />
                  <Point X="-27.01230859375" Y="4.200884765625" />
                  <Point X="-26.99511328125" Y="4.189395019531" />
                  <Point X="-26.989443359375" Y="4.186443359375" />
                  <Point X="-26.899896484375" Y="4.139828125" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.777447265625" Y="4.134483886719" />
                  <Point X="-26.771541015625" Y="4.136930664062" />
                  <Point X="-26.6782734375" Y="4.175563476562" />
                  <Point X="-26.660146484375" Y="4.185508789062" />
                  <Point X="-26.642416015625" Y="4.197922851562" />
                  <Point X="-26.626859375" Y="4.211563964844" />
                  <Point X="-26.614626953125" Y="4.228249511719" />
                  <Point X="-26.603806640625" Y="4.246993652344" />
                  <Point X="-26.5954765625" Y="4.265928710937" />
                  <Point X="-26.5935546875" Y="4.272025878906" />
                  <Point X="-26.563197265625" Y="4.368305175781" />
                  <Point X="-26.5591640625" Y="4.388583007812" />
                  <Point X="-26.55727734375" Y="4.410143066406" />
                  <Point X="-26.557728515625" Y="4.430824707031" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-25.94963671875" Y="4.809808105469" />
                  <Point X="-25.913859375" Y="4.813995117188" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.145412109375" Y="4.329266601562" />
                  <Point X="-25.133904296875" Y="4.28631640625" />
                  <Point X="-25.121130859375" Y="4.258127441406" />
                  <Point X="-25.1032734375" Y="4.231400390625" />
                  <Point X="-25.0821171875" Y="4.208810546875" />
                  <Point X="-25.054822265625" Y="4.194219726563" />
                  <Point X="-25.024384765625" Y="4.18388671875" />
                  <Point X="-24.99384765625" Y="4.178844238281" />
                  <Point X="-24.9633125" Y="4.183884765625" />
                  <Point X="-24.932873046875" Y="4.194216308594" />
                  <Point X="-24.905576171875" Y="4.208805175781" />
                  <Point X="-24.88441796875" Y="4.231394042969" />
                  <Point X="-24.86655859375" Y="4.258120605469" />
                  <Point X="-24.853783203125" Y="4.286314453125" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.15595703125" Y="4.83173828125" />
                  <Point X="-24.1263515625" Y="4.824591308594" />
                  <Point X="-23.518970703125" Y="4.677950195312" />
                  <Point X="-23.50109765625" Y="4.671467773438" />
                  <Point X="-23.105361328125" Y="4.527931640625" />
                  <Point X="-23.086716796875" Y="4.519212402344" />
                  <Point X="-22.705416015625" Y="4.340891113281" />
                  <Point X="-22.687384765625" Y="4.330385742188" />
                  <Point X="-22.319025390625" Y="4.115778320312" />
                  <Point X="-22.302037109375" Y="4.103697265625" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.8011015625" Y="2.639977050781" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.859169921875" Y="2.536755859375" />
                  <Point X="-22.868201171875" Y="2.511275878906" />
                  <Point X="-22.888392578125" Y="2.435770996094" />
                  <Point X="-22.8915546875" Y="2.414670898438" />
                  <Point X="-22.892390625" Y="2.391616455078" />
                  <Point X="-22.89176953125" Y="2.376803222656" />
                  <Point X="-22.883900390625" Y="2.311531494141" />
                  <Point X="-22.878556640625" Y="2.289603271484" />
                  <Point X="-22.8702890625" Y="2.267512939453" />
                  <Point X="-22.859923828125" Y="2.247466552734" />
                  <Point X="-22.857365234375" Y="2.243696289062" />
                  <Point X="-22.81696484375" Y="2.184157958984" />
                  <Point X="-22.80301953125" Y="2.16790625" />
                  <Point X="-22.785951171875" Y="2.152048095703" />
                  <Point X="-22.77462890625" Y="2.143033691406" />
                  <Point X="-22.71508984375" Y="2.102634521484" />
                  <Point X="-22.695046875" Y="2.092272216797" />
                  <Point X="-22.672958984375" Y="2.084006347656" />
                  <Point X="-22.6510390625" Y="2.0786640625" />
                  <Point X="-22.646904296875" Y="2.078165283203" />
                  <Point X="-22.581615234375" Y="2.070292480469" />
                  <Point X="-22.559806640625" Y="2.070183105469" />
                  <Point X="-22.536123046875" Y="2.072799560547" />
                  <Point X="-22.522013671875" Y="2.075449707031" />
                  <Point X="-22.4465078125" Y="2.095640625" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.032673828125" Y="2.906190429688" />
                  <Point X="-20.876716796875" Y="2.689447021484" />
                  <Point X="-20.867259765625" Y="2.673817138672" />
                  <Point X="-20.73780078125" Y="2.459884033203" />
                  <Point X="-21.701654296875" Y="1.720291992188" />
                  <Point X="-21.76921484375" Y="1.668451171875" />
                  <Point X="-21.78117578125" Y="1.657533081055" />
                  <Point X="-21.79946875" Y="1.637137329102" />
                  <Point X="-21.85380859375" Y="1.566245483398" />
                  <Point X="-21.864951171875" Y="1.54764050293" />
                  <Point X="-21.874703125" Y="1.52610559082" />
                  <Point X="-21.87965234375" Y="1.512502685547" />
                  <Point X="-21.89989453125" Y="1.440121704102" />
                  <Point X="-21.90334765625" Y="1.41782421875" />
                  <Point X="-21.9041640625" Y="1.39425378418" />
                  <Point X="-21.902259765625" Y="1.371760009766" />
                  <Point X="-21.90120703125" Y="1.36666015625" />
                  <Point X="-21.88458984375" Y="1.286127319336" />
                  <Point X="-21.87776171875" Y="1.26542199707" />
                  <Point X="-21.867703125" Y="1.243694458008" />
                  <Point X="-21.86085546875" Y="1.231390258789" />
                  <Point X="-21.81566015625" Y="1.16269519043" />
                  <Point X="-21.801107421875" Y="1.145449707031" />
                  <Point X="-21.78386328125" Y="1.129359985352" />
                  <Point X="-21.765646484375" Y="1.116030639648" />
                  <Point X="-21.761498046875" Y="1.113695922852" />
                  <Point X="-21.69600390625" Y="1.076828369141" />
                  <Point X="-21.67551171875" Y="1.068273803711" />
                  <Point X="-21.651927734375" Y="1.061534545898" />
                  <Point X="-21.6382734375" Y="1.058697265625" />
                  <Point X="-21.54971875" Y="1.046993896484" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.223162109375" Y="1.21663659668" />
                  <Point X="-20.15405859375" Y="0.932785217285" />
                  <Point X="-20.151078125" Y="0.91364440918" />
                  <Point X="-20.109134765625" Y="0.644238891602" />
                  <Point X="-21.205994140625" Y="0.350335998535" />
                  <Point X="-21.283419921875" Y="0.329589874268" />
                  <Point X="-21.298939453125" Y="0.323944946289" />
                  <Point X="-21.323962890625" Y="0.311883239746" />
                  <Point X="-21.410962890625" Y="0.261595367432" />
                  <Point X="-21.428638671875" Y="0.248425598145" />
                  <Point X="-21.446255859375" Y="0.231793457031" />
                  <Point X="-21.4557734375" Y="0.221365463257" />
                  <Point X="-21.507974609375" Y="0.154849853516" />
                  <Point X="-21.51969921875" Y="0.135567611694" />
                  <Point X="-21.52947265625" Y="0.11410369873" />
                  <Point X="-21.536318359375" Y="0.092599098206" />
                  <Point X="-21.537419921875" Y="0.086845481873" />
                  <Point X="-21.5548203125" Y="-0.004011537552" />
                  <Point X="-21.556419921875" Y="-0.026131343842" />
                  <Point X="-21.555318359375" Y="-0.050694671631" />
                  <Point X="-21.55371875" Y="-0.064307685852" />
                  <Point X="-21.536318359375" Y="-0.155164550781" />
                  <Point X="-21.529474609375" Y="-0.176659744263" />
                  <Point X="-21.519705078125" Y="-0.198117752075" />
                  <Point X="-21.507994140625" Y="-0.217384048462" />
                  <Point X="-21.504669921875" Y="-0.221622329712" />
                  <Point X="-21.45246875" Y="-0.288137786865" />
                  <Point X="-21.43649609375" Y="-0.304132568359" />
                  <Point X="-21.416673828125" Y="-0.319737060547" />
                  <Point X="-21.405453125" Y="-0.327340087891" />
                  <Point X="-21.318453125" Y="-0.377627929688" />
                  <Point X="-21.307291015625" Y="-0.383137786865" />
                  <Point X="-21.283419921875" Y="-0.392150024414" />
                  <Point X="-20.108525390625" Y="-0.706961914062" />
                  <Point X="-20.144974609375" Y="-0.948729492188" />
                  <Point X="-20.148796875" Y="-0.965472167969" />
                  <Point X="-20.198822265625" Y="-1.18469921875" />
                  <Point X="-21.485826171875" Y="-1.015262145996" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.598365234375" Y="-1.003193481445" />
                  <Point X="-21.626322265625" Y="-1.006256774902" />
                  <Point X="-21.63615234375" Y="-1.007859069824" />
                  <Point X="-21.806904296875" Y="-1.044972290039" />
                  <Point X="-21.8372734375" Y="-1.055693725586" />
                  <Point X="-21.868509765625" Y="-1.076037353516" />
                  <Point X="-21.887544921875" Y="-1.094671875" />
                  <Point X="-21.894134765625" Y="-1.10182019043" />
                  <Point X="-21.99734375" Y="-1.225947631836" />
                  <Point X="-22.01337109375" Y="-1.250416015625" />
                  <Point X="-22.02548828125" Y="-1.282390991211" />
                  <Point X="-22.030021484375" Y="-1.307123657227" />
                  <Point X="-22.031177734375" Y="-1.315546386719" />
                  <Point X="-22.04596875" Y="-1.47629699707" />
                  <Point X="-22.044888671875" Y="-1.508485717773" />
                  <Point X="-22.0347578125" Y="-1.544657104492" />
                  <Point X="-22.022263671875" Y="-1.569128417969" />
                  <Point X="-22.017564453125" Y="-1.577304443359" />
                  <Point X="-21.923068359375" Y="-1.724287109375" />
                  <Point X="-21.913064453125" Y="-1.737241210938" />
                  <Point X="-21.889369140625" Y="-1.760909423828" />
                  <Point X="-20.786875" Y="-2.606883300781" />
                  <Point X="-20.8751796875" Y="-2.749770751953" />
                  <Point X="-20.883099609375" Y="-2.761024414062" />
                  <Point X="-20.971017578125" Y="-2.885944580078" />
                  <Point X="-22.119072265625" Y="-2.223114501953" />
                  <Point X="-22.199044921875" Y="-2.176942871094" />
                  <Point X="-22.220603515625" Y="-2.167825683594" />
                  <Point X="-22.2495859375" Y="-2.159599121094" />
                  <Point X="-22.258642578125" Y="-2.157501220703" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.494025390625" Y="-2.119082275391" />
                  <Point X="-22.53125390625" Y="-2.126161621094" />
                  <Point X="-22.5576171875" Y="-2.136958740234" />
                  <Point X="-22.565857421875" Y="-2.140803710938" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.75885546875" Y="-2.246129638672" />
                  <Point X="-22.7826015625" Y="-2.271526123047" />
                  <Point X="-22.797119140625" Y="-2.294285888672" />
                  <Point X="-22.80109375" Y="-2.301129638672" />
                  <Point X="-22.889947265625" Y="-2.469956542969" />
                  <Point X="-22.901267578125" Y="-2.500112304688" />
                  <Point X="-22.905697265625" Y="-2.538049560547" />
                  <Point X="-22.90316015625" Y="-2.567419921875" />
                  <Point X="-22.902" Y="-2.576128417969" />
                  <Point X="-22.865296875" Y="-2.779349609375" />
                  <Point X="-22.86101171875" Y="-2.795140136719" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.13871484375" Y="-4.054905029297" />
                  <Point X="-22.218146484375" Y="-4.111641601562" />
                  <Point X="-22.2269921875" Y="-4.117368164063" />
                  <Point X="-22.298232421875" Y="-4.163480957031" />
                  <Point X="-23.18042578125" Y="-3.01378515625" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25833984375" Y="-2.917221191406" />
                  <Point X="-23.283662109375" Y="-2.897440917969" />
                  <Point X="-23.290767578125" Y="-2.892396972656" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.520255859375" Y="-2.749644287109" />
                  <Point X="-23.55791796875" Y="-2.7419375" />
                  <Point X="-23.588255859375" Y="-2.741994628906" />
                  <Point X="-23.59678125" Y="-2.742394042969" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.844619140625" Y="-2.768534667969" />
                  <Point X="-23.876697265625" Y="-2.783205078125" />
                  <Point X="-23.900248046875" Y="-2.799864990234" />
                  <Point X="-23.90612109375" Y="-2.804373291016" />
                  <Point X="-24.07538671875" Y="-2.945111328125" />
                  <Point X="-24.097384765625" Y="-2.968639160156" />
                  <Point X="-24.116162109375" Y="-3.002524658203" />
                  <Point X="-24.125486328125" Y="-3.032596191406" />
                  <Point X="-24.127580078125" Y="-3.040553710938" />
                  <Point X="-24.178189453125" Y="-3.273396484375" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323119628906" />
                  <Point X="-23.97793359375" Y="-4.859915527344" />
                  <Point X="-24.0243203125" Y="-4.870083007812" />
                  <Point X="-24.0324921875" Y="-4.871567871094" />
                  <Point X="-24.07068359375" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.058419921875" Y="-4.752637695312" />
                  <Point X="-26.069052734375" Y="-4.74990234375" />
                  <Point X="-26.141244140625" Y="-4.731328125" />
                  <Point X="-26.123646484375" Y="-4.597654785156" />
                  <Point X="-26.1207734375" Y="-4.575841308594" />
                  <Point X="-26.119962890625" Y="-4.56397265625" />
                  <Point X="-26.119830078125" Y="-4.540227050781" />
                  <Point X="-26.1205078125" Y="-4.528350097656" />
                  <Point X="-26.124646484375" Y="-4.493943847656" />
                  <Point X="-26.126939453125" Y="-4.479566894531" />
                  <Point X="-26.18386328125" Y="-4.193397460938" />
                  <Point X="-26.186861328125" Y="-4.182043457031" />
                  <Point X="-26.19686328125" Y="-4.151866699219" />
                  <Point X="-26.206908203125" Y="-4.130724609375" />
                  <Point X="-26.227892578125" Y="-4.097773925781" />
                  <Point X="-26.24009765625" Y="-4.082387695312" />
                  <Point X="-26.264322265625" Y="-4.057612792969" />
                  <Point X="-26.274896484375" Y="-4.047596191406" />
                  <Point X="-26.494265625" Y="-3.855214355469" />
                  <Point X="-26.503556640625" Y="-3.848032714844" />
                  <Point X="-26.529857421875" Y="-3.830183105469" />
                  <Point X="-26.55069921875" Y="-3.819523681641" />
                  <Point X="-26.587416015625" Y="-3.806153564453" />
                  <Point X="-26.60646875" Y="-3.801376953125" />
                  <Point X="-26.640771484375" Y="-3.796469482422" />
                  <Point X="-26.655251953125" Y="-3.794961181641" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9581484375" Y="-3.7758359375" />
                  <Point X="-26.989884765625" Y="-3.777686767578" />
                  <Point X="-27.012908203125" Y="-3.781918701172" />
                  <Point X="-27.050169921875" Y="-3.793661621094" />
                  <Point X="-27.068189453125" Y="-3.801468994141" />
                  <Point X="-27.098390625" Y="-3.818457275391" />
                  <Point X="-27.11080078125" Y="-3.826077880859" />
                  <Point X="-27.35340234375" Y="-3.988180664062" />
                  <Point X="-27.3596796875" Y="-3.992757324219" />
                  <Point X="-27.380447265625" Y="-4.010137695313" />
                  <Point X="-27.4027578125" Y="-4.032773193359" />
                  <Point X="-27.41046875" Y="-4.041628173828" />
                  <Point X="-27.503201171875" Y="-4.162479492188" />
                  <Point X="-27.747595703125" Y="-4.011156005859" />
                  <Point X="-27.76228515625" Y="-3.999845214844" />
                  <Point X="-27.98086328125" Y="-3.831547119141" />
                  <Point X="-27.385826171875" Y="-2.800910400391" />
                  <Point X="-27.341490234375" Y="-2.724120117188" />
                  <Point X="-27.3348515625" Y="-2.710086181641" />
                  <Point X="-27.32394921875" Y="-2.681121582031" />
                  <Point X="-27.319685546875" Y="-2.666190917969" />
                  <Point X="-27.3134140625" Y="-2.634665039062" />
                  <Point X="-27.31162890625" Y="-2.61890625" />
                  <Point X="-27.310705078125" Y="-2.587312255859" />
                  <Point X="-27.315048828125" Y="-2.556005615234" />
                  <Point X="-27.324541015625" Y="-2.525857421875" />
                  <Point X="-27.33055078125" Y="-2.511180664062" />
                  <Point X="-27.34580078125" Y="-2.481318603516" />
                  <Point X="-27.353572265625" Y="-2.468653808594" />
                  <Point X="-27.371" Y="-2.4446875" />
                  <Point X="-27.38065625" Y="-2.433385986328" />
                  <Point X="-27.396970703125" Y="-2.417070556641" />
                  <Point X="-27.408810546875" Y="-2.407025146484" />
                  <Point X="-27.433966796875" Y="-2.388997070312" />
                  <Point X="-27.447283203125" Y="-2.381014404297" />
                  <Point X="-27.476111328125" Y="-2.366796386719" />
                  <Point X="-27.490552734375" Y="-2.361089111328" />
                  <Point X="-27.520171875" Y="-2.352103271484" />
                  <Point X="-27.550859375" Y="-2.3480625" />
                  <Point X="-27.581794921875" Y="-2.349074707031" />
                  <Point X="-27.597220703125" Y="-2.350849121094" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.793087890625" Y="-3.017708740234" />
                  <Point X="-29.00401171875" Y="-2.740597900391" />
                  <Point X="-29.014548828125" Y="-2.722928955078" />
                  <Point X="-29.181265625" Y="-2.443372558594" />
                  <Point X="-28.125693359375" Y="-1.633404663086" />
                  <Point X="-28.048123046875" Y="-1.573881713867" />
                  <Point X="-28.035921875" Y="-1.562703491211" />
                  <Point X="-28.013646484375" Y="-1.538400634766" />
                  <Point X="-28.003572265625" Y="-1.525275634766" />
                  <Point X="-27.9851484375" Y="-1.496373535156" />
                  <Point X="-27.9781484375" Y="-1.483218261719" />
                  <Point X="-27.966291015625" Y="-1.45597277832" />
                  <Point X="-27.96143359375" Y="-1.44188293457" />
                  <Point X="-27.9541875" Y="-1.413907226562" />
                  <Point X="-27.951552734375" Y="-1.398808959961" />
                  <Point X="-27.948748046875" Y="-1.368389160156" />
                  <Point X="-27.948578125" Y="-1.353067626953" />
                  <Point X="-27.950783203125" Y="-1.321402709961" />
                  <Point X="-27.953171875" Y="-1.305847167969" />
                  <Point X="-27.96048828125" Y="-1.27533996582" />
                  <Point X="-27.9727421875" Y="-1.246459228516" />
                  <Point X="-27.989595703125" Y="-1.219999023438" />
                  <Point X="-27.999123046875" Y="-1.207467651367" />
                  <Point X="-28.02153515625" Y="-1.182576293945" />
                  <Point X="-28.03204296875" Y="-1.172564086914" />
                  <Point X="-28.05445703125" Y="-1.154258544922" />
                  <Point X="-28.06636328125" Y="-1.145965087891" />
                  <Point X="-28.09126953125" Y="-1.131306640625" />
                  <Point X="-28.105435546875" Y="-1.124479736328" />
                  <Point X="-28.13469921875" Y="-1.113255981445" />
                  <Point X="-28.149796875" Y="-1.108859130859" />
                  <Point X="-28.181685546875" Y="-1.102378051758" />
                  <Point X="-28.19730078125" Y="-1.100532226562" />
                  <Point X="-28.228623046875" Y="-1.09944140625" />
                  <Point X="-28.244330078125" Y="-1.100196533203" />
                  <Point X="-29.660919921875" Y="-1.286694091797" />
                  <Point X="-29.740759765625" Y="-0.974120056152" />
                  <Point X="-29.743548828125" Y="-0.954624694824" />
                  <Point X="-29.78644921875" Y="-0.654654296875" />
                  <Point X="-28.59665234375" Y="-0.335848693848" />
                  <Point X="-28.508287109375" Y="-0.312171295166" />
                  <Point X="-28.49966796875" Y="-0.309415679932" />
                  <Point X="-28.471580078125" Y="-0.298097442627" />
                  <Point X="-28.441712890625" Y="-0.282327056885" />
                  <Point X="-28.431904296875" Y="-0.276364471436" />
                  <Point X="-28.405810546875" Y="-0.258254760742" />
                  <Point X="-28.393076171875" Y="-0.247657348633" />
                  <Point X="-28.369642578125" Y="-0.224413772583" />
                  <Point X="-28.358943359375" Y="-0.21176789856" />
                  <Point X="-28.339146484375" Y="-0.183735534668" />
                  <Point X="-28.33153125" Y="-0.170927658081" />
                  <Point X="-28.318400390625" Y="-0.144282699585" />
                  <Point X="-28.312884765625" Y="-0.130445602417" />
                  <Point X="-28.304185546875" Y="-0.102413986206" />
                  <Point X="-28.300869140625" Y="-0.087677879333" />
                  <Point X="-28.29661328125" Y="-0.057855354309" />
                  <Point X="-28.295673828125" Y="-0.042768791199" />
                  <Point X="-28.296224609375" Y="-0.011354816437" />
                  <Point X="-28.2974140625" Y="0.002048294544" />
                  <Point X="-28.301669921875" Y="0.028540029526" />
                  <Point X="-28.304736328125" Y="0.041628505707" />
                  <Point X="-28.313435546875" Y="0.069660415649" />
                  <Point X="-28.319736328125" Y="0.085053588867" />
                  <Point X="-28.334927734375" Y="0.114504852295" />
                  <Point X="-28.343818359375" Y="0.128562942505" />
                  <Point X="-28.364716796875" Y="0.15596647644" />
                  <Point X="-28.374599609375" Y="0.167019424438" />
                  <Point X="-28.395970703125" Y="0.187454910278" />
                  <Point X="-28.407458984375" Y="0.196837478638" />
                  <Point X="-28.43355859375" Y="0.214952529907" />
                  <Point X="-28.440482421875" Y="0.219328292847" />
                  <Point X="-28.465615234375" Y="0.232834396362" />
                  <Point X="-28.49656640625" Y="0.245635726929" />
                  <Point X="-28.508287109375" Y="0.249611099243" />
                  <Point X="-29.785443359375" Y="0.591824523926" />
                  <Point X="-29.731328125" Y="0.957536865234" />
                  <Point X="-29.725716796875" Y="0.978247802734" />
                  <Point X="-29.6335859375" Y="1.318237182617" />
                  <Point X="-28.841408203125" Y="1.21394519043" />
                  <Point X="-28.778064453125" Y="1.20560559082" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.7470546875" Y="1.204364257812" />
                  <Point X="-28.736701171875" Y="1.204703369141" />
                  <Point X="-28.715140625" Y="1.20658984375" />
                  <Point X="-28.70488671875" Y="1.208053833008" />
                  <Point X="-28.684599609375" Y="1.212089233398" />
                  <Point X="-28.6709140625" Y="1.215812255859" />
                  <Point X="-28.613146484375" Y="1.234026367188" />
                  <Point X="-28.603443359375" Y="1.237678344727" />
                  <Point X="-28.5845078125" Y="1.246011108398" />
                  <Point X="-28.57526953125" Y="1.250693847656" />
                  <Point X="-28.55651953125" Y="1.261520385742" />
                  <Point X="-28.54783984375" Y="1.267184814453" />
                  <Point X="-28.531154296875" Y="1.279422119141" />
                  <Point X="-28.51590625" Y="1.29339831543" />
                  <Point X="-28.502263671875" Y="1.308958374023" />
                  <Point X="-28.49586328125" Y="1.317115478516" />
                  <Point X="-28.48344921875" Y="1.334853027344" />
                  <Point X="-28.47798046875" Y="1.343654174805" />
                  <Point X="-28.468033203125" Y="1.361797485352" />
                  <Point X="-28.462107421875" Y="1.374633666992" />
                  <Point X="-28.43893359375" Y="1.430581542969" />
                  <Point X="-28.4355" Y="1.440348754883" />
                  <Point X="-28.429712890625" Y="1.460200805664" />
                  <Point X="-28.427359375" Y="1.470285644531" />
                  <Point X="-28.423599609375" Y="1.491600830078" />
                  <Point X="-28.422359375" Y="1.501888427734" />
                  <Point X="-28.421005859375" Y="1.52253515625" />
                  <Point X="-28.421908203125" Y="1.543205200195" />
                  <Point X="-28.425056640625" Y="1.563655273438" />
                  <Point X="-28.427189453125" Y="1.573794189453" />
                  <Point X="-28.43279296875" Y="1.594700683594" />
                  <Point X="-28.43601953125" Y="1.604550292969" />
                  <Point X="-28.44351953125" Y="1.623828491211" />
                  <Point X="-28.449564453125" Y="1.636659423828" />
                  <Point X="-28.477533203125" Y="1.69038671875" />
                  <Point X="-28.4828046875" Y="1.69929309082" />
                  <Point X="-28.494291015625" Y="1.716483642578" />
                  <Point X="-28.500505859375" Y="1.724767822266" />
                  <Point X="-28.51441796875" Y="1.741348388672" />
                  <Point X="-28.5215" Y="1.748909423828" />
                  <Point X="-28.536443359375" Y="1.76321472168" />
                  <Point X="-28.5443046875" Y="1.769958984375" />
                  <Point X="-29.22761328125" Y="2.294280761719" />
                  <Point X="-29.00228125" Y="2.680325927734" />
                  <Point X="-28.987427734375" Y="2.699420166016" />
                  <Point X="-28.726337890625" Y="3.035013183594" />
                  <Point X="-28.29305859375" Y="2.784859619141" />
                  <Point X="-28.25415625" Y="2.762398681641" />
                  <Point X="-28.24491796875" Y="2.757716796875" />
                  <Point X="-28.225986328125" Y="2.749386474609" />
                  <Point X="-28.21629296875" Y="2.745738037109" />
                  <Point X="-28.195650390625" Y="2.739229248047" />
                  <Point X="-28.185615234375" Y="2.736657226562" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.1499765625" Y="2.730712158203" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310791016" />
                  <Point X="-27.97643359375" Y="2.734227294922" />
                  <Point X="-27.956994140625" Y="2.741302978516" />
                  <Point X="-27.938443359375" Y="2.750451904297" />
                  <Point X="-27.929419921875" Y="2.755531738281" />
                  <Point X="-27.911166015625" Y="2.767160888672" />
                  <Point X="-27.902751953125" Y="2.773189697266" />
                  <Point X="-27.886626953125" Y="2.786129638672" />
                  <Point X="-27.875298828125" Y="2.796657226562" />
                  <Point X="-27.81819140625" Y="2.853764160156" />
                  <Point X="-27.8112734375" Y="2.861478759766" />
                  <Point X="-27.798318359375" Y="2.877618408203" />
                  <Point X="-27.79228125" Y="2.886043457031" />
                  <Point X="-27.78065234375" Y="2.904297851562" />
                  <Point X="-27.7755703125" Y="2.913325683594" />
                  <Point X="-27.766423828125" Y="2.931874511719" />
                  <Point X="-27.759353515625" Y="2.951299072266" />
                  <Point X="-27.754435546875" Y="2.971387939453" />
                  <Point X="-27.7525234375" Y="2.981573242188" />
                  <Point X="-27.749697265625" Y="3.003032226562" />
                  <Point X="-27.748908203125" Y="3.013364257812" />
                  <Point X="-27.74845703125" Y="3.034040527344" />
                  <Point X="-27.749240234375" Y="3.049479736328" />
                  <Point X="-27.756279296875" Y="3.129934326172" />
                  <Point X="-27.7577421875" Y="3.140191894531" />
                  <Point X="-27.761779296875" Y="3.160492675781" />
                  <Point X="-27.764353515625" Y="3.170535888672" />
                  <Point X="-27.77086328125" Y="3.191177978516" />
                  <Point X="-27.774513671875" Y="3.200873291016" />
                  <Point X="-27.78284375" Y="3.21980078125" />
                  <Point X="-27.7875234375" Y="3.229032958984" />
                  <Point X="-28.05938671875" Y="3.699915527344" />
                  <Point X="-27.648373046875" Y="4.015036621094" />
                  <Point X="-27.624974609375" Y="4.028035644531" />
                  <Point X="-27.192525390625" Y="4.268295410156" />
                  <Point X="-27.130474609375" Y="4.187428710937" />
                  <Point X="-27.11181640625" Y="4.164042480469" />
                  <Point X="-27.097509765625" Y="4.149099121094" />
                  <Point X="-27.089951171875" Y="4.142020507812" />
                  <Point X="-27.07337109375" Y="4.128108886719" />
                  <Point X="-27.065087890625" Y="4.121895507812" />
                  <Point X="-27.047892578125" Y="4.110405761719" />
                  <Point X="-27.033310546875" Y="4.102177734375" />
                  <Point X="-26.943763671875" Y="4.0555625" />
                  <Point X="-26.934326171875" Y="4.051286621094" />
                  <Point X="-26.915044921875" Y="4.0437890625" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031378173828" />
                  <Point X="-26.780818359375" Y="4.035136474609" />
                  <Point X="-26.7707265625" Y="4.037489013672" />
                  <Point X="-26.750861328125" Y="4.043279541016" />
                  <Point X="-26.735181640625" Y="4.049163818359" />
                  <Point X="-26.6419140625" Y="4.087796630859" />
                  <Point X="-26.632578125" Y="4.092275390625" />
                  <Point X="-26.614451171875" Y="4.102220703125" />
                  <Point X="-26.60566015625" Y="4.1076875" />
                  <Point X="-26.5879296875" Y="4.1201015625" />
                  <Point X="-26.579783203125" Y="4.126494140625" />
                  <Point X="-26.5642265625" Y="4.140135253906" />
                  <Point X="-26.5502421875" Y="4.155395507812" />
                  <Point X="-26.538009765625" Y="4.172081054688" />
                  <Point X="-26.5323515625" Y="4.180754882812" />
                  <Point X="-26.52153125" Y="4.199499023438" />
                  <Point X="-26.516849609375" Y="4.208738769531" />
                  <Point X="-26.50851953125" Y="4.227673828125" />
                  <Point X="-26.50294921875" Y="4.243466308594" />
                  <Point X="-26.472591796875" Y="4.339745605469" />
                  <Point X="-26.470021484375" Y="4.349772949219" />
                  <Point X="-26.46598828125" Y="4.37005078125" />
                  <Point X="-26.464525390625" Y="4.380301269531" />
                  <Point X="-26.462638671875" Y="4.401861328125" />
                  <Point X="-26.46230078125" Y="4.41221484375" />
                  <Point X="-26.462751953125" Y="4.432896484375" />
                  <Point X="-26.463541015625" Y="4.443225097656" />
                  <Point X="-26.479265625" Y="4.562655273438" />
                  <Point X="-25.9311796875" Y="4.716319824219" />
                  <Point X="-25.90281640625" Y="4.719639160156" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.23717578125" Y="4.304678710938" />
                  <Point X="-25.22566796875" Y="4.261728515625" />
                  <Point X="-25.220435546875" Y="4.247106445313" />
                  <Point X="-25.207662109375" Y="4.218917480469" />
                  <Point X="-25.20012109375" Y="4.205350585938" />
                  <Point X="-25.182263671875" Y="4.178623535156" />
                  <Point X="-25.17261328125" Y="4.166461425781" />
                  <Point X="-25.15145703125" Y="4.143871582031" />
                  <Point X="-25.12690234375" Y="4.125029785156" />
                  <Point X="-25.099607421875" Y="4.110438964844" />
                  <Point X="-25.085361328125" Y="4.104262207031" />
                  <Point X="-25.054923828125" Y="4.093929199219" />
                  <Point X="-25.039861328125" Y="4.090156005859" />
                  <Point X="-25.00932421875" Y="4.085113525391" />
                  <Point X="-24.978375" Y="4.085112792969" />
                  <Point X="-24.94783984375" Y="4.090153320312" />
                  <Point X="-24.932779296875" Y="4.093925292969" />
                  <Point X="-24.90233984375" Y="4.104256835938" />
                  <Point X="-24.88809375" Y="4.110431640625" />
                  <Point X="-24.860796875" Y="4.125020507812" />
                  <Point X="-24.8362421875" Y="4.143861816406" />
                  <Point X="-24.815083984375" Y="4.166450683594" />
                  <Point X="-24.8054296875" Y="4.178612304688" />
                  <Point X="-24.7875703125" Y="4.205338867188" />
                  <Point X="-24.78002734375" Y="4.218911132812" />
                  <Point X="-24.767251953125" Y="4.247104980469" />
                  <Point X="-24.76201953125" Y="4.2617265625" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.17212109375" Y="4.737911621094" />
                  <Point X="-24.14864453125" Y="4.732244140625" />
                  <Point X="-23.546400390625" Y="4.586843261719" />
                  <Point X="-23.53348828125" Y="4.58216015625" />
                  <Point X="-23.141751953125" Y="4.44007421875" />
                  <Point X="-23.1269609375" Y="4.433157714844" />
                  <Point X="-22.749541015625" Y="4.256651367187" />
                  <Point X="-22.735208984375" Y="4.248301269531" />
                  <Point X="-22.370572265625" Y="4.035862548828" />
                  <Point X="-22.35709375" Y="4.026277587891" />
                  <Point X="-22.18221875" Y="3.901915283203" />
                  <Point X="-22.883373046875" Y="2.687477050781" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.93836328125" Y="2.591571777344" />
                  <Point X="-22.9487109375" Y="2.568493408203" />
                  <Point X="-22.9577421875" Y="2.543013427734" />
                  <Point X="-22.9599765625" Y="2.535818359375" />
                  <Point X="-22.98016796875" Y="2.460313476562" />
                  <Point X="-22.98234375" Y="2.449850585938" />
                  <Point X="-22.985505859375" Y="2.428750488281" />
                  <Point X="-22.9864921875" Y="2.41811328125" />
                  <Point X="-22.987328125" Y="2.395058837891" />
                  <Point X="-22.987306640625" Y="2.38763671875" />
                  <Point X="-22.9860859375" Y="2.365432373047" />
                  <Point X="-22.978216796875" Y="2.300160644531" />
                  <Point X="-22.97619921875" Y="2.289038818359" />
                  <Point X="-22.97085546875" Y="2.267110595703" />
                  <Point X="-22.967529296875" Y="2.256304199219" />
                  <Point X="-22.95926171875" Y="2.234213867188" />
                  <Point X="-22.95467578125" Y="2.223879638672" />
                  <Point X="-22.944310546875" Y="2.203833251953" />
                  <Point X="-22.93597265625" Y="2.190350830078" />
                  <Point X="-22.895572265625" Y="2.1308125" />
                  <Point X="-22.889060546875" Y="2.122293701172" />
                  <Point X="-22.875115234375" Y="2.106041992188" />
                  <Point X="-22.867681640625" Y="2.098309082031" />
                  <Point X="-22.85061328125" Y="2.082450927734" />
                  <Point X="-22.845123046875" Y="2.077726806641" />
                  <Point X="-22.82796875" Y="2.064422119141" />
                  <Point X="-22.7684296875" Y="2.024022827148" />
                  <Point X="-22.75871875" Y="2.018245727539" />
                  <Point X="-22.73867578125" Y="2.007883422852" />
                  <Point X="-22.72834375" Y="2.003298339844" />
                  <Point X="-22.706255859375" Y="1.995032470703" />
                  <Point X="-22.695453125" Y="1.991708007812" />
                  <Point X="-22.673533203125" Y="1.986365722656" />
                  <Point X="-22.65828125" Y="1.983848876953" />
                  <Point X="-22.5929921875" Y="1.975976196289" />
                  <Point X="-22.582091796875" Y="1.975293701172" />
                  <Point X="-22.560283203125" Y="1.975184326172" />
                  <Point X="-22.549375" Y="1.975757568359" />
                  <Point X="-22.52569140625" Y="1.978373901367" />
                  <Point X="-22.5185859375" Y="1.979432250977" />
                  <Point X="-22.49747265625" Y="1.983674316406" />
                  <Point X="-22.421966796875" Y="2.003865112305" />
                  <Point X="-22.416005859375" Y="2.005670654297" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572875977" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.059595703125" Y="2.780950439453" />
                  <Point X="-20.956029296875" Y="2.637018310547" />
                  <Point X="-20.9485390625" Y="2.624637939453" />
                  <Point X="-20.86311328125" Y="2.483471923828" />
                  <Point X="-21.759486328125" Y="1.795660522461" />
                  <Point X="-21.827046875" Y="1.743819702148" />
                  <Point X="-21.83326171875" Y="1.738615356445" />
                  <Point X="-21.8518984375" Y="1.720963500977" />
                  <Point X="-21.87019140625" Y="1.700567749023" />
                  <Point X="-21.8748671875" Y="1.694931152344" />
                  <Point X="-21.92920703125" Y="1.624039428711" />
                  <Point X="-21.935310546875" Y="1.615056762695" />
                  <Point X="-21.946453125" Y="1.596451782227" />
                  <Point X="-21.9514921875" Y="1.586829711914" />
                  <Point X="-21.961244140625" Y="1.565294677734" />
                  <Point X="-21.963978515625" Y="1.558586914062" />
                  <Point X="-21.971142578125" Y="1.538088867188" />
                  <Point X="-21.991384765625" Y="1.465707885742" />
                  <Point X="-21.993775390625" Y="1.454660644531" />
                  <Point X="-21.997228515625" Y="1.43236315918" />
                  <Point X="-21.998291015625" Y="1.421112792969" />
                  <Point X="-21.999107421875" Y="1.397542480469" />
                  <Point X="-21.998826171875" Y="1.386239868164" />
                  <Point X="-21.996921875" Y="1.36374597168" />
                  <Point X="-21.99424609375" Y="1.347454711914" />
                  <Point X="-21.97762890625" Y="1.266921875" />
                  <Point X="-21.974810546875" Y="1.256374633789" />
                  <Point X="-21.967982421875" Y="1.235669311523" />
                  <Point X="-21.96397265625" Y="1.22551171875" />
                  <Point X="-21.9539140625" Y="1.203784179688" />
                  <Point X="-21.950712890625" Y="1.197496582031" />
                  <Point X="-21.94021875" Y="1.17917590332" />
                  <Point X="-21.8950234375" Y="1.110480712891" />
                  <Point X="-21.888263671875" Y="1.101427856445" />
                  <Point X="-21.8737109375" Y="1.084182373047" />
                  <Point X="-21.86591796875" Y="1.075989746094" />
                  <Point X="-21.848673828125" Y="1.059900024414" />
                  <Point X="-21.8399609375" Y="1.052692016602" />
                  <Point X="-21.821744140625" Y="1.039362670898" />
                  <Point X="-21.808091796875" Y="1.030906616211" />
                  <Point X="-21.74259765625" Y="0.994039123535" />
                  <Point X="-21.7326015625" Y="0.989160583496" />
                  <Point X="-21.712109375" Y="0.980606140137" />
                  <Point X="-21.70161328125" Y="0.976930114746" />
                  <Point X="-21.678029296875" Y="0.970190795898" />
                  <Point X="-21.671255859375" Y="0.968521484375" />
                  <Point X="-21.650720703125" Y="0.964516235352" />
                  <Point X="-21.562166015625" Y="0.952812744141" />
                  <Point X="-21.555966796875" Y="0.952199584961" />
                  <Point X="-21.53428125" Y="0.951222961426" />
                  <Point X="-21.50878125" Y="0.952032348633" />
                  <Point X="-21.49939453125" Y="0.952797180176" />
                  <Point X="-20.295298828125" Y="1.111319824219" />
                  <Point X="-20.247310546875" Y="0.914204223633" />
                  <Point X="-20.244947265625" Y="0.899027770996" />
                  <Point X="-20.21612890625" Y="0.713921264648" />
                  <Point X="-21.23058203125" Y="0.442098999023" />
                  <Point X="-21.3080078125" Y="0.421352874756" />
                  <Point X="-21.315892578125" Y="0.418867614746" />
                  <Point X="-21.340189453125" Y="0.409522216797" />
                  <Point X="-21.365212890625" Y="0.397460571289" />
                  <Point X="-21.37150390625" Y="0.394131713867" />
                  <Point X="-21.45850390625" Y="0.343843811035" />
                  <Point X="-21.46772265625" Y="0.337775085449" />
                  <Point X="-21.4853984375" Y="0.324605316162" />
                  <Point X="-21.49385546875" Y="0.317504272461" />
                  <Point X="-21.51147265625" Y="0.300872039795" />
                  <Point X="-21.516423828125" Y="0.295835784912" />
                  <Point X="-21.5305078125" Y="0.280016082764" />
                  <Point X="-21.582708984375" Y="0.213500488281" />
                  <Point X="-21.589146484375" Y="0.20420664978" />
                  <Point X="-21.60087109375" Y="0.184924468994" />
                  <Point X="-21.606158203125" Y="0.174935958862" />
                  <Point X="-21.615931640625" Y="0.153472137451" />
                  <Point X="-21.61999609375" Y="0.142920806885" />
                  <Point X="-21.626841796875" Y="0.121416114807" />
                  <Point X="-21.630724609375" Y="0.104709266663" />
                  <Point X="-21.648125" Y="0.013852222443" />
                  <Point X="-21.649572265625" Y="0.002840603113" />
                  <Point X="-21.651171875" Y="-0.019279239655" />
                  <Point X="-21.65132421875" Y="-0.030387464523" />
                  <Point X="-21.65022265625" Y="-0.054950817108" />
                  <Point X="-21.649669921875" Y="-0.061781524658" />
                  <Point X="-21.6470234375" Y="-0.082176750183" />
                  <Point X="-21.629623046875" Y="-0.17303364563" />
                  <Point X="-21.626841796875" Y="-0.183985671997" />
                  <Point X="-21.619998046875" Y="-0.20548085022" />
                  <Point X="-21.615935546875" Y="-0.216024002075" />
                  <Point X="-21.606166015625" Y="-0.237482040405" />
                  <Point X="-21.600884765625" Y="-0.247462356567" />
                  <Point X="-21.589173828125" Y="-0.266728637695" />
                  <Point X="-21.579404296875" Y="-0.280272949219" />
                  <Point X="-21.527203125" Y="-0.346788543701" />
                  <Point X="-21.519689453125" Y="-0.355266418457" />
                  <Point X="-21.503716796875" Y="-0.371261230469" />
                  <Point X="-21.4952578125" Y="-0.378778137207" />
                  <Point X="-21.475435546875" Y="-0.394382629395" />
                  <Point X="-21.469962890625" Y="-0.398383117676" />
                  <Point X="-21.452994140625" Y="-0.409588531494" />
                  <Point X="-21.365994140625" Y="-0.459876434326" />
                  <Point X="-21.36050390625" Y="-0.462814727783" />
                  <Point X="-21.340845703125" Y="-0.47201461792" />
                  <Point X="-21.316974609375" Y="-0.481026977539" />
                  <Point X="-21.3080078125" Y="-0.48391293335" />
                  <Point X="-20.215119140625" Y="-0.776751403809" />
                  <Point X="-20.238380859375" Y="-0.931038208008" />
                  <Point X="-20.2414140625" Y="-0.94432824707" />
                  <Point X="-20.2721953125" Y="-1.079219726563" />
                  <Point X="-21.47342578125" Y="-0.921074829102" />
                  <Point X="-21.563216796875" Y="-0.909253540039" />
                  <Point X="-21.574583984375" Y="-0.908446533203" />
                  <Point X="-21.59733203125" Y="-0.908199035645" />
                  <Point X="-21.608712890625" Y="-0.908758605957" />
                  <Point X="-21.636669921875" Y="-0.911821899414" />
                  <Point X="-21.656330078125" Y="-0.915026489258" />
                  <Point X="-21.82708203125" Y="-0.952139709473" />
                  <Point X="-21.838529296875" Y="-0.955390991211" />
                  <Point X="-21.8688984375" Y="-0.966112365723" />
                  <Point X="-21.889119140625" Y="-0.976088195801" />
                  <Point X="-21.92035546875" Y="-0.996431884766" />
                  <Point X="-21.934966796875" Y="-1.008151672363" />
                  <Point X="-21.954001953125" Y="-1.026786132813" />
                  <Point X="-21.967181640625" Y="-1.041082763672" />
                  <Point X="-22.070390625" Y="-1.165210205078" />
                  <Point X="-22.0768125" Y="-1.173893432617" />
                  <Point X="-22.09283984375" Y="-1.198361816406" />
                  <Point X="-22.10220703125" Y="-1.216751220703" />
                  <Point X="-22.11432421875" Y="-1.248726196289" />
                  <Point X="-22.118931640625" Y="-1.265263916016" />
                  <Point X="-22.12346484375" Y="-1.289996582031" />
                  <Point X="-22.12577734375" Y="-1.306842041016" />
                  <Point X="-22.140568359375" Y="-1.467592651367" />
                  <Point X="-22.140916015625" Y="-1.479482910156" />
                  <Point X="-22.1398359375" Y="-1.511671630859" />
                  <Point X="-22.136369140625" Y="-1.534107299805" />
                  <Point X="-22.12623828125" Y="-1.570278686523" />
                  <Point X="-22.1193671875" Y="-1.587855834961" />
                  <Point X="-22.106873046875" Y="-1.612327148438" />
                  <Point X="-22.097474609375" Y="-1.628679199219" />
                  <Point X="-22.002978515625" Y="-1.775661865234" />
                  <Point X="-21.9982578125" Y="-1.782352539063" />
                  <Point X="-21.980201171875" Y="-1.804454956055" />
                  <Point X="-21.956505859375" Y="-1.828123046875" />
                  <Point X="-21.947201171875" Y="-1.836277832031" />
                  <Point X="-20.912826171875" Y="-2.629981689453" />
                  <Point X="-20.954498046875" Y="-2.697409423828" />
                  <Point X="-20.9607890625" Y="-2.706349365234" />
                  <Point X="-20.998724609375" Y="-2.760251220703" />
                  <Point X="-22.071572265625" Y="-2.140842041016" />
                  <Point X="-22.151544921875" Y="-2.094670410156" />
                  <Point X="-22.16204296875" Y="-2.089445556641" />
                  <Point X="-22.1836015625" Y="-2.080328369141" />
                  <Point X="-22.194662109375" Y="-2.076436035156" />
                  <Point X="-22.22364453125" Y="-2.068209472656" />
                  <Point X="-22.2417578125" Y="-2.064013671875" />
                  <Point X="-22.444978515625" Y="-2.027312011719" />
                  <Point X="-22.456798828125" Y="-2.025934692383" />
                  <Point X="-22.4889609375" Y="-2.024217285156" />
                  <Point X="-22.5117734375" Y="-2.025754638672" />
                  <Point X="-22.549001953125" Y="-2.032834106445" />
                  <Point X="-22.5672578125" Y="-2.038249023438" />
                  <Point X="-22.59362109375" Y="-2.049045898438" />
                  <Point X="-22.6101015625" Y="-2.056735839844" />
                  <Point X="-22.778927734375" Y="-2.145588134766" />
                  <Point X="-22.788185546875" Y="-2.151153564453" />
                  <Point X="-22.812357421875" Y="-2.167627197266" />
                  <Point X="-22.828248046875" Y="-2.181247070313" />
                  <Point X="-22.851994140625" Y="-2.206643554688" />
                  <Point X="-22.8626953125" Y="-2.2204375" />
                  <Point X="-22.877212890625" Y="-2.243197265625" />
                  <Point X="-22.885162109375" Y="-2.256884765625" />
                  <Point X="-22.974015625" Y="-2.425711669922" />
                  <Point X="-22.97888671875" Y="-2.436569091797" />
                  <Point X="-22.99020703125" Y="-2.466724853516" />
                  <Point X="-22.995626953125" Y="-2.489094726562" />
                  <Point X="-23.000056640625" Y="-2.527031982422" />
                  <Point X="-23.000345703125" Y="-2.546225585938" />
                  <Point X="-22.99780859375" Y="-2.575595947266" />
                  <Point X="-22.99548828125" Y="-2.593012939453" />
                  <Point X="-22.95878515625" Y="-2.796234130859" />
                  <Point X="-22.95698046875" Y="-2.80423046875" />
                  <Point X="-22.94876171875" Y="-2.831540771484" />
                  <Point X="-22.935927734375" Y="-2.862479248047" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.264103515625" Y="-4.027722900391" />
                  <Point X="-22.2762421875" Y="-4.036082519531" />
                  <Point X="-23.105056640625" Y="-2.955952880859" />
                  <Point X="-23.16608203125" Y="-2.876422851562" />
                  <Point X="-23.17398828125" Y="-2.867368164062" />
                  <Point X="-23.190876953125" Y="-2.850334228516" />
                  <Point X="-23.199859375" Y="-2.842354980469" />
                  <Point X="-23.225181640625" Y="-2.822574707031" />
                  <Point X="-23.239392578125" Y="-2.812486816406" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.450216796875" Y="-2.677832519531" />
                  <Point X="-23.4792734375" Y="-2.663938476562" />
                  <Point X="-23.5012109375" Y="-2.656572998047" />
                  <Point X="-23.538873046875" Y="-2.648866210938" />
                  <Point X="-23.55809765625" Y="-2.646937744141" />
                  <Point X="-23.588435546875" Y="-2.646994873047" />
                  <Point X="-23.605486328125" Y="-2.647793701172" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.835373046875" Y="-2.669564941406" />
                  <Point X="-23.864005859375" Y="-2.675533935547" />
                  <Point X="-23.884130859375" Y="-2.682140869141" />
                  <Point X="-23.916208984375" Y="-2.696811279297" />
                  <Point X="-23.931560546875" Y="-2.705648681641" />
                  <Point X="-23.955111328125" Y="-2.72230859375" />
                  <Point X="-23.966857421875" Y="-2.731325195312" />
                  <Point X="-24.136123046875" Y="-2.872063232422" />
                  <Point X="-24.144779296875" Y="-2.880229980469" />
                  <Point X="-24.16677734375" Y="-2.9037578125" />
                  <Point X="-24.18048046875" Y="-2.922593017578" />
                  <Point X="-24.1992578125" Y="-2.956478515625" />
                  <Point X="-24.206900390625" Y="-2.974389648438" />
                  <Point X="-24.216224609375" Y="-3.004461181641" />
                  <Point X="-24.220412109375" Y="-3.020376220703" />
                  <Point X="-24.271021484375" Y="-3.253218994141" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323169433594" />
                  <Point X="-24.2744453125" Y="-3.335519775391" />
                  <Point X="-24.16691015625" Y="-4.152330078125" />
                  <Point X="-24.321076171875" Y="-3.576971435547" />
                  <Point X="-24.344931640625" Y="-3.487937255859" />
                  <Point X="-24.348869140625" Y="-3.476311035156" />
                  <Point X="-24.35820703125" Y="-3.453664550781" />
                  <Point X="-24.363607421875" Y="-3.442644042969" />
                  <Point X="-24.380580078125" Y="-3.413026367188" />
                  <Point X="-24.389341796875" Y="-3.399161132812" />
                  <Point X="-24.543318359375" Y="-3.177309814453" />
                  <Point X="-24.550703125" Y="-3.167977294922" />
                  <Point X="-24.57223046875" Y="-3.144022460938" />
                  <Point X="-24.589880859375" Y="-3.128687255859" />
                  <Point X="-24.622181640625" Y="-3.106967529297" />
                  <Point X="-24.63945703125" Y="-3.097779296875" />
                  <Point X="-24.669283203125" Y="-3.085670898438" />
                  <Point X="-24.684431640625" Y="-3.080255371094" />
                  <Point X="-24.922703125" Y="-3.006305175781" />
                  <Point X="-24.933185546875" Y="-3.0036953125" />
                  <Point X="-24.961923828125" Y="-2.998252441406" />
                  <Point X="-24.983267578125" Y="-2.996663818359" />
                  <Point X="-25.018953125" Y="-2.998042236328" />
                  <Point X="-25.03673046875" Y="-3.000423339844" />
                  <Point X="-25.066638671875" Y="-3.007353271484" />
                  <Point X="-25.080072265625" Y="-3.010988037109" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.32946484375" Y="-3.089170654297" />
                  <Point X="-25.358787109375" Y="-3.102486572266" />
                  <Point X="-25.378845703125" Y="-3.114798339844" />
                  <Point X="-25.409484375" Y="-3.139305664062" />
                  <Point X="-25.42352734375" Y="-3.153159423828" />
                  <Point X="-25.444794921875" Y="-3.179026855469" />
                  <Point X="-25.454119140625" Y="-3.191358886719" />
                  <Point X="-25.608095703125" Y="-3.413210205078" />
                  <Point X="-25.61246875" Y="-3.420130371094" />
                  <Point X="-25.625974609375" Y="-3.445260498047" />
                  <Point X="-25.63877734375" Y="-3.476213378906" />
                  <Point X="-25.64275390625" Y="-3.487936279297" />
                  <Point X="-25.98542578125" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.241863613751" Y="0.707025661169" />
                  <Point X="-20.392162426384" Y="1.098567454397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.909974503589" Y="2.447514034288" />
                  <Point X="-21.013237886083" Y="2.716524342808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.334132011536" Y="0.682302384841" />
                  <Point X="-20.489026024644" Y="1.085815084574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.988580064486" Y="2.387197850992" />
                  <Point X="-21.125190205517" Y="2.743079435518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.426400409321" Y="0.657579108513" />
                  <Point X="-20.585889622903" Y="1.073062714752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.067185625384" Y="2.326881667697" />
                  <Point X="-21.20848815473" Y="2.694987341709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.518668807106" Y="0.632855832184" />
                  <Point X="-20.682753221162" Y="1.06031034493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.145791186282" Y="2.266565484402" />
                  <Point X="-21.291786103942" Y="2.646895247899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.61093720489" Y="0.608132555856" />
                  <Point X="-20.779616819422" Y="1.047557975108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.22439674718" Y="2.206249301106" />
                  <Point X="-21.375084053155" Y="2.59880315409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.703205602675" Y="0.583409279528" />
                  <Point X="-20.876480417681" Y="1.034805605286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.303002308078" Y="2.145933117811" />
                  <Point X="-21.458382002367" Y="2.550711060281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.234218913764" Y="-0.903433485656" />
                  <Point X="-20.290612559986" Y="-0.756523014564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.79547400046" Y="0.5586860032" />
                  <Point X="-20.97334401594" Y="1.022053235463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.381607868975" Y="2.085616934515" />
                  <Point X="-21.54167995158" Y="2.502618966471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.270817469526" Y="-1.073181658672" />
                  <Point X="-20.404037800928" Y="-0.726130830142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.887742398245" Y="0.533962726871" />
                  <Point X="-21.070207614199" Y="1.009300865641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.460213429873" Y="2.02530075122" />
                  <Point X="-21.624977900793" Y="2.454526872662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.375478005972" Y="-1.065622310085" />
                  <Point X="-20.517463041869" Y="-0.695738645719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.98001079603" Y="0.509239450543" />
                  <Point X="-21.167071212459" Y="0.996548495819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.538818990771" Y="1.964984567925" />
                  <Point X="-21.708275850005" Y="2.406434778853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.242354405582" Y="3.797756983673" />
                  <Point X="-22.319930148715" Y="3.999848703795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.482653045575" Y="-1.051512456823" />
                  <Point X="-20.630888282811" Y="-0.665346461297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.072279193815" Y="0.484516174215" />
                  <Point X="-21.263934810718" Y="0.983796125997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.617424551669" Y="1.904668384629" />
                  <Point X="-21.791573799218" Y="2.358342685043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.303475417303" Y="3.691891992516" />
                  <Point X="-22.45422048273" Y="4.084596314017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.589828085177" Y="-1.037402603561" />
                  <Point X="-20.744313523752" Y="-0.634954276875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.1645475916" Y="0.459792897886" />
                  <Point X="-21.360798408977" Y="0.971043756174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.696030112567" Y="1.844352201334" />
                  <Point X="-21.87487174843" Y="2.310250591234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.364596429024" Y="3.586027001359" />
                  <Point X="-22.585292223443" Y="4.160959202025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.69700312478" Y="-1.023292750298" />
                  <Point X="-20.857738764694" Y="-0.604562092452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.256815998764" Y="0.435069645993" />
                  <Point X="-21.457662007237" Y="0.958291386352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.774635688327" Y="1.784036056757" />
                  <Point X="-21.958169697643" Y="2.262158497425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.425717440745" Y="3.480162010203" />
                  <Point X="-22.716363964156" Y="4.237322090033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.804178164382" Y="-1.009182897036" />
                  <Point X="-20.971164005635" Y="-0.57416990803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.34742859189" Y="0.406032851055" />
                  <Point X="-21.557126385004" Y="0.952314278785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.85209783637" Y="1.720741181136" />
                  <Point X="-22.041467646855" Y="2.214066403615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.486838452466" Y="3.374297019046" />
                  <Point X="-22.842171598693" Y="4.299971512604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.911353203985" Y="-0.995073043774" />
                  <Point X="-21.084589246576" Y="-0.543777723608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.431341969977" Y="0.359544004277" />
                  <Point X="-21.664608860678" Y="0.967225030395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.920897570465" Y="1.634879945666" />
                  <Point X="-22.124765596068" Y="2.165974309806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.547959464187" Y="3.268432027889" />
                  <Point X="-22.966195071711" Y="4.357973035516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.018528243587" Y="-0.980963190511" />
                  <Point X="-21.198014487518" Y="-0.513385539185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.510816485413" Y="0.301491524945" />
                  <Point X="-21.786049834348" Y="1.018498912495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.977194649319" Y="1.516448179748" />
                  <Point X="-22.208063545281" Y="2.117882215997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.609080475908" Y="3.162567036732" />
                  <Point X="-23.09021854473" Y="4.415974558428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.12570328319" Y="-0.966853337249" />
                  <Point X="-21.311520745172" Y="-0.482782299011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.580083068741" Y="0.216846473308" />
                  <Point X="-22.291361494493" Y="2.069790122187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.670201487629" Y="3.056702045575" />
                  <Point X="-23.210848691898" Y="4.465136165274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.232878322793" Y="-0.952743483987" />
                  <Point X="-21.438063033451" Y="-0.418219038009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.63341129705" Y="0.0906805873" />
                  <Point X="-22.374930745021" Y="2.022404792467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.73132249935" Y="2.950837054418" />
                  <Point X="-23.329067082998" Y="4.508014932759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.340053362395" Y="-0.938633630724" />
                  <Point X="-22.465141054666" Y="1.992320013231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.792443511071" Y="2.844972063261" />
                  <Point X="-23.447285474098" Y="4.550893700244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.447228401998" Y="-0.924523777462" />
                  <Point X="-22.560322130024" Y="1.975184521399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.853564522792" Y="2.739107072104" />
                  <Point X="-23.564523596174" Y="4.591218779615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.554403472452" Y="-0.910413843828" />
                  <Point X="-22.665888730558" Y="1.985104247632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.914685581779" Y="2.633242204079" />
                  <Point X="-23.676676348979" Y="4.618296019109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.964657337992" Y="-2.711845720069" />
                  <Point X="-21.030844079584" Y="-2.53942336332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.654505764605" Y="-0.914729127604" />
                  <Point X="-22.787573146941" Y="2.03701231968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.967806877151" Y="2.506537239339" />
                  <Point X="-23.788829101785" Y="4.645373258604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.061817997616" Y="-2.723824218578" />
                  <Point X="-21.175090538015" Y="-2.428739162256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.748463389346" Y="-0.935051817261" />
                  <Point X="-23.90098185459" Y="4.672450498098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.192550174334" Y="-2.648345925022" />
                  <Point X="-21.319336996446" Y="-2.318054961192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.841950987406" Y="-0.956598968285" />
                  <Point X="-24.013134607395" Y="4.699527737592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.323282351052" Y="-2.572867631466" />
                  <Point X="-21.463583454876" Y="-2.207370760127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.926520984996" Y="-1.001377262777" />
                  <Point X="-24.125287360201" Y="4.726604977086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.454014527769" Y="-2.49738933791" />
                  <Point X="-21.607829913307" Y="-2.096686559063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.9985544786" Y="-1.07881426671" />
                  <Point X="-24.233868685937" Y="4.744378330976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.584746704487" Y="-2.421911044354" />
                  <Point X="-21.752076371738" Y="-1.986002357999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.06817283659" Y="-1.162542914023" />
                  <Point X="-24.339889647459" Y="4.755481708053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.715478881204" Y="-2.346432750798" />
                  <Point X="-21.896322830169" Y="-1.875318156934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.122670449014" Y="-1.28566245026" />
                  <Point X="-24.445910608982" Y="4.766585085129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.846211057922" Y="-2.270954457241" />
                  <Point X="-22.08576723902" Y="-1.646889269482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.140043170297" Y="-1.505495634435" />
                  <Point X="-24.551931570504" Y="4.777688462206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.976943234639" Y="-2.195476163685" />
                  <Point X="-24.636068273544" Y="4.73178139682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.10767527202" Y="-2.119998233115" />
                  <Point X="-24.677899640163" Y="4.575665162146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.229867203512" Y="-2.066768039004" />
                  <Point X="-24.719731006782" Y="4.419548927472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.339456311582" Y="-2.046369322377" />
                  <Point X="-24.761562373401" Y="4.263432692798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.448696815658" Y="-2.026878750201" />
                  <Point X="-24.822892965982" Y="4.158113678445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.548226172297" Y="-2.03268658202" />
                  <Point X="-24.903789201048" Y="4.103764905378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.635601635282" Y="-2.070156389289" />
                  <Point X="-24.998388282118" Y="4.085113266589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.720257717911" Y="-2.114710424586" />
                  <Point X="-25.112517898322" Y="4.117340411305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.359101947506" Y="4.759713821361" />
                  <Point X="-25.367756643224" Y="4.782260074536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.803912843408" Y="-2.161872042363" />
                  <Point X="-25.465140387911" Y="4.770862732483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.875490388498" Y="-2.240496832786" />
                  <Point X="-25.562524132597" Y="4.75946539043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.299746838295" Y="-4.005450729901" />
                  <Point X="-22.344949709347" Y="-3.887693224831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.934773587174" Y="-2.35114949061" />
                  <Point X="-25.659907877283" Y="4.748068048377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.503370371139" Y="-3.740083961589" />
                  <Point X="-22.648591067156" Y="-3.361771114428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.990965486586" Y="-2.46985525834" />
                  <Point X="-25.75729162197" Y="4.736670706324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.706993903983" Y="-3.474717193277" />
                  <Point X="-25.854675366656" Y="4.725273364271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.910617436827" Y="-3.209350424966" />
                  <Point X="-25.950877296179" Y="4.710797288459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.114241012159" Y="-2.943983545969" />
                  <Point X="-26.042748658964" Y="4.685039700595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.275350697547" Y="-2.789369136763" />
                  <Point X="-26.13462002175" Y="4.659282112732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.410450542994" Y="-2.702512677161" />
                  <Point X="-26.226491384535" Y="4.633524524868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.532284756199" Y="-2.650214371051" />
                  <Point X="-26.31836274732" Y="4.607766937004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.633966702701" Y="-2.650414514556" />
                  <Point X="-26.410234110106" Y="4.58200934914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.732253606888" Y="-2.659459045671" />
                  <Point X="-26.466058602587" Y="4.462346453631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.830417935219" Y="-2.668822897807" />
                  <Point X="-26.494312182922" Y="4.270858876384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.920487514581" Y="-2.699274291962" />
                  <Point X="-26.551303960592" Y="4.154236862756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.999518658925" Y="-2.758481792477" />
                  <Point X="-26.629852076793" Y="4.093771030909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.076657225742" Y="-2.822619626011" />
                  <Point X="-26.717326718022" Y="4.056559591799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.152947805217" Y="-2.888966542096" />
                  <Point X="-26.809096985329" Y="4.03053864121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.213609654553" Y="-2.99602769216" />
                  <Point X="-26.916099538795" Y="4.044199152725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.251362024413" Y="-3.162770076686" />
                  <Point X="-27.041995310987" Y="4.107078181738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.267991870286" Y="-3.38453851747" />
                  <Point X="-27.203334381709" Y="4.262290160171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.214874536756" Y="-3.788004572608" />
                  <Point X="-27.28720609906" Y="4.215692783463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.557574656802" Y="-3.160330907823" />
                  <Point X="-27.37107781641" Y="4.169095406755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.690834345392" Y="-3.078268220725" />
                  <Point X="-27.45494953376" Y="4.122498030047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.806355955023" Y="-3.042414809153" />
                  <Point X="-27.538821251111" Y="4.075900653339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.921877564655" Y="-3.006561397582" />
                  <Point X="-27.622692968461" Y="4.029303276631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.026517611495" Y="-2.999055426242" />
                  <Point X="-27.70292139785" Y="3.973214810294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.119051966912" Y="-3.023085859253" />
                  <Point X="-27.781541755204" Y="3.912937173086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.209978089486" Y="-3.051305882055" />
                  <Point X="-27.860162112559" Y="3.852659535879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.30090421206" Y="-3.079525904857" />
                  <Point X="-27.938782469913" Y="3.792381898671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.386708826871" Y="-3.121087911526" />
                  <Point X="-27.748509970723" Y="3.031614421304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.897238685108" Y="3.419065968754" />
                  <Point X="-28.017402827268" Y="3.732104261464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.458867112088" Y="-3.198199822194" />
                  <Point X="-27.793653939204" Y="2.884127809517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.524387906941" Y="-3.292602986428" />
                  <Point X="-27.865570302031" Y="2.806385669475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.589908701793" Y="-3.387006150663" />
                  <Point X="-27.944677835056" Y="2.747377168278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.646934666901" Y="-3.50353910297" />
                  <Point X="-28.037412378677" Y="2.723868243372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.68876592684" Y="-3.659655615557" />
                  <Point X="-28.141514080638" Y="2.729971778352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.730597186778" Y="-3.815772128144" />
                  <Point X="-28.256165741616" Y="2.7635588962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.772428446716" Y="-3.971888640731" />
                  <Point X="-28.386898170819" Y="2.839037847502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.814259706654" Y="-4.128005153318" />
                  <Point X="-28.517630277006" Y="2.91451595732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.856090966593" Y="-4.284121665905" />
                  <Point X="-28.648362383193" Y="2.989994067137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.897922226531" Y="-4.440238178492" />
                  <Point X="-28.753835177785" Y="2.999669420539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.939753486469" Y="-4.596354691078" />
                  <Point X="-28.821974109214" Y="2.912086735269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.981584746407" Y="-4.752471203665" />
                  <Point X="-26.170602251218" Y="-4.260063768849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.237594288505" Y="-4.085543545089" />
                  <Point X="-28.890113040642" Y="2.824504049998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.086003781646" Y="-4.745540987234" />
                  <Point X="-26.128530588442" Y="-4.634754867892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.394022184929" Y="-3.943125613117" />
                  <Point X="-28.448203862507" Y="1.408200612037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.604925821932" Y="1.816475274731" />
                  <Point X="-28.95825197207" Y="2.736921364727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.541402021614" Y="-3.824278682626" />
                  <Point X="-28.508942854214" Y="1.301340424718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.749172354963" Y="1.927159670137" />
                  <Point X="-29.024001526569" Y="2.643114139746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.654379860774" Y="-3.795052019693" />
                  <Point X="-28.588746656066" Y="1.244145765828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.893418887995" Y="2.037844065543" />
                  <Point X="-29.085389020998" Y="2.537943359778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.75877818458" Y="-3.788175758387" />
                  <Point X="-28.678805173511" Y="1.213665554394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.037665421026" Y="2.148528460948" />
                  <Point X="-29.146776515428" Y="2.43277257981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.863163246775" Y="-3.781334044761" />
                  <Point X="-27.311469543759" Y="-2.613456212853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.380539637014" Y="-2.433522468219" />
                  <Point X="-28.296035359144" Y="-0.048574573723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.381226657576" Y="0.173356346229" />
                  <Point X="-28.777452032307" Y="1.205558737095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.181911954057" Y="2.259212856354" />
                  <Point X="-29.208164009857" Y="2.327601799843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.966838017045" Y="-3.776342704859" />
                  <Point X="-27.359064238024" Y="-2.7545584657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.512679813745" Y="-2.354376209224" />
                  <Point X="-28.343528727777" Y="-0.189940788866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.512712018442" Y="0.250796751575" />
                  <Point X="-28.884613512636" Y="1.219633267242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.060269021776" Y="-3.798037286546" />
                  <Point X="-27.420185540285" Y="-2.860422699975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.614475085174" Y="-2.354281131202" />
                  <Point X="-27.962984762065" Y="-1.446382382992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.08178919967" Y="-1.13688624175" />
                  <Point X="-28.416276088128" Y="-0.265518106346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.626137283459" Y="0.281188998715" />
                  <Point X="-28.991788545503" Y="1.233743102959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.143004114143" Y="-3.847595672569" />
                  <Point X="-27.481306567256" Y="-2.966287651405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.703128926368" Y="-2.388420649379" />
                  <Point X="-28.024764700644" Y="-1.550530810997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.197505712617" Y="-1.100525089681" />
                  <Point X="-28.501018428063" Y="-0.309847433678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.739562548476" Y="0.311581245856" />
                  <Point X="-29.098963578371" Y="1.247852938675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.223990504454" Y="-3.901709583196" />
                  <Point X="-27.542427594226" Y="-3.072152602836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.786426875962" Y="-2.436512742193" />
                  <Point X="-28.101760674544" Y="-1.615040111778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.296741890597" Y="-1.10709667802" />
                  <Point X="-28.593155885782" Y="-0.334911820541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.852987813492" Y="0.341973492996" />
                  <Point X="-29.206138611238" Y="1.261962774392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.304976894765" Y="-3.955823493822" />
                  <Point X="-27.603548621197" Y="-3.178017554267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.869724825557" Y="-2.484604835007" />
                  <Point X="-28.180366152532" Y="-1.675356511062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.393605502522" Y="-1.11984901224" />
                  <Point X="-28.685424282448" Y="-0.359635099782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.966413078509" Y="0.372365740136" />
                  <Point X="-29.313313644106" Y="1.276072610109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.384361847288" Y="-4.014109292517" />
                  <Point X="-27.664669648167" Y="-3.283882505698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.953022775151" Y="-2.532696927821" />
                  <Point X="-28.258971744195" Y="-1.735672614211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.490469114448" Y="-1.13260134646" />
                  <Point X="-28.777692678688" Y="-0.384358380135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.079838343525" Y="0.402757987277" />
                  <Point X="-29.420488676973" Y="1.290182445826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.453853424073" Y="-4.09816821616" />
                  <Point X="-27.725790675138" Y="-3.389747457129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.036320724746" Y="-2.580789020635" />
                  <Point X="-28.337577335858" Y="-1.79598871736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.587332726373" Y="-1.145353680681" />
                  <Point X="-28.869961074928" Y="-0.409081660488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.193263608542" Y="0.433150234417" />
                  <Point X="-29.527663709841" Y="1.304292281542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.539569407678" Y="-4.139961115016" />
                  <Point X="-27.786911702108" Y="-3.495612408559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.119618674341" Y="-2.628881113449" />
                  <Point X="-28.416182927522" Y="-1.856304820508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.684196338299" Y="-1.158106014901" />
                  <Point X="-28.962229471168" Y="-0.433804940841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.306688873559" Y="0.463542481558" />
                  <Point X="-29.634078161335" Y="1.31642073504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.673055014913" Y="-4.057309889728" />
                  <Point X="-27.848032729079" Y="-3.60147735999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.202916623935" Y="-2.676973206263" />
                  <Point X="-28.494788519185" Y="-1.916620923657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.781059950224" Y="-1.170858349121" />
                  <Point X="-29.054497867408" Y="-0.458528221194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.420114138575" Y="0.493934728698" />
                  <Point X="-29.676186946729" Y="1.161027200985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.811384295679" Y="-3.962040463492" />
                  <Point X="-27.909153756049" Y="-3.707342311421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.28621457353" Y="-2.725065299077" />
                  <Point X="-28.573394110848" Y="-1.976937026806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.87792356215" Y="-1.183610683342" />
                  <Point X="-29.146766263648" Y="-0.483251501547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.533539403592" Y="0.524326975838" />
                  <Point X="-29.718295732123" Y="1.005633666929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.9558383221" Y="-3.850815529326" />
                  <Point X="-27.97027478302" Y="-3.813207262852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.369512523125" Y="-2.773157391891" />
                  <Point X="-28.651999702511" Y="-2.037253129955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.974787174076" Y="-1.196363017562" />
                  <Point X="-29.239034659888" Y="-0.5079747819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.646964668609" Y="0.554719222979" />
                  <Point X="-29.750877522622" Y="0.82542146265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.452810472719" Y="-2.821249484705" />
                  <Point X="-28.730605294174" Y="-2.097569233104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.071650786001" Y="-1.209115351783" />
                  <Point X="-29.331303056128" Y="-0.532698062253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.760389933625" Y="0.585111470119" />
                  <Point X="-29.779189742881" Y="0.634086647631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.536108422314" Y="-2.869341577519" />
                  <Point X="-28.809210885837" Y="-2.157885336253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.168514397927" Y="-1.221867686003" />
                  <Point X="-29.423571452368" Y="-0.557421342606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.619406371908" Y="-2.917433670333" />
                  <Point X="-28.887816477501" Y="-2.218201439402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.265378009852" Y="-1.234620020223" />
                  <Point X="-29.515839848608" Y="-0.582144622959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.702704321503" Y="-2.965525763148" />
                  <Point X="-28.966422069164" Y="-2.278517542551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.362241621778" Y="-1.247372354444" />
                  <Point X="-29.608108244848" Y="-0.606867903311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.786002271098" Y="-3.013617855962" />
                  <Point X="-29.045027660827" Y="-2.3388336457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.459105233703" Y="-1.260124688664" />
                  <Point X="-29.700376641088" Y="-0.631591183664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.980915956582" Y="-2.770941015761" />
                  <Point X="-29.12363325249" Y="-2.399149748849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.555968845629" Y="-1.272877022885" />
                  <Point X="-29.782391739839" Y="-0.683025217181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.652832457555" Y="-1.285629357105" />
                  <Point X="-29.677816549961" Y="-1.220543571187" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.504603515625" Y="-3.626146972656" />
                  <Point X="-24.528458984375" Y="-3.537112792969" />
                  <Point X="-24.545431640625" Y="-3.507495117188" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.71092578125" Y="-3.273825195312" />
                  <Point X="-24.740751953125" Y="-3.261716796875" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-24.99384375" Y="-3.18551953125" />
                  <Point X="-25.023751953125" Y="-3.192449462891" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.27676171875" Y="-3.273825439453" />
                  <Point X="-25.298029296875" Y="-3.299692871094" />
                  <Point X="-25.452005859375" Y="-3.521544189453" />
                  <Point X="-25.4592265625" Y="-3.537111816406" />
                  <Point X="-25.835951171875" Y="-4.943067382812" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.116396484375" Y="-4.933909667969" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.312021484375" Y="-4.572854980469" />
                  <Point X="-26.3091484375" Y="-4.551041503906" />
                  <Point X="-26.313287109375" Y="-4.516635253906" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.37594921875" Y="-4.215220703125" />
                  <Point X="-26.400173828125" Y="-4.190445800781" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.633375" Y="-3.989461914063" />
                  <Point X="-26.667677734375" Y="-3.984554443359" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.975041015625" Y="-3.967068115234" />
                  <Point X="-27.0052421875" Y="-3.984056396484" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.25973046875" Y="-4.157293457031" />
                  <Point X="-27.45709375" Y="-4.414500976563" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-27.87819921875" Y="-4.150389648438" />
                  <Point X="-28.228580078125" Y="-3.880608154297" />
                  <Point X="-27.550369140625" Y="-2.705910400391" />
                  <Point X="-27.506033203125" Y="-2.629120117188" />
                  <Point X="-27.49976171875" Y="-2.597594238281" />
                  <Point X="-27.51501171875" Y="-2.567732177734" />
                  <Point X="-27.531326171875" Y="-2.551416748047" />
                  <Point X="-27.560154296875" Y="-2.537198730469" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.804291015625" Y="-3.243569824219" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-29.16169921875" Y="-2.847135742188" />
                  <Point X="-29.177736328125" Y="-2.820242675781" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-28.241357421875" Y="-1.482667480469" />
                  <Point X="-28.163787109375" Y="-1.42314453125" />
                  <Point X="-28.14536328125" Y="-1.394242553711" />
                  <Point X="-28.1381171875" Y="-1.366266845703" />
                  <Point X="-28.140322265625" Y="-1.334601928711" />
                  <Point X="-28.162734375" Y="-1.309710693359" />
                  <Point X="-28.187640625" Y="-1.295052368164" />
                  <Point X="-28.219529296875" Y="-1.288571044922" />
                  <Point X="-29.750333984375" Y="-1.490105224609" />
                  <Point X="-29.803283203125" Y="-1.497076049805" />
                  <Point X="-29.927392578125" Y="-1.011186828613" />
                  <Point X="-29.931634765625" Y="-0.981531738281" />
                  <Point X="-29.99839453125" Y="-0.5147421875" />
                  <Point X="-28.645828125" Y="-0.152322845459" />
                  <Point X="-28.557462890625" Y="-0.128645339966" />
                  <Point X="-28.540236328125" Y="-0.120273399353" />
                  <Point X="-28.514142578125" Y="-0.102163726807" />
                  <Point X="-28.494345703125" Y="-0.074131278992" />
                  <Point X="-28.485646484375" Y="-0.046099601746" />
                  <Point X="-28.486197265625" Y="-0.014685463905" />
                  <Point X="-28.494896484375" Y="0.01334636879" />
                  <Point X="-28.515794921875" Y="0.040749809265" />
                  <Point X="-28.54189453125" Y="0.05886472702" />
                  <Point X="-28.557462890625" Y="0.066085227966" />
                  <Point X="-29.95286328125" Y="0.439981689453" />
                  <Point X="-29.998185546875" Y="0.45212588501" />
                  <Point X="-29.91764453125" Y="0.996414855957" />
                  <Point X="-29.909103515625" Y="1.027936035156" />
                  <Point X="-29.773515625" Y="1.528298706055" />
                  <Point X="-28.816607421875" Y="1.402319458008" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866699219" />
                  <Point X="-28.728044921875" Y="1.397020141602" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.65152734375" Y="1.426060668945" />
                  <Point X="-28.63911328125" Y="1.443798095703" />
                  <Point X="-28.637646484375" Y="1.447342163086" />
                  <Point X="-28.61447265625" Y="1.503290039063" />
                  <Point X="-28.610712890625" Y="1.524605224609" />
                  <Point X="-28.61631640625" Y="1.54551171875" />
                  <Point X="-28.618087890625" Y="1.5489140625" />
                  <Point X="-28.646056640625" Y="1.602641357422" />
                  <Point X="-28.65996875" Y="1.619221923828" />
                  <Point X="-29.460373046875" Y="2.233394042969" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.16001171875" Y="2.7870078125" />
                  <Point X="-29.137392578125" Y="2.816083740234" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.19805859375" Y="2.949404541016" />
                  <Point X="-28.15915625" Y="2.926943603516" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.13341796875" Y="2.919989013672" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927404541016" />
                  <Point X="-28.009634765625" Y="2.931020996094" />
                  <Point X="-27.95252734375" Y="2.988127929688" />
                  <Point X="-27.9408984375" Y="3.006382324219" />
                  <Point X="-27.938072265625" Y="3.027841308594" />
                  <Point X="-27.938517578125" Y="3.032936279297" />
                  <Point X="-27.945556640625" Y="3.113390869141" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.30675" Y="3.748363037109" />
                  <Point X="-28.306826171875" Y="3.749623535156" />
                  <Point X="-27.752876953125" Y="4.174331054688" />
                  <Point X="-27.71724609375" Y="4.194125976562" />
                  <Point X="-27.141548828125" Y="4.513971679688" />
                  <Point X="-26.979736328125" Y="4.303093261719" />
                  <Point X="-26.967826171875" Y="4.287572265625" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.945576171875" Y="4.270708984375" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.807900390625" Y="4.224697265625" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.69690234375" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.29448828125" />
                  <Point X="-26.68416015625" Y="4.300585449219" />
                  <Point X="-26.653802734375" Y="4.396864746094" />
                  <Point X="-26.651916015625" Y="4.418424804688" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.68521484375" Y="4.702240234375" />
                  <Point X="-25.96809375" Y="4.903296386719" />
                  <Point X="-25.92490234375" Y="4.908351074219" />
                  <Point X="-25.22419921875" Y="4.990358398438" />
                  <Point X="-25.0536484375" Y="4.353854492188" />
                  <Point X="-25.042140625" Y="4.310904296875" />
                  <Point X="-25.024283203125" Y="4.284177246094" />
                  <Point X="-24.993845703125" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.28417578125" />
                  <Point X="-24.945546875" Y="4.31090234375" />
                  <Point X="-24.763810546875" Y="4.989149902344" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.139794921875" Y="4.925565429688" />
                  <Point X="-24.10405859375" Y="4.916938476562" />
                  <Point X="-23.491544921875" Y="4.769058105469" />
                  <Point X="-23.46870703125" Y="4.760774902344" />
                  <Point X="-23.06896484375" Y="4.615785644531" />
                  <Point X="-23.04647265625" Y="4.605267089844" />
                  <Point X="-22.661298828125" Y="4.425134277344" />
                  <Point X="-22.6395625" Y="4.412470703125" />
                  <Point X="-22.26747265625" Y="4.195689941406" />
                  <Point X="-22.24698046875" Y="4.1811171875" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.718830078125" Y="2.592477050781" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.77642578125" Y="2.486733398438" />
                  <Point X="-22.7966171875" Y="2.411228515625" />
                  <Point X="-22.797453125" Y="2.388174072266" />
                  <Point X="-22.789583984375" Y="2.32290234375" />
                  <Point X="-22.78131640625" Y="2.300812011719" />
                  <Point X="-22.7787578125" Y="2.297041748047" />
                  <Point X="-22.738357421875" Y="2.237503417969" />
                  <Point X="-22.7212890625" Y="2.221645263672" />
                  <Point X="-22.66175" Y="2.18124609375" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.63552734375" Y="2.172481445312" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5465546875" Y="2.167225097656" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.0554609375" Y="3.002730957031" />
                  <Point X="-21.005751953125" Y="3.031430908203" />
                  <Point X="-20.797404296875" Y="2.741875244141" />
                  <Point X="-20.785982421875" Y="2.722999755859" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.643822265625" Y="1.644923461914" />
                  <Point X="-21.7113828125" Y="1.593082763672" />
                  <Point X="-21.7240703125" Y="1.579343383789" />
                  <Point X="-21.77841015625" Y="1.508451538086" />
                  <Point X="-21.788162109375" Y="1.486916503906" />
                  <Point X="-21.808404296875" Y="1.414535644531" />
                  <Point X="-21.809220703125" Y="1.390965087891" />
                  <Point X="-21.80816796875" Y="1.385865112305" />
                  <Point X="-21.79155078125" Y="1.305332275391" />
                  <Point X="-21.7814921875" Y="1.283604736328" />
                  <Point X="-21.736296875" Y="1.214909667969" />
                  <Point X="-21.719052734375" Y="1.198819946289" />
                  <Point X="-21.714904296875" Y="1.196485229492" />
                  <Point X="-21.64941015625" Y="1.159617553711" />
                  <Point X="-21.625826171875" Y="1.152878295898" />
                  <Point X="-21.537271484375" Y="1.141174926758" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.1909609375" Y="1.316695556641" />
                  <Point X="-20.151025390625" Y="1.321953125" />
                  <Point X="-20.060806640625" Y="0.951365966797" />
                  <Point X="-20.057208984375" Y="0.928260131836" />
                  <Point X="-20.002140625" Y="0.57455645752" />
                  <Point X="-21.18140625" Y="0.258572998047" />
                  <Point X="-21.25883203125" Y="0.237826919556" />
                  <Point X="-21.276421875" Y="0.22963470459" />
                  <Point X="-21.363421875" Y="0.179346893311" />
                  <Point X="-21.3810390625" Y="0.162714797974" />
                  <Point X="-21.433240234375" Y="0.096199226379" />
                  <Point X="-21.443013671875" Y="0.074735321045" />
                  <Point X="-21.444115234375" Y="0.068981674194" />
                  <Point X="-21.461515625" Y="-0.021875238419" />
                  <Point X="-21.4604140625" Y="-0.046438518524" />
                  <Point X="-21.443013671875" Y="-0.137295425415" />
                  <Point X="-21.433244140625" Y="-0.15875340271" />
                  <Point X="-21.429935546875" Y="-0.162971588135" />
                  <Point X="-21.377734375" Y="-0.229487014771" />
                  <Point X="-21.357912109375" Y="-0.245091552734" />
                  <Point X="-21.270912109375" Y="-0.295379364014" />
                  <Point X="-21.25883203125" Y="-0.300387023926" />
                  <Point X="-20.0361953125" Y="-0.627991638184" />
                  <Point X="-20.001931640625" Y="-0.637172668457" />
                  <Point X="-20.051568359375" Y="-0.966412353516" />
                  <Point X="-20.0561796875" Y="-0.986613769531" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-21.4982265625" Y="-1.109449462891" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.615974609375" Y="-1.10069152832" />
                  <Point X="-21.7867265625" Y="-1.13780480957" />
                  <Point X="-21.802052734375" Y="-1.143923095703" />
                  <Point X="-21.821087890625" Y="-1.162557617188" />
                  <Point X="-21.924296875" Y="-1.286685180664" />
                  <Point X="-21.932044921875" Y="-1.299518066406" />
                  <Point X="-21.936578125" Y="-1.324250732422" />
                  <Point X="-21.951369140625" Y="-1.485001220703" />
                  <Point X="-21.9501484375" Y="-1.501458374023" />
                  <Point X="-21.937654296875" Y="-1.5259296875" />
                  <Point X="-21.843158203125" Y="-1.672912353516" />
                  <Point X="-21.831537109375" Y="-1.685540893555" />
                  <Point X="-20.696923828125" Y="-2.556161132812" />
                  <Point X="-20.660923828125" Y="-2.583784179688" />
                  <Point X="-20.7958671875" Y="-2.802139648438" />
                  <Point X="-20.805408203125" Y="-2.815697509766" />
                  <Point X="-20.943310546875" Y="-3.011638183594" />
                  <Point X="-22.166572265625" Y="-2.305386962891" />
                  <Point X="-22.246544921875" Y="-2.259215332031" />
                  <Point X="-22.27552734375" Y="-2.250988769531" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.49525" Y="-2.214074462891" />
                  <Point X="-22.52161328125" Y="-2.224871582031" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7025078125" Y="-2.322614746094" />
                  <Point X="-22.717025390625" Y="-2.345374511719" />
                  <Point X="-22.80587890625" Y="-2.514201416016" />
                  <Point X="-22.811048828125" Y="-2.529873535156" />
                  <Point X="-22.80851171875" Y="-2.559243896484" />
                  <Point X="-22.77180859375" Y="-2.762465087891" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.036802734375" Y="-4.041423339844" />
                  <Point X="-22.013326171875" Y="-4.082087158203" />
                  <Point X="-22.16470703125" Y="-4.190215820312" />
                  <Point X="-22.175365234375" Y="-4.197115722656" />
                  <Point X="-22.32022265625" Y="-4.29087890625" />
                  <Point X="-23.255794921875" Y="-3.071617431641" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.342142578125" Y="-2.972307128906" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.55773828125" Y="-2.836937255859" />
                  <Point X="-23.588076171875" Y="-2.836994384766" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.821833984375" Y="-2.860761474609" />
                  <Point X="-23.845384765625" Y="-2.877421386719" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.025423828125" Y="-3.030659667969" />
                  <Point X="-24.034748046875" Y="-3.060731201172" />
                  <Point X="-24.085357421875" Y="-3.293573974609" />
                  <Point X="-24.0860703125" Y="-3.310719726562" />
                  <Point X="-23.879447265625" Y="-4.880173339844" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-24.005646484375" Y="-4.963245605469" />
                  <Point X="-24.015509765625" Y="-4.965037597656" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#122" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.011508702859" Y="4.398075539562" Z="0.15" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.15" />
                  <Point X="-0.936872690232" Y="4.989292220936" Z="0.15" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.15" />
                  <Point X="-1.704870465505" Y="4.78166399326" Z="0.15" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.15" />
                  <Point X="-1.747969421667" Y="4.749468478074" Z="0.15" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.15" />
                  <Point X="-1.738002751349" Y="4.346901142356" Z="0.15" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.15" />
                  <Point X="-1.833192735946" Y="4.30217107463" Z="0.15" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.15" />
                  <Point X="-1.928644595176" Y="4.346339371174" Z="0.15" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.15" />
                  <Point X="-1.946224711962" Y="4.36481210394" Z="0.15" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.15" />
                  <Point X="-2.747686667459" Y="4.269113413819" Z="0.15" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.15" />
                  <Point X="-3.343404395585" Y="3.820583624271" Z="0.15" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.15" />
                  <Point X="-3.356208371882" Y="3.754643037716" Z="0.15" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.15" />
                  <Point X="-2.994485763037" Y="3.059858457517" Z="0.15" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.15" />
                  <Point X="-3.051147157687" Y="2.997656410199" Z="0.15" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.15" />
                  <Point X="-3.135218008089" Y="3.001078936136" Z="0.15" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.15" />
                  <Point X="-3.179216310581" Y="3.023985576864" Z="0.15" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.15" />
                  <Point X="-4.183011954007" Y="2.878066195908" Z="0.15" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.15" />
                  <Point X="-4.527406680284" Y="2.298588571582" Z="0.15" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.15" />
                  <Point X="-4.496967274814" Y="2.225006398789" Z="0.15" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.15" />
                  <Point X="-3.668593594178" Y="1.557106749496" Z="0.15" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.15" />
                  <Point X="-3.690002024496" Y="1.497743894349" Z="0.15" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.15" />
                  <Point X="-3.74923782805" Y="1.475985973062" Z="0.15" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.15" />
                  <Point X="-3.816238884618" Y="1.483171776995" Z="0.15" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.15" />
                  <Point X="-4.963520230196" Y="1.072293295875" Z="0.15" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.15" />
                  <Point X="-5.055116201663" Y="0.481857461341" Z="0.15" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.15" />
                  <Point X="-4.971961173475" Y="0.422965458243" Z="0.15" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.15" />
                  <Point X="-3.55046068527" Y="0.030954106588" Z="0.15" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.15" />
                  <Point X="-3.54010769313" Y="0.001775156517" Z="0.15" />
                  <Point X="-3.539556741714" Y="0" Z="0.15" />
                  <Point X="-3.548256908005" Y="-0.028031794544" Z="0.15" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.15" />
                  <Point X="-3.574907909853" Y="-0.047921876546" Z="0.15" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.15" />
                  <Point X="-3.664926615297" Y="-0.072746598513" Z="0.15" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.15" />
                  <Point X="-4.987287092" Y="-0.957330728527" Z="0.15" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.15" />
                  <Point X="-4.854988431792" Y="-1.489506992252" Z="0.15" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.15" />
                  <Point X="-4.749962810529" Y="-1.508397429029" Z="0.15" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.15" />
                  <Point X="-3.194253479099" Y="-1.321521547764" Z="0.15" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.15" />
                  <Point X="-3.199921959956" Y="-1.350425562282" Z="0.15" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.15" />
                  <Point X="-3.277952523737" Y="-1.411720043219" Z="0.15" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.15" />
                  <Point X="-4.226837027934" Y="-2.814572192927" Z="0.15" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.15" />
                  <Point X="-3.882911418271" Y="-3.272766588511" Z="0.15" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.15" />
                  <Point X="-3.785448550137" Y="-3.255591123261" Z="0.15" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.15" />
                  <Point X="-2.556524164256" Y="-2.571806304876" Z="0.15" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.15" />
                  <Point X="-2.599825889907" Y="-2.649629832355" Z="0.15" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.15" />
                  <Point X="-2.914860386648" Y="-4.158726613664" Z="0.15" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.15" />
                  <Point X="-2.477283757517" Y="-4.433340229116" Z="0.15" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.15" />
                  <Point X="-2.43772407707" Y="-4.43208659527" Z="0.15" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.15" />
                  <Point X="-1.983619209206" Y="-3.99434975326" Z="0.15" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.15" />
                  <Point X="-1.67710446766" Y="-4.003167467923" Z="0.15" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.15" />
                  <Point X="-1.439297858026" Y="-4.19675763859" Z="0.15" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.15" />
                  <Point X="-1.368483254394" Y="-4.495110101238" Z="0.15" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.15" />
                  <Point X="-1.367750313795" Y="-4.535045545261" Z="0.15" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.15" />
                  <Point X="-1.135012120963" Y="-4.951052599742" Z="0.15" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.15" />
                  <Point X="-0.835518759953" Y="-5.010298071087" Z="0.15" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.15" />
                  <Point X="-0.793811413325" Y="-4.924728651172" Z="0.15" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.15" />
                  <Point X="-0.26310990206" Y="-3.296921753366" Z="0.15" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.15" />
                  <Point X="-0.015088830121" Y="-3.208922384478" Z="0.15" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.15" />
                  <Point X="0.238270249241" Y="-3.27818966081" Z="0.15" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.15" />
                  <Point X="0.407335957372" Y="-3.504724002402" Z="0.15" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.15" />
                  <Point X="0.440943447076" Y="-3.607807378644" Z="0.15" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.15" />
                  <Point X="0.987269864611" Y="-4.982952496531" Z="0.15" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.15" />
                  <Point X="1.166390058479" Y="-4.944092599902" Z="0.15" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.15" />
                  <Point X="1.163968286797" Y="-4.8423672347" Z="0.15" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.15" />
                  <Point X="1.007955103049" Y="-3.040071127358" Z="0.15" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.15" />
                  <Point X="1.180424798384" Y="-2.884587897561" Z="0.15" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.15" />
                  <Point X="1.410348949887" Y="-2.855503978055" Z="0.15" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.15" />
                  <Point X="1.62466130522" Y="-2.983085105949" Z="0.15" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.15" />
                  <Point X="1.698379597175" Y="-3.070775494288" Z="0.15" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.15" />
                  <Point X="2.845646816922" Y="-4.207809783023" Z="0.15" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.15" />
                  <Point X="3.0352424511" Y="-4.073164982596" Z="0.15" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.15" />
                  <Point X="3.000340966759" Y="-3.985143387291" Z="0.15" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.15" />
                  <Point X="2.234535298159" Y="-2.519077473294" Z="0.15" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.15" />
                  <Point X="2.3210634839" Y="-2.337381411913" Z="0.15" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.15" />
                  <Point X="2.495516966459" Y="-2.237837826067" Z="0.15" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.15" />
                  <Point X="2.709429330218" Y="-2.268912711332" Z="0.15" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.15" />
                  <Point X="2.802270209816" Y="-2.317408533515" Z="0.15" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.15" />
                  <Point X="4.229323494738" Y="-2.813194915548" Z="0.15" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.15" />
                  <Point X="4.38971032048" Y="-2.555716420361" Z="0.15" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.15" />
                  <Point X="4.327357336097" Y="-2.485213481267" Z="0.15" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.15" />
                  <Point X="3.098246623784" Y="-1.467610399045" Z="0.15" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.15" />
                  <Point X="3.107054717467" Y="-1.297552003838" Z="0.15" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.15" />
                  <Point X="3.211199707681" Y="-1.163244746868" Z="0.15" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.15" />
                  <Point X="3.388486774659" Y="-1.118270693293" Z="0.15" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.15" />
                  <Point X="3.489091522686" Y="-1.127741720224" Z="0.15" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.15" />
                  <Point X="4.986410051966" Y="-0.966457639766" Z="0.15" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.15" />
                  <Point X="5.044645683828" Y="-0.591510038319" Z="0.15" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.15" />
                  <Point X="4.970589745638" Y="-0.548415230941" Z="0.15" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.15" />
                  <Point X="3.660952157854" Y="-0.170522764267" Z="0.15" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.15" />
                  <Point X="3.603242389607" Y="-0.100822726381" Z="0.15" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.15" />
                  <Point X="3.582536615348" Y="-0.005753651719" Z="0.15" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.15" />
                  <Point X="3.598834835076" Y="0.090856879526" Z="0.15" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.15" />
                  <Point X="3.652137048793" Y="0.163126012194" Z="0.15" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.15" />
                  <Point X="3.742443256498" Y="0.217626079975" Z="0.15" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.15" />
                  <Point X="3.825378071668" Y="0.241556701828" Z="0.15" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.15" />
                  <Point X="4.986037699857" Y="0.967232133109" Z="0.15" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.15" />
                  <Point X="4.886820031753" Y="1.383874968824" Z="0.15" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.15" />
                  <Point X="4.796356313982" Y="1.397547841595" Z="0.15" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.15" />
                  <Point X="3.374568376336" Y="1.233727445843" Z="0.15" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.15" />
                  <Point X="3.303466093602" Y="1.271336324322" Z="0.15" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.15" />
                  <Point X="3.25412306934" Y="1.342366178082" Z="0.15" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.15" />
                  <Point X="3.234644310549" Y="1.42724933336" Z="0.15" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.15" />
                  <Point X="3.253834124319" Y="1.504730072772" Z="0.15" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.15" />
                  <Point X="3.309456934829" Y="1.580205752644" Z="0.15" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.15" />
                  <Point X="3.380458303065" Y="1.636535820832" Z="0.15" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.15" />
                  <Point X="4.250638612368" Y="2.780165975547" Z="0.15" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.15" />
                  <Point X="4.016311573031" Y="3.109099700803" Z="0.15" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.15" />
                  <Point X="3.913382100258" Y="3.077312237825" Z="0.15" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.15" />
                  <Point X="2.434373113763" Y="2.246807798973" Z="0.15" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.15" />
                  <Point X="2.364301394354" Y="2.253401971021" Z="0.15" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.15" />
                  <Point X="2.300628430385" Y="2.294299730617" Z="0.15" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.15" />
                  <Point X="2.256458869618" Y="2.356396429997" Z="0.15" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.15" />
                  <Point X="2.246027731738" Y="2.425457049478" Z="0.15" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.15" />
                  <Point X="2.265720143684" Y="2.505096462119" Z="0.15" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.15" />
                  <Point X="2.318313115035" Y="2.598756981697" Z="0.15" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.15" />
                  <Point X="2.775838676083" Y="4.253144432393" Z="0.15" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.15" />
                  <Point X="2.379449882441" Y="4.486953207086" Z="0.15" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.15" />
                  <Point X="1.968551955935" Y="4.681838926665" Z="0.15" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.15" />
                  <Point X="1.542185104964" Y="4.839059007699" Z="0.15" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.15" />
                  <Point X="0.901518366163" Y="4.996821896732" Z="0.15" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.15" />
                  <Point X="0.233105642505" Y="5.072148512605" Z="0.15" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.15" />
                  <Point X="0.181735858694" Y="5.033371966389" Z="0.15" />
                  <Point X="0" Y="4.355124473572" Z="0.15" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>