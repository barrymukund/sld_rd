<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#209" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3352" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004715820312" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.073763671875" Y="-4.867004394531" />
                  <Point X="-24.4366953125" Y="-3.512525878906" />
                  <Point X="-24.44227734375" Y="-3.497141357422" />
                  <Point X="-24.457634765625" Y="-3.467376464844" />
                  <Point X="-24.605974609375" Y="-3.253647705078" />
                  <Point X="-24.62136328125" Y="-3.231476074219" />
                  <Point X="-24.64325" Y="-3.209019775391" />
                  <Point X="-24.669505859375" Y="-3.189776367188" />
                  <Point X="-24.69750390625" Y="-3.175669189453" />
                  <Point X="-24.92705078125" Y="-3.104426269531" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766357422" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.266369140625" Y="-3.168278564453" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.189776611328" />
                  <Point X="-25.344439453125" Y="-3.209020507813" />
                  <Point X="-25.36632421875" Y="-3.231476806641" />
                  <Point X="-25.514662109375" Y="-3.445205322266" />
                  <Point X="-25.53005078125" Y="-3.467377197266" />
                  <Point X="-25.5381875" Y="-3.481572265625" />
                  <Point X="-25.550990234375" Y="-3.512524169922" />
                  <Point X="-25.588640625" Y="-3.653035400391" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.0554453125" Y="-4.84998828125" />
                  <Point X="-26.07933203125" Y="-4.8453515625" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.2149609375" Y="-4.563438476562" />
                  <Point X="-26.21419921875" Y="-4.547929199219" />
                  <Point X="-26.2165078125" Y="-4.51622265625" />
                  <Point X="-26.27134765625" Y="-4.240530761719" />
                  <Point X="-26.277037109375" Y="-4.211931152344" />
                  <Point X="-26.287939453125" Y="-4.182963867188" />
                  <Point X="-26.30401171875" Y="-4.155126953125" />
                  <Point X="-26.32364453125" Y="-4.131203613281" />
                  <Point X="-26.53498046875" Y="-3.945865966797" />
                  <Point X="-26.556904296875" Y="-3.926639404297" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.92351953125" Y="-3.872581787109" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.983419921875" Y="-3.873709228516" />
                  <Point X="-27.01446875" Y="-3.882029785156" />
                  <Point X="-27.042658203125" Y="-3.894802490234" />
                  <Point X="-27.27637890625" Y="-4.050969970703" />
                  <Point X="-27.300623046875" Y="-4.067170654297" />
                  <Point X="-27.312783203125" Y="-4.076821044922" />
                  <Point X="-27.335099609375" Y="-4.099459960938" />
                  <Point X="-27.35623828125" Y="-4.127006835938" />
                  <Point X="-27.4801484375" Y="-4.288489257813" />
                  <Point X="-27.766689453125" Y="-4.111068847656" />
                  <Point X="-27.801712890625" Y="-4.089383300781" />
                  <Point X="-28.104720703125" Y="-3.856076904297" />
                  <Point X="-28.098228515625" Y="-3.844830566406" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647654296875" />
                  <Point X="-27.406587890625" Y="-2.616127441406" />
                  <Point X="-27.40557421875" Y="-2.585193603516" />
                  <Point X="-27.41455859375" Y="-2.555575927734" />
                  <Point X="-27.428775390625" Y="-2.526746582031" />
                  <Point X="-27.4468046875" Y="-2.501586669922" />
                  <Point X="-27.4625234375" Y="-2.485868652344" />
                  <Point X="-27.464134765625" Y="-2.484257324219" />
                  <Point X="-27.4892578125" Y="-2.466248046875" />
                  <Point X="-27.518091796875" Y="-2.452014404297" />
                  <Point X="-27.54771875" Y="-2.443016601562" />
                  <Point X="-27.5786640625" Y="-2.4440234375" />
                  <Point X="-27.610203125" Y="-2.450292724609" />
                  <Point X="-27.639181640625" Y="-2.461197021484" />
                  <Point X="-27.76037109375" Y="-2.531165039062" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-29.055193359375" Y="-2.830207763672" />
                  <Point X="-29.082859375" Y="-2.793860107422" />
                  <Point X="-29.306142578125" Y="-2.419450439453" />
                  <Point X="-29.286052734375" Y="-2.404034912109" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.084578125" Y="-1.475593139648" />
                  <Point X="-28.06661328125" Y="-1.448461914063" />
                  <Point X="-28.053857421875" Y="-1.419832885742" />
                  <Point X="-28.046876953125" Y="-1.392881347656" />
                  <Point X="-28.046158203125" Y="-1.390107666016" />
                  <Point X="-28.04334765625" Y="-1.359676269531" />
                  <Point X="-28.045552734375" Y="-1.328001464844" />
                  <Point X="-28.052552734375" Y="-1.298251586914" />
                  <Point X="-28.068634765625" Y="-1.272263549805" />
                  <Point X="-28.08946875" Y="-1.248303466797" />
                  <Point X="-28.11297265625" Y="-1.228765869141" />
                  <Point X="-28.136966796875" Y="-1.21464453125" />
                  <Point X="-28.13944921875" Y="-1.21318347168" />
                  <Point X="-28.168705078125" Y="-1.201961547852" />
                  <Point X="-28.20059765625" Y="-1.195476318359" />
                  <Point X="-28.231927734375" Y="-1.194383666992" />
                  <Point X="-28.384916015625" Y="-1.214525146484" />
                  <Point X="-29.732099609375" Y="-1.391885498047" />
                  <Point X="-29.823255859375" Y="-1.035020629883" />
                  <Point X="-29.83407421875" Y="-0.992657043457" />
                  <Point X="-29.892423828125" Y="-0.584698791504" />
                  <Point X="-29.87718359375" Y="-0.58061517334" />
                  <Point X="-28.532875" Y="-0.220408401489" />
                  <Point X="-28.5174921875" Y="-0.214827835083" />
                  <Point X="-28.4877265625" Y="-0.19946937561" />
                  <Point X="-28.46258203125" Y="-0.182017730713" />
                  <Point X="-28.45998046875" Y="-0.180212036133" />
                  <Point X="-28.43752734375" Y="-0.15832963562" />
                  <Point X="-28.418275390625" Y="-0.132064422607" />
                  <Point X="-28.404166015625" Y="-0.104059860229" />
                  <Point X="-28.39578515625" Y="-0.077054382324" />
                  <Point X="-28.394916015625" Y="-0.074252784729" />
                  <Point X="-28.390646484375" Y="-0.046095363617" />
                  <Point X="-28.390646484375" Y="-0.016464664459" />
                  <Point X="-28.394916015625" Y="0.011692755699" />
                  <Point X="-28.403296875" Y="0.038698238373" />
                  <Point X="-28.404166015625" Y="0.041499832153" />
                  <Point X="-28.41826953125" Y="0.069496055603" />
                  <Point X="-28.437513671875" Y="0.095756698608" />
                  <Point X="-28.45997265625" Y="0.11764730072" />
                  <Point X="-28.4851171875" Y="0.135098953247" />
                  <Point X="-28.497017578125" Y="0.14213381958" />
                  <Point X="-28.5151953125" Y="0.151164596558" />
                  <Point X="-28.532875" Y="0.157848220825" />
                  <Point X="-28.672330078125" Y="0.195215377808" />
                  <Point X="-29.89181640625" Y="0.521975524902" />
                  <Point X="-29.8314609375" Y="0.929854309082" />
                  <Point X="-29.82448828125" Y="0.976967834473" />
                  <Point X="-29.70355078125" Y="1.423268066406" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228149414" />
                  <Point X="-28.70313671875" Y="1.305263549805" />
                  <Point X="-28.647484375" Y="1.322810668945" />
                  <Point X="-28.6417109375" Y="1.324630981445" />
                  <Point X="-28.622775390625" Y="1.332962402344" />
                  <Point X="-28.60403125" Y="1.343784667969" />
                  <Point X="-28.587353515625" Y="1.356014160156" />
                  <Point X="-28.573716796875" Y="1.371563354492" />
                  <Point X="-28.56130078125" Y="1.389293701172" />
                  <Point X="-28.55134765625" Y="1.407433105469" />
                  <Point X="-28.529017578125" Y="1.461344848633" />
                  <Point X="-28.526701171875" Y="1.4669375" />
                  <Point X="-28.5209140625" Y="1.486796386719" />
                  <Point X="-28.51715625" Y="1.508110717773" />
                  <Point X="-28.515802734375" Y="1.52875" />
                  <Point X="-28.51894921875" Y="1.549192871094" />
                  <Point X="-28.52455078125" Y="1.570098510742" />
                  <Point X="-28.532046875" Y="1.589375366211" />
                  <Point X="-28.558990234375" Y="1.641135498047" />
                  <Point X="-28.561802734375" Y="1.646534790039" />
                  <Point X="-28.573296875" Y="1.663730102539" />
                  <Point X="-28.587205078125" Y="1.680299316406" />
                  <Point X="-28.60213671875" Y="1.694590087891" />
                  <Point X="-28.68212890625" Y="1.755970703125" />
                  <Point X="-29.351859375" Y="2.269873291016" />
                  <Point X="-29.108244140625" Y="2.687244628906" />
                  <Point X="-29.0811484375" Y="2.733665527344" />
                  <Point X="-28.75050390625" Y="3.158661621094" />
                  <Point X="-28.206658203125" Y="2.844672119141" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.069283203125" Y="2.819015136719" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818763183594" />
                  <Point X="-28.01910546875" Y="2.821588623047" />
                  <Point X="-27.99901171875" Y="2.826505615234" />
                  <Point X="-27.980458984375" Y="2.835655273438" />
                  <Point X="-27.962205078125" Y="2.84728515625" />
                  <Point X="-27.946078125" Y="2.860229492188" />
                  <Point X="-27.891060546875" Y="2.915245605469" />
                  <Point X="-27.885353515625" Y="2.920952880859" />
                  <Point X="-27.87240625" Y="2.937083740234" />
                  <Point X="-27.86077734375" Y="2.955336669922" />
                  <Point X="-27.85162890625" Y="2.973886474609" />
                  <Point X="-27.8467109375" Y="2.993976318359" />
                  <Point X="-27.843884765625" Y="3.01543359375" />
                  <Point X="-27.84343359375" Y="3.036119140625" />
                  <Point X="-27.85021484375" Y="3.113627929688" />
                  <Point X="-27.85091796875" Y="3.121668701172" />
                  <Point X="-27.854955078125" Y="3.141958251953" />
                  <Point X="-27.86146484375" Y="3.162602539062" />
                  <Point X="-27.869794921875" Y="3.181533203125" />
                  <Point X="-27.9052421875" Y="3.242929443359" />
                  <Point X="-28.18333203125" Y="3.724595947266" />
                  <Point X="-27.747802734375" Y="4.058511962891" />
                  <Point X="-27.700623046875" Y="4.094685302734" />
                  <Point X="-27.167037109375" Y="4.391133789062" />
                  <Point X="-27.0431953125" Y="4.229740722656" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.99511328125" Y="4.18939453125" />
                  <Point X="-26.908845703125" Y="4.144486328125" />
                  <Point X="-26.899896484375" Y="4.139827636719" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.79731640625" Y="4.128692382812" />
                  <Point X="-26.777455078125" Y="4.134481445312" />
                  <Point X="-26.687603515625" Y="4.171700195312" />
                  <Point X="-26.67828125" Y="4.175561035156" />
                  <Point X="-26.66015234375" Y="4.185505859375" />
                  <Point X="-26.642419921875" Y="4.197920410156" />
                  <Point X="-26.62686328125" Y="4.211560546875" />
                  <Point X="-26.614630859375" Y="4.228245117188" />
                  <Point X="-26.60380859375" Y="4.246990722656" />
                  <Point X="-26.595478515625" Y="4.265923339844" />
                  <Point X="-26.566240234375" Y="4.358658203125" />
                  <Point X="-26.563205078125" Y="4.368280273437" />
                  <Point X="-26.559166015625" Y="4.388571289062" />
                  <Point X="-26.55727734375" Y="4.410138671875" />
                  <Point X="-26.557728515625" Y="4.430830078125" />
                  <Point X="-26.561759765625" Y="4.461440429688" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.0107109375" Y="4.792685058594" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.294712890625" Y="4.886457519531" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.85378515625" Y="4.286313964844" />
                  <Point X="-24.83562109375" Y="4.354098144531" />
                  <Point X="-24.692580078125" Y="4.8879375" />
                  <Point X="-24.209294921875" Y="4.837324707031" />
                  <Point X="-24.15595703125" Y="4.83173828125" />
                  <Point X="-23.572619140625" Y="4.690903320312" />
                  <Point X="-23.5189765625" Y="4.677952148438" />
                  <Point X="-23.1394140625" Y="4.540281738281" />
                  <Point X="-23.105349609375" Y="4.527926269531" />
                  <Point X="-22.738203125" Y="4.356225097656" />
                  <Point X="-22.705423828125" Y="4.34089453125" />
                  <Point X="-22.350720703125" Y="4.134243652344" />
                  <Point X="-22.319021484375" Y="4.115775390625" />
                  <Point X="-22.05673828125" Y="3.929253662109" />
                  <Point X="-22.071771484375" Y="3.903215087891" />
                  <Point X="-22.852416015625" Y="2.551098144531" />
                  <Point X="-22.857923828125" Y="2.539927978516" />
                  <Point X="-22.866921875" Y="2.516056152344" />
                  <Point X="-22.886375" Y="2.443315917969" />
                  <Point X="-22.88884375" Y="2.430729248047" />
                  <Point X="-22.89219921875" Y="2.404281738281" />
                  <Point X="-22.892271484375" Y="2.380950195312" />
                  <Point X="-22.88469140625" Y="2.318089111328" />
                  <Point X="-22.88390625" Y="2.311563964844" />
                  <Point X="-22.878564453125" Y="2.289627929688" />
                  <Point X="-22.870296875" Y="2.267527099609" />
                  <Point X="-22.8599296875" Y="2.247471679688" />
                  <Point X="-22.821009765625" Y="2.19011328125" />
                  <Point X="-22.812982421875" Y="2.179869384766" />
                  <Point X="-22.795638671875" Y="2.160616699219" />
                  <Point X="-22.778396484375" Y="2.145590576172" />
                  <Point X="-22.7210390625" Y="2.106670410156" />
                  <Point X="-22.715087890625" Y="2.102633056641" />
                  <Point X="-22.6950390625" Y="2.092269042969" />
                  <Point X="-22.672955078125" Y="2.084005371094" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.58813671875" Y="2.071078857422" />
                  <Point X="-22.574708984375" Y="2.070417724609" />
                  <Point X="-22.549283203125" Y="2.070968017578" />
                  <Point X="-22.526794921875" Y="2.074170654297" />
                  <Point X="-22.4540546875" Y="2.093622558594" />
                  <Point X="-22.44661328125" Y="2.095943847656" />
                  <Point X="-22.42698046875" Y="2.102963623047" />
                  <Point X="-22.41146484375" Y="2.110145263672" />
                  <Point X="-22.27119921875" Y="2.191128173828" />
                  <Point X="-21.032673828125" Y="2.906190917969" />
                  <Point X="-20.89553125" Y="2.71559375" />
                  <Point X="-20.8767265625" Y="2.689461425781" />
                  <Point X="-20.73780078125" Y="2.459883056641" />
                  <Point X="-20.741421875" Y="2.457104248047" />
                  <Point X="-21.76921484375" Y="1.668451538086" />
                  <Point X="-21.7785703125" Y="1.660245605469" />
                  <Point X="-21.796025390625" Y="1.641628051758" />
                  <Point X="-21.848376953125" Y="1.573331665039" />
                  <Point X="-21.85521875" Y="1.563094360352" />
                  <Point X="-21.8691171875" Y="1.539060302734" />
                  <Point X="-21.878369140625" Y="1.517090209961" />
                  <Point X="-21.89787109375" Y="1.447359375" />
                  <Point X="-21.89989453125" Y="1.440125732422" />
                  <Point X="-21.90334765625" Y="1.417827026367" />
                  <Point X="-21.9041640625" Y="1.394256958008" />
                  <Point X="-21.902259765625" Y="1.371769287109" />
                  <Point X="-21.88625" Y="1.294184936523" />
                  <Point X="-21.882921875" Y="1.28212890625" />
                  <Point X="-21.8740625" Y="1.256699951172" />
                  <Point X="-21.86371484375" Y="1.235740478516" />
                  <Point X="-21.820185546875" Y="1.169576293945" />
                  <Point X="-21.815669921875" Y="1.162710327148" />
                  <Point X="-21.80111328125" Y="1.145459960938" />
                  <Point X="-21.7838671875" Y="1.129365966797" />
                  <Point X="-21.76565234375" Y="1.11603527832" />
                  <Point X="-21.7025546875" Y="1.080517211914" />
                  <Point X="-21.69084765625" Y="1.074942260742" />
                  <Point X="-21.666326171875" Y="1.065259155273" />
                  <Point X="-21.643880859375" Y="1.059438476562" />
                  <Point X="-21.5585703125" Y="1.048163574219" />
                  <Point X="-21.551197265625" Y="1.047480102539" />
                  <Point X="-21.52926953125" Y="1.046307373047" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-21.378552734375" Y="1.064526367188" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.162140625" Y="0.965983276367" />
                  <Point X="-20.15405859375" Y="0.932785827637" />
                  <Point X="-20.1091328125" Y="0.644239013672" />
                  <Point X="-21.283419921875" Y="0.329589782715" />
                  <Point X="-21.29520703125" Y="0.325587219238" />
                  <Point X="-21.318453125" Y="0.315068450928" />
                  <Point X="-21.402267578125" Y="0.266621673584" />
                  <Point X="-21.41212890625" Y="0.260070404053" />
                  <Point X="-21.43513671875" Y="0.242623306274" />
                  <Point X="-21.45246875" Y="0.225576187134" />
                  <Point X="-21.5027578125" Y="0.161496047974" />
                  <Point X="-21.507974609375" Y="0.15484854126" />
                  <Point X="-21.519701171875" Y="0.135562362671" />
                  <Point X="-21.529474609375" Y="0.114095405579" />
                  <Point X="-21.536318359375" Y="0.092601127625" />
                  <Point X="-21.55308203125" Y="0.005070747852" />
                  <Point X="-21.55459375" Y="-0.006887602329" />
                  <Point X="-21.55633203125" Y="-0.03476997757" />
                  <Point X="-21.5548203125" Y="-0.058550674438" />
                  <Point X="-21.538056640625" Y="-0.1460809021" />
                  <Point X="-21.536318359375" Y="-0.15516116333" />
                  <Point X="-21.529474609375" Y="-0.176655441284" />
                  <Point X="-21.519701171875" Y="-0.198122390747" />
                  <Point X="-21.507974609375" Y="-0.217408721924" />
                  <Point X="-21.457685546875" Y="-0.281488708496" />
                  <Point X="-21.4493203125" Y="-0.290810302734" />
                  <Point X="-21.429791015625" Y="-0.309879272461" />
                  <Point X="-21.410962890625" Y="-0.324155914307" />
                  <Point X="-21.3271484375" Y="-0.372602722168" />
                  <Point X="-21.320919921875" Y="-0.375901580811" />
                  <Point X="-21.30014453125" Y="-0.385934295654" />
                  <Point X="-21.283419921875" Y="-0.392149963379" />
                  <Point X="-21.161228515625" Y="-0.424890869141" />
                  <Point X="-20.108525390625" Y="-0.706961853027" />
                  <Point X="-20.140466796875" Y="-0.918817504883" />
                  <Point X="-20.144974609375" Y="-0.948726013184" />
                  <Point X="-20.19882421875" Y="-1.18469934082" />
                  <Point X="-20.2096171875" Y="-1.183278320313" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710266113" />
                  <Point X="-21.62533984375" Y="-1.005508666992" />
                  <Point X="-21.78983984375" Y="-1.041263671875" />
                  <Point X="-21.806904296875" Y="-1.04497265625" />
                  <Point X="-21.8360234375" Y="-1.056596313477" />
                  <Point X="-21.8638515625" Y="-1.073488891602" />
                  <Point X="-21.8876015625" Y="-1.093960327148" />
                  <Point X="-21.98703125" Y="-1.21354296875" />
                  <Point X="-21.997345703125" Y="-1.225948242188" />
                  <Point X="-22.012068359375" Y="-1.250333984375" />
                  <Point X="-22.02341015625" Y="-1.277718505859" />
                  <Point X="-22.030240234375" Y="-1.305365478516" />
                  <Point X="-22.044490234375" Y="-1.460230224609" />
                  <Point X="-22.04596875" Y="-1.476295654297" />
                  <Point X="-22.043650390625" Y="-1.507564086914" />
                  <Point X="-22.03591796875" Y="-1.539188354492" />
                  <Point X="-22.023546875" Y="-1.567998901367" />
                  <Point X="-21.93251171875" Y="-1.709599975586" />
                  <Point X="-21.927830078125" Y="-1.716239624023" />
                  <Point X="-21.906765625" Y="-1.7435546875" />
                  <Point X="-21.889369140625" Y="-1.760909057617" />
                  <Point X="-21.7759765625" Y="-1.847918945312" />
                  <Point X="-20.786876953125" Y="-2.606882080078" />
                  <Point X="-20.862478515625" Y="-2.729216552734" />
                  <Point X="-20.87520703125" Y="-2.749813964844" />
                  <Point X="-20.971017578125" Y="-2.885946044922" />
                  <Point X="-20.9824375" Y="-2.879352294922" />
                  <Point X="-22.199044921875" Y="-2.176943359375" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.2457734375" Y="-2.159825195312" />
                  <Point X="-22.441552734375" Y="-2.124467529297" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.49321875" Y="-2.120395507812" />
                  <Point X="-22.525392578125" Y="-2.125354003906" />
                  <Point X="-22.555166015625" Y="-2.135177490234" />
                  <Point X="-22.7178125" Y="-2.220776611328" />
                  <Point X="-22.73468359375" Y="-2.229656494141" />
                  <Point X="-22.757611328125" Y="-2.246546386719" />
                  <Point X="-22.7785703125" Y="-2.267503417969" />
                  <Point X="-22.795466796875" Y="-2.290437011719" />
                  <Point X="-22.88106640625" Y="-2.453082519531" />
                  <Point X="-22.889947265625" Y="-2.469955078125" />
                  <Point X="-22.899771484375" Y="-2.499734130859" />
                  <Point X="-22.904728515625" Y="-2.531908935547" />
                  <Point X="-22.90432421875" Y="-2.563259033203" />
                  <Point X="-22.86896484375" Y="-2.759039550781" />
                  <Point X="-22.865296875" Y="-2.779349365234" />
                  <Point X="-22.86101171875" Y="-2.795139648438" />
                  <Point X="-22.8481796875" Y="-2.826077880859" />
                  <Point X="-22.775314453125" Y="-2.952286865234" />
                  <Point X="-22.13871484375" Y="-4.054906494141" />
                  <Point X="-22.203060546875" Y="-4.1008671875" />
                  <Point X="-22.21815234375" Y="-4.111646972656" />
                  <Point X="-22.298232421875" Y="-4.163481445312" />
                  <Point X="-22.313068359375" Y="-4.144147949219" />
                  <Point X="-23.241451171875" Y="-2.934254882812" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.47116796875" Y="-2.776416503906" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.794078125" Y="-2.760549560547" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397705078" />
                  <Point X="-23.871021484375" Y="-2.780740966797" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-24.058470703125" Y="-2.931046386719" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968859375" />
                  <Point X="-24.11275" Y="-2.996684570312" />
                  <Point X="-24.124375" Y="-3.025807617188" />
                  <Point X="-24.173130859375" Y="-3.250125" />
                  <Point X="-24.178189453125" Y="-3.273395507812" />
                  <Point X="-24.180275390625" Y="-3.289626708984" />
                  <Point X="-24.1802578125" Y="-3.323120605469" />
                  <Point X="-24.159607421875" Y="-3.479971923828" />
                  <Point X="-23.977935546875" Y="-4.859915039062" />
                  <Point X="-24.0100546875" Y="-4.866955566406" />
                  <Point X="-24.024341796875" Y="-4.870087402344" />
                  <Point X="-24.070681640625" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.03734375" Y="-4.756729003906" />
                  <Point X="-26.058423828125" Y="-4.752637207031" />
                  <Point X="-26.141244140625" Y="-4.731328613281" />
                  <Point X="-26.1207734375" Y="-4.575838378906" />
                  <Point X="-26.120076171875" Y="-4.568098632812" />
                  <Point X="-26.11944921875" Y="-4.541030273438" />
                  <Point X="-26.1217578125" Y="-4.509323730469" />
                  <Point X="-26.123333984375" Y="-4.497688476562" />
                  <Point X="-26.178173828125" Y="-4.221996582031" />
                  <Point X="-26.18386328125" Y="-4.193396972656" />
                  <Point X="-26.188125" Y="-4.178467773438" />
                  <Point X="-26.19902734375" Y="-4.149500488281" />
                  <Point X="-26.20566796875" Y="-4.135462402344" />
                  <Point X="-26.221740234375" Y="-4.107625488281" />
                  <Point X="-26.23057421875" Y="-4.094860595703" />
                  <Point X="-26.25020703125" Y="-4.070937255859" />
                  <Point X="-26.261005859375" Y="-4.059779052734" />
                  <Point X="-26.472341796875" Y="-3.87444140625" />
                  <Point X="-26.494265625" Y="-3.85521484375" />
                  <Point X="-26.50673828125" Y="-3.845965332031" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.917306640625" Y="-3.77778515625" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.961931640625" Y="-3.776132324219" />
                  <Point X="-26.992734375" Y="-3.779166748047" />
                  <Point X="-27.008009765625" Y="-3.781947021484" />
                  <Point X="-27.03905859375" Y="-3.790267578125" />
                  <Point X="-27.05367578125" Y="-3.795498046875" />
                  <Point X="-27.081865234375" Y="-3.808270751953" />
                  <Point X="-27.0954375" Y="-3.815812988281" />
                  <Point X="-27.329158203125" Y="-3.97198046875" />
                  <Point X="-27.35340234375" Y="-3.988181152344" />
                  <Point X="-27.359677734375" Y="-3.992756591797" />
                  <Point X="-27.380439453125" Y="-4.010129638672" />
                  <Point X="-27.402755859375" Y="-4.032768554688" />
                  <Point X="-27.410466796875" Y="-4.041625488281" />
                  <Point X="-27.43160546875" Y="-4.069172363281" />
                  <Point X="-27.503201171875" Y="-4.162478515625" />
                  <Point X="-27.716677734375" Y="-4.030298339844" />
                  <Point X="-27.747591796875" Y="-4.011157226562" />
                  <Point X="-27.98086328125" Y="-3.831546386719" />
                  <Point X="-27.341486328125" Y="-2.724114746094" />
                  <Point X="-27.3348515625" Y="-2.710084960938" />
                  <Point X="-27.32394921875" Y="-2.681119628906" />
                  <Point X="-27.319685546875" Y="-2.666188964844" />
                  <Point X="-27.3134140625" Y="-2.634662109375" />
                  <Point X="-27.311638671875" Y="-2.619238769531" />
                  <Point X="-27.310625" Y="-2.588304931641" />
                  <Point X="-27.3146640625" Y="-2.557616699219" />
                  <Point X="-27.3236484375" Y="-2.527999023438" />
                  <Point X="-27.32935546875" Y="-2.513559082031" />
                  <Point X="-27.343572265625" Y="-2.484729736328" />
                  <Point X="-27.3515546875" Y="-2.471411376953" />
                  <Point X="-27.369583984375" Y="-2.446251464844" />
                  <Point X="-27.379630859375" Y="-2.434409912109" />
                  <Point X="-27.395349609375" Y="-2.418691894531" />
                  <Point X="-27.408787109375" Y="-2.407046142578" />
                  <Point X="-27.43391015625" Y="-2.389036865234" />
                  <Point X="-27.44720703125" Y="-2.381062011719" />
                  <Point X="-27.476041015625" Y="-2.366828369141" />
                  <Point X="-27.490484375" Y="-2.361114013672" />
                  <Point X="-27.520111328125" Y="-2.352116210938" />
                  <Point X="-27.55080859375" Y="-2.348066894531" />
                  <Point X="-27.58175390625" Y="-2.349073730469" />
                  <Point X="-27.597185546875" Y="-2.350846435547" />
                  <Point X="-27.628724609375" Y="-2.357115722656" />
                  <Point X="-27.64366015625" Y="-2.361379150391" />
                  <Point X="-27.672638671875" Y="-2.372283447266" />
                  <Point X="-27.686681640625" Y="-2.378924316406" />
                  <Point X="-27.80787109375" Y="-2.448892333984" />
                  <Point X="-28.793087890625" Y="-3.017708007813" />
                  <Point X="-28.979599609375" Y="-2.772669677734" />
                  <Point X="-29.00401953125" Y="-2.740586425781" />
                  <Point X="-29.181265625" Y="-2.443373535156" />
                  <Point X="-28.048123046875" Y="-1.573881958008" />
                  <Point X="-28.036482421875" Y="-1.563308837891" />
                  <Point X="-28.01510546875" Y="-1.540388549805" />
                  <Point X="-28.005369140625" Y="-1.528041503906" />
                  <Point X="-27.987404296875" Y="-1.50091027832" />
                  <Point X="-27.979837890625" Y="-1.487125732422" />
                  <Point X="-27.96708203125" Y="-1.458496582031" />
                  <Point X="-27.961892578125" Y="-1.443651977539" />
                  <Point X="-27.9549140625" Y="-1.416711669922" />
                  <Point X="-27.951560546875" Y="-1.398844360352" />
                  <Point X="-27.94875" Y="-1.368412963867" />
                  <Point X="-27.948576171875" Y="-1.353078613281" />
                  <Point X="-27.95078125" Y="-1.321403808594" />
                  <Point X="-27.953078125" Y="-1.306242675781" />
                  <Point X="-27.960078125" Y="-1.276492797852" />
                  <Point X="-27.97176953125" Y="-1.248260986328" />
                  <Point X="-27.9878515625" Y="-1.222272949219" />
                  <Point X="-27.9969453125" Y="-1.209928100586" />
                  <Point X="-28.017779296875" Y="-1.185968017578" />
                  <Point X="-28.028740234375" Y="-1.175247558594" />
                  <Point X="-28.052244140625" Y="-1.155709960938" />
                  <Point X="-28.064787109375" Y="-1.146892822266" />
                  <Point X="-28.08878125" Y="-1.132771484375" />
                  <Point X="-28.10542578125" Y="-1.124484863281" />
                  <Point X="-28.134681640625" Y="-1.113262939453" />
                  <Point X="-28.149775390625" Y="-1.108866821289" />
                  <Point X="-28.18166796875" Y="-1.102381591797" />
                  <Point X="-28.197287109375" Y="-1.100534057617" />
                  <Point X="-28.2286171875" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196411133" />
                  <Point X="-28.39731640625" Y="-1.120337890625" />
                  <Point X="-29.66091796875" Y="-1.286694458008" />
                  <Point X="-29.7312109375" Y="-1.011509033203" />
                  <Point X="-29.7407578125" Y="-0.974122375488" />
                  <Point X="-29.786451171875" Y="-0.654654724121" />
                  <Point X="-28.508287109375" Y="-0.312171295166" />
                  <Point X="-28.5004765625" Y="-0.309713348389" />
                  <Point X="-28.473931640625" Y="-0.299251953125" />
                  <Point X="-28.444166015625" Y="-0.2838934021" />
                  <Point X="-28.43355859375" Y="-0.277513763428" />
                  <Point X="-28.4084140625" Y="-0.260062164307" />
                  <Point X="-28.39367578125" Y="-0.248246337891" />
                  <Point X="-28.37122265625" Y="-0.226363845825" />
                  <Point X="-28.36090625" Y="-0.214491699219" />
                  <Point X="-28.341654296875" Y="-0.188226470947" />
                  <Point X="-28.333435546875" Y="-0.17480909729" />
                  <Point X="-28.319326171875" Y="-0.146804519653" />
                  <Point X="-28.313435546875" Y="-0.132217330933" />
                  <Point X="-28.3050546875" Y="-0.10521181488" />
                  <Point X="-28.300990234375" Y="-0.088494857788" />
                  <Point X="-28.296720703125" Y="-0.060337505341" />
                  <Point X="-28.295646484375" Y="-0.046095420837" />
                  <Point X="-28.295646484375" Y="-0.01646462059" />
                  <Point X="-28.296720703125" Y="-0.002222533226" />
                  <Point X="-28.300990234375" Y="0.025934818268" />
                  <Point X="-28.304185546875" Y="0.039850231171" />
                  <Point X="-28.31256640625" Y="0.066855758667" />
                  <Point X="-28.31932421875" Y="0.084240470886" />
                  <Point X="-28.333427734375" Y="0.112236717224" />
                  <Point X="-28.341642578125" Y="0.125649635315" />
                  <Point X="-28.36088671875" Y="0.151910263062" />
                  <Point X="-28.371205078125" Y="0.16378717041" />
                  <Point X="-28.3936640625" Y="0.185677841187" />
                  <Point X="-28.4058046875" Y="0.195691741943" />
                  <Point X="-28.43094921875" Y="0.213143341064" />
                  <Point X="-28.4367734375" Y="0.216878540039" />
                  <Point X="-28.45475" Y="0.227212875366" />
                  <Point X="-28.472927734375" Y="0.236243652344" />
                  <Point X="-28.4816015625" Y="0.240026702881" />
                  <Point X="-28.508287109375" Y="0.249611099243" />
                  <Point X="-28.6477421875" Y="0.286978302002" />
                  <Point X="-29.7854453125" Y="0.591824890137" />
                  <Point X="-29.737484375" Y="0.915948181152" />
                  <Point X="-29.73133203125" Y="0.95752130127" />
                  <Point X="-29.6335859375" Y="1.318237182617" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815551758" />
                  <Point X="-28.747056640625" Y="1.204364257812" />
                  <Point X="-28.736705078125" Y="1.204703369141" />
                  <Point X="-28.71514453125" Y="1.20658984375" />
                  <Point X="-28.704890625" Y="1.208053588867" />
                  <Point X="-28.684603515625" Y="1.212088989258" />
                  <Point X="-28.6745703125" Y="1.214660522461" />
                  <Point X="-28.61891796875" Y="1.232207519531" />
                  <Point X="-28.603451171875" Y="1.23767565918" />
                  <Point X="-28.584515625" Y="1.246007080078" />
                  <Point X="-28.5752734375" Y="1.250690673828" />
                  <Point X="-28.556529296875" Y="1.261512939453" />
                  <Point X="-28.547853515625" Y="1.267174316406" />
                  <Point X="-28.53117578125" Y="1.279403930664" />
                  <Point X="-28.5159296875" Y="1.29337512207" />
                  <Point X="-28.50229296875" Y="1.308924194336" />
                  <Point X="-28.495900390625" Y="1.3170703125" />
                  <Point X="-28.483484375" Y="1.33480065918" />
                  <Point X="-28.478015625" Y="1.343594604492" />
                  <Point X="-28.4680625" Y="1.361733764648" />
                  <Point X="-28.463578125" Y="1.371079467773" />
                  <Point X="-28.441248046875" Y="1.424991088867" />
                  <Point X="-28.435494140625" Y="1.440358886719" />
                  <Point X="-28.42970703125" Y="1.460217773438" />
                  <Point X="-28.427357421875" Y="1.470301757812" />
                  <Point X="-28.423599609375" Y="1.491616210938" />
                  <Point X="-28.422359375" Y="1.501894042969" />
                  <Point X="-28.421005859375" Y="1.522533447266" />
                  <Point X="-28.421908203125" Y="1.543201782227" />
                  <Point X="-28.4250546875" Y="1.563644775391" />
                  <Point X="-28.427185546875" Y="1.573780151367" />
                  <Point X="-28.432787109375" Y="1.594685913086" />
                  <Point X="-28.436009765625" Y="1.604529052734" />
                  <Point X="-28.443505859375" Y="1.623805908203" />
                  <Point X="-28.447779296875" Y="1.633239868164" />
                  <Point X="-28.47472265625" Y="1.68499987793" />
                  <Point X="-28.482822265625" Y="1.699328613281" />
                  <Point X="-28.49431640625" Y="1.716523925781" />
                  <Point X="-28.500533203125" Y="1.724807739258" />
                  <Point X="-28.51444140625" Y="1.741376953125" />
                  <Point X="-28.52151953125" Y="1.748931152344" />
                  <Point X="-28.536451171875" Y="1.763221923828" />
                  <Point X="-28.5443046875" Y="1.769958374023" />
                  <Point X="-28.624296875" Y="1.831339111328" />
                  <Point X="-29.22761328125" Y="2.294280517578" />
                  <Point X="-29.026197265625" Y="2.639354980469" />
                  <Point X="-29.00228515625" Y="2.680322509766" />
                  <Point X="-28.726337890625" Y="3.035012695312" />
                  <Point X="-28.254158203125" Y="2.762399658203" />
                  <Point X="-28.244919921875" Y="2.757717773438" />
                  <Point X="-28.225984375" Y="2.749385742188" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.0775625" Y="2.724376708984" />
                  <Point X="-28.059169921875" Y="2.723334472656" />
                  <Point X="-28.038490234375" Y="2.723785888672" />
                  <Point X="-28.0281640625" Y="2.724576171875" />
                  <Point X="-28.006705078125" Y="2.727401611328" />
                  <Point X="-27.996525390625" Y="2.729311279297" />
                  <Point X="-27.976431640625" Y="2.734228271484" />
                  <Point X="-27.9569921875" Y="2.741303466797" />
                  <Point X="-27.938439453125" Y="2.750453125" />
                  <Point X="-27.929412109375" Y="2.755534912109" />
                  <Point X="-27.911158203125" Y="2.767164794922" />
                  <Point X="-27.902740234375" Y="2.773198486328" />
                  <Point X="-27.88661328125" Y="2.786142822266" />
                  <Point X="-27.878904296875" Y="2.793053466797" />
                  <Point X="-27.82388671875" Y="2.848069580078" />
                  <Point X="-27.811265625" Y="2.861487792969" />
                  <Point X="-27.798318359375" Y="2.877618652344" />
                  <Point X="-27.79228515625" Y="2.886038574219" />
                  <Point X="-27.78065625" Y="2.904291503906" />
                  <Point X="-27.775576171875" Y="2.913316650391" />
                  <Point X="-27.766427734375" Y="2.931866455078" />
                  <Point X="-27.759353515625" Y="2.951297607422" />
                  <Point X="-27.754435546875" Y="2.971387451172" />
                  <Point X="-27.7525234375" Y="2.981570800781" />
                  <Point X="-27.749697265625" Y="3.003028076172" />
                  <Point X="-27.748908203125" Y="3.013362060547" />
                  <Point X="-27.74845703125" Y="3.034047607422" />
                  <Point X="-27.748794921875" Y="3.044399169922" />
                  <Point X="-27.755576171875" Y="3.121907958984" />
                  <Point X="-27.757744140625" Y="3.140207763672" />
                  <Point X="-27.76178125" Y="3.160497314453" />
                  <Point X="-27.764353515625" Y="3.170527832031" />
                  <Point X="-27.77086328125" Y="3.191172119141" />
                  <Point X="-27.77451171875" Y="3.200864990234" />
                  <Point X="-27.782841796875" Y="3.219795654297" />
                  <Point X="-27.7875234375" Y="3.229033447266" />
                  <Point X="-27.822970703125" Y="3.2904296875" />
                  <Point X="-28.05938671875" Y="3.699914794922" />
                  <Point X="-27.69" Y="3.983120117188" />
                  <Point X="-27.6483671875" Y="4.015041015625" />
                  <Point X="-27.192525390625" Y="4.268296386719" />
                  <Point X="-27.118564453125" Y="4.171908203125" />
                  <Point X="-27.1118203125" Y="4.164047363281" />
                  <Point X="-27.097517578125" Y="4.149106445313" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.06509375" Y="4.121898925781" />
                  <Point X="-27.04789453125" Y="4.11040625" />
                  <Point X="-27.03898046875" Y="4.105128417969" />
                  <Point X="-26.952712890625" Y="4.060220458984" />
                  <Point X="-26.93432421875" Y="4.051285400391" />
                  <Point X="-26.91504296875" Y="4.043788330078" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031377929688" />
                  <Point X="-26.780822265625" Y="4.035135253906" />
                  <Point X="-26.770732421875" Y="4.037487548828" />
                  <Point X="-26.75087109375" Y="4.043276611328" />
                  <Point X="-26.741099609375" Y="4.046713134766" />
                  <Point X="-26.651248046875" Y="4.083931884766" />
                  <Point X="-26.632591796875" Y="4.092270019531" />
                  <Point X="-26.614462890625" Y="4.10221484375" />
                  <Point X="-26.60566796875" Y="4.107682617188" />
                  <Point X="-26.587935546875" Y="4.120097167969" />
                  <Point X="-26.5797890625" Y="4.126489746094" />
                  <Point X="-26.564232421875" Y="4.140129882813" />
                  <Point X="-26.550248046875" Y="4.155389648437" />
                  <Point X="-26.538015625" Y="4.17207421875" />
                  <Point X="-26.532357421875" Y="4.180746582031" />
                  <Point X="-26.52153515625" Y="4.1994921875" />
                  <Point X="-26.516853515625" Y="4.208731445312" />
                  <Point X="-26.5085234375" Y="4.2276640625" />
                  <Point X="-26.504875" Y="4.237356933594" />
                  <Point X="-26.47563671875" Y="4.330091796875" />
                  <Point X="-26.470033203125" Y="4.349733886719" />
                  <Point X="-26.465994140625" Y="4.370024902344" />
                  <Point X="-26.46452734375" Y="4.380283691406" />
                  <Point X="-26.462638671875" Y="4.401851074219" />
                  <Point X="-26.46230078125" Y="4.412209472656" />
                  <Point X="-26.462751953125" Y="4.432900878906" />
                  <Point X="-26.463541015625" Y="4.443233886719" />
                  <Point X="-26.467572265625" Y="4.473844238281" />
                  <Point X="-26.479263671875" Y="4.562655273438" />
                  <Point X="-25.985064453125" Y="4.701212402344" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.365224609375" Y="4.782556640625" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.780025390625" Y="4.218915527344" />
                  <Point X="-24.767251953125" Y="4.247107910156" />
                  <Point X="-24.7620234375" Y="4.261724609375" />
                  <Point X="-24.743859375" Y="4.329508789063" />
                  <Point X="-24.621810546875" Y="4.785006347656" />
                  <Point X="-24.219189453125" Y="4.742841308594" />
                  <Point X="-24.172123046875" Y="4.737911621094" />
                  <Point X="-23.5949140625" Y="4.598556640625" />
                  <Point X="-23.546408203125" Y="4.586845703125" />
                  <Point X="-23.171806640625" Y="4.450974609375" />
                  <Point X="-23.141740234375" Y="4.440069335938" />
                  <Point X="-22.778447265625" Y="4.270170410156" />
                  <Point X="-22.74954296875" Y="4.25665234375" />
                  <Point X="-22.39854296875" Y="4.052158447266" />
                  <Point X="-22.370568359375" Y="4.035860595703" />
                  <Point X="-22.18221875" Y="3.901916015625" />
                  <Point X="-22.934689453125" Y="2.59859765625" />
                  <Point X="-22.93762109375" Y="2.593111328125" />
                  <Point X="-22.946818359375" Y="2.573435302734" />
                  <Point X="-22.95581640625" Y="2.549563476562" />
                  <Point X="-22.958697265625" Y="2.540599853516" />
                  <Point X="-22.978150390625" Y="2.467859619141" />
                  <Point X="-22.983087890625" Y="2.442686279297" />
                  <Point X="-22.986443359375" Y="2.416238769531" />
                  <Point X="-22.98719921875" Y="2.404575927734" />
                  <Point X="-22.987271484375" Y="2.381244384766" />
                  <Point X="-22.986587890625" Y="2.369577148438" />
                  <Point X="-22.979009765625" Y="2.306739990234" />
                  <Point X="-22.976208984375" Y="2.289086669922" />
                  <Point X="-22.9708671875" Y="2.267150634766" />
                  <Point X="-22.96754296875" Y="2.256342529297" />
                  <Point X="-22.959275390625" Y="2.234241699219" />
                  <Point X="-22.9546875" Y="2.223902832031" />
                  <Point X="-22.9443203125" Y="2.203847412109" />
                  <Point X="-22.938541015625" Y="2.194130859375" />
                  <Point X="-22.89962109375" Y="2.136772460937" />
                  <Point X="-22.88356640625" Y="2.116284667969" />
                  <Point X="-22.86622265625" Y="2.097031982422" />
                  <Point X="-22.858052734375" Y="2.088996826172" />
                  <Point X="-22.840810546875" Y="2.073970703125" />
                  <Point X="-22.83173828125" Y="2.066979736328" />
                  <Point X="-22.774380859375" Y="2.028059692383" />
                  <Point X="-22.758712890625" Y="2.018241943359" />
                  <Point X="-22.7386640625" Y="2.007877929688" />
                  <Point X="-22.72833203125" Y="2.003294189453" />
                  <Point X="-22.706248046875" Y="1.995030517578" />
                  <Point X="-22.695447265625" Y="1.991706420898" />
                  <Point X="-22.67352734375" Y="1.986364868164" />
                  <Point X="-22.662408203125" Y="1.984346801758" />
                  <Point X="-22.599509765625" Y="1.976762084961" />
                  <Point X="-22.59280859375" Y="1.976193847656" />
                  <Point X="-22.572654296875" Y="1.975439941406" />
                  <Point X="-22.547228515625" Y="1.975990234375" />
                  <Point X="-22.535888671875" Y="1.976916992188" />
                  <Point X="-22.513400390625" Y="1.980119628906" />
                  <Point X="-22.502251953125" Y="1.982395507812" />
                  <Point X="-22.42951171875" Y="2.001847412109" />
                  <Point X="-22.41462890625" Y="2.006489990234" />
                  <Point X="-22.39499609375" Y="2.013509765625" />
                  <Point X="-22.387076171875" Y="2.016751098633" />
                  <Point X="-22.36396484375" Y="2.027872924805" />
                  <Point X="-22.22369921875" Y="2.108855957031" />
                  <Point X="-21.059595703125" Y="2.780951171875" />
                  <Point X="-20.97264453125" Y="2.660108154297" />
                  <Point X="-20.956044921875" Y="2.6370390625" />
                  <Point X="-20.863115234375" Y="2.483471191406" />
                  <Point X="-21.82705078125" Y="1.743817626953" />
                  <Point X="-21.831859375" Y="1.739870849609" />
                  <Point X="-21.847875" Y="1.725222290039" />
                  <Point X="-21.865330078125" Y="1.706604736328" />
                  <Point X="-21.871421875" Y="1.699422729492" />
                  <Point X="-21.9237734375" Y="1.631126342773" />
                  <Point X="-21.93745703125" Y="1.610651733398" />
                  <Point X="-21.95135546875" Y="1.586617675781" />
                  <Point X="-21.956669921875" Y="1.575930541992" />
                  <Point X="-21.965921875" Y="1.553960327148" />
                  <Point X="-21.969859375" Y="1.542677490234" />
                  <Point X="-21.989361328125" Y="1.472946655273" />
                  <Point X="-21.993775390625" Y="1.4546640625" />
                  <Point X="-21.997228515625" Y="1.432365234375" />
                  <Point X="-21.998291015625" Y="1.421115600586" />
                  <Point X="-21.999107421875" Y="1.397545532227" />
                  <Point X="-21.998826171875" Y="1.386240844727" />
                  <Point X="-21.996921875" Y="1.363753173828" />
                  <Point X="-21.995298828125" Y="1.35257019043" />
                  <Point X="-21.9792890625" Y="1.274985961914" />
                  <Point X="-21.9726328125" Y="1.250873779297" />
                  <Point X="-21.9637734375" Y="1.225444824219" />
                  <Point X="-21.95924609375" Y="1.21464465332" />
                  <Point X="-21.9488984375" Y="1.193685180664" />
                  <Point X="-21.943080078125" Y="1.183526733398" />
                  <Point X="-21.89955078125" Y="1.117362548828" />
                  <Point X="-21.8882734375" Y="1.101443603516" />
                  <Point X="-21.873716796875" Y="1.084193237305" />
                  <Point X="-21.865927734375" Y="1.076005004883" />
                  <Point X="-21.848681640625" Y="1.059911010742" />
                  <Point X="-21.83997265625" Y="1.052703613281" />
                  <Point X="-21.8217578125" Y="1.039372924805" />
                  <Point X="-21.812251953125" Y="1.03325" />
                  <Point X="-21.749154296875" Y="0.997731933594" />
                  <Point X="-21.725740234375" Y="0.986581970215" />
                  <Point X="-21.70121875" Y="0.976898742676" />
                  <Point X="-21.690173828125" Y="0.973301025391" />
                  <Point X="-21.667728515625" Y="0.967480224609" />
                  <Point X="-21.656328125" Y="0.965257385254" />
                  <Point X="-21.571017578125" Y="0.95398260498" />
                  <Point X="-21.556271484375" Y="0.952615722656" />
                  <Point X="-21.53434375" Y="0.951442932129" />
                  <Point X="-21.525591796875" Y="0.951378540039" />
                  <Point X="-21.49939453125" Y="0.952797180176" />
                  <Point X="-21.36615234375" Y="0.970339111328" />
                  <Point X="-20.295296875" Y="1.111319702148" />
                  <Point X="-20.2544453125" Y="0.943512573242" />
                  <Point X="-20.247310546875" Y="0.914204467773" />
                  <Point X="-20.216126953125" Y="0.713921325684" />
                  <Point X="-21.3080078125" Y="0.421352722168" />
                  <Point X="-21.313966796875" Y="0.419544891357" />
                  <Point X="-21.33437109375" Y="0.412138702393" />
                  <Point X="-21.3576171875" Y="0.401619934082" />
                  <Point X="-21.365994140625" Y="0.397316833496" />
                  <Point X="-21.44980859375" Y="0.34887008667" />
                  <Point X="-21.46953125" Y="0.335767333984" />
                  <Point X="-21.4925390625" Y="0.318320220947" />
                  <Point X="-21.501751953125" Y="0.310352844238" />
                  <Point X="-21.519083984375" Y="0.293305633545" />
                  <Point X="-21.527203125" Y="0.284226257324" />
                  <Point X="-21.5774921875" Y="0.22014616394" />
                  <Point X="-21.5891484375" Y="0.204203979492" />
                  <Point X="-21.600875" Y="0.184917770386" />
                  <Point X="-21.606162109375" Y="0.17492616272" />
                  <Point X="-21.615935546875" Y="0.153459213257" />
                  <Point X="-21.61999609375" Y="0.142917541504" />
                  <Point X="-21.62683984375" Y="0.121423248291" />
                  <Point X="-21.629623046875" Y="0.11047063446" />
                  <Point X="-21.64638671875" Y="0.022940216064" />
                  <Point X="-21.64941015625" Y="-0.000976478875" />
                  <Point X="-21.6511484375" Y="-0.028858879089" />
                  <Point X="-21.651140625" Y="-0.040796863556" />
                  <Point X="-21.64962890625" Y="-0.064577568054" />
                  <Point X="-21.648125" Y="-0.076420135498" />
                  <Point X="-21.631361328125" Y="-0.163950408936" />
                  <Point X="-21.62683984375" Y="-0.183983291626" />
                  <Point X="-21.61999609375" Y="-0.205477584839" />
                  <Point X="-21.615935546875" Y="-0.216019256592" />
                  <Point X="-21.606162109375" Y="-0.237486190796" />
                  <Point X="-21.600875" Y="-0.247477661133" />
                  <Point X="-21.5891484375" Y="-0.26676385498" />
                  <Point X="-21.582708984375" Y="-0.276058898926" />
                  <Point X="-21.532419921875" Y="-0.34013885498" />
                  <Point X="-21.515689453125" Y="-0.358781799316" />
                  <Point X="-21.49616015625" Y="-0.377850708008" />
                  <Point X="-21.48719140625" Y="-0.385578063965" />
                  <Point X="-21.46836328125" Y="-0.399854644775" />
                  <Point X="-21.45850390625" Y="-0.406404296875" />
                  <Point X="-21.374689453125" Y="-0.454851043701" />
                  <Point X="-21.362232421875" Y="-0.461448730469" />
                  <Point X="-21.34145703125" Y="-0.471481506348" />
                  <Point X="-21.333240234375" Y="-0.47498336792" />
                  <Point X="-21.3080078125" Y="-0.48391293335" />
                  <Point X="-21.18581640625" Y="-0.516653808594" />
                  <Point X="-20.21512109375" Y="-0.776751159668" />
                  <Point X="-20.234404296875" Y="-0.904654541016" />
                  <Point X="-20.238380859375" Y="-0.931040710449" />
                  <Point X="-20.2721953125" Y="-1.079220092773" />
                  <Point X="-21.563216796875" Y="-0.909253662109" />
                  <Point X="-21.571375" Y="-0.908535522461" />
                  <Point X="-21.59990234375" Y="-0.908042541504" />
                  <Point X="-21.63327734375" Y="-0.910840820312" />
                  <Point X="-21.645517578125" Y="-0.912676147461" />
                  <Point X="-21.810017578125" Y="-0.948431274414" />
                  <Point X="-21.842123046875" Y="-0.956742248535" />
                  <Point X="-21.8712421875" Y="-0.968365905762" />
                  <Point X="-21.8853203125" Y="-0.975387451172" />
                  <Point X="-21.9131484375" Y="-0.992280090332" />
                  <Point X="-21.925876953125" Y="-1.001530822754" />
                  <Point X="-21.949626953125" Y="-1.022002258301" />
                  <Point X="-21.9606484375" Y="-1.033223022461" />
                  <Point X="-22.060078125" Y="-1.152805541992" />
                  <Point X="-22.078673828125" Y="-1.17684777832" />
                  <Point X="-22.093396484375" Y="-1.201233276367" />
                  <Point X="-22.099837890625" Y="-1.213982299805" />
                  <Point X="-22.1111796875" Y="-1.24136706543" />
                  <Point X="-22.11563671875" Y="-1.254934082031" />
                  <Point X="-22.122466796875" Y="-1.282581054688" />
                  <Point X="-22.12483984375" Y="-1.296660766602" />
                  <Point X="-22.13908984375" Y="-1.451525390625" />
                  <Point X="-22.140708984375" Y="-1.483320068359" />
                  <Point X="-22.138390625" Y="-1.514588500977" />
                  <Point X="-22.135931640625" Y="-1.530127807617" />
                  <Point X="-22.12819921875" Y="-1.561752075195" />
                  <Point X="-22.1232109375" Y="-1.576671386719" />
                  <Point X="-22.11083984375" Y="-1.605481933594" />
                  <Point X="-22.10345703125" Y="-1.619373168945" />
                  <Point X="-22.012421875" Y="-1.760974243164" />
                  <Point X="-22.00305859375" Y="-1.774253540039" />
                  <Point X="-21.981994140625" Y="-1.801568603516" />
                  <Point X="-21.973859375" Y="-1.810811157227" />
                  <Point X="-21.956462890625" Y="-1.828165649414" />
                  <Point X="-21.947201171875" Y="-1.83627734375" />
                  <Point X="-21.83380859375" Y="-1.923287231445" />
                  <Point X="-20.912828125" Y="-2.629980957031" />
                  <Point X="-20.943291015625" Y="-2.679274658203" />
                  <Point X="-20.9545234375" Y="-2.697451660156" />
                  <Point X="-20.998724609375" Y="-2.760253173828" />
                  <Point X="-22.15154296875" Y="-2.094672363281" />
                  <Point X="-22.158806640625" Y="-2.090886230469" />
                  <Point X="-22.184970703125" Y="-2.079513916016" />
                  <Point X="-22.216876953125" Y="-2.069326416016" />
                  <Point X="-22.228888671875" Y="-2.066337646484" />
                  <Point X="-22.42466796875" Y="-2.030979858398" />
                  <Point X="-22.444978515625" Y="-2.027312011719" />
                  <Point X="-22.460638671875" Y="-2.025807495117" />
                  <Point X="-22.491994140625" Y="-2.025403442383" />
                  <Point X="-22.507689453125" Y="-2.02650402832" />
                  <Point X="-22.53986328125" Y="-2.031462524414" />
                  <Point X="-22.555158203125" Y="-2.035137695313" />
                  <Point X="-22.584931640625" Y="-2.044961181641" />
                  <Point X="-22.59941015625" Y="-2.051109375" />
                  <Point X="-22.762056640625" Y="-2.136708496094" />
                  <Point X="-22.791029296875" Y="-2.153169433594" />
                  <Point X="-22.81395703125" Y="-2.170059326172" />
                  <Point X="-22.824783203125" Y="-2.179368164063" />
                  <Point X="-22.8457421875" Y="-2.200325195313" />
                  <Point X="-22.8550546875" Y="-2.211153808594" />
                  <Point X="-22.871951171875" Y="-2.234087402344" />
                  <Point X="-22.87953515625" Y="-2.246192382812" />
                  <Point X="-22.965134765625" Y="-2.408837890625" />
                  <Point X="-22.9801640625" Y="-2.440192138672" />
                  <Point X="-22.98998828125" Y="-2.469971191406" />
                  <Point X="-22.9936640625" Y="-2.485268554688" />
                  <Point X="-22.99862109375" Y="-2.517443359375" />
                  <Point X="-22.999720703125" Y="-2.533134033203" />
                  <Point X="-22.99931640625" Y="-2.564484130859" />
                  <Point X="-22.9978125" Y="-2.580143554688" />
                  <Point X="-22.962453125" Y="-2.775924072266" />
                  <Point X="-22.95878515625" Y="-2.796233886719" />
                  <Point X="-22.95698046875" Y="-2.80423046875" />
                  <Point X="-22.948763671875" Y="-2.831535644531" />
                  <Point X="-22.935931640625" Y="-2.862473876953" />
                  <Point X="-22.930453125" Y="-2.873577148438" />
                  <Point X="-22.857587890625" Y="-2.999786132812" />
                  <Point X="-22.26410546875" Y="-4.027725097656" />
                  <Point X="-22.276244140625" Y="-4.036083984375" />
                  <Point X="-23.166083984375" Y="-2.876420410156" />
                  <Point X="-23.17134765625" Y="-2.870141113281" />
                  <Point X="-23.19116796875" Y="-2.849625732422" />
                  <Point X="-23.21674609375" Y="-2.828004638672" />
                  <Point X="-23.22669921875" Y="-2.820646972656" />
                  <Point X="-23.41979296875" Y="-2.696506347656" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.453716796875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663873535156" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.802783203125" Y="-2.66594921875" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670339355469" />
                  <Point X="-23.866423828125" Y="-2.677171386719" />
                  <Point X="-23.8799921875" Y="-2.681629394531" />
                  <Point X="-23.907376953125" Y="-2.69297265625" />
                  <Point X="-23.920123046875" Y="-2.699414306641" />
                  <Point X="-23.94450390625" Y="-2.714134521484" />
                  <Point X="-23.956138671875" Y="-2.722413085938" />
                  <Point X="-24.11920703125" Y="-2.857998291016" />
                  <Point X="-24.136123046875" Y="-2.872063476562" />
                  <Point X="-24.14734375" Y="-2.883084960938" />
                  <Point X="-24.167814453125" Y="-2.906832763672" />
                  <Point X="-24.177064453125" Y="-2.919559082031" />
                  <Point X="-24.19395703125" Y="-2.947384277344" />
                  <Point X="-24.20098046875" Y="-2.961465820312" />
                  <Point X="-24.21260546875" Y="-2.990588867188" />
                  <Point X="-24.21720703125" Y="-3.005630371094" />
                  <Point X="-24.265962890625" Y="-3.229947753906" />
                  <Point X="-24.271021484375" Y="-3.253218261719" />
                  <Point X="-24.2724140625" Y="-3.261286376953" />
                  <Point X="-24.275275390625" Y="-3.289676513672" />
                  <Point X="-24.2752578125" Y="-3.323170410156" />
                  <Point X="-24.2744453125" Y="-3.335520996094" />
                  <Point X="-24.253794921875" Y="-3.492372314453" />
                  <Point X="-24.16691015625" Y="-4.152326171875" />
                  <Point X="-24.34493359375" Y="-3.487936523438" />
                  <Point X="-24.347392578125" Y="-3.480123535156" />
                  <Point X="-24.3578515625" Y="-3.453581787109" />
                  <Point X="-24.373208984375" Y="-3.423816894531" />
                  <Point X="-24.37958984375" Y="-3.413209228516" />
                  <Point X="-24.5279296875" Y="-3.19948046875" />
                  <Point X="-24.543318359375" Y="-3.177308837891" />
                  <Point X="-24.553330078125" Y="-3.165169189453" />
                  <Point X="-24.575216796875" Y="-3.142712890625" />
                  <Point X="-24.587091796875" Y="-3.132396240234" />
                  <Point X="-24.61334765625" Y="-3.113152832031" />
                  <Point X="-24.6267578125" Y="-3.104937255859" />
                  <Point X="-24.654755859375" Y="-3.090830078125" />
                  <Point X="-24.66934375" Y="-3.084938476562" />
                  <Point X="-24.898890625" Y="-3.013695556641" />
                  <Point X="-24.922703125" Y="-3.006305175781" />
                  <Point X="-24.936623046875" Y="-3.003109130859" />
                  <Point X="-24.964783203125" Y="-2.99883984375" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766357422" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109375" />
                  <Point X="-25.064984375" Y="-3.006305419922" />
                  <Point X="-25.294529296875" Y="-3.077548095703" />
                  <Point X="-25.31834375" Y="-3.084938720703" />
                  <Point X="-25.3329296875" Y="-3.090829101562" />
                  <Point X="-25.3609296875" Y="-3.104936523438" />
                  <Point X="-25.37434375" Y="-3.113153564453" />
                  <Point X="-25.400599609375" Y="-3.132397460938" />
                  <Point X="-25.412474609375" Y="-3.142716796875" />
                  <Point X="-25.434359375" Y="-3.165173095703" />
                  <Point X="-25.444369140625" Y="-3.177310058594" />
                  <Point X="-25.59270703125" Y="-3.391038574219" />
                  <Point X="-25.608095703125" Y="-3.413210449219" />
                  <Point X="-25.612470703125" Y="-3.420133544922" />
                  <Point X="-25.625974609375" Y="-3.445260742188" />
                  <Point X="-25.63877734375" Y="-3.476212646484" />
                  <Point X="-25.64275390625" Y="-3.487936035156" />
                  <Point X="-25.680404296875" Y="-3.628447265625" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.235139986774" Y="0.708826798515" />
                  <Point X="-20.392582934336" Y="1.098511768189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.931074211185" Y="2.431324448196" />
                  <Point X="-21.069923473908" Y="2.774988432969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.327592063757" Y="0.684054338385" />
                  <Point X="-20.489868993672" Y="1.08570383423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.009287359998" Y="2.371309404146" />
                  <Point X="-21.153004431531" Y="2.72702163851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.420044140741" Y="0.659281878255" />
                  <Point X="-20.587155053008" Y="1.07289590027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.087500508812" Y="2.311294360096" />
                  <Point X="-21.236085389155" Y="2.67905484405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.512496217724" Y="0.634509418126" />
                  <Point X="-20.684441112344" Y="1.060087966311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.165713657625" Y="2.251279316046" />
                  <Point X="-21.319166346778" Y="2.631088049591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.604948294708" Y="0.609736957996" />
                  <Point X="-20.78172717168" Y="1.047280032352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.243926806439" Y="2.191264271996" />
                  <Point X="-21.402247304402" Y="2.583121255132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.697400371692" Y="0.584964497867" />
                  <Point X="-20.879013231016" Y="1.034472098392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.322139955252" Y="2.131249227947" />
                  <Point X="-21.485328262025" Y="2.535154460672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.224515805321" Y="-0.839065255982" />
                  <Point X="-20.253889330978" Y="-0.766363228791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.789852448675" Y="0.560192037737" />
                  <Point X="-20.976299290352" Y="1.021664164433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.400353104066" Y="2.071234183897" />
                  <Point X="-21.568409219649" Y="2.487187666213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.25694621711" Y="-1.012396550557" />
                  <Point X="-20.368789014633" Y="-0.735575912758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.882304525659" Y="0.535419577607" />
                  <Point X="-21.073585349688" Y="1.008856230474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.478566252879" Y="2.011219139847" />
                  <Point X="-21.651490177273" Y="2.439220871753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.217657521394" Y="3.840534222022" />
                  <Point X="-22.26674315954" Y="3.962025439689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.335791296552" Y="-1.070847511415" />
                  <Point X="-20.483688698288" Y="-0.704788596725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.974756602642" Y="0.510647117478" />
                  <Point X="-21.170871409024" Y="0.996048296514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.556779401693" Y="1.951204095797" />
                  <Point X="-21.734571134896" Y="2.391254077294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.277935870038" Y="3.736128989853" />
                  <Point X="-22.407798751488" Y="4.057550900477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.444008281128" Y="-1.056600456018" />
                  <Point X="-20.598588381943" Y="-0.674001280691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.067208679626" Y="0.485874657348" />
                  <Point X="-21.26815746836" Y="0.983240362555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.634992550506" Y="1.891189051747" />
                  <Point X="-21.81765209252" Y="2.343287282835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.338214218681" Y="3.631723757684" />
                  <Point X="-22.541802258541" Y="4.135621838653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.552225265703" Y="-1.042353400622" />
                  <Point X="-20.713488065597" Y="-0.643213964658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.159660756609" Y="0.461102197218" />
                  <Point X="-21.365443527696" Y="0.970432428595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.71320569932" Y="1.831174007697" />
                  <Point X="-21.900733050143" Y="2.295320488375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.398492567325" Y="3.527318525515" />
                  <Point X="-22.675805765594" Y="4.213692776829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.660442250278" Y="-1.028106345226" />
                  <Point X="-20.828387749252" Y="-0.612426648625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.252112833593" Y="0.436329737089" />
                  <Point X="-21.462729506384" Y="0.957624295026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.791418848133" Y="1.771158963647" />
                  <Point X="-21.983814007767" Y="2.247353693916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.458770915969" Y="3.422913293346" />
                  <Point X="-22.806358862492" Y="4.283223650193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.768659234854" Y="-1.01385928983" />
                  <Point X="-20.943287432907" Y="-0.581639332592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.343187925679" Y="0.408149119743" />
                  <Point X="-21.563434993082" Y="0.953279740775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.867001768079" Y="1.704633874705" />
                  <Point X="-22.06689496539" Y="2.199386899457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.519049264612" Y="3.318508061177" />
                  <Point X="-22.93268966483" Y="4.342303977797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.876876219429" Y="-0.999612234434" />
                  <Point X="-21.058187116562" Y="-0.550852016559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.42702033781" Y="0.362042240457" />
                  <Point X="-21.672090162513" Y="0.968611341746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.933739010739" Y="1.616214966202" />
                  <Point X="-22.149975923014" Y="2.151420104997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.579327613256" Y="3.214102829008" />
                  <Point X="-23.059020467168" Y="4.401384305402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.985093204004" Y="-0.985365179038" />
                  <Point X="-21.173086800217" Y="-0.520064700525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.506650606553" Y="0.305534691312" />
                  <Point X="-21.79725621116" Y="1.024808802806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.984842985259" Y="1.489102361252" />
                  <Point X="-22.233056866046" Y="2.103453274424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.6396059619" Y="3.109697596839" />
                  <Point X="-23.183184387828" Y="4.455101412655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.09331018858" Y="-0.971118123642" />
                  <Point X="-21.287986387488" Y="-0.489277623049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.575590987093" Y="0.222568740413" />
                  <Point X="-22.316137694126" Y="2.055486159332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.699884310544" Y="3.00529236467" />
                  <Point X="-23.303238333339" Y="4.498645974447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.201527173155" Y="-0.956871068245" />
                  <Point X="-21.413395779143" Y="-0.432477866913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.630632234413" Y="0.105201227609" />
                  <Point X="-22.400800431082" Y="2.011434406103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.760162659187" Y="2.900887132501" />
                  <Point X="-23.42329227885" Y="4.542190536239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.30974415773" Y="-0.942624012849" />
                  <Point X="-21.575175564808" Y="-0.285658226708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.637681314465" Y="-0.130951067469" />
                  <Point X="-22.492574352955" Y="1.984983453176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.820441007831" Y="2.796481900332" />
                  <Point X="-23.54334622436" Y="4.58573509803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.417961142306" Y="-0.928376957453" />
                  <Point X="-22.591463594998" Y="1.976143535657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.880719356475" Y="2.692076668163" />
                  <Point X="-23.657048038743" Y="4.613557583577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.526178126881" Y="-0.914129902057" />
                  <Point X="-22.700888841653" Y="1.993381144642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.940611132634" Y="2.586714635521" />
                  <Point X="-23.770583501928" Y="4.640968335458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.926470817916" Y="-2.652056958816" />
                  <Point X="-20.945527611945" Y="-2.604889738446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.630076240096" Y="-0.910572428387" />
                  <Point X="-22.833694423234" Y="2.068487113231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.983516331234" Y="2.439309348075" />
                  <Point X="-23.884118965112" Y="4.66837908734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.990143469751" Y="-2.74806099578" />
                  <Point X="-21.094025931473" Y="-2.490942880471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.72473076442" Y="-0.929893640059" />
                  <Point X="-23.997654428297" Y="4.695789839222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.11474082623" Y="-2.69327109723" />
                  <Point X="-21.242524251001" Y="-2.376996022496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.818784915005" Y="-0.950700828878" />
                  <Point X="-24.111189891481" Y="4.723200591103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.248373427116" Y="-2.616118184033" />
                  <Point X="-21.391022570529" Y="-2.263049164521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.906160460487" Y="-0.988038145388" />
                  <Point X="-24.221691942824" Y="4.743103385215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.382006028002" Y="-2.538965270836" />
                  <Point X="-21.539520890056" Y="-2.149102306546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.980647514563" Y="-1.057275597538" />
                  <Point X="-24.328679626963" Y="4.754307815263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.515638628887" Y="-2.461812357639" />
                  <Point X="-21.688019209584" Y="-2.035155448571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.049602153946" Y="-1.140206256561" />
                  <Point X="-24.435667311103" Y="4.765512245312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.649271229773" Y="-2.384659444442" />
                  <Point X="-21.836517537977" Y="-1.921208568654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.111184910833" Y="-1.241382965037" />
                  <Point X="-24.542654995242" Y="4.77671667536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.782903830658" Y="-2.307506531244" />
                  <Point X="-21.993172588986" Y="-1.787073091824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.137170440282" Y="-1.43066590316" />
                  <Point X="-24.632438914457" Y="4.745340293014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.916536431544" Y="-2.230353618047" />
                  <Point X="-24.673294728631" Y="4.59286260112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.05016903243" Y="-2.15320070485" />
                  <Point X="-24.714150542806" Y="4.440384909226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.181853877363" Y="-2.080868656804" />
                  <Point X="-24.755006929257" Y="4.287908633766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.295010299412" Y="-2.054396064654" />
                  <Point X="-24.810651457817" Y="4.172034294427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.405535843125" Y="-2.034435124886" />
                  <Point X="-24.888204351414" Y="4.110385061371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.510995177684" Y="-2.027013492791" />
                  <Point X="-24.980454690026" Y="4.08511328125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.602964774608" Y="-2.052980132972" />
                  <Point X="-25.091776106392" Y="4.107044074959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.687459139464" Y="-2.097448621774" />
                  <Point X="-25.462559179611" Y="4.771165004509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.771817356515" Y="-2.142254088215" />
                  <Point X="-25.560393814578" Y="4.759714842882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.849194159029" Y="-2.204339161996" />
                  <Point X="-25.658228449545" Y="4.748264681255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.910768199251" Y="-2.305537444974" />
                  <Point X="-25.756063084512" Y="4.736814519628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.356078789922" Y="-3.932041290175" />
                  <Point X="-22.441697566029" Y="-3.720127383026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.968584873458" Y="-2.416035535178" />
                  <Point X="-25.853897719479" Y="4.725364358002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.572486446477" Y="-3.650012924901" />
                  <Point X="-22.782997028253" Y="-3.128980951441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.992211056162" Y="-2.611158061413" />
                  <Point X="-25.950514050183" Y="4.710898787511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.788894103031" Y="-3.367984559628" />
                  <Point X="-26.04254962919" Y="4.685095458714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.005301759585" Y="-3.085956194354" />
                  <Point X="-26.134585060847" Y="4.659291765215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.209510712872" Y="-2.834120679167" />
                  <Point X="-26.226620492505" Y="4.633488071716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.34924701245" Y="-2.741860581578" />
                  <Point X="-26.318655924162" Y="4.607684378217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.483323976298" Y="-2.663607831454" />
                  <Point X="-26.410691355819" Y="4.581880684718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.592651297683" Y="-2.646612596018" />
                  <Point X="-26.466638780832" Y="4.466756040408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.691439256672" Y="-2.655703197892" />
                  <Point X="-26.492401043734" Y="4.276920498188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.79022721566" Y="-2.664793799766" />
                  <Point X="-26.547354667826" Y="4.159336110281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.885041043515" Y="-2.683720721364" />
                  <Point X="-26.624510167298" Y="4.096703292252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.967913548926" Y="-2.732203453154" />
                  <Point X="-26.71169476106" Y="4.058893353649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.044609633896" Y="-2.795973361978" />
                  <Point X="-26.802996627821" Y="4.031274023319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.121305713049" Y="-2.859743285201" />
                  <Point X="-26.909823366311" Y="4.042080098907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.190594823238" Y="-2.941846099931" />
                  <Point X="-27.037431574248" Y="4.104322116317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.234723856164" Y="-3.086222291125" />
                  <Point X="-27.203646050249" Y="4.262118000272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.270564064656" Y="-3.251114042705" />
                  <Point X="-27.287323872865" Y="4.21562849851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.243279423149" Y="-3.572245280641" />
                  <Point X="-27.371001695482" Y="4.169138996747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.193754586072" Y="-3.948422934251" />
                  <Point X="-24.276271050786" Y="-3.744187517246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.452350753337" Y="-3.308374960308" />
                  <Point X="-27.454679518098" Y="4.122649494985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.639625181459" Y="-3.098453865725" />
                  <Point X="-27.538357340715" Y="4.076159993223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.758758737322" Y="-3.057187348251" />
                  <Point X="-27.622035163331" Y="4.029670491461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.875909667347" Y="-3.020828001924" />
                  <Point X="-27.701978037692" Y="3.973936668372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.987687907286" Y="-2.997766530196" />
                  <Point X="-27.780206535691" Y="3.91395961489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.084279277708" Y="-3.012293879552" />
                  <Point X="-27.85843503369" Y="3.853982561407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.175323534523" Y="-3.040550816874" />
                  <Point X="-27.936663531689" Y="3.794005507925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.266367791338" Y="-3.068807754196" />
                  <Point X="-27.752258527788" Y="3.083987726623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.865261094112" Y="3.363678892933" />
                  <Point X="-28.014892029688" Y="3.734028454442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.355364490654" Y="-3.102132574162" />
                  <Point X="-27.781550339873" Y="2.902888125184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.432942621489" Y="-3.163719342862" />
                  <Point X="-27.850935061621" Y="2.821021957369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.49843756721" Y="-3.255213044186" />
                  <Point X="-27.927443995028" Y="2.756788832169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.563198873327" Y="-3.348522567249" />
                  <Point X="-28.017459486381" Y="2.725985610978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.626278288359" Y="-3.445994916824" />
                  <Point X="-28.120798549316" Y="2.728159386652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.670283680022" Y="-3.590677130881" />
                  <Point X="-28.23310044891" Y="2.752516961507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.711139921163" Y="-3.743153765997" />
                  <Point X="-28.365534197345" Y="2.826702610763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.751996100085" Y="-3.895630555108" />
                  <Point X="-28.499166743258" Y="2.9038553879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.792852279008" Y="-4.048107344219" />
                  <Point X="-28.632799289172" Y="2.981008165037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.83370845793" Y="-4.20058413333" />
                  <Point X="-28.746571616295" Y="3.009005175739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.874564636852" Y="-4.353060922441" />
                  <Point X="-28.814010335806" Y="2.922322483369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.915420815775" Y="-4.505537711552" />
                  <Point X="-28.881449055317" Y="2.835639790999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.956276994697" Y="-4.658014500664" />
                  <Point X="-28.429157013423" Y="1.462578323761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.557409369416" Y="1.780014044" />
                  <Point X="-28.948887774828" Y="2.74895709863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.017281170697" Y="-4.760623247084" />
                  <Point X="-26.123243326908" Y="-4.498357707288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.32208093413" Y="-4.006217359689" />
                  <Point X="-28.481367558173" Y="1.338204576238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.705907952248" Y="1.893961553674" />
                  <Point X="-29.014891968875" Y="2.658723831144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.130456375707" Y="-4.734104165476" />
                  <Point X="-26.138868457659" Y="-4.713283532027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.480768375138" Y="-3.86705154109" />
                  <Point X="-28.553612049928" Y="1.26341658757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.854406283433" Y="2.007908440502" />
                  <Point X="-29.075440991401" Y="2.554988540342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.609854051126" Y="-3.80115266193" />
                  <Point X="-28.640690062582" Y="1.22534285147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.002904614618" Y="2.121855327331" />
                  <Point X="-29.135990046556" Y="2.451253330302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.716436615156" Y="-3.790950939339" />
                  <Point X="-28.734876609056" Y="1.204863353973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.151402945804" Y="2.235802214159" />
                  <Point X="-29.196539101712" Y="2.347518120261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.821684528946" Y="-3.784052592011" />
                  <Point X="-27.313440363968" Y="-2.566914189657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.352635242803" Y="-2.469903460331" />
                  <Point X="-28.306348406665" Y="-0.109380546525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.438586821271" Y="0.217921014982" />
                  <Point X="-28.840984102967" Y="1.21388923676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.926932416884" Y="-3.777154308667" />
                  <Point X="-27.347899867764" Y="-2.735223305278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.500247851371" Y="-2.358148813909" />
                  <Point X="-28.364616464369" Y="-0.218761423371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.559382842919" Y="0.263302279666" />
                  <Point X="-28.949201075593" Y="1.228136262578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.025556963126" Y="-3.786649371283" />
                  <Point X="-27.408178271688" Y="-2.839628400622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.605029076894" Y="-2.352405560573" />
                  <Point X="-27.966669046652" Y="-1.457315225755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.100062411335" Y="-1.127155062496" />
                  <Point X="-28.44142764531" Y="-0.28224645967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.674282602539" Y="0.294089783718" />
                  <Point X="-29.057418048218" Y="1.242383288397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.111813890884" Y="-3.826755363815" />
                  <Point X="-27.468456675613" Y="-2.944033495967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.694866408054" Y="-2.383649743715" />
                  <Point X="-28.029381663785" Y="-1.55569543199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.213507349036" Y="-1.099968369049" />
                  <Point X="-28.529501397774" Y="-0.31785565326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.789182266632" Y="0.324877051334" />
                  <Point X="-29.165635020843" Y="1.256630314215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.192494087256" Y="-3.880664250887" />
                  <Point X="-27.528735079537" Y="-3.048438591312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.777947527023" Y="-2.431616138831" />
                  <Point X="-28.106421054771" Y="-1.618615628606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.31226248002" Y="-1.109140223085" />
                  <Point X="-28.621953464472" Y="-0.342628138847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.904081930724" Y="0.35566431895" />
                  <Point X="-29.273851993468" Y="1.270877340034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.273174283628" Y="-3.934573137958" />
                  <Point X="-27.589013483461" Y="-3.152843686656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.861028530143" Y="-2.479582820684" />
                  <Point X="-28.184634183887" Y="-1.678630721411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.409548490626" Y="-1.121948277656" />
                  <Point X="-28.71440553117" Y="-0.367400624435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.018981594817" Y="0.386451586565" />
                  <Point X="-29.382068966093" Y="1.285124365852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.353845496898" Y="-3.988504258986" />
                  <Point X="-27.649291887386" Y="-3.257248782001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.944109468048" Y="-2.527549663948" />
                  <Point X="-28.262847313002" Y="-1.738645814217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.506834534927" Y="-1.134756248827" />
                  <Point X="-28.806857597867" Y="-0.392173110022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.13388125891" Y="0.417238854181" />
                  <Point X="-29.490285938718" Y="1.299371391671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.426436019333" Y="-4.062435791667" />
                  <Point X="-27.70957029131" Y="-3.361653877346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.027190405953" Y="-2.575516507212" />
                  <Point X="-28.341060442117" Y="-1.798660907022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.604120579229" Y="-1.147564219998" />
                  <Point X="-28.899309664565" Y="-0.416945595609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.248780923003" Y="0.448026121797" />
                  <Point X="-29.598502911344" Y="1.313618417489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.493555686978" Y="-4.149908165116" />
                  <Point X="-27.769848695234" Y="-3.46605897269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.110271343858" Y="-2.623483350476" />
                  <Point X="-28.419273571232" Y="-1.858675999828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.70140662353" Y="-1.160372191169" />
                  <Point X="-28.991761731263" Y="-0.441718081197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.363680587096" Y="0.478813389412" />
                  <Point X="-29.661383645179" Y="1.215654314696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.620208921443" Y="-4.090029789992" />
                  <Point X="-27.830127099159" Y="-3.570464068035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.193352281763" Y="-2.67145019374" />
                  <Point X="-28.497486700347" Y="-1.918691092633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.798692667832" Y="-1.17318016234" />
                  <Point X="-29.084213797961" Y="-0.466490566784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.478580251189" Y="0.509600657028" />
                  <Point X="-29.702516168668" Y="1.063861502389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.757672168455" Y="-4.003395694928" />
                  <Point X="-27.890405503083" Y="-3.674869163379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.276433219669" Y="-2.719417037004" />
                  <Point X="-28.575699829462" Y="-1.978706185438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.895978712133" Y="-1.185988133512" />
                  <Point X="-29.176665864659" Y="-0.491263052372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.593479915282" Y="0.540387924644" />
                  <Point X="-29.739556983022" Y="0.901941354594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.906400164234" Y="-3.888880368283" />
                  <Point X="-27.950683907007" Y="-3.779274258724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.359514157574" Y="-2.767383880268" />
                  <Point X="-28.653912958577" Y="-2.038721278244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.993264756435" Y="-1.198796104683" />
                  <Point X="-29.269117931357" Y="-0.516035537959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.708379579375" Y="0.571175192259" />
                  <Point X="-29.767023149942" Y="0.716323122807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.442595095479" Y="-2.815350723532" />
                  <Point X="-28.732126087692" Y="-2.098736371049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.090550800736" Y="-1.211604075854" />
                  <Point X="-29.361569998055" Y="-0.540808023547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.525676033384" Y="-2.863317566796" />
                  <Point X="-28.810339216807" Y="-2.158751463855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.187836845038" Y="-1.224412047025" />
                  <Point X="-29.454022064753" Y="-0.565580509134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.608756971289" Y="-2.91128441006" />
                  <Point X="-28.888552345922" Y="-2.21876655666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.285122889339" Y="-1.237220018196" />
                  <Point X="-29.546474131451" Y="-0.590352994722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.691837909194" Y="-2.959251253324" />
                  <Point X="-28.966765475037" Y="-2.278781649466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.382408933641" Y="-1.250027989367" />
                  <Point X="-29.638926198149" Y="-0.615125480309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.7749188471" Y="-3.007218096588" />
                  <Point X="-29.044978604152" Y="-2.338796742271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.479694977942" Y="-1.262835960538" />
                  <Point X="-29.731378264847" Y="-0.639897965897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.963707929179" Y="-2.793548101908" />
                  <Point X="-29.123191733267" Y="-2.398811835077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.576981022244" Y="-1.275643931709" />
                  <Point X="-29.763749276789" Y="-0.813376280249" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.0001640625" Y="0.001625976562" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.16552734375" Y="-4.891590820312" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521543701172" />
                  <Point X="-24.68401953125" Y="-3.307814941406" />
                  <Point X="-24.699408203125" Y="-3.285643310547" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.9552109375" Y="-3.195156982422" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766357422" />
                  <Point X="-25.238208984375" Y="-3.259009033203" />
                  <Point X="-25.2620234375" Y="-3.266399658203" />
                  <Point X="-25.288279296875" Y="-3.285643554688" />
                  <Point X="-25.4366171875" Y="-3.499372070312" />
                  <Point X="-25.452005859375" Y="-3.521543945312" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.496876953125" Y="-3.677623535156" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-26.073546875" Y="-4.943247558594" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.534756835938" />
                  <Point X="-26.364521484375" Y="-4.259064941406" />
                  <Point X="-26.3702109375" Y="-4.230465332031" />
                  <Point X="-26.386283203125" Y="-4.202628417969" />
                  <Point X="-26.597619140625" Y="-4.017290527344" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.929732421875" Y="-3.967378417969" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791992188" />
                  <Point X="-27.223599609375" Y="-4.129959472656" />
                  <Point X="-27.24784375" Y="-4.14616015625" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.28087109375" Y="-4.184841308594" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.816701171875" Y="-4.191839355469" />
                  <Point X="-27.855833984375" Y="-4.167609863281" />
                  <Point X="-28.22478125" Y="-3.883532470703" />
                  <Point X="-28.228580078125" Y="-3.880607421875" />
                  <Point X="-28.1805" Y="-3.797330566406" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597592773438" />
                  <Point X="-27.513978515625" Y="-2.568763427734" />
                  <Point X="-27.529697265625" Y="-2.553045410156" />
                  <Point X="-27.53130859375" Y="-2.551434082031" />
                  <Point X="-27.560142578125" Y="-2.537200439453" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-27.71287109375" Y="-2.613437744141" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-29.130787109375" Y="-2.887745849609" />
                  <Point X="-29.16169921875" Y="-2.847134277344" />
                  <Point X="-29.4262109375" Y="-2.403590087891" />
                  <Point X="-29.43101953125" Y="-2.395526855469" />
                  <Point X="-29.343884765625" Y="-2.328666259766" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.396013549805" />
                  <Point X="-28.138841796875" Y="-1.369062133789" />
                  <Point X="-28.138119140625" Y="-1.366273803711" />
                  <Point X="-28.14032421875" Y="-1.334598999023" />
                  <Point X="-28.161158203125" Y="-1.310638916016" />
                  <Point X="-28.18515234375" Y="-1.296517456055" />
                  <Point X="-28.187634765625" Y="-1.295056274414" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.372515625" Y="-1.308712402344" />
                  <Point X="-29.80328125" Y="-1.497076416016" />
                  <Point X="-29.91530078125" Y="-1.058531982422" />
                  <Point X="-29.927390625" Y="-1.01119140625" />
                  <Point X="-29.997375" Y="-0.5218828125" />
                  <Point X="-29.998396484375" Y="-0.514742492676" />
                  <Point X="-29.901771484375" Y="-0.488852142334" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.54189453125" Y="-0.121425048828" />
                  <Point X="-28.51675" Y="-0.103973358154" />
                  <Point X="-28.5141484375" Y="-0.102167739868" />
                  <Point X="-28.494896484375" Y="-0.075902435303" />
                  <Point X="-28.486515625" Y="-0.04889692688" />
                  <Point X="-28.485646484375" Y="-0.046095401764" />
                  <Point X="-28.485646484375" Y="-0.016464675903" />
                  <Point X="-28.49402734375" Y="0.010540828705" />
                  <Point X="-28.494896484375" Y="0.013342353821" />
                  <Point X="-28.514140625" Y="0.03960294342" />
                  <Point X="-28.53928515625" Y="0.057054634094" />
                  <Point X="-28.53929296875" Y="0.057059352875" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-28.69691796875" Y="0.103452529907" />
                  <Point X="-29.998185546875" Y="0.452126037598" />
                  <Point X="-29.9254375" Y="0.943760253906" />
                  <Point X="-29.91764453125" Y="0.996415588379" />
                  <Point X="-29.7767734375" Y="1.516277099609" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.7168984375" Y="1.520845092773" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.67605078125" Y="1.413413696289" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.651533203125" Y="1.426056396484" />
                  <Point X="-28.6391171875" Y="1.443786743164" />
                  <Point X="-28.616787109375" Y="1.497698486328" />
                  <Point X="-28.614470703125" Y="1.503291137695" />
                  <Point X="-28.610712890625" Y="1.52460534668" />
                  <Point X="-28.616314453125" Y="1.545510864258" />
                  <Point X="-28.6432578125" Y="1.597270996094" />
                  <Point X="-28.646060546875" Y="1.60265246582" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-28.7399609375" Y="1.680602294922" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.190291015625" Y="2.735134277344" />
                  <Point X="-29.16001171875" Y="2.7870078125" />
                  <Point X="-28.786849609375" Y="3.26665625" />
                  <Point X="-28.774669921875" Y="3.282310791016" />
                  <Point X="-28.750994140625" Y="3.268640869141" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.06100390625" Y="2.913653564453" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775634766" />
                  <Point X="-28.013251953125" Y="2.927405517578" />
                  <Point X="-27.958234375" Y="2.982421630859" />
                  <Point X="-27.95252734375" Y="2.98812890625" />
                  <Point X="-27.9408984375" Y="3.006381835938" />
                  <Point X="-27.938072265625" Y="3.027839111328" />
                  <Point X="-27.944853515625" Y="3.105347900391" />
                  <Point X="-27.945556640625" Y="3.113388671875" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-27.987513671875" Y="3.195429199219" />
                  <Point X="-28.30727734375" Y="3.749276855469" />
                  <Point X="-27.80560546875" Y="4.133903808594" />
                  <Point X="-27.75287890625" Y="4.174329589844" />
                  <Point X="-27.16515234375" Y="4.500857421875" />
                  <Point X="-27.141548828125" Y="4.513971679688" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.864978515625" Y="4.228752441406" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.813810546875" Y="4.222249511719" />
                  <Point X="-26.723958984375" Y="4.259468261719" />
                  <Point X="-26.71463671875" Y="4.263329101563" />
                  <Point X="-26.696904296875" Y="4.275743652344" />
                  <Point X="-26.68608203125" Y="4.294489257812" />
                  <Point X="-26.65683984375" Y="4.387236816406" />
                  <Point X="-26.6538046875" Y="4.396858886719" />
                  <Point X="-26.651916015625" Y="4.418426269531" />
                  <Point X="-26.655947265625" Y="4.449036621094" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.036357421875" Y="4.884157714844" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.25559375" Y="4.986684082031" />
                  <Point X="-25.22419921875" Y="4.990358398438" />
                  <Point X="-25.21722265625" Y="4.964318359375" />
                  <Point X="-25.16892578125" Y="4.977258789062" />
                  <Point X="-25.21722265625" Y="4.964317871094" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.9273828125" Y="4.3786875" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.199400390625" Y="4.931808105469" />
                  <Point X="-24.13979296875" Y="4.925564941406" />
                  <Point X="-23.55032421875" Y="4.78325" />
                  <Point X="-23.491544921875" Y="4.76905859375" />
                  <Point X="-23.107021484375" Y="4.629588867188" />
                  <Point X="-23.0689609375" Y="4.615784179688" />
                  <Point X="-22.697958984375" Y="4.442279785156" />
                  <Point X="-22.6613046875" Y="4.425137207031" />
                  <Point X="-22.3028984375" Y="4.216328613281" />
                  <Point X="-22.26747265625" Y="4.195688964844" />
                  <Point X="-21.9312578125" Y="3.956592285156" />
                  <Point X="-21.9895" Y="3.85571484375" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491512451172" />
                  <Point X="-22.794599609375" Y="2.418772216797" />
                  <Point X="-22.797955078125" Y="2.392324707031" />
                  <Point X="-22.79037109375" Y="2.329438476563" />
                  <Point X="-22.7895859375" Y="2.322913330078" />
                  <Point X="-22.781318359375" Y="2.3008125" />
                  <Point X="-22.7423984375" Y="2.243454101562" />
                  <Point X="-22.742392578125" Y="2.243446533203" />
                  <Point X="-22.7250546875" Y="2.224201416016" />
                  <Point X="-22.667697265625" Y="2.18528125" />
                  <Point X="-22.66174609375" Y="2.181243896484" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.576763671875" Y="2.165395507813" />
                  <Point X="-22.551337890625" Y="2.165945800781" />
                  <Point X="-22.47859765625" Y="2.185397705078" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-22.31869921875" Y="2.273400390625" />
                  <Point X="-21.005751953125" Y="3.031430908203" />
                  <Point X="-20.81841796875" Y="2.771079589844" />
                  <Point X="-20.79741015625" Y="2.741886230469" />
                  <Point X="-20.612486328125" Y="2.436295166016" />
                  <Point X="-20.68358984375" Y="2.381735595703" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583833374023" />
                  <Point X="-21.77298046875" Y="1.515536987305" />
                  <Point X="-21.772982421875" Y="1.515532714844" />
                  <Point X="-21.78687890625" Y="1.491502929688" />
                  <Point X="-21.806380859375" Y="1.421772094727" />
                  <Point X="-21.808404296875" Y="1.414538452148" />
                  <Point X="-21.809220703125" Y="1.390968383789" />
                  <Point X="-21.7932109375" Y="1.313384033203" />
                  <Point X="-21.7843515625" Y="1.287954956055" />
                  <Point X="-21.740814453125" Y="1.221780151367" />
                  <Point X="-21.736298828125" Y="1.214914672852" />
                  <Point X="-21.719052734375" Y="1.198820556641" />
                  <Point X="-21.655955078125" Y="1.163302612305" />
                  <Point X="-21.63143359375" Y="1.153619506836" />
                  <Point X="-21.546123046875" Y="1.142344482422" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-21.390953125" Y="1.158713623047" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.0698359375" Y="0.988453857422" />
                  <Point X="-20.060806640625" Y="0.951366027832" />
                  <Point X="-20.002138671875" Y="0.574556518555" />
                  <Point X="-20.080958984375" Y="0.553436767578" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819961548" />
                  <Point X="-21.3547265625" Y="0.184373214722" />
                  <Point X="-21.377734375" Y="0.166926239014" />
                  <Point X="-21.4280234375" Y="0.102846038818" />
                  <Point X="-21.433240234375" Y="0.096198562622" />
                  <Point X="-21.443013671875" Y="0.074731613159" />
                  <Point X="-21.45977734375" Y="-0.01279867363" />
                  <Point X="-21.461515625" Y="-0.040681129456" />
                  <Point X="-21.444751953125" Y="-0.128211410522" />
                  <Point X="-21.443013671875" Y="-0.137291687012" />
                  <Point X="-21.433240234375" Y="-0.158758636475" />
                  <Point X="-21.382951171875" Y="-0.222838684082" />
                  <Point X="-21.363421875" Y="-0.241907516479" />
                  <Point X="-21.279607421875" Y="-0.290354278564" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-21.136640625" Y="-0.333127838135" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.046529296875" Y="-0.932980224609" />
                  <Point X="-20.051568359375" Y="-0.966410705566" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-20.222017578125" Y="-1.277465576172" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.769662109375" Y="-1.134096069336" />
                  <Point X="-21.7867265625" Y="-1.137805053711" />
                  <Point X="-21.8145546875" Y="-1.154697631836" />
                  <Point X="-21.913984375" Y="-1.274280273438" />
                  <Point X="-21.924298828125" Y="-1.286685546875" />
                  <Point X="-21.935640625" Y="-1.314070068359" />
                  <Point X="-21.949890625" Y="-1.468934936523" />
                  <Point X="-21.951369140625" Y="-1.485000366211" />
                  <Point X="-21.94363671875" Y="-1.516624633789" />
                  <Point X="-21.8526015625" Y="-1.658225708008" />
                  <Point X="-21.831537109375" Y="-1.685540771484" />
                  <Point X="-21.71814453125" Y="-1.77255065918" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.7816640625" Y="-2.779158203125" />
                  <Point X="-20.795875" Y="-2.802154296875" />
                  <Point X="-20.943310546875" Y="-3.011639160156" />
                  <Point X="-21.0299375" Y="-2.961624511719" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.4584375" Y="-2.217955078125" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219245605469" />
                  <Point X="-22.673568359375" Y="-2.304844726562" />
                  <Point X="-22.690439453125" Y="-2.313724609375" />
                  <Point X="-22.7113984375" Y="-2.334681640625" />
                  <Point X="-22.796998046875" Y="-2.497327148438" />
                  <Point X="-22.80587890625" Y="-2.514199707031" />
                  <Point X="-22.8108359375" Y="-2.546374511719" />
                  <Point X="-22.7754765625" Y="-2.742155029297" />
                  <Point X="-22.77180859375" Y="-2.76246484375" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.693041015625" Y="-2.904787597656" />
                  <Point X="-22.01332421875" Y="-4.082088378906" />
                  <Point X="-22.14784375" Y="-4.178172363281" />
                  <Point X="-22.164705078125" Y="-4.190215332031" />
                  <Point X="-22.32022265625" Y="-4.290879394531" />
                  <Point X="-22.3884375" Y="-4.20198046875" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.52254296875" Y="-2.856326660156" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.785373046875" Y="-2.855149902344" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509277344" />
                  <Point X="-23.997734375" Y="-3.004094482422" />
                  <Point X="-24.014650390625" Y="-3.018159667969" />
                  <Point X="-24.03154296875" Y="-3.045984863281" />
                  <Point X="-24.080298828125" Y="-3.270302246094" />
                  <Point X="-24.085357421875" Y="-3.293572753906" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-24.065419921875" Y="-3.467571533203" />
                  <Point X="-23.872357421875" Y="-4.934028320312" />
                  <Point X="-23.989712890625" Y="-4.959752441406" />
                  <Point X="-24.005669921875" Y="-4.963250488281" />
                  <Point X="-24.139798828125" Y="-4.987616699219" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#208" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.175081857811" Y="5.008538895752" Z="2.3" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.3" />
                  <Point X="-0.267562846851" Y="5.067625162536" Z="2.3" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.3" />
                  <Point X="-1.056011556553" Y="4.963581276655" Z="2.3" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.3" />
                  <Point X="-1.711675831429" Y="4.47379101261" Z="2.3" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.3" />
                  <Point X="-1.710679761189" Y="4.433558384292" Z="2.3" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.3" />
                  <Point X="-1.749246320335" Y="4.336943110796" Z="2.3" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.3" />
                  <Point X="-1.848048242248" Y="4.304383266851" Z="2.3" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.3" />
                  <Point X="-2.115494499671" Y="4.58540888818" Z="2.3" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.3" />
                  <Point X="-2.19559270463" Y="4.575844749492" Z="2.3" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.3" />
                  <Point X="-2.842184969013" Y="4.204863058547" Z="2.3" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.3" />
                  <Point X="-3.036971817282" Y="3.201709137485" Z="2.3" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.3" />
                  <Point X="-3.000821215918" Y="3.132272282341" Z="2.3" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.3" />
                  <Point X="-2.999747307384" Y="3.049056260502" Z="2.3" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.3" />
                  <Point X="-3.062804183266" Y="2.994743483255" Z="2.3" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.3" />
                  <Point X="-3.732150210812" Y="3.343222131464" Z="2.3" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.3" />
                  <Point X="-3.832469669063" Y="3.328638930847" Z="2.3" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.3" />
                  <Point X="-4.239629434203" Y="2.791619920246" Z="2.3" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.3" />
                  <Point X="-3.776554861353" Y="1.672214573506" Z="2.3" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.3" />
                  <Point X="-3.693767095323" Y="1.605464601615" Z="2.3" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.3" />
                  <Point X="-3.669139004608" Y="1.548111683173" Z="2.3" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.3" />
                  <Point X="-3.697243240715" Y="1.492379787339" Z="2.3" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.3" />
                  <Point X="-4.716530088512" Y="1.601697398357" Z="2.3" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.3" />
                  <Point X="-4.831189525224" Y="1.560634153161" Z="2.3" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.3" />
                  <Point X="-4.981054388172" Y="0.982360130902" Z="2.3" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.3" />
                  <Point X="-3.716017224558" Y="0.086436286216" Z="2.3" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.3" />
                  <Point X="-3.573952293499" Y="0.047258624225" Z="2.3" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.3" />
                  <Point X="-3.547938363747" Y="0.027005450117" Z="2.3" />
                  <Point X="-3.539556741714" Y="0" Z="2.3" />
                  <Point X="-3.540426237388" Y="-0.002801500944" Z="2.3" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.3" />
                  <Point X="-3.551416301624" Y="-0.031617358909" Z="2.3" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.3" />
                  <Point X="-4.920870564215" Y="-0.40927577054" Z="2.3" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.3" />
                  <Point X="-5.053027430509" Y="-0.497681215694" Z="2.3" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.3" />
                  <Point X="-4.969902039786" Y="-1.039624501403" Z="2.3" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.3" />
                  <Point X="-3.372147617408" Y="-1.327004652437" Z="2.3" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.3" />
                  <Point X="-3.216669838293" Y="-1.308328254216" Z="2.3" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.3" />
                  <Point X="-3.193400437328" Y="-1.32524570409" Z="2.3" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.3" />
                  <Point X="-4.380479141201" Y="-2.257718436053" Z="2.3" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.3" />
                  <Point X="-4.475310773542" Y="-2.397919649902" Z="2.3" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.3" />
                  <Point X="-4.176730078599" Y="-2.88674935967" Z="2.3" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.3" />
                  <Point X="-2.694027794091" Y="-2.62545906471" Z="2.3" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.3" />
                  <Point X="-2.571208942036" Y="-2.557121527095" Z="2.3" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.3" />
                  <Point X="-3.229957948458" Y="-3.741050588401" Z="2.3" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.3" />
                  <Point X="-3.26144253424" Y="-3.891869903608" Z="2.3" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.3" />
                  <Point X="-2.849181077579" Y="-4.203070643405" Z="2.3" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.3" />
                  <Point X="-2.247359792249" Y="-4.183999115925" Z="2.3" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.3" />
                  <Point X="-2.201976496723" Y="-4.140251642886" Z="2.3" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.3" />
                  <Point X="-1.939158277608" Y="-3.985991535096" Z="2.3" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.3" />
                  <Point X="-1.63674308558" Y="-4.023602585412" Z="2.3" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.3" />
                  <Point X="-1.419717115145" Y="-4.237540330011" Z="2.3" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.3" />
                  <Point X="-1.408566892278" Y="-4.845078105354" Z="2.3" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.3" />
                  <Point X="-1.385307009081" Y="-4.886653900794" Z="2.3" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.3" />
                  <Point X="-1.089230409292" Y="-4.961051746018" Z="2.3" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.3" />
                  <Point X="-0.454736685573" Y="-3.659284449193" Z="2.3" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.3" />
                  <Point X="-0.401698312223" Y="-3.496601230557" Z="2.3" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.3" />
                  <Point X="-0.229546321777" Y="-3.275482075668" Z="2.3" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.3" />
                  <Point X="0.023812757584" Y="-3.21162996962" Z="2.3" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.3" />
                  <Point X="0.268747547209" Y="-3.305044525211" Z="2.3" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.3" />
                  <Point X="0.780018174828" Y="-4.873251580623" Z="2.3" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.3" />
                  <Point X="0.834618103204" Y="-5.010683750354" Z="2.3" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.3" />
                  <Point X="1.014838066144" Y="-4.977312709439" Z="2.3" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.3" />
                  <Point X="0.977995661709" Y="-3.429765071398" Z="2.3" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.3" />
                  <Point X="0.962403685218" Y="-3.249643380016" Z="2.3" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.3" />
                  <Point X="1.028076358918" Y="-3.011260629196" Z="2.3" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.3" />
                  <Point X="1.213051145272" Y="-2.8736595015" Z="2.3" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.3" />
                  <Point X="1.444261539678" Y="-2.867104724982" Z="2.3" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.3" />
                  <Point X="2.565737666781" Y="-4.201138307297" Z="2.3" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.3" />
                  <Point X="2.680395691756" Y="-4.314773651907" Z="2.3" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.3" />
                  <Point X="2.875059809325" Y="-4.187579713786" Z="2.3" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.3" />
                  <Point X="2.344103645653" Y="-2.848507481002" Z="2.3" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.3" />
                  <Point X="2.267568934327" Y="-2.701988675889" Z="2.3" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.3" />
                  <Point X="2.241091029694" Y="-2.489335613581" Z="2.3" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.3" />
                  <Point X="2.343562764791" Y="-2.317810280273" Z="2.3" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.3" />
                  <Point X="2.526518127624" Y="-2.235879075164" Z="2.3" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.3" />
                  <Point X="3.938906116105" Y="-2.973645854622" Z="2.3" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.3" />
                  <Point X="4.081525994341" Y="-3.023194805725" Z="2.3" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.3" />
                  <Point X="4.254712077643" Y="-2.774163917398" Z="2.3" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.3" />
                  <Point X="3.306136336338" Y="-1.701602942527" Z="2.3" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.3" />
                  <Point X="3.183298862798" Y="-1.59990356717" Z="2.3" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.3" />
                  <Point X="3.093740841843" Y="-1.44223707401" Z="2.3" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.3" />
                  <Point X="3.118306102067" Y="-1.274966915628" Z="2.3" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.3" />
                  <Point X="3.234800429135" Y="-1.151675056278" Z="2.3" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.3" />
                  <Point X="4.765300121862" Y="-1.295757757065" Z="2.3" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.3" />
                  <Point X="4.914942317134" Y="-1.279639006519" Z="2.3" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.3" />
                  <Point X="4.996755932335" Y="-0.909152639149" Z="2.3" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.3" />
                  <Point X="3.870143194204" Y="-0.25355147971" Z="2.3" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.3" />
                  <Point X="3.73925785497" Y="-0.215784860795" Z="2.3" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.3" />
                  <Point X="3.650225807876" Y="-0.160690642134" Z="2.3" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.3" />
                  <Point X="3.598197754771" Y="-0.087530283664" Z="2.3" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.3" />
                  <Point X="3.583173695653" Y="0.009080247515" Z="2.3" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.3" />
                  <Point X="3.605153630524" Y="0.103258096441" Z="2.3" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.3" />
                  <Point X="3.664137559383" Y="0.172363983447" Z="2.3" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.3" />
                  <Point X="4.925824623103" Y="0.536420453059" Z="2.3" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.3" />
                  <Point X="5.041821087001" Y="0.608944543603" Z="2.3" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.3" />
                  <Point X="4.972589305268" Y="1.031560581445" Z="2.3" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.3" />
                  <Point X="3.596365071641" Y="1.239565948829" Z="2.3" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.3" />
                  <Point X="3.454271412851" Y="1.223193718625" Z="2.3" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.3" />
                  <Point X="3.36241508817" Y="1.238153146198" Z="2.3" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.3" />
                  <Point X="3.294801710001" Y="1.280536432323" Z="2.3" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.3" />
                  <Point X="3.249600322321" Y="1.354764874685" Z="2.3" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.3" />
                  <Point X="3.235615030407" Y="1.439582957712" Z="2.3" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.3" />
                  <Point X="3.260546826895" Y="1.516398662658" Z="2.3" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.3" />
                  <Point X="4.340690402616" Y="2.373347800183" Z="2.3" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.3" />
                  <Point X="4.427656328182" Y="2.487642336438" Z="2.3" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.3" />
                  <Point X="4.216010020457" Y="2.831564161416" Z="2.3" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.3" />
                  <Point X="2.650144297997" Y="2.347981594241" Z="2.3" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.3" />
                  <Point X="2.50233196065" Y="2.264980879976" Z="2.3" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.3" />
                  <Point X="2.423066539097" Y="2.246315983013" Z="2.3" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.3" />
                  <Point X="2.354216517416" Y="2.257938029164" Z="2.3" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.3" />
                  <Point X="2.292820571071" Y="2.302808342966" Z="2.3" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.3" />
                  <Point X="2.253113719746" Y="2.366691904736" Z="2.3" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.3" />
                  <Point X="2.247547062681" Y="2.437137615232" Z="2.3" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.3" />
                  <Point X="3.047643758618" Y="3.861994783958" Z="2.3" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.3" />
                  <Point X="3.093368918636" Y="4.027334466271" Z="2.3" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.3" />
                  <Point X="2.716115440868" Y="4.290811073191" Z="2.3" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.3" />
                  <Point X="2.317064829412" Y="4.518851111919" Z="2.3" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.3" />
                  <Point X="1.903870687266" Y="4.707872974747" Z="2.3" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.3" />
                  <Point X="1.455252497482" Y="4.863133407318" Z="2.3" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.3" />
                  <Point X="0.799651051753" Y="5.012816077128" Z="2.3" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.3" />
                  <Point X="0.018162703742" Y="4.422908610199" Z="2.3" />
                  <Point X="0" Y="4.355124473572" Z="2.3" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>