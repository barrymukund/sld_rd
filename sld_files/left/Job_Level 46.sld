<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#207" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3285" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.081650390625" Y="-4.837573730469" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140136719" />
                  <Point X="-24.457634765625" Y="-3.467376953125" />
                  <Point X="-24.602751953125" Y="-3.258291992188" />
                  <Point X="-24.62136328125" Y="-3.2314765625" />
                  <Point X="-24.643248046875" Y="-3.209021240234" />
                  <Point X="-24.66950390625" Y="-3.189777099609" />
                  <Point X="-24.69750390625" Y="-3.175668945312" />
                  <Point X="-24.9220625" Y="-3.105974121094" />
                  <Point X="-24.95086328125" Y="-3.097035644531" />
                  <Point X="-24.9790234375" Y="-3.092766357422" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.2613828125" Y="-3.16673046875" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.18977734375" />
                  <Point X="-25.344439453125" Y="-3.209021484375" />
                  <Point X="-25.36632421875" Y="-3.231477294922" />
                  <Point X="-25.511439453125" Y="-3.440562011719" />
                  <Point X="-25.53005078125" Y="-3.467377685547" />
                  <Point X="-25.5381875" Y="-3.481571777344" />
                  <Point X="-25.550990234375" Y="-3.512524658203" />
                  <Point X="-25.596525390625" Y="-3.68246484375" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.049544921875" Y="-4.851133789062" />
                  <Point X="-26.079333984375" Y="-4.8453515625" />
                  <Point X="-26.24641796875" Y="-4.802362792969" />
                  <Point X="-26.2149609375" Y="-4.563438964844" />
                  <Point X="-26.21419921875" Y="-4.547929199219" />
                  <Point X="-26.2165078125" Y="-4.51622265625" />
                  <Point X="-26.27015625" Y="-4.246520996094" />
                  <Point X="-26.277037109375" Y="-4.211931152344" />
                  <Point X="-26.287939453125" Y="-4.182963867188" />
                  <Point X="-26.30401171875" Y="-4.155126953125" />
                  <Point X="-26.32364453125" Y="-4.131203613281" />
                  <Point X="-26.530388671875" Y="-3.949892822266" />
                  <Point X="-26.556904296875" Y="-3.926639404297" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.917423828125" Y="-3.872981201172" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.983419921875" Y="-3.873708984375" />
                  <Point X="-27.01446875" Y="-3.882029296875" />
                  <Point X="-27.042658203125" Y="-3.894802246094" />
                  <Point X="-27.27130078125" Y="-4.047576416016" />
                  <Point X="-27.300623046875" Y="-4.067170166016" />
                  <Point X="-27.31278515625" Y="-4.076822021484" />
                  <Point X="-27.335099609375" Y="-4.099459472656" />
                  <Point X="-27.360666015625" Y="-4.132775878906" />
                  <Point X="-27.4801484375" Y="-4.288488769531" />
                  <Point X="-27.758041015625" Y="-4.116424316406" />
                  <Point X="-27.801708984375" Y="-4.089386474609" />
                  <Point X="-28.104720703125" Y="-3.856076904297" />
                  <Point X="-28.08357421875" Y="-3.819448974609" />
                  <Point X="-27.42376171875" Y="-2.676619873047" />
                  <Point X="-27.412859375" Y="-2.647654296875" />
                  <Point X="-27.406587890625" Y="-2.616127441406" />
                  <Point X="-27.40557421875" Y="-2.585193603516" />
                  <Point X="-27.41455859375" Y="-2.555575683594" />
                  <Point X="-27.428775390625" Y="-2.526746582031" />
                  <Point X="-27.446802734375" Y="-2.501588623047" />
                  <Point X="-27.462166015625" Y="-2.486225341797" />
                  <Point X="-27.46412109375" Y="-2.484270263672" />
                  <Point X="-27.4892734375" Y="-2.466236816406" />
                  <Point X="-27.518107421875" Y="-2.452008056641" />
                  <Point X="-27.54773046875" Y="-2.443014892578" />
                  <Point X="-27.578671875" Y="-2.444023681641" />
                  <Point X="-27.61020703125" Y="-2.450293457031" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-27.785751953125" Y="-2.545819580078" />
                  <Point X="-28.818021484375" Y="-3.14180078125" />
                  <Point X="-29.048359375" Y="-2.839185302734" />
                  <Point X="-29.082865234375" Y="-2.793851318359" />
                  <Point X="-29.306142578125" Y="-2.419449951172" />
                  <Point X="-29.260412109375" Y="-2.384360595703" />
                  <Point X="-28.105955078125" Y="-1.498513793945" />
                  <Point X="-28.084580078125" Y="-1.475596069336" />
                  <Point X="-28.066615234375" Y="-1.448467041016" />
                  <Point X="-28.053857421875" Y="-1.419838989258" />
                  <Point X="-28.04702734375" Y="-1.393473022461" />
                  <Point X="-28.046146484375" Y="-1.390069091797" />
                  <Point X="-28.043345703125" Y="-1.359626953125" />
                  <Point X="-28.045560546875" Y="-1.327961547852" />
                  <Point X="-28.052564453125" Y="-1.298224487305" />
                  <Point X="-28.06864453125" Y="-1.272248291016" />
                  <Point X="-28.089474609375" Y="-1.24829699707" />
                  <Point X="-28.11297265625" Y="-1.228766235352" />
                  <Point X="-28.1364453125" Y="-1.214951538086" />
                  <Point X="-28.139451171875" Y="-1.213182617188" />
                  <Point X="-28.16870703125" Y="-1.201960205078" />
                  <Point X="-28.20059765625" Y="-1.195475952148" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-28.416958984375" Y="-1.218743530273" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.82058203125" Y="-1.045481933594" />
                  <Point X="-29.834076171875" Y="-0.992650878906" />
                  <Point X="-29.892423828125" Y="-0.584698303223" />
                  <Point X="-29.8479765625" Y="-0.572788818359" />
                  <Point X="-28.532875" Y="-0.220408462524" />
                  <Point X="-28.5174921875" Y="-0.214827133179" />
                  <Point X="-28.4877265625" Y="-0.199468673706" />
                  <Point X="-28.463142578125" Y="-0.182405807495" />
                  <Point X="-28.45999609375" Y="-0.180223022461" />
                  <Point X="-28.43753515625" Y="-0.158339859009" />
                  <Point X="-28.41828125" Y="-0.13207800293" />
                  <Point X="-28.404166015625" Y="-0.104066452026" />
                  <Point X="-28.395966796875" Y="-0.077647789001" />
                  <Point X="-28.3948984375" Y="-0.074200775146" />
                  <Point X="-28.39064453125" Y="-0.046084190369" />
                  <Point X="-28.390646484375" Y="-0.016453187943" />
                  <Point X="-28.394916015625" Y="0.011699375153" />
                  <Point X="-28.403115234375" Y="0.038118034363" />
                  <Point X="-28.404166015625" Y="0.041505542755" />
                  <Point X="-28.418263671875" Y="0.069486732483" />
                  <Point X="-28.4375078125" Y="0.095750419617" />
                  <Point X="-28.459974609375" Y="0.117646942139" />
                  <Point X="-28.48457421875" Y="0.13471925354" />
                  <Point X="-28.49607421875" Y="0.141553924561" />
                  <Point X="-28.514798828125" Y="0.150965835571" />
                  <Point X="-28.532875" Y="0.157848327637" />
                  <Point X="-28.7015390625" Y="0.203041778564" />
                  <Point X="-29.891814453125" Y="0.521975402832" />
                  <Point X="-29.833181640625" Y="0.918215881348" />
                  <Point X="-29.82448828125" Y="0.976971069336" />
                  <Point X="-29.703548828125" Y="1.423267944336" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.744982421875" Y="1.299341796875" />
                  <Point X="-28.723421875" Y="1.301228271484" />
                  <Point X="-28.70313671875" Y="1.305263793945" />
                  <Point X="-28.648693359375" Y="1.3224296875" />
                  <Point X="-28.6417109375" Y="1.324631103516" />
                  <Point X="-28.62277734375" Y="1.332961791992" />
                  <Point X="-28.604033203125" Y="1.343783935547" />
                  <Point X="-28.5873515625" Y="1.35601550293" />
                  <Point X="-28.573712890625" Y="1.371567626953" />
                  <Point X="-28.561298828125" Y="1.389297363281" />
                  <Point X="-28.551349609375" Y="1.407430908203" />
                  <Point X="-28.52950390625" Y="1.460171264648" />
                  <Point X="-28.52669921875" Y="1.466941040039" />
                  <Point X="-28.520908203125" Y="1.486817016602" />
                  <Point X="-28.517154296875" Y="1.508130249023" />
                  <Point X="-28.515806640625" Y="1.528762695312" />
                  <Point X="-28.518953125" Y="1.549198486328" />
                  <Point X="-28.5245546875" Y="1.570102294922" />
                  <Point X="-28.53205078125" Y="1.58937902832" />
                  <Point X="-28.55841015625" Y="1.640014770508" />
                  <Point X="-28.561791015625" Y="1.646508911133" />
                  <Point X="-28.573283203125" Y="1.663709350586" />
                  <Point X="-28.5871953125" Y="1.680288330078" />
                  <Point X="-28.60213671875" Y="1.694590087891" />
                  <Point X="-28.6988828125" Y="1.768826293945" />
                  <Point X="-29.351859375" Y="2.269873291016" />
                  <Point X="-29.114935546875" Y="2.675779296875" />
                  <Point X="-29.081150390625" Y="2.733663574219" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.20665625" Y="2.844671386719" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.07096875" Y="2.819162597656" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.99901171875" Y="2.826505126953" />
                  <Point X="-27.980458984375" Y="2.835655029297" />
                  <Point X="-27.962205078125" Y="2.847284912109" />
                  <Point X="-27.946078125" Y="2.860229248047" />
                  <Point X="-27.892255859375" Y="2.914050048828" />
                  <Point X="-27.885353515625" Y="2.920952636719" />
                  <Point X="-27.872404296875" Y="2.937086425781" />
                  <Point X="-27.860775390625" Y="2.955340820312" />
                  <Point X="-27.85162890625" Y="2.973889648438" />
                  <Point X="-27.8467109375" Y="2.993978515625" />
                  <Point X="-27.843884765625" Y="3.015437255859" />
                  <Point X="-27.84343359375" Y="3.036122802734" />
                  <Point X="-27.850068359375" Y="3.111947509766" />
                  <Point X="-27.85091796875" Y="3.121665527344" />
                  <Point X="-27.854953125" Y="3.141948974609" />
                  <Point X="-27.861462890625" Y="3.162597412109" />
                  <Point X="-27.86979296875" Y="3.181532958984" />
                  <Point X="-27.9126640625" Y="3.255788085938" />
                  <Point X="-28.183330078125" Y="3.724596923828" />
                  <Point X="-27.759458984375" Y="4.049574951172" />
                  <Point X="-27.70062109375" Y="4.094686523438" />
                  <Point X="-27.16703515625" Y="4.391134765625" />
                  <Point X="-27.0431953125" Y="4.229741210938" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.99511328125" Y="4.18939453125" />
                  <Point X="-26.910720703125" Y="4.145462402344" />
                  <Point X="-26.899896484375" Y="4.139827636719" />
                  <Point X="-26.8806171875" Y="4.132331054688" />
                  <Point X="-26.8597109375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.12358203125" />
                  <Point X="-26.818626953125" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.777453125" Y="4.134481445312" />
                  <Point X="-26.689552734375" Y="4.170891601563" />
                  <Point X="-26.678279296875" Y="4.175561035156" />
                  <Point X="-26.6601484375" Y="4.185507324219" />
                  <Point X="-26.64241796875" Y="4.197920898438" />
                  <Point X="-26.626865234375" Y="4.21155859375" />
                  <Point X="-26.614634765625" Y="4.228239257812" />
                  <Point X="-26.6038125" Y="4.246981933594" />
                  <Point X="-26.595478515625" Y="4.265919433594" />
                  <Point X="-26.566869140625" Y="4.356658691406" />
                  <Point X="-26.56319921875" Y="4.368295898438" />
                  <Point X="-26.5591640625" Y="4.38858203125" />
                  <Point X="-26.55727734375" Y="4.410145996094" />
                  <Point X="-26.557728515625" Y="4.430828613281" />
                  <Point X="-26.562603515625" Y="4.467850097656" />
                  <Point X="-26.584201171875" Y="4.6318984375" />
                  <Point X="-26.02580078125" Y="4.788454589844" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.133904296875" Y="4.286315917969" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.83181640625" Y="4.368296386719" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.222470703125" Y="4.838704589844" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.585498046875" Y="4.694012207031" />
                  <Point X="-23.51897265625" Y="4.677950683594" />
                  <Point X="-23.14782421875" Y="4.543333007813" />
                  <Point X="-23.105357421875" Y="4.527928710938" />
                  <Point X="-22.74630859375" Y="4.360015136719" />
                  <Point X="-22.705419921875" Y="4.340893554688" />
                  <Point X="-22.358548828125" Y="4.1388046875" />
                  <Point X="-22.319021484375" Y="4.115775878906" />
                  <Point X="-22.05673828125" Y="3.929253662109" />
                  <Point X="-22.088732421875" Y="3.873837402344" />
                  <Point X="-22.852416015625" Y="2.551098144531" />
                  <Point X="-22.857921875" Y="2.539932861328" />
                  <Point X="-22.866919921875" Y="2.516061279297" />
                  <Point X="-22.88594921875" Y="2.444901367188" />
                  <Point X="-22.8883203125" Y="2.433059326172" />
                  <Point X="-22.8921015625" Y="2.405028808594" />
                  <Point X="-22.892271484375" Y="2.380955566406" />
                  <Point X="-22.8848515625" Y="2.319422607422" />
                  <Point X="-22.883900390625" Y="2.311530761719" />
                  <Point X="-22.87855859375" Y="2.289611328125" />
                  <Point X="-22.87029296875" Y="2.267521240234" />
                  <Point X="-22.859927734375" Y="2.247470947266" />
                  <Point X="-22.8218515625" Y="2.191358886719" />
                  <Point X="-22.81430859375" Y="2.181657226562" />
                  <Point X="-22.796123046875" Y="2.161157714844" />
                  <Point X="-22.7783984375" Y="2.145591308594" />
                  <Point X="-22.722287109375" Y="2.107516845703" />
                  <Point X="-22.71508984375" Y="2.102633789063" />
                  <Point X="-22.695041015625" Y="2.092269775391" />
                  <Point X="-22.672955078125" Y="2.084005371094" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.589501953125" Y="2.071243652344" />
                  <Point X="-22.57676171875" Y="2.070570068359" />
                  <Point X="-22.54996875" Y="2.070955810547" />
                  <Point X="-22.52679296875" Y="2.074170898438" />
                  <Point X="-22.4556328125" Y="2.093200195312" />
                  <Point X="-22.44872265625" Y="2.095333007812" />
                  <Point X="-22.42751171875" Y="2.102775146484" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-22.2418203125" Y="2.208089111328" />
                  <Point X="-21.032673828125" Y="2.906190673828" />
                  <Point X="-20.90017578125" Y="2.722049560547" />
                  <Point X="-20.876720703125" Y="2.689452392578" />
                  <Point X="-20.73780078125" Y="2.459884277344" />
                  <Point X="-20.76375390625" Y="2.439969238281" />
                  <Point X="-21.76921484375" Y="1.668451538086" />
                  <Point X="-21.778568359375" Y="1.660246704102" />
                  <Point X="-21.79602734375" Y="1.641627319336" />
                  <Point X="-21.847240234375" Y="1.574814819336" />
                  <Point X="-21.8536875" Y="1.565250976562" />
                  <Point X="-21.868724609375" Y="1.539733032227" />
                  <Point X="-21.878369140625" Y="1.517090087891" />
                  <Point X="-21.897447265625" Y="1.448874389648" />
                  <Point X="-21.89989453125" Y="1.440125488281" />
                  <Point X="-21.90334765625" Y="1.417827026367" />
                  <Point X="-21.9041640625" Y="1.394257202148" />
                  <Point X="-21.902259765625" Y="1.37176953125" />
                  <Point X="-21.88659765625" Y="1.295870849609" />
                  <Point X="-21.883513671875" Y="1.284525268555" />
                  <Point X="-21.874306640625" Y="1.25741003418" />
                  <Point X="-21.863716796875" Y="1.235740966797" />
                  <Point X="-21.821123046875" Y="1.170998901367" />
                  <Point X="-21.81566015625" Y="1.162695678711" />
                  <Point X="-21.801107421875" Y="1.145452270508" />
                  <Point X="-21.783865234375" Y="1.129362915039" />
                  <Point X="-21.76565234375" Y="1.11603503418" />
                  <Point X="-21.70392578125" Y="1.081288818359" />
                  <Point X="-21.69289453125" Y="1.075983764648" />
                  <Point X="-21.667001953125" Y="1.065529174805" />
                  <Point X="-21.643880859375" Y="1.059438476562" />
                  <Point X="-21.560423828125" Y="1.048408691406" />
                  <Point X="-21.553630859375" Y="1.047758178711" />
                  <Point X="-21.529849609375" Y="1.046340087891" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-21.35064453125" Y="1.068200439453" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.164134765625" Y="0.974176879883" />
                  <Point X="-20.154060546875" Y="0.932790161133" />
                  <Point X="-20.1091328125" Y="0.644238830566" />
                  <Point X="-20.131138671875" Y="0.638342468262" />
                  <Point X="-21.283419921875" Y="0.329589874268" />
                  <Point X="-21.295212890625" Y="0.325584899902" />
                  <Point X="-21.318453125" Y="0.315067657471" />
                  <Point X="-21.400447265625" Y="0.267673522949" />
                  <Point X="-21.40966796875" Y="0.261603118896" />
                  <Point X="-21.434498046875" Y="0.243101852417" />
                  <Point X="-21.452470703125" Y="0.225574462891" />
                  <Point X="-21.50166796875" Y="0.162886703491" />
                  <Point X="-21.5079765625" Y="0.154846817017" />
                  <Point X="-21.519701171875" Y="0.135564727783" />
                  <Point X="-21.52947265625" Y="0.11410521698" />
                  <Point X="-21.536318359375" Y="0.092605018616" />
                  <Point X="-21.552716796875" Y="0.006976568699" />
                  <Point X="-21.55417578125" Y="-0.00420142746" />
                  <Point X="-21.556279296875" Y="-0.033995754242" />
                  <Point X="-21.5548203125" Y="-0.058554679871" />
                  <Point X="-21.538421875" Y="-0.144182983398" />
                  <Point X="-21.536318359375" Y="-0.155165161133" />
                  <Point X="-21.52947265625" Y="-0.176665512085" />
                  <Point X="-21.519701171875" Y="-0.19812487793" />
                  <Point X="-21.5079765625" Y="-0.217406814575" />
                  <Point X="-21.458779296875" Y="-0.280094573975" />
                  <Point X="-21.45095703125" Y="-0.28888079834" />
                  <Point X="-21.430333984375" Y="-0.309343505859" />
                  <Point X="-21.410962890625" Y="-0.324155212402" />
                  <Point X="-21.32896875" Y="-0.371549346924" />
                  <Point X="-21.3232734375" Y="-0.374588348389" />
                  <Point X="-21.300677734375" Y="-0.3856746521" />
                  <Point X="-21.283419921875" Y="-0.392150024414" />
                  <Point X="-21.13563671875" Y="-0.431748199463" />
                  <Point X="-20.108525390625" Y="-0.706961975098" />
                  <Point X="-20.1393515625" Y="-0.911428894043" />
                  <Point X="-20.144974609375" Y="-0.948726074219" />
                  <Point X="-20.198822265625" Y="-1.18469934082" />
                  <Point X="-20.239296875" Y="-1.17937097168" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710327148" />
                  <Point X="-21.62533984375" Y="-1.005508850098" />
                  <Point X="-21.786265625" Y="-1.040486572266" />
                  <Point X="-21.806904296875" Y="-1.044972412109" />
                  <Point X="-21.8360234375" Y="-1.056596313477" />
                  <Point X="-21.8638515625" Y="-1.073489013672" />
                  <Point X="-21.8876015625" Y="-1.093960327148" />
                  <Point X="-21.98487109375" Y="-1.210944824219" />
                  <Point X="-21.997345703125" Y="-1.225948242188" />
                  <Point X="-22.0120703125" Y="-1.250337036133" />
                  <Point X="-22.023412109375" Y="-1.277724121094" />
                  <Point X="-22.030240234375" Y="-1.305367431641" />
                  <Point X="-22.044181640625" Y="-1.456867675781" />
                  <Point X="-22.04596875" Y="-1.476297851562" />
                  <Point X="-22.043650390625" Y="-1.507565307617" />
                  <Point X="-22.035919921875" Y="-1.539183959961" />
                  <Point X="-22.023548828125" Y="-1.567996704102" />
                  <Point X="-21.934490234375" Y="-1.706521240234" />
                  <Point X="-21.923068359375" Y="-1.724287231445" />
                  <Point X="-21.9130625" Y="-1.737241943359" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.7522265625" Y="-1.866142578125" />
                  <Point X="-20.786875" Y="-2.6068828125" />
                  <Point X="-20.859337890625" Y="-2.724136962891" />
                  <Point X="-20.8751875" Y="-2.749785400391" />
                  <Point X="-20.971017578125" Y="-2.8859453125" />
                  <Point X="-21.00887109375" Y="-2.864090576172" />
                  <Point X="-22.199044921875" Y="-2.176942871094" />
                  <Point X="-22.21387109375" Y="-2.170011474609" />
                  <Point X="-22.245775390625" Y="-2.159825195312" />
                  <Point X="-22.43730078125" Y="-2.125235595703" />
                  <Point X="-22.461865234375" Y="-2.120799560547" />
                  <Point X="-22.49321875" Y="-2.120395507812" />
                  <Point X="-22.525390625" Y="-2.125353271484" />
                  <Point X="-22.555166015625" Y="-2.135176757812" />
                  <Point X="-22.71427734375" Y="-2.218916015625" />
                  <Point X="-22.73468359375" Y="-2.229655761719" />
                  <Point X="-22.757615234375" Y="-2.246548828125" />
                  <Point X="-22.77857421875" Y="-2.2675078125" />
                  <Point X="-22.795466796875" Y="-2.290437988281" />
                  <Point X="-22.87920703125" Y="-2.449549560547" />
                  <Point X="-22.889947265625" Y="-2.469956054688" />
                  <Point X="-22.899771484375" Y="-2.499735839844" />
                  <Point X="-22.904728515625" Y="-2.531911865234" />
                  <Point X="-22.90432421875" Y="-2.563260986328" />
                  <Point X="-22.869734375" Y="-2.754787597656" />
                  <Point X="-22.865296875" Y="-2.779351318359" />
                  <Point X="-22.86101171875" Y="-2.795146484375" />
                  <Point X="-22.8481796875" Y="-2.826078125" />
                  <Point X="-22.760052734375" Y="-2.978720458984" />
                  <Point X="-22.13871484375" Y="-4.054905761719" />
                  <Point X="-22.199337890625" Y="-4.098206542969" />
                  <Point X="-22.21817578125" Y="-4.111661132813" />
                  <Point X="-22.298232421875" Y="-4.163481933594" />
                  <Point X="-22.333240234375" Y="-4.117860351563" />
                  <Point X="-23.241451171875" Y="-2.934254882812" />
                  <Point X="-23.25249609375" Y="-2.922177490234" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.46697265625" Y="-2.779113769531" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.520009765625" Y="-2.751167236328" />
                  <Point X="-23.551630859375" Y="-2.743435546875" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.789490234375" Y="-2.760127441406" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397949219" />
                  <Point X="-23.871021484375" Y="-2.780741455078" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-24.054927734375" Y="-2.928100341797" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968861328125" />
                  <Point X="-24.11275" Y="-2.996687744141" />
                  <Point X="-24.124375" Y="-3.02580859375" />
                  <Point X="-24.172072265625" Y="-3.245252197266" />
                  <Point X="-24.178189453125" Y="-3.273396484375" />
                  <Point X="-24.180275390625" Y="-3.289627685547" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.155283203125" Y="-3.512822509766" />
                  <Point X="-23.977935546875" Y="-4.859915527344" />
                  <Point X="-24.00652734375" Y="-4.866183105469" />
                  <Point X="-24.024318359375" Y="-4.870083007812" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.031443359375" Y="-4.757874511719" />
                  <Point X="-26.05842578125" Y="-4.752637207031" />
                  <Point X="-26.14124609375" Y="-4.731328613281" />
                  <Point X="-26.1207734375" Y="-4.57583984375" />
                  <Point X="-26.120076171875" Y="-4.568099121094" />
                  <Point X="-26.11944921875" Y="-4.541030273438" />
                  <Point X="-26.1217578125" Y="-4.509323730469" />
                  <Point X="-26.123333984375" Y="-4.497688476562" />
                  <Point X="-26.176982421875" Y="-4.227986816406" />
                  <Point X="-26.18386328125" Y="-4.193396972656" />
                  <Point X="-26.188125" Y="-4.178467773438" />
                  <Point X="-26.19902734375" Y="-4.149500488281" />
                  <Point X="-26.20566796875" Y="-4.135462402344" />
                  <Point X="-26.221740234375" Y="-4.107625488281" />
                  <Point X="-26.23057421875" Y="-4.094860595703" />
                  <Point X="-26.25020703125" Y="-4.070937255859" />
                  <Point X="-26.261005859375" Y="-4.059779052734" />
                  <Point X="-26.46775" Y="-3.878468261719" />
                  <Point X="-26.494265625" Y="-3.85521484375" />
                  <Point X="-26.50673828125" Y="-3.845965332031" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.9112109375" Y="-3.778184570312" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.992732421875" Y="-3.779166503906" />
                  <Point X="-27.008009765625" Y="-3.781946533203" />
                  <Point X="-27.03905859375" Y="-3.790266845703" />
                  <Point X="-27.053677734375" Y="-3.795497802734" />
                  <Point X="-27.0818671875" Y="-3.808270751953" />
                  <Point X="-27.0954375" Y="-3.815812744141" />
                  <Point X="-27.324080078125" Y="-3.968586914062" />
                  <Point X="-27.35340234375" Y="-3.988180664062" />
                  <Point X="-27.359677734375" Y="-3.992755859375" />
                  <Point X="-27.38044140625" Y="-4.010131347656" />
                  <Point X="-27.402755859375" Y="-4.032768798828" />
                  <Point X="-27.410466796875" Y="-4.041624511719" />
                  <Point X="-27.436033203125" Y="-4.074940917969" />
                  <Point X="-27.503203125" Y="-4.162477539063" />
                  <Point X="-27.708029296875" Y="-4.035653808594" />
                  <Point X="-27.747587890625" Y="-4.011160400391" />
                  <Point X="-27.980861328125" Y="-3.831546630859" />
                  <Point X="-27.34148828125" Y="-2.724118652344" />
                  <Point X="-27.3348515625" Y="-2.710084960938" />
                  <Point X="-27.32394921875" Y="-2.681119384766" />
                  <Point X="-27.319685546875" Y="-2.666188964844" />
                  <Point X="-27.3134140625" Y="-2.634662109375" />
                  <Point X="-27.311638671875" Y="-2.619238769531" />
                  <Point X="-27.310625" Y="-2.588304931641" />
                  <Point X="-27.3146640625" Y="-2.557616943359" />
                  <Point X="-27.3236484375" Y="-2.527999023438" />
                  <Point X="-27.32935546875" Y="-2.51355859375" />
                  <Point X="-27.343572265625" Y="-2.484729492188" />
                  <Point X="-27.3515546875" Y="-2.471412353516" />
                  <Point X="-27.36958203125" Y="-2.446254394531" />
                  <Point X="-27.379626953125" Y="-2.434413574219" />
                  <Point X="-27.394990234375" Y="-2.419050292969" />
                  <Point X="-27.408765625" Y="-2.407063720703" />
                  <Point X="-27.43391796875" Y="-2.389030273438" />
                  <Point X="-27.447234375" Y="-2.381044921875" />
                  <Point X="-27.476068359375" Y="-2.366816162109" />
                  <Point X="-27.490509765625" Y="-2.361104736328" />
                  <Point X="-27.5201328125" Y="-2.352111572266" />
                  <Point X="-27.550826171875" Y="-2.348065429688" />
                  <Point X="-27.581767578125" Y="-2.34907421875" />
                  <Point X="-27.597197265625" Y="-2.350847412109" />
                  <Point X="-27.628732421875" Y="-2.3571171875" />
                  <Point X="-27.643666015625" Y="-2.361380859375" />
                  <Point X="-27.672640625" Y="-2.372284667969" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-27.833251953125" Y="-2.463547119141" />
                  <Point X="-28.7930859375" Y="-3.017707763672" />
                  <Point X="-28.972765625" Y="-2.781646972656" />
                  <Point X="-29.00402734375" Y="-2.740575195312" />
                  <Point X="-29.181265625" Y="-2.443374267578" />
                  <Point X="-28.048123046875" Y="-1.573883056641" />
                  <Point X="-28.036482421875" Y="-1.563309936523" />
                  <Point X="-28.015107421875" Y="-1.540392211914" />
                  <Point X="-28.005373046875" Y="-1.528047363281" />
                  <Point X="-27.987408203125" Y="-1.500918334961" />
                  <Point X="-27.979841796875" Y="-1.48713684082" />
                  <Point X="-27.967083984375" Y="-1.458508789062" />
                  <Point X="-27.961892578125" Y="-1.443662353516" />
                  <Point X="-27.9550625" Y="-1.417296264648" />
                  <Point X="-27.951546875" Y="-1.398772705078" />
                  <Point X="-27.94874609375" Y="-1.368330566406" />
                  <Point X="-27.948578125" Y="-1.352998413086" />
                  <Point X="-27.95079296875" Y="-1.321333007812" />
                  <Point X="-27.95308984375" Y="-1.306182373047" />
                  <Point X="-27.96009375" Y="-1.2764453125" />
                  <Point X="-27.9717890625" Y="-1.248221801758" />
                  <Point X="-27.987869140625" Y="-1.222245483398" />
                  <Point X="-27.9969609375" Y="-1.20990637207" />
                  <Point X="-28.017791015625" Y="-1.185955078125" />
                  <Point X="-28.02875" Y="-1.17523815918" />
                  <Point X="-28.052248046875" Y="-1.155707397461" />
                  <Point X="-28.064787109375" Y="-1.146893554688" />
                  <Point X="-28.088259765625" Y="-1.133078735352" />
                  <Point X="-28.105427734375" Y="-1.12448449707" />
                  <Point X="-28.13468359375" Y="-1.113262207031" />
                  <Point X="-28.14977734375" Y="-1.108865112305" />
                  <Point X="-28.18166796875" Y="-1.102380859375" />
                  <Point X="-28.197287109375" Y="-1.100533691406" />
                  <Point X="-28.2286171875" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-28.429359375" Y="-1.124556274414" />
                  <Point X="-29.66091796875" Y="-1.286694213867" />
                  <Point X="-29.728537109375" Y="-1.021970825195" />
                  <Point X="-29.740759765625" Y="-0.974114624023" />
                  <Point X="-29.786451171875" Y="-0.654654541016" />
                  <Point X="-28.508287109375" Y="-0.312171478271" />
                  <Point X="-28.50047265625" Y="-0.309711914062" />
                  <Point X="-28.473931640625" Y="-0.299251251221" />
                  <Point X="-28.444166015625" Y="-0.283892700195" />
                  <Point X="-28.43355859375" Y="-0.277512786865" />
                  <Point X="-28.4089921875" Y="-0.260462158203" />
                  <Point X="-28.393701171875" Y="-0.248267654419" />
                  <Point X="-28.371240234375" Y="-0.226384414673" />
                  <Point X="-28.360919921875" Y="-0.2145103302" />
                  <Point X="-28.341666015625" Y="-0.188248519897" />
                  <Point X="-28.333443359375" Y="-0.174828323364" />
                  <Point X="-28.319328125" Y="-0.146816772461" />
                  <Point X="-28.313435546875" Y="-0.132225418091" />
                  <Point X="-28.305236328125" Y="-0.105806655884" />
                  <Point X="-28.300966796875" Y="-0.088412124634" />
                  <Point X="-28.296712890625" Y="-0.060295497894" />
                  <Point X="-28.29564453125" Y="-0.046077934265" />
                  <Point X="-28.295646484375" Y="-0.016446987152" />
                  <Point X="-28.296720703125" Y="-0.00220861578" />
                  <Point X="-28.300990234375" Y="0.02594383049" />
                  <Point X="-28.304185546875" Y="0.039858352661" />
                  <Point X="-28.312384765625" Y="0.06627696228" />
                  <Point X="-28.319326171875" Y="0.08425038147" />
                  <Point X="-28.333423828125" Y="0.112231460571" />
                  <Point X="-28.3416328125" Y="0.125636062622" />
                  <Point X="-28.360876953125" Y="0.151899810791" />
                  <Point X="-28.371201171875" Y="0.163783401489" />
                  <Point X="-28.39366796875" Y="0.185680007935" />
                  <Point X="-28.405810546875" Y="0.19569303894" />
                  <Point X="-28.43041015625" Y="0.212765350342" />
                  <Point X="-28.4360390625" Y="0.216385055542" />
                  <Point X="-28.453408203125" Y="0.226434341431" />
                  <Point X="-28.4721328125" Y="0.235846328735" />
                  <Point X="-28.480994140625" Y="0.239748123169" />
                  <Point X="-28.508287109375" Y="0.249611343384" />
                  <Point X="-28.676951171875" Y="0.294804748535" />
                  <Point X="-29.7854453125" Y="0.591824829102" />
                  <Point X="-29.739205078125" Y="0.904309936523" />
                  <Point X="-29.73133203125" Y="0.957524414062" />
                  <Point X="-29.633583984375" Y="1.318237060547" />
                  <Point X="-28.778064453125" Y="1.20560559082" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.7470546875" Y="1.204364379883" />
                  <Point X="-28.736701171875" Y="1.204703369141" />
                  <Point X="-28.715140625" Y="1.20658984375" />
                  <Point X="-28.70488671875" Y="1.208054321289" />
                  <Point X="-28.6846015625" Y="1.21208972168" />
                  <Point X="-28.6745703125" Y="1.214660766602" />
                  <Point X="-28.620126953125" Y="1.231826538086" />
                  <Point X="-28.603451171875" Y="1.237676025391" />
                  <Point X="-28.584517578125" Y="1.246006713867" />
                  <Point X="-28.57527734375" Y="1.250689819336" />
                  <Point X="-28.556533203125" Y="1.261511962891" />
                  <Point X="-28.547859375" Y="1.267171875" />
                  <Point X="-28.531177734375" Y="1.279403320312" />
                  <Point X="-28.51592578125" Y="1.293378051758" />
                  <Point X="-28.502287109375" Y="1.308930175781" />
                  <Point X="-28.495892578125" Y="1.317079101562" />
                  <Point X="-28.483478515625" Y="1.334808837891" />
                  <Point X="-28.47801171875" Y="1.343600585938" />
                  <Point X="-28.4680625" Y="1.361734008789" />
                  <Point X="-28.463580078125" Y="1.371076049805" />
                  <Point X="-28.441734375" Y="1.42381640625" />
                  <Point X="-28.4354921875" Y="1.44036706543" />
                  <Point X="-28.429701171875" Y="1.460243164062" />
                  <Point X="-28.42734765625" Y="1.470338378906" />
                  <Point X="-28.42359375" Y="1.491651489258" />
                  <Point X="-28.42235546875" Y="1.501938354492" />
                  <Point X="-28.4210078125" Y="1.522570800781" />
                  <Point X="-28.421912109375" Y="1.543219482422" />
                  <Point X="-28.42505859375" Y="1.563655273438" />
                  <Point X="-28.42719140625" Y="1.573788085938" />
                  <Point X="-28.43279296875" Y="1.594691894531" />
                  <Point X="-28.436013671875" Y="1.604533081055" />
                  <Point X="-28.443509765625" Y="1.623809692383" />
                  <Point X="-28.44778515625" Y="1.633245239258" />
                  <Point X="-28.47414453125" Y="1.683880981445" />
                  <Point X="-28.482798828125" Y="1.699285644531" />
                  <Point X="-28.494291015625" Y="1.716486083984" />
                  <Point X="-28.500509765625" Y="1.724776000977" />
                  <Point X="-28.514421875" Y="1.741354980469" />
                  <Point X="-28.521505859375" Y="1.748916381836" />
                  <Point X="-28.536447265625" Y="1.763218139648" />
                  <Point X="-28.5443046875" Y="1.769958496094" />
                  <Point X="-28.64105078125" Y="1.844194824219" />
                  <Point X="-29.22761328125" Y="2.294280517578" />
                  <Point X="-29.032888671875" Y="2.627889648438" />
                  <Point X="-29.00228515625" Y="2.680321289062" />
                  <Point X="-28.726337890625" Y="3.035013183594" />
                  <Point X="-28.25415625" Y="2.762398925781" />
                  <Point X="-28.24491796875" Y="2.757717285156" />
                  <Point X="-28.225984375" Y="2.749385986328" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.079248046875" Y="2.724524169922" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310791016" />
                  <Point X="-27.976431640625" Y="2.734227783203" />
                  <Point X="-27.9569921875" Y="2.741303466797" />
                  <Point X="-27.938439453125" Y="2.750453369141" />
                  <Point X="-27.929412109375" Y="2.755534667969" />
                  <Point X="-27.911158203125" Y="2.767164550781" />
                  <Point X="-27.902740234375" Y="2.773198242188" />
                  <Point X="-27.88661328125" Y="2.786142578125" />
                  <Point X="-27.878904296875" Y="2.793053222656" />
                  <Point X="-27.82508203125" Y="2.846874023438" />
                  <Point X="-27.811265625" Y="2.861488525391" />
                  <Point X="-27.79831640625" Y="2.877622314453" />
                  <Point X="-27.79228125" Y="2.886044189453" />
                  <Point X="-27.78065234375" Y="2.904298583984" />
                  <Point X="-27.7755703125" Y="2.913326416016" />
                  <Point X="-27.766423828125" Y="2.931875244141" />
                  <Point X="-27.759353515625" Y="2.951299804688" />
                  <Point X="-27.754435546875" Y="2.971388671875" />
                  <Point X="-27.7525234375" Y="2.981573974609" />
                  <Point X="-27.749697265625" Y="3.003032714844" />
                  <Point X="-27.748908203125" Y="3.013365722656" />
                  <Point X="-27.74845703125" Y="3.034051269531" />
                  <Point X="-27.748794921875" Y="3.044403808594" />
                  <Point X="-27.7554296875" Y="3.120228515625" />
                  <Point X="-27.757744140625" Y="3.140201416016" />
                  <Point X="-27.761779296875" Y="3.160484863281" />
                  <Point X="-27.764349609375" Y="3.170513427734" />
                  <Point X="-27.770859375" Y="3.191161865234" />
                  <Point X="-27.774505859375" Y="3.2008515625" />
                  <Point X="-27.7828359375" Y="3.219787109375" />
                  <Point X="-27.78751953125" Y="3.229032958984" />
                  <Point X="-27.830390625" Y="3.303288085938" />
                  <Point X="-28.0593828125" Y="3.699916748047" />
                  <Point X="-27.70165625" Y="3.974183105469" />
                  <Point X="-27.648365234375" Y="4.015042480469" />
                  <Point X="-27.1925234375" Y="4.268296875" />
                  <Point X="-27.118564453125" Y="4.171909179688" />
                  <Point X="-27.1118203125" Y="4.164048828125" />
                  <Point X="-27.097517578125" Y="4.149107421875" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.06509375" Y="4.121898925781" />
                  <Point X="-27.04789453125" Y="4.11040625" />
                  <Point X="-27.03898046875" Y="4.105128417969" />
                  <Point X="-26.954587890625" Y="4.061196533203" />
                  <Point X="-26.934326171875" Y="4.051285644531" />
                  <Point X="-26.915046875" Y="4.0437890625" />
                  <Point X="-26.905205078125" Y="4.040568359375" />
                  <Point X="-26.884298828125" Y="4.034966308594" />
                  <Point X="-26.8741640625" Y="4.032834960938" />
                  <Point X="-26.853720703125" Y="4.029687988281" />
                  <Point X="-26.833052734375" Y="4.028785400391" />
                  <Point X="-26.812412109375" Y="4.030138427734" />
                  <Point X="-26.802130859375" Y="4.031378417969" />
                  <Point X="-26.78081640625" Y="4.03513671875" />
                  <Point X="-26.77073046875" Y="4.037488037109" />
                  <Point X="-26.75087109375" Y="4.043276123047" />
                  <Point X="-26.74109765625" Y="4.046713134766" />
                  <Point X="-26.653197265625" Y="4.083123291016" />
                  <Point X="-26.632587890625" Y="4.092270751953" />
                  <Point X="-26.61445703125" Y="4.102216796875" />
                  <Point X="-26.605662109375" Y="4.107685058594" />
                  <Point X="-26.587931640625" Y="4.120098632812" />
                  <Point X="-26.57978515625" Y="4.1264921875" />
                  <Point X="-26.564232421875" Y="4.140129882813" />
                  <Point X="-26.550251953125" Y="4.155385253906" />
                  <Point X="-26.538021484375" Y="4.172065917969" />
                  <Point X="-26.532365234375" Y="4.180735351563" />
                  <Point X="-26.52154296875" Y="4.199478027344" />
                  <Point X="-26.516859375" Y="4.208715820313" />
                  <Point X="-26.508525390625" Y="4.227653320312" />
                  <Point X="-26.504875" Y="4.237353027344" />
                  <Point X="-26.476265625" Y="4.328092285156" />
                  <Point X="-26.470025390625" Y="4.349762207031" />
                  <Point X="-26.465990234375" Y="4.370048339844" />
                  <Point X="-26.464525390625" Y="4.380301757812" />
                  <Point X="-26.462638671875" Y="4.401865722656" />
                  <Point X="-26.46230078125" Y="4.412217773438" />
                  <Point X="-26.462751953125" Y="4.432900390625" />
                  <Point X="-26.463541015625" Y="4.443230957031" />
                  <Point X="-26.468416015625" Y="4.480252441406" />
                  <Point X="-26.479265625" Y="4.562655761719" />
                  <Point X="-26.000154296875" Y="4.696981933594" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.22566796875" Y="4.261728027344" />
                  <Point X="-25.220435546875" Y="4.247106933594" />
                  <Point X="-25.20766015625" Y="4.218912597656" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.740052734375" Y="4.343708496094" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.232365234375" Y="4.744221191406" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.60779296875" Y="4.601665527344" />
                  <Point X="-23.546404296875" Y="4.586844238281" />
                  <Point X="-23.180216796875" Y="4.454025878906" />
                  <Point X="-23.14174609375" Y="4.440071289062" />
                  <Point X="-22.786552734375" Y="4.273960449219" />
                  <Point X="-22.7495390625" Y="4.256650390625" />
                  <Point X="-22.40637109375" Y="4.056719726562" />
                  <Point X="-22.3705703125" Y="4.035862548828" />
                  <Point X="-22.18221875" Y="3.901915771484" />
                  <Point X="-22.934689453125" Y="2.59859765625" />
                  <Point X="-22.937619140625" Y="2.593114013672" />
                  <Point X="-22.94681640625" Y="2.573440429688" />
                  <Point X="-22.955814453125" Y="2.549568847656" />
                  <Point X="-22.9586953125" Y="2.540603515625" />
                  <Point X="-22.977724609375" Y="2.469443603516" />
                  <Point X="-22.982466796875" Y="2.445759521484" />
                  <Point X="-22.986248046875" Y="2.417729003906" />
                  <Point X="-22.987099609375" Y="2.405699462891" />
                  <Point X="-22.98726953125" Y="2.381626220703" />
                  <Point X="-22.986587890625" Y="2.369582519531" />
                  <Point X="-22.97916796875" Y="2.308049560547" />
                  <Point X="-22.97619921875" Y="2.289037353516" />
                  <Point X="-22.970857421875" Y="2.267117919922" />
                  <Point X="-22.967533203125" Y="2.256318847656" />
                  <Point X="-22.959267578125" Y="2.234228759766" />
                  <Point X="-22.95468359375" Y="2.223894775391" />
                  <Point X="-22.944318359375" Y="2.203844482422" />
                  <Point X="-22.938537109375" Y="2.194128173828" />
                  <Point X="-22.9004609375" Y="2.138016113281" />
                  <Point X="-22.885375" Y="2.118612792969" />
                  <Point X="-22.867189453125" Y="2.09811328125" />
                  <Point X="-22.8588125" Y="2.089777587891" />
                  <Point X="-22.841087890625" Y="2.074211181641" />
                  <Point X="-22.831740234375" Y="2.06698046875" />
                  <Point X="-22.77562890625" Y="2.028905883789" />
                  <Point X="-22.75871484375" Y="2.018242797852" />
                  <Point X="-22.738666015625" Y="2.007878662109" />
                  <Point X="-22.728333984375" Y="2.003295043945" />
                  <Point X="-22.706248046875" Y="1.995030517578" />
                  <Point X="-22.695447265625" Y="1.991706542969" />
                  <Point X="-22.67352734375" Y="1.986364746094" />
                  <Point X="-22.662408203125" Y="1.984346801758" />
                  <Point X="-22.600875" Y="1.976926879883" />
                  <Point X="-22.57539453125" Y="1.975579956055" />
                  <Point X="-22.5486015625" Y="1.975965820312" />
                  <Point X="-22.5369140625" Y="1.976856933594" />
                  <Point X="-22.51373828125" Y="1.980072021484" />
                  <Point X="-22.50225" Y="1.982395751953" />
                  <Point X="-22.43108984375" Y="2.001425048828" />
                  <Point X="-22.41726953125" Y="2.005690673828" />
                  <Point X="-22.39605859375" Y="2.0131328125" />
                  <Point X="-22.38786328125" Y="2.016444702148" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-22.1943203125" Y="2.125816650391" />
                  <Point X="-21.059595703125" Y="2.780950683594" />
                  <Point X="-20.9772890625" Y="2.666563720703" />
                  <Point X="-20.956037109375" Y="2.637028320312" />
                  <Point X="-20.863115234375" Y="2.483472412109" />
                  <Point X="-21.827048828125" Y="1.743819335938" />
                  <Point X="-21.831861328125" Y="1.739868652344" />
                  <Point X="-21.847869140625" Y="1.725227661133" />
                  <Point X="-21.865328125" Y="1.706608276367" />
                  <Point X="-21.87142578125" Y="1.699421142578" />
                  <Point X="-21.922638671875" Y="1.632608642578" />
                  <Point X="-21.935533203125" Y="1.613481201172" />
                  <Point X="-21.9505703125" Y="1.587963256836" />
                  <Point X="-21.956126953125" Y="1.5769609375" />
                  <Point X="-21.965771484375" Y="1.554317993164" />
                  <Point X="-21.969859375" Y="1.542677246094" />
                  <Point X="-21.9889375" Y="1.474461547852" />
                  <Point X="-21.993775390625" Y="1.454663818359" />
                  <Point X="-21.997228515625" Y="1.432365356445" />
                  <Point X="-21.998291015625" Y="1.421115600586" />
                  <Point X="-21.999107421875" Y="1.397545776367" />
                  <Point X="-21.998826171875" Y="1.386241088867" />
                  <Point X="-21.996921875" Y="1.363753417969" />
                  <Point X="-21.995298828125" Y="1.3525703125" />
                  <Point X="-21.97963671875" Y="1.276671630859" />
                  <Point X="-21.97346875" Y="1.25398059082" />
                  <Point X="-21.96426171875" Y="1.226865356445" />
                  <Point X="-21.95966015625" Y="1.215697509766" />
                  <Point X="-21.9490703125" Y="1.194028442383" />
                  <Point X="-21.94308203125" Y="1.183526977539" />
                  <Point X="-21.90048828125" Y="1.118784912109" />
                  <Point X="-21.888259765625" Y="1.101424072266" />
                  <Point X="-21.87370703125" Y="1.084180664062" />
                  <Point X="-21.865919921875" Y="1.075995239258" />
                  <Point X="-21.848677734375" Y="1.059905883789" />
                  <Point X="-21.839966796875" Y="1.052697753906" />
                  <Point X="-21.82175390625" Y="1.039369750977" />
                  <Point X="-21.812251953125" Y="1.033249755859" />
                  <Point X="-21.750525390625" Y="0.998503417969" />
                  <Point X="-21.728462890625" Y="0.987893371582" />
                  <Point X="-21.7025703125" Y="0.977438781738" />
                  <Point X="-21.691201171875" Y="0.973663146973" />
                  <Point X="-21.668080078125" Y="0.96757244873" />
                  <Point X="-21.656328125" Y="0.965257507324" />
                  <Point X="-21.57287109375" Y="0.954227600098" />
                  <Point X="-21.55928515625" Y="0.952926574707" />
                  <Point X="-21.53550390625" Y="0.951508544922" />
                  <Point X="-21.5264609375" Y="0.951400512695" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-21.338244140625" Y="0.974013183594" />
                  <Point X="-20.295296875" Y="1.111319824219" />
                  <Point X="-20.256439453125" Y="0.951705932617" />
                  <Point X="-20.2473125" Y="0.914210144043" />
                  <Point X="-20.216126953125" Y="0.713921142578" />
                  <Point X="-21.3080078125" Y="0.421352966309" />
                  <Point X="-21.31396875" Y="0.4195440979" />
                  <Point X="-21.334380859375" Y="0.412134796143" />
                  <Point X="-21.35762109375" Y="0.401617645264" />
                  <Point X="-21.365994140625" Y="0.397316192627" />
                  <Point X="-21.44798828125" Y="0.349922149658" />
                  <Point X="-21.4664296875" Y="0.337781433105" />
                  <Point X="-21.491259765625" Y="0.31928012085" />
                  <Point X="-21.500826171875" Y="0.31111416626" />
                  <Point X="-21.518798828125" Y="0.293586761475" />
                  <Point X="-21.527205078125" Y="0.284225158691" />
                  <Point X="-21.57640234375" Y="0.221537368774" />
                  <Point X="-21.5891484375" Y="0.204203918457" />
                  <Point X="-21.600873046875" Y="0.184921890259" />
                  <Point X="-21.60616015625" Y="0.174933242798" />
                  <Point X="-21.615931640625" Y="0.153473724365" />
                  <Point X="-21.619994140625" Y="0.142927597046" />
                  <Point X="-21.62683984375" Y="0.121427505493" />
                  <Point X="-21.629623046875" Y="0.110473403931" />
                  <Point X="-21.646021484375" Y="0.024845062256" />
                  <Point X="-21.648939453125" Y="0.002489056826" />
                  <Point X="-21.65104296875" Y="-0.02730522728" />
                  <Point X="-21.651111328125" Y="-0.039629486084" />
                  <Point X="-21.64965234375" Y="-0.064188529968" />
                  <Point X="-21.648125" Y="-0.076423164368" />
                  <Point X="-21.6317265625" Y="-0.162051498413" />
                  <Point X="-21.62683984375" Y="-0.183987350464" />
                  <Point X="-21.619994140625" Y="-0.205487731934" />
                  <Point X="-21.615931640625" Y="-0.216034317017" />
                  <Point X="-21.60616015625" Y="-0.237493682861" />
                  <Point X="-21.600873046875" Y="-0.247482330322" />
                  <Point X="-21.5891484375" Y="-0.266764221191" />
                  <Point X="-21.5827109375" Y="-0.276057617188" />
                  <Point X="-21.533513671875" Y="-0.338745269775" />
                  <Point X="-21.517869140625" Y="-0.3563175354" />
                  <Point X="-21.49724609375" Y="-0.37678024292" />
                  <Point X="-21.4880390625" Y="-0.384810333252" />
                  <Point X="-21.46866796875" Y="-0.399621948242" />
                  <Point X="-21.45850390625" Y="-0.406403747559" />
                  <Point X="-21.376509765625" Y="-0.453797973633" />
                  <Point X="-21.365119140625" Y="-0.45987588501" />
                  <Point X="-21.3425234375" Y="-0.470962280273" />
                  <Point X="-21.33405078125" Y="-0.474619598389" />
                  <Point X="-21.3080078125" Y="-0.483912963867" />
                  <Point X="-21.160224609375" Y="-0.523511169434" />
                  <Point X="-20.215119140625" Y="-0.776751342773" />
                  <Point X="-20.2332890625" Y="-0.897266418457" />
                  <Point X="-20.238380859375" Y="-0.931040161133" />
                  <Point X="-20.2721953125" Y="-1.079219970703" />
                  <Point X="-21.563216796875" Y="-0.909253601074" />
                  <Point X="-21.571376953125" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042602539" />
                  <Point X="-21.63327734375" Y="-0.910841003418" />
                  <Point X="-21.645517578125" Y="-0.912676391602" />
                  <Point X="-21.806443359375" Y="-0.947654052734" />
                  <Point X="-21.82708203125" Y="-0.952139892578" />
                  <Point X="-21.842125" Y="-0.95674230957" />
                  <Point X="-21.871244140625" Y="-0.968366271973" />
                  <Point X="-21.8853203125" Y="-0.975387634277" />
                  <Point X="-21.9131484375" Y="-0.992280456543" />
                  <Point X="-21.925875" Y="-1.001530883789" />
                  <Point X="-21.949625" Y="-1.022002197266" />
                  <Point X="-21.9606484375" Y="-1.033223022461" />
                  <Point X="-22.05791796875" Y="-1.150207519531" />
                  <Point X="-22.078673828125" Y="-1.17684753418" />
                  <Point X="-22.0933984375" Y="-1.201236206055" />
                  <Point X="-22.099841796875" Y="-1.21398840332" />
                  <Point X="-22.11118359375" Y="-1.241375488281" />
                  <Point X="-22.115640625" Y="-1.254942993164" />
                  <Point X="-22.12246875" Y="-1.282586303711" />
                  <Point X="-22.12483984375" Y="-1.296662109375" />
                  <Point X="-22.13878125" Y="-1.448162353516" />
                  <Point X="-22.140708984375" Y="-1.483322265625" />
                  <Point X="-22.138390625" Y="-1.51458996582" />
                  <Point X="-22.135931640625" Y="-1.530127441406" />
                  <Point X="-22.128201171875" Y="-1.56174597168" />
                  <Point X="-22.123212890625" Y="-1.576664550781" />
                  <Point X="-22.110841796875" Y="-1.605477294922" />
                  <Point X="-22.103458984375" Y="-1.619371582031" />
                  <Point X="-22.014400390625" Y="-1.757896118164" />
                  <Point X="-22.002978515625" Y="-1.775662109375" />
                  <Point X="-21.99825390625" Y="-1.782358032227" />
                  <Point X="-21.980201171875" Y="-1.804454101562" />
                  <Point X="-21.9565078125" Y="-1.828121337891" />
                  <Point X="-21.947201171875" Y="-1.836277587891" />
                  <Point X="-21.81005859375" Y="-1.941511108398" />
                  <Point X="-20.912826171875" Y="-2.629981201172" />
                  <Point X="-20.940150390625" Y="-2.674194580078" />
                  <Point X="-20.9545078125" Y="-2.697428222656" />
                  <Point X="-20.998724609375" Y="-2.760252441406" />
                  <Point X="-22.151544921875" Y="-2.094670410156" />
                  <Point X="-22.158810546875" Y="-2.090883300781" />
                  <Point X="-22.1849765625" Y="-2.079512207031" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228890625" Y="-2.066337646484" />
                  <Point X="-22.420416015625" Y="-2.031747924805" />
                  <Point X="-22.44498046875" Y="-2.027311889648" />
                  <Point X="-22.460640625" Y="-2.025807495117" />
                  <Point X="-22.491994140625" Y="-2.025403442383" />
                  <Point X="-22.5076875" Y="-2.02650378418" />
                  <Point X="-22.539859375" Y="-2.031461547852" />
                  <Point X="-22.555154296875" Y="-2.035136474609" />
                  <Point X="-22.5849296875" Y="-2.044959838867" />
                  <Point X="-22.59941015625" Y="-2.051108642578" />
                  <Point X="-22.758521484375" Y="-2.134847900391" />
                  <Point X="-22.778927734375" Y="-2.145587646484" />
                  <Point X="-22.791029296875" Y="-2.153169189453" />
                  <Point X="-22.8139609375" Y="-2.170062255859" />
                  <Point X="-22.824791015625" Y="-2.179373779297" />
                  <Point X="-22.84575" Y="-2.200332763672" />
                  <Point X="-22.855060546875" Y="-2.211161132813" />
                  <Point X="-22.871953125" Y="-2.234091308594" />
                  <Point X="-22.87953515625" Y="-2.246193115234" />
                  <Point X="-22.963275390625" Y="-2.4053046875" />
                  <Point X="-22.974015625" Y="-2.425711181641" />
                  <Point X="-22.9801640625" Y="-2.440193603516" />
                  <Point X="-22.98998828125" Y="-2.469973388672" />
                  <Point X="-22.9936640625" Y="-2.485270751953" />
                  <Point X="-22.99862109375" Y="-2.517446777344" />
                  <Point X="-22.999720703125" Y="-2.533136962891" />
                  <Point X="-22.99931640625" Y="-2.564486083984" />
                  <Point X="-22.9978125" Y="-2.580145019531" />
                  <Point X="-22.96322265625" Y="-2.771671630859" />
                  <Point X="-22.95878515625" Y="-2.796235351563" />
                  <Point X="-22.956982421875" Y="-2.804225341797" />
                  <Point X="-22.948759765625" Y="-2.831549072266" />
                  <Point X="-22.935927734375" Y="-2.862480712891" />
                  <Point X="-22.930453125" Y="-2.873577636719" />
                  <Point X="-22.842326171875" Y="-3.026219970703" />
                  <Point X="-22.26410546875" Y="-4.027723144531" />
                  <Point X="-22.27624609375" Y="-4.036083740234" />
                  <Point X="-23.166083984375" Y="-2.876421386719" />
                  <Point X="-23.171345703125" Y="-2.870143066406" />
                  <Point X="-23.191169921875" Y="-2.849624023438" />
                  <Point X="-23.216748046875" Y="-2.828003662109" />
                  <Point X="-23.22669921875" Y="-2.820646728516" />
                  <Point X="-23.41559765625" Y="-2.699203369141" />
                  <Point X="-23.43982421875" Y="-2.683627929688" />
                  <Point X="-23.453716796875" Y="-2.676245605469" />
                  <Point X="-23.48252734375" Y="-2.663874511719" />
                  <Point X="-23.4974453125" Y="-2.658885742188" />
                  <Point X="-23.52906640625" Y="-2.651154052734" />
                  <Point X="-23.54460546875" Y="-2.648695800781" />
                  <Point X="-23.575875" Y="-2.646376953125" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.7981953125" Y="-2.665527099609" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.838775390625" Y="-2.670339355469" />
                  <Point X="-23.86642578125" Y="-2.677171630859" />
                  <Point X="-23.8799921875" Y="-2.681629882812" />
                  <Point X="-23.907376953125" Y="-2.692973388672" />
                  <Point X="-23.920123046875" Y="-2.6994140625" />
                  <Point X="-23.94450390625" Y="-2.714133789062" />
                  <Point X="-23.956138671875" Y="-2.722412841797" />
                  <Point X="-24.1156640625" Y="-2.855052001953" />
                  <Point X="-24.136123046875" Y="-2.872063232422" />
                  <Point X="-24.147345703125" Y="-2.883087890625" />
                  <Point X="-24.16781640625" Y="-2.906837646484" />
                  <Point X="-24.177064453125" Y="-2.919562744141" />
                  <Point X="-24.19395703125" Y="-2.947389160156" />
                  <Point X="-24.20098046875" Y="-2.961466552734" />
                  <Point X="-24.21260546875" Y="-2.990587402344" />
                  <Point X="-24.21720703125" Y="-3.005630859375" />
                  <Point X="-24.264904296875" Y="-3.225074462891" />
                  <Point X="-24.271021484375" Y="-3.25321875" />
                  <Point X="-24.2724140625" Y="-3.261287353516" />
                  <Point X="-24.275275390625" Y="-3.289677490234" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520019531" />
                  <Point X="-24.249470703125" Y="-3.525222412109" />
                  <Point X="-24.166912109375" Y="-4.152317382812" />
                  <Point X="-24.344931640625" Y="-3.487938476562" />
                  <Point X="-24.347392578125" Y="-3.480122314453" />
                  <Point X="-24.357853515625" Y="-3.453578613281" />
                  <Point X="-24.3732109375" Y="-3.423815429688" />
                  <Point X="-24.37958984375" Y="-3.413209716797" />
                  <Point X="-24.52470703125" Y="-3.204124755859" />
                  <Point X="-24.543318359375" Y="-3.177309326172" />
                  <Point X="-24.553330078125" Y="-3.165171386719" />
                  <Point X="-24.57521484375" Y="-3.142716064453" />
                  <Point X="-24.587087890625" Y="-3.132398681641" />
                  <Point X="-24.61334375" Y="-3.113154541016" />
                  <Point X="-24.626755859375" Y="-3.104937988281" />
                  <Point X="-24.654755859375" Y="-3.090829833984" />
                  <Point X="-24.66934375" Y="-3.084938232422" />
                  <Point X="-24.89390234375" Y="-3.015243408203" />
                  <Point X="-24.922703125" Y="-3.006304931641" />
                  <Point X="-24.936623046875" Y="-3.003108886719" />
                  <Point X="-24.964783203125" Y="-2.998839599609" />
                  <Point X="-24.9790234375" Y="-2.997766357422" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.28954296875" Y="-3.075999755859" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.332931640625" Y="-3.090830078125" />
                  <Point X="-25.360931640625" Y="-3.104938232422" />
                  <Point X="-25.37434375" Y="-3.113154785156" />
                  <Point X="-25.400599609375" Y="-3.132398925781" />
                  <Point X="-25.412474609375" Y="-3.142717041016" />
                  <Point X="-25.434359375" Y="-3.165172851562" />
                  <Point X="-25.444369140625" Y="-3.177310546875" />
                  <Point X="-25.589484375" Y="-3.386395263672" />
                  <Point X="-25.608095703125" Y="-3.4132109375" />
                  <Point X="-25.61246875" Y="-3.420131591797" />
                  <Point X="-25.625974609375" Y="-3.445261230469" />
                  <Point X="-25.63877734375" Y="-3.476214111328" />
                  <Point X="-25.64275390625" Y="-3.487937011719" />
                  <Point X="-25.6882890625" Y="-3.657877197266" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.332936188954" Y="-3.96220357631" />
                  <Point X="-22.313281226217" Y="-3.942548613573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.08257432926" Y="-2.711841716616" />
                  <Point X="-20.962555324161" Y="-2.591822711517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.391267471226" Y="-3.886184570156" />
                  <Point X="-22.362456983685" Y="-3.857374082615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.167748956327" Y="-2.662666055258" />
                  <Point X="-21.038574270685" Y="-2.533491369615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.449598753498" Y="-3.810165564003" />
                  <Point X="-22.411632741152" Y="-3.772199551657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.252923583395" Y="-2.6134903939" />
                  <Point X="-21.114593217209" Y="-2.475160027714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.50793003577" Y="-3.734146557849" />
                  <Point X="-22.460808498619" Y="-3.687025020699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.338098210462" Y="-2.564314732541" />
                  <Point X="-21.190612163732" Y="-2.416828685812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.566261318042" Y="-3.658127551696" />
                  <Point X="-22.509984256087" Y="-3.601850489741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.423272837529" Y="-2.515139071183" />
                  <Point X="-21.266631110256" Y="-2.35849734391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.624592600314" Y="-3.582108545542" />
                  <Point X="-22.559160013554" Y="-3.516675958783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.508447464596" Y="-2.465963409825" />
                  <Point X="-21.34265005678" Y="-2.300166002008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.682923882586" Y="-3.506089539389" />
                  <Point X="-22.608335771021" Y="-3.431501427824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.593622091664" Y="-2.416787748467" />
                  <Point X="-21.418669003304" Y="-2.241834660107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.741255164858" Y="-3.430070533235" />
                  <Point X="-22.657511528489" Y="-3.346326896866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.678796718731" Y="-2.367612087109" />
                  <Point X="-21.494687949827" Y="-2.183503318205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.376652534645" Y="-1.065467903023" />
                  <Point X="-20.237698460485" Y="-0.926513828863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.79958644713" Y="-3.354051527082" />
                  <Point X="-22.706687285956" Y="-3.261152365908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.763971345798" Y="-2.31843642575" />
                  <Point X="-21.570706896351" Y="-2.125171976303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.495372966382" Y="-1.049838046334" />
                  <Point X="-20.220771672036" Y="-0.775236751988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.857917729402" Y="-3.278032520928" />
                  <Point X="-22.755863043423" Y="-3.17597783495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.849145972865" Y="-2.269260764392" />
                  <Point X="-21.646725842875" Y="-2.066840634401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.614093398118" Y="-1.034208189645" />
                  <Point X="-20.326730410366" Y="-0.746845201892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.916249011673" Y="-3.202013514775" />
                  <Point X="-22.805038800891" Y="-3.090803303992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.934320599933" Y="-2.220085103034" />
                  <Point X="-21.722744789398" Y="-2.0085092925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.732813829855" Y="-1.018578332956" />
                  <Point X="-20.432689148696" Y="-0.718453651797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.974580293945" Y="-3.125994508621" />
                  <Point X="-22.854214427327" Y="-3.005628642003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.019495227" Y="-2.170909441676" />
                  <Point X="-21.798763735922" Y="-1.950177950598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.851534261592" Y="-1.002948476268" />
                  <Point X="-20.538647887026" Y="-0.690062101701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.032911576217" Y="-3.049975502468" />
                  <Point X="-22.903389642789" Y="-2.92045356904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.104669854067" Y="-2.121733780318" />
                  <Point X="-21.874782574413" Y="-1.891846500663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.970254693328" Y="-0.987318619579" />
                  <Point X="-20.644606625356" Y="-0.661670551606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.188613380438" Y="-4.071327018263" />
                  <Point X="-24.178858892215" Y="-4.06157253004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.091242858489" Y="-2.973956496314" />
                  <Point X="-22.948777269476" Y="-2.831490907301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.193937542508" Y="-2.076651180333" />
                  <Point X="-21.950592139113" Y="-1.833305776938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.088975125065" Y="-0.97168876289" />
                  <Point X="-20.750565363685" Y="-0.63327900151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.217004904334" Y="-3.965368253734" />
                  <Point X="-24.194488731108" Y="-3.942852080508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.149574140761" Y="-2.897937490161" />
                  <Point X="-22.972414169477" Y="-2.720777518876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.304346815966" Y="-2.052710165366" />
                  <Point X="-22.012495579631" Y="-1.760858929031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.207695556801" Y="-0.956058906201" />
                  <Point X="-20.856524102015" Y="-0.604887451415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.245396428231" Y="-3.859409489205" />
                  <Point X="-24.210118570001" Y="-3.824131630975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.215253713109" Y="-2.829266774083" />
                  <Point X="-22.992966214142" Y="-2.606979275117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.418145010209" Y="-2.032158071183" />
                  <Point X="-22.065069982019" Y="-1.679083042993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.326415988538" Y="-0.940429049512" />
                  <Point X="-20.962482840345" Y="-0.576495901319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.273787952128" Y="-3.753450724676" />
                  <Point X="-24.225748408894" Y="-3.705411181443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.296261902564" Y="-2.775924675113" />
                  <Point X="-22.989829594283" Y="-2.469492366832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.555630960949" Y="-2.035293733498" />
                  <Point X="-22.11533941051" Y="-1.595002183059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.445136420274" Y="-0.924799192823" />
                  <Point X="-21.068441578675" Y="-0.548104351224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.302179476024" Y="-3.647491960148" />
                  <Point X="-24.241378247787" Y="-3.58669073191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.378038059746" Y="-2.723350543869" />
                  <Point X="-22.140522665338" Y="-1.485835149461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.563882537951" Y="-0.909195022074" />
                  <Point X="-21.17440033008" Y="-0.519712814203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.330570999921" Y="-3.541533195619" />
                  <Point X="-24.257008039548" Y="-3.467970235245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.461808763051" Y="-2.672770958749" />
                  <Point X="-22.128806623886" Y="-1.339768819584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.71732083022" Y="-0.928283025918" />
                  <Point X="-21.28035916614" Y="-0.491321361838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.364358954217" Y="-3.440970861489" />
                  <Point X="-24.272637780704" Y="-3.349249687977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.570186859983" Y="-2.646798767256" />
                  <Point X="-21.980567744098" Y="-1.05717965137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.9223748794" Y="-0.998986786672" />
                  <Point X="-21.376938341231" Y="-0.453550248503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.417019466814" Y="-3.359281085661" />
                  <Point X="-24.259930748433" Y="-3.20219236728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.715671502001" Y="-2.657933120848" />
                  <Point X="-21.461885690317" Y="-0.404147309164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.390555035599" Y="0.667183345554" />
                  <Point X="-20.233330103151" Y="0.824408278002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.472062995155" Y="-3.279974325577" />
                  <Point X="-24.22261908594" Y="-3.030530416361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.870647738583" Y="-2.678559069005" />
                  <Point X="-21.532251564655" Y="-0.340162895076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.574080846566" Y="0.618007823013" />
                  <Point X="-20.253296161023" Y="0.938792508556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.07485033904" Y="-4.748411381036" />
                  <Point X="-25.945963463165" Y="-4.619524505162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.527106512241" Y="-3.200667554237" />
                  <Point X="-21.590681672858" Y="-0.264242714854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.757606657533" Y="0.568832300471" />
                  <Point X="-20.27959939076" Y="1.046839567244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.133532346464" Y="-4.672743100035" />
                  <Point X="-25.89678738026" Y="-4.43599813383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.590607928545" Y="-3.129818682115" />
                  <Point X="-21.63010768368" Y="-0.169318437251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.9411324685" Y="0.519656777929" />
                  <Point X="-20.35768266957" Y="1.10310657686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.120584038487" Y="-4.525444503632" />
                  <Point X="-25.847611297354" Y="-4.252471762499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.677535382375" Y="-3.08239584752" />
                  <Point X="-21.650194918526" Y="-0.055055383671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.124658279467" Y="0.470481255388" />
                  <Point X="-20.512402170972" Y="1.082737363883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.140563251688" Y="-4.411073428407" />
                  <Point X="-25.798435214449" Y="-4.068945391168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.780064393072" Y="-3.050574569792" />
                  <Point X="-21.632135431092" Y="0.097354392189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.308193075802" Y="0.421296747478" />
                  <Point X="-20.667121672373" Y="1.062368150907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.162853899205" Y="-4.2990137875" />
                  <Point X="-25.749259131543" Y="-3.885419019837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.88259340377" Y="-3.018753292064" />
                  <Point X="-20.821841173775" Y="1.04199893793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.185578566875" Y="-4.187388166744" />
                  <Point X="-25.700083048637" Y="-3.701892648506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.995956897029" Y="-2.997766496898" />
                  <Point X="-20.976560675177" Y="1.021629724954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.229276482124" Y="-4.096735793567" />
                  <Point X="-25.650907519697" Y="-3.51836683114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.17208628838" Y="-3.039545599824" />
                  <Point X="-21.131280176579" Y="1.001260511978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.295989778816" Y="-4.029098801834" />
                  <Point X="-25.444749380478" Y="-3.177858403495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.395695323586" Y="-3.128804346604" />
                  <Point X="-21.285999677981" Y="0.980891299001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.367567621689" Y="-3.966326356282" />
                  <Point X="-21.440719242804" Y="0.960522022604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.439145464563" Y="-3.90355391073" />
                  <Point X="-21.58037254202" Y="0.955219011813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.512391664387" Y="-3.842449822128" />
                  <Point X="-21.695012854531" Y="0.974928987728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.606278477284" Y="-3.8019863466" />
                  <Point X="-21.785884697655" Y="1.018407433029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.728784021894" Y="-3.790141602784" />
                  <Point X="-21.864226953996" Y="1.074415465113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.854870095207" Y="-3.781877387672" />
                  <Point X="-21.921805635087" Y="1.151187072448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.985829541411" Y="-3.778486545451" />
                  <Point X="-21.968372158632" Y="1.238970837328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.503800773204" Y="-4.162107488819" />
                  <Point X="-27.500011629488" Y="-4.158318345102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.282491752472" Y="-3.940798468086" />
                  <Point X="-21.994242405012" Y="1.347450879374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.586775184094" Y="-4.110731611283" />
                  <Point X="-21.98402814425" Y="1.492015428561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.419502911931" Y="2.05654066088" />
                  <Point X="-20.911919987679" Y="2.564123585132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.669749594983" Y="-4.059355733747" />
                  <Point X="-20.963288186455" Y="2.647105674782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.752286645394" Y="-4.007542495732" />
                  <Point X="-21.019507158528" Y="2.725236991134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.828192009078" Y="-3.949097570991" />
                  <Point X="-21.150801462172" Y="2.728292975916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.904097372762" Y="-3.890652646249" />
                  <Point X="-21.468677972111" Y="2.544766754402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.980002736446" Y="-3.832207721508" />
                  <Point X="-21.786554482049" Y="2.361240532889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.799411879357" Y="-3.517266575994" />
                  <Point X="-22.104430991988" Y="2.177714311376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.615886512029" Y="-3.19939092024" />
                  <Point X="-22.407310746163" Y="2.009184845626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.432361144701" Y="-2.881515264486" />
                  <Point X="-22.575264044935" Y="1.975581835279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.31257576807" Y="-2.62737959943" />
                  <Point X="-22.693873215389" Y="1.991322953252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.330593845658" Y="-2.511047388592" />
                  <Point X="-22.784572118102" Y="2.034974338963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.383968636417" Y="-2.430071890926" />
                  <Point X="-22.861472379272" Y="2.092424366219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.462003706416" Y="-2.373756672499" />
                  <Point X="-22.920580759089" Y="2.167666274828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.571331284452" Y="-2.348733962111" />
                  <Point X="-22.96719155031" Y="2.255405772032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.803068085163" Y="-2.446120474396" />
                  <Point X="-22.986629521846" Y="2.370318088922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.120944401182" Y="-2.62964650199" />
                  <Point X="-22.961615900551" Y="2.529681998642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.438820780959" Y="-2.813172593341" />
                  <Point X="-22.808522017354" Y="2.817126170264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.756697160735" Y="-2.996698684692" />
                  <Point X="-22.624996318995" Y="3.135002157049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.844504068889" Y="-2.95015530442" />
                  <Point X="-22.441470620636" Y="3.452878143833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.902569181832" Y="-2.873870128937" />
                  <Point X="-22.257944922277" Y="3.770754130617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.960634294774" Y="-2.797584953454" />
                  <Point X="-22.22833667176" Y="3.93471266956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.016709249305" Y="-2.719309619559" />
                  <Point X="-22.306851152349" Y="3.990548477397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.066899060254" Y="-2.635149142083" />
                  <Point X="-22.386567542724" Y="4.045182375446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.117088871204" Y="-2.550988664607" />
                  <Point X="-28.442927456599" Y="-1.876827250003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.95019660435" Y="-1.384096397754" />
                  <Point X="-22.471459679354" Y="4.094640527242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.167278682153" Y="-2.466828187132" />
                  <Point X="-29.020349200405" Y="-2.319898705383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.965016372898" Y="-1.264565877876" />
                  <Point X="-22.556351654392" Y="4.14409884063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.019289993542" Y="-1.184489210095" />
                  <Point X="-22.64124362943" Y="4.193557154017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.097569419775" Y="-1.128418347903" />
                  <Point X="-22.726135604468" Y="4.243015467404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.203807718577" Y="-1.100306358279" />
                  <Point X="-22.815842954787" Y="4.287658405511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.352257274442" Y="-1.114405625719" />
                  <Point X="-22.907383230792" Y="4.330468417932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.506976742913" Y="-1.134774805764" />
                  <Point X="-22.998923506797" Y="4.373278430352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.661696264757" Y="-1.155144039182" />
                  <Point X="-23.090463782801" Y="4.416088442773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.816415786601" Y="-1.175513272601" />
                  <Point X="-23.185104013051" Y="4.455798500949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.971135308445" Y="-1.19588250602" />
                  <Point X="-23.283694831813" Y="4.491557970613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.125854830289" Y="-1.216251739439" />
                  <Point X="-23.382285650575" Y="4.527317440276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.280574352134" Y="-1.236620972858" />
                  <Point X="-23.480876469336" Y="4.56307690994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.435293873978" Y="-1.256990206276" />
                  <Point X="-28.479911950087" Y="-0.301608282385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.315404392666" Y="-0.137100724964" />
                  <Point X="-23.582697121515" Y="4.595606546187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.590013395822" Y="-1.277359439695" />
                  <Point X="-28.667481427725" Y="-0.354827471598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.299109739255" Y="0.013544216872" />
                  <Point X="-23.690919150561" Y="4.621734805566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.675726046958" Y="-1.228721802406" />
                  <Point X="-28.851007341468" Y="-0.404003096915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.333936170562" Y="0.11306807399" />
                  <Point X="-23.799141222525" Y="4.647863022027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.70306125159" Y="-1.121706718612" />
                  <Point X="-29.03453325521" Y="-0.453178722232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.394767679727" Y="0.186586853251" />
                  <Point X="-23.907363294489" Y="4.673991238489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.730396285862" Y="-1.014691464459" />
                  <Point X="-29.218059168953" Y="-0.50235434755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.477496686947" Y="0.238208134456" />
                  <Point X="-24.015585366453" Y="4.700119454951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.75119685504" Y="-0.901141745212" />
                  <Point X="-29.401585082696" Y="-0.551529972867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.580968771318" Y="0.269086338511" />
                  <Point X="-24.123807438416" Y="4.726247671412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.768008106778" Y="-0.783602708524" />
                  <Point X="-29.585110996439" Y="-0.600705598184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.686927500724" Y="0.297477897531" />
                  <Point X="-24.854725645887" Y="4.129679752367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.804699719353" Y="4.179705678901" />
                  <Point X="-24.239442974107" Y="4.744962424147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.784819358516" Y="-0.666063671836" />
                  <Point X="-29.768636910181" Y="-0.649881223502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.792886234734" Y="0.325869451946" />
                  <Point X="-25.030195668521" Y="4.088560018159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.727243772189" Y="4.39151191449" />
                  <Point X="-24.361056955705" Y="4.757698730975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.898844968744" Y="0.354261006361" />
                  <Point X="-25.127566201546" Y="4.125539773559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.678067761175" Y="4.575038213931" />
                  <Point X="-24.482670937302" Y="4.770435037803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.004803702755" Y="0.382652560776" />
                  <Point X="-25.192906787113" Y="4.194549476417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.62889175016" Y="4.758564513371" />
                  <Point X="-24.604284918899" Y="4.783171344632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.110762436765" Y="0.411044115191" />
                  <Point X="-25.232939737077" Y="4.288866814879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.216721170775" Y="0.439435669606" />
                  <Point X="-25.26133114627" Y="4.394825694112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.322679904785" Y="0.467827224022" />
                  <Point X="-25.289722555463" Y="4.500784573344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.428638638796" Y="0.496218778437" />
                  <Point X="-28.7185674051" Y="1.206290012132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.422316083776" Y="1.502541333457" />
                  <Point X="-25.318113964656" Y="4.606743452577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.534597372806" Y="0.524610332852" />
                  <Point X="-28.844814314329" Y="1.214393391329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.441238554856" Y="1.617969150802" />
                  <Point X="-25.346505373849" Y="4.712702331809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.640556106816" Y="0.553001887267" />
                  <Point X="-28.963534736723" Y="1.23002325736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.487394289521" Y="1.706163704562" />
                  <Point X="-25.417069242896" Y="4.776488751187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.746514840827" Y="0.581393441682" />
                  <Point X="-29.082255159117" Y="1.245653123392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.552025422792" Y="1.775882859717" />
                  <Point X="-25.569227436436" Y="4.758680846073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.770684897223" Y="0.691573673711" />
                  <Point X="-29.200975581511" Y="1.261282989423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.628044147154" Y="1.83421442378" />
                  <Point X="-25.721385629976" Y="4.740872940958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.747351532137" Y="0.849257327223" />
                  <Point X="-29.319696003905" Y="1.276912855455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.704063005676" Y="1.892545853684" />
                  <Point X="-25.873543823516" Y="4.723065035844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.715681799466" Y="1.015277348319" />
                  <Point X="-29.438416426299" Y="1.292542721486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.780081891889" Y="1.950877255896" />
                  <Point X="-28.002831510436" Y="2.728127637349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.753248756536" Y="2.977710391249" />
                  <Point X="-26.643557053263" Y="4.087402094522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.512627780747" Y="4.218331367038" />
                  <Point X="-26.047154414318" Y="4.683804733467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.665741699885" Y="1.199567736325" />
                  <Point X="-29.557136848693" Y="1.308172587518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.856100778103" Y="2.009208658108" />
                  <Point X="-28.135834565394" Y="2.729474870817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.754597016119" Y="3.110712420092" />
                  <Point X="-26.836378784369" Y="4.028930651842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.462611508735" Y="4.402697927476" />
                  <Point X="-26.233846747079" Y="4.631462689132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.932119664316" Y="2.067540060319" />
                  <Point X="-28.242851668205" Y="2.756808056431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.781930595856" Y="3.21772912878" />
                  <Point X="-26.943759720709" Y="4.055900003927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.474348647843" Y="4.525311076793" />
                  <Point X="-26.420539079839" Y="4.579120644797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.00813855053" Y="2.125871462531" />
                  <Point X="-28.32861962699" Y="2.805390386071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.830511889498" Y="3.303498123564" />
                  <Point X="-27.032338944874" Y="4.101671068187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.084157436744" Y="2.184202864743" />
                  <Point X="-28.413794289929" Y="2.854566011558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.879687306936" Y="3.388672994551" />
                  <Point X="-27.108147886867" Y="4.16021241462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.160176322957" Y="2.242534266955" />
                  <Point X="-28.498968952867" Y="2.903741637045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.928862724375" Y="3.473847865537" />
                  <Point X="-27.167294087138" Y="4.235416502775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.206348106403" Y="2.330712771934" />
                  <Point X="-28.584143615805" Y="2.952917262533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.978038141813" Y="3.559022736524" />
                  <Point X="-27.364072290608" Y="4.17298858773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.017981185204" Y="2.653429981559" />
                  <Point X="-28.669318278743" Y="3.00209288802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.027213559252" Y="3.644197607511" />
                  <Point X="-27.682673825989" Y="3.988737340774" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.0001640625" Y="0.001626220703" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.1734140625" Y="-4.862161621094" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.680796875" Y="-3.312459228516" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399658203" />
                  <Point X="-24.95022265625" Y="-3.196704833984" />
                  <Point X="-24.9790234375" Y="-3.187766357422" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.23322265625" Y="-3.257461181641" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285644042969" />
                  <Point X="-25.43339453125" Y="-3.494728759766" />
                  <Point X="-25.452005859375" Y="-3.521544433594" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.50476171875" Y="-3.707052490234" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-26.067646484375" Y="-4.944393066406" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.35158984375" Y="-4.873396972656" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.534756835938" />
                  <Point X="-26.363330078125" Y="-4.265055175781" />
                  <Point X="-26.3702109375" Y="-4.230465332031" />
                  <Point X="-26.386283203125" Y="-4.202628417969" />
                  <Point X="-26.59302734375" Y="-4.021317382812" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.92363671875" Y="-3.967777832031" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791748047" />
                  <Point X="-27.218521484375" Y="-4.126565917969" />
                  <Point X="-27.24784375" Y="-4.146159667969" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.285298828125" Y="-4.190610839844" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.808052734375" Y="-4.197194824219" />
                  <Point X="-27.855830078125" Y="-4.167612792969" />
                  <Point X="-28.216720703125" Y="-3.88973828125" />
                  <Point X="-28.228580078125" Y="-3.880607177734" />
                  <Point X="-28.165845703125" Y="-3.771948730469" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597592773438" />
                  <Point X="-27.513978515625" Y="-2.568763671875" />
                  <Point X="-27.529341796875" Y="-2.553400390625" />
                  <Point X="-27.5313125" Y="-2.551428710938" />
                  <Point X="-27.560146484375" Y="-2.537199951172" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-27.738251953125" Y="-2.628092041016" />
                  <Point X="-28.84295703125" Y="-3.265893798828" />
                  <Point X="-29.123953125" Y="-2.896723632812" />
                  <Point X="-29.161703125" Y="-2.847126953125" />
                  <Point X="-29.420431640625" Y="-2.413279296875" />
                  <Point X="-29.43101953125" Y="-2.395526367188" />
                  <Point X="-29.318244140625" Y="-2.308991699219" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.39601574707" />
                  <Point X="-28.1389921875" Y="-1.369649780273" />
                  <Point X="-28.13811328125" Y="-1.366255493164" />
                  <Point X="-28.140328125" Y="-1.334590209961" />
                  <Point X="-28.161158203125" Y="-1.310638916016" />
                  <Point X="-28.184630859375" Y="-1.29682421875" />
                  <Point X="-28.18763671875" Y="-1.295055297852" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.40455859375" Y="-1.312930786133" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.912626953125" Y="-1.068993041992" />
                  <Point X="-29.927392578125" Y="-1.011188293457" />
                  <Point X="-29.995845703125" Y="-0.532571838379" />
                  <Point X="-29.998396484375" Y="-0.514742248535" />
                  <Point X="-29.872564453125" Y="-0.481025848389" />
                  <Point X="-28.557462890625" Y="-0.128645431519" />
                  <Point X="-28.54189453125" Y="-0.121424766541" />
                  <Point X="-28.517296875" Y="-0.104352302551" />
                  <Point X="-28.514150390625" Y="-0.102169281006" />
                  <Point X="-28.494896484375" Y="-0.075907463074" />
                  <Point X="-28.486697265625" Y="-0.049488822937" />
                  <Point X="-28.48564453125" Y="-0.046090393066" />
                  <Point X="-28.485646484375" Y="-0.016459506989" />
                  <Point X="-28.493845703125" Y="0.009959137917" />
                  <Point X="-28.49489453125" Y="0.01333733654" />
                  <Point X="-28.514138671875" Y="0.039600975037" />
                  <Point X="-28.53873828125" Y="0.056673442841" />
                  <Point X="-28.557462890625" Y="0.066085334778" />
                  <Point X="-28.726126953125" Y="0.111278778076" />
                  <Point X="-29.998185546875" Y="0.452125976563" />
                  <Point X="-29.927158203125" Y="0.932122009277" />
                  <Point X="-29.91764453125" Y="0.996417785645" />
                  <Point X="-29.779849609375" Y="1.504921142578" />
                  <Point X="-29.773515625" Y="1.528298706055" />
                  <Point X="-29.6959609375" Y="1.518088623047" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866699219" />
                  <Point X="-28.677259765625" Y="1.413032714844" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.651533203125" Y="1.426056152344" />
                  <Point X="-28.639119140625" Y="1.443785888672" />
                  <Point X="-28.6172734375" Y="1.496526123047" />
                  <Point X="-28.61446875" Y="1.503295776367" />
                  <Point X="-28.61071484375" Y="1.524609008789" />
                  <Point X="-28.61631640625" Y="1.545512817383" />
                  <Point X="-28.64267578125" Y="1.59614855957" />
                  <Point X="-28.646056640625" Y="1.602642700195" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-28.75671484375" Y="1.693457885742" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.196982421875" Y="2.723668945313" />
                  <Point X="-29.160013671875" Y="2.787006103516" />
                  <Point X="-28.795001953125" Y="3.256177734375" />
                  <Point X="-28.774669921875" Y="3.282310791016" />
                  <Point X="-28.738134765625" Y="3.261216796875" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.062689453125" Y="2.913801025391" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927405273438" />
                  <Point X="-27.9594296875" Y="2.981226074219" />
                  <Point X="-27.95252734375" Y="2.988128662109" />
                  <Point X="-27.9408984375" Y="3.006383056641" />
                  <Point X="-27.938072265625" Y="3.027841796875" />
                  <Point X="-27.94470703125" Y="3.103666503906" />
                  <Point X="-27.945556640625" Y="3.113384521484" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-27.9949375" Y="3.208288085938" />
                  <Point X="-28.30727734375" Y="3.749277099609" />
                  <Point X="-27.81726171875" Y="4.124966796875" />
                  <Point X="-27.752876953125" Y="4.174331054688" />
                  <Point X="-27.1779921875" Y="4.493724121094" />
                  <Point X="-27.141546875" Y="4.513972167969" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.866853515625" Y="4.229728515625" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.218491699219" />
                  <Point X="-26.81380859375" Y="4.22225" />
                  <Point X="-26.725908203125" Y="4.25866015625" />
                  <Point X="-26.714634765625" Y="4.263329589844" />
                  <Point X="-26.696904296875" Y="4.275743164062" />
                  <Point X="-26.68608203125" Y="4.294485839844" />
                  <Point X="-26.65747265625" Y="4.385225097656" />
                  <Point X="-26.653802734375" Y="4.396862304687" />
                  <Point X="-26.651916015625" Y="4.418426269531" />
                  <Point X="-26.656791015625" Y="4.455447753906" />
                  <Point X="-26.68913671875" Y="4.701141113281" />
                  <Point X="-26.051447265625" Y="4.879927246094" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.271158203125" Y="4.984862304688" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.21341796875" Y="4.95012109375" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.923580078125" Y="4.392884277344" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.212576171875" Y="4.933187988281" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.563203125" Y="4.786358886719" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.115431640625" Y="4.632640136719" />
                  <Point X="-23.068966796875" Y="4.615786132813" />
                  <Point X="-22.706064453125" Y="4.446069824219" />
                  <Point X="-22.661302734375" Y="4.42513671875" />
                  <Point X="-22.3107265625" Y="4.220889648438" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-21.9368359375" Y="3.96055859375" />
                  <Point X="-21.9312578125" Y="3.956592285156" />
                  <Point X="-22.0064609375" Y="3.826337158203" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.77514453125" Y="2.491519042969" />
                  <Point X="-22.794173828125" Y="2.420359130859" />
                  <Point X="-22.797955078125" Y="2.392328613281" />
                  <Point X="-22.79053515625" Y="2.330795654297" />
                  <Point X="-22.789583984375" Y="2.322903808594" />
                  <Point X="-22.781318359375" Y="2.300813720703" />
                  <Point X="-22.7432421875" Y="2.244701660156" />
                  <Point X="-22.725056640625" Y="2.224202148438" />
                  <Point X="-22.6689453125" Y="2.186127685547" />
                  <Point X="-22.661748046875" Y="2.181244628906" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.57812890625" Y="2.165560302734" />
                  <Point X="-22.5513359375" Y="2.165946044922" />
                  <Point X="-22.48017578125" Y="2.184975341797" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-22.2893203125" Y="2.290361572266" />
                  <Point X="-21.005751953125" Y="3.031430664062" />
                  <Point X="-20.8230625" Y="2.777535400391" />
                  <Point X="-20.79740234375" Y="2.741873046875" />
                  <Point X="-20.613080078125" Y="2.437279541016" />
                  <Point X="-20.612484375" Y="2.436296630859" />
                  <Point X="-20.705921875" Y="2.364600341797" />
                  <Point X="-21.7113828125" Y="1.593082641602" />
                  <Point X="-21.72062890625" Y="1.583833496094" />
                  <Point X="-21.771841796875" Y="1.517020996094" />
                  <Point X="-21.78687890625" Y="1.491502929688" />
                  <Point X="-21.80595703125" Y="1.423287231445" />
                  <Point X="-21.808404296875" Y="1.414538452148" />
                  <Point X="-21.809220703125" Y="1.39096862793" />
                  <Point X="-21.79355859375" Y="1.315069946289" />
                  <Point X="-21.7843515625" Y="1.287954833984" />
                  <Point X="-21.7417578125" Y="1.223212768555" />
                  <Point X="-21.736294921875" Y="1.214909545898" />
                  <Point X="-21.719052734375" Y="1.1988203125" />
                  <Point X="-21.657326171875" Y="1.16407409668" />
                  <Point X="-21.63143359375" Y="1.153619506836" />
                  <Point X="-21.5479765625" Y="1.14258972168" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-21.363044921875" Y="1.162387695312" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.071830078125" Y="0.996647644043" />
                  <Point X="-20.06080859375" Y="0.951368652344" />
                  <Point X="-20.00272265625" Y="0.578305786133" />
                  <Point X="-20.002138671875" Y="0.574556640625" />
                  <Point X="-20.10655078125" Y="0.546579467773" />
                  <Point X="-21.25883203125" Y="0.237826904297" />
                  <Point X="-21.270912109375" Y="0.232819091797" />
                  <Point X="-21.35290625" Y="0.185424972534" />
                  <Point X="-21.377736328125" Y="0.166923675537" />
                  <Point X="-21.42693359375" Y="0.104235946655" />
                  <Point X="-21.4332421875" Y="0.096196144104" />
                  <Point X="-21.443013671875" Y="0.07473664093" />
                  <Point X="-21.459412109375" Y="-0.010891898155" />
                  <Point X="-21.461515625" Y="-0.040686157227" />
                  <Point X="-21.4451171875" Y="-0.126314544678" />
                  <Point X="-21.443013671875" Y="-0.137296737671" />
                  <Point X="-21.4332421875" Y="-0.158756088257" />
                  <Point X="-21.384044921875" Y="-0.221443969727" />
                  <Point X="-21.363421875" Y="-0.241906646729" />
                  <Point X="-21.281427734375" Y="-0.289300750732" />
                  <Point X="-21.25883203125" Y="-0.300386993408" />
                  <Point X="-21.111048828125" Y="-0.339985137939" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.0454140625" Y="-0.925591186523" />
                  <Point X="-20.051568359375" Y="-0.966413513184" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-20.251697265625" Y="-1.273558227539" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341308594" />
                  <Point X="-21.766087890625" Y="-1.133319091797" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.8145546875" Y="-1.154697631836" />
                  <Point X="-21.91182421875" Y="-1.271682250977" />
                  <Point X="-21.924298828125" Y="-1.286685668945" />
                  <Point X="-21.935640625" Y="-1.314072753906" />
                  <Point X="-21.94958203125" Y="-1.465572875977" />
                  <Point X="-21.951369140625" Y="-1.485003173828" />
                  <Point X="-21.943638671875" Y="-1.516621826172" />
                  <Point X="-21.854580078125" Y="-1.655146362305" />
                  <Point X="-21.843158203125" Y="-1.672912353516" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-21.69439453125" Y="-1.790774047852" />
                  <Point X="-20.660923828125" Y="-2.583784179688" />
                  <Point X="-20.778525390625" Y="-2.774079101562" />
                  <Point X="-20.7958671875" Y="-2.802141357422" />
                  <Point X="-20.943310546875" Y="-3.011638183594" />
                  <Point X="-21.05637109375" Y="-2.94636328125" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.26266015625" Y="-2.253312744141" />
                  <Point X="-22.454185546875" Y="-2.218723144531" />
                  <Point X="-22.47875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.670033203125" Y="-2.302984130859" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.334682861328" />
                  <Point X="-22.795138671875" Y="-2.493794433594" />
                  <Point X="-22.80587890625" Y="-2.514200927734" />
                  <Point X="-22.8108359375" Y="-2.546376953125" />
                  <Point X="-22.77624609375" Y="-2.737903564453" />
                  <Point X="-22.77180859375" Y="-2.762467285156" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.677779296875" Y="-2.931220947266" />
                  <Point X="-22.01332421875" Y="-4.082087646484" />
                  <Point X="-22.14412109375" Y="-4.17551171875" />
                  <Point X="-22.164720703125" Y="-4.190224609375" />
                  <Point X="-22.32022265625" Y="-4.290879882812" />
                  <Point X="-22.408609375" Y="-4.175692871094" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467529297" />
                  <Point X="-23.51834765625" Y="-2.859024169922" />
                  <Point X="-23.54257421875" Y="-2.843448730469" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.78078515625" Y="-2.854727783203" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509521484" />
                  <Point X="-23.99419140625" Y="-3.001148681641" />
                  <Point X="-24.014650390625" Y="-3.018159912109" />
                  <Point X="-24.03154296875" Y="-3.045986328125" />
                  <Point X="-24.079240234375" Y="-3.265429931641" />
                  <Point X="-24.085357421875" Y="-3.29357421875" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-24.061095703125" Y="-3.500422607422" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.986185546875" Y="-4.958979980469" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#206" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.171277830952" Y="4.994342073515" Z="2.25" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.25" />
                  <Point X="-0.283128192046" Y="5.06580346622" Z="2.25" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.25" />
                  <Point X="-1.071101298622" Y="4.959350642157" Z="2.25" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.25" />
                  <Point X="-1.712519868411" Y="4.480202116458" Z="2.25" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.25" />
                  <Point X="-1.711315179565" Y="4.431543099596" Z="2.25" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.25" />
                  <Point X="-1.751198562558" Y="4.336134458792" Z="2.25" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.25" />
                  <Point X="-1.849922576037" Y="4.305358990207" Z="2.25" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.25" />
                  <Point X="-2.111557992981" Y="4.580278730407" Z="2.25" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.25" />
                  <Point X="-2.208432099115" Y="4.568711462616" Z="2.25" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.25" />
                  <Point X="-2.853841234747" Y="4.195926327517" Z="2.25" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.25" />
                  <Point X="-3.044395923203" Y="3.214568065397" Z="2.25" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.25" />
                  <Point X="-3.000673879805" Y="3.130588239903" Z="2.25" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.25" />
                  <Point X="-3.00094265274" Y="3.047860915146" Z="2.25" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.25" />
                  <Point X="-3.064488225704" Y="2.994890819368" Z="2.25" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.25" />
                  <Point X="-3.7192912829" Y="3.335798025543" Z="2.25" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.25" />
                  <Point X="-3.840621815225" Y="3.318160495151" Z="2.25" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.25" />
                  <Point X="-4.246321928298" Y="2.780154074928" Z="2.25" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.25" />
                  <Point X="-3.793308638411" Y="1.685070197349" Z="2.25" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.25" />
                  <Point X="-3.693181665063" Y="1.604340000402" Z="2.25" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.25" />
                  <Point X="-3.669624191117" Y="1.546940339246" Z="2.25" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.25" />
                  <Point X="-3.698452417165" Y="1.491998535844" Z="2.25" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.25" />
                  <Point X="-4.69559308377" Y="1.598940988558" Z="2.25" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.25" />
                  <Point X="-4.834266983479" Y="1.549277389038" Z="2.25" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.25" />
                  <Point X="-4.982776755928" Y="0.970720533935" Z="2.25" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.25" />
                  <Point X="-3.74522522337" Y="0.09426254603" Z="2.25" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.25" />
                  <Point X="-3.573405977029" Y="0.046879449396" Z="2.25" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.25" />
                  <Point X="-3.547756255128" Y="0.026418699103" Z="2.25" />
                  <Point X="-3.539556741714" Y="0" Z="2.25" />
                  <Point X="-3.540608346007" Y="-0.003388251958" Z="2.25" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.25" />
                  <Point X="-3.551962618094" Y="-0.031996533738" Z="2.25" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.25" />
                  <Point X="-4.891662565403" Y="-0.401449510726" Z="2.25" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.25" />
                  <Point X="-5.051498585428" Y="-0.508370739248" Z="2.25" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.25" />
                  <Point X="-4.967229630298" Y="-1.050086884911" Z="2.25" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.25" />
                  <Point X="-3.404189831201" Y="-1.331223089101" Z="2.25" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.25" />
                  <Point X="-3.216148527614" Y="-1.308635074996" Z="2.25" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.25" />
                  <Point X="-3.193552100645" Y="-1.325831282188" Z="2.25" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.25" />
                  <Point X="-4.354838987306" Y="-2.238044054824" Z="2.25" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.25" />
                  <Point X="-4.469532314342" Y="-2.407609243926" Z="2.25" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.25" />
                  <Point X="-4.169897086498" Y="-2.895726504527" Z="2.25" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.25" />
                  <Point X="-2.719409672138" Y="-2.64011329863" Z="2.25" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.25" />
                  <Point X="-2.570867435576" Y="-2.557463033555" Z="2.25" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.25" />
                  <Point X="-3.215303714538" Y="-3.715668710354" Z="2.25" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.25" />
                  <Point X="-3.253382484296" Y="-3.898075873609" Z="2.25" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.25" />
                  <Point X="-2.840532302694" Y="-4.208425750049" Z="2.25" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.25" />
                  <Point X="-2.25178686864" Y="-4.189768592189" Z="2.25" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.25" />
                  <Point X="-2.196898420269" Y="-4.136858575686" Z="2.25" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.25" />
                  <Point X="-1.933064002958" Y="-3.986390975394" Z="2.25" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.25" />
                  <Point X="-1.632151336102" Y="-4.027629447113" Z="2.25" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.25" />
                  <Point X="-1.418525630012" Y="-4.243530324691" Z="2.25" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.25" />
                  <Point X="-1.407617669522" Y="-4.837868045817" Z="2.25" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.25" />
                  <Point X="-1.37948619773" Y="-4.888151544955" Z="2.25" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.25" />
                  <Point X="-1.083330138377" Y="-4.962197009392" Z="2.25" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.25" />
                  <Point X="-0.462622144358" Y="-3.688713384123" Z="2.25" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.25" />
                  <Point X="-0.39847532594" Y="-3.491957521785" Z="2.25" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.25" />
                  <Point X="-0.22455893825" Y="-3.273934175873" Z="2.25" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.25" />
                  <Point X="0.028800141111" Y="-3.213177869415" Z="2.25" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.25" />
                  <Point X="0.271970533492" Y="-3.309688233982" Z="2.25" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.25" />
                  <Point X="0.772132716044" Y="-4.843822645693" Z="2.25" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.25" />
                  <Point X="0.838168144167" Y="-5.010038837474" Z="2.25" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.25" />
                  <Point X="1.018362531082" Y="-4.976540148752" Z="2.25" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.25" />
                  <Point X="0.982320606478" Y="-3.462616284498" Z="2.25" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.25" />
                  <Point X="0.963463020517" Y="-3.244769606698" Z="2.25" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.25" />
                  <Point X="1.031619345882" Y="-3.008314751716" Z="2.25" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.25" />
                  <Point X="1.217639466309" Y="-2.873237280024" Z="2.25" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.25" />
                  <Point X="1.448456883062" Y="-2.869801943144" Z="2.25" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.25" />
                  <Point X="2.545566548883" Y="-4.174850800018" Z="2.25" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.25" />
                  <Point X="2.684238741178" Y="-4.312286120072" Z="2.25" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.25" />
                  <Point X="2.87878498704" Y="-4.184918906084" Z="2.25" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.25" />
                  <Point X="2.359364978702" Y="-2.874940874171" Z="2.25" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.25" />
                  <Point X="2.26680071023" Y="-2.697734926991" Z="2.25" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.25" />
                  <Point X="2.24295085421" Y="-2.485801794938" Z="2.25" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.25" />
                  <Point X="2.347096583435" Y="-2.315950455757" Z="2.25" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.25" />
                  <Point X="2.530771876521" Y="-2.23664729926" Z="2.25" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.25" />
                  <Point X="3.912472722936" Y="-2.958384521573" Z="2.25" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.25" />
                  <Point X="4.084963145513" Y="-3.018311087349" Z="2.25" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.25" />
                  <Point X="4.257851571663" Y="-2.769083743048" Z="2.25" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.25" />
                  <Point X="3.329885661914" Y="-1.719826443428" Z="2.25" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.25" />
                  <Point X="3.181320903751" Y="-1.596826981865" Z="2.25" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.25" />
                  <Point X="3.094050466858" Y="-1.438872304936" Z="2.25" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.25" />
                  <Point X="3.120466418477" Y="-1.272368725657" Z="2.25" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.25" />
                  <Point X="3.238374530194" Y="-1.150898210627" Z="2.25" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.25" />
                  <Point X="4.735620852114" Y="-1.291850407371" Z="2.25" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.25" />
                  <Point X="4.916604357478" Y="-1.27235571892" Z="2.25" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.25" />
                  <Point X="4.997869647486" Y="-0.90176560192" Z="2.25" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.25" />
                  <Point X="3.89573497447" Y="-0.26040877625" Z="2.25" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.25" />
                  <Point X="3.737436792246" Y="-0.214732253899" Z="2.25" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.25" />
                  <Point X="3.649133170242" Y="-0.159298365023" Z="2.25" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.25" />
                  <Point X="3.597833542226" Y="-0.085628501526" Z="2.25" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.25" />
                  <Point X="3.583537908198" Y="0.010982029655" Z="2.25" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.25" />
                  <Point X="3.606246268158" Y="0.104650373552" Z="2.25" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.25" />
                  <Point X="3.665958622106" Y="0.173416590343" Z="2.25" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.25" />
                  <Point X="4.900232842837" Y="0.529563156519" Z="2.25" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.25" />
                  <Point X="5.040523798928" Y="0.617276813127" Z="2.25" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.25" />
                  <Point X="4.970594671" Y="1.039753939291" Z="2.25" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.25" />
                  <Point X="3.624271844719" Y="1.243239946335" Z="2.25" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.25" />
                  <Point X="3.452417853862" Y="1.223438689025" Z="2.25" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.25" />
                  <Point X="3.36104418132" Y="1.238924848015" Z="2.25" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.25" />
                  <Point X="3.293855695102" Y="1.281974333387" Z="2.25" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.25" />
                  <Point X="3.249252508093" Y="1.35645055977" Z="2.25" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.25" />
                  <Point X="3.236038730266" Y="1.4410980069" Z="2.25" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.25" />
                  <Point X="3.261684271265" Y="1.517882548471" Z="2.25" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.25" />
                  <Point X="4.318359423557" Y="2.356212637873" Z="2.25" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.25" />
                  <Point X="4.423539637117" Y="2.494445211766" Z="2.25" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.25" />
                  <Point X="4.211365870517" Y="2.838018476285" Z="2.25" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.25" />
                  <Point X="2.679521921305" Y="2.364942771999" Z="2.25" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.25" />
                  <Point X="2.50075152235" Y="2.264558250185" Z="2.25" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.25" />
                  <Point X="2.421699907824" Y="2.246480773431" Z="2.25" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.25" />
                  <Point X="2.352970282834" Y="2.258783650128" Z="2.25" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.25" />
                  <Point X="2.291974950107" Y="2.304054577548" Z="2.25" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.25" />
                  <Point X="2.252948929327" Y="2.368058536009" Z="2.25" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.25" />
                  <Point X="2.247969692472" Y="2.438718053532" Z="2.25" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.25" />
                  <Point X="3.030682580861" Y="3.83261716065" Z="2.25" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.25" />
                  <Point X="3.08598449439" Y="4.032585860832" Z="2.25" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.25" />
                  <Point X="2.708286009277" Y="4.295372518166" Z="2.25" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.25" />
                  <Point X="2.308959878866" Y="4.522641526216" Z="2.25" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.25" />
                  <Point X="1.895459394655" Y="4.710923812723" Z="2.25" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.25" />
                  <Point X="1.442374959544" Y="4.866242441956" Z="2.25" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.25" />
                  <Point X="0.786475577119" Y="5.014195901209" Z="2.25" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.25" />
                  <Point X="0.021966730601" Y="4.437105432436" Z="2.25" />
                  <Point X="0" Y="4.355124473572" Z="2.25" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>