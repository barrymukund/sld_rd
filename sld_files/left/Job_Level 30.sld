<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#175" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2213" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.20781640625" Y="-4.366711425781" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140136719" />
                  <Point X="-24.457634765625" Y="-3.467376953125" />
                  <Point X="-24.55118359375" Y="-3.332591308594" />
                  <Point X="-24.62136328125" Y="-3.2314765625" />
                  <Point X="-24.643248046875" Y="-3.209020751953" />
                  <Point X="-24.66950390625" Y="-3.189776855469" />
                  <Point X="-24.69750390625" Y="-3.175669433594" />
                  <Point X="-24.842263671875" Y="-3.130740966797" />
                  <Point X="-24.95086328125" Y="-3.097036132812" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.181583984375" Y="-3.141964111328" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.189776855469" />
                  <Point X="-25.344439453125" Y="-3.209020751953" />
                  <Point X="-25.36632421875" Y="-3.231477294922" />
                  <Point X="-25.45987109375" Y="-3.366262695312" />
                  <Point X="-25.53005078125" Y="-3.467377685547" />
                  <Point X="-25.5381875" Y="-3.481572021484" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.722693359375" Y="-4.153327636719" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-25.955140625" Y="-4.869458007812" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.24406640625" Y="-4.802967773438" />
                  <Point X="-26.24641796875" Y="-4.802362792969" />
                  <Point X="-26.242513671875" Y="-4.772712402344" />
                  <Point X="-26.2149609375" Y="-4.5634375" />
                  <Point X="-26.21419921875" Y="-4.547929199219" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.251091796875" Y="-4.342361328125" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.182964355469" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.456921875" Y="-4.014322753906" />
                  <Point X="-26.556904296875" Y="-3.926639404297" />
                  <Point X="-26.5831875" Y="-3.910295898438" />
                  <Point X="-26.612884765625" Y="-3.897994628906" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.819916015625" Y="-3.879372314453" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708740234" />
                  <Point X="-27.014466796875" Y="-3.882028564453" />
                  <Point X="-27.042658203125" Y="-3.894801757812" />
                  <Point X="-27.19005078125" Y="-3.993286865234" />
                  <Point X="-27.300623046875" Y="-4.067169677734" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.3351015625" Y="-4.099461914062" />
                  <Point X="-27.4315" Y="-4.22508984375" />
                  <Point X="-27.4801484375" Y="-4.288488769531" />
                  <Point X="-27.61966015625" Y="-4.202106445313" />
                  <Point X="-27.80171484375" Y="-4.089383300781" />
                  <Point X="-28.0298046875" Y="-3.91376171875" />
                  <Point X="-28.104720703125" Y="-3.856078613281" />
                  <Point X="-27.84910546875" Y="-3.413338623047" />
                  <Point X="-27.423759765625" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647655273438" />
                  <Point X="-27.406587890625" Y="-2.616130126953" />
                  <Point X="-27.40557421875" Y="-2.585197753906" />
                  <Point X="-27.414556640625" Y="-2.555581298828" />
                  <Point X="-27.428771484375" Y="-2.526752929688" />
                  <Point X="-27.446798828125" Y="-2.501592773438" />
                  <Point X="-27.464146484375" Y="-2.484244140625" />
                  <Point X="-27.489302734375" Y="-2.466216064453" />
                  <Point X="-27.5181328125" Y="-2.451997314453" />
                  <Point X="-27.547751953125" Y="-2.44301171875" />
                  <Point X="-27.5786875" Y="-2.444023925781" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.19186328125" Y="-2.780287353516" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-28.939033203125" Y="-2.982819580078" />
                  <Point X="-29.082857421875" Y="-2.79386328125" />
                  <Point X="-29.246384765625" Y="-2.519654541016" />
                  <Point X="-29.306142578125" Y="-2.419450195312" />
                  <Point X="-28.850169921875" Y="-2.069570068359" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.084578125" Y="-1.475594482422" />
                  <Point X="-28.06661328125" Y="-1.448464233398" />
                  <Point X="-28.053857421875" Y="-1.419834960938" />
                  <Point X="-28.04615234375" Y="-1.390087768555" />
                  <Point X="-28.04334765625" Y="-1.359662963867" />
                  <Point X="-28.0455546875" Y="-1.327991699219" />
                  <Point X="-28.0525546875" Y="-1.298242553711" />
                  <Point X="-28.068638671875" Y="-1.272255615234" />
                  <Point X="-28.08947265625" Y="-1.248298706055" />
                  <Point X="-28.11297265625" Y="-1.228766723633" />
                  <Point X="-28.139455078125" Y="-1.213180297852" />
                  <Point X="-28.168716796875" Y="-1.201956176758" />
                  <Point X="-28.200603515625" Y="-1.195474731445" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-28.929634765625" Y="-1.286238647461" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.77782421875" Y="-1.212879638672" />
                  <Point X="-29.834076171875" Y="-0.992650146484" />
                  <Point X="-29.877341796875" Y="-0.690153747559" />
                  <Point X="-29.892423828125" Y="-0.584698364258" />
                  <Point X="-29.3806484375" Y="-0.44756854248" />
                  <Point X="-28.532875" Y="-0.220408309937" />
                  <Point X="-28.50980078125" Y="-0.210823822021" />
                  <Point X="-28.478376953125" Y="-0.192598495483" />
                  <Point X="-28.465201171875" Y="-0.183384674072" />
                  <Point X="-28.441484375" Y="-0.163609725952" />
                  <Point X="-28.42545703125" Y="-0.146474822998" />
                  <Point X="-28.414119140625" Y="-0.125934738159" />
                  <Point X="-28.401408203125" Y="-0.094165161133" />
                  <Point X="-28.3969296875" Y="-0.07973551178" />
                  <Point X="-28.39071875" Y="-0.052139865875" />
                  <Point X="-28.38840234375" Y="-0.030766374588" />
                  <Point X="-28.39094921875" Y="-0.009419221878" />
                  <Point X="-28.39848046875" Y="0.022430803299" />
                  <Point X="-28.40315625" Y="0.036907051086" />
                  <Point X="-28.414546875" Y="0.06442224884" />
                  <Point X="-28.42612890625" Y="0.084825958252" />
                  <Point X="-28.442357421875" Y="0.10176890564" />
                  <Point X="-28.47003515625" Y="0.124293083191" />
                  <Point X="-28.483361328125" Y="0.13337272644" />
                  <Point X="-28.51082421875" Y="0.148848815918" />
                  <Point X="-28.532875" Y="0.157848327637" />
                  <Point X="-29.1688671875" Y="0.328261871338" />
                  <Point X="-29.89181640625" Y="0.521975402832" />
                  <Point X="-29.860740234375" Y="0.731981811523" />
                  <Point X="-29.82448828125" Y="0.976968444824" />
                  <Point X="-29.737396484375" Y="1.298364990234" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-29.373369140625" Y="1.379798828125" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228027344" />
                  <Point X="-28.703134765625" Y="1.305263793945" />
                  <Point X="-28.6680390625" Y="1.316329589844" />
                  <Point X="-28.641708984375" Y="1.324631103516" />
                  <Point X="-28.62277734375" Y="1.332962036133" />
                  <Point X="-28.604033203125" Y="1.343784179688" />
                  <Point X="-28.5873515625" Y="1.35601574707" />
                  <Point X="-28.573712890625" Y="1.371567993164" />
                  <Point X="-28.561298828125" Y="1.389298095703" />
                  <Point X="-28.551349609375" Y="1.407432983398" />
                  <Point X="-28.537267578125" Y="1.441431762695" />
                  <Point X="-28.526703125" Y="1.466937255859" />
                  <Point X="-28.520916015625" Y="1.486788452148" />
                  <Point X="-28.51715625" Y="1.508103759766" />
                  <Point X="-28.515802734375" Y="1.528750244141" />
                  <Point X="-28.518951171875" Y="1.549200195312" />
                  <Point X="-28.5245546875" Y="1.570106811523" />
                  <Point X="-28.53205078125" Y="1.589378295898" />
                  <Point X="-28.54904296875" Y="1.622020263672" />
                  <Point X="-28.561791015625" Y="1.646508056641" />
                  <Point X="-28.573283203125" Y="1.663707763672" />
                  <Point X="-28.5871953125" Y="1.680287231445" />
                  <Point X="-28.60213671875" Y="1.694590087891" />
                  <Point X="-28.966943359375" Y="1.974516479492" />
                  <Point X="-29.351859375" Y="2.269873535156" />
                  <Point X="-29.222015625" Y="2.492325683594" />
                  <Point X="-29.081146484375" Y="2.733665771484" />
                  <Point X="-28.850455078125" Y="3.030188964844" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.579890625" Y="3.060158447266" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.097912109375" Y="2.821520019531" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.04056640625" Y="2.818762939453" />
                  <Point X="-28.019107421875" Y="2.821587890625" />
                  <Point X="-27.999015625" Y="2.826504150391" />
                  <Point X="-27.980462890625" Y="2.83565234375" />
                  <Point X="-27.962208984375" Y="2.847281005859" />
                  <Point X="-27.946076171875" Y="2.860228759766" />
                  <Point X="-27.911380859375" Y="2.894924072266" />
                  <Point X="-27.8853515625" Y="2.920952148438" />
                  <Point X="-27.872404296875" Y="2.937083984375" />
                  <Point X="-27.860775390625" Y="2.955337890625" />
                  <Point X="-27.85162890625" Y="2.973887207031" />
                  <Point X="-27.8467109375" Y="2.993976318359" />
                  <Point X="-27.843884765625" Y="3.015434814453" />
                  <Point X="-27.84343359375" Y="3.03612109375" />
                  <Point X="-27.8477109375" Y="3.085001220703" />
                  <Point X="-27.85091796875" Y="3.121670654297" />
                  <Point X="-27.854955078125" Y="3.141959472656" />
                  <Point X="-27.86146484375" Y="3.162603271484" />
                  <Point X="-27.869794921875" Y="3.181533203125" />
                  <Point X="-28.031453125" Y="3.461531005859" />
                  <Point X="-28.18333203125" Y="3.724595703125" />
                  <Point X="-27.945958984375" Y="3.906587158203" />
                  <Point X="-27.70062109375" Y="4.094685791016" />
                  <Point X="-27.33728515625" Y="4.296547363281" />
                  <Point X="-27.16703515625" Y="4.391134277344" />
                  <Point X="-27.157453125" Y="4.378645507812" />
                  <Point X="-27.0431953125" Y="4.229740722656" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.99511328125" Y="4.189395019531" />
                  <Point X="-26.9407109375" Y="4.16107421875" />
                  <Point X="-26.899896484375" Y="4.139828125" />
                  <Point X="-26.88061328125" Y="4.132330078125" />
                  <Point X="-26.859705078125" Y="4.126728515625" />
                  <Point X="-26.839263671875" Y="4.123582519531" />
                  <Point X="-26.818625" Y="4.124935546875" />
                  <Point X="-26.797310546875" Y="4.128693847656" />
                  <Point X="-26.777451171875" Y="4.134482421875" />
                  <Point X="-26.720787109375" Y="4.157954101562" />
                  <Point X="-26.67827734375" Y="4.175562011719" />
                  <Point X="-26.66014453125" Y="4.185509765625" />
                  <Point X="-26.6424140625" Y="4.197924316406" />
                  <Point X="-26.626859375" Y="4.211565429688" />
                  <Point X="-26.614626953125" Y="4.22825" />
                  <Point X="-26.603806640625" Y="4.246994140625" />
                  <Point X="-26.595478515625" Y="4.265921386719" />
                  <Point X="-26.57703515625" Y="4.324416015625" />
                  <Point X="-26.56319921875" Y="4.368297851562" />
                  <Point X="-26.5591640625" Y="4.388583984375" />
                  <Point X="-26.55727734375" Y="4.410145507812" />
                  <Point X="-26.557728515625" Y="4.430827148438" />
                  <Point X="-26.576107421875" Y="4.570426269531" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.267236328125" Y="4.720764160156" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.509162109375" Y="4.861359375" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.24431640625" Y="4.698384277344" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.770951171875" Y="4.595445800781" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.43327734375" Y="4.860781738281" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.791537109375" Y="4.743756835938" />
                  <Point X="-23.5189765625" Y="4.677951660156" />
                  <Point X="-23.28240625" Y="4.592145996094" />
                  <Point X="-23.105349609375" Y="4.527926757812" />
                  <Point X="-22.87598828125" Y="4.420662109375" />
                  <Point X="-22.705427734375" Y="4.340896484375" />
                  <Point X="-22.4838203125" Y="4.211787597656" />
                  <Point X="-22.319017578125" Y="4.1157734375" />
                  <Point X="-22.11004296875" Y="3.967161621094" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.360111328125" Y="3.403795166016" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857919921875" Y="2.539935546875" />
                  <Point X="-22.866921875" Y="2.516058349609" />
                  <Point X="-22.879189453125" Y="2.470185546875" />
                  <Point X="-22.888392578125" Y="2.435771972656" />
                  <Point X="-22.891380859375" Y="2.417936035156" />
                  <Point X="-22.892271484375" Y="2.380953369141" />
                  <Point X="-22.88748828125" Y="2.341286376953" />
                  <Point X="-22.883900390625" Y="2.311528564453" />
                  <Point X="-22.878556640625" Y="2.289602050781" />
                  <Point X="-22.8702890625" Y="2.267511962891" />
                  <Point X="-22.859927734375" Y="2.247469726562" />
                  <Point X="-22.8353828125" Y="2.211297363281" />
                  <Point X="-22.81696875" Y="2.184161132813" />
                  <Point X="-22.80553515625" Y="2.170329589844" />
                  <Point X="-22.778400390625" Y="2.145592529297" />
                  <Point X="-22.742228515625" Y="2.121047851562" />
                  <Point X="-22.715091796875" Y="2.102635009766" />
                  <Point X="-22.695044921875" Y="2.092271728516" />
                  <Point X="-22.67295703125" Y="2.084006103516" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.611369140625" Y="2.073880371094" />
                  <Point X="-22.581611328125" Y="2.070291992188" />
                  <Point X="-22.563533203125" Y="2.069845703125" />
                  <Point X="-22.526794921875" Y="2.074171142578" />
                  <Point X="-22.480921875" Y="2.086438232422" />
                  <Point X="-22.4465078125" Y="2.095640869141" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.771779296875" Y="2.479467773438" />
                  <Point X="-21.032671875" Y="2.90619140625" />
                  <Point X="-20.97448046875" Y="2.825318359375" />
                  <Point X="-20.87673046875" Y="2.689468017578" />
                  <Point X="-20.760224609375" Y="2.496941162109" />
                  <Point X="-20.73780078125" Y="2.459883789063" />
                  <Point X="-21.121048828125" Y="2.165806152344" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.7785703125" Y="1.660245605469" />
                  <Point X="-21.79602734375" Y="1.641626708984" />
                  <Point X="-21.829041015625" Y="1.598556518555" />
                  <Point X="-21.85380859375" Y="1.566245239258" />
                  <Point X="-21.86339453125" Y="1.55091027832" />
                  <Point X="-21.8783671875" Y="1.517088378906" />
                  <Point X="-21.890666015625" Y="1.473113525391" />
                  <Point X="-21.899892578125" Y="1.440124023438" />
                  <Point X="-21.90334765625" Y="1.417824829102" />
                  <Point X="-21.9041640625" Y="1.394252807617" />
                  <Point X="-21.902259765625" Y="1.371766723633" />
                  <Point X="-21.8921640625" Y="1.322838989258" />
                  <Point X="-21.88458984375" Y="1.286133911133" />
                  <Point X="-21.879318359375" Y="1.268976074219" />
                  <Point X="-21.863716796875" Y="1.235741210938" />
                  <Point X="-21.8362578125" Y="1.194005371094" />
                  <Point X="-21.81566015625" Y="1.162695678711" />
                  <Point X="-21.80110546875" Y="1.145450317383" />
                  <Point X="-21.783859375" Y="1.129359130859" />
                  <Point X="-21.76565234375" Y="1.116034667969" />
                  <Point X="-21.725861328125" Y="1.093635742188" />
                  <Point X="-21.696009765625" Y="1.076832275391" />
                  <Point X="-21.6794765625" Y="1.069501586914" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.590080078125" Y="1.052328125" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.90413671875" Y="1.126984375" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.196048828125" Y="1.105270385742" />
                  <Point X="-20.154060546875" Y="0.932788146973" />
                  <Point X="-20.117349609375" Y="0.69700579834" />
                  <Point X="-20.109134765625" Y="0.644238769531" />
                  <Point X="-20.540607421875" Y="0.528625488281" />
                  <Point X="-21.283419921875" Y="0.32958972168" />
                  <Point X="-21.2952109375" Y="0.325585357666" />
                  <Point X="-21.318453125" Y="0.315067932129" />
                  <Point X="-21.371310546875" Y="0.284515563965" />
                  <Point X="-21.410962890625" Y="0.26159552002" />
                  <Point X="-21.4256875" Y="0.251096191406" />
                  <Point X="-21.45246875" Y="0.22557673645" />
                  <Point X="-21.48418359375" Y="0.185165283203" />
                  <Point X="-21.507974609375" Y="0.154849090576" />
                  <Point X="-21.51969921875" Y="0.135566101074" />
                  <Point X="-21.52947265625" Y="0.114101722717" />
                  <Point X="-21.536318359375" Y="0.092604255676" />
                  <Point X="-21.546890625" Y="0.037404319763" />
                  <Point X="-21.5548203125" Y="-0.004006072998" />
                  <Point X="-21.556515625" Y="-0.021876356125" />
                  <Point X="-21.5548203125" Y="-0.05855286026" />
                  <Point X="-21.54425" Y="-0.113752952576" />
                  <Point X="-21.536318359375" Y="-0.155163345337" />
                  <Point X="-21.529470703125" Y="-0.176667633057" />
                  <Point X="-21.519697265625" Y="-0.198129730225" />
                  <Point X="-21.507974609375" Y="-0.217409240723" />
                  <Point X="-21.476259765625" Y="-0.257820709229" />
                  <Point X="-21.45246875" Y="-0.288137023926" />
                  <Point X="-21.44" Y="-0.301235473633" />
                  <Point X="-21.410962890625" Y="-0.324155670166" />
                  <Point X="-21.35810546875" Y="-0.35470803833" />
                  <Point X="-21.318453125" Y="-0.377628082275" />
                  <Point X="-21.307291015625" Y="-0.383137786865" />
                  <Point X="-21.283419921875" Y="-0.392149871826" />
                  <Point X="-20.72616796875" Y="-0.54146472168" />
                  <Point X="-20.108525390625" Y="-0.706961730957" />
                  <Point X="-20.121533203125" Y="-0.793236572266" />
                  <Point X="-20.144974609375" Y="-0.948724182129" />
                  <Point X="-20.19201171875" Y="-1.154843261719" />
                  <Point X="-20.19882421875" Y="-1.18469909668" />
                  <Point X="-20.7141640625" Y="-1.116853149414" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710144043" />
                  <Point X="-21.62533984375" Y="-1.005508728027" />
                  <Point X="-21.729080078125" Y="-1.028057128906" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.8360234375" Y="-1.056596313477" />
                  <Point X="-21.8638515625" Y="-1.073489013672" />
                  <Point X="-21.8876015625" Y="-1.093960083008" />
                  <Point X="-21.950306640625" Y="-1.169373535156" />
                  <Point X="-21.997345703125" Y="-1.225947998047" />
                  <Point X="-22.012068359375" Y="-1.250334472656" />
                  <Point X="-22.02341015625" Y="-1.277719238281" />
                  <Point X="-22.030240234375" Y="-1.305365966797" />
                  <Point X="-22.0392265625" Y="-1.403029785156" />
                  <Point X="-22.04596875" Y="-1.476296386719" />
                  <Point X="-22.04365234375" Y="-1.507560791016" />
                  <Point X="-22.035921875" Y="-1.539182495117" />
                  <Point X="-22.023548828125" Y="-1.567996704102" />
                  <Point X="-21.966138671875" Y="-1.657295898438" />
                  <Point X="-21.923068359375" Y="-1.724287231445" />
                  <Point X="-21.9130625" Y="-1.737243896484" />
                  <Point X="-21.889369140625" Y="-1.760909423828" />
                  <Point X="-21.37223828125" Y="-2.157718505859" />
                  <Point X="-20.786876953125" Y="-2.606882324219" />
                  <Point X="-20.809107421875" Y="-2.642854492188" />
                  <Point X="-20.875185546875" Y="-2.749778808594" />
                  <Point X="-20.971017578125" Y="-2.885944824219" />
                  <Point X="-21.4318046875" Y="-2.619909423828" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.245775390625" Y="-2.159825195312" />
                  <Point X="-22.3692421875" Y="-2.13752734375" />
                  <Point X="-22.461865234375" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135177001953" />
                  <Point X="-22.657736328125" Y="-2.189159179688" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.757615234375" Y="-2.246549072266" />
                  <Point X="-22.77857421875" Y="-2.267508300781" />
                  <Point X="-22.795466796875" Y="-2.290438476562" />
                  <Point X="-22.84944921875" Y="-2.393009033203" />
                  <Point X="-22.889947265625" Y="-2.469956542969" />
                  <Point X="-22.899771484375" Y="-2.499735107422" />
                  <Point X="-22.904728515625" Y="-2.531908935547" />
                  <Point X="-22.90432421875" Y="-2.563259277344" />
                  <Point X="-22.882025390625" Y="-2.686726074219" />
                  <Point X="-22.865296875" Y="-2.779349609375" />
                  <Point X="-22.86101171875" Y="-2.795140869141" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.515869140625" Y="-3.401655273438" />
                  <Point X="-22.138712890625" Y="-4.05490625" />
                  <Point X="-22.139732421875" Y="-4.055633789062" />
                  <Point X="-22.218130859375" Y="-4.111630371094" />
                  <Point X="-22.298234375" Y="-4.163480957031" />
                  <Point X="-22.6559765625" Y="-3.697260498047" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.399845703125" Y="-2.822269287109" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.716078125" Y="-2.753371826172" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-23.99823828125" Y="-2.880966308594" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968861572266" />
                  <Point X="-24.11275" Y="-2.996688476562" />
                  <Point X="-24.124375" Y="-3.02580859375" />
                  <Point X="-24.155123046875" Y="-3.167271972656" />
                  <Point X="-24.178189453125" Y="-3.273396484375" />
                  <Point X="-24.180275390625" Y="-3.289627685547" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.086083984375" Y="-4.038441894531" />
                  <Point X="-23.977935546875" Y="-4.859915039062" />
                  <Point X="-24.0243203125" Y="-4.870083007812" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.05842578125" Y="-4.752636230469" />
                  <Point X="-26.141244140625" Y="-4.731328613281" />
                  <Point X="-26.1207734375" Y="-4.57583984375" />
                  <Point X="-26.120076171875" Y="-4.568098144531" />
                  <Point X="-26.11944921875" Y="-4.541030273438" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497688964844" />
                  <Point X="-26.15791796875" Y="-4.323827148438" />
                  <Point X="-26.18386328125" Y="-4.193397460938" />
                  <Point X="-26.188125" Y="-4.178468261719" />
                  <Point X="-26.19902734375" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135462890625" />
                  <Point X="-26.221740234375" Y="-4.107625976562" />
                  <Point X="-26.23057421875" Y="-4.094861083984" />
                  <Point X="-26.25020703125" Y="-4.070937744141" />
                  <Point X="-26.261005859375" Y="-4.059779296875" />
                  <Point X="-26.394283203125" Y="-3.942897949219" />
                  <Point X="-26.494265625" Y="-3.855214599609" />
                  <Point X="-26.50673828125" Y="-3.845964599609" />
                  <Point X="-26.533021484375" Y="-3.82962109375" />
                  <Point X="-26.54683203125" Y="-3.822527587891" />
                  <Point X="-26.576529296875" Y="-3.810226318359" />
                  <Point X="-26.5913125" Y="-3.805476318359" />
                  <Point X="-26.621455078125" Y="-3.798447998047" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.813703125" Y="-3.784575683594" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166259766" />
                  <Point X="-27.008005859375" Y="-3.781946044922" />
                  <Point X="-27.0390546875" Y="-3.790265869141" />
                  <Point X="-27.053673828125" Y="-3.795496337891" />
                  <Point X="-27.081865234375" Y="-3.80826953125" />
                  <Point X="-27.0954375" Y="-3.815812255859" />
                  <Point X="-27.242830078125" Y="-3.914297363281" />
                  <Point X="-27.35340234375" Y="-3.988180175781" />
                  <Point X="-27.35968359375" Y="-3.992759765625" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032770507812" />
                  <Point X="-27.410470703125" Y="-4.041629394531" />
                  <Point X="-27.503201171875" Y="-4.162478027344" />
                  <Point X="-27.5696484375" Y="-4.1213359375" />
                  <Point X="-27.74759375" Y="-4.011157226562" />
                  <Point X="-27.97184765625" Y="-3.838489257813" />
                  <Point X="-27.980861328125" Y="-3.831549072266" />
                  <Point X="-27.76683203125" Y="-3.460838623047" />
                  <Point X="-27.341486328125" Y="-2.724119628906" />
                  <Point X="-27.33484765625" Y="-2.710080566406" />
                  <Point X="-27.323947265625" Y="-2.681116210938" />
                  <Point X="-27.319685546875" Y="-2.666190917969" />
                  <Point X="-27.3134140625" Y="-2.634665771484" />
                  <Point X="-27.311638671875" Y="-2.619241699219" />
                  <Point X="-27.310625" Y="-2.588309326172" />
                  <Point X="-27.3146640625" Y="-2.557625244141" />
                  <Point X="-27.323646484375" Y="-2.528008789062" />
                  <Point X="-27.3293515625" Y="-2.513568115234" />
                  <Point X="-27.34356640625" Y="-2.484739746094" />
                  <Point X="-27.351548828125" Y="-2.471421875" />
                  <Point X="-27.369576171875" Y="-2.44626171875" />
                  <Point X="-27.37962109375" Y="-2.434419433594" />
                  <Point X="-27.39696875" Y="-2.417070800781" />
                  <Point X="-27.40880859375" Y="-2.407025634766" />
                  <Point X="-27.43396484375" Y="-2.388997558594" />
                  <Point X="-27.44728125" Y="-2.381014648438" />
                  <Point X="-27.476111328125" Y="-2.366795898438" />
                  <Point X="-27.4905546875" Y="-2.361088623047" />
                  <Point X="-27.520173828125" Y="-2.352103027344" />
                  <Point X="-27.550859375" Y="-2.3480625" />
                  <Point X="-27.581794921875" Y="-2.349074707031" />
                  <Point X="-27.597220703125" Y="-2.350849121094" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.23936328125" Y="-2.698014892578" />
                  <Point X="-28.793087890625" Y="-3.017708251953" />
                  <Point X="-28.863439453125" Y="-2.92528125" />
                  <Point X="-29.00401953125" Y="-2.740588134766" />
                  <Point X="-29.16479296875" Y="-2.470995849609" />
                  <Point X="-29.181265625" Y="-2.443373535156" />
                  <Point X="-28.792337890625" Y="-2.144938720703" />
                  <Point X="-28.048123046875" Y="-1.573881958008" />
                  <Point X="-28.036484375" Y="-1.563310913086" />
                  <Point X="-28.015107421875" Y="-1.540392089844" />
                  <Point X="-28.005369140625" Y="-1.528044189453" />
                  <Point X="-27.987404296875" Y="-1.50091394043" />
                  <Point X="-27.979837890625" Y="-1.487127685547" />
                  <Point X="-27.96708203125" Y="-1.458498413086" />
                  <Point X="-27.961892578125" Y="-1.443655639648" />
                  <Point X="-27.9541875" Y="-1.413908447266" />
                  <Point X="-27.951552734375" Y="-1.398808349609" />
                  <Point X="-27.948748046875" Y="-1.368383666992" />
                  <Point X="-27.948578125" Y="-1.353058837891" />
                  <Point X="-27.95078515625" Y="-1.321387573242" />
                  <Point X="-27.953080078125" Y="-1.306232543945" />
                  <Point X="-27.960080078125" Y="-1.276483276367" />
                  <Point X="-27.971775390625" Y="-1.24824597168" />
                  <Point X="-27.987859375" Y="-1.222259033203" />
                  <Point X="-27.996953125" Y="-1.209915283203" />
                  <Point X="-28.017787109375" Y="-1.185958496094" />
                  <Point X="-28.02875" Y="-1.175239257812" />
                  <Point X="-28.05225" Y="-1.155707275391" />
                  <Point X="-28.064787109375" Y="-1.14689453125" />
                  <Point X="-28.09126953125" Y="-1.131308105469" />
                  <Point X="-28.105431640625" Y="-1.124481689453" />
                  <Point X="-28.134693359375" Y="-1.113257568359" />
                  <Point X="-28.14979296875" Y="-1.108859741211" />
                  <Point X="-28.1816796875" Y="-1.102378295898" />
                  <Point X="-28.197296875" Y="-1.100532348633" />
                  <Point X="-28.22862109375" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-28.94203515625" Y="-1.192051513672" />
                  <Point X="-29.66091796875" Y="-1.286694335938" />
                  <Point X="-29.685779296875" Y="-1.189368041992" />
                  <Point X="-29.740759765625" Y="-0.974112854004" />
                  <Point X="-29.783298828125" Y="-0.676702880859" />
                  <Point X="-29.786451171875" Y="-0.654654602051" />
                  <Point X="-29.356060546875" Y="-0.539331481934" />
                  <Point X="-28.508287109375" Y="-0.312171325684" />
                  <Point X="-28.49643359375" Y="-0.308140686035" />
                  <Point X="-28.473359375" Y="-0.298556304932" />
                  <Point X="-28.462138671875" Y="-0.293002410889" />
                  <Point X="-28.43071484375" Y="-0.274777069092" />
                  <Point X="-28.423935546875" Y="-0.27045111084" />
                  <Point X="-28.40436328125" Y="-0.256349182129" />
                  <Point X="-28.380646484375" Y="-0.236574142456" />
                  <Point X="-28.372103515625" Y="-0.228505111694" />
                  <Point X="-28.356076171875" Y="-0.211370239258" />
                  <Point X="-28.342287109375" Y="-0.192384109497" />
                  <Point X="-28.33094921875" Y="-0.171843978882" />
                  <Point X="-28.325916015625" Y="-0.161224273682" />
                  <Point X="-28.313205078125" Y="-0.12945463562" />
                  <Point X="-28.310677734375" Y="-0.122325050354" />
                  <Point X="-28.304248046875" Y="-0.100595344543" />
                  <Point X="-28.298037109375" Y="-0.072999633789" />
                  <Point X="-28.296271484375" Y="-0.062375774384" />
                  <Point X="-28.293955078125" Y="-0.041002315521" />
                  <Point X="-28.294072265625" Y="-0.019512037277" />
                  <Point X="-28.296619140625" Y="0.001835116029" />
                  <Point X="-28.298498046875" Y="0.012441589355" />
                  <Point X="-28.306029296875" Y="0.044291625977" />
                  <Point X="-28.308080078125" Y="0.051630180359" />
                  <Point X="-28.315380859375" Y="0.073244110107" />
                  <Point X="-28.326771484375" Y="0.100759407043" />
                  <Point X="-28.3319296875" Y="0.111319511414" />
                  <Point X="-28.34351171875" Y="0.131723205566" />
                  <Point X="-28.3575234375" Y="0.150539016724" />
                  <Point X="-28.373751953125" Y="0.167482025146" />
                  <Point X="-28.382392578125" Y="0.175452819824" />
                  <Point X="-28.4100703125" Y="0.197976913452" />
                  <Point X="-28.41654296875" Y="0.202802108765" />
                  <Point X="-28.43672265625" Y="0.216136108398" />
                  <Point X="-28.464185546875" Y="0.231612060547" />
                  <Point X="-28.474927734375" Y="0.236805389404" />
                  <Point X="-28.496978515625" Y="0.245804962158" />
                  <Point X="-28.508287109375" Y="0.249611343384" />
                  <Point X="-29.144279296875" Y="0.420024871826" />
                  <Point X="-29.7854453125" Y="0.591824768066" />
                  <Point X="-29.766763671875" Y="0.718075317383" />
                  <Point X="-29.73133203125" Y="0.957522583008" />
                  <Point X="-29.645703125" Y="1.273518066406" />
                  <Point X="-29.6335859375" Y="1.318237060547" />
                  <Point X="-29.38576953125" Y="1.285611450195" />
                  <Point X="-28.778064453125" Y="1.20560559082" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.20470324707" />
                  <Point X="-28.71514453125" Y="1.206589477539" />
                  <Point X="-28.704890625" Y="1.208053588867" />
                  <Point X="-28.6846015625" Y="1.212089233398" />
                  <Point X="-28.67456640625" Y="1.214660766602" />
                  <Point X="-28.639470703125" Y="1.2257265625" />
                  <Point X="-28.613140625" Y="1.234028076172" />
                  <Point X="-28.6034453125" Y="1.237677734375" />
                  <Point X="-28.584513671875" Y="1.246008789062" />
                  <Point X="-28.57527734375" Y="1.250690063477" />
                  <Point X="-28.556533203125" Y="1.261512207031" />
                  <Point X="-28.547859375" Y="1.267172119141" />
                  <Point X="-28.531177734375" Y="1.279403686523" />
                  <Point X="-28.51592578125" Y="1.293378662109" />
                  <Point X="-28.502287109375" Y="1.308930908203" />
                  <Point X="-28.495892578125" Y="1.317080200195" />
                  <Point X="-28.483478515625" Y="1.334810302734" />
                  <Point X="-28.478009765625" Y="1.343603881836" />
                  <Point X="-28.468060546875" Y="1.361738769531" />
                  <Point X="-28.463580078125" Y="1.371079711914" />
                  <Point X="-28.449498046875" Y="1.405078491211" />
                  <Point X="-28.43893359375" Y="1.430583984375" />
                  <Point X="-28.4355" Y="1.440349243164" />
                  <Point X="-28.429712890625" Y="1.460200439453" />
                  <Point X="-28.427359375" Y="1.470286499023" />
                  <Point X="-28.423599609375" Y="1.49160168457" />
                  <Point X="-28.422359375" Y="1.501889160156" />
                  <Point X="-28.421005859375" Y="1.522535522461" />
                  <Point X="-28.421908203125" Y="1.543205932617" />
                  <Point X="-28.425056640625" Y="1.563655761719" />
                  <Point X="-28.427189453125" Y="1.573794555664" />
                  <Point X="-28.43279296875" Y="1.594701171875" />
                  <Point X="-28.436017578125" Y="1.604545654297" />
                  <Point X="-28.443513671875" Y="1.623817138672" />
                  <Point X="-28.44778515625" Y="1.633244140625" />
                  <Point X="-28.46477734375" Y="1.665886108398" />
                  <Point X="-28.477525390625" Y="1.690373901367" />
                  <Point X="-28.48280078125" Y="1.699286254883" />
                  <Point X="-28.49429296875" Y="1.716485961914" />
                  <Point X="-28.500509765625" Y="1.72477331543" />
                  <Point X="-28.514421875" Y="1.741352783203" />
                  <Point X="-28.521501953125" Y="1.748912719727" />
                  <Point X="-28.536443359375" Y="1.763215698242" />
                  <Point X="-28.5443046875" Y="1.769958618164" />
                  <Point X="-28.909111328125" Y="2.049885009766" />
                  <Point X="-29.22761328125" Y="2.294280761719" />
                  <Point X="-29.13996875" Y="2.444436035156" />
                  <Point X="-29.00228125" Y="2.680323730469" />
                  <Point X="-28.775474609375" Y="2.971854736328" />
                  <Point X="-28.726337890625" Y="3.035013183594" />
                  <Point X="-28.627390625" Y="2.977885986328" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.10619140625" Y="2.726881591797" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.038494140625" Y="2.723785644531" />
                  <Point X="-28.02816796875" Y="2.724575683594" />
                  <Point X="-28.006708984375" Y="2.727400634766" />
                  <Point X="-27.99652734375" Y="2.729310302734" />
                  <Point X="-27.976435546875" Y="2.7342265625" />
                  <Point X="-27.957001953125" Y="2.741299316406" />
                  <Point X="-27.93844921875" Y="2.750447509766" />
                  <Point X="-27.929419921875" Y="2.755529541016" />
                  <Point X="-27.911166015625" Y="2.767158203125" />
                  <Point X="-27.90274609375" Y="2.773191650391" />
                  <Point X="-27.88661328125" Y="2.786139404297" />
                  <Point X="-27.878900390625" Y="2.793053710938" />
                  <Point X="-27.844205078125" Y="2.827749023438" />
                  <Point X="-27.81817578125" Y="2.853777099609" />
                  <Point X="-27.811263671875" Y="2.861489257812" />
                  <Point X="-27.79831640625" Y="2.87762109375" />
                  <Point X="-27.79228125" Y="2.886040771484" />
                  <Point X="-27.78065234375" Y="2.904294677734" />
                  <Point X="-27.7755703125" Y="2.91332421875" />
                  <Point X="-27.766423828125" Y="2.931873535156" />
                  <Point X="-27.759353515625" Y="2.951297607422" />
                  <Point X="-27.754435546875" Y="2.97138671875" />
                  <Point X="-27.7525234375" Y="2.981571533203" />
                  <Point X="-27.749697265625" Y="3.003030029297" />
                  <Point X="-27.748908203125" Y="3.01336328125" />
                  <Point X="-27.74845703125" Y="3.034049560547" />
                  <Point X="-27.748794921875" Y="3.044402587891" />
                  <Point X="-27.753072265625" Y="3.093282714844" />
                  <Point X="-27.756279296875" Y="3.129952148438" />
                  <Point X="-27.757744140625" Y="3.140210449219" />
                  <Point X="-27.76178125" Y="3.160499267578" />
                  <Point X="-27.764353515625" Y="3.170529785156" />
                  <Point X="-27.77086328125" Y="3.191173583984" />
                  <Point X="-27.77451171875" Y="3.200866943359" />
                  <Point X="-27.782841796875" Y="3.219796875" />
                  <Point X="-27.7875234375" Y="3.229033447266" />
                  <Point X="-27.949181640625" Y="3.50903125" />
                  <Point X="-28.05938671875" Y="3.699914550781" />
                  <Point X="-27.88815625" Y="3.8311953125" />
                  <Point X="-27.648365234375" Y="4.015041503906" />
                  <Point X="-27.2911484375" Y="4.213503417969" />
                  <Point X="-27.1925234375" Y="4.268296875" />
                  <Point X="-27.11856640625" Y="4.171912109375" />
                  <Point X="-27.1118203125" Y="4.164047363281" />
                  <Point X="-27.097517578125" Y="4.149106445313" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.065091796875" Y="4.121897460938" />
                  <Point X="-27.047892578125" Y="4.110405273438" />
                  <Point X="-27.03898046875" Y="4.105129394531" />
                  <Point X="-26.984578125" Y="4.07680859375" />
                  <Point X="-26.943763671875" Y="4.0555625" />
                  <Point X="-26.93432421875" Y="4.051286132812" />
                  <Point X="-26.915041015625" Y="4.043788085938" />
                  <Point X="-26.905197265625" Y="4.040566162109" />
                  <Point X="-26.8842890625" Y="4.034964599609" />
                  <Point X="-26.87415625" Y="4.032833984375" />
                  <Point X="-26.85371484375" Y="4.029687988281" />
                  <Point X="-26.833048828125" Y="4.028786132812" />
                  <Point X="-26.81241015625" Y="4.030139160156" />
                  <Point X="-26.80212890625" Y="4.03137890625" />
                  <Point X="-26.780814453125" Y="4.035137207031" />
                  <Point X="-26.7707265625" Y="4.037489257812" />
                  <Point X="-26.7508671875" Y="4.043277832031" />
                  <Point X="-26.741095703125" Y="4.046714111328" />
                  <Point X="-26.684431640625" Y="4.070185791016" />
                  <Point X="-26.641921875" Y="4.087793701172" />
                  <Point X="-26.632583984375" Y="4.092272460938" />
                  <Point X="-26.614451171875" Y="4.102220214844" />
                  <Point X="-26.60565625" Y="4.107689453125" />
                  <Point X="-26.58792578125" Y="4.120104003906" />
                  <Point X="-26.579775390625" Y="4.126499511719" />
                  <Point X="-26.564220703125" Y="4.140140625" />
                  <Point X="-26.550244140625" Y="4.15539453125" />
                  <Point X="-26.53801171875" Y="4.172079101562" />
                  <Point X="-26.5323515625" Y="4.180755371094" />
                  <Point X="-26.52153125" Y="4.199499511719" />
                  <Point X="-26.5168515625" Y="4.208733398438" />
                  <Point X="-26.5085234375" Y="4.227660644531" />
                  <Point X="-26.504875" Y="4.237354003906" />
                  <Point X="-26.486431640625" Y="4.295848632813" />
                  <Point X="-26.472595703125" Y="4.33973046875" />
                  <Point X="-26.470025390625" Y="4.349764160156" />
                  <Point X="-26.465990234375" Y="4.370050292969" />
                  <Point X="-26.464525390625" Y="4.380302734375" />
                  <Point X="-26.462638671875" Y="4.401864257812" />
                  <Point X="-26.46230078125" Y="4.412217285156" />
                  <Point X="-26.462751953125" Y="4.432898925781" />
                  <Point X="-26.463541015625" Y="4.443227539062" />
                  <Point X="-26.479263671875" Y="4.562655761719" />
                  <Point X="-26.24158984375" Y="4.629291503906" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.498119140625" Y="4.767003417969" />
                  <Point X="-25.36522265625" Y="4.782557128906" />
                  <Point X="-25.336080078125" Y="4.673796386719" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.6791875" Y="4.570857910156" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.443171875" Y="4.766298339844" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.81383203125" Y="4.65141015625" />
                  <Point X="-23.546408203125" Y="4.586844726562" />
                  <Point X="-23.314798828125" Y="4.502838867188" />
                  <Point X="-23.141734375" Y="4.440067382812" />
                  <Point X="-22.916232421875" Y="4.334607910156" />
                  <Point X="-22.74955078125" Y="4.25665625" />
                  <Point X="-22.531642578125" Y="4.129702636719" />
                  <Point X="-22.370564453125" Y="4.035858398438" />
                  <Point X="-22.182216796875" Y="3.901915527344" />
                  <Point X="-22.4423828125" Y="3.451295166016" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.93762109375" Y="2.593110595703" />
                  <Point X="-22.9468125" Y="2.573448974609" />
                  <Point X="-22.955814453125" Y="2.549571777344" />
                  <Point X="-22.958697265625" Y="2.540601318359" />
                  <Point X="-22.97096484375" Y="2.494728515625" />
                  <Point X="-22.98016796875" Y="2.460314941406" />
                  <Point X="-22.9820859375" Y="2.451469726562" />
                  <Point X="-22.986353515625" Y="2.420223144531" />
                  <Point X="-22.987244140625" Y="2.383240478516" />
                  <Point X="-22.986587890625" Y="2.369580322266" />
                  <Point X="-22.9818046875" Y="2.329913330078" />
                  <Point X="-22.978216796875" Y="2.300155517578" />
                  <Point X="-22.97619921875" Y="2.289034423828" />
                  <Point X="-22.97085546875" Y="2.267107910156" />
                  <Point X="-22.967529296875" Y="2.256302490234" />
                  <Point X="-22.95926171875" Y="2.234212402344" />
                  <Point X="-22.9546796875" Y="2.223884521484" />
                  <Point X="-22.944318359375" Y="2.203842285156" />
                  <Point X="-22.9385390625" Y="2.194127929688" />
                  <Point X="-22.913994140625" Y="2.157955566406" />
                  <Point X="-22.895580078125" Y="2.130819335937" />
                  <Point X="-22.89019140625" Y="2.123633789062" />
                  <Point X="-22.869537109375" Y="2.100124267578" />
                  <Point X="-22.84240234375" Y="2.075387207031" />
                  <Point X="-22.8317421875" Y="2.066981689453" />
                  <Point X="-22.7955703125" Y="2.042437011719" />
                  <Point X="-22.76843359375" Y="2.024024291992" />
                  <Point X="-22.758716796875" Y="2.018244384766" />
                  <Point X="-22.738669921875" Y="2.007881103516" />
                  <Point X="-22.72833984375" Y="2.003297485352" />
                  <Point X="-22.706251953125" Y="1.995031860352" />
                  <Point X="-22.695451171875" Y="1.991707397461" />
                  <Point X="-22.673529296875" Y="1.986365112305" />
                  <Point X="-22.662408203125" Y="1.984346801758" />
                  <Point X="-22.6227421875" Y="1.979563598633" />
                  <Point X="-22.592984375" Y="1.975975219727" />
                  <Point X="-22.583955078125" Y="1.975320922852" />
                  <Point X="-22.55242578125" Y="1.975497436523" />
                  <Point X="-22.5156875" Y="1.979822875977" />
                  <Point X="-22.50225390625" Y="1.982395996094" />
                  <Point X="-22.456380859375" Y="1.994662963867" />
                  <Point X="-22.421966796875" Y="2.003865600586" />
                  <Point X="-22.4160078125" Y="2.005670288086" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572753906" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.724279296875" Y="2.3971953125" />
                  <Point X="-21.05959375" Y="2.780951416016" />
                  <Point X="-21.05159375" Y="2.769832763672" />
                  <Point X="-20.95605078125" Y="2.637048339844" />
                  <Point X="-20.863115234375" Y="2.483471679688" />
                  <Point X="-21.178880859375" Y="2.241174560547" />
                  <Point X="-21.827046875" Y="1.743819824219" />
                  <Point X="-21.831857421875" Y="1.739871337891" />
                  <Point X="-21.847873046875" Y="1.725223632813" />
                  <Point X="-21.865330078125" Y="1.706604736328" />
                  <Point X="-21.87142578125" Y="1.699420166016" />
                  <Point X="-21.904439453125" Y="1.656349975586" />
                  <Point X="-21.92920703125" Y="1.624038574219" />
                  <Point X="-21.934365234375" Y="1.616601074219" />
                  <Point X="-21.950263671875" Y="1.589366210938" />
                  <Point X="-21.965236328125" Y="1.555544311523" />
                  <Point X="-21.96985546875" Y="1.54267590332" />
                  <Point X="-21.982154296875" Y="1.498701049805" />
                  <Point X="-21.991380859375" Y="1.465711547852" />
                  <Point X="-21.993771484375" Y="1.454669921875" />
                  <Point X="-21.9972265625" Y="1.432370727539" />
                  <Point X="-21.998291015625" Y="1.42111328125" />
                  <Point X="-21.999107421875" Y="1.397541137695" />
                  <Point X="-21.998826171875" Y="1.386236206055" />
                  <Point X="-21.996921875" Y="1.36375012207" />
                  <Point X="-21.995298828125" Y="1.352568969727" />
                  <Point X="-21.985203125" Y="1.303641235352" />
                  <Point X="-21.97762890625" Y="1.266936157227" />
                  <Point X="-21.975400390625" Y="1.258233764648" />
                  <Point X="-21.965314453125" Y="1.228606689453" />
                  <Point X="-21.949712890625" Y="1.195371826172" />
                  <Point X="-21.943080078125" Y="1.183526000977" />
                  <Point X="-21.91562109375" Y="1.141790161133" />
                  <Point X="-21.8950234375" Y="1.11048046875" />
                  <Point X="-21.888259765625" Y="1.101423339844" />
                  <Point X="-21.873705078125" Y="1.084177978516" />
                  <Point X="-21.8659140625" Y="1.075989746094" />
                  <Point X="-21.84866796875" Y="1.0598984375" />
                  <Point X="-21.83996484375" Y="1.052695800781" />
                  <Point X="-21.8217578125" Y="1.039371337891" />
                  <Point X="-21.81225390625" Y="1.033249633789" />
                  <Point X="-21.772462890625" Y="1.010850708008" />
                  <Point X="-21.742611328125" Y="0.994047241211" />
                  <Point X="-21.734515625" Y="0.98998626709" />
                  <Point X="-21.7053203125" Y="0.978084411621" />
                  <Point X="-21.669724609375" Y="0.96802142334" />
                  <Point X="-21.656328125" Y="0.965257629395" />
                  <Point X="-21.60252734375" Y="0.958147094727" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.555966796875" Y="0.952199829102" />
                  <Point X="-21.53428125" Y="0.951222900391" />
                  <Point X="-21.50878125" Y="0.952032287598" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-20.891736328125" Y="1.032797119141" />
                  <Point X="-20.295296875" Y="1.111319824219" />
                  <Point X="-20.288353515625" Y="1.082799438477" />
                  <Point X="-20.2473125" Y="0.914207275391" />
                  <Point X="-20.21612890625" Y="0.713921203613" />
                  <Point X="-20.5651953125" Y="0.62038848877" />
                  <Point X="-21.3080078125" Y="0.42135269165" />
                  <Point X="-21.31396875" Y="0.419543792725" />
                  <Point X="-21.334376953125" Y="0.412136291504" />
                  <Point X="-21.357619140625" Y="0.401618835449" />
                  <Point X="-21.365994140625" Y="0.397316650391" />
                  <Point X="-21.4188515625" Y="0.366764373779" />
                  <Point X="-21.45850390625" Y="0.343844207764" />
                  <Point X="-21.4661171875" Y="0.338945587158" />
                  <Point X="-21.49122265625" Y="0.319871917725" />
                  <Point X="-21.51800390625" Y="0.294352478027" />
                  <Point X="-21.527203125" Y="0.284227386475" />
                  <Point X="-21.55891796875" Y="0.243815933228" />
                  <Point X="-21.582708984375" Y="0.213499832153" />
                  <Point X="-21.5891484375" Y="0.20420451355" />
                  <Point X="-21.600873046875" Y="0.184921585083" />
                  <Point X="-21.606158203125" Y="0.174933837891" />
                  <Point X="-21.615931640625" Y="0.153469421387" />
                  <Point X="-21.619994140625" Y="0.142927444458" />
                  <Point X="-21.62683984375" Y="0.121430030823" />
                  <Point X="-21.629623046875" Y="0.110474441528" />
                  <Point X="-21.6401953125" Y="0.055274559021" />
                  <Point X="-21.648125" Y="0.013864208221" />
                  <Point X="-21.649396484375" Y="0.004966006279" />
                  <Point X="-21.6514140625" Y="-0.026262935638" />
                  <Point X="-21.64971875" Y="-0.062939350128" />
                  <Point X="-21.648125" Y="-0.076419891357" />
                  <Point X="-21.6375546875" Y="-0.131619918823" />
                  <Point X="-21.629623046875" Y="-0.173030273438" />
                  <Point X="-21.62683984375" Y="-0.183988250732" />
                  <Point X="-21.6199921875" Y="-0.205492492676" />
                  <Point X="-21.615927734375" Y="-0.216038772583" />
                  <Point X="-21.606154296875" Y="-0.237500808716" />
                  <Point X="-21.600869140625" Y="-0.247485748291" />
                  <Point X="-21.589146484375" Y="-0.266765258789" />
                  <Point X="-21.582708984375" Y="-0.276059997559" />
                  <Point X="-21.550994140625" Y="-0.316471435547" />
                  <Point X="-21.527203125" Y="-0.346787689209" />
                  <Point X="-21.52127734375" Y="-0.353637878418" />
                  <Point X="-21.498859375" Y="-0.375804077148" />
                  <Point X="-21.469822265625" Y="-0.398724273682" />
                  <Point X="-21.45850390625" Y="-0.40640435791" />
                  <Point X="-21.405646484375" Y="-0.436956756592" />
                  <Point X="-21.365994140625" Y="-0.459876800537" />
                  <Point X="-21.360501953125" Y="-0.462815368652" />
                  <Point X="-21.340845703125" Y="-0.472014831543" />
                  <Point X="-21.316974609375" Y="-0.481027008057" />
                  <Point X="-21.3080078125" Y="-0.483912841797" />
                  <Point X="-20.750755859375" Y="-0.633227722168" />
                  <Point X="-20.21512109375" Y="-0.776750854492" />
                  <Point X="-20.215470703125" Y="-0.779073303223" />
                  <Point X="-20.238380859375" Y="-0.931036682129" />
                  <Point X="-20.272197265625" Y="-1.079219604492" />
                  <Point X="-20.701763671875" Y="-1.02266595459" />
                  <Point X="-21.563216796875" Y="-0.909253601074" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042297363" />
                  <Point X="-21.63327734375" Y="-0.910840881348" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.7492578125" Y="-0.93522467041" />
                  <Point X="-21.82708203125" Y="-0.952140075684" />
                  <Point X="-21.842125" Y="-0.95674230957" />
                  <Point X="-21.871244140625" Y="-0.968366088867" />
                  <Point X="-21.8853203125" Y="-0.975387634277" />
                  <Point X="-21.9131484375" Y="-0.992280456543" />
                  <Point X="-21.925875" Y="-1.001530395508" />
                  <Point X="-21.949625" Y="-1.022001586914" />
                  <Point X="-21.9606484375" Y="-1.033222290039" />
                  <Point X="-22.023353515625" Y="-1.108635864258" />
                  <Point X="-22.070392578125" Y="-1.165210205078" />
                  <Point X="-22.078673828125" Y="-1.176848510742" />
                  <Point X="-22.093396484375" Y="-1.201234985352" />
                  <Point X="-22.099837890625" Y="-1.213983276367" />
                  <Point X="-22.1111796875" Y="-1.241368164062" />
                  <Point X="-22.11563671875" Y="-1.254934692383" />
                  <Point X="-22.122466796875" Y="-1.282581420898" />
                  <Point X="-22.12483984375" Y="-1.296661499023" />
                  <Point X="-22.133826171875" Y="-1.394325317383" />
                  <Point X="-22.140568359375" Y="-1.467591918945" />
                  <Point X="-22.140708984375" Y="-1.483315795898" />
                  <Point X="-22.138392578125" Y="-1.514580200195" />
                  <Point X="-22.135935546875" Y="-1.530120849609" />
                  <Point X="-22.128205078125" Y="-1.561742553711" />
                  <Point X="-22.12321484375" Y="-1.576666625977" />
                  <Point X="-22.110841796875" Y="-1.605480712891" />
                  <Point X="-22.103458984375" Y="-1.61937097168" />
                  <Point X="-22.046048828125" Y="-1.708670166016" />
                  <Point X="-22.002978515625" Y="-1.775661499023" />
                  <Point X="-21.9982578125" Y="-1.782352539063" />
                  <Point X="-21.980197265625" Y="-1.804458496094" />
                  <Point X="-21.95650390625" Y="-1.828124023438" />
                  <Point X="-21.947201171875" Y="-1.836277954102" />
                  <Point X="-21.4300703125" Y="-2.233086914062" />
                  <Point X="-20.912828125" Y="-2.629981201172" />
                  <Point X="-20.954494140625" Y="-2.697402832031" />
                  <Point X="-20.9987265625" Y="-2.760250976562" />
                  <Point X="-21.3843046875" Y="-2.537636962891" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.18497265625" Y="-2.079513427734" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228892578125" Y="-2.066337646484" />
                  <Point X="-22.352359375" Y="-2.044039672852" />
                  <Point X="-22.444982421875" Y="-2.027311889648" />
                  <Point X="-22.460640625" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.55515625" Y="-2.03513659668" />
                  <Point X="-22.584931640625" Y="-2.044960571289" />
                  <Point X="-22.59941015625" Y="-2.051109130859" />
                  <Point X="-22.70198046875" Y="-2.105091308594" />
                  <Point X="-22.778927734375" Y="-2.145588134766" />
                  <Point X="-22.791029296875" Y="-2.153169433594" />
                  <Point X="-22.8139609375" Y="-2.1700625" />
                  <Point X="-22.824791015625" Y="-2.179374267578" />
                  <Point X="-22.84575" Y="-2.200333496094" />
                  <Point X="-22.855060546875" Y="-2.211161621094" />
                  <Point X="-22.871953125" Y="-2.234091796875" />
                  <Point X="-22.87953515625" Y="-2.246193847656" />
                  <Point X="-22.933517578125" Y="-2.348764404297" />
                  <Point X="-22.974015625" Y="-2.425711914062" />
                  <Point X="-22.9801640625" Y="-2.440193115234" />
                  <Point X="-22.98998828125" Y="-2.469971679688" />
                  <Point X="-22.9936640625" Y="-2.485269042969" />
                  <Point X="-22.99862109375" Y="-2.517442871094" />
                  <Point X="-22.999720703125" Y="-2.533134033203" />
                  <Point X="-22.99931640625" Y="-2.564484375" />
                  <Point X="-22.9978125" Y="-2.580143554688" />
                  <Point X="-22.975513671875" Y="-2.703610351562" />
                  <Point X="-22.95878515625" Y="-2.796233886719" />
                  <Point X="-22.95698046875" Y="-2.804229248047" />
                  <Point X="-22.94876171875" Y="-2.831542236328" />
                  <Point X="-22.935927734375" Y="-2.862479980469" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.598140625" Y="-3.449155273438" />
                  <Point X="-22.264103515625" Y="-4.027721923828" />
                  <Point X="-22.27624609375" Y="-4.036081787109" />
                  <Point X="-22.580607421875" Y="-3.639428222656" />
                  <Point X="-23.16608203125" Y="-2.876422851562" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849625732422" />
                  <Point X="-23.21674609375" Y="-2.828004638672" />
                  <Point X="-23.22669921875" Y="-2.820646972656" />
                  <Point X="-23.348470703125" Y="-2.742359130859" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.453716796875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663873535156" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.724783203125" Y="-2.658771484375" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920125" Y="-2.699414794922" />
                  <Point X="-23.944505859375" Y="-2.714135742188" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-24.058974609375" Y="-2.807918457031" />
                  <Point X="-24.136123046875" Y="-2.872063720703" />
                  <Point X="-24.147345703125" Y="-2.883088378906" />
                  <Point X="-24.16781640625" Y="-2.906838378906" />
                  <Point X="-24.177064453125" Y="-2.919563720703" />
                  <Point X="-24.19395703125" Y="-2.947390625" />
                  <Point X="-24.200978515625" Y="-2.961466552734" />
                  <Point X="-24.212603515625" Y="-2.990586669922" />
                  <Point X="-24.21720703125" Y="-3.005630859375" />
                  <Point X="-24.247955078125" Y="-3.147094238281" />
                  <Point X="-24.271021484375" Y="-3.25321875" />
                  <Point X="-24.2724140625" Y="-3.261287353516" />
                  <Point X="-24.275275390625" Y="-3.289677490234" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520019531" />
                  <Point X="-24.180271484375" Y="-4.050841796875" />
                  <Point X="-24.166912109375" Y="-4.152312011719" />
                  <Point X="-24.344931640625" Y="-3.487937255859" />
                  <Point X="-24.347392578125" Y="-3.480122314453" />
                  <Point X="-24.357853515625" Y="-3.453578613281" />
                  <Point X="-24.3732109375" Y="-3.423815429688" />
                  <Point X="-24.37958984375" Y="-3.413209716797" />
                  <Point X="-24.473138671875" Y="-3.278424072266" />
                  <Point X="-24.543318359375" Y="-3.177309326172" />
                  <Point X="-24.553328125" Y="-3.165172119141" />
                  <Point X="-24.575212890625" Y="-3.142716308594" />
                  <Point X="-24.587087890625" Y="-3.132397705078" />
                  <Point X="-24.61334375" Y="-3.113153808594" />
                  <Point X="-24.6267578125" Y="-3.104936767578" />
                  <Point X="-24.6547578125" Y="-3.090829345703" />
                  <Point X="-24.66934375" Y="-3.084938964844" />
                  <Point X="-24.814103515625" Y="-3.040010498047" />
                  <Point X="-24.922703125" Y="-3.006305664062" />
                  <Point X="-24.936623046875" Y="-3.003109619141" />
                  <Point X="-24.964783203125" Y="-2.998840087891" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.209744140625" Y="-3.051233398438" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.3329296875" Y="-3.090829589844" />
                  <Point X="-25.3609296875" Y="-3.104937255859" />
                  <Point X="-25.37434375" Y="-3.113153808594" />
                  <Point X="-25.400599609375" Y="-3.132397705078" />
                  <Point X="-25.412474609375" Y="-3.142717285156" />
                  <Point X="-25.434359375" Y="-3.165173828125" />
                  <Point X="-25.444369140625" Y="-3.177310791016" />
                  <Point X="-25.537916015625" Y="-3.312096191406" />
                  <Point X="-25.608095703125" Y="-3.413211181641" />
                  <Point X="-25.61246875" Y="-3.420132080078" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213378906" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.81445703125" Y="-4.128739746094" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.081616784495" Y="-2.712394203017" />
                  <Point X="-20.980426944899" Y="-2.578110750381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.164507006489" Y="-2.664537429471" />
                  <Point X="-21.055798354497" Y="-2.520276175779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.296803839711" Y="-4.009290265129" />
                  <Point X="-22.284314263809" Y="-3.992716038103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.247397228484" Y="-2.616680655925" />
                  <Point X="-21.131169764094" Y="-2.462441601177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.356818733997" Y="-3.931076906404" />
                  <Point X="-22.335916512763" Y="-3.903338721955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.330287450478" Y="-2.568823882379" />
                  <Point X="-21.206541173691" Y="-2.404607026574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.416833628282" Y="-3.852863547679" />
                  <Point X="-22.387518761717" Y="-3.813961405807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.413177688325" Y="-2.52096712987" />
                  <Point X="-21.281912583288" Y="-2.346772451972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.321820046679" Y="-1.072686622988" />
                  <Point X="-20.248502962052" Y="-0.975391565498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.476848522568" Y="-3.774650188954" />
                  <Point X="-22.439121010671" Y="-3.724584089658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.49606795583" Y="-2.473110416719" />
                  <Point X="-21.357283992885" Y="-2.288937877369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.430036995536" Y="-1.058439551173" />
                  <Point X="-20.217324448053" Y="-0.776160466542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.536863416853" Y="-3.696436830229" />
                  <Point X="-22.490723259625" Y="-3.63520677351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.578958223335" Y="-2.425253703567" />
                  <Point X="-21.432655402249" Y="-2.231103302456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.538253944392" Y="-1.044192479358" />
                  <Point X="-20.316293954912" Y="-0.749641624711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.596878354969" Y="-3.618223529669" />
                  <Point X="-22.542325508578" Y="-3.545829457362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.661848490839" Y="-2.377396990415" />
                  <Point X="-21.508026805025" Y="-2.173268718802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.646470893249" Y="-1.029945407543" />
                  <Point X="-20.41526346177" Y="-0.72312278288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.656893410923" Y="-3.540010385485" />
                  <Point X="-22.593927757532" Y="-3.456452141213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.744738758344" Y="-2.329540277263" />
                  <Point X="-21.583398207802" Y="-2.115434135149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.754687859882" Y="-1.015698359319" />
                  <Point X="-20.514232968629" Y="-0.696603941048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.716908466877" Y="-3.461797241301" />
                  <Point X="-22.645529825928" Y="-3.367074585455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.827629025849" Y="-2.281683564112" />
                  <Point X="-21.658769610578" Y="-2.057599551495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.862904845089" Y="-1.001451335742" />
                  <Point X="-20.613202475488" Y="-0.670085099217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.77692352283" Y="-3.383584097117" />
                  <Point X="-22.697131878271" Y="-3.277697008396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.910519293354" Y="-2.23382685096" />
                  <Point X="-21.734141013355" Y="-1.999764967841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.971121830295" Y="-0.987204312164" />
                  <Point X="-20.712171982347" Y="-0.643566257386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.836938578784" Y="-3.305370952933" />
                  <Point X="-22.748733930615" Y="-3.188319431338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.993409560858" Y="-2.185970137808" />
                  <Point X="-21.809512416131" Y="-1.941930384187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.079338815501" Y="-0.972957288587" />
                  <Point X="-20.811141527873" Y="-0.617047466868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.896953634738" Y="-3.227157808749" />
                  <Point X="-22.800335982959" Y="-3.098941854279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.076299828363" Y="-2.138113424657" />
                  <Point X="-21.884883818907" Y="-1.884095800533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.187555800707" Y="-0.95871026501" />
                  <Point X="-20.910111098107" Y="-0.590528709138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.956968690691" Y="-3.148944664565" />
                  <Point X="-22.851938035303" Y="-3.00956427722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.159452424001" Y="-2.090604832696" />
                  <Point X="-21.959445158277" Y="-1.82518622643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.295772785914" Y="-0.944463241433" />
                  <Point X="-21.00908066834" Y="-0.564009951407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.016983746645" Y="-3.070731520381" />
                  <Point X="-22.903540087646" Y="-2.920186700161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.256378136111" Y="-2.061373783626" />
                  <Point X="-22.020519341831" Y="-1.748378592043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.40398977112" Y="-0.930216217855" />
                  <Point X="-21.108050238573" Y="-0.537491193677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.076998802599" Y="-2.992518376197" />
                  <Point X="-22.950740133445" Y="-2.824967463111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.361081781408" Y="-2.042464400516" />
                  <Point X="-22.075283338986" Y="-1.663197057472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.512206756326" Y="-0.915969194278" />
                  <Point X="-21.207019808807" Y="-0.510972435946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.301166704644" Y="0.691135235082" />
                  <Point X="-20.227750167263" Y="0.788562270835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.137013858552" Y="-2.914305232013" />
                  <Point X="-22.976041516465" Y="-2.700687719021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.467416941393" Y="-2.025720110524" />
                  <Point X="-22.1250023987" Y="-1.571320664795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.626891689031" Y="-0.910305426935" />
                  <Point X="-21.30598937904" Y="-0.484453678216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.450214711225" Y="0.651197663182" />
                  <Point X="-20.248462598668" Y="0.918931759402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.172672339578" Y="-4.130814642818" />
                  <Point X="-24.170178181731" Y="-4.127504783563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.200978298424" Y="-2.841333097305" />
                  <Point X="-22.998529793105" Y="-2.572674856677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.609505587898" Y="-2.056422299673" />
                  <Point X="-22.137059880592" Y="-1.429465670295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.767632053325" Y="-0.939218385158" />
                  <Point X="-21.394155800865" Y="-0.443598658333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.599262648419" Y="0.611260183362" />
                  <Point X="-20.277507230733" Y="1.038244044231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.203874827585" Y="-4.014365929543" />
                  <Point X="-24.18786983093" Y="-3.993126581612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.279003457691" Y="-2.78702016746" />
                  <Point X="-22.898232271689" Y="-2.281719736855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.814334624004" Y="-2.170383797947" />
                  <Point X="-22.107407169927" Y="-1.232259380753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.947746301992" Y="-1.020382252745" />
                  <Point X="-21.475760991546" Y="-0.394036590637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.748310351422" Y="0.571323014324" />
                  <Point X="-20.346470346316" Y="1.10458271222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.235077315592" Y="-3.897917216268" />
                  <Point X="-24.205561108527" Y="-3.858747886528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.359135293178" Y="-2.735502891383" />
                  <Point X="-21.543479833491" Y="-0.32604671576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.897358054425" Y="0.531385845286" />
                  <Point X="-20.478523900358" Y="1.087197540558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.266279803599" Y="-3.781468502993" />
                  <Point X="-24.223252386124" Y="-3.724369191445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.439267489288" Y="-2.68398609387" />
                  <Point X="-21.601844384186" Y="-0.245643277119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.046405757428" Y="0.491448676248" />
                  <Point X="-20.610577454401" Y="1.069812368896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.297482291606" Y="-3.665019789718" />
                  <Point X="-24.240943663722" Y="-3.589990496362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.533009623438" Y="-2.650530294154" />
                  <Point X="-21.637011507233" Y="-0.134455812243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.195453460431" Y="0.451511507211" />
                  <Point X="-20.742631008443" Y="1.052427197234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.328684779613" Y="-3.548571076442" />
                  <Point X="-24.258634941319" Y="-3.455611801279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.653209587132" Y="-2.652185220127" />
                  <Point X="-21.649044078679" Y="0.007432239536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.349097250075" Y="0.405475125197" />
                  <Point X="-20.874684562486" Y="1.035042025572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.36525154249" Y="-3.439240996355" />
                  <Point X="-24.275259571919" Y="-3.319817617821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.781025673663" Y="-2.663947082472" />
                  <Point X="-21.006738147736" Y="1.017656812496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.420342399594" Y="-3.354493219586" />
                  <Point X="-24.241428262817" Y="-3.117066140861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.932198889931" Y="-2.70670490288" />
                  <Point X="-21.138791737614" Y="1.000271593279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.477374333923" Y="-3.272321339298" />
                  <Point X="-21.270845327492" Y="0.982886374061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.534406408903" Y="-3.190149645661" />
                  <Point X="-21.40289891737" Y="0.965501154844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.601744904498" Y="-3.121655034129" />
                  <Point X="-21.532570318777" Y="0.951277206502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.688540552347" Y="-3.078980935739" />
                  <Point X="-21.642377576636" Y="0.96341386699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.067017452189" Y="-4.750425753991" />
                  <Point X="-25.933596216884" Y="-4.573369794586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.784946316683" Y="-3.049059892669" />
                  <Point X="-21.739443859342" Y="0.992458372578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.134822236462" Y="-4.682549928435" />
                  <Point X="-25.867959078127" Y="-4.328410556085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.88135236859" Y="-3.019139231218" />
                  <Point X="-21.822587655227" Y="1.039978642205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.121983691072" Y="-4.507656789851" />
                  <Point X="-25.802322039341" Y="-4.083451450252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.984199824476" Y="-2.997766601562" />
                  <Point X="-21.891733446098" Y="1.1060748919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.146325746298" Y="-4.38210397478" />
                  <Point X="-25.736685441317" Y="-3.838492929329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.123202911277" Y="-3.024374114683" />
                  <Point X="-21.947020158463" Y="1.190562759959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.123036988753" Y="2.284025358424" />
                  <Point X="-20.9119418687" Y="2.56415804436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.171168466978" Y="-4.257215565206" />
                  <Point X="-25.671048843294" Y="-3.593534408406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.278468558281" Y="-3.072562774109" />
                  <Point X="-21.984248471667" Y="1.29901493311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.405064653879" Y="2.067617819272" />
                  <Point X="-20.965779395604" Y="2.65056904648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.202855064651" Y="-4.141409287157" />
                  <Point X="-21.995807111551" Y="1.441531913314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.687091719822" Y="1.851211075261" />
                  <Point X="-21.023882286997" Y="2.731319718742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.260606389261" Y="-4.060192070016" />
                  <Point X="-21.140737957602" Y="2.734102819595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.332199722547" Y="-3.997343818809" />
                  <Point X="-21.351298012354" Y="2.612536002703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.40382134861" Y="-3.934533113385" />
                  <Point X="-21.561858067106" Y="2.490969185811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.475442713153" Y="-3.871722060913" />
                  <Point X="-21.772418161682" Y="2.36940231607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.554828802882" Y="-3.81921514679" />
                  <Point X="-21.982978390629" Y="2.247835268014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.655493126273" Y="-3.794945402461" />
                  <Point X="-22.193538619575" Y="2.126268219958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.768847340391" Y="-3.787515711909" />
                  <Point X="-22.398621032708" Y="2.011970479012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.882201650185" Y="-3.780086148322" />
                  <Point X="-22.544341044181" Y="1.976449305786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.00168997086" Y="-3.780796692112" />
                  <Point X="-22.657764691798" Y="1.983786854974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.1993502264" Y="-3.88524489726" />
                  <Point X="-22.752984855298" Y="2.015281243494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.519574048638" Y="-4.152340448913" />
                  <Point X="-22.832517807427" Y="2.067593264629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.600683186855" Y="-4.102120097363" />
                  <Point X="-22.899487397322" Y="2.13657743056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.681792255357" Y="-4.051899653299" />
                  <Point X="-22.953853680107" Y="2.222286749926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.76180056678" Y="-4.000218455253" />
                  <Point X="-22.983170276295" Y="2.341238126173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.837077138105" Y="-3.942258026012" />
                  <Point X="-22.962440816093" Y="2.526602862396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.91235370943" Y="-3.884297596771" />
                  <Point X="-22.631752863381" Y="3.123296411021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.945813767341" Y="-3.770844779948" />
                  <Point X="-22.241990937458" Y="3.79838376989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.556052125219" Y="-3.095757797696" />
                  <Point X="-22.247788161712" Y="3.948546406871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.311443821338" Y="-2.613295801299" />
                  <Point X="-22.325237110123" Y="4.003623994348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.339593831711" Y="-2.492796313387" />
                  <Point X="-22.404848298299" Y="4.055832192742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.399722741327" Y="-2.414734258115" />
                  <Point X="-22.487510659737" Y="4.10399134746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.481073822082" Y="-2.364834975158" />
                  <Point X="-22.570173009303" Y="4.152150517933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.588753685998" Y="-2.349875167554" />
                  <Point X="-22.652835345271" Y="4.200309706449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.762646651281" Y="-2.422783113242" />
                  <Point X="-22.73549768124" Y="4.248468894965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.97320641297" Y="-2.544349541227" />
                  <Point X="-22.822553752697" Y="4.290797399555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.18376617466" Y="-2.665915969212" />
                  <Point X="-22.910509784681" Y="4.331931616186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.394326352465" Y="-2.787482949401" />
                  <Point X="-22.99846593088" Y="4.37306568125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.604886679563" Y="-2.909050127708" />
                  <Point X="-23.086422085028" Y="4.414199735765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.799435319458" Y="-3.009369079428" />
                  <Point X="-23.176405927677" Y="4.452642956755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.859210343172" Y="-2.930837401704" />
                  <Point X="-23.269825518427" Y="4.486526786018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.918985251864" Y="-2.852305571339" />
                  <Point X="-23.363245135037" Y="4.520410580965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.978760151799" Y="-2.773773729353" />
                  <Point X="-28.110842921759" Y="-1.622008663634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.954270955716" Y="-1.414230646887" />
                  <Point X="-23.456664775651" Y="4.554294344057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.034363474323" Y="-2.689706017167" />
                  <Point X="-28.392870832955" Y="-1.838416529333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.963394155273" Y="-1.268481728209" />
                  <Point X="-23.550368649882" Y="4.5878009164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.086913925117" Y="-2.601587007361" />
                  <Point X="-28.674898744152" Y="-2.054824395033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.019154111451" Y="-1.184621875901" />
                  <Point X="-23.651011212913" Y="4.612099537702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.139464375911" Y="-2.513467997555" />
                  <Point X="-28.95692645815" Y="-2.27123199904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.096156945911" Y="-1.128952275215" />
                  <Point X="-23.751653775943" Y="4.636398159005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.193988554381" Y="-1.10092339122" />
                  <Point X="-23.852296442331" Y="4.660696643147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.319890012653" Y="-1.110144456047" />
                  <Point X="-23.952939275798" Y="4.684994905568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.451943590819" Y="-1.127529659721" />
                  <Point X="-24.053582109266" Y="4.709293167988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.583997168984" Y="-1.144914863396" />
                  <Point X="-24.154224942733" Y="4.733591430408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.716050747149" Y="-1.16230006707" />
                  <Point X="-24.26276834142" Y="4.747405288667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.848104325315" Y="-1.179685270744" />
                  <Point X="-24.836910067455" Y="4.143349297662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.780190144339" Y="4.218619177916" />
                  <Point X="-24.373020445568" Y="4.758951618191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.980157903216" Y="-1.197070474068" />
                  <Point X="-24.999746941009" Y="4.08511328125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.704280487379" Y="4.477210508503" />
                  <Point X="-24.483272519344" Y="4.77049798802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.112211480468" Y="-1.214455676529" />
                  <Point X="-28.357394328469" Y="-0.2127794837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.349255948061" Y="-0.201979488122" />
                  <Point X="-25.099612814231" Y="4.110442604741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.638643808295" Y="4.722169136996" />
                  <Point X="-24.593524539988" Y="4.782044428358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.244265057719" Y="-1.231840878991" />
                  <Point X="-28.562112436396" Y="-0.326593775309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.300440552397" Y="0.020656543316" />
                  <Point X="-25.174531183037" Y="4.16887838478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.376318634971" Y="-1.249226081452" />
                  <Point X="-28.711160171056" Y="-0.366530986357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.34015491039" Y="0.125809623603" />
                  <Point X="-25.22497546063" Y="4.259792380826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.508372212222" Y="-1.266611283914" />
                  <Point X="-28.860207905715" Y="-0.406468197404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.406757436074" Y="0.195280900195" />
                  <Point X="-25.256305897266" Y="4.376071300536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.640425789474" Y="-1.283996486375" />
                  <Point X="-29.009255640375" Y="-0.446405408452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.4898347576" Y="0.242889384277" />
                  <Point X="-25.287507982097" Y="4.492520548844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.686359444849" Y="-1.187096692473" />
                  <Point X="-29.158303375035" Y="-0.4863426195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.587689767553" Y="0.270887213455" />
                  <Point X="-25.318710066928" Y="4.608969797151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.716472153732" Y="-1.069201793453" />
                  <Point X="-29.307351109694" Y="-0.526279830547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.686659307292" Y="0.297406011653" />
                  <Point X="-25.349912360469" Y="4.725418768492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.744430634443" Y="-0.948448137095" />
                  <Point X="-29.456398916106" Y="-0.566217136813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.785628847032" Y="0.32392480985" />
                  <Point X="-25.43166847203" Y="4.774780557415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.763407108873" Y="-0.815774955814" />
                  <Point X="-29.60544675735" Y="-0.606154489303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.884598386771" Y="0.350443608047" />
                  <Point X="-25.562126904633" Y="4.7595121834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.782383583303" Y="-0.683101774532" />
                  <Point X="-29.754494598594" Y="-0.646091841793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.983567926511" Y="0.376962406244" />
                  <Point X="-25.692585340326" Y="4.744243805284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.08253746625" Y="0.403481204441" />
                  <Point X="-25.823043776019" Y="4.728975427167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.181507004342" Y="0.430000004825" />
                  <Point X="-28.553354699252" Y="1.263586268484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.452873209158" Y="1.396929709581" />
                  <Point X="-25.956985794773" Y="4.709084178188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.280476539702" Y="0.456518808834" />
                  <Point X="-28.715265740117" Y="1.206578873547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.430169758124" Y="1.584914020116" />
                  <Point X="-26.107801383288" Y="4.666800945837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.379446075062" Y="0.483037612843" />
                  <Point X="-28.829817747629" Y="1.212419038579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.474308865188" Y="1.684195260062" />
                  <Point X="-26.672662575803" Y="4.075060638782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.472274402048" Y="4.340984727078" />
                  <Point X="-26.258617013091" Y="4.624517658694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.478415610421" Y="0.509556416853" />
                  <Point X="-28.938034727957" Y="1.22666606863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.534858690954" Y="1.761698740735" />
                  <Point X="-26.826144732758" Y="4.029238750591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.471072425874" Y="4.500435616742" />
                  <Point X="-26.40943296731" Y="4.582233941036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.577385145781" Y="0.536075220862" />
                  <Point X="-29.046251708285" Y="1.24091309868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.609747032" Y="1.820174368958" />
                  <Point X="-27.869687307447" Y="2.802266794115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.761193310044" Y="2.946243191546" />
                  <Point X="-26.929807246904" Y="4.049529761404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.676354681141" Y="0.562594024871" />
                  <Point X="-29.154468688613" Y="1.255160128731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.68511842871" Y="1.878008960662" />
                  <Point X="-28.047935212789" Y="2.723579647774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.754764380544" Y="3.112630482554" />
                  <Point X="-27.015912167342" Y="4.093120486027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.775324216501" Y="0.58911282888" />
                  <Point X="-29.262685668941" Y="1.269407158782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.760489825421" Y="1.935843552365" />
                  <Point X="-28.160584529329" Y="2.731944769006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.786911346409" Y="3.227825831379" />
                  <Point X="-27.094679711914" Y="4.146448237298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.759351807819" Y="0.768164744516" />
                  <Point X="-29.370902649269" Y="1.283654188833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.835861222132" Y="1.993678144068" />
                  <Point X="-28.255851420776" Y="2.763377147447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.838465515235" Y="3.317266952012" />
                  <Point X="-27.15684846098" Y="4.22180333419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.728927932542" Y="0.966394404064" />
                  <Point X="-29.479119636168" Y="1.297901210165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.911232618051" Y="2.051512736822" />
                  <Point X="-28.338741617734" Y="2.811233954217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.890067885529" Y="3.406644107137" />
                  <Point X="-27.27550772386" Y="4.222192987254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.66213172154" Y="1.212891783384" />
                  <Point X="-29.587336624112" Y="1.312148230108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.98660398664" Y="2.109347365845" />
                  <Point X="-28.421631814692" Y="2.859090760987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.941670255822" Y="3.496021262263" />
                  <Point X="-27.48012548007" Y="4.10851186687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.061975355228" Y="2.167181994867" />
                  <Point X="-28.50452201165" Y="2.906947567758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.993272082715" Y="3.585399138505" />
                  <Point X="-27.698448951552" Y="3.976642648009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.137346723817" Y="2.22501662389" />
                  <Point X="-28.587412208608" Y="2.954804374528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.044873817033" Y="3.674777137598" />
                  <Point X="-27.980157065304" Y="3.760659167852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.212718092406" Y="2.282851252912" />
                  <Point X="-28.670302448256" Y="3.002661124647" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.0001640625" Y="0.001626220703" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.299580078125" Y="-4.391298828125" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.629228515625" Y="-3.386758544922" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.870423828125" Y="-3.221471435547" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.153423828125" Y="-3.232694824219" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643798828" />
                  <Point X="-25.381826171875" Y="-3.420429199219" />
                  <Point X="-25.452005859375" Y="-3.521544189453" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.6309296875" Y="-4.177915527344" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.9732421875" Y="-4.962717285156" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.26773828125" Y="-4.894971191406" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.336701171875" Y="-4.760313476562" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.344265625" Y="-4.360895507812" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.519560546875" Y="-4.085747558594" />
                  <Point X="-26.61954296875" Y="-3.998064208984" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.82612890625" Y="-3.974168945312" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791259766" />
                  <Point X="-27.137271484375" Y="-4.072276367188" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.356130859375" Y="-4.282922363281" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.669671875" Y="-4.282876953125" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.08776171875" Y="-3.989034179688" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.93137890625" Y="-3.365838623047" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597594482422" />
                  <Point X="-27.5139765625" Y="-2.568766113281" />
                  <Point X="-27.53132421875" Y="-2.551417480469" />
                  <Point X="-27.560154296875" Y="-2.537198730469" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.14436328125" Y="-2.862559814453" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-29.014626953125" Y="-3.040357666016" />
                  <Point X="-29.161697265625" Y="-2.847136962891" />
                  <Point X="-29.3279765625" Y="-2.568312988281" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-28.908001953125" Y="-1.994201538086" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.396014526367" />
                  <Point X="-28.1381171875" Y="-1.366267211914" />
                  <Point X="-28.14032421875" Y="-1.334595825195" />
                  <Point X="-28.161158203125" Y="-1.310638916016" />
                  <Point X="-28.187640625" Y="-1.295052490234" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.917234375" Y="-1.38042590332" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.869869140625" Y="-1.236390991211" />
                  <Point X="-29.927392578125" Y="-1.011187744141" />
                  <Point X="-29.971384765625" Y="-0.703604248047" />
                  <Point X="-29.998396484375" Y="-0.514742248535" />
                  <Point X="-29.405236328125" Y="-0.355805633545" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.5260390625" Y="-0.110420043945" />
                  <Point X="-28.502322265625" Y="-0.09064516449" />
                  <Point X="-28.489611328125" Y="-0.058875602722" />
                  <Point X="-28.483400390625" Y="-0.031280040741" />
                  <Point X="-28.490931640625" Y="0.000569912016" />
                  <Point X="-28.502322265625" Y="0.028085084915" />
                  <Point X="-28.53" Y="0.05060931778" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.193455078125" Y="0.236498886108" />
                  <Point X="-29.998185546875" Y="0.452126037598" />
                  <Point X="-29.954716796875" Y="0.745887878418" />
                  <Point X="-29.91764453125" Y="0.996414855957" />
                  <Point X="-29.82908984375" Y="1.323211791992" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.36096875" Y="1.473986083984" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.696607421875" Y="1.406932495117" />
                  <Point X="-28.67027734375" Y="1.415234008789" />
                  <Point X="-28.651533203125" Y="1.426056152344" />
                  <Point X="-28.639119140625" Y="1.443786132813" />
                  <Point X="-28.625037109375" Y="1.47778503418" />
                  <Point X="-28.61447265625" Y="1.503290527344" />
                  <Point X="-28.610712890625" Y="1.524605834961" />
                  <Point X="-28.61631640625" Y="1.545512451172" />
                  <Point X="-28.63330859375" Y="1.578154418945" />
                  <Point X="-28.646056640625" Y="1.602642211914" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-29.024775390625" Y="1.899147949219" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.3040625" Y="2.540215332031" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-28.925435546875" Y="3.088523193359" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.532390625" Y="3.142430908203" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.0896328125" Y="2.916158447266" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775146484" />
                  <Point X="-28.013251953125" Y="2.927403808594" />
                  <Point X="-27.978556640625" Y="2.962099121094" />
                  <Point X="-27.95252734375" Y="2.988127197266" />
                  <Point X="-27.9408984375" Y="3.006381103516" />
                  <Point X="-27.938072265625" Y="3.027839599609" />
                  <Point X="-27.942349609375" Y="3.076719726562" />
                  <Point X="-27.945556640625" Y="3.113389160156" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.113724609375" Y="3.414030761719" />
                  <Point X="-28.30727734375" Y="3.749276855469" />
                  <Point X="-28.00376171875" Y="3.981979003906" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.383421875" Y="4.379591308594" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.082083984375" Y="4.436478027344" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.89684375" Y="4.24533984375" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.83512109375" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.757142578125" Y="4.245722167969" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.69690234375" Y="4.275744628906" />
                  <Point X="-26.68608203125" Y="4.294488769531" />
                  <Point X="-26.667638671875" Y="4.352983398438" />
                  <Point X="-26.653802734375" Y="4.396865234375" />
                  <Point X="-26.651916015625" Y="4.418426757812" />
                  <Point X="-26.670294921875" Y="4.558025878906" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.2928828125" Y="4.812236816406" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.520205078125" Y="4.955715332031" />
                  <Point X="-25.22419921875" Y="4.990358398438" />
                  <Point X="-25.152552734375" Y="4.722972167969" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.86271484375" Y="4.620033691406" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.4233828125" Y="4.955265136719" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.7692421875" Y="4.836103515625" />
                  <Point X="-23.491544921875" Y="4.76905859375" />
                  <Point X="-23.250013671875" Y="4.681453125" />
                  <Point X="-23.06896484375" Y="4.615785644531" />
                  <Point X="-22.835744140625" Y="4.506716308594" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.435998046875" Y="4.293873046875" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-22.054986328125" Y="4.044581054688" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.27783984375" Y="3.356295166016" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491515380859" />
                  <Point X="-22.7874140625" Y="2.445642578125" />
                  <Point X="-22.7966171875" Y="2.411229003906" />
                  <Point X="-22.797955078125" Y="2.392326416016" />
                  <Point X="-22.793171875" Y="2.352659423828" />
                  <Point X="-22.789583984375" Y="2.322901611328" />
                  <Point X="-22.78131640625" Y="2.300811523438" />
                  <Point X="-22.756771484375" Y="2.264639160156" />
                  <Point X="-22.738357421875" Y="2.237502929688" />
                  <Point X="-22.72505859375" Y="2.224203369141" />
                  <Point X="-22.68888671875" Y="2.199658691406" />
                  <Point X="-22.66175" Y="2.181245849609" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.59999609375" Y="2.168197021484" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5513359375" Y="2.165946289062" />
                  <Point X="-22.505462890625" Y="2.178213378906" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.819279296875" Y="2.561740234375" />
                  <Point X="-21.005751953125" Y="3.031430664062" />
                  <Point X="-20.897369140625" Y="2.880804443359" />
                  <Point X="-20.79740234375" Y="2.741874267578" />
                  <Point X="-20.678947265625" Y="2.546125" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.063216796875" Y="2.090437744141" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583833374023" />
                  <Point X="-21.753642578125" Y="1.540763061523" />
                  <Point X="-21.77841015625" Y="1.508452026367" />
                  <Point X="-21.78687890625" Y="1.491500976562" />
                  <Point X="-21.799177734375" Y="1.447526000977" />
                  <Point X="-21.808404296875" Y="1.414536499023" />
                  <Point X="-21.809220703125" Y="1.390964477539" />
                  <Point X="-21.799125" Y="1.342036743164" />
                  <Point X="-21.79155078125" Y="1.305331665039" />
                  <Point X="-21.784353515625" Y="1.287956298828" />
                  <Point X="-21.75689453125" Y="1.246220581055" />
                  <Point X="-21.736296875" Y="1.214910888672" />
                  <Point X="-21.71905078125" Y="1.198819702148" />
                  <Point X="-21.679259765625" Y="1.176420776367" />
                  <Point X="-21.649408203125" Y="1.15961730957" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.5776328125" Y="1.146509155273" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.916537109375" Y="1.221171630859" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.103744140625" Y="1.127740966797" />
                  <Point X="-20.06080859375" Y="0.951367431641" />
                  <Point X="-20.02348046875" Y="0.71162109375" />
                  <Point X="-20.002140625" Y="0.574556396484" />
                  <Point X="-20.51601953125" Y="0.436862640381" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819198608" />
                  <Point X="-21.32376953125" Y="0.202266799927" />
                  <Point X="-21.363421875" Y="0.179346832275" />
                  <Point X="-21.377734375" Y="0.166926086426" />
                  <Point X="-21.40944921875" Y="0.126514572144" />
                  <Point X="-21.433240234375" Y="0.096198410034" />
                  <Point X="-21.443013671875" Y="0.074734046936" />
                  <Point X="-21.4535859375" Y="0.019534093857" />
                  <Point X="-21.461515625" Y="-0.021876363754" />
                  <Point X="-21.461515625" Y="-0.040685844421" />
                  <Point X="-21.4509453125" Y="-0.095885948181" />
                  <Point X="-21.443013671875" Y="-0.137296401978" />
                  <Point X="-21.433240234375" Y="-0.158758483887" />
                  <Point X="-21.401525390625" Y="-0.199169845581" />
                  <Point X="-21.377734375" Y="-0.229486160278" />
                  <Point X="-21.363421875" Y="-0.241906906128" />
                  <Point X="-21.310564453125" Y="-0.27245916748" />
                  <Point X="-21.270912109375" Y="-0.295379272461" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.701580078125" Y="-0.449701843262" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.027595703125" Y="-0.807399108887" />
                  <Point X="-20.051568359375" Y="-0.966413085938" />
                  <Point X="-20.099392578125" Y="-1.175979370117" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-20.726564453125" Y="-1.211040527344" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.70890234375" Y="-1.120889648438" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.8145546875" Y="-1.154697631836" />
                  <Point X="-21.877259765625" Y="-1.230111206055" />
                  <Point X="-21.924298828125" Y="-1.286685668945" />
                  <Point X="-21.935640625" Y="-1.31407043457" />
                  <Point X="-21.944626953125" Y="-1.41173425293" />
                  <Point X="-21.951369140625" Y="-1.485000732422" />
                  <Point X="-21.943638671875" Y="-1.516622436523" />
                  <Point X="-21.886228515625" Y="-1.605921630859" />
                  <Point X="-21.843158203125" Y="-1.672912963867" />
                  <Point X="-21.831537109375" Y="-1.685540771484" />
                  <Point X="-21.31440625" Y="-2.082350097656" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.72829296875" Y="-2.692795898438" />
                  <Point X="-20.795865234375" Y="-2.802136962891" />
                  <Point X="-20.8947734375" Y="-2.942674072266" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.4793046875" Y="-2.702181884766" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.386125" Y="-2.231014892578" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.6134921875" Y="-2.273227050781" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.334683105469" />
                  <Point X="-22.765380859375" Y="-2.437253662109" />
                  <Point X="-22.80587890625" Y="-2.514201171875" />
                  <Point X="-22.8108359375" Y="-2.546375" />
                  <Point X="-22.788537109375" Y="-2.669841796875" />
                  <Point X="-22.77180859375" Y="-2.762465332031" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.43359765625" Y="-3.354155273438" />
                  <Point X="-22.01332421875" Y="-4.082088867188" />
                  <Point X="-22.084515625" Y="-4.132938964844" />
                  <Point X="-22.1646796875" Y="-4.190197265625" />
                  <Point X="-22.27528515625" Y="-4.261791015625" />
                  <Point X="-22.320224609375" Y="-4.290878417969" />
                  <Point X="-22.731345703125" Y="-3.755092773438" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.451220703125" Y="-2.902179443359" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.707373046875" Y="-2.847972167969" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.937501953125" Y="-2.954014160156" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045986328125" />
                  <Point X="-24.062291015625" Y="-3.187449707031" />
                  <Point X="-24.085357421875" Y="-3.29357421875" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-23.991896484375" Y="-4.026041992188" />
                  <Point X="-23.872357421875" Y="-4.934028320312" />
                  <Point X="-23.929794921875" Y="-4.946618652344" />
                  <Point X="-24.005650390625" Y="-4.96324609375" />
                  <Point X="-24.1078125" Y="-4.981805664062" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#174" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.110413401202" Y="4.767192917724" Z="1.45" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.45" />
                  <Point X="-0.532173715165" Y="5.036656325159" Z="1.45" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.45" />
                  <Point X="-1.31253717172" Y="4.891660490197" Z="1.45" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.45" />
                  <Point X="-1.726024460128" Y="4.582779778026" Z="1.45" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.45" />
                  <Point X="-1.721481873578" Y="4.399298544457" Z="1.45" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.45" />
                  <Point X="-1.782434438134" Y="4.32319602673" Z="1.45" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.45" />
                  <Point X="-1.879911916661" Y="4.320970563909" Z="1.45" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.45" />
                  <Point X="-2.048573885926" Y="4.498196206039" Z="1.45" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.45" />
                  <Point X="-2.413862410865" Y="4.454578872598" Z="1.45" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.45" />
                  <Point X="-3.040341486495" Y="4.052938631042" Z="1.45" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.45" />
                  <Point X="-3.163181617938" Y="3.420310911995" Z="1.45" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.45" />
                  <Point X="-2.998316501989" Y="3.103643560899" Z="1.45" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.45" />
                  <Point X="-3.020068178434" Y="3.028735389452" Z="1.45" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.45" />
                  <Point X="-3.091432904708" Y="2.997248197184" Z="1.45" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.45" />
                  <Point X="-3.513548436302" Y="3.217012330808" Z="1.45" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.45" />
                  <Point X="-3.971056153808" Y="3.150505524011" Z="1.45" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.45" />
                  <Point X="-4.353401833816" Y="2.596700549844" Z="1.45" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.45" />
                  <Point X="-4.061369071326" Y="1.89076017885" Z="1.45" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.45" />
                  <Point X="-3.683814780916" Y="1.586346381009" Z="1.45" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.45" />
                  <Point X="-3.677387175261" Y="1.528198836428" Z="1.45" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.45" />
                  <Point X="-3.717799240359" Y="1.485898511927" Z="1.45" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.45" />
                  <Point X="-4.360601007903" Y="1.554838431772" Z="1.45" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.45" />
                  <Point X="-4.883506315562" Y="1.367569163071" Z="1.45" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.45" />
                  <Point X="-5.010334640017" Y="0.784486982471" Z="1.45" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.45" />
                  <Point X="-4.212553204362" Y="0.219482703064" Z="1.45" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.45" />
                  <Point X="-3.564664913502" Y="0.040812652136" Z="1.45" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.45" />
                  <Point X="-3.544842517224" Y="0.01703068288" Z="1.45" />
                  <Point X="-3.539556741714" Y="0" Z="1.45" />
                  <Point X="-3.543522083911" Y="-0.012776268181" Z="1.45" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.45" />
                  <Point X="-3.560703681622" Y="-0.038063330998" Z="1.45" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.45" />
                  <Point X="-4.42433458441" Y="-0.276229353692" Z="1.45" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.45" />
                  <Point X="-5.027037064122" Y="-0.679403116116" Z="1.45" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.45" />
                  <Point X="-4.924471078486" Y="-1.217485021041" Z="1.45" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.45" />
                  <Point X="-3.916865251897" Y="-1.398718075741" Z="1.45" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.45" />
                  <Point X="-3.207807556751" Y="-1.313544207479" Z="1.45" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.45" />
                  <Point X="-3.195978713716" Y="-1.335200531747" Z="1.45" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.45" />
                  <Point X="-3.944596524994" Y="-1.923253955165" Z="1.45" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.45" />
                  <Point X="-4.377076967139" Y="-2.562642748307" Z="1.45" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.45" />
                  <Point X="-4.060569212888" Y="-3.039360822235" Z="1.45" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.45" />
                  <Point X="-3.1255197209" Y="-2.874581041347" Z="1.45" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.45" />
                  <Point X="-2.565403332216" Y="-2.562927136915" Z="1.45" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.45" />
                  <Point X="-2.980835971822" Y="-3.309558661592" Z="1.45" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.45" />
                  <Point X="-3.124421685192" Y="-3.99737139363" Z="1.45" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.45" />
                  <Point X="-2.702151904531" Y="-4.29410745636" Z="1.45" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.45" />
                  <Point X="-2.322620090899" Y="-4.28208021241" Z="1.45" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.45" />
                  <Point X="-2.115649197007" Y="-4.082569500476" Z="1.45" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.45" />
                  <Point X="-1.835555608559" Y="-3.992782020167" Z="1.45" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.45" />
                  <Point X="-1.558683344454" Y="-4.092059234343" Z="1.45" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.45" />
                  <Point X="-1.399461867872" Y="-4.339370239566" Z="1.45" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.45" />
                  <Point X="-1.392430105436" Y="-4.722507093224" Z="1.45" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.45" />
                  <Point X="-1.286353216104" Y="-4.912113851541" Z="1.45" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.45" />
                  <Point X="-0.988925803739" Y="-4.980521223371" Z="1.45" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.45" />
                  <Point X="-0.588789484917" Y="-4.159576342999" Z="1.45" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.45" />
                  <Point X="-0.346907545414" Y="-3.417658181435" Z="1.45" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.45" />
                  <Point X="-0.14476080182" Y="-3.249167779151" Z="1.45" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.45" />
                  <Point X="0.108598277541" Y="-3.237944266137" Z="1.45" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.45" />
                  <Point X="0.323538314018" Y="-3.383987574333" Z="1.45" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.45" />
                  <Point X="0.645965375485" Y="-4.372959686817" Z="1.45" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.45" />
                  <Point X="0.894968799574" Y="-4.999720231401" Z="1.45" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.45" />
                  <Point X="1.07475397009" Y="-4.964179177762" Z="1.45" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.45" />
                  <Point X="1.05151972279" Y="-3.988235694099" Z="1.45" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.45" />
                  <Point X="0.980412385291" Y="-3.166789233616" Z="1.45" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.45" />
                  <Point X="1.088307137311" Y="-2.961180712038" Z="1.45" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.45" />
                  <Point X="1.29105260291" Y="-2.866481736417" Z="1.45" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.45" />
                  <Point X="1.515582377218" Y="-2.912957433737" Z="1.45" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.45" />
                  <Point X="2.222828662518" Y="-3.754250683549" Z="1.45" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.45" />
                  <Point X="2.745727531938" Y="-4.27248561072" Z="1.45" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.45" />
                  <Point X="2.938387830492" Y="-4.142345982851" Z="1.45" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.45" />
                  <Point X="2.603546307486" Y="-3.297875164883" Z="1.45" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.45" />
                  <Point X="2.254509124679" Y="-2.629674944631" Z="1.45" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.45" />
                  <Point X="2.272708046473" Y="-2.429260696643" Z="1.45" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.45" />
                  <Point X="2.40363768173" Y="-2.286193263494" Z="1.45" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.45" />
                  <Point X="2.598831858882" Y="-2.248938884812" Z="1.45" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.45" />
                  <Point X="3.489538432224" Y="-2.714203192789" Z="1.45" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.45" />
                  <Point X="4.139957564265" Y="-2.940171593329" Z="1.45" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.45" />
                  <Point X="4.308083475974" Y="-2.687800953453" Z="1.45" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.45" />
                  <Point X="3.709874871127" Y="-2.011402457843" Z="1.45" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.45" />
                  <Point X="3.149673559002" Y="-1.547601616981" Z="1.45" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.45" />
                  <Point X="3.09900446709" Y="-1.385035999756" Z="1.45" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.45" />
                  <Point X="3.155031481031" Y="-1.230797686118" Z="1.45" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.45" />
                  <Point X="3.295560147133" Y="-1.138468680214" Z="1.45" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.45" />
                  <Point X="4.260752536141" Y="-1.229332812268" Z="1.45" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.45" />
                  <Point X="4.943197002997" Y="-1.155823117338" Z="1.45" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.45" />
                  <Point X="5.015689089902" Y="-0.783573006263" Z="1.45" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.45" />
                  <Point X="4.305203458724" Y="-0.370125520894" Z="1.45" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.45" />
                  <Point X="3.708299788668" Y="-0.197890543563" Z="1.45" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.45" />
                  <Point X="3.631650968095" Y="-0.137021931255" Z="1.45" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.45" />
                  <Point X="3.592006141511" Y="-0.055199987314" Z="1.45" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.45" />
                  <Point X="3.589365308914" Y="0.041410543892" Z="1.45" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.45" />
                  <Point X="3.623728470305" Y="0.12692680732" Z="1.45" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.45" />
                  <Point X="3.695095625684" Y="0.190258300679" Z="1.45" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.45" />
                  <Point X="4.490764358582" Y="0.419846411875" Z="1.45" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.45" />
                  <Point X="5.019767189758" Y="0.750593125501" Z="1.45" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.45" />
                  <Point X="4.938680522715" Y="1.170847664828" Z="1.45" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.45" />
                  <Point X="4.070780213962" Y="1.302023906434" Z="1.45" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.45" />
                  <Point X="3.422760910043" Y="1.227358215432" Z="1.45" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.45" />
                  <Point X="3.339109671713" Y="1.251272077084" Z="1.45" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.45" />
                  <Point X="3.278719456717" Y="1.304980750414" Z="1.45" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.45" />
                  <Point X="3.243687480458" Y="1.383421521138" Z="1.45" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.45" />
                  <Point X="3.242817928" Y="1.465338793899" Z="1.45" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.45" />
                  <Point X="3.279883381194" Y="1.54162472149" Z="1.45" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.45" />
                  <Point X="3.961063758608" Y="2.082050040905" Z="1.45" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.45" />
                  <Point X="4.35767258007" Y="2.603291217016" Z="1.45" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.45" />
                  <Point X="4.137059471475" Y="2.941287514197" Z="1.45" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.45" />
                  <Point X="3.14956389424" Y="2.636321616123" Z="1.45" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.45" />
                  <Point X="2.475464509555" Y="2.257796173533" Z="1.45" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.45" />
                  <Point X="2.399833807454" Y="2.249117420132" Z="1.45" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.45" />
                  <Point X="2.33303052952" Y="2.272313585552" Z="1.45" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.45" />
                  <Point X="2.278445014683" Y="2.323994330862" Z="1.45" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.45" />
                  <Point X="2.250312282627" Y="2.389924636378" Z="1.45" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.45" />
                  <Point X="2.254731769124" Y="2.464005066327" Z="1.45" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.45" />
                  <Point X="2.759303736736" Y="3.362575187715" Z="1.45" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.45" />
                  <Point X="2.967833706464" Y="4.116608173807" Z="1.45" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.45" />
                  <Point X="2.583015103816" Y="4.368355637755" Z="1.45" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.45" />
                  <Point X="2.17928067013" Y="4.583288154958" Z="1.45" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.45" />
                  <Point X="1.760878712868" Y="4.759737220333" Z="1.45" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.45" />
                  <Point X="1.236334352542" Y="4.915986996156" Z="1.45" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.45" />
                  <Point X="0.57566798298" Y="5.036273086503" Z="1.45" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.45" />
                  <Point X="0.082831160351" Y="4.664254588227" Z="1.45" />
                  <Point X="0" Y="4.355124473572" Z="1.45" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>