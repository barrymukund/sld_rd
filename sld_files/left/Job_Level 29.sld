<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#173" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2146" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.215703125" Y="-4.337282226563" />
                  <Point X="-24.4366953125" Y="-3.512524414063" />
                  <Point X="-24.44227734375" Y="-3.497140136719" />
                  <Point X="-24.457634765625" Y="-3.467376953125" />
                  <Point X="-24.5479609375" Y="-3.337235107422" />
                  <Point X="-24.62136328125" Y="-3.2314765625" />
                  <Point X="-24.643248046875" Y="-3.209020751953" />
                  <Point X="-24.66950390625" Y="-3.189776855469" />
                  <Point X="-24.69750390625" Y="-3.175669189453" />
                  <Point X="-24.83727734375" Y="-3.132288574219" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.17659765625" Y="-3.140416259766" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.189776855469" />
                  <Point X="-25.344439453125" Y="-3.209020751953" />
                  <Point X="-25.36632421875" Y="-3.231477294922" />
                  <Point X="-25.4566484375" Y="-3.361618896484" />
                  <Point X="-25.53005078125" Y="-3.467377685547" />
                  <Point X="-25.5381875" Y="-3.481572021484" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.730578125" Y="-4.182756347656" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-25.949240234375" Y="-4.870603027344" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.238244140625" Y="-4.804465332031" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.241564453125" Y="-4.765502929688" />
                  <Point X="-26.2149609375" Y="-4.563437988281" />
                  <Point X="-26.21419921875" Y="-4.547929199219" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.249900390625" Y="-4.348351074219" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.182964355469" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.452330078125" Y="-4.018349609375" />
                  <Point X="-26.556904296875" Y="-3.926639404297" />
                  <Point X="-26.5831875" Y="-3.910295898438" />
                  <Point X="-26.612884765625" Y="-3.897994628906" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.813822265625" Y="-3.879771728516" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708740234" />
                  <Point X="-27.014466796875" Y="-3.882028564453" />
                  <Point X="-27.042658203125" Y="-3.894801757812" />
                  <Point X="-27.18497265625" Y="-3.989893798828" />
                  <Point X="-27.300623046875" Y="-4.067169677734" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.3351015625" Y="-4.099461425781" />
                  <Point X="-27.435927734375" Y="-4.230858886719" />
                  <Point X="-27.4801484375" Y="-4.288488769531" />
                  <Point X="-27.61101171875" Y="-4.207461425781" />
                  <Point X="-27.801712890625" Y="-4.089384277344" />
                  <Point X="-28.021744140625" Y="-3.919967773438" />
                  <Point X="-28.104720703125" Y="-3.856078369141" />
                  <Point X="-27.834451171875" Y="-3.387956787109" />
                  <Point X="-27.423759765625" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647655029297" />
                  <Point X="-27.406587890625" Y="-2.616129150391" />
                  <Point X="-27.40557421875" Y="-2.585194824219" />
                  <Point X="-27.41455859375" Y="-2.555576416016" />
                  <Point X="-27.428775390625" Y="-2.526747558594" />
                  <Point X="-27.44680078125" Y="-2.501591552734" />
                  <Point X="-27.4641484375" Y="-2.484242919922" />
                  <Point X="-27.489306640625" Y="-2.466214355469" />
                  <Point X="-27.518134765625" Y="-2.451997070313" />
                  <Point X="-27.547751953125" Y="-2.443011962891" />
                  <Point X="-27.5786875" Y="-2.444024169922" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.217244140625" Y="-2.794941650391" />
                  <Point X="-28.8180234375" Y="-3.141801513672" />
                  <Point X="-28.932201171875" Y="-2.991796386719" />
                  <Point X="-29.082859375" Y="-2.793860595703" />
                  <Point X="-29.24060546875" Y="-2.529344238281" />
                  <Point X="-29.306142578125" Y="-2.419449951172" />
                  <Point X="-28.824529296875" Y="-2.049895751953" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.08458203125" Y="-1.475598754883" />
                  <Point X="-28.0666171875" Y="-1.448471679688" />
                  <Point X="-28.053857421875" Y="-1.419838500977" />
                  <Point X="-28.046150390625" Y="-1.390083862305" />
                  <Point X="-28.043345703125" Y="-1.359646850586" />
                  <Point X="-28.045556640625" Y="-1.327977539062" />
                  <Point X="-28.05255859375" Y="-1.298234985352" />
                  <Point X="-28.068640625" Y="-1.27225378418" />
                  <Point X="-28.08947265625" Y="-1.248298828125" />
                  <Point X="-28.112970703125" Y="-1.228766845703" />
                  <Point X="-28.139453125" Y="-1.213180419922" />
                  <Point X="-28.16871875" Y="-1.201956054688" />
                  <Point X="-28.20060546875" Y="-1.195474731445" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-28.96167578125" Y="-1.29045703125" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.77515234375" Y="-1.223341918945" />
                  <Point X="-29.834078125" Y="-0.992650268555" />
                  <Point X="-29.8758125" Y="-0.700843444824" />
                  <Point X="-29.892423828125" Y="-0.584698486328" />
                  <Point X="-29.351439453125" Y="-0.43974230957" />
                  <Point X="-28.532875" Y="-0.220408432007" />
                  <Point X="-28.50992578125" Y="-0.210896179199" />
                  <Point X="-28.479048828125" Y="-0.193050796509" />
                  <Point X="-28.4658828125" Y="-0.18387689209" />
                  <Point X="-28.441619140625" Y="-0.163721862793" />
                  <Point X="-28.42555078125" Y="-0.146601470947" />
                  <Point X="-28.414171875" Y="-0.126062675476" />
                  <Point X="-28.401642578125" Y="-0.094878707886" />
                  <Point X="-28.397146484375" Y="-0.080476837158" />
                  <Point X="-28.39075390625" Y="-0.052296051025" />
                  <Point X="-28.388400390625" Y="-0.030907636642" />
                  <Point X="-28.390921875" Y="-0.009538374901" />
                  <Point X="-28.398271484375" Y="0.021723304749" />
                  <Point X="-28.40290625" Y="0.036153862" />
                  <Point X="-28.414478515625" Y="0.064257087708" />
                  <Point X="-28.426033203125" Y="0.084697891235" />
                  <Point X="-28.44225" Y="0.101680091858" />
                  <Point X="-28.469380859375" Y="0.123825996399" />
                  <Point X="-28.48265625" Y="0.132905029297" />
                  <Point X="-28.510666015625" Y="0.148759689331" />
                  <Point X="-28.532875" Y="0.157848297119" />
                  <Point X="-29.19807421875" Y="0.336088256836" />
                  <Point X="-29.89181640625" Y="0.521975524902" />
                  <Point X="-29.862462890625" Y="0.720342346191" />
                  <Point X="-29.82448828125" Y="0.97696862793" />
                  <Point X="-29.740474609375" Y="1.287008422852" />
                  <Point X="-29.70355078125" Y="1.423268066406" />
                  <Point X="-29.35243359375" Y="1.377042358398" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228149414" />
                  <Point X="-28.703134765625" Y="1.305263671875" />
                  <Point X="-28.669248046875" Y="1.315948364258" />
                  <Point X="-28.641708984375" Y="1.324631225586" />
                  <Point X="-28.622775390625" Y="1.332962768555" />
                  <Point X="-28.60403125" Y="1.34378527832" />
                  <Point X="-28.5873515625" Y="1.356015014648" />
                  <Point X="-28.57371484375" Y="1.371564453125" />
                  <Point X="-28.561298828125" Y="1.389295288086" />
                  <Point X="-28.551349609375" Y="1.407432250977" />
                  <Point X="-28.537751953125" Y="1.440259765625" />
                  <Point X="-28.526703125" Y="1.466936645508" />
                  <Point X="-28.520912109375" Y="1.486806152344" />
                  <Point X="-28.51715625" Y="1.508121337891" />
                  <Point X="-28.515806640625" Y="1.528755859375" />
                  <Point X="-28.518951171875" Y="1.549193725586" />
                  <Point X="-28.524552734375" Y="1.570099975586" />
                  <Point X="-28.532048828125" Y="1.589377807617" />
                  <Point X="-28.548455078125" Y="1.620895263672" />
                  <Point X="-28.5617890625" Y="1.646507446289" />
                  <Point X="-28.573283203125" Y="1.663709716797" />
                  <Point X="-28.5871953125" Y="1.680288574219" />
                  <Point X="-28.60213671875" Y="1.694590332031" />
                  <Point X="-28.983697265625" Y="1.987372070312" />
                  <Point X="-29.351859375" Y="2.269873535156" />
                  <Point X="-29.228708984375" Y="2.480859863281" />
                  <Point X="-29.0811484375" Y="2.733664794922" />
                  <Point X="-28.858607421875" Y="3.019710449219" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.567033203125" Y="3.052734619141" />
                  <Point X="-28.20665625" Y="2.844671142578" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.09959765625" Y="2.821667236328" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.04056640625" Y="2.818762939453" />
                  <Point X="-28.019107421875" Y="2.821587890625" />
                  <Point X="-27.999015625" Y="2.826504150391" />
                  <Point X="-27.980462890625" Y="2.83565234375" />
                  <Point X="-27.962208984375" Y="2.847281005859" />
                  <Point X="-27.946076171875" Y="2.860228759766" />
                  <Point X="-27.912576171875" Y="2.893728759766" />
                  <Point X="-27.8853515625" Y="2.920952148438" />
                  <Point X="-27.872404296875" Y="2.937085449219" />
                  <Point X="-27.860775390625" Y="2.955340087891" />
                  <Point X="-27.85162890625" Y="2.973888916016" />
                  <Point X="-27.8467109375" Y="2.993977539062" />
                  <Point X="-27.843884765625" Y="3.015436767578" />
                  <Point X="-27.84343359375" Y="3.036120361328" />
                  <Point X="-27.8475625" Y="3.08331640625" />
                  <Point X="-27.85091796875" Y="3.121669921875" />
                  <Point X="-27.854955078125" Y="3.141962646484" />
                  <Point X="-27.86146484375" Y="3.162604980469" />
                  <Point X="-27.869794921875" Y="3.181533203125" />
                  <Point X="-28.038876953125" Y="3.474390136719" />
                  <Point X="-28.18333203125" Y="3.724595703125" />
                  <Point X="-27.9576171875" Y="3.897650390625" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.350125" Y="4.2894140625" />
                  <Point X="-27.16703515625" Y="4.391133789062" />
                  <Point X="-27.153517578125" Y="4.373516113281" />
                  <Point X="-27.0431953125" Y="4.229741210938" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.99511328125" Y="4.18939453125" />
                  <Point X="-26.942583984375" Y="4.162049316406" />
                  <Point X="-26.899896484375" Y="4.139827636719" />
                  <Point X="-26.8806171875" Y="4.132331054688" />
                  <Point X="-26.8597109375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.12358203125" />
                  <Point X="-26.818626953125" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.777453125" Y="4.134481933594" />
                  <Point X="-26.7227421875" Y="4.157145019531" />
                  <Point X="-26.678279296875" Y="4.175561523437" />
                  <Point X="-26.6601484375" Y="4.185508300781" />
                  <Point X="-26.64241796875" Y="4.197922363281" />
                  <Point X="-26.626865234375" Y="4.211560546875" />
                  <Point X="-26.6146328125" Y="4.228241699219" />
                  <Point X="-26.603810546875" Y="4.246985351563" />
                  <Point X="-26.595478515625" Y="4.265921875" />
                  <Point X="-26.577671875" Y="4.322401367188" />
                  <Point X="-26.56319921875" Y="4.368298339844" />
                  <Point X="-26.5591640625" Y="4.388583007812" />
                  <Point X="-26.55727734375" Y="4.410145996094" />
                  <Point X="-26.557728515625" Y="4.430826660156" />
                  <Point X="-26.576951171875" Y="4.576836914062" />
                  <Point X="-26.584201171875" Y="4.6318984375" />
                  <Point X="-26.282326171875" Y="4.716533691406" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.5247265625" Y="4.859537597656" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.240513671875" Y="4.6841875" />
                  <Point X="-25.133904296875" Y="4.286315917969" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286314941406" />
                  <Point X="-24.767146484375" Y="4.609642089844" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.446451171875" Y="4.862161621094" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.804416015625" Y="4.746865722656" />
                  <Point X="-23.51897265625" Y="4.677950195312" />
                  <Point X="-23.29081640625" Y="4.595197265625" />
                  <Point X="-23.105353515625" Y="4.527928222656" />
                  <Point X="-22.88409375" Y="4.424452148438" />
                  <Point X="-22.705423828125" Y="4.340893554688" />
                  <Point X="-22.491650390625" Y="4.216349609375" />
                  <Point X="-22.319021484375" Y="4.115775878906" />
                  <Point X="-22.117427734375" Y="3.972413085938" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.377072265625" Y="3.374417480469" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857921875" Y="2.539933105469" />
                  <Point X="-22.866921875" Y="2.516058837891" />
                  <Point X="-22.878767578125" Y="2.471766357422" />
                  <Point X="-22.888392578125" Y="2.435772460938" />
                  <Point X="-22.891380859375" Y="2.417936035156" />
                  <Point X="-22.892271484375" Y="2.380952636719" />
                  <Point X="-22.88765234375" Y="2.34265234375" />
                  <Point X="-22.883900390625" Y="2.311527832031" />
                  <Point X="-22.878556640625" Y="2.289605224609" />
                  <Point X="-22.8702890625" Y="2.267513671875" />
                  <Point X="-22.859927734375" Y="2.247469726562" />
                  <Point X="-22.836228515625" Y="2.212543457031" />
                  <Point X="-22.81696875" Y="2.184161132813" />
                  <Point X="-22.805533203125" Y="2.170328125" />
                  <Point X="-22.778400390625" Y="2.145592773438" />
                  <Point X="-22.743474609375" Y="2.121893798828" />
                  <Point X="-22.715091796875" Y="2.102635253906" />
                  <Point X="-22.695046875" Y="2.092272216797" />
                  <Point X="-22.672958984375" Y="2.084006347656" />
                  <Point X="-22.65103515625" Y="2.078663330078" />
                  <Point X="-22.612734375" Y="2.074044921875" />
                  <Point X="-22.581611328125" Y="2.070291748047" />
                  <Point X="-22.563533203125" Y="2.069845703125" />
                  <Point X="-22.526794921875" Y="2.074171142578" />
                  <Point X="-22.482501953125" Y="2.086015625" />
                  <Point X="-22.4465078125" Y="2.095640869141" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.74240234375" Y="2.496428955078" />
                  <Point X="-21.032673828125" Y="2.906190673828" />
                  <Point X="-20.979126953125" Y="2.831772705078" />
                  <Point X="-20.87671875" Y="2.689450927734" />
                  <Point X="-20.764341796875" Y="2.503744140625" />
                  <Point X="-20.73780078125" Y="2.459883544922" />
                  <Point X="-21.14337890625" Y="2.148670898438" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.778572265625" Y="1.660243408203" />
                  <Point X="-21.79602734375" Y="1.641626953125" />
                  <Point X="-21.827904296875" Y="1.600040527344" />
                  <Point X="-21.85380859375" Y="1.566245605469" />
                  <Point X="-21.86339453125" Y="1.550911010742" />
                  <Point X="-21.878369140625" Y="1.517088500977" />
                  <Point X="-21.890244140625" Y="1.474628540039" />
                  <Point X="-21.89989453125" Y="1.440124023438" />
                  <Point X="-21.90334765625" Y="1.417824951172" />
                  <Point X="-21.9041640625" Y="1.394253051758" />
                  <Point X="-21.902259765625" Y="1.371766845703" />
                  <Point X="-21.89251171875" Y="1.324524414062" />
                  <Point X="-21.88458984375" Y="1.286133911133" />
                  <Point X="-21.8793203125" Y="1.268978759766" />
                  <Point X="-21.86371875" Y="1.235741699219" />
                  <Point X="-21.83720703125" Y="1.195443969727" />
                  <Point X="-21.815662109375" Y="1.162696411133" />
                  <Point X="-21.80110546875" Y="1.14544934082" />
                  <Point X="-21.783861328125" Y="1.129359863281" />
                  <Point X="-21.76565234375" Y="1.116034301758" />
                  <Point X="-21.72723046875" Y="1.094407104492" />
                  <Point X="-21.696009765625" Y="1.07683190918" />
                  <Point X="-21.679478515625" Y="1.069501953125" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.59193359375" Y="1.052573120117" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.87623046875" Y="1.130658325195" />
                  <Point X="-20.22316015625" Y="1.21663659668" />
                  <Point X="-20.19804296875" Y="1.113463623047" />
                  <Point X="-20.154060546875" Y="0.932788146973" />
                  <Point X="-20.118646484375" Y="0.70533807373" />
                  <Point X="-20.109134765625" Y="0.644238708496" />
                  <Point X="-20.56619921875" Y="0.521768249512" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.295208984375" Y="0.32558605957" />
                  <Point X="-21.318453125" Y="0.315067749023" />
                  <Point X="-21.36948828125" Y="0.285568023682" />
                  <Point X="-21.410962890625" Y="0.261595336914" />
                  <Point X="-21.425685546875" Y="0.251097961426" />
                  <Point X="-21.45246875" Y="0.225576385498" />
                  <Point X="-21.48308984375" Y="0.186557159424" />
                  <Point X="-21.507974609375" Y="0.154848602295" />
                  <Point X="-21.51969921875" Y="0.135567596436" />
                  <Point X="-21.52947265625" Y="0.114103675842" />
                  <Point X="-21.536318359375" Y="0.092603782654" />
                  <Point X="-21.546525390625" Y="0.03930563736" />
                  <Point X="-21.5548203125" Y="-0.004006679058" />
                  <Point X="-21.556515625" Y="-0.02187528801" />
                  <Point X="-21.5548203125" Y="-0.058553302765" />
                  <Point X="-21.54461328125" Y="-0.111851455688" />
                  <Point X="-21.536318359375" Y="-0.155163925171" />
                  <Point X="-21.52947265625" Y="-0.176663803101" />
                  <Point X="-21.51969921875" Y="-0.198127731323" />
                  <Point X="-21.507974609375" Y="-0.217408584595" />
                  <Point X="-21.477353515625" Y="-0.256427825928" />
                  <Point X="-21.45246875" Y="-0.288136535645" />
                  <Point X="-21.439998046875" Y="-0.301237854004" />
                  <Point X="-21.410962890625" Y="-0.324155609131" />
                  <Point X="-21.359927734375" Y="-0.353655151367" />
                  <Point X="-21.318453125" Y="-0.377627868652" />
                  <Point X="-21.3072890625" Y="-0.383138763428" />
                  <Point X="-21.283419921875" Y="-0.392149963379" />
                  <Point X="-20.700576171875" Y="-0.548322021484" />
                  <Point X="-20.108525390625" Y="-0.706961730957" />
                  <Point X="-20.120419921875" Y="-0.785849365234" />
                  <Point X="-20.144974609375" Y="-0.948724365234" />
                  <Point X="-20.190349609375" Y="-1.147559814453" />
                  <Point X="-20.19882421875" Y="-1.184698974609" />
                  <Point X="-20.74384375" Y="-1.112945922852" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710266113" />
                  <Point X="-21.62533984375" Y="-1.005508666992" />
                  <Point X="-21.725505859375" Y="-1.027280395508" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.836025390625" Y="-1.056597045898" />
                  <Point X="-21.8638515625" Y="-1.073489013672" />
                  <Point X="-21.8876015625" Y="-1.093960327148" />
                  <Point X="-21.94814453125" Y="-1.166775268555" />
                  <Point X="-21.997345703125" Y="-1.225947998047" />
                  <Point X="-22.012064453125" Y="-1.250329956055" />
                  <Point X="-22.023408203125" Y="-1.277716186523" />
                  <Point X="-22.030240234375" Y="-1.305365722656" />
                  <Point X="-22.03891796875" Y="-1.399665039062" />
                  <Point X="-22.04596875" Y="-1.476296020508" />
                  <Point X="-22.043650390625" Y="-1.507567382812" />
                  <Point X="-22.03591796875" Y="-1.539188354492" />
                  <Point X="-22.023546875" Y="-1.567996337891" />
                  <Point X="-21.968115234375" Y="-1.654218994141" />
                  <Point X="-21.92306640625" Y="-1.724286987305" />
                  <Point X="-21.913064453125" Y="-1.737238647461" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.34848828125" Y="-2.175942138672" />
                  <Point X="-20.786875" Y="-2.606883056641" />
                  <Point X="-20.80596484375" Y="-2.6377734375" />
                  <Point X="-20.8751875" Y="-2.749781738281" />
                  <Point X="-20.969025390625" Y="-2.883114013672" />
                  <Point X="-20.971017578125" Y="-2.885945068359" />
                  <Point X="-21.45823828125" Y="-2.604648193359" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.2457734375" Y="-2.159825195312" />
                  <Point X="-22.364986328125" Y="-2.138295410156" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135176757812" />
                  <Point X="-22.654203125" Y="-2.187299072266" />
                  <Point X="-22.73468359375" Y="-2.229655761719" />
                  <Point X="-22.757615234375" Y="-2.246549560547" />
                  <Point X="-22.77857421875" Y="-2.267509033203" />
                  <Point X="-22.795466796875" Y="-2.290438476562" />
                  <Point X="-22.84758984375" Y="-2.389475097656" />
                  <Point X="-22.889947265625" Y="-2.469956542969" />
                  <Point X="-22.899771484375" Y="-2.499735107422" />
                  <Point X="-22.904728515625" Y="-2.531908203125" />
                  <Point X="-22.90432421875" Y="-2.563259521484" />
                  <Point X="-22.88279296875" Y="-2.682472412109" />
                  <Point X="-22.865296875" Y="-2.779349853516" />
                  <Point X="-22.86101171875" Y="-2.795139648438" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.500607421875" Y="-3.428088623047" />
                  <Point X="-22.138712890625" Y="-4.054906005859" />
                  <Point X="-22.218134765625" Y="-4.111634277344" />
                  <Point X="-22.298234375" Y="-4.163481445312" />
                  <Point X="-22.6761484375" Y="-3.670972900391" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.395650390625" Y="-2.824966552734" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.711490234375" Y="-2.752949707031" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-23.9946953125" Y="-2.878020507812" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968860839844" />
                  <Point X="-24.11275" Y="-2.996687255859" />
                  <Point X="-24.124375" Y="-3.025808837891" />
                  <Point X="-24.1540625" Y="-3.1623984375" />
                  <Point X="-24.178189453125" Y="-3.273396728516" />
                  <Point X="-24.180275390625" Y="-3.289627197266" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.0817578125" Y="-4.071293212891" />
                  <Point X="-23.97793359375" Y="-4.859916015625" />
                  <Point X="-24.02430859375" Y="-4.870081054688" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.05842578125" Y="-4.752636230469" />
                  <Point X="-26.14124609375" Y="-4.731328125" />
                  <Point X="-26.1207734375" Y="-4.575834960938" />
                  <Point X="-26.120076171875" Y="-4.568098144531" />
                  <Point X="-26.11944921875" Y="-4.541030273438" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497688964844" />
                  <Point X="-26.1567265625" Y="-4.329816894531" />
                  <Point X="-26.18386328125" Y="-4.193397460938" />
                  <Point X="-26.188125" Y="-4.178468261719" />
                  <Point X="-26.19902734375" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135462890625" />
                  <Point X="-26.221740234375" Y="-4.107625976562" />
                  <Point X="-26.23057421875" Y="-4.094861083984" />
                  <Point X="-26.25020703125" Y="-4.070937744141" />
                  <Point X="-26.261005859375" Y="-4.059779296875" />
                  <Point X="-26.38969140625" Y="-3.946924804688" />
                  <Point X="-26.494265625" Y="-3.855214599609" />
                  <Point X="-26.50673828125" Y="-3.845964599609" />
                  <Point X="-26.533021484375" Y="-3.82962109375" />
                  <Point X="-26.54683203125" Y="-3.822527587891" />
                  <Point X="-26.576529296875" Y="-3.810226318359" />
                  <Point X="-26.5913125" Y="-3.805476318359" />
                  <Point X="-26.621455078125" Y="-3.798447998047" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.807609375" Y="-3.784975097656" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166259766" />
                  <Point X="-27.008005859375" Y="-3.781946044922" />
                  <Point X="-27.0390546875" Y="-3.790265869141" />
                  <Point X="-27.053673828125" Y="-3.795496337891" />
                  <Point X="-27.081865234375" Y="-3.80826953125" />
                  <Point X="-27.0954375" Y="-3.815812255859" />
                  <Point X="-27.237751953125" Y="-3.910904296875" />
                  <Point X="-27.35340234375" Y="-3.988180175781" />
                  <Point X="-27.35968359375" Y="-3.992759765625" />
                  <Point X="-27.380439453125" Y="-4.010130859375" />
                  <Point X="-27.402755859375" Y="-4.032769287109" />
                  <Point X="-27.410470703125" Y="-4.041628662109" />
                  <Point X="-27.503203125" Y="-4.162477539063" />
                  <Point X="-27.561" Y="-4.126690917969" />
                  <Point X="-27.74759375" Y="-4.011157470703" />
                  <Point X="-27.963787109375" Y="-3.8446953125" />
                  <Point X="-27.98086328125" Y="-3.831547851562" />
                  <Point X="-27.7521796875" Y="-3.435456787109" />
                  <Point X="-27.34148828125" Y="-2.724119628906" />
                  <Point X="-27.33484765625" Y="-2.710080322266" />
                  <Point X="-27.323947265625" Y="-2.681115722656" />
                  <Point X="-27.319685546875" Y="-2.666190429688" />
                  <Point X="-27.3134140625" Y="-2.634664550781" />
                  <Point X="-27.311638671875" Y="-2.619240478516" />
                  <Point X="-27.310625" Y="-2.588306152344" />
                  <Point X="-27.3146640625" Y="-2.557618652344" />
                  <Point X="-27.3236484375" Y="-2.528000244141" />
                  <Point X="-27.32935546875" Y="-2.513559082031" />
                  <Point X="-27.343572265625" Y="-2.484730224609" />
                  <Point X="-27.351552734375" Y="-2.471414550781" />
                  <Point X="-27.369578125" Y="-2.446258544922" />
                  <Point X="-27.379623046875" Y="-2.434418212891" />
                  <Point X="-27.396970703125" Y="-2.417069580078" />
                  <Point X="-27.4088125" Y="-2.407023193359" />
                  <Point X="-27.433970703125" Y="-2.388994628906" />
                  <Point X="-27.447287109375" Y="-2.381012451172" />
                  <Point X="-27.476115234375" Y="-2.366795166016" />
                  <Point X="-27.4905546875" Y="-2.361088378906" />
                  <Point X="-27.520171875" Y="-2.352103271484" />
                  <Point X="-27.550859375" Y="-2.348062744141" />
                  <Point X="-27.581794921875" Y="-2.349074951172" />
                  <Point X="-27.597220703125" Y="-2.350849365234" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.264744140625" Y="-2.712669189453" />
                  <Point X="-28.793087890625" Y="-3.017708740234" />
                  <Point X="-28.856607421875" Y="-2.934258056641" />
                  <Point X="-29.00401953125" Y="-2.7405859375" />
                  <Point X="-29.159013671875" Y="-2.480685791016" />
                  <Point X="-29.181265625" Y="-2.443373291016" />
                  <Point X="-28.766697265625" Y="-2.125264404297" />
                  <Point X="-28.048123046875" Y="-1.573882080078" />
                  <Point X="-28.036484375" Y="-1.563311035156" />
                  <Point X="-28.015111328125" Y="-1.540396362305" />
                  <Point X="-28.005376953125" Y="-1.528052734375" />
                  <Point X="-27.987412109375" Y="-1.50092565918" />
                  <Point X="-27.97984375" Y="-1.487140625" />
                  <Point X="-27.967083984375" Y="-1.458507446289" />
                  <Point X="-27.961892578125" Y="-1.443659301758" />
                  <Point X="-27.954185546875" Y="-1.413904785156" />
                  <Point X="-27.95155078125" Y="-1.39880090332" />
                  <Point X="-27.94874609375" Y="-1.368363891602" />
                  <Point X="-27.948576171875" Y="-1.353030517578" />
                  <Point X="-27.950787109375" Y="-1.321361206055" />
                  <Point X="-27.953083984375" Y="-1.306207763672" />
                  <Point X="-27.9600859375" Y="-1.276465454102" />
                  <Point X="-27.97178125" Y="-1.248234985352" />
                  <Point X="-27.98786328125" Y="-1.222253662109" />
                  <Point X="-27.996955078125" Y="-1.20991394043" />
                  <Point X="-28.017787109375" Y="-1.185959106445" />
                  <Point X="-28.02874609375" Y="-1.175241821289" />
                  <Point X="-28.052244140625" Y="-1.155709838867" />
                  <Point X="-28.06478515625" Y="-1.14689465332" />
                  <Point X="-28.091267578125" Y="-1.131308227539" />
                  <Point X="-28.10543359375" Y="-1.12448046875" />
                  <Point X="-28.13469921875" Y="-1.113256103516" />
                  <Point X="-28.149796875" Y="-1.108859741211" />
                  <Point X="-28.18168359375" Y="-1.102378295898" />
                  <Point X="-28.197298828125" Y="-1.100532348633" />
                  <Point X="-28.22862109375" Y="-1.09944152832" />
                  <Point X="-28.244328125" Y="-1.100196655273" />
                  <Point X="-28.974076171875" Y="-1.196269775391" />
                  <Point X="-29.66091796875" Y="-1.286694335938" />
                  <Point X="-29.683107421875" Y="-1.199830078125" />
                  <Point X="-29.740763671875" Y="-0.974111633301" />
                  <Point X="-29.78176953125" Y="-0.687393310547" />
                  <Point X="-29.786451171875" Y="-0.654654602051" />
                  <Point X="-29.3268515625" Y="-0.531505310059" />
                  <Point X="-28.508287109375" Y="-0.312171325684" />
                  <Point X="-28.4965" Y="-0.308168334961" />
                  <Point X="-28.47355078125" Y="-0.298656158447" />
                  <Point X="-28.462388671875" Y="-0.293147155762" />
                  <Point X="-28.43151171875" Y="-0.275301849365" />
                  <Point X="-28.42473828125" Y="-0.270995361328" />
                  <Point X="-28.4051796875" Y="-0.256953613281" />
                  <Point X="-28.380916015625" Y="-0.236798568726" />
                  <Point X="-28.372349609375" Y="-0.228735031128" />
                  <Point X="-28.35628125" Y="-0.211614562988" />
                  <Point X="-28.342451171875" Y="-0.192640029907" />
                  <Point X="-28.331072265625" Y="-0.172101242065" />
                  <Point X="-28.326021484375" Y="-0.161480499268" />
                  <Point X="-28.3134921875" Y="-0.130296585083" />
                  <Point X="-28.310958984375" Y="-0.12318914032" />
                  <Point X="-28.3045" Y="-0.101492874146" />
                  <Point X="-28.298107421875" Y="-0.073312042236" />
                  <Point X="-28.29632421875" Y="-0.062686840057" />
                  <Point X="-28.293970703125" Y="-0.041298370361" />
                  <Point X="-28.2940546875" Y="-0.019775247574" />
                  <Point X="-28.296576171875" Y="0.001594049692" />
                  <Point X="-28.298443359375" Y="0.012203347206" />
                  <Point X="-28.30579296875" Y="0.043464984894" />
                  <Point X="-28.307822265625" Y="0.050773517609" />
                  <Point X="-28.3150625" Y="0.072326065063" />
                  <Point X="-28.326634765625" Y="0.100429321289" />
                  <Point X="-28.33177734375" Y="0.111006217957" />
                  <Point X="-28.34333203125" Y="0.131446914673" />
                  <Point X="-28.357328125" Y="0.150306869507" />
                  <Point X="-28.373544921875" Y="0.167289108276" />
                  <Point X="-28.382177734375" Y="0.175275222778" />
                  <Point X="-28.40930859375" Y="0.197421066284" />
                  <Point X="-28.415751953125" Y="0.202241500854" />
                  <Point X="-28.435859375" Y="0.215579376221" />
                  <Point X="-28.463869140625" Y="0.231434020996" />
                  <Point X="-28.474685546875" Y="0.236682342529" />
                  <Point X="-28.49689453125" Y="0.2457709198" />
                  <Point X="-28.508287109375" Y="0.249611190796" />
                  <Point X="-29.173486328125" Y="0.427851226807" />
                  <Point X="-29.7854453125" Y="0.591824951172" />
                  <Point X="-29.768486328125" Y="0.706436096191" />
                  <Point X="-29.73133203125" Y="0.957523254395" />
                  <Point X="-29.64878125" Y="1.262161621094" />
                  <Point X="-29.6335859375" Y="1.318237182617" />
                  <Point X="-29.364833984375" Y="1.282855102539" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.204703369141" />
                  <Point X="-28.71514453125" Y="1.20658972168" />
                  <Point X="-28.704890625" Y="1.208053344727" />
                  <Point X="-28.6846015625" Y="1.212088989258" />
                  <Point X="-28.67456640625" Y="1.214660766602" />
                  <Point X="-28.6406796875" Y="1.225345458984" />
                  <Point X="-28.613140625" Y="1.234028320312" />
                  <Point X="-28.6034453125" Y="1.237677612305" />
                  <Point X="-28.58451171875" Y="1.246009155273" />
                  <Point X="-28.5752734375" Y="1.25069140625" />
                  <Point X="-28.556529296875" Y="1.261513916016" />
                  <Point X="-28.547857421875" Y="1.267172241211" />
                  <Point X="-28.531177734375" Y="1.279401977539" />
                  <Point X="-28.515927734375" Y="1.293376586914" />
                  <Point X="-28.502291015625" Y="1.308926025391" />
                  <Point X="-28.495896484375" Y="1.317072387695" />
                  <Point X="-28.48348046875" Y="1.334803344727" />
                  <Point X="-28.4780078125" Y="1.343605102539" />
                  <Point X="-28.46805859375" Y="1.36174206543" />
                  <Point X="-28.46358203125" Y="1.371077148438" />
                  <Point X="-28.449984375" Y="1.403904663086" />
                  <Point X="-28.438935546875" Y="1.430581542969" />
                  <Point X="-28.435498046875" Y="1.440354492188" />
                  <Point X="-28.42970703125" Y="1.460224121094" />
                  <Point X="-28.427353515625" Y="1.47032043457" />
                  <Point X="-28.42359765625" Y="1.491635742188" />
                  <Point X="-28.422359375" Y="1.501921020508" />
                  <Point X="-28.421009765625" Y="1.522555541992" />
                  <Point X="-28.421912109375" Y="1.543202392578" />
                  <Point X="-28.425056640625" Y="1.563640258789" />
                  <Point X="-28.4271875" Y="1.573780395508" />
                  <Point X="-28.4327890625" Y="1.594686767578" />
                  <Point X="-28.43601171875" Y="1.604529052734" />
                  <Point X="-28.4435078125" Y="1.623806884766" />
                  <Point X="-28.44778125" Y="1.633242431641" />
                  <Point X="-28.4641875" Y="1.664759765625" />
                  <Point X="-28.477521484375" Y="1.690371948242" />
                  <Point X="-28.482798828125" Y="1.699286499023" />
                  <Point X="-28.49429296875" Y="1.716488769531" />
                  <Point X="-28.500509765625" Y="1.724776611328" />
                  <Point X="-28.514421875" Y="1.74135546875" />
                  <Point X="-28.521505859375" Y="1.748916625977" />
                  <Point X="-28.536447265625" Y="1.763218383789" />
                  <Point X="-28.5443046875" Y="1.769958862305" />
                  <Point X="-28.925865234375" Y="2.062740722656" />
                  <Point X="-29.22761328125" Y="2.294281005859" />
                  <Point X="-29.146662109375" Y="2.432970458984" />
                  <Point X="-29.00228515625" Y="2.680322265625" />
                  <Point X="-28.783626953125" Y="2.961376220703" />
                  <Point X="-28.726337890625" Y="3.035013183594" />
                  <Point X="-28.614533203125" Y="2.970462402344" />
                  <Point X="-28.25415625" Y="2.762398925781" />
                  <Point X="-28.24491796875" Y="2.757716796875" />
                  <Point X="-28.225984375" Y="2.749385742188" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.107876953125" Y="2.727028808594" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.038494140625" Y="2.723785644531" />
                  <Point X="-28.02816796875" Y="2.724575683594" />
                  <Point X="-28.006708984375" Y="2.727400634766" />
                  <Point X="-27.99652734375" Y="2.729310302734" />
                  <Point X="-27.976435546875" Y="2.7342265625" />
                  <Point X="-27.957001953125" Y="2.741299316406" />
                  <Point X="-27.93844921875" Y="2.750447509766" />
                  <Point X="-27.929419921875" Y="2.755529541016" />
                  <Point X="-27.911166015625" Y="2.767158203125" />
                  <Point X="-27.90274609375" Y="2.773191650391" />
                  <Point X="-27.88661328125" Y="2.786139404297" />
                  <Point X="-27.878900390625" Y="2.793053710938" />
                  <Point X="-27.845400390625" Y="2.826553710938" />
                  <Point X="-27.81817578125" Y="2.853777099609" />
                  <Point X="-27.811259765625" Y="2.861492431641" />
                  <Point X="-27.7983125" Y="2.877625732422" />
                  <Point X="-27.79228125" Y="2.886043701172" />
                  <Point X="-27.78065234375" Y="2.904298339844" />
                  <Point X="-27.7755703125" Y="2.913325683594" />
                  <Point X="-27.766423828125" Y="2.931874511719" />
                  <Point X="-27.759353515625" Y="2.951298828125" />
                  <Point X="-27.754435546875" Y="2.971387451172" />
                  <Point X="-27.7525234375" Y="2.981573242188" />
                  <Point X="-27.749697265625" Y="3.003032470703" />
                  <Point X="-27.748908203125" Y="3.013364990234" />
                  <Point X="-27.74845703125" Y="3.034048583984" />
                  <Point X="-27.748794921875" Y="3.044399658203" />
                  <Point X="-27.752923828125" Y="3.091595703125" />
                  <Point X="-27.756279296875" Y="3.12994921875" />
                  <Point X="-27.757744140625" Y="3.140206298828" />
                  <Point X="-27.76178125" Y="3.160499023438" />
                  <Point X="-27.764353515625" Y="3.170534667969" />
                  <Point X="-27.77086328125" Y="3.191177001953" />
                  <Point X="-27.774513671875" Y="3.200871582031" />
                  <Point X="-27.78284375" Y="3.219799804688" />
                  <Point X="-27.7875234375" Y="3.229033447266" />
                  <Point X="-27.95660546875" Y="3.521890380859" />
                  <Point X="-28.05938671875" Y="3.699915039062" />
                  <Point X="-27.899814453125" Y="3.822258789062" />
                  <Point X="-27.6483671875" Y="4.015041015625" />
                  <Point X="-27.30398828125" Y="4.206370117188" />
                  <Point X="-27.192525390625" Y="4.268296386719" />
                  <Point X="-27.11856640625" Y="4.171911621094" />
                  <Point X="-27.1118203125" Y="4.164048828125" />
                  <Point X="-27.097517578125" Y="4.149107421875" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.06509375" Y="4.121898925781" />
                  <Point X="-27.04789453125" Y="4.11040625" />
                  <Point X="-27.03898046875" Y="4.105128417969" />
                  <Point X="-26.986451171875" Y="4.077783447266" />
                  <Point X="-26.943763671875" Y="4.055561767578" />
                  <Point X="-26.934326171875" Y="4.051285644531" />
                  <Point X="-26.915046875" Y="4.0437890625" />
                  <Point X="-26.905205078125" Y="4.040568359375" />
                  <Point X="-26.884298828125" Y="4.034966308594" />
                  <Point X="-26.8741640625" Y="4.032834960938" />
                  <Point X="-26.853720703125" Y="4.029687988281" />
                  <Point X="-26.833052734375" Y="4.028785400391" />
                  <Point X="-26.812412109375" Y="4.030138427734" />
                  <Point X="-26.802130859375" Y="4.031378417969" />
                  <Point X="-26.78081640625" Y="4.03513671875" />
                  <Point X="-26.770728515625" Y="4.037488769531" />
                  <Point X="-26.750869140625" Y="4.04327734375" />
                  <Point X="-26.74109765625" Y="4.046713867188" />
                  <Point X="-26.68638671875" Y="4.069376953125" />
                  <Point X="-26.641923828125" Y="4.087793457031" />
                  <Point X="-26.6325859375" Y="4.092272216797" />
                  <Point X="-26.614455078125" Y="4.10221875" />
                  <Point X="-26.605662109375" Y="4.107687011719" />
                  <Point X="-26.587931640625" Y="4.120101074219" />
                  <Point X="-26.579783203125" Y="4.126494628906" />
                  <Point X="-26.56423046875" Y="4.1401328125" />
                  <Point X="-26.550255859375" Y="4.155382324219" />
                  <Point X="-26.5380234375" Y="4.172063476562" />
                  <Point X="-26.532361328125" Y="4.180739746094" />
                  <Point X="-26.5215390625" Y="4.199483398437" />
                  <Point X="-26.51685546875" Y="4.208725097656" />
                  <Point X="-26.5085234375" Y="4.227661621094" />
                  <Point X="-26.504875" Y="4.237356445312" />
                  <Point X="-26.487068359375" Y="4.2938359375" />
                  <Point X="-26.472595703125" Y="4.339732910156" />
                  <Point X="-26.470025390625" Y="4.349763671875" />
                  <Point X="-26.465990234375" Y="4.370048339844" />
                  <Point X="-26.464525390625" Y="4.380302246094" />
                  <Point X="-26.462638671875" Y="4.401865234375" />
                  <Point X="-26.46230078125" Y="4.412218261719" />
                  <Point X="-26.462751953125" Y="4.432898925781" />
                  <Point X="-26.463541015625" Y="4.4432265625" />
                  <Point X="-26.479263671875" Y="4.562655761719" />
                  <Point X="-26.2566796875" Y="4.625060546875" />
                  <Point X="-25.931173828125" Y="4.7163203125" />
                  <Point X="-25.51368359375" Y="4.765181640625" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.33227734375" Y="4.659600097656" />
                  <Point X="-25.22566796875" Y="4.261728515625" />
                  <Point X="-25.220435546875" Y="4.247106933594" />
                  <Point X="-25.20766015625" Y="4.218912597656" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218911621094" />
                  <Point X="-24.767251953125" Y="4.247104980469" />
                  <Point X="-24.76201953125" Y="4.2617265625" />
                  <Point X="-24.6753828125" Y="4.585053710938" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.456345703125" Y="4.767678222656" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.8267109375" Y="4.654519042969" />
                  <Point X="-23.54640234375" Y="4.586843261719" />
                  <Point X="-23.323208984375" Y="4.505890136719" />
                  <Point X="-23.14173828125" Y="4.440069335938" />
                  <Point X="-22.924337890625" Y="4.338397949219" />
                  <Point X="-22.74955078125" Y="4.256654785156" />
                  <Point X="-22.53947265625" Y="4.134264160156" />
                  <Point X="-22.3705703125" Y="4.035861572266" />
                  <Point X="-22.18221875" Y="3.901916259766" />
                  <Point X="-22.459345703125" Y="3.421917480469" />
                  <Point X="-22.934689453125" Y="2.598597900391" />
                  <Point X="-22.937619140625" Y="2.593115234375" />
                  <Point X="-22.946814453125" Y="2.573443603516" />
                  <Point X="-22.955814453125" Y="2.549569335938" />
                  <Point X="-22.958697265625" Y="2.540603271484" />
                  <Point X="-22.97054296875" Y="2.496310791016" />
                  <Point X="-22.98016796875" Y="2.460316894531" />
                  <Point X="-22.9820859375" Y="2.451469726562" />
                  <Point X="-22.986353515625" Y="2.420223144531" />
                  <Point X="-22.987244140625" Y="2.383239746094" />
                  <Point X="-22.986587890625" Y="2.369577636719" />
                  <Point X="-22.98196875" Y="2.33127734375" />
                  <Point X="-22.978216796875" Y="2.300152832031" />
                  <Point X="-22.976197265625" Y="2.289029785156" />
                  <Point X="-22.970853515625" Y="2.267107177734" />
                  <Point X="-22.967529296875" Y="2.256307617188" />
                  <Point X="-22.95926171875" Y="2.234216064453" />
                  <Point X="-22.9546796875" Y="2.223889160156" />
                  <Point X="-22.944318359375" Y="2.203845214844" />
                  <Point X="-22.9385390625" Y="2.194128173828" />
                  <Point X="-22.91483984375" Y="2.159201904297" />
                  <Point X="-22.895580078125" Y="2.130819580078" />
                  <Point X="-22.890189453125" Y="2.123631347656" />
                  <Point X="-22.86953515625" Y="2.100123046875" />
                  <Point X="-22.84240234375" Y="2.075387695312" />
                  <Point X="-22.8317421875" Y="2.066981933594" />
                  <Point X="-22.79681640625" Y="2.043283081055" />
                  <Point X="-22.76843359375" Y="2.024024414062" />
                  <Point X="-22.758720703125" Y="2.01824597168" />
                  <Point X="-22.73867578125" Y="2.00788293457" />
                  <Point X="-22.72834375" Y="2.003298461914" />
                  <Point X="-22.706255859375" Y="1.995032470703" />
                  <Point X="-22.695453125" Y="1.991707763672" />
                  <Point X="-22.673529296875" Y="1.986364746094" />
                  <Point X="-22.662408203125" Y="1.984346557617" />
                  <Point X="-22.624107421875" Y="1.979728149414" />
                  <Point X="-22.592984375" Y="1.975974975586" />
                  <Point X="-22.583955078125" Y="1.975320800781" />
                  <Point X="-22.55242578125" Y="1.975497436523" />
                  <Point X="-22.5156875" Y="1.979822875977" />
                  <Point X="-22.50225390625" Y="1.982395996094" />
                  <Point X="-22.4579609375" Y="1.994240234375" />
                  <Point X="-22.421966796875" Y="2.003865600586" />
                  <Point X="-22.4160078125" Y="2.005670288086" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572753906" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.69490234375" Y="2.414156494141" />
                  <Point X="-21.059595703125" Y="2.780950683594" />
                  <Point X="-21.056240234375" Y="2.776287109375" />
                  <Point X="-20.95603515625" Y="2.637026123047" />
                  <Point X="-20.86311328125" Y="2.483471679688" />
                  <Point X="-21.2012109375" Y="2.224039306641" />
                  <Point X="-21.827046875" Y="1.743819824219" />
                  <Point X="-21.831859375" Y="1.739869506836" />
                  <Point X="-21.847875" Y="1.725222045898" />
                  <Point X="-21.865330078125" Y="1.70660559082" />
                  <Point X="-21.87142578125" Y="1.699421142578" />
                  <Point X="-21.903302734375" Y="1.657834716797" />
                  <Point X="-21.92920703125" Y="1.624039794922" />
                  <Point X="-21.93436328125" Y="1.616602416992" />
                  <Point X="-21.95026171875" Y="1.589370605469" />
                  <Point X="-21.965236328125" Y="1.555548095703" />
                  <Point X="-21.969859375" Y="1.54267578125" />
                  <Point X="-21.981734375" Y="1.500215820312" />
                  <Point X="-21.991384765625" Y="1.465711303711" />
                  <Point X="-21.993775390625" Y="1.454661987305" />
                  <Point X="-21.997228515625" Y="1.432362915039" />
                  <Point X="-21.998291015625" Y="1.42111328125" />
                  <Point X="-21.999107421875" Y="1.397541381836" />
                  <Point X="-21.998826171875" Y="1.386236450195" />
                  <Point X="-21.996921875" Y="1.363750244141" />
                  <Point X="-21.995298828125" Y="1.352568847656" />
                  <Point X="-21.98555078125" Y="1.305326416016" />
                  <Point X="-21.97762890625" Y="1.266935791016" />
                  <Point X="-21.97540234375" Y="1.258239135742" />
                  <Point X="-21.96531640625" Y="1.228611694336" />
                  <Point X="-21.94971484375" Y="1.195374511719" />
                  <Point X="-21.943083984375" Y="1.183528076172" />
                  <Point X="-21.916572265625" Y="1.14323034668" />
                  <Point X="-21.89502734375" Y="1.110482788086" />
                  <Point X="-21.888259765625" Y="1.101422851562" />
                  <Point X="-21.873703125" Y="1.08417578125" />
                  <Point X="-21.8659140625" Y="1.075988891602" />
                  <Point X="-21.848669921875" Y="1.059899414062" />
                  <Point X="-21.83996484375" Y="1.052695800781" />
                  <Point X="-21.821755859375" Y="1.039370239258" />
                  <Point X="-21.812251953125" Y="1.033248291016" />
                  <Point X="-21.773830078125" Y="1.011621154785" />
                  <Point X="-21.742609375" Y="0.994045898438" />
                  <Point X="-21.734517578125" Y="0.98998626709" />
                  <Point X="-21.705322265625" Y="0.978084533691" />
                  <Point X="-21.669724609375" Y="0.96802130127" />
                  <Point X="-21.656328125" Y="0.965257629395" />
                  <Point X="-21.604380859375" Y="0.958392028809" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.555966796875" Y="0.952199829102" />
                  <Point X="-21.53428125" Y="0.951222900391" />
                  <Point X="-21.50878125" Y="0.952032287598" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-20.863830078125" Y="1.036471069336" />
                  <Point X="-20.295296875" Y="1.111319946289" />
                  <Point X="-20.29034765625" Y="1.090992431641" />
                  <Point X="-20.2473125" Y="0.914206726074" />
                  <Point X="-20.216126953125" Y="0.713921203613" />
                  <Point X="-20.590787109375" Y="0.613531188965" />
                  <Point X="-21.3080078125" Y="0.42135269165" />
                  <Point X="-21.31396875" Y="0.419543792725" />
                  <Point X="-21.334375" Y="0.412137023926" />
                  <Point X="-21.357619140625" Y="0.401618682861" />
                  <Point X="-21.365994140625" Y="0.397316040039" />
                  <Point X="-21.417029296875" Y="0.367816345215" />
                  <Point X="-21.45850390625" Y="0.343843597412" />
                  <Point X="-21.466115234375" Y="0.338946777344" />
                  <Point X="-21.491220703125" Y="0.319873413086" />
                  <Point X="-21.51800390625" Y="0.294351898193" />
                  <Point X="-21.527203125" Y="0.284225769043" />
                  <Point X="-21.55782421875" Y="0.245206451416" />
                  <Point X="-21.582708984375" Y="0.213497909546" />
                  <Point X="-21.58914453125" Y="0.204207794189" />
                  <Point X="-21.600869140625" Y="0.18492678833" />
                  <Point X="-21.606158203125" Y="0.174936065674" />
                  <Point X="-21.615931640625" Y="0.153472091675" />
                  <Point X="-21.619994140625" Y="0.142926544189" />
                  <Point X="-21.62683984375" Y="0.121426612854" />
                  <Point X="-21.629623046875" Y="0.110472366333" />
                  <Point X="-21.639830078125" Y="0.057174259186" />
                  <Point X="-21.648125" Y="0.013861829758" />
                  <Point X="-21.649396484375" Y="0.004966303349" />
                  <Point X="-21.6514140625" Y="-0.02626159668" />
                  <Point X="-21.64971875" Y="-0.062939647675" />
                  <Point X="-21.648125" Y="-0.076421829224" />
                  <Point X="-21.63791796875" Y="-0.129720077515" />
                  <Point X="-21.629623046875" Y="-0.173032501221" />
                  <Point X="-21.62683984375" Y="-0.183986755371" />
                  <Point X="-21.619994140625" Y="-0.205486694336" />
                  <Point X="-21.615931640625" Y="-0.216032241821" />
                  <Point X="-21.606158203125" Y="-0.237496063232" />
                  <Point X="-21.600869140625" Y="-0.247487228394" />
                  <Point X="-21.58914453125" Y="-0.266768066406" />
                  <Point X="-21.582708984375" Y="-0.276057891846" />
                  <Point X="-21.552087890625" Y="-0.315077209473" />
                  <Point X="-21.527203125" Y="-0.346785919189" />
                  <Point X="-21.521279296875" Y="-0.353635192871" />
                  <Point X="-21.498857421875" Y="-0.375807647705" />
                  <Point X="-21.469822265625" Y="-0.398725463867" />
                  <Point X="-21.45850390625" Y="-0.406404052734" />
                  <Point X="-21.40746875" Y="-0.435903625488" />
                  <Point X="-21.365994140625" Y="-0.459876342773" />
                  <Point X="-21.36050390625" Y="-0.462814483643" />
                  <Point X="-21.340841796875" Y="-0.472016021729" />
                  <Point X="-21.31697265625" Y="-0.481027160645" />
                  <Point X="-21.3080078125" Y="-0.483912963867" />
                  <Point X="-20.7251640625" Y="-0.640085021973" />
                  <Point X="-20.21512109375" Y="-0.776750732422" />
                  <Point X="-20.238380859375" Y="-0.931038513184" />
                  <Point X="-20.272197265625" Y="-1.079219482422" />
                  <Point X="-20.731443359375" Y="-1.018758666992" />
                  <Point X="-21.563216796875" Y="-0.909253601074" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042480469" />
                  <Point X="-21.63327734375" Y="-0.910840881348" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.74568359375" Y="-0.934447998047" />
                  <Point X="-21.82708203125" Y="-0.952140075684" />
                  <Point X="-21.842123046875" Y="-0.95674230957" />
                  <Point X="-21.871244140625" Y="-0.968366699219" />
                  <Point X="-21.885322265625" Y="-0.975388977051" />
                  <Point X="-21.9131484375" Y="-0.992280883789" />
                  <Point X="-21.925875" Y="-1.001530883789" />
                  <Point X="-21.949625" Y="-1.022002197266" />
                  <Point X="-21.960650390625" Y="-1.033223510742" />
                  <Point X="-22.021193359375" Y="-1.106038452148" />
                  <Point X="-22.07039453125" Y="-1.165211181641" />
                  <Point X="-22.07867578125" Y="-1.176851318359" />
                  <Point X="-22.09339453125" Y="-1.201233276367" />
                  <Point X="-22.09983203125" Y="-1.213974975586" />
                  <Point X="-22.11117578125" Y="-1.241361328125" />
                  <Point X="-22.115634765625" Y="-1.254927612305" />
                  <Point X="-22.122466796875" Y="-1.282577026367" />
                  <Point X="-22.12483984375" Y="-1.29666027832" />
                  <Point X="-22.133517578125" Y="-1.390959594727" />
                  <Point X="-22.140568359375" Y="-1.467590576172" />
                  <Point X="-22.140708984375" Y="-1.483319702148" />
                  <Point X="-22.138390625" Y="-1.514591064453" />
                  <Point X="-22.135931640625" Y="-1.530133300781" />
                  <Point X="-22.12819921875" Y="-1.561754394531" />
                  <Point X="-22.123208984375" Y="-1.576674194336" />
                  <Point X="-22.110837890625" Y="-1.605482177734" />
                  <Point X="-22.10345703125" Y="-1.619370117188" />
                  <Point X="-22.048025390625" Y="-1.705592773438" />
                  <Point X="-22.0029765625" Y="-1.775660766602" />
                  <Point X="-21.998255859375" Y="-1.782352172852" />
                  <Point X="-21.980205078125" Y="-1.804448974609" />
                  <Point X="-21.956509765625" Y="-1.828119628906" />
                  <Point X="-21.947201171875" Y="-1.836277587891" />
                  <Point X="-21.4063203125" Y="-2.251310791016" />
                  <Point X="-20.912828125" Y="-2.629980712891" />
                  <Point X="-20.9545234375" Y="-2.697447998047" />
                  <Point X="-20.998724609375" Y="-2.760251953125" />
                  <Point X="-21.41073828125" Y="-2.522375732422" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.184970703125" Y="-2.079513916016" />
                  <Point X="-22.216876953125" Y="-2.069326416016" />
                  <Point X="-22.228888671875" Y="-2.066337646484" />
                  <Point X="-22.3481015625" Y="-2.044807739258" />
                  <Point X="-22.444978515625" Y="-2.027311889648" />
                  <Point X="-22.460638671875" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.55515625" Y="-2.035136474609" />
                  <Point X="-22.584931640625" Y="-2.044960083008" />
                  <Point X="-22.59941015625" Y="-2.051108642578" />
                  <Point X="-22.698447265625" Y="-2.103230957031" />
                  <Point X="-22.778927734375" Y="-2.145587646484" />
                  <Point X="-22.79103125" Y="-2.153170410156" />
                  <Point X="-22.813962890625" Y="-2.170064208984" />
                  <Point X="-22.824791015625" Y="-2.179375244141" />
                  <Point X="-22.84575" Y="-2.200334716797" />
                  <Point X="-22.85505859375" Y="-2.211161132813" />
                  <Point X="-22.871951171875" Y="-2.234090576172" />
                  <Point X="-22.87953515625" Y="-2.246193603516" />
                  <Point X="-22.931658203125" Y="-2.345230224609" />
                  <Point X="-22.974015625" Y="-2.425711669922" />
                  <Point X="-22.9801640625" Y="-2.440193115234" />
                  <Point X="-22.98998828125" Y="-2.469971679688" />
                  <Point X="-22.9936640625" Y="-2.485268798828" />
                  <Point X="-22.99862109375" Y="-2.517441894531" />
                  <Point X="-22.999720703125" Y="-2.533133300781" />
                  <Point X="-22.99931640625" Y="-2.564484619141" />
                  <Point X="-22.9978125" Y="-2.58014453125" />
                  <Point X="-22.97628125" Y="-2.699357421875" />
                  <Point X="-22.95878515625" Y="-2.796234863281" />
                  <Point X="-22.95698046875" Y="-2.804231689453" />
                  <Point X="-22.94876171875" Y="-2.831539794922" />
                  <Point X="-22.935927734375" Y="-2.862478759766" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.58287890625" Y="-3.475588623047" />
                  <Point X="-22.264103515625" Y="-4.027722900391" />
                  <Point X="-22.276244140625" Y="-4.036083496094" />
                  <Point X="-22.600779296875" Y="-3.613140625" />
                  <Point X="-23.16608203125" Y="-2.876422851562" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849625732422" />
                  <Point X="-23.21674609375" Y="-2.828004638672" />
                  <Point X="-23.22669921875" Y="-2.820646972656" />
                  <Point X="-23.344275390625" Y="-2.745056396484" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.453716796875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663873535156" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.7201953125" Y="-2.658349365234" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920125" Y="-2.699414794922" />
                  <Point X="-23.944505859375" Y="-2.714135742188" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-24.055431640625" Y="-2.80497265625" />
                  <Point X="-24.136123046875" Y="-2.872063720703" />
                  <Point X="-24.147345703125" Y="-2.883087158203" />
                  <Point X="-24.16781640625" Y="-2.906836425781" />
                  <Point X="-24.177064453125" Y="-2.919562255859" />
                  <Point X="-24.19395703125" Y="-2.947388671875" />
                  <Point X="-24.20098046875" Y="-2.961466796875" />
                  <Point X="-24.21260546875" Y="-2.990588378906" />
                  <Point X="-24.21720703125" Y="-3.005631835938" />
                  <Point X="-24.24689453125" Y="-3.142221435547" />
                  <Point X="-24.271021484375" Y="-3.253219726562" />
                  <Point X="-24.2724140625" Y="-3.261286865234" />
                  <Point X="-24.275275390625" Y="-3.289677001953" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520263672" />
                  <Point X="-24.1759453125" Y="-4.083693359375" />
                  <Point X="-24.16691015625" Y="-4.152326660156" />
                  <Point X="-24.344931640625" Y="-3.487936767578" />
                  <Point X="-24.347392578125" Y="-3.480121582031" />
                  <Point X="-24.357853515625" Y="-3.453578613281" />
                  <Point X="-24.3732109375" Y="-3.423815429688" />
                  <Point X="-24.37958984375" Y="-3.413209716797" />
                  <Point X="-24.469916015625" Y="-3.283067871094" />
                  <Point X="-24.543318359375" Y="-3.177309326172" />
                  <Point X="-24.553328125" Y="-3.165172119141" />
                  <Point X="-24.575212890625" Y="-3.142716308594" />
                  <Point X="-24.587087890625" Y="-3.132397705078" />
                  <Point X="-24.61334375" Y="-3.113153808594" />
                  <Point X="-24.6267578125" Y="-3.104937255859" />
                  <Point X="-24.6547578125" Y="-3.090829589844" />
                  <Point X="-24.66934375" Y="-3.084938476562" />
                  <Point X="-24.8091171875" Y="-3.041557861328" />
                  <Point X="-24.922703125" Y="-3.006305175781" />
                  <Point X="-24.936623046875" Y="-3.003109130859" />
                  <Point X="-24.964783203125" Y="-2.99883984375" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.2047578125" Y="-3.049685546875" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.3329296875" Y="-3.090829589844" />
                  <Point X="-25.3609296875" Y="-3.104937255859" />
                  <Point X="-25.37434375" Y="-3.113153808594" />
                  <Point X="-25.400599609375" Y="-3.132397705078" />
                  <Point X="-25.412474609375" Y="-3.142717285156" />
                  <Point X="-25.434359375" Y="-3.165173828125" />
                  <Point X="-25.444369140625" Y="-3.177310791016" />
                  <Point X="-25.534693359375" Y="-3.307452392578" />
                  <Point X="-25.608095703125" Y="-3.413211181641" />
                  <Point X="-25.61246875" Y="-3.420132080078" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213378906" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.822341796875" Y="-4.158168457031" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.128848908171" Y="-4.637169478091" />
                  <Point X="-25.961848260852" Y="-4.678807415961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.119462449425" Y="-4.541601490316" />
                  <Point X="-25.937256429317" Y="-4.587030553416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.67567946145" Y="-4.055684715929" />
                  <Point X="-27.462115525123" Y="-4.108932185653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.134837168398" Y="-4.439859847552" />
                  <Point X="-25.912664597782" Y="-4.49525369087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.884562050027" Y="-3.905696142502" />
                  <Point X="-27.397260052134" Y="-4.027194176418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.155329082285" Y="-4.3368423448" />
                  <Point X="-25.888072766247" Y="-4.403476828325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.956753477485" Y="-3.789788503283" />
                  <Point X="-27.301131336226" Y="-3.953253462383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.175821439694" Y="-4.233824731465" />
                  <Point X="-25.863480934712" Y="-4.31169996578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.907339195524" Y="-3.704200572728" />
                  <Point X="-27.194420710701" Y="-3.881951114739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.210381150597" Y="-4.127299732979" />
                  <Point X="-25.838889103177" Y="-4.219923103234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.857924913564" Y="-3.618612642173" />
                  <Point X="-27.086630664038" Y="-3.810917897012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.329531253905" Y="-3.999683980894" />
                  <Point X="-25.814297371217" Y="-4.128146215862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.808510631603" Y="-3.533024711618" />
                  <Point X="-26.794563220413" Y="-3.785830194638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.485521710084" Y="-3.862882897204" />
                  <Point X="-25.789705844081" Y="-4.036369277421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.759096349642" Y="-3.447436781063" />
                  <Point X="-25.765114316944" Y="-3.944592338981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.709682054309" Y="-3.361848853842" />
                  <Point X="-25.740522789808" Y="-3.85281540054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.81553286406" Y="-2.988220985216" />
                  <Point X="-28.764187183395" Y="-3.001022901231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.660267756799" Y="-3.276260927164" />
                  <Point X="-25.715931262672" Y="-3.7610384621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.168367010933" Y="-4.146889566257" />
                  <Point X="-24.167600766495" Y="-4.147080612453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.9075112335" Y="-2.867379907271" />
                  <Point X="-28.645751431894" Y="-2.93264395583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.610853459289" Y="-3.190673000486" />
                  <Point X="-25.691339735536" Y="-3.669261523659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.196479371038" Y="-4.041972072869" />
                  <Point X="-24.180927600408" Y="-4.045849564781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.999488280432" Y="-2.746539159064" />
                  <Point X="-28.527315680392" Y="-2.864265010429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.561439161778" Y="-3.105085073808" />
                  <Point X="-25.666748208399" Y="-3.577484585219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.224591731142" Y="-3.937054579481" />
                  <Point X="-24.19425509712" Y="-3.944618351855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.069227345382" Y="-2.631242962491" />
                  <Point X="-28.408879928891" Y="-2.795886065028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.512024864268" Y="-3.01949714713" />
                  <Point X="-25.642010242476" Y="-3.485744158069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.252704091247" Y="-3.832137086093" />
                  <Point X="-24.207582593832" Y="-3.843387138928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.13781408651" Y="-2.516234072516" />
                  <Point X="-28.29044417739" Y="-2.727507119626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.462610566758" Y="-2.933909220452" />
                  <Point X="-25.598084994133" Y="-3.398787657724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.280816451351" Y="-3.727219592704" />
                  <Point X="-24.220910090545" Y="-3.742155926002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.145972186885" Y="-2.416291734854" />
                  <Point X="-28.172008365088" Y="-2.659128189384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.413196269248" Y="-2.848321293773" />
                  <Point X="-25.540155802765" Y="-3.315322732526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.308928811455" Y="-3.622302099316" />
                  <Point X="-24.234237587257" Y="-3.640924713075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.049667702145" Y="-2.342394844911" />
                  <Point X="-28.053572535937" Y="-2.590749263344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.363781971738" Y="-2.762733367095" />
                  <Point X="-25.482227166586" Y="-3.231857668904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.33704117156" Y="-3.517384605928" />
                  <Point X="-24.247565083969" Y="-3.539693500149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.280878819772" Y="-4.03004345862" />
                  <Point X="-22.271036660528" Y="-4.032497384528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.953363217405" Y="-2.268497954968" />
                  <Point X="-27.935136706785" Y="-2.522370337303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.322252281134" Y="-2.675179587124" />
                  <Point X="-25.41923561353" Y="-3.149654932235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.383234063063" Y="-3.407959129755" />
                  <Point X="-24.260892580682" Y="-3.438462287222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.373779871654" Y="-3.908972330104" />
                  <Point X="-22.325750477788" Y="-3.920947402955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.857058732665" Y="-2.194601065025" />
                  <Point X="-27.816700877633" Y="-2.453991411262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.311732167216" Y="-2.579894251329" />
                  <Point X="-25.303984639374" Y="-3.080481932659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.465408372224" Y="-3.289562478579" />
                  <Point X="-24.274220077394" Y="-3.337231074296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.466680923535" Y="-3.787901201589" />
                  <Point X="-22.391783373457" Y="-3.806575258167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.760754264032" Y="-2.120704171066" />
                  <Point X="-27.698265048482" Y="-2.385612485221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.35109964735" Y="-2.472170541342" />
                  <Point X="-25.129051835687" Y="-3.026189284446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.548592693482" Y="-3.170914003104" />
                  <Point X="-24.268320523203" Y="-3.240793703572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.559581975416" Y="-3.666830073073" />
                  <Point X="-22.457816269126" Y="-3.69220311338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.664450040289" Y="-2.046807216049" />
                  <Point X="-24.248132916999" Y="-3.147918744321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.652483012268" Y="-3.545758948304" />
                  <Point X="-22.523849164796" Y="-3.577830968592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.568145816546" Y="-1.972910261032" />
                  <Point X="-24.227946560426" Y="-3.055043473501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.745384037146" Y="-3.424687826521" />
                  <Point X="-22.589882039461" Y="-3.463458829042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.471841592803" Y="-1.899013306015" />
                  <Point X="-24.201848815632" Y="-2.963642077301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.838285062023" Y="-3.303616704738" />
                  <Point X="-22.655914737088" Y="-3.349086733632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.37553736906" Y="-1.825116350997" />
                  <Point X="-24.144289276319" Y="-2.880084987495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.931186086901" Y="-3.182545582955" />
                  <Point X="-22.721947434714" Y="-3.234714638222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.279233145318" Y="-1.75121939598" />
                  <Point X="-24.054838314681" Y="-2.804479322324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.024087111778" Y="-3.061474461172" />
                  <Point X="-22.78798013234" Y="-3.120342542813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.182928921575" Y="-1.677322440963" />
                  <Point X="-23.964249591586" Y="-2.729157332945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.116988136656" Y="-2.940403339389" />
                  <Point X="-22.854012829966" Y="-3.005970447403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.681593902143" Y="-1.205754999639" />
                  <Point X="-29.461999096147" Y="-1.260506134053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.086624697832" Y="-1.603425485946" />
                  <Point X="-23.819065131868" Y="-2.667447589542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.240688708148" Y="-2.81165302816" />
                  <Point X="-22.920045527593" Y="-2.891598351993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.708304236949" Y="-1.101187070418" />
                  <Point X="-29.205008912063" Y="-1.226672688612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.004082030475" Y="-1.526097389559" />
                  <Point X="-22.961093066647" Y="-2.783455756271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.735014474454" Y="-0.996619165457" />
                  <Point X="-28.948018712696" Y="-1.192839246982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.960687915274" Y="-1.439008462849" />
                  <Point X="-22.979609306598" Y="-2.680930844355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.752158666436" Y="-0.894436343522" />
                  <Point X="-28.691028377887" Y="-1.159005839122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.949209266397" Y="-1.343962116661" />
                  <Point X="-22.997975883488" Y="-2.578443247632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.766679081925" Y="-0.792907702539" />
                  <Point X="-28.434038043078" Y="-1.125172431262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.977497006239" Y="-1.239000896193" />
                  <Point X="-22.992834579226" Y="-2.481816823968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.781199497414" Y="-0.691379061557" />
                  <Point X="-22.956749720005" Y="-2.392905495062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.66563927629" Y="-0.622283165909" />
                  <Point X="-22.911197903532" Y="-2.306354543701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.476362971105" Y="-0.571566754279" />
                  <Point X="-22.862080593397" Y="-2.220692569753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.227283935735" Y="-2.628293155463" />
                  <Point X="-20.953908682431" Y="-2.696453261396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.287086702841" Y="-0.520850333442" />
                  <Point X="-22.77625758676" Y="-2.144182353808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.525764471904" Y="-2.455965304704" />
                  <Point X="-20.953741735967" Y="-2.598586591036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.097810573397" Y="-0.470133877994" />
                  <Point X="-22.650025787982" Y="-2.077747181304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.824244905875" Y="-2.283637479426" />
                  <Point X="-21.142754222853" Y="-2.45355219038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.908534443952" Y="-0.419417422546" />
                  <Point X="-22.465929395568" Y="-2.025739272367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.122725339846" Y="-2.111309654148" />
                  <Point X="-21.331766709739" Y="-2.308517789724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.719258314507" Y="-0.368700967098" />
                  <Point X="-21.520778923002" Y="-2.16348345729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.529982185063" Y="-0.31798451165" />
                  <Point X="-21.70979095804" Y="-2.018449169293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.399845740517" Y="-0.252522876678" />
                  <Point X="-21.898802993078" Y="-1.873414881295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.330927924766" Y="-0.171797723251" />
                  <Point X="-22.022981578959" Y="-1.744545387693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.299988278443" Y="-0.081603548689" />
                  <Point X="-22.09794296622" Y="-1.627947119929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.299371440871" Y="0.016150951219" />
                  <Point X="-22.137507278972" Y="-1.520174334058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.339102897521" Y="0.123965410744" />
                  <Point X="-22.13642294487" Y="-1.422536394126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.283987733875" Y="0.457459954697" />
                  <Point X="-22.12761561032" Y="-1.326824014472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.772805832893" Y="0.677244289867" />
                  <Point X="-22.108042109015" Y="-1.233795941672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.758833586674" Y="0.77166891241" />
                  <Point X="-22.056663829425" Y="-1.148697690724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.744861229599" Y="0.866093507314" />
                  <Point X="-21.989234388441" Y="-1.067601443789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.730543802497" Y="0.960432066597" />
                  <Point X="-21.908924400724" Y="-0.989716677847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.70569172818" Y="1.052144043329" />
                  <Point X="-21.741528520438" Y="-0.933544863574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.680839653862" Y="1.143856020061" />
                  <Point X="-21.315425798176" Y="-0.941875909534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.655987579544" Y="1.235567996793" />
                  <Point X="-20.483406744521" Y="-1.051413263721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.551548323445" Y="1.30743666044" />
                  <Point X="-20.256423117744" Y="-1.010098343275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.745644711691" Y="1.204410617225" />
                  <Point X="-20.236295880493" Y="-0.917208332353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.564206451749" Y="1.257081273023" />
                  <Point X="-20.222070348842" Y="-0.822846860961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.483448721557" Y="1.334854404228" />
                  <Point X="-21.427298819679" Y="-0.424441358569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.442283128292" Y="1.422498963862" />
                  <Point X="-21.571267215478" Y="-0.290637711184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.421489367176" Y="1.51522279172" />
                  <Point X="-21.628226831081" Y="-0.178527789195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.441274774376" Y="1.61806414257" />
                  <Point X="-21.648217996114" Y="-0.075635137154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.506839389829" Y="1.732319531987" />
                  <Point X="-21.64659176776" Y="0.021867693367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.678687301475" Y="1.873074323379" />
                  <Point X="-21.628410946753" Y="0.115243000363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.867699675483" Y="2.018108695891" />
                  <Point X="-21.589591625891" Y="0.203472551409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.05671142957" Y="2.163142913839" />
                  <Point X="-21.526018108027" Y="0.285530187955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.222833431223" Y="2.302470075528" />
                  <Point X="-21.430991936176" Y="0.359745797097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.172945792509" Y="2.387939984989" />
                  <Point X="-21.29710840005" Y="0.42427317721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.123058007607" Y="2.473409858002" />
                  <Point X="-21.107832028821" Y="0.474989572374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.073170059921" Y="2.558879690427" />
                  <Point X="-20.918555657593" Y="0.525705967539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.023282112235" Y="2.644349522853" />
                  <Point X="-20.729279286364" Y="0.576422362703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.965339199658" Y="2.72781102697" />
                  <Point X="-20.540002977634" Y="0.62713877345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.901542222083" Y="2.809812948752" />
                  <Point X="-21.83264473905" Y="1.047338857004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.469332629172" Y="0.956754974239" />
                  <Point X="-20.35072683934" Y="0.677855226692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.837745244507" Y="2.891814870533" />
                  <Point X="-21.934627549978" Y="1.170674322365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.212342516304" Y="0.990588437436" />
                  <Point X="-20.220708606391" Y="0.743346335125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.773948271156" Y="2.973816793369" />
                  <Point X="-28.503588515698" Y="2.906408535491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.920707295497" Y="2.761079924964" />
                  <Point X="-21.980319516867" Y="1.279974904004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.955352403437" Y="1.024421900632" />
                  <Point X="-20.23656919676" Y="0.845209119234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.834467410009" Y="2.837486201338" />
                  <Point X="-21.998502693672" Y="1.38241677395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.698362213675" Y="1.058255344657" />
                  <Point X="-20.255499205437" Y="0.94783719528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.772327827147" Y="2.919901358033" />
                  <Point X="-21.988027954671" Y="1.477713422983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.441371981381" Y="1.092088778077" />
                  <Point X="-20.280873193282" Y="1.052071935782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.749012707385" Y="3.011996540575" />
                  <Point X="-21.959498243571" Y="1.568508461881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.754648592356" Y="3.111310019308" />
                  <Point X="-21.906796493696" Y="1.653276734626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.781088069088" Y="3.215810416026" />
                  <Point X="-21.838190152674" Y="1.734079547425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.845720679808" Y="3.329833430563" />
                  <Point X="-22.852938721087" Y="2.084993076164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.472912864577" Y="1.990241988331" />
                  <Point X="-21.743055879941" Y="1.808268203991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.911753907416" Y="3.444205658112" />
                  <Point X="-22.945410036341" Y="2.205957059305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.323908267929" Y="2.050999264623" />
                  <Point X="-21.646751332995" Y="1.882165078425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.9777866672" Y="3.55857776902" />
                  <Point X="-22.979694778385" Y="2.312413500355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.205472588823" Y="2.119378228074" />
                  <Point X="-21.550446786049" Y="1.956061952858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.043818436379" Y="3.672949632942" />
                  <Point X="-22.986550792203" Y="2.412031191376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.087036909717" Y="2.187757191526" />
                  <Point X="-21.454142239103" Y="2.029958827291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.985742450623" Y="3.756377958188" />
                  <Point X="-22.968126614923" Y="2.505345822839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.968601230611" Y="2.256136154977" />
                  <Point X="-21.357837692157" Y="2.103855701725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.889378414791" Y="3.830260000376" />
                  <Point X="-22.936424977249" Y="2.595350011619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.850165551505" Y="2.324515118429" />
                  <Point X="-21.26153314521" Y="2.177752576158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.793013736631" Y="3.904141882414" />
                  <Point X="-22.887132829751" Y="2.680968393715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.731729872399" Y="2.39289408188" />
                  <Point X="-21.16522876903" Y="2.251649493168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.696649058471" Y="3.978023764452" />
                  <Point X="-22.837718609381" Y="2.766556339627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.613294074375" Y="2.461273015682" />
                  <Point X="-21.068924679127" Y="2.325546481555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.587673064579" Y="4.048761292326" />
                  <Point X="-22.788304389012" Y="2.852144285538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.494858222686" Y="2.529651936103" />
                  <Point X="-20.972620589224" Y="2.399443469943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.466033543857" Y="4.116341448345" />
                  <Point X="-22.738890168643" Y="2.93773223145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.376422370996" Y="2.598030856525" />
                  <Point X="-20.876316499321" Y="2.47334045833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.344394023134" Y="4.183921604365" />
                  <Point X="-27.04669753572" Y="4.109697533705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.764437981719" Y="4.039322322822" />
                  <Point X="-22.689475948273" Y="3.023320177361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.257986519307" Y="2.666409776946" />
                  <Point X="-20.923322909536" Y="2.582968767498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.222754518841" Y="4.251501764481" />
                  <Point X="-27.169438006129" Y="4.238208464848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.617441662705" Y="4.100580318965" />
                  <Point X="-22.640061727904" Y="3.108908123273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.139550667618" Y="2.734788697368" />
                  <Point X="-21.00163807602" Y="2.700403226338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.534305598714" Y="4.177760464954" />
                  <Point X="-22.590647507535" Y="3.194496069184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.495821189682" Y="4.266073518898" />
                  <Point X="-22.541233287165" Y="3.280084015096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.468549639003" Y="4.357182252421" />
                  <Point X="-22.491819066796" Y="3.365671961007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.464985915251" Y="4.454202011084" />
                  <Point X="-25.111303208046" Y="4.116691005213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.984651874851" Y="4.08511328125" />
                  <Point X="-22.442404870641" Y="3.451259912956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.478312817228" Y="4.555433075726" />
                  <Point X="-25.217729226813" Y="4.241134286711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.83460334862" Y="4.145610276664" />
                  <Point X="-22.392990720902" Y="3.536847876478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.307612049792" Y="4.610780889085" />
                  <Point X="-25.248435620871" Y="4.346698545404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.775548992404" Y="4.228794666757" />
                  <Point X="-22.343576571162" Y="3.622435839999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.122771831257" Y="4.662603341341" />
                  <Point X="-25.276548194305" Y="4.451616091981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.74654647316" Y="4.319471821345" />
                  <Point X="-22.294162421423" Y="3.708023803521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.937931399084" Y="4.714425740331" />
                  <Point X="-25.304660767739" Y="4.556533638559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.721954527216" Y="4.411248655365" />
                  <Point X="-22.244748271684" Y="3.793611767042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.673700641434" Y="4.746453908025" />
                  <Point X="-25.332773328336" Y="4.661451181935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.697362581272" Y="4.503025489385" />
                  <Point X="-22.195334121944" Y="3.879199730564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.406456821838" Y="4.777730835001" />
                  <Point X="-25.360885174186" Y="4.766368547106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.672770721475" Y="4.594802344884" />
                  <Point X="-22.337955119553" Y="4.01266743385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.648179586554" Y="4.686579381115" />
                  <Point X="-22.619154932238" Y="4.180686716334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.623588451634" Y="4.778356417345" />
                  <Point X="-22.99893557122" Y="4.373284959359" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.307466796875" Y="-4.361870117188" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.626005859375" Y="-3.39140234375" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.8654375" Y="-3.223019287109" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.1484375" Y="-3.231146972656" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643798828" />
                  <Point X="-25.378603515625" Y="-3.415785400391" />
                  <Point X="-25.452005859375" Y="-3.521544189453" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.638814453125" Y="-4.207344238281" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.967341796875" Y="-4.963862304688" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.261916015625" Y="-4.89646875" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.335751953125" Y="-4.753103515625" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.34307421875" Y="-4.366885253906" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.51496875" Y="-4.089774414062" />
                  <Point X="-26.61954296875" Y="-3.998064208984" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.82003515625" Y="-3.974568359375" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791259766" />
                  <Point X="-27.132193359375" Y="-4.068883300781" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.36055859375" Y="-4.288691894531" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.6610234375" Y="-4.288231933594" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.079701171875" Y="-3.995240234375" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.916724609375" Y="-3.340456787109" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.59759375" />
                  <Point X="-27.513978515625" Y="-2.568764892578" />
                  <Point X="-27.531326171875" Y="-2.551416259766" />
                  <Point X="-27.560154296875" Y="-2.537198974609" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.169744140625" Y="-2.877214111328" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-29.007794921875" Y="-3.049334716797" />
                  <Point X="-29.161697265625" Y="-2.847136962891" />
                  <Point X="-29.322197265625" Y="-2.578002441406" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-28.882361328125" Y="-1.97452722168" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.396017700195" />
                  <Point X="-28.138115234375" Y="-1.366263061523" />
                  <Point X="-28.140326171875" Y="-1.33459375" />
                  <Point X="-28.161158203125" Y="-1.310638916016" />
                  <Point X="-28.187640625" Y="-1.295052368164" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.949275390625" Y="-1.384644287109" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.867197265625" Y="-1.246853515625" />
                  <Point X="-29.927392578125" Y="-1.011188354492" />
                  <Point X="-29.96985546875" Y="-0.714293762207" />
                  <Point X="-29.99839453125" Y="-0.514742248535" />
                  <Point X="-29.37602734375" Y="-0.347979400635" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.5265859375" Y="-0.110799880981" />
                  <Point X="-28.502322265625" Y="-0.090644966125" />
                  <Point X="-28.48979296875" Y="-0.059460945129" />
                  <Point X="-28.483400390625" Y="-0.031280040741" />
                  <Point X="-28.49075" Y="-1.8321589E-05" />
                  <Point X="-28.502322265625" Y="0.028084888458" />
                  <Point X="-28.529453125" Y="0.050230701447" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.222662109375" Y="0.244325271606" />
                  <Point X="-29.998185546875" Y="0.452126068115" />
                  <Point X="-29.956439453125" Y="0.73424822998" />
                  <Point X="-29.91764453125" Y="0.996414733887" />
                  <Point X="-29.83216796875" Y="1.311854980469" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.340033203125" Y="1.471229736328" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.69781640625" Y="1.406551269531" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.651533203125" Y="1.426056640625" />
                  <Point X="-28.6391171875" Y="1.443787353516" />
                  <Point X="-28.62551953125" Y="1.476614868164" />
                  <Point X="-28.614470703125" Y="1.503291748047" />
                  <Point X="-28.61071484375" Y="1.524606933594" />
                  <Point X="-28.61631640625" Y="1.545513183594" />
                  <Point X="-28.63272265625" Y="1.577030639648" />
                  <Point X="-28.646056640625" Y="1.602642822266" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-29.041529296875" Y="1.912003540039" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.310755859375" Y="2.528749511719" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-28.933587890625" Y="3.078044677734" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.519533203125" Y="3.135006835938" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.091318359375" Y="2.916305664062" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775146484" />
                  <Point X="-28.013251953125" Y="2.927403808594" />
                  <Point X="-27.979751953125" Y="2.960903808594" />
                  <Point X="-27.95252734375" Y="2.988127197266" />
                  <Point X="-27.9408984375" Y="3.006381835938" />
                  <Point X="-27.938072265625" Y="3.027841064453" />
                  <Point X="-27.942201171875" Y="3.075037109375" />
                  <Point X="-27.945556640625" Y="3.113390625" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.1211484375" Y="3.426889892578" />
                  <Point X="-28.30727734375" Y="3.749276611328" />
                  <Point X="-28.015419921875" Y="3.973041992188" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.39626171875" Y="4.372458007812" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.0781484375" Y="4.431348144531" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.898716796875" Y="4.246315429688" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.218491699219" />
                  <Point X="-26.81380859375" Y="4.22225" />
                  <Point X="-26.75909765625" Y="4.244913085938" />
                  <Point X="-26.714634765625" Y="4.263329589844" />
                  <Point X="-26.696904296875" Y="4.275743652344" />
                  <Point X="-26.68608203125" Y="4.294487304688" />
                  <Point X="-26.668275390625" Y="4.350966796875" />
                  <Point X="-26.653802734375" Y="4.396863769531" />
                  <Point X="-26.651916015625" Y="4.418426757812" />
                  <Point X="-26.671138671875" Y="4.564437011719" />
                  <Point X="-26.68913671875" Y="4.701141113281" />
                  <Point X="-26.30797265625" Y="4.808006347656" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.53576953125" Y="4.953893554688" />
                  <Point X="-25.22419921875" Y="4.990358398438" />
                  <Point X="-25.14875" Y="4.708774902344" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.85891015625" Y="4.63423046875" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.436556640625" Y="4.956645019531" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.78212109375" Y="4.839212402344" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.258423828125" Y="4.684504394531" />
                  <Point X="-23.068970703125" Y="4.615788085938" />
                  <Point X="-22.843849609375" Y="4.510506347656" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.443828125" Y="4.298434570312" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-22.06237109375" Y="4.049832519531" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.29480078125" Y="3.326917480469" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491514404297" />
                  <Point X="-22.7869921875" Y="2.447221923828" />
                  <Point X="-22.7966171875" Y="2.411228027344" />
                  <Point X="-22.797955078125" Y="2.392327636719" />
                  <Point X="-22.7933359375" Y="2.35402734375" />
                  <Point X="-22.789583984375" Y="2.322902832031" />
                  <Point X="-22.78131640625" Y="2.300811279297" />
                  <Point X="-22.7576171875" Y="2.265885009766" />
                  <Point X="-22.738357421875" Y="2.237502685547" />
                  <Point X="-22.72505859375" Y="2.224203613281" />
                  <Point X="-22.6901328125" Y="2.200504638672" />
                  <Point X="-22.66175" Y="2.18124609375" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.601361328125" Y="2.168361816406" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5513359375" Y="2.165946289062" />
                  <Point X="-22.50704296875" Y="2.177790771484" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.78990234375" Y="2.578701416016" />
                  <Point X="-21.005751953125" Y="3.031430664062" />
                  <Point X="-20.902013671875" Y="2.887258544922" />
                  <Point X="-20.79740234375" Y="2.741874267578" />
                  <Point X="-20.683064453125" Y="2.552927734375" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.085546875" Y="2.073302490234" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583832763672" />
                  <Point X="-21.752505859375" Y="1.542246337891" />
                  <Point X="-21.77841015625" Y="1.508451416016" />
                  <Point X="-21.78687890625" Y="1.491501098633" />
                  <Point X="-21.79875390625" Y="1.449041259766" />
                  <Point X="-21.808404296875" Y="1.414536621094" />
                  <Point X="-21.809220703125" Y="1.39096472168" />
                  <Point X="-21.79947265625" Y="1.34372253418" />
                  <Point X="-21.79155078125" Y="1.30533190918" />
                  <Point X="-21.784353515625" Y="1.287955200195" />
                  <Point X="-21.757841796875" Y="1.247657348633" />
                  <Point X="-21.736296875" Y="1.214909790039" />
                  <Point X="-21.719052734375" Y="1.1988203125" />
                  <Point X="-21.680630859375" Y="1.177193115234" />
                  <Point X="-21.64941015625" Y="1.159617919922" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.579486328125" Y="1.14675402832" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.888630859375" Y="1.224845581055" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.10573828125" Y="1.135934570312" />
                  <Point X="-20.06080859375" Y="0.951367614746" />
                  <Point X="-20.02477734375" Y="0.719953430176" />
                  <Point X="-20.002140625" Y="0.574556396484" />
                  <Point X="-20.541611328125" Y="0.430005371094" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819503784" />
                  <Point X="-21.321947265625" Y="0.203319747925" />
                  <Point X="-21.363421875" Y="0.179347137451" />
                  <Point X="-21.377734375" Y="0.166927001953" />
                  <Point X="-21.40835546875" Y="0.127907806396" />
                  <Point X="-21.433240234375" Y="0.096199172974" />
                  <Point X="-21.443013671875" Y="0.07473526001" />
                  <Point X="-21.453220703125" Y="0.021437068939" />
                  <Point X="-21.461515625" Y="-0.021875299454" />
                  <Point X="-21.461515625" Y="-0.040684780121" />
                  <Point X="-21.45130859375" Y="-0.093982971191" />
                  <Point X="-21.443013671875" Y="-0.137295333862" />
                  <Point X="-21.433240234375" Y="-0.158759246826" />
                  <Point X="-21.402619140625" Y="-0.197778442383" />
                  <Point X="-21.377734375" Y="-0.229487075806" />
                  <Point X="-21.363421875" Y="-0.241907211304" />
                  <Point X="-21.31238671875" Y="-0.271406829834" />
                  <Point X="-21.270912109375" Y="-0.295379577637" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.67598828125" Y="-0.456559082031" />
                  <Point X="-20.001931640625" Y="-0.637172424316" />
                  <Point X="-20.026482421875" Y="-0.800012207031" />
                  <Point X="-20.051568359375" Y="-0.966412902832" />
                  <Point X="-20.09773046875" Y="-1.168696044922" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-20.756244140625" Y="-1.207133178711" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.705328125" Y="-1.120112792969" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.814552734375" Y="-1.154697021484" />
                  <Point X="-21.875095703125" Y="-1.227512084961" />
                  <Point X="-21.924296875" Y="-1.286684936523" />
                  <Point X="-21.935640625" Y="-1.314071166992" />
                  <Point X="-21.944318359375" Y="-1.408370361328" />
                  <Point X="-21.951369140625" Y="-1.485001464844" />
                  <Point X="-21.94363671875" Y="-1.516622436523" />
                  <Point X="-21.888205078125" Y="-1.602844970703" />
                  <Point X="-21.84315625" Y="-1.672913085938" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-21.29065625" Y="-2.100573730469" />
                  <Point X="-20.660923828125" Y="-2.583784179688" />
                  <Point X="-20.72515234375" Y="-2.687715332031" />
                  <Point X="-20.795869140625" Y="-2.802142578125" />
                  <Point X="-20.891337890625" Y="-2.937790771484" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.50573828125" Y="-2.686920654297" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.38187109375" Y="-2.231782958984" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.609958984375" Y="-2.2713671875" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.334683349609" />
                  <Point X="-22.763521484375" Y="-2.433719970703" />
                  <Point X="-22.80587890625" Y="-2.514201416016" />
                  <Point X="-22.8108359375" Y="-2.546374511719" />
                  <Point X="-22.7893046875" Y="-2.665587402344" />
                  <Point X="-22.77180859375" Y="-2.76246484375" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.4183359375" Y="-3.380588623047" />
                  <Point X="-22.01332421875" Y="-4.082088867188" />
                  <Point X="-22.080791015625" Y="-4.130278320312" />
                  <Point X="-22.164685546875" Y="-4.190202148438" />
                  <Point X="-22.271443359375" Y="-4.259304199219" />
                  <Point X="-22.320224609375" Y="-4.29087890625" />
                  <Point X="-22.751517578125" Y="-3.728805175781" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.447025390625" Y="-2.904876708984" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.70278515625" Y="-2.847550048828" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.933958984375" Y="-2.951068359375" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045985839844" />
                  <Point X="-24.06123046875" Y="-3.182575439453" />
                  <Point X="-24.085357421875" Y="-3.293573730469" />
                  <Point X="-24.0860703125" Y="-3.310719970703" />
                  <Point X="-23.9875703125" Y="-4.058893066406" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.92626953125" Y="-4.945846191406" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.10426171875" Y="-4.981160644531" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#172" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.106609374343" Y="4.752996095487" Z="1.4" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.4" />
                  <Point X="-0.547739060359" Y="5.034834628843" Z="1.4" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.4" />
                  <Point X="-1.327626913789" Y="4.887429855699" Z="1.4" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.4" />
                  <Point X="-1.72686849711" Y="4.589190881874" Z="1.4" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.4" />
                  <Point X="-1.722117291954" Y="4.397283259761" Z="1.4" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.4" />
                  <Point X="-1.784386680358" Y="4.322387374726" Z="1.4" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.4" />
                  <Point X="-1.88178625045" Y="4.321946287265" Z="1.4" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.4" />
                  <Point X="-2.044637379235" Y="4.493066048266" Z="1.4" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.4" />
                  <Point X="-2.426701805349" Y="4.447445585722" Z="1.4" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.4" />
                  <Point X="-3.051997752229" Y="4.044001900013" Z="1.4" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.4" />
                  <Point X="-3.170605723859" Y="3.433169839907" Z="1.4" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.4" />
                  <Point X="-2.998169165875" Y="3.101959518461" Z="1.4" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.4" />
                  <Point X="-3.02126352379" Y="3.027540044096" Z="1.4" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.4" />
                  <Point X="-3.093116947145" Y="2.997395533298" Z="1.4" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.4" />
                  <Point X="-3.50068950839" Y="3.209588224887" Z="1.4" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.4" />
                  <Point X="-3.97920829997" Y="3.140027088315" Z="1.4" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.4" />
                  <Point X="-4.360094327911" Y="2.585234704526" Z="1.4" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.4" />
                  <Point X="-4.078122848383" Y="1.903615802694" Z="1.4" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.4" />
                  <Point X="-3.683229350657" Y="1.585221779797" Z="1.4" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.4" />
                  <Point X="-3.67787236177" Y="1.527027492502" Z="1.4" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.4" />
                  <Point X="-3.719008416809" Y="1.485517260433" Z="1.4" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.4" />
                  <Point X="-4.339664003161" Y="1.552082021973" Z="1.4" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.4" />
                  <Point X="-4.886583773817" Y="1.356212398948" Z="1.4" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.4" />
                  <Point X="-5.012057007773" Y="0.772847385504" Z="1.4" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.4" />
                  <Point X="-4.241761203174" Y="0.227308962878" Z="1.4" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.4" />
                  <Point X="-3.564118597031" Y="0.040433477307" Z="1.4" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.4" />
                  <Point X="-3.544660408605" Y="0.016443931866" Z="1.4" />
                  <Point X="-3.539556741714" Y="0" Z="1.4" />
                  <Point X="-3.54370419253" Y="-0.013363019195" Z="1.4" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.4" />
                  <Point X="-3.561249998092" Y="-0.038442505827" Z="1.4" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.4" />
                  <Point X="-4.395126585598" Y="-0.268403093878" Z="1.4" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.4" />
                  <Point X="-5.02550821904" Y="-0.69009263967" Z="1.4" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.4" />
                  <Point X="-4.921798668998" Y="-1.227947404549" Z="1.4" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.4" />
                  <Point X="-3.948907465691" Y="-1.402936512406" Z="1.4" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.4" />
                  <Point X="-3.207286246072" Y="-1.313851028259" Z="1.4" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.4" />
                  <Point X="-3.196130377033" Y="-1.335786109845" Z="1.4" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.4" />
                  <Point X="-3.9189563711" Y="-1.903579573936" Z="1.4" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.4" />
                  <Point X="-4.371298507939" Y="-2.572332342331" Z="1.4" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.4" />
                  <Point X="-4.053736220787" Y="-3.048337967092" Z="1.4" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.4" />
                  <Point X="-3.150901598947" Y="-2.889235275267" Z="1.4" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.4" />
                  <Point X="-2.565061825756" Y="-2.563268643375" Z="1.4" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.4" />
                  <Point X="-2.966181737902" Y="-3.284176783545" Z="1.4" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.4" />
                  <Point X="-3.116361635248" Y="-4.003577363632" Z="1.4" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.4" />
                  <Point X="-2.693503129646" Y="-4.299462563005" Z="1.4" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.4" />
                  <Point X="-2.327047167291" Y="-4.287849688674" Z="1.4" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.4" />
                  <Point X="-2.110571120553" Y="-4.079176433275" Z="1.4" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.4" />
                  <Point X="-1.829461333909" Y="-3.993181460465" Z="1.4" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.4" />
                  <Point X="-1.554091594976" Y="-4.096086096045" Z="1.4" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.4" />
                  <Point X="-1.398270382738" Y="-4.345360234246" Z="1.4" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.4" />
                  <Point X="-1.39148088268" Y="-4.715297033687" Z="1.4" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.4" />
                  <Point X="-1.280532404753" Y="-4.913611495702" Z="1.4" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.4" />
                  <Point X="-0.983025532825" Y="-4.981666486745" Z="1.4" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.4" />
                  <Point X="-0.596674943702" Y="-4.189005277928" Z="1.4" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.4" />
                  <Point X="-0.343684559131" Y="-3.413014472663" Z="1.4" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.4" />
                  <Point X="-0.139773418293" Y="-3.247619879356" Z="1.4" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.4" />
                  <Point X="0.113585661068" Y="-3.239492165932" Z="1.4" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.4" />
                  <Point X="0.326761300301" Y="-3.388631283105" Z="1.4" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.4" />
                  <Point X="0.6380799167" Y="-4.343530751887" Z="1.4" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.4" />
                  <Point X="0.898518840537" Y="-4.999075318521" Z="1.4" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.4" />
                  <Point X="1.078278435028" Y="-4.963406617075" Z="1.4" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.4" />
                  <Point X="1.05584466756" Y="-4.021086907199" Z="1.4" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.4" />
                  <Point X="0.981471720589" Y="-3.161915460299" Z="1.4" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.4" />
                  <Point X="1.091850124276" Y="-2.958234834558" Z="1.4" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.4" />
                  <Point X="1.295640923948" Y="-2.866059514941" Z="1.4" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.4" />
                  <Point X="1.519777720602" Y="-2.915654651899" Z="1.4" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.4" />
                  <Point X="2.20265754462" Y="-3.72796317627" Z="1.4" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.4" />
                  <Point X="2.74957058136" Y="-4.269998078885" Z="1.4" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.4" />
                  <Point X="2.942113008207" Y="-4.139685175149" Z="1.4" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.4" />
                  <Point X="2.618807640535" Y="-3.324308558053" Z="1.4" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.4" />
                  <Point X="2.253740900582" Y="-2.625421195733" Z="1.4" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.4" />
                  <Point X="2.274567870989" Y="-2.425726877999" Z="1.4" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.4" />
                  <Point X="2.407171500373" Y="-2.284333438978" Z="1.4" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.4" />
                  <Point X="2.60308560778" Y="-2.249707108909" Z="1.4" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.4" />
                  <Point X="3.463105039054" Y="-2.69894185974" Z="1.4" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.4" />
                  <Point X="4.143394715437" Y="-2.935287874953" Z="1.4" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.4" />
                  <Point X="4.311222969993" Y="-2.682720779103" Z="1.4" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.4" />
                  <Point X="3.733624196702" Y="-2.029625958744" Z="1.4" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.4" />
                  <Point X="3.147695599955" Y="-1.544525031676" Z="1.4" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.4" />
                  <Point X="3.099314092104" Y="-1.381671230682" Z="1.4" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.4" />
                  <Point X="3.157191797441" Y="-1.228199496147" Z="1.4" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.4" />
                  <Point X="3.299134248192" Y="-1.137691834563" Z="1.4" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.4" />
                  <Point X="4.231073266393" Y="-1.225425462574" Z="1.4" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.4" />
                  <Point X="4.944859043342" Y="-1.148539829739" Z="1.4" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.4" />
                  <Point X="5.016802805053" Y="-0.776185969034" Z="1.4" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.4" />
                  <Point X="4.33079523899" Y="-0.376982817434" Z="1.4" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.4" />
                  <Point X="3.706478725945" Y="-0.196837936667" Z="1.4" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.4" />
                  <Point X="3.630558330461" Y="-0.135629654144" Z="1.4" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.4" />
                  <Point X="3.591641928966" Y="-0.053298205175" Z="1.4" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.4" />
                  <Point X="3.589729521458" Y="0.043312326031" Z="1.4" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.4" />
                  <Point X="3.624821107939" Y="0.128319084431" Z="1.4" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.4" />
                  <Point X="3.696916688408" Y="0.191310907575" Z="1.4" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.4" />
                  <Point X="4.465172578316" Y="0.412989115334" Z="1.4" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.4" />
                  <Point X="5.018469901685" Y="0.758925395024" Z="1.4" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.4" />
                  <Point X="4.936685888448" Y="1.179041022674" Z="1.4" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.4" />
                  <Point X="4.098686987039" Y="1.30569790394" Z="1.4" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.4" />
                  <Point X="3.420907351054" Y="1.227603185832" Z="1.4" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.4" />
                  <Point X="3.337738764863" Y="1.252043778901" Z="1.4" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.4" />
                  <Point X="3.277773441818" Y="1.306418651478" Z="1.4" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.4" />
                  <Point X="3.24333966623" Y="1.385107206223" Z="1.4" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.4" />
                  <Point X="3.243241627859" Y="1.466853843086" Z="1.4" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.4" />
                  <Point X="3.281020825565" Y="1.543108607303" Z="1.4" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.4" />
                  <Point X="3.938732779548" Y="2.064914878594" Z="1.4" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.4" />
                  <Point X="4.353555889004" Y="2.610094092344" Z="1.4" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.4" />
                  <Point X="4.132415321535" Y="2.947741829066" Z="1.4" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.4" />
                  <Point X="3.178941517548" Y="2.653282793881" Z="1.4" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.4" />
                  <Point X="2.473884071255" Y="2.257373543742" Z="1.4" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.4" />
                  <Point X="2.398467176181" Y="2.249282210551" Z="1.4" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.4" />
                  <Point X="2.331784294938" Y="2.273159206516" Z="1.4" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.4" />
                  <Point X="2.277599393719" Y="2.325240565444" Z="1.4" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.4" />
                  <Point X="2.250147492208" Y="2.391291267651" Z="1.4" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.4" />
                  <Point X="2.255154398915" Y="2.465585504627" Z="1.4" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.4" />
                  <Point X="2.742342558979" Y="3.333197564407" Z="1.4" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.4" />
                  <Point X="2.960449282218" Y="4.121859568368" Z="1.4" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.4" />
                  <Point X="2.575185672225" Y="4.372917082729" Z="1.4" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.4" />
                  <Point X="2.171175719584" Y="4.587078569254" Z="1.4" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.4" />
                  <Point X="1.752467420256" Y="4.762788058308" Z="1.4" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.4" />
                  <Point X="1.223456814604" Y="4.919096030794" Z="1.4" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.4" />
                  <Point X="0.562492508346" Y="5.037652910583" Z="1.4" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.4" />
                  <Point X="0.08663518721" Y="4.678451410464" Z="1.4" />
                  <Point X="0" Y="4.355124473572" Z="1.4" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>