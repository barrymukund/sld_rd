<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#131" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="739" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004715820312" />
                  <Width Value="9.78389453125" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.381296875" Y="-3.719274658203" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140136719" />
                  <Point X="-24.457634765625" Y="-3.467377441406" />
                  <Point X="-24.48027734375" Y="-3.434753417969" />
                  <Point X="-24.62136328125" Y="-3.231477050781" />
                  <Point X="-24.643248046875" Y="-3.209020751953" />
                  <Point X="-24.66950390625" Y="-3.189776855469" />
                  <Point X="-24.69750390625" Y="-3.175669677734" />
                  <Point X="-24.732541015625" Y="-3.164794921875" />
                  <Point X="-24.95086328125" Y="-3.097036376953" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097036132812" />
                  <Point X="-25.071861328125" Y="-3.107910644531" />
                  <Point X="-25.29018359375" Y="-3.175669433594" />
                  <Point X="-25.31818359375" Y="-3.18977734375" />
                  <Point X="-25.344439453125" Y="-3.209021484375" />
                  <Point X="-25.36632421875" Y="-3.231477050781" />
                  <Point X="-25.388966796875" Y="-3.264100830078" />
                  <Point X="-25.53005078125" Y="-3.467377441406" />
                  <Point X="-25.5381875" Y="-3.481571777344" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.896173828125" Y="-4.800764160156" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.0793359375" Y="-4.845351074219" />
                  <Point X="-26.116009765625" Y="-4.835915527344" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.221630859375" Y="-4.614093261719" />
                  <Point X="-26.2149609375" Y="-4.563439453125" />
                  <Point X="-26.21419921875" Y="-4.547930175781" />
                  <Point X="-26.2165078125" Y="-4.51622265625" />
                  <Point X="-26.22487890625" Y="-4.474140625" />
                  <Point X="-26.277037109375" Y="-4.211931152344" />
                  <Point X="-26.287939453125" Y="-4.182964355469" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131205566406" />
                  <Point X="-26.35590234375" Y="-4.102915039062" />
                  <Point X="-26.556904296875" Y="-3.926640380859" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.685841796875" Y="-3.88816015625" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708740234" />
                  <Point X="-27.014466796875" Y="-3.882028564453" />
                  <Point X="-27.042658203125" Y="-3.894801513672" />
                  <Point X="-27.078333984375" Y="-3.918639160156" />
                  <Point X="-27.300623046875" Y="-4.067169433594" />
                  <Point X="-27.312787109375" Y="-4.076824462891" />
                  <Point X="-27.3351015625" Y="-4.099462402344" />
                  <Point X="-27.4801484375" Y="-4.288489257813" />
                  <Point X="-27.80171484375" Y="-4.089383056641" />
                  <Point X="-27.852482421875" Y="-4.050293212891" />
                  <Point X="-28.10472265625" Y="-3.856077392578" />
                  <Point X="-27.526712890625" Y="-2.854937255859" />
                  <Point X="-27.42376171875" Y="-2.676619384766" />
                  <Point X="-27.412861328125" Y="-2.647663085938" />
                  <Point X="-27.406587890625" Y="-2.616142089844" />
                  <Point X="-27.4057734375" Y="-2.583754638672" />
                  <Point X="-27.415890625" Y="-2.552977539063" />
                  <Point X="-27.4325078125" Y="-2.521744140625" />
                  <Point X="-27.449201171875" Y="-2.499189941406" />
                  <Point X="-27.464150390625" Y="-2.484240722656" />
                  <Point X="-27.489310546875" Y="-2.466212402344" />
                  <Point X="-27.518140625" Y="-2.451995361328" />
                  <Point X="-27.5477578125" Y="-2.443011230469" />
                  <Point X="-27.578689453125" Y="-2.444024169922" />
                  <Point X="-27.61021484375" Y="-2.450295166016" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.750263671875" Y="-3.102680419922" />
                  <Point X="-28.8180234375" Y="-3.141801513672" />
                  <Point X="-29.08284765625" Y="-2.793877929688" />
                  <Point X="-29.119255859375" Y="-2.732825927734" />
                  <Point X="-29.306140625" Y="-2.419448974609" />
                  <Point X="-28.2860859375" Y="-1.636733642578" />
                  <Point X="-28.105955078125" Y="-1.498513305664" />
                  <Point X="-28.083068359375" Y="-1.475884399414" />
                  <Point X="-28.064505859375" Y="-1.446796264648" />
                  <Point X="-28.05672265625" Y="-1.427609619141" />
                  <Point X="-28.052791015625" Y="-1.415716674805" />
                  <Point X="-28.04615234375" Y="-1.390083374023" />
                  <Point X="-28.042037109375" Y="-1.358598266602" />
                  <Point X="-28.04273828125" Y="-1.335733154297" />
                  <Point X="-28.0488828125" Y="-1.313698364258" />
                  <Point X="-28.06088671875" Y="-1.284715942383" />
                  <Point X="-28.073083984375" Y="-1.263502441406" />
                  <Point X="-28.090294921875" Y="-1.246108276367" />
                  <Point X="-28.106458984375" Y="-1.233523681641" />
                  <Point X="-28.116634765625" Y="-1.226611206055" />
                  <Point X="-28.139455078125" Y="-1.213180297852" />
                  <Point X="-28.168716796875" Y="-1.201956665039" />
                  <Point X="-28.200603515625" Y="-1.195474975586" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-29.6345625" Y="-1.379044189453" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.834078125" Y="-0.992644958496" />
                  <Point X="-29.84370703125" Y="-0.925322570801" />
                  <Point X="-29.892421875" Y="-0.584698425293" />
                  <Point X="-28.738072265625" Y="-0.275390899658" />
                  <Point X="-28.532875" Y="-0.220408309937" />
                  <Point X="-28.514345703125" Y="-0.213296936035" />
                  <Point X="-28.49494140625" Y="-0.203413406372" />
                  <Point X="-28.483892578125" Y="-0.196807495117" />
                  <Point X="-28.4599765625" Y="-0.180209365845" />
                  <Point X="-28.436021484375" Y="-0.15868321228" />
                  <Point X="-28.41606640625" Y="-0.130455505371" />
                  <Point X="-28.407361328125" Y="-0.111594490051" />
                  <Point X="-28.402884765625" Y="-0.099940483093" />
                  <Point X="-28.3949140625" Y="-0.074255729675" />
                  <Point X="-28.39071484375" Y="-0.042501937866" />
                  <Point X="-28.391994140625" Y="-0.008741326332" />
                  <Point X="-28.396193359375" Y="0.015817751884" />
                  <Point X="-28.4041640625" Y="0.041502658844" />
                  <Point X="-28.417482421875" Y="0.070829620361" />
                  <Point X="-28.438546875" Y="0.098496459961" />
                  <Point X="-28.454205078125" Y="0.112678840637" />
                  <Point X="-28.463814453125" Y="0.120313140869" />
                  <Point X="-28.48773046875" Y="0.136911270142" />
                  <Point X="-28.50192578125" Y="0.14504737854" />
                  <Point X="-28.532875" Y="0.157848327637" />
                  <Point X="-29.811443359375" Y="0.500439666748" />
                  <Point X="-29.89181640625" Y="0.521975769043" />
                  <Point X="-29.82448828125" Y="0.976964477539" />
                  <Point X="-29.805099609375" Y="1.048515869141" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-28.912755859375" Y="1.319157836914" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228027344" />
                  <Point X="-28.7031328125" Y="1.305264282227" />
                  <Point X="-28.694638671875" Y="1.307942626953" />
                  <Point X="-28.64170703125" Y="1.324631835938" />
                  <Point X="-28.62277734375" Y="1.332962158203" />
                  <Point X="-28.604033203125" Y="1.343784301758" />
                  <Point X="-28.5873515625" Y="1.356015991211" />
                  <Point X="-28.573712890625" Y="1.371568481445" />
                  <Point X="-28.561298828125" Y="1.389298583984" />
                  <Point X="-28.551349609375" Y="1.407435180664" />
                  <Point X="-28.54794140625" Y="1.415664428711" />
                  <Point X="-28.526703125" Y="1.466939331055" />
                  <Point X="-28.520916015625" Y="1.486788085938" />
                  <Point X="-28.51715625" Y="1.508103027344" />
                  <Point X="-28.515802734375" Y="1.52875" />
                  <Point X="-28.518951171875" Y="1.549200195312" />
                  <Point X="-28.5245546875" Y="1.570106445312" />
                  <Point X="-28.532052734375" Y="1.589381225586" />
                  <Point X="-28.536166015625" Y="1.597281982422" />
                  <Point X="-28.56179296875" Y="1.646510986328" />
                  <Point X="-28.57328125" Y="1.663704956055" />
                  <Point X="-28.587193359375" Y="1.680285400391" />
                  <Point X="-28.60213671875" Y="1.694590454102" />
                  <Point X="-29.335525390625" Y="2.257340087891" />
                  <Point X="-29.351859375" Y="2.269874023438" />
                  <Point X="-29.081146484375" Y="2.733666748047" />
                  <Point X="-29.029802734375" Y="2.7996640625" />
                  <Point X="-28.75050390625" Y="3.158662353516" />
                  <Point X="-28.296994140625" Y="2.896828369141" />
                  <Point X="-28.20665625" Y="2.844671142578" />
                  <Point X="-28.187724609375" Y="2.836340820312" />
                  <Point X="-28.16708203125" Y="2.82983203125" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.1349609375" Y="2.824761230469" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.999013671875" Y="2.826504638672" />
                  <Point X="-27.980462890625" Y="2.835653564453" />
                  <Point X="-27.962208984375" Y="2.847282714844" />
                  <Point X="-27.946080078125" Y="2.8602265625" />
                  <Point X="-27.937681640625" Y="2.868624267578" />
                  <Point X="-27.88535546875" Y="2.920949951172" />
                  <Point X="-27.872404296875" Y="2.937085693359" />
                  <Point X="-27.860775390625" Y="2.955340087891" />
                  <Point X="-27.85162890625" Y="2.973888916016" />
                  <Point X="-27.8467109375" Y="2.993977783203" />
                  <Point X="-27.843884765625" Y="3.015436523438" />
                  <Point X="-27.84343359375" Y="3.036121337891" />
                  <Point X="-27.84446875" Y="3.047952636719" />
                  <Point X="-27.85091796875" Y="3.121670898438" />
                  <Point X="-27.854955078125" Y="3.141963378906" />
                  <Point X="-27.86146484375" Y="3.162605224609" />
                  <Point X="-27.869794921875" Y="3.181532470703" />
                  <Point X="-28.183333984375" Y="3.724596191406" />
                  <Point X="-27.700626953125" Y="4.094683105469" />
                  <Point X="-27.619751953125" Y="4.139614746094" />
                  <Point X="-27.167037109375" Y="4.391133789062" />
                  <Point X="-27.0708515625" Y="4.265781738281" />
                  <Point X="-27.0431953125" Y="4.229740234375" />
                  <Point X="-27.028890625" Y="4.214797851563" />
                  <Point X="-27.012310546875" Y="4.200885742188" />
                  <Point X="-26.99511328125" Y="4.189395019531" />
                  <Point X="-26.9819453125" Y="4.182540039063" />
                  <Point X="-26.899896484375" Y="4.139828125" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.777451171875" Y="4.134482421875" />
                  <Point X="-26.763736328125" Y="4.140163574219" />
                  <Point X="-26.67827734375" Y="4.175562011719" />
                  <Point X="-26.660146484375" Y="4.185508789062" />
                  <Point X="-26.642416015625" Y="4.197922851562" />
                  <Point X="-26.626859375" Y="4.211563476563" />
                  <Point X="-26.614626953125" Y="4.228248535156" />
                  <Point X="-26.603806640625" Y="4.2469921875" />
                  <Point X="-26.5954765625" Y="4.265927734375" />
                  <Point X="-26.591013671875" Y="4.2800859375" />
                  <Point X="-26.563197265625" Y="4.368304199219" />
                  <Point X="-26.5591640625" Y="4.388583496094" />
                  <Point X="-26.55727734375" Y="4.410145996094" />
                  <Point X="-26.557728515625" Y="4.430826660156" />
                  <Point X="-26.58419921875" Y="4.631898925781" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.851599609375" Y="4.821281738281" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.16062890625" Y="4.386054199219" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183885742188" />
                  <Point X="-24.932869140625" Y="4.194217773438" />
                  <Point X="-24.905576171875" Y="4.208806152344" />
                  <Point X="-24.88441796875" Y="4.231394042969" />
                  <Point X="-24.86655859375" Y="4.258120117188" />
                  <Point X="-24.853783203125" Y="4.286314453125" />
                  <Point X="-24.692580078125" Y="4.8879375" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-24.07484375" Y="4.812155761719" />
                  <Point X="-23.51897265625" Y="4.677951171875" />
                  <Point X="-23.467455078125" Y="4.659265136719" />
                  <Point X="-23.105357421875" Y="4.527930175781" />
                  <Point X="-23.054296875" Y="4.50405078125" />
                  <Point X="-22.7054296875" Y="4.340897460937" />
                  <Point X="-22.6560703125" Y="4.312139648438" />
                  <Point X="-22.319009765625" Y="4.115767089844" />
                  <Point X="-22.272498046875" Y="4.082691650391" />
                  <Point X="-22.056736328125" Y="3.929254638672" />
                  <Point X="-22.7332578125" Y="2.757487304688" />
                  <Point X="-22.852416015625" Y="2.55109765625" />
                  <Point X="-22.859984375" Y="2.534482910156" />
                  <Point X="-22.867955078125" Y="2.511297607422" />
                  <Point X="-22.869890625" Y="2.504956054688" />
                  <Point X="-22.888392578125" Y="2.435772949219" />
                  <Point X="-22.891615234375" Y="2.411828125" />
                  <Point X="-22.891794921875" Y="2.38332421875" />
                  <Point X="-22.89111328125" Y="2.371353759766" />
                  <Point X="-22.883900390625" Y="2.311530029297" />
                  <Point X="-22.87855859375" Y="2.289608398438" />
                  <Point X="-22.87029296875" Y="2.267518798828" />
                  <Point X="-22.859927734375" Y="2.247467773438" />
                  <Point X="-22.853986328125" Y="2.238712402344" />
                  <Point X="-22.81696875" Y="2.184159179688" />
                  <Point X="-22.8008125" Y="2.165917236328" />
                  <Point X="-22.7787578125" Y="2.146676269531" />
                  <Point X="-22.76964453125" Y="2.139650390625" />
                  <Point X="-22.71508984375" Y="2.102633789063" />
                  <Point X="-22.695044921875" Y="2.092271728516" />
                  <Point X="-22.67295703125" Y="2.084006103516" />
                  <Point X="-22.65103515625" Y="2.078663330078" />
                  <Point X="-22.64143359375" Y="2.077505615234" />
                  <Point X="-22.581611328125" Y="2.070291748047" />
                  <Point X="-22.556740234375" Y="2.070572265625" />
                  <Point X="-22.526734375" Y="2.074879150391" />
                  <Point X="-22.51569140625" Y="2.077140136719" />
                  <Point X="-22.4465078125" Y="2.095640625" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.125470703125" Y="2.852613769531" />
                  <Point X="-21.032673828125" Y="2.906190673828" />
                  <Point X="-20.87672265625" Y="2.68945703125" />
                  <Point X="-20.850794921875" Y="2.646606933594" />
                  <Point X="-20.73780078125" Y="2.459883300781" />
                  <Point X="-21.612330078125" Y="1.788832641602" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.78283984375" Y="1.655683959961" />
                  <Point X="-21.800076171875" Y="1.636009277344" />
                  <Point X="-21.804017578125" Y="1.631202636719" />
                  <Point X="-21.85380859375" Y="1.56624621582" />
                  <Point X="-21.866162109375" Y="1.544848144531" />
                  <Point X="-21.877607421875" Y="1.517253540039" />
                  <Point X="-21.88134375" Y="1.50644519043" />
                  <Point X="-21.899892578125" Y="1.440124633789" />
                  <Point X="-21.90334765625" Y="1.417816162109" />
                  <Point X="-21.904162109375" Y="1.394237915039" />
                  <Point X="-21.902255859375" Y="1.371750610352" />
                  <Point X="-21.89981640625" Y="1.359927001953" />
                  <Point X="-21.884591796875" Y="1.28613684082" />
                  <Point X="-21.876392578125" Y="1.262590698242" />
                  <Point X="-21.862548828125" Y="1.235111328125" />
                  <Point X="-21.857072265625" Y="1.225640136719" />
                  <Point X="-21.815662109375" Y="1.162696533203" />
                  <Point X="-21.801107421875" Y="1.145450317383" />
                  <Point X="-21.78386328125" Y="1.129360473633" />
                  <Point X="-21.7656484375" Y="1.116032226562" />
                  <Point X="-21.756015625" Y="1.110610595703" />
                  <Point X="-21.696005859375" Y="1.076829711914" />
                  <Point X="-21.672365234375" Y="1.067432739258" />
                  <Point X="-21.6413671875" Y="1.059713745117" />
                  <Point X="-21.630859375" Y="1.057717651367" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.2901875" Y="1.207812133789" />
                  <Point X="-20.22316015625" Y="1.21663659668" />
                  <Point X="-20.1540625" Y="0.932803955078" />
                  <Point X="-20.145890625" Y="0.880315490723" />
                  <Point X="-20.109134765625" Y="0.644238769531" />
                  <Point X="-21.103626953125" Y="0.377764953613" />
                  <Point X="-21.283419921875" Y="0.329589874268" />
                  <Point X="-21.301224609375" Y="0.32284387207" />
                  <Point X="-21.32609765625" Y="0.310441375732" />
                  <Point X="-21.33124609375" Y="0.307673187256" />
                  <Point X="-21.410962890625" Y="0.261595825195" />
                  <Point X="-21.430919921875" Y="0.246197158813" />
                  <Point X="-21.452908203125" Y="0.223995681763" />
                  <Point X="-21.46014453125" Y="0.215795669556" />
                  <Point X="-21.507974609375" Y="0.154849243164" />
                  <Point X="-21.51969921875" Y="0.135567611694" />
                  <Point X="-21.52947265625" Y="0.114103851318" />
                  <Point X="-21.536318359375" Y="0.092603042603" />
                  <Point X="-21.538876953125" Y="0.079242149353" />
                  <Point X="-21.5548203125" Y="-0.004007439137" />
                  <Point X="-21.556216796875" Y="-0.029407131195" />
                  <Point X="-21.553658203125" Y="-0.061577281952" />
                  <Point X="-21.55226171875" Y="-0.071914360046" />
                  <Point X="-21.536318359375" Y="-0.155164260864" />
                  <Point X="-21.52947265625" Y="-0.176664001465" />
                  <Point X="-21.51969921875" Y="-0.198127761841" />
                  <Point X="-21.5079765625" Y="-0.217406814575" />
                  <Point X="-21.50030078125" Y="-0.227188186646" />
                  <Point X="-21.452470703125" Y="-0.288134460449" />
                  <Point X="-21.433798828125" Y="-0.306178833008" />
                  <Point X="-21.406693359375" Y="-0.325994171143" />
                  <Point X="-21.398169921875" Y="-0.33155090332" />
                  <Point X="-21.318453125" Y="-0.377628234863" />
                  <Point X="-21.307291015625" Y="-0.383138244629" />
                  <Point X="-21.283419921875" Y="-0.392149871826" />
                  <Point X="-20.163150390625" Y="-0.692325378418" />
                  <Point X="-20.10852734375" Y="-0.706961853027" />
                  <Point X="-20.1449765625" Y="-0.948727416992" />
                  <Point X="-20.155447265625" Y="-0.994608215332" />
                  <Point X="-20.19882421875" Y="-1.18469909668" />
                  <Point X="-21.367109375" Y="-1.030891601562" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710144043" />
                  <Point X="-21.62533984375" Y="-1.005508728027" />
                  <Point X="-21.65044921875" Y="-1.010966369629" />
                  <Point X="-21.806904296875" Y="-1.044972412109" />
                  <Point X="-21.8360234375" Y="-1.056595947266" />
                  <Point X="-21.8638515625" Y="-1.07348840332" />
                  <Point X="-21.8876015625" Y="-1.093958862305" />
                  <Point X="-21.902779296875" Y="-1.112212158203" />
                  <Point X="-21.997345703125" Y="-1.225946899414" />
                  <Point X="-22.012068359375" Y="-1.250334838867" />
                  <Point X="-22.02341015625" Y="-1.277720214844" />
                  <Point X="-22.030240234375" Y="-1.305363647461" />
                  <Point X="-22.032416015625" Y="-1.329002685547" />
                  <Point X="-22.04596875" Y="-1.476294067383" />
                  <Point X="-22.04365234375" Y="-1.507561767578" />
                  <Point X="-22.035921875" Y="-1.539182739258" />
                  <Point X="-22.02355078125" Y="-1.567993408203" />
                  <Point X="-22.00965625" Y="-1.589607543945" />
                  <Point X="-21.9230703125" Y="-1.724283935547" />
                  <Point X="-21.9130625" Y="-1.737243286133" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-20.849751953125" Y="-2.558635742188" />
                  <Point X="-20.786876953125" Y="-2.606881835938" />
                  <Point X="-20.875185546875" Y="-2.749781005859" />
                  <Point X="-20.896845703125" Y="-2.780556152344" />
                  <Point X="-20.971017578125" Y="-2.885944824219" />
                  <Point X="-22.01333984375" Y="-2.28416015625" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.245775390625" Y="-2.159824951172" />
                  <Point X="-22.27566015625" Y="-2.154427978516" />
                  <Point X="-22.461865234375" Y="-2.120799316406" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135177001953" />
                  <Point X="-22.5799921875" Y="-2.148242919922" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.757615234375" Y="-2.246549804688" />
                  <Point X="-22.77857421875" Y="-2.267509521484" />
                  <Point X="-22.795466796875" Y="-2.290437988281" />
                  <Point X="-22.808533203125" Y="-2.315264404297" />
                  <Point X="-22.889947265625" Y="-2.469956054688" />
                  <Point X="-22.899771484375" Y="-2.499735351563" />
                  <Point X="-22.904728515625" Y="-2.531908203125" />
                  <Point X="-22.904322265625" Y="-2.563262451172" />
                  <Point X="-22.898923828125" Y="-2.593146728516" />
                  <Point X="-22.865294921875" Y="-2.779352783203" />
                  <Point X="-22.86101171875" Y="-2.795139648438" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.180119140625" Y="-3.983189941406" />
                  <Point X="-22.13871484375" Y="-4.054905273438" />
                  <Point X="-22.218158203125" Y="-4.111649414062" />
                  <Point X="-22.242359375" Y="-4.127314941406" />
                  <Point X="-22.298232421875" Y="-4.163480957031" />
                  <Point X="-23.0997421875" Y="-3.118935302734" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900556884766" />
                  <Point X="-23.307548828125" Y="-2.881607666016" />
                  <Point X="-23.49119921875" Y="-2.763538085938" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.615134765625" Y="-2.744083007813" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-23.92029296875" Y="-2.816156982422" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968861083984" />
                  <Point X="-24.11275" Y="-2.996687744141" />
                  <Point X="-24.124375" Y="-3.025810791016" />
                  <Point X="-24.13181640625" Y="-3.060051025391" />
                  <Point X="-24.178189453125" Y="-3.273398681641" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323119873047" />
                  <Point X="-23.99093359375" Y="-4.76116796875" />
                  <Point X="-23.97793359375" Y="-4.859915039062" />
                  <Point X="-24.024328125" Y="-4.870084960938" />
                  <Point X="-24.046689453125" Y="-4.874146972656" />
                  <Point X="-24.07068359375" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.058421875" Y="-4.752637695312" />
                  <Point X="-26.092337890625" Y="-4.743911621094" />
                  <Point X="-26.141244140625" Y="-4.731328125" />
                  <Point X="-26.127443359375" Y="-4.626492675781" />
                  <Point X="-26.1207734375" Y="-4.575838867188" />
                  <Point X="-26.120076171875" Y="-4.568099609375" />
                  <Point X="-26.11944921875" Y="-4.541031738281" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497687988281" />
                  <Point X="-26.131705078125" Y="-4.455605957031" />
                  <Point X="-26.18386328125" Y="-4.193396484375" />
                  <Point X="-26.188126953125" Y="-4.178467285156" />
                  <Point X="-26.199029296875" Y="-4.149500488281" />
                  <Point X="-26.20566796875" Y="-4.135462890625" />
                  <Point X="-26.221740234375" Y="-4.107625976562" />
                  <Point X="-26.230576171875" Y="-4.094858886719" />
                  <Point X="-26.250208984375" Y="-4.070937011719" />
                  <Point X="-26.261005859375" Y="-4.059781982422" />
                  <Point X="-26.293263671875" Y="-4.031491455078" />
                  <Point X="-26.494265625" Y="-3.855216796875" />
                  <Point X="-26.506736328125" Y="-3.845967529297" />
                  <Point X="-26.53301953125" Y="-3.829622558594" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.67962890625" Y="-3.793363525391" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166259766" />
                  <Point X="-27.008005859375" Y="-3.781946044922" />
                  <Point X="-27.0390546875" Y="-3.790265869141" />
                  <Point X="-27.053673828125" Y="-3.79549609375" />
                  <Point X="-27.081865234375" Y="-3.808269042969" />
                  <Point X="-27.0954375" Y="-3.815811767578" />
                  <Point X="-27.13111328125" Y="-3.839649414062" />
                  <Point X="-27.35340234375" Y="-3.9881796875" />
                  <Point X="-27.35968359375" Y="-3.992760009766" />
                  <Point X="-27.380443359375" Y="-4.010134521484" />
                  <Point X="-27.4027578125" Y="-4.032772460938" />
                  <Point X="-27.410470703125" Y="-4.041629638672" />
                  <Point X="-27.503201171875" Y="-4.162478515625" />
                  <Point X="-27.747595703125" Y="-4.011155517578" />
                  <Point X="-27.794525390625" Y="-3.975020996094" />
                  <Point X="-27.98086328125" Y="-3.831546875" />
                  <Point X="-27.44444140625" Y="-2.902437255859" />
                  <Point X="-27.341490234375" Y="-2.724119384766" />
                  <Point X="-27.334853515625" Y="-2.710088623047" />
                  <Point X="-27.323953125" Y="-2.681132324219" />
                  <Point X="-27.319689453125" Y="-2.666206787109" />
                  <Point X="-27.313416015625" Y="-2.634685791016" />
                  <Point X="-27.3116171875" Y="-2.618530273438" />
                  <Point X="-27.310802734375" Y="-2.586142822266" />
                  <Point X="-27.315525390625" Y="-2.554087646484" />
                  <Point X="-27.325642578125" Y="-2.523310546875" />
                  <Point X="-27.332021484375" Y="-2.508356689453" />
                  <Point X="-27.348638671875" Y="-2.477123291016" />
                  <Point X="-27.3561484375" Y="-2.465227050781" />
                  <Point X="-27.372841796875" Y="-2.442672851562" />
                  <Point X="-27.382025390625" Y="-2.432014892578" />
                  <Point X="-27.396974609375" Y="-2.417065673828" />
                  <Point X="-27.408818359375" Y="-2.407018554688" />
                  <Point X="-27.433978515625" Y="-2.388990234375" />
                  <Point X="-27.447294921875" Y="-2.381009033203" />
                  <Point X="-27.476125" Y="-2.366791992188" />
                  <Point X="-27.490564453125" Y="-2.3610859375" />
                  <Point X="-27.520181640625" Y="-2.352101806641" />
                  <Point X="-27.5508671875" Y="-2.348062011719" />
                  <Point X="-27.581798828125" Y="-2.349074951172" />
                  <Point X="-27.59722265625" Y="-2.350849609375" />
                  <Point X="-27.628748046875" Y="-2.357120605469" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372285888672" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.793087890625" Y="-3.017708496094" />
                  <Point X="-29.004001953125" Y="-2.740610839844" />
                  <Point X="-29.037662109375" Y="-2.684168212891" />
                  <Point X="-29.181263671875" Y="-2.443371826172" />
                  <Point X="-28.22825390625" Y="-1.712102172852" />
                  <Point X="-28.048123046875" Y="-1.573881958008" />
                  <Point X="-28.039162109375" Y="-1.566067871094" />
                  <Point X="-28.016275390625" Y="-1.543439086914" />
                  <Point X="-28.002984375" Y="-1.526989135742" />
                  <Point X="-27.984421875" Y="-1.497901000977" />
                  <Point X="-27.97647265625" Y="-1.482507324219" />
                  <Point X="-27.968689453125" Y="-1.463320678711" />
                  <Point X="-27.960826171875" Y="-1.439534545898" />
                  <Point X="-27.9541875" Y="-1.413901245117" />
                  <Point X="-27.951953125" Y="-1.402395507812" />
                  <Point X="-27.947837890625" Y="-1.370910400391" />
                  <Point X="-27.94708203125" Y="-1.355686523438" />
                  <Point X="-27.947783203125" Y="-1.332821289062" />
                  <Point X="-27.95123046875" Y="-1.310215454102" />
                  <Point X="-27.957375" Y="-1.288180664062" />
                  <Point X="-27.96111328125" Y="-1.277346069336" />
                  <Point X="-27.9731171875" Y="-1.248363647461" />
                  <Point X="-27.978529296875" Y="-1.237362670898" />
                  <Point X="-27.9907265625" Y="-1.216149291992" />
                  <Point X="-28.0055546875" Y="-1.196683959961" />
                  <Point X="-28.022765625" Y="-1.179289794922" />
                  <Point X="-28.03193359375" Y="-1.171148071289" />
                  <Point X="-28.04809765625" Y="-1.158563476562" />
                  <Point X="-28.06844921875" Y="-1.144738769531" />
                  <Point X="-28.09126953125" Y="-1.131307861328" />
                  <Point X="-28.10543359375" Y="-1.124481201172" />
                  <Point X="-28.1346953125" Y="-1.113257446289" />
                  <Point X="-28.14979296875" Y="-1.108860595703" />
                  <Point X="-28.1816796875" Y="-1.10237890625" />
                  <Point X="-28.197296875" Y="-1.100532592773" />
                  <Point X="-28.22862109375" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-29.646962890625" Y="-1.284856933594" />
                  <Point X="-29.66091796875" Y="-1.286694213867" />
                  <Point X="-29.740763671875" Y="-0.974107299805" />
                  <Point X="-29.7496640625" Y="-0.911871887207" />
                  <Point X="-29.786451171875" Y="-0.654654663086" />
                  <Point X="-28.713484375" Y="-0.367153869629" />
                  <Point X="-28.508287109375" Y="-0.312171295166" />
                  <Point X="-28.4988359375" Y="-0.309100616455" />
                  <Point X="-28.471228515625" Y="-0.297948699951" />
                  <Point X="-28.45182421875" Y="-0.288065124512" />
                  <Point X="-28.4297265625" Y="-0.274853302002" />
                  <Point X="-28.405810546875" Y="-0.258255096436" />
                  <Point X="-28.396478515625" Y="-0.250871368408" />
                  <Point X="-28.3725234375" Y="-0.22934513855" />
                  <Point X="-28.358447265625" Y="-0.213522445679" />
                  <Point X="-28.3384921875" Y="-0.185294815063" />
                  <Point X="-28.329810546875" Y="-0.170266067505" />
                  <Point X="-28.32110546875" Y="-0.151404937744" />
                  <Point X="-28.31215234375" Y="-0.128097015381" />
                  <Point X="-28.304181640625" Y="-0.102412307739" />
                  <Point X="-28.300734375" Y="-0.086710456848" />
                  <Point X="-28.29653515625" Y="-0.054956588745" />
                  <Point X="-28.295783203125" Y="-0.038904727936" />
                  <Point X="-28.2970625" Y="-0.00514400959" />
                  <Point X="-28.298353515625" Y="0.007269861698" />
                  <Point X="-28.302552734375" Y="0.031828895569" />
                  <Point X="-28.3054609375" Y="0.043974205017" />
                  <Point X="-28.313431640625" Y="0.069659057617" />
                  <Point X="-28.317666015625" Y="0.080784370422" />
                  <Point X="-28.330984375" Y="0.110111373901" />
                  <Point X="-28.341896484375" Y="0.128377716064" />
                  <Point X="-28.3629609375" Y="0.15604460144" />
                  <Point X="-28.374771484375" Y="0.16890776062" />
                  <Point X="-28.3904296875" Y="0.183090240479" />
                  <Point X="-28.4096484375" Y="0.198358856201" />
                  <Point X="-28.433564453125" Y="0.214957061768" />
                  <Point X="-28.440490234375" Y="0.219332962036" />
                  <Point X="-28.465615234375" Y="0.232834594727" />
                  <Point X="-28.496564453125" Y="0.245635482788" />
                  <Point X="-28.508287109375" Y="0.249611297607" />
                  <Point X="-29.7854453125" Y="0.591825012207" />
                  <Point X="-29.73133203125" Y="0.957518432617" />
                  <Point X="-29.71340625" Y="1.023669250488" />
                  <Point X="-29.6335859375" Y="1.318237060547" />
                  <Point X="-28.92515625" Y="1.224970581055" />
                  <Point X="-28.778064453125" Y="1.20560559082" />
                  <Point X="-28.767736328125" Y="1.204815307617" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.20470324707" />
                  <Point X="-28.71514453125" Y="1.206589477539" />
                  <Point X="-28.704890625" Y="1.208053588867" />
                  <Point X="-28.684599609375" Y="1.21208984375" />
                  <Point X="-28.6660703125" Y="1.21733996582" />
                  <Point X="-28.613138671875" Y="1.234029174805" />
                  <Point X="-28.60344140625" Y="1.237679199219" />
                  <Point X="-28.58451171875" Y="1.246009399414" />
                  <Point X="-28.57527734375" Y="1.250690185547" />
                  <Point X="-28.556533203125" Y="1.261512329102" />
                  <Point X="-28.547857421875" Y="1.267172485352" />
                  <Point X="-28.53117578125" Y="1.279404052734" />
                  <Point X="-28.51592578125" Y="1.293379394531" />
                  <Point X="-28.502287109375" Y="1.308931884766" />
                  <Point X="-28.495892578125" Y="1.317080688477" />
                  <Point X="-28.483478515625" Y="1.334810913086" />
                  <Point X="-28.4780078125" Y="1.343607666016" />
                  <Point X="-28.46805859375" Y="1.361744262695" />
                  <Point X="-28.460171875" Y="1.379313720703" />
                  <Point X="-28.43893359375" Y="1.430588623047" />
                  <Point X="-28.4355" Y="1.440348266602" />
                  <Point X="-28.429712890625" Y="1.460197021484" />
                  <Point X="-28.427359375" Y="1.470285644531" />
                  <Point X="-28.423599609375" Y="1.491600585938" />
                  <Point X="-28.422359375" Y="1.501888671875" />
                  <Point X="-28.421005859375" Y="1.522535644531" />
                  <Point X="-28.421908203125" Y="1.543205444336" />
                  <Point X="-28.425056640625" Y="1.563655761719" />
                  <Point X="-28.427189453125" Y="1.573794921875" />
                  <Point X="-28.43279296875" Y="1.594701293945" />
                  <Point X="-28.436017578125" Y="1.604547973633" />
                  <Point X="-28.443515625" Y="1.623822631836" />
                  <Point X="-28.45190234375" Y="1.641151489258" />
                  <Point X="-28.477529296875" Y="1.690380493164" />
                  <Point X="-28.482802734375" Y="1.6992890625" />
                  <Point X="-28.494291015625" Y="1.716483032227" />
                  <Point X="-28.500505859375" Y="1.724768310547" />
                  <Point X="-28.51441796875" Y="1.741348876953" />
                  <Point X="-28.5215" Y="1.74891015625" />
                  <Point X="-28.536443359375" Y="1.763215209961" />
                  <Point X="-28.5443046875" Y="1.769958984375" />
                  <Point X="-29.22761328125" Y="2.294281005859" />
                  <Point X="-29.00228515625" Y="2.680319091797" />
                  <Point X="-28.9548203125" Y="2.741330810547" />
                  <Point X="-28.726337890625" Y="3.035012939453" />
                  <Point X="-28.344494140625" Y="2.814555908203" />
                  <Point X="-28.25415625" Y="2.762398681641" />
                  <Point X="-28.24491796875" Y="2.757716796875" />
                  <Point X="-28.225986328125" Y="2.749386474609" />
                  <Point X="-28.21629296875" Y="2.745738037109" />
                  <Point X="-28.195650390625" Y="2.739229248047" />
                  <Point X="-28.185615234375" Y="2.736657226562" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.143240234375" Y="2.730122802734" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310791016" />
                  <Point X="-27.97643359375" Y="2.734227294922" />
                  <Point X="-27.956994140625" Y="2.741302978516" />
                  <Point X="-27.938443359375" Y="2.750451904297" />
                  <Point X="-27.929419921875" Y="2.755531738281" />
                  <Point X="-27.911166015625" Y="2.767160888672" />
                  <Point X="-27.902748046875" Y="2.77319140625" />
                  <Point X="-27.886619140625" Y="2.786135253906" />
                  <Point X="-27.870509765625" Y="2.801446289062" />
                  <Point X="-27.81818359375" Y="2.853771972656" />
                  <Point X="-27.811267578125" Y="2.861484863281" />
                  <Point X="-27.79831640625" Y="2.877620605469" />
                  <Point X="-27.79228125" Y="2.886043457031" />
                  <Point X="-27.78065234375" Y="2.904297851562" />
                  <Point X="-27.7755703125" Y="2.913325683594" />
                  <Point X="-27.766423828125" Y="2.931874511719" />
                  <Point X="-27.759353515625" Y="2.951299072266" />
                  <Point X="-27.754435546875" Y="2.971387939453" />
                  <Point X="-27.7525234375" Y="2.981573242188" />
                  <Point X="-27.749697265625" Y="3.003031982422" />
                  <Point X="-27.748908203125" Y="3.013364990234" />
                  <Point X="-27.74845703125" Y="3.034049804688" />
                  <Point X="-27.749830078125" Y="3.056232910156" />
                  <Point X="-27.756279296875" Y="3.129951171875" />
                  <Point X="-27.757744140625" Y="3.140207519531" />
                  <Point X="-27.76178125" Y="3.1605" />
                  <Point X="-27.764353515625" Y="3.170536132812" />
                  <Point X="-27.77086328125" Y="3.191177978516" />
                  <Point X="-27.774513671875" Y="3.200873535156" />
                  <Point X="-27.78284375" Y="3.21980078125" />
                  <Point X="-27.7875234375" Y="3.229032714844" />
                  <Point X="-28.059388671875" Y="3.699915283203" />
                  <Point X="-27.648373046875" Y="4.015037597656" />
                  <Point X="-27.573615234375" Y="4.0565703125" />
                  <Point X="-27.192525390625" Y="4.268295898437" />
                  <Point X="-27.146220703125" Y="4.207949707031" />
                  <Point X="-27.118564453125" Y="4.171908203125" />
                  <Point X="-27.111818359375" Y="4.164045410156" />
                  <Point X="-27.097513671875" Y="4.149103027344" />
                  <Point X="-27.089955078125" Y="4.142022949219" />
                  <Point X="-27.073375" Y="4.128110839844" />
                  <Point X="-27.06508984375" Y="4.121895996094" />
                  <Point X="-27.047892578125" Y="4.110405273438" />
                  <Point X="-27.0258125" Y="4.098274414062" />
                  <Point X="-26.943763671875" Y="4.0555625" />
                  <Point X="-26.934326171875" Y="4.051286621094" />
                  <Point X="-26.915044921875" Y="4.0437890625" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031378173828" />
                  <Point X="-26.780818359375" Y="4.035136474609" />
                  <Point X="-26.770728515625" Y="4.037488525391" />
                  <Point X="-26.7508671875" Y="4.043277587891" />
                  <Point X="-26.727380859375" Y="4.052395751953" />
                  <Point X="-26.641921875" Y="4.087794189453" />
                  <Point X="-26.632583984375" Y="4.092272705078" />
                  <Point X="-26.614453125" Y="4.102219238281" />
                  <Point X="-26.60566015625" Y="4.1076875" />
                  <Point X="-26.5879296875" Y="4.1201015625" />
                  <Point X="-26.579783203125" Y="4.126493164063" />
                  <Point X="-26.5642265625" Y="4.140133789063" />
                  <Point X="-26.550244140625" Y="4.155393554688" />
                  <Point X="-26.53801171875" Y="4.172078613281" />
                  <Point X="-26.5323515625" Y="4.180752929688" />
                  <Point X="-26.52153125" Y="4.199496582031" />
                  <Point X="-26.516849609375" Y="4.208737792969" />
                  <Point X="-26.50851953125" Y="4.227673339844" />
                  <Point X="-26.500408203125" Y="4.251525878906" />
                  <Point X="-26.472591796875" Y="4.339744140625" />
                  <Point X="-26.470021484375" Y="4.3497734375" />
                  <Point X="-26.46598828125" Y="4.370052734375" />
                  <Point X="-26.464525390625" Y="4.380302734375" />
                  <Point X="-26.462638671875" Y="4.401865234375" />
                  <Point X="-26.46230078125" Y="4.412218261719" />
                  <Point X="-26.462751953125" Y="4.432898925781" />
                  <Point X="-26.463541015625" Y="4.443226074219" />
                  <Point X="-26.479263671875" Y="4.562655761719" />
                  <Point X="-25.931173828125" Y="4.7163203125" />
                  <Point X="-25.840556640625" Y="4.72692578125" />
                  <Point X="-25.36522265625" Y="4.782556152344" />
                  <Point X="-25.252392578125" Y="4.361466308594" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.978369140625" Y="4.085113037109" />
                  <Point X="-24.94783203125" Y="4.090154541016" />
                  <Point X="-24.93276953125" Y="4.093927246094" />
                  <Point X="-24.90233203125" Y="4.104259277344" />
                  <Point X="-24.8880859375" Y="4.110435058594" />
                  <Point X="-24.86079296875" Y="4.1250234375" />
                  <Point X="-24.8362421875" Y="4.143860839844" />
                  <Point X="-24.815083984375" Y="4.166448730469" />
                  <Point X="-24.8054296875" Y="4.178611816406" />
                  <Point X="-24.7875703125" Y="4.205337890625" />
                  <Point X="-24.78002734375" Y="4.218911132812" />
                  <Point X="-24.767251953125" Y="4.24710546875" />
                  <Point X="-24.76201953125" Y="4.261727050781" />
                  <Point X="-24.62180859375" Y="4.785006347656" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-24.097138671875" Y="4.719809082031" />
                  <Point X="-23.546400390625" Y="4.58684375" />
                  <Point X="-23.49984765625" Y="4.569958496094" />
                  <Point X="-23.14174609375" Y="4.440073242188" />
                  <Point X="-23.094541015625" Y="4.417996582031" />
                  <Point X="-22.74955859375" Y="4.256660644531" />
                  <Point X="-22.70389453125" Y="4.230055175781" />
                  <Point X="-22.37055078125" Y="4.035848144531" />
                  <Point X="-22.327552734375" Y="4.005271240234" />
                  <Point X="-22.18221484375" Y="3.901916259766" />
                  <Point X="-22.815529296875" Y="2.804987304688" />
                  <Point X="-22.9346875" Y="2.59859765625" />
                  <Point X="-22.938869140625" Y="2.590478759766" />
                  <Point X="-22.94982421875" Y="2.565368164062" />
                  <Point X="-22.957794921875" Y="2.542182861328" />
                  <Point X="-22.961666015625" Y="2.529499755859" />
                  <Point X="-22.98016796875" Y="2.460316650391" />
                  <Point X="-22.98254296875" Y="2.448444335938" />
                  <Point X="-22.985765625" Y="2.424499511719" />
                  <Point X="-22.98661328125" Y="2.412427001953" />
                  <Point X="-22.98679296875" Y="2.383923095703" />
                  <Point X="-22.9854296875" Y="2.359982177734" />
                  <Point X="-22.978216796875" Y="2.300158447266" />
                  <Point X="-22.97619921875" Y="2.289038818359" />
                  <Point X="-22.970857421875" Y="2.2671171875" />
                  <Point X="-22.967533203125" Y="2.256315185547" />
                  <Point X="-22.959267578125" Y="2.234225585938" />
                  <Point X="-22.95468359375" Y="2.223893554688" />
                  <Point X="-22.944318359375" Y="2.203842529297" />
                  <Point X="-22.932595703125" Y="2.185368164062" />
                  <Point X="-22.895578125" Y="2.130814941406" />
                  <Point X="-22.8880859375" Y="2.121172851563" />
                  <Point X="-22.8719296875" Y="2.102930908203" />
                  <Point X="-22.863265625" Y="2.094331054688" />
                  <Point X="-22.8412109375" Y="2.075090087891" />
                  <Point X="-22.822984375" Y="2.061038330078" />
                  <Point X="-22.7684296875" Y="2.024021850586" />
                  <Point X="-22.75871484375" Y="2.018242797852" />
                  <Point X="-22.738669921875" Y="2.007880737305" />
                  <Point X="-22.72833984375" Y="2.003297485352" />
                  <Point X="-22.706251953125" Y="1.995031860352" />
                  <Point X="-22.695451171875" Y="1.991707763672" />
                  <Point X="-22.673529296875" Y="1.986364990234" />
                  <Point X="-22.652806640625" Y="1.983188720703" />
                  <Point X="-22.592984375" Y="1.975974853516" />
                  <Point X="-22.5805390625" Y="1.975297729492" />
                  <Point X="-22.55566796875" Y="1.975578125" />
                  <Point X="-22.5432421875" Y="1.976536010742" />
                  <Point X="-22.513236328125" Y="1.980842895508" />
                  <Point X="-22.491150390625" Y="1.985364868164" />
                  <Point X="-22.421966796875" Y="2.003865478516" />
                  <Point X="-22.416005859375" Y="2.005670532227" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572753906" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.077970703125" Y="2.770341308594" />
                  <Point X="-21.05959375" Y="2.780951416016" />
                  <Point X="-20.95605078125" Y="2.637051025391" />
                  <Point X="-20.93207421875" Y="2.597426513672" />
                  <Point X="-20.863115234375" Y="2.483470947266" />
                  <Point X="-21.670162109375" Y="1.864201171875" />
                  <Point X="-21.827046875" Y="1.743819824219" />
                  <Point X="-21.834173828125" Y="1.737772583008" />
                  <Point X="-21.854296875" Y="1.71828503418" />
                  <Point X="-21.871533203125" Y="1.698610351562" />
                  <Point X="-21.879416015625" Y="1.688997070313" />
                  <Point X="-21.92920703125" Y="1.624040771484" />
                  <Point X="-21.93608203125" Y="1.613744262695" />
                  <Point X="-21.948435546875" Y="1.592346191406" />
                  <Point X="-21.9539140625" Y="1.581244506836" />
                  <Point X="-21.965359375" Y="1.553650024414" />
                  <Point X="-21.97283203125" Y="1.532033203125" />
                  <Point X="-21.991380859375" Y="1.465712646484" />
                  <Point X="-21.9937734375" Y="1.454664672852" />
                  <Point X="-21.997228515625" Y="1.432356201172" />
                  <Point X="-21.998291015625" Y="1.421095581055" />
                  <Point X="-21.99910546875" Y="1.397517456055" />
                  <Point X="-21.998822265625" Y="1.386213500977" />
                  <Point X="-21.996916015625" Y="1.363726196289" />
                  <Point X="-21.992857421875" Y="1.340730834961" />
                  <Point X="-21.9776328125" Y="1.266940551758" />
                  <Point X="-21.97430859375" Y="1.254895874023" />
                  <Point X="-21.966109375" Y="1.231349731445" />
                  <Point X="-21.961234375" Y="1.219848510742" />
                  <Point X="-21.947390625" Y="1.192369140625" />
                  <Point X="-21.9364375" Y="1.173426635742" />
                  <Point X="-21.89502734375" Y="1.110483154297" />
                  <Point X="-21.888263671875" Y="1.10142590332" />
                  <Point X="-21.873708984375" Y="1.0841796875" />
                  <Point X="-21.86591796875" Y="1.075990600586" />
                  <Point X="-21.848673828125" Y="1.059900756836" />
                  <Point X="-21.839962890625" Y="1.052693237305" />
                  <Point X="-21.821748046875" Y="1.039364990234" />
                  <Point X="-21.802611328125" Y="1.027822631836" />
                  <Point X="-21.7426015625" Y="0.994041687012" />
                  <Point X="-21.73109765625" Y="0.988548339844" />
                  <Point X="-21.70745703125" Y="0.979151367188" />
                  <Point X="-21.6953203125" Y="0.975247924805" />
                  <Point X="-21.664322265625" Y="0.967528869629" />
                  <Point X="-21.643306640625" Y="0.963536560059" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.555966796875" Y="0.952199768066" />
                  <Point X="-21.53428125" Y="0.951222900391" />
                  <Point X="-21.50878125" Y="0.952032287598" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-20.295296875" Y="1.111319702148" />
                  <Point X="-20.247314453125" Y="0.914221435547" />
                  <Point X="-20.239759765625" Y="0.86570111084" />
                  <Point X="-20.21612890625" Y="0.713921142578" />
                  <Point X="-21.12821484375" Y="0.469527923584" />
                  <Point X="-21.3080078125" Y="0.421352874756" />
                  <Point X="-21.317080078125" Y="0.418427093506" />
                  <Point X="-21.3436171875" Y="0.407860900879" />
                  <Point X="-21.368490234375" Y="0.395458465576" />
                  <Point X="-21.378787109375" Y="0.389921966553" />
                  <Point X="-21.45850390625" Y="0.343844573975" />
                  <Point X="-21.46899609375" Y="0.336809204102" />
                  <Point X="-21.488953125" Y="0.321410552979" />
                  <Point X="-21.49841796875" Y="0.313047393799" />
                  <Point X="-21.52040625" Y="0.290845947266" />
                  <Point X="-21.53487890625" Y="0.274446014404" />
                  <Point X="-21.582708984375" Y="0.213499511719" />
                  <Point X="-21.589146484375" Y="0.204207168579" />
                  <Point X="-21.60087109375" Y="0.184925582886" />
                  <Point X="-21.606158203125" Y="0.174936203003" />
                  <Point X="-21.615931640625" Y="0.15347253418" />
                  <Point X="-21.619994140625" Y="0.142925506592" />
                  <Point X="-21.62683984375" Y="0.121424690247" />
                  <Point X="-21.632181640625" Y="0.097109840393" />
                  <Point X="-21.648125" Y="0.013860244751" />
                  <Point X="-21.649677734375" Y="0.00120783329" />
                  <Point X="-21.65107421875" Y="-0.024191810608" />
                  <Point X="-21.65091796875" Y="-0.036939044952" />
                  <Point X="-21.648359375" Y="-0.069109199524" />
                  <Point X="-21.64556640625" Y="-0.089783233643" />
                  <Point X="-21.629623046875" Y="-0.173033126831" />
                  <Point X="-21.62683984375" Y="-0.183987228394" />
                  <Point X="-21.619994140625" Y="-0.205487014771" />
                  <Point X="-21.615931640625" Y="-0.216032684326" />
                  <Point X="-21.606158203125" Y="-0.237496353149" />
                  <Point X="-21.60087109375" Y="-0.247484558105" />
                  <Point X="-21.5891484375" Y="-0.26676361084" />
                  <Point X="-21.575037109375" Y="-0.2858359375" />
                  <Point X="-21.52720703125" Y="-0.346782287598" />
                  <Point X="-21.51848828125" Y="-0.356447692871" />
                  <Point X="-21.49981640625" Y="-0.374492126465" />
                  <Point X="-21.48986328125" Y="-0.382870910645" />
                  <Point X="-21.4627578125" Y="-0.402686187744" />
                  <Point X="-21.4457109375" Y="-0.413799743652" />
                  <Point X="-21.365994140625" Y="-0.45987701416" />
                  <Point X="-21.36050390625" Y="-0.462814544678" />
                  <Point X="-21.34084375" Y="-0.472015930176" />
                  <Point X="-21.31697265625" Y="-0.481027526855" />
                  <Point X="-21.3080078125" Y="-0.483912872314" />
                  <Point X="-20.21512109375" Y="-0.776751037598" />
                  <Point X="-20.2383828125" Y="-0.931036621094" />
                  <Point X="-20.24806640625" Y="-0.973471191406" />
                  <Point X="-20.272197265625" Y="-1.079219604492" />
                  <Point X="-21.354708984375" Y="-0.936704406738" />
                  <Point X="-21.563216796875" Y="-0.909253601074" />
                  <Point X="-21.571375" Y="-0.908535644531" />
                  <Point X="-21.59990234375" Y="-0.908042358398" />
                  <Point X="-21.63327734375" Y="-0.910840942383" />
                  <Point X="-21.645517578125" Y="-0.912676269531" />
                  <Point X="-21.670626953125" Y="-0.918133850098" />
                  <Point X="-21.82708203125" Y="-0.952139953613" />
                  <Point X="-21.842123046875" Y="-0.956741882324" />
                  <Point X="-21.8712421875" Y="-0.968365539551" />
                  <Point X="-21.8853203125" Y="-0.975387084961" />
                  <Point X="-21.9131484375" Y="-0.992279541016" />
                  <Point X="-21.925875" Y="-1.001528930664" />
                  <Point X="-21.949625" Y="-1.021999328613" />
                  <Point X="-21.9606484375" Y="-1.033220092773" />
                  <Point X="-21.975826171875" Y="-1.051473388672" />
                  <Point X="-22.070392578125" Y="-1.165208129883" />
                  <Point X="-22.07867578125" Y="-1.176849609375" />
                  <Point X="-22.0933984375" Y="-1.201237670898" />
                  <Point X="-22.099837890625" Y="-1.21398425293" />
                  <Point X="-22.1111796875" Y="-1.241369628906" />
                  <Point X="-22.11563671875" Y="-1.254933105469" />
                  <Point X="-22.122466796875" Y="-1.282576538086" />
                  <Point X="-22.12483984375" Y="-1.296656494141" />
                  <Point X="-22.127015625" Y="-1.320295532227" />
                  <Point X="-22.140568359375" Y="-1.467587036133" />
                  <Point X="-22.140708984375" Y="-1.483312744141" />
                  <Point X="-22.138392578125" Y="-1.514580444336" />
                  <Point X="-22.135935546875" Y="-1.530122314453" />
                  <Point X="-22.128205078125" Y="-1.561743286133" />
                  <Point X="-22.12321484375" Y="-1.576665649414" />
                  <Point X="-22.11084375" Y="-1.605476318359" />
                  <Point X="-22.103462890625" Y="-1.619364624023" />
                  <Point X="-22.089568359375" Y="-1.640978759766" />
                  <Point X="-22.002982421875" Y="-1.775655151367" />
                  <Point X="-21.998259765625" Y="-1.782348754883" />
                  <Point X="-21.98019921875" Y="-1.804457397461" />
                  <Point X="-21.956505859375" Y="-1.828123291016" />
                  <Point X="-21.947201171875" Y="-1.836277709961" />
                  <Point X="-20.912828125" Y="-2.629980224609" />
                  <Point X="-20.954509765625" Y="-2.697428955078" />
                  <Point X="-20.974533203125" Y="-2.725878173828" />
                  <Point X="-20.998724609375" Y="-2.760251464844" />
                  <Point X="-21.96583984375" Y="-2.201887695312" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.18497265625" Y="-2.079513671875" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228892578125" Y="-2.066337158203" />
                  <Point X="-22.25877734375" Y="-2.060940185547" />
                  <Point X="-22.444982421875" Y="-2.027311523438" />
                  <Point X="-22.460640625" Y="-2.025807250977" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.55515625" Y="-2.03513659668" />
                  <Point X="-22.584931640625" Y="-2.044960571289" />
                  <Point X="-22.59941015625" Y="-2.051109130859" />
                  <Point X="-22.624236328125" Y="-2.064175048828" />
                  <Point X="-22.778927734375" Y="-2.145588134766" />
                  <Point X="-22.79103125" Y="-2.153170654297" />
                  <Point X="-22.813962890625" Y="-2.170064453125" />
                  <Point X="-22.824791015625" Y="-2.179375732422" />
                  <Point X="-22.84575" Y="-2.200335449219" />
                  <Point X="-22.85505859375" Y="-2.21116015625" />
                  <Point X="-22.871951171875" Y="-2.234088623047" />
                  <Point X="-22.87953515625" Y="-2.246192382812" />
                  <Point X="-22.8926015625" Y="-2.271018798828" />
                  <Point X="-22.974015625" Y="-2.425710449219" />
                  <Point X="-22.9801640625" Y="-2.440193359375" />
                  <Point X="-22.98998828125" Y="-2.46997265625" />
                  <Point X="-22.9936640625" Y="-2.485269042969" />
                  <Point X="-22.99862109375" Y="-2.517441894531" />
                  <Point X="-22.999720703125" Y="-2.533138916016" />
                  <Point X="-22.999314453125" Y="-2.564493164062" />
                  <Point X="-22.99780859375" Y="-2.580150390625" />
                  <Point X="-22.99241015625" Y="-2.610034667969" />
                  <Point X="-22.95878125" Y="-2.796240722656" />
                  <Point X="-22.95698046875" Y="-2.804228271484" />
                  <Point X="-22.94876171875" Y="-2.831539794922" />
                  <Point X="-22.935927734375" Y="-2.862478759766" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.264103515625" Y="-4.027722167969" />
                  <Point X="-22.276244140625" Y="-4.036082275391" />
                  <Point X="-23.024373046875" Y="-3.061102783203" />
                  <Point X="-23.16608203125" Y="-2.876422607422" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849626220703" />
                  <Point X="-23.21674609375" Y="-2.828004882812" />
                  <Point X="-23.22669921875" Y="-2.820646484375" />
                  <Point X="-23.256173828125" Y="-2.801697265625" />
                  <Point X="-23.43982421875" Y="-2.683627685547" />
                  <Point X="-23.453716796875" Y="-2.676244628906" />
                  <Point X="-23.482529296875" Y="-2.663873291016" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.62383984375" Y="-2.649482666016" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920125" Y="-2.699414794922" />
                  <Point X="-23.944505859375" Y="-2.714135742188" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-23.981029296875" Y="-2.743109130859" />
                  <Point X="-24.136123046875" Y="-2.872063720703" />
                  <Point X="-24.147345703125" Y="-2.883087646484" />
                  <Point X="-24.16781640625" Y="-2.906837158203" />
                  <Point X="-24.177064453125" Y="-2.919562744141" />
                  <Point X="-24.19395703125" Y="-2.947389404297" />
                  <Point X="-24.20098046875" Y="-2.961468994141" />
                  <Point X="-24.21260546875" Y="-2.990592041016" />
                  <Point X="-24.21720703125" Y="-3.005635498047" />
                  <Point X="-24.2246484375" Y="-3.039875732422" />
                  <Point X="-24.271021484375" Y="-3.253223388672" />
                  <Point X="-24.2724140625" Y="-3.261287597656" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323169677734" />
                  <Point X="-24.2744453125" Y="-3.335520019531" />
                  <Point X="-24.16691015625" Y="-4.152326660156" />
                  <Point X="-24.289533203125" Y="-3.694687011719" />
                  <Point X="-24.344931640625" Y="-3.487937011719" />
                  <Point X="-24.347392578125" Y="-3.480122314453" />
                  <Point X="-24.357853515625" Y="-3.453577880859" />
                  <Point X="-24.3732109375" Y="-3.423815185547" />
                  <Point X="-24.37958984375" Y="-3.413210693359" />
                  <Point X="-24.402232421875" Y="-3.380586669922" />
                  <Point X="-24.543318359375" Y="-3.177310302734" />
                  <Point X="-24.553328125" Y="-3.165173339844" />
                  <Point X="-24.575212890625" Y="-3.142717041016" />
                  <Point X="-24.587087890625" Y="-3.132397705078" />
                  <Point X="-24.61334375" Y="-3.113153808594" />
                  <Point X="-24.626759765625" Y="-3.104936523438" />
                  <Point X="-24.654759765625" Y="-3.090829345703" />
                  <Point X="-24.66934375" Y="-3.084939453125" />
                  <Point X="-24.704380859375" Y="-3.074064697266" />
                  <Point X="-24.922703125" Y="-3.006306152344" />
                  <Point X="-24.93662109375" Y="-3.003109863281" />
                  <Point X="-24.96478125" Y="-2.998840087891" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.998840087891" />
                  <Point X="-25.051064453125" Y="-3.003109619141" />
                  <Point X="-25.064984375" Y="-3.006305664062" />
                  <Point X="-25.100021484375" Y="-3.017180175781" />
                  <Point X="-25.31834375" Y="-3.084938964844" />
                  <Point X="-25.3329296875" Y="-3.090830078125" />
                  <Point X="-25.3609296875" Y="-3.104937988281" />
                  <Point X="-25.37434375" Y="-3.113154785156" />
                  <Point X="-25.400599609375" Y="-3.132398925781" />
                  <Point X="-25.412474609375" Y="-3.142716552734" />
                  <Point X="-25.434359375" Y="-3.165172119141" />
                  <Point X="-25.444369140625" Y="-3.177310058594" />
                  <Point X="-25.46701171875" Y="-3.209933837891" />
                  <Point X="-25.608095703125" Y="-3.413210449219" />
                  <Point X="-25.61246875" Y="-3.420131835938" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213623047" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.337227947303" Y="-3.956606719281" />
                  <Point X="-22.310479591597" Y="-3.947396521801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.39821175398" Y="-3.87713116317" />
                  <Point X="-22.358868671628" Y="-3.863584253525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.459195560658" Y="-3.79765560706" />
                  <Point X="-22.407257751659" Y="-3.779771985248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.520179367336" Y="-3.71818005095" />
                  <Point X="-22.45564683169" Y="-3.695959716972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.581163174013" Y="-3.63870449484" />
                  <Point X="-22.504035911721" Y="-3.612147448696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.086830492258" Y="-4.745328590806" />
                  <Point X="-25.968781260015" Y="-4.704680980417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.183611030482" Y="-4.089997575967" />
                  <Point X="-24.175484371835" Y="-4.08719934299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.642146980691" Y="-3.55922893873" />
                  <Point X="-22.552424991752" Y="-3.528335180419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.131903648863" Y="-4.660374558531" />
                  <Point X="-25.939122836872" Y="-4.59399480165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.208258673231" Y="-3.998010475255" />
                  <Point X="-24.188138452073" Y="-3.991082527524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.703130787369" Y="-3.47975338262" />
                  <Point X="-22.600814071783" Y="-3.444522912143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.119789650064" Y="-4.555729409524" />
                  <Point X="-25.909464413729" Y="-4.483308622882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.23290631598" Y="-3.906023374543" />
                  <Point X="-24.20079253231" Y="-3.894965712058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.764114594046" Y="-3.40027782651" />
                  <Point X="-22.649203151814" Y="-3.360710643866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.131006507186" Y="-4.459117718453" />
                  <Point X="-25.879805990586" Y="-4.372622444115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.257553958729" Y="-3.814036273831" />
                  <Point X="-24.213446612548" Y="-3.798848896592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.825098400724" Y="-3.3208022704" />
                  <Point X="-22.697592231845" Y="-3.27689837559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.072850462259" Y="-2.717454919856" />
                  <Point X="-20.938242672977" Y="-2.671105741043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.149711455072" Y="-4.365084383802" />
                  <Point X="-25.850147567444" Y="-4.261936265347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.282201601478" Y="-3.72204917312" />
                  <Point X="-24.226100692786" Y="-3.702732081126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.886082207402" Y="-3.24132671429" />
                  <Point X="-22.745981311876" Y="-3.193086107314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.181862536602" Y="-2.654516822522" />
                  <Point X="-20.974087592972" Y="-2.582974172081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.168416385996" Y="-4.271051043312" />
                  <Point X="-25.820489144301" Y="-4.15125008658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.306849373369" Y="-3.630062116875" />
                  <Point X="-24.238754773024" Y="-3.60661526566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.947066014079" Y="-3.16185115818" />
                  <Point X="-22.794370391908" Y="-3.109273839037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.290874610945" Y="-2.591578725188" />
                  <Point X="-21.064469938896" Y="-2.513621344823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.188494569991" Y="-4.177490551773" />
                  <Point X="-25.790830721158" Y="-4.040563907812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.331497199938" Y="-3.538075079457" />
                  <Point X="-24.251408853262" Y="-3.510498450194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.008049820757" Y="-3.08237560207" />
                  <Point X="-22.842759471939" Y="-3.025461570761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.399886685288" Y="-2.528640627853" />
                  <Point X="-21.15485228482" Y="-2.444268517566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.232721334491" Y="-4.092245083324" />
                  <Point X="-25.761172298015" Y="-3.929877729045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.360877427044" Y="-3.447717538222" />
                  <Point X="-24.2640629335" Y="-3.414381634728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.0690333144" Y="-3.002899938173" />
                  <Point X="-22.89114855197" Y="-2.941649302485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.508898759632" Y="-2.465702530519" />
                  <Point X="-21.245234630744" Y="-2.374915690309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.3087175513" Y="-4.017938714564" />
                  <Point X="-25.731513874872" Y="-3.819191550278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.412933869034" Y="-3.365168043936" />
                  <Point X="-24.275260649949" Y="-3.317763352994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.13001669363" Y="-2.923424234881" />
                  <Point X="-22.938063720186" Y="-2.857329525671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.617910833975" Y="-2.402764433184" />
                  <Point X="-21.335616976668" Y="-2.305562863051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.390985096184" Y="-3.945791737233" />
                  <Point X="-25.701855451729" Y="-3.70850537151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.469217836814" Y="-3.284074203515" />
                  <Point X="-24.262236111714" Y="-3.212804680117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.195892010436" Y="-2.845632960779" />
                  <Point X="-22.964264890688" Y="-2.765877347463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.726922908318" Y="-2.33982633585" />
                  <Point X="-21.425999322591" Y="-2.236210035794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.473252641069" Y="-3.873644759901" />
                  <Point X="-25.672197028586" Y="-3.597819192743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.525501804594" Y="-3.202980363095" />
                  <Point X="-24.238630474506" Y="-3.104202642685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.292418183374" Y="-2.778395622814" />
                  <Point X="-22.981348200535" Y="-2.671285638057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.835934982661" Y="-2.276888238516" />
                  <Point X="-21.516381668515" Y="-2.166857208537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.539182391282" Y="-4.140199842014" />
                  <Point X="-27.467047232037" Y="-4.115361714797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.578773805134" Y="-3.809504645762" />
                  <Point X="-25.642473772749" Y="-3.487110690288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.595150454668" Y="-3.126488351831" />
                  <Point X="-24.214033052356" Y="-2.99525910631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.394191625576" Y="-2.712965064551" />
                  <Point X="-22.998150336079" Y="-2.576597112574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.944947057004" Y="-2.213950141181" />
                  <Point X="-21.606764014439" Y="-2.09750438128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.643462293884" Y="-4.075632327278" />
                  <Point X="-27.315042864015" Y="-3.962548448833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.800656383406" Y="-3.785430979657" />
                  <Point X="-25.573064866153" Y="-3.362737322426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.720326269038" Y="-3.069115876522" />
                  <Point X="-24.127695919143" Y="-2.865056882579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.515857693328" Y="-2.654384086565" />
                  <Point X="-22.990863316525" Y="-2.47361402581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.053959000228" Y="-2.151011998699" />
                  <Point X="-21.697146360363" Y="-2.028151554022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.747722371759" Y="-4.011057986341" />
                  <Point X="-25.481432698252" Y="-3.23071187204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.873794778205" Y="-3.021485357286" />
                  <Point X="-22.936495359647" Y="-2.354419672266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.164257096388" Y="-2.088516714187" />
                  <Point X="-21.787528706286" Y="-1.958798726765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.837890385954" Y="-3.941631358752" />
                  <Point X="-22.869614249306" Y="-2.230916694455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.335647932282" Y="-2.047057346937" />
                  <Point X="-21.87791105221" Y="-1.889445899508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.928058567514" Y="-3.87220478879" />
                  <Point X="-21.965506215484" Y="-1.819133368301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.950861658384" Y="-3.779582557932" />
                  <Point X="-22.026191795363" Y="-1.739555124469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.878459605384" Y="-3.654178567113" />
                  <Point X="-22.079080210842" Y="-1.657292101629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.364821324357" Y="-1.067025430685" />
                  <Point X="-20.261279085742" Y="-1.031372978788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.806057552384" Y="-3.528774576293" />
                  <Point X="-22.12460951864" Y="-1.572495134805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.575909973411" Y="-1.039235116693" />
                  <Point X="-20.237107194857" Y="-0.922575964578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.733655499384" Y="-3.403370585474" />
                  <Point X="-22.140657424163" Y="-1.477546907099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.786998622465" Y="-1.011444802702" />
                  <Point X="-20.221129192964" Y="-0.816600332608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.661253446384" Y="-3.277966594654" />
                  <Point X="-22.131964469746" Y="-1.37407971814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.998087271519" Y="-0.98365448871" />
                  <Point X="-20.317515008964" Y="-0.749314665874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.588851393384" Y="-3.152562603835" />
                  <Point X="-22.119161055805" Y="-1.269197184463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.209175920573" Y="-0.955864174719" />
                  <Point X="-20.481613919833" Y="-0.705344487484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.516449340384" Y="-3.027158613015" />
                  <Point X="-22.054924596001" Y="-1.146604832859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.420264394692" Y="-0.928073800493" />
                  <Point X="-20.645712830701" Y="-0.661374309094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.444047290894" Y="-2.901754623404" />
                  <Point X="-21.926196876515" Y="-1.001806359732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.704706985176" Y="-0.925541274079" />
                  <Point X="-20.809811741569" Y="-0.617404130704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.371645882829" Y="-2.776350854654" />
                  <Point X="-20.973910652437" Y="-0.573433952314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.317933128934" Y="-2.657382105589" />
                  <Point X="-21.138009563305" Y="-0.529463773924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.315246158561" Y="-2.555982942781" />
                  <Point X="-21.302108474173" Y="-0.485493595533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.8252554935" Y="-2.975446888413" />
                  <Point X="-28.564192317682" Y="-2.885555628165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.353884569694" Y="-2.468813249955" />
                  <Point X="-21.423304679721" Y="-0.426750831017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.88585054052" Y="-2.895837471617" />
                  <Point X="-28.133015084516" Y="-2.636615435852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.42744839637" Y="-2.393669342106" />
                  <Point X="-21.516501962499" Y="-0.358367264248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.946445587539" Y="-2.816228054822" />
                  <Point X="-27.70183785135" Y="-2.387675243538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.593717006179" Y="-2.350446250974" />
                  <Point X="-21.579629019813" Y="-0.279629688514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.00649482351" Y="-2.736430700211" />
                  <Point X="-21.623561611015" Y="-0.194282928076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.056205736855" Y="-2.653073575645" />
                  <Point X="-21.643481833311" Y="-0.100668045962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.105916667493" Y="-2.569716457033" />
                  <Point X="-21.649875859487" Y="-0.002395721022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.155627598132" Y="-2.486359338422" />
                  <Point X="-21.630503437582" Y="0.104748703489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.066229696009" Y="-2.355103207438" />
                  <Point X="-21.573912244128" Y="0.224708578777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.828701970702" Y="-2.17284188798" />
                  <Point X="-21.36553143579" Y="0.39693380988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.247624507599" Y="0.437532421054" />
                  <Point X="-20.227770755827" Y="0.788696229307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.591174245396" Y="-1.990580568523" />
                  <Point X="-20.242617944807" Y="0.884057896873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.353646520089" Y="-1.808319249065" />
                  <Point X="-20.262744053751" Y="0.977601886529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.116120313476" Y="-1.626058452535" />
                  <Point X="-20.285312097613" Y="1.070305050562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.974111583187" Y="-1.476686960656" />
                  <Point X="-20.558709346889" Y="1.076640792951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.947648761165" Y="-1.367101115595" />
                  <Point X="-21.031138682423" Y="1.014444292112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.963318166337" Y="-1.272022559767" />
                  <Point X="-21.502771549953" Y="0.952522037199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.013371935248" Y="-1.18878348984" />
                  <Point X="-21.711994210981" Y="0.980954862394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.111577780725" Y="-1.122124509511" />
                  <Point X="-21.825718635786" Y="1.042270367341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.39872504362" Y="-1.120523276494" />
                  <Point X="-21.89953098942" Y="1.117328700495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.871154432381" Y="-1.182719795661" />
                  <Point X="-21.95123476524" Y="1.199999627482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.343583821143" Y="-1.244916314828" />
                  <Point X="-21.9823415338" Y="1.289762672819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.668662343294" Y="-1.25637586178" />
                  <Point X="-21.998686227261" Y="1.384608708243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.692252129489" Y="-1.164024511846" />
                  <Point X="-21.984607543701" Y="1.489930352464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.715841915684" Y="-1.071673161911" />
                  <Point X="-21.940860235638" Y="1.60546772335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.739431701878" Y="-0.979321811977" />
                  <Point X="-21.823947679228" Y="1.746197909575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.753685224764" Y="-0.88375572878" />
                  <Point X="-21.586419138341" Y="1.928459509859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.767380538892" Y="-0.787997438895" />
                  <Point X="-21.348892329306" Y="2.110720513819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.781075853021" Y="-0.692239149009" />
                  <Point X="-29.270134446933" Y="-0.51630791412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.348044276724" Y="-0.198806806574" />
                  <Point X="-21.111365520271" Y="2.292981517779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.300087654507" Y="-0.081820052592" />
                  <Point X="-20.873838711236" Y="2.475242521739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.30028820649" Y="0.018584856535" />
                  <Point X="-22.609745059863" Y="1.977995996535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.214827459041" Y="2.113977031472" />
                  <Point X="-20.911160112764" Y="2.562865697339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.330347342975" Y="0.108708630524" />
                  <Point X="-22.768225908467" Y="2.023900628896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.783650201367" Y="2.362917232225" />
                  <Point X="-20.96229701057" Y="2.64573181608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.395175368586" Y="0.186860515904" />
                  <Point X="-22.86106199774" Y="2.092408564562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.352472943693" Y="2.611857432977" />
                  <Point X="-21.020237379966" Y="2.726255311685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.506495827405" Y="0.249003772721" />
                  <Point X="-22.923180797404" Y="2.171493311246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.670386383716" Y="0.293045693339" />
                  <Point X="-22.967639032881" Y="2.256659077846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.834485264115" Y="0.33701588222" />
                  <Point X="-22.98439071943" Y="2.351364974311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.998584144514" Y="0.380986071102" />
                  <Point X="-22.981676968403" Y="2.452773358438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.162683024913" Y="0.424956259983" />
                  <Point X="-22.95027384449" Y="2.564060285857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.326781905312" Y="0.468926448865" />
                  <Point X="-22.883306418154" Y="2.687592984648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.490880785711" Y="0.512896637746" />
                  <Point X="-22.810905026521" Y="2.812996747741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.654979666109" Y="0.556866826628" />
                  <Point X="-22.73850268733" Y="2.938400837104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.782234499948" Y="0.613523438125" />
                  <Point X="-22.666100348138" Y="3.063804926467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.766568722913" Y="0.719391562455" />
                  <Point X="-22.593698008946" Y="3.18920901583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.750902945878" Y="0.825259686784" />
                  <Point X="-22.521295669755" Y="3.314613105193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.735237168844" Y="0.931127811114" />
                  <Point X="-28.893832371457" Y="1.220846716808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.465027838735" Y="1.368495958128" />
                  <Point X="-22.448893330563" Y="3.440017194557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.708789023085" Y="1.040708602731" />
                  <Point X="-29.104920985857" Y="1.248637042732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.425170679347" Y="1.482693843405" />
                  <Point X="-22.376490991372" Y="3.56542128392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.678761469562" Y="1.151521883282" />
                  <Point X="-29.316009600209" Y="1.276427368672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.429318827376" Y="1.581739486208" />
                  <Point X="-22.30408865218" Y="3.690825373283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.648733916039" Y="1.262335163832" />
                  <Point X="-29.527098214561" Y="1.304217694613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.466596045036" Y="1.669377875534" />
                  <Point X="-22.231686312988" Y="3.816229462646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.523064779113" Y="1.750408095816" />
                  <Point X="-22.212364216458" Y="3.923356558741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.610520850546" Y="1.820768520185" />
                  <Point X="-22.307558385441" Y="3.991052542449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.7009031024" Y="1.890121379833" />
                  <Point X="-22.40721840768" Y="4.057210809564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.791285354255" Y="1.959474239481" />
                  <Point X="-22.515612688197" Y="4.120361630372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.881667606109" Y="2.028827099129" />
                  <Point X="-22.624006968714" Y="4.18351245118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.972049857963" Y="2.098179958777" />
                  <Point X="-22.732400269965" Y="4.246663609176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.062432109818" Y="2.167532818425" />
                  <Point X="-22.853708463898" Y="4.305367812999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.152814361672" Y="2.236885678073" />
                  <Point X="-22.977446117139" Y="4.363235486898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.214958361834" Y="2.315961747529" />
                  <Point X="-28.028283959642" Y="2.724566512188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.918911314735" Y="2.762226533968" />
                  <Point X="-23.10118367787" Y="4.421103192649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.141560702491" Y="2.441708553105" />
                  <Point X="-28.235649908883" Y="2.753638654521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.776456371236" Y="2.911751669377" />
                  <Point X="-23.237268063938" Y="4.474719545501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.068163043148" Y="2.56745535868" />
                  <Point X="-28.346749418575" Y="2.815857990224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.748724779519" Y="3.021774386878" />
                  <Point X="-23.379374597975" Y="4.526262306516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.991346876533" Y="2.694379250705" />
                  <Point X="-28.455761608916" Y="2.878796047617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.755404210953" Y="3.119948438907" />
                  <Point X="-23.52148090794" Y="4.577805144685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.884578818417" Y="2.831616406045" />
                  <Point X="-28.564773799258" Y="2.94173410501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.779470304476" Y="3.212135783076" />
                  <Point X="-25.127314152237" Y="4.125346381048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.766345754499" Y="4.249637767914" />
                  <Point X="-23.687849243016" Y="4.620993897654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.777808867284" Y="2.968854213202" />
                  <Point X="-28.673785989599" Y="3.004672162403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.826439454961" Y="3.296436972304" />
                  <Point X="-25.197625051768" Y="4.201610361537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.735490391349" Y="4.360736086177" />
                  <Point X="-23.859377156703" Y="4.662406065235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.874828654875" Y="3.380249199302" />
                  <Point X="-25.233199287915" Y="4.289835134423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.705832443817" Y="4.471422101179" />
                  <Point X="-24.03090507039" Y="4.703818232815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.923217854788" Y="3.464061426299" />
                  <Point X="-25.257846905277" Y="4.381822243876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.676174496285" Y="4.582108116181" />
                  <Point X="-24.211660890405" Y="4.742052977434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.971607054702" Y="3.547873653297" />
                  <Point X="-25.282494634398" Y="4.473809314847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.646516548752" Y="4.692794131182" />
                  <Point X="-24.435406629908" Y="4.76548510568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.019996254615" Y="3.631685880294" />
                  <Point X="-26.860902075169" Y="4.030794012681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.57772156695" Y="4.128300881206" />
                  <Point X="-25.307142363518" Y="4.565796385819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.015159864541" Y="3.733825147658" />
                  <Point X="-26.99830666437" Y="4.083955783139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.49903928574" Y="4.255867328016" />
                  <Point X="-25.331790092639" Y="4.657783456791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.777275202616" Y="3.916209370249" />
                  <Point X="-27.098353906913" Y="4.14998071961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.466485972928" Y="4.367550297234" />
                  <Point X="-25.35643782176" Y="4.749770527762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.430474179479" Y="4.136096503546" />
                  <Point X="-27.162015559342" Y="4.228534219484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.466791780372" Y="4.467918963999" />
                  <Point X="-25.649716171239" Y="4.749260658369" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.473060546875" Y="-3.743862304688" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.558322265625" Y="-3.488920166016" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.760701171875" Y="-3.255525146484" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.043701171875" Y="-3.198641113281" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285644042969" />
                  <Point X="-25.310921875" Y="-3.318267822266" />
                  <Point X="-25.452005859375" Y="-3.521544433594" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.80441015625" Y="-4.825352050781" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.1396796875" Y="-4.927919433594" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.315818359375" Y="-4.601693359375" />
                  <Point X="-26.3091484375" Y="-4.551039550781" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.318052734375" Y="-4.492675292969" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.418541015625" Y="-4.174338378906" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.6920546875" Y="-3.982956787109" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791259766" />
                  <Point X="-27.0255546875" Y="-3.99762890625" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294921875" />
                  <Point X="-27.45352734375" Y="-4.409851074219" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.479400390625" Y="-4.400688964844" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-27.910439453125" Y="-4.125565429687" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.608984375" Y="-2.807437255859" />
                  <Point X="-27.506033203125" Y="-2.629119384766" />
                  <Point X="-27.499759765625" Y="-2.597598388672" />
                  <Point X="-27.516376953125" Y="-2.566364990234" />
                  <Point X="-27.531326171875" Y="-2.551415771484" />
                  <Point X="-27.56015625" Y="-2.537198730469" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.702763671875" Y="-3.184952880859" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-28.86430078125" Y="-3.237854736328" />
                  <Point X="-29.161697265625" Y="-2.847136962891" />
                  <Point X="-29.200849609375" Y="-2.781483886719" />
                  <Point X="-29.43101953125" Y="-2.395526367188" />
                  <Point X="-28.34391796875" Y="-1.561364990234" />
                  <Point X="-28.163787109375" Y="-1.42314465332" />
                  <Point X="-28.1525390625" Y="-1.411085693359" />
                  <Point X="-28.144755859375" Y="-1.391898803711" />
                  <Point X="-28.1381171875" Y="-1.36626550293" />
                  <Point X="-28.13665234375" Y="-1.35005065918" />
                  <Point X="-28.14865625" Y="-1.321068359375" />
                  <Point X="-28.1648203125" Y="-1.308483642578" />
                  <Point X="-28.187640625" Y="-1.295052734375" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.622162109375" Y="-1.473231445312" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.811076171875" Y="-1.466563354492" />
                  <Point X="-29.927392578125" Y="-1.011187561035" />
                  <Point X="-29.93775" Y="-0.93877355957" />
                  <Point X="-29.99839453125" Y="-0.514742248535" />
                  <Point X="-28.76266015625" Y="-0.183627975464" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.53805859375" Y="-0.118761795044" />
                  <Point X="-28.514142578125" Y="-0.102163635254" />
                  <Point X="-28.502322265625" Y="-0.090645118713" />
                  <Point X="-28.4936171875" Y="-0.071784034729" />
                  <Point X="-28.485646484375" Y="-0.046099205017" />
                  <Point X="-28.48692578125" Y="-0.012338674545" />
                  <Point X="-28.494896484375" Y="0.013346157074" />
                  <Point X="-28.502322265625" Y="0.028085039139" />
                  <Point X="-28.51798046875" Y="0.042267414093" />
                  <Point X="-28.541896484375" Y="0.058865581512" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.83603125" Y="0.408676696777" />
                  <Point X="-29.998185546875" Y="0.452126098633" />
                  <Point X="-29.992609375" Y="0.489816741943" />
                  <Point X="-29.91764453125" Y="0.996414855957" />
                  <Point X="-29.89679296875" Y="1.073363037109" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-28.90035546875" Y="1.413345092773" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.723208984375" Y="1.398545043945" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.651533203125" Y="1.426056274414" />
                  <Point X="-28.639119140625" Y="1.443786376953" />
                  <Point X="-28.6357109375" Y="1.45201550293" />
                  <Point X="-28.61447265625" Y="1.503290527344" />
                  <Point X="-28.610712890625" Y="1.52460546875" />
                  <Point X="-28.61631640625" Y="1.54551171875" />
                  <Point X="-28.6204296875" Y="1.553412475586" />
                  <Point X="-28.646056640625" Y="1.602641479492" />
                  <Point X="-28.65996875" Y="1.619221923828" />
                  <Point X="-29.393357421875" Y="2.181971679688" />
                  <Point X="-29.47610546875" Y="2.245466308594" />
                  <Point X="-29.451298828125" Y="2.287966796875" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-29.104783203125" Y="2.857997558594" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.249494140625" Y="2.979100830078" />
                  <Point X="-28.15915625" Y="2.926943603516" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.126681640625" Y="2.919399658203" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927404541016" />
                  <Point X="-28.004853515625" Y="2.935802246094" />
                  <Point X="-27.95252734375" Y="2.988127929688" />
                  <Point X="-27.9408984375" Y="3.006382324219" />
                  <Point X="-27.938072265625" Y="3.027841064453" />
                  <Point X="-27.939107421875" Y="3.039672363281" />
                  <Point X="-27.945556640625" Y="3.113390625" />
                  <Point X="-27.95206640625" Y="3.134032470703" />
                  <Point X="-28.2770546875" Y="3.696926757812" />
                  <Point X="-28.30727734375" Y="3.749276611328" />
                  <Point X="-28.260201171875" Y="3.785370605469" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.665888671875" Y="4.222659179687" />
                  <Point X="-27.141548828125" Y="4.513971679688" />
                  <Point X="-26.995482421875" Y="4.323614257813" />
                  <Point X="-26.967826171875" Y="4.287572753906" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.938078125" Y="4.266805664062" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.800091796875" Y="4.227931640625" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.69690234375" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.294487792969" />
                  <Point X="-26.681619140625" Y="4.308645996094" />
                  <Point X="-26.653802734375" Y="4.396864257812" />
                  <Point X="-26.651916015625" Y="4.418426757812" />
                  <Point X="-26.68886328125" Y="4.6990703125" />
                  <Point X="-26.68913671875" Y="4.701141113281" />
                  <Point X="-26.624857421875" Y="4.719163085938" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.862642578125" Y="4.915637695312" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.068865234375" Y="4.410642089844" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176269531" />
                  <Point X="-24.945546875" Y="4.31090234375" />
                  <Point X="-24.77902734375" Y="4.932362792969" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.7132421875" Y="4.98562109375" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-24.052548828125" Y="4.904502441406" />
                  <Point X="-23.491544921875" Y="4.76905859375" />
                  <Point X="-23.4350625" Y="4.748571777344" />
                  <Point X="-23.06896484375" Y="4.615785644531" />
                  <Point X="-23.014052734375" Y="4.59010546875" />
                  <Point X="-22.66130078125" Y="4.425134765625" />
                  <Point X="-22.60824609375" Y="4.394224609375" />
                  <Point X="-22.26747265625" Y="4.195688964844" />
                  <Point X="-22.217443359375" Y="4.160111816406" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.650986328125" Y="2.709987304688" />
                  <Point X="-22.77014453125" Y="2.50359765625" />
                  <Point X="-22.778115234375" Y="2.480412353516" />
                  <Point X="-22.7966171875" Y="2.411229248047" />
                  <Point X="-22.796796875" Y="2.382725341797" />
                  <Point X="-22.789583984375" Y="2.322901611328" />
                  <Point X="-22.781318359375" Y="2.300812011719" />
                  <Point X="-22.775376953125" Y="2.292056640625" />
                  <Point X="-22.738359375" Y="2.237503417969" />
                  <Point X="-22.7163046875" Y="2.218262451172" />
                  <Point X="-22.66175" Y="2.181245849609" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.630060546875" Y="2.171822509766" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.540232421875" Y="2.168915527344" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.172970703125" Y="2.934886230469" />
                  <Point X="-21.005751953125" Y="3.031430419922" />
                  <Point X="-20.999541015625" Y="3.022799316406" />
                  <Point X="-20.79740234375" Y="2.741874755859" />
                  <Point X="-20.769515625" Y="2.695788085938" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.554498046875" Y="1.713464111328" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.728619140625" Y="1.573408203125" />
                  <Point X="-21.77841015625" Y="1.508451782227" />
                  <Point X="-21.78985546875" Y="1.480857177734" />
                  <Point X="-21.808404296875" Y="1.414536621094" />
                  <Point X="-21.80921875" Y="1.390958374023" />
                  <Point X="-21.806775390625" Y="1.379123291016" />
                  <Point X="-21.79155078125" Y="1.305333007812" />
                  <Point X="-21.77770703125" Y="1.277853637695" />
                  <Point X="-21.736296875" Y="1.21491003418" />
                  <Point X="-21.719052734375" Y="1.19882019043" />
                  <Point X="-21.709419921875" Y="1.19339855957" />
                  <Point X="-21.64941015625" Y="1.159617675781" />
                  <Point X="-21.618412109375" Y="1.15189855957" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.302587890625" Y="1.301999511719" />
                  <Point X="-20.151025390625" Y="1.321953125" />
                  <Point X="-20.147626953125" Y="1.307994628906" />
                  <Point X="-20.06080859375" Y="0.951367431641" />
                  <Point X="-20.052021484375" Y="0.894930786133" />
                  <Point X="-20.002140625" Y="0.574556396484" />
                  <Point X="-21.0790390625" Y="0.286002075195" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.283705078125" Y="0.225424362183" />
                  <Point X="-21.363421875" Y="0.179346984863" />
                  <Point X="-21.38541015625" Y="0.157145462036" />
                  <Point X="-21.433240234375" Y="0.096199020386" />
                  <Point X="-21.443013671875" Y="0.07473526001" />
                  <Point X="-21.445572265625" Y="0.06137443161" />
                  <Point X="-21.461515625" Y="-0.021875299454" />
                  <Point X="-21.45895703125" Y="-0.054045455933" />
                  <Point X="-21.443013671875" Y="-0.137295333862" />
                  <Point X="-21.433240234375" Y="-0.158759094238" />
                  <Point X="-21.425564453125" Y="-0.168540481567" />
                  <Point X="-21.377734375" Y="-0.22948677063" />
                  <Point X="-21.35062890625" Y="-0.249302047729" />
                  <Point X="-21.270912109375" Y="-0.295379425049" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.1385625" Y="-0.60056237793" />
                  <Point X="-20.001931640625" Y="-0.637172546387" />
                  <Point X="-20.00309375" Y="-0.644884338379" />
                  <Point X="-20.051568359375" Y="-0.966412902832" />
                  <Point X="-20.062828125" Y="-1.015747009277" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-21.379509765625" Y="-1.125078857422" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.630271484375" Y="-1.103798828125" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.8145546875" Y="-1.154697387695" />
                  <Point X="-21.829732421875" Y="-1.172950805664" />
                  <Point X="-21.924298828125" Y="-1.286685424805" />
                  <Point X="-21.935640625" Y="-1.314070800781" />
                  <Point X="-21.93781640625" Y="-1.337709838867" />
                  <Point X="-21.951369140625" Y="-1.485001220703" />
                  <Point X="-21.943638671875" Y="-1.516622192383" />
                  <Point X="-21.929744140625" Y="-1.538236328125" />
                  <Point X="-21.843158203125" Y="-1.672912719727" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-20.791919921875" Y="-2.483267089844" />
                  <Point X="-20.66092578125" Y="-2.583783691406" />
                  <Point X="-20.795865234375" Y="-2.802138671875" />
                  <Point X="-20.81915625" Y="-2.835232421875" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-22.06083984375" Y="-2.366432617188" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.29254296875" Y="-2.247915771484" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.535748046875" Y="-2.232310791016" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.33468359375" />
                  <Point X="-22.72446484375" Y="-2.359510009766" />
                  <Point X="-22.80587890625" Y="-2.514201660156" />
                  <Point X="-22.8108359375" Y="-2.546374511719" />
                  <Point X="-22.8054375" Y="-2.576258789062" />
                  <Point X="-22.77180859375" Y="-2.76246484375" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.09784765625" Y="-3.935689941406" />
                  <Point X="-22.01332421875" Y="-4.082087646484" />
                  <Point X="-22.16470703125" Y="-4.190216308594" />
                  <Point X="-22.19073828125" Y="-4.207065917969" />
                  <Point X="-22.32022265625" Y="-4.29087890625" />
                  <Point X="-23.175111328125" Y="-3.176767578125" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.358923828125" Y="-2.961518066406" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.6064296875" Y="-2.838683349609" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.859556640625" Y="-2.889204833984" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045986083984" />
                  <Point X="-24.038984375" Y="-3.080226318359" />
                  <Point X="-24.085357421875" Y="-3.293573974609" />
                  <Point X="-24.0860703125" Y="-3.310719726562" />
                  <Point X="-23.89674609375" Y="-4.748768066406" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.0297109375" Y="-4.9676171875" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#130" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.026724810296" Y="4.45486282851" Z="0.35" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.35" />
                  <Point X="-0.874611309453" Y="4.996579006201" Z="0.35" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.35" />
                  <Point X="-1.64451149723" Y="4.79858653125" Z="0.35" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.35" />
                  <Point X="-1.744593273738" Y="4.723824062682" Z="0.35" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.35" />
                  <Point X="-1.735461077846" Y="4.35496228114" Z="0.35" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.35" />
                  <Point X="-1.825383767052" Y="4.305405682645" Z="0.35" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.35" />
                  <Point X="-1.92114726002" Y="4.342436477748" Z="0.35" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.35" />
                  <Point X="-1.961970738725" Y="4.385332735032" Z="0.35" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.35" />
                  <Point X="-2.696329089521" Y="4.297646561323" Z="0.35" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.35" />
                  <Point X="-3.296779332648" Y="3.85633054839" Z="0.35" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.35" />
                  <Point X="-3.326511948198" Y="3.703207326067" Z="0.35" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.35" />
                  <Point X="-2.995075107491" Y="3.066594627268" Z="0.35" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.35" />
                  <Point X="-3.046365776264" Y="3.002437791622" Z="0.35" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.35" />
                  <Point X="-3.128481838338" Y="3.000489591682" Z="0.35" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.35" />
                  <Point X="-3.230652022231" Y="3.053682000547" Z="0.35" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.35" />
                  <Point X="-4.150403369361" Y="2.919979938693" Z="0.35" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.35" />
                  <Point X="-4.500636703905" Y="2.344451952853" Z="0.35" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.35" />
                  <Point X="-4.429952166585" Y="2.173583903414" Z="0.35" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.35" />
                  <Point X="-3.670935315214" Y="1.561605154344" Z="0.35" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.35" />
                  <Point X="-3.68806127846" Y="1.502429270054" Z="0.35" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.35" />
                  <Point X="-3.744401122251" Y="1.477510979042" Z="0.35" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.35" />
                  <Point X="-3.899986903585" Y="1.494197416192" Z="0.35" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.35" />
                  <Point X="-4.951210397175" Y="1.117720352367" Z="0.35" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.35" />
                  <Point X="-5.048226730641" Y="0.528415849207" Z="0.35" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.35" />
                  <Point X="-4.855129178227" Y="0.391660418985" Z="0.35" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.35" />
                  <Point X="-3.552645951152" Y="0.032470805903" Z="0.35" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.35" />
                  <Point X="-3.540836127606" Y="0.004122160573" Z="0.35" />
                  <Point X="-3.539556741714" Y="0" Z="0.35" />
                  <Point X="-3.547528473529" Y="-0.025684790488" Z="0.35" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.35" />
                  <Point X="-3.572722643971" Y="-0.046405177231" Z="0.35" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.35" />
                  <Point X="-3.781758610545" Y="-0.104051637771" Z="0.35" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.35" />
                  <Point X="-4.993402472326" Y="-0.91457263431" Z="0.35" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.35" />
                  <Point X="-4.865678069745" Y="-1.44765745822" Z="0.35" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.35" />
                  <Point X="-4.621793955354" Y="-1.49152368237" Z="0.35" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.35" />
                  <Point X="-3.196338721814" Y="-1.320294264643" Z="0.35" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.35" />
                  <Point X="-3.199315306688" Y="-1.348083249892" Z="0.35" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.35" />
                  <Point X="-3.380513139315" Y="-1.490417568134" Z="0.35" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.35" />
                  <Point X="-4.249950864735" Y="-2.775813816832" Z="0.35" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.35" />
                  <Point X="-3.910243386673" Y="-3.236858009084" Z="0.35" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.35" />
                  <Point X="-3.683921037946" Y="-3.196974187582" Z="0.35" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.35" />
                  <Point X="-2.557890190096" Y="-2.570440279036" Z="0.35" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.35" />
                  <Point X="-2.658442825586" Y="-2.751157344546" Z="0.35" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.35" />
                  <Point X="-2.947100586424" Y="-4.133902733659" Z="0.35" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.35" />
                  <Point X="-2.511878857058" Y="-4.411919802538" Z="0.35" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.35" />
                  <Point X="-2.420015771506" Y="-4.409008690214" Z="0.35" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.35" />
                  <Point X="-2.003931515021" Y="-4.007922022062" Z="0.35" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.35" />
                  <Point X="-1.70148156626" Y="-4.001569706729" Z="0.35" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.35" />
                  <Point X="-1.457664855938" Y="-4.180650191783" Z="0.35" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.35" />
                  <Point X="-1.373249194929" Y="-4.47115012252" Z="0.35" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.35" />
                  <Point X="-1.371547204816" Y="-4.563885783409" Z="0.35" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.35" />
                  <Point X="-1.15829536637" Y="-4.945062023096" Z="0.35" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.35" />
                  <Point X="-0.859119843613" Y="-5.005717017592" Z="0.35" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.35" />
                  <Point X="-0.762269578185" Y="-4.807012911453" Z="0.35" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.35" />
                  <Point X="-0.276001847191" Y="-3.315496588454" Z="0.35" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.35" />
                  <Point X="-0.035038364228" Y="-3.215113983659" Z="0.35" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.35" />
                  <Point X="0.218320715133" Y="-3.271998061629" Z="0.35" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.35" />
                  <Point X="0.394444012241" Y="-3.486149167314" Z="0.35" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.35" />
                  <Point X="0.472485282216" Y="-3.725523118363" Z="0.35" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.35" />
                  <Point X="0.973069700759" Y="-4.98553214805" Z="0.35" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.35" />
                  <Point X="1.152292198727" Y="-4.947182842649" Z="0.35" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.35" />
                  <Point X="1.146668507719" Y="-4.710962382299" Z="0.35" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.35" />
                  <Point X="1.003717761855" Y="-3.059566220629" Z="0.35" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.35" />
                  <Point X="1.166252850527" Y="-2.896371407481" Z="0.35" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.35" />
                  <Point X="1.391995665737" Y="-2.857192863956" Z="0.35" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.35" />
                  <Point X="1.607879931681" Y="-2.972296233301" Z="0.35" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.35" />
                  <Point X="1.779064068766" Y="-3.175925523405" Z="0.35" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.35" />
                  <Point X="2.830274619233" Y="-4.217759910361" Z="0.35" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.35" />
                  <Point X="3.020341740237" Y="-4.083808213405" Z="0.35" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.35" />
                  <Point X="2.939295634563" Y="-3.879409814613" Z="0.35" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.35" />
                  <Point X="2.237608194546" Y="-2.536092468884" Z="0.35" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.35" />
                  <Point X="2.313624185834" Y="-2.351516686487" Z="0.35" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.35" />
                  <Point X="2.481381691885" Y="-2.245277124133" Z="0.35" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.35" />
                  <Point X="2.692414334628" Y="-2.265839814944" Z="0.35" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.35" />
                  <Point X="2.908003782494" Y="-2.378453865711" Z="0.35" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.35" />
                  <Point X="4.215574890049" Y="-2.832729789052" Z="0.35" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.35" />
                  <Point X="4.377152344402" Y="-2.57603711776" Z="0.35" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.35" />
                  <Point X="4.232360033794" Y="-2.412319477663" Z="0.35" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.35" />
                  <Point X="3.106158459972" Y="-1.479916740266" Z="0.35" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.35" />
                  <Point X="3.105816217409" Y="-1.311011080133" Z="0.35" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.35" />
                  <Point X="3.202558442043" Y="-1.173637506752" Z="0.35" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.35" />
                  <Point X="3.374190370424" Y="-1.121378075896" Z="0.35" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.35" />
                  <Point X="3.607808601679" Y="-1.143371119" Z="0.35" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.35" />
                  <Point X="4.979761890586" Y="-0.995590790162" Z="0.35" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.35" />
                  <Point X="5.040190823224" Y="-0.621058187234" Z="0.35" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.35" />
                  <Point X="4.868222624575" Y="-0.52098604478" Z="0.35" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.35" />
                  <Point X="3.668236408749" Y="-0.174733191851" Z="0.35" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.35" />
                  <Point X="3.607612940144" Y="-0.106391834823" Z="0.35" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.35" />
                  <Point X="3.583993465527" Y="-0.013360780272" Z="0.35" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.35" />
                  <Point X="3.597377984898" Y="0.083249750967" Z="0.35" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.35" />
                  <Point X="3.647766498257" Y="0.157556903752" Z="0.35" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.35" />
                  <Point X="3.735159005603" Y="0.213415652391" Z="0.35" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.35" />
                  <Point X="3.927745192732" Y="0.268985887989" Z="0.35" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.35" />
                  <Point X="4.99122685215" Y="0.933903055015" Z="0.35" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.35" />
                  <Point X="4.894798568825" Y="1.35110153744" Z="0.35" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.35" />
                  <Point X="4.684729221671" Y="1.38285185157" Z="0.35" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.35" />
                  <Point X="3.381982612291" Y="1.232747564241" Z="0.35" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.35" />
                  <Point X="3.308949721004" Y="1.268249517055" Z="0.35" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.35" />
                  <Point X="3.257907128937" Y="1.336614573825" Z="0.35" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.35" />
                  <Point X="3.236035567458" Y="1.420506593018" Z="0.35" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.35" />
                  <Point X="3.252139324885" Y="1.498669876022" Z="0.35" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.35" />
                  <Point X="3.304907157347" Y="1.57427020939" Z="0.35" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.35" />
                  <Point X="3.469782219302" Y="1.705076470074" Z="0.35" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.35" />
                  <Point X="4.267105376629" Y="2.752954474234" Z="0.35" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.35" />
                  <Point X="4.034888172792" Y="3.083282441325" Z="0.35" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.35" />
                  <Point X="3.795871607025" Y="3.009467526794" Z="0.35" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.35" />
                  <Point X="2.440694866961" Y="2.248498318137" Z="0.35" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.35" />
                  <Point X="2.369767919447" Y="2.252742809346" Z="0.35" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.35" />
                  <Point X="2.305613368714" Y="2.290917246761" Z="0.35" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.35" />
                  <Point X="2.259841353474" Y="2.351411491669" Z="0.35" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.35" />
                  <Point X="2.246686893413" Y="2.419990524386" Z="0.35" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.35" />
                  <Point X="2.264029624521" Y="2.498774708921" Z="0.35" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.35" />
                  <Point X="2.386157826066" Y="2.71626747493" Z="0.35" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.35" />
                  <Point X="2.805376373065" Y="4.232138854149" Z="0.35" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.35" />
                  <Point X="2.410767608807" Y="4.468707427189" Z="0.35" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.35" />
                  <Point X="2.000971758119" Y="4.666677269479" Z="0.35" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.35" />
                  <Point X="1.57583027541" Y="4.826855655796" Z="0.35" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.35" />
                  <Point X="0.953028517914" Y="4.984385758182" Z="0.35" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.35" />
                  <Point X="0.285807541039" Y="5.066629216282" Z="0.35" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.35" />
                  <Point X="0.166519751257" Y="4.976584677441" Z="0.35" />
                  <Point X="0" Y="4.355124473572" Z="0.35" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>