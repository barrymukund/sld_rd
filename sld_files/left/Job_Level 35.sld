<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#185" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2548" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000473632812" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.168390625" Y="-4.51385546875" />
                  <Point X="-24.4366953125" Y="-3.512524414063" />
                  <Point X="-24.44227734375" Y="-3.497140136719" />
                  <Point X="-24.457634765625" Y="-3.467376953125" />
                  <Point X="-24.567298828125" Y="-3.309372802734" />
                  <Point X="-24.62136328125" Y="-3.2314765625" />
                  <Point X="-24.643248046875" Y="-3.209020751953" />
                  <Point X="-24.66950390625" Y="-3.189776855469" />
                  <Point X="-24.69750390625" Y="-3.175669189453" />
                  <Point X="-24.867201171875" Y="-3.123001220703" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.206521484375" Y="-3.149703613281" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.189776855469" />
                  <Point X="-25.344439453125" Y="-3.209020751953" />
                  <Point X="-25.36632421875" Y="-3.231477050781" />
                  <Point X="-25.475986328125" Y="-3.389480957031" />
                  <Point X="-25.53005078125" Y="-3.467377441406" />
                  <Point X="-25.5381875" Y="-3.481572021484" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.683265625" Y="-4.006182861328" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-25.984640625" Y="-4.863731445312" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.24641796875" Y="-4.802362792969" />
                  <Point X="-26.2149609375" Y="-4.5634375" />
                  <Point X="-26.21419921875" Y="-4.547929199219" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.257048828125" Y="-4.312411132812" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.182963867188" />
                  <Point X="-26.30401171875" Y="-4.155126953125" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.47987890625" Y="-3.994188476562" />
                  <Point X="-26.556904296875" Y="-3.926639404297" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.85038671875" Y="-3.877375244141" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708740234" />
                  <Point X="-27.014466796875" Y="-3.882028564453" />
                  <Point X="-27.042658203125" Y="-3.894801757812" />
                  <Point X="-27.21544140625" Y="-4.010252197266" />
                  <Point X="-27.300623046875" Y="-4.067169677734" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.335099609375" Y="-4.099461425781" />
                  <Point X="-27.40936328125" Y="-4.196241699219" />
                  <Point X="-27.480146484375" Y="-4.288489257813" />
                  <Point X="-27.662904296875" Y="-4.175330566406" />
                  <Point X="-27.801712890625" Y="-4.089384033203" />
                  <Point X="-28.070103515625" Y="-3.882731933594" />
                  <Point X="-28.104720703125" Y="-3.856077880859" />
                  <Point X="-27.922376953125" Y="-3.540247802734" />
                  <Point X="-27.423759765625" Y="-2.676619384766" />
                  <Point X="-27.412859375" Y="-2.647655029297" />
                  <Point X="-27.406587890625" Y="-2.616129394531" />
                  <Point X="-27.40557421875" Y="-2.585194824219" />
                  <Point X="-27.41455859375" Y="-2.555576660156" />
                  <Point X="-27.428775390625" Y="-2.526747802734" />
                  <Point X="-27.44680078125" Y="-2.501591796875" />
                  <Point X="-27.4641484375" Y="-2.484243164062" />
                  <Point X="-27.4893046875" Y="-2.466215087891" />
                  <Point X="-27.5181328125" Y="-2.451997314453" />
                  <Point X="-27.547751953125" Y="-2.44301171875" />
                  <Point X="-27.5786875" Y="-2.444023925781" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.064953125" Y="-2.707016113281" />
                  <Point X="-28.818021484375" Y="-3.141800537109" />
                  <Point X="-28.973197265625" Y="-2.93793359375" />
                  <Point X="-29.0828671875" Y="-2.793849365234" />
                  <Point X="-29.27527734375" Y="-2.471206787109" />
                  <Point X="-29.306142578125" Y="-2.419450439453" />
                  <Point X="-28.97837109375" Y="-2.167942382812" />
                  <Point X="-28.105955078125" Y="-1.498513305664" />
                  <Point X="-28.084580078125" Y="-1.475598388672" />
                  <Point X="-28.066615234375" Y="-1.448471069336" />
                  <Point X="-28.053859375" Y="-1.41984375" />
                  <Point X="-28.048697265625" Y="-1.399919067383" />
                  <Point X="-28.046146484375" Y="-1.390066162109" />
                  <Point X="-28.043345703125" Y="-1.359644287109" />
                  <Point X="-28.045556640625" Y="-1.327977294922" />
                  <Point X="-28.05255859375" Y="-1.298235961914" />
                  <Point X="-28.068640625" Y="-1.272255615234" />
                  <Point X="-28.089470703125" Y="-1.248301879883" />
                  <Point X="-28.112970703125" Y="-1.228767211914" />
                  <Point X="-28.130708984375" Y="-1.218327514648" />
                  <Point X="-28.139453125" Y="-1.213180786133" />
                  <Point X="-28.168712890625" Y="-1.201957763672" />
                  <Point X="-28.2006015625" Y="-1.195475219727" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-28.769423828125" Y="-1.265146362305" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.791185546875" Y="-1.160567871094" />
                  <Point X="-29.83407421875" Y="-0.992651855469" />
                  <Point X="-29.884984375" Y="-0.636705749512" />
                  <Point X="-29.892421875" Y="-0.584698303223" />
                  <Point X="-29.5266875" Y="-0.486699859619" />
                  <Point X="-28.532875" Y="-0.220408432007" />
                  <Point X="-28.5174921875" Y="-0.214827087402" />
                  <Point X="-28.48773046875" Y="-0.199470611572" />
                  <Point X="-28.469140625" Y="-0.186569046021" />
                  <Point X="-28.4599765625" Y="-0.180208724976" />
                  <Point X="-28.43751953125" Y="-0.158323898315" />
                  <Point X="-28.418275390625" Y="-0.132068557739" />
                  <Point X="-28.4041640625" Y="-0.104063087463" />
                  <Point X="-28.39796875" Y="-0.084098556519" />
                  <Point X="-28.3949140625" Y="-0.074256164551" />
                  <Point X="-28.390646484375" Y="-0.046100574493" />
                  <Point X="-28.390646484375" Y="-0.016459560394" />
                  <Point X="-28.3949140625" Y="0.011696336746" />
                  <Point X="-28.401109375" Y="0.031660564423" />
                  <Point X="-28.4041640625" Y="0.041503105164" />
                  <Point X="-28.418275390625" Y="0.069508430481" />
                  <Point X="-28.43751953125" Y="0.095763755798" />
                  <Point X="-28.4599765625" Y="0.117648590088" />
                  <Point X="-28.47856640625" Y="0.13055015564" />
                  <Point X="-28.48700390625" Y="0.135775695801" />
                  <Point X="-28.511736328125" Y="0.149356689453" />
                  <Point X="-28.532875" Y="0.157848297119" />
                  <Point X="-29.022826171875" Y="0.289130554199" />
                  <Point X="-29.89181640625" Y="0.521975524902" />
                  <Point X="-29.85212890625" Y="0.790179931641" />
                  <Point X="-29.82448828125" Y="0.976968139648" />
                  <Point X="-29.722009765625" Y="1.355148925781" />
                  <Point X="-29.70355078125" Y="1.423268066406" />
                  <Point X="-29.4780546875" Y="1.393580932617" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744982421875" Y="1.299341796875" />
                  <Point X="-28.723421875" Y="1.301228271484" />
                  <Point X="-28.70313671875" Y="1.305263671875" />
                  <Point X="-28.661994140625" Y="1.318235961914" />
                  <Point X="-28.6417109375" Y="1.324631225586" />
                  <Point X="-28.62277734375" Y="1.332962036133" />
                  <Point X="-28.604033203125" Y="1.343784057617" />
                  <Point X="-28.5873515625" Y="1.356015625" />
                  <Point X="-28.573712890625" Y="1.371567993164" />
                  <Point X="-28.561298828125" Y="1.389297851562" />
                  <Point X="-28.551349609375" Y="1.407432861328" />
                  <Point X="-28.534841796875" Y="1.447288208008" />
                  <Point X="-28.526703125" Y="1.466937011719" />
                  <Point X="-28.520916015625" Y="1.486788085938" />
                  <Point X="-28.51715625" Y="1.508103271484" />
                  <Point X="-28.515802734375" Y="1.528745849609" />
                  <Point X="-28.51894921875" Y="1.549192260742" />
                  <Point X="-28.52455078125" Y="1.570098510742" />
                  <Point X="-28.532048828125" Y="1.589378662109" />
                  <Point X="-28.55196875" Y="1.627643554688" />
                  <Point X="-28.5617890625" Y="1.646508300781" />
                  <Point X="-28.573283203125" Y="1.663710083008" />
                  <Point X="-28.587197265625" Y="1.680290405273" />
                  <Point X="-28.60213671875" Y="1.694590332031" />
                  <Point X="-28.883173828125" Y="1.91023828125" />
                  <Point X="-29.351859375" Y="2.269873535156" />
                  <Point X="-29.1885546875" Y="2.549655029297" />
                  <Point X="-29.0811484375" Y="2.7336640625" />
                  <Point X="-28.8096953125" Y="3.082581054688" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.644185546875" Y="3.097279052734" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.0894921875" Y="2.820783203125" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.99901171875" Y="2.826504882812" />
                  <Point X="-27.9804609375" Y="2.835654541016" />
                  <Point X="-27.96220703125" Y="2.847284179688" />
                  <Point X="-27.946078125" Y="2.860228515625" />
                  <Point X="-27.905404296875" Y="2.900900634766" />
                  <Point X="-27.885353515625" Y="2.920951904297" />
                  <Point X="-27.87240625" Y="2.937084228516" />
                  <Point X="-27.86077734375" Y="2.955337646484" />
                  <Point X="-27.85162890625" Y="2.973887207031" />
                  <Point X="-27.8467109375" Y="2.993976806641" />
                  <Point X="-27.843884765625" Y="3.015434814453" />
                  <Point X="-27.84343359375" Y="3.036120605469" />
                  <Point X="-27.848447265625" Y="3.093420898438" />
                  <Point X="-27.85091796875" Y="3.121670166016" />
                  <Point X="-27.854955078125" Y="3.141959960938" />
                  <Point X="-27.86146484375" Y="3.162603515625" />
                  <Point X="-27.869794921875" Y="3.181533203125" />
                  <Point X="-27.99433203125" Y="3.397236572266" />
                  <Point X="-28.18333203125" Y="3.724595703125" />
                  <Point X="-27.887677734375" Y="3.951270751953" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.273087890625" Y="4.332213867188" />
                  <Point X="-27.16703515625" Y="4.391133789062" />
                  <Point X="-27.0431953125" Y="4.229741699219" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.995111328125" Y="4.18939453125" />
                  <Point X="-26.9313359375" Y="4.1561953125" />
                  <Point X="-26.89989453125" Y="4.139827636719" />
                  <Point X="-26.8806171875" Y="4.132331054688" />
                  <Point X="-26.8597109375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.12358203125" />
                  <Point X="-26.81862890625" Y="4.124934570312" />
                  <Point X="-26.7973125" Y="4.128692871094" />
                  <Point X="-26.777451171875" Y="4.134481933594" />
                  <Point X="-26.711025390625" Y="4.161997070312" />
                  <Point X="-26.67827734375" Y="4.175561523437" />
                  <Point X="-26.660142578125" Y="4.185511230469" />
                  <Point X="-26.6424140625" Y="4.197925292969" />
                  <Point X="-26.62686328125" Y="4.211562011719" />
                  <Point X="-26.6146328125" Y="4.228240722656" />
                  <Point X="-26.603810546875" Y="4.246983886719" />
                  <Point X="-26.595478515625" Y="4.265921386719" />
                  <Point X="-26.573859375" Y="4.3344921875" />
                  <Point X="-26.56319921875" Y="4.368297851562" />
                  <Point X="-26.5591640625" Y="4.388583007812" />
                  <Point X="-26.55727734375" Y="4.410146484375" />
                  <Point X="-26.557728515625" Y="4.430826660156" />
                  <Point X="-26.57188671875" Y="4.538370605469" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.191787109375" Y="4.741916992188" />
                  <Point X="-25.949630859375" Y="4.80980859375" />
                  <Point X="-25.431333984375" Y="4.870467773438" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.263337890625" Y="4.769368652344" />
                  <Point X="-25.133904296875" Y="4.286315917969" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286314941406" />
                  <Point X="-24.789970703125" Y="4.5244609375" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.3673984375" Y="4.853882324219" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.727150390625" Y="4.728211425781" />
                  <Point X="-23.51897265625" Y="4.677950195312" />
                  <Point X="-23.240349609375" Y="4.576892578125" />
                  <Point X="-23.105353515625" Y="4.527928222656" />
                  <Point X="-22.835462890625" Y="4.401709960938" />
                  <Point X="-22.705423828125" Y="4.340894042969" />
                  <Point X="-22.444673828125" Y="4.188980957031" />
                  <Point X="-22.319021484375" Y="4.115775878906" />
                  <Point X="-22.07312109375" Y="3.940904541016" />
                  <Point X="-22.05673828125" Y="3.929253662109" />
                  <Point X="-22.275306640625" Y="3.55068359375" />
                  <Point X="-22.852416015625" Y="2.551098144531" />
                  <Point X="-22.857919921875" Y="2.539935546875" />
                  <Point X="-22.866921875" Y="2.516058349609" />
                  <Point X="-22.881302734375" Y="2.462283203125" />
                  <Point X="-22.888392578125" Y="2.435771972656" />
                  <Point X="-22.891380859375" Y="2.417936523438" />
                  <Point X="-22.892271484375" Y="2.380953613281" />
                  <Point X="-22.8866640625" Y="2.334453613281" />
                  <Point X="-22.883900390625" Y="2.311528808594" />
                  <Point X="-22.878556640625" Y="2.289603515625" />
                  <Point X="-22.8702890625" Y="2.267512939453" />
                  <Point X="-22.859927734375" Y="2.247469726562" />
                  <Point X="-22.831154296875" Y="2.205066162109" />
                  <Point X="-22.81696875" Y="2.184161132813" />
                  <Point X="-22.80553125" Y="2.170326904297" />
                  <Point X="-22.7783984375" Y="2.145592529297" />
                  <Point X="-22.73599609375" Y="2.116819824219" />
                  <Point X="-22.71508984375" Y="2.102635009766" />
                  <Point X="-22.695044921875" Y="2.092271484375" />
                  <Point X="-22.672958984375" Y="2.084006347656" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.60453515625" Y="2.073056396484" />
                  <Point X="-22.581611328125" Y="2.070291992188" />
                  <Point X="-22.563533203125" Y="2.069845458984" />
                  <Point X="-22.52679296875" Y="2.074170898438" />
                  <Point X="-22.473017578125" Y="2.088551269531" />
                  <Point X="-22.446505859375" Y="2.095640625" />
                  <Point X="-22.434716796875" Y="2.099637939453" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.91866796875" Y="2.394661865234" />
                  <Point X="-21.032673828125" Y="2.906190673828" />
                  <Point X="-20.95126171875" Y="2.793046875" />
                  <Point X="-20.87672265625" Y="2.689453613281" />
                  <Point X="-20.739642578125" Y="2.462926513672" />
                  <Point X="-20.73780078125" Y="2.459883056641" />
                  <Point X="-21.00939453125" Y="2.251482177734" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.7785703125" Y="1.660244873047" />
                  <Point X="-21.79602734375" Y="1.641626953125" />
                  <Point X="-21.834728515625" Y="1.591137207031" />
                  <Point X="-21.85380859375" Y="1.566245605469" />
                  <Point X="-21.86339453125" Y="1.550909057617" />
                  <Point X="-21.8783671875" Y="1.517088623047" />
                  <Point X="-21.89278515625" Y="1.465538330078" />
                  <Point X="-21.899892578125" Y="1.440124023438" />
                  <Point X="-21.90334765625" Y="1.41782434082" />
                  <Point X="-21.9041640625" Y="1.394254882812" />
                  <Point X="-21.902259765625" Y="1.371766601562" />
                  <Point X="-21.890423828125" Y="1.314410400391" />
                  <Point X="-21.88458984375" Y="1.286133911133" />
                  <Point X="-21.8793203125" Y="1.268979003906" />
                  <Point X="-21.863716796875" Y="1.235741577148" />
                  <Point X="-21.83152734375" Y="1.18681640625" />
                  <Point X="-21.81566015625" Y="1.162696166992" />
                  <Point X="-21.801111328125" Y="1.145455810547" />
                  <Point X="-21.7838671875" Y="1.129364257812" />
                  <Point X="-21.76565234375" Y="1.116034912109" />
                  <Point X="-21.719005859375" Y="1.08977734375" />
                  <Point X="-21.696009765625" Y="1.076832397461" />
                  <Point X="-21.679478515625" Y="1.069501342773" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.5808125" Y="1.051103271484" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-21.043671875" Y="1.108614379883" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.186076171875" Y="1.064303466797" />
                  <Point X="-20.154060546875" Y="0.932788818359" />
                  <Point X="-20.11086328125" Y="0.655344543457" />
                  <Point X="-20.109134765625" Y="0.644238647461" />
                  <Point X="-20.4126484375" Y="0.562912109375" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.295212890625" Y="0.325584991455" />
                  <Point X="-21.318453125" Y="0.315067901611" />
                  <Point X="-21.380416015625" Y="0.279252502441" />
                  <Point X="-21.410962890625" Y="0.261595489502" />
                  <Point X="-21.4256875" Y="0.251096755981" />
                  <Point X="-21.45246875" Y="0.225576538086" />
                  <Point X="-21.489646484375" Y="0.178203826904" />
                  <Point X="-21.507974609375" Y="0.154848907471" />
                  <Point X="-21.51969921875" Y="0.135567443848" />
                  <Point X="-21.52947265625" Y="0.11410382843" />
                  <Point X="-21.536318359375" Y="0.092603935242" />
                  <Point X="-21.5487109375" Y="0.02789509964" />
                  <Point X="-21.5548203125" Y="-0.004006679058" />
                  <Point X="-21.556515625" Y="-0.021875591278" />
                  <Point X="-21.5548203125" Y="-0.058553455353" />
                  <Point X="-21.542427734375" Y="-0.123262290955" />
                  <Point X="-21.536318359375" Y="-0.155164077759" />
                  <Point X="-21.52947265625" Y="-0.176663955688" />
                  <Point X="-21.51969921875" Y="-0.198127578735" />
                  <Point X="-21.507974609375" Y="-0.217409042358" />
                  <Point X="-21.470796875" Y="-0.264781768799" />
                  <Point X="-21.45246875" Y="-0.28813684082" />
                  <Point X="-21.44" Y="-0.301236328125" />
                  <Point X="-21.410962890625" Y="-0.324155609131" />
                  <Point X="-21.349" Y="-0.359970855713" />
                  <Point X="-21.318453125" Y="-0.377627868652" />
                  <Point X="-21.30729296875" Y="-0.383137237549" />
                  <Point X="-21.283419921875" Y="-0.392149963379" />
                  <Point X="-20.854126953125" Y="-0.507178344727" />
                  <Point X="-20.108525390625" Y="-0.706961853027" />
                  <Point X="-20.1271015625" Y="-0.830171875" />
                  <Point X="-20.144974609375" Y="-0.94872479248" />
                  <Point X="-20.19882421875" Y="-1.18469909668" />
                  <Point X="-20.565767578125" Y="-1.136390014648" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710266113" />
                  <Point X="-21.62533984375" Y="-1.005508666992" />
                  <Point X="-21.746951171875" Y="-1.031941162109" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.836021484375" Y="-1.056595825195" />
                  <Point X="-21.863849609375" Y="-1.073488037109" />
                  <Point X="-21.887603515625" Y="-1.093960083008" />
                  <Point X="-21.961109375" Y="-1.182364624023" />
                  <Point X="-21.99734765625" Y="-1.225948242188" />
                  <Point X="-22.012068359375" Y="-1.250335205078" />
                  <Point X="-22.02341015625" Y="-1.277720825195" />
                  <Point X="-22.030240234375" Y="-1.305365966797" />
                  <Point X="-22.040775390625" Y="-1.419853759766" />
                  <Point X="-22.04596875" Y="-1.476296142578" />
                  <Point X="-22.043650390625" Y="-1.507562133789" />
                  <Point X="-22.035919921875" Y="-1.539182617188" />
                  <Point X="-22.023548828125" Y="-1.567996582031" />
                  <Point X="-21.956248046875" Y="-1.672678710938" />
                  <Point X="-21.923068359375" Y="-1.724287231445" />
                  <Point X="-21.9130625" Y="-1.73724230957" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.490984375" Y="-2.066601074219" />
                  <Point X="-20.786876953125" Y="-2.606882324219" />
                  <Point X="-20.8248046875" Y="-2.668255126953" />
                  <Point X="-20.875189453125" Y="-2.749782714844" />
                  <Point X="-20.971017578125" Y="-2.885944824219" />
                  <Point X="-21.29963671875" Y="-2.696216064453" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.2457734375" Y="-2.159825195312" />
                  <Point X="-22.3905078125" Y="-2.133686035156" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395507812" />
                  <Point X="-22.525390625" Y="-2.125353515625" />
                  <Point X="-22.555166015625" Y="-2.135177001953" />
                  <Point X="-22.67540625" Y="-2.198458251953" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.75761328125" Y="-2.246548583984" />
                  <Point X="-22.778572265625" Y="-2.267507324219" />
                  <Point X="-22.795466796875" Y="-2.290438476562" />
                  <Point X="-22.858748046875" Y="-2.410677978516" />
                  <Point X="-22.889947265625" Y="-2.469956542969" />
                  <Point X="-22.899771484375" Y="-2.499734863281" />
                  <Point X="-22.904728515625" Y="-2.531908447266" />
                  <Point X="-22.90432421875" Y="-2.563259277344" />
                  <Point X="-22.87818359375" Y="-2.707994628906" />
                  <Point X="-22.865296875" Y="-2.779349609375" />
                  <Point X="-22.86101171875" Y="-2.795139648438" />
                  <Point X="-22.8481796875" Y="-2.826078613281" />
                  <Point X="-22.592177734375" Y="-3.26948828125" />
                  <Point X="-22.13871484375" Y="-4.054905517578" />
                  <Point X="-22.158361328125" Y="-4.068937744141" />
                  <Point X="-22.218185546875" Y="-4.111666992188" />
                  <Point X="-22.298232421875" Y="-4.163481445312" />
                  <Point X="-22.55512109375" Y="-3.828697998047" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922177490234" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.42082421875" Y="-2.808783203125" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.520009765625" Y="-2.751167236328" />
                  <Point X="-23.551630859375" Y="-2.743435546875" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.73901953125" Y="-2.755482910156" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.843634765625" Y="-2.769396972656" />
                  <Point X="-23.87101953125" Y="-2.780739746094" />
                  <Point X="-23.89540234375" Y="-2.7954609375" />
                  <Point X="-24.015953125" Y="-2.895695556641" />
                  <Point X="-24.07538671875" Y="-2.945111328125" />
                  <Point X="-24.095857421875" Y="-2.968860351563" />
                  <Point X="-24.11275" Y="-2.996686523438" />
                  <Point X="-24.124375" Y="-3.02580859375" />
                  <Point X="-24.16041796875" Y="-3.191640869141" />
                  <Point X="-24.178189453125" Y="-3.273396484375" />
                  <Point X="-24.180275390625" Y="-3.289626953125" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.10770703125" Y="-3.874186035156" />
                  <Point X="-23.97793359375" Y="-4.859916015625" />
                  <Point X="-24.024318359375" Y="-4.870083007812" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.05841796875" Y="-4.752638183594" />
                  <Point X="-26.14124609375" Y="-4.731327636719" />
                  <Point X="-26.1207734375" Y="-4.575838378906" />
                  <Point X="-26.120076171875" Y="-4.568098144531" />
                  <Point X="-26.11944921875" Y="-4.541030273438" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497689453125" />
                  <Point X="-26.163875" Y="-4.293877441406" />
                  <Point X="-26.18386328125" Y="-4.193397949219" />
                  <Point X="-26.188125" Y="-4.17846875" />
                  <Point X="-26.19902734375" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135462402344" />
                  <Point X="-26.221740234375" Y="-4.107625488281" />
                  <Point X="-26.230576171875" Y="-4.094859863281" />
                  <Point X="-26.250208984375" Y="-4.070937011719" />
                  <Point X="-26.261005859375" Y="-4.059779785156" />
                  <Point X="-26.417240234375" Y="-3.922764160156" />
                  <Point X="-26.494265625" Y="-3.855215087891" />
                  <Point X="-26.50673828125" Y="-3.845965332031" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.844173828125" Y="-3.782578613281" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166259766" />
                  <Point X="-27.008005859375" Y="-3.781946044922" />
                  <Point X="-27.0390546875" Y="-3.790265869141" />
                  <Point X="-27.053673828125" Y="-3.795496337891" />
                  <Point X="-27.081865234375" Y="-3.80826953125" />
                  <Point X="-27.0954375" Y="-3.815812255859" />
                  <Point X="-27.268220703125" Y="-3.931262695313" />
                  <Point X="-27.35340234375" Y="-3.988180175781" />
                  <Point X="-27.35968359375" Y="-3.992759765625" />
                  <Point X="-27.380443359375" Y="-4.010133789063" />
                  <Point X="-27.4027578125" Y="-4.032772216797" />
                  <Point X="-27.410466796875" Y="-4.041628417969" />
                  <Point X="-27.48473046875" Y="-4.138408691406" />
                  <Point X="-27.50319921875" Y="-4.162478515625" />
                  <Point X="-27.612892578125" Y="-4.094559814453" />
                  <Point X="-27.74759375" Y="-4.011156494141" />
                  <Point X="-27.980861328125" Y="-3.831548095703" />
                  <Point X="-27.840103515625" Y="-3.587747558594" />
                  <Point X="-27.341486328125" Y="-2.724119140625" />
                  <Point X="-27.33484765625" Y="-2.710080322266" />
                  <Point X="-27.323947265625" Y="-2.681115966797" />
                  <Point X="-27.319685546875" Y="-2.666190429688" />
                  <Point X="-27.3134140625" Y="-2.634664794922" />
                  <Point X="-27.311638671875" Y="-2.619240722656" />
                  <Point X="-27.310625" Y="-2.588306152344" />
                  <Point X="-27.3146640625" Y="-2.557618408203" />
                  <Point X="-27.3236484375" Y="-2.528000244141" />
                  <Point X="-27.32935546875" Y="-2.513559326172" />
                  <Point X="-27.343572265625" Y="-2.48473046875" />
                  <Point X="-27.351552734375" Y="-2.471414794922" />
                  <Point X="-27.369578125" Y="-2.446258789062" />
                  <Point X="-27.379623046875" Y="-2.434418457031" />
                  <Point X="-27.396970703125" Y="-2.417069824219" />
                  <Point X="-27.408810546875" Y="-2.407024658203" />
                  <Point X="-27.433966796875" Y="-2.388996582031" />
                  <Point X="-27.447283203125" Y="-2.381013671875" />
                  <Point X="-27.476111328125" Y="-2.366795898438" />
                  <Point X="-27.4905546875" Y="-2.361088623047" />
                  <Point X="-27.520173828125" Y="-2.352103027344" />
                  <Point X="-27.550859375" Y="-2.3480625" />
                  <Point X="-27.581794921875" Y="-2.349074707031" />
                  <Point X="-27.597220703125" Y="-2.350849121094" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.112453125" Y="-2.624743652344" />
                  <Point X="-28.7930859375" Y="-3.017707519531" />
                  <Point X="-28.897603515625" Y="-2.880395019531" />
                  <Point X="-29.004029296875" Y="-2.740573242188" />
                  <Point X="-29.181265625" Y="-2.443374023438" />
                  <Point X="-28.9205390625" Y="-2.243311035156" />
                  <Point X="-28.048123046875" Y="-1.573881835938" />
                  <Point X="-28.036486328125" Y="-1.563313720703" />
                  <Point X="-28.015111328125" Y="-1.540398803711" />
                  <Point X="-28.005373046875" Y="-1.528052001953" />
                  <Point X="-27.987408203125" Y="-1.500924682617" />
                  <Point X="-27.97983984375" Y="-1.48713671875" />
                  <Point X="-27.967083984375" Y="-1.458509399414" />
                  <Point X="-27.961896484375" Y="-1.443669799805" />
                  <Point X="-27.956734375" Y="-1.423745117188" />
                  <Point X="-27.956728515625" Y="-1.423728271484" />
                  <Point X="-27.951546875" Y="-1.398775512695" />
                  <Point X="-27.94874609375" Y="-1.368353637695" />
                  <Point X="-27.948576171875" Y="-1.353027832031" />
                  <Point X="-27.950787109375" Y="-1.321360717773" />
                  <Point X="-27.953083984375" Y="-1.306206787109" />
                  <Point X="-27.9600859375" Y="-1.276465454102" />
                  <Point X="-27.97178125" Y="-1.248234741211" />
                  <Point X="-27.98786328125" Y="-1.222254272461" />
                  <Point X="-27.996955078125" Y="-1.209917358398" />
                  <Point X="-28.01778515625" Y="-1.185963623047" />
                  <Point X="-28.0287421875" Y="-1.175246459961" />
                  <Point X="-28.0522421875" Y="-1.155711791992" />
                  <Point X="-28.06478515625" Y="-1.14689440918" />
                  <Point X="-28.0825234375" Y="-1.136454711914" />
                  <Point X="-28.105431640625" Y="-1.124481811523" />
                  <Point X="-28.13469140625" Y="-1.113258789062" />
                  <Point X="-28.149787109375" Y="-1.108861938477" />
                  <Point X="-28.18167578125" Y="-1.102379516602" />
                  <Point X="-28.197294921875" Y="-1.100532836914" />
                  <Point X="-28.22862109375" Y="-1.09944152832" />
                  <Point X="-28.244328125" Y="-1.100196655273" />
                  <Point X="-28.78182421875" Y="-1.170959106445" />
                  <Point X="-29.66091796875" Y="-1.286694213867" />
                  <Point X="-29.699140625" Y="-1.137056640625" />
                  <Point X="-29.740759765625" Y="-0.974114624023" />
                  <Point X="-29.786451171875" Y="-0.654654541016" />
                  <Point X="-29.502099609375" Y="-0.578462890625" />
                  <Point X="-28.508287109375" Y="-0.312171417236" />
                  <Point X="-28.50047265625" Y="-0.309711853027" />
                  <Point X="-28.473931640625" Y="-0.299251190186" />
                  <Point X="-28.444169921875" Y="-0.283894744873" />
                  <Point X="-28.43356640625" Y="-0.277516571045" />
                  <Point X="-28.4149765625" Y="-0.264615081787" />
                  <Point X="-28.393673828125" Y="-0.248245010376" />
                  <Point X="-28.371216796875" Y="-0.226360290527" />
                  <Point X="-28.360896484375" Y="-0.21448487854" />
                  <Point X="-28.34165234375" Y="-0.188229614258" />
                  <Point X="-28.3334375" Y="-0.174816833496" />
                  <Point X="-28.319326171875" Y="-0.146811386108" />
                  <Point X="-28.313431640625" Y="-0.132218704224" />
                  <Point X="-28.307236328125" Y="-0.112254043579" />
                  <Point X="-28.300986328125" Y="-0.08849281311" />
                  <Point X="-28.29671875" Y="-0.060337249756" />
                  <Point X="-28.295646484375" Y="-0.046100517273" />
                  <Point X="-28.295646484375" Y="-0.016459615707" />
                  <Point X="-28.29671875" Y="-0.002223030806" />
                  <Point X="-28.300986328125" Y="0.025932828903" />
                  <Point X="-28.304181640625" Y="0.03985225296" />
                  <Point X="-28.310376953125" Y="0.059816467285" />
                  <Point X="-28.319326171875" Y="0.084251548767" />
                  <Point X="-28.3334375" Y="0.112256858826" />
                  <Point X="-28.341654296875" Y="0.12566947937" />
                  <Point X="-28.3608984375" Y="0.151924743652" />
                  <Point X="-28.371216796875" Y="0.163800018311" />
                  <Point X="-28.393673828125" Y="0.185684875488" />
                  <Point X="-28.4058125" Y="0.195694625854" />
                  <Point X="-28.42440234375" Y="0.208596130371" />
                  <Point X="-28.44127734375" Y="0.219047302246" />
                  <Point X="-28.466009765625" Y="0.232628311157" />
                  <Point X="-28.47632421875" Y="0.237509979248" />
                  <Point X="-28.497462890625" Y="0.246001541138" />
                  <Point X="-28.508287109375" Y="0.249611297607" />
                  <Point X="-28.99823828125" Y="0.380893432617" />
                  <Point X="-29.7854453125" Y="0.591825012207" />
                  <Point X="-29.75815234375" Y="0.776273681641" />
                  <Point X="-29.73133203125" Y="0.957520996094" />
                  <Point X="-29.6335859375" Y="1.318237182617" />
                  <Point X="-29.490455078125" Y="1.299393676758" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815551758" />
                  <Point X="-28.7470546875" Y="1.204364379883" />
                  <Point X="-28.736701171875" Y="1.204703369141" />
                  <Point X="-28.715140625" Y="1.20658984375" />
                  <Point X="-28.70488671875" Y="1.208054077148" />
                  <Point X="-28.6846015625" Y="1.212089477539" />
                  <Point X="-28.6745703125" Y="1.214660644531" />
                  <Point X="-28.633427734375" Y="1.2276328125" />
                  <Point X="-28.61314453125" Y="1.234028320312" />
                  <Point X="-28.603451171875" Y="1.237676391602" />
                  <Point X="-28.584517578125" Y="1.246007324219" />
                  <Point X="-28.57527734375" Y="1.250689819336" />
                  <Point X="-28.556533203125" Y="1.26151184082" />
                  <Point X="-28.547859375" Y="1.26717199707" />
                  <Point X="-28.531177734375" Y="1.279403564453" />
                  <Point X="-28.51592578125" Y="1.29337878418" />
                  <Point X="-28.502287109375" Y="1.308931274414" />
                  <Point X="-28.495892578125" Y="1.317079711914" />
                  <Point X="-28.483478515625" Y="1.334809570312" />
                  <Point X="-28.478009765625" Y="1.343603881836" />
                  <Point X="-28.468060546875" Y="1.361738891602" />
                  <Point X="-28.463580078125" Y="1.371079467773" />
                  <Point X="-28.447072265625" Y="1.410934814453" />
                  <Point X="-28.43893359375" Y="1.430583618164" />
                  <Point X="-28.4355" Y="1.440348754883" />
                  <Point X="-28.429712890625" Y="1.460199829102" />
                  <Point X="-28.427359375" Y="1.470285888672" />
                  <Point X="-28.423599609375" Y="1.491601074219" />
                  <Point X="-28.422359375" Y="1.501887573242" />
                  <Point X="-28.421005859375" Y="1.522530273438" />
                  <Point X="-28.421908203125" Y="1.54319519043" />
                  <Point X="-28.4250546875" Y="1.563641479492" />
                  <Point X="-28.427185546875" Y="1.573779052734" />
                  <Point X="-28.432787109375" Y="1.594685302734" />
                  <Point X="-28.436009765625" Y="1.604531738281" />
                  <Point X="-28.4435078125" Y="1.623812011719" />
                  <Point X="-28.447783203125" Y="1.633245605469" />
                  <Point X="-28.467703125" Y="1.671510498047" />
                  <Point X="-28.4775234375" Y="1.690375244141" />
                  <Point X="-28.48280078125" Y="1.699288330078" />
                  <Point X="-28.494294921875" Y="1.716490112305" />
                  <Point X="-28.50051171875" Y="1.724778808594" />
                  <Point X="-28.51442578125" Y="1.741359130859" />
                  <Point X="-28.5215078125" Y="1.748918579102" />
                  <Point X="-28.536447265625" Y="1.763218383789" />
                  <Point X="-28.5443046875" Y="1.769958862305" />
                  <Point X="-28.825341796875" Y="1.985606811523" />
                  <Point X="-29.22761328125" Y="2.294281005859" />
                  <Point X="-29.1065078125" Y="2.501765625" />
                  <Point X="-29.00228515625" Y="2.680321044922" />
                  <Point X="-28.73471484375" Y="3.024246826172" />
                  <Point X="-28.726337890625" Y="3.035013427734" />
                  <Point X="-28.691685546875" Y="3.015006591797" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.097771484375" Y="2.726144775391" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310546875" />
                  <Point X="-27.976431640625" Y="2.734227294922" />
                  <Point X="-27.95698828125" Y="2.7413046875" />
                  <Point X="-27.9384375" Y="2.750454345703" />
                  <Point X="-27.929416015625" Y="2.755533691406" />
                  <Point X="-27.911162109375" Y="2.767163330078" />
                  <Point X="-27.90274609375" Y="2.773194091797" />
                  <Point X="-27.8866171875" Y="2.786138427734" />
                  <Point X="-27.878904296875" Y="2.793052001953" />
                  <Point X="-27.83823046875" Y="2.833724121094" />
                  <Point X="-27.8181796875" Y="2.853775390625" />
                  <Point X="-27.811263671875" Y="2.861489990234" />
                  <Point X="-27.79831640625" Y="2.877622314453" />
                  <Point X="-27.79228515625" Y="2.886040039062" />
                  <Point X="-27.78065625" Y="2.904293457031" />
                  <Point X="-27.775576171875" Y="2.913317138672" />
                  <Point X="-27.766427734375" Y="2.931866699219" />
                  <Point X="-27.759353515625" Y="2.951298095703" />
                  <Point X="-27.754435546875" Y="2.971387695312" />
                  <Point X="-27.7525234375" Y="2.981571777344" />
                  <Point X="-27.749697265625" Y="3.003029785156" />
                  <Point X="-27.748908203125" Y="3.01336328125" />
                  <Point X="-27.74845703125" Y="3.034049072266" />
                  <Point X="-27.748794921875" Y="3.044401367188" />
                  <Point X="-27.75380859375" Y="3.101701660156" />
                  <Point X="-27.756279296875" Y="3.129950927734" />
                  <Point X="-27.757744140625" Y="3.140209228516" />
                  <Point X="-27.76178125" Y="3.160499023438" />
                  <Point X="-27.764353515625" Y="3.170530517578" />
                  <Point X="-27.77086328125" Y="3.191174072266" />
                  <Point X="-27.77451171875" Y="3.200867675781" />
                  <Point X="-27.782841796875" Y="3.219797363281" />
                  <Point X="-27.7875234375" Y="3.229033447266" />
                  <Point X="-27.912060546875" Y="3.444736816406" />
                  <Point X="-28.05938671875" Y="3.699914550781" />
                  <Point X="-27.829875" Y="3.87587890625" />
                  <Point X="-27.648365234375" Y="4.015041748047" />
                  <Point X="-27.226951171875" Y="4.249169921875" />
                  <Point X="-27.1925234375" Y="4.268296386719" />
                  <Point X="-27.118564453125" Y="4.171909667969" />
                  <Point X="-27.111822265625" Y="4.164050292969" />
                  <Point X="-27.09751953125" Y="4.149108398438" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.06508984375" Y="4.121895996094" />
                  <Point X="-27.047888671875" Y="4.110403320312" />
                  <Point X="-27.0389765625" Y="4.105128417969" />
                  <Point X="-26.975201171875" Y="4.071929199219" />
                  <Point X="-26.943759765625" Y="4.055561523438" />
                  <Point X="-26.934326171875" Y="4.051286865234" />
                  <Point X="-26.915048828125" Y="4.043790283203" />
                  <Point X="-26.905205078125" Y="4.040568359375" />
                  <Point X="-26.884298828125" Y="4.034966308594" />
                  <Point X="-26.8741640625" Y="4.032834960938" />
                  <Point X="-26.853720703125" Y="4.029687988281" />
                  <Point X="-26.8330546875" Y="4.028785400391" />
                  <Point X="-26.812416015625" Y="4.030137939453" />
                  <Point X="-26.802134765625" Y="4.031377685547" />
                  <Point X="-26.780818359375" Y="4.035135986328" />
                  <Point X="-26.770728515625" Y="4.037488037109" />
                  <Point X="-26.7508671875" Y="4.043277099609" />
                  <Point X="-26.741095703125" Y="4.046713623047" />
                  <Point X="-26.674669921875" Y="4.074228759766" />
                  <Point X="-26.641921875" Y="4.087793212891" />
                  <Point X="-26.63258203125" Y="4.092273681641" />
                  <Point X="-26.614447265625" Y="4.102223632812" />
                  <Point X="-26.60565234375" Y="4.107692871094" />
                  <Point X="-26.587923828125" Y="4.120106933594" />
                  <Point X="-26.579779296875" Y="4.126498535156" />
                  <Point X="-26.564228515625" Y="4.140135253906" />
                  <Point X="-26.55025390625" Y="4.155384277344" />
                  <Point X="-26.5380234375" Y="4.172062988281" />
                  <Point X="-26.532361328125" Y="4.180737792969" />
                  <Point X="-26.5215390625" Y="4.199480957031" />
                  <Point X="-26.51685546875" Y="4.208725585937" />
                  <Point X="-26.5085234375" Y="4.227663085938" />
                  <Point X="-26.504875" Y="4.237355957031" />
                  <Point X="-26.483255859375" Y="4.305926757812" />
                  <Point X="-26.472595703125" Y="4.339732421875" />
                  <Point X="-26.470025390625" Y="4.349763671875" />
                  <Point X="-26.465990234375" Y="4.370048828125" />
                  <Point X="-26.464525390625" Y="4.380302734375" />
                  <Point X="-26.462638671875" Y="4.401866210938" />
                  <Point X="-26.46230078125" Y="4.41221875" />
                  <Point X="-26.462751953125" Y="4.432898925781" />
                  <Point X="-26.463541015625" Y="4.4432265625" />
                  <Point X="-26.47769921875" Y="4.550770507812" />
                  <Point X="-26.479263671875" Y="4.562655273438" />
                  <Point X="-26.166140625" Y="4.650443847656" />
                  <Point X="-25.931169921875" Y="4.716320800781" />
                  <Point X="-25.420291015625" Y="4.776111816406" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.3551015625" Y="4.74478125" />
                  <Point X="-25.22566796875" Y="4.261728515625" />
                  <Point X="-25.220435546875" Y="4.247106933594" />
                  <Point X="-25.20766015625" Y="4.218912597656" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218911621094" />
                  <Point X="-24.767251953125" Y="4.247104980469" />
                  <Point X="-24.76201953125" Y="4.2617265625" />
                  <Point X="-24.69820703125" Y="4.499872558594" />
                  <Point X="-24.621806640625" Y="4.785005859375" />
                  <Point X="-24.37729296875" Y="4.759398925781" />
                  <Point X="-24.172123046875" Y="4.737912109375" />
                  <Point X="-23.7494453125" Y="4.635864746094" />
                  <Point X="-23.546404296875" Y="4.58684375" />
                  <Point X="-23.2727421875" Y="4.487585449219" />
                  <Point X="-23.1417421875" Y="4.440070800781" />
                  <Point X="-22.87570703125" Y="4.315655273438" />
                  <Point X="-22.74954296875" Y="4.256651367187" />
                  <Point X="-22.49249609375" Y="4.106895996094" />
                  <Point X="-22.3705703125" Y="4.035861572266" />
                  <Point X="-22.18221875" Y="3.901915527344" />
                  <Point X="-22.357578125" Y="3.598183837891" />
                  <Point X="-22.9346875" Y="2.598598388672" />
                  <Point X="-22.93762109375" Y="2.593110107422" />
                  <Point X="-22.9468125" Y="2.573448974609" />
                  <Point X="-22.955814453125" Y="2.549571777344" />
                  <Point X="-22.958697265625" Y="2.540601318359" />
                  <Point X="-22.973078125" Y="2.486826171875" />
                  <Point X="-22.98016796875" Y="2.460314941406" />
                  <Point X="-22.9820859375" Y="2.451470214844" />
                  <Point X="-22.986353515625" Y="2.420223632812" />
                  <Point X="-22.987244140625" Y="2.383240722656" />
                  <Point X="-22.986587890625" Y="2.369580078125" />
                  <Point X="-22.98098046875" Y="2.323080078125" />
                  <Point X="-22.978216796875" Y="2.300155273438" />
                  <Point X="-22.97619921875" Y="2.289033447266" />
                  <Point X="-22.97085546875" Y="2.267108154297" />
                  <Point X="-22.967529296875" Y="2.2563046875" />
                  <Point X="-22.95926171875" Y="2.234214111328" />
                  <Point X="-22.9546796875" Y="2.223887207031" />
                  <Point X="-22.944318359375" Y="2.203843994141" />
                  <Point X="-22.9385390625" Y="2.194127685547" />
                  <Point X="-22.909765625" Y="2.151724121094" />
                  <Point X="-22.895580078125" Y="2.130819091797" />
                  <Point X="-22.890185546875" Y="2.123628417969" />
                  <Point X="-22.86953125" Y="2.100120361328" />
                  <Point X="-22.8423984375" Y="2.075385986328" />
                  <Point X="-22.831740234375" Y="2.066982177734" />
                  <Point X="-22.789337890625" Y="2.038209350586" />
                  <Point X="-22.768431640625" Y="2.024024536133" />
                  <Point X="-22.758720703125" Y="2.018246704102" />
                  <Point X="-22.73867578125" Y="2.007883056641" />
                  <Point X="-22.728341796875" Y="2.003297607422" />
                  <Point X="-22.706255859375" Y="1.995032470703" />
                  <Point X="-22.695451171875" Y="1.991707519531" />
                  <Point X="-22.67352734375" Y="1.986364868164" />
                  <Point X="-22.662408203125" Y="1.984346801758" />
                  <Point X="-22.615908203125" Y="1.978739624023" />
                  <Point X="-22.592984375" Y="1.975975219727" />
                  <Point X="-22.58395703125" Y="1.975320922852" />
                  <Point X="-22.55242578125" Y="1.975497070312" />
                  <Point X="-22.515685546875" Y="1.979822387695" />
                  <Point X="-22.50225" Y="1.982395751953" />
                  <Point X="-22.448474609375" Y="1.996776123047" />
                  <Point X="-22.421962890625" Y="2.003865478516" />
                  <Point X="-22.416" Y="2.005671875" />
                  <Point X="-22.39559765625" Y="2.01306640625" />
                  <Point X="-22.372345703125" Y="2.023573486328" />
                  <Point X="-22.36396484375" Y="2.027872680664" />
                  <Point X="-21.87116796875" Y="2.312389404297" />
                  <Point X="-21.059595703125" Y="2.780950683594" />
                  <Point X="-21.028375" Y="2.737561035156" />
                  <Point X="-20.956041015625" Y="2.637031494141" />
                  <Point X="-20.863115234375" Y="2.483470703125" />
                  <Point X="-21.0672265625" Y="2.326850830078" />
                  <Point X="-21.827046875" Y="1.743820068359" />
                  <Point X="-21.831861328125" Y="1.739868530273" />
                  <Point X="-21.84787109375" Y="1.725224731445" />
                  <Point X="-21.865328125" Y="1.706606811523" />
                  <Point X="-21.87142578125" Y="1.699420776367" />
                  <Point X="-21.910126953125" Y="1.648931030273" />
                  <Point X="-21.92920703125" Y="1.624039550781" />
                  <Point X="-21.9343671875" Y="1.61659777832" />
                  <Point X="-21.95026171875" Y="1.589366333008" />
                  <Point X="-21.965234375" Y="1.555545898438" />
                  <Point X="-21.96985546875" Y="1.542677001953" />
                  <Point X="-21.9842734375" Y="1.491126708984" />
                  <Point X="-21.991380859375" Y="1.465712402344" />
                  <Point X="-21.993771484375" Y="1.454669555664" />
                  <Point X="-21.9972265625" Y="1.432369995117" />
                  <Point X="-21.998291015625" Y="1.421113037109" />
                  <Point X="-21.999107421875" Y="1.397543579102" />
                  <Point X="-21.998826171875" Y="1.386239013672" />
                  <Point X="-21.996921875" Y="1.363750732422" />
                  <Point X="-21.995298828125" Y="1.352567138672" />
                  <Point X="-21.983462890625" Y="1.2952109375" />
                  <Point X="-21.97762890625" Y="1.266934448242" />
                  <Point X="-21.97540234375" Y="1.258238769531" />
                  <Point X="-21.96531640625" Y="1.228608032227" />
                  <Point X="-21.949712890625" Y="1.195370605469" />
                  <Point X="-21.943080078125" Y="1.183525878906" />
                  <Point X="-21.910890625" Y="1.134600708008" />
                  <Point X="-21.8950234375" Y="1.11048046875" />
                  <Point X="-21.888263671875" Y="1.101427856445" />
                  <Point X="-21.87371484375" Y="1.08418762207" />
                  <Point X="-21.86592578125" Y="1.075999633789" />
                  <Point X="-21.848681640625" Y="1.059908081055" />
                  <Point X="-21.83996875" Y="1.05269934082" />
                  <Point X="-21.82175390625" Y="1.039369873047" />
                  <Point X="-21.812251953125" Y="1.033249633789" />
                  <Point X="-21.76560546875" Y="1.006991943359" />
                  <Point X="-21.742609375" Y="0.994047058105" />
                  <Point X="-21.734521484375" Y="0.989988769531" />
                  <Point X="-21.7053203125" Y="0.978083618164" />
                  <Point X="-21.66972265625" Y="0.968020935059" />
                  <Point X="-21.656328125" Y="0.965257629395" />
                  <Point X="-21.593259765625" Y="0.956922241211" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.555966796875" Y="0.952199768066" />
                  <Point X="-21.53428125" Y="0.951222839355" />
                  <Point X="-21.50878125" Y="0.952032409668" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-21.031271484375" Y="1.014427124023" />
                  <Point X="-20.295296875" Y="1.111319702148" />
                  <Point X="-20.278380859375" Y="1.041832885742" />
                  <Point X="-20.2473125" Y="0.914210144043" />
                  <Point X="-20.21612890625" Y="0.713921081543" />
                  <Point X="-20.437236328125" Y="0.654675048828" />
                  <Point X="-21.3080078125" Y="0.421352752686" />
                  <Point X="-21.31396875" Y="0.419544311523" />
                  <Point X="-21.334380859375" Y="0.412135162354" />
                  <Point X="-21.35762109375" Y="0.401618011475" />
                  <Point X="-21.365994140625" Y="0.397316711426" />
                  <Point X="-21.42795703125" Y="0.361501281738" />
                  <Point X="-21.45850390625" Y="0.343844299316" />
                  <Point X="-21.466115234375" Y="0.338947021484" />
                  <Point X="-21.491224609375" Y="0.319871551514" />
                  <Point X="-21.518005859375" Y="0.294351379395" />
                  <Point X="-21.527203125" Y="0.284226898193" />
                  <Point X="-21.564380859375" Y="0.236854248047" />
                  <Point X="-21.582708984375" Y="0.213499359131" />
                  <Point X="-21.589146484375" Y="0.20420715332" />
                  <Point X="-21.60087109375" Y="0.184925720215" />
                  <Point X="-21.606158203125" Y="0.174936325073" />
                  <Point X="-21.615931640625" Y="0.15347265625" />
                  <Point X="-21.619994140625" Y="0.14292666626" />
                  <Point X="-21.62683984375" Y="0.121426742554" />
                  <Point X="-21.629623046875" Y="0.110472938538" />
                  <Point X="-21.642015625" Y="0.045764026642" />
                  <Point X="-21.648125" Y="0.01386227417" />
                  <Point X="-21.649396484375" Y="0.004966154575" />
                  <Point X="-21.6514140625" Y="-0.026262037277" />
                  <Point X="-21.64971875" Y="-0.062939785004" />
                  <Point X="-21.648125" Y="-0.076422409058" />
                  <Point X="-21.635732421875" Y="-0.141131317139" />
                  <Point X="-21.629623046875" Y="-0.173033065796" />
                  <Point X="-21.62683984375" Y="-0.183986877441" />
                  <Point X="-21.619994140625" Y="-0.205486801147" />
                  <Point X="-21.615931640625" Y="-0.216032791138" />
                  <Point X="-21.606158203125" Y="-0.237496459961" />
                  <Point X="-21.60087109375" Y="-0.247485839844" />
                  <Point X="-21.589146484375" Y="-0.266767272949" />
                  <Point X="-21.582708984375" Y="-0.27605947876" />
                  <Point X="-21.54553125" Y="-0.323432128906" />
                  <Point X="-21.527203125" Y="-0.346787322998" />
                  <Point X="-21.521279296875" Y="-0.353634979248" />
                  <Point X="-21.498859375" Y="-0.375806091309" />
                  <Point X="-21.469822265625" Y="-0.398725372314" />
                  <Point X="-21.45850390625" Y="-0.406404418945" />
                  <Point X="-21.396541015625" Y="-0.442219696045" />
                  <Point X="-21.365994140625" Y="-0.459876708984" />
                  <Point X="-21.360505859375" Y="-0.462813201904" />
                  <Point X="-21.340845703125" Y="-0.472014434814" />
                  <Point X="-21.31697265625" Y="-0.48102722168" />
                  <Point X="-21.3080078125" Y="-0.483912872314" />
                  <Point X="-20.87871484375" Y="-0.598941345215" />
                  <Point X="-20.21512109375" Y="-0.776751098633" />
                  <Point X="-20.2210390625" Y="-0.816008911133" />
                  <Point X="-20.238380859375" Y="-0.931037414551" />
                  <Point X="-20.2721953125" Y="-1.079219604492" />
                  <Point X="-20.5533671875" Y="-1.042202758789" />
                  <Point X="-21.563216796875" Y="-0.909253540039" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042419434" />
                  <Point X="-21.63327734375" Y="-0.910840820312" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.76712890625" Y="-0.939108642578" />
                  <Point X="-21.82708203125" Y="-0.952140014648" />
                  <Point X="-21.842125" Y="-0.956742553711" />
                  <Point X="-21.8712421875" Y="-0.968365905762" />
                  <Point X="-21.88531640625" Y="-0.97538659668" />
                  <Point X="-21.91314453125" Y="-0.992278747559" />
                  <Point X="-21.925869140625" Y="-1.001525939941" />
                  <Point X="-21.949623046875" Y="-1.02199798584" />
                  <Point X="-21.96065234375" Y="-1.033222900391" />
                  <Point X="-22.034158203125" Y="-1.121627441406" />
                  <Point X="-22.070396484375" Y="-1.165211181641" />
                  <Point X="-22.0786796875" Y="-1.176854248047" />
                  <Point X="-22.093400390625" Y="-1.201241210938" />
                  <Point X="-22.099837890625" Y="-1.213984985352" />
                  <Point X="-22.1111796875" Y="-1.241370483398" />
                  <Point X="-22.11563671875" Y="-1.254935058594" />
                  <Point X="-22.122466796875" Y="-1.282580200195" />
                  <Point X="-22.12483984375" Y="-1.296660888672" />
                  <Point X="-22.135375" Y="-1.411148681641" />
                  <Point X="-22.140568359375" Y="-1.467591064453" />
                  <Point X="-22.140708984375" Y="-1.483321044922" />
                  <Point X="-22.138390625" Y="-1.514587036133" />
                  <Point X="-22.135931640625" Y="-1.530122924805" />
                  <Point X="-22.128201171875" Y="-1.561743286133" />
                  <Point X="-22.12321484375" Y="-1.576661743164" />
                  <Point X="-22.11084375" Y="-1.605475708008" />
                  <Point X="-22.103458984375" Y="-1.619371459961" />
                  <Point X="-22.036158203125" Y="-1.724053466797" />
                  <Point X="-22.002978515625" Y="-1.775661987305" />
                  <Point X="-21.99825390625" Y="-1.782357055664" />
                  <Point X="-21.98019921875" Y="-1.804454833984" />
                  <Point X="-21.956505859375" Y="-1.828121948242" />
                  <Point X="-21.947201171875" Y="-1.836277709961" />
                  <Point X="-21.54881640625" Y="-2.141969482422" />
                  <Point X="-20.912828125" Y="-2.629981201172" />
                  <Point X="-20.954498046875" Y="-2.697408203125" />
                  <Point X="-20.9987265625" Y="-2.760250732422" />
                  <Point X="-21.25213671875" Y="-2.613943603516" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.184970703125" Y="-2.079513916016" />
                  <Point X="-22.216876953125" Y="-2.069326416016" />
                  <Point X="-22.228888671875" Y="-2.066337646484" />
                  <Point X="-22.373623046875" Y="-2.040198608398" />
                  <Point X="-22.444978515625" Y="-2.027312011719" />
                  <Point X="-22.460638671875" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403442383" />
                  <Point X="-22.507685546875" Y="-2.02650390625" />
                  <Point X="-22.539859375" Y="-2.031461791992" />
                  <Point X="-22.555154296875" Y="-2.03513659668" />
                  <Point X="-22.5849296875" Y="-2.044960083008" />
                  <Point X="-22.59941015625" Y="-2.051108886719" />
                  <Point X="-22.719650390625" Y="-2.114390136719" />
                  <Point X="-22.778927734375" Y="-2.145587890625" />
                  <Point X="-22.79103125" Y="-2.153170898438" />
                  <Point X="-22.8139609375" Y="-2.170063476562" />
                  <Point X="-22.824787109375" Y="-2.179373046875" />
                  <Point X="-22.84574609375" Y="-2.200331787109" />
                  <Point X="-22.855056640625" Y="-2.211157958984" />
                  <Point X="-22.871951171875" Y="-2.234089111328" />
                  <Point X="-22.87953515625" Y="-2.246194091797" />
                  <Point X="-22.94281640625" Y="-2.36643359375" />
                  <Point X="-22.974015625" Y="-2.425712158203" />
                  <Point X="-22.9801640625" Y="-2.440192871094" />
                  <Point X="-22.98998828125" Y="-2.469971191406" />
                  <Point X="-22.9936640625" Y="-2.485268798828" />
                  <Point X="-22.99862109375" Y="-2.517442382812" />
                  <Point X="-22.999720703125" Y="-2.533133544922" />
                  <Point X="-22.99931640625" Y="-2.564484375" />
                  <Point X="-22.9978125" Y="-2.580144042969" />
                  <Point X="-22.971671875" Y="-2.724879394531" />
                  <Point X="-22.95878515625" Y="-2.796234375" />
                  <Point X="-22.95698046875" Y="-2.804230957031" />
                  <Point X="-22.948763671875" Y="-2.831534912109" />
                  <Point X="-22.935931640625" Y="-2.862473876953" />
                  <Point X="-22.930453125" Y="-2.873578613281" />
                  <Point X="-22.674451171875" Y="-3.31698828125" />
                  <Point X="-22.264107421875" Y="-4.02772265625" />
                  <Point X="-22.276244140625" Y="-4.036082275391" />
                  <Point X="-22.479751953125" Y="-3.770865722656" />
                  <Point X="-23.16608203125" Y="-2.876422851562" />
                  <Point X="-23.171345703125" Y="-2.870144042969" />
                  <Point X="-23.191169921875" Y="-2.849624023438" />
                  <Point X="-23.216748046875" Y="-2.828003662109" />
                  <Point X="-23.22669921875" Y="-2.820646728516" />
                  <Point X="-23.36944921875" Y="-2.728872802734" />
                  <Point X="-23.43982421875" Y="-2.683627929688" />
                  <Point X="-23.453716796875" Y="-2.676245605469" />
                  <Point X="-23.48252734375" Y="-2.663874511719" />
                  <Point X="-23.4974453125" Y="-2.658885742188" />
                  <Point X="-23.52906640625" Y="-2.651154052734" />
                  <Point X="-23.54460546875" Y="-2.648695800781" />
                  <Point X="-23.575875" Y="-2.646376953125" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.747724609375" Y="-2.660882568359" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670339111328" />
                  <Point X="-23.866421875" Y="-2.677170410156" />
                  <Point X="-23.87998828125" Y="-2.681627929688" />
                  <Point X="-23.907373046875" Y="-2.692970703125" />
                  <Point X="-23.92012109375" Y="-2.699412841797" />
                  <Point X="-23.94450390625" Y="-2.714134033203" />
                  <Point X="-23.956138671875" Y="-2.722413085938" />
                  <Point X="-24.076689453125" Y="-2.822647705078" />
                  <Point X="-24.136123046875" Y="-2.872063476562" />
                  <Point X="-24.14734375" Y="-2.883086669922" />
                  <Point X="-24.167814453125" Y="-2.906835693359" />
                  <Point X="-24.177064453125" Y="-2.919561523438" />
                  <Point X="-24.19395703125" Y="-2.947387695312" />
                  <Point X="-24.20098046875" Y="-2.961466552734" />
                  <Point X="-24.21260546875" Y="-2.990588623047" />
                  <Point X="-24.21720703125" Y="-3.005631835938" />
                  <Point X="-24.25325" Y="-3.171464111328" />
                  <Point X="-24.271021484375" Y="-3.253219726562" />
                  <Point X="-24.2724140625" Y="-3.261286621094" />
                  <Point X="-24.275275390625" Y="-3.289676757812" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520263672" />
                  <Point X="-24.20189453125" Y="-3.886586181641" />
                  <Point X="-24.166908203125" Y="-4.152329589844" />
                  <Point X="-24.344931640625" Y="-3.487936767578" />
                  <Point X="-24.347392578125" Y="-3.480121582031" />
                  <Point X="-24.357853515625" Y="-3.453578613281" />
                  <Point X="-24.3732109375" Y="-3.423815429688" />
                  <Point X="-24.37958984375" Y="-3.413209716797" />
                  <Point X="-24.48925390625" Y="-3.255205566406" />
                  <Point X="-24.543318359375" Y="-3.177309326172" />
                  <Point X="-24.553328125" Y="-3.165172119141" />
                  <Point X="-24.575212890625" Y="-3.142716308594" />
                  <Point X="-24.587087890625" Y="-3.132397705078" />
                  <Point X="-24.61334375" Y="-3.113153808594" />
                  <Point X="-24.6267578125" Y="-3.104937255859" />
                  <Point X="-24.6547578125" Y="-3.090829589844" />
                  <Point X="-24.66934375" Y="-3.084938476562" />
                  <Point X="-24.839041015625" Y="-3.032270507812" />
                  <Point X="-24.922703125" Y="-3.006305175781" />
                  <Point X="-24.936623046875" Y="-3.003109130859" />
                  <Point X="-24.964783203125" Y="-2.99883984375" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.234681640625" Y="-3.058972900391" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.3329296875" Y="-3.090829589844" />
                  <Point X="-25.3609296875" Y="-3.104937255859" />
                  <Point X="-25.37434375" Y="-3.113153808594" />
                  <Point X="-25.400599609375" Y="-3.132397705078" />
                  <Point X="-25.412474609375" Y="-3.142717041016" />
                  <Point X="-25.434359375" Y="-3.165173339844" />
                  <Point X="-25.444369140625" Y="-3.177310302734" />
                  <Point X="-25.55403125" Y="-3.335314208984" />
                  <Point X="-25.608095703125" Y="-3.413210693359" />
                  <Point X="-25.612470703125" Y="-3.420132568359" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213378906" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.775029296875" Y="-3.981594970703" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.334880639465" Y="-3.959665700048" />
                  <Point X="-22.310794324486" Y="-3.9468587792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.393517138305" Y="-3.883249124705" />
                  <Point X="-22.358323372424" Y="-3.864536267451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.452153637145" Y="-3.806832549363" />
                  <Point X="-22.405852420362" Y="-3.782213755702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.510790134199" Y="-3.73041597307" />
                  <Point X="-22.4533814683" Y="-3.699891243953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.569426629666" Y="-3.653999395934" />
                  <Point X="-22.500910516238" Y="-3.617568732204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.628063125132" Y="-3.577582818798" />
                  <Point X="-22.548439564176" Y="-3.535246220455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.042909403175" Y="-2.7347416342" />
                  <Point X="-20.945591923637" Y="-2.682997012464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.686699620599" Y="-3.501166241662" />
                  <Point X="-22.595968612114" Y="-3.452923708706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.139923012162" Y="-2.678730530282" />
                  <Point X="-20.968253129357" Y="-2.587452034463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.745336116065" Y="-3.424749664525" />
                  <Point X="-22.643497660052" Y="-3.370601196957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.236936621148" Y="-2.622719426364" />
                  <Point X="-21.051079181013" Y="-2.5238972725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.185509333809" Y="-4.08290919281" />
                  <Point X="-24.176666714779" Y="-4.078207488872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.803972611532" Y="-3.348333087389" />
                  <Point X="-22.691026638442" Y="-3.288278648229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.333950486705" Y="-2.566708458867" />
                  <Point X="-21.133905232669" Y="-2.460342510538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.210743893221" Y="-3.988732491238" />
                  <Point X="-24.189905250989" Y="-3.97765238862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.862609106998" Y="-3.271916510253" />
                  <Point X="-22.738555486957" Y="-3.205956030445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.43096439993" Y="-2.510697516716" />
                  <Point X="-21.216731284325" Y="-2.396787748575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.235978452633" Y="-3.894555789666" />
                  <Point X="-24.203143794396" Y="-3.877097292196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.921245602465" Y="-3.195499933116" />
                  <Point X="-22.786084335472" Y="-3.123633412661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.527978313155" Y="-2.454686574565" />
                  <Point X="-21.299557335981" Y="-2.333232986612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.053827652095" Y="-4.753529199542" />
                  <Point X="-25.969914916867" Y="-4.708912006785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.261213012045" Y="-3.800379088093" />
                  <Point X="-24.216382406877" Y="-3.776542232499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.979882097932" Y="-3.11908335598" />
                  <Point X="-22.833613183988" Y="-3.041310794877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.62499222638" Y="-2.398675632413" />
                  <Point X="-21.382383387637" Y="-2.26967822465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.135737112617" Y="-4.689487077429" />
                  <Point X="-25.936295006633" Y="-4.583441828607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.286447571457" Y="-3.706202386521" />
                  <Point X="-24.229621019359" Y="-3.675987172802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.038518593398" Y="-3.042666778844" />
                  <Point X="-22.881142032503" Y="-2.958988177093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.722006139605" Y="-2.342664690262" />
                  <Point X="-21.465209439293" Y="-2.206123462687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.120593499953" Y="-4.573840920931" />
                  <Point X="-25.902675096399" Y="-4.457971650428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.311682130869" Y="-3.612025684949" />
                  <Point X="-24.24285963184" Y="-3.575432113105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.097155088865" Y="-2.966250201707" />
                  <Point X="-22.928670881018" Y="-2.876665559309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.81902005283" Y="-2.286653748111" />
                  <Point X="-21.548035490949" Y="-2.142568700724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.128728025195" Y="-4.470571969908" />
                  <Point X="-25.869055186165" Y="-4.33250147225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.336916690281" Y="-3.517848983376" />
                  <Point X="-24.256098244321" Y="-3.474877053408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.155791584331" Y="-2.889833624571" />
                  <Point X="-22.960622573693" Y="-2.786060420847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.916033966055" Y="-2.23064280596" />
                  <Point X="-21.630861554879" Y="-2.079013945287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.148082926082" Y="-4.373268998444" />
                  <Point X="-25.835435275931" Y="-4.207031294071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.370887863891" Y="-3.428317621975" />
                  <Point X="-24.269336856803" Y="-3.374321993711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.227299419081" Y="-2.82026085993" />
                  <Point X="-22.978351954649" Y="-2.687893145103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.01304787928" Y="-2.174631863808" />
                  <Point X="-21.713687618925" Y="-2.015459189913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.167438073078" Y="-4.275966157837" />
                  <Point X="-25.801815365696" Y="-4.081561115893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.42413171248" Y="-3.349033723632" />
                  <Point X="-24.273167754492" Y="-3.268764763328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.31889938712" Y="-2.76137127206" />
                  <Point X="-22.996081876199" Y="-2.589726156798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.110061792505" Y="-2.118620921657" />
                  <Point X="-21.796513682971" Y="-1.951904434538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.187901274778" Y="-4.179252480368" />
                  <Point X="-25.768195520694" Y="-3.956090972399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.478678509042" Y="-3.270442615015" />
                  <Point X="-24.248117861333" Y="-3.147851344058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.410498953014" Y="-2.702481470367" />
                  <Point X="-22.992445785174" Y="-2.480198658091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.21880515541" Y="-2.068846638528" />
                  <Point X="-21.879339747018" Y="-1.888349679163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.340640319358" Y="-1.070208680985" />
                  <Point X="-20.260403670287" Y="-1.027546097909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.230868073193" Y="-4.094504177518" />
                  <Point X="-25.734575931378" Y="-3.830620964855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.533225267346" Y="-3.191851486056" />
                  <Point X="-24.221677085597" Y="-3.026198379403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.520336024934" Y="-2.653288722638" />
                  <Point X="-22.928041545729" Y="-2.338360161723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.368889137817" Y="-2.0410535527" />
                  <Point X="-21.960645717586" Y="-1.823986675749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.502835687087" Y="-1.048855332963" />
                  <Point X="-20.234644743402" Y="-0.906255678719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.301358903378" Y="-4.024390661957" />
                  <Point X="-25.700956342062" Y="-3.705150957312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.602508094407" Y="-3.121095663842" />
                  <Point X="-24.13845472202" Y="-2.874354108949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.734723011705" Y="-2.659686150714" />
                  <Point X="-22.814097422686" Y="-2.170180842003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.568228580702" Y="-2.039450059769" />
                  <Point X="-22.020591523743" Y="-1.748266271457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.66503097541" Y="-1.02750194272" />
                  <Point X="-20.217010573208" Y="-0.789285269292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.377737389918" Y="-3.957407668811" />
                  <Point X="-25.667336752746" Y="-3.579680949768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.711976382772" Y="-3.071706830417" />
                  <Point X="-22.072142483912" Y="-1.668082248374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.8272262278" Y="-1.00614853337" />
                  <Point X="-20.335253076367" Y="-0.74456176863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.454116300531" Y="-3.890424901148" />
                  <Point X="-25.62853735956" Y="-3.451456791653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.839749233748" Y="-3.032050705576" />
                  <Point X="-22.119376116871" Y="-1.585602661694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.989421480189" Y="-0.984795124021" />
                  <Point X="-20.469803097869" Y="-0.708509129277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.537647384477" Y="-3.827245011504" />
                  <Point X="-25.519858285446" Y="-3.286076948107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.977798950738" Y="-2.997858887322" />
                  <Point X="-22.140278797252" Y="-1.489122659184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.151616732579" Y="-0.963441714672" />
                  <Point X="-20.60435311937" Y="-0.672456489924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.676648086523" Y="-3.793558840974" />
                  <Point X="-22.132256857712" Y="-1.377263163456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.313811984968" Y="-0.942088305323" />
                  <Point X="-20.738903140872" Y="-0.636403850571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.535373602641" Y="-4.142557142287" />
                  <Point X="-27.455204458782" Y="-4.099930452369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.856796492023" Y="-3.781751292462" />
                  <Point X="-22.117313942398" Y="-1.261723719631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.476007237358" Y="-0.920734895973" />
                  <Point X="-20.873453162373" Y="-0.600351211218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.628862287169" Y="-4.084671802789" />
                  <Point X="-22.016702228253" Y="-1.100633367469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.67543642762" Y="-0.919179122569" />
                  <Point X="-21.008003354835" Y="-0.564298662767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.722350690217" Y="-4.026786313625" />
                  <Point X="-21.142553554255" Y="-0.528246118014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.807933128238" Y="-3.964697148289" />
                  <Point X="-21.277103753674" Y="-0.492193573262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.890591253209" Y="-3.901053098125" />
                  <Point X="-21.39123739171" Y="-0.44528535026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.973249378181" Y="-3.83740904796" />
                  <Point X="-21.484394237588" Y="-0.387223569022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.899480010264" Y="-3.690591024455" />
                  <Point X="-21.551785969635" Y="-0.315462233752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.809843919268" Y="-3.53533651464" />
                  <Point X="-21.606421471391" Y="-0.236918290523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.72020734111" Y="-3.380081745796" />
                  <Point X="-21.635075823081" Y="-0.14455992476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.630570762952" Y="-3.224826976952" />
                  <Point X="-21.650539277578" Y="-0.045187834547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.229704252372" Y="0.71028354919" />
                  <Point X="-20.216643760167" Y="0.717227936078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.540934184793" Y="-3.069572208108" />
                  <Point X="-21.637500708672" Y="0.069339050332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.637629847173" Y="0.600979817834" />
                  <Point X="-20.232114675768" Y="0.816596059152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.451297606635" Y="-2.914317439264" />
                  <Point X="-21.590551522835" Y="0.201896530066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.045553543785" Y="0.491677095763" />
                  <Point X="-20.247721862604" Y="0.915891725525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.361661028477" Y="-2.75906267042" />
                  <Point X="-20.270912688182" Y="1.011155099652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.312329014731" Y="-2.625238218613" />
                  <Point X="-20.294103687276" Y="1.10641838152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.325064969075" Y="-2.524415890844" />
                  <Point X="-20.550406301489" Y="1.077734018999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.372950919617" Y="-2.442283147575" />
                  <Point X="-20.819353118001" Y="1.04232661486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.453919459984" Y="-2.377740729341" />
                  <Point X="-21.088300091757" Y="1.006919127112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.81232940154" Y="-2.992425952513" />
                  <Point X="-28.014983040442" Y="-2.568469372017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.610766031799" Y="-2.353543376083" />
                  <Point X="-21.357247649833" Y="0.971511328677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.870630733301" Y="-2.915831165674" />
                  <Point X="-21.588278904836" Y="0.956263986199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.928931693529" Y="-2.839236181287" />
                  <Point X="-21.730381424854" Y="0.988300890857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.987232333881" Y="-2.762641026818" />
                  <Point X="-21.828039269635" Y="1.043969448527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.03871035238" Y="-2.682418219961" />
                  <Point X="-21.897687881915" Y="1.114530779291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.087427185988" Y="-2.600727265056" />
                  <Point X="-21.949321147652" Y="1.194671039727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.136144019596" Y="-2.519036310151" />
                  <Point X="-21.981399286985" Y="1.285208945309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.147565990923" Y="-2.417515325218" />
                  <Point X="-21.998607229674" Y="1.383653474697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.690921193184" Y="-2.067118824526" />
                  <Point X="-21.981729722191" Y="1.500221559424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.234275628043" Y="-1.716721915799" />
                  <Point X="-21.91406480551" Y="1.643793788631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.972076596987" Y="-1.469714063198" />
                  <Point X="-21.598557575498" Y="1.919146113402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.948805280106" Y="-1.34974632971" />
                  <Point X="-21.141910463057" Y="2.269543844843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.969698801222" Y="-1.253261457132" />
                  <Point X="-20.882300736901" Y="2.515174939606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.027540209478" Y="-1.176422124627" />
                  <Point X="-20.931560508555" Y="2.596577209232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.120987885166" Y="-1.118514980342" />
                  <Point X="-20.984208164981" Y="2.676178108571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.303556277635" Y="-1.107994161725" />
                  <Point X="-22.48037795359" Y="1.988244675416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.876032712247" Y="2.309580740218" />
                  <Point X="-21.040203014114" Y="2.753999273978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.572503068832" Y="-1.143401552404" />
                  <Point X="-22.682264255201" Y="1.988493979542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.841449967145" Y="-1.178809000038" />
                  <Point X="-22.790120126802" Y="2.038740150167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.110397241499" Y="-1.214216647616" />
                  <Point X="-22.871920827077" Y="2.102840101129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.379344515853" Y="-1.249624295194" />
                  <Point X="-22.929007380922" Y="2.180080796845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.648291790207" Y="-1.285031942773" />
                  <Point X="-22.970409874306" Y="2.265660855433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.683978944198" Y="-1.196412984324" />
                  <Point X="-22.986029537318" Y="2.364949888106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.708175515676" Y="-1.101684374777" />
                  <Point X="-22.975383806761" Y="2.478204478266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.732371466693" Y="-1.006955435325" />
                  <Point X="-22.927323911274" Y="2.611352532796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.750103044527" Y="-0.908789327682" />
                  <Point X="-28.749670675956" Y="-0.376850001574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.320166137061" Y="-0.148478387302" />
                  <Point X="-22.837687963421" Y="2.766606966501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.764404315672" Y="-0.808799293619" />
                  <Point X="-29.157595100161" Y="-0.486153110513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.295646484375" Y="-0.027846901892" />
                  <Point X="-22.748052015567" Y="2.921861400206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.778705586818" Y="-0.708809259557" />
                  <Point X="-29.565519369523" Y="-0.595456137121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.314085719167" Y="0.069942937872" />
                  <Point X="-22.658416067713" Y="3.077115833912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.361327289474" Y="0.152418304188" />
                  <Point X="-22.568780119859" Y="3.232370267617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.439935041428" Y="0.218215975889" />
                  <Point X="-22.479144172005" Y="3.387624701322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.558127605157" Y="0.262966029817" />
                  <Point X="-22.389508224152" Y="3.542879135027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.692677674673" Y="0.29901864364" />
                  <Point X="-22.299872146059" Y="3.698133637982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.827227744189" Y="0.335071257463" />
                  <Point X="-22.210235995901" Y="3.853388179254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.961777813705" Y="0.371123871286" />
                  <Point X="-22.241729819437" Y="3.944236771056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.096327897581" Y="0.407176477474" />
                  <Point X="-22.32829973204" Y="4.005800886843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.230877986794" Y="0.443229080824" />
                  <Point X="-22.419979941115" Y="4.064647809796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.365428076008" Y="0.479281684174" />
                  <Point X="-22.51653650693" Y="4.120901927879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.499978165222" Y="0.515334287523" />
                  <Point X="-22.613093355236" Y="4.177155895759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.634528254435" Y="0.551386890873" />
                  <Point X="-22.709650203542" Y="4.233409863638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.769078343649" Y="0.587439494223" />
                  <Point X="-22.812723087921" Y="4.286199193681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.770266994949" Y="0.694401631931" />
                  <Point X="-28.802725756933" Y="1.208852433705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.453965873739" Y="1.394291352984" />
                  <Point X="-22.920383865096" Y="4.336549097853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.752986439855" Y="0.811184020874" />
                  <Point X="-28.964921022361" Y="1.230205836122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.421217797482" Y="1.519297968815" />
                  <Point X="-23.028045290172" Y="4.386898657529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.735705369972" Y="0.927966683536" />
                  <Point X="-29.127116287789" Y="1.251559238538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.440681042604" Y="1.616543332628" />
                  <Point X="-23.135706715249" Y="4.437248217205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.705888972745" Y="1.051414497975" />
                  <Point X="-29.289311553217" Y="1.272912640955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.484010523572" Y="1.701098793744" />
                  <Point X="-23.255293798219" Y="4.4812567921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.671825412963" Y="1.177120568802" />
                  <Point X="-29.451506818645" Y="1.294266043372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.549443188408" Y="1.773901783527" />
                  <Point X="-23.375589524847" Y="4.524888574479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.637761853181" Y="1.30282663963" />
                  <Point X="-29.613702111315" Y="1.315619431303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.632269180919" Y="1.837456576938" />
                  <Point X="-23.4958853176" Y="4.568520321698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.71509517343" Y="1.901011370348" />
                  <Point X="-23.627125815766" Y="4.606332665823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.797921165941" Y="1.964566163759" />
                  <Point X="-23.766290441396" Y="4.639931676637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.880747202713" Y="2.028120933636" />
                  <Point X="-25.011446893838" Y="4.085464401795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.788439052076" Y="4.204039774595" />
                  <Point X="-23.905455589261" Y="4.673530409774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.96357326139" Y="2.091675691865" />
                  <Point X="-25.132013543255" Y="4.128952131972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.74203522676" Y="4.336307280996" />
                  <Point X="-24.044620737126" Y="4.707129142912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.046399320067" Y="2.155230450095" />
                  <Point X="-25.197649540037" Y="4.201647008242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.708414735114" Y="4.461777768317" />
                  <Point X="-24.186291011212" Y="4.739395876809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.129225378744" Y="2.218785208325" />
                  <Point X="-28.163365938957" Y="2.732341782118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.761222114363" Y="2.94616544654" />
                  <Point X="-25.233317316651" Y="4.290276269825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.674795176252" Y="4.587247759667" />
                  <Point X="-24.355348219443" Y="4.757100719518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.212051437421" Y="2.282339966554" />
                  <Point X="-28.280542029554" Y="2.777632304398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.750129804733" Y="3.059657487004" />
                  <Point X="-25.258551886948" Y="4.38445296561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.641176024073" Y="4.712717534781" />
                  <Point X="-24.524405647972" Y="4.774805445093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.153660404264" Y="2.420981184423" />
                  <Point X="-28.377555783223" Y="2.833643331387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.761907037072" Y="3.160989576306" />
                  <Point X="-25.283786457245" Y="4.478629661394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.062596585056" Y="2.576994830795" />
                  <Point X="-28.474569536891" Y="2.889654358376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.798977793391" Y="3.248872860348" />
                  <Point X="-25.309021027543" Y="4.572806357179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.95407208688" Y="2.742292484857" />
                  <Point X="-28.57158329056" Y="2.945665385365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.846506954401" Y="3.331195311975" />
                  <Point X="-25.33425559784" Y="4.666983052963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.81130924251" Y="2.925794990515" />
                  <Point X="-28.668597044229" Y="3.001676412354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.894036115411" Y="3.413517763603" />
                  <Point X="-25.359489867584" Y="4.761159908555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.941565030792" Y="3.495840345834" />
                  <Point X="-26.912555753829" Y="4.042974283662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.49639561734" Y="4.264250553315" />
                  <Point X="-25.565739552373" Y="4.759089160691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.989093796118" Y="3.57816300785" />
                  <Point X="-27.018279374738" Y="4.094354192091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.463746517703" Y="4.389204542343" />
                  <Point X="-25.825206498979" Y="4.728722292792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.036622561443" Y="3.660485669867" />
                  <Point X="-27.104311733443" Y="4.156204130355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.470145856284" Y="4.493396108478" />
                  <Point X="-26.184421116371" Y="4.645318647549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.820808331574" Y="3.882830286191" />
                  <Point X="-27.164511253805" Y="4.231789632412" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.260154296875" Y="-4.538443359375" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.64534375" Y="-3.363540039062" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.895361328125" Y="-3.213731933594" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.178361328125" Y="-3.240434326172" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643798828" />
                  <Point X="-25.39794140625" Y="-3.443647705078" />
                  <Point X="-25.452005859375" Y="-3.521544189453" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.591501953125" Y="-4.030770751953" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-26.0027421875" Y="-4.956990722656" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.296841796875" Y="-4.887482910156" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.341447265625" Y="-4.796363769531" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.534756835938" />
                  <Point X="-26.35022265625" Y="-4.330944824219" />
                  <Point X="-26.3702109375" Y="-4.230465332031" />
                  <Point X="-26.386283203125" Y="-4.202628417969" />
                  <Point X="-26.542517578125" Y="-4.065613037109" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.856599609375" Y="-3.972171875" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791259766" />
                  <Point X="-27.162662109375" Y="-4.089241699219" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.33399609375" Y="-4.254074707031" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.712916015625" Y="-4.256101074219" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.128060546875" Y="-3.958004394531" />
                  <Point X="-28.228580078125" Y="-3.880607666016" />
                  <Point X="-28.004650390625" Y="-3.492748046875" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597593994141" />
                  <Point X="-27.513978515625" Y="-2.568765136719" />
                  <Point X="-27.531326171875" Y="-2.551416503906" />
                  <Point X="-27.560154296875" Y="-2.537198730469" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.017453125" Y="-2.789288574219" />
                  <Point X="-28.84295703125" Y="-3.265893554688" />
                  <Point X="-29.048791015625" Y="-2.995472167969" />
                  <Point X="-29.161705078125" Y="-2.847125244141" />
                  <Point X="-29.356869140625" Y="-2.519865234375" />
                  <Point X="-29.43101953125" Y="-2.395526855469" />
                  <Point X="-29.036203125" Y="-2.092573730469" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.396017456055" />
                  <Point X="-28.14066015625" Y="-1.376092895508" />
                  <Point X="-28.138115234375" Y="-1.366260864258" />
                  <Point X="-28.140326171875" Y="-1.33459387207" />
                  <Point X="-28.16115625" Y="-1.310640136719" />
                  <Point X="-28.17889453125" Y="-1.300200439453" />
                  <Point X="-28.187638671875" Y="-1.295053588867" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.7570234375" Y="-1.359333618164" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.88323046875" Y="-1.184078979492" />
                  <Point X="-29.927390625" Y="-1.011188415527" />
                  <Point X="-29.97902734375" Y="-0.65015625" />
                  <Point X="-29.99839453125" Y="-0.5147421875" />
                  <Point X="-29.551275390625" Y="-0.394936920166" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.54189453125" Y="-0.121424743652" />
                  <Point X="-28.5233046875" Y="-0.108523155212" />
                  <Point X="-28.514140625" Y="-0.102162719727" />
                  <Point X="-28.494896484375" Y="-0.075907455444" />
                  <Point X="-28.488701171875" Y="-0.055942951202" />
                  <Point X="-28.485646484375" Y="-0.046100574493" />
                  <Point X="-28.485646484375" Y="-0.016459503174" />
                  <Point X="-28.491841796875" Y="0.003504843235" />
                  <Point X="-28.494896484375" Y="0.013347373962" />
                  <Point X="-28.514140625" Y="0.039602642059" />
                  <Point X="-28.53273046875" Y="0.052504230499" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.0474140625" Y="0.197367584229" />
                  <Point X="-29.998185546875" Y="0.452126037598" />
                  <Point X="-29.94610546875" Y="0.804085754395" />
                  <Point X="-29.91764453125" Y="0.996414733887" />
                  <Point X="-29.813703125" Y="1.379995605469" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.465654296875" Y="1.487768188477" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866699219" />
                  <Point X="-28.690560546875" Y="1.408838989258" />
                  <Point X="-28.67027734375" Y="1.41523425293" />
                  <Point X="-28.651533203125" Y="1.426056274414" />
                  <Point X="-28.639119140625" Y="1.443786132813" />
                  <Point X="-28.622611328125" Y="1.483641601562" />
                  <Point X="-28.61447265625" Y="1.503290405273" />
                  <Point X="-28.610712890625" Y="1.52460546875" />
                  <Point X="-28.616314453125" Y="1.54551171875" />
                  <Point X="-28.636234375" Y="1.583776611328" />
                  <Point X="-28.6460546875" Y="1.602641357422" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-28.941005859375" Y="1.834869750977" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.2706015625" Y="2.597544433594" />
                  <Point X="-29.16001171875" Y="2.787007568359" />
                  <Point X="-28.88467578125" Y="3.140915283203" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.596685546875" Y="3.179551513672" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.081212890625" Y="2.915421630859" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927405029297" />
                  <Point X="-27.972578125" Y="2.968077148438" />
                  <Point X="-27.95252734375" Y="2.988128417969" />
                  <Point X="-27.9408984375" Y="3.006381835938" />
                  <Point X="-27.938072265625" Y="3.02783984375" />
                  <Point X="-27.9430859375" Y="3.085140136719" />
                  <Point X="-27.945556640625" Y="3.113389404297" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.076603515625" Y="3.349736328125" />
                  <Point X="-28.30727734375" Y="3.749276855469" />
                  <Point X="-27.94548046875" Y="4.026662597656" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.319224609375" Y="4.4152578125" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.101767578125" Y="4.46212890625" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.887470703125" Y="4.240461425781" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.218491699219" />
                  <Point X="-26.813806640625" Y="4.22225" />
                  <Point X="-26.747380859375" Y="4.249765136719" />
                  <Point X="-26.7146328125" Y="4.263329589844" />
                  <Point X="-26.696904296875" Y="4.275743652344" />
                  <Point X="-26.68608203125" Y="4.294486816406" />
                  <Point X="-26.664462890625" Y="4.363057617187" />
                  <Point X="-26.653802734375" Y="4.39686328125" />
                  <Point X="-26.651916015625" Y="4.418426757812" />
                  <Point X="-26.66607421875" Y="4.525970703125" />
                  <Point X="-26.68913671875" Y="4.701141113281" />
                  <Point X="-26.21743359375" Y="4.833390136719" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.442376953125" Y="4.964823730469" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.17157421875" Y="4.793956054688" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.881734375" Y="4.549049316406" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.35750390625" Y="4.948365722656" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.70485546875" Y="4.820558105469" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.20795703125" Y="4.666199707031" />
                  <Point X="-23.06896484375" Y="4.615786132813" />
                  <Point X="-22.79521875" Y="4.487764648438" />
                  <Point X="-22.6613046875" Y="4.42513671875" />
                  <Point X="-22.3968515625" Y="4.271065917969" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-22.018064453125" Y="4.018323974609" />
                  <Point X="-21.9312578125" Y="3.956592529297" />
                  <Point X="-22.19303515625" Y="3.503183349609" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491515380859" />
                  <Point X="-22.78952734375" Y="2.437740234375" />
                  <Point X="-22.7966171875" Y="2.411229003906" />
                  <Point X="-22.797955078125" Y="2.392327148438" />
                  <Point X="-22.79234765625" Y="2.345827148438" />
                  <Point X="-22.789583984375" Y="2.32290234375" />
                  <Point X="-22.78131640625" Y="2.300811767578" />
                  <Point X="-22.75254296875" Y="2.258408203125" />
                  <Point X="-22.738357421875" Y="2.237503173828" />
                  <Point X="-22.725056640625" Y="2.224202880859" />
                  <Point X="-22.682654296875" Y="2.195430175781" />
                  <Point X="-22.661748046875" Y="2.181245361328" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.593162109375" Y="2.167373046875" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5513359375" Y="2.165946044922" />
                  <Point X="-22.497560546875" Y="2.180326416016" />
                  <Point X="-22.471048828125" Y="2.187415771484" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.96616796875" Y="2.476934326172" />
                  <Point X="-21.005751953125" Y="3.031430664062" />
                  <Point X="-20.8741484375" Y="2.848532714844" />
                  <Point X="-20.797404296875" Y="2.741875" />
                  <Point X="-20.658365234375" Y="2.512110351562" />
                  <Point X="-20.612486328125" Y="2.436295654297" />
                  <Point X="-20.9515625" Y="2.176113525391" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583833129883" />
                  <Point X="-21.759330078125" Y="1.533343383789" />
                  <Point X="-21.77841015625" Y="1.508451782227" />
                  <Point X="-21.78687890625" Y="1.491500244141" />
                  <Point X="-21.801296875" Y="1.439949951172" />
                  <Point X="-21.808404296875" Y="1.414535644531" />
                  <Point X="-21.809220703125" Y="1.390966064453" />
                  <Point X="-21.797384765625" Y="1.333609863281" />
                  <Point X="-21.79155078125" Y="1.305333374023" />
                  <Point X="-21.784353515625" Y="1.287957275391" />
                  <Point X="-21.7521640625" Y="1.239032104492" />
                  <Point X="-21.736296875" Y="1.214911865234" />
                  <Point X="-21.719052734375" Y="1.19882019043" />
                  <Point X="-21.67240625" Y="1.17256262207" />
                  <Point X="-21.64941015625" Y="1.159617675781" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.568365234375" Y="1.145284301758" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-21.056072265625" Y="1.202801635742" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.093771484375" Y="1.086774047852" />
                  <Point X="-20.06080859375" Y="0.951367614746" />
                  <Point X="-20.016994140625" Y="0.669959655762" />
                  <Point X="-20.002140625" Y="0.574556335449" />
                  <Point X="-20.388060546875" Y="0.471149169922" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819046021" />
                  <Point X="-21.332875" Y="0.197003723145" />
                  <Point X="-21.363421875" Y="0.179346679688" />
                  <Point X="-21.377734375" Y="0.166926239014" />
                  <Point X="-21.414912109375" Y="0.11955342865" />
                  <Point X="-21.433240234375" Y="0.096198562622" />
                  <Point X="-21.443013671875" Y="0.074734954834" />
                  <Point X="-21.45540625" Y="0.010026067734" />
                  <Point X="-21.461515625" Y="-0.021875602722" />
                  <Point X="-21.461515625" Y="-0.040684474945" />
                  <Point X="-21.449123046875" Y="-0.105393363953" />
                  <Point X="-21.443013671875" Y="-0.137295028687" />
                  <Point X="-21.433240234375" Y="-0.158758636475" />
                  <Point X="-21.3960625" Y="-0.20613130188" />
                  <Point X="-21.377734375" Y="-0.229486312866" />
                  <Point X="-21.363421875" Y="-0.24190675354" />
                  <Point X="-21.301458984375" Y="-0.277722076416" />
                  <Point X="-21.270912109375" Y="-0.295379119873" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.8295390625" Y="-0.415415435791" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.0331640625" Y="-0.844334289551" />
                  <Point X="-20.051568359375" Y="-0.966414001465" />
                  <Point X="-20.107703125" Y="-1.212395996094" />
                  <Point X="-20.125453125" Y="-1.290178588867" />
                  <Point X="-20.57816796875" Y="-1.230577392578" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.7267734375" Y="-1.124773681641" />
                  <Point X="-21.7867265625" Y="-1.137805053711" />
                  <Point X="-21.8145546875" Y="-1.154697265625" />
                  <Point X="-21.888060546875" Y="-1.243101806641" />
                  <Point X="-21.924298828125" Y="-1.286685424805" />
                  <Point X="-21.935640625" Y="-1.314071044922" />
                  <Point X="-21.94617578125" Y="-1.42855871582" />
                  <Point X="-21.951369140625" Y="-1.485001342773" />
                  <Point X="-21.943638671875" Y="-1.516621826172" />
                  <Point X="-21.876337890625" Y="-1.621304077148" />
                  <Point X="-21.843158203125" Y="-1.672912475586" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-21.43315234375" Y="-1.991232543945" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.743990234375" Y="-2.718196533203" />
                  <Point X="-20.7958671875" Y="-2.802139160156" />
                  <Point X="-20.911958984375" Y="-2.967092529297" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.34713671875" Y="-2.778488525391" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.407392578125" Y="-2.227173583984" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219245117188" />
                  <Point X="-22.631162109375" Y="-2.282526367188" />
                  <Point X="-22.690439453125" Y="-2.313724121094" />
                  <Point X="-22.7113984375" Y="-2.334682861328" />
                  <Point X="-22.7746796875" Y="-2.454922363281" />
                  <Point X="-22.80587890625" Y="-2.514200927734" />
                  <Point X="-22.8108359375" Y="-2.546374511719" />
                  <Point X="-22.7846953125" Y="-2.691109863281" />
                  <Point X="-22.77180859375" Y="-2.76246484375" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.509904296875" Y="-3.22198828125" />
                  <Point X="-22.01332421875" Y="-4.082087890625" />
                  <Point X="-22.10314453125" Y="-4.146243164062" />
                  <Point X="-22.164720703125" Y="-4.190224609375" />
                  <Point X="-22.294498046875" Y="-4.274228515625" />
                  <Point X="-22.32022265625" Y="-4.290879394531" />
                  <Point X="-22.630490234375" Y="-3.886530273438" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467529297" />
                  <Point X="-23.47219921875" Y="-2.888693603516" />
                  <Point X="-23.54257421875" Y="-2.843448730469" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.730314453125" Y="-2.850083251953" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868508789062" />
                  <Point X="-23.955216796875" Y="-2.968743408203" />
                  <Point X="-24.014650390625" Y="-3.018159179688" />
                  <Point X="-24.03154296875" Y="-3.045985351562" />
                  <Point X="-24.0675859375" Y="-3.211817626953" />
                  <Point X="-24.085357421875" Y="-3.293573242188" />
                  <Point X="-24.0860703125" Y="-3.310719970703" />
                  <Point X="-24.01351953125" Y="-3.861785888672" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.947416015625" Y="-4.950481933594" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.1255625" Y="-4.985029785156" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#184" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.129433535499" Y="4.838177028909" Z="1.7" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.7" />
                  <Point X="-0.45434698919" Y="5.045764806741" Z="1.7" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.7" />
                  <Point X="-1.237088461377" Y="4.912813662684" Z="1.7" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.7" />
                  <Point X="-1.721804275216" Y="4.550724258786" Z="1.7" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.7" />
                  <Point X="-1.718304781699" Y="4.409374967938" Z="1.7" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.7" />
                  <Point X="-1.772673227017" Y="4.327239286749" Z="1.7" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.7" />
                  <Point X="-1.870540247716" Y="4.316091947127" Z="1.7" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.7" />
                  <Point X="-2.06825641938" Y="4.523846994904" Z="1.7" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.7" />
                  <Point X="-2.349665438443" Y="4.490245306979" Z="1.7" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.7" />
                  <Point X="-2.982060157824" Y="4.097622286191" Z="1.7" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.7" />
                  <Point X="-3.126061088333" Y="3.356016272433" Z="1.7" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.7" />
                  <Point X="-2.999053182556" Y="3.112063773088" Z="1.7" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.7" />
                  <Point X="-3.014091451655" Y="3.034712116231" Z="1.7" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.7" />
                  <Point X="-3.083012692519" Y="2.996511516617" Z="1.7" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.7" />
                  <Point X="-3.577843075864" Y="3.254132860412" Z="1.7" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.7" />
                  <Point X="-3.930295423001" Y="3.202897702492" Z="1.7" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.7" />
                  <Point X="-4.319939363342" Y="2.654029776433" Z="1.7" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.7" />
                  <Point X="-3.97760018604" Y="1.826482059631" Z="1.7" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.7" />
                  <Point X="-3.686741932212" Y="1.59196938707" Z="1.7" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.7" />
                  <Point X="-3.674961242716" Y="1.534055556059" Z="1.7" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.7" />
                  <Point X="-3.711753358111" Y="1.487804769401" Z="1.7" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.7" />
                  <Point X="-4.465286031611" Y="1.568620480768" Z="1.7" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.7" />
                  <Point X="-4.868119024286" Y="1.424352983686" Z="1.7" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.7" />
                  <Point X="-5.00172280124" Y="0.842684967303" Z="1.7" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.7" />
                  <Point X="-4.066513210302" Y="0.180351403991" Z="1.7" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.7" />
                  <Point X="-3.567396495854" Y="0.04270852628" Z="1.7" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.7" />
                  <Point X="-3.545753060319" Y="0.01996443795" Z="1.7" />
                  <Point X="-3.539556741714" Y="0" Z="1.7" />
                  <Point X="-3.542611540816" Y="-0.009842513111" Z="1.7" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.7" />
                  <Point X="-3.557972099269" Y="-0.036167456854" Z="1.7" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.7" />
                  <Point X="-4.57037457847" Y="-0.315360652765" Z="1.7" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.7" />
                  <Point X="-5.03468128953" Y="-0.625955498345" Z="1.7" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.7" />
                  <Point X="-4.937833125927" Y="-1.165173103501" Z="1.7" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.7" />
                  <Point X="-3.75665418293" Y="-1.377625892416" Z="1.7" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.7" />
                  <Point X="-3.210414110146" Y="-1.312010103578" Z="1.7" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.7" />
                  <Point X="-3.195220397131" Y="-1.33227264126" Z="1.7" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.7" />
                  <Point X="-4.072797294467" Y="-2.021625861309" Z="1.7" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.7" />
                  <Point X="-4.40596926314" Y="-2.514194778188" Z="1.7" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.7" />
                  <Point X="-4.094734173391" Y="-2.994475097951" Z="1.7" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.7" />
                  <Point X="-2.998610330662" Y="-2.801309871748" Z="1.7" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.7" />
                  <Point X="-2.567110864516" Y="-2.561219604615" Z="1.7" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.7" />
                  <Point X="-3.054107141421" Y="-3.43646805183" Z="1.7" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.7" />
                  <Point X="-3.164721934912" Y="-3.966341543624" Z="1.7" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.7" />
                  <Point X="-2.745395778957" Y="-4.267331923138" Z="1.7" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.7" />
                  <Point X="-2.300484708943" Y="-4.253232831091" Z="1.7" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.7" />
                  <Point X="-2.141039579276" Y="-4.099534836479" Z="1.7" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.7" />
                  <Point X="-1.866026981808" Y="-3.990784818675" Z="1.7" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.7" />
                  <Point X="-1.581642091844" Y="-4.071924925834" Z="1.7" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.7" />
                  <Point X="-1.40541929354" Y="-4.309420266168" Z="1.7" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.7" />
                  <Point X="-1.397176219213" Y="-4.75855739091" Z="1.7" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.7" />
                  <Point X="-1.315457272862" Y="-4.904625630733" Z="1.7" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.7" />
                  <Point X="-1.018427158314" Y="-4.974794906503" Z="1.7" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.7" />
                  <Point X="-0.549362190992" Y="-4.01243166835" Z="1.7" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.7" />
                  <Point X="-0.363022476829" Y="-3.440876725295" Z="1.7" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.7" />
                  <Point X="-0.169697719454" Y="-3.256907278127" Z="1.7" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.7" />
                  <Point X="0.083661359907" Y="-3.230204767161" Z="1.7" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.7" />
                  <Point X="0.307423382604" Y="-3.360769030473" Z="1.7" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.7" />
                  <Point X="0.685392669409" Y="-4.520104361466" Z="1.7" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.7" />
                  <Point X="0.87721859476" Y="-5.002944795799" Z="1.7" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.7" />
                  <Point X="1.0571316454" Y="-4.968041981196" Z="1.7" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.7" />
                  <Point X="1.029894998943" Y="-3.823979628599" Z="1.7" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.7" />
                  <Point X="0.975115708799" Y="-3.191158100205" Z="1.7" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.7" />
                  <Point X="1.07059220249" Y="-2.975910099437" Z="1.7" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.7" />
                  <Point X="1.268110997722" Y="-2.868592843794" Z="1.7" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.7" />
                  <Point X="1.494605660294" Y="-2.899471342927" Z="1.7" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.7" />
                  <Point X="2.323684252007" Y="-3.885688219946" Z="1.7" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.7" />
                  <Point X="2.726512284826" Y="-4.284923269893" Z="1.7" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.7" />
                  <Point X="2.919761941913" Y="-4.155650021361" Z="1.7" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.7" />
                  <Point X="2.527239642241" Y="-3.165708199036" Z="1.7" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.7" />
                  <Point X="2.258350245164" Y="-2.650943689118" Z="1.7" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.7" />
                  <Point X="2.263408923891" Y="-2.44692978986" Z="1.7" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.7" />
                  <Point X="2.385968588512" Y="-2.295492386076" Z="1.7" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.7" />
                  <Point X="2.577563114394" Y="-2.245097764327" Z="1.7" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.7" />
                  <Point X="3.621705398071" Y="-2.790509858034" Z="1.7" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.7" />
                  <Point X="4.122771808405" Y="-2.96459018521" Z="1.7" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.7" />
                  <Point X="4.292386005877" Y="-2.713201825202" Z="1.7" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.7" />
                  <Point X="3.591128243248" Y="-1.920284953338" Z="1.7" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.7" />
                  <Point X="3.159563354236" Y="-1.562984543507" Z="1.7" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.7" />
                  <Point X="3.097456342017" Y="-1.401859845125" Z="1.7" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.7" />
                  <Point X="3.144229898983" Y="-1.243788635974" Z="1.7" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.7" />
                  <Point X="3.27768964184" Y="-1.142352908468" Z="1.7" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.7" />
                  <Point X="4.409148884883" Y="-1.248869560737" Z="1.7" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.7" />
                  <Point X="4.934886801273" Y="-1.192239555332" Z="1.7" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.7" />
                  <Point X="5.010120514147" Y="-0.820508192406" Z="1.7" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.7" />
                  <Point X="4.177244557395" Y="-0.335839038193" Z="1.7" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.7" />
                  <Point X="3.717405102286" Y="-0.203153578043" Z="1.7" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.7" />
                  <Point X="3.637114156266" Y="-0.143983316807" Z="1.7" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.7" />
                  <Point X="3.593827204234" Y="-0.064708898005" Z="1.7" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.7" />
                  <Point X="3.58754424619" Y="0.031901633193" Z="1.7" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.7" />
                  <Point X="3.618265282134" Y="0.119965421768" Z="1.7" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.7" />
                  <Point X="3.685990312066" Y="0.184995266199" Z="1.7" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.7" />
                  <Point X="4.618723259912" Y="0.454132894576" Z="1.7" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.7" />
                  <Point X="5.026253630123" Y="0.708931777884" Z="1.7" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.7" />
                  <Point X="4.948653694054" Y="1.129880875598" Z="1.7" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.7" />
                  <Point X="3.931246348573" Y="1.283653918903" Z="1.7" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.7" />
                  <Point X="3.432028704986" Y="1.22613336343" Z="1.7" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.7" />
                  <Point X="3.345964205965" Y="1.247413568" Z="1.7" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.7" />
                  <Point X="3.283449531212" Y="1.297791245093" Z="1.7" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.7" />
                  <Point X="3.245426551594" Y="1.374993095711" Z="1.7" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.7" />
                  <Point X="3.240699428708" Y="1.457763547961" Z="1.7" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.7" />
                  <Point X="3.274196159341" Y="1.534205292421" Z="1.7" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.7" />
                  <Point X="4.072718653904" Y="2.167725852457" Z="1.7" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.7" />
                  <Point X="4.378256035397" Y="2.569276840375" Z="1.7" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.7" />
                  <Point X="4.160280221175" Y="2.909015939849" Z="1.7" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.7" />
                  <Point X="3.002675777698" Y="2.551515727334" Z="1.7" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.7" />
                  <Point X="2.483366701054" Y="2.259909322487" Z="1.7" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.7" />
                  <Point X="2.40666696382" Y="2.248293468038" Z="1.7" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.7" />
                  <Point X="2.33926170243" Y="2.268085480732" Z="1.7" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.7" />
                  <Point X="2.282673119503" Y="2.317763157952" Z="1.7" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.7" />
                  <Point X="2.251136234721" Y="2.383091480013" Z="1.7" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.7" />
                  <Point X="2.25261862017" Y="2.456102874828" Z="1.7" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.7" />
                  <Point X="2.844109625525" Y="3.509463304257" Z="1.7" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.7" />
                  <Point X="3.004755827691" Y="4.090351201002" Z="1.7" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.7" />
                  <Point X="2.622162261773" Y="4.345548412883" Z="1.7" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.7" />
                  <Point X="2.21980542286" Y="4.564336083476" Z="1.7" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.7" />
                  <Point X="1.802935175926" Y="4.744483030455" Z="1.7" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.7" />
                  <Point X="1.30072204223" Y="4.900441822969" Z="1.7" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.7" />
                  <Point X="0.641545356148" Y="5.029373966098" Z="1.7" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.7" />
                  <Point X="0.063811026054" Y="4.593270477042" Z="1.7" />
                  <Point X="0" Y="4.355124473572" Z="1.7" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>