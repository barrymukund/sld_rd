<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#139" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1007" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004716064453" />
                  <Width Value="9.78389453125" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.349755859375" Y="-3.836989990234" />
                  <Point X="-24.4366953125" Y="-3.512524414063" />
                  <Point X="-24.44227734375" Y="-3.497140136719" />
                  <Point X="-24.457634765625" Y="-3.467376708984" />
                  <Point X="-24.493169921875" Y="-3.416177978516" />
                  <Point X="-24.62136328125" Y="-3.231476318359" />
                  <Point X="-24.643248046875" Y="-3.209020751953" />
                  <Point X="-24.66950390625" Y="-3.189776855469" />
                  <Point X="-24.69750390625" Y="-3.175669433594" />
                  <Point X="-24.7524921875" Y="-3.158603027344" />
                  <Point X="-24.95086328125" Y="-3.097036132812" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097036132812" />
                  <Point X="-25.091810546875" Y="-3.114102294922" />
                  <Point X="-25.29018359375" Y="-3.175669433594" />
                  <Point X="-25.31818359375" Y="-3.189776855469" />
                  <Point X="-25.344439453125" Y="-3.209020751953" />
                  <Point X="-25.36632421875" Y="-3.231478271484" />
                  <Point X="-25.401857421875" Y="-3.282677001953" />
                  <Point X="-25.53005078125" Y="-3.467378662109" />
                  <Point X="-25.5381875" Y="-3.481571533203" />
                  <Point X="-25.550990234375" Y="-3.512524169922" />
                  <Point X="-25.864630859375" Y="-4.683048339844" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.079341796875" Y="-4.845349609375" />
                  <Point X="-26.139291015625" Y="-4.829925292969" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.225427734375" Y="-4.642932128906" />
                  <Point X="-26.2149609375" Y="-4.563438476562" />
                  <Point X="-26.21419921875" Y="-4.5479296875" />
                  <Point X="-26.2165078125" Y="-4.516223632812" />
                  <Point X="-26.22964453125" Y="-4.450181640625" />
                  <Point X="-26.277037109375" Y="-4.211932128906" />
                  <Point X="-26.287939453125" Y="-4.182964355469" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131205078125" />
                  <Point X="-26.37426953125" Y="-4.086806884766" />
                  <Point X="-26.556904296875" Y="-3.926639892578" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.71021875" Y="-3.886562255859" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708496094" />
                  <Point X="-27.014466796875" Y="-3.882028076172" />
                  <Point X="-27.042658203125" Y="-3.894802001953" />
                  <Point X="-27.09864453125" Y="-3.932211914062" />
                  <Point X="-27.300623046875" Y="-4.067170166016" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.3351015625" Y="-4.099461914062" />
                  <Point X="-27.4801484375" Y="-4.288488769531" />
                  <Point X="-27.80171484375" Y="-4.089383544922" />
                  <Point X="-27.88472265625" Y="-4.025469238281" />
                  <Point X="-28.10472265625" Y="-3.856077392578" />
                  <Point X="-27.585330078125" Y="-2.95646484375" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647655273438" />
                  <Point X="-27.406587890625" Y="-2.616130126953" />
                  <Point X="-27.40557421875" Y="-2.585197998047" />
                  <Point X="-27.414556640625" Y="-2.555581542969" />
                  <Point X="-27.428771484375" Y="-2.526752929688" />
                  <Point X="-27.446798828125" Y="-2.501592529297" />
                  <Point X="-27.464146484375" Y="-2.484243896484" />
                  <Point X="-27.4893046875" Y="-2.466215576172" />
                  <Point X="-27.518134765625" Y="-2.451997070313" />
                  <Point X="-27.547751953125" Y="-2.44301171875" />
                  <Point X="-27.5786875" Y="-2.444023925781" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.648736328125" Y="-3.044063476562" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-29.082859375" Y="-2.793861816406" />
                  <Point X="-29.142373046875" Y="-2.694067382812" />
                  <Point X="-29.306142578125" Y="-2.419450195312" />
                  <Point X="-28.388646484375" Y="-1.715431518555" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.0830703125" Y="-1.475887207031" />
                  <Point X="-28.064025390625" Y="-1.445587524414" />
                  <Point X="-28.0556328125" Y="-1.424056030273" />
                  <Point X="-28.0521796875" Y="-1.413369750977" />
                  <Point X="-28.0461484375" Y="-1.390078735352" />
                  <Point X="-28.04203515625" Y="-1.358597290039" />
                  <Point X="-28.042736328125" Y="-1.335728759766" />
                  <Point X="-28.0488828125" Y="-1.313690673828" />
                  <Point X="-28.060888671875" Y="-1.284709716797" />
                  <Point X="-28.073482421875" Y="-1.262984130859" />
                  <Point X="-28.091326171875" Y="-1.245316772461" />
                  <Point X="-28.109576171875" Y="-1.231505004883" />
                  <Point X="-28.11871875" Y="-1.225385131836" />
                  <Point X="-28.139453125" Y="-1.213181396484" />
                  <Point X="-28.168716796875" Y="-1.20195703125" />
                  <Point X="-28.200603515625" Y="-1.195475097656" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-29.50639453125" Y="-1.362170410156" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.834078125" Y="-0.992648803711" />
                  <Point X="-29.849822265625" Y="-0.882564880371" />
                  <Point X="-29.892421875" Y="-0.584698425293" />
                  <Point X="-28.854904296875" Y="-0.306695983887" />
                  <Point X="-28.532875" Y="-0.220408309937" />
                  <Point X="-28.513103515625" Y="-0.212652740479" />
                  <Point X="-28.491513671875" Y="-0.201252365112" />
                  <Point X="-28.48170703125" Y="-0.195290344238" />
                  <Point X="-28.4599765625" Y="-0.180208755493" />
                  <Point X="-28.436021484375" Y="-0.158682907104" />
                  <Point X="-28.415521484375" Y="-0.129254547119" />
                  <Point X="-28.406087890625" Y="-0.108046394348" />
                  <Point X="-28.40215625" Y="-0.097592590332" />
                  <Point X="-28.3949140625" Y="-0.074254814148" />
                  <Point X="-28.38947265625" Y="-0.045516647339" />
                  <Point X="-28.3902421875" Y="-0.012664651871" />
                  <Point X="-28.39449609375" Y="0.008623494148" />
                  <Point X="-28.396921875" Y="0.018163824081" />
                  <Point X="-28.4041640625" Y="0.041501747131" />
                  <Point X="-28.417482421875" Y="0.070829170227" />
                  <Point X="-28.4395703125" Y="0.099409172058" />
                  <Point X="-28.4574140625" Y="0.115108398438" />
                  <Point X="-28.466" Y="0.121829376221" />
                  <Point X="-28.48773046875" Y="0.136910964966" />
                  <Point X="-28.50192578125" Y="0.14504737854" />
                  <Point X="-28.532875" Y="0.157848327637" />
                  <Point X="-29.694611328125" Y="0.46913458252" />
                  <Point X="-29.891814453125" Y="0.521975341797" />
                  <Point X="-29.8917421875" Y="0.522468994141" />
                  <Point X="-29.82448828125" Y="0.976969909668" />
                  <Point X="-29.792791015625" Y="1.093943725586" />
                  <Point X="-29.70355078125" Y="1.423268066406" />
                  <Point X="-28.99650390625" Y="1.33018347168" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228027344" />
                  <Point X="-28.7031328125" Y="1.305264282227" />
                  <Point X="-28.689802734375" Y="1.309467773438" />
                  <Point X="-28.64170703125" Y="1.324631713867" />
                  <Point X="-28.622775390625" Y="1.332962402344" />
                  <Point X="-28.60403125" Y="1.343784790039" />
                  <Point X="-28.587353515625" Y="1.356014282227" />
                  <Point X="-28.573716796875" Y="1.371563354492" />
                  <Point X="-28.56130078125" Y="1.389293701172" />
                  <Point X="-28.551349609375" Y="1.407430786133" />
                  <Point X="-28.546" Y="1.420345458984" />
                  <Point X="-28.526703125" Y="1.466935180664" />
                  <Point X="-28.5209140625" Y="1.486795166016" />
                  <Point X="-28.51715625" Y="1.508107666016" />
                  <Point X="-28.515802734375" Y="1.528744262695" />
                  <Point X="-28.518947265625" Y="1.549184692383" />
                  <Point X="-28.524546875" Y="1.570088745117" />
                  <Point X="-28.532044921875" Y="1.58937097168" />
                  <Point X="-28.538501953125" Y="1.601775878906" />
                  <Point X="-28.5617890625" Y="1.646506225586" />
                  <Point X="-28.573283203125" Y="1.663709350586" />
                  <Point X="-28.5871953125" Y="1.680288330078" />
                  <Point X="-28.60213671875" Y="1.694590454102" />
                  <Point X="-29.268509765625" Y="2.205917480469" />
                  <Point X="-29.351859375" Y="2.269873046875" />
                  <Point X="-29.34248046875" Y="2.285940185547" />
                  <Point X="-29.08114453125" Y="2.733670898438" />
                  <Point X="-28.9971953125" Y="2.841577636719" />
                  <Point X="-28.75050390625" Y="3.158662597656" />
                  <Point X="-28.348431640625" Y="2.926524902344" />
                  <Point X="-28.20665625" Y="2.844671142578" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.128224609375" Y="2.824171875" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.999013671875" Y="2.826504638672" />
                  <Point X="-27.980462890625" Y="2.835653076172" />
                  <Point X="-27.962208984375" Y="2.847281982422" />
                  <Point X="-27.946078125" Y="2.860228027344" />
                  <Point X="-27.9328984375" Y="2.873407226562" />
                  <Point X="-27.885353515625" Y="2.920951416016" />
                  <Point X="-27.872404296875" Y="2.937084960938" />
                  <Point X="-27.860775390625" Y="2.955339111328" />
                  <Point X="-27.85162890625" Y="2.973888183594" />
                  <Point X="-27.8467109375" Y="2.993977294922" />
                  <Point X="-27.843884765625" Y="3.015436035156" />
                  <Point X="-27.84343359375" Y="3.036123291016" />
                  <Point X="-27.84505859375" Y="3.054690673828" />
                  <Point X="-27.85091796875" Y="3.121672851562" />
                  <Point X="-27.854955078125" Y="3.141961669922" />
                  <Point X="-27.86146484375" Y="3.162604492188" />
                  <Point X="-27.869794921875" Y="3.181532958984" />
                  <Point X="-28.1650859375" Y="3.692991455078" />
                  <Point X="-28.18333203125" Y="3.724596435547" />
                  <Point X="-28.1557734375" Y="3.745726074219" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.56839453125" Y="4.1681484375" />
                  <Point X="-27.167037109375" Y="4.391133789062" />
                  <Point X="-27.08659765625" Y="4.286302246094" />
                  <Point X="-27.0431953125" Y="4.229740234375" />
                  <Point X="-27.028890625" Y="4.214797851563" />
                  <Point X="-27.012310546875" Y="4.200885742188" />
                  <Point X="-26.995111328125" Y="4.189394042969" />
                  <Point X="-26.9744453125" Y="4.178636230469" />
                  <Point X="-26.89989453125" Y="4.139827148438" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.77744921875" Y="4.134482910156" />
                  <Point X="-26.75592578125" Y="4.143398925781" />
                  <Point X="-26.678275390625" Y="4.1755625" />
                  <Point X="-26.66014453125" Y="4.185509765625" />
                  <Point X="-26.6424140625" Y="4.197924316406" />
                  <Point X="-26.626859375" Y="4.211565429688" />
                  <Point X="-26.614626953125" Y="4.22825" />
                  <Point X="-26.603806640625" Y="4.246994140625" />
                  <Point X="-26.595478515625" Y="4.265920898438" />
                  <Point X="-26.58847265625" Y="4.288140136719" />
                  <Point X="-26.56319921875" Y="4.368297363281" />
                  <Point X="-26.5591640625" Y="4.388583496094" />
                  <Point X="-26.55727734375" Y="4.410144042969" />
                  <Point X="-26.557728515625" Y="4.430826171875" />
                  <Point X="-26.584201171875" Y="4.6318984375" />
                  <Point X="-26.5388515625" Y="4.644612792969" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.789337890625" Y="4.828568847656" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.17584375" Y="4.442841308594" />
                  <Point X="-25.133904296875" Y="4.286315917969" />
                  <Point X="-25.12112890625" Y="4.258122558594" />
                  <Point X="-25.10326953125" Y="4.231395507813" />
                  <Point X="-25.08211328125" Y="4.208808105469" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.702478515625" Y="4.850988769531" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.670435546875" Y="4.885618652344" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-24.023333984375" Y="4.799719238281" />
                  <Point X="-23.5189765625" Y="4.677951660156" />
                  <Point X="-23.43380859375" Y="4.647061035156" />
                  <Point X="-23.105349609375" Y="4.527926757812" />
                  <Point X="-23.021876953125" Y="4.488889160156" />
                  <Point X="-22.705427734375" Y="4.340895996094" />
                  <Point X="-22.62475" Y="4.293893554688" />
                  <Point X="-22.319017578125" Y="4.1157734375" />
                  <Point X="-22.242962890625" Y="4.061686523438" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.665412109375" Y="2.874998046875" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.860427734375" Y="2.533160400391" />
                  <Point X="-22.87008984375" Y="2.503652587891" />
                  <Point X="-22.87158203125" Y="2.4986328125" />
                  <Point X="-22.888392578125" Y="2.435771484375" />
                  <Point X="-22.891607421875" Y="2.409885498047" />
                  <Point X="-22.891126953125" Y="2.37591015625" />
                  <Point X="-22.890453125" Y="2.365882324219" />
                  <Point X="-22.883900390625" Y="2.311531494141" />
                  <Point X="-22.87855859375" Y="2.289610351562" />
                  <Point X="-22.87029296875" Y="2.267520019531" />
                  <Point X="-22.859927734375" Y="2.247467773438" />
                  <Point X="-22.850603515625" Y="2.233727539062" />
                  <Point X="-22.81696875" Y="2.184159179688" />
                  <Point X="-22.799318359375" Y="2.164641113281" />
                  <Point X="-22.77227734375" Y="2.142017578125" />
                  <Point X="-22.76466015625" Y="2.136268798828" />
                  <Point X="-22.715091796875" Y="2.102634765625" />
                  <Point X="-22.695046875" Y="2.092272216797" />
                  <Point X="-22.672958984375" Y="2.084006347656" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.63596875" Y="2.076846679688" />
                  <Point X="-22.581611328125" Y="2.070291992188" />
                  <Point X="-22.554763671875" Y="2.070877441406" />
                  <Point X="-22.518435546875" Y="2.076874755859" />
                  <Point X="-22.5093671875" Y="2.078830810547" />
                  <Point X="-22.446505859375" Y="2.095640869141" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.242982421875" Y="2.784769042969" />
                  <Point X="-21.032673828125" Y="2.906190673828" />
                  <Point X="-20.876716796875" Y="2.689447265625" />
                  <Point X="-20.834326171875" Y="2.619394287109" />
                  <Point X="-20.73780078125" Y="2.459883789063" />
                  <Point X="-21.523005859375" Y="1.857373168945" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.778572265625" Y="1.660243408203" />
                  <Point X="-21.796025390625" Y="1.641627929688" />
                  <Point X="-21.80856640625" Y="1.625267456055" />
                  <Point X="-21.853806640625" Y="1.566246459961" />
                  <Point X="-21.866904296875" Y="1.543004272461" />
                  <Point X="-21.880044921875" Y="1.509349609375" />
                  <Point X="-21.8830390625" Y="1.500384155273" />
                  <Point X="-21.899892578125" Y="1.440123901367" />
                  <Point X="-21.90334765625" Y="1.417824829102" />
                  <Point X="-21.9041640625" Y="1.39425390625" />
                  <Point X="-21.902259765625" Y="1.371762695312" />
                  <Point X="-21.898423828125" Y="1.353177246094" />
                  <Point X="-21.88458984375" Y="1.286130004883" />
                  <Point X="-21.875474609375" Y="1.26081628418" />
                  <Point X="-21.85784765625" Y="1.227585205078" />
                  <Point X="-21.853287109375" Y="1.219887451172" />
                  <Point X="-21.81566015625" Y="1.162695556641" />
                  <Point X="-21.801107421875" Y="1.145449951172" />
                  <Point X="-21.78386328125" Y="1.129360229492" />
                  <Point X="-21.76565234375" Y="1.116034301758" />
                  <Point X="-21.750537109375" Y="1.107526000977" />
                  <Point X="-21.696009765625" Y="1.07683190918" />
                  <Point X="-21.6794765625" Y="1.069501098633" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.623443359375" Y="1.056737670898" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.401814453125" Y="1.193116333008" />
                  <Point X="-20.223162109375" Y="1.216636474609" />
                  <Point X="-20.154060546875" Y="0.932785583496" />
                  <Point X="-20.140701171875" Y="0.84698638916" />
                  <Point X="-20.109134765625" Y="0.644238769531" />
                  <Point X="-21.001259765625" Y="0.405194396973" />
                  <Point X="-21.283419921875" Y="0.329589874268" />
                  <Point X="-21.2952109375" Y="0.325585662842" />
                  <Point X="-21.318453125" Y="0.315068237305" />
                  <Point X="-21.33853125" Y="0.303462799072" />
                  <Point X="-21.410962890625" Y="0.261595672607" />
                  <Point X="-21.43232421875" Y="0.244748321533" />
                  <Point X="-21.45868359375" Y="0.216977661133" />
                  <Point X="-21.464515625" Y="0.210226028442" />
                  <Point X="-21.507974609375" Y="0.154848632812" />
                  <Point X="-21.51969921875" Y="0.135567459106" />
                  <Point X="-21.52947265625" Y="0.11410369873" />
                  <Point X="-21.536318359375" Y="0.092604103088" />
                  <Point X="-21.540333984375" Y="0.071636238098" />
                  <Point X="-21.5548203125" Y="-0.004006376743" />
                  <Point X="-21.556515625" Y="-0.021875444412" />
                  <Point X="-21.5548203125" Y="-0.058553619385" />
                  <Point X="-21.5508046875" Y="-0.079521484375" />
                  <Point X="-21.536318359375" Y="-0.155164260864" />
                  <Point X="-21.52947265625" Y="-0.176663848877" />
                  <Point X="-21.51969921875" Y="-0.198127609253" />
                  <Point X="-21.507974609375" Y="-0.217409545898" />
                  <Point X="-21.495927734375" Y="-0.232759796143" />
                  <Point X="-21.45246875" Y="-0.288137176514" />
                  <Point X="-21.439998046875" Y="-0.301236999512" />
                  <Point X="-21.410962890625" Y="-0.324155670166" />
                  <Point X="-21.390884765625" Y="-0.335761108398" />
                  <Point X="-21.318453125" Y="-0.377628234863" />
                  <Point X="-21.307291015625" Y="-0.383138244629" />
                  <Point X="-21.283419921875" Y="-0.392149871826" />
                  <Point X="-20.265517578125" Y="-0.664896118164" />
                  <Point X="-20.10852734375" Y="-0.706961425781" />
                  <Point X="-20.14498046875" Y="-0.948749572754" />
                  <Point X="-20.16209375" Y="-1.023744384766" />
                  <Point X="-20.19882421875" Y="-1.18469921875" />
                  <Point X="-21.248392578125" Y="-1.046520996094" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710144043" />
                  <Point X="-21.62533984375" Y="-1.005508728027" />
                  <Point X="-21.66474609375" Y="-1.014073791504" />
                  <Point X="-21.806904296875" Y="-1.044972412109" />
                  <Point X="-21.836025390625" Y="-1.056597412109" />
                  <Point X="-21.8638515625" Y="-1.073489746094" />
                  <Point X="-21.8876015625" Y="-1.093960205078" />
                  <Point X="-21.911419921875" Y="-1.122606323242" />
                  <Point X="-21.997345703125" Y="-1.225948242188" />
                  <Point X="-22.012064453125" Y="-1.250329467773" />
                  <Point X="-22.023408203125" Y="-1.27771484375" />
                  <Point X="-22.030240234375" Y="-1.305364990234" />
                  <Point X="-22.033654296875" Y="-1.342463256836" />
                  <Point X="-22.04596875" Y="-1.476295410156" />
                  <Point X="-22.04365234375" Y="-1.507561767578" />
                  <Point X="-22.035921875" Y="-1.539182739258" />
                  <Point X="-22.02355078125" Y="-1.567994995117" />
                  <Point X="-22.001744140625" Y="-1.601915649414" />
                  <Point X="-21.9230703125" Y="-1.724285522461" />
                  <Point X="-21.9130625" Y="-1.737243286133" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-20.94475" Y="-2.485741943359" />
                  <Point X="-20.786876953125" Y="-2.606882568359" />
                  <Point X="-20.8751953125" Y="-2.749794921875" />
                  <Point X="-20.91059375" Y="-2.800091064453" />
                  <Point X="-20.971017578125" Y="-2.8859453125" />
                  <Point X="-21.90760546875" Y="-2.345205566406" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.2457734375" Y="-2.159825195312" />
                  <Point X="-22.292671875" Y="-2.151355224609" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.55516796875" Y="-2.135177246094" />
                  <Point X="-22.59412890625" Y="-2.155682617187" />
                  <Point X="-22.734685546875" Y="-2.22965625" />
                  <Point X="-22.757615234375" Y="-2.246549804688" />
                  <Point X="-22.77857421875" Y="-2.267509521484" />
                  <Point X="-22.795466796875" Y="-2.290438232422" />
                  <Point X="-22.81597265625" Y="-2.329399902344" />
                  <Point X="-22.889947265625" Y="-2.469956298828" />
                  <Point X="-22.899771484375" Y="-2.499735351563" />
                  <Point X="-22.904728515625" Y="-2.531908447266" />
                  <Point X="-22.90432421875" Y="-2.563260009766" />
                  <Point X="-22.895853515625" Y="-2.610159179688" />
                  <Point X="-22.865296875" Y="-2.779350341797" />
                  <Point X="-22.86101171875" Y="-2.795139892578" />
                  <Point X="-22.8481796875" Y="-2.826078857422" />
                  <Point X="-22.24116796875" Y="-3.877456542969" />
                  <Point X="-22.138716796875" Y="-4.054905761719" />
                  <Point X="-22.218158203125" Y="-4.111648925781" />
                  <Point X="-22.257732421875" Y="-4.137265136719" />
                  <Point X="-22.298232421875" Y="-4.163480957031" />
                  <Point X="-23.019056640625" Y="-3.224085449219" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900556884766" />
                  <Point X="-23.324330078125" Y="-2.870818847656" />
                  <Point X="-23.49119921875" Y="-2.763538085938" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.63348828125" Y="-2.745771972656" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.895404296875" Y="-2.795461181641" />
                  <Point X="-23.934466796875" Y="-2.827940673828" />
                  <Point X="-24.075388671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968861083984" />
                  <Point X="-24.11275" Y="-2.996687744141" />
                  <Point X="-24.124375" Y="-3.025808349609" />
                  <Point X="-24.1360546875" Y="-3.079543701172" />
                  <Point X="-24.178189453125" Y="-3.273396240234" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.008234375" Y="-4.629763671875" />
                  <Point X="-23.97793359375" Y="-4.859915527344" />
                  <Point X="-24.024318359375" Y="-4.870083007812" />
                  <Point X="-24.060890625" Y="-4.8767265625" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.05843359375" Y="-4.752635253906" />
                  <Point X="-26.115619140625" Y="-4.737921875" />
                  <Point X="-26.141244140625" Y="-4.731328613281" />
                  <Point X="-26.131240234375" Y="-4.655331542969" />
                  <Point X="-26.1207734375" Y="-4.575837890625" />
                  <Point X="-26.120076171875" Y="-4.568098632812" />
                  <Point X="-26.11944921875" Y="-4.541030761719" />
                  <Point X="-26.1217578125" Y="-4.509324707031" />
                  <Point X="-26.123333984375" Y="-4.497689941406" />
                  <Point X="-26.136470703125" Y="-4.431647949219" />
                  <Point X="-26.18386328125" Y="-4.1933984375" />
                  <Point X="-26.188125" Y="-4.178469238281" />
                  <Point X="-26.19902734375" Y="-4.149501464844" />
                  <Point X="-26.20566796875" Y="-4.135462890625" />
                  <Point X="-26.221740234375" Y="-4.107625976562" />
                  <Point X="-26.230576171875" Y="-4.094859619141" />
                  <Point X="-26.250208984375" Y="-4.070937255859" />
                  <Point X="-26.261005859375" Y="-4.05978125" />
                  <Point X="-26.311630859375" Y="-4.015383056641" />
                  <Point X="-26.494265625" Y="-3.855216064453" />
                  <Point X="-26.506736328125" Y="-3.845966308594" />
                  <Point X="-26.53301953125" Y="-3.829621826172" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.704005859375" Y="-3.791765625" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166015625" />
                  <Point X="-27.008005859375" Y="-3.781945556641" />
                  <Point X="-27.0390546875" Y="-3.790265136719" />
                  <Point X="-27.05367578125" Y="-3.795496582031" />
                  <Point X="-27.0818671875" Y="-3.808270507812" />
                  <Point X="-27.0954375" Y="-3.815812988281" />
                  <Point X="-27.151423828125" Y="-3.853222900391" />
                  <Point X="-27.35340234375" Y="-3.988181152344" />
                  <Point X="-27.359681640625" Y="-3.992758789062" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032770507812" />
                  <Point X="-27.410470703125" Y="-4.041629150391" />
                  <Point X="-27.503201171875" Y="-4.162478027344" />
                  <Point X="-27.747599609375" Y="-4.011153320313" />
                  <Point X="-27.826765625" Y="-3.950197265625" />
                  <Point X="-27.980865234375" Y="-3.831547119141" />
                  <Point X="-27.50305859375" Y="-3.00396484375" />
                  <Point X="-27.341490234375" Y="-2.724119628906" />
                  <Point X="-27.3348515625" Y="-2.7100859375" />
                  <Point X="-27.32394921875" Y="-2.681121582031" />
                  <Point X="-27.319685546875" Y="-2.666190917969" />
                  <Point X="-27.3134140625" Y="-2.634665771484" />
                  <Point X="-27.311638671875" Y="-2.619241699219" />
                  <Point X="-27.310625" Y="-2.588309570312" />
                  <Point X="-27.3146640625" Y="-2.557625488281" />
                  <Point X="-27.323646484375" Y="-2.528009033203" />
                  <Point X="-27.3293515625" Y="-2.513568603516" />
                  <Point X="-27.34356640625" Y="-2.484739990234" />
                  <Point X="-27.351546875" Y="-2.471422363281" />
                  <Point X="-27.36957421875" Y="-2.446261962891" />
                  <Point X="-27.37962109375" Y="-2.434419189453" />
                  <Point X="-27.39696875" Y="-2.417070556641" />
                  <Point X="-27.408810546875" Y="-2.407023681641" />
                  <Point X="-27.43396875" Y="-2.388995361328" />
                  <Point X="-27.44728515625" Y="-2.381013916016" />
                  <Point X="-27.476115234375" Y="-2.366795410156" />
                  <Point X="-27.4905546875" Y="-2.361088623047" />
                  <Point X="-27.520171875" Y="-2.352103271484" />
                  <Point X="-27.550859375" Y="-2.3480625" />
                  <Point X="-27.581794921875" Y="-2.349074707031" />
                  <Point X="-27.597220703125" Y="-2.350849121094" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.696236328125" Y="-2.961791015625" />
                  <Point X="-28.793087890625" Y="-3.017708251953" />
                  <Point X="-29.004021484375" Y="-2.740586425781" />
                  <Point X="-29.06078125" Y="-2.645408691406" />
                  <Point X="-29.181265625" Y="-2.443373779297" />
                  <Point X="-28.330814453125" Y="-1.790800170898" />
                  <Point X="-28.048123046875" Y="-1.573882080078" />
                  <Point X="-28.039162109375" Y="-1.566069091797" />
                  <Point X="-28.01627734375" Y="-1.543442749023" />
                  <Point X="-28.002638671875" Y="-1.526442382812" />
                  <Point X="-27.98359375" Y="-1.496142700195" />
                  <Point X="-27.97551171875" Y="-1.480088500977" />
                  <Point X="-27.967119140625" Y="-1.458557006836" />
                  <Point X="-27.960212890625" Y="-1.437184692383" />
                  <Point X="-27.954181640625" Y="-1.413893676758" />
                  <Point X="-27.95194921875" Y="-1.40238659668" />
                  <Point X="-27.9478359375" Y="-1.370905151367" />
                  <Point X="-27.947080078125" Y="-1.355685913086" />
                  <Point X="-27.94778125" Y="-1.332817382812" />
                  <Point X="-27.951228515625" Y="-1.31020703125" />
                  <Point X="-27.957375" Y="-1.288168945312" />
                  <Point X="-27.961115234375" Y="-1.277331787109" />
                  <Point X="-27.97312109375" Y="-1.248350830078" />
                  <Point X="-27.97869921875" Y="-1.23706652832" />
                  <Point X="-27.99129296875" Y="-1.215340942383" />
                  <Point X="-28.006642578125" Y="-1.195476196289" />
                  <Point X="-28.024486328125" Y="-1.177808959961" />
                  <Point X="-28.03399609375" Y="-1.169565063477" />
                  <Point X="-28.05224609375" Y="-1.155753295898" />
                  <Point X="-28.07053125" Y="-1.143513549805" />
                  <Point X="-28.091265625" Y="-1.131309936523" />
                  <Point X="-28.105431640625" Y="-1.124482177734" />
                  <Point X="-28.1346953125" Y="-1.1132578125" />
                  <Point X="-28.14979296875" Y="-1.108861083984" />
                  <Point X="-28.1816796875" Y="-1.102379150391" />
                  <Point X="-28.197294921875" Y="-1.100532714844" />
                  <Point X="-28.228619140625" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-29.518794921875" Y="-1.267983154297" />
                  <Point X="-29.66091796875" Y="-1.286694213867" />
                  <Point X="-29.740763671875" Y="-0.974109680176" />
                  <Point X="-29.755779296875" Y="-0.869114807129" />
                  <Point X="-29.786451171875" Y="-0.654654724121" />
                  <Point X="-28.83031640625" Y="-0.398458953857" />
                  <Point X="-28.508287109375" Y="-0.312171203613" />
                  <Point X="-28.49818359375" Y="-0.308847717285" />
                  <Point X="-28.478412109375" Y="-0.301092132568" />
                  <Point X="-28.468744140625" Y="-0.29666003418" />
                  <Point X="-28.447154296875" Y="-0.285259765625" />
                  <Point X="-28.427541015625" Y="-0.273335632324" />
                  <Point X="-28.405810546875" Y="-0.258254119873" />
                  <Point X="-28.39648046875" Y="-0.250871124268" />
                  <Point X="-28.372525390625" Y="-0.229345336914" />
                  <Point X="-28.3580703125" Y="-0.212984191895" />
                  <Point X="-28.3375703125" Y="-0.183555831909" />
                  <Point X="-28.328720703125" Y="-0.167864227295" />
                  <Point X="-28.319287109375" Y="-0.146656051636" />
                  <Point X="-28.311423828125" Y="-0.125748527527" />
                  <Point X="-28.304181640625" Y="-0.102410728455" />
                  <Point X="-28.301572265625" Y="-0.091928504944" />
                  <Point X="-28.296130859375" Y="-0.063190349579" />
                  <Point X="-28.294498046875" Y="-0.043291980743" />
                  <Point X="-28.295267578125" Y="-0.010439940453" />
                  <Point X="-28.297083984375" Y="0.005950780869" />
                  <Point X="-28.301337890625" Y="0.027238922119" />
                  <Point X="-28.306189453125" Y="0.046319568634" />
                  <Point X="-28.313431640625" Y="0.069657516479" />
                  <Point X="-28.317666015625" Y="0.080782974243" />
                  <Point X="-28.330984375" Y="0.110110427856" />
                  <Point X="-28.342314453125" Y="0.128922210693" />
                  <Point X="-28.36440234375" Y="0.157502243042" />
                  <Point X="-28.376818359375" Y="0.170733383179" />
                  <Point X="-28.394662109375" Y="0.18643270874" />
                  <Point X="-28.411833984375" Y="0.199874755859" />
                  <Point X="-28.433564453125" Y="0.214956268311" />
                  <Point X="-28.44048828125" Y="0.219332015991" />
                  <Point X="-28.465615234375" Y="0.23283454895" />
                  <Point X="-28.496564453125" Y="0.2456355896" />
                  <Point X="-28.508287109375" Y="0.249611236572" />
                  <Point X="-29.6700234375" Y="0.560897460938" />
                  <Point X="-29.7854453125" Y="0.591824829102" />
                  <Point X="-29.73133203125" Y="0.957522766113" />
                  <Point X="-29.70109765625" Y="1.069096923828" />
                  <Point X="-29.6335859375" Y="1.318237182617" />
                  <Point X="-29.008904296875" Y="1.23599621582" />
                  <Point X="-28.778064453125" Y="1.20560559082" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.20470324707" />
                  <Point X="-28.71514453125" Y="1.206589477539" />
                  <Point X="-28.704890625" Y="1.208053710938" />
                  <Point X="-28.684599609375" Y="1.21208984375" />
                  <Point X="-28.661232421875" Y="1.218865722656" />
                  <Point X="-28.61313671875" Y="1.234029663086" />
                  <Point X="-28.603443359375" Y="1.237678100586" />
                  <Point X="-28.58451171875" Y="1.246008789062" />
                  <Point X="-28.5752734375" Y="1.250690673828" />
                  <Point X="-28.556529296875" Y="1.261513183594" />
                  <Point X="-28.547853515625" Y="1.267174438477" />
                  <Point X="-28.53117578125" Y="1.279403930664" />
                  <Point X="-28.5159296875" Y="1.293375" />
                  <Point X="-28.50229296875" Y="1.308924072266" />
                  <Point X="-28.495900390625" Y="1.3170703125" />
                  <Point X="-28.483484375" Y="1.33480065918" />
                  <Point X="-28.478013671875" Y="1.343596801758" />
                  <Point X="-28.4680625" Y="1.361733886719" />
                  <Point X="-28.458232421875" Y="1.383989501953" />
                  <Point X="-28.438935546875" Y="1.430579223633" />
                  <Point X="-28.435498046875" Y="1.440349609375" />
                  <Point X="-28.429708984375" Y="1.460209716797" />
                  <Point X="-28.427357421875" Y="1.470299194336" />
                  <Point X="-28.423599609375" Y="1.491611572266" />
                  <Point X="-28.422359375" Y="1.501890136719" />
                  <Point X="-28.421005859375" Y="1.522526855469" />
                  <Point X="-28.421908203125" Y="1.543188964844" />
                  <Point X="-28.425052734375" Y="1.563629394531" />
                  <Point X="-28.427181640625" Y="1.573765869141" />
                  <Point X="-28.43278125" Y="1.594669921875" />
                  <Point X="-28.436005859375" Y="1.604518798828" />
                  <Point X="-28.44350390625" Y="1.623801147461" />
                  <Point X="-28.454234375" Y="1.645639038086" />
                  <Point X="-28.477521484375" Y="1.690369384766" />
                  <Point X="-28.482798828125" Y="1.699283447266" />
                  <Point X="-28.49429296875" Y="1.716486572266" />
                  <Point X="-28.500509765625" Y="1.724776000977" />
                  <Point X="-28.514421875" Y="1.741354858398" />
                  <Point X="-28.52150390625" Y="1.748915527344" />
                  <Point X="-28.5364453125" Y="1.763217651367" />
                  <Point X="-28.5443046875" Y="1.769958984375" />
                  <Point X="-29.210677734375" Y="2.281285888672" />
                  <Point X="-29.22761328125" Y="2.294280761719" />
                  <Point X="-29.00228125" Y="2.680327880859" />
                  <Point X="-28.92221484375" Y="2.783243896484" />
                  <Point X="-28.726337890625" Y="3.035013916016" />
                  <Point X="-28.395931640625" Y="2.844252685547" />
                  <Point X="-28.25415625" Y="2.762398925781" />
                  <Point X="-28.24491796875" Y="2.757716796875" />
                  <Point X="-28.225984375" Y="2.749385742188" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.13650390625" Y="2.729533447266" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310791016" />
                  <Point X="-27.97643359375" Y="2.734227294922" />
                  <Point X="-27.95699609375" Y="2.741302001953" />
                  <Point X="-27.9384453125" Y="2.750450439453" />
                  <Point X="-27.929419921875" Y="2.755530761719" />
                  <Point X="-27.911166015625" Y="2.767159667969" />
                  <Point X="-27.902748046875" Y="2.773192138672" />
                  <Point X="-27.8866171875" Y="2.786138183594" />
                  <Point X="-27.865724609375" Y="2.806230957031" />
                  <Point X="-27.8181796875" Y="2.853775146484" />
                  <Point X="-27.811265625" Y="2.861486816406" />
                  <Point X="-27.79831640625" Y="2.877620361328" />
                  <Point X="-27.79228125" Y="2.886042236328" />
                  <Point X="-27.78065234375" Y="2.904296386719" />
                  <Point X="-27.7755703125" Y="2.913324951172" />
                  <Point X="-27.766423828125" Y="2.931874023438" />
                  <Point X="-27.759353515625" Y="2.951298583984" />
                  <Point X="-27.754435546875" Y="2.971387695312" />
                  <Point X="-27.7525234375" Y="2.981572753906" />
                  <Point X="-27.749697265625" Y="3.003031494141" />
                  <Point X="-27.748908203125" Y="3.013364746094" />
                  <Point X="-27.74845703125" Y="3.034052001953" />
                  <Point X="-27.750419921875" Y="3.062973388672" />
                  <Point X="-27.756279296875" Y="3.129955566406" />
                  <Point X="-27.757744140625" Y="3.140212646484" />
                  <Point X="-27.76178125" Y="3.160501464844" />
                  <Point X="-27.764353515625" Y="3.170533203125" />
                  <Point X="-27.77086328125" Y="3.191176025391" />
                  <Point X="-27.77451171875" Y="3.200870605469" />
                  <Point X="-27.782841796875" Y="3.219799072266" />
                  <Point X="-27.7875234375" Y="3.229032958984" />
                  <Point X="-28.059388671875" Y="3.699916015625" />
                  <Point X="-27.648369140625" Y="4.015039306641" />
                  <Point X="-27.5222578125" Y="4.085104248047" />
                  <Point X="-27.192525390625" Y="4.268296386719" />
                  <Point X="-27.161966796875" Y="4.228470214844" />
                  <Point X="-27.118564453125" Y="4.171908203125" />
                  <Point X="-27.111818359375" Y="4.164045410156" />
                  <Point X="-27.097513671875" Y="4.149103027344" />
                  <Point X="-27.089955078125" Y="4.142022949219" />
                  <Point X="-27.073375" Y="4.128110839844" />
                  <Point X="-27.065087890625" Y="4.121895019531" />
                  <Point X="-27.047888671875" Y="4.110403320312" />
                  <Point X="-27.0389765625" Y="4.105127441406" />
                  <Point X="-27.018310546875" Y="4.094369873047" />
                  <Point X="-26.943759765625" Y="4.055560791016" />
                  <Point X="-26.93432421875" Y="4.05128515625" />
                  <Point X="-26.915044921875" Y="4.043788574219" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031378173828" />
                  <Point X="-26.780818359375" Y="4.035136474609" />
                  <Point X="-26.770728515625" Y="4.037488525391" />
                  <Point X="-26.750865234375" Y="4.043278076172" />
                  <Point X="-26.741091796875" Y="4.046715332031" />
                  <Point X="-26.719568359375" Y="4.055631347656" />
                  <Point X="-26.64191796875" Y="4.087794921875" />
                  <Point X="-26.632580078125" Y="4.092274169922" />
                  <Point X="-26.61444921875" Y="4.102221191406" />
                  <Point X="-26.60565625" Y="4.107689453125" />
                  <Point X="-26.58792578125" Y="4.120104003906" />
                  <Point X="-26.579775390625" Y="4.126499511719" />
                  <Point X="-26.564220703125" Y="4.140140625" />
                  <Point X="-26.550244140625" Y="4.15539453125" />
                  <Point X="-26.53801171875" Y="4.172079101562" />
                  <Point X="-26.5323515625" Y="4.180755371094" />
                  <Point X="-26.52153125" Y="4.199499511719" />
                  <Point X="-26.5168515625" Y="4.208732421875" />
                  <Point X="-26.5085234375" Y="4.227659179688" />
                  <Point X="-26.504875" Y="4.237353027344" />
                  <Point X="-26.497869140625" Y="4.259572265625" />
                  <Point X="-26.472595703125" Y="4.339729492188" />
                  <Point X="-26.470025390625" Y="4.349763671875" />
                  <Point X="-26.465990234375" Y="4.370049804688" />
                  <Point X="-26.464525390625" Y="4.380301757812" />
                  <Point X="-26.462638671875" Y="4.401862304688" />
                  <Point X="-26.46230078125" Y="4.412215820312" />
                  <Point X="-26.462751953125" Y="4.432897949219" />
                  <Point X="-26.463541015625" Y="4.4432265625" />
                  <Point X="-26.479265625" Y="4.562655273438" />
                  <Point X="-25.931173828125" Y="4.7163203125" />
                  <Point X="-25.778294921875" Y="4.734212890625" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.267607421875" Y="4.418253417969" />
                  <Point X="-25.22566796875" Y="4.261728027344" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912597656" />
                  <Point X="-25.2001171875" Y="4.205341308594" />
                  <Point X="-25.1822578125" Y="4.178614257812" />
                  <Point X="-25.17260546875" Y="4.166452636719" />
                  <Point X="-25.15144921875" Y="4.143865234375" />
                  <Point X="-25.126896484375" Y="4.125025390625" />
                  <Point X="-25.0996015625" Y="4.110436035156" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-24.04562890625" Y="4.707372558594" />
                  <Point X="-23.546408203125" Y="4.586844726562" />
                  <Point X="-23.466201171875" Y="4.55775390625" />
                  <Point X="-23.141734375" Y="4.440067871094" />
                  <Point X="-23.06212109375" Y="4.402834960938" />
                  <Point X="-22.7495546875" Y="4.256658203125" />
                  <Point X="-22.672572265625" Y="4.211808105469" />
                  <Point X="-22.37055859375" Y="4.035854980469" />
                  <Point X="-22.29801953125" Y="3.984267578125" />
                  <Point X="-22.182216796875" Y="3.901915527344" />
                  <Point X="-22.74768359375" Y="2.922498046875" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.93915625" Y="2.589840576172" />
                  <Point X="-22.9507109375" Y="2.562722900391" />
                  <Point X="-22.960373046875" Y="2.533215087891" />
                  <Point X="-22.963357421875" Y="2.523175537109" />
                  <Point X="-22.98016796875" Y="2.460314208984" />
                  <Point X="-22.98266796875" Y="2.447479736328" />
                  <Point X="-22.9858828125" Y="2.42159375" />
                  <Point X="-22.98659765625" Y="2.408542236328" />
                  <Point X="-22.9861171875" Y="2.374566894531" />
                  <Point X="-22.98476953125" Y="2.354511230469" />
                  <Point X="-22.978216796875" Y="2.300160400391" />
                  <Point X="-22.97619921875" Y="2.289039794922" />
                  <Point X="-22.970857421875" Y="2.267118652344" />
                  <Point X="-22.967533203125" Y="2.256318115234" />
                  <Point X="-22.959267578125" Y="2.234227783203" />
                  <Point X="-22.954685546875" Y="2.223896728516" />
                  <Point X="-22.9443203125" Y="2.203844482422" />
                  <Point X="-22.929212890625" Y="2.180383056641" />
                  <Point X="-22.895578125" Y="2.130814697266" />
                  <Point X="-22.8874296875" Y="2.120439941406" />
                  <Point X="-22.869779296875" Y="2.100921875" />
                  <Point X="-22.86027734375" Y="2.091778564453" />
                  <Point X="-22.833236328125" Y="2.069155029297" />
                  <Point X="-22.818001953125" Y="2.057657470703" />
                  <Point X="-22.76843359375" Y="2.0240234375" />
                  <Point X="-22.75871875" Y="2.018244628906" />
                  <Point X="-22.738673828125" Y="2.007882080078" />
                  <Point X="-22.72834375" Y="2.003298339844" />
                  <Point X="-22.706255859375" Y="1.995032470703" />
                  <Point X="-22.695451171875" Y="1.991707519531" />
                  <Point X="-22.67352734375" Y="1.986364746094" />
                  <Point X="-22.647341796875" Y="1.982529907227" />
                  <Point X="-22.592984375" Y="1.975975341797" />
                  <Point X="-22.579541015625" Y="1.975314575195" />
                  <Point X="-22.552693359375" Y="1.975900024414" />
                  <Point X="-22.5392890625" Y="1.977146118164" />
                  <Point X="-22.5029609375" Y="1.983143432617" />
                  <Point X="-22.48482421875" Y="1.987055664062" />
                  <Point X="-22.421962890625" Y="2.003865722656" />
                  <Point X="-22.416" Y="2.005671875" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572875977" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.195482421875" Y="2.702496582031" />
                  <Point X="-21.059595703125" Y="2.780950683594" />
                  <Point X="-20.95603125" Y="2.63701953125" />
                  <Point X="-20.915603515625" Y="2.570211425781" />
                  <Point X="-20.863115234375" Y="2.483471679688" />
                  <Point X="-21.580837890625" Y="1.932741699219" />
                  <Point X="-21.827046875" Y="1.743819946289" />
                  <Point X="-21.831859375" Y="1.739869384766" />
                  <Point X="-21.847876953125" Y="1.725219970703" />
                  <Point X="-21.865330078125" Y="1.706604492188" />
                  <Point X="-21.871421875" Y="1.699423095703" />
                  <Point X="-21.883962890625" Y="1.68306262207" />
                  <Point X="-21.929203125" Y="1.624041503906" />
                  <Point X="-21.9365703125" Y="1.612885986328" />
                  <Point X="-21.94966796875" Y="1.589643798828" />
                  <Point X="-21.9553984375" Y="1.577556884766" />
                  <Point X="-21.9685390625" Y="1.54390234375" />
                  <Point X="-21.97452734375" Y="1.525971679688" />
                  <Point X="-21.991380859375" Y="1.465711547852" />
                  <Point X="-21.993771484375" Y="1.454669921875" />
                  <Point X="-21.9972265625" Y="1.432370849609" />
                  <Point X="-21.998291015625" Y="1.42111328125" />
                  <Point X="-21.999107421875" Y="1.397542480469" />
                  <Point X="-21.998826171875" Y="1.386239013672" />
                  <Point X="-21.996921875" Y="1.363747924805" />
                  <Point X="-21.991462890625" Y="1.333974487305" />
                  <Point X="-21.97762890625" Y="1.266927124023" />
                  <Point X="-21.973970703125" Y="1.253944580078" />
                  <Point X="-21.96485546875" Y="1.228630737305" />
                  <Point X="-21.9593984375" Y="1.216299804688" />
                  <Point X="-21.941771484375" Y="1.183068725586" />
                  <Point X="-21.932650390625" Y="1.167673217773" />
                  <Point X="-21.8950234375" Y="1.110481323242" />
                  <Point X="-21.888263671875" Y="1.101428466797" />
                  <Point X="-21.8737109375" Y="1.084182861328" />
                  <Point X="-21.86591796875" Y="1.075989868164" />
                  <Point X="-21.848673828125" Y="1.059900268555" />
                  <Point X="-21.83996484375" Y="1.052694091797" />
                  <Point X="-21.82175390625" Y="1.039368164063" />
                  <Point X="-21.79713671875" Y="1.024740356445" />
                  <Point X="-21.742609375" Y="0.994046264648" />
                  <Point X="-21.734517578125" Y="0.989986206055" />
                  <Point X="-21.705318359375" Y="0.978083557129" />
                  <Point X="-21.66972265625" Y="0.968021240234" />
                  <Point X="-21.656328125" Y="0.965257751465" />
                  <Point X="-21.635890625" Y="0.962556640625" />
                  <Point X="-21.562166015625" Y="0.952812927246" />
                  <Point X="-21.555966796875" Y="0.952199707031" />
                  <Point X="-21.53428125" Y="0.951222839355" />
                  <Point X="-21.50878125" Y="0.952032348633" />
                  <Point X="-21.49939453125" Y="0.952797180176" />
                  <Point X="-20.3894140625" Y="1.098929077148" />
                  <Point X="-20.295298828125" Y="1.111319702148" />
                  <Point X="-20.2473125" Y="0.91420513916" />
                  <Point X="-20.2345703125" Y="0.832370483398" />
                  <Point X="-20.21612890625" Y="0.713921081543" />
                  <Point X="-21.02584765625" Y="0.496957397461" />
                  <Point X="-21.3080078125" Y="0.421352813721" />
                  <Point X="-21.31396875" Y="0.419544219971" />
                  <Point X="-21.334376953125" Y="0.412136566162" />
                  <Point X="-21.357619140625" Y="0.401619110107" />
                  <Point X="-21.365994140625" Y="0.397316925049" />
                  <Point X="-21.386072265625" Y="0.385711578369" />
                  <Point X="-21.45850390625" Y="0.343844360352" />
                  <Point X="-21.46979296875" Y="0.336188201904" />
                  <Point X="-21.491154296875" Y="0.319340759277" />
                  <Point X="-21.5012265625" Y="0.310149780273" />
                  <Point X="-21.5275859375" Y="0.282379150391" />
                  <Point X="-21.53925" Y="0.26887588501" />
                  <Point X="-21.582708984375" Y="0.213498413086" />
                  <Point X="-21.58914453125" Y="0.20420741272" />
                  <Point X="-21.600869140625" Y="0.184926269531" />
                  <Point X="-21.606158203125" Y="0.174936141968" />
                  <Point X="-21.615931640625" Y="0.153472335815" />
                  <Point X="-21.619994140625" Y="0.14292678833" />
                  <Point X="-21.62683984375" Y="0.121427314758" />
                  <Point X="-21.629623046875" Y="0.110473068237" />
                  <Point X="-21.633638671875" Y="0.089505203247" />
                  <Point X="-21.648125" Y="0.013862569809" />
                  <Point X="-21.649396484375" Y="0.004966303349" />
                  <Point X="-21.6514140625" Y="-0.026261734009" />
                  <Point X="-21.64971875" Y="-0.062939918518" />
                  <Point X="-21.648125" Y="-0.076422683716" />
                  <Point X="-21.644109375" Y="-0.097390548706" />
                  <Point X="-21.629623046875" Y="-0.173033325195" />
                  <Point X="-21.62683984375" Y="-0.183987426758" />
                  <Point X="-21.619994140625" Y="-0.205487060547" />
                  <Point X="-21.615931640625" Y="-0.216032440186" />
                  <Point X="-21.606158203125" Y="-0.237496261597" />
                  <Point X="-21.60087109375" Y="-0.247485046387" />
                  <Point X="-21.589146484375" Y="-0.266766937256" />
                  <Point X="-21.570662109375" Y="-0.291410522461" />
                  <Point X="-21.527203125" Y="-0.346787994385" />
                  <Point X="-21.521275390625" Y="-0.353639801025" />
                  <Point X="-21.498857421875" Y="-0.37580557251" />
                  <Point X="-21.469822265625" Y="-0.398724243164" />
                  <Point X="-21.45850390625" Y="-0.40640447998" />
                  <Point X="-21.43842578125" Y="-0.418009857178" />
                  <Point X="-21.365994140625" Y="-0.459877044678" />
                  <Point X="-21.36050390625" Y="-0.462814575195" />
                  <Point X="-21.34084375" Y="-0.472015960693" />
                  <Point X="-21.31697265625" Y="-0.481027557373" />
                  <Point X="-21.3080078125" Y="-0.483912780762" />
                  <Point X="-20.29010546875" Y="-0.756659057617" />
                  <Point X="-20.21512109375" Y="-0.776750793457" />
                  <Point X="-20.23838671875" Y="-0.931063964844" />
                  <Point X="-20.254712890625" Y="-1.002609313965" />
                  <Point X="-20.2721953125" Y="-1.079219970703" />
                  <Point X="-21.2359921875" Y="-0.95233380127" />
                  <Point X="-21.563216796875" Y="-0.909253662109" />
                  <Point X="-21.571375" Y="-0.908535705566" />
                  <Point X="-21.59990234375" Y="-0.908042419434" />
                  <Point X="-21.63327734375" Y="-0.910840942383" />
                  <Point X="-21.645517578125" Y="-0.912676147461" />
                  <Point X="-21.684923828125" Y="-0.921241271973" />
                  <Point X="-21.82708203125" Y="-0.952139831543" />
                  <Point X="-21.842125" Y="-0.956742675781" />
                  <Point X="-21.87124609375" Y="-0.968367675781" />
                  <Point X="-21.88532421875" Y="-0.975389770508" />
                  <Point X="-21.913150390625" Y="-0.992282104492" />
                  <Point X="-21.925875" Y="-1.001530334473" />
                  <Point X="-21.949625" Y="-1.022000732422" />
                  <Point X="-21.960650390625" Y="-1.033223144531" />
                  <Point X="-21.98446875" Y="-1.061869262695" />
                  <Point X="-22.07039453125" Y="-1.165211181641" />
                  <Point X="-22.078673828125" Y="-1.176850585938" />
                  <Point X="-22.093392578125" Y="-1.201231811523" />
                  <Point X="-22.09983203125" Y="-1.213973510742" />
                  <Point X="-22.11117578125" Y="-1.241358886719" />
                  <Point X="-22.115634765625" Y="-1.254926757812" />
                  <Point X="-22.122466796875" Y="-1.282576904297" />
                  <Point X="-22.12483984375" Y="-1.296659179688" />
                  <Point X="-22.12825390625" Y="-1.333757324219" />
                  <Point X="-22.140568359375" Y="-1.467589599609" />
                  <Point X="-22.140708984375" Y="-1.483314331055" />
                  <Point X="-22.138392578125" Y="-1.514580688477" />
                  <Point X="-22.135935546875" Y="-1.530122314453" />
                  <Point X="-22.128205078125" Y="-1.561743286133" />
                  <Point X="-22.12321484375" Y="-1.576663818359" />
                  <Point X="-22.11084375" Y="-1.605476196289" />
                  <Point X="-22.103462890625" Y="-1.619367797852" />
                  <Point X="-22.08165625" Y="-1.653288452148" />
                  <Point X="-22.002982421875" Y="-1.775658325195" />
                  <Point X="-21.998255859375" Y="-1.782354858398" />
                  <Point X="-21.98019921875" Y="-1.804457397461" />
                  <Point X="-21.956505859375" Y="-1.828123291016" />
                  <Point X="-21.947201171875" Y="-1.836277709961" />
                  <Point X="-21.00258203125" Y="-2.561110351562" />
                  <Point X="-20.912828125" Y="-2.629981201172" />
                  <Point X="-20.954513671875" Y="-2.697435058594" />
                  <Point X="-20.98828125" Y="-2.7454140625" />
                  <Point X="-20.998724609375" Y="-2.760252197266" />
                  <Point X="-21.86010546875" Y="-2.262933105469" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.184970703125" Y="-2.079513916016" />
                  <Point X="-22.216876953125" Y="-2.069326416016" />
                  <Point X="-22.228888671875" Y="-2.066337646484" />
                  <Point X="-22.275787109375" Y="-2.057867675781" />
                  <Point X="-22.444978515625" Y="-2.027312011719" />
                  <Point X="-22.460638671875" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.555154296875" Y="-2.035136230469" />
                  <Point X="-22.584931640625" Y="-2.044960449219" />
                  <Point X="-22.5994140625" Y="-2.051109619141" />
                  <Point X="-22.638375" Y="-2.071614990234" />
                  <Point X="-22.778931640625" Y="-2.145588623047" />
                  <Point X="-22.79103515625" Y="-2.153172851563" />
                  <Point X="-22.81396484375" Y="-2.17006640625" />
                  <Point X="-22.824791015625" Y="-2.179375732422" />
                  <Point X="-22.84575" Y="-2.200335449219" />
                  <Point X="-22.85505859375" Y="-2.211160400391" />
                  <Point X="-22.871951171875" Y="-2.234089111328" />
                  <Point X="-22.87953515625" Y="-2.246192871094" />
                  <Point X="-22.900041015625" Y="-2.285154541016" />
                  <Point X="-22.974015625" Y="-2.4257109375" />
                  <Point X="-22.9801640625" Y="-2.440193359375" />
                  <Point X="-22.98998828125" Y="-2.469972412109" />
                  <Point X="-22.9936640625" Y="-2.485269042969" />
                  <Point X="-22.99862109375" Y="-2.517442138672" />
                  <Point X="-22.999720703125" Y="-2.533133544922" />
                  <Point X="-22.99931640625" Y="-2.564485107422" />
                  <Point X="-22.9978125" Y="-2.580145263672" />
                  <Point X="-22.989341796875" Y="-2.627044433594" />
                  <Point X="-22.95878515625" Y="-2.796235595703" />
                  <Point X="-22.95698046875" Y="-2.804232421875" />
                  <Point X="-22.948763671875" Y="-2.83153515625" />
                  <Point X="-22.935931640625" Y="-2.862474121094" />
                  <Point X="-22.930453125" Y="-2.873578857422" />
                  <Point X="-22.32344140625" Y="-3.924956542969" />
                  <Point X="-22.264107421875" Y="-4.027724609375" />
                  <Point X="-22.276244140625" Y="-4.036081542969" />
                  <Point X="-22.9436875" Y="-3.166253173828" />
                  <Point X="-23.16608203125" Y="-2.876422851562" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849626220703" />
                  <Point X="-23.21674609375" Y="-2.828004882812" />
                  <Point X="-23.22669921875" Y="-2.820646484375" />
                  <Point X="-23.272955078125" Y="-2.790908447266" />
                  <Point X="-23.43982421875" Y="-2.683627685547" />
                  <Point X="-23.453716796875" Y="-2.676244628906" />
                  <Point X="-23.482529296875" Y="-2.663873291016" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.642193359375" Y="-2.651171630859" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920123046875" Y="-2.699413085938" />
                  <Point X="-23.944505859375" Y="-2.714134033203" />
                  <Point X="-23.956142578125" Y="-2.722413330078" />
                  <Point X="-23.995205078125" Y="-2.754892822266" />
                  <Point X="-24.136126953125" Y="-2.872063720703" />
                  <Point X="-24.147349609375" Y="-2.883091064453" />
                  <Point X="-24.167818359375" Y="-2.906840576172" />
                  <Point X="-24.177064453125" Y="-2.919562744141" />
                  <Point X="-24.19395703125" Y="-2.947389404297" />
                  <Point X="-24.20098046875" Y="-2.961466308594" />
                  <Point X="-24.21260546875" Y="-2.990586914062" />
                  <Point X="-24.21720703125" Y="-3.005630615234" />
                  <Point X="-24.22888671875" Y="-3.059365966797" />
                  <Point X="-24.271021484375" Y="-3.253218505859" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520263672" />
                  <Point X="-24.16691015625" Y="-4.152324707031" />
                  <Point X="-24.2579921875" Y="-3.81240234375" />
                  <Point X="-24.344931640625" Y="-3.487936767578" />
                  <Point X="-24.347392578125" Y="-3.480121582031" />
                  <Point X="-24.357853515625" Y="-3.453578857422" />
                  <Point X="-24.3732109375" Y="-3.423815429688" />
                  <Point X="-24.37958984375" Y="-3.413209228516" />
                  <Point X="-24.415125" Y="-3.362010498047" />
                  <Point X="-24.543318359375" Y="-3.177308837891" />
                  <Point X="-24.553328125" Y="-3.165171386719" />
                  <Point X="-24.575212890625" Y="-3.142715820312" />
                  <Point X="-24.587087890625" Y="-3.132397705078" />
                  <Point X="-24.61334375" Y="-3.113153808594" />
                  <Point X="-24.6267578125" Y="-3.104936767578" />
                  <Point X="-24.6547578125" Y="-3.090829345703" />
                  <Point X="-24.66934375" Y="-3.084938964844" />
                  <Point X="-24.72433203125" Y="-3.067872558594" />
                  <Point X="-24.922703125" Y="-3.006305664062" />
                  <Point X="-24.936623046875" Y="-3.003109619141" />
                  <Point X="-24.964783203125" Y="-2.998840087891" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.998840087891" />
                  <Point X="-25.051064453125" Y="-3.003109619141" />
                  <Point X="-25.064984375" Y="-3.006305664062" />
                  <Point X="-25.119970703125" Y="-3.023371826172" />
                  <Point X="-25.31834375" Y="-3.084938964844" />
                  <Point X="-25.3329296875" Y="-3.090829345703" />
                  <Point X="-25.3609296875" Y="-3.104936767578" />
                  <Point X="-25.37434375" Y="-3.113153808594" />
                  <Point X="-25.400599609375" Y="-3.132397705078" />
                  <Point X="-25.4124765625" Y="-3.14271875" />
                  <Point X="-25.434361328125" Y="-3.165176269531" />
                  <Point X="-25.444369140625" Y="-3.177312744141" />
                  <Point X="-25.47990234375" Y="-3.228511474609" />
                  <Point X="-25.608095703125" Y="-3.413213134766" />
                  <Point X="-25.612466796875" Y="-3.420129394531" />
                  <Point X="-25.625974609375" Y="-3.445260742188" />
                  <Point X="-25.63877734375" Y="-3.476213378906" />
                  <Point X="-25.64275390625" Y="-3.487936279297" />
                  <Point X="-25.95639453125" Y="-4.658460449219" />
                  <Point X="-25.98542578125" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.757206823443" Y="-0.646818717481" />
                  <Point X="-29.565421064343" Y="-1.274121669819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.665387942655" Y="-0.622215900248" />
                  <Point X="-29.469924137865" Y="-1.261549197964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.122411094403" Y="-2.398213146127" />
                  <Point X="-29.032165570199" Y="-2.693392955279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.573569061867" Y="-0.597613083016" />
                  <Point X="-29.374427190329" Y="-1.248976794986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.041946893828" Y="-2.33647084339" />
                  <Point X="-28.860915824738" Y="-2.92859678975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.481750181079" Y="-0.573010265784" />
                  <Point X="-29.278930242792" Y="-1.236404392008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.961482693252" Y="-2.274728540654" />
                  <Point X="-28.743146357891" Y="-2.988874514878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.770145889667" Y="0.695218436698" />
                  <Point X="-29.734349483463" Y="0.578133667731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.389931300291" Y="-0.548407448551" />
                  <Point X="-29.183433295256" Y="-1.223831989031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.881018492676" Y="-2.212986237917" />
                  <Point X="-28.658709832709" Y="-2.940125100481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.737746508867" Y="0.914173681055" />
                  <Point X="-29.626144560014" Y="0.54914015442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.298112419503" Y="-0.523804631319" />
                  <Point X="-29.087936347719" Y="-1.211259586053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.8005542921" Y="-2.15124393518" />
                  <Point X="-28.57427330693" Y="-2.891375688037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.693895915355" Y="1.095673696328" />
                  <Point X="-29.517939669706" Y="0.520146749509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.206293538715" Y="-0.499201814086" />
                  <Point X="-28.992439400183" Y="-1.198687183076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.720090091525" Y="-2.089501632444" />
                  <Point X="-28.489836781151" Y="-2.842626275593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.64721864182" Y="1.267928057845" />
                  <Point X="-29.409734779399" Y="0.491153344597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.114474657927" Y="-0.474598996854" />
                  <Point X="-28.896942452647" Y="-1.186114780098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.639625890949" Y="-2.027759329707" />
                  <Point X="-28.405400255372" Y="-2.793876863149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.560309576746" Y="1.308590158661" />
                  <Point X="-29.301529889091" Y="0.462159939686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.022655777139" Y="-0.449996179621" />
                  <Point X="-28.80144550511" Y="-1.17354237712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.559161690373" Y="-1.96601702697" />
                  <Point X="-28.320963729593" Y="-2.745127450705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.456802682397" Y="1.294963206134" />
                  <Point X="-29.193324998783" Y="0.433166534774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.930836896351" Y="-0.425393362389" />
                  <Point X="-28.705948557574" Y="-1.160969974143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.478697489797" Y="-1.904274724234" />
                  <Point X="-28.236527203814" Y="-2.696378038262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.921112562535" Y="-3.728052843596" />
                  <Point X="-27.861333450028" Y="-3.923581510271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.353295788048" Y="1.281336253607" />
                  <Point X="-29.085120108476" Y="0.404173129863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.839018015563" Y="-0.400790545157" />
                  <Point X="-28.610451610038" Y="-1.148397571165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.398233289221" Y="-1.842532421497" />
                  <Point X="-28.152090678035" Y="-2.647628625818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.856164423841" Y="-3.615559789225" />
                  <Point X="-27.732328529786" Y="-4.020608747559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.2497888937" Y="1.26770930108" />
                  <Point X="-28.976915218168" Y="0.375179724951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.747199148055" Y="-0.376187684486" />
                  <Point X="-28.514954662501" Y="-1.135825168188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.317769106027" Y="-1.78079006191" />
                  <Point X="-28.067654152256" Y="-2.598879213374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.791216285147" Y="-3.503066734854" />
                  <Point X="-27.609791576989" Y="-4.096480216592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.146281999351" Y="1.254082348553" />
                  <Point X="-28.86871032786" Y="0.34618632004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.655380281937" Y="-0.351584819268" />
                  <Point X="-28.419457714965" Y="-1.12325276521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.237305012657" Y="-1.719047408517" />
                  <Point X="-27.983217626477" Y="-2.55012980093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.726268146453" Y="-3.390573680482" />
                  <Point X="-27.493956657161" Y="-4.150430323539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.042775105003" Y="1.240455396026" />
                  <Point X="-28.760505437553" Y="0.317192915128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.56356141582" Y="-0.32698195405" />
                  <Point X="-28.323960767429" Y="-1.110680362232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.156840919288" Y="-1.657304755124" />
                  <Point X="-27.898781100697" Y="-2.501380388486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.661320007759" Y="-3.278080626111" />
                  <Point X="-27.4229196725" Y="-4.057852986941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.939268214721" Y="1.226828456803" />
                  <Point X="-28.652300547245" Y="0.288199510217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.472907516127" Y="-0.298568655353" />
                  <Point X="-28.228050082978" Y="-1.099461231725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.076376825919" Y="-1.595562101731" />
                  <Point X="-27.814344574918" Y="-2.452630976042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.596371869065" Y="-3.16558757174" />
                  <Point X="-27.346325463067" Y="-3.983452513541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.187363049038" Y="2.363238939143" />
                  <Point X="-29.147482042782" Y="2.232794045404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.835761326419" Y="1.21320152405" />
                  <Point X="-28.544095656937" Y="0.259206105305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.389944893381" Y="-0.244998323315" />
                  <Point X="-28.123135789394" Y="-1.117691579726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.999631049815" Y="-1.521657380572" />
                  <Point X="-27.729908049139" Y="-2.403881563598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.531423730371" Y="-3.053094517369" />
                  <Point X="-27.263835985492" Y="-3.928334593382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.122169827326" Y="2.474930363082" />
                  <Point X="-29.017693471182" Y="2.133203600021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.733897519135" Y="1.204948867153" />
                  <Point X="-28.430596692817" Y="0.212896565202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.320106935402" Y="-0.148499147156" />
                  <Point X="-27.995674124787" Y="-1.209671055277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.9476324656" Y="-1.366808242026" />
                  <Point X="-27.643569643438" Y="-2.361352920078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.466475666651" Y="-2.94060121777" />
                  <Point X="-27.181346507916" Y="-3.873216673223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.056976605613" Y="2.586621787022" />
                  <Point X="-28.887904899582" Y="2.033613154639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.640782808479" Y="1.225313215867" />
                  <Point X="-27.548184534611" Y="-2.348414709166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.401527661062" Y="-2.82810772803" />
                  <Point X="-27.098857244584" Y="-3.818098052307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.99079758131" Y="2.695088795975" />
                  <Point X="-28.758116327983" Y="1.934022709256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.553178097132" Y="1.26369996025" />
                  <Point X="-27.436990854654" Y="-2.387184004527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.336927902035" Y="-2.714475175113" />
                  <Point X="-27.010375344292" Y="-3.782580463679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.919482365945" Y="2.786756080942" />
                  <Point X="-28.628327756383" Y="1.834432263873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.478181698629" Y="1.343326637713" />
                  <Point X="-26.912402378406" Y="-3.778106751805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.848166617986" Y="2.878421623876" />
                  <Point X="-28.491775448363" Y="1.712718633512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.423810522927" Y="1.490415379182" />
                  <Point X="-26.811030325954" Y="-3.784750951125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.776850870027" Y="2.97008716681" />
                  <Point X="-26.709658273502" Y="-3.791395150444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.691148873554" Y="3.01469741112" />
                  <Point X="-26.607140986557" Y="-3.801785243004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.570514398122" Y="2.945048665159" />
                  <Point X="-26.490438774558" Y="-3.85857213482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.44987992269" Y="2.875399919198" />
                  <Point X="-26.354705232075" Y="-3.977607703782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.13613526842" Y="-4.692517841724" />
                  <Point X="-26.122820336529" Y="-4.736069021564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.329245581586" Y="2.805751612601" />
                  <Point X="-26.208718842342" Y="-4.130178825019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.17356930497" Y="-4.24514778137" />
                  <Point X="-26.015890174099" Y="-4.760892979462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.211051632943" Y="2.74408547008" />
                  <Point X="-25.952411209664" Y="-4.643594472617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.106458245444" Y="2.726904758587" />
                  <Point X="-25.906012078656" Y="-4.470430347885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.007247379524" Y="2.727329481894" />
                  <Point X="-25.859612947648" Y="-4.297266223154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.918630177697" Y="2.762404519138" />
                  <Point X="-25.81321381664" Y="-4.124102098423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.840423694377" Y="2.831531482273" />
                  <Point X="-25.766814685632" Y="-3.950937973692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.016603584653" Y="3.732718781592" />
                  <Point X="-27.947128649095" Y="3.505476506701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.769718192798" Y="2.925193051177" />
                  <Point X="-25.720415554624" Y="-3.777773848961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.936126640156" Y="3.79441940084" />
                  <Point X="-25.674016423616" Y="-3.60460972423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.855649695659" Y="3.856120020087" />
                  <Point X="-25.624329397878" Y="-3.442199818585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.775172751161" Y="3.917820639335" />
                  <Point X="-25.556554579834" Y="-3.338952415767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.694695806664" Y="3.979521258582" />
                  <Point X="-25.487591837702" Y="-3.239590537563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.612334768698" Y="4.035059285775" />
                  <Point X="-25.416580318584" Y="-3.146929906929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.527417857901" Y="4.082237429625" />
                  <Point X="-25.334196176717" Y="-3.091467449192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.442500907551" Y="4.129415444102" />
                  <Point X="-25.243913827363" Y="-3.061838864097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.357583954642" Y="4.176593450209" />
                  <Point X="-25.153182323266" Y="-3.033679397966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.272667001732" Y="4.223771456316" />
                  <Point X="-25.062392534689" Y="-3.005710571781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.183238595206" Y="4.256193162547" />
                  <Point X="-24.965161043717" Y="-2.998811604742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.037473197597" Y="4.104344873977" />
                  <Point X="-24.857325676323" Y="-3.026596354663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.920235637534" Y="4.045806937543" />
                  <Point X="-24.747570601665" Y="-3.060660184115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.816032115595" Y="4.02990141884" />
                  <Point X="-24.63613809772" Y="-3.100210637546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.723996954806" Y="4.053796816064" />
                  <Point X="-24.489534310068" Y="-3.254801176381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.635928835291" Y="4.09066782063" />
                  <Point X="-24.205569656755" Y="-3.858678862343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.554842662062" Y="4.150375742482" />
                  <Point X="-24.275266939086" Y="-3.305780480046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.493308714322" Y="4.274036112277" />
                  <Point X="-24.238184483617" Y="-3.102142882732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.477040180522" Y="4.545752979781" />
                  <Point X="-24.18876930374" Y="-2.938843809342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.390477552501" Y="4.587548225141" />
                  <Point X="-24.115172093404" Y="-2.854640593597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.298979649927" Y="4.613200914806" />
                  <Point X="-24.03596583191" Y="-2.788783757523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.207481747354" Y="4.638853604471" />
                  <Point X="-23.956759711239" Y="-2.722926460834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.11598384478" Y="4.664506294136" />
                  <Point X="-23.870953058994" Y="-2.67865853013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.024485942206" Y="4.690158983801" />
                  <Point X="-23.776244570773" Y="-2.663507192935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.932988039632" Y="4.715811673466" />
                  <Point X="-23.679622201002" Y="-2.654615880221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.837166594367" Y="4.727322692198" />
                  <Point X="-23.582781705755" Y="-2.646438023792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.741257655578" Y="4.738547532506" />
                  <Point X="-23.477442770151" Y="-2.666057313256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.645348665012" Y="4.749772203462" />
                  <Point X="-23.356316330083" Y="-2.737315203036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.549439674446" Y="4.760996874418" />
                  <Point X="-23.232672827791" Y="-2.816806032382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.45353068388" Y="4.772221545374" />
                  <Point X="-23.081341880213" Y="-2.986858414639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.167234759911" Y="4.160718616681" />
                  <Point X="-22.91620445804" Y="-3.202069740479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.046856751542" Y="4.091908736683" />
                  <Point X="-22.984542834038" Y="-2.653616140522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.90248112265" Y="-2.922027904093" />
                  <Point X="-22.751067154411" Y="-3.417280678579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.947040427502" Y="4.090353095716" />
                  <Point X="-22.96191125127" Y="-2.402711868393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.691323501248" Y="-3.287764519082" />
                  <Point X="-22.585929850782" Y="-3.63249161668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.858773520883" Y="4.12657389696" />
                  <Point X="-22.899073463605" Y="-2.283316166834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.480165879847" Y="-3.653501134072" />
                  <Point X="-22.420792547153" Y="-3.84770255478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.784953502426" Y="4.210048340177" />
                  <Point X="-22.829937142672" Y="-2.184522039303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.269009247013" Y="-4.019234515601" />
                  <Point X="-22.26601251307" Y="-4.029036390667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.733711761855" Y="4.367373002737" />
                  <Point X="-22.747549113772" Y="-2.129072295481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.6873124674" Y="4.540536592858" />
                  <Point X="-22.661977190673" Y="-2.084036600332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.640913172945" Y="4.713700182979" />
                  <Point X="-22.575531427734" Y="-2.041859106516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.561440106364" Y="4.778684338936" />
                  <Point X="-22.48117915837" Y="-2.025542629939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.458813461813" Y="4.767936553765" />
                  <Point X="-22.377575890922" Y="-2.03948480467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.356186817262" Y="4.757188768593" />
                  <Point X="-22.272429577904" Y="-2.058474054047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.253560172711" Y="4.746440983422" />
                  <Point X="-22.163849818056" Y="-2.088693601973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.149977406128" Y="4.732565863998" />
                  <Point X="-22.04363399988" Y="-2.156972981756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.04271957598" Y="4.706670153289" />
                  <Point X="-22.135914723146" Y="-1.53020749254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.117994351295" Y="-1.588822387733" />
                  <Point X="-21.922999702668" Y="-2.226621144785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.935461825953" Y="4.680774704646" />
                  <Point X="-22.117914715521" Y="-1.26415402073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.941710973196" Y="-1.840490492701" />
                  <Point X="-21.802365375625" Y="-2.296269405388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.828204075927" Y="4.654879256003" />
                  <Point X="-22.054623022889" Y="-1.146242975417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.811922421778" Y="-1.940080872069" />
                  <Point X="-21.681731016088" Y="-2.365917772277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.7209463259" Y="4.62898380736" />
                  <Point X="-21.981989542063" Y="-1.058887542485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.682133870361" Y="-2.039671251438" />
                  <Point X="-21.56109665655" Y="-2.435566139166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.613688575874" Y="4.603088358717" />
                  <Point X="-22.970093919298" Y="2.497985091014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.840964055521" Y="2.075620337954" />
                  <Point X="-21.904599235482" Y="-0.987090985524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.552345318944" Y="-2.139261630806" />
                  <Point X="-21.440462297012" Y="-2.505214506055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.504763843835" Y="4.571740457594" />
                  <Point X="-22.912998720514" Y="2.636163954455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.718371313996" Y="1.999566392216" />
                  <Point X="-21.816638169865" Y="-0.949869823439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.422556767526" Y="-2.238852010174" />
                  <Point X="-21.319827937475" Y="-2.574862872944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.393033411671" Y="4.531215524873" />
                  <Point X="-22.848050657757" Y="2.748657257204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.612538986286" Y="1.978333289888" />
                  <Point X="-21.723487464832" Y="-0.929623207027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.292768216109" Y="-2.338442389543" />
                  <Point X="-21.199193577937" Y="-2.644511239832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.281302856093" Y="4.49069018848" />
                  <Point X="-22.783102595" Y="2.861150559953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.514106343654" Y="1.981303466877" />
                  <Point X="-21.629973752685" Y="-0.910563933382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.162979664692" Y="-2.438032768911" />
                  <Point X="-21.078559218399" Y="-2.714159606721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.169572300515" Y="4.450164852087" />
                  <Point X="-22.718154538737" Y="2.973643883945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.421688967406" Y="2.003948693667" />
                  <Point X="-21.529683914118" Y="-0.913668370582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.033191113275" Y="-2.537623148279" />
                  <Point X="-20.975301476048" Y="-2.726971619786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.054700369167" Y="4.39936453853" />
                  <Point X="-22.653206490265" Y="3.086137233418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.334808930802" Y="2.04470574233" />
                  <Point X="-21.42617700303" Y="-0.927295377861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.938786262611" Y="4.345155423467" />
                  <Point X="-22.588258441792" Y="3.19863058289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.250372422198" Y="2.093455210951" />
                  <Point X="-21.635204192009" Y="0.081330594429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.494357752559" Y="-0.379357350851" />
                  <Point X="-21.322670091942" Y="-0.94092238514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.822872156056" Y="4.290946308405" />
                  <Point X="-22.52331039332" Y="3.311123932363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.165935913594" Y="2.142204679572" />
                  <Point X="-21.975964048748" Y="1.520834708001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.830785499179" Y="1.045977068997" />
                  <Point X="-21.578076096087" Y="0.219401856178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.371345359604" Y="-0.456783914571" />
                  <Point X="-21.219163184701" Y="-0.954549379838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.705136839881" Y="4.23078028509" />
                  <Point X="-22.458362344848" Y="3.423617281836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.08149940499" Y="2.190954148193" />
                  <Point X="-21.914172104363" Y="1.643651208794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.711452078387" Y="0.980583881012" />
                  <Point X="-21.505199971905" Y="0.305963638456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.259757863837" Y="-0.496841323403" />
                  <Point X="-21.115656297269" Y="-0.968176309741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.584266915475" Y="4.160361420235" />
                  <Point X="-22.393414296375" Y="3.536110631309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.997062896386" Y="2.239703616814" />
                  <Point X="-21.841541075178" Y="1.731014660685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.605366508872" Y="0.958522462062" />
                  <Point X="-21.423607444026" Y="0.364015348878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.151552964972" Y="-0.525834756304" />
                  <Point X="-21.012149409837" Y="-0.981803239644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.463397014504" Y="4.089942632032" />
                  <Point X="-22.328466247903" Y="3.648603980782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.912626387782" Y="2.288453085435" />
                  <Point X="-21.761493712479" Y="1.794120378684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.504156765692" Y="0.952409152451" />
                  <Point X="-21.338419571716" Y="0.410307217553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.043348066108" Y="-0.554828189205" />
                  <Point X="-20.908642522406" Y="-0.995430169546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.341119154264" Y="4.014918616567" />
                  <Point X="-22.263518199431" Y="3.761097330255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.828189879178" Y="2.337202554056" />
                  <Point X="-21.681029439658" Y="1.855862445116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.408589602942" Y="0.964751891814" />
                  <Point X="-21.247419263778" Y="0.437587465935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.935143167243" Y="-0.583821622105" />
                  <Point X="-20.805135634974" Y="-1.009057099449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.214179089855" Y="3.924645218389" />
                  <Point X="-22.198570150958" Y="3.873590679728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.743753370574" Y="2.385952022677" />
                  <Point X="-21.600565166837" Y="1.917604511549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.313092662912" Y="0.977324319343" />
                  <Point X="-21.155600376487" Y="0.462190261898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.826938268378" Y="-0.612815055006" />
                  <Point X="-20.701628747542" Y="-1.022684029352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.65931686197" Y="2.434701491298" />
                  <Point X="-21.520101000139" Y="1.979346925095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.217595722882" Y="0.989896746872" />
                  <Point X="-21.063781489196" Y="0.486793057861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.718733369513" Y="-0.641808487906" />
                  <Point X="-20.598121860111" Y="-1.036310959255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.574880353366" Y="2.483450959919" />
                  <Point X="-21.43963686791" Y="2.041089451383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.122098782851" Y="1.0024691744" />
                  <Point X="-20.971962605768" Y="0.51139586646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.610528470648" Y="-0.670801920807" />
                  <Point X="-20.494614972679" Y="-1.049937889158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.490443844762" Y="2.532200428541" />
                  <Point X="-21.359172735681" Y="2.102831977671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.026601842821" Y="1.015041601929" />
                  <Point X="-20.88014372506" Y="0.535998683952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.502323571784" Y="-0.699795353708" />
                  <Point X="-20.391108085247" Y="-1.063564819061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.406007336158" Y="2.580949897162" />
                  <Point X="-21.278708603451" Y="2.164574503959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.931104902791" Y="1.027614029458" />
                  <Point X="-20.788324844352" Y="0.560601501445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.394118672919" Y="-0.728788786608" />
                  <Point X="-20.287601197816" Y="-1.077191748964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.321570827554" Y="2.629699365783" />
                  <Point X="-21.198244471222" Y="2.226317030247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.83560796276" Y="1.040186456987" />
                  <Point X="-20.696505963643" Y="0.585204318938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.28591377925" Y="-0.757782202515" />
                  <Point X="-20.236586575917" Y="-0.919124214698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.23713431895" Y="2.678448834404" />
                  <Point X="-21.117780338993" Y="2.288059556535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.74011102273" Y="1.052758884516" />
                  <Point X="-20.604687082935" Y="0.609807136431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.152697795898" Y="2.727198255768" />
                  <Point X="-21.037316206764" Y="2.349802082823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.6446140827" Y="1.065331312044" />
                  <Point X="-20.512868202226" Y="0.634409953924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.068261258781" Y="2.775947631126" />
                  <Point X="-20.956852074534" Y="2.411544609111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.549117142669" Y="1.077903739573" />
                  <Point X="-20.421049321518" Y="0.659012771417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.896234976415" Y="2.538203858887" />
                  <Point X="-20.876387942305" Y="2.473287135399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.453620202639" Y="1.090476167102" />
                  <Point X="-20.32923044081" Y="0.683615588909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.358123272646" Y="1.103048627459" />
                  <Point X="-20.237411560101" Y="0.708218406402" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.44151953125" Y="-3.861577880859" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.57121484375" Y="-3.470345458984" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.78065234375" Y="-3.249333496094" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.063650390625" Y="-3.204832763672" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643798828" />
                  <Point X="-25.3238125" Y="-3.336842529297" />
                  <Point X="-25.452005859375" Y="-3.521544189453" />
                  <Point X="-25.4592265625" Y="-3.537112060547" />
                  <Point X="-25.7728671875" Y="-4.707636230469" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.867037109375" Y="-4.98333203125" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.162962890625" Y="-4.921928710938" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.319615234375" Y="-4.630532714844" />
                  <Point X="-26.3091484375" Y="-4.5510390625" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.322818359375" Y="-4.468715332031" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.436908203125" Y="-4.158230957031" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.716431640625" Y="-3.981358886719" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791015625" />
                  <Point X="-27.045865234375" Y="-4.011200927734" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.435818359375" Y="-4.386772949219" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.513994140625" Y="-4.379268554688" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-27.9426796875" Y="-4.100741699219" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.6676015625" Y="-2.90896484375" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597594482422" />
                  <Point X="-27.5139765625" Y="-2.568765869141" />
                  <Point X="-27.53132421875" Y="-2.551417236328" />
                  <Point X="-27.560154296875" Y="-2.537198730469" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.601236328125" Y="-3.1263359375" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-28.8916328125" Y="-3.201946289062" />
                  <Point X="-29.16169921875" Y="-2.847135742188" />
                  <Point X="-29.22396484375" Y="-2.742725830078" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-28.446478515625" Y="-1.640062744141" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.1525390625" Y="-1.411086547852" />
                  <Point X="-28.144146484375" Y="-1.389555053711" />
                  <Point X="-28.138115234375" Y="-1.366263916016" />
                  <Point X="-28.136650390625" Y="-1.350049560547" />
                  <Point X="-28.14865625" Y="-1.321068481445" />
                  <Point X="-28.16690625" Y="-1.307256713867" />
                  <Point X="-28.187640625" Y="-1.295052978516" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.493994140625" Y="-1.456357666016" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.821765625" Y="-1.424713867188" />
                  <Point X="-29.927392578125" Y="-1.011187744141" />
                  <Point X="-29.943865234375" Y="-0.89601550293" />
                  <Point X="-29.99839453125" Y="-0.514742248535" />
                  <Point X="-28.8794921875" Y="-0.214933044434" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.535873046875" Y="-0.117245048523" />
                  <Point X="-28.514142578125" Y="-0.102163482666" />
                  <Point X="-28.502322265625" Y="-0.09064490509" />
                  <Point X="-28.492888671875" Y="-0.069436729431" />
                  <Point X="-28.485646484375" Y="-0.046098899841" />
                  <Point X="-28.483400390625" Y="-0.031280040741" />
                  <Point X="-28.487654296875" Y="-0.009991976738" />
                  <Point X="-28.494896484375" Y="0.013345852852" />
                  <Point X="-28.502322265625" Y="0.028084823608" />
                  <Point X="-28.520166015625" Y="0.043784011841" />
                  <Point X="-28.541896484375" Y="0.058865581512" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.71919921875" Y="0.377371643066" />
                  <Point X="-29.998185546875" Y="0.452125976563" />
                  <Point X="-29.98571875" Y="0.536375244141" />
                  <Point X="-29.91764453125" Y="0.996414672852" />
                  <Point X="-29.884484375" Y="1.118789916992" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-28.984103515625" Y="1.424370727539" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.718373046875" Y="1.400069946289" />
                  <Point X="-28.67027734375" Y="1.415234008789" />
                  <Point X="-28.651533203125" Y="1.426056396484" />
                  <Point X="-28.6391171875" Y="1.443786743164" />
                  <Point X="-28.633767578125" Y="1.456701416016" />
                  <Point X="-28.614470703125" Y="1.503291137695" />
                  <Point X="-28.610712890625" Y="1.524603515625" />
                  <Point X="-28.6163125" Y="1.545507568359" />
                  <Point X="-28.62276953125" Y="1.557912353516" />
                  <Point X="-28.646056640625" Y="1.602642822266" />
                  <Point X="-28.65996875" Y="1.619221923828" />
                  <Point X="-29.326341796875" Y="2.130549072266" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.42452734375" Y="2.333830322266" />
                  <Point X="-29.16001171875" Y="2.787007568359" />
                  <Point X="-29.07217578125" Y="2.899911132812" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.300931640625" Y="3.008797119141" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.1199453125" Y="2.918810302734" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927404296875" />
                  <Point X="-28.000072265625" Y="2.940583496094" />
                  <Point X="-27.95252734375" Y="2.988127685547" />
                  <Point X="-27.9408984375" Y="3.006381835938" />
                  <Point X="-27.938072265625" Y="3.027840576172" />
                  <Point X="-27.939697265625" Y="3.046407958984" />
                  <Point X="-27.945556640625" Y="3.113390136719" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.247357421875" Y="3.645491455078" />
                  <Point X="-28.30727734375" Y="3.749276855469" />
                  <Point X="-28.213576171875" Y="3.821117431641" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.61453125" Y="4.251192382813" />
                  <Point X="-27.141548828125" Y="4.513971679688" />
                  <Point X="-27.011228515625" Y="4.344134765625" />
                  <Point X="-26.967826171875" Y="4.287572753906" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.930580078125" Y="4.262902832031" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.792283203125" Y="4.231166503906" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.69690234375" Y="4.275744628906" />
                  <Point X="-26.68608203125" Y="4.294488769531" />
                  <Point X="-26.679076171875" Y="4.316708007812" />
                  <Point X="-26.653802734375" Y="4.396865234375" />
                  <Point X="-26.651916015625" Y="4.41842578125" />
                  <Point X="-26.68548828125" Y="4.673424804688" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.564498046875" Y="4.736085449219" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.800380859375" Y="4.922924804688" />
                  <Point X="-25.22419921875" Y="4.990358398438" />
                  <Point X="-25.084080078125" Y="4.467429199219" />
                  <Point X="-25.042140625" Y="4.310903808594" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.7942421875" Y="4.875576660156" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.660541015625" Y="4.980102050781" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-24.0010390625" Y="4.892065917969" />
                  <Point X="-23.491544921875" Y="4.76905859375" />
                  <Point X="-23.401416015625" Y="4.736368164062" />
                  <Point X="-23.06896484375" Y="4.615785644531" />
                  <Point X="-22.9816328125" Y="4.574943359375" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.576927734375" Y="4.375979003906" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-22.18790625" Y="4.139105957031" />
                  <Point X="-21.9312578125" Y="3.956592529297" />
                  <Point X="-22.583140625" Y="2.827498046875" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.779806640625" Y="2.474090087891" />
                  <Point X="-22.7966171875" Y="2.411228759766" />
                  <Point X="-22.79613671875" Y="2.377253417969" />
                  <Point X="-22.789583984375" Y="2.322902587891" />
                  <Point X="-22.781318359375" Y="2.300812255859" />
                  <Point X="-22.771994140625" Y="2.287072021484" />
                  <Point X="-22.738359375" Y="2.237503662109" />
                  <Point X="-22.711318359375" Y="2.214880126953" />
                  <Point X="-22.66175" Y="2.18124609375" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.624595703125" Y="2.171163330078" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.53391015625" Y="2.170605957031" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.290482421875" Y="2.867041503906" />
                  <Point X="-21.005751953125" Y="3.031430419922" />
                  <Point X="-20.98096484375" Y="2.996981933594" />
                  <Point X="-20.79740234375" Y="2.741874511719" />
                  <Point X="-20.753048828125" Y="2.668576660156" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.465173828125" Y="1.782004638672" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583832763672" />
                  <Point X="-21.733169921875" Y="1.567472290039" />
                  <Point X="-21.77841015625" Y="1.508451416016" />
                  <Point X="-21.79155078125" Y="1.47479675293" />
                  <Point X="-21.808404296875" Y="1.414536376953" />
                  <Point X="-21.809220703125" Y="1.390965454102" />
                  <Point X="-21.805384765625" Y="1.372380126953" />
                  <Point X="-21.79155078125" Y="1.305332763672" />
                  <Point X="-21.773923828125" Y="1.27210168457" />
                  <Point X="-21.736296875" Y="1.214909790039" />
                  <Point X="-21.719052734375" Y="1.198819946289" />
                  <Point X="-21.7039375" Y="1.190311645508" />
                  <Point X="-21.64941015625" Y="1.159617553711" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.61099609375" Y="1.150918701172" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.41421484375" Y="1.287303588867" />
                  <Point X="-20.151025390625" Y="1.321953125" />
                  <Point X="-20.1396484375" Y="1.275221191406" />
                  <Point X="-20.06080859375" Y="0.951367248535" />
                  <Point X="-20.04683203125" Y="0.861601806641" />
                  <Point X="-20.002140625" Y="0.574556396484" />
                  <Point X="-20.976671875" Y="0.313431396484" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819351196" />
                  <Point X="-21.290990234375" Y="0.221213943481" />
                  <Point X="-21.363421875" Y="0.179346832275" />
                  <Point X="-21.38978125" Y="0.151576187134" />
                  <Point X="-21.433240234375" Y="0.096198867798" />
                  <Point X="-21.443013671875" Y="0.074735107422" />
                  <Point X="-21.447029296875" Y="0.053767250061" />
                  <Point X="-21.461515625" Y="-0.021875450134" />
                  <Point X="-21.461515625" Y="-0.040684627533" />
                  <Point X="-21.4575" Y="-0.061652484894" />
                  <Point X="-21.443013671875" Y="-0.137295181274" />
                  <Point X="-21.433240234375" Y="-0.15875894165" />
                  <Point X="-21.421193359375" Y="-0.174109298706" />
                  <Point X="-21.377734375" Y="-0.229486618042" />
                  <Point X="-21.363421875" Y="-0.241906906128" />
                  <Point X="-21.34334375" Y="-0.253512313843" />
                  <Point X="-21.270912109375" Y="-0.295379425049" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.2409296875" Y="-0.573133239746" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.007548828125" Y="-0.674432312012" />
                  <Point X="-20.051568359375" Y="-0.966412719727" />
                  <Point X="-20.069474609375" Y="-1.044880004883" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-21.26079296875" Y="-1.140708251953" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.644568359375" Y="-1.10690625" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.814552734375" Y="-1.154697265625" />
                  <Point X="-21.83837109375" Y="-1.183343383789" />
                  <Point X="-21.924296875" Y="-1.286685424805" />
                  <Point X="-21.935640625" Y="-1.314070800781" />
                  <Point X="-21.9390546875" Y="-1.351168945312" />
                  <Point X="-21.951369140625" Y="-1.485001220703" />
                  <Point X="-21.943638671875" Y="-1.516622192383" />
                  <Point X="-21.92183203125" Y="-1.55054284668" />
                  <Point X="-21.843158203125" Y="-1.672912719727" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-20.88691796875" Y="-2.410373291016" />
                  <Point X="-20.660923828125" Y="-2.583784179688" />
                  <Point X="-20.67178125" Y="-2.601352539062" />
                  <Point X="-20.795869140625" Y="-2.802142578125" />
                  <Point X="-20.83290625" Y="-2.854767578125" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.95510546875" Y="-2.427478027344" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.309556640625" Y="-2.244842773438" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.5498828125" Y="-2.239750244141" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.33468359375" />
                  <Point X="-22.731904296875" Y="-2.373645263672" />
                  <Point X="-22.80587890625" Y="-2.514201660156" />
                  <Point X="-22.8108359375" Y="-2.546374755859" />
                  <Point X="-22.802365234375" Y="-2.593273925781" />
                  <Point X="-22.77180859375" Y="-2.762465087891" />
                  <Point X="-22.76590625" Y="-2.778578857422" />
                  <Point X="-22.15889453125" Y="-3.829956542969" />
                  <Point X="-22.01332421875" Y="-4.082087646484" />
                  <Point X="-22.01746484375" Y="-4.085044433594" />
                  <Point X="-22.16470703125" Y="-4.190215820312" />
                  <Point X="-22.206111328125" Y="-4.217016113281" />
                  <Point X="-22.32022265625" Y="-4.29087890625" />
                  <Point X="-23.09442578125" Y="-3.281917724609" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.375705078125" Y="-2.950729248047" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.624783203125" Y="-2.840372314453" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.873728515625" Y="-2.900988525391" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045986083984" />
                  <Point X="-24.04322265625" Y="-3.099721435547" />
                  <Point X="-24.085357421875" Y="-3.293573974609" />
                  <Point X="-24.0860703125" Y="-3.310719970703" />
                  <Point X="-23.914046875" Y="-4.617363769531" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.04391015625" Y="-4.970196777344" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#138" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.041940917734" Y="4.511650117458" Z="0.55" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.55" />
                  <Point X="-0.812349928673" Y="5.003865791466" Z="0.55" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.55" />
                  <Point X="-1.584152528955" Y="4.815509069241" Z="0.55" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.55" />
                  <Point X="-1.741217125809" Y="4.69817964729" Z="0.55" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.55" />
                  <Point X="-1.732919404343" Y="4.363023419925" Z="0.55" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.55" />
                  <Point X="-1.817574798158" Y="4.30864029066" Z="0.55" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.55" />
                  <Point X="-1.913649924864" Y="4.338533584323" Z="0.55" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.55" />
                  <Point X="-1.977716765489" Y="4.405853366124" Z="0.55" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.55" />
                  <Point X="-2.644971511584" Y="4.326179708828" Z="0.55" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.55" />
                  <Point X="-3.250154269711" Y="3.892077472508" Z="0.55" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.55" />
                  <Point X="-3.296815524515" Y="3.651771614417" Z="0.55" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.55" />
                  <Point X="-2.995664451945" Y="3.073330797019" Z="0.55" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.55" />
                  <Point X="-3.04158439484" Y="3.007219173046" Z="0.55" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.55" />
                  <Point X="-3.121745668587" Y="2.999900247228" Z="0.55" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.55" />
                  <Point X="-3.28208773388" Y="3.083378424231" Z="0.55" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.55" />
                  <Point X="-4.117794784715" Y="2.961893681479" Z="0.55" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.55" />
                  <Point X="-4.473866727525" Y="2.390315334124" Z="0.55" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.55" />
                  <Point X="-4.362937058356" Y="2.122161408039" Z="0.55" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.55" />
                  <Point X="-3.673277036251" Y="1.566103559192" Z="0.55" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.55" />
                  <Point X="-3.686120532424" Y="1.507114645758" Z="0.55" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.55" />
                  <Point X="-3.739564416453" Y="1.479035985021" Z="0.55" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.55" />
                  <Point X="-3.983734922552" Y="1.505223055388" Z="0.55" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.55" />
                  <Point X="-4.938900564155" Y="1.163147408859" Z="0.55" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.55" />
                  <Point X="-5.041337259618" Y="0.574974237073" Z="0.55" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.55" />
                  <Point X="-4.738297182979" Y="0.360355379726" Z="0.55" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.55" />
                  <Point X="-3.554831217034" Y="0.033987505218" Z="0.55" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.55" />
                  <Point X="-3.541564562082" Y="0.006469164629" Z="0.55" />
                  <Point X="-3.539556741714" Y="0" Z="0.55" />
                  <Point X="-3.546800039053" Y="-0.023337786432" Z="0.55" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.55" />
                  <Point X="-3.570537378089" Y="-0.044888477916" Z="0.55" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.55" />
                  <Point X="-3.898590605794" Y="-0.13535667703" Z="0.55" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.55" />
                  <Point X="-4.999517852653" Y="-0.871814540093" Z="0.55" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.55" />
                  <Point X="-4.876367707698" Y="-1.405807924187" Z="0.55" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.55" />
                  <Point X="-4.49362510018" Y="-1.47464993571" Z="0.55" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.55" />
                  <Point X="-3.19842396453" Y="-1.319066981523" Z="0.55" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.55" />
                  <Point X="-3.19870865342" Y="-1.345740937502" Z="0.55" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.55" />
                  <Point X="-3.483073754893" Y="-1.569115093049" Z="0.55" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.55" />
                  <Point X="-4.273064701536" Y="-2.737055440736" Z="0.55" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.55" />
                  <Point X="-3.937575355076" Y="-3.200949429657" Z="0.55" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.55" />
                  <Point X="-3.582393525756" Y="-3.138357251903" Z="0.55" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.55" />
                  <Point X="-2.559256215936" Y="-2.569074253196" Z="0.55" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.55" />
                  <Point X="-2.717059761265" Y="-2.852684856736" Z="0.55" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.55" />
                  <Point X="-2.9793407862" Y="-4.109078853654" Z="0.55" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.55" />
                  <Point X="-2.546473956598" Y="-4.39049937596" Z="0.55" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.55" />
                  <Point X="-2.402307465941" Y="-4.385930785159" Z="0.55" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.55" />
                  <Point X="-2.024243820837" Y="-4.021494290865" Z="0.55" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.55" />
                  <Point X="-1.72585866486" Y="-3.999971945536" Z="0.55" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.55" />
                  <Point X="-1.47603185385" Y="-4.164542744976" Z="0.55" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.55" />
                  <Point X="-1.378015135464" Y="-4.447190143801" Z="0.55" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.55" />
                  <Point X="-1.375344095838" Y="-4.592726021557" Z="0.55" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.55" />
                  <Point X="-1.181578611776" Y="-4.939071446449" Z="0.55" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.55" />
                  <Point X="-0.882720927272" Y="-5.001135964097" Z="0.55" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.55" />
                  <Point X="-0.730727743045" Y="-4.689297171734" Z="0.55" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.55" />
                  <Point X="-0.288893792323" Y="-3.334071423541" Z="0.55" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.55" />
                  <Point X="-0.054987898336" Y="-3.221305582839" Z="0.55" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.55" />
                  <Point X="0.198371181025" Y="-3.265806462449" Z="0.55" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.55" />
                  <Point X="0.381552067109" Y="-3.467574332227" Z="0.55" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.55" />
                  <Point X="0.504027117356" Y="-3.843238858082" Z="0.55" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.55" />
                  <Point X="0.958869536907" Y="-4.988111799568" Z="0.55" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.55" />
                  <Point X="1.138194338975" Y="-4.950273085397" Z="0.55" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.55" />
                  <Point X="1.129368728641" Y="-4.579557529899" Z="0.55" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.55" />
                  <Point X="0.999480420662" Y="-3.079061313899" Z="0.55" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.55" />
                  <Point X="1.152080902669" Y="-2.9081549174" Z="0.55" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.55" />
                  <Point X="1.373642381586" Y="-2.858881749858" Z="0.55" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.55" />
                  <Point X="1.591098558143" Y="-2.961507360653" Z="0.55" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.55" />
                  <Point X="1.859748540357" Y="-3.281075552522" Z="0.55" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.55" />
                  <Point X="2.814902421543" Y="-4.227710037699" Z="0.55" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.55" />
                  <Point X="3.005441029374" Y="-4.094451444213" Z="0.55" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.55" />
                  <Point X="2.878250302367" Y="-3.773676241935" Z="0.55" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.55" />
                  <Point X="2.240681090934" Y="-2.553107464475" Z="0.55" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.55" />
                  <Point X="2.306184887768" Y="-2.365651961061" Z="0.55" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.55" />
                  <Point X="2.467246417311" Y="-2.252716422199" Z="0.55" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.55" />
                  <Point X="2.675399339038" Y="-2.262766918557" Z="0.55" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.55" />
                  <Point X="3.013737355172" Y="-2.439499197907" Z="0.55" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.55" />
                  <Point X="4.201826285361" Y="-2.852264662557" Z="0.55" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.55" />
                  <Point X="4.364594368324" Y="-2.596357815159" Z="0.55" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.55" />
                  <Point X="4.137362731491" Y="-2.33942547406" Z="0.55" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.55" />
                  <Point X="3.114070296159" Y="-1.492223081487" Z="0.55" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.55" />
                  <Point X="3.104577717351" Y="-1.324470156428" Z="0.55" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.55" />
                  <Point X="3.193917176404" Y="-1.184030266637" Z="0.55" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.55" />
                  <Point X="3.359893966189" Y="-1.1244854585" Z="0.55" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.55" />
                  <Point X="3.726525680672" Y="-1.159000517776" Z="0.55" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.55" />
                  <Point X="4.973113729206" Y="-1.024723940557" Z="0.55" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.55" />
                  <Point X="5.03573596262" Y="-0.650606336148" Z="0.55" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.55" />
                  <Point X="4.765855503511" Y="-0.493556858619" Z="0.55" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.55" />
                  <Point X="3.675520659643" Y="-0.178943619435" Z="0.55" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.55" />
                  <Point X="3.61198349068" Y="-0.111960943265" Z="0.55" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.55" />
                  <Point X="3.585450315706" Y="-0.020967908825" Z="0.55" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.55" />
                  <Point X="3.595921134719" Y="0.075642622408" Z="0.55" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.55" />
                  <Point X="3.64339594772" Y="0.15198779531" Z="0.55" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.55" />
                  <Point X="3.727874754709" Y="0.209205224807" Z="0.55" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.55" />
                  <Point X="4.030112313795" Y="0.29641507415" Z="0.55" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.55" />
                  <Point X="4.996416004442" Y="0.900573976922" Z="0.55" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.55" />
                  <Point X="4.902777105896" Y="1.318328106056" Z="0.55" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.55" />
                  <Point X="4.57310212936" Y="1.368155861545" Z="0.55" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.55" />
                  <Point X="3.389396848246" Y="1.23176768264" Z="0.55" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.55" />
                  <Point X="3.314433348405" Y="1.265162709787" Z="0.55" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.55" />
                  <Point X="3.261691188533" Y="1.330862969569" Z="0.55" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.55" />
                  <Point X="3.237426824367" Y="1.413763852676" Z="0.55" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.55" />
                  <Point X="3.250444525452" Y="1.492609679273" Z="0.55" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.55" />
                  <Point X="3.300357379864" Y="1.568334666135" Z="0.55" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.55" />
                  <Point X="3.559106135539" Y="1.773617119316" Z="0.55" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.55" />
                  <Point X="4.283572140891" Y="2.725742972922" Z="0.55" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.55" />
                  <Point X="4.053464772552" Y="3.057465181847" Z="0.55" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.55" />
                  <Point X="3.678361113791" Y="2.941622815763" Z="0.55" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.55" />
                  <Point X="2.44701662016" Y="2.2501888373" Z="0.55" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.55" />
                  <Point X="2.375234444539" Y="2.252083647671" Z="0.55" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.55" />
                  <Point X="2.310598307042" Y="2.287534762905" Z="0.55" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.55" />
                  <Point X="2.26322383733" Y="2.34642655334" Z="0.55" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.55" />
                  <Point X="2.247346055088" Y="2.414523999293" Z="0.55" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.55" />
                  <Point X="2.262339105358" Y="2.492452955722" Z="0.55" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.55" />
                  <Point X="2.454002537097" Y="2.833777968164" Z="0.55" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.55" />
                  <Point X="2.834914070046" Y="4.211133275905" Z="0.55" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.55" />
                  <Point X="2.442085335172" Y="4.450461647292" Z="0.55" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.55" />
                  <Point X="2.033391560302" Y="4.651515612293" Z="0.55" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.55" />
                  <Point X="1.609475445857" Y="4.814652303894" Z="0.55" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.55" />
                  <Point X="1.004538669664" Y="4.971949619632" Z="0.55" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.55" />
                  <Point X="0.338509439574" Y="5.061109919958" Z="0.55" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.55" />
                  <Point X="0.151303643819" Y="4.919797388493" Z="0.55" />
                  <Point X="0" Y="4.355124473572" Z="0.55" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>