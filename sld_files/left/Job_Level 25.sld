<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#165" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1878" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.247244140625" Y="-4.21956640625" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140625" />
                  <Point X="-24.457634765625" Y="-3.467376953125" />
                  <Point X="-24.535068359375" Y="-3.355809814453" />
                  <Point X="-24.62136328125" Y="-3.2314765625" />
                  <Point X="-24.643248046875" Y="-3.209020263672" />
                  <Point X="-24.66950390625" Y="-3.189776611328" />
                  <Point X="-24.69750390625" Y="-3.175669189453" />
                  <Point X="-24.817328125" Y="-3.138480224609" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097036132812" />
                  <Point X="-25.156646484375" Y="-3.134224853516" />
                  <Point X="-25.29018359375" Y="-3.175669433594" />
                  <Point X="-25.31818359375" Y="-3.189776855469" />
                  <Point X="-25.344439453125" Y="-3.209020751953" />
                  <Point X="-25.36632421875" Y="-3.231477539062" />
                  <Point X="-25.443755859375" Y="-3.343044433594" />
                  <Point X="-25.53005078125" Y="-3.467377929688" />
                  <Point X="-25.5381875" Y="-3.481572021484" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.76212109375" Y="-4.300472167969" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-25.925638671875" Y="-4.875184082031" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.2149609375" Y="-4.810456054688" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.237767578125" Y="-4.736663085938" />
                  <Point X="-26.2149609375" Y="-4.563438476562" />
                  <Point X="-26.21419921875" Y="-4.5479296875" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.245134765625" Y="-4.372311035156" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.182964355469" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.433962890625" Y="-4.034457275391" />
                  <Point X="-26.556904296875" Y="-3.926639648438" />
                  <Point X="-26.5831875" Y="-3.910295898438" />
                  <Point X="-26.612884765625" Y="-3.897994628906" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.7894453125" Y="-3.881369628906" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708496094" />
                  <Point X="-27.014466796875" Y="-3.882028076172" />
                  <Point X="-27.042658203125" Y="-3.894801513672" />
                  <Point X="-27.16466015625" Y="-3.976321289062" />
                  <Point X="-27.300623046875" Y="-4.067169677734" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.3351015625" Y="-4.099461914062" />
                  <Point X="-27.453634765625" Y="-4.253937011719" />
                  <Point X="-27.4801484375" Y="-4.288489746094" />
                  <Point X="-27.57641796875" Y="-4.228881835938" />
                  <Point X="-27.80171484375" Y="-4.0893828125" />
                  <Point X="-27.98950390625" Y="-3.944791748047" />
                  <Point X="-28.10472265625" Y="-3.856077392578" />
                  <Point X="-27.7758359375" Y="-3.286429199219" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647655517578" />
                  <Point X="-27.406587890625" Y="-2.616130859375" />
                  <Point X="-27.40557421875" Y="-2.585198486328" />
                  <Point X="-27.414556640625" Y="-2.555581787109" />
                  <Point X="-27.428771484375" Y="-2.526753417969" />
                  <Point X="-27.446798828125" Y="-2.501593261719" />
                  <Point X="-27.464146484375" Y="-2.484244628906" />
                  <Point X="-27.489302734375" Y="-2.466216796875" />
                  <Point X="-27.5181328125" Y="-2.451997802734" />
                  <Point X="-27.547751953125" Y="-2.443011962891" />
                  <Point X="-27.5786875" Y="-2.444024169922" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.318771484375" Y="-2.85355859375" />
                  <Point X="-28.8180234375" Y="-3.141801513672" />
                  <Point X="-28.904869140625" Y="-3.027705078125" />
                  <Point X="-29.082859375" Y="-2.793861083984" />
                  <Point X="-29.2174921875" Y="-2.568102539062" />
                  <Point X="-29.306142578125" Y="-2.419449951172" />
                  <Point X="-28.72196875" Y="-1.971198364258" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.084576171875" Y="-1.475592285156" />
                  <Point X="-28.066611328125" Y="-1.448460449219" />
                  <Point X="-28.053857421875" Y="-1.419833496094" />
                  <Point X="-28.04615234375" Y="-1.390086303711" />
                  <Point X="-28.04334765625" Y="-1.359655273438" />
                  <Point X="-28.045556640625" Y="-1.327985839844" />
                  <Point X="-28.052556640625" Y="-1.298243164062" />
                  <Point X="-28.06863671875" Y="-1.272261230469" />
                  <Point X="-28.089466796875" Y="-1.248305053711" />
                  <Point X="-28.11296875" Y="-1.228768066406" />
                  <Point X="-28.139451171875" Y="-1.213181396484" />
                  <Point X="-28.168712890625" Y="-1.201957641602" />
                  <Point X="-28.2006015625" Y="-1.195475097656" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-29.089845703125" Y="-1.307330810547" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.7644609375" Y="-1.265191894531" />
                  <Point X="-29.834076171875" Y="-0.992650817871" />
                  <Point X="-29.869697265625" Y="-0.743601379395" />
                  <Point X="-29.892423828125" Y="-0.584698425293" />
                  <Point X="-29.234607421875" Y="-0.408437225342" />
                  <Point X="-28.532875" Y="-0.220408432007" />
                  <Point X="-28.510474609375" Y="-0.211210540771" />
                  <Point X="-28.481783203125" Y="-0.194881835938" />
                  <Point X="-28.4685625" Y="-0.185800216675" />
                  <Point X="-28.44211328125" Y="-0.164128967285" />
                  <Point X="-28.425919921875" Y="-0.147104141235" />
                  <Point X="-28.414400390625" Y="-0.126625663757" />
                  <Point X="-28.402599609375" Y="-0.097788986206" />
                  <Point X="-28.398005859375" Y="-0.083388938904" />
                  <Point X="-28.390884765625" Y="-0.052860404968" />
                  <Point X="-28.388400390625" Y="-0.031473009109" />
                  <Point X="-28.390796875" Y="-0.010075713158" />
                  <Point X="-28.39741796875" Y="0.018839895248" />
                  <Point X="-28.4019375" Y="0.033220058441" />
                  <Point X="-28.41423828125" Y="0.063668598175" />
                  <Point X="-28.425666015625" Y="0.084197692871" />
                  <Point X="-28.44178125" Y="0.101294540405" />
                  <Point X="-28.4667265625" Y="0.121922851562" />
                  <Point X="-28.479890625" Y="0.131055770874" />
                  <Point X="-28.5100859375" Y="0.148428787231" />
                  <Point X="-28.532875" Y="0.157848297119" />
                  <Point X="-29.31490625" Y="0.367393066406" />
                  <Point X="-29.891814453125" Y="0.521975463867" />
                  <Point X="-29.8693515625" Y="0.673783874512" />
                  <Point X="-29.82448828125" Y="0.976968078613" />
                  <Point X="-29.752783203125" Y="1.241581176758" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-29.268685546875" Y="1.366016845703" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744982421875" Y="1.299341796875" />
                  <Point X="-28.723421875" Y="1.301228271484" />
                  <Point X="-28.70313671875" Y="1.305263427734" />
                  <Point X="-28.6740859375" Y="1.314423217773" />
                  <Point X="-28.6417109375" Y="1.324631225586" />
                  <Point X="-28.62277734375" Y="1.332962402344" />
                  <Point X="-28.604033203125" Y="1.343784423828" />
                  <Point X="-28.5873515625" Y="1.356015991211" />
                  <Point X="-28.573712890625" Y="1.371568359375" />
                  <Point X="-28.561298828125" Y="1.389298217773" />
                  <Point X="-28.551349609375" Y="1.407432983398" />
                  <Point X="-28.539693359375" Y="1.435575073242" />
                  <Point X="-28.526703125" Y="1.466937011719" />
                  <Point X="-28.520916015625" Y="1.486788818359" />
                  <Point X="-28.51715625" Y="1.508104614258" />
                  <Point X="-28.515802734375" Y="1.528750854492" />
                  <Point X="-28.518951171875" Y="1.549200561523" />
                  <Point X="-28.5245546875" Y="1.570107299805" />
                  <Point X="-28.532048828125" Y="1.589377441406" />
                  <Point X="-28.54611328125" Y="1.616396362305" />
                  <Point X="-28.5617890625" Y="1.646507080078" />
                  <Point X="-28.573283203125" Y="1.663709716797" />
                  <Point X="-28.5871953125" Y="1.680288574219" />
                  <Point X="-28.60213671875" Y="1.694590087891" />
                  <Point X="-29.0507109375" Y="2.038794433594" />
                  <Point X="-29.351859375" Y="2.269873535156" />
                  <Point X="-29.255478515625" Y="2.434996582031" />
                  <Point X="-29.081146484375" Y="2.733665771484" />
                  <Point X="-28.891216796875" Y="2.977796875" />
                  <Point X="-28.75050390625" Y="3.158662353516" />
                  <Point X="-28.51559765625" Y="3.023038330078" />
                  <Point X="-28.20665625" Y="2.844671142578" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.10633203125" Y="2.822256591797" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.04056640625" Y="2.818762939453" />
                  <Point X="-28.019107421875" Y="2.821587890625" />
                  <Point X="-27.999015625" Y="2.826504150391" />
                  <Point X="-27.980462890625" Y="2.835652832031" />
                  <Point X="-27.962208984375" Y="2.847281738281" />
                  <Point X="-27.946076171875" Y="2.860228515625" />
                  <Point X="-27.917357421875" Y="2.888947021484" />
                  <Point X="-27.8853515625" Y="2.920951904297" />
                  <Point X="-27.872404296875" Y="2.937084228516" />
                  <Point X="-27.860775390625" Y="2.955338134766" />
                  <Point X="-27.85162890625" Y="2.973887207031" />
                  <Point X="-27.8467109375" Y="2.9939765625" />
                  <Point X="-27.843884765625" Y="3.015434814453" />
                  <Point X="-27.84343359375" Y="3.036122314453" />
                  <Point X="-27.846974609375" Y="3.076582275391" />
                  <Point X="-27.85091796875" Y="3.121671875" />
                  <Point X="-27.854955078125" Y="3.141959472656" />
                  <Point X="-27.86146484375" Y="3.162603271484" />
                  <Point X="-27.869794921875" Y="3.181532958984" />
                  <Point X="-28.068572265625" Y="3.525825439453" />
                  <Point X="-28.18333203125" Y="3.724595947266" />
                  <Point X="-28.0042421875" Y="3.861903320312" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.401482421875" Y="4.260880859375" />
                  <Point X="-27.16703515625" Y="4.391133789062" />
                  <Point X="-27.137771484375" Y="4.352995117187" />
                  <Point X="-27.0431953125" Y="4.229741210938" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.99511328125" Y="4.18939453125" />
                  <Point X="-26.95008203125" Y="4.165952636719" />
                  <Point X="-26.899896484375" Y="4.139827636719" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.777451171875" Y="4.134482421875" />
                  <Point X="-26.730548828125" Y="4.153910644531" />
                  <Point X="-26.67827734375" Y="4.175562011719" />
                  <Point X="-26.660146484375" Y="4.185508789062" />
                  <Point X="-26.642416015625" Y="4.197922851562" />
                  <Point X="-26.626859375" Y="4.211563964844" />
                  <Point X="-26.614626953125" Y="4.228249511719" />
                  <Point X="-26.603806640625" Y="4.246993652344" />
                  <Point X="-26.595478515625" Y="4.265921875" />
                  <Point X="-26.580212890625" Y="4.31433984375" />
                  <Point X="-26.56319921875" Y="4.368298339844" />
                  <Point X="-26.5591640625" Y="4.388583496094" />
                  <Point X="-26.55727734375" Y="4.410145019531" />
                  <Point X="-26.557728515625" Y="4.430826660156" />
                  <Point X="-26.580328125" Y="4.602481445312" />
                  <Point X="-26.584201171875" Y="4.631897460938" />
                  <Point X="-26.34268359375" Y="4.699610351562" />
                  <Point X="-25.949630859375" Y="4.80980859375" />
                  <Point X="-25.58698828125" Y="4.852250488281" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.225296875" Y="4.627399902344" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.751931640625" Y="4.6664296875" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.499154296875" Y="4.867680664062" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.85592578125" Y="4.759301757813" />
                  <Point X="-23.5189765625" Y="4.677952148438" />
                  <Point X="-23.324462890625" Y="4.607400390625" />
                  <Point X="-23.105349609375" Y="4.527926269531" />
                  <Point X="-22.916513671875" Y="4.439613769531" />
                  <Point X="-22.705427734375" Y="4.340895996094" />
                  <Point X="-22.522966796875" Y="4.234594726563" />
                  <Point X="-22.319017578125" Y="4.1157734375" />
                  <Point X="-22.14696484375" Y="3.993418212891" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.44491796875" Y="3.256906982422" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857919921875" Y="2.539935546875" />
                  <Point X="-22.866921875" Y="2.516058349609" />
                  <Point X="-22.877076171875" Y="2.478087646484" />
                  <Point X="-22.888392578125" Y="2.435771972656" />
                  <Point X="-22.891380859375" Y="2.417936035156" />
                  <Point X="-22.892271484375" Y="2.380953613281" />
                  <Point X="-22.8883125" Y="2.348119873047" />
                  <Point X="-22.883900390625" Y="2.311528808594" />
                  <Point X="-22.878556640625" Y="2.289601318359" />
                  <Point X="-22.8702890625" Y="2.267511474609" />
                  <Point X="-22.859927734375" Y="2.247470214844" />
                  <Point X="-22.839611328125" Y="2.217529052734" />
                  <Point X="-22.81696875" Y="2.184161621094" />
                  <Point X="-22.805533203125" Y="2.170327636719" />
                  <Point X="-22.778400390625" Y="2.145592529297" />
                  <Point X="-22.748458984375" Y="2.125276123047" />
                  <Point X="-22.715091796875" Y="2.102635009766" />
                  <Point X="-22.695046875" Y="2.092272705078" />
                  <Point X="-22.672958984375" Y="2.084006591797" />
                  <Point X="-22.65103515625" Y="2.078663330078" />
                  <Point X="-22.618201171875" Y="2.074704101562" />
                  <Point X="-22.581611328125" Y="2.070291748047" />
                  <Point X="-22.563533203125" Y="2.069845703125" />
                  <Point X="-22.52679296875" Y="2.074171142578" />
                  <Point X="-22.488822265625" Y="2.084325195313" />
                  <Point X="-22.446505859375" Y="2.095640869141" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.624890625" Y="2.564273681641" />
                  <Point X="-21.032671875" Y="2.90619140625" />
                  <Point X="-20.997701171875" Y="2.85758984375" />
                  <Point X="-20.876728515625" Y="2.689465087891" />
                  <Point X="-20.78080859375" Y="2.530955810547" />
                  <Point X="-20.73780078125" Y="2.459883544922" />
                  <Point X="-21.232703125" Y="2.080130371094" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.77857421875" Y="1.660241577148" />
                  <Point X="-21.796025390625" Y="1.641627685547" />
                  <Point X="-21.823353515625" Y="1.605976928711" />
                  <Point X="-21.853806640625" Y="1.566246337891" />
                  <Point X="-21.863392578125" Y="1.550912231445" />
                  <Point X="-21.878369140625" Y="1.517087890625" />
                  <Point X="-21.888548828125" Y="1.480688232422" />
                  <Point X="-21.89989453125" Y="1.440123413086" />
                  <Point X="-21.90334765625" Y="1.417825317383" />
                  <Point X="-21.9041640625" Y="1.39425378418" />
                  <Point X="-21.902259765625" Y="1.371765869141" />
                  <Point X="-21.89390234375" Y="1.331266479492" />
                  <Point X="-21.88458984375" Y="1.286133178711" />
                  <Point X="-21.8793203125" Y="1.268979980469" />
                  <Point X="-21.863716796875" Y="1.235740966797" />
                  <Point X="-21.84098828125" Y="1.201194824219" />
                  <Point X="-21.81566015625" Y="1.162695556641" />
                  <Point X="-21.801107421875" Y="1.145451416016" />
                  <Point X="-21.78386328125" Y="1.129361206055" />
                  <Point X="-21.76565234375" Y="1.116034790039" />
                  <Point X="-21.73271484375" Y="1.097494262695" />
                  <Point X="-21.696009765625" Y="1.07683215332" />
                  <Point X="-21.679478515625" Y="1.069501342773" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.59934765625" Y="1.053552978516" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.764603515625" Y="1.145354370117" />
                  <Point X="-20.22316015625" Y="1.21663659668" />
                  <Point X="-20.206021484375" Y="1.146236938477" />
                  <Point X="-20.154060546875" Y="0.932788391113" />
                  <Point X="-20.1238359375" Y="0.738667114258" />
                  <Point X="-20.109134765625" Y="0.644238708496" />
                  <Point X="-20.66856640625" Y="0.494339141846" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.2952109375" Y="0.325585296631" />
                  <Point X="-21.318453125" Y="0.315068023682" />
                  <Point X="-21.362205078125" Y="0.289778839111" />
                  <Point X="-21.410962890625" Y="0.26159564209" />
                  <Point X="-21.4256875" Y="0.251095840454" />
                  <Point X="-21.452466796875" Y="0.225577148438" />
                  <Point X="-21.47871875" Y="0.192127105713" />
                  <Point X="-21.50797265625" Y="0.154849517822" />
                  <Point X="-21.51969921875" Y="0.135568344116" />
                  <Point X="-21.52947265625" Y="0.114105499268" />
                  <Point X="-21.536318359375" Y="0.092603630066" />
                  <Point X="-21.545068359375" Y="0.0469126091" />
                  <Point X="-21.5548203125" Y="-0.004006982803" />
                  <Point X="-21.556515625" Y="-0.021875137329" />
                  <Point X="-21.5548203125" Y="-0.058553001404" />
                  <Point X="-21.5460703125" Y="-0.104244171143" />
                  <Point X="-21.536318359375" Y="-0.155163619995" />
                  <Point X="-21.52947265625" Y="-0.176663360596" />
                  <Point X="-21.51969921875" Y="-0.198127578735" />
                  <Point X="-21.507974609375" Y="-0.217408447266" />
                  <Point X="-21.481724609375" Y="-0.250858337402" />
                  <Point X="-21.45246875" Y="-0.288136077881" />
                  <Point X="-21.43999609375" Y="-0.301239685059" />
                  <Point X="-21.410962890625" Y="-0.324155609131" />
                  <Point X="-21.3672109375" Y="-0.349444946289" />
                  <Point X="-21.318453125" Y="-0.37762802124" />
                  <Point X="-21.307291015625" Y="-0.383137695312" />
                  <Point X="-21.283419921875" Y="-0.392149810791" />
                  <Point X="-20.5982109375" Y="-0.575751342773" />
                  <Point X="-20.108525390625" Y="-0.706961853027" />
                  <Point X="-20.11596484375" Y="-0.756301269531" />
                  <Point X="-20.144974609375" Y="-0.948724182129" />
                  <Point X="-20.183701171875" Y="-1.118426879883" />
                  <Point X="-20.19882421875" Y="-1.18469909668" />
                  <Point X="-20.862560546875" Y="-1.09731652832" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710266113" />
                  <Point X="-21.62533984375" Y="-1.005508666992" />
                  <Point X="-21.711208984375" Y="-1.024172729492" />
                  <Point X="-21.806904296875" Y="-1.044972412109" />
                  <Point X="-21.836025390625" Y="-1.056597167969" />
                  <Point X="-21.8638515625" Y="-1.073489379883" />
                  <Point X="-21.8876015625" Y="-1.093960083008" />
                  <Point X="-21.93950390625" Y="-1.15638269043" />
                  <Point X="-21.997345703125" Y="-1.225947998047" />
                  <Point X="-22.012064453125" Y="-1.250329956055" />
                  <Point X="-22.023408203125" Y="-1.277716064453" />
                  <Point X="-22.030240234375" Y="-1.305365356445" />
                  <Point X="-22.0376796875" Y="-1.386205078125" />
                  <Point X="-22.04596875" Y="-1.476295532227" />
                  <Point X="-22.043650390625" Y="-1.50756237793" />
                  <Point X="-22.035919921875" Y="-1.539182739258" />
                  <Point X="-22.023548828125" Y="-1.567996826172" />
                  <Point X="-21.97602734375" Y="-1.641913085938" />
                  <Point X="-21.923068359375" Y="-1.724287353516" />
                  <Point X="-21.9130625" Y="-1.737241943359" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.253490234375" Y="-2.248836425781" />
                  <Point X="-20.786876953125" Y="-2.606882568359" />
                  <Point X="-20.79341015625" Y="-2.617453857422" />
                  <Point X="-20.875197265625" Y="-2.749797851562" />
                  <Point X="-20.955275390625" Y="-2.863579101562" />
                  <Point X="-20.971017578125" Y="-2.885945556641" />
                  <Point X="-21.563970703125" Y="-2.543602783203" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.2457734375" Y="-2.159825195312" />
                  <Point X="-22.347970703125" Y="-2.141368408203" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395507812" />
                  <Point X="-22.525390625" Y="-2.125353515625" />
                  <Point X="-22.555166015625" Y="-2.135176757812" />
                  <Point X="-22.640068359375" Y="-2.179859619141" />
                  <Point X="-22.73468359375" Y="-2.229655761719" />
                  <Point X="-22.757615234375" Y="-2.246549316406" />
                  <Point X="-22.77857421875" Y="-2.267508544922" />
                  <Point X="-22.795466796875" Y="-2.290438476562" />
                  <Point X="-22.840150390625" Y="-2.37533984375" />
                  <Point X="-22.889947265625" Y="-2.469956542969" />
                  <Point X="-22.899771484375" Y="-2.499735351563" />
                  <Point X="-22.904728515625" Y="-2.531908935547" />
                  <Point X="-22.90432421875" Y="-2.563259033203" />
                  <Point X="-22.8858671875" Y="-2.66545703125" />
                  <Point X="-22.865296875" Y="-2.779349365234" />
                  <Point X="-22.86101171875" Y="-2.795140869141" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.4395625" Y="-3.533822265625" />
                  <Point X="-22.13871484375" Y="-4.054903808594" />
                  <Point X="-22.2181640625" Y="-4.111653808594" />
                  <Point X="-22.298232421875" Y="-4.163480957031" />
                  <Point X="-22.75683203125" Y="-3.565822998047" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.378869140625" Y="-2.835755371094" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.69313671875" Y="-2.751260742188" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-23.9805234375" Y="-2.866237060547" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968861572266" />
                  <Point X="-24.11275" Y="-2.996688476562" />
                  <Point X="-24.124375" Y="-3.02580859375" />
                  <Point X="-24.149826171875" Y="-3.142903076172" />
                  <Point X="-24.178189453125" Y="-3.273396484375" />
                  <Point X="-24.180275390625" Y="-3.289627685547" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.064458984375" Y="-4.202698242188" />
                  <Point X="-23.977935546875" Y="-4.859915527344" />
                  <Point X="-24.024318359375" Y="-4.870083007812" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.058431640625" Y="-4.752634765625" />
                  <Point X="-26.14124609375" Y="-4.731328125" />
                  <Point X="-26.1207734375" Y="-4.575836914062" />
                  <Point X="-26.120076171875" Y="-4.568098632812" />
                  <Point X="-26.11944921875" Y="-4.541030761719" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497688964844" />
                  <Point X="-26.1519609375" Y="-4.353776855469" />
                  <Point X="-26.18386328125" Y="-4.193397460938" />
                  <Point X="-26.188125" Y="-4.178468261719" />
                  <Point X="-26.19902734375" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135462890625" />
                  <Point X="-26.221740234375" Y="-4.107625976562" />
                  <Point X="-26.23057421875" Y="-4.094861083984" />
                  <Point X="-26.25020703125" Y="-4.070937744141" />
                  <Point X="-26.261005859375" Y="-4.059779296875" />
                  <Point X="-26.37132421875" Y="-3.963032470703" />
                  <Point X="-26.494265625" Y="-3.85521484375" />
                  <Point X="-26.50673828125" Y="-3.845965087891" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822527587891" />
                  <Point X="-26.576529296875" Y="-3.810226318359" />
                  <Point X="-26.5913125" Y="-3.805476318359" />
                  <Point X="-26.621455078125" Y="-3.798447998047" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.783232421875" Y="-3.786572998047" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166015625" />
                  <Point X="-27.008005859375" Y="-3.781945556641" />
                  <Point X="-27.0390546875" Y="-3.790265136719" />
                  <Point X="-27.053673828125" Y="-3.79549609375" />
                  <Point X="-27.081865234375" Y="-3.80826953125" />
                  <Point X="-27.0954375" Y="-3.815812011719" />
                  <Point X="-27.217439453125" Y="-3.897331787109" />
                  <Point X="-27.35340234375" Y="-3.988180175781" />
                  <Point X="-27.35968359375" Y="-3.992759765625" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032770507812" />
                  <Point X="-27.410470703125" Y="-4.041629394531" />
                  <Point X="-27.503201171875" Y="-4.162479003906" />
                  <Point X="-27.52640625" Y="-4.148111328125" />
                  <Point X="-27.74759375" Y="-4.01115625" />
                  <Point X="-27.931546875" Y="-3.869519287109" />
                  <Point X="-27.98086328125" Y="-3.831546875" />
                  <Point X="-27.693564453125" Y="-3.333929199219" />
                  <Point X="-27.341490234375" Y="-2.724119628906" />
                  <Point X="-27.3348515625" Y="-2.710086181641" />
                  <Point X="-27.32394921875" Y="-2.681122070312" />
                  <Point X="-27.319685546875" Y="-2.66619140625" />
                  <Point X="-27.3134140625" Y="-2.634666748047" />
                  <Point X="-27.311638671875" Y="-2.619242431641" />
                  <Point X="-27.310625" Y="-2.588310058594" />
                  <Point X="-27.3146640625" Y="-2.557626220703" />
                  <Point X="-27.323646484375" Y="-2.528009521484" />
                  <Point X="-27.3293515625" Y="-2.513568603516" />
                  <Point X="-27.34356640625" Y="-2.484740234375" />
                  <Point X="-27.351548828125" Y="-2.471422363281" />
                  <Point X="-27.369576171875" Y="-2.446262207031" />
                  <Point X="-27.37962109375" Y="-2.434419921875" />
                  <Point X="-27.39696875" Y="-2.417071289062" />
                  <Point X="-27.40880859375" Y="-2.407025878906" />
                  <Point X="-27.43396484375" Y="-2.388998046875" />
                  <Point X="-27.44728125" Y="-2.381015625" />
                  <Point X="-27.476111328125" Y="-2.366796630859" />
                  <Point X="-27.490552734375" Y="-2.361089355469" />
                  <Point X="-27.520171875" Y="-2.352103515625" />
                  <Point X="-27.550859375" Y="-2.348062744141" />
                  <Point X="-27.581794921875" Y="-2.349074951172" />
                  <Point X="-27.597220703125" Y="-2.350849365234" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.366271484375" Y="-2.771286132812" />
                  <Point X="-28.793087890625" Y="-3.017708740234" />
                  <Point X="-28.829275390625" Y="-2.970166503906" />
                  <Point X="-29.004021484375" Y="-2.740584472656" />
                  <Point X="-29.135900390625" Y="-2.519444091797" />
                  <Point X="-29.181265625" Y="-2.443373535156" />
                  <Point X="-28.66413671875" Y="-2.046567016602" />
                  <Point X="-28.048123046875" Y="-1.573882080078" />
                  <Point X="-28.036484375" Y="-1.563310668945" />
                  <Point X="-28.01510546875" Y="-1.540389526367" />
                  <Point X="-28.005365234375" Y="-1.528039794922" />
                  <Point X="-27.987400390625" Y="-1.500907958984" />
                  <Point X="-27.979833984375" Y="-1.487121582031" />
                  <Point X="-27.967080078125" Y="-1.458494628906" />
                  <Point X="-27.961892578125" Y="-1.443654174805" />
                  <Point X="-27.9541875" Y="-1.413906982422" />
                  <Point X="-27.951552734375" Y="-1.398805053711" />
                  <Point X="-27.948748046875" Y="-1.368373901367" />
                  <Point X="-27.948578125" Y="-1.353045043945" />
                  <Point X="-27.950787109375" Y="-1.321375488281" />
                  <Point X="-27.953083984375" Y="-1.306222045898" />
                  <Point X="-27.960083984375" Y="-1.276479370117" />
                  <Point X="-27.971775390625" Y="-1.248248413086" />
                  <Point X="-27.98785546875" Y="-1.222266479492" />
                  <Point X="-27.996947265625" Y="-1.209926635742" />
                  <Point X="-28.01777734375" Y="-1.185970458984" />
                  <Point X="-28.02873828125" Y="-1.175250732422" />
                  <Point X="-28.052240234375" Y="-1.155713745117" />
                  <Point X="-28.06478125" Y="-1.146896118164" />
                  <Point X="-28.091263671875" Y="-1.131309448242" />
                  <Point X="-28.1054296875" Y="-1.124482421875" />
                  <Point X="-28.13469140625" Y="-1.113258789062" />
                  <Point X="-28.149787109375" Y="-1.108861816406" />
                  <Point X="-28.18167578125" Y="-1.102379272461" />
                  <Point X="-28.197294921875" Y="-1.100532714844" />
                  <Point X="-28.22862109375" Y="-1.09944152832" />
                  <Point X="-28.244328125" Y="-1.100196655273" />
                  <Point X="-29.10224609375" Y="-1.213143554688" />
                  <Point X="-29.66091796875" Y="-1.286694213867" />
                  <Point X="-29.672416015625" Y="-1.241680908203" />
                  <Point X="-29.740759765625" Y="-0.974114868164" />
                  <Point X="-29.775654296875" Y="-0.730150512695" />
                  <Point X="-29.786451171875" Y="-0.654654602051" />
                  <Point X="-29.21001953125" Y="-0.500200256348" />
                  <Point X="-28.508287109375" Y="-0.312171325684" />
                  <Point X="-28.496791015625" Y="-0.308288421631" />
                  <Point X="-28.474390625" Y="-0.299090576172" />
                  <Point X="-28.463486328125" Y="-0.293775848389" />
                  <Point X="-28.434794921875" Y="-0.27744708252" />
                  <Point X="-28.427994140625" Y="-0.273186798096" />
                  <Point X="-28.408353515625" Y="-0.2592840271" />
                  <Point X="-28.381904296875" Y="-0.237612731934" />
                  <Point X="-28.37327734375" Y="-0.229602096558" />
                  <Point X="-28.357083984375" Y="-0.212577346802" />
                  <Point X="-28.34312109375" Y="-0.193680099487" />
                  <Point X="-28.3316015625" Y="-0.173201644897" />
                  <Point X="-28.326478515625" Y="-0.162606170654" />
                  <Point X="-28.314677734375" Y="-0.133769454956" />
                  <Point X="-28.31209375" Y="-0.126661270142" />
                  <Point X="-28.305490234375" Y="-0.104969314575" />
                  <Point X="-28.298369140625" Y="-0.074440834045" />
                  <Point X="-28.29651953125" Y="-0.063822021484" />
                  <Point X="-28.29403515625" Y="-0.042434593201" />
                  <Point X="-28.293990234375" Y="-0.020899133682" />
                  <Point X="-28.29638671875" Y="0.000498104453" />
                  <Point X="-28.298193359375" Y="0.01112865448" />
                  <Point X="-28.304814453125" Y="0.040044132233" />
                  <Point X="-28.3067890625" Y="0.047323680878" />
                  <Point X="-28.313853515625" Y="0.068804595947" />
                  <Point X="-28.326154296875" Y="0.099253120422" />
                  <Point X="-28.331232421875" Y="0.10987474823" />
                  <Point X="-28.34266015625" Y="0.130403884888" />
                  <Point X="-28.35653515625" Y="0.149358947754" />
                  <Point X="-28.372650390625" Y="0.166455780029" />
                  <Point X="-28.381240234375" Y="0.174505203247" />
                  <Point X="-28.406185546875" Y="0.195133468628" />
                  <Point X="-28.41257421875" Y="0.199977386475" />
                  <Point X="-28.432513671875" Y="0.213399215698" />
                  <Point X="-28.462708984375" Y="0.230772201538" />
                  <Point X="-28.473796875" Y="0.236224578857" />
                  <Point X="-28.4965859375" Y="0.24564414978" />
                  <Point X="-28.508287109375" Y="0.249611190796" />
                  <Point X="-29.290318359375" Y="0.459155944824" />
                  <Point X="-29.7854453125" Y="0.591824890137" />
                  <Point X="-29.775375" Y="0.659878173828" />
                  <Point X="-29.73133203125" Y="0.957521057129" />
                  <Point X="-29.66108984375" Y="1.216734130859" />
                  <Point X="-29.6335859375" Y="1.318237060547" />
                  <Point X="-29.2810859375" Y="1.271829589844" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.7470546875" Y="1.204364379883" />
                  <Point X="-28.736701171875" Y="1.204703369141" />
                  <Point X="-28.715140625" Y="1.20658984375" />
                  <Point X="-28.70488671875" Y="1.208053833008" />
                  <Point X="-28.6846015625" Y="1.212088989258" />
                  <Point X="-28.6745703125" Y="1.214660400391" />
                  <Point X="-28.64551953125" Y="1.22382019043" />
                  <Point X="-28.61314453125" Y="1.234028198242" />
                  <Point X="-28.60344921875" Y="1.237677001953" />
                  <Point X="-28.584515625" Y="1.246008300781" />
                  <Point X="-28.57527734375" Y="1.250690185547" />
                  <Point X="-28.556533203125" Y="1.261512207031" />
                  <Point X="-28.547859375" Y="1.267172241211" />
                  <Point X="-28.531177734375" Y="1.279403930664" />
                  <Point X="-28.51592578125" Y="1.293379150391" />
                  <Point X="-28.502287109375" Y="1.308931518555" />
                  <Point X="-28.495892578125" Y="1.317080078125" />
                  <Point X="-28.483478515625" Y="1.334809936523" />
                  <Point X="-28.478009765625" Y="1.343603759766" />
                  <Point X="-28.468060546875" Y="1.361738525391" />
                  <Point X="-28.463580078125" Y="1.371079589844" />
                  <Point X="-28.451923828125" Y="1.399221679688" />
                  <Point X="-28.43893359375" Y="1.430583618164" />
                  <Point X="-28.4355" Y="1.440349731445" />
                  <Point X="-28.429712890625" Y="1.460201538086" />
                  <Point X="-28.427359375" Y="1.470286987305" />
                  <Point X="-28.423599609375" Y="1.491602783203" />
                  <Point X="-28.422359375" Y="1.501890014648" />
                  <Point X="-28.421005859375" Y="1.522536376953" />
                  <Point X="-28.421908203125" Y="1.543206787109" />
                  <Point X="-28.425056640625" Y="1.563656616211" />
                  <Point X="-28.427189453125" Y="1.573794921875" />
                  <Point X="-28.43279296875" Y="1.594701538086" />
                  <Point X="-28.436013671875" Y="1.604540527344" />
                  <Point X="-28.4435078125" Y="1.623810668945" />
                  <Point X="-28.44778125" Y="1.633241821289" />
                  <Point X="-28.461845703125" Y="1.660260742188" />
                  <Point X="-28.477521484375" Y="1.690371459961" />
                  <Point X="-28.482798828125" Y="1.69928515625" />
                  <Point X="-28.49429296875" Y="1.716487915039" />
                  <Point X="-28.500509765625" Y="1.724776611328" />
                  <Point X="-28.514421875" Y="1.74135546875" />
                  <Point X="-28.521505859375" Y="1.748917236328" />
                  <Point X="-28.536447265625" Y="1.76321875" />
                  <Point X="-28.5443046875" Y="1.769958496094" />
                  <Point X="-28.99287890625" Y="2.114162841797" />
                  <Point X="-29.22761328125" Y="2.294280761719" />
                  <Point X="-29.173431640625" Y="2.387106933594" />
                  <Point X="-29.00228125" Y="2.680324951172" />
                  <Point X="-28.816236328125" Y="2.919462890625" />
                  <Point X="-28.726337890625" Y="3.035013671875" />
                  <Point X="-28.56309765625" Y="2.940766113281" />
                  <Point X="-28.25415625" Y="2.762398925781" />
                  <Point X="-28.24491796875" Y="2.757716796875" />
                  <Point X="-28.225984375" Y="2.749385742188" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.114611328125" Y="2.727618164062" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.038494140625" Y="2.723785644531" />
                  <Point X="-28.02816796875" Y="2.724575683594" />
                  <Point X="-28.006708984375" Y="2.727400634766" />
                  <Point X="-27.99652734375" Y="2.729310302734" />
                  <Point X="-27.976435546875" Y="2.7342265625" />
                  <Point X="-27.957" Y="2.741300292969" />
                  <Point X="-27.938447265625" Y="2.750448974609" />
                  <Point X="-27.929419921875" Y="2.755530517578" />
                  <Point X="-27.911166015625" Y="2.767159423828" />
                  <Point X="-27.90275" Y="2.773190185547" />
                  <Point X="-27.8866171875" Y="2.786136962891" />
                  <Point X="-27.878900390625" Y="2.793052978516" />
                  <Point X="-27.850181640625" Y="2.821771484375" />
                  <Point X="-27.81817578125" Y="2.853776367188" />
                  <Point X="-27.81126171875" Y="2.861489990234" />
                  <Point X="-27.798314453125" Y="2.877622314453" />
                  <Point X="-27.79228125" Y="2.886041015625" />
                  <Point X="-27.78065234375" Y="2.904294921875" />
                  <Point X="-27.7755703125" Y="2.913323974609" />
                  <Point X="-27.766423828125" Y="2.931873046875" />
                  <Point X="-27.759353515625" Y="2.951297851562" />
                  <Point X="-27.754435546875" Y="2.971387207031" />
                  <Point X="-27.7525234375" Y="2.981571777344" />
                  <Point X="-27.749697265625" Y="3.003030029297" />
                  <Point X="-27.748908203125" Y="3.013363525391" />
                  <Point X="-27.74845703125" Y="3.034051025391" />
                  <Point X="-27.748794921875" Y="3.044405029297" />
                  <Point X="-27.7523359375" Y="3.084864990234" />
                  <Point X="-27.756279296875" Y="3.129954589844" />
                  <Point X="-27.757744140625" Y="3.140212646484" />
                  <Point X="-27.76178125" Y="3.160500244141" />
                  <Point X="-27.764353515625" Y="3.170529785156" />
                  <Point X="-27.77086328125" Y="3.191173583984" />
                  <Point X="-27.77451171875" Y="3.200867431641" />
                  <Point X="-27.782841796875" Y="3.219797119141" />
                  <Point X="-27.7875234375" Y="3.229032958984" />
                  <Point X="-27.98630078125" Y="3.573325439453" />
                  <Point X="-28.05938671875" Y="3.699915039062" />
                  <Point X="-27.946439453125" Y="3.78651171875" />
                  <Point X="-27.6483671875" Y="4.015041015625" />
                  <Point X="-27.355345703125" Y="4.177836914063" />
                  <Point X="-27.1925234375" Y="4.268296386719" />
                  <Point X="-27.118564453125" Y="4.17191015625" />
                  <Point X="-27.1118203125" Y="4.164048828125" />
                  <Point X="-27.097517578125" Y="4.149107421875" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.06509375" Y="4.121898925781" />
                  <Point X="-27.04789453125" Y="4.11040625" />
                  <Point X="-27.03898046875" Y="4.105128417969" />
                  <Point X="-26.99394921875" Y="4.081686767578" />
                  <Point X="-26.943763671875" Y="4.055561767578" />
                  <Point X="-26.93432421875" Y="4.051285400391" />
                  <Point X="-26.91504296875" Y="4.043788330078" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031378173828" />
                  <Point X="-26.780818359375" Y="4.035136474609" />
                  <Point X="-26.770728515625" Y="4.037488525391" />
                  <Point X="-26.7508671875" Y="4.043277587891" />
                  <Point X="-26.741095703125" Y="4.046714355469" />
                  <Point X="-26.694193359375" Y="4.066142578125" />
                  <Point X="-26.641921875" Y="4.087793945312" />
                  <Point X="-26.632583984375" Y="4.092272705078" />
                  <Point X="-26.614453125" Y="4.102219238281" />
                  <Point X="-26.60566015625" Y="4.1076875" />
                  <Point X="-26.5879296875" Y="4.1201015625" />
                  <Point X="-26.579783203125" Y="4.126494140625" />
                  <Point X="-26.5642265625" Y="4.140135253906" />
                  <Point X="-26.5502421875" Y="4.155395507812" />
                  <Point X="-26.538009765625" Y="4.172081054688" />
                  <Point X="-26.5323515625" Y="4.180754882812" />
                  <Point X="-26.52153125" Y="4.199499023438" />
                  <Point X="-26.5168515625" Y="4.208734375" />
                  <Point X="-26.5085234375" Y="4.227662597656" />
                  <Point X="-26.504875" Y="4.23735546875" />
                  <Point X="-26.489609375" Y="4.2857734375" />
                  <Point X="-26.472595703125" Y="4.339731933594" />
                  <Point X="-26.470025390625" Y="4.349764160156" />
                  <Point X="-26.465990234375" Y="4.370049316406" />
                  <Point X="-26.464525390625" Y="4.380302246094" />
                  <Point X="-26.462638671875" Y="4.401863769531" />
                  <Point X="-26.46230078125" Y="4.412216796875" />
                  <Point X="-26.462751953125" Y="4.4328984375" />
                  <Point X="-26.463541015625" Y="4.443227050781" />
                  <Point X="-26.479263671875" Y="4.562654296875" />
                  <Point X="-26.317037109375" Y="4.608137207031" />
                  <Point X="-25.931169921875" Y="4.716320800781" />
                  <Point X="-25.5759453125" Y="4.75789453125" />
                  <Point X="-25.36522265625" Y="4.782556152344" />
                  <Point X="-25.317060546875" Y="4.602812011719" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.66016796875" Y="4.641841796875" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.509048828125" Y="4.773197265625" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.878220703125" Y="4.666955078125" />
                  <Point X="-23.54640625" Y="4.586845214844" />
                  <Point X="-23.35685546875" Y="4.518093261719" />
                  <Point X="-23.141734375" Y="4.440067382812" />
                  <Point X="-22.9567578125" Y="4.353559570312" />
                  <Point X="-22.7495546875" Y="4.256657226562" />
                  <Point X="-22.5707890625" Y="4.152509277344" />
                  <Point X="-22.370564453125" Y="4.035858398438" />
                  <Point X="-22.202021484375" Y="3.915998779297" />
                  <Point X="-22.18221875" Y="3.901915527344" />
                  <Point X="-22.527189453125" Y="3.304406982422" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.93762109375" Y="2.593110595703" />
                  <Point X="-22.9468125" Y="2.573448974609" />
                  <Point X="-22.955814453125" Y="2.549571777344" />
                  <Point X="-22.958697265625" Y="2.540601318359" />
                  <Point X="-22.9688515625" Y="2.502630615234" />
                  <Point X="-22.98016796875" Y="2.460314941406" />
                  <Point X="-22.9820859375" Y="2.451469726562" />
                  <Point X="-22.986353515625" Y="2.420223144531" />
                  <Point X="-22.987244140625" Y="2.383240722656" />
                  <Point X="-22.986587890625" Y="2.369581298828" />
                  <Point X="-22.98262890625" Y="2.336747558594" />
                  <Point X="-22.978216796875" Y="2.300156494141" />
                  <Point X="-22.97619921875" Y="2.289035644531" />
                  <Point X="-22.97085546875" Y="2.267108154297" />
                  <Point X="-22.967529296875" Y="2.256301513672" />
                  <Point X="-22.95926171875" Y="2.234211669922" />
                  <Point X="-22.954677734375" Y="2.223882324219" />
                  <Point X="-22.94431640625" Y="2.203841064453" />
                  <Point X="-22.9385390625" Y="2.194129150391" />
                  <Point X="-22.91822265625" Y="2.164187988281" />
                  <Point X="-22.895580078125" Y="2.130820556641" />
                  <Point X="-22.89019140625" Y="2.123634277344" />
                  <Point X="-22.86953515625" Y="2.100122070312" />
                  <Point X="-22.84240234375" Y="2.075386962891" />
                  <Point X="-22.8317421875" Y="2.066981201172" />
                  <Point X="-22.80180078125" Y="2.046664672852" />
                  <Point X="-22.76843359375" Y="2.02402355957" />
                  <Point X="-22.75871875" Y="2.018244384766" />
                  <Point X="-22.738673828125" Y="2.007882080078" />
                  <Point X="-22.72834375" Y="2.003299194336" />
                  <Point X="-22.706255859375" Y="1.995033081055" />
                  <Point X="-22.695453125" Y="1.991708251953" />
                  <Point X="-22.673529296875" Y="1.986365112305" />
                  <Point X="-22.662408203125" Y="1.984346557617" />
                  <Point X="-22.62957421875" Y="1.980387329102" />
                  <Point X="-22.592984375" Y="1.975974975586" />
                  <Point X="-22.583955078125" Y="1.975320800781" />
                  <Point X="-22.55242578125" Y="1.975497314453" />
                  <Point X="-22.515685546875" Y="1.979822875977" />
                  <Point X="-22.50225" Y="1.982395996094" />
                  <Point X="-22.464279296875" Y="1.992550048828" />
                  <Point X="-22.421962890625" Y="2.003865722656" />
                  <Point X="-22.416" Y="2.005671875" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572753906" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.577390625" Y="2.482001220703" />
                  <Point X="-21.05959375" Y="2.780951416016" />
                  <Point X="-20.956046875" Y="2.637044921875" />
                  <Point X="-20.863115234375" Y="2.483471435547" />
                  <Point X="-21.29053515625" Y="2.155498779297" />
                  <Point X="-21.827046875" Y="1.743819946289" />
                  <Point X="-21.831861328125" Y="1.739869018555" />
                  <Point X="-21.84787890625" Y="1.725217285156" />
                  <Point X="-21.865330078125" Y="1.706603393555" />
                  <Point X="-21.871421875" Y="1.699423339844" />
                  <Point X="-21.89875" Y="1.663772460938" />
                  <Point X="-21.929203125" Y="1.624041870117" />
                  <Point X="-21.934361328125" Y="1.616604248047" />
                  <Point X="-21.9502578125" Y="1.589374267578" />
                  <Point X="-21.965234375" Y="1.555549926758" />
                  <Point X="-21.969859375" Y="1.542674316406" />
                  <Point X="-21.9800390625" Y="1.506274536133" />
                  <Point X="-21.991384765625" Y="1.465709960938" />
                  <Point X="-21.993775390625" Y="1.454661987305" />
                  <Point X="-21.997228515625" Y="1.432363891602" />
                  <Point X="-21.998291015625" Y="1.421113647461" />
                  <Point X="-21.999107421875" Y="1.397542114258" />
                  <Point X="-21.998826171875" Y="1.386237792969" />
                  <Point X="-21.996921875" Y="1.36374987793" />
                  <Point X="-21.995298828125" Y="1.35256628418" />
                  <Point X="-21.98694140625" Y="1.312066772461" />
                  <Point X="-21.97762890625" Y="1.26693359375" />
                  <Point X="-21.975400390625" Y="1.258235473633" />
                  <Point X="-21.96531640625" Y="1.228610595703" />
                  <Point X="-21.949712890625" Y="1.195371582031" />
                  <Point X="-21.943080078125" Y="1.183526123047" />
                  <Point X="-21.9203515625" Y="1.148979980469" />
                  <Point X="-21.8950234375" Y="1.110480712891" />
                  <Point X="-21.88826171875" Y="1.101425415039" />
                  <Point X="-21.873708984375" Y="1.084181274414" />
                  <Point X="-21.86591796875" Y="1.075992431641" />
                  <Point X="-21.848673828125" Y="1.05990222168" />
                  <Point X="-21.83996484375" Y="1.052696044922" />
                  <Point X="-21.82175390625" Y="1.039369628906" />
                  <Point X="-21.812251953125" Y="1.033249267578" />
                  <Point X="-21.779314453125" Y="1.01470880127" />
                  <Point X="-21.742609375" Y="0.994046630859" />
                  <Point X="-21.734521484375" Y="0.989988037109" />
                  <Point X="-21.7053203125" Y="0.978083496094" />
                  <Point X="-21.66972265625" Y="0.968020996094" />
                  <Point X="-21.656328125" Y="0.965257629395" />
                  <Point X="-21.611794921875" Y="0.959371887207" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.555966796875" Y="0.952199829102" />
                  <Point X="-21.53428125" Y="0.951222900391" />
                  <Point X="-21.50878125" Y="0.952032287598" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-20.752203125" Y="1.051166992188" />
                  <Point X="-20.295296875" Y="1.111319946289" />
                  <Point X="-20.2473125" Y="0.914206542969" />
                  <Point X="-20.217705078125" Y="0.724051757812" />
                  <Point X="-20.216126953125" Y="0.713921142578" />
                  <Point X="-20.693154296875" Y="0.586102050781" />
                  <Point X="-21.3080078125" Y="0.42135269165" />
                  <Point X="-21.313970703125" Y="0.419543487549" />
                  <Point X="-21.334376953125" Y="0.412136413574" />
                  <Point X="-21.357619140625" Y="0.401619140625" />
                  <Point X="-21.365994140625" Y="0.397316802979" />
                  <Point X="-21.40974609375" Y="0.372027709961" />
                  <Point X="-21.45850390625" Y="0.343844512939" />
                  <Point X="-21.466119140625" Y="0.338944396973" />
                  <Point X="-21.491224609375" Y="0.319870147705" />
                  <Point X="-21.51800390625" Y="0.294351593018" />
                  <Point X="-21.52719921875" Y="0.284228424072" />
                  <Point X="-21.553451171875" Y="0.250778320313" />
                  <Point X="-21.582705078125" Y="0.21350088501" />
                  <Point X="-21.589140625" Y="0.204214324951" />
                  <Point X="-21.6008671875" Y="0.184933181763" />
                  <Point X="-21.606158203125" Y="0.174938446045" />
                  <Point X="-21.615931640625" Y="0.153475509644" />
                  <Point X="-21.61999609375" Y="0.142925964355" />
                  <Point X="-21.626841796875" Y="0.121424087524" />
                  <Point X="-21.629623046875" Y="0.11047177124" />
                  <Point X="-21.638373046875" Y="0.064780776978" />
                  <Point X="-21.648125" Y="0.013861235619" />
                  <Point X="-21.649396484375" Y="0.004966154575" />
                  <Point X="-21.6514140625" Y="-0.026261449814" />
                  <Point X="-21.64971875" Y="-0.062939350128" />
                  <Point X="-21.648125" Y="-0.076421081543" />
                  <Point X="-21.639375" Y="-0.122112220764" />
                  <Point X="-21.629623046875" Y="-0.173031768799" />
                  <Point X="-21.62683984375" Y="-0.183986618042" />
                  <Point X="-21.619994140625" Y="-0.205486404419" />
                  <Point X="-21.615931640625" Y="-0.216031341553" />
                  <Point X="-21.606158203125" Y="-0.23749546814" />
                  <Point X="-21.600869140625" Y="-0.247486938477" />
                  <Point X="-21.58914453125" Y="-0.266767791748" />
                  <Point X="-21.582708984375" Y="-0.276057159424" />
                  <Point X="-21.556458984375" Y="-0.309507110596" />
                  <Point X="-21.527203125" Y="-0.346784881592" />
                  <Point X="-21.521279296875" Y="-0.353634155273" />
                  <Point X="-21.498853515625" Y="-0.375809875488" />
                  <Point X="-21.4698203125" Y="-0.398725738525" />
                  <Point X="-21.45850390625" Y="-0.40640435791" />
                  <Point X="-21.414751953125" Y="-0.431693572998" />
                  <Point X="-21.365994140625" Y="-0.459876647949" />
                  <Point X="-21.360501953125" Y="-0.462815368652" />
                  <Point X="-21.340845703125" Y="-0.472014678955" />
                  <Point X="-21.316974609375" Y="-0.481026855469" />
                  <Point X="-21.3080078125" Y="-0.483912689209" />
                  <Point X="-20.622798828125" Y="-0.667514282227" />
                  <Point X="-20.21512109375" Y="-0.776751037598" />
                  <Point X="-20.238380859375" Y="-0.93103894043" />
                  <Point X="-20.272197265625" Y="-1.079219726563" />
                  <Point X="-20.85016015625" Y="-1.003129333496" />
                  <Point X="-21.563216796875" Y="-0.909253601074" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042480469" />
                  <Point X="-21.63327734375" Y="-0.910840881348" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.73138671875" Y="-0.931340270996" />
                  <Point X="-21.82708203125" Y="-0.952139892578" />
                  <Point X="-21.842125" Y="-0.956742492676" />
                  <Point X="-21.87124609375" Y="-0.9683671875" />
                  <Point X="-21.88532421875" Y="-0.975389465332" />
                  <Point X="-21.913150390625" Y="-0.992281616211" />
                  <Point X="-21.925875" Y="-1.001530273438" />
                  <Point X="-21.949625" Y="-1.022000976562" />
                  <Point X="-21.960650390625" Y="-1.033223022461" />
                  <Point X="-22.012552734375" Y="-1.095645629883" />
                  <Point X="-22.07039453125" Y="-1.1652109375" />
                  <Point X="-22.07867578125" Y="-1.176851318359" />
                  <Point X="-22.09339453125" Y="-1.201233276367" />
                  <Point X="-22.09983203125" Y="-1.213974853516" />
                  <Point X="-22.11117578125" Y="-1.241360961914" />
                  <Point X="-22.115634765625" Y="-1.254927368164" />
                  <Point X="-22.122466796875" Y="-1.282576660156" />
                  <Point X="-22.12483984375" Y="-1.296659545898" />
                  <Point X="-22.132279296875" Y="-1.377499267578" />
                  <Point X="-22.140568359375" Y="-1.46758972168" />
                  <Point X="-22.140708984375" Y="-1.48332019043" />
                  <Point X="-22.138390625" Y="-1.514587036133" />
                  <Point X="-22.135931640625" Y="-1.530123291016" />
                  <Point X="-22.128201171875" Y="-1.561743652344" />
                  <Point X="-22.12321484375" Y="-1.576661865234" />
                  <Point X="-22.11084375" Y="-1.605475952148" />
                  <Point X="-22.103458984375" Y="-1.619371826172" />
                  <Point X="-22.0559375" Y="-1.693288085938" />
                  <Point X="-22.002978515625" Y="-1.775662353516" />
                  <Point X="-21.99825390625" Y="-1.782358642578" />
                  <Point X="-21.980201171875" Y="-1.804454101562" />
                  <Point X="-21.9565078125" Y="-1.828121337891" />
                  <Point X="-21.947201171875" Y="-1.836277587891" />
                  <Point X="-21.311322265625" Y="-2.324205078125" />
                  <Point X="-20.912828125" Y="-2.629980957031" />
                  <Point X="-20.954525390625" Y="-2.697453369141" />
                  <Point X="-20.99872265625" Y="-2.760252929688" />
                  <Point X="-21.516470703125" Y="-2.461330322266" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.184970703125" Y="-2.079513916016" />
                  <Point X="-22.216876953125" Y="-2.069326416016" />
                  <Point X="-22.228888671875" Y="-2.066337646484" />
                  <Point X="-22.3310859375" Y="-2.047880737305" />
                  <Point X="-22.444978515625" Y="-2.027311889648" />
                  <Point X="-22.460638671875" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403442383" />
                  <Point X="-22.507685546875" Y="-2.02650378418" />
                  <Point X="-22.539859375" Y="-2.031461914062" />
                  <Point X="-22.555154296875" Y="-2.035136474609" />
                  <Point X="-22.5849296875" Y="-2.044959594727" />
                  <Point X="-22.59941015625" Y="-2.051108398438" />
                  <Point X="-22.6843125" Y="-2.095791259766" />
                  <Point X="-22.778927734375" Y="-2.145587402344" />
                  <Point X="-22.791029296875" Y="-2.153169921875" />
                  <Point X="-22.8139609375" Y="-2.170063476562" />
                  <Point X="-22.824791015625" Y="-2.179374511719" />
                  <Point X="-22.84575" Y="-2.200333740234" />
                  <Point X="-22.85505859375" Y="-2.211161376953" />
                  <Point X="-22.871951171875" Y="-2.234091308594" />
                  <Point X="-22.87953515625" Y="-2.246193603516" />
                  <Point X="-22.92421875" Y="-2.331094970703" />
                  <Point X="-22.974015625" Y="-2.425711669922" />
                  <Point X="-22.9801640625" Y="-2.440193359375" />
                  <Point X="-22.98998828125" Y="-2.469972167969" />
                  <Point X="-22.9936640625" Y="-2.485269287109" />
                  <Point X="-22.99862109375" Y="-2.517442871094" />
                  <Point X="-22.999720703125" Y="-2.533134033203" />
                  <Point X="-22.99931640625" Y="-2.564484130859" />
                  <Point X="-22.9978125" Y="-2.580143066406" />
                  <Point X="-22.97935546875" Y="-2.682341064453" />
                  <Point X="-22.95878515625" Y="-2.796233398438" />
                  <Point X="-22.95698046875" Y="-2.804228759766" />
                  <Point X="-22.94876171875" Y="-2.831542236328" />
                  <Point X="-22.935927734375" Y="-2.862479980469" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.521833984375" Y="-3.581322265625" />
                  <Point X="-22.264103515625" Y="-4.027721679687" />
                  <Point X="-22.2762421875" Y="-4.036082763672" />
                  <Point X="-22.681462890625" Y="-3.507990722656" />
                  <Point X="-23.16608203125" Y="-2.876422851562" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849625732422" />
                  <Point X="-23.21674609375" Y="-2.828004638672" />
                  <Point X="-23.22669921875" Y="-2.820646972656" />
                  <Point X="-23.327494140625" Y="-2.755845214844" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.453716796875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663873535156" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.701841796875" Y="-2.656660400391" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920125" Y="-2.699414794922" />
                  <Point X="-23.944505859375" Y="-2.714135742188" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-24.041259765625" Y="-2.793189208984" />
                  <Point X="-24.136123046875" Y="-2.872063720703" />
                  <Point X="-24.147345703125" Y="-2.883088378906" />
                  <Point X="-24.16781640625" Y="-2.906838378906" />
                  <Point X="-24.177064453125" Y="-2.919563720703" />
                  <Point X="-24.19395703125" Y="-2.947390625" />
                  <Point X="-24.200978515625" Y="-2.961466552734" />
                  <Point X="-24.212603515625" Y="-2.990586669922" />
                  <Point X="-24.21720703125" Y="-3.005630859375" />
                  <Point X="-24.242658203125" Y="-3.122725341797" />
                  <Point X="-24.271021484375" Y="-3.25321875" />
                  <Point X="-24.2724140625" Y="-3.261287353516" />
                  <Point X="-24.275275390625" Y="-3.289677490234" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520019531" />
                  <Point X="-24.166912109375" Y="-4.152315429688" />
                  <Point X="-24.344931640625" Y="-3.487937011719" />
                  <Point X="-24.347392578125" Y="-3.480121337891" />
                  <Point X="-24.357853515625" Y="-3.453579589844" />
                  <Point X="-24.3732109375" Y="-3.423815917969" />
                  <Point X="-24.37958984375" Y="-3.413209960938" />
                  <Point X="-24.4570234375" Y="-3.301642822266" />
                  <Point X="-24.543318359375" Y="-3.177309570312" />
                  <Point X="-24.553328125" Y="-3.165172851562" />
                  <Point X="-24.575212890625" Y="-3.142716552734" />
                  <Point X="-24.587087890625" Y="-3.132396972656" />
                  <Point X="-24.61334375" Y="-3.113153320312" />
                  <Point X="-24.6267578125" Y="-3.104936523438" />
                  <Point X="-24.6547578125" Y="-3.090829101562" />
                  <Point X="-24.66934375" Y="-3.084938476562" />
                  <Point X="-24.78916796875" Y="-3.047749511719" />
                  <Point X="-24.922703125" Y="-3.006305175781" />
                  <Point X="-24.936623046875" Y="-3.003109130859" />
                  <Point X="-24.964783203125" Y="-2.99883984375" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.998840087891" />
                  <Point X="-25.051064453125" Y="-3.003109619141" />
                  <Point X="-25.064984375" Y="-3.006305664062" />
                  <Point X="-25.184806640625" Y="-3.043494384766" />
                  <Point X="-25.31834375" Y="-3.084938964844" />
                  <Point X="-25.3329296875" Y="-3.090829345703" />
                  <Point X="-25.3609296875" Y="-3.104936767578" />
                  <Point X="-25.37434375" Y="-3.113153808594" />
                  <Point X="-25.400599609375" Y="-3.132397705078" />
                  <Point X="-25.412474609375" Y="-3.142717773438" />
                  <Point X="-25.434359375" Y="-3.165174560547" />
                  <Point X="-25.444369140625" Y="-3.177311279297" />
                  <Point X="-25.52180078125" Y="-3.288878173828" />
                  <Point X="-25.608095703125" Y="-3.413211669922" />
                  <Point X="-25.61246875" Y="-3.420131835938" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213378906" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.853884765625" Y="-4.275884277344" />
                  <Point X="-25.985427734375" Y="-4.766805664062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.364419304852" Y="-1.067078443224" />
                  <Point X="-20.297988648393" Y="-0.754546776654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.458897807828" Y="-1.05464009027" />
                  <Point X="-20.389877539861" Y="-0.729925259431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.553376310803" Y="-1.042201737317" />
                  <Point X="-20.481766431329" Y="-0.705303742207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.013827996481" Y="-2.751531838966" />
                  <Point X="-20.977451450021" Y="-2.580393643212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.647854813779" Y="-1.029763384363" />
                  <Point X="-20.573655322797" Y="-0.680682224984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.28091307834" Y="0.696561752609" />
                  <Point X="-20.241959251224" Y="0.879825100535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.100334311196" Y="-2.701587289084" />
                  <Point X="-21.060954415253" Y="-2.516319444919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.742333316754" Y="-1.017325031409" />
                  <Point X="-20.665544205795" Y="-0.656060667908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.383901048706" Y="0.668966209061" />
                  <Point X="-20.292402817264" Y="1.099431543665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.186840625912" Y="-2.651642739203" />
                  <Point X="-21.144457380484" Y="-2.452245246627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.83681181973" Y="-1.004886678456" />
                  <Point X="-20.757433079054" Y="-0.631439065017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.486889019072" Y="0.641370665513" />
                  <Point X="-20.389638231395" Y="1.098899649388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.273346940627" Y="-2.601698189321" />
                  <Point X="-21.227960345715" Y="-2.388171048334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.931290325571" Y="-0.992448338982" />
                  <Point X="-20.849321952312" Y="-0.606817462126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.589876989438" Y="0.613775121964" />
                  <Point X="-20.489556670653" Y="1.085745114315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.359853255342" Y="-2.55175363944" />
                  <Point X="-21.311463310966" Y="-2.32409685013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.025768831883" Y="-0.980010001727" />
                  <Point X="-20.941210825571" Y="-0.582195859234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.692864959805" Y="0.586179578416" />
                  <Point X="-20.58947510991" Y="1.072590579242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.446359570057" Y="-2.501809089559" />
                  <Point X="-21.39496628737" Y="-2.260022704403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.120247338195" Y="-0.967571664472" />
                  <Point X="-21.03309969883" Y="-0.557574256343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.795852924742" Y="0.558584060407" />
                  <Point X="-20.689393549168" Y="1.05943604417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.532865891517" Y="-2.451864571406" />
                  <Point X="-21.478469263774" Y="-2.195948558675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.214725844508" Y="-0.955133327217" />
                  <Point X="-21.124988572089" Y="-0.532952653451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.898840889665" Y="0.53098854247" />
                  <Point X="-20.789311988037" Y="1.046281510923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.619372241816" Y="-2.401920188936" />
                  <Point X="-21.561972240179" Y="-2.131874412948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.30920435082" Y="-0.942694989961" />
                  <Point X="-21.216877445347" Y="-0.50833105056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.001828854587" Y="0.503393024532" />
                  <Point X="-20.88923042625" Y="1.033126980769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.705878592116" Y="-2.351975806466" />
                  <Point X="-21.645475216583" Y="-2.06780026722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.403682857132" Y="-0.930256652706" />
                  <Point X="-21.308758187114" Y="-0.483671192005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.104816819509" Y="0.475797506595" />
                  <Point X="-20.989148864462" Y="1.019972450614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.792384942416" Y="-2.302031423996" />
                  <Point X="-21.728978192988" Y="-2.003726121493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.498161363445" Y="-0.917818315451" />
                  <Point X="-21.397011938798" Y="-0.441947686703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.207804784432" Y="0.448201988658" />
                  <Point X="-21.089067302674" Y="1.006817920459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.878891292715" Y="-2.252087041526" />
                  <Point X="-21.812481169392" Y="-1.939651975765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.593230315777" Y="-0.908157808318" />
                  <Point X="-21.482773947827" Y="-0.388501453878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.31081518822" Y="0.420500904155" />
                  <Point X="-21.188985740886" Y="0.993663390304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.874110628135" Y="2.475034326481" />
                  <Point X="-20.869925203853" Y="2.494725199577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.338083186564" Y="-3.955490288603" />
                  <Point X="-22.329397897395" Y="-3.914629215669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.965397643015" Y="-2.202142659056" />
                  <Point X="-21.895984145796" Y="-1.875577830038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.693531331495" Y="-0.923112224127" />
                  <Point X="-21.561688960604" Y="-0.302842636329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.419430721068" Y="0.366429760714" />
                  <Point X="-21.288904179099" Y="0.980508860149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.990160940099" Y="2.385985297351" />
                  <Point X="-20.941800672769" Y="2.613502467133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.414137797624" Y="-3.856374339013" />
                  <Point X="-22.400385655574" Y="-3.791675597454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.051903993315" Y="-2.152198276586" />
                  <Point X="-21.978376174585" Y="-1.806277086707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.795358071548" Y="-0.945244608581" />
                  <Point X="-21.630379552054" Y="-0.169081698353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.536561057123" Y="0.27229961773" />
                  <Point X="-21.388822617311" Y="0.967354329995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.106211252063" Y="2.296936268222" />
                  <Point X="-21.016160930568" Y="2.720589722096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.490192408685" Y="-3.757258389423" />
                  <Point X="-22.471373413754" Y="-3.66872197924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.138410343614" Y="-2.102253894116" />
                  <Point X="-22.052589038935" Y="-1.698496400088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.900897392614" Y="-0.984843313449" />
                  <Point X="-21.488741055523" Y="0.95419979984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.222261564027" Y="2.207887239093" />
                  <Point X="-21.106168668334" Y="2.754061371667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.566247019746" Y="-3.658142439834" />
                  <Point X="-22.542361126447" Y="-3.54576814703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.227948205917" Y="-2.066571654294" />
                  <Point X="-22.123581498349" Y="-1.575564899444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.024668847819" Y="-1.110217465552" />
                  <Point X="-21.585502633636" Y="0.955897128762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.338311799852" Y="2.118838568167" />
                  <Point X="-21.216877122056" Y="2.690143809663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.642301630806" Y="-3.559026490244" />
                  <Point X="-22.613348727326" Y="-3.422813788773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.321466938435" Y="-2.049617936349" />
                  <Point X="-21.679462758089" Y="0.970774260917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.454361926875" Y="2.029790409117" />
                  <Point X="-21.327585575778" Y="2.62622624766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.718356214796" Y="-3.459910413294" />
                  <Point X="-22.684336328205" Y="-3.299859430515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.414998841059" Y="-2.032726178877" />
                  <Point X="-21.768536193031" Y="1.008641459687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.570412053898" Y="1.940742250068" />
                  <Point X="-21.438294029499" Y="2.562308685657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.79441077005" Y="-3.360794201155" />
                  <Point X="-22.755323929083" Y="-3.176905072257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.510904010454" Y="-2.026999763719" />
                  <Point X="-21.853754979039" Y="1.06464335589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.686462180921" Y="1.851694091018" />
                  <Point X="-21.549002483221" Y="2.498391123654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.870465325304" Y="-3.261677989016" />
                  <Point X="-22.826311529962" Y="-3.053950713999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.614881526638" Y="-2.059250754314" />
                  <Point X="-21.929874433524" Y="1.163454241154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.802512307943" Y="1.762645931969" />
                  <Point X="-21.659710931349" Y="2.434473587969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.946519880558" Y="-3.162561776877" />
                  <Point X="-22.89729913084" Y="-2.930996355742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.724237129479" Y="-2.11680365333" />
                  <Point X="-21.991112179242" Y="1.332278061564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.929054710151" Y="1.624235499166" />
                  <Point X="-21.770419377548" Y="2.370556061361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.022574435813" Y="-3.063445564738" />
                  <Point X="-22.961996916024" Y="-2.778450741182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.837323151185" Y="-2.191906793259" />
                  <Point X="-21.881127823746" Y="2.306638534753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.098628991067" Y="-2.964329352598" />
                  <Point X="-21.991836269944" Y="2.242721008144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.174940684852" Y="-2.866422882136" />
                  <Point X="-22.102544716143" Y="2.178803481536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.258048995999" Y="-2.800491982355" />
                  <Point X="-22.213253162341" Y="2.114885954928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.343494849421" Y="-2.745558354345" />
                  <Point X="-22.32396160854" Y="2.05096842832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.428940789452" Y="-2.690625133797" />
                  <Point X="-22.431646352141" Y="2.001276303995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.518236257975" Y="-2.653802520898" />
                  <Point X="-22.5337816311" Y="1.977692358113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.6142528676" Y="-2.648600390798" />
                  <Point X="-22.630312237854" Y="1.980476321843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.245017824726" Y="3.793144018859" />
                  <Point X="-22.216687347908" Y="3.926428433114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.713312804865" Y="-2.657715991547" />
                  <Point X="-22.723007892396" Y="2.001302317217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.398731317476" Y="3.526903655385" />
                  <Point X="-22.301056471079" Y="3.986427678681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.812372773591" Y="-2.66683174031" />
                  <Point X="-22.80939314542" Y="2.051816417576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.552444803593" Y="3.260663323116" />
                  <Point X="-22.385786922076" Y="4.044727010482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.199440532877" Y="-4.030917612305" />
                  <Point X="-24.189222964685" Y="-3.982847733344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.9159750219" Y="-2.697317234366" />
                  <Point X="-22.891016313614" Y="2.124734365612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.706158255973" Y="2.994423149569" />
                  <Point X="-22.472207362272" Y="4.095075568213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.253599819025" Y="-3.828792257874" />
                  <Point X="-24.226370169095" Y="-3.700686826942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.031804523347" Y="-2.785327431685" />
                  <Point X="-22.962837681867" Y="2.243766156775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.859871708353" Y="2.728182976022" />
                  <Point X="-22.558627802468" Y="4.145424125944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.307759105173" Y="-3.626666903444" />
                  <Point X="-24.263517373505" Y="-3.418525920541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.150479402918" Y="-2.886724080603" />
                  <Point X="-22.645048315188" Y="4.195772342477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.365103853225" Y="-3.439527969" />
                  <Point X="-22.731468839785" Y="4.246120503132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.438569426775" Y="-3.328231555578" />
                  <Point X="-22.819407736077" Y="4.289325286589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.512921403017" Y="-3.221105338956" />
                  <Point X="-22.907748493336" Y="4.330639462845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.590635531981" Y="-3.129796807264" />
                  <Point X="-22.99608924531" Y="4.371953663969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.677673448866" Y="-3.082353248956" />
                  <Point X="-23.084429990696" Y="4.413267896079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.768785197687" Y="-3.054075563033" />
                  <Point X="-23.173413607655" Y="4.451557655235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.859896945675" Y="-3.025797873195" />
                  <Point X="-23.263584198806" Y="4.484263139869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.951710466899" Y="-3.000821766853" />
                  <Point X="-23.353754789957" Y="4.516968624502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.049261001626" Y="-3.002836186974" />
                  <Point X="-23.443925326728" Y="4.549674364971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.152922092798" Y="-3.033598514931" />
                  <Point X="-23.534095861562" Y="4.582380114551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.256904081719" Y="-3.065870548102" />
                  <Point X="-23.626175572187" Y="4.606103898226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.362539874932" Y="-3.105923118743" />
                  <Point X="-23.718557140651" Y="4.628407552421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.488286382682" Y="-3.240589162512" />
                  <Point X="-23.810938709115" Y="4.650711206616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.632019412408" Y="-3.459875139137" />
                  <Point X="-23.903320264652" Y="4.673014921625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.006086071369" Y="-4.762795643086" />
                  <Point X="-23.995701785539" Y="4.695318799651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.098838917712" Y="-4.742238713981" />
                  <Point X="-24.088083306426" Y="4.717622677678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.133315174027" Y="-4.447511984755" />
                  <Point X="-24.180701998126" Y="4.738810754754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.180266844038" Y="-4.211477462427" />
                  <Point X="-24.275709465814" Y="4.748760524394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.248070055794" Y="-4.07354173122" />
                  <Point X="-24.370716933503" Y="4.758710294034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.329499404174" Y="-3.999711932656" />
                  <Point X="-24.465724401191" Y="4.768660063674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.411361924817" Y="-3.92792004936" />
                  <Point X="-24.560731832044" Y="4.778610006608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.493224405207" Y="-3.856127976686" />
                  <Point X="-24.77398924913" Y="4.232237503865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.580330456476" Y="-3.809004965453" />
                  <Point X="-24.897906087114" Y="4.106179379564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.674203696214" Y="-3.793719072848" />
                  <Point X="-24.999506181052" Y="4.08511328125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.769991573894" Y="-3.787440843558" />
                  <Point X="-25.091950897695" Y="4.107119846618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.86577944707" Y="-3.781162593072" />
                  <Point X="-25.175647199025" Y="4.17028447008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.961832247885" Y="-3.776130729134" />
                  <Point X="-25.241094890223" Y="4.319302054227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.064072303455" Y="-3.800207610215" />
                  <Point X="-25.295254002181" Y="4.521428228156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.175945828894" Y="-3.869606403697" />
                  <Point X="-25.34941317274" Y="4.723554126392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.28914561966" Y="-3.945244784969" />
                  <Point X="-25.435748684565" Y="4.774302240693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.405555058719" Y="-4.035983374244" />
                  <Point X="-25.535348716323" Y="4.762645695128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.526498770821" Y="-4.148054040999" />
                  <Point X="-25.634948767359" Y="4.750989058869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.612325401565" Y="-4.094911829442" />
                  <Point X="-25.734548831659" Y="4.73933236021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.698152032309" Y="-4.041769617885" />
                  <Point X="-27.462701986939" Y="-2.934064245158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.361432383241" Y="-2.457628218427" />
                  <Point X="-25.834148895959" Y="4.72767566155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.782976548986" Y="-3.983912830314" />
                  <Point X="-27.616415088883" Y="-3.200302770038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.442836553414" Y="-2.383679965708" />
                  <Point X="-25.933844170147" Y="4.715571035582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.866439340798" Y="-3.919649630944" />
                  <Point X="-27.77012838932" Y="-3.466542228748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.532891137106" Y="-2.350428708892" />
                  <Point X="-26.037121151818" Y="4.686615800748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.949902089048" Y="-3.85538622663" />
                  <Point X="-27.923841889766" Y="-3.732782628431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.631609493734" Y="-2.357937299094" />
                  <Point X="-26.140398133489" Y="4.657660565914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.739699170904" Y="-2.409534486079" />
                  <Point X="-26.243675115159" Y="4.62870533108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.850407621047" Y="-2.473452031246" />
                  <Point X="-26.346952108722" Y="4.599750040302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.96111607119" Y="-2.537369576413" />
                  <Point X="-26.533513118889" Y="4.178974257364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.468820481674" Y="4.483329186266" />
                  <Point X="-26.450229131445" Y="4.570794612329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.071824521334" Y="-2.60128712158" />
                  <Point X="-26.650797923256" Y="4.084117398104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.182532971477" Y="-2.665204666747" />
                  <Point X="-26.756979751669" Y="4.041495933825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.29324142162" Y="-2.729122211914" />
                  <Point X="-28.047583355695" Y="-1.573391878326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.976760027553" Y="-1.240194316294" />
                  <Point X="-26.856520216014" Y="4.030119630908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.403949873313" Y="-2.793039764373" />
                  <Point X="-28.163652914553" Y="-1.662531456972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.055446335197" Y="-1.153459525693" />
                  <Point X="-26.947789248613" Y="4.05765735483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.514658328011" Y="-2.856957330968" />
                  <Point X="-28.279703150214" Y="-1.751580127126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.143479662695" Y="-1.110699006125" />
                  <Point X="-27.0352356777" Y="4.103179014331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.625366782709" Y="-2.920874897563" />
                  <Point X="-28.395753385876" Y="-1.84062879728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.238308158721" Y="-1.099907241015" />
                  <Point X="-27.117910746155" Y="4.171148160724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.736075237407" Y="-2.984792464158" />
                  <Point X="-28.511803621537" Y="-1.929677467434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.33811656725" Y="-1.11254412221" />
                  <Point X="-27.194632692668" Y="4.267124543854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.829911196501" Y="-2.969331179914" />
                  <Point X="-28.627853857198" Y="-2.018726137588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.438034997816" Y="-1.125698616389" />
                  <Point X="-27.3047600367" Y="4.205940887995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.905832074204" Y="-2.86958606434" />
                  <Point X="-28.743904041746" Y="-2.107774567272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.537953428381" Y="-1.138853110569" />
                  <Point X="-28.333363737761" Y="-0.176334291989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.296176262351" Y="-0.001380975477" />
                  <Point X="-27.41488744933" Y="4.144756909404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.981752951906" Y="-2.769840948766" />
                  <Point X="-28.859954203045" Y="-2.196822887577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.637871858947" Y="-1.152007604749" />
                  <Point X="-28.454343003942" Y="-0.288572227536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.360388839504" Y="0.153447363392" />
                  <Point X="-27.772386547532" Y="2.919780650644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.748498123026" Y="3.032166851844" />
                  <Point X="-27.525014920242" Y="4.083572656626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.054621559207" Y="-2.655736029959" />
                  <Point X="-28.976004364344" Y="-2.285871207882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.737790289512" Y="-1.165162098929" />
                  <Point X="-28.559392163799" Y="-0.325864905224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.443432692486" Y="0.219681514994" />
                  <Point X="-27.900239415799" Y="2.775204959763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.799402072746" Y="3.249607360051" />
                  <Point X="-27.635142391153" Y="4.022388403847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.126223314477" Y="-2.535671040942" />
                  <Point X="-29.092054525642" Y="-2.374919528187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.837708720078" Y="-1.178316593109" />
                  <Point X="-28.662380143908" Y="-0.353460494607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.532797335076" Y="0.256178689493" />
                  <Point X="-28.007546325616" Y="2.727290403635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.87038970766" Y="3.372561558182" />
                  <Point X="-27.750464847119" Y="3.936763667961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.937627150643" Y="-1.191471087289" />
                  <Point X="-28.765368124017" Y="-0.381056083991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.624686216446" Y="0.280800254226" />
                  <Point X="-28.10478181371" Y="2.726758161388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.941377342575" Y="3.495515756313" />
                  <Point X="-27.866496288876" Y="3.847803416176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.037545581209" Y="-1.204625581468" />
                  <Point X="-28.868356104125" Y="-0.408651673374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.716575097816" Y="0.305421818958" />
                  <Point X="-28.503587786068" Y="1.307448338742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.438875003491" Y="1.611898044124" />
                  <Point X="-28.199027008925" Y="2.740294141056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.01236492017" Y="3.618470224112" />
                  <Point X="-27.98252781483" Y="3.758842768274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.137464014603" Y="-1.217780088957" />
                  <Point X="-28.971344084234" Y="-0.436247262758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.808463979185" Y="0.33004338369" />
                  <Point X="-28.616543901697" Y="1.232956358858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.509681160518" Y="1.735706028586" />
                  <Point X="-28.287374342696" Y="2.781577377256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.237382453195" Y="-1.230934620895" />
                  <Point X="-29.074332064343" Y="-0.463842852141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.900352860555" Y="0.354664948423" />
                  <Point X="-28.719348899394" Y="1.206221634036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.59177970817" Y="1.806387492108" />
                  <Point X="-28.373880688921" Y="2.831521778896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.337300891786" Y="-1.244089152833" />
                  <Point X="-29.177320044452" Y="-0.491438441524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.992241741925" Y="0.379286513155" />
                  <Point X="-28.815553108857" Y="1.210541176288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.675282653003" Y="1.87046178637" />
                  <Point X="-28.460387035146" Y="2.881466180536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.437219330377" Y="-1.257243684771" />
                  <Point X="-29.280308016614" Y="-0.51903399352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.084130623295" Y="0.403908077887" />
                  <Point X="-28.910031622524" Y="1.222979478942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.758785597835" Y="1.934536080632" />
                  <Point X="-28.546893381371" Y="2.931410582176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.537137768969" Y="-1.270398216709" />
                  <Point X="-29.383295985078" Y="-0.546629528122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.176019504664" Y="0.428529642619" />
                  <Point X="-29.004510136191" Y="1.235417781595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.842288542668" Y="1.998610374893" />
                  <Point X="-28.633399655864" Y="2.981355321287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.63705620756" Y="-1.283552748647" />
                  <Point X="-29.486283953543" Y="-0.574225062724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.267908386034" Y="0.453151207352" />
                  <Point X="-29.098988649859" Y="1.247856084248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.9257914875" Y="2.062684669155" />
                  <Point X="-28.719905913824" Y="3.031300138182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.701268529272" Y="-1.128723208023" />
                  <Point X="-29.589271922007" Y="-0.601820597326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.359797267204" Y="0.477772773024" />
                  <Point X="-29.193467163526" Y="1.260294386902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.009294441927" Y="2.126758918279" />
                  <Point X="-28.850034076585" Y="2.876020028312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.750722460902" Y="-0.904460901052" />
                  <Point X="-29.692259890472" Y="-0.629416131927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.45168614831" Y="0.502394338998" />
                  <Point X="-29.287945676888" Y="1.272732690992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.092797435563" Y="2.190832982937" />
                  <Point X="-28.983667008294" Y="2.704251276928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.543575029415" Y="0.527015904973" />
                  <Point X="-29.382424186349" Y="1.285171013431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.1763004292" Y="2.254907047594" />
                  <Point X="-29.133750275146" Y="2.455089783519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.635463910521" Y="0.551637470948" />
                  <Point X="-29.476902695811" Y="1.297609335871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.727352791627" Y="0.576259036922" />
                  <Point X="-29.571381205272" Y="1.31004765831" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.3390078125" Y="-4.244154296875" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521543945312" />
                  <Point X="-24.61311328125" Y="-3.409976806641" />
                  <Point X="-24.699408203125" Y="-3.285643554688" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.84548828125" Y="-3.2292109375" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.128486328125" Y="-3.224955322266" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643798828" />
                  <Point X="-25.3657109375" Y="-3.397210693359" />
                  <Point X="-25.452005859375" Y="-3.521544189453" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.670357421875" Y="-4.325060058594" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.943740234375" Y="-4.968443359375" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.2386328125" Y="-4.902459472656" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.331955078125" Y="-4.724263671875" />
                  <Point X="-26.3091484375" Y="-4.5510390625" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.33830859375" Y="-4.390845214844" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.4966015625" Y="-4.105881835938" />
                  <Point X="-26.61954296875" Y="-3.998064208984" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.795658203125" Y="-3.976166259766" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791015625" />
                  <Point X="-27.111880859375" Y="-4.055310791016" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.378265625" Y="-4.31176953125" />
                  <Point X="-27.45709375" Y="-4.414500488281" />
                  <Point X="-27.6264296875" Y="-4.30965234375" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.0474609375" Y="-4.020064208984" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.858107421875" Y="-3.238929199219" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597594970703" />
                  <Point X="-27.5139765625" Y="-2.568766601562" />
                  <Point X="-27.53132421875" Y="-2.55141796875" />
                  <Point X="-27.560154296875" Y="-2.537198974609" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.271271484375" Y="-2.935831054688" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-28.980462890625" Y="-3.085243408203" />
                  <Point X="-29.161697265625" Y="-2.847136962891" />
                  <Point X="-29.299083984375" Y="-2.616760986328" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-28.77980078125" Y="-1.895829711914" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.396012939453" />
                  <Point X="-28.1381171875" Y="-1.366265625" />
                  <Point X="-28.140326171875" Y="-1.334596069336" />
                  <Point X="-28.16115625" Y="-1.310640014648" />
                  <Point X="-28.187638671875" Y="-1.295053466797" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.0774453125" Y="-1.401518066406" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.856505859375" Y="-1.288702758789" />
                  <Point X="-29.927392578125" Y="-1.0111875" />
                  <Point X="-29.963740234375" Y="-0.757051818848" />
                  <Point X="-29.99839453125" Y="-0.514742248535" />
                  <Point X="-29.2591953125" Y="-0.316674346924" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.528771484375" Y="-0.112316635132" />
                  <Point X="-28.502322265625" Y="-0.090645294189" />
                  <Point X="-28.490521484375" Y="-0.061808555603" />
                  <Point X="-28.483400390625" Y="-0.031280040741" />
                  <Point X="-28.490021484375" Y="-0.00236456275" />
                  <Point X="-28.502322265625" Y="0.028083959579" />
                  <Point X="-28.527267578125" Y="0.048712276459" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.339494140625" Y="0.275630187988" />
                  <Point X="-29.998185546875" Y="0.452126037598" />
                  <Point X="-29.963328125" Y="0.687690002441" />
                  <Point X="-29.91764453125" Y="0.996414855957" />
                  <Point X="-29.8444765625" Y="1.266428100586" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.25628515625" Y="1.460204101562" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866699219" />
                  <Point X="-28.70265234375" Y="1.405026367188" />
                  <Point X="-28.67027734375" Y="1.41523425293" />
                  <Point X="-28.651533203125" Y="1.426056396484" />
                  <Point X="-28.639119140625" Y="1.443786376953" />
                  <Point X="-28.627462890625" Y="1.471928466797" />
                  <Point X="-28.61447265625" Y="1.503290527344" />
                  <Point X="-28.610712890625" Y="1.524606323242" />
                  <Point X="-28.61631640625" Y="1.545513061523" />
                  <Point X="-28.630380859375" Y="1.572532104492" />
                  <Point X="-28.646056640625" Y="1.602642822266" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-29.10854296875" Y="1.963426025391" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.337525390625" Y="2.482886230469" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-28.966197265625" Y="3.036130859375" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.46809765625" Y="3.105310546875" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.098052734375" Y="2.916895019531" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775146484" />
                  <Point X="-28.013251953125" Y="2.927404052734" />
                  <Point X="-27.984533203125" Y="2.956122558594" />
                  <Point X="-27.95252734375" Y="2.988127441406" />
                  <Point X="-27.9408984375" Y="3.006381347656" />
                  <Point X="-27.938072265625" Y="3.027839599609" />
                  <Point X="-27.94161328125" Y="3.068299560547" />
                  <Point X="-27.945556640625" Y="3.113389160156" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.15084375" Y="3.478325439453" />
                  <Point X="-28.30727734375" Y="3.749276611328" />
                  <Point X="-28.062044921875" Y="3.937294921875" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.447619140625" Y="4.343924804688" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.06240234375" Y="4.410827148438" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.90621484375" Y="4.25021875" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.766904296875" Y="4.241678710938" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.69690234375" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.29448828125" />
                  <Point X="-26.67081640625" Y="4.34290625" />
                  <Point X="-26.653802734375" Y="4.396864746094" />
                  <Point X="-26.651916015625" Y="4.418426269531" />
                  <Point X="-26.674515625" Y="4.590081054688" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.368330078125" Y="4.791083496094" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.59803125" Y="4.946606445312" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.133533203125" Y="4.651987792969" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.8436953125" Y="4.691017578125" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.489259765625" Y="4.9621640625" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.833630859375" Y="4.8516484375" />
                  <Point X="-23.491544921875" Y="4.76905859375" />
                  <Point X="-23.2920703125" Y="4.696707519531" />
                  <Point X="-23.06896484375" Y="4.615785644531" />
                  <Point X="-22.87626953125" Y="4.52566796875" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.47514453125" Y="4.316680175781" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-22.091908203125" Y="4.070837890625" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.362646484375" Y="3.209406982422" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491515380859" />
                  <Point X="-22.78530078125" Y="2.453544677734" />
                  <Point X="-22.7966171875" Y="2.411229003906" />
                  <Point X="-22.797955078125" Y="2.392325927734" />
                  <Point X="-22.79399609375" Y="2.3594921875" />
                  <Point X="-22.789583984375" Y="2.322901123047" />
                  <Point X="-22.78131640625" Y="2.300811279297" />
                  <Point X="-22.761" Y="2.270870117188" />
                  <Point X="-22.738357421875" Y="2.237502685547" />
                  <Point X="-22.72505859375" Y="2.224203857422" />
                  <Point X="-22.6951171875" Y="2.203887451172" />
                  <Point X="-22.66175" Y="2.181246337891" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.606828125" Y="2.169020996094" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5513359375" Y="2.165946289062" />
                  <Point X="-22.513365234375" Y="2.176100341797" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.672390625" Y="2.646546142578" />
                  <Point X="-21.005751953125" Y="3.031430664062" />
                  <Point X="-20.92058984375" Y="2.913075927734" />
                  <Point X="-20.79740234375" Y="2.741874267578" />
                  <Point X="-20.69953125" Y="2.580139404297" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.17487109375" Y="2.004761962891" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583832275391" />
                  <Point X="-21.74795703125" Y="1.548181396484" />
                  <Point X="-21.77841015625" Y="1.508450927734" />
                  <Point X="-21.78687890625" Y="1.491501464844" />
                  <Point X="-21.79705859375" Y="1.455101806641" />
                  <Point X="-21.808404296875" Y="1.414536987305" />
                  <Point X="-21.809220703125" Y="1.390965454102" />
                  <Point X="-21.80086328125" Y="1.350466186523" />
                  <Point X="-21.79155078125" Y="1.305332763672" />
                  <Point X="-21.784353515625" Y="1.287955810547" />
                  <Point X="-21.761625" Y="1.253409667969" />
                  <Point X="-21.736296875" Y="1.214910400391" />
                  <Point X="-21.719052734375" Y="1.19882019043" />
                  <Point X="-21.686115234375" Y="1.180279541016" />
                  <Point X="-21.64941015625" Y="1.159617675781" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.586900390625" Y="1.147734008789" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.77700390625" Y="1.239541625977" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.113716796875" Y="1.168707763672" />
                  <Point X="-20.06080859375" Y="0.951367858887" />
                  <Point X="-20.029966796875" Y="0.753282409668" />
                  <Point X="-20.002140625" Y="0.574556396484" />
                  <Point X="-20.643978515625" Y="0.40257623291" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819198608" />
                  <Point X="-21.3146640625" Y="0.207530014038" />
                  <Point X="-21.363421875" Y="0.179346832275" />
                  <Point X="-21.377734375" Y="0.166925933838" />
                  <Point X="-21.403986328125" Y="0.133475875854" />
                  <Point X="-21.433240234375" Y="0.096198257446" />
                  <Point X="-21.443013671875" Y="0.074735412598" />
                  <Point X="-21.451763671875" Y="0.029044403076" />
                  <Point X="-21.461515625" Y="-0.021875146866" />
                  <Point X="-21.461515625" Y="-0.040684932709" />
                  <Point X="-21.452765625" Y="-0.086376091003" />
                  <Point X="-21.443013671875" Y="-0.13729548645" />
                  <Point X="-21.433240234375" Y="-0.15875970459" />
                  <Point X="-21.406990234375" Y="-0.192209609985" />
                  <Point X="-21.377734375" Y="-0.229487380981" />
                  <Point X="-21.363421875" Y="-0.241906906128" />
                  <Point X="-21.319669921875" Y="-0.267196228027" />
                  <Point X="-21.270912109375" Y="-0.295379272461" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.573623046875" Y="-0.483988372803" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.02202734375" Y="-0.770463928223" />
                  <Point X="-20.051568359375" Y="-0.966412719727" />
                  <Point X="-20.09108203125" Y="-1.139562988281" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-20.8749609375" Y="-1.19150378418" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.69103125" Y="-1.117005249023" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.814552734375" Y="-1.154697143555" />
                  <Point X="-21.866455078125" Y="-1.217119750977" />
                  <Point X="-21.924296875" Y="-1.286685180664" />
                  <Point X="-21.935640625" Y="-1.314071166992" />
                  <Point X="-21.943080078125" Y="-1.394911010742" />
                  <Point X="-21.951369140625" Y="-1.485001464844" />
                  <Point X="-21.943638671875" Y="-1.516621826172" />
                  <Point X="-21.8961171875" Y="-1.590538085938" />
                  <Point X="-21.843158203125" Y="-1.672912353516" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-21.195658203125" Y="-2.173467773437" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.712595703125" Y="-2.667395019531" />
                  <Point X="-20.795865234375" Y="-2.802138183594" />
                  <Point X="-20.877587890625" Y="-2.918255615234" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.611470703125" Y="-2.625875244141" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.36485546875" Y="-2.234855957031" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219245117188" />
                  <Point X="-22.59582421875" Y="-2.263927978516" />
                  <Point X="-22.690439453125" Y="-2.313724121094" />
                  <Point X="-22.7113984375" Y="-2.334683349609" />
                  <Point X="-22.75608203125" Y="-2.419584716797" />
                  <Point X="-22.80587890625" Y="-2.514201416016" />
                  <Point X="-22.8108359375" Y="-2.546375" />
                  <Point X="-22.79237890625" Y="-2.648572998047" />
                  <Point X="-22.77180859375" Y="-2.762465332031" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.357291015625" Y="-3.486322265625" />
                  <Point X="-22.01332421875" Y="-4.082087646484" />
                  <Point X="-22.065892578125" Y="-4.119634765625" />
                  <Point X="-22.16470703125" Y="-4.190215820312" />
                  <Point X="-22.2560703125" Y="-4.249354003906" />
                  <Point X="-22.32022265625" Y="-4.29087890625" />
                  <Point X="-22.832201171875" Y="-3.623655273438" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.430244140625" Y="-2.915665527344" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.684431640625" Y="-2.845861083984" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.919787109375" Y="-2.939284912109" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045986328125" />
                  <Point X="-24.056994140625" Y="-3.163080810547" />
                  <Point X="-24.085357421875" Y="-3.29357421875" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-23.970271484375" Y="-4.190298339844" />
                  <Point X="-23.872357421875" Y="-4.934028320312" />
                  <Point X="-23.912171875" Y="-4.942755859375" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.0900625" Y="-4.978581054688" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#164" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.091393266905" Y="4.696208806539" Z="1.2" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.2" />
                  <Point X="-0.610000441139" Y="5.027547843578" Z="1.2" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.2" />
                  <Point X="-1.387985882063" Y="4.870507317709" Z="1.2" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.2" />
                  <Point X="-1.730244645039" Y="4.614835297266" Z="1.2" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.2" />
                  <Point X="-1.724658965457" Y="4.389222120976" Z="1.2" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.2" />
                  <Point X="-1.792195649252" Y="4.319152766711" Z="1.2" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.2" />
                  <Point X="-1.889283585606" Y="4.32584918069" Z="1.2" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.2" />
                  <Point X="-2.028891352471" Y="4.472545417174" Z="1.2" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.2" />
                  <Point X="-2.478059383287" Y="4.418912438217" Z="1.2" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.2" />
                  <Point X="-3.098622815166" Y="4.008254975894" Z="1.2" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.2" />
                  <Point X="-3.200302147543" Y="3.484605551556" Z="1.2" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.2" />
                  <Point X="-2.997579821421" Y="3.09522334871" Z="1.2" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.2" />
                  <Point X="-3.026044905214" Y="3.022758662673" Z="1.2" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.2" />
                  <Point X="-3.099853116897" Y="2.997984877752" Z="1.2" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.2" />
                  <Point X="-3.449253796741" Y="3.179891801203" Z="1.2" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.2" />
                  <Point X="-4.011816884616" Y="3.09811334553" Z="1.2" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.2" />
                  <Point X="-4.386864304291" Y="2.539371323255" Z="1.2" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.2" />
                  <Point X="-4.145137956612" Y="1.955038298069" Z="1.2" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.2" />
                  <Point X="-3.68088762962" Y="1.580723374949" Z="1.2" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.2" />
                  <Point X="-3.679813107807" Y="1.522342116798" Z="1.2" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.2" />
                  <Point X="-3.723845122607" Y="1.483992254453" Z="1.2" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.2" />
                  <Point X="-4.255915984194" Y="1.541056382777" Z="1.2" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.2" />
                  <Point X="-4.898893606838" Y="1.310785342457" Z="1.2" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.2" />
                  <Point X="-5.018946478795" Y="0.726288997638" Z="1.2" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.2" />
                  <Point X="-4.358593198422" Y="0.258614002137" Z="1.2" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.2" />
                  <Point X="-3.56193333115" Y="0.038916777992" Z="1.2" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.2" />
                  <Point X="-3.543931974129" Y="0.01409692781" Z="1.2" />
                  <Point X="-3.539556741714" Y="0" Z="1.2" />
                  <Point X="-3.544432627006" Y="-0.015710023251" Z="1.2" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.2" />
                  <Point X="-3.563435263974" Y="-0.039959205142" Z="1.2" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.2" />
                  <Point X="-4.27829459035" Y="-0.237098054619" Z="1.2" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.2" />
                  <Point X="-5.019392838714" Y="-0.732850733887" Z="1.2" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.2" />
                  <Point X="-4.911109031045" Y="-1.269796938582" Z="1.2" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.2" />
                  <Point X="-4.077076320865" Y="-1.419810259065" Z="1.2" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.2" />
                  <Point X="-3.205201003356" Y="-1.31507831138" Z="1.2" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.2" />
                  <Point X="-3.1967370303" Y="-1.338128422235" Z="1.2" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.2" />
                  <Point X="-3.816395755522" Y="-1.824882049022" Z="1.2" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.2" />
                  <Point X="-4.348184671138" Y="-2.611090718426" Z="1.2" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.2" />
                  <Point X="-4.026404252384" Y="-3.084246546519" Z="1.2" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.2" />
                  <Point X="-3.252429111138" Y="-2.947852210946" Z="1.2" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.2" />
                  <Point X="-2.563695799916" Y="-2.564634669216" Z="1.2" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.2" />
                  <Point X="-2.907564802223" Y="-3.182649271355" Z="1.2" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.2" />
                  <Point X="-3.084121435472" Y="-4.028401243637" Z="1.2" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.2" />
                  <Point X="-2.658908030106" Y="-4.320882989583" Z="1.2" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.2" />
                  <Point X="-2.344755472855" Y="-4.310927593729" Z="1.2" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.2" />
                  <Point X="-2.090258814738" Y="-4.065604164473" Z="1.2" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.2" />
                  <Point X="-1.805084235309" Y="-3.994779221658" Z="1.2" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.2" />
                  <Point X="-1.535724597064" Y="-4.112193542852" Z="1.2" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.2" />
                  <Point X="-1.393504442203" Y="-4.369320212965" Z="1.2" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.2" />
                  <Point X="-1.387683991658" Y="-4.686456795539" Z="1.2" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.2" />
                  <Point X="-1.257249159347" Y="-4.919602072349" Z="1.2" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.2" />
                  <Point X="-0.959424449165" Y="-4.986247540239" Z="1.2" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.2" />
                  <Point X="-0.628216778841" Y="-4.306721017647" Z="1.2" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.2" />
                  <Point X="-0.330792614" Y="-3.394439637576" Z="1.2" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.2" />
                  <Point X="-0.119823884185" Y="-3.241428280176" Z="1.2" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.2" />
                  <Point X="0.133535195176" Y="-3.245683765113" Z="1.2" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.2" />
                  <Point X="0.339653245432" Y="-3.407206118192" Z="1.2" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.2" />
                  <Point X="0.60653808156" Y="-4.225815012168" Z="1.2" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.2" />
                  <Point X="0.912719004389" Y="-4.996495667003" Z="1.2" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.2" />
                  <Point X="1.092376294781" Y="-4.960316374327" Z="1.2" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.2" />
                  <Point X="1.073144446638" Y="-4.152491759599" Z="1.2" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.2" />
                  <Point X="0.985709061783" Y="-3.142420367028" Z="1.2" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.2" />
                  <Point X="1.106022072133" Y="-2.946451324639" Z="1.2" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.2" />
                  <Point X="1.313994208098" Y="-2.864370629039" Z="1.2" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.2" />
                  <Point X="1.536559094141" Y="-2.926443524547" Z="1.2" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.2" />
                  <Point X="2.121973073029" Y="-3.622813147153" Z="1.2" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.2" />
                  <Point X="2.76494277905" Y="-4.260047951547" Z="1.2" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.2" />
                  <Point X="2.95701371907" Y="-4.12904194434" Z="1.2" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.2" />
                  <Point X="2.679852972731" Y="-3.430042130731" Z="1.2" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.2" />
                  <Point X="2.250668004195" Y="-2.608406200143" Z="1.2" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.2" />
                  <Point X="2.282007169055" Y="-2.411591603426" Z="1.2" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.2" />
                  <Point X="2.421306774947" Y="-2.276894140912" Z="1.2" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.2" />
                  <Point X="2.62010060337" Y="-2.252780005296" Z="1.2" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.2" />
                  <Point X="3.357371466376" Y="-2.637896527544" Z="1.2" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.2" />
                  <Point X="4.157143320125" Y="-2.915753001448" Z="1.2" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.2" />
                  <Point X="4.323780946071" Y="-2.662400081705" Z="1.2" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.2" />
                  <Point X="3.828621499006" Y="-2.102519962348" Z="1.2" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.2" />
                  <Point X="3.139783763768" Y="-1.532218690455" Z="1.2" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.2" />
                  <Point X="3.100552592162" Y="-1.368212154387" Z="1.2" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.2" />
                  <Point X="3.165833063079" Y="-1.217806736262" Z="1.2" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.2" />
                  <Point X="3.313430652426" Y="-1.13458445196" Z="1.2" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.2" />
                  <Point X="4.1123561874" Y="-1.209796063798" Z="1.2" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.2" />
                  <Point X="4.951507204722" Y="-1.119406679343" Z="1.2" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.2" />
                  <Point X="5.021257665657" Y="-0.74663782012" Z="1.2" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.2" />
                  <Point X="4.433162360054" Y="-0.404412003595" Z="1.2" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.2" />
                  <Point X="3.69919447505" Y="-0.192627509083" Z="1.2" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.2" />
                  <Point X="3.626187779925" Y="-0.130060545702" Z="1.2" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.2" />
                  <Point X="3.590185078787" Y="-0.045691076622" Z="1.2" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.2" />
                  <Point X="3.591186371637" Y="0.050919454591" Z="1.2" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.2" />
                  <Point X="3.629191658476" Y="0.133888192873" Z="1.2" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.2" />
                  <Point X="3.704200939302" Y="0.195521335159" Z="1.2" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.2" />
                  <Point X="4.362805457252" Y="0.385559929173" Z="1.2" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.2" />
                  <Point X="5.013280749392" Y="0.792254473118" Z="1.2" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.2" />
                  <Point X="4.928707351377" Y="1.211814454058" Z="1.2" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.2" />
                  <Point X="4.21031407935" Y="1.320393893965" Z="1.2" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.2" />
                  <Point X="3.413493115099" Y="1.228583067434" Z="1.2" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.2" />
                  <Point X="3.332255137461" Y="1.255130586169" Z="1.2" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.2" />
                  <Point X="3.273989382221" Y="1.312170255735" Z="1.2" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.2" />
                  <Point X="3.241948409321" Y="1.391849946565" Z="1.2" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.2" />
                  <Point X="3.244936427292" Y="1.472914039836" Z="1.2" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.2" />
                  <Point X="3.285570603047" Y="1.549044150558" Z="1.2" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.2" />
                  <Point X="3.849408863311" Y="1.996374229352" Z="1.2" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.2" />
                  <Point X="4.337089124742" Y="2.637305593656" Z="1.2" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.2" />
                  <Point X="4.113838721774" Y="2.973559088544" Z="1.2" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.2" />
                  <Point X="3.296452010782" Y="2.721127504912" Z="1.2" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.2" />
                  <Point X="2.467562318056" Y="2.255683024579" Z="1.2" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.2" />
                  <Point X="2.393000651089" Y="2.249941372226" Z="1.2" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.2" />
                  <Point X="2.326799356609" Y="2.276541690372" Z="1.2" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.2" />
                  <Point X="2.274216909863" Y="2.330225503773" Z="1.2" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.2" />
                  <Point X="2.249488330533" Y="2.396757792743" Z="1.2" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.2" />
                  <Point X="2.256844918078" Y="2.471907257825" Z="1.2" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.2" />
                  <Point X="2.674497847948" Y="3.215687071173" Z="1.2" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.2" />
                  <Point X="2.930911585237" Y="4.142865146612" Z="1.2" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.2" />
                  <Point X="2.543867945859" Y="4.391162862626" Z="1.2" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.2" />
                  <Point X="2.1387559174" Y="4.60224022644" Z="1.2" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.2" />
                  <Point X="1.718822249809" Y="4.774991410211" Z="1.2" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.2" />
                  <Point X="1.171946662854" Y="4.931532169344" Z="1.2" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.2" />
                  <Point X="0.509790609812" Y="5.043172206907" Z="1.2" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.2" />
                  <Point X="0.101851294648" Y="4.735238699412" Z="1.2" />
                  <Point X="0" Y="4.355124473572" Z="1.2" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>