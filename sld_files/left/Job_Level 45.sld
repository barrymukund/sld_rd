<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#205" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3218" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.08953515625" Y="-4.808145507812" />
                  <Point X="-24.4366953125" Y="-3.512524902344" />
                  <Point X="-24.44227734375" Y="-3.497139160156" />
                  <Point X="-24.457634765625" Y="-3.467377197266" />
                  <Point X="-24.599529296875" Y="-3.262936035156" />
                  <Point X="-24.62136328125" Y="-3.231476806641" />
                  <Point X="-24.64324609375" Y="-3.209022460938" />
                  <Point X="-24.669501953125" Y="-3.189777832031" />
                  <Point X="-24.69750390625" Y="-3.175669189453" />
                  <Point X="-24.91707421875" Y="-3.107522216797" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9790234375" Y="-3.092766357422" />
                  <Point X="-25.0086640625" Y="-3.092766357422" />
                  <Point X="-25.03682421875" Y="-3.097035644531" />
                  <Point X="-25.25639453125" Y="-3.165182373047" />
                  <Point X="-25.29018359375" Y="-3.175668945312" />
                  <Point X="-25.318185546875" Y="-3.189777832031" />
                  <Point X="-25.34444140625" Y="-3.209022460938" />
                  <Point X="-25.36632421875" Y="-3.231477294922" />
                  <Point X="-25.508216796875" Y="-3.435918212891" />
                  <Point X="-25.53005078125" Y="-3.467377685547" />
                  <Point X="-25.5381875" Y="-3.481571533203" />
                  <Point X="-25.550990234375" Y="-3.512523925781" />
                  <Point X="-25.604412109375" Y="-3.711893066406" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.04364453125" Y="-4.852278808594" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.246416015625" Y="-4.802362304688" />
                  <Point X="-26.2149609375" Y="-4.563438964844" />
                  <Point X="-26.21419921875" Y="-4.547929199219" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.26896484375" Y="-4.252511230469" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.18296484375" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.525796875" Y="-3.953919677734" />
                  <Point X="-26.556904296875" Y="-3.926639404297" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.911330078125" Y="-3.873380859375" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.983419921875" Y="-3.873708984375" />
                  <Point X="-27.01446875" Y="-3.882029296875" />
                  <Point X="-27.042658203125" Y="-3.894802246094" />
                  <Point X="-27.26622265625" Y="-4.044183349609" />
                  <Point X="-27.300623046875" Y="-4.067170166016" />
                  <Point X="-27.31278515625" Y="-4.076822021484" />
                  <Point X="-27.335099609375" Y="-4.0994609375" />
                  <Point X="-27.365091796875" Y="-4.138546386719" />
                  <Point X="-27.480146484375" Y="-4.288489746094" />
                  <Point X="-27.749392578125" Y="-4.121779785156" />
                  <Point X="-27.801712890625" Y="-4.089384277344" />
                  <Point X="-28.10472265625" Y="-3.856077636719" />
                  <Point X="-28.068919921875" Y="-3.794066894531" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647654541016" />
                  <Point X="-27.406587890625" Y="-2.616128173828" />
                  <Point X="-27.40557421875" Y="-2.585194091797" />
                  <Point X="-27.41455859375" Y="-2.555575927734" />
                  <Point X="-27.428775390625" Y="-2.526747070312" />
                  <Point X="-27.446802734375" Y="-2.50158984375" />
                  <Point X="-27.4618125" Y="-2.486579589844" />
                  <Point X="-27.464125" Y="-2.484265625" />
                  <Point X="-27.4892890625" Y="-2.466226318359" />
                  <Point X="-27.51812109375" Y="-2.452002685547" />
                  <Point X="-27.5477421875" Y="-2.443013427734" />
                  <Point X="-27.5786796875" Y="-2.444023925781" />
                  <Point X="-27.6102109375" Y="-2.450294189453" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-27.811134765625" Y="-2.560473876953" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-29.04152734375" Y="-2.848162109375" />
                  <Point X="-29.082861328125" Y="-2.793859375" />
                  <Point X="-29.306142578125" Y="-2.419450927734" />
                  <Point X="-29.234771484375" Y="-2.364686279297" />
                  <Point X="-28.105955078125" Y="-1.498513549805" />
                  <Point X="-28.084576171875" Y="-1.475591308594" />
                  <Point X="-28.066611328125" Y="-1.448458740234" />
                  <Point X="-28.053857421875" Y="-1.419833374023" />
                  <Point X="-28.0471796875" Y="-1.394053100586" />
                  <Point X="-28.04615234375" Y="-1.39008605957" />
                  <Point X="-28.04334765625" Y="-1.359654418945" />
                  <Point X="-28.045556640625" Y="-1.327985839844" />
                  <Point X="-28.052556640625" Y="-1.298242797852" />
                  <Point X="-28.06863671875" Y="-1.272260620117" />
                  <Point X="-28.089466796875" Y="-1.248305053711" />
                  <Point X="-28.112970703125" Y="-1.228768188477" />
                  <Point X="-28.135921875" Y="-1.215260253906" />
                  <Point X="-28.139453125" Y="-1.213181518555" />
                  <Point X="-28.168712890625" Y="-1.201958496094" />
                  <Point X="-28.2006015625" Y="-1.195475585938" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-28.449" Y="-1.222962036133" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.81791015625" Y="-1.055944580078" />
                  <Point X="-29.83407421875" Y="-0.992653259277" />
                  <Point X="-29.892423828125" Y="-0.584698425293" />
                  <Point X="-29.818767578125" Y="-0.564962524414" />
                  <Point X="-28.532875" Y="-0.220408279419" />
                  <Point X="-28.517490234375" Y="-0.21482649231" />
                  <Point X="-28.48773046875" Y="-0.19947076416" />
                  <Point X="-28.463677734375" Y="-0.18277746582" />
                  <Point X="-28.4599765625" Y="-0.180208724976" />
                  <Point X="-28.437521484375" Y="-0.158324645996" />
                  <Point X="-28.41827734375" Y="-0.132069473267" />
                  <Point X="-28.40416796875" Y="-0.10406855011" />
                  <Point X="-28.396150390625" Y="-0.078236717224" />
                  <Point X="-28.394912109375" Y="-0.074247360229" />
                  <Point X="-28.390642578125" Y="-0.046068092346" />
                  <Point X="-28.3906484375" Y="-0.016439979553" />
                  <Point X="-28.39491796875" Y="0.011703926086" />
                  <Point X="-28.402931640625" Y="0.037521038055" />
                  <Point X="-28.404140625" Y="0.041423721313" />
                  <Point X="-28.418263671875" Y="0.069485961914" />
                  <Point X="-28.437513671875" Y="0.095755561829" />
                  <Point X="-28.4599765625" Y="0.117648742676" />
                  <Point X="-28.484029296875" Y="0.134341888428" />
                  <Point X="-28.495166015625" Y="0.140992736816" />
                  <Point X="-28.51443359375" Y="0.150781539917" />
                  <Point X="-28.532875" Y="0.157848297119" />
                  <Point X="-28.73074609375" Y="0.210867904663" />
                  <Point X="-29.891814453125" Y="0.521975463867" />
                  <Point X="-29.834904296875" Y="0.906575927734" />
                  <Point X="-29.82448828125" Y="0.976968688965" />
                  <Point X="-29.70355078125" Y="1.423267822266" />
                  <Point X="-29.68742578125" Y="1.421144897461" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744982421875" Y="1.299341796875" />
                  <Point X="-28.723421875" Y="1.301228271484" />
                  <Point X="-28.70313671875" Y="1.305263671875" />
                  <Point X="-28.64990234375" Y="1.322048461914" />
                  <Point X="-28.6417109375" Y="1.324631225586" />
                  <Point X="-28.62277734375" Y="1.332961303711" />
                  <Point X="-28.604033203125" Y="1.343783081055" />
                  <Point X="-28.5873515625" Y="1.356014160156" />
                  <Point X="-28.573712890625" Y="1.371566162109" />
                  <Point X="-28.561298828125" Y="1.389295288086" />
                  <Point X="-28.551349609375" Y="1.407431762695" />
                  <Point X="-28.529990234375" Y="1.459000610352" />
                  <Point X="-28.526703125" Y="1.466935791016" />
                  <Point X="-28.520916015625" Y="1.486788452148" />
                  <Point X="-28.51715625" Y="1.508105957031" />
                  <Point X="-28.515802734375" Y="1.528750732422" />
                  <Point X="-28.518951171875" Y="1.549198974609" />
                  <Point X="-28.5245546875" Y="1.570107421875" />
                  <Point X="-28.53205078125" Y="1.589380615234" />
                  <Point X="-28.55782421875" Y="1.638891601563" />
                  <Point X="-28.561791015625" Y="1.646510253906" />
                  <Point X="-28.573287109375" Y="1.663715454102" />
                  <Point X="-28.58719921875" Y="1.680292480469" />
                  <Point X="-28.60213671875" Y="1.694590332031" />
                  <Point X="-28.71563671875" Y="1.781682006836" />
                  <Point X="-29.351859375" Y="2.269873291016" />
                  <Point X="-29.12162890625" Y="2.664313232422" />
                  <Point X="-29.0811484375" Y="2.7336640625" />
                  <Point X="-28.75050390625" Y="3.158662353516" />
                  <Point X="-28.20665625" Y="2.844671142578" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.07265234375" Y="2.819309814453" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.99901171875" Y="2.826505126953" />
                  <Point X="-27.980458984375" Y="2.835655029297" />
                  <Point X="-27.962205078125" Y="2.847284912109" />
                  <Point X="-27.946078125" Y="2.860229248047" />
                  <Point X="-27.893451171875" Y="2.912854736328" />
                  <Point X="-27.885353515625" Y="2.920952636719" />
                  <Point X="-27.872404296875" Y="2.937085449219" />
                  <Point X="-27.860775390625" Y="2.955339355469" />
                  <Point X="-27.85162890625" Y="2.973888427734" />
                  <Point X="-27.8467109375" Y="2.993977783203" />
                  <Point X="-27.843884765625" Y="3.015436035156" />
                  <Point X="-27.84343359375" Y="3.036120361328" />
                  <Point X="-27.849919921875" Y="3.110261230469" />
                  <Point X="-27.85091796875" Y="3.121669921875" />
                  <Point X="-27.854955078125" Y="3.141962158203" />
                  <Point X="-27.86146484375" Y="3.162604736328" />
                  <Point X="-27.869794921875" Y="3.181534179688" />
                  <Point X="-27.920091796875" Y="3.268648193359" />
                  <Point X="-28.18333203125" Y="3.724595947266" />
                  <Point X="-27.771115234375" Y="4.040637939453" />
                  <Point X="-27.70062109375" Y="4.094686279297" />
                  <Point X="-27.167037109375" Y="4.391134277344" />
                  <Point X="-27.0431953125" Y="4.229740722656" />
                  <Point X="-27.028888671875" Y="4.214796875" />
                  <Point X="-27.012306640625" Y="4.200883789062" />
                  <Point X="-26.995111328125" Y="4.18939453125" />
                  <Point X="-26.91259375" Y="4.146437988281" />
                  <Point X="-26.89989453125" Y="4.139827636719" />
                  <Point X="-26.880615234375" Y="4.132330078125" />
                  <Point X="-26.8597109375" Y="4.126729003906" />
                  <Point X="-26.83926953125" Y="4.123582519531" />
                  <Point X="-26.818630859375" Y="4.124934570312" />
                  <Point X="-26.79731640625" Y="4.128691894531" />
                  <Point X="-26.777453125" Y="4.134481445312" />
                  <Point X="-26.691505859375" Y="4.170083007812" />
                  <Point X="-26.678279296875" Y="4.175561035156" />
                  <Point X="-26.660146484375" Y="4.185508789062" />
                  <Point X="-26.642416015625" Y="4.197923339844" />
                  <Point X="-26.62686328125" Y="4.2115625" />
                  <Point X="-26.614630859375" Y="4.228244628906" />
                  <Point X="-26.60380859375" Y="4.246989746094" />
                  <Point X="-26.595478515625" Y="4.265921386719" />
                  <Point X="-26.56750390625" Y="4.354645019531" />
                  <Point X="-26.56319921875" Y="4.368297851562" />
                  <Point X="-26.5591640625" Y="4.388583984375" />
                  <Point X="-26.55727734375" Y="4.410145507812" />
                  <Point X="-26.557728515625" Y="4.430828125" />
                  <Point X="-26.563447265625" Y="4.474260742188" />
                  <Point X="-26.584201171875" Y="4.631897460938" />
                  <Point X="-26.040888671875" Y="4.784223144531" />
                  <Point X="-25.949630859375" Y="4.80980859375" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.133904296875" Y="4.286314941406" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286314941406" />
                  <Point X="-24.82801171875" Y="4.382492675781" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.235646484375" Y="4.840084472656" />
                  <Point X="-24.15595703125" Y="4.83173828125" />
                  <Point X="-23.598375" Y="4.69712109375" />
                  <Point X="-23.51897265625" Y="4.677950195312" />
                  <Point X="-23.156236328125" Y="4.546384277344" />
                  <Point X="-23.105353515625" Y="4.527928710938" />
                  <Point X="-22.7544140625" Y="4.363805664063" />
                  <Point X="-22.705423828125" Y="4.340894042969" />
                  <Point X="-22.36637890625" Y="4.143366210938" />
                  <Point X="-22.31902734375" Y="4.115778320312" />
                  <Point X="-22.05673828125" Y="3.929255126953" />
                  <Point X="-22.1056953125" Y="3.844459228516" />
                  <Point X="-22.85241796875" Y="2.55109765625" />
                  <Point X="-22.857919921875" Y="2.539934814453" />
                  <Point X="-22.866921875" Y="2.516058349609" />
                  <Point X="-22.885529296875" Y="2.446479003906" />
                  <Point X="-22.8878125" Y="2.435280761719" />
                  <Point X="-22.892013671875" Y="2.405670166016" />
                  <Point X="-22.892271484375" Y="2.380950927734" />
                  <Point X="-22.885015625" Y="2.320784423828" />
                  <Point X="-22.883900390625" Y="2.311532714844" />
                  <Point X="-22.878560546875" Y="2.289619628906" />
                  <Point X="-22.870294921875" Y="2.267525634766" />
                  <Point X="-22.859927734375" Y="2.247470458984" />
                  <Point X="-22.822697265625" Y="2.192604492187" />
                  <Point X="-22.81557421875" Y="2.183380371094" />
                  <Point X="-22.79654296875" Y="2.161635742187" />
                  <Point X="-22.7783984375" Y="2.145591552734" />
                  <Point X="-22.723533203125" Y="2.108362548828" />
                  <Point X="-22.71508984375" Y="2.102634033203" />
                  <Point X="-22.69504296875" Y="2.092270263672" />
                  <Point X="-22.67295703125" Y="2.084005615234" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.590869140625" Y="2.071408447266" />
                  <Point X="-22.57875" Y="2.070728027344" />
                  <Point X="-22.550587890625" Y="2.07094921875" />
                  <Point X="-22.52679296875" Y="2.074170898438" />
                  <Point X="-22.457212890625" Y="2.09277734375" />
                  <Point X="-22.45076171875" Y="2.094750244141" />
                  <Point X="-22.42797265625" Y="2.102614990234" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-22.212443359375" Y="2.225050292969" />
                  <Point X="-21.032673828125" Y="2.906190429688" />
                  <Point X="-20.9048203125" Y="2.728503662109" />
                  <Point X="-20.87671875" Y="2.689450439453" />
                  <Point X="-20.73780078125" Y="2.459883544922" />
                  <Point X="-20.786083984375" Y="2.422833496094" />
                  <Point X="-21.76921484375" Y="1.668451171875" />
                  <Point X="-21.778576171875" Y="1.660239013672" />
                  <Point X="-21.79602734375" Y="1.641625854492" />
                  <Point X="-21.846103515625" Y="1.576297363281" />
                  <Point X="-21.852201171875" Y="1.567322387695" />
                  <Point X="-21.868375" Y="1.540322753906" />
                  <Point X="-21.878369140625" Y="1.517090209961" />
                  <Point X="-21.8970234375" Y="1.450389282227" />
                  <Point X="-21.89989453125" Y="1.440125610352" />
                  <Point X="-21.90334765625" Y="1.417827270508" />
                  <Point X="-21.9041640625" Y="1.394257202148" />
                  <Point X="-21.902259765625" Y="1.371769165039" />
                  <Point X="-21.8869453125" Y="1.297556274414" />
                  <Point X="-21.88407421875" Y="1.286845947266" />
                  <Point X="-21.874521484375" Y="1.258047485352" />
                  <Point X="-21.863716796875" Y="1.235742797852" />
                  <Point X="-21.822068359375" Y="1.172438476562" />
                  <Point X="-21.81566015625" Y="1.162697265625" />
                  <Point X="-21.801111328125" Y="1.145455200195" />
                  <Point X="-21.7838671875" Y="1.129363769531" />
                  <Point X="-21.76565234375" Y="1.116034912109" />
                  <Point X="-21.705296875" Y="1.082060302734" />
                  <Point X="-21.6948671875" Y="1.077000976562" />
                  <Point X="-21.667603515625" Y="1.065774902344" />
                  <Point X="-21.643880859375" Y="1.059438598633" />
                  <Point X="-21.56227734375" Y="1.048653442383" />
                  <Point X="-21.555978515625" Y="1.048033569336" />
                  <Point X="-21.53034375" Y="1.04637097168" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-21.32273828125" Y="1.071874389648" />
                  <Point X="-20.22316015625" Y="1.21663659668" />
                  <Point X="-20.16612890625" Y="0.982370300293" />
                  <Point X="-20.154060546875" Y="0.932791015625" />
                  <Point X="-20.109134765625" Y="0.644238464355" />
                  <Point X="-20.15673046875" Y="0.631485046387" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.2952109375" Y="0.325585601807" />
                  <Point X="-21.318451171875" Y="0.315068481445" />
                  <Point X="-21.398623046875" Y="0.268727050781" />
                  <Point X="-21.4072734375" Y="0.263078033447" />
                  <Point X="-21.43392578125" Y="0.243526351929" />
                  <Point X="-21.45246875" Y="0.225576843262" />
                  <Point X="-21.500572265625" Y="0.164281311035" />
                  <Point X="-21.507974609375" Y="0.154849212646" />
                  <Point X="-21.519701171875" Y="0.135564102173" />
                  <Point X="-21.529474609375" Y="0.114097755432" />
                  <Point X="-21.536318359375" Y="0.092602416992" />
                  <Point X="-21.552353515625" Y="0.008875617027" />
                  <Point X="-21.55376171875" Y="-0.001620838284" />
                  <Point X="-21.556228515625" Y="-0.033309505463" />
                  <Point X="-21.5548203125" Y="-0.058551937103" />
                  <Point X="-21.53878515625" Y="-0.142278747559" />
                  <Point X="-21.536318359375" Y="-0.155162399292" />
                  <Point X="-21.529474609375" Y="-0.176657745361" />
                  <Point X="-21.519701171875" Y="-0.198124084473" />
                  <Point X="-21.507974609375" Y="-0.217409194946" />
                  <Point X="-21.45987109375" Y="-0.278704864502" />
                  <Point X="-21.452525390625" Y="-0.287016448975" />
                  <Point X="-21.4308125" Y="-0.308867736816" />
                  <Point X="-21.41096484375" Y="-0.324154998779" />
                  <Point X="-21.330791015625" Y="-0.370496307373" />
                  <Point X="-21.325541015625" Y="-0.373314758301" />
                  <Point X="-21.301123046875" Y="-0.385454193115" />
                  <Point X="-21.283419921875" Y="-0.392149963379" />
                  <Point X="-21.110044921875" Y="-0.438605407715" />
                  <Point X="-20.108525390625" Y="-0.706961853027" />
                  <Point X="-20.13823828125" Y="-0.904042114258" />
                  <Point X="-20.144974609375" Y="-0.948725158691" />
                  <Point X="-20.19882421875" Y="-1.18469934082" />
                  <Point X="-20.2689765625" Y="-1.175463623047" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710266113" />
                  <Point X="-21.62533984375" Y="-1.005508666992" />
                  <Point X="-21.78269140625" Y="-1.039709960938" />
                  <Point X="-21.806904296875" Y="-1.04497265625" />
                  <Point X="-21.8360234375" Y="-1.056596557617" />
                  <Point X="-21.8638515625" Y="-1.073489379883" />
                  <Point X="-21.8876015625" Y="-1.093960571289" />
                  <Point X="-21.9827109375" Y="-1.208347045898" />
                  <Point X="-21.997345703125" Y="-1.225948608398" />
                  <Point X="-22.012068359375" Y="-1.250334350586" />
                  <Point X="-22.02341015625" Y="-1.27771875" />
                  <Point X="-22.030240234375" Y="-1.305365722656" />
                  <Point X="-22.04387109375" Y="-1.453501098633" />
                  <Point X="-22.04596875" Y="-1.476296020508" />
                  <Point X="-22.043650390625" Y="-1.50756628418" />
                  <Point X="-22.03591796875" Y="-1.539187744141" />
                  <Point X="-22.023546875" Y="-1.567996948242" />
                  <Point X="-21.936466796875" Y="-1.703444946289" />
                  <Point X="-21.92306640625" Y="-1.724287841797" />
                  <Point X="-21.913064453125" Y="-1.73723840332" />
                  <Point X="-21.889369140625" Y="-1.760909057617" />
                  <Point X="-21.728478515625" Y="-1.884365966797" />
                  <Point X="-20.786876953125" Y="-2.606882324219" />
                  <Point X="-20.85619921875" Y="-2.719056396484" />
                  <Point X="-20.875205078125" Y="-2.749808349609" />
                  <Point X="-20.971017578125" Y="-2.885945556641" />
                  <Point X="-21.0353046875" Y="-2.848829589844" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.245775390625" Y="-2.159825195312" />
                  <Point X="-22.433048828125" Y="-2.12600390625" />
                  <Point X="-22.461865234375" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395507812" />
                  <Point X="-22.525390625" Y="-2.125353515625" />
                  <Point X="-22.555166015625" Y="-2.135177001953" />
                  <Point X="-22.710744140625" Y="-2.217056396484" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.757615234375" Y="-2.246549804688" />
                  <Point X="-22.77857421875" Y="-2.267509277344" />
                  <Point X="-22.795466796875" Y="-2.290438720703" />
                  <Point X="-22.87734765625" Y="-2.446016601562" />
                  <Point X="-22.889947265625" Y="-2.469956787109" />
                  <Point X="-22.899771484375" Y="-2.499735839844" />
                  <Point X="-22.904728515625" Y="-2.531909912109" />
                  <Point X="-22.90432421875" Y="-2.563260009766" />
                  <Point X="-22.870501953125" Y="-2.750532958984" />
                  <Point X="-22.865296875" Y="-2.779350341797" />
                  <Point X="-22.86101171875" Y="-2.795143066406" />
                  <Point X="-22.8481796875" Y="-2.826078369141" />
                  <Point X="-22.744791015625" Y="-3.005154052734" />
                  <Point X="-22.13871484375" Y="-4.054906738281" />
                  <Point X="-22.195609375" Y="-4.095544921875" />
                  <Point X="-22.2181484375" Y="-4.111643554688" />
                  <Point X="-22.298232421875" Y="-4.163480957031" />
                  <Point X="-22.35341015625" Y="-4.091572998047" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922177490234" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.46277734375" Y="-2.781811035156" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.520009765625" Y="-2.751167236328" />
                  <Point X="-23.551630859375" Y="-2.743435546875" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.78490234375" Y="-2.759705322266" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397705078" />
                  <Point X="-23.871021484375" Y="-2.780740966797" />
                  <Point X="-23.89540234375" Y="-2.7954609375" />
                  <Point X="-24.051384765625" Y="-2.925154296875" />
                  <Point X="-24.07538671875" Y="-2.945111328125" />
                  <Point X="-24.095857421875" Y="-2.968859130859" />
                  <Point X="-24.11275" Y="-2.996684082031" />
                  <Point X="-24.124375" Y="-3.025807373047" />
                  <Point X="-24.17101171875" Y="-3.240377197266" />
                  <Point X="-24.178189453125" Y="-3.273395263672" />
                  <Point X="-24.180275390625" Y="-3.289626464844" />
                  <Point X="-24.1802578125" Y="-3.323120605469" />
                  <Point X="-24.15095703125" Y="-3.545674316406" />
                  <Point X="-23.977935546875" Y="-4.859915527344" />
                  <Point X="-24.00300390625" Y="-4.865410644531" />
                  <Point X="-24.024318359375" Y="-4.870083007812" />
                  <Point X="-24.070681640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.02554296875" Y="-4.75901953125" />
                  <Point X="-26.05842578125" Y="-4.75263671875" />
                  <Point X="-26.141244140625" Y="-4.731328125" />
                  <Point X="-26.1207734375" Y="-4.575838867188" />
                  <Point X="-26.120076171875" Y="-4.568099121094" />
                  <Point X="-26.11944921875" Y="-4.541030273438" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497688964844" />
                  <Point X="-26.175791015625" Y="-4.233977050781" />
                  <Point X="-26.18386328125" Y="-4.193397460938" />
                  <Point X="-26.188126953125" Y="-4.178467773438" />
                  <Point X="-26.199029296875" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135463867188" />
                  <Point X="-26.221740234375" Y="-4.107626464844" />
                  <Point X="-26.23057421875" Y="-4.094861083984" />
                  <Point X="-26.25020703125" Y="-4.070937744141" />
                  <Point X="-26.261005859375" Y="-4.059779541016" />
                  <Point X="-26.463158203125" Y="-3.882495117188" />
                  <Point X="-26.494265625" Y="-3.85521484375" />
                  <Point X="-26.50673828125" Y="-3.845965332031" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.9051171875" Y="-3.778584228516" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.992732421875" Y="-3.779166503906" />
                  <Point X="-27.008009765625" Y="-3.781946533203" />
                  <Point X="-27.03905859375" Y="-3.790266845703" />
                  <Point X="-27.053677734375" Y="-3.795497802734" />
                  <Point X="-27.0818671875" Y="-3.808270751953" />
                  <Point X="-27.0954375" Y="-3.815812744141" />
                  <Point X="-27.319001953125" Y="-3.965193847656" />
                  <Point X="-27.35340234375" Y="-3.988180664062" />
                  <Point X="-27.359677734375" Y="-3.992755859375" />
                  <Point X="-27.380443359375" Y="-4.010133544922" />
                  <Point X="-27.4027578125" Y="-4.032772460938" />
                  <Point X="-27.410466796875" Y="-4.041627441406" />
                  <Point X="-27.440458984375" Y="-4.080712890625" />
                  <Point X="-27.50319921875" Y="-4.162479003906" />
                  <Point X="-27.699380859375" Y="-4.041009033203" />
                  <Point X="-27.747591796875" Y="-4.011157958984" />
                  <Point X="-27.98086328125" Y="-3.831547363281" />
                  <Point X="-27.341490234375" Y="-2.724120361328" />
                  <Point X="-27.3348515625" Y="-2.710085205078" />
                  <Point X="-27.32394921875" Y="-2.681120117188" />
                  <Point X="-27.319685546875" Y="-2.666189453125" />
                  <Point X="-27.3134140625" Y="-2.634663085938" />
                  <Point X="-27.311638671875" Y="-2.619239501953" />
                  <Point X="-27.310625" Y="-2.588305419922" />
                  <Point X="-27.3146640625" Y="-2.557617675781" />
                  <Point X="-27.3236484375" Y="-2.527999511719" />
                  <Point X="-27.32935546875" Y="-2.51355859375" />
                  <Point X="-27.343572265625" Y="-2.484729736328" />
                  <Point X="-27.3515546875" Y="-2.471411865234" />
                  <Point X="-27.36958203125" Y="-2.446254638672" />
                  <Point X="-27.379626953125" Y="-2.434415771484" />
                  <Point X="-27.394615234375" Y="-2.419426513672" />
                  <Point X="-27.408775390625" Y="-2.407055419922" />
                  <Point X="-27.433939453125" Y="-2.389016113281" />
                  <Point X="-27.447259765625" Y="-2.381029541016" />
                  <Point X="-27.476091796875" Y="-2.366805908203" />
                  <Point X="-27.490533203125" Y="-2.361096679688" />
                  <Point X="-27.520154296875" Y="-2.352107421875" />
                  <Point X="-27.55084375" Y="-2.348063964844" />
                  <Point X="-27.58178125" Y="-2.349074462891" />
                  <Point X="-27.597208984375" Y="-2.350848388672" />
                  <Point X="-27.628740234375" Y="-2.357118652344" />
                  <Point X="-27.643671875" Y="-2.361382324219" />
                  <Point X="-27.672642578125" Y="-2.372285400391" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-27.858634765625" Y="-2.478201416016" />
                  <Point X="-28.793087890625" Y="-3.017708251953" />
                  <Point X="-28.96593359375" Y="-2.790624023438" />
                  <Point X="-29.004021484375" Y="-2.740584716797" />
                  <Point X="-29.181265625" Y="-2.443374755859" />
                  <Point X="-29.176939453125" Y="-2.440054931641" />
                  <Point X="-28.048123046875" Y="-1.573882324219" />
                  <Point X="-28.036482421875" Y="-1.563309204102" />
                  <Point X="-28.015103515625" Y="-1.540386962891" />
                  <Point X="-28.005365234375" Y="-1.528037841797" />
                  <Point X="-27.987400390625" Y="-1.500905273438" />
                  <Point X="-27.979833984375" Y="-1.487121704102" />
                  <Point X="-27.967080078125" Y="-1.458496337891" />
                  <Point X="-27.961892578125" Y="-1.443654541016" />
                  <Point X="-27.95521484375" Y="-1.417874267578" />
                  <Point X="-27.951552734375" Y="-1.3988046875" />
                  <Point X="-27.948748046875" Y="-1.368373046875" />
                  <Point X="-27.948578125" Y="-1.353043945312" />
                  <Point X="-27.950787109375" Y="-1.321375366211" />
                  <Point X="-27.953083984375" Y="-1.306222290039" />
                  <Point X="-27.960083984375" Y="-1.276479248047" />
                  <Point X="-27.971775390625" Y="-1.248248413086" />
                  <Point X="-27.98785546875" Y="-1.222266235352" />
                  <Point X="-27.996947265625" Y="-1.209925048828" />
                  <Point X="-28.01777734375" Y="-1.185969482422" />
                  <Point X="-28.028740234375" Y="-1.175248046875" />
                  <Point X="-28.052244140625" Y="-1.155711181641" />
                  <Point X="-28.06478515625" Y="-1.146895751953" />
                  <Point X="-28.087736328125" Y="-1.133387817383" />
                  <Point X="-28.105431640625" Y="-1.124482543945" />
                  <Point X="-28.13469140625" Y="-1.113259521484" />
                  <Point X="-28.149787109375" Y="-1.108862792969" />
                  <Point X="-28.18167578125" Y="-1.102379882812" />
                  <Point X="-28.19729296875" Y="-1.100533203125" />
                  <Point X="-28.228619140625" Y="-1.09944152832" />
                  <Point X="-28.244328125" Y="-1.100196655273" />
                  <Point X="-28.461400390625" Y="-1.128774780273" />
                  <Point X="-29.66091796875" Y="-1.286694213867" />
                  <Point X="-29.725865234375" Y="-1.032433227539" />
                  <Point X="-29.7407578125" Y="-0.974118225098" />
                  <Point X="-29.786451171875" Y="-0.654654785156" />
                  <Point X="-28.508287109375" Y="-0.312171325684" />
                  <Point X="-28.500474609375" Y="-0.309712219238" />
                  <Point X="-28.473927734375" Y="-0.299250213623" />
                  <Point X="-28.44416796875" Y="-0.283894500732" />
                  <Point X="-28.433564453125" Y="-0.277516052246" />
                  <Point X="-28.40951171875" Y="-0.260822723389" />
                  <Point X="-28.393671875" Y="-0.248243270874" />
                  <Point X="-28.371216796875" Y="-0.226359146118" />
                  <Point X="-28.360900390625" Y="-0.214485809326" />
                  <Point X="-28.34165625" Y="-0.188230682373" />
                  <Point X="-28.333439453125" Y="-0.174818511963" />
                  <Point X="-28.319330078125" Y="-0.146817657471" />
                  <Point X="-28.3134375" Y="-0.132228988647" />
                  <Point X="-28.305419921875" Y="-0.106397285461" />
                  <Point X="-28.300984375" Y="-0.088478713989" />
                  <Point X="-28.29671484375" Y="-0.060299362183" />
                  <Point X="-28.295642578125" Y="-0.046049251556" />
                  <Point X="-28.2956484375" Y="-0.016421127319" />
                  <Point X="-28.29672265625" Y="-0.002191226959" />
                  <Point X="-28.3009921875" Y="0.025952749252" />
                  <Point X="-28.3041875" Y="0.039866527557" />
                  <Point X="-28.312201171875" Y="0.065683662415" />
                  <Point X="-28.31928125" Y="0.08413117981" />
                  <Point X="-28.333404296875" Y="0.112193565369" />
                  <Point X="-28.341634765625" Y="0.125638290405" />
                  <Point X="-28.360884765625" Y="0.151907836914" />
                  <Point X="-28.37120703125" Y="0.163787857056" />
                  <Point X="-28.393669921875" Y="0.185681045532" />
                  <Point X="-28.405810546875" Y="0.195694366455" />
                  <Point X="-28.42986328125" Y="0.212387390137" />
                  <Point X="-28.45213671875" Y="0.225688995361" />
                  <Point X="-28.471404296875" Y="0.235477752686" />
                  <Point X="-28.480439453125" Y="0.23949130249" />
                  <Point X="-28.508287109375" Y="0.249611190796" />
                  <Point X="-28.706158203125" Y="0.302630767822" />
                  <Point X="-29.7854453125" Y="0.591824829102" />
                  <Point X="-29.740927734375" Y="0.892669921875" />
                  <Point X="-29.73133203125" Y="0.957522583008" />
                  <Point X="-29.6335859375" Y="1.318236938477" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.7470546875" Y="1.204364379883" />
                  <Point X="-28.736701171875" Y="1.204703369141" />
                  <Point X="-28.715140625" Y="1.20658984375" />
                  <Point X="-28.70488671875" Y="1.208054077148" />
                  <Point X="-28.6846015625" Y="1.212089477539" />
                  <Point X="-28.6745703125" Y="1.214660766602" />
                  <Point X="-28.6213359375" Y="1.23144543457" />
                  <Point X="-28.603453125" Y="1.237675170898" />
                  <Point X="-28.58451953125" Y="1.246005126953" />
                  <Point X="-28.57527734375" Y="1.250688476562" />
                  <Point X="-28.556533203125" Y="1.261510375977" />
                  <Point X="-28.547859375" Y="1.267169921875" />
                  <Point X="-28.531177734375" Y="1.279401000977" />
                  <Point X="-28.51592578125" Y="1.293376464844" />
                  <Point X="-28.502287109375" Y="1.308928466797" />
                  <Point X="-28.495892578125" Y="1.317076416016" />
                  <Point X="-28.483478515625" Y="1.334805541992" />
                  <Point X="-28.4780078125" Y="1.343604125977" />
                  <Point X="-28.46805859375" Y="1.361740600586" />
                  <Point X="-28.463580078125" Y="1.371078491211" />
                  <Point X="-28.442220703125" Y="1.422647338867" />
                  <Point X="-28.4355" Y="1.440349609375" />
                  <Point X="-28.429712890625" Y="1.460202270508" />
                  <Point X="-28.427359375" Y="1.470287963867" />
                  <Point X="-28.423599609375" Y="1.49160546875" />
                  <Point X="-28.422359375" Y="1.501890869141" />
                  <Point X="-28.421005859375" Y="1.522535522461" />
                  <Point X="-28.42191015625" Y="1.543207641602" />
                  <Point X="-28.42505859375" Y="1.563655761719" />
                  <Point X="-28.427189453125" Y="1.573791503906" />
                  <Point X="-28.43279296875" Y="1.594699829102" />
                  <Point X="-28.436015625" Y="1.604543701172" />
                  <Point X="-28.44351171875" Y="1.623816894531" />
                  <Point X="-28.44778515625" Y="1.63324621582" />
                  <Point X="-28.47355859375" Y="1.682757202148" />
                  <Point X="-28.48280078125" Y="1.699289428711" />
                  <Point X="-28.494296875" Y="1.716494506836" />
                  <Point X="-28.500517578125" Y="1.724786254883" />
                  <Point X="-28.5144296875" Y="1.74136328125" />
                  <Point X="-28.521509765625" Y="1.748920898438" />
                  <Point X="-28.536447265625" Y="1.76321875" />
                  <Point X="-28.5443046875" Y="1.769958862305" />
                  <Point X="-28.6578046875" Y="1.857050537109" />
                  <Point X="-29.22761328125" Y="2.294280761719" />
                  <Point X="-29.03958203125" Y="2.616423583984" />
                  <Point X="-29.00228515625" Y="2.680321777344" />
                  <Point X="-28.726337890625" Y="3.035013427734" />
                  <Point X="-28.25415625" Y="2.762398681641" />
                  <Point X="-28.24491796875" Y="2.757716796875" />
                  <Point X="-28.225984375" Y="2.749385742188" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.080931640625" Y="2.724671386719" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310791016" />
                  <Point X="-27.976431640625" Y="2.734227783203" />
                  <Point X="-27.9569921875" Y="2.741303466797" />
                  <Point X="-27.938439453125" Y="2.750453369141" />
                  <Point X="-27.929412109375" Y="2.755534667969" />
                  <Point X="-27.911158203125" Y="2.767164550781" />
                  <Point X="-27.902740234375" Y="2.773198242188" />
                  <Point X="-27.88661328125" Y="2.786142578125" />
                  <Point X="-27.878904296875" Y="2.793053222656" />
                  <Point X="-27.82627734375" Y="2.845678710938" />
                  <Point X="-27.811267578125" Y="2.861486328125" />
                  <Point X="-27.798318359375" Y="2.877619140625" />
                  <Point X="-27.79228125" Y="2.886042236328" />
                  <Point X="-27.78065234375" Y="2.904296142578" />
                  <Point X="-27.7755703125" Y="2.913325195312" />
                  <Point X="-27.766423828125" Y="2.931874267578" />
                  <Point X="-27.759353515625" Y="2.951299072266" />
                  <Point X="-27.754435546875" Y="2.971388427734" />
                  <Point X="-27.7525234375" Y="2.981572998047" />
                  <Point X="-27.749697265625" Y="3.00303125" />
                  <Point X="-27.748908203125" Y="3.013364257812" />
                  <Point X="-27.74845703125" Y="3.034048583984" />
                  <Point X="-27.748794921875" Y="3.044399902344" />
                  <Point X="-27.75528125" Y="3.118540771484" />
                  <Point X="-27.757744140625" Y="3.140206787109" />
                  <Point X="-27.76178125" Y="3.160499023438" />
                  <Point X="-27.764353515625" Y="3.170533935547" />
                  <Point X="-27.77086328125" Y="3.191176513672" />
                  <Point X="-27.77451171875" Y="3.200869140625" />
                  <Point X="-27.782841796875" Y="3.219798583984" />
                  <Point X="-27.7875234375" Y="3.229035400391" />
                  <Point X="-27.8378203125" Y="3.316149414062" />
                  <Point X="-28.05938671875" Y="3.699915039062" />
                  <Point X="-27.7133125" Y="3.96524609375" />
                  <Point X="-27.648365234375" Y="4.015041748047" />
                  <Point X="-27.192525390625" Y="4.268296875" />
                  <Point X="-27.118564453125" Y="4.171908203125" />
                  <Point X="-27.111818359375" Y="4.164044433594" />
                  <Point X="-27.09751171875" Y="4.149100585938" />
                  <Point X="-27.089951171875" Y="4.142020507812" />
                  <Point X="-27.073369140625" Y="4.128107421875" />
                  <Point X="-27.065083984375" Y="4.121893554688" />
                  <Point X="-27.047888671875" Y="4.110404296875" />
                  <Point X="-27.038978515625" Y="4.10512890625" />
                  <Point X="-26.9564609375" Y="4.062172119141" />
                  <Point X="-26.934326171875" Y="4.051287353516" />
                  <Point X="-26.915046875" Y="4.043789794922" />
                  <Point X="-26.905203125" Y="4.040566894531" />
                  <Point X="-26.884298828125" Y="4.034965820313" />
                  <Point X="-26.8741640625" Y="4.032834716797" />
                  <Point X="-26.85372265625" Y="4.029688232422" />
                  <Point X="-26.83305859375" Y="4.028785644531" />
                  <Point X="-26.812419921875" Y="4.030137695312" />
                  <Point X="-26.802138671875" Y="4.031377197266" />
                  <Point X="-26.78082421875" Y="4.035134521484" />
                  <Point X="-26.770732421875" Y="4.037487060547" />
                  <Point X="-26.750869140625" Y="4.043276611328" />
                  <Point X="-26.74109765625" Y="4.046713378906" />
                  <Point X="-26.655150390625" Y="4.082314941406" />
                  <Point X="-26.6325859375" Y="4.092271484375" />
                  <Point X="-26.614453125" Y="4.102219238281" />
                  <Point X="-26.605658203125" Y="4.107688476562" />
                  <Point X="-26.587927734375" Y="4.120103027344" />
                  <Point X="-26.579779296875" Y="4.126498046875" />
                  <Point X="-26.5642265625" Y="4.140137207031" />
                  <Point X="-26.550251953125" Y="4.155386230469" />
                  <Point X="-26.53801953125" Y="4.172068359375" />
                  <Point X="-26.532357421875" Y="4.180745605469" />
                  <Point X="-26.52153515625" Y="4.199490722656" />
                  <Point X="-26.516853515625" Y="4.208729003906" />
                  <Point X="-26.5085234375" Y="4.227660644531" />
                  <Point X="-26.504875" Y="4.237354003906" />
                  <Point X="-26.476900390625" Y="4.326077636719" />
                  <Point X="-26.470025390625" Y="4.349764160156" />
                  <Point X="-26.465990234375" Y="4.370050292969" />
                  <Point X="-26.464525390625" Y="4.380302734375" />
                  <Point X="-26.462638671875" Y="4.401864257812" />
                  <Point X="-26.46230078125" Y="4.412217285156" />
                  <Point X="-26.462751953125" Y="4.432899902344" />
                  <Point X="-26.463541015625" Y="4.443229492188" />
                  <Point X="-26.469259765625" Y="4.486662109375" />
                  <Point X="-26.479263671875" Y="4.562654296875" />
                  <Point X="-26.0152421875" Y="4.69275" />
                  <Point X="-25.931169921875" Y="4.716320800781" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.22566796875" Y="4.261727050781" />
                  <Point X="-25.220435546875" Y="4.247104980469" />
                  <Point X="-25.20766015625" Y="4.218911621094" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218911621094" />
                  <Point X="-24.767251953125" Y="4.247104980469" />
                  <Point X="-24.76201953125" Y="4.2617265625" />
                  <Point X="-24.736248046875" Y="4.357904296875" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.245541015625" Y="4.745601074219" />
                  <Point X="-24.17212109375" Y="4.737911621094" />
                  <Point X="-23.620669921875" Y="4.604774414062" />
                  <Point X="-23.546404296875" Y="4.586843261719" />
                  <Point X="-23.18862890625" Y="4.457077148438" />
                  <Point X="-23.141736328125" Y="4.440068847656" />
                  <Point X="-22.794658203125" Y="4.277751464844" />
                  <Point X="-22.749546875" Y="4.256653808594" />
                  <Point X="-22.414201171875" Y="4.061281005859" />
                  <Point X="-22.370580078125" Y="4.035866210938" />
                  <Point X="-22.18221875" Y="3.901915771484" />
                  <Point X="-22.187966796875" Y="3.891959228516" />
                  <Point X="-22.934689453125" Y="2.59859765625" />
                  <Point X="-22.93762890625" Y="2.593096923828" />
                  <Point X="-22.9468125" Y="2.573448974609" />
                  <Point X="-22.955814453125" Y="2.549572509766" />
                  <Point X="-22.958697265625" Y="2.5406015625" />
                  <Point X="-22.9773046875" Y="2.471022216797" />
                  <Point X="-22.98187109375" Y="2.448625732422" />
                  <Point X="-22.986072265625" Y="2.419015136719" />
                  <Point X="-22.9870078125" Y="2.406660888672" />
                  <Point X="-22.987265625" Y="2.381941650391" />
                  <Point X="-22.986587890625" Y="2.369576660156" />
                  <Point X="-22.97933203125" Y="2.30941015625" />
                  <Point X="-22.97619921875" Y="2.289041015625" />
                  <Point X="-22.970859375" Y="2.267127929688" />
                  <Point X="-22.967537109375" Y="2.256332275391" />
                  <Point X="-22.959271484375" Y="2.23423828125" />
                  <Point X="-22.954685546875" Y="2.223900878906" />
                  <Point X="-22.944318359375" Y="2.203845703125" />
                  <Point X="-22.938537109375" Y="2.194127929688" />
                  <Point X="-22.901306640625" Y="2.139261962891" />
                  <Point X="-22.887060546875" Y="2.120813720703" />
                  <Point X="-22.868029296875" Y="2.099069091797" />
                  <Point X="-22.85947265625" Y="2.090468017578" />
                  <Point X="-22.841328125" Y="2.074423828125" />
                  <Point X="-22.831740234375" Y="2.066980712891" />
                  <Point X="-22.776875" Y="2.029751708984" />
                  <Point X="-22.758716796875" Y="2.018244262695" />
                  <Point X="-22.738669921875" Y="2.007880493164" />
                  <Point X="-22.728337890625" Y="2.003295776367" />
                  <Point X="-22.706251953125" Y="1.99503112793" />
                  <Point X="-22.69544921875" Y="1.991706542969" />
                  <Point X="-22.67352734375" Y="1.986364501953" />
                  <Point X="-22.662408203125" Y="1.984346801758" />
                  <Point X="-22.6022421875" Y="1.977091674805" />
                  <Point X="-22.57800390625" Y="1.975730957031" />
                  <Point X="-22.549841796875" Y="1.975952148438" />
                  <Point X="-22.537841796875" Y="1.976808105469" />
                  <Point X="-22.514046875" Y="1.980029907227" />
                  <Point X="-22.502251953125" Y="1.982395629883" />
                  <Point X="-22.432671875" Y="2.001002075195" />
                  <Point X="-22.41976953125" Y="2.004947631836" />
                  <Point X="-22.39698046875" Y="2.01281237793" />
                  <Point X="-22.388546875" Y="2.016182495117" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-22.164943359375" Y="2.142777832031" />
                  <Point X="-21.059595703125" Y="2.780950439453" />
                  <Point X="-20.98193359375" Y="2.673017822266" />
                  <Point X="-20.956033203125" Y="2.637024169922" />
                  <Point X="-20.86311328125" Y="2.483471435547" />
                  <Point X="-21.827048828125" Y="1.743818969727" />
                  <Point X="-21.83186328125" Y="1.739866577148" />
                  <Point X="-21.84787890625" Y="1.725216064453" />
                  <Point X="-21.865330078125" Y="1.706602905273" />
                  <Point X="-21.87142578125" Y="1.699420288086" />
                  <Point X="-21.921501953125" Y="1.634091796875" />
                  <Point X="-21.933697265625" Y="1.616141845703" />
                  <Point X="-21.94987109375" Y="1.589142211914" />
                  <Point X="-21.955642578125" Y="1.577863525391" />
                  <Point X="-21.96563671875" Y="1.554630981445" />
                  <Point X="-21.969859375" Y="1.542677124023" />
                  <Point X="-21.988513671875" Y="1.475976196289" />
                  <Point X="-21.993775390625" Y="1.4546640625" />
                  <Point X="-21.997228515625" Y="1.432365722656" />
                  <Point X="-21.998291015625" Y="1.421115844727" />
                  <Point X="-21.999107421875" Y="1.397545776367" />
                  <Point X="-21.998826171875" Y="1.386241210938" />
                  <Point X="-21.996921875" Y="1.363753051758" />
                  <Point X="-21.995298828125" Y="1.352569702148" />
                  <Point X="-21.979984375" Y="1.278356811523" />
                  <Point X="-21.9742421875" Y="1.256936035156" />
                  <Point X="-21.964689453125" Y="1.228137573242" />
                  <Point X="-21.960017578125" Y="1.216631591797" />
                  <Point X="-21.949212890625" Y="1.194326904297" />
                  <Point X="-21.943080078125" Y="1.183528320312" />
                  <Point X="-21.901431640625" Y="1.120224121094" />
                  <Point X="-21.888265625" Y="1.101432495117" />
                  <Point X="-21.873716796875" Y="1.084190429688" />
                  <Point X="-21.86592578125" Y="1.075998779297" />
                  <Point X="-21.848681640625" Y="1.059907470703" />
                  <Point X="-21.83996875" Y="1.052697753906" />
                  <Point X="-21.82175390625" Y="1.039368896484" />
                  <Point X="-21.812251953125" Y="1.033249633789" />
                  <Point X="-21.751896484375" Y="0.999275085449" />
                  <Point X="-21.731037109375" Y="0.98915637207" />
                  <Point X="-21.7037734375" Y="0.977930297852" />
                  <Point X="-21.692119140625" Y="0.973992553711" />
                  <Point X="-21.668396484375" Y="0.96765612793" />
                  <Point X="-21.656328125" Y="0.965257629395" />
                  <Point X="-21.574724609375" Y="0.95447253418" />
                  <Point X="-21.562126953125" Y="0.953232727051" />
                  <Point X="-21.5364921875" Y="0.951570068359" />
                  <Point X="-21.527203125" Y="0.951422973633" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-21.310337890625" Y="0.977687133789" />
                  <Point X="-20.295296875" Y="1.111319946289" />
                  <Point X="-20.25843359375" Y="0.959899230957" />
                  <Point X="-20.2473125" Y="0.914210021973" />
                  <Point X="-20.21612890625" Y="0.713920532227" />
                  <Point X="-21.3080078125" Y="0.42135269165" />
                  <Point X="-21.31396875" Y="0.419544250488" />
                  <Point X="-21.33437890625" Y="0.412135681152" />
                  <Point X="-21.357619140625" Y="0.401618530273" />
                  <Point X="-21.3659921875" Y="0.397316802979" />
                  <Point X="-21.4461640625" Y="0.35097543335" />
                  <Point X="-21.46346484375" Y="0.339677398682" />
                  <Point X="-21.4901171875" Y="0.320125762939" />
                  <Point X="-21.5" Y="0.311784912109" />
                  <Point X="-21.51854296875" Y="0.293835418701" />
                  <Point X="-21.527203125" Y="0.284226654053" />
                  <Point X="-21.575306640625" Y="0.222931152344" />
                  <Point X="-21.589146484375" Y="0.204206741333" />
                  <Point X="-21.600873046875" Y="0.184921585083" />
                  <Point X="-21.606162109375" Y="0.174928771973" />
                  <Point X="-21.615935546875" Y="0.153462432861" />
                  <Point X="-21.619998046875" Y="0.142918670654" />
                  <Point X="-21.626841796875" Y="0.121423347473" />
                  <Point X="-21.629623046875" Y="0.11047177124" />
                  <Point X="-21.645658203125" Y="0.02674505806" />
                  <Point X="-21.648474609375" Y="0.005752072811" />
                  <Point X="-21.65094140625" Y="-0.025936559677" />
                  <Point X="-21.651080078125" Y="-0.038601013184" />
                  <Point X="-21.649671875" Y="-0.063843425751" />
                  <Point X="-21.648125" Y="-0.076421379089" />
                  <Point X="-21.63208984375" Y="-0.160148086548" />
                  <Point X="-21.626841796875" Y="-0.183983337402" />
                  <Point X="-21.619998046875" Y="-0.205478668213" />
                  <Point X="-21.615935546875" Y="-0.21602243042" />
                  <Point X="-21.606162109375" Y="-0.237488769531" />
                  <Point X="-21.600873046875" Y="-0.247481582642" />
                  <Point X="-21.589146484375" Y="-0.26676675415" />
                  <Point X="-21.582708984375" Y="-0.276058929443" />
                  <Point X="-21.53460546875" Y="-0.33735458374" />
                  <Point X="-21.5199140625" Y="-0.353977905273" />
                  <Point X="-21.498201171875" Y="-0.375829193115" />
                  <Point X="-21.488783203125" Y="-0.38413067627" />
                  <Point X="-21.468935546875" Y="-0.399417877197" />
                  <Point X="-21.458505859375" Y="-0.406403900146" />
                  <Point X="-21.37833203125" Y="-0.452745269775" />
                  <Point X="-21.36783203125" Y="-0.458382080078" />
                  <Point X="-21.3434140625" Y="-0.470521453857" />
                  <Point X="-21.33473046875" Y="-0.474310882568" />
                  <Point X="-21.3080078125" Y="-0.483912963867" />
                  <Point X="-21.1346328125" Y="-0.530368469238" />
                  <Point X="-20.215119140625" Y="-0.776751159668" />
                  <Point X="-20.23217578125" Y="-0.889879394531" />
                  <Point X="-20.238380859375" Y="-0.931037536621" />
                  <Point X="-20.2721953125" Y="-1.079219970703" />
                  <Point X="-21.563216796875" Y="-0.909253601074" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042480469" />
                  <Point X="-21.63327734375" Y="-0.910840881348" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.802869140625" Y="-0.946877502441" />
                  <Point X="-21.82708203125" Y="-0.952140197754" />
                  <Point X="-21.842125" Y="-0.956742614746" />
                  <Point X="-21.871244140625" Y="-0.968366394043" />
                  <Point X="-21.8853203125" Y="-0.975388122559" />
                  <Point X="-21.9131484375" Y="-0.992280883789" />
                  <Point X="-21.925875" Y="-1.001531005859" />
                  <Point X="-21.949625" Y="-1.022002197266" />
                  <Point X="-21.9606484375" Y="-1.033223266602" />
                  <Point X="-22.0557578125" Y="-1.147609741211" />
                  <Point X="-22.070392578125" Y="-1.165211303711" />
                  <Point X="-22.078673828125" Y="-1.176848022461" />
                  <Point X="-22.093396484375" Y="-1.201233886719" />
                  <Point X="-22.099837890625" Y="-1.213982666016" />
                  <Point X="-22.1111796875" Y="-1.24136706543" />
                  <Point X="-22.11563671875" Y="-1.254934204102" />
                  <Point X="-22.122466796875" Y="-1.282581176758" />
                  <Point X="-22.12483984375" Y="-1.296661010742" />
                  <Point X="-22.138470703125" Y="-1.444796386719" />
                  <Point X="-22.140568359375" Y="-1.467591308594" />
                  <Point X="-22.140708984375" Y="-1.483319946289" />
                  <Point X="-22.138390625" Y="-1.514590209961" />
                  <Point X="-22.135931640625" Y="-1.530131835938" />
                  <Point X="-22.12819921875" Y="-1.561753295898" />
                  <Point X="-22.1232109375" Y="-1.576672241211" />
                  <Point X="-22.11083984375" Y="-1.605481445313" />
                  <Point X="-22.10345703125" Y="-1.619371582031" />
                  <Point X="-22.016376953125" Y="-1.754819458008" />
                  <Point X="-22.0029765625" Y="-1.775662475586" />
                  <Point X="-21.99825390625" Y="-1.782356079102" />
                  <Point X="-21.980205078125" Y="-1.804448608398" />
                  <Point X="-21.956509765625" Y="-1.828119140625" />
                  <Point X="-21.947201171875" Y="-1.83627734375" />
                  <Point X="-21.786310546875" Y="-1.95973425293" />
                  <Point X="-20.912830078125" Y="-2.629980224609" />
                  <Point X="-20.937013671875" Y="-2.669114501953" />
                  <Point X="-20.9545390625" Y="-2.697471679688" />
                  <Point X="-20.998724609375" Y="-2.760252685547" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.18497265625" Y="-2.079513427734" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228892578125" Y="-2.066337646484" />
                  <Point X="-22.416166015625" Y="-2.032516235352" />
                  <Point X="-22.444982421875" Y="-2.027311889648" />
                  <Point X="-22.460640625" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403442383" />
                  <Point X="-22.507685546875" Y="-2.02650378418" />
                  <Point X="-22.539859375" Y="-2.031461914062" />
                  <Point X="-22.555154296875" Y="-2.03513659668" />
                  <Point X="-22.5849296875" Y="-2.044960083008" />
                  <Point X="-22.59941015625" Y="-2.051108886719" />
                  <Point X="-22.75498828125" Y="-2.13298828125" />
                  <Point X="-22.778927734375" Y="-2.145587890625" />
                  <Point X="-22.79103125" Y="-2.153170654297" />
                  <Point X="-22.813962890625" Y="-2.170064453125" />
                  <Point X="-22.824791015625" Y="-2.179375488281" />
                  <Point X="-22.84575" Y="-2.200334960938" />
                  <Point X="-22.85505859375" Y="-2.211161376953" />
                  <Point X="-22.871951171875" Y="-2.234090820312" />
                  <Point X="-22.87953515625" Y="-2.246193847656" />
                  <Point X="-22.961416015625" Y="-2.401771728516" />
                  <Point X="-22.974015625" Y="-2.425711914062" />
                  <Point X="-22.9801640625" Y="-2.440193847656" />
                  <Point X="-22.98998828125" Y="-2.469972900391" />
                  <Point X="-22.9936640625" Y="-2.485270019531" />
                  <Point X="-22.99862109375" Y="-2.517444091797" />
                  <Point X="-22.999720703125" Y="-2.533135009766" />
                  <Point X="-22.99931640625" Y="-2.564485107422" />
                  <Point X="-22.9978125" Y="-2.580144287109" />
                  <Point X="-22.963990234375" Y="-2.767417236328" />
                  <Point X="-22.95878515625" Y="-2.796234619141" />
                  <Point X="-22.956982421875" Y="-2.804227783203" />
                  <Point X="-22.94876171875" Y="-2.831541992188" />
                  <Point X="-22.9359296875" Y="-2.862477294922" />
                  <Point X="-22.930453125" Y="-2.873578125" />
                  <Point X="-22.827064453125" Y="-3.052653808594" />
                  <Point X="-22.26410546875" Y="-4.027724853516" />
                  <Point X="-22.27804296875" Y="-4.033740234375" />
                  <Point X="-23.166083984375" Y="-2.876422363281" />
                  <Point X="-23.171345703125" Y="-2.870144042969" />
                  <Point X="-23.191169921875" Y="-2.849624023438" />
                  <Point X="-23.216748046875" Y="-2.828003662109" />
                  <Point X="-23.22669921875" Y="-2.820646728516" />
                  <Point X="-23.41140234375" Y="-2.701900634766" />
                  <Point X="-23.43982421875" Y="-2.683627929688" />
                  <Point X="-23.453716796875" Y="-2.676245605469" />
                  <Point X="-23.48252734375" Y="-2.663874511719" />
                  <Point X="-23.4974453125" Y="-2.658885742188" />
                  <Point X="-23.52906640625" Y="-2.651154052734" />
                  <Point X="-23.54460546875" Y="-2.648695800781" />
                  <Point X="-23.575875" Y="-2.646376953125" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.793607421875" Y="-2.665104980469" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670339355469" />
                  <Point X="-23.866423828125" Y="-2.677171386719" />
                  <Point X="-23.8799921875" Y="-2.681629394531" />
                  <Point X="-23.907376953125" Y="-2.69297265625" />
                  <Point X="-23.920123046875" Y="-2.6994140625" />
                  <Point X="-23.94450390625" Y="-2.714134033203" />
                  <Point X="-23.956138671875" Y="-2.722412597656" />
                  <Point X="-24.11212109375" Y="-2.852105957031" />
                  <Point X="-24.136123046875" Y="-2.872062988281" />
                  <Point X="-24.14734375" Y="-2.883084716797" />
                  <Point X="-24.167814453125" Y="-2.906832519531" />
                  <Point X="-24.177064453125" Y="-2.91955859375" />
                  <Point X="-24.19395703125" Y="-2.947383544922" />
                  <Point X="-24.20098046875" Y="-2.961465576172" />
                  <Point X="-24.21260546875" Y="-2.990588867188" />
                  <Point X="-24.21720703125" Y="-3.005630126953" />
                  <Point X="-24.26384375" Y="-3.220199951172" />
                  <Point X="-24.271021484375" Y="-3.253218017578" />
                  <Point X="-24.2724140625" Y="-3.261286132812" />
                  <Point X="-24.275275390625" Y="-3.289676269531" />
                  <Point X="-24.2752578125" Y="-3.323170410156" />
                  <Point X="-24.2744453125" Y="-3.335520996094" />
                  <Point X="-24.24514453125" Y="-3.558074707031" />
                  <Point X="-24.166912109375" Y="-4.152315429688" />
                  <Point X="-24.344931640625" Y="-3.487936279297" />
                  <Point X="-24.347390625" Y="-3.480124755859" />
                  <Point X="-24.357853515625" Y="-3.453576171875" />
                  <Point X="-24.3732109375" Y="-3.423814208984" />
                  <Point X="-24.37958984375" Y="-3.413209716797" />
                  <Point X="-24.521484375" Y="-3.208768554688" />
                  <Point X="-24.543318359375" Y="-3.177309326172" />
                  <Point X="-24.553328125" Y="-3.165173095703" />
                  <Point X="-24.5752109375" Y="-3.14271875" />
                  <Point X="-24.587083984375" Y="-3.132400634766" />
                  <Point X="-24.61333984375" Y="-3.113156005859" />
                  <Point X="-24.626755859375" Y="-3.104938232422" />
                  <Point X="-24.6547578125" Y="-3.090829589844" />
                  <Point X="-24.66934375" Y="-3.084938720703" />
                  <Point X="-24.8889140625" Y="-3.016791748047" />
                  <Point X="-24.922703125" Y="-3.006305419922" />
                  <Point X="-24.936623046875" Y="-3.003109375" />
                  <Point X="-24.964783203125" Y="-2.99883984375" />
                  <Point X="-24.9790234375" Y="-2.997766357422" />
                  <Point X="-25.0086640625" Y="-2.997766357422" />
                  <Point X="-25.022904296875" Y="-2.998839599609" />
                  <Point X="-25.051064453125" Y="-3.003108886719" />
                  <Point X="-25.064984375" Y="-3.006304931641" />
                  <Point X="-25.2845546875" Y="-3.074451660156" />
                  <Point X="-25.31834375" Y="-3.084938232422" />
                  <Point X="-25.3329296875" Y="-3.090829589844" />
                  <Point X="-25.360931640625" Y="-3.104938476562" />
                  <Point X="-25.37434765625" Y="-3.113156005859" />
                  <Point X="-25.400603515625" Y="-3.132400634766" />
                  <Point X="-25.412478515625" Y="-3.142719482422" />
                  <Point X="-25.434361328125" Y="-3.165174316406" />
                  <Point X="-25.444369140625" Y="-3.177310302734" />
                  <Point X="-25.58626171875" Y="-3.381751220703" />
                  <Point X="-25.608095703125" Y="-3.413210693359" />
                  <Point X="-25.61246875" Y="-3.420130859375" />
                  <Point X="-25.625974609375" Y="-3.445260498047" />
                  <Point X="-25.63877734375" Y="-3.476212890625" />
                  <Point X="-25.64275390625" Y="-3.487935546875" />
                  <Point X="-25.69617578125" Y="-3.6873046875" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.83311790494" Y="-3.94530595286" />
                  <Point X="-27.454069895532" Y="-4.098451289511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.1284360974" Y="-4.63404210981" />
                  <Point X="-25.967298234357" Y="-4.699146032455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.95820716781" Y="-3.792305809536" />
                  <Point X="-27.391443191445" Y="-4.021293319846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.119885683376" Y="-4.535035900763" />
                  <Point X="-25.942525743434" Y="-4.606693967912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.910240372156" Y="-3.709224852395" />
                  <Point X="-27.303059887517" Y="-3.954541692004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.137719575759" Y="-4.425369739977" />
                  <Point X="-25.917753252511" Y="-4.51424190337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.862273576502" Y="-3.626143895254" />
                  <Point X="-27.207499089151" Y="-3.890689960152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.159882020129" Y="-4.313954730669" />
                  <Point X="-25.892980761588" Y="-4.421789838828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.814306780848" Y="-3.543062938113" />
                  <Point X="-27.111938290785" Y="-3.826838228299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.182044681101" Y="-4.202539633847" />
                  <Point X="-25.868208270665" Y="-4.329337774286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.766339985194" Y="-3.459981980972" />
                  <Point X="-26.97954558452" Y="-3.777867553185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.24824056379" Y="-4.073333960644" />
                  <Point X="-25.843435779741" Y="-4.236885709744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.875548357663" Y="-2.909371908024" />
                  <Point X="-28.716644092464" Y="-2.973573398562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.71837318954" Y="-3.376901023831" />
                  <Point X="-26.689134495132" Y="-3.792740449016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.459890090309" Y="-3.885361200691" />
                  <Point X="-25.818663288818" Y="-4.144433645202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.988171266101" Y="-2.761408498831" />
                  <Point X="-28.612238909587" Y="-2.913295030003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.670406393886" Y="-3.29382006669" />
                  <Point X="-25.793890797895" Y="-4.051981580659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.073191825468" Y="-2.624597162557" />
                  <Point X="-28.507833726709" Y="-2.853016661444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.622439598232" Y="-3.21073910955" />
                  <Point X="-25.769118306972" Y="-3.959529516117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.15369130606" Y="-2.489612460678" />
                  <Point X="-28.403428543832" Y="-2.792738292885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.574472802578" Y="-3.127658152409" />
                  <Point X="-25.744345816049" Y="-3.867077451575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.123756459327" Y="-2.39924612327" />
                  <Point X="-28.299023360954" Y="-2.732459924327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.526506006924" Y="-3.044577195268" />
                  <Point X="-25.719573325126" Y="-3.774625387033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.036284382168" Y="-2.332126335916" />
                  <Point X="-28.194618178077" Y="-2.672181555768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.47853921127" Y="-2.961496238127" />
                  <Point X="-25.69480081099" Y="-3.682173331869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.94881230501" Y="-2.265006548562" />
                  <Point X="-28.0902129952" Y="-2.611903187209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.430572415616" Y="-2.878415280986" />
                  <Point X="-25.670027901838" Y="-3.589721436303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.861340227851" Y="-2.197886761208" />
                  <Point X="-27.985807812322" Y="-2.55162481865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.382605619962" Y="-2.795334323845" />
                  <Point X="-25.645254992686" Y="-3.497269540736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.184266691185" Y="-4.087547130181" />
                  <Point X="-24.174942992742" Y="-4.091314148873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.773868150692" Y="-2.130766973854" />
                  <Point X="-27.881402629445" Y="-2.491346450092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.33567843426" Y="-2.711833337019" />
                  <Point X="-25.606258925345" Y="-3.410564174092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.215053900806" Y="-3.972647489519" />
                  <Point X="-24.189189863821" Y="-3.983097238767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.686396073534" Y="-2.0636471865" />
                  <Point X="-27.776997163533" Y="-2.431068195886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.311633682658" Y="-2.619087246706" />
                  <Point X="-25.550720199185" Y="-3.330542475455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.245841110428" Y="-3.857747848857" />
                  <Point X="-24.2034367349" Y="-3.874880328661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.598923996375" Y="-1.996527399146" />
                  <Point X="-27.670699901076" Y="-2.371554277099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.331875651983" Y="-2.508448159681" />
                  <Point X="-25.495181102598" Y="-3.250520926481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.27662832005" Y="-3.742848208195" />
                  <Point X="-24.217683605978" Y="-3.766663418554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.511451919216" Y="-1.929407611792" />
                  <Point X="-25.438974860245" Y="-3.170768921893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.307415529672" Y="-3.627948567533" />
                  <Point X="-24.231930477057" Y="-3.658446508448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.423979842057" Y="-1.862287824438" />
                  <Point X="-25.355315426632" Y="-3.102108726557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.338202739294" Y="-3.513048926871" />
                  <Point X="-24.24617739836" Y="-3.55022957805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.66850091215" Y="-1.257007872962" />
                  <Point X="-29.611219084193" Y="-1.28015123372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.336507764899" Y="-1.795168037084" />
                  <Point X="-25.219863800443" Y="-3.054373935315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.398246883011" Y="-3.386328717547" />
                  <Point X="-24.260424962234" Y="-3.442012388036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.697684894598" Y="-1.142755978124" />
                  <Point X="-29.419946210582" Y="-1.254969690395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.24903568774" Y="-1.72804824973" />
                  <Point X="-25.076439672295" Y="-3.00986024395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.497073778892" Y="-3.243939259239" />
                  <Point X="-24.274555742418" Y="-3.333842381696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.726868641147" Y="-1.028504178596" />
                  <Point X="-29.228673336972" Y="-1.229788147071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.161563610581" Y="-1.660928462376" />
                  <Point X="-24.266943123244" Y="-3.234457278935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.748907634177" Y="-0.917139046866" />
                  <Point X="-29.037400463361" Y="-1.204606603746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.074091533422" Y="-1.593808675022" />
                  <Point X="-24.246470539321" Y="-3.140267939197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.387224453188" Y="-3.891452118276" />
                  <Point X="-22.329262190486" Y="-3.914870392516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.764461604737" Y="-0.80839403429" />
                  <Point X="-28.84612758975" Y="-1.179425060421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.000704236017" Y="-1.520998267263" />
                  <Point X="-24.225998456601" Y="-3.046078396958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.501171140101" Y="-3.742953867861" />
                  <Point X="-22.406415239559" Y="-3.781237736733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.780015575297" Y="-0.699649021713" />
                  <Point X="-28.65485471614" Y="-1.154243517096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.95967993958" Y="-1.435112358366" />
                  <Point X="-24.197767536236" Y="-2.955023628611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.615117827014" Y="-3.594455617447" />
                  <Point X="-22.483568288633" Y="-3.64760508095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.697062956161" Y="-0.630703254792" />
                  <Point X="-28.463581842529" Y="-1.129061973771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.949719863005" Y="-1.336675689959" />
                  <Point X="-24.140022531716" Y="-2.875893324294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.729064513927" Y="-3.445957367033" />
                  <Point X="-22.560721337706" Y="-3.513972425166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.544586025699" Y="-0.589847132979" />
                  <Point X="-28.272309015081" Y="-1.103880411796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.99148378149" Y="-1.217341171043" />
                  <Point X="-24.057567312371" Y="-2.806746594812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.84301120084" Y="-3.297459116619" />
                  <Point X="-22.63787438678" Y="-3.380339769383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.392109095237" Y="-0.548991011166" />
                  <Point X="-23.974635844448" Y="-2.737792282245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.956957887753" Y="-3.148960866204" />
                  <Point X="-22.715027435854" Y="-3.2467071136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.239632164775" Y="-0.508134889353" />
                  <Point X="-23.868990884106" Y="-2.678014816296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.070904574666" Y="-3.00046261579" />
                  <Point X="-22.792180484927" Y="-3.113074457817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.087155234313" Y="-0.46727876754" />
                  <Point X="-23.674477735855" Y="-2.654142428905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.191651659371" Y="-2.849216826322" />
                  <Point X="-22.869333006491" Y="-2.979442015161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.934678303851" Y="-0.426422645727" />
                  <Point X="-22.942110235778" Y="-2.847577305331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.782201373389" Y="-0.385566523914" />
                  <Point X="-22.970057086455" Y="-2.733825244174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.629724442927" Y="-0.344710402101" />
                  <Point X="-22.990018488404" Y="-2.623299513727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.481376903375" Y="-0.302185898064" />
                  <Point X="-22.998609337221" Y="-2.517367784949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.384181780644" Y="-0.238994476117" />
                  <Point X="-22.973638720789" Y="-2.424995768309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.325994199609" Y="-0.160042984318" />
                  <Point X="-22.929169510131" Y="-2.340501695102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.298015854506" Y="-0.068886168941" />
                  <Point X="-22.884700184088" Y="-2.256007668514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.779481784735" Y="0.632125720107" />
                  <Point X="-29.471576781045" Y="0.507724023551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.303225981785" Y="0.035679659674" />
                  <Point X="-22.823506325656" Y="-2.178270791626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.765175454683" Y="0.728806388125" />
                  <Point X="-28.718615282333" Y="0.305968631581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.374394364319" Y="0.166894353223" />
                  <Point X="-22.723283829411" Y="-2.116302507974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.750869124631" Y="0.825487056143" />
                  <Point X="-22.61314851405" Y="-2.058339263216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.285145335" Y="-2.594887375544" />
                  <Point X="-20.971520658761" Y="-2.721599969814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.736563170078" Y="0.922167875872" />
                  <Point X="-22.429362182455" Y="-2.030132960576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.87629431896" Y="-2.253586882094" />
                  <Point X="-20.919195032103" Y="-2.640280094713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.715458293649" Y="1.016101752856" />
                  <Point X="-21.159427651954" Y="-2.440759015438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.690433345278" Y="1.108451817969" />
                  <Point X="-21.441454396125" Y="-2.224352013851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.665408396906" Y="1.200801883081" />
                  <Point X="-21.723481140295" Y="-2.007945012265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.640383448535" Y="1.293151948193" />
                  <Point X="-21.983482575259" Y="-1.800436813231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.359589846632" Y="1.282164769532" />
                  <Point X="-22.077287818346" Y="-1.660076234348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.98341310266" Y="1.232640299972" />
                  <Point X="-22.134903280895" Y="-1.534337275911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.681142926967" Y="1.212976022259" />
                  <Point X="-22.137196602779" Y="-1.430949913171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.553143888284" Y="1.263721854303" />
                  <Point X="-22.128106488723" Y="-1.332161757091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.481873179505" Y="1.337387419377" />
                  <Point X="-22.10946698638" Y="-1.237231804319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.441832212652" Y="1.423670619216" />
                  <Point X="-22.061272786311" Y="-1.154242724526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.421313619858" Y="1.517841370164" />
                  <Point X="-21.997502346353" Y="-1.077546854148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.446537506708" Y="1.630493282523" />
                  <Point X="-21.927845324673" Y="-1.003229317166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.551174263251" Y="1.775230076907" />
                  <Point X="-21.809925514991" Y="-0.948411212269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.833200985822" Y="1.991637069766" />
                  <Point X="-21.644974806079" Y="-0.912594824085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.115227500789" Y="2.208043978748" />
                  <Point X="-21.320582423062" Y="-0.94119705373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.198504615445" Y="2.344150917636" />
                  <Point X="-20.944404992854" Y="-0.990721800547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.150111665508" Y="2.42705969727" />
                  <Point X="-20.568227562646" Y="-1.040246547364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.10171871557" Y="2.509968476904" />
                  <Point X="-20.267634447504" Y="-1.059233248632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.053325765633" Y="2.592877256538" />
                  <Point X="-20.246227115556" Y="-0.96542157161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.004932667739" Y="2.675785976394" />
                  <Point X="-21.527639518627" Y="-0.345236554105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.941015678909" Y="-0.582247970051" />
                  <Point X="-20.229157032572" Y="-0.869857532259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.944953494092" Y="2.754013617791" />
                  <Point X="-21.620004690386" Y="-0.205457801806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.884303784196" Y="2.831970344958" />
                  <Point X="-21.644966820035" Y="-0.092911646221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.8236540743" Y="2.909927072125" />
                  <Point X="-21.647811027999" Y="0.010698288943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.763004364404" Y="2.987883799292" />
                  <Point X="-28.368952793303" Y="2.828676630236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.119988340882" Y="2.728088462157" />
                  <Point X="-21.63045161777" Y="0.1061454325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.931463191038" Y="2.754380157945" />
                  <Point X="-21.595145159741" Y="0.194341498069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.848596167989" Y="2.823360507931" />
                  <Point X="-21.536066262867" Y="0.272932874893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.783654744876" Y="2.899583270404" />
                  <Point X="-21.457426032916" Y="0.343620960141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.751536381525" Y="2.989067409834" />
                  <Point X="-21.35278749407" Y="0.403805046769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.752968640336" Y="3.09210688051" />
                  <Point X="-21.208568146826" Y="0.447997448764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.775797586869" Y="3.203791174172" />
                  <Point X="-21.056091091907" Y="0.488853520293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.849236741801" Y="3.335923319322" />
                  <Point X="-20.903614036989" Y="0.529709591822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.926389176429" Y="3.469555726853" />
                  <Point X="-20.75113698207" Y="0.570565663351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.003541611056" Y="3.603188134384" />
                  <Point X="-21.917159690103" Y="1.14413021787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.45730797833" Y="0.958338066319" />
                  <Point X="-20.598659927151" Y="0.61142173488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.035216194401" Y="3.718446297302" />
                  <Point X="-21.97806548883" Y="1.271198558416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.266035295377" Y="0.983519686674" />
                  <Point X="-20.446182872233" Y="0.652277806409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.947696309551" Y="3.785546769095" />
                  <Point X="-21.998458443694" Y="1.381898647557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.074762481987" Y="1.00870125433" />
                  <Point X="-20.293705817314" Y="0.693133877938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.860176424701" Y="3.852647240888" />
                  <Point X="-21.987416768229" Y="1.479898321647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.883489668597" Y="1.033882821985" />
                  <Point X="-20.224491091304" Y="0.767630113971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.772656539851" Y="3.919747712681" />
                  <Point X="-21.958699779163" Y="1.570756705491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.692216855207" Y="1.059064389641" />
                  <Point X="-20.241514307603" Y="0.876968740358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.685137153617" Y="3.986848385928" />
                  <Point X="-21.907398774725" Y="1.652490554841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.500944041817" Y="1.084245957296" />
                  <Point X="-20.265552645296" Y="0.989141659766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.586453725459" Y="4.049438493451" />
                  <Point X="-22.917437245989" Y="2.163033386888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.483051320949" Y="1.987530081039" />
                  <Point X="-21.843566086653" Y="1.729161275349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.309671228427" Y="1.109427524952" />
                  <Point X="-20.293217808817" Y="1.102779911925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.479679837099" Y="4.108759842873" />
                  <Point X="-22.976231123451" Y="2.289248455856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.349694834995" Y="2.036111363882" />
                  <Point X="-21.757787231733" Y="1.796965168893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.372905948739" Y="4.168081192296" />
                  <Point X="-22.987117877405" Y="2.396107790522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.245289732503" Y="2.096389764919" />
                  <Point X="-21.670314967375" Y="1.864084880614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.266132060379" Y="4.227402541719" />
                  <Point X="-27.113146166444" Y="4.165592228386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.787388117934" Y="4.033977433511" />
                  <Point X="-22.97161326969" Y="2.492304322938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.140884606044" Y="2.156668156272" />
                  <Point X="-21.582842703017" Y="1.931204592334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.654327247182" Y="4.082678152649" />
                  <Point X="-22.942371359798" Y="2.582950625003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.036479399545" Y="2.216946515287" />
                  <Point X="-21.495370438658" Y="1.998324304055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.558475770158" Y="4.146412442701" />
                  <Point X="-22.895500755043" Y="2.666474472015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.932074193046" Y="2.277224874302" />
                  <Point X="-21.4078981743" Y="2.065444015775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.508186754323" Y="4.228555161986" />
                  <Point X="-22.847533945122" Y="2.749555423392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.827668986547" Y="2.337503233317" />
                  <Point X="-21.320425909942" Y="2.132563727496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.479054484561" Y="4.319245761539" />
                  <Point X="-22.799567135201" Y="2.832636374769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.723263780048" Y="2.397781592333" />
                  <Point X="-21.232953645584" Y="2.199683439216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.4623606506" Y="4.414961815363" />
                  <Point X="-22.751600325281" Y="2.915717326146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.618858573549" Y="2.458059951348" />
                  <Point X="-21.145481381226" Y="2.266803150937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.473924237167" Y="4.522094608155" />
                  <Point X="-22.70363351536" Y="2.998798277522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.51445336705" Y="2.518338310363" />
                  <Point X="-21.058009116868" Y="2.333922862657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.385664694286" Y="4.588896238706" />
                  <Point X="-22.655666705439" Y="3.081879228899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.410048160551" Y="2.578616669378" />
                  <Point X="-20.97053685251" Y="2.401042574378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.235953982704" Y="4.630869985492" />
                  <Point X="-25.20515211473" Y="4.214398997191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.918753168573" Y="4.098686311892" />
                  <Point X="-22.607699895518" Y="3.164960180276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.305642954052" Y="2.638895028393" />
                  <Point X="-20.883064588151" Y="2.468162286098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.086243271121" Y="4.672843732279" />
                  <Point X="-25.244724645426" Y="4.332848137969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.819968235093" Y="4.161235408603" />
                  <Point X="-22.559733085597" Y="3.248041131652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.201237747553" Y="2.699173387408" />
                  <Point X="-20.926461870425" Y="2.588156726821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.936532316197" Y="4.714817380749" />
                  <Point X="-25.275511626145" Y="4.447747686148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.76905500044" Y="4.243125927115" />
                  <Point X="-22.511766275676" Y="3.331122083029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.096832541054" Y="2.759451746423" />
                  <Point X="-21.022523917639" Y="2.729429113757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.741574611106" Y="4.738510155518" />
                  <Point X="-25.306298606863" Y="4.562647234327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.742430982908" Y="4.33482992635" />
                  <Point X="-22.463799465756" Y="3.414203034406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.544936048449" Y="4.761523819748" />
                  <Point X="-25.337085587581" Y="4.677546782506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.717658399347" Y="4.427281953464" />
                  <Point X="-22.415832655835" Y="3.497283985782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.692885987436" Y="4.519734049929" />
                  <Point X="-22.367865845914" Y="3.580364937159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.668113575524" Y="4.612186146394" />
                  <Point X="-22.319899035993" Y="3.663445888536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.643341163613" Y="4.704638242859" />
                  <Point X="-22.271932226072" Y="3.746526839913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.577061466429" Y="4.780320307511" />
                  <Point X="-22.223965416151" Y="3.829607791289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.23472529444" Y="4.744468316529" />
                  <Point X="-22.225480450995" Y="3.932680705654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.65720087335" Y="4.613594104903" />
                  <Point X="-22.694803907315" Y="4.224760490961" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.181298828125" Y="-4.832732910156" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544677734" />
                  <Point X="-24.67757421875" Y="-3.317103515625" />
                  <Point X="-24.699408203125" Y="-3.285644287109" />
                  <Point X="-24.7256640625" Y="-3.266399658203" />
                  <Point X="-24.945234375" Y="-3.198252685547" />
                  <Point X="-24.9790234375" Y="-3.187766357422" />
                  <Point X="-25.0086640625" Y="-3.187766357422" />
                  <Point X="-25.228234375" Y="-3.255913085938" />
                  <Point X="-25.2620234375" Y="-3.266399658203" />
                  <Point X="-25.288279296875" Y="-3.285644287109" />
                  <Point X="-25.430171875" Y="-3.490085205078" />
                  <Point X="-25.452005859375" Y="-3.521544677734" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.5126484375" Y="-3.736481445312" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-26.06174609375" Y="-4.945538085938" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.351587890625" Y="-4.873396484375" />
                  <Point X="-26.350939453125" Y="-4.868464355469" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.362138671875" Y="-4.271045410156" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.202628417969" />
                  <Point X="-26.588435546875" Y="-4.025344238281" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.91754296875" Y="-3.968177490234" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791748047" />
                  <Point X="-27.213443359375" Y="-4.123172851562" />
                  <Point X="-27.24784375" Y="-4.146159667969" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.289724609375" Y="-4.196379882812" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.799404296875" Y="-4.202550292969" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.208662109375" Y="-3.895944580078" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-28.15119140625" Y="-3.746566894531" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597593261719" />
                  <Point X="-27.513978515625" Y="-2.568764404297" />
                  <Point X="-27.529005859375" Y="-2.553736816406" />
                  <Point X="-27.531318359375" Y="-2.551423095703" />
                  <Point X="-27.560150390625" Y="-2.537199462891" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-27.763634765625" Y="-2.642746337891" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-29.11712109375" Y="-2.905700195312" />
                  <Point X="-29.16169921875" Y="-2.847134277344" />
                  <Point X="-29.414654296875" Y="-2.422969238281" />
                  <Point X="-29.43101953125" Y="-2.395526855469" />
                  <Point X="-29.292603515625" Y="-2.289317382812" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.396012207031" />
                  <Point X="-28.13914453125" Y="-1.370231933594" />
                  <Point X="-28.1381171875" Y="-1.366264892578" />
                  <Point X="-28.140326171875" Y="-1.334596191406" />
                  <Point X="-28.16115625" Y="-1.310640625" />
                  <Point X="-28.184107421875" Y="-1.29713269043" />
                  <Point X="-28.187638671875" Y="-1.295054077148" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.436599609375" Y="-1.317149291992" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.909955078125" Y="-1.079455810547" />
                  <Point X="-29.927390625" Y="-1.01118951416" />
                  <Point X="-29.99431640625" Y="-0.543261230469" />
                  <Point X="-29.99839453125" Y="-0.514742248535" />
                  <Point X="-29.84335546875" Y="-0.473199523926" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.541896484375" Y="-0.121425354004" />
                  <Point X="-28.51784375" Y="-0.104732116699" />
                  <Point X="-28.514142578125" Y="-0.102163330078" />
                  <Point X="-28.4948984375" Y="-0.075908210754" />
                  <Point X="-28.486880859375" Y="-0.050076286316" />
                  <Point X="-28.485642578125" Y="-0.046086883545" />
                  <Point X="-28.4856484375" Y="-0.016458742142" />
                  <Point X="-28.493662109375" Y="0.009358324051" />
                  <Point X="-28.494892578125" Y="0.013333683014" />
                  <Point X="-28.514142578125" Y="0.039603248596" />
                  <Point X="-28.5381953125" Y="0.056296489716" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-28.755333984375" Y="0.119104988098" />
                  <Point X="-29.998185546875" Y="0.452126037598" />
                  <Point X="-29.928880859375" Y="0.920482055664" />
                  <Point X="-29.91764453125" Y="0.996415466309" />
                  <Point X="-29.782927734375" Y="1.493563598633" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.675025390625" Y="1.515332275391" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866699219" />
                  <Point X="-28.67846875" Y="1.412651489258" />
                  <Point X="-28.67027734375" Y="1.41523425293" />
                  <Point X="-28.651533203125" Y="1.426055908203" />
                  <Point X="-28.639119140625" Y="1.44378503418" />
                  <Point X="-28.617759765625" Y="1.495353881836" />
                  <Point X="-28.61447265625" Y="1.50328918457" />
                  <Point X="-28.610712890625" Y="1.524606567383" />
                  <Point X="-28.61631640625" Y="1.545515014648" />
                  <Point X="-28.64208984375" Y="1.595026000977" />
                  <Point X="-28.646056640625" Y="1.60264465332" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-28.77346875" Y="1.706313476562" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.20367578125" Y="2.712202880859" />
                  <Point X="-29.16001171875" Y="2.787007080078" />
                  <Point X="-28.803154296875" Y="3.245699462891" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.725275390625" Y="3.253792724609" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.064373046875" Y="2.913948242188" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927405273438" />
                  <Point X="-27.960625" Y="2.980030761719" />
                  <Point X="-27.95252734375" Y="2.988128662109" />
                  <Point X="-27.9408984375" Y="3.006382568359" />
                  <Point X="-27.938072265625" Y="3.027840820312" />
                  <Point X="-27.94455859375" Y="3.101981689453" />
                  <Point X="-27.945556640625" Y="3.113390380859" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.00236328125" Y="3.221146972656" />
                  <Point X="-28.30727734375" Y="3.749276855469" />
                  <Point X="-27.82891796875" Y="4.116029785156" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.19083203125" Y="4.486590820312" />
                  <Point X="-27.141548828125" Y="4.513971679688" />
                  <Point X="-27.1411328125" Y="4.513430664062" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.951244140625" Y="4.27366015625" />
                  <Point X="-26.8687265625" Y="4.230703613281" />
                  <Point X="-26.85602734375" Y="4.224093261719" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.81380859375" Y="4.222249511719" />
                  <Point X="-26.727861328125" Y="4.257851074219" />
                  <Point X="-26.714634765625" Y="4.263329101563" />
                  <Point X="-26.696904296875" Y="4.275743652344" />
                  <Point X="-26.68608203125" Y="4.294488769531" />
                  <Point X="-26.658107421875" Y="4.383212402344" />
                  <Point X="-26.653802734375" Y="4.396865234375" />
                  <Point X="-26.651916015625" Y="4.418426757812" />
                  <Point X="-26.657634765625" Y="4.461859375" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.06653515625" Y="4.875696289062" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.286724609375" Y="4.983040527344" />
                  <Point X="-25.22419921875" Y="4.990358398438" />
                  <Point X="-25.20961328125" Y="4.935923828125" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.919775390625" Y="4.407081054688" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.225751953125" Y="4.934567871094" />
                  <Point X="-24.13979296875" Y="4.925564941406" />
                  <Point X="-23.576080078125" Y="4.789467773438" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.12384375" Y="4.63569140625" />
                  <Point X="-23.068970703125" Y="4.615788085938" />
                  <Point X="-22.714169921875" Y="4.449859863281" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.318556640625" Y="4.225451171875" />
                  <Point X="-22.26747265625" Y="4.195688964844" />
                  <Point X="-21.94421875" Y="3.965810302734" />
                  <Point X="-21.9312578125" Y="3.956593261719" />
                  <Point X="-22.023421875" Y="3.796959472656" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491515136719" />
                  <Point X="-22.79375390625" Y="2.421935791016" />
                  <Point X="-22.797955078125" Y="2.392325195312" />
                  <Point X="-22.79069921875" Y="2.332158691406" />
                  <Point X="-22.789583984375" Y="2.322906982422" />
                  <Point X="-22.781318359375" Y="2.300812988281" />
                  <Point X="-22.744087890625" Y="2.245947021484" />
                  <Point X="-22.725056640625" Y="2.224202392578" />
                  <Point X="-22.67019140625" Y="2.186973388672" />
                  <Point X="-22.661748046875" Y="2.181244873047" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.57949609375" Y="2.165725097656" />
                  <Point X="-22.551333984375" Y="2.165946289062" />
                  <Point X="-22.48175390625" Y="2.184552734375" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-22.259943359375" Y="2.307322753906" />
                  <Point X="-21.005751953125" Y="3.031430419922" />
                  <Point X="-20.82770703125" Y="2.783989501953" />
                  <Point X="-20.79740234375" Y="2.741873291016" />
                  <Point X="-20.617197265625" Y="2.444081787109" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-20.728251953125" Y="2.347465087891" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583831542969" />
                  <Point X="-21.770705078125" Y="1.518502929688" />
                  <Point X="-21.78687890625" Y="1.491503295898" />
                  <Point X="-21.805533203125" Y="1.424802490234" />
                  <Point X="-21.808404296875" Y="1.414538696289" />
                  <Point X="-21.809220703125" Y="1.39096862793" />
                  <Point X="-21.79390625" Y="1.316755737305" />
                  <Point X="-21.784353515625" Y="1.28795703125" />
                  <Point X="-21.742705078125" Y="1.224652832031" />
                  <Point X="-21.736296875" Y="1.214911621094" />
                  <Point X="-21.719052734375" Y="1.19882019043" />
                  <Point X="-21.658697265625" Y="1.164845581055" />
                  <Point X="-21.63143359375" Y="1.153619506836" />
                  <Point X="-21.549830078125" Y="1.142834472656" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-21.335138671875" Y="1.166061645508" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.07382421875" Y="1.004841308594" />
                  <Point X="-20.06080859375" Y="0.951370727539" />
                  <Point X="-20.004021484375" Y="0.58663659668" />
                  <Point X="-20.002140625" Y="0.574556213379" />
                  <Point X="-20.132142578125" Y="0.539722106934" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.27091015625" Y="0.232820114136" />
                  <Point X="-21.35108203125" Y="0.186478652954" />
                  <Point X="-21.377734375" Y="0.166927001953" />
                  <Point X="-21.425837890625" Y="0.105631439209" />
                  <Point X="-21.433240234375" Y="0.096199325562" />
                  <Point X="-21.443013671875" Y="0.074732978821" />
                  <Point X="-21.459048828125" Y="-0.008993789673" />
                  <Point X="-21.461515625" Y="-0.040682498932" />
                  <Point X="-21.44548046875" Y="-0.124409263611" />
                  <Point X="-21.443013671875" Y="-0.137293060303" />
                  <Point X="-21.433240234375" Y="-0.158759399414" />
                  <Point X="-21.38513671875" Y="-0.220054946899" />
                  <Point X="-21.363423828125" Y="-0.241906143188" />
                  <Point X="-21.28325" Y="-0.288247467041" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-21.08545703125" Y="-0.346842346191" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.04430078125" Y="-0.918204528809" />
                  <Point X="-20.051568359375" Y="-0.966412536621" />
                  <Point X="-20.124322265625" Y="-1.285228393555" />
                  <Point X="-20.125451171875" Y="-1.290178710938" />
                  <Point X="-20.281376953125" Y="-1.269650878906" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.762513671875" Y="-1.132542358398" />
                  <Point X="-21.7867265625" Y="-1.137805053711" />
                  <Point X="-21.8145546875" Y="-1.154697875977" />
                  <Point X="-21.9096640625" Y="-1.269084350586" />
                  <Point X="-21.924298828125" Y="-1.286686035156" />
                  <Point X="-21.935640625" Y="-1.31407043457" />
                  <Point X="-21.949271484375" Y="-1.462205810547" />
                  <Point X="-21.951369140625" Y="-1.485000732422" />
                  <Point X="-21.94363671875" Y="-1.516622436523" />
                  <Point X="-21.856556640625" Y="-1.65207043457" />
                  <Point X="-21.84315625" Y="-1.672913085938" />
                  <Point X="-21.831537109375" Y="-1.685540771484" />
                  <Point X="-21.670646484375" Y="-1.808997680664" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.775384765625" Y="-2.768997802734" />
                  <Point X="-20.79587109375" Y="-2.802145751953" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.0828046875" Y="-2.931102050781" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.449931640625" Y="-2.219491455078" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219245117188" />
                  <Point X="-22.6665" Y="-2.301124511719" />
                  <Point X="-22.690439453125" Y="-2.313724121094" />
                  <Point X="-22.7113984375" Y="-2.33468359375" />
                  <Point X="-22.793279296875" Y="-2.490261474609" />
                  <Point X="-22.80587890625" Y="-2.514201660156" />
                  <Point X="-22.8108359375" Y="-2.546375732422" />
                  <Point X="-22.777013671875" Y="-2.733648681641" />
                  <Point X="-22.77180859375" Y="-2.762466064453" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.662517578125" Y="-2.957654296875" />
                  <Point X="-22.01332421875" Y="-4.082088378906" />
                  <Point X="-22.140392578125" Y="-4.172850097656" />
                  <Point X="-22.16469921875" Y="-4.1902109375" />
                  <Point X="-22.32022265625" Y="-4.29087890625" />
                  <Point X="-22.428779296875" Y="-4.149405273438" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467529297" />
                  <Point X="-23.51415234375" Y="-2.861721435547" />
                  <Point X="-23.54257421875" Y="-2.843448730469" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.776197265625" Y="-2.854305664062" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509277344" />
                  <Point X="-23.9906484375" Y="-2.998202636719" />
                  <Point X="-24.014650390625" Y="-3.018159667969" />
                  <Point X="-24.03154296875" Y="-3.045984619141" />
                  <Point X="-24.0781796875" Y="-3.260554443359" />
                  <Point X="-24.085357421875" Y="-3.293572509766" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-24.05676953125" Y="-3.533273925781" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.982662109375" Y="-4.958207519531" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#204" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.167473804092" Y="4.980145251278" Z="2.2" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.2" />
                  <Point X="-0.298693537241" Y="5.063981769904" Z="2.2" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.2" />
                  <Point X="-1.08619104069" Y="4.95512000766" Z="2.2" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.2" />
                  <Point X="-1.713363905394" Y="4.486613220306" Z="2.2" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.2" />
                  <Point X="-1.711950597941" Y="4.4295278149" Z="2.2" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.2" />
                  <Point X="-1.753150804782" Y="4.335325806788" Z="2.2" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.2" />
                  <Point X="-1.851796909826" Y="4.306334713563" Z="2.2" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.2" />
                  <Point X="-2.10762148629" Y="4.575148572634" Z="2.2" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.2" />
                  <Point X="-2.221271493599" Y="4.56157817574" Z="2.2" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.2" />
                  <Point X="-2.865497500482" Y="4.186989596487" Z="2.2" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.2" />
                  <Point X="-3.051820029124" Y="3.227426993309" Z="2.2" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.2" />
                  <Point X="-3.000526543691" Y="3.128904197465" Z="2.2" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.2" />
                  <Point X="-3.002137998096" Y="3.04666556979" Z="2.2" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.2" />
                  <Point X="-3.066172268141" Y="2.995038155482" Z="2.2" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.2" />
                  <Point X="-3.706432354988" Y="3.328373919622" Z="2.2" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.2" />
                  <Point X="-3.848773961386" Y="3.307682059455" Z="2.2" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.2" />
                  <Point X="-4.253014422393" Y="2.768688229611" Z="2.2" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.2" />
                  <Point X="-3.810062415468" Y="1.697925821193" Z="2.2" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.2" />
                  <Point X="-3.692596234804" Y="1.60321539919" Z="2.2" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.2" />
                  <Point X="-3.670109377626" Y="1.54576899532" Z="2.2" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.2" />
                  <Point X="-3.699661593614" Y="1.491617284349" Z="2.2" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.2" />
                  <Point X="-4.674656079028" Y="1.596184578759" Z="2.2" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.2" />
                  <Point X="-4.837344441734" Y="1.537920624915" Z="2.2" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.2" />
                  <Point X="-4.984499123684" Y="0.959080936969" Z="2.2" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.2" />
                  <Point X="-3.774433222182" Y="0.102088805845" Z="2.2" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.2" />
                  <Point X="-3.572859660558" Y="0.046500274567" Z="2.2" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.2" />
                  <Point X="-3.547574146509" Y="0.025831948089" Z="2.2" />
                  <Point X="-3.539556741714" Y="0" Z="2.2" />
                  <Point X="-3.540790454626" Y="-0.003975002972" Z="2.2" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.2" />
                  <Point X="-3.552508934565" Y="-0.032375708567" Z="2.2" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.2" />
                  <Point X="-4.862454566591" Y="-0.393623250911" Z="2.2" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.2" />
                  <Point X="-5.049969740346" Y="-0.519060262802" Z="2.2" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.2" />
                  <Point X="-4.964557220809" Y="-1.06054926842" Z="2.2" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.2" />
                  <Point X="-3.436232044995" Y="-1.335441525766" Z="2.2" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.2" />
                  <Point X="-3.215627216935" Y="-1.308941895776" Z="2.2" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.2" />
                  <Point X="-3.193703763962" Y="-1.326416860285" Z="2.2" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.2" />
                  <Point X="-4.329198833412" Y="-2.218369673595" Z="2.2" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.2" />
                  <Point X="-4.463753855142" Y="-2.417298837949" Z="2.2" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.2" />
                  <Point X="-4.163064094397" Y="-2.904703649384" Z="2.2" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.2" />
                  <Point X="-2.744791550186" Y="-2.65476753255" Z="2.2" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.2" />
                  <Point X="-2.570525929116" Y="-2.557804540015" Z="2.2" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.2" />
                  <Point X="-3.200649480618" Y="-3.690286832306" Z="2.2" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.2" />
                  <Point X="-3.245322434352" Y="-3.904281843611" Z="2.2" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.2" />
                  <Point X="-2.831883527809" Y="-4.213780856694" Z="2.2" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.2" />
                  <Point X="-2.256213945031" Y="-4.195538068452" Z="2.2" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.2" />
                  <Point X="-2.191820343815" Y="-4.133465508485" Z="2.2" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.2" />
                  <Point X="-1.926969728308" Y="-3.986790415692" Z="2.2" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.2" />
                  <Point X="-1.627559586624" Y="-4.031656308815" Z="2.2" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.2" />
                  <Point X="-1.417334144878" Y="-4.249520319371" Z="2.2" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.2" />
                  <Point X="-1.406668446767" Y="-4.83065798628" Z="2.2" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.2" />
                  <Point X="-1.373665386378" Y="-4.889649189117" Z="2.2" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.2" />
                  <Point X="-1.077429867462" Y="-4.963342272766" Z="2.2" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.2" />
                  <Point X="-0.470507603143" Y="-3.718142319052" Z="2.2" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.2" />
                  <Point X="-0.395252339657" Y="-3.487313813014" Z="2.2" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.2" />
                  <Point X="-0.219571554723" Y="-3.272386276078" Z="2.2" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.2" />
                  <Point X="0.033787524638" Y="-3.21472576921" Z="2.2" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.2" />
                  <Point X="0.275193519775" Y="-3.314331942754" Z="2.2" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.2" />
                  <Point X="0.764247257259" Y="-4.814393710763" Z="2.2" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.2" />
                  <Point X="0.84171818513" Y="-5.009393924595" Z="2.2" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.2" />
                  <Point X="1.02188699602" Y="-4.975767588065" Z="2.2" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.2" />
                  <Point X="0.986645551248" Y="-3.495467497598" Z="2.2" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.2" />
                  <Point X="0.964522355815" Y="-3.239895833381" Z="2.2" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.2" />
                  <Point X="1.035162332847" Y="-3.005368874236" Z="2.2" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.2" />
                  <Point X="1.222227787347" Y="-2.872815058549" Z="2.2" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.2" />
                  <Point X="1.452652226447" Y="-2.872499161306" Z="2.2" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.2" />
                  <Point X="2.525395430985" Y="-4.148563292738" Z="2.2" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.2" />
                  <Point X="2.688081790601" Y="-4.309798588238" Z="2.2" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.2" />
                  <Point X="2.882510164756" Y="-4.182258098382" Z="2.2" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.2" />
                  <Point X="2.374626311751" Y="-2.901374267341" Z="2.2" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.2" />
                  <Point X="2.266032486134" Y="-2.693481178094" Z="2.2" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.2" />
                  <Point X="2.244810678727" Y="-2.482267976294" Z="2.2" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.2" />
                  <Point X="2.350630402078" Y="-2.31409063124" Z="2.2" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.2" />
                  <Point X="2.535025625419" Y="-2.237415523357" Z="2.2" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.2" />
                  <Point X="3.886039329766" Y="-2.943123188524" Z="2.2" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.2" />
                  <Point X="4.088400296685" Y="-3.013427368972" Z="2.2" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.2" />
                  <Point X="4.260991065682" Y="-2.764003568699" Z="2.2" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.2" />
                  <Point X="3.35363498749" Y="-1.738049944329" Z="2.2" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.2" />
                  <Point X="3.179342944704" Y="-1.593750396559" Z="2.2" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.2" />
                  <Point X="3.094360091872" Y="-1.435507535862" Z="2.2" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.2" />
                  <Point X="3.122626734887" Y="-1.269770535686" Z="2.2" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.2" />
                  <Point X="3.241948631253" Y="-1.150121364976" Z="2.2" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.2" />
                  <Point X="4.705941582365" Y="-1.287943057677" Z="2.2" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.2" />
                  <Point X="4.918266397823" Y="-1.265072431321" Z="2.2" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.2" />
                  <Point X="4.998983362637" Y="-0.894378564692" Z="2.2" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.2" />
                  <Point X="3.921326754736" Y="-0.26726607279" Z="2.2" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.2" />
                  <Point X="3.735615729523" Y="-0.213679647003" Z="2.2" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.2" />
                  <Point X="3.648040532608" Y="-0.157906087913" Z="2.2" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.2" />
                  <Point X="3.597469329681" Y="-0.083726719387" Z="2.2" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.2" />
                  <Point X="3.583902120743" Y="0.012883811795" Z="2.2" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.2" />
                  <Point X="3.607338905792" Y="0.106042650662" Z="2.2" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.2" />
                  <Point X="3.66777968483" Y="0.174469197239" Z="2.2" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.2" />
                  <Point X="4.874641062571" Y="0.522705859978" Z="2.2" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.2" />
                  <Point X="5.039226510854" Y="0.62560908265" Z="2.2" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.2" />
                  <Point X="4.968600036732" Y="1.047947297137" Z="2.2" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.2" />
                  <Point X="3.652178617796" Y="1.246913943841" Z="2.2" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.2" />
                  <Point X="3.450564294873" Y="1.223683659425" Z="2.2" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.2" />
                  <Point X="3.359673274469" Y="1.239696549832" Z="2.2" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.2" />
                  <Point X="3.292909680203" Y="1.283412234451" Z="2.2" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.2" />
                  <Point X="3.248904693866" Y="1.358136244856" Z="2.2" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.2" />
                  <Point X="3.236462430124" Y="1.442613056087" Z="2.2" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.2" />
                  <Point X="3.262821715636" Y="1.519366434285" Z="2.2" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.2" />
                  <Point X="4.296028444498" Y="2.339077475562" Z="2.2" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.2" />
                  <Point X="4.419422946051" Y="2.501248087094" Z="2.2" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.2" />
                  <Point X="4.206721720577" Y="2.844472791155" Z="2.2" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.2" />
                  <Point X="2.708899544614" Y="2.381903949757" Z="2.2" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.2" />
                  <Point X="2.499171084051" Y="2.264135620395" Z="2.2" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.2" />
                  <Point X="2.420333276551" Y="2.24664556385" Z="2.2" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.2" />
                  <Point X="2.351724048252" Y="2.259629271092" Z="2.2" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.2" />
                  <Point X="2.291129329143" Y="2.305300812131" Z="2.2" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.2" />
                  <Point X="2.252784138909" Y="2.369425167282" Z="2.2" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.2" />
                  <Point X="2.248392322263" Y="2.440298491831" Z="2.2" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.2" />
                  <Point X="3.013721403103" Y="3.803239537341" Z="2.2" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.2" />
                  <Point X="3.078600070145" Y="4.037837255393" Z="2.2" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.2" />
                  <Point X="2.700456577686" Y="4.29993396314" Z="2.2" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.2" />
                  <Point X="2.30085492832" Y="4.526431940512" Z="2.2" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.2" />
                  <Point X="1.887048102043" Y="4.713974650699" Z="2.2" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.2" />
                  <Point X="1.429497421607" Y="4.869351476594" Z="2.2" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.2" />
                  <Point X="0.773300102485" Y="5.015575725289" Z="2.2" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.2" />
                  <Point X="0.025770757461" Y="4.451302254673" Z="2.2" />
                  <Point X="0" Y="4.355124473572" Z="2.2" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>