<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#181" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2414" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000475585938" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.18416015625" Y="-4.454998046875" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497139648438" />
                  <Point X="-24.457634765625" Y="-3.467376953125" />
                  <Point X="-24.560853515625" Y="-3.31866015625" />
                  <Point X="-24.62136328125" Y="-3.2314765625" />
                  <Point X="-24.643248046875" Y="-3.209021484375" />
                  <Point X="-24.66950390625" Y="-3.18977734375" />
                  <Point X="-24.69750390625" Y="-3.175669433594" />
                  <Point X="-24.8572265625" Y="-3.126097167969" />
                  <Point X="-24.95086328125" Y="-3.097036132812" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.196546875" Y="-3.146607910156" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.18977734375" />
                  <Point X="-25.344439453125" Y="-3.209021484375" />
                  <Point X="-25.36632421875" Y="-3.231477294922" />
                  <Point X="-25.469541015625" Y="-3.380193847656" />
                  <Point X="-25.53005078125" Y="-3.467377685547" />
                  <Point X="-25.5381875" Y="-3.481571777344" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.699037109375" Y="-4.065040527344" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-25.97283984375" Y="-4.866021972656" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.24641796875" Y="-4.802362304688" />
                  <Point X="-26.245361328125" Y="-4.794342773438" />
                  <Point X="-26.2149609375" Y="-4.5634375" />
                  <Point X="-26.21419921875" Y="-4.547929199219" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.254666015625" Y="-4.324391113281" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.182963867188" />
                  <Point X="-26.30401171875" Y="-4.155126953125" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.4706953125" Y="-4.002242431641" />
                  <Point X="-26.556904296875" Y="-3.926639648438" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.83819921875" Y="-3.878174072266" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708740234" />
                  <Point X="-27.014466796875" Y="-3.882028564453" />
                  <Point X="-27.042658203125" Y="-3.894801757812" />
                  <Point X="-27.20528515625" Y="-4.003466064453" />
                  <Point X="-27.300623046875" Y="-4.067169677734" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.3351015625" Y="-4.099461914062" />
                  <Point X="-27.41821875" Y="-4.20778125" />
                  <Point X="-27.4801484375" Y="-4.288489257813" />
                  <Point X="-27.645607421875" Y="-4.186041015625" />
                  <Point X="-27.80171484375" Y="-4.0893828125" />
                  <Point X="-28.053984375" Y="-3.895143798828" />
                  <Point X="-28.104720703125" Y="-3.856078369141" />
                  <Point X="-27.893068359375" Y="-3.489484130859" />
                  <Point X="-27.423759765625" Y="-2.676619384766" />
                  <Point X="-27.412859375" Y="-2.647654296875" />
                  <Point X="-27.406587890625" Y="-2.616127441406" />
                  <Point X="-27.40557421875" Y="-2.585193603516" />
                  <Point X="-27.41455859375" Y="-2.555575927734" />
                  <Point X="-27.428775390625" Y="-2.526746582031" />
                  <Point X="-27.446802734375" Y="-2.501588378906" />
                  <Point X="-27.464146484375" Y="-2.484244628906" />
                  <Point X="-27.489296875" Y="-2.466220214844" />
                  <Point X="-27.518126953125" Y="-2.451999755859" />
                  <Point X="-27.54774609375" Y="-2.443012695312" />
                  <Point X="-27.57868359375" Y="-2.444023925781" />
                  <Point X="-27.610212890625" Y="-2.450294433594" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.115716796875" Y="-2.736324707031" />
                  <Point X="-28.818021484375" Y="-3.141800537109" />
                  <Point X="-28.95953125" Y="-2.955887939453" />
                  <Point X="-29.0828671875" Y="-2.793849365234" />
                  <Point X="-29.263720703125" Y="-2.4905859375" />
                  <Point X="-29.306142578125" Y="-2.419450439453" />
                  <Point X="-28.92708984375" Y="-2.128593505859" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.084578125" Y="-1.475594360352" />
                  <Point X="-28.06661328125" Y="-1.448463867188" />
                  <Point X="-28.053857421875" Y="-1.419834838867" />
                  <Point X="-28.04615234375" Y="-1.390087646484" />
                  <Point X="-28.04334765625" Y="-1.359662963867" />
                  <Point X="-28.0455546875" Y="-1.3279921875" />
                  <Point X="-28.0525546875" Y="-1.298242797852" />
                  <Point X="-28.068640625" Y="-1.272255859375" />
                  <Point X="-28.089474609375" Y="-1.248299194336" />
                  <Point X="-28.112970703125" Y="-1.228767822266" />
                  <Point X="-28.139453125" Y="-1.213181152344" />
                  <Point X="-28.16871484375" Y="-1.20195715332" />
                  <Point X="-28.2006015625" Y="-1.195475097656" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-28.8335078125" Y="-1.273583251953" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.785841796875" Y="-1.181492553711" />
                  <Point X="-29.834078125" Y="-0.992650512695" />
                  <Point X="-29.881927734375" Y="-0.658085327148" />
                  <Point X="-29.892423828125" Y="-0.584698486328" />
                  <Point X="-29.468271484375" Y="-0.471047393799" />
                  <Point X="-28.532875" Y="-0.220408432007" />
                  <Point X="-28.517494140625" Y="-0.214828460693" />
                  <Point X="-28.487728515625" Y="-0.199470611572" />
                  <Point X="-28.470232421875" Y="-0.18732723999" />
                  <Point X="-28.459974609375" Y="-0.1802084198" />
                  <Point X="-28.437517578125" Y="-0.158321914673" />
                  <Point X="-28.418271484375" Y="-0.132061584473" />
                  <Point X="-28.4041640625" Y="-0.104060661316" />
                  <Point X="-28.3949140625" Y="-0.074253738403" />
                  <Point X="-28.390646484375" Y="-0.046096931458" />
                  <Point X="-28.390646484375" Y="-0.01646320343" />
                  <Point X="-28.3949140625" Y="0.011693604469" />
                  <Point X="-28.4041640625" Y="0.041500526428" />
                  <Point X="-28.418271484375" Y="0.069501594543" />
                  <Point X="-28.437517578125" Y="0.095761787415" />
                  <Point X="-28.459974609375" Y="0.117648284912" />
                  <Point X="-28.477470703125" Y="0.129791671753" />
                  <Point X="-28.4855234375" Y="0.134804244995" />
                  <Point X="-28.51134765625" Y="0.149142196655" />
                  <Point X="-28.532875" Y="0.157848297119" />
                  <Point X="-29.0812421875" Y="0.304783172607" />
                  <Point X="-29.891814453125" Y="0.521975402832" />
                  <Point X="-29.855572265625" Y="0.766900756836" />
                  <Point X="-29.82448828125" Y="0.97696875" />
                  <Point X="-29.7281640625" Y="1.332435302734" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-29.436181640625" Y="1.388068115234" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744984375" Y="1.299341674805" />
                  <Point X="-28.723423828125" Y="1.301227783203" />
                  <Point X="-28.703134765625" Y="1.305263427734" />
                  <Point X="-28.664412109375" Y="1.317473022461" />
                  <Point X="-28.641708984375" Y="1.324630859375" />
                  <Point X="-28.62277734375" Y="1.332961303711" />
                  <Point X="-28.604033203125" Y="1.343783203125" />
                  <Point X="-28.5873515625" Y="1.356014892578" />
                  <Point X="-28.573712890625" Y="1.371567382813" />
                  <Point X="-28.561298828125" Y="1.389297241211" />
                  <Point X="-28.551349609375" Y="1.407432983398" />
                  <Point X="-28.5358125" Y="1.444945800781" />
                  <Point X="-28.526703125" Y="1.466937255859" />
                  <Point X="-28.520916015625" Y="1.486788330078" />
                  <Point X="-28.51715625" Y="1.508104492188" />
                  <Point X="-28.515802734375" Y="1.528750244141" />
                  <Point X="-28.518951171875" Y="1.54919934082" />
                  <Point X="-28.5245546875" Y="1.570106811523" />
                  <Point X="-28.532048828125" Y="1.589378417969" />
                  <Point X="-28.550796875" Y="1.62539440918" />
                  <Point X="-28.5617890625" Y="1.646508300781" />
                  <Point X="-28.57328515625" Y="1.66371081543" />
                  <Point X="-28.587197265625" Y="1.680289306641" />
                  <Point X="-28.60213671875" Y="1.694590332031" />
                  <Point X="-28.916681640625" Y="1.93594934082" />
                  <Point X="-29.351859375" Y="2.269873535156" />
                  <Point X="-29.201939453125" Y="2.526723388672" />
                  <Point X="-29.0811484375" Y="2.733664550781" />
                  <Point X="-28.826" Y="3.061624267578" />
                  <Point X="-28.75050390625" Y="3.158662353516" />
                  <Point X="-28.61846875" Y="3.082431152344" />
                  <Point X="-28.20665625" Y="2.844671142578" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.092861328125" Y="2.821077880859" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.04056640625" Y="2.818762939453" />
                  <Point X="-28.019109375" Y="2.821587646484" />
                  <Point X="-27.999017578125" Y="2.82650390625" />
                  <Point X="-27.98046484375" Y="2.835652099609" />
                  <Point X="-27.9622109375" Y="2.847280761719" />
                  <Point X="-27.946078125" Y="2.860228515625" />
                  <Point X="-27.907796875" Y="2.898510009766" />
                  <Point X="-27.885353515625" Y="2.920951904297" />
                  <Point X="-27.87241015625" Y="2.937078613281" />
                  <Point X="-27.860779296875" Y="2.955333007812" />
                  <Point X="-27.85162890625" Y="2.973884521484" />
                  <Point X="-27.8467109375" Y="2.993976806641" />
                  <Point X="-27.843884765625" Y="3.015435791016" />
                  <Point X="-27.84343359375" Y="3.036120605469" />
                  <Point X="-27.84815234375" Y="3.090052734375" />
                  <Point X="-27.85091796875" Y="3.121670166016" />
                  <Point X="-27.854955078125" Y="3.141961181641" />
                  <Point X="-27.86146484375" Y="3.162604248047" />
                  <Point X="-27.869794921875" Y="3.181532958984" />
                  <Point X="-28.0091796875" Y="3.422954101562" />
                  <Point X="-28.18333203125" Y="3.724595703125" />
                  <Point X="-27.910990234375" Y="3.933397216797" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.298767578125" Y="4.317947265625" />
                  <Point X="-27.16703515625" Y="4.391133789062" />
                  <Point X="-27.0431953125" Y="4.229741699219" />
                  <Point X="-27.028888671875" Y="4.214796875" />
                  <Point X="-27.012306640625" Y="4.200883789062" />
                  <Point X="-26.995111328125" Y="4.18939453125" />
                  <Point X="-26.9350859375" Y="4.158146484375" />
                  <Point X="-26.89989453125" Y="4.139827636719" />
                  <Point X="-26.88061328125" Y="4.132330078125" />
                  <Point X="-26.85970703125" Y="4.126728515625" />
                  <Point X="-26.839263671875" Y="4.12358203125" />
                  <Point X="-26.818623046875" Y="4.124935546875" />
                  <Point X="-26.79730859375" Y="4.128694335938" />
                  <Point X="-26.777451171875" Y="4.134481933594" />
                  <Point X="-26.7149296875" Y="4.160379394531" />
                  <Point X="-26.67827734375" Y="4.175561523437" />
                  <Point X="-26.66014453125" Y="4.185509765625" />
                  <Point X="-26.6424140625" Y="4.197924316406" />
                  <Point X="-26.626859375" Y="4.211565429688" />
                  <Point X="-26.614626953125" Y="4.22825" />
                  <Point X="-26.603806640625" Y="4.246994140625" />
                  <Point X="-26.595478515625" Y="4.265921875" />
                  <Point X="-26.57512890625" Y="4.330462402344" />
                  <Point X="-26.56319921875" Y="4.368298339844" />
                  <Point X="-26.5591640625" Y="4.388583984375" />
                  <Point X="-26.55727734375" Y="4.410145019531" />
                  <Point X="-26.557728515625" Y="4.430827148438" />
                  <Point X="-26.573576171875" Y="4.551193359375" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.221966796875" Y="4.733456054688" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.46246484375" Y="4.86682421875" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.255728515625" Y="4.740974609375" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.78236328125" Y="4.55285546875" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.39375" Y="4.856642089844" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.75290625" Y="4.7344296875" />
                  <Point X="-23.51897265625" Y="4.677950195312" />
                  <Point X="-23.257171875" Y="4.582994140625" />
                  <Point X="-23.105353515625" Y="4.527928222656" />
                  <Point X="-22.851673828125" Y="4.409290527344" />
                  <Point X="-22.705423828125" Y="4.340893554688" />
                  <Point X="-22.46033203125" Y="4.198103515625" />
                  <Point X="-22.31902734375" Y="4.115777832031" />
                  <Point X="-22.087888671875" Y="3.951407226562" />
                  <Point X="-22.05673828125" Y="3.929254150391" />
                  <Point X="-22.309228515625" Y="3.491927978516" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857919921875" Y="2.539936767578" />
                  <Point X="-22.866921875" Y="2.516058105469" />
                  <Point X="-22.88045703125" Y="2.465443847656" />
                  <Point X="-22.888392578125" Y="2.435771728516" />
                  <Point X="-22.891380859375" Y="2.417936279297" />
                  <Point X="-22.892271484375" Y="2.380953369141" />
                  <Point X="-22.886994140625" Y="2.337186523438" />
                  <Point X="-22.883900390625" Y="2.311528564453" />
                  <Point X="-22.878556640625" Y="2.289601074219" />
                  <Point X="-22.8702890625" Y="2.267511474609" />
                  <Point X="-22.859927734375" Y="2.247469482422" />
                  <Point X="-22.832845703125" Y="2.207558349609" />
                  <Point X="-22.81696875" Y="2.184160888672" />
                  <Point X="-22.805529296875" Y="2.170325439453" />
                  <Point X="-22.7783984375" Y="2.145592529297" />
                  <Point X="-22.73848828125" Y="2.118510986328" />
                  <Point X="-22.71508984375" Y="2.102635009766" />
                  <Point X="-22.695044921875" Y="2.092271972656" />
                  <Point X="-22.672958984375" Y="2.084006591797" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.60726953125" Y="2.073385986328" />
                  <Point X="-22.581611328125" Y="2.070291992188" />
                  <Point X="-22.563533203125" Y="2.069845458984" />
                  <Point X="-22.52679296875" Y="2.074170898438" />
                  <Point X="-22.476177734375" Y="2.087705810547" />
                  <Point X="-22.446505859375" Y="2.095640625" />
                  <Point X="-22.4347109375" Y="2.099639648438" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.859912109375" Y="2.428584228516" />
                  <Point X="-21.032673828125" Y="2.906190429688" />
                  <Point X="-20.96055078125" Y="2.805955566406" />
                  <Point X="-20.876720703125" Y="2.689452392578" />
                  <Point X="-20.747875" Y="2.476532470703" />
                  <Point X="-20.73780078125" Y="2.459883789063" />
                  <Point X="-21.054056640625" Y="2.217211914062" />
                  <Point X="-21.76921484375" Y="1.668451171875" />
                  <Point X="-21.778568359375" Y="1.660246582031" />
                  <Point X="-21.79602734375" Y="1.641626831055" />
                  <Point X="-21.832453125" Y="1.594105102539" />
                  <Point X="-21.85380859375" Y="1.566245727539" />
                  <Point X="-21.86339453125" Y="1.550908813477" />
                  <Point X="-21.8783671875" Y="1.517088500977" />
                  <Point X="-21.8919375" Y="1.468568237305" />
                  <Point X="-21.899892578125" Y="1.440124023438" />
                  <Point X="-21.90334765625" Y="1.417824707031" />
                  <Point X="-21.9041640625" Y="1.394255249023" />
                  <Point X="-21.902259765625" Y="1.371766357422" />
                  <Point X="-21.891119140625" Y="1.317781616211" />
                  <Point X="-21.88458984375" Y="1.286133544922" />
                  <Point X="-21.8793203125" Y="1.268981689453" />
                  <Point X="-21.863716796875" Y="1.235741577148" />
                  <Point X="-21.833419921875" Y="1.189692016602" />
                  <Point X="-21.81566015625" Y="1.162696166992" />
                  <Point X="-21.801109375" Y="1.145454101562" />
                  <Point X="-21.783865234375" Y="1.129362915039" />
                  <Point X="-21.76565234375" Y="1.116034667969" />
                  <Point X="-21.721748046875" Y="1.091320556641" />
                  <Point X="-21.696009765625" Y="1.076832397461" />
                  <Point X="-21.6794765625" Y="1.069500976563" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.58451953125" Y="1.051593261719" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.987857421875" Y="1.115962402344" />
                  <Point X="-20.22316015625" Y="1.21663659668" />
                  <Point X="-20.190064453125" Y="1.080690063477" />
                  <Point X="-20.154060546875" Y="0.93278894043" />
                  <Point X="-20.11345703125" Y="0.672008972168" />
                  <Point X="-20.1091328125" Y="0.644238891602" />
                  <Point X="-20.46383203125" Y="0.549197387695" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.2952109375" Y="0.325585296631" />
                  <Point X="-21.318453125" Y="0.315067901611" />
                  <Point X="-21.3767734375" Y="0.281357818604" />
                  <Point X="-21.410962890625" Y="0.261595489502" />
                  <Point X="-21.425685546875" Y="0.251097213745" />
                  <Point X="-21.45246875" Y="0.225576538086" />
                  <Point X="-21.4874609375" Y="0.18098828125" />
                  <Point X="-21.507974609375" Y="0.154848907471" />
                  <Point X="-21.51969921875" Y="0.135567443848" />
                  <Point X="-21.52947265625" Y="0.114103675842" />
                  <Point X="-21.536318359375" Y="0.092603935242" />
                  <Point X="-21.547982421875" Y="0.031698511124" />
                  <Point X="-21.5548203125" Y="-0.004006679058" />
                  <Point X="-21.556515625" Y="-0.021875440598" />
                  <Point X="-21.5548203125" Y="-0.058553455353" />
                  <Point X="-21.54315625" Y="-0.119458877563" />
                  <Point X="-21.536318359375" Y="-0.155164077759" />
                  <Point X="-21.52947265625" Y="-0.176663803101" />
                  <Point X="-21.51969921875" Y="-0.198127578735" />
                  <Point X="-21.507974609375" Y="-0.217409042358" />
                  <Point X="-21.472982421875" Y="-0.261997314453" />
                  <Point X="-21.45246875" Y="-0.28813684082" />
                  <Point X="-21.439998046875" Y="-0.301237091064" />
                  <Point X="-21.410962890625" Y="-0.324155456543" />
                  <Point X="-21.352642578125" Y="-0.357865661621" />
                  <Point X="-21.318453125" Y="-0.377627868652" />
                  <Point X="-21.307291015625" Y="-0.383137695312" />
                  <Point X="-21.283419921875" Y="-0.392149963379" />
                  <Point X="-20.802943359375" Y="-0.520892944336" />
                  <Point X="-20.10852734375" Y="-0.706961730957" />
                  <Point X="-20.124875" Y="-0.815397644043" />
                  <Point X="-20.144974609375" Y="-0.948725036621" />
                  <Point X="-20.196998046875" Y="-1.176693115234" />
                  <Point X="-20.19882421875" Y="-1.18469921875" />
                  <Point X="-20.625126953125" Y="-1.128575561523" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710266113" />
                  <Point X="-21.62533984375" Y="-1.005508666992" />
                  <Point X="-21.739802734375" Y="-1.030387695312" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.8360234375" Y="-1.056596313477" />
                  <Point X="-21.8638515625" Y="-1.073488891602" />
                  <Point X="-21.8876015625" Y="-1.093960327148" />
                  <Point X="-21.956787109375" Y="-1.177168212891" />
                  <Point X="-21.997345703125" Y="-1.225947998047" />
                  <Point X="-22.012068359375" Y="-1.250335205078" />
                  <Point X="-22.02341015625" Y="-1.277720581055" />
                  <Point X="-22.030240234375" Y="-1.305365844727" />
                  <Point X="-22.04015625" Y="-1.413124145508" />
                  <Point X="-22.04596875" Y="-1.476296142578" />
                  <Point X="-22.043650390625" Y="-1.507567138672" />
                  <Point X="-22.03591796875" Y="-1.539188232422" />
                  <Point X="-22.023546875" Y="-1.567996337891" />
                  <Point X="-21.960203125" Y="-1.666525512695" />
                  <Point X="-21.92306640625" Y="-1.724286987305" />
                  <Point X="-21.913064453125" Y="-1.73723840332" />
                  <Point X="-21.889369140625" Y="-1.760909301758" />
                  <Point X="-21.443486328125" Y="-2.103048095703" />
                  <Point X="-20.786876953125" Y="-2.606882568359" />
                  <Point X="-20.818525390625" Y="-2.658094970703" />
                  <Point X="-20.875197265625" Y="-2.749796875" />
                  <Point X="-20.971017578125" Y="-2.885945068359" />
                  <Point X="-21.35250390625" Y="-2.665693603516" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.245775390625" Y="-2.159825195312" />
                  <Point X="-22.38200390625" Y="-2.13522265625" />
                  <Point X="-22.461865234375" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135176757812" />
                  <Point X="-22.668337890625" Y="-2.19473828125" />
                  <Point X="-22.73468359375" Y="-2.229655761719" />
                  <Point X="-22.757615234375" Y="-2.246549560547" />
                  <Point X="-22.77857421875" Y="-2.267509033203" />
                  <Point X="-22.795466796875" Y="-2.290438476562" />
                  <Point X="-22.855029296875" Y="-2.403610351562" />
                  <Point X="-22.889947265625" Y="-2.469956542969" />
                  <Point X="-22.899771484375" Y="-2.499735351563" />
                  <Point X="-22.904728515625" Y="-2.531908935547" />
                  <Point X="-22.90432421875" Y="-2.563259277344" />
                  <Point X="-22.879720703125" Y="-2.699487304688" />
                  <Point X="-22.865296875" Y="-2.779349609375" />
                  <Point X="-22.86101171875" Y="-2.795140869141" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.56165234375" Y="-3.322354980469" />
                  <Point X="-22.138712890625" Y="-4.05490625" />
                  <Point X="-22.150908203125" Y="-4.063616210938" />
                  <Point X="-22.218134765625" Y="-4.111634277344" />
                  <Point X="-22.298232421875" Y="-4.163480957031" />
                  <Point X="-22.59546484375" Y="-3.776122802734" />
                  <Point X="-23.241451171875" Y="-2.934254882812" />
                  <Point X="-23.25249609375" Y="-2.922177490234" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.41243359375" Y="-2.814177490234" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.520009765625" Y="-2.751167236328" />
                  <Point X="-23.551630859375" Y="-2.743435546875" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.729841796875" Y="-2.754638427734" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.843634765625" Y="-2.769396972656" />
                  <Point X="-23.87101953125" Y="-2.780739746094" />
                  <Point X="-23.89540234375" Y="-2.7954609375" />
                  <Point X="-24.0088671875" Y="-2.889803710938" />
                  <Point X="-24.07538671875" Y="-2.945111328125" />
                  <Point X="-24.095857421875" Y="-2.968861328125" />
                  <Point X="-24.11275" Y="-2.996688232422" />
                  <Point X="-24.124375" Y="-3.02580859375" />
                  <Point X="-24.15830078125" Y="-3.181893310547" />
                  <Point X="-24.178189453125" Y="-3.273396484375" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.09905859375" Y="-3.939888427734" />
                  <Point X="-23.977935546875" Y="-4.859915527344" />
                  <Point X="-24.024318359375" Y="-4.870083007812" />
                  <Point X="-24.07068359375" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.058431640625" Y="-4.752634765625" />
                  <Point X="-26.14124609375" Y="-4.731328125" />
                  <Point X="-26.120775390625" Y="-4.575847167969" />
                  <Point X="-26.120076171875" Y="-4.568098144531" />
                  <Point X="-26.11944921875" Y="-4.541030273438" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497689453125" />
                  <Point X="-26.1614921875" Y="-4.305857421875" />
                  <Point X="-26.18386328125" Y="-4.193397949219" />
                  <Point X="-26.188125" Y="-4.17846875" />
                  <Point X="-26.19902734375" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135462402344" />
                  <Point X="-26.221740234375" Y="-4.107625488281" />
                  <Point X="-26.230576171875" Y="-4.094859863281" />
                  <Point X="-26.250208984375" Y="-4.070937011719" />
                  <Point X="-26.261005859375" Y="-4.059779785156" />
                  <Point X="-26.408056640625" Y="-3.930818115234" />
                  <Point X="-26.494265625" Y="-3.855215332031" />
                  <Point X="-26.506736328125" Y="-3.845965820312" />
                  <Point X="-26.53301953125" Y="-3.829621582031" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.831986328125" Y="-3.783377441406" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166259766" />
                  <Point X="-27.008005859375" Y="-3.781946044922" />
                  <Point X="-27.0390546875" Y="-3.790265869141" />
                  <Point X="-27.053673828125" Y="-3.795496337891" />
                  <Point X="-27.081865234375" Y="-3.80826953125" />
                  <Point X="-27.0954375" Y="-3.815812255859" />
                  <Point X="-27.258064453125" Y="-3.9244765625" />
                  <Point X="-27.35340234375" Y="-3.988180175781" />
                  <Point X="-27.35968359375" Y="-3.992759765625" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032770507812" />
                  <Point X="-27.410470703125" Y="-4.041629150391" />
                  <Point X="-27.493587890625" Y="-4.149948730469" />
                  <Point X="-27.503203125" Y="-4.162478515625" />
                  <Point X="-27.595595703125" Y="-4.105270507813" />
                  <Point X="-27.74759375" Y="-4.011156738281" />
                  <Point X="-27.980861328125" Y="-3.831548583984" />
                  <Point X="-27.810794921875" Y="-3.536983886719" />
                  <Point X="-27.341486328125" Y="-2.724119140625" />
                  <Point X="-27.33484765625" Y="-2.710079589844" />
                  <Point X="-27.323947265625" Y="-2.681114501953" />
                  <Point X="-27.319685546875" Y="-2.666188964844" />
                  <Point X="-27.3134140625" Y="-2.634662109375" />
                  <Point X="-27.311638671875" Y="-2.619238769531" />
                  <Point X="-27.310625" Y="-2.588304931641" />
                  <Point X="-27.3146640625" Y="-2.557616699219" />
                  <Point X="-27.3236484375" Y="-2.527999023438" />
                  <Point X="-27.32935546875" Y="-2.513559082031" />
                  <Point X="-27.343572265625" Y="-2.484729736328" />
                  <Point X="-27.3515546875" Y="-2.471412841797" />
                  <Point X="-27.36958203125" Y="-2.446254638672" />
                  <Point X="-27.379626953125" Y="-2.434413330078" />
                  <Point X="-27.396970703125" Y="-2.417069580078" />
                  <Point X="-27.408806640625" Y="-2.407026855469" />
                  <Point X="-27.43395703125" Y="-2.389002441406" />
                  <Point X="-27.447271484375" Y="-2.381020751953" />
                  <Point X="-27.4761015625" Y="-2.366800292969" />
                  <Point X="-27.49054296875" Y="-2.361092285156" />
                  <Point X="-27.520162109375" Y="-2.352105224609" />
                  <Point X="-27.550849609375" Y="-2.348063476562" />
                  <Point X="-27.581787109375" Y="-2.349074707031" />
                  <Point X="-27.59721484375" Y="-2.350848632812" />
                  <Point X="-27.628744140625" Y="-2.357119140625" />
                  <Point X="-27.64367578125" Y="-2.361383056641" />
                  <Point X="-27.67264453125" Y="-2.372285888672" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.163216796875" Y="-2.654052246094" />
                  <Point X="-28.7930859375" Y="-3.017707763672" />
                  <Point X="-28.8839375" Y="-2.898349365234" />
                  <Point X="-29.004029296875" Y="-2.740572509766" />
                  <Point X="-29.181265625" Y="-2.443374267578" />
                  <Point X="-28.8692578125" Y="-2.203962158203" />
                  <Point X="-28.048123046875" Y="-1.573882080078" />
                  <Point X="-28.036482421875" Y="-1.563310668945" />
                  <Point X="-28.01510546875" Y="-1.540391479492" />
                  <Point X="-28.005369140625" Y="-1.528043701172" />
                  <Point X="-27.987404296875" Y="-1.500913330078" />
                  <Point X="-27.979837890625" Y="-1.487127563477" />
                  <Point X="-27.96708203125" Y="-1.458498535156" />
                  <Point X="-27.961892578125" Y="-1.443655517578" />
                  <Point X="-27.9541875" Y="-1.413908203125" />
                  <Point X="-27.951552734375" Y="-1.398808227539" />
                  <Point X="-27.948748046875" Y="-1.368383544922" />
                  <Point X="-27.948578125" Y="-1.353058837891" />
                  <Point X="-27.95078515625" Y="-1.321387939453" />
                  <Point X="-27.953080078125" Y="-1.306233032227" />
                  <Point X="-27.960080078125" Y="-1.276483642578" />
                  <Point X="-27.97177734375" Y="-1.248241821289" />
                  <Point X="-27.98786328125" Y="-1.222254882812" />
                  <Point X="-27.99695703125" Y="-1.209915405273" />
                  <Point X="-28.017791015625" Y="-1.185958618164" />
                  <Point X="-28.02874609375" Y="-1.175243652344" />
                  <Point X="-28.0522421875" Y="-1.155712402344" />
                  <Point X="-28.064783203125" Y="-1.146895874023" />
                  <Point X="-28.091265625" Y="-1.131309204102" />
                  <Point X="-28.105431640625" Y="-1.124482421875" />
                  <Point X="-28.134693359375" Y="-1.113258422852" />
                  <Point X="-28.1497890625" Y="-1.108861206055" />
                  <Point X="-28.18167578125" Y="-1.102379150391" />
                  <Point X="-28.197294921875" Y="-1.100532714844" />
                  <Point X="-28.22862109375" Y="-1.09944152832" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-28.845908203125" Y="-1.179395996094" />
                  <Point X="-29.66091796875" Y="-1.286694213867" />
                  <Point X="-29.693796875" Y="-1.157980957031" />
                  <Point X="-29.740763671875" Y="-0.974110961914" />
                  <Point X="-29.786451171875" Y="-0.654654663086" />
                  <Point X="-29.44368359375" Y="-0.562810302734" />
                  <Point X="-28.508287109375" Y="-0.312171356201" />
                  <Point X="-28.5004765625" Y="-0.309713134766" />
                  <Point X="-28.47393359375" Y="-0.299253204346" />
                  <Point X="-28.44416796875" Y="-0.28389541626" />
                  <Point X="-28.433560546875" Y="-0.27751473999" />
                  <Point X="-28.416064453125" Y="-0.265371368408" />
                  <Point X="-28.393669921875" Y="-0.248242279053" />
                  <Point X="-28.371212890625" Y="-0.226355773926" />
                  <Point X="-28.360892578125" Y="-0.214479629517" />
                  <Point X="-28.341646484375" Y="-0.188219299316" />
                  <Point X="-28.333431640625" Y="-0.174805938721" />
                  <Point X="-28.31932421875" Y="-0.146805084229" />
                  <Point X="-28.313431640625" Y="-0.132217453003" />
                  <Point X="-28.304181640625" Y="-0.102410545349" />
                  <Point X="-28.300986328125" Y="-0.088489784241" />
                  <Point X="-28.29671875" Y="-0.060333034515" />
                  <Point X="-28.295646484375" Y="-0.046096897125" />
                  <Point X="-28.295646484375" Y="-0.016463129044" />
                  <Point X="-28.29671875" Y="-0.002227139235" />
                  <Point X="-28.300986328125" Y="0.025929613113" />
                  <Point X="-28.304181640625" Y="0.039850372314" />
                  <Point X="-28.313431640625" Y="0.069657287598" />
                  <Point X="-28.31932421875" Y="0.084244766235" />
                  <Point X="-28.333431640625" Y="0.112245765686" />
                  <Point X="-28.341646484375" Y="0.125659568787" />
                  <Point X="-28.360892578125" Y="0.151919754028" />
                  <Point X="-28.371212890625" Y="0.163795608521" />
                  <Point X="-28.393669921875" Y="0.185682113647" />
                  <Point X="-28.405806640625" Y="0.195692459106" />
                  <Point X="-28.423302734375" Y="0.207835830688" />
                  <Point X="-28.439408203125" Y="0.217861190796" />
                  <Point X="-28.465232421875" Y="0.232199142456" />
                  <Point X="-28.47573046875" Y="0.237212631226" />
                  <Point X="-28.4972578125" Y="0.245918655396" />
                  <Point X="-28.508287109375" Y="0.249611190796" />
                  <Point X="-29.056654296875" Y="0.396546173096" />
                  <Point X="-29.7854453125" Y="0.591824829102" />
                  <Point X="-29.761595703125" Y="0.752994812012" />
                  <Point X="-29.73133203125" Y="0.95752130127" />
                  <Point X="-29.636470703125" Y="1.307588256836" />
                  <Point X="-29.6335859375" Y="1.318236938477" />
                  <Point X="-29.44858203125" Y="1.293880859375" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815551758" />
                  <Point X="-28.747056640625" Y="1.204364257812" />
                  <Point X="-28.736705078125" Y="1.204703125" />
                  <Point X="-28.71514453125" Y="1.206589233398" />
                  <Point X="-28.704890625" Y="1.208053100586" />
                  <Point X="-28.6846015625" Y="1.212088745117" />
                  <Point X="-28.67456640625" Y="1.214660522461" />
                  <Point X="-28.63584375" Y="1.226870239258" />
                  <Point X="-28.613140625" Y="1.234028076172" />
                  <Point X="-28.603447265625" Y="1.237676757812" />
                  <Point X="-28.584515625" Y="1.246007202148" />
                  <Point X="-28.57527734375" Y="1.250688842773" />
                  <Point X="-28.556533203125" Y="1.261510742188" />
                  <Point X="-28.547857421875" Y="1.267171386719" />
                  <Point X="-28.53117578125" Y="1.279403076172" />
                  <Point X="-28.51592578125" Y="1.293378295898" />
                  <Point X="-28.502287109375" Y="1.308930786133" />
                  <Point X="-28.495892578125" Y="1.317079101562" />
                  <Point X="-28.483478515625" Y="1.334808837891" />
                  <Point X="-28.478009765625" Y="1.343604736328" />
                  <Point X="-28.468060546875" Y="1.361740600586" />
                  <Point X="-28.463580078125" Y="1.371080444336" />
                  <Point X="-28.44804296875" Y="1.408593261719" />
                  <Point X="-28.43893359375" Y="1.430584716797" />
                  <Point X="-28.4355" Y="1.440348999023" />
                  <Point X="-28.429712890625" Y="1.460200073242" />
                  <Point X="-28.427359375" Y="1.470286865234" />
                  <Point X="-28.423599609375" Y="1.491603027344" />
                  <Point X="-28.422359375" Y="1.501889770508" />
                  <Point X="-28.421005859375" Y="1.522535522461" />
                  <Point X="-28.421908203125" Y="1.543206542969" />
                  <Point X="-28.425056640625" Y="1.563655761719" />
                  <Point X="-28.427189453125" Y="1.573792724609" />
                  <Point X="-28.43279296875" Y="1.594700195312" />
                  <Point X="-28.436013671875" Y="1.604537719727" />
                  <Point X="-28.4435078125" Y="1.623809326172" />
                  <Point X="-28.44778125" Y="1.633243286133" />
                  <Point X="-28.466529296875" Y="1.669259277344" />
                  <Point X="-28.477521484375" Y="1.690373168945" />
                  <Point X="-28.482802734375" Y="1.69929296875" />
                  <Point X="-28.494298828125" Y="1.716495483398" />
                  <Point X="-28.500513671875" Y="1.724778564453" />
                  <Point X="-28.51442578125" Y="1.741356933594" />
                  <Point X="-28.52150390625" Y="1.748914916992" />
                  <Point X="-28.536443359375" Y="1.763215820313" />
                  <Point X="-28.5443046875" Y="1.769958862305" />
                  <Point X="-28.858849609375" Y="2.011317871094" />
                  <Point X="-29.22761328125" Y="2.294281005859" />
                  <Point X="-29.119892578125" Y="2.478833984375" />
                  <Point X="-29.00228515625" Y="2.680321533203" />
                  <Point X="-28.75101953125" Y="3.003290039062" />
                  <Point X="-28.726337890625" Y="3.035013671875" />
                  <Point X="-28.66596875" Y="3.000158935547" />
                  <Point X="-28.25415625" Y="2.762398925781" />
                  <Point X="-28.24491796875" Y="2.757716796875" />
                  <Point X="-28.225984375" Y="2.749385742188" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.101140625" Y="2.726439453125" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.038494140625" Y="2.723785644531" />
                  <Point X="-28.02816796875" Y="2.724575683594" />
                  <Point X="-28.0067109375" Y="2.727400390625" />
                  <Point X="-27.996529296875" Y="2.729310058594" />
                  <Point X="-27.9764375" Y="2.734226318359" />
                  <Point X="-27.95700390625" Y="2.741299072266" />
                  <Point X="-27.938451171875" Y="2.750447265625" />
                  <Point X="-27.929421875" Y="2.755529296875" />
                  <Point X="-27.91116796875" Y="2.767157958984" />
                  <Point X="-27.902748046875" Y="2.77319140625" />
                  <Point X="-27.886615234375" Y="2.786139160156" />
                  <Point X="-27.87890234375" Y="2.793053466797" />
                  <Point X="-27.84062109375" Y="2.831334960938" />
                  <Point X="-27.818177734375" Y="2.853776855469" />
                  <Point X="-27.811265625" Y="2.86148828125" />
                  <Point X="-27.798322265625" Y="2.877614990234" />
                  <Point X="-27.792291015625" Y="2.886030273438" />
                  <Point X="-27.78066015625" Y="2.904284667969" />
                  <Point X="-27.775580078125" Y="2.913308837891" />
                  <Point X="-27.7664296875" Y="2.931860351562" />
                  <Point X="-27.759353515625" Y="2.951298339844" />
                  <Point X="-27.754435546875" Y="2.971390625" />
                  <Point X="-27.7525234375" Y="2.981572265625" />
                  <Point X="-27.749697265625" Y="3.00303125" />
                  <Point X="-27.748908203125" Y="3.013364257812" />
                  <Point X="-27.74845703125" Y="3.034049072266" />
                  <Point X="-27.748794921875" Y="3.044400878906" />
                  <Point X="-27.753513671875" Y="3.098333007812" />
                  <Point X="-27.756279296875" Y="3.129950439453" />
                  <Point X="-27.757744140625" Y="3.140208007812" />
                  <Point X="-27.76178125" Y="3.160499023438" />
                  <Point X="-27.764353515625" Y="3.170532470703" />
                  <Point X="-27.77086328125" Y="3.191175537109" />
                  <Point X="-27.77451171875" Y="3.200869873047" />
                  <Point X="-27.782841796875" Y="3.219798583984" />
                  <Point X="-27.7875234375" Y="3.229032958984" />
                  <Point X="-27.926908203125" Y="3.470454101562" />
                  <Point X="-28.05938671875" Y="3.699914550781" />
                  <Point X="-27.8531875" Y="3.858005371094" />
                  <Point X="-27.64836328125" Y="4.015042236328" />
                  <Point X="-27.252630859375" Y="4.234903320312" />
                  <Point X="-27.1925234375" Y="4.268296875" />
                  <Point X="-27.118564453125" Y="4.171909667969" />
                  <Point X="-27.1118203125" Y="4.164047851562" />
                  <Point X="-27.097513671875" Y="4.149103027344" />
                  <Point X="-27.089951171875" Y="4.142020507812" />
                  <Point X="-27.073369140625" Y="4.128107421875" />
                  <Point X="-27.065083984375" Y="4.121893554688" />
                  <Point X="-27.047888671875" Y="4.110404296875" />
                  <Point X="-27.038978515625" Y="4.10512890625" />
                  <Point X="-26.978953125" Y="4.073880859375" />
                  <Point X="-26.94376171875" Y="4.055562011719" />
                  <Point X="-26.93432421875" Y="4.051286132812" />
                  <Point X="-26.91504296875" Y="4.043788574219" />
                  <Point X="-26.90519921875" Y="4.040566894531" />
                  <Point X="-26.88429296875" Y="4.034965332031" />
                  <Point X="-26.874158203125" Y="4.032834228516" />
                  <Point X="-26.85371484375" Y="4.029687744141" />
                  <Point X="-26.833046875" Y="4.028785644531" />
                  <Point X="-26.81240625" Y="4.030139160156" />
                  <Point X="-26.802125" Y="4.031379150391" />
                  <Point X="-26.780810546875" Y="4.035137939453" />
                  <Point X="-26.7707265625" Y="4.037489257812" />
                  <Point X="-26.750869140625" Y="4.043276855469" />
                  <Point X="-26.741095703125" Y="4.046713378906" />
                  <Point X="-26.67857421875" Y="4.072610839844" />
                  <Point X="-26.641921875" Y="4.08779296875" />
                  <Point X="-26.63258203125" Y="4.092272949219" />
                  <Point X="-26.61444921875" Y="4.102221191406" />
                  <Point X="-26.60565625" Y="4.107689453125" />
                  <Point X="-26.58792578125" Y="4.120104003906" />
                  <Point X="-26.579775390625" Y="4.126499511719" />
                  <Point X="-26.564220703125" Y="4.140140625" />
                  <Point X="-26.550244140625" Y="4.15539453125" />
                  <Point X="-26.53801171875" Y="4.172079101562" />
                  <Point X="-26.5323515625" Y="4.180755371094" />
                  <Point X="-26.52153125" Y="4.199499511719" />
                  <Point X="-26.5168515625" Y="4.208734375" />
                  <Point X="-26.5085234375" Y="4.227662109375" />
                  <Point X="-26.504875" Y="4.237354980469" />
                  <Point X="-26.484525390625" Y="4.301895507813" />
                  <Point X="-26.472595703125" Y="4.339731445312" />
                  <Point X="-26.470025390625" Y="4.349764160156" />
                  <Point X="-26.465990234375" Y="4.370049804688" />
                  <Point X="-26.464525390625" Y="4.380302734375" />
                  <Point X="-26.462638671875" Y="4.401863769531" />
                  <Point X="-26.46230078125" Y="4.412216796875" />
                  <Point X="-26.462751953125" Y="4.432898925781" />
                  <Point X="-26.463541015625" Y="4.443228027344" />
                  <Point X="-26.479265625" Y="4.562655273438" />
                  <Point X="-26.1963203125" Y="4.641983398438" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.451421875" Y="4.772468261719" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.3474921875" Y="4.71638671875" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.690599609375" Y="4.528267578125" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.40364453125" Y="4.762158691406" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.775201171875" Y="4.642083007812" />
                  <Point X="-23.546404296875" Y="4.58684375" />
                  <Point X="-23.289564453125" Y="4.493687011719" />
                  <Point X="-23.14173828125" Y="4.440069335938" />
                  <Point X="-22.89191796875" Y="4.323236328125" />
                  <Point X="-22.74955078125" Y="4.256654785156" />
                  <Point X="-22.508154296875" Y="4.116018066406" />
                  <Point X="-22.370580078125" Y="4.035865478516" />
                  <Point X="-22.18221875" Y="3.901915283203" />
                  <Point X="-22.3915" Y="3.539427978516" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.937619140625" Y="2.593114257812" />
                  <Point X="-22.9468125" Y="2.573448242188" />
                  <Point X="-22.955814453125" Y="2.549569580078" />
                  <Point X="-22.958697265625" Y="2.540600341797" />
                  <Point X="-22.972232421875" Y="2.489986083984" />
                  <Point X="-22.98016796875" Y="2.460313964844" />
                  <Point X="-22.9820859375" Y="2.451469970703" />
                  <Point X="-22.986353515625" Y="2.420223388672" />
                  <Point X="-22.987244140625" Y="2.383240478516" />
                  <Point X="-22.986587890625" Y="2.369580810547" />
                  <Point X="-22.981310546875" Y="2.325813964844" />
                  <Point X="-22.978216796875" Y="2.300156005859" />
                  <Point X="-22.97619921875" Y="2.289035400391" />
                  <Point X="-22.97085546875" Y="2.267107910156" />
                  <Point X="-22.967529296875" Y="2.256301025391" />
                  <Point X="-22.95926171875" Y="2.234211425781" />
                  <Point X="-22.954677734375" Y="2.223883544922" />
                  <Point X="-22.94431640625" Y="2.203841552734" />
                  <Point X="-22.9385390625" Y="2.194127441406" />
                  <Point X="-22.91145703125" Y="2.154216308594" />
                  <Point X="-22.895580078125" Y="2.130818847656" />
                  <Point X="-22.89018359375" Y="2.123625244141" />
                  <Point X="-22.869529296875" Y="2.100119384766" />
                  <Point X="-22.8423984375" Y="2.075386474609" />
                  <Point X="-22.831740234375" Y="2.066981933594" />
                  <Point X="-22.791830078125" Y="2.039900512695" />
                  <Point X="-22.768431640625" Y="2.024024536133" />
                  <Point X="-22.75871875" Y="2.018245727539" />
                  <Point X="-22.738673828125" Y="2.00788269043" />
                  <Point X="-22.728341796875" Y="2.003298461914" />
                  <Point X="-22.706255859375" Y="1.995033203125" />
                  <Point X="-22.695453125" Y="1.991708007812" />
                  <Point X="-22.673529296875" Y="1.986364990234" />
                  <Point X="-22.662408203125" Y="1.984346923828" />
                  <Point X="-22.618642578125" Y="1.979069213867" />
                  <Point X="-22.592984375" Y="1.975975219727" />
                  <Point X="-22.58395703125" Y="1.975320800781" />
                  <Point X="-22.55242578125" Y="1.975497070312" />
                  <Point X="-22.515685546875" Y="1.979822509766" />
                  <Point X="-22.502251953125" Y="1.982395507812" />
                  <Point X="-22.45163671875" Y="1.995930419922" />
                  <Point X="-22.42196484375" Y="2.003865234375" />
                  <Point X="-22.416001953125" Y="2.005671142578" />
                  <Point X="-22.395587890625" Y="2.013069458008" />
                  <Point X="-22.372341796875" Y="2.023574829102" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.812412109375" Y="2.346311767578" />
                  <Point X="-21.059595703125" Y="2.780950439453" />
                  <Point X="-21.0376640625" Y="2.750469970703" />
                  <Point X="-20.9560390625" Y="2.637031738281" />
                  <Point X="-20.863115234375" Y="2.483471923828" />
                  <Point X="-21.111888671875" Y="2.292580566406" />
                  <Point X="-21.827046875" Y="1.743819824219" />
                  <Point X="-21.831861328125" Y="1.739869262695" />
                  <Point X="-21.847869140625" Y="1.725226928711" />
                  <Point X="-21.865328125" Y="1.706607177734" />
                  <Point X="-21.87142578125" Y="1.699420288086" />
                  <Point X="-21.9078515625" Y="1.65189855957" />
                  <Point X="-21.92920703125" Y="1.624039306641" />
                  <Point X="-21.9343671875" Y="1.616597167969" />
                  <Point X="-21.95026171875" Y="1.589366333008" />
                  <Point X="-21.965234375" Y="1.555545898438" />
                  <Point X="-21.96985546875" Y="1.542676513672" />
                  <Point X="-21.98342578125" Y="1.49415625" />
                  <Point X="-21.991380859375" Y="1.465712036133" />
                  <Point X="-21.993771484375" Y="1.454669799805" />
                  <Point X="-21.9972265625" Y="1.432370483398" />
                  <Point X="-21.998291015625" Y="1.42111340332" />
                  <Point X="-21.999107421875" Y="1.397543945312" />
                  <Point X="-21.998826171875" Y="1.386239746094" />
                  <Point X="-21.996921875" Y="1.363750610352" />
                  <Point X="-21.995298828125" Y="1.352566162109" />
                  <Point X="-21.984158203125" Y="1.298581420898" />
                  <Point X="-21.97762890625" Y="1.26693347168" />
                  <Point X="-21.975400390625" Y="1.258233886719" />
                  <Point X="-21.96531640625" Y="1.22861340332" />
                  <Point X="-21.949712890625" Y="1.195373291016" />
                  <Point X="-21.943080078125" Y="1.183526733398" />
                  <Point X="-21.912783203125" Y="1.137477050781" />
                  <Point X="-21.8950234375" Y="1.110481445312" />
                  <Point X="-21.88826171875" Y="1.101426513672" />
                  <Point X="-21.8737109375" Y="1.084184448242" />
                  <Point X="-21.865921875" Y="1.075997192383" />
                  <Point X="-21.848677734375" Y="1.05990612793" />
                  <Point X="-21.83996875" Y="1.052698486328" />
                  <Point X="-21.821755859375" Y="1.039370239258" />
                  <Point X="-21.812251953125" Y="1.033249511719" />
                  <Point X="-21.76834765625" Y="1.008535339355" />
                  <Point X="-21.742609375" Y="0.994047119141" />
                  <Point X="-21.73451953125" Y="0.989987915039" />
                  <Point X="-21.705318359375" Y="0.978083374023" />
                  <Point X="-21.66972265625" Y="0.968020996094" />
                  <Point X="-21.656328125" Y="0.96525769043" />
                  <Point X="-21.596966796875" Y="0.95741217041" />
                  <Point X="-21.562166015625" Y="0.952813049316" />
                  <Point X="-21.555966796875" Y="0.952199829102" />
                  <Point X="-21.53428125" Y="0.951222900391" />
                  <Point X="-21.50878125" Y="0.952032287598" />
                  <Point X="-21.49939453125" Y="0.952797302246" />
                  <Point X="-20.97545703125" Y="1.021775085449" />
                  <Point X="-20.295296875" Y="1.111319946289" />
                  <Point X="-20.282369140625" Y="1.058218994141" />
                  <Point X="-20.2473125" Y="0.914209228516" />
                  <Point X="-20.216126953125" Y="0.713920959473" />
                  <Point X="-20.488419921875" Y="0.640960327148" />
                  <Point X="-21.3080078125" Y="0.421352813721" />
                  <Point X="-21.313970703125" Y="0.419543457031" />
                  <Point X="-21.334376953125" Y="0.412136260986" />
                  <Point X="-21.357619140625" Y="0.401618804932" />
                  <Point X="-21.365994140625" Y="0.397316619873" />
                  <Point X="-21.424314453125" Y="0.363606567383" />
                  <Point X="-21.45850390625" Y="0.343844177246" />
                  <Point X="-21.466119140625" Y="0.338944702148" />
                  <Point X="-21.491220703125" Y="0.319873840332" />
                  <Point X="-21.51800390625" Y="0.294353210449" />
                  <Point X="-21.527203125" Y="0.284226654053" />
                  <Point X="-21.5621953125" Y="0.239638305664" />
                  <Point X="-21.582708984375" Y="0.213498962402" />
                  <Point X="-21.589146484375" Y="0.204207199097" />
                  <Point X="-21.60087109375" Y="0.184925765991" />
                  <Point X="-21.606158203125" Y="0.174936080933" />
                  <Point X="-21.615931640625" Y="0.153472259521" />
                  <Point X="-21.619994140625" Y="0.142926727295" />
                  <Point X="-21.62683984375" Y="0.121426940918" />
                  <Point X="-21.629623046875" Y="0.110472694397" />
                  <Point X="-21.641287109375" Y="0.049567337036" />
                  <Point X="-21.648125" Y="0.013862178802" />
                  <Point X="-21.649396484375" Y="0.004966207981" />
                  <Point X="-21.6514140625" Y="-0.026261835098" />
                  <Point X="-21.64971875" Y="-0.062939880371" />
                  <Point X="-21.648125" Y="-0.076422203064" />
                  <Point X="-21.6364609375" Y="-0.137327713013" />
                  <Point X="-21.629623046875" Y="-0.173032867432" />
                  <Point X="-21.62683984375" Y="-0.183987121582" />
                  <Point X="-21.619994140625" Y="-0.205486755371" />
                  <Point X="-21.615931640625" Y="-0.216032440186" />
                  <Point X="-21.606158203125" Y="-0.237496261597" />
                  <Point X="-21.60087109375" Y="-0.247485946655" />
                  <Point X="-21.589146484375" Y="-0.26676739502" />
                  <Point X="-21.582708984375" Y="-0.276059143066" />
                  <Point X="-21.547716796875" Y="-0.320647338867" />
                  <Point X="-21.527203125" Y="-0.346786987305" />
                  <Point X="-21.52127734375" Y="-0.353638336182" />
                  <Point X="-21.498857421875" Y="-0.375806182861" />
                  <Point X="-21.469822265625" Y="-0.39872442627" />
                  <Point X="-21.45850390625" Y="-0.406404052734" />
                  <Point X="-21.40018359375" Y="-0.440114257812" />
                  <Point X="-21.365994140625" Y="-0.459876495361" />
                  <Point X="-21.36050390625" Y="-0.462814758301" />
                  <Point X="-21.340845703125" Y="-0.472014526367" />
                  <Point X="-21.316974609375" Y="-0.481026855469" />
                  <Point X="-21.3080078125" Y="-0.483912963867" />
                  <Point X="-20.82753125" Y="-0.612655883789" />
                  <Point X="-20.21512109375" Y="-0.776751098633" />
                  <Point X="-20.2188125" Y="-0.801235595703" />
                  <Point X="-20.238380859375" Y="-0.931035400391" />
                  <Point X="-20.272197265625" Y="-1.079219848633" />
                  <Point X="-20.6127265625" Y="-1.034388305664" />
                  <Point X="-21.563216796875" Y="-0.909253662109" />
                  <Point X="-21.571375" Y="-0.908535644531" />
                  <Point X="-21.59990234375" Y="-0.908042541504" />
                  <Point X="-21.63327734375" Y="-0.910840942383" />
                  <Point X="-21.645517578125" Y="-0.912676147461" />
                  <Point X="-21.75998046875" Y="-0.937555175781" />
                  <Point X="-21.82708203125" Y="-0.952140136719" />
                  <Point X="-21.842125" Y="-0.956742370605" />
                  <Point X="-21.871244140625" Y="-0.968366027832" />
                  <Point X="-21.8853203125" Y="-0.975387573242" />
                  <Point X="-21.9131484375" Y="-0.992280029297" />
                  <Point X="-21.925876953125" Y="-1.001530944824" />
                  <Point X="-21.949626953125" Y="-1.022002380371" />
                  <Point X="-21.9606484375" Y="-1.03322277832" />
                  <Point X="-22.029833984375" Y="-1.116430664063" />
                  <Point X="-22.070392578125" Y="-1.165210449219" />
                  <Point X="-22.078673828125" Y="-1.176849609375" />
                  <Point X="-22.093396484375" Y="-1.201236816406" />
                  <Point X="-22.099837890625" Y="-1.213984619141" />
                  <Point X="-22.1111796875" Y="-1.241369995117" />
                  <Point X="-22.11563671875" Y="-1.254934814453" />
                  <Point X="-22.122466796875" Y="-1.282580078125" />
                  <Point X="-22.12483984375" Y="-1.296660766602" />
                  <Point X="-22.134755859375" Y="-1.404418945312" />
                  <Point X="-22.140568359375" Y="-1.467590942383" />
                  <Point X="-22.140708984375" Y="-1.483319946289" />
                  <Point X="-22.138390625" Y="-1.514590942383" />
                  <Point X="-22.135931640625" Y="-1.53013293457" />
                  <Point X="-22.12819921875" Y="-1.56175402832" />
                  <Point X="-22.123208984375" Y="-1.576673950195" />
                  <Point X="-22.110837890625" Y="-1.605482055664" />
                  <Point X="-22.10345703125" Y="-1.619370361328" />
                  <Point X="-22.04011328125" Y="-1.717899536133" />
                  <Point X="-22.0029765625" Y="-1.775660888672" />
                  <Point X="-21.998255859375" Y="-1.782352783203" />
                  <Point X="-21.980205078125" Y="-1.804448120117" />
                  <Point X="-21.956509765625" Y="-1.828119018555" />
                  <Point X="-21.947201171875" Y="-1.836277832031" />
                  <Point X="-21.501318359375" Y="-2.178416503906" />
                  <Point X="-20.912828125" Y="-2.629980712891" />
                  <Point X="-20.95453125" Y="-2.697459716797" />
                  <Point X="-20.998724609375" Y="-2.760251953125" />
                  <Point X="-21.30500390625" Y="-2.583421142578" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.18497265625" Y="-2.079513427734" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228892578125" Y="-2.066337646484" />
                  <Point X="-22.36512109375" Y="-2.041734985352" />
                  <Point X="-22.444982421875" Y="-2.027311889648" />
                  <Point X="-22.460640625" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503295898" />
                  <Point X="-22.539859375" Y="-2.03146105957" />
                  <Point X="-22.55515625" Y="-2.035136352539" />
                  <Point X="-22.584931640625" Y="-2.044960083008" />
                  <Point X="-22.59941015625" Y="-2.051108642578" />
                  <Point X="-22.71258203125" Y="-2.110670166016" />
                  <Point X="-22.778927734375" Y="-2.145587646484" />
                  <Point X="-22.79103125" Y="-2.153170410156" />
                  <Point X="-22.813962890625" Y="-2.170064208984" />
                  <Point X="-22.824791015625" Y="-2.179375244141" />
                  <Point X="-22.84575" Y="-2.200334716797" />
                  <Point X="-22.85505859375" Y="-2.211161132813" />
                  <Point X="-22.871951171875" Y="-2.234090576172" />
                  <Point X="-22.87953515625" Y="-2.246193603516" />
                  <Point X="-22.93909765625" Y="-2.359365478516" />
                  <Point X="-22.974015625" Y="-2.425711669922" />
                  <Point X="-22.9801640625" Y="-2.440193359375" />
                  <Point X="-22.98998828125" Y="-2.469972167969" />
                  <Point X="-22.9936640625" Y="-2.485269287109" />
                  <Point X="-22.99862109375" Y="-2.517442871094" />
                  <Point X="-22.999720703125" Y="-2.533134033203" />
                  <Point X="-22.99931640625" Y="-2.564484375" />
                  <Point X="-22.9978125" Y="-2.580143554688" />
                  <Point X="-22.973208984375" Y="-2.716371582031" />
                  <Point X="-22.95878515625" Y="-2.796233886719" />
                  <Point X="-22.95698046875" Y="-2.804229248047" />
                  <Point X="-22.94876171875" Y="-2.831542236328" />
                  <Point X="-22.935927734375" Y="-2.862479980469" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.643923828125" Y="-3.369854980469" />
                  <Point X="-22.264103515625" Y="-4.027723388672" />
                  <Point X="-22.276244140625" Y="-4.036082763672" />
                  <Point X="-22.520095703125" Y="-3.718290283203" />
                  <Point X="-23.16608203125" Y="-2.876422363281" />
                  <Point X="-23.171345703125" Y="-2.870143066406" />
                  <Point X="-23.191169921875" Y="-2.849624023438" />
                  <Point X="-23.216748046875" Y="-2.828003662109" />
                  <Point X="-23.22669921875" Y="-2.820646728516" />
                  <Point X="-23.36105859375" Y="-2.734267089844" />
                  <Point X="-23.43982421875" Y="-2.683627929688" />
                  <Point X="-23.453716796875" Y="-2.676245605469" />
                  <Point X="-23.48252734375" Y="-2.663874511719" />
                  <Point X="-23.4974453125" Y="-2.658885742188" />
                  <Point X="-23.52906640625" Y="-2.651154052734" />
                  <Point X="-23.54460546875" Y="-2.648695800781" />
                  <Point X="-23.575875" Y="-2.646376953125" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.738546875" Y="-2.660038085938" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670339111328" />
                  <Point X="-23.866421875" Y="-2.677170410156" />
                  <Point X="-23.87998828125" Y="-2.681627929688" />
                  <Point X="-23.907373046875" Y="-2.692970703125" />
                  <Point X="-23.92012109375" Y="-2.699412841797" />
                  <Point X="-23.94450390625" Y="-2.714134033203" />
                  <Point X="-23.956138671875" Y="-2.722413085938" />
                  <Point X="-24.069603515625" Y="-2.816755859375" />
                  <Point X="-24.136123046875" Y="-2.872063476562" />
                  <Point X="-24.147345703125" Y="-2.883088134766" />
                  <Point X="-24.16781640625" Y="-2.906838134766" />
                  <Point X="-24.177064453125" Y="-2.919563476563" />
                  <Point X="-24.19395703125" Y="-2.947390380859" />
                  <Point X="-24.200978515625" Y="-2.961466552734" />
                  <Point X="-24.212603515625" Y="-2.990586914062" />
                  <Point X="-24.21720703125" Y="-3.005631103516" />
                  <Point X="-24.2511328125" Y="-3.161715820312" />
                  <Point X="-24.271021484375" Y="-3.253218994141" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520019531" />
                  <Point X="-24.19324609375" Y="-3.952288330078" />
                  <Point X="-24.166912109375" Y="-4.152313964844" />
                  <Point X="-24.344931640625" Y="-3.487937255859" />
                  <Point X="-24.347392578125" Y="-3.480123291016" />
                  <Point X="-24.357853515625" Y="-3.453577392578" />
                  <Point X="-24.3732109375" Y="-3.423814697266" />
                  <Point X="-24.37958984375" Y="-3.413209472656" />
                  <Point X="-24.48280859375" Y="-3.264492675781" />
                  <Point X="-24.543318359375" Y="-3.177309082031" />
                  <Point X="-24.553330078125" Y="-3.165170898438" />
                  <Point X="-24.57521484375" Y="-3.142715820312" />
                  <Point X="-24.587087890625" Y="-3.132398925781" />
                  <Point X="-24.61334375" Y="-3.113154785156" />
                  <Point X="-24.6267578125" Y="-3.104937988281" />
                  <Point X="-24.6547578125" Y="-3.090830078125" />
                  <Point X="-24.66934375" Y="-3.084938964844" />
                  <Point X="-24.82906640625" Y="-3.035366699219" />
                  <Point X="-24.922703125" Y="-3.006305664062" />
                  <Point X="-24.936623046875" Y="-3.003109619141" />
                  <Point X="-24.964783203125" Y="-2.998840087891" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.22470703125" Y="-3.055877197266" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.332931640625" Y="-3.090830078125" />
                  <Point X="-25.360931640625" Y="-3.104938232422" />
                  <Point X="-25.37434375" Y="-3.113154785156" />
                  <Point X="-25.400599609375" Y="-3.132398925781" />
                  <Point X="-25.412474609375" Y="-3.142717041016" />
                  <Point X="-25.434359375" Y="-3.165172851562" />
                  <Point X="-25.444369140625" Y="-3.177310546875" />
                  <Point X="-25.5475859375" Y="-3.326027099609" />
                  <Point X="-25.608095703125" Y="-3.4132109375" />
                  <Point X="-25.61246875" Y="-3.420131591797" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213623047" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.79080078125" Y="-4.040452636719" />
                  <Point X="-25.985427734375" Y="-4.766805664062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.538352598956" Y="1.305699307387" />
                  <Point X="-29.746077557242" Y="0.581276287514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.086949766767" Y="2.535272312894" />
                  <Point X="-29.168958592329" Y="2.249273550105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.443119261237" Y="1.293161673423" />
                  <Point X="-29.654300615343" Y="0.556684768845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.913983528689" Y="2.793821518401" />
                  <Point X="-29.087953535387" Y="2.18711600425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.347885937066" Y="1.280623992211" />
                  <Point X="-29.562523673443" Y="0.532093250176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.757467496515" Y="2.995002038237" />
                  <Point X="-29.006948478444" Y="2.124958458394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.252652612895" Y="1.268086310999" />
                  <Point X="-29.470746731543" Y="0.507501731508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.658411461539" Y="2.9957957339" />
                  <Point X="-28.925943421501" Y="2.062800912539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.157419288723" Y="1.255548629787" />
                  <Point X="-29.378969789643" Y="0.482910212839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.698395679262" Y="-0.631060248355" />
                  <Point X="-29.759399462286" Y="-0.8438057224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.573620388582" Y="2.946841594979" />
                  <Point X="-28.844938359406" Y="2.000643384653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.062185964552" Y="1.243010948575" />
                  <Point X="-29.287192847744" Y="0.45831869417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.591341946818" Y="-0.602375267024" />
                  <Point X="-29.720586209132" Y="-1.053103574202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.488829315624" Y="2.897887456058" />
                  <Point X="-28.763933272459" Y="1.938485943433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.966952640381" Y="1.230473267364" />
                  <Point X="-29.195415905844" Y="0.433727175501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.484288214374" Y="-0.573690285692" />
                  <Point X="-29.67402529021" Y="-1.235382104495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.404038242667" Y="2.848933317137" />
                  <Point X="-28.682928185513" Y="1.876328502214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.87171931621" Y="1.217935586152" />
                  <Point X="-29.103638963944" Y="0.409135656832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.377234481099" Y="-0.545005301464" />
                  <Point X="-29.587124595696" Y="-1.276979118728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.319247169709" Y="2.799979178216" />
                  <Point X="-28.601923098566" Y="1.814171060995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.77646156765" Y="1.205483082907" />
                  <Point X="-29.011862036597" Y="0.384544087411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.270180747316" Y="-0.516320315465" />
                  <Point X="-29.484418928413" Y="-1.263457642644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.935026458994" Y="3.795260282924" />
                  <Point X="-27.994562150912" Y="3.587634651007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.233924353753" Y="2.752879447508" />
                  <Point X="-28.521741369307" Y="1.749142230281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.675036044735" Y="1.214540165031" />
                  <Point X="-28.920085124516" Y="0.359952464754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.163127013534" Y="-0.487635329466" />
                  <Point X="-29.38171326113" Y="-1.249936166559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.808348518701" Y="3.892383010156" />
                  <Point X="-27.928529469078" Y="3.473262227938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.141660901488" Y="2.729984592116" />
                  <Point X="-28.45317515899" Y="1.643605271041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.563970213986" Y="1.257216995939" />
                  <Point X="-28.828308212434" Y="0.335360842098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.056073279751" Y="-0.458950343467" />
                  <Point X="-29.279007593847" Y="-1.236414690474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.681670542681" Y="3.989505861984" />
                  <Point X="-27.862496669405" Y="3.358890215824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.044648479679" Y="2.723651361701" />
                  <Point X="-28.736531300352" Y="0.310769219441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.949019545969" Y="-0.430265357468" />
                  <Point X="-29.176301926565" Y="-1.22289321439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.561715852716" Y="4.063181828914" />
                  <Point X="-27.796463866766" Y="3.244518214053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.938075859324" Y="2.750658505785" />
                  <Point X="-28.64475438827" Y="0.286177596785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.841965812186" Y="-0.401580371469" />
                  <Point X="-29.073596259282" Y="-1.209371738305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.444159497681" Y="4.128493807964" />
                  <Point X="-27.750249861154" Y="3.061029853268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.805356250722" Y="2.868851034342" />
                  <Point X="-28.552977476189" Y="0.261585974128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.734912078403" Y="-0.37289538547" />
                  <Point X="-28.970890591999" Y="-1.19585026222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.326603142647" Y="4.193805787015" />
                  <Point X="-28.462940459712" Y="0.230926614407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.627858344621" Y="-0.344210399471" />
                  <Point X="-28.868184924716" Y="-1.182328786136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.209047019261" Y="4.259116958212" />
                  <Point X="-28.380708008578" Y="0.173049500784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.520804610838" Y="-0.315525413472" />
                  <Point X="-28.765479264011" Y="-1.168807332987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.116763123284" Y="-2.393879737707" />
                  <Point X="-29.147291276281" Y="-2.500344059412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.130692566287" Y="4.187715657791" />
                  <Point X="-28.312441599622" Y="0.066467009943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.405215209878" Y="-0.25707301847" />
                  <Point X="-28.662773605126" Y="-1.155285886192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.990055509877" Y="-2.296653528028" />
                  <Point X="-29.08055264555" Y="-2.6122545461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.053044901365" Y="4.113849494509" />
                  <Point X="-28.560067946242" Y="-1.141764439397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.863347893921" Y="-2.19942730946" />
                  <Point X="-29.013814014819" Y="-2.724165032788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.96740152601" Y="4.067867687279" />
                  <Point X="-28.457362287358" Y="-1.128242992602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.736640225867" Y="-2.102200909204" />
                  <Point X="-28.942768800741" Y="-2.821056678509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.878365103692" Y="4.033718841038" />
                  <Point X="-28.354656628474" Y="-1.114721545806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.609932557813" Y="-2.004974508948" />
                  <Point X="-28.870983636456" Y="-2.915367811188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.779009303652" Y="4.035557941716" />
                  <Point X="-28.25195096959" Y="-1.101200099011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.483224889759" Y="-1.907748108693" />
                  <Point X="-28.799198124793" Y="-3.009677732419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.668340605799" Y="4.076849805626" />
                  <Point X="-28.155014703916" Y="-1.107798917432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.356517221705" Y="-1.810521708437" />
                  <Point X="-28.684734354927" Y="-2.955150879551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.425921130663" Y="4.577611233221" />
                  <Point X="-26.463828597086" Y="4.445412187289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.544899465345" Y="4.16268447035" />
                  <Point X="-28.067019681682" Y="-1.145579557366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.229809553651" Y="-1.713295308181" />
                  <Point X="-28.566298538419" Y="-2.886771853855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.318452936237" Y="4.607741615258" />
                  <Point X="-27.989529299725" Y="-1.219994231535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.103101885596" Y="-1.616068907925" />
                  <Point X="-28.447862721911" Y="-2.818392828159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.21098474181" Y="4.637871997294" />
                  <Point X="-28.329426905403" Y="-2.750013802463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.103516593808" Y="4.668002217428" />
                  <Point X="-28.210991088895" Y="-2.681634776767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.996048453141" Y="4.698132411978" />
                  <Point X="-28.09255530101" Y="-2.613255850889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.890644583092" Y="4.721063639364" />
                  <Point X="-27.974119532476" Y="-2.544876992497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.788384347391" Y="4.733031710917" />
                  <Point X="-27.855683763941" Y="-2.476498134106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.68612411169" Y="4.74499978247" />
                  <Point X="-27.737247995407" Y="-2.408119275715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.583863875989" Y="4.756967854023" />
                  <Point X="-27.623496219396" Y="-2.356075440501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.481603640287" Y="4.768935925576" />
                  <Point X="-27.52340679231" Y="-2.351677878266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.915083930141" Y="-3.717618386062" />
                  <Point X="-27.953740697125" Y="-3.852430553594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.379343400736" Y="4.780904010556" />
                  <Point X="-27.435086790156" Y="-2.388325178536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.718739382354" Y="-3.3775393256" />
                  <Point X="-27.872785814522" Y="-3.914763078167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.324075063035" Y="4.628991858279" />
                  <Point X="-27.357645848917" Y="-2.462912272977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.522395398587" Y="-3.037462232112" />
                  <Point X="-27.791830931919" Y="-3.97709560274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.276335545238" Y="4.450823590727" />
                  <Point X="-27.709527823031" Y="-4.034726303492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.22859602744" Y="4.272655323175" />
                  <Point X="-27.625600382272" Y="-4.086692285815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.163123643712" Y="4.156328908398" />
                  <Point X="-27.541673027205" Y="-4.138658566983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.079769267488" Y="4.10236441254" />
                  <Point X="-27.417737307522" Y="-4.051099099513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.985887503377" Y="4.08511328125" />
                  <Point X="-27.288415730399" Y="-3.944756915016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.878297515999" Y="4.115668405785" />
                  <Point X="-27.166164061068" Y="-3.86307042907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.588538551861" Y="4.78152225109" />
                  <Point X="-27.047305738161" Y="-3.793217948455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.492591420614" Y="4.771473910985" />
                  <Point X="-26.943558672284" Y="-3.776064683871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.396644286186" Y="4.761425581972" />
                  <Point X="-26.846553351564" Y="-3.782422678725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.300697111347" Y="4.75137739389" />
                  <Point X="-26.749548047035" Y="-3.788780730042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.204749936508" Y="4.741329205808" />
                  <Point X="-26.652542745366" Y="-3.795138791335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.11112411478" Y="4.723185497354" />
                  <Point X="-26.560003296456" Y="-3.817071132044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.018694519536" Y="4.700870051387" />
                  <Point X="-26.476563924105" Y="-3.870739211181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.926264924291" Y="4.678554605419" />
                  <Point X="-26.397593910563" Y="-3.939993796787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.833835329047" Y="4.656239159452" />
                  <Point X="-26.318624152404" Y="-4.009249273017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.741405750062" Y="4.633923656779" />
                  <Point X="-26.240779183075" Y="-4.082427354061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.648976199288" Y="4.611608055726" />
                  <Point X="-26.179729606373" Y="-4.214177929941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.556546648513" Y="4.589292454673" />
                  <Point X="-26.139251250036" Y="-4.41766887685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.466708957934" Y="4.55793796294" />
                  <Point X="-26.131110865427" Y="-4.733935733447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.377190728434" Y="4.525469378023" />
                  <Point X="-26.038740311034" Y="-4.756457079335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.287672500895" Y="4.493000786269" />
                  <Point X="-25.517180893319" Y="-3.282218984135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.19815436415" Y="4.460531877877" />
                  <Point X="-25.36892157299" Y="-3.109833040448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.109514694623" Y="4.424999390221" />
                  <Point X="-25.257543845584" Y="-3.066068496633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.022372203477" Y="4.384245621054" />
                  <Point X="-25.149060953953" Y="-3.03239944491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.935229712331" Y="4.343491851887" />
                  <Point X="-25.04141411894" Y="-3.001646069115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.848087294453" Y="4.302737827204" />
                  <Point X="-24.942739427872" Y="-3.002182277702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.760944948976" Y="4.261983550031" />
                  <Point X="-24.851435723432" Y="-3.028424171525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.675941443534" Y="4.213770251222" />
                  <Point X="-24.760683770557" Y="-3.056590251721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.591259665992" Y="4.164434953891" />
                  <Point X="-24.669931848622" Y="-3.084756439823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.506577895054" Y="4.115099633529" />
                  <Point X="-22.954444064479" Y="2.553204685369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.967538039234" Y="2.507540568683" />
                  <Point X="-24.585228064709" Y="-3.134014991819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.421896472252" Y="4.065763099072" />
                  <Point X="-22.751733829181" Y="2.9154835364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.951699065296" Y="2.218121883706" />
                  <Point X="-24.511825297011" Y="-3.222684870992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.33823637888" Y="4.012864765609" />
                  <Point X="-22.555389111932" Y="3.255563187844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.882480782493" Y="2.114858971471" />
                  <Point X="-24.441890125738" Y="-3.323447696024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.256147120171" Y="3.954488280655" />
                  <Point X="-22.359044316934" Y="3.595643110432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.802977358986" Y="2.047464607483" />
                  <Point X="-24.372330644106" Y="-3.425520706535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.717930571541" Y="1.999402250957" />
                  <Point X="-24.10812599463" Y="-2.848785347285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.266028060588" Y="-3.399455292817" />
                  <Point X="-24.319072703253" Y="-3.584443945818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.624722300717" Y="1.979802369454" />
                  <Point X="-23.978358565548" Y="-2.740888292224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.23493080455" Y="-3.63566202441" />
                  <Point X="-24.271332868191" Y="-3.762611106935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.52624453613" Y="1.97857939661" />
                  <Point X="-23.860865662537" Y="-2.675797596677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.203833548513" Y="-3.871868756003" />
                  <Point X="-24.223593033128" Y="-3.940778268053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.419994370428" Y="2.004462007678" />
                  <Point X="-23.758032415339" Y="-2.661831196554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.172736256318" Y="-4.1080753615" />
                  <Point X="-24.175853198066" Y="-4.118945429171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.304630024651" Y="2.062129541982" />
                  <Point X="-23.656525533295" Y="-2.652490381425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.186194227378" Y="2.130508500599" />
                  <Point X="-23.556359078308" Y="-2.647824190978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.067758430105" Y="2.198887459216" />
                  <Point X="-23.464368793742" Y="-2.67167169535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.949322632831" Y="2.267266417834" />
                  <Point X="-23.379997817349" Y="-2.722090885097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.830886835558" Y="2.335645376451" />
                  <Point X="-23.296552457452" Y="-2.775738083182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.712451047631" Y="2.404024302474" />
                  <Point X="-21.941983932096" Y="1.603548005852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.998839882558" Y="1.405267742995" />
                  <Point X="-23.213498531185" Y="-2.830750372564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.594015261432" Y="2.472403222472" />
                  <Point X="-21.796130795273" Y="1.76754259043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.956221748617" Y="1.209239087407" />
                  <Point X="-23.138189581301" Y="-2.912772604451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.475579475232" Y="2.54078214247" />
                  <Point X="-21.669423159985" Y="1.864768876414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.888295202641" Y="1.101471353505" />
                  <Point X="-22.830780251723" Y="-2.18536461977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.977009695894" Y="-2.695327295487" />
                  <Point X="-23.066246056932" Y="-3.006531469887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.357143689033" Y="2.609161062468" />
                  <Point X="-21.542715524698" Y="1.961995162399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.809476969831" Y="1.031687445581" />
                  <Point X="-22.710169424021" Y="-2.109400428618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.929677347798" Y="-2.874915532537" />
                  <Point X="-22.994302532563" Y="-3.100290335324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.238707902833" Y="2.677539982466" />
                  <Point X="-21.416007889411" Y="2.059221448383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.723852617279" Y="0.985639297955" />
                  <Point X="-22.593962736237" Y="-2.048795298633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.86364456985" Y="-2.989287620414" />
                  <Point X="-22.922359008194" Y="-3.19404920076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.120272116634" Y="2.745918902464" />
                  <Point X="-21.289300254123" Y="2.156447734367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.631798136587" Y="0.962015672078" />
                  <Point X="-22.488439848644" Y="-2.025449007745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.797611791901" Y="-3.103659708291" />
                  <Point X="-22.850415483825" Y="-3.287808066196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.025132535968" Y="2.733054298797" />
                  <Point X="-21.162592618836" Y="2.253674020352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.536041726162" Y="0.951302209422" />
                  <Point X="-22.392845571439" Y="-2.036727896133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.731579013953" Y="-3.218031796168" />
                  <Point X="-22.778471959456" Y="-3.381566931633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.954545737574" Y="2.6345639676" />
                  <Point X="-21.035884916963" Y="2.350900538547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.434328265025" Y="0.961363451463" />
                  <Point X="-22.030279934234" Y="-1.116967007567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.139603947091" Y="-1.498225149063" />
                  <Point X="-22.298883072187" Y="-2.053697470527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.665546236004" Y="-3.332403884045" />
                  <Point X="-22.706528435087" Y="-3.475325797069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.88749142093" Y="2.523754408525" />
                  <Point X="-20.90917717067" Y="2.448127211655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.331622597244" Y="0.974884929284" />
                  <Point X="-21.531083482065" Y="0.27928215858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.642100473231" Y="-0.107880099923" />
                  <Point X="-21.892020487772" Y="-0.979454768439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.084132515107" Y="-1.649429027402" />
                  <Point X="-22.205571402561" Y="-2.072936757553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.599513470391" Y="-3.446776014939" />
                  <Point X="-22.634584910718" Y="-3.569084662505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.228916929464" Y="0.988406407106" />
                  <Point X="-21.404849157951" Y="0.374857812342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.58892421773" Y="-0.267088209881" />
                  <Point X="-21.782586467392" Y="-0.942468736582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.015787053791" Y="-1.755735829901" />
                  <Point X="-22.118453428826" Y="-2.113776029095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.533480710783" Y="-3.561148166778" />
                  <Point X="-22.562641386349" Y="-3.662843527942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.126211261683" Y="1.001927884927" />
                  <Point X="-21.291413492114" Y="0.425799240369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.516319325092" Y="-0.358540610064" />
                  <Point X="-21.677189072386" Y="-0.919560090356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.94134268563" Y="-1.840773216574" />
                  <Point X="-22.033662353494" Y="-2.162730159734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.467447951175" Y="-3.675520318616" />
                  <Point X="-22.490697814839" Y="-3.756602228976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.023505593902" Y="1.015449362748" />
                  <Point X="-21.184359782165" Y="0.45448414325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.435095332263" Y="-0.419934635748" />
                  <Point X="-21.575180554878" Y="-0.908469864463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.860337618795" Y="-1.902930727931" />
                  <Point X="-21.948871278162" Y="-2.211684290374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.401415191567" Y="-3.789892470454" />
                  <Point X="-22.418754175103" Y="-3.85036069208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.92079992746" Y="1.028970835903" />
                  <Point X="-21.077306072216" Y="0.483169046131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.349975412163" Y="-0.467741948394" />
                  <Point X="-21.479728606129" Y="-0.920245111166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.77933255196" Y="-1.965088239289" />
                  <Point X="-21.86408020283" Y="-2.260638421013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.335382431959" Y="-3.904264622293" />
                  <Point X="-22.346810535367" Y="-3.944119155184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.818094262193" Y="1.042492304956" />
                  <Point X="-20.970252362267" Y="0.511853949012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.259510149038" Y="-0.496907834566" />
                  <Point X="-21.3844952949" Y="-0.932782837514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.698327485125" Y="-2.027245750646" />
                  <Point X="-21.779289127498" Y="-2.309592551653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.269349672351" Y="-4.018636774131" />
                  <Point X="-22.273886802973" Y="-4.034459628995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.715388596927" Y="1.056013774009" />
                  <Point X="-20.863198652318" Y="0.540538851893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.167733197785" Y="-0.52149932062" />
                  <Point X="-21.289261983672" Y="-0.945320563862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.61732241829" Y="-2.089403262003" />
                  <Point X="-21.694498052165" Y="-2.358546682292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.61268293166" Y="1.069535243061" />
                  <Point X="-20.756144942369" Y="0.569223754774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.075956246533" Y="-0.546090806673" />
                  <Point X="-21.194028672443" Y="-0.95785829021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.536317351455" Y="-2.151560773361" />
                  <Point X="-21.609706976833" Y="-2.407500812931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.509977266394" Y="1.083056712114" />
                  <Point X="-20.64909123242" Y="0.597908657655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.984179295281" Y="-0.570682292726" />
                  <Point X="-21.098795361215" Y="-0.970396016558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.455312262052" Y="-2.213718206012" />
                  <Point X="-21.524915901501" Y="-2.456454943571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.407271601127" Y="1.096578181167" />
                  <Point X="-20.542037522471" Y="0.626593560536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.892402344029" Y="-0.59527377878" />
                  <Point X="-21.003562049986" Y="-0.982933742907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.37430715548" Y="-2.275875578788" />
                  <Point X="-21.440124826169" Y="-2.50540907421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.304565935861" Y="1.11009965022" />
                  <Point X="-20.434983807394" Y="0.655278481302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.800625403321" Y="-0.619865301605" />
                  <Point X="-20.908328738757" Y="-0.995471469255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.293302048907" Y="-2.338032951564" />
                  <Point X="-21.355333750837" Y="-2.554363204849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.254014133844" Y="0.941738983276" />
                  <Point X="-20.32793008717" Y="0.683963420013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.708848488036" Y="-0.644456913089" />
                  <Point X="-20.813095427529" Y="-1.008009195603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.212296942335" Y="-2.40019032434" />
                  <Point X="-21.270542687866" Y="-2.6033173786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.217669914824" Y="0.723830586172" />
                  <Point X="-20.220876366947" Y="0.712648358725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.617071572751" Y="-0.669048524574" />
                  <Point X="-20.7178621163" Y="-1.020546921951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.131291835762" Y="-2.462347697116" />
                  <Point X="-21.18575164295" Y="-2.652271615313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.525294657465" Y="-0.693640136058" />
                  <Point X="-20.622628805071" Y="-1.033084648299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.05028672919" Y="-2.524505069892" />
                  <Point X="-21.100960598034" Y="-2.701225852027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.43351774218" Y="-0.718231747542" />
                  <Point X="-20.52739548886" Y="-1.04562235727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.969281622617" Y="-2.586662442668" />
                  <Point X="-21.016169553118" Y="-2.75018008874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.341740826894" Y="-0.742823359026" />
                  <Point X="-20.43216217207" Y="-1.058160064225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.249963911609" Y="-0.76741497051" />
                  <Point X="-20.336928855281" Y="-1.070697771179" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.275923828125" Y="-4.479585449219" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544433594" />
                  <Point X="-24.6388984375" Y="-3.372827636719" />
                  <Point X="-24.699408203125" Y="-3.285644042969" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.88538671875" Y="-3.216827636719" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.16838671875" Y="-3.237338623047" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285644042969" />
                  <Point X="-25.39149609375" Y="-3.434360595703" />
                  <Point X="-25.452005859375" Y="-3.521544433594" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.6072734375" Y="-4.089628417969" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.99094140625" Y="-4.95928125" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.28519921875" Y="-4.890478027344" />
                  <Point X="-26.351587890625" Y="-4.873396484375" />
                  <Point X="-26.339548828125" Y="-4.781943847656" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.534756835938" />
                  <Point X="-26.34783984375" Y="-4.342924804687" />
                  <Point X="-26.3702109375" Y="-4.230465332031" />
                  <Point X="-26.386283203125" Y="-4.202628417969" />
                  <Point X="-26.533333984375" Y="-4.073666748047" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.844412109375" Y="-3.972970703125" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791259766" />
                  <Point X="-27.152505859375" Y="-4.082455566406" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.342849609375" Y="-4.265613769531" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.695619140625" Y="-4.266811523438" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.11194140625" Y="-3.970416259766" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.975341796875" Y="-3.441984375" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597592773438" />
                  <Point X="-27.513978515625" Y="-2.568763427734" />
                  <Point X="-27.531322265625" Y="-2.551419677734" />
                  <Point X="-27.56015234375" Y="-2.53719921875" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.068216796875" Y="-2.818597167969" />
                  <Point X="-28.84295703125" Y="-3.265893554688" />
                  <Point X="-29.035125" Y="-3.013426513672" />
                  <Point X="-29.161705078125" Y="-2.847125244141" />
                  <Point X="-29.3453125" Y="-2.539244384766" />
                  <Point X="-29.43101953125" Y="-2.395526855469" />
                  <Point X="-28.984921875" Y="-2.053224853516" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.396014404297" />
                  <Point X="-28.1381171875" Y="-1.366267089844" />
                  <Point X="-28.14032421875" Y="-1.334596435547" />
                  <Point X="-28.161158203125" Y="-1.310639770508" />
                  <Point X="-28.187640625" Y="-1.295053100586" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.821107421875" Y="-1.367770507812" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.87788671875" Y="-1.20500390625" />
                  <Point X="-29.927392578125" Y="-1.011188415527" />
                  <Point X="-29.975970703125" Y="-0.671535583496" />
                  <Point X="-29.99839453125" Y="-0.514742248535" />
                  <Point X="-29.492859375" Y="-0.379284484863" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.541896484375" Y="-0.121426269531" />
                  <Point X="-28.524400390625" Y="-0.109282974243" />
                  <Point X="-28.514142578125" Y="-0.102164085388" />
                  <Point X="-28.494896484375" Y="-0.075903800964" />
                  <Point X="-28.485646484375" Y="-0.046096923828" />
                  <Point X="-28.485646484375" Y="-0.016463153839" />
                  <Point X="-28.494896484375" Y="0.013343723297" />
                  <Point X="-28.514142578125" Y="0.039604011536" />
                  <Point X="-28.531638671875" Y="0.051747299194" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.105830078125" Y="0.213020202637" />
                  <Point X="-29.998185546875" Y="0.452126037598" />
                  <Point X="-29.949548828125" Y="0.780806945801" />
                  <Point X="-29.91764453125" Y="0.996414794922" />
                  <Point X="-29.819857421875" Y="1.357282104492" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.42378125" Y="1.482255371094" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866333008" />
                  <Point X="-28.69298046875" Y="1.408075927734" />
                  <Point X="-28.67027734375" Y="1.415233764648" />
                  <Point X="-28.651533203125" Y="1.426055664062" />
                  <Point X="-28.639119140625" Y="1.443785522461" />
                  <Point X="-28.62358203125" Y="1.481298339844" />
                  <Point X="-28.61447265625" Y="1.503289794922" />
                  <Point X="-28.610712890625" Y="1.524605957031" />
                  <Point X="-28.61631640625" Y="1.545513427734" />
                  <Point X="-28.635064453125" Y="1.581529296875" />
                  <Point X="-28.646056640625" Y="1.602643188477" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-28.974513671875" Y="1.860580932617" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.283986328125" Y="2.574612792969" />
                  <Point X="-29.16001171875" Y="2.7870078125" />
                  <Point X="-28.90098046875" Y="3.119958496094" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.57096875" Y="3.164703369141" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.08458203125" Y="2.915716308594" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.0315078125" Y="2.915774902344" />
                  <Point X="-28.01325390625" Y="2.927403564453" />
                  <Point X="-27.97497265625" Y="2.965685058594" />
                  <Point X="-27.952529296875" Y="2.988126953125" />
                  <Point X="-27.9408984375" Y="3.006381347656" />
                  <Point X="-27.938072265625" Y="3.027840332031" />
                  <Point X="-27.942791015625" Y="3.081772460938" />
                  <Point X="-27.945556640625" Y="3.113389892578" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.091451171875" Y="3.375454101562" />
                  <Point X="-28.30727734375" Y="3.749276855469" />
                  <Point X="-27.96879296875" Y="4.0087890625" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.344904296875" Y="4.400991210938" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.09389453125" Y="4.451868652344" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.951244140625" Y="4.27366015625" />
                  <Point X="-26.89121875" Y="4.242412109375" />
                  <Point X="-26.85602734375" Y="4.224093261719" />
                  <Point X="-26.83512109375" Y="4.218491699219" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.75128515625" Y="4.248147949219" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.69690234375" Y="4.275744628906" />
                  <Point X="-26.68608203125" Y="4.294488769531" />
                  <Point X="-26.665732421875" Y="4.359029296875" />
                  <Point X="-26.653802734375" Y="4.396865234375" />
                  <Point X="-26.651916015625" Y="4.418426269531" />
                  <Point X="-26.667763671875" Y="4.538792480469" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.24761328125" Y="4.824928710938" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.4735078125" Y="4.961180175781" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.16396484375" Y="4.7655625" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.874126953125" Y="4.577443359375" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.38385546875" Y="4.951125488281" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.730611328125" Y="4.826776367188" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.224779296875" Y="4.672301269531" />
                  <Point X="-23.068970703125" Y="4.615788085938" />
                  <Point X="-22.8114296875" Y="4.495344726562" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.412509765625" Y="4.280188476563" />
                  <Point X="-22.26747265625" Y="4.195688964844" />
                  <Point X="-22.03283203125" Y="4.028826904297" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.22695703125" Y="3.444427978516" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491515869141" />
                  <Point X="-22.788681640625" Y="2.440901611328" />
                  <Point X="-22.7966171875" Y="2.411229492188" />
                  <Point X="-22.797955078125" Y="2.392325927734" />
                  <Point X="-22.792677734375" Y="2.348559082031" />
                  <Point X="-22.789583984375" Y="2.322901123047" />
                  <Point X="-22.78131640625" Y="2.300811523438" />
                  <Point X="-22.754234375" Y="2.260900390625" />
                  <Point X="-22.738357421875" Y="2.237502929688" />
                  <Point X="-22.725056640625" Y="2.224203125" />
                  <Point X="-22.685146484375" Y="2.197121582031" />
                  <Point X="-22.661748046875" Y="2.181245605469" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.595896484375" Y="2.167702636719" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.551333984375" Y="2.165946289062" />
                  <Point X="-22.50071875" Y="2.179481201172" />
                  <Point X="-22.471046875" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.907412109375" Y="2.510856689453" />
                  <Point X="-21.005751953125" Y="3.031430419922" />
                  <Point X="-20.8834375" Y="2.861441162109" />
                  <Point X="-20.79740234375" Y="2.741873291016" />
                  <Point X="-20.66659765625" Y="2.525716308594" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-20.996224609375" Y="2.141843261719" />
                  <Point X="-21.7113828125" Y="1.593082641602" />
                  <Point X="-21.72062890625" Y="1.583833496094" />
                  <Point X="-21.7570546875" Y="1.536311645508" />
                  <Point X="-21.77841015625" Y="1.508452392578" />
                  <Point X="-21.78687890625" Y="1.491500488281" />
                  <Point X="-21.80044921875" Y="1.442980224609" />
                  <Point X="-21.808404296875" Y="1.414536010742" />
                  <Point X="-21.809220703125" Y="1.390966552734" />
                  <Point X="-21.798080078125" Y="1.336981811523" />
                  <Point X="-21.79155078125" Y="1.305333740234" />
                  <Point X="-21.784353515625" Y="1.287956420898" />
                  <Point X="-21.754056640625" Y="1.241906860352" />
                  <Point X="-21.736296875" Y="1.214911010742" />
                  <Point X="-21.719052734375" Y="1.198819824219" />
                  <Point X="-21.6751484375" Y="1.174105712891" />
                  <Point X="-21.64941015625" Y="1.159617553711" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.572072265625" Y="1.145774291992" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-21.0002578125" Y="1.210149658203" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.097759765625" Y="1.103161010742" />
                  <Point X="-20.06080859375" Y="0.951367980957" />
                  <Point X="-20.019587890625" Y="0.686624450684" />
                  <Point X="-20.002140625" Y="0.57455645752" />
                  <Point X="-20.439244140625" Y="0.457434539795" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819198608" />
                  <Point X="-21.329232421875" Y="0.199109161377" />
                  <Point X="-21.363421875" Y="0.179346832275" />
                  <Point X="-21.377734375" Y="0.166926544189" />
                  <Point X="-21.4127265625" Y="0.122338218689" />
                  <Point X="-21.433240234375" Y="0.096198867798" />
                  <Point X="-21.443013671875" Y="0.074735107422" />
                  <Point X="-21.454677734375" Y="0.013829734802" />
                  <Point X="-21.461515625" Y="-0.021875450134" />
                  <Point X="-21.461515625" Y="-0.040684627533" />
                  <Point X="-21.4498515625" Y="-0.101590003967" />
                  <Point X="-21.443013671875" Y="-0.137295181274" />
                  <Point X="-21.433240234375" Y="-0.15875894165" />
                  <Point X="-21.398248046875" Y="-0.203347106934" />
                  <Point X="-21.377734375" Y="-0.229486618042" />
                  <Point X="-21.363421875" Y="-0.241906906128" />
                  <Point X="-21.3051015625" Y="-0.275617095947" />
                  <Point X="-21.270912109375" Y="-0.295379272461" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.77835546875" Y="-0.429129943848" />
                  <Point X="-20.001931640625" Y="-0.637172424316" />
                  <Point X="-20.0309375" Y="-0.82956048584" />
                  <Point X="-20.051568359375" Y="-0.966413391113" />
                  <Point X="-20.10437890625" Y="-1.197829345703" />
                  <Point X="-20.125453125" Y="-1.290178588867" />
                  <Point X="-20.63752734375" Y="-1.222762817383" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.719625" Y="-1.123220214844" />
                  <Point X="-21.7867265625" Y="-1.137805053711" />
                  <Point X="-21.8145546875" Y="-1.154697631836" />
                  <Point X="-21.883740234375" Y="-1.237905639648" />
                  <Point X="-21.924298828125" Y="-1.286685546875" />
                  <Point X="-21.935640625" Y="-1.314071044922" />
                  <Point X="-21.945556640625" Y="-1.421829223633" />
                  <Point X="-21.951369140625" Y="-1.485001342773" />
                  <Point X="-21.94363671875" Y="-1.516622436523" />
                  <Point X="-21.88029296875" Y="-1.615151489258" />
                  <Point X="-21.84315625" Y="-1.672913085938" />
                  <Point X="-21.831537109375" Y="-1.685540771484" />
                  <Point X="-21.385654296875" Y="-2.0276796875" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.7377109375" Y="-2.708036132812" />
                  <Point X="-20.7958671875" Y="-2.802140136719" />
                  <Point X="-20.9050859375" Y="-2.957325439453" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.40000390625" Y="-2.747966064453" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.39888671875" Y="-2.228710205078" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.62409375" Y="-2.278806396484" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.334683349609" />
                  <Point X="-22.7709609375" Y="-2.447855224609" />
                  <Point X="-22.80587890625" Y="-2.514201416016" />
                  <Point X="-22.8108359375" Y="-2.546375" />
                  <Point X="-22.786232421875" Y="-2.682603027344" />
                  <Point X="-22.77180859375" Y="-2.762465332031" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.479380859375" Y="-3.274854980469" />
                  <Point X="-22.01332421875" Y="-4.082088867188" />
                  <Point X="-22.09569140625" Y="-4.140921386719" />
                  <Point X="-22.164685546875" Y="-4.190202148438" />
                  <Point X="-22.286814453125" Y="-4.26925390625" />
                  <Point X="-22.32022265625" Y="-4.29087890625" />
                  <Point X="-22.670833984375" Y="-3.833955322266" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467529297" />
                  <Point X="-23.46380859375" Y="-2.894087890625" />
                  <Point X="-23.54257421875" Y="-2.843448730469" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.72113671875" Y="-2.849238769531" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868508789062" />
                  <Point X="-23.948130859375" Y="-2.9628515625" />
                  <Point X="-24.014650390625" Y="-3.018159179688" />
                  <Point X="-24.03154296875" Y="-3.045986083984" />
                  <Point X="-24.06546875" Y="-3.202070800781" />
                  <Point X="-24.085357421875" Y="-3.293573974609" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-24.00487109375" Y="-3.927488525391" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.9403671875" Y="-4.948936523438" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.1184609375" Y="-4.983740234375" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#180" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.12182548178" Y="4.809783384435" Z="1.6" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.6" />
                  <Point X="-0.48547767958" Y="5.042121414108" Z="1.6" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.6" />
                  <Point X="-1.267267945514" Y="4.904352393689" Z="1.6" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.6" />
                  <Point X="-1.723492349181" Y="4.563546466482" Z="1.6" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.6" />
                  <Point X="-1.719575618451" Y="4.405344398545" Z="1.6" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.6" />
                  <Point X="-1.776577711464" Y="4.325621982742" Z="1.6" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.6" />
                  <Point X="-1.874288915294" Y="4.318043393839" Z="1.6" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.6" />
                  <Point X="-2.060383405998" Y="4.513586679358" Z="1.6" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.6" />
                  <Point X="-2.375344227412" Y="4.475978733226" Z="1.6" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.6" />
                  <Point X="-3.005372689292" Y="4.079748824131" Z="1.6" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.6" />
                  <Point X="-3.140909300175" Y="3.381734128258" Z="1.6" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.6" />
                  <Point X="-2.998758510329" Y="3.108695688212" Z="1.6" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.6" />
                  <Point X="-3.016482142367" Y="3.03232142552" Z="1.6" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.6" />
                  <Point X="-3.086380777394" Y="2.996806188844" Z="1.6" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.6" />
                  <Point X="-3.552125220039" Y="3.239284648571" Z="1.6" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.6" />
                  <Point X="-3.946599715324" Y="3.1819408311" Z="1.6" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.6" />
                  <Point X="-4.333324351532" Y="2.631098085797" Z="1.6" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.6" />
                  <Point X="-4.011107740154" Y="1.852193307319" Z="1.6" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.6" />
                  <Point X="-3.685571071694" Y="1.589720184646" Z="1.6" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.6" />
                  <Point X="-3.675931615734" Y="1.531712868207" Z="1.6" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.6" />
                  <Point X="-3.71417171101" Y="1.487042266412" Z="1.6" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.6" />
                  <Point X="-4.423412022128" Y="1.56310766117" Z="1.6" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.6" />
                  <Point X="-4.874273940796" Y="1.40163945544" Z="1.6" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.6" />
                  <Point X="-5.005167536751" Y="0.81940577337" Z="1.6" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.6" />
                  <Point X="-4.124929207926" Y="0.19600392362" Z="1.6" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.6" />
                  <Point X="-3.566303862913" Y="0.041950176622" Z="1.6" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.6" />
                  <Point X="-3.545388843081" Y="0.018790935922" Z="1.6" />
                  <Point X="-3.539556741714" Y="0" Z="1.6" />
                  <Point X="-3.542975758054" Y="-0.011016015139" Z="1.6" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.6" />
                  <Point X="-3.55906473221" Y="-0.036925806512" Z="1.6" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.6" />
                  <Point X="-4.511958580846" Y="-0.299708133136" Z="1.6" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.6" />
                  <Point X="-5.031623599367" Y="-0.647334545453" Z="1.6" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.6" />
                  <Point X="-4.932488306951" Y="-1.186097870517" Z="1.6" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.6" />
                  <Point X="-3.820738610517" Y="-1.386062765746" Z="1.6" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.6" />
                  <Point X="-3.209371488788" Y="-1.312623745139" Z="1.6" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.6" />
                  <Point X="-3.195523723765" Y="-1.333443797455" Z="1.6" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.6" />
                  <Point X="-4.021516986678" Y="-1.982277098851" Z="1.6" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.6" />
                  <Point X="-4.39441234474" Y="-2.533573966236" Z="1.6" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.6" />
                  <Point X="-4.08106818919" Y="-3.012429387665" Z="1.6" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.6" />
                  <Point X="-3.049374086757" Y="-2.830618339587" Z="1.6" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.6" />
                  <Point X="-2.566427851596" Y="-2.561902617535" Z="1.6" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.6" />
                  <Point X="-3.024798673581" Y="-3.385704295735" Z="1.6" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.6" />
                  <Point X="-3.148601835024" Y="-3.978753483626" Z="1.6" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.6" />
                  <Point X="-2.728098229187" Y="-4.278042136427" Z="1.6" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.6" />
                  <Point X="-2.309338861726" Y="-4.264771783619" Z="1.6" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.6" />
                  <Point X="-2.130883426369" Y="-4.092748702078" Z="1.6" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.6" />
                  <Point X="-1.853838432508" Y="-3.991583699272" Z="1.6" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.6" />
                  <Point X="-1.572458592888" Y="-4.079978649237" Z="1.6" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.6" />
                  <Point X="-1.403036323273" Y="-4.321400255527" Z="1.6" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.6" />
                  <Point X="-1.395277773702" Y="-4.744137271836" Z="1.6" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.6" />
                  <Point X="-1.303815650159" Y="-4.907620919056" Z="1.6" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.6" />
                  <Point X="-1.006626616484" Y="-4.97708543325" Z="1.6" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.6" />
                  <Point X="-0.565133108562" Y="-4.071289538209" Z="1.6" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.6" />
                  <Point X="-0.356576504263" Y="-3.431589307751" Z="1.6" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.6" />
                  <Point X="-0.1597229524" Y="-3.253811478536" Z="1.6" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.6" />
                  <Point X="0.09363612696" Y="-3.233300566752" Z="1.6" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.6" />
                  <Point X="0.313869355169" Y="-3.370056448017" Z="1.6" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.6" />
                  <Point X="0.669621751839" Y="-4.461246491606" Z="1.6" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.6" />
                  <Point X="0.884318676686" Y="-5.00165497004" Z="1.6" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.6" />
                  <Point X="1.064180575276" Y="-4.966496859822" Z="1.6" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.6" />
                  <Point X="1.038544888482" Y="-3.889682054799" Z="1.6" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.6" />
                  <Point X="0.977234379396" Y="-3.181410553569" Z="1.6" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.6" />
                  <Point X="1.077678176419" Y="-2.970018344478" Z="1.6" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.6" />
                  <Point X="1.277287639797" Y="-2.867748400843" Z="1.6" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.6" />
                  <Point X="1.502996347064" Y="-2.904865779251" Z="1.6" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.6" />
                  <Point X="2.283342016211" Y="-3.833113205387" Z="1.6" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.6" />
                  <Point X="2.73419838367" Y="-4.279948206223" Z="1.6" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.6" />
                  <Point X="2.927212297344" Y="-4.150328405957" Z="1.6" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.6" />
                  <Point X="2.557762308339" Y="-3.218574985375" Z="1.6" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.6" />
                  <Point X="2.25681379697" Y="-2.642436191323" Z="1.6" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.6" />
                  <Point X="2.267128572924" Y="-2.439862152573" Z="1.6" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.6" />
                  <Point X="2.393036225799" Y="-2.291772737043" Z="1.6" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.6" />
                  <Point X="2.586070612189" Y="-2.246634212521" Z="1.6" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.6" />
                  <Point X="3.568838611732" Y="-2.759987191936" Z="1.6" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.6" />
                  <Point X="4.129646110749" Y="-2.954822748458" Z="1.6" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.6" />
                  <Point X="4.298664993916" Y="-2.703041476502" Z="1.6" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.6" />
                  <Point X="3.638626894399" Y="-1.95673195514" Z="1.6" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.6" />
                  <Point X="3.155607436142" Y="-1.556831372897" Z="1.6" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.6" />
                  <Point X="3.098075592046" Y="-1.395130306977" Z="1.6" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.6" />
                  <Point X="3.148550531802" Y="-1.238592256032" Z="1.6" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.6" />
                  <Point X="3.284837843957" Y="-1.140799217167" Z="1.6" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.6" />
                  <Point X="4.349790345386" Y="-1.241054861349" Z="1.6" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.6" />
                  <Point X="4.938210881963" Y="-1.177672980134" Z="1.6" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.6" />
                  <Point X="5.012347944449" Y="-0.805734117949" Z="1.6" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.6" />
                  <Point X="4.228428117927" Y="-0.349553631273" Z="1.6" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.6" />
                  <Point X="3.713762976839" Y="-0.201048364251" Z="1.6" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.6" />
                  <Point X="3.634928880998" Y="-0.141198762586" Z="1.6" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.6" />
                  <Point X="3.593098779145" Y="-0.060905333728" Z="1.6" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.6" />
                  <Point X="3.588272671279" Y="0.035705197472" Z="1.6" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.6" />
                  <Point X="3.620450557402" Y="0.122749975989" Z="1.6" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.6" />
                  <Point X="3.689632437513" Y="0.187100479991" Z="1.6" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.6" />
                  <Point X="4.56753969938" Y="0.440418301495" Z="1.6" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.6" />
                  <Point X="5.023659053977" Y="0.725596316931" Z="1.6" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.6" />
                  <Point X="4.944664425519" Y="1.14626759129" Z="1.6" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.6" />
                  <Point X="3.987059894729" Y="1.291001913915" Z="1.6" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.6" />
                  <Point X="3.428321587009" Y="1.226623304231" Z="1.6" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.6" />
                  <Point X="3.343222392264" Y="1.248956971634" Z="1.6" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.6" />
                  <Point X="3.281557501414" Y="1.300667047221" Z="1.6" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.6" />
                  <Point X="3.244730923139" Y="1.378364465881" Z="1.6" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.6" />
                  <Point X="3.241546828425" Y="1.460793646336" Z="1.6" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.6" />
                  <Point X="3.276471048083" Y="1.537173064049" Z="1.6" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.6" />
                  <Point X="4.028056695786" Y="2.133455527836" Z="1.6" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.6" />
                  <Point X="4.370022653266" Y="2.582882591032" Z="1.6" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.6" />
                  <Point X="4.150991921295" Y="2.921924569588" Z="1.6" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.6" />
                  <Point X="3.061431024315" Y="2.58543808285" Z="1.6" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.6" />
                  <Point X="2.480205824454" Y="2.259064062905" Z="1.6" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.6" />
                  <Point X="2.403933701274" Y="2.248623048876" Z="1.6" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.6" />
                  <Point X="2.336769233266" Y="2.26977672266" Z="1.6" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.6" />
                  <Point X="2.280981877575" Y="2.320255627116" Z="1.6" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.6" />
                  <Point X="2.250806653883" Y="2.385824742559" Z="1.6" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.6" />
                  <Point X="2.253463879752" Y="2.459263751428" Z="1.6" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.6" />
                  <Point X="2.81018727001" Y="3.45070805764" Z="1.6" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.6" />
                  <Point X="2.9899869792" Y="4.100853990124" Z="1.6" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.6" />
                  <Point X="2.60650339859" Y="4.354671302832" Z="1.6" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.6" />
                  <Point X="2.203595521768" Y="4.571916912069" Z="1.6" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.6" />
                  <Point X="1.786112590703" Y="4.750584706406" Z="1.6" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.6" />
                  <Point X="1.274966966355" Y="4.906659892244" Z="1.6" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.6" />
                  <Point X="0.615194406881" Y="5.03213361426" Z="1.6" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.6" />
                  <Point X="0.071419079773" Y="4.621664121516" Z="1.6" />
                  <Point X="0" Y="4.355124473572" Z="1.6" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>