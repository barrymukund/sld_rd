<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#195" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2883" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.128962890625" Y="-4.661000488281" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140136719" />
                  <Point X="-24.457634765625" Y="-3.467376953125" />
                  <Point X="-24.5834140625" Y="-3.286154296875" />
                  <Point X="-24.62136328125" Y="-3.2314765625" />
                  <Point X="-24.643248046875" Y="-3.209020751953" />
                  <Point X="-24.66950390625" Y="-3.189776855469" />
                  <Point X="-24.69750390625" Y="-3.175669189453" />
                  <Point X="-24.892138671875" Y="-3.11526171875" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.231458984375" Y="-3.157443115234" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.18977734375" />
                  <Point X="-25.344439453125" Y="-3.209021484375" />
                  <Point X="-25.36632421875" Y="-3.231477294922" />
                  <Point X="-25.4921015625" Y="-3.412699707031" />
                  <Point X="-25.53005078125" Y="-3.467377685547" />
                  <Point X="-25.5381875" Y="-3.481571777344" />
                  <Point X="-25.550990234375" Y="-3.512524658203" />
                  <Point X="-25.643837890625" Y="-3.859038574219" />
                  <Point X="-25.9165859375" Y="-4.876941894531" />
                  <Point X="-26.014142578125" Y="-4.858005371094" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.24641796875" Y="-4.802362304688" />
                  <Point X="-26.2149609375" Y="-4.563436523438" />
                  <Point X="-26.21419921875" Y="-4.547928710938" />
                  <Point X="-26.2165078125" Y="-4.516223632812" />
                  <Point X="-26.2630078125" Y="-4.282461914062" />
                  <Point X="-26.277037109375" Y="-4.211932128906" />
                  <Point X="-26.287939453125" Y="-4.182966308594" />
                  <Point X="-26.30401171875" Y="-4.155128417969" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.502837890625" Y="-3.974053955078" />
                  <Point X="-26.556904296875" Y="-3.926639404297" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.880859375" Y="-3.875377929688" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708740234" />
                  <Point X="-27.014466796875" Y="-3.882028564453" />
                  <Point X="-27.042658203125" Y="-3.894801757812" />
                  <Point X="-27.24083203125" Y="-4.027217529297" />
                  <Point X="-27.300623046875" Y="-4.067169677734" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.335099609375" Y="-4.0994609375" />
                  <Point X="-27.387228515625" Y="-4.167394042969" />
                  <Point X="-27.480146484375" Y="-4.288489257813" />
                  <Point X="-27.7061484375" Y="-4.148555175781" />
                  <Point X="-27.801712890625" Y="-4.089384033203" />
                  <Point X="-28.10472265625" Y="-3.856077636719" />
                  <Point X="-27.9956484375" Y="-3.667157470703" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647654296875" />
                  <Point X="-27.406587890625" Y="-2.616127441406" />
                  <Point X="-27.40557421875" Y="-2.585193603516" />
                  <Point X="-27.41455859375" Y="-2.555575683594" />
                  <Point X="-27.428775390625" Y="-2.526746582031" />
                  <Point X="-27.4468046875" Y="-2.501588134766" />
                  <Point X="-27.464146484375" Y="-2.484246582031" />
                  <Point X="-27.4892890625" Y="-2.466225585938" />
                  <Point X="-27.518119140625" Y="-2.452002685547" />
                  <Point X="-27.547740234375" Y="-2.443013183594" />
                  <Point X="-27.5786796875" Y="-2.444023681641" />
                  <Point X="-27.6102109375" Y="-2.450294189453" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-27.938044921875" Y="-2.633745117188" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-29.00736328125" Y="-2.893048095703" />
                  <Point X="-29.082857421875" Y="-2.793862060547" />
                  <Point X="-29.304169921875" Y="-2.422758544922" />
                  <Point X="-29.306142578125" Y="-2.419450195312" />
                  <Point X="-29.106572265625" Y="-2.266314208984" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.084580078125" Y="-1.475596191406" />
                  <Point X="-28.066615234375" Y="-1.448466918945" />
                  <Point X="-28.053857421875" Y="-1.419839111328" />
                  <Point X="-28.0479375" Y="-1.396986694336" />
                  <Point X="-28.046150390625" Y="-1.390085449219" />
                  <Point X="-28.043345703125" Y="-1.359645874023" />
                  <Point X="-28.045556640625" Y="-1.32797668457" />
                  <Point X="-28.052560546875" Y="-1.298231933594" />
                  <Point X="-28.06864453125" Y="-1.272249267578" />
                  <Point X="-28.089478515625" Y="-1.248294677734" />
                  <Point X="-28.11297265625" Y="-1.228766845703" />
                  <Point X="-28.13331640625" Y="-1.21679296875" />
                  <Point X="-28.139455078125" Y="-1.213180419922" />
                  <Point X="-28.168720703125" Y="-1.201955322266" />
                  <Point X="-28.20060546875" Y="-1.195474609375" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-28.609212890625" Y="-1.244054321289" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.804548828125" Y="-1.108256347656" />
                  <Point X="-29.83407421875" Y="-0.992652893066" />
                  <Point X="-29.892423828125" Y="-0.584698303223" />
                  <Point X="-29.672728515625" Y="-0.525831115723" />
                  <Point X="-28.532875" Y="-0.220408279419" />
                  <Point X="-28.517494140625" Y="-0.214828155518" />
                  <Point X="-28.48773046875" Y="-0.199471221924" />
                  <Point X="-28.46641015625" Y="-0.184673629761" />
                  <Point X="-28.4599765625" Y="-0.180209030151" />
                  <Point X="-28.437525390625" Y="-0.158329818726" />
                  <Point X="-28.418279296875" Y="-0.132074783325" />
                  <Point X="-28.404166015625" Y="-0.104064453125" />
                  <Point X="-28.397060546875" Y="-0.081166275024" />
                  <Point X="-28.394916015625" Y="-0.074257530212" />
                  <Point X="-28.3906484375" Y="-0.04610269928" />
                  <Point X="-28.3906484375" Y="-0.016457435608" />
                  <Point X="-28.394916015625" Y="0.011697399139" />
                  <Point X="-28.402021484375" Y="0.034595581055" />
                  <Point X="-28.404166015625" Y="0.041504470825" />
                  <Point X="-28.418279296875" Y="0.069514648438" />
                  <Point X="-28.437525390625" Y="0.095769828796" />
                  <Point X="-28.459978515625" Y="0.11764919281" />
                  <Point X="-28.481298828125" Y="0.132446624756" />
                  <Point X="-28.490904296875" Y="0.138303466797" />
                  <Point X="-28.51290234375" Y="0.149986618042" />
                  <Point X="-28.532875" Y="0.157848297119" />
                  <Point X="-28.876787109375" Y="0.249999313354" />
                  <Point X="-29.891814453125" Y="0.521975402832" />
                  <Point X="-29.843515625" Y="0.848377990723" />
                  <Point X="-29.82448828125" Y="0.976968811035" />
                  <Point X="-29.70662109375" Y="1.411932739258" />
                  <Point X="-29.703548828125" Y="1.423267822266" />
                  <Point X="-29.582740234375" Y="1.407362792969" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744984375" Y="1.299341674805" />
                  <Point X="-28.723423828125" Y="1.301227783203" />
                  <Point X="-28.703134765625" Y="1.305263427734" />
                  <Point X="-28.655947265625" Y="1.320141967773" />
                  <Point X="-28.641708984375" Y="1.324630859375" />
                  <Point X="-28.62277734375" Y="1.332961791992" />
                  <Point X="-28.604033203125" Y="1.343783935547" />
                  <Point X="-28.5873515625" Y="1.356015625" />
                  <Point X="-28.573712890625" Y="1.371567993164" />
                  <Point X="-28.561298828125" Y="1.389297851562" />
                  <Point X="-28.551349609375" Y="1.407432617188" />
                  <Point X="-28.532416015625" Y="1.453144775391" />
                  <Point X="-28.526703125" Y="1.466936889648" />
                  <Point X="-28.520916015625" Y="1.486789428711" />
                  <Point X="-28.51715625" Y="1.508106567383" />
                  <Point X="-28.515802734375" Y="1.528751708984" />
                  <Point X="-28.518951171875" Y="1.549200195312" />
                  <Point X="-28.5245546875" Y="1.570108154297" />
                  <Point X="-28.532048828125" Y="1.589379760742" />
                  <Point X="-28.55489453125" Y="1.633267822266" />
                  <Point X="-28.5617890625" Y="1.646509643555" />
                  <Point X="-28.5732890625" Y="1.663716674805" />
                  <Point X="-28.587201171875" Y="1.680293334961" />
                  <Point X="-28.60213671875" Y="1.69458996582" />
                  <Point X="-28.799404296875" Y="1.845959960938" />
                  <Point X="-29.351859375" Y="2.269873535156" />
                  <Point X="-29.155091796875" Y="2.606983886719" />
                  <Point X="-29.0811484375" Y="2.733664794922" />
                  <Point X="-28.76893359375" Y="3.134973144531" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.70848046875" Y="3.134399902344" />
                  <Point X="-28.20665625" Y="2.844671142578" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.081072265625" Y="2.820046630859" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.99901171875" Y="2.826505126953" />
                  <Point X="-27.980458984375" Y="2.835655029297" />
                  <Point X="-27.962205078125" Y="2.847284912109" />
                  <Point X="-27.946078125" Y="2.860229003906" />
                  <Point X="-27.899427734375" Y="2.906877685547" />
                  <Point X="-27.885353515625" Y="2.920952392578" />
                  <Point X="-27.872404296875" Y="2.937084960938" />
                  <Point X="-27.860775390625" Y="2.955338623047" />
                  <Point X="-27.85162890625" Y="2.973887939453" />
                  <Point X="-27.8467109375" Y="2.993977050781" />
                  <Point X="-27.843884765625" Y="3.015434814453" />
                  <Point X="-27.84343359375" Y="3.036120117188" />
                  <Point X="-27.84918359375" Y="3.101840576172" />
                  <Point X="-27.85091796875" Y="3.121669677734" />
                  <Point X="-27.854955078125" Y="3.141960449219" />
                  <Point X="-27.86146484375" Y="3.162603759766" />
                  <Point X="-27.869794921875" Y="3.181532958984" />
                  <Point X="-27.9572109375" Y="3.332941650391" />
                  <Point X="-28.18333203125" Y="3.724595703125" />
                  <Point X="-27.829396484375" Y="3.995954345703" />
                  <Point X="-27.70062109375" Y="4.094686279297" />
                  <Point X="-27.208892578125" Y="4.367880371094" />
                  <Point X="-27.16703515625" Y="4.391134765625" />
                  <Point X="-27.0431953125" Y="4.229741699219" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.99511328125" Y="4.189395019531" />
                  <Point X="-26.921966796875" Y="4.151316894531" />
                  <Point X="-26.899896484375" Y="4.139828125" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.797314453125" Y="4.128692871094" />
                  <Point X="-26.777453125" Y="4.134481933594" />
                  <Point X="-26.701267578125" Y="4.166040039062" />
                  <Point X="-26.678279296875" Y="4.175561523437" />
                  <Point X="-26.6601484375" Y="4.185508300781" />
                  <Point X="-26.64241796875" Y="4.197922363281" />
                  <Point X="-26.626865234375" Y="4.211560546875" />
                  <Point X="-26.6146328125" Y="4.228241699219" />
                  <Point X="-26.603810546875" Y="4.246985351563" />
                  <Point X="-26.595478515625" Y="4.265920898438" />
                  <Point X="-26.570681640625" Y="4.344568359375" />
                  <Point X="-26.56319921875" Y="4.368297363281" />
                  <Point X="-26.5591640625" Y="4.388583007812" />
                  <Point X="-26.55727734375" Y="4.410145507812" />
                  <Point X="-26.557728515625" Y="4.430827636719" />
                  <Point X="-26.56766796875" Y="4.506315917969" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.116337890625" Y="4.763070800781" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.3535078125" Y="4.879576171875" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.282357421875" Y="4.840352539062" />
                  <Point X="-25.133904296875" Y="4.286315917969" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.8089921875" Y="4.453477539062" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.301521484375" Y="4.846983398438" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.66276171875" Y="4.712666503906" />
                  <Point X="-23.51897265625" Y="4.677950195312" />
                  <Point X="-23.19829296875" Y="4.561638183594" />
                  <Point X="-23.105353515625" Y="4.527928222656" />
                  <Point X="-22.794939453125" Y="4.3827578125" />
                  <Point X="-22.705423828125" Y="4.340893554688" />
                  <Point X="-22.405525390625" Y="4.166173339844" />
                  <Point X="-22.319021484375" Y="4.115775878906" />
                  <Point X="-22.05673828125" Y="3.929253417969" />
                  <Point X="-22.1905" Y="3.697571289062" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857919921875" Y="2.539935546875" />
                  <Point X="-22.866921875" Y="2.516058349609" />
                  <Point X="-22.883416015625" Y="2.454381103516" />
                  <Point X="-22.885322265625" Y="2.445608886719" />
                  <Point X="-22.89163671875" Y="2.408099365234" />
                  <Point X="-22.892271484375" Y="2.380954589844" />
                  <Point X="-22.88583984375" Y="2.327621337891" />
                  <Point X="-22.883900390625" Y="2.311529785156" />
                  <Point X="-22.878560546875" Y="2.289613525391" />
                  <Point X="-22.870294921875" Y="2.267521728516" />
                  <Point X="-22.8599296875" Y="2.247469726562" />
                  <Point X="-22.826927734375" Y="2.198834960938" />
                  <Point X="-22.821365234375" Y="2.191441894531" />
                  <Point X="-22.798107421875" Y="2.163468505859" />
                  <Point X="-22.778400390625" Y="2.145592529297" />
                  <Point X="-22.729765625" Y="2.112591796875" />
                  <Point X="-22.715091796875" Y="2.102635009766" />
                  <Point X="-22.695046875" Y="2.092272705078" />
                  <Point X="-22.672958984375" Y="2.084006591797" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.597703125" Y="2.072232421875" />
                  <Point X="-22.587966796875" Y="2.071563232422" />
                  <Point X="-22.55297265625" Y="2.070960449219" />
                  <Point X="-22.526794921875" Y="2.074171142578" />
                  <Point X="-22.4651171875" Y="2.090664550781" />
                  <Point X="-22.4602890625" Y="2.092093505859" />
                  <Point X="-22.429595703125" Y="2.102071289063" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-22.0655546875" Y="2.309855957031" />
                  <Point X="-21.032673828125" Y="2.906190673828" />
                  <Point X="-20.928041015625" Y="2.760775390625" />
                  <Point X="-20.87671875" Y="2.689449951172" />
                  <Point X="-20.73780078125" Y="2.459883300781" />
                  <Point X="-20.89773828125" Y="2.337157714844" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.778572265625" Y="1.660242797852" />
                  <Point X="-21.79602734375" Y="1.641626831055" />
                  <Point X="-21.840416015625" Y="1.583717529297" />
                  <Point X="-21.85380859375" Y="1.566245483398" />
                  <Point X="-21.863390625" Y="1.550915893555" />
                  <Point X="-21.878369140625" Y="1.51708972168" />
                  <Point X="-21.894904296875" Y="1.457964111328" />
                  <Point X="-21.89989453125" Y="1.44012512207" />
                  <Point X="-21.90334765625" Y="1.417826904297" />
                  <Point X="-21.9041640625" Y="1.394252563477" />
                  <Point X="-21.902259765625" Y="1.371766357422" />
                  <Point X="-21.888685546875" Y="1.305981811523" />
                  <Point X="-21.88458984375" Y="1.286133544922" />
                  <Point X="-21.8793203125" Y="1.268977905273" />
                  <Point X="-21.86371875" Y="1.235741577148" />
                  <Point X="-21.82680078125" Y="1.179626708984" />
                  <Point X="-21.815662109375" Y="1.162696166992" />
                  <Point X="-21.80110546875" Y="1.145448974609" />
                  <Point X="-21.783861328125" Y="1.129359863281" />
                  <Point X="-21.76565234375" Y="1.11603515625" />
                  <Point X="-21.712150390625" Y="1.085919189453" />
                  <Point X="-21.703966796875" Y="1.081818847656" />
                  <Point X="-21.669849609375" Y="1.066733886719" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.571544921875" Y="1.049878540039" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-21.183205078125" Y="1.090244384766" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.176103515625" Y="1.023336547852" />
                  <Point X="-20.15405859375" Y="0.932784545898" />
                  <Point X="-20.109134765625" Y="0.644238647461" />
                  <Point X="-20.284689453125" Y="0.597198486328" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.295212890625" Y="0.325584533691" />
                  <Point X="-21.318453125" Y="0.315067749023" />
                  <Point X="-21.389521484375" Y="0.273989318848" />
                  <Point X="-21.410962890625" Y="0.261595336914" />
                  <Point X="-21.425685546875" Y="0.251097351074" />
                  <Point X="-21.45246875" Y="0.225576538086" />
                  <Point X="-21.495109375" Y="0.171242416382" />
                  <Point X="-21.507974609375" Y="0.154848907471" />
                  <Point X="-21.519697265625" Y="0.135569717407" />
                  <Point X="-21.529470703125" Y="0.114107467651" />
                  <Point X="-21.536318359375" Y="0.092604545593" />
                  <Point X="-21.55053125" Y="0.018386648178" />
                  <Point X="-21.5548203125" Y="-0.004006072044" />
                  <Point X="-21.556515625" Y="-0.021874074936" />
                  <Point X="-21.5548203125" Y="-0.05855406189" />
                  <Point X="-21.540607421875" Y="-0.13277180481" />
                  <Point X="-21.536318359375" Y="-0.155164520264" />
                  <Point X="-21.529470703125" Y="-0.176667449951" />
                  <Point X="-21.519697265625" Y="-0.198129699707" />
                  <Point X="-21.507974609375" Y="-0.217408737183" />
                  <Point X="-21.465333984375" Y="-0.271743011475" />
                  <Point X="-21.45246875" Y="-0.288136535645" />
                  <Point X="-21.439998046875" Y="-0.301237243652" />
                  <Point X="-21.410962890625" Y="-0.324155456543" />
                  <Point X="-21.33989453125" Y="-0.365233886719" />
                  <Point X="-21.318453125" Y="-0.377627868652" />
                  <Point X="-21.30729296875" Y="-0.383136657715" />
                  <Point X="-21.283419921875" Y="-0.392149963379" />
                  <Point X="-20.9820859375" Y="-0.472891815186" />
                  <Point X="-20.108525390625" Y="-0.706961853027" />
                  <Point X="-20.132669921875" Y="-0.867107177734" />
                  <Point X="-20.144974609375" Y="-0.948725036621" />
                  <Point X="-20.19882421875" Y="-1.18469909668" />
                  <Point X="-20.41737109375" Y="-1.155926879883" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710266113" />
                  <Point X="-21.62533984375" Y="-1.005508666992" />
                  <Point X="-21.7648203125" Y="-1.035825683594" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.8360234375" Y="-1.056596557617" />
                  <Point X="-21.863849609375" Y="-1.07348840332" />
                  <Point X="-21.8876015625" Y="-1.093959838867" />
                  <Point X="-21.971908203125" Y="-1.195355224609" />
                  <Point X="-21.997345703125" Y="-1.225947875977" />
                  <Point X="-22.01206640625" Y="-1.250330932617" />
                  <Point X="-22.02341015625" Y="-1.277718139648" />
                  <Point X="-22.030240234375" Y="-1.305366455078" />
                  <Point X="-22.04232421875" Y="-1.436677978516" />
                  <Point X="-22.04596875" Y="-1.47629675293" />
                  <Point X="-22.043650390625" Y="-1.507568603516" />
                  <Point X="-22.03591796875" Y="-1.539189208984" />
                  <Point X="-22.023546875" Y="-1.567997192383" />
                  <Point X="-21.946357421875" Y="-1.688062133789" />
                  <Point X="-21.92306640625" Y="-1.724287841797" />
                  <Point X="-21.913064453125" Y="-1.737240234375" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.60973046875" Y="-1.975483520508" />
                  <Point X="-20.786876953125" Y="-2.606882324219" />
                  <Point X="-20.840501953125" Y="-2.693656005859" />
                  <Point X="-20.875201171875" Y="-2.749801269531" />
                  <Point X="-20.971017578125" Y="-2.885944824219" />
                  <Point X="-21.167470703125" Y="-2.772522949219" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.2457734375" Y="-2.159825195312" />
                  <Point X="-22.41177734375" Y="-2.129844970703" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135177001953" />
                  <Point X="-22.69307421875" Y="-2.207757324219" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.75761328125" Y="-2.246547851562" />
                  <Point X="-22.778572265625" Y="-2.267506347656" />
                  <Point X="-22.795466796875" Y="-2.290437988281" />
                  <Point X="-22.868046875" Y="-2.428346679688" />
                  <Point X="-22.889947265625" Y="-2.469956054688" />
                  <Point X="-22.899771484375" Y="-2.499735107422" />
                  <Point X="-22.904728515625" Y="-2.531910400391" />
                  <Point X="-22.90432421875" Y="-2.563259765625" />
                  <Point X="-22.87434375" Y="-2.729263916016" />
                  <Point X="-22.865296875" Y="-2.779350097656" />
                  <Point X="-22.86101171875" Y="-2.795143066406" />
                  <Point X="-22.8481796875" Y="-2.826078369141" />
                  <Point X="-22.668484375" Y="-3.137321044922" />
                  <Point X="-22.13871484375" Y="-4.054907226563" />
                  <Point X="-22.176984375" Y="-4.082241699219" />
                  <Point X="-22.218134765625" Y="-4.111634277344" />
                  <Point X="-22.298234375" Y="-4.163481445312" />
                  <Point X="-22.454265625" Y="-3.960135742188" />
                  <Point X="-23.241451171875" Y="-2.934255371094" />
                  <Point X="-23.25249609375" Y="-2.922177490234" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.44180078125" Y="-2.795297119141" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.520009765625" Y="-2.751167236328" />
                  <Point X="-23.551630859375" Y="-2.743435546875" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.7619609375" Y="-2.757593994141" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.843634765625" Y="-2.769396972656" />
                  <Point X="-23.87101953125" Y="-2.780739746094" />
                  <Point X="-23.89540234375" Y="-2.7954609375" />
                  <Point X="-24.03366796875" Y="-2.910424804688" />
                  <Point X="-24.07538671875" Y="-2.945111328125" />
                  <Point X="-24.095857421875" Y="-2.968859863281" />
                  <Point X="-24.11275" Y="-2.996685791016" />
                  <Point X="-24.124375" Y="-3.025808105469" />
                  <Point X="-24.16571484375" Y="-3.216009277344" />
                  <Point X="-24.178189453125" Y="-3.273395996094" />
                  <Point X="-24.180275390625" Y="-3.289626708984" />
                  <Point X="-24.1802578125" Y="-3.323120605469" />
                  <Point X="-24.12933203125" Y="-3.709930419922" />
                  <Point X="-23.977935546875" Y="-4.859915527344" />
                  <Point X="-23.985380859375" Y="-4.861547851562" />
                  <Point X="-24.024318359375" Y="-4.870083007812" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-25.996041015625" Y="-4.76474609375" />
                  <Point X="-26.05842578125" Y="-4.75263671875" />
                  <Point X="-26.14124609375" Y="-4.731328125" />
                  <Point X="-26.1207734375" Y="-4.575837402344" />
                  <Point X="-26.120076171875" Y="-4.568097167969" />
                  <Point X="-26.11944921875" Y="-4.541029785156" />
                  <Point X="-26.1217578125" Y="-4.509324707031" />
                  <Point X="-26.123333984375" Y="-4.497689453125" />
                  <Point X="-26.169833984375" Y="-4.263927734375" />
                  <Point X="-26.18386328125" Y="-4.193397949219" />
                  <Point X="-26.188126953125" Y="-4.178467285156" />
                  <Point X="-26.199029296875" Y="-4.149501464844" />
                  <Point X="-26.20566796875" Y="-4.135466308594" />
                  <Point X="-26.221740234375" Y="-4.107628417969" />
                  <Point X="-26.23057421875" Y="-4.094863525391" />
                  <Point X="-26.25020703125" Y="-4.070939208984" />
                  <Point X="-26.261005859375" Y="-4.059779785156" />
                  <Point X="-26.44019921875" Y="-3.902629638672" />
                  <Point X="-26.494265625" Y="-3.855215087891" />
                  <Point X="-26.50673828125" Y="-3.845965332031" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.874646484375" Y="-3.780581298828" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166259766" />
                  <Point X="-27.008005859375" Y="-3.781946044922" />
                  <Point X="-27.0390546875" Y="-3.790265869141" />
                  <Point X="-27.053673828125" Y="-3.795496337891" />
                  <Point X="-27.081865234375" Y="-3.80826953125" />
                  <Point X="-27.0954375" Y="-3.815812255859" />
                  <Point X="-27.293611328125" Y="-3.948228027344" />
                  <Point X="-27.35340234375" Y="-3.988180175781" />
                  <Point X="-27.35968359375" Y="-3.992759765625" />
                  <Point X="-27.38044140625" Y="-4.010133056641" />
                  <Point X="-27.402755859375" Y="-4.032770996094" />
                  <Point X="-27.410466796875" Y="-4.041627197266" />
                  <Point X="-27.462595703125" Y="-4.109560546875" />
                  <Point X="-27.503201171875" Y="-4.162478515625" />
                  <Point X="-27.65613671875" Y="-4.067784423828" />
                  <Point X="-27.747591796875" Y="-4.011157714844" />
                  <Point X="-27.98086328125" Y="-3.831547119141" />
                  <Point X="-27.913376953125" Y="-3.714657714844" />
                  <Point X="-27.341490234375" Y="-2.724119873047" />
                  <Point X="-27.3348515625" Y="-2.710084960938" />
                  <Point X="-27.32394921875" Y="-2.681119628906" />
                  <Point X="-27.319685546875" Y="-2.666188964844" />
                  <Point X="-27.3134140625" Y="-2.634662109375" />
                  <Point X="-27.311638671875" Y="-2.619238769531" />
                  <Point X="-27.310625" Y="-2.588304931641" />
                  <Point X="-27.3146640625" Y="-2.557616943359" />
                  <Point X="-27.3236484375" Y="-2.527999023438" />
                  <Point X="-27.32935546875" Y="-2.51355859375" />
                  <Point X="-27.343572265625" Y="-2.484729492188" />
                  <Point X="-27.351556640625" Y="-2.471409179688" />
                  <Point X="-27.3695859375" Y="-2.446250732422" />
                  <Point X="-27.379630859375" Y="-2.434412597656" />
                  <Point X="-27.39697265625" Y="-2.417071044922" />
                  <Point X="-27.408802734375" Y="-2.407031982422" />
                  <Point X="-27.4339453125" Y="-2.389010986328" />
                  <Point X="-27.4472578125" Y="-2.381029052734" />
                  <Point X="-27.476087890625" Y="-2.366806152344" />
                  <Point X="-27.49053125" Y="-2.361096679688" />
                  <Point X="-27.52015234375" Y="-2.352107177734" />
                  <Point X="-27.550841796875" Y="-2.348063720703" />
                  <Point X="-27.58178125" Y="-2.34907421875" />
                  <Point X="-27.597208984375" Y="-2.350848144531" />
                  <Point X="-27.628740234375" Y="-2.357118652344" />
                  <Point X="-27.643671875" Y="-2.361382324219" />
                  <Point X="-27.672642578125" Y="-2.372285400391" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-27.985544921875" Y="-2.55147265625" />
                  <Point X="-28.793087890625" Y="-3.017708251953" />
                  <Point X="-28.93176953125" Y="-2.835509765625" />
                  <Point X="-29.00401953125" Y="-2.740586669922" />
                  <Point X="-29.181265625" Y="-2.443373291016" />
                  <Point X="-29.048740234375" Y="-2.341682617188" />
                  <Point X="-28.048123046875" Y="-1.573881958008" />
                  <Point X="-28.036482421875" Y="-1.563310302734" />
                  <Point X="-28.015107421875" Y="-1.540393066406" />
                  <Point X="-28.005373046875" Y="-1.528047241211" />
                  <Point X="-27.987408203125" Y="-1.50091796875" />
                  <Point X="-27.979841796875" Y="-1.487137084961" />
                  <Point X="-27.967083984375" Y="-1.458509155273" />
                  <Point X="-27.961892578125" Y="-1.443662475586" />
                  <Point X="-27.95597265625" Y="-1.420810058594" />
                  <Point X="-27.95155078125" Y="-1.398801757812" />
                  <Point X="-27.94874609375" Y="-1.368362182617" />
                  <Point X="-27.948576171875" Y="-1.353029663086" />
                  <Point X="-27.950787109375" Y="-1.321360473633" />
                  <Point X="-27.9530859375" Y="-1.306202758789" />
                  <Point X="-27.96008984375" Y="-1.276458007812" />
                  <Point X="-27.97178515625" Y="-1.248229370117" />
                  <Point X="-27.987869140625" Y="-1.222246704102" />
                  <Point X="-27.996962890625" Y="-1.209905639648" />
                  <Point X="-28.017796875" Y="-1.185951049805" />
                  <Point X="-28.02875390625" Y="-1.175236206055" />
                  <Point X="-28.052248046875" Y="-1.155708496094" />
                  <Point X="-28.06478515625" Y="-1.146895385742" />
                  <Point X="-28.08512890625" Y="-1.134921508789" />
                  <Point X="-28.10543359375" Y="-1.124481201172" />
                  <Point X="-28.13469921875" Y="-1.113256103516" />
                  <Point X="-28.149798828125" Y="-1.108858886719" />
                  <Point X="-28.18168359375" Y="-1.102378173828" />
                  <Point X="-28.197298828125" Y="-1.100532104492" />
                  <Point X="-28.22862109375" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196655273" />
                  <Point X="-28.62161328125" Y="-1.14986706543" />
                  <Point X="-29.66091796875" Y="-1.286694335938" />
                  <Point X="-29.71250390625" Y="-1.084744628906" />
                  <Point X="-29.7407578125" Y="-0.974116943359" />
                  <Point X="-29.786451171875" Y="-0.654654296875" />
                  <Point X="-29.648140625" Y="-0.617594055176" />
                  <Point X="-28.508287109375" Y="-0.312171173096" />
                  <Point X="-28.5004765625" Y="-0.309712677002" />
                  <Point X="-28.47393359375" Y="-0.29925289917" />
                  <Point X="-28.444169921875" Y="-0.283895965576" />
                  <Point X="-28.4335625" Y="-0.277515441895" />
                  <Point X="-28.4122421875" Y="-0.262717803955" />
                  <Point X="-28.393673828125" Y="-0.24824520874" />
                  <Point X="-28.37122265625" Y="-0.226365982056" />
                  <Point X="-28.36090625" Y="-0.214494873047" />
                  <Point X="-28.34166015625" Y="-0.188239898682" />
                  <Point X="-28.333439453125" Y="-0.174821777344" />
                  <Point X="-28.319326171875" Y="-0.146811569214" />
                  <Point X="-28.31343359375" Y="-0.132219329834" />
                  <Point X="-28.306328125" Y="-0.109321144104" />
                  <Point X="-28.30098828125" Y="-0.088494613647" />
                  <Point X="-28.296720703125" Y="-0.06033978653" />
                  <Point X="-28.2956484375" Y="-0.0461027565" />
                  <Point X="-28.2956484375" Y="-0.016457389832" />
                  <Point X="-28.296720703125" Y="-0.002220356941" />
                  <Point X="-28.30098828125" Y="0.025934467316" />
                  <Point X="-28.30418359375" Y="0.03985226059" />
                  <Point X="-28.3112890625" Y="0.062750442505" />
                  <Point X="-28.319326171875" Y="0.084251716614" />
                  <Point X="-28.333439453125" Y="0.112261932373" />
                  <Point X="-28.34166015625" Y="0.125679603577" />
                  <Point X="-28.36090625" Y="0.151934738159" />
                  <Point X="-28.371224609375" Y="0.163808670044" />
                  <Point X="-28.393677734375" Y="0.185688034058" />
                  <Point X="-28.405810546875" Y="0.195693634033" />
                  <Point X="-28.427130859375" Y="0.210491119385" />
                  <Point X="-28.44634375" Y="0.222204681396" />
                  <Point X="-28.468341796875" Y="0.233887786865" />
                  <Point X="-28.478107421875" Y="0.238384963989" />
                  <Point X="-28.498080078125" Y="0.246246673584" />
                  <Point X="-28.508287109375" Y="0.249611190796" />
                  <Point X="-28.85219921875" Y="0.341762298584" />
                  <Point X="-29.785443359375" Y="0.591824829102" />
                  <Point X="-29.7495390625" Y="0.834472045898" />
                  <Point X="-29.73133203125" Y="0.957522583008" />
                  <Point X="-29.633583984375" Y="1.318236816406" />
                  <Point X="-29.595140625" Y="1.313175537109" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.747056640625" Y="1.204364257812" />
                  <Point X="-28.736705078125" Y="1.204703125" />
                  <Point X="-28.71514453125" Y="1.206589355469" />
                  <Point X="-28.704890625" Y="1.208053100586" />
                  <Point X="-28.6846015625" Y="1.212088745117" />
                  <Point X="-28.67456640625" Y="1.214660522461" />
                  <Point X="-28.62737890625" Y="1.2295390625" />
                  <Point X="-28.6034453125" Y="1.237677612305" />
                  <Point X="-28.584513671875" Y="1.246008544922" />
                  <Point X="-28.57527734375" Y="1.250689819336" />
                  <Point X="-28.556533203125" Y="1.261511962891" />
                  <Point X="-28.547857421875" Y="1.267172119141" />
                  <Point X="-28.53117578125" Y="1.279403808594" />
                  <Point X="-28.51592578125" Y="1.29337878418" />
                  <Point X="-28.502287109375" Y="1.308931152344" />
                  <Point X="-28.495892578125" Y="1.317079711914" />
                  <Point X="-28.483478515625" Y="1.334809570312" />
                  <Point X="-28.478009765625" Y="1.343603393555" />
                  <Point X="-28.468060546875" Y="1.36173828125" />
                  <Point X="-28.463580078125" Y="1.371079345703" />
                  <Point X="-28.444646484375" Y="1.416791625977" />
                  <Point X="-28.4355" Y="1.440350585938" />
                  <Point X="-28.429712890625" Y="1.46020300293" />
                  <Point X="-28.427359375" Y="1.470288696289" />
                  <Point X="-28.423599609375" Y="1.491605834961" />
                  <Point X="-28.422359375" Y="1.501891601563" />
                  <Point X="-28.421005859375" Y="1.522536743164" />
                  <Point X="-28.421908203125" Y="1.543208374023" />
                  <Point X="-28.425056640625" Y="1.563656860352" />
                  <Point X="-28.427189453125" Y="1.57379309082" />
                  <Point X="-28.43279296875" Y="1.594701049805" />
                  <Point X="-28.436013671875" Y="1.6045390625" />
                  <Point X="-28.4435078125" Y="1.623810668945" />
                  <Point X="-28.44778125" Y="1.633244384766" />
                  <Point X="-28.470626953125" Y="1.677132324219" />
                  <Point X="-28.4828046875" Y="1.699297119141" />
                  <Point X="-28.4943046875" Y="1.716504272461" />
                  <Point X="-28.500521484375" Y="1.724788330078" />
                  <Point X="-28.51443359375" Y="1.741364990234" />
                  <Point X="-28.521509765625" Y="1.748920410156" />
                  <Point X="-28.5364453125" Y="1.763217041016" />
                  <Point X="-28.5443046875" Y="1.769958251953" />
                  <Point X="-28.741572265625" Y="1.92132824707" />
                  <Point X="-29.22761328125" Y="2.294281005859" />
                  <Point X="-29.073044921875" Y="2.559094482422" />
                  <Point X="-29.00228515625" Y="2.680321044922" />
                  <Point X="-28.726337890625" Y="3.035012939453" />
                  <Point X="-28.25415625" Y="2.762398681641" />
                  <Point X="-28.24491796875" Y="2.757716796875" />
                  <Point X="-28.225984375" Y="2.749385742188" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.0893515625" Y="2.725408203125" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310791016" />
                  <Point X="-27.976431640625" Y="2.734227783203" />
                  <Point X="-27.9569921875" Y="2.741303466797" />
                  <Point X="-27.938439453125" Y="2.750453369141" />
                  <Point X="-27.929412109375" Y="2.755534667969" />
                  <Point X="-27.911158203125" Y="2.767164550781" />
                  <Point X="-27.902740234375" Y="2.773197753906" />
                  <Point X="-27.88661328125" Y="2.786141845703" />
                  <Point X="-27.878904296875" Y="2.793052734375" />
                  <Point X="-27.83225390625" Y="2.839701416016" />
                  <Point X="-27.811267578125" Y="2.861485595703" />
                  <Point X="-27.798318359375" Y="2.877618164062" />
                  <Point X="-27.79228125" Y="2.886041259766" />
                  <Point X="-27.78065234375" Y="2.904294921875" />
                  <Point X="-27.7755703125" Y="2.913324951172" />
                  <Point X="-27.766423828125" Y="2.931874267578" />
                  <Point X="-27.759353515625" Y="2.951298339844" />
                  <Point X="-27.754435546875" Y="2.971387451172" />
                  <Point X="-27.7525234375" Y="2.981571777344" />
                  <Point X="-27.749697265625" Y="3.003029541016" />
                  <Point X="-27.748908203125" Y="3.01336328125" />
                  <Point X="-27.74845703125" Y="3.034048583984" />
                  <Point X="-27.748794921875" Y="3.044400146484" />
                  <Point X="-27.754544921875" Y="3.110120605469" />
                  <Point X="-27.757744140625" Y="3.140207763672" />
                  <Point X="-27.76178125" Y="3.160498535156" />
                  <Point X="-27.764353515625" Y="3.17053125" />
                  <Point X="-27.77086328125" Y="3.191174560547" />
                  <Point X="-27.77451171875" Y="3.200868652344" />
                  <Point X="-27.782841796875" Y="3.219797851562" />
                  <Point X="-27.7875234375" Y="3.229032958984" />
                  <Point X="-27.874939453125" Y="3.380441650391" />
                  <Point X="-28.05938671875" Y="3.699914550781" />
                  <Point X="-27.77159375" Y="3.9205625" />
                  <Point X="-27.648365234375" Y="4.015041748047" />
                  <Point X="-27.192525390625" Y="4.268297363281" />
                  <Point X="-27.118564453125" Y="4.171909667969" />
                  <Point X="-27.111822265625" Y="4.164050292969" />
                  <Point X="-27.09751953125" Y="4.149108398438" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.065091796875" Y="4.121897460938" />
                  <Point X="-27.047892578125" Y="4.110405273438" />
                  <Point X="-27.03898046875" Y="4.105129394531" />
                  <Point X="-26.965833984375" Y="4.067051025391" />
                  <Point X="-26.943763671875" Y="4.055562255859" />
                  <Point X="-26.934326171875" Y="4.051286621094" />
                  <Point X="-26.915044921875" Y="4.0437890625" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031377929688" />
                  <Point X="-26.7808203125" Y="4.035135742188" />
                  <Point X="-26.77073046875" Y="4.037488037109" />
                  <Point X="-26.750869140625" Y="4.043277099609" />
                  <Point X="-26.74109765625" Y="4.046713867188" />
                  <Point X="-26.664912109375" Y="4.078271972656" />
                  <Point X="-26.641923828125" Y="4.087793457031" />
                  <Point X="-26.6325859375" Y="4.092272216797" />
                  <Point X="-26.614455078125" Y="4.10221875" />
                  <Point X="-26.605662109375" Y="4.107687011719" />
                  <Point X="-26.587931640625" Y="4.120101074219" />
                  <Point X="-26.579783203125" Y="4.126494628906" />
                  <Point X="-26.56423046875" Y="4.1401328125" />
                  <Point X="-26.550255859375" Y="4.155382324219" />
                  <Point X="-26.5380234375" Y="4.172063476562" />
                  <Point X="-26.532361328125" Y="4.180739746094" />
                  <Point X="-26.5215390625" Y="4.199483398437" />
                  <Point X="-26.51685546875" Y="4.208723632812" />
                  <Point X="-26.5085234375" Y="4.227659179688" />
                  <Point X="-26.504875" Y="4.237354492188" />
                  <Point X="-26.480078125" Y="4.316001953125" />
                  <Point X="-26.472595703125" Y="4.339730957031" />
                  <Point X="-26.470025390625" Y="4.349763183594" />
                  <Point X="-26.465990234375" Y="4.370048828125" />
                  <Point X="-26.464525390625" Y="4.380302246094" />
                  <Point X="-26.462638671875" Y="4.401864746094" />
                  <Point X="-26.46230078125" Y="4.412217285156" />
                  <Point X="-26.462751953125" Y="4.432899414062" />
                  <Point X="-26.463541015625" Y="4.443229003906" />
                  <Point X="-26.47348046875" Y="4.518717285156" />
                  <Point X="-26.479265625" Y="4.562655273438" />
                  <Point X="-26.09069140625" Y="4.671598144531" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.22566796875" Y="4.261728515625" />
                  <Point X="-25.220435546875" Y="4.247106933594" />
                  <Point X="-25.20766015625" Y="4.218912597656" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.717228515625" Y="4.428889648438" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.311416015625" Y="4.7525" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.685056640625" Y="4.620319824219" />
                  <Point X="-23.546404296875" Y="4.586843261719" />
                  <Point X="-23.230685546875" Y="4.472331054688" />
                  <Point X="-23.14173828125" Y="4.440069335938" />
                  <Point X="-22.83518359375" Y="4.296703613281" />
                  <Point X="-22.74955078125" Y="4.256655273438" />
                  <Point X="-22.45334765625" Y="4.084088134766" />
                  <Point X="-22.3705703125" Y="4.035861572266" />
                  <Point X="-22.182216796875" Y="3.901915039062" />
                  <Point X="-22.272771484375" Y="3.745071289062" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.93762109375" Y="2.593110595703" />
                  <Point X="-22.9468125" Y="2.573448974609" />
                  <Point X="-22.955814453125" Y="2.549571777344" />
                  <Point X="-22.958697265625" Y="2.540601318359" />
                  <Point X="-22.97519140625" Y="2.478924072266" />
                  <Point X="-22.97900390625" Y="2.461379638672" />
                  <Point X="-22.985318359375" Y="2.423870117188" />
                  <Point X="-22.986611328125" Y="2.4103203125" />
                  <Point X="-22.98724609375" Y="2.383175537109" />
                  <Point X="-22.986587890625" Y="2.369580566406" />
                  <Point X="-22.98015625" Y="2.316247314453" />
                  <Point X="-22.976201171875" Y="2.289041259766" />
                  <Point X="-22.970861328125" Y="2.267125" />
                  <Point X="-22.967537109375" Y="2.256323242188" />
                  <Point X="-22.959271484375" Y="2.234231445312" />
                  <Point X="-22.9546875" Y="2.223898193359" />
                  <Point X="-22.944322265625" Y="2.203846191406" />
                  <Point X="-22.938541015625" Y="2.194127441406" />
                  <Point X="-22.9055390625" Y="2.145492675781" />
                  <Point X="-22.8944140625" Y="2.130706542969" />
                  <Point X="-22.87115625" Y="2.102733154297" />
                  <Point X="-22.86193359375" Y="2.093104003906" />
                  <Point X="-22.8422265625" Y="2.075228027344" />
                  <Point X="-22.8317421875" Y="2.066981201172" />
                  <Point X="-22.783107421875" Y="2.03398046875" />
                  <Point X="-22.75871875" Y="2.018244384766" />
                  <Point X="-22.738673828125" Y="2.007882080078" />
                  <Point X="-22.72834375" Y="2.003299194336" />
                  <Point X="-22.706255859375" Y="1.995033081055" />
                  <Point X="-22.695453125" Y="1.991708007812" />
                  <Point X="-22.673529296875" Y="1.986365112305" />
                  <Point X="-22.662408203125" Y="1.984346801758" />
                  <Point X="-22.609076171875" Y="1.977915649414" />
                  <Point X="-22.589603515625" Y="1.976577148438" />
                  <Point X="-22.554609375" Y="1.975974487305" />
                  <Point X="-22.541408203125" Y="1.976666992188" />
                  <Point X="-22.51523046875" Y="1.979877685547" />
                  <Point X="-22.50225390625" Y="1.982395996094" />
                  <Point X="-22.440576171875" Y="1.998889160156" />
                  <Point X="-22.430919921875" Y="2.001747436523" />
                  <Point X="-22.4002265625" Y="2.011725219727" />
                  <Point X="-22.390951171875" Y="2.015286987305" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-22.0180546875" Y="2.227583496094" />
                  <Point X="-21.059595703125" Y="2.780950683594" />
                  <Point X="-21.005154296875" Y="2.705289550781" />
                  <Point X="-20.956033203125" Y="2.637024169922" />
                  <Point X="-20.86311328125" Y="2.483470947266" />
                  <Point X="-20.9555703125" Y="2.412526123047" />
                  <Point X="-21.827046875" Y="1.743819824219" />
                  <Point X="-21.83186328125" Y="1.7398671875" />
                  <Point X="-21.847873046875" Y="1.725222412109" />
                  <Point X="-21.865328125" Y="1.706606323242" />
                  <Point X="-21.87142578125" Y="1.699420898438" />
                  <Point X="-21.915814453125" Y="1.64151159668" />
                  <Point X="-21.92920703125" Y="1.624039550781" />
                  <Point X="-21.934365234375" Y="1.616599365234" />
                  <Point X="-21.950255859375" Y="1.589380371094" />
                  <Point X="-21.965234375" Y="1.555554199219" />
                  <Point X="-21.969859375" Y="1.54267590332" />
                  <Point X="-21.98639453125" Y="1.483550292969" />
                  <Point X="-21.993775390625" Y="1.454663452148" />
                  <Point X="-21.997228515625" Y="1.432365356445" />
                  <Point X="-21.998291015625" Y="1.421114868164" />
                  <Point X="-21.999107421875" Y="1.397540649414" />
                  <Point X="-21.998826171875" Y="1.386235839844" />
                  <Point X="-21.996921875" Y="1.363749633789" />
                  <Point X="-21.995298828125" Y="1.352567993164" />
                  <Point X="-21.981724609375" Y="1.286783569336" />
                  <Point X="-21.97762890625" Y="1.266935302734" />
                  <Point X="-21.97540234375" Y="1.258239501953" />
                  <Point X="-21.96531640625" Y="1.228609985352" />
                  <Point X="-21.94971484375" Y="1.195373657227" />
                  <Point X="-21.943083984375" Y="1.183527832031" />
                  <Point X="-21.906166015625" Y="1.127412841797" />
                  <Point X="-21.89502734375" Y="1.110482299805" />
                  <Point X="-21.88826171875" Y="1.101422851562" />
                  <Point X="-21.873705078125" Y="1.084175537109" />
                  <Point X="-21.8659140625" Y="1.075987792969" />
                  <Point X="-21.848669921875" Y="1.059898681641" />
                  <Point X="-21.839962890625" Y="1.052694091797" />
                  <Point X="-21.82175390625" Y="1.039369384766" />
                  <Point X="-21.812251953125" Y="1.033249389648" />
                  <Point X="-21.75875" Y="1.003133483887" />
                  <Point X="-21.7423828125" Y="0.994933044434" />
                  <Point X="-21.708265625" Y="0.979847961426" />
                  <Point X="-21.69554296875" Y="0.975274230957" />
                  <Point X="-21.66957421875" Y="0.967979064941" />
                  <Point X="-21.656328125" Y="0.965257629395" />
                  <Point X="-21.5839921875" Y="0.955697509766" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.555966796875" Y="0.952199829102" />
                  <Point X="-21.53428125" Y="0.951222900391" />
                  <Point X="-21.50878125" Y="0.952032287598" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-21.1708046875" Y="0.996057067871" />
                  <Point X="-20.295296875" Y="1.111319702148" />
                  <Point X="-20.268408203125" Y="1.000866210938" />
                  <Point X="-20.247310546875" Y="0.914203369141" />
                  <Point X="-20.21612890625" Y="0.713920776367" />
                  <Point X="-20.30927734375" Y="0.688961303711" />
                  <Point X="-21.3080078125" Y="0.42135269165" />
                  <Point X="-21.313970703125" Y="0.419543334961" />
                  <Point X="-21.33437890625" Y="0.412135101318" />
                  <Point X="-21.357619140625" Y="0.401618377686" />
                  <Point X="-21.365994140625" Y="0.397316497803" />
                  <Point X="-21.4370625" Y="0.35623815918" />
                  <Point X="-21.45850390625" Y="0.343844207764" />
                  <Point X="-21.4661171875" Y="0.338945159912" />
                  <Point X="-21.491220703125" Y="0.319873718262" />
                  <Point X="-21.51800390625" Y="0.294352935791" />
                  <Point X="-21.527203125" Y="0.284226654053" />
                  <Point X="-21.56984375" Y="0.229892501831" />
                  <Point X="-21.582708984375" Y="0.213499099731" />
                  <Point X="-21.589146484375" Y="0.204205413818" />
                  <Point X="-21.600869140625" Y="0.184926193237" />
                  <Point X="-21.606154296875" Y="0.174940673828" />
                  <Point X="-21.615927734375" Y="0.15347833252" />
                  <Point X="-21.6199921875" Y="0.142933990479" />
                  <Point X="-21.62683984375" Y="0.12143107605" />
                  <Point X="-21.629623046875" Y="0.110472511292" />
                  <Point X="-21.6438359375" Y="0.036254692078" />
                  <Point X="-21.648125" Y="0.013861978531" />
                  <Point X="-21.649396484375" Y="0.004967195034" />
                  <Point X="-21.6514140625" Y="-0.026260259628" />
                  <Point X="-21.64971875" Y="-0.062940242767" />
                  <Point X="-21.648125" Y="-0.07642212677" />
                  <Point X="-21.633912109375" Y="-0.150639801025" />
                  <Point X="-21.629623046875" Y="-0.173032501221" />
                  <Point X="-21.62683984375" Y="-0.183991073608" />
                  <Point X="-21.6199921875" Y="-0.205493972778" />
                  <Point X="-21.615927734375" Y="-0.216038330078" />
                  <Point X="-21.606154296875" Y="-0.237500671387" />
                  <Point X="-21.600869140625" Y="-0.247486495972" />
                  <Point X="-21.589146484375" Y="-0.266765563965" />
                  <Point X="-21.582708984375" Y="-0.276058807373" />
                  <Point X="-21.540068359375" Y="-0.330393096924" />
                  <Point X="-21.527203125" Y="-0.346786499023" />
                  <Point X="-21.52127734375" Y="-0.35363684082" />
                  <Point X="-21.498857421875" Y="-0.37580645752" />
                  <Point X="-21.469822265625" Y="-0.398724700928" />
                  <Point X="-21.45850390625" Y="-0.406404205322" />
                  <Point X="-21.387435546875" Y="-0.447482666016" />
                  <Point X="-21.365994140625" Y="-0.459876647949" />
                  <Point X="-21.360501953125" Y="-0.462815063477" />
                  <Point X="-21.34084765625" Y="-0.472013183594" />
                  <Point X="-21.316974609375" Y="-0.481026428223" />
                  <Point X="-21.3080078125" Y="-0.483912963867" />
                  <Point X="-21.006673828125" Y="-0.564654724121" />
                  <Point X="-20.215119140625" Y="-0.776751159668" />
                  <Point X="-20.226607421875" Y="-0.852944519043" />
                  <Point X="-20.238380859375" Y="-0.93103729248" />
                  <Point X="-20.2721953125" Y="-1.079219726563" />
                  <Point X="-20.404970703125" Y="-1.061739624023" />
                  <Point X="-21.563216796875" Y="-0.909253601074" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042480469" />
                  <Point X="-21.63327734375" Y="-0.910840881348" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.784998046875" Y="-0.942993225098" />
                  <Point X="-21.82708203125" Y="-0.952140075684" />
                  <Point X="-21.842125" Y="-0.956742614746" />
                  <Point X="-21.871244140625" Y="-0.968366577148" />
                  <Point X="-21.8853203125" Y="-0.975388427734" />
                  <Point X="-21.913146484375" Y="-0.992280273438" />
                  <Point X="-21.92587109375" Y="-1.001527893066" />
                  <Point X="-21.949623046875" Y="-1.021999389648" />
                  <Point X="-21.960650390625" Y="-1.033222900391" />
                  <Point X="-22.04495703125" Y="-1.134618164062" />
                  <Point X="-22.07039453125" Y="-1.1652109375" />
                  <Point X="-22.078673828125" Y="-1.176848022461" />
                  <Point X="-22.09339453125" Y="-1.201231201172" />
                  <Point X="-22.0998359375" Y="-1.213977050781" />
                  <Point X="-22.1111796875" Y="-1.241364257812" />
                  <Point X="-22.11563671875" Y="-1.254934814453" />
                  <Point X="-22.122466796875" Y="-1.282583129883" />
                  <Point X="-22.12483984375" Y="-1.296660888672" />
                  <Point X="-22.136923828125" Y="-1.42797253418" />
                  <Point X="-22.140568359375" Y="-1.467591186523" />
                  <Point X="-22.140708984375" Y="-1.48332019043" />
                  <Point X="-22.138390625" Y="-1.514592163086" />
                  <Point X="-22.135931640625" Y="-1.530134765625" />
                  <Point X="-22.12819921875" Y="-1.561755371094" />
                  <Point X="-22.123208984375" Y="-1.576675048828" />
                  <Point X="-22.110837890625" Y="-1.605483032227" />
                  <Point X="-22.10345703125" Y="-1.619371459961" />
                  <Point X="-22.026267578125" Y="-1.739436401367" />
                  <Point X="-22.0029765625" Y="-1.775662109375" />
                  <Point X="-21.9982578125" Y="-1.782350952148" />
                  <Point X="-21.980203125" Y="-1.804452758789" />
                  <Point X="-21.9565078125" Y="-1.828121704102" />
                  <Point X="-21.947201171875" Y="-1.836277587891" />
                  <Point X="-21.6675625" Y="-2.050852050781" />
                  <Point X="-20.912828125" Y="-2.62998046875" />
                  <Point X="-20.92131640625" Y="-2.643714355469" />
                  <Point X="-20.954537109375" Y="-2.697467041016" />
                  <Point X="-20.998724609375" Y="-2.760251953125" />
                  <Point X="-21.119970703125" Y="-2.690250488281" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.184970703125" Y="-2.079513916016" />
                  <Point X="-22.216876953125" Y="-2.069326416016" />
                  <Point X="-22.228888671875" Y="-2.066337646484" />
                  <Point X="-22.394892578125" Y="-2.036357299805" />
                  <Point X="-22.444978515625" Y="-2.027311889648" />
                  <Point X="-22.460638671875" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.55515625" Y="-2.03513659668" />
                  <Point X="-22.584931640625" Y="-2.044960571289" />
                  <Point X="-22.59941015625" Y="-2.051109130859" />
                  <Point X="-22.737318359375" Y="-2.123689453125" />
                  <Point X="-22.778927734375" Y="-2.145588134766" />
                  <Point X="-22.791029296875" Y="-2.153169677734" />
                  <Point X="-22.813958984375" Y="-2.170061523438" />
                  <Point X="-22.824787109375" Y="-2.179371826172" />
                  <Point X="-22.84574609375" Y="-2.200330322266" />
                  <Point X="-22.855056640625" Y="-2.211157714844" />
                  <Point X="-22.871951171875" Y="-2.234089355469" />
                  <Point X="-22.87953515625" Y="-2.246193603516" />
                  <Point X="-22.952115234375" Y="-2.384102294922" />
                  <Point X="-22.974015625" Y="-2.425711669922" />
                  <Point X="-22.9801640625" Y="-2.440193115234" />
                  <Point X="-22.98998828125" Y="-2.469972167969" />
                  <Point X="-22.9936640625" Y="-2.485269775391" />
                  <Point X="-22.99862109375" Y="-2.517445068359" />
                  <Point X="-22.999720703125" Y="-2.533135498047" />
                  <Point X="-22.99931640625" Y="-2.564484863281" />
                  <Point X="-22.9978125" Y="-2.580143798828" />
                  <Point X="-22.96783203125" Y="-2.746147949219" />
                  <Point X="-22.95878515625" Y="-2.796234130859" />
                  <Point X="-22.956982421875" Y="-2.804227294922" />
                  <Point X="-22.94876171875" Y="-2.831541992188" />
                  <Point X="-22.9359296875" Y="-2.862477294922" />
                  <Point X="-22.930453125" Y="-2.873578125" />
                  <Point X="-22.7507578125" Y="-3.184820800781" />
                  <Point X="-22.26410546875" Y="-4.027724853516" />
                  <Point X="-22.276244140625" Y="-4.036083496094" />
                  <Point X="-22.378896484375" Y="-3.902303710938" />
                  <Point X="-23.16608203125" Y="-2.876423339844" />
                  <Point X="-23.171345703125" Y="-2.870145019531" />
                  <Point X="-23.191169921875" Y="-2.849624023438" />
                  <Point X="-23.216748046875" Y="-2.828003662109" />
                  <Point X="-23.22669921875" Y="-2.820646728516" />
                  <Point X="-23.39042578125" Y="-2.71538671875" />
                  <Point X="-23.43982421875" Y="-2.683627929688" />
                  <Point X="-23.453716796875" Y="-2.676245605469" />
                  <Point X="-23.48252734375" Y="-2.663874511719" />
                  <Point X="-23.4974453125" Y="-2.658885742188" />
                  <Point X="-23.52906640625" Y="-2.651154052734" />
                  <Point X="-23.54460546875" Y="-2.648695800781" />
                  <Point X="-23.575875" Y="-2.646376953125" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.770666015625" Y="-2.662993652344" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670339111328" />
                  <Point X="-23.866421875" Y="-2.677170410156" />
                  <Point X="-23.87998828125" Y="-2.681627929688" />
                  <Point X="-23.907373046875" Y="-2.692970703125" />
                  <Point X="-23.92012109375" Y="-2.699412841797" />
                  <Point X="-23.94450390625" Y="-2.714134033203" />
                  <Point X="-23.956138671875" Y="-2.722413085938" />
                  <Point X="-24.094404296875" Y="-2.837376953125" />
                  <Point X="-24.136123046875" Y="-2.872063476562" />
                  <Point X="-24.14734375" Y="-2.8830859375" />
                  <Point X="-24.167814453125" Y="-2.906834472656" />
                  <Point X="-24.177064453125" Y="-2.919560546875" />
                  <Point X="-24.19395703125" Y="-2.947386474609" />
                  <Point X="-24.20098046875" Y="-2.961466064453" />
                  <Point X="-24.21260546875" Y="-2.990588378906" />
                  <Point X="-24.21720703125" Y="-3.005631103516" />
                  <Point X="-24.258546875" Y="-3.195832275391" />
                  <Point X="-24.271021484375" Y="-3.253218994141" />
                  <Point X="-24.2724140625" Y="-3.261286376953" />
                  <Point X="-24.275275390625" Y="-3.289676513672" />
                  <Point X="-24.2752578125" Y="-3.323170410156" />
                  <Point X="-24.2744453125" Y="-3.335520996094" />
                  <Point X="-24.22351953125" Y="-3.722330810547" />
                  <Point X="-24.16691015625" Y="-4.152321289062" />
                  <Point X="-24.344931640625" Y="-3.487937255859" />
                  <Point X="-24.347392578125" Y="-3.480122314453" />
                  <Point X="-24.357853515625" Y="-3.453578613281" />
                  <Point X="-24.3732109375" Y="-3.423815429688" />
                  <Point X="-24.37958984375" Y="-3.413209716797" />
                  <Point X="-24.505369140625" Y="-3.231987060547" />
                  <Point X="-24.543318359375" Y="-3.177309326172" />
                  <Point X="-24.553328125" Y="-3.165172119141" />
                  <Point X="-24.575212890625" Y="-3.142716308594" />
                  <Point X="-24.587087890625" Y="-3.132397705078" />
                  <Point X="-24.61334375" Y="-3.113153808594" />
                  <Point X="-24.6267578125" Y="-3.104937255859" />
                  <Point X="-24.6547578125" Y="-3.090829589844" />
                  <Point X="-24.66934375" Y="-3.084938476562" />
                  <Point X="-24.863978515625" Y="-3.024531005859" />
                  <Point X="-24.922703125" Y="-3.006305175781" />
                  <Point X="-24.936623046875" Y="-3.003109130859" />
                  <Point X="-24.964783203125" Y="-2.99883984375" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.259619140625" Y="-3.066712402344" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.332931640625" Y="-3.090830078125" />
                  <Point X="-25.360931640625" Y="-3.104938232422" />
                  <Point X="-25.37434375" Y="-3.113154785156" />
                  <Point X="-25.400599609375" Y="-3.132398925781" />
                  <Point X="-25.412474609375" Y="-3.142717041016" />
                  <Point X="-25.434359375" Y="-3.165172851562" />
                  <Point X="-25.444369140625" Y="-3.177310546875" />
                  <Point X="-25.570146484375" Y="-3.358532958984" />
                  <Point X="-25.608095703125" Y="-3.4132109375" />
                  <Point X="-25.61246875" Y="-3.420131591797" />
                  <Point X="-25.625974609375" Y="-3.445261230469" />
                  <Point X="-25.63877734375" Y="-3.476214111328" />
                  <Point X="-25.64275390625" Y="-3.487937011719" />
                  <Point X="-25.7356015625" Y="-3.834450927734" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.134420064943" Y="-4.679484136649" />
                  <Point X="-25.95957433588" Y="-4.670320860272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.121807714096" Y="-4.58369277848" />
                  <Point X="-25.933720937385" Y="-4.5738355682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.125115062264" Y="-4.488735736383" />
                  <Point X="-25.907867538891" Y="-4.477350276128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.143843203883" Y="-4.394586863826" />
                  <Point X="-25.882014140396" Y="-4.380864984056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.162571345502" Y="-4.300437991268" />
                  <Point X="-25.856160741901" Y="-4.284379691985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.181299063424" Y="-4.206289096506" />
                  <Point X="-25.830307343406" Y="-4.187894399913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.180545378266" Y="-4.101434038974" />
                  <Point X="-24.173657122635" Y="-4.101073040793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.301656248647" Y="-4.002965632172" />
                  <Point X="-22.279083262273" Y="-4.001782632084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.218574145005" Y="-4.113112227884" />
                  <Point X="-25.804453944911" Y="-4.091409107841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.205682538286" Y="-4.007621048838" />
                  <Point X="-24.186095466028" Y="-4.006594533879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.371830049704" Y="-3.911512912379" />
                  <Point X="-22.332393914013" Y="-3.909446152084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.618111864315" Y="-4.091328518906" />
                  <Point X="-27.441502906571" Y="-4.082072835629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.303586475176" Y="-4.02243716245" />
                  <Point X="-25.778600546416" Y="-3.994923815769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.230819698307" Y="-3.913808058703" />
                  <Point X="-24.19853380942" Y="-3.912116026964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.442004132378" Y="-3.820060207346" />
                  <Point X="-22.385704565753" Y="-3.817109672084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.757531115557" Y="-4.003504799383" />
                  <Point X="-27.343894427307" Y="-3.981827019122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.405943871851" Y="-3.932671113433" />
                  <Point X="-25.752747147922" Y="-3.898438523697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.255956858327" Y="-3.819995068567" />
                  <Point X="-24.210972152813" Y="-3.817637520049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.512178246585" Y="-3.728607503965" />
                  <Point X="-22.439015217493" Y="-3.724773192084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.873209206426" Y="-3.914436858368" />
                  <Point X="-27.189406212286" Y="-3.878600261978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.511398544214" Y="-3.843067385757" />
                  <Point X="-25.726893876724" Y="-3.801953238297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.281094018347" Y="-3.726182078432" />
                  <Point X="-24.223410496206" Y="-3.723159013135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.582352360792" Y="-3.637154800583" />
                  <Point X="-22.492325869233" Y="-3.632436712084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.976934623426" Y="-3.824742504258" />
                  <Point X="-25.701040856171" Y="-3.705467966032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.306231178368" Y="-3.632369088296" />
                  <Point X="-24.235849141123" Y="-3.628680522022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.652526475" Y="-3.545702097202" />
                  <Point X="-22.545636520974" Y="-3.540100232084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.920297194719" Y="-3.726643889525" />
                  <Point X="-25.675187835619" Y="-3.608982693767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.331368338388" Y="-3.53855609816" />
                  <Point X="-24.248287788708" Y="-3.53420203105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.722700589207" Y="-3.454249393821" />
                  <Point X="-22.598947172714" Y="-3.447763752084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.863659968433" Y="-3.628545285401" />
                  <Point X="-25.649334815067" Y="-3.512497421503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.362256992838" Y="-3.445044531075" />
                  <Point X="-24.260726436292" Y="-3.439723540077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.792874703414" Y="-3.36279669044" />
                  <Point X="-22.652257824454" Y="-3.355427272084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.807022770322" Y="-3.530446682754" />
                  <Point X="-25.609399395231" Y="-3.415274121964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.421370521975" Y="-3.353012166993" />
                  <Point X="-24.273165083876" Y="-3.345245049105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.863048817622" Y="-3.271343987059" />
                  <Point X="-22.705568476194" Y="-3.263090792084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.750385572211" Y="-3.432348080106" />
                  <Point X="-25.541015084919" Y="-3.316559879253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.485079305536" Y="-3.26122062999" />
                  <Point X="-24.270314195834" Y="-3.249965267524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.933222931829" Y="-3.179891283678" />
                  <Point X="-22.758879072223" Y="-3.170754309165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.693748374101" Y="-3.334249477458" />
                  <Point X="-25.472497661854" Y="-3.217838660399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.549774682515" Y="-3.169480798158" />
                  <Point X="-24.249397924903" Y="-3.153738719343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.003397046037" Y="-3.088438580297" />
                  <Point X="-22.812189358259" Y="-3.078417809999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.63711117599" Y="-3.236150874811" />
                  <Point X="-25.380823136982" Y="-3.117903829263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.68125678081" Y="-3.081241110075" />
                  <Point X="-24.228483296095" Y="-3.057512257223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.073571160244" Y="-2.996985876916" />
                  <Point X="-22.865499644296" Y="-2.986081310833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.580473977879" Y="-3.138052272163" />
                  <Point X="-25.064509599856" Y="-3.006196166356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.954343372218" Y="-3.000422599013" />
                  <Point X="-24.200711270564" Y="-2.960926414169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.143745274451" Y="-2.905533173535" />
                  <Point X="-22.918809930333" Y="-2.893744811668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.523836779768" Y="-3.039953669515" />
                  <Point X="-24.123732809021" Y="-2.861761771077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.235179614801" Y="-2.815194671393" />
                  <Point X="-22.957787607744" Y="-2.800657172313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.097595790459" Y="-2.703168650128" />
                  <Point X="-20.953390116038" Y="-2.695611150972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.797747776302" Y="-3.011586142978" />
                  <Point X="-28.780960244893" Y="-3.010706345737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.467199581657" Y="-2.941855066868" />
                  <Point X="-24.001622878531" Y="-2.760231887922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.371997016088" Y="-2.727234594692" />
                  <Point X="-22.975005276849" Y="-2.706429139245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.248654360513" Y="-2.615954921457" />
                  <Point X="-20.951408208723" Y="-2.600376910741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.867379280324" Y="-2.920105002602" />
                  <Point X="-28.599739873583" Y="-2.906078615646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.410562383546" Y="-2.84375646422" />
                  <Point X="-22.992024802679" Y="-2.612190721928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.399712924324" Y="-2.528741192458" />
                  <Point X="-21.067458327385" Y="-2.511328466876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.937010688624" Y="-2.82862385721" />
                  <Point X="-28.418519502273" Y="-2.801450885555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.353925185436" Y="-2.745657861573" />
                  <Point X="-22.998615032905" Y="-2.51740572839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.550771488134" Y="-2.441527463459" />
                  <Point X="-21.183508446046" Y="-2.42228002301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.006090605467" Y="-2.737113809375" />
                  <Point X="-28.237299130963" Y="-2.696823155464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.316176547369" Y="-2.648549166411" />
                  <Point X="-22.971457877483" Y="-2.420852109313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.701830051945" Y="-2.35431373446" />
                  <Point X="-21.299558564707" Y="-2.333231579145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.061103173293" Y="-2.644866523018" />
                  <Point X="-28.056078759653" Y="-2.592195425373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.315941280085" Y="-2.553406463705" />
                  <Point X="-22.919969956352" Y="-2.323023368836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.852888615755" Y="-2.267100005461" />
                  <Point X="-21.415608683368" Y="-2.24418313528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.116115741119" Y="-2.55261923666" />
                  <Point X="-27.874857605827" Y="-2.487567654273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.359338342666" Y="-2.460550434513" />
                  <Point X="-22.865274265385" Y="-2.225026516267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.003947179566" Y="-2.179886276463" />
                  <Point X="-21.531658802029" Y="-2.155134691415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.171128308945" Y="-2.460371950303" />
                  <Point X="-27.693635953351" Y="-2.382939857038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.467416241876" Y="-2.37108418433" />
                  <Point X="-22.736264954339" Y="-2.123135051898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.155344205255" Y="-2.092690285499" />
                  <Point X="-21.647708920691" Y="-2.066086247549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.072721695199" Y="-2.36008430534" />
                  <Point X="-21.7637590274" Y="-1.977037803058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.939657292643" Y="-2.25798032263" />
                  <Point X="-21.879809131643" Y="-1.887989358437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.8065928006" Y="-2.155876335231" />
                  <Point X="-21.98516357635" Y="-1.798380378052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.673528308557" Y="-2.053772347832" />
                  <Point X="-22.047433666849" Y="-1.706513442341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.540463816515" Y="-1.951668360433" />
                  <Point X="-22.106069283955" Y="-1.61445603195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.407399324472" Y="-1.849564373034" />
                  <Point X="-22.137382114723" Y="-1.520966695004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.274334832429" Y="-1.747460385634" />
                  <Point X="-22.136724069073" Y="-1.425801835423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.141270340386" Y="-1.645356398235" />
                  <Point X="-22.127927248995" Y="-1.330210440748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.018266098707" Y="-1.543779646217" />
                  <Point X="-22.108147417579" Y="-1.23404345084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.962616504562" Y="-1.4457328017" />
                  <Point X="-22.045811837165" Y="-1.13564620863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.470635352677" Y="-1.053094707099" />
                  <Point X="-20.263759656602" Y="-1.04225281128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.948796191003" Y="-1.349878136887" />
                  <Point X="-21.963110480976" Y="-1.036181641339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.987478857743" Y="-0.985050954567" />
                  <Point X="-20.241788649966" Y="-0.945970986744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.968653385159" Y="-1.255788435466" />
                  <Point X="-21.716634867318" Y="-0.92813402891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.50432236281" Y="-0.917007202034" />
                  <Point X="-20.226166756032" Y="-0.850021905105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.670332209127" Y="-1.249839270813" />
                  <Point X="-29.189614026667" Y="-1.22464589841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.041688094992" Y="-1.164485649549" />
                  <Point X="-20.285161657987" Y="-0.757983324035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.694311258536" Y="-1.155965586672" />
                  <Point X="-20.582112817693" Y="-0.678415502001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.718289342631" Y="-1.062091851941" />
                  <Point X="-20.879063977399" Y="-0.598847679967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.74160648739" Y="-0.968183478848" />
                  <Point X="-21.176015902101" Y="-0.519279898025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.755111931747" Y="-0.873760896325" />
                  <Point X="-21.406871930197" Y="-0.436248176922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.768617376104" Y="-0.779338313802" />
                  <Point X="-21.526675502296" Y="-0.347396443216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.782122820462" Y="-0.684915731279" />
                  <Point X="-21.595761383467" Y="-0.255886707958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.486545699643" Y="-0.5742948179" />
                  <Point X="-21.631614507023" Y="-0.162635317674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.045190365982" Y="-0.456033992119" />
                  <Point X="-21.649070988551" Y="-0.068419800235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.603835032322" Y="-0.337773166337" />
                  <Point X="-21.645629455561" Y="0.026890935736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.375653886434" Y="-0.230684326337" />
                  <Point X="-21.626329783033" Y="0.123032761584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.313464038153" Y="-0.132294721625" />
                  <Point X="-21.577020722758" Y="0.220747312801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.2956484375" Y="-0.036230672688" />
                  <Point X="-21.490513007769" Y="0.320411362904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.309862985578" Y="0.058154747283" />
                  <Point X="-21.290592396972" Y="0.426019131019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.359971128126" Y="0.150659063678" />
                  <Point X="-20.849236126332" Y="0.544280005906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.480827435995" Y="0.23945562584" />
                  <Point X="-20.407879855691" Y="0.662540880792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.769044988281" Y="0.319481156844" />
                  <Point X="-20.224437099937" Y="0.767285081117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.065995671616" Y="0.399049003844" />
                  <Point X="-20.239127912119" Y="0.861645541144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.362946313746" Y="0.478616853003" />
                  <Point X="-20.257440984298" Y="0.955816166569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.659896955877" Y="0.558184702162" />
                  <Point X="-21.696282408148" Y="0.975540055675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.082191539087" Y="1.0077231944" />
                  <Point X="-20.280307962193" Y="1.049748131909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.77725477999" Y="0.647164612089" />
                  <Point X="-21.851496570894" Y="1.062535998962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.763068410236" Y="0.743038461093" />
                  <Point X="-21.923584256777" Y="1.153888416301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.748882064096" Y="0.83891230886" />
                  <Point X="-21.97141039377" Y="1.246512327539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.73469620421" Y="0.934786131144" />
                  <Point X="-21.99281301139" Y="1.340521036748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.711383301541" Y="1.031138281471" />
                  <Point X="-21.996751596685" Y="1.435444997109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.685233045442" Y="1.127639131191" />
                  <Point X="-21.972893757241" Y="1.531825706363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.659082789344" Y="1.224139980911" />
                  <Point X="-29.129615642502" Y="1.25188817828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.526722835859" Y="1.283484451422" />
                  <Point X="-21.925049502404" Y="1.62946349038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.458988661646" Y="1.382164621944" />
                  <Point X="-21.843911583861" Y="1.728846121377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.425817038121" Y="1.479033445938" />
                  <Point X="-21.713690476997" Y="1.830801093273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.427268425648" Y="1.57408775481" />
                  <Point X="-21.580625715343" Y="1.932905094802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.46546521353" Y="1.667216318851" />
                  <Point X="-22.569210355166" Y="1.976225942065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.526943937922" Y="1.978441031131" />
                  <Point X="-21.447560953689" Y="2.035009096331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.531898834454" Y="1.758865053179" />
                  <Point X="-22.818904648605" Y="2.058270391516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.260638605835" Y="2.087527875067" />
                  <Point X="-21.314496192036" Y="2.13711309786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.64602827569" Y="1.848014155483" />
                  <Point X="-22.907745843457" Y="2.148744794655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.079417660596" Y="2.192155635236" />
                  <Point X="-21.181431430382" Y="2.239217099388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.762077699818" Y="1.937062635747" />
                  <Point X="-22.961819382632" Y="2.241041293419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.898197120279" Y="2.296783374184" />
                  <Point X="-21.048366668728" Y="2.341321100917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.878127778782" Y="2.026111081693" />
                  <Point X="-22.982428744361" Y="2.335091575408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.716976787267" Y="2.401411102268" />
                  <Point X="-20.915302013589" Y="2.443425096864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.994177857746" Y="2.115159527638" />
                  <Point X="-22.984265277902" Y="2.430125699633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.535756454255" Y="2.506038830352" />
                  <Point X="-20.897026261727" Y="2.539513261304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.11022793671" Y="2.204207973584" />
                  <Point X="-22.962495890502" Y="2.526396957753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.354536121242" Y="2.610666558436" />
                  <Point X="-20.952823144993" Y="2.631719443431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.226278015674" Y="2.29325641953" />
                  <Point X="-22.920168474785" Y="2.623745616483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.17331578823" Y="2.71529428652" />
                  <Point X="-21.018202398468" Y="2.723423434815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.170993761744" Y="2.391284117377" />
                  <Point X="-22.863531280488" Y="2.721844218931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.113715198362" Y="2.489416332554" />
                  <Point X="-22.806894086191" Y="2.819942821379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.056436316195" Y="2.587548564438" />
                  <Point X="-22.750256891894" Y="2.918041423826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.998071019243" Y="2.685737732909" />
                  <Point X="-28.146204694619" Y="2.730382155228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.960218039157" Y="2.740129302817" />
                  <Point X="-22.693619697597" Y="3.016140026274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.920914514244" Y="2.784911706863" />
                  <Point X="-28.34539181836" Y="2.815073573281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.829956267299" Y="2.842086405876" />
                  <Point X="-22.6369825033" Y="3.114238628722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.843758009244" Y="2.884085680817" />
                  <Point X="-28.496450393551" Y="2.902287301683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.763205816312" Y="2.940715021648" />
                  <Point X="-22.580345309003" Y="3.212337231169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.766601504244" Y="2.983259654771" />
                  <Point X="-28.647508968743" Y="2.989501030086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.7485407689" Y="3.036613957085" />
                  <Point X="-22.523708114706" Y="3.310435833617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.756798193914" Y="3.131311576648" />
                  <Point X="-22.467070920409" Y="3.408534436065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.785448776062" Y="3.224940436132" />
                  <Point X="-22.410433726112" Y="3.506633038513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.838479671946" Y="3.317291577515" />
                  <Point X="-22.353796531815" Y="3.60473164096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.891790191509" Y="3.409628064442" />
                  <Point X="-22.297159337518" Y="3.702830243408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.945100547691" Y="3.501964559932" />
                  <Point X="-22.240521775048" Y="3.800928865151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.998410903874" Y="3.594301055421" />
                  <Point X="-22.183883934156" Y="3.899027501485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.051721260056" Y="3.686637550911" />
                  <Point X="-22.303138743231" Y="3.987907994643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.945353837057" Y="3.787342404208" />
                  <Point X="-22.439297748063" Y="4.07590257644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.812170636435" Y="3.88945261286" />
                  <Point X="-22.589107973576" Y="4.163181728077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.678988693465" Y="3.991562755602" />
                  <Point X="-26.877614782108" Y="4.033560982671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.763726072879" Y="4.039529637007" />
                  <Point X="-25.127245797849" Y="4.125293934062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.840882121196" Y="4.140301618423" />
                  <Point X="-22.738918387837" Y="4.250460869822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.502776411674" Y="4.095928022842" />
                  <Point X="-27.060885014317" Y="4.119086569662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.559434345503" Y="4.145366485635" />
                  <Point X="-25.20620069534" Y="4.216286456091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.770879359175" Y="4.239100680594" />
                  <Point X="-22.919484019956" Y="4.336128198897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.313715248675" Y="4.200966671414" />
                  <Point X="-27.147543128221" Y="4.209675383225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.502950921993" Y="4.243457029297" />
                  <Point X="-25.238527881114" Y="4.309722632944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.742189362257" Y="4.33573463249" />
                  <Point X="-23.102400497337" Y="4.421672325393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.472479544024" Y="4.340184339418" />
                  <Point X="-25.263664856715" Y="4.403535632744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.716336167203" Y="4.4322199139" />
                  <Point X="-23.321621487236" Y="4.50531381301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.462974506318" Y="4.435812850206" />
                  <Point X="-25.288801832317" Y="4.497348632545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.69048282412" Y="4.528705203067" />
                  <Point X="-23.552599821207" Y="4.588339124334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.475007184241" Y="4.530312617147" />
                  <Point X="-25.313938807918" Y="4.591161632346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.664629481038" Y="4.625190492235" />
                  <Point X="-23.876345119433" Y="4.66650272507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.204809053117" Y="4.639603474035" />
                  <Point X="-25.339075783519" Y="4.684974632146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.638776137955" Y="4.721675781403" />
                  <Point X="-24.224433159208" Y="4.743390576781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.424360393144" Y="4.775635428019" />
                  <Point X="-25.364212759121" Y="4.778787631947" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.2207265625" Y="-4.685588378906" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.661458984375" Y="-3.340321533203" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.920298828125" Y="-3.205992431641" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.203298828125" Y="-3.248173828125" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285644042969" />
                  <Point X="-25.414056640625" Y="-3.466866455078" />
                  <Point X="-25.452005859375" Y="-3.521544433594" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.55207421875" Y="-3.883626220703" />
                  <Point X="-25.847744140625" Y="-4.987077148438" />
                  <Point X="-26.032244140625" Y="-4.951264648438" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.3259453125" Y="-4.879994628906" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.346193359375" Y="-4.8324140625" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.5347578125" />
                  <Point X="-26.356181640625" Y="-4.30099609375" />
                  <Point X="-26.3702109375" Y="-4.230466308594" />
                  <Point X="-26.386283203125" Y="-4.202628417969" />
                  <Point X="-26.5654765625" Y="-4.045478515625" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.887072265625" Y="-3.970174560547" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791259766" />
                  <Point X="-27.188052734375" Y="-4.10620703125" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.311861328125" Y="-4.225227539062" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.75616015625" Y="-4.229325683594" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.168361328125" Y="-3.926974609375" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-28.077919921875" Y="-3.619657470703" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597592773438" />
                  <Point X="-27.513978515625" Y="-2.568763671875" />
                  <Point X="-27.5313203125" Y="-2.551422119141" />
                  <Point X="-27.560150390625" Y="-2.53719921875" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-27.890544921875" Y="-2.716017578125" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-29.08295703125" Y="-2.950586181641" />
                  <Point X="-29.161697265625" Y="-2.847135986328" />
                  <Point X="-29.38576171875" Y="-2.471417236328" />
                  <Point X="-29.43101953125" Y="-2.395526855469" />
                  <Point X="-29.164404296875" Y="-2.190945556641" />
                  <Point X="-28.163787109375" Y="-1.423144897461" />
                  <Point X="-28.145822265625" Y="-1.39601574707" />
                  <Point X="-28.13990234375" Y="-1.373163330078" />
                  <Point X="-28.138115234375" Y="-1.366262084961" />
                  <Point X="-28.140326171875" Y="-1.334592895508" />
                  <Point X="-28.16116015625" Y="-1.310638305664" />
                  <Point X="-28.18150390625" Y="-1.298664428711" />
                  <Point X="-28.187642578125" Y="-1.295051757812" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.5968125" Y="-1.338241577148" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.89659375" Y="-1.131767700195" />
                  <Point X="-29.927390625" Y="-1.011189697266" />
                  <Point X="-29.986671875" Y="-0.596708679199" />
                  <Point X="-29.99839453125" Y="-0.5147421875" />
                  <Point X="-29.69731640625" Y="-0.434068237305" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.5418984375" Y="-0.121427024841" />
                  <Point X="-28.520578125" Y="-0.106629463196" />
                  <Point X="-28.51414453125" Y="-0.102164848328" />
                  <Point X="-28.4948984375" Y="-0.075909736633" />
                  <Point X="-28.48779296875" Y="-0.053011520386" />
                  <Point X="-28.4856484375" Y="-0.046102703094" />
                  <Point X="-28.4856484375" Y="-0.016457374573" />
                  <Point X="-28.49275390625" Y="0.006440839291" />
                  <Point X="-28.4948984375" Y="0.013349655151" />
                  <Point X="-28.51414453125" Y="0.03960477066" />
                  <Point X="-28.53546484375" Y="0.054402183533" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-28.901375" Y="0.158236282349" />
                  <Point X="-29.998185546875" Y="0.45212600708" />
                  <Point X="-29.9374921875" Y="0.862284179688" />
                  <Point X="-29.91764453125" Y="0.996415283203" />
                  <Point X="-29.798314453125" Y="1.436779907227" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.57033984375" Y="1.501550170898" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866333008" />
                  <Point X="-28.684515625" Y="1.410744873047" />
                  <Point X="-28.67027734375" Y="1.415233764648" />
                  <Point X="-28.651533203125" Y="1.426055908203" />
                  <Point X="-28.639119140625" Y="1.443785888672" />
                  <Point X="-28.620185546875" Y="1.489498046875" />
                  <Point X="-28.61447265625" Y="1.503290161133" />
                  <Point X="-28.610712890625" Y="1.524607299805" />
                  <Point X="-28.61631640625" Y="1.545515258789" />
                  <Point X="-28.639162109375" Y="1.589403320312" />
                  <Point X="-28.646056640625" Y="1.602645019531" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-28.857236328125" Y="1.770591674805" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.237138671875" Y="2.654873535156" />
                  <Point X="-29.16001171875" Y="2.787007568359" />
                  <Point X="-28.8439140625" Y="3.193307373047" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.66098046875" Y="3.216672119141" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.07279296875" Y="2.914685058594" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927405273438" />
                  <Point X="-27.9666015625" Y="2.974053955078" />
                  <Point X="-27.95252734375" Y="2.988128662109" />
                  <Point X="-27.9408984375" Y="3.006382324219" />
                  <Point X="-27.938072265625" Y="3.027840087891" />
                  <Point X="-27.943822265625" Y="3.093560546875" />
                  <Point X="-27.945556640625" Y="3.113389648438" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.039482421875" Y="3.285441650391" />
                  <Point X="-28.30727734375" Y="3.749276855469" />
                  <Point X="-27.88719921875" Y="4.071346191406" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.255029296875" Y="4.450924316406" />
                  <Point X="-27.141546875" Y="4.513972167969" />
                  <Point X="-27.12144921875" Y="4.487779785156" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.878099609375" Y="4.235582519531" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.81380859375" Y="4.22225" />
                  <Point X="-26.737623046875" Y="4.253808105469" />
                  <Point X="-26.714634765625" Y="4.263329589844" />
                  <Point X="-26.696904296875" Y="4.275743652344" />
                  <Point X="-26.68608203125" Y="4.294487304688" />
                  <Point X="-26.66128515625" Y="4.373134765625" />
                  <Point X="-26.653802734375" Y="4.396863769531" />
                  <Point X="-26.651916015625" Y="4.418426269531" />
                  <Point X="-26.66185546875" Y="4.493914550781" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.141984375" Y="4.854543457031" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.36455078125" Y="4.973932128906" />
                  <Point X="-25.22419921875" Y="4.990358398438" />
                  <Point X="-25.19059375" Y="4.864939941406" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.900755859375" Y="4.478065429688" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.291626953125" Y="4.941466796875" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.640466796875" Y="4.805013183594" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.165900390625" Y="4.6509453125" />
                  <Point X="-23.068970703125" Y="4.615788085938" />
                  <Point X="-22.7546953125" Y="4.468812011719" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.357703125" Y="4.248258300781" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-21.981142578125" Y="3.992066894531" />
                  <Point X="-21.9312578125" Y="3.956592529297" />
                  <Point X="-22.108228515625" Y="3.650071289062" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491515380859" />
                  <Point X="-22.791640625" Y="2.429838134766" />
                  <Point X="-22.797955078125" Y="2.392328613281" />
                  <Point X="-22.7915234375" Y="2.338995361328" />
                  <Point X="-22.789583984375" Y="2.322903808594" />
                  <Point X="-22.781318359375" Y="2.300812011719" />
                  <Point X="-22.74831640625" Y="2.252177246094" />
                  <Point X="-22.72505859375" Y="2.224203857422" />
                  <Point X="-22.676423828125" Y="2.191203125" />
                  <Point X="-22.66175" Y="2.181246337891" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.586330078125" Y="2.166549072266" />
                  <Point X="-22.5513359375" Y="2.165946289062" />
                  <Point X="-22.489658203125" Y="2.182439697266" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-22.1130546875" Y="2.392128417969" />
                  <Point X="-21.005751953125" Y="3.031430664062" />
                  <Point X="-20.850927734375" Y="2.816261230469" />
                  <Point X="-20.79740234375" Y="2.741875" />
                  <Point X="-20.63778125" Y="2.478095947266" />
                  <Point X="-20.612486328125" Y="2.436295654297" />
                  <Point X="-20.83990625" Y="2.261789306641" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583832641602" />
                  <Point X="-21.765017578125" Y="1.525923461914" />
                  <Point X="-21.77841015625" Y="1.508451293945" />
                  <Point X="-21.78687890625" Y="1.491503417969" />
                  <Point X="-21.8034140625" Y="1.432377929688" />
                  <Point X="-21.808404296875" Y="1.41453894043" />
                  <Point X="-21.809220703125" Y="1.390964599609" />
                  <Point X="-21.795646484375" Y="1.325180053711" />
                  <Point X="-21.79155078125" Y="1.305331787109" />
                  <Point X="-21.784353515625" Y="1.287955444336" />
                  <Point X="-21.747435546875" Y="1.231840576172" />
                  <Point X="-21.736296875" Y="1.21491003418" />
                  <Point X="-21.719052734375" Y="1.198820800781" />
                  <Point X="-21.66555078125" Y="1.168704711914" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.55909765625" Y="1.144059448242" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-21.19560546875" Y="1.184431640625" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.083798828125" Y="1.045807006836" />
                  <Point X="-20.060806640625" Y="0.951366699219" />
                  <Point X="-20.0105078125" Y="0.628298217773" />
                  <Point X="-20.002140625" Y="0.574556335449" />
                  <Point X="-20.2601015625" Y="0.505435577393" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232818893433" />
                  <Point X="-21.34198046875" Y="0.191740509033" />
                  <Point X="-21.363421875" Y="0.1793465271" />
                  <Point X="-21.377734375" Y="0.166926391602" />
                  <Point X="-21.420375" Y="0.112592285156" />
                  <Point X="-21.433240234375" Y="0.09619871521" />
                  <Point X="-21.443013671875" Y="0.074736480713" />
                  <Point X="-21.4572265625" Y="0.000518648803" />
                  <Point X="-21.461515625" Y="-0.021874082565" />
                  <Point X="-21.461515625" Y="-0.040685997009" />
                  <Point X="-21.447302734375" Y="-0.114903823853" />
                  <Point X="-21.443013671875" Y="-0.137296554565" />
                  <Point X="-21.433240234375" Y="-0.158758789063" />
                  <Point X="-21.390599609375" Y="-0.213093048096" />
                  <Point X="-21.377734375" Y="-0.229486465454" />
                  <Point X="-21.363421875" Y="-0.241906600952" />
                  <Point X="-21.292353515625" Y="-0.282984985352" />
                  <Point X="-21.270912109375" Y="-0.295378967285" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.957498046875" Y="-0.381128875732" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.038732421875" Y="-0.88126953125" />
                  <Point X="-20.051568359375" Y="-0.96641217041" />
                  <Point X="-20.11601171875" Y="-1.248811767578" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-20.429771484375" Y="-1.250114135742" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.744642578125" Y="-1.128657958984" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.814552734375" Y="-1.154696777344" />
                  <Point X="-21.898859375" Y="-1.256092163086" />
                  <Point X="-21.924296875" Y="-1.286684814453" />
                  <Point X="-21.935640625" Y="-1.314072021484" />
                  <Point X="-21.947724609375" Y="-1.445383544922" />
                  <Point X="-21.951369140625" Y="-1.485002441406" />
                  <Point X="-21.94363671875" Y="-1.516622924805" />
                  <Point X="-21.866447265625" Y="-1.636687866211" />
                  <Point X="-21.84315625" Y="-1.672913452148" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-21.5518984375" Y="-1.900114990234" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.7596875" Y="-2.743597412109" />
                  <Point X="-20.795869140625" Y="-2.802142578125" />
                  <Point X="-20.929146484375" Y="-2.99151171875" />
                  <Point X="-20.943310546875" Y="-3.011638183594" />
                  <Point X="-21.214970703125" Y="-2.854795410156" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.428662109375" Y="-2.223332519531" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.648830078125" Y="-2.291825195312" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.334682373047" />
                  <Point X="-22.783978515625" Y="-2.472591064453" />
                  <Point X="-22.80587890625" Y="-2.514200439453" />
                  <Point X="-22.8108359375" Y="-2.546375732422" />
                  <Point X="-22.78085546875" Y="-2.712379882812" />
                  <Point X="-22.77180859375" Y="-2.762466064453" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.5862109375" Y="-3.089821289062" />
                  <Point X="-22.01332421875" Y="-4.082088867188" />
                  <Point X="-22.121767578125" Y="-4.159546875" />
                  <Point X="-22.164685546875" Y="-4.190202148438" />
                  <Point X="-22.313716796875" Y="-4.286666992188" />
                  <Point X="-22.320224609375" Y="-4.29087890625" />
                  <Point X="-22.529634765625" Y="-4.017967773438" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467529297" />
                  <Point X="-23.49317578125" Y="-2.875207519531" />
                  <Point X="-23.54257421875" Y="-2.843448730469" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.753255859375" Y="-2.852194335938" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868508789062" />
                  <Point X="-23.972931640625" Y="-2.98347265625" />
                  <Point X="-24.014650390625" Y="-3.018159179688" />
                  <Point X="-24.03154296875" Y="-3.045985107422" />
                  <Point X="-24.0728828125" Y="-3.236186279297" />
                  <Point X="-24.085357421875" Y="-3.293572998047" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-24.03514453125" Y="-3.697530029297" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.9650390625" Y="-4.954344726562" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#194" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.148453669796" Y="4.909161140093" Z="1.95" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.95" />
                  <Point X="-0.376520263215" Y="5.054873288322" Z="1.95" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.95" />
                  <Point X="-1.161639751034" Y="4.933966835172" Z="1.95" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.95" />
                  <Point X="-1.717584090305" Y="4.518668739546" Z="1.95" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.95" />
                  <Point X="-1.71512768982" Y="4.419451391419" Z="1.95" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.95" />
                  <Point X="-1.762912015899" Y="4.331282546769" Z="1.95" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.95" />
                  <Point X="-1.861168578771" Y="4.311213330345" Z="1.95" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.95" />
                  <Point X="-2.087938952835" Y="4.549497783769" Z="1.95" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.95" />
                  <Point X="-2.285468466021" Y="4.525911741359" Z="1.95" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.95" />
                  <Point X="-2.923778829153" Y="4.142305941339" Z="1.95" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.95" />
                  <Point X="-3.088940558729" Y="3.291721632871" Z="1.95" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.95" />
                  <Point X="-2.999789863124" Y="3.120483985277" Z="1.95" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.95" />
                  <Point X="-3.008114724876" Y="3.040688843011" Z="1.95" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.95" />
                  <Point X="-3.07459248033" Y="2.995774836049" Z="1.95" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.95" />
                  <Point X="-3.642137715426" Y="3.291253390017" Z="1.95" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.95" />
                  <Point X="-3.889534692194" Y="3.255289880974" Z="1.95" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.95" />
                  <Point X="-4.286476892867" Y="2.711359003022" Z="1.95" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.95" />
                  <Point X="-3.893831300754" Y="1.762203940412" Z="1.95" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.95" />
                  <Point X="-3.689669083508" Y="1.59759239313" Z="1.95" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.95" />
                  <Point X="-3.672535310171" Y="1.53991227569" Z="1.95" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.95" />
                  <Point X="-3.705707475863" Y="1.489711026875" Z="1.95" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.95" />
                  <Point X="-4.56997105532" Y="1.582402529763" Z="1.95" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.95" />
                  <Point X="-4.85273173301" Y="1.481136804301" Z="1.95" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.95" />
                  <Point X="-4.993110962462" Y="0.900882952136" Z="1.95" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.95" />
                  <Point X="-3.920473216242" Y="0.141220104918" Z="1.95" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.95" />
                  <Point X="-3.570128078206" Y="0.044604400423" Z="1.95" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.95" />
                  <Point X="-3.546663603414" Y="0.02289819302" Z="1.95" />
                  <Point X="-3.539556741714" Y="0" Z="1.95" />
                  <Point X="-3.541700997721" Y="-0.006908758041" Z="1.95" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.95" />
                  <Point X="-3.555240516917" Y="-0.034271582711" Z="1.95" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.95" />
                  <Point X="-4.716414572531" Y="-0.354491951838" Z="1.95" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.95" />
                  <Point X="-5.042325514938" Y="-0.572507880574" Z="1.95" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.95" />
                  <Point X="-4.951195173368" Y="-1.11286118596" Z="1.95" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.95" />
                  <Point X="-3.596443113962" Y="-1.356533709091" Z="1.95" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.95" />
                  <Point X="-3.21302066354" Y="-1.310475999677" Z="1.95" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.95" />
                  <Point X="-3.194462080547" Y="-1.329344750772" Z="1.95" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.95" />
                  <Point X="-4.200998063939" Y="-2.119997767452" Z="1.95" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.95" />
                  <Point X="-4.434861559141" Y="-2.465746808069" Z="1.95" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.95" />
                  <Point X="-4.128899133894" Y="-2.949589373668" Z="1.95" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.95" />
                  <Point X="-2.871700940424" Y="-2.728038702149" Z="1.95" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.95" />
                  <Point X="-2.568818396816" Y="-2.559512072315" Z="1.95" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.95" />
                  <Point X="-3.127378311019" Y="-3.563377442068" Z="1.95" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.95" />
                  <Point X="-3.205022184632" Y="-3.935311693617" Z="1.95" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.95" />
                  <Point X="-2.788639653383" Y="-4.240556389916" Z="1.95" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.95" />
                  <Point X="-2.278349326987" Y="-4.224385449772" Z="1.95" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.95" />
                  <Point X="-2.166429961546" Y="-4.116500172482" Z="1.95" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.95" />
                  <Point X="-1.896498355058" Y="-3.988787617184" Z="1.95" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.95" />
                  <Point X="-1.604600839234" Y="-4.051790617324" Z="1.95" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.95" />
                  <Point X="-1.411376719209" Y="-4.279470292769" Z="1.95" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.95" />
                  <Point X="-1.40192233299" Y="-4.794607688595" Z="1.95" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.95" />
                  <Point X="-1.34456132962" Y="-4.897137409925" Z="1.95" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.95" />
                  <Point X="-1.047928512888" Y="-4.969068589634" Z="1.95" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.95" />
                  <Point X="-0.509934897067" Y="-3.865286993701" Z="1.95" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.95" />
                  <Point X="-0.379137408243" Y="-3.464095269154" Z="1.95" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.95" />
                  <Point X="-0.194634637089" Y="-3.264646777102" Z="1.95" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.95" />
                  <Point X="0.058724442272" Y="-3.222465268186" Z="1.95" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.95" />
                  <Point X="0.291308451189" Y="-3.337550486614" Z="1.95" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.95" />
                  <Point X="0.724819963334" Y="-4.667249036115" Z="1.95" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.95" />
                  <Point X="0.859468389945" Y="-5.006169360197" Z="1.95" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.95" />
                  <Point X="1.03950932071" Y="-4.971904784631" Z="1.95" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.95" />
                  <Point X="1.008270275095" Y="-3.659723563098" Z="1.95" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.95" />
                  <Point X="0.969819032307" Y="-3.215526966793" Z="1.95" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.95" />
                  <Point X="1.052877267668" Y="-2.990639486837" Z="1.95" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.95" />
                  <Point X="1.245169392534" Y="-2.870703951172" Z="1.95" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.95" />
                  <Point X="1.473628943371" Y="-2.885985252117" Z="1.95" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.95" />
                  <Point X="2.424539841496" Y="-4.017125756342" Z="1.95" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.95" />
                  <Point X="2.707297037713" Y="-4.297360929065" Z="1.95" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.95" />
                  <Point X="2.901136053335" Y="-4.168954059871" Z="1.95" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.95" />
                  <Point X="2.450932976996" Y="-3.033541233188" Z="1.95" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.95" />
                  <Point X="2.262191365649" Y="-2.672212433606" Z="1.95" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.95" />
                  <Point X="2.254109801309" Y="-2.464598883077" Z="1.95" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.95" />
                  <Point X="2.368299495295" Y="-2.304791508658" Z="1.95" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.95" />
                  <Point X="2.556294369907" Y="-2.241256643842" Z="1.95" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.95" />
                  <Point X="3.753872363919" Y="-2.866816523279" Z="1.95" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.95" />
                  <Point X="4.105586052545" Y="-2.989008777091" Z="1.95" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.95" />
                  <Point X="4.276688535779" Y="-2.73860269695" Z="1.95" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.95" />
                  <Point X="3.472381615369" Y="-1.829167448834" Z="1.95" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.95" />
                  <Point X="3.16945314947" Y="-1.578367470033" Z="1.95" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.95" />
                  <Point X="3.095908216945" Y="-1.418683690494" Z="1.95" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.95" />
                  <Point X="3.133428316935" Y="-1.25677958583" Z="1.95" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.95" />
                  <Point X="3.259819136546" Y="-1.146237136722" Z="1.95" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.95" />
                  <Point X="4.557545233624" Y="-1.268406309207" Z="1.95" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.95" />
                  <Point X="4.926576599548" Y="-1.228655993326" Z="1.95" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.95" />
                  <Point X="5.004551938392" Y="-0.857443378549" Z="1.95" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.95" />
                  <Point X="4.049285656065" Y="-0.301552555492" Z="1.95" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.95" />
                  <Point X="3.726510415904" Y="-0.208416612523" Z="1.95" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.95" />
                  <Point X="3.642577344437" Y="-0.15094470236" Z="1.95" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.95" />
                  <Point X="3.595648266958" Y="-0.074217808696" Z="1.95" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.95" />
                  <Point X="3.585723183466" Y="0.022392722494" Z="1.95" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.95" />
                  <Point X="3.612802093963" Y="0.113004036215" Z="1.95" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.95" />
                  <Point X="3.676884998448" Y="0.179732231719" Z="1.95" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.95" />
                  <Point X="4.746682161241" Y="0.488419377277" Z="1.95" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.95" />
                  <Point X="5.032740070489" Y="0.667270430267" Z="1.95" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.95" />
                  <Point X="4.958626865393" Y="1.088914086368" Z="1.95" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.95" />
                  <Point X="3.791712483185" Y="1.265283931372" Z="1.95" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.95" />
                  <Point X="3.44129649993" Y="1.224908511428" Z="1.95" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.95" />
                  <Point X="3.352818740217" Y="1.243555058916" Z="1.95" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.95" />
                  <Point X="3.288179605708" Y="1.290601739772" Z="1.95" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.95" />
                  <Point X="3.24716562273" Y="1.366564670283" Z="1.95" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.95" />
                  <Point X="3.238580929416" Y="1.450188302024" Z="1.95" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.95" />
                  <Point X="3.268508937489" Y="1.526785863353" Z="1.95" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.95" />
                  <Point X="4.184373549201" Y="2.25340166401" Z="1.95" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.95" />
                  <Point X="4.398839490724" Y="2.535262463735" Z="1.95" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.95" />
                  <Point X="4.183500970876" Y="2.876744365502" Z="1.95" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.95" />
                  <Point X="2.855787661156" Y="2.466709838545" Z="1.95" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.95" />
                  <Point X="2.491268892552" Y="2.262022471441" Z="1.95" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.95" />
                  <Point X="2.413500120185" Y="2.247469515944" Z="1.95" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.95" />
                  <Point X="2.345492875341" Y="2.263857375912" Z="1.95" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.95" />
                  <Point X="2.286901224323" Y="2.311531985041" Z="1.95" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.95" />
                  <Point X="2.251960186815" Y="2.376258323647" Z="1.95" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.95" />
                  <Point X="2.250505471217" Y="2.44820068333" Z="1.95" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.95" />
                  <Point X="2.928915514314" Y="3.656351420799" Z="1.95" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.95" />
                  <Point X="3.041677948918" Y="4.064094228197" Z="1.95" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.95" />
                  <Point X="2.661309419729" Y="4.322741188011" Z="1.95" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.95" />
                  <Point X="2.26033017559" Y="4.545384011994" Z="1.95" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.95" />
                  <Point X="1.844991638984" Y="4.729228840577" Z="1.95" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.95" />
                  <Point X="1.365109731919" Y="4.884896649781" Z="1.95" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.95" />
                  <Point X="0.707422729317" Y="5.022474845694" Z="1.95" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.95" />
                  <Point X="0.044790891757" Y="4.522286365857" Z="1.95" />
                  <Point X="0" Y="4.355124473572" Z="1.95" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>