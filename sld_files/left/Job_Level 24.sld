<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#163" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1811" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000475585938" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.255130859375" Y="-4.190137207031" />
                  <Point X="-24.4366953125" Y="-3.512524414063" />
                  <Point X="-24.44227734375" Y="-3.497140136719" />
                  <Point X="-24.457634765625" Y="-3.467376953125" />
                  <Point X="-24.531845703125" Y="-3.360453613281" />
                  <Point X="-24.62136328125" Y="-3.2314765625" />
                  <Point X="-24.643248046875" Y="-3.209020751953" />
                  <Point X="-24.66950390625" Y="-3.189776855469" />
                  <Point X="-24.69750390625" Y="-3.175669433594" />
                  <Point X="-24.81233984375" Y="-3.140028320312" />
                  <Point X="-24.95086328125" Y="-3.097036132812" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.15166015625" Y="-3.132676757812" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.189776855469" />
                  <Point X="-25.344439453125" Y="-3.209020751953" />
                  <Point X="-25.36632421875" Y="-3.231477539062" />
                  <Point X="-25.440533203125" Y="-3.338400634766" />
                  <Point X="-25.53005078125" Y="-3.467377929688" />
                  <Point X="-25.5381875" Y="-3.481572021484" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.770005859375" Y="-4.329901367188" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-25.91973828125" Y="-4.876329589844" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.209140625" Y="-4.811953613281" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.236818359375" Y="-4.729453125" />
                  <Point X="-26.2149609375" Y="-4.563438476562" />
                  <Point X="-26.21419921875" Y="-4.5479296875" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.243943359375" Y="-4.378301269531" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.182964355469" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.42937109375" Y="-4.038484130859" />
                  <Point X="-26.556904296875" Y="-3.926639648438" />
                  <Point X="-26.5831875" Y="-3.910295898438" />
                  <Point X="-26.612884765625" Y="-3.897994628906" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.7833515625" Y="-3.881769042969" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708496094" />
                  <Point X="-27.014466796875" Y="-3.882028076172" />
                  <Point X="-27.042658203125" Y="-3.894801513672" />
                  <Point X="-27.15958203125" Y="-3.972928222656" />
                  <Point X="-27.300623046875" Y="-4.067169677734" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.3351015625" Y="-4.099461914062" />
                  <Point X="-27.4580625" Y="-4.259706542969" />
                  <Point X="-27.4801484375" Y="-4.288488769531" />
                  <Point X="-27.567767578125" Y="-4.234236816406" />
                  <Point X="-27.801712890625" Y="-4.089384277344" />
                  <Point X="-27.981443359375" Y="-3.950997558594" />
                  <Point X="-28.10472265625" Y="-3.856077148438" />
                  <Point X="-27.761181640625" Y="-3.261047363281" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647655273438" />
                  <Point X="-27.406587890625" Y="-2.616129882812" />
                  <Point X="-27.40557421875" Y="-2.585197753906" />
                  <Point X="-27.414556640625" Y="-2.555581298828" />
                  <Point X="-27.428771484375" Y="-2.526752685547" />
                  <Point X="-27.446798828125" Y="-2.501592285156" />
                  <Point X="-27.464146484375" Y="-2.484243652344" />
                  <Point X="-27.4893046875" Y="-2.46621484375" />
                  <Point X="-27.518134765625" Y="-2.451996826172" />
                  <Point X="-27.547751953125" Y="-2.443011962891" />
                  <Point X="-27.5786875" Y="-2.444024169922" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.344154296875" Y="-2.868212646484" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-28.89803515625" Y="-3.036682373047" />
                  <Point X="-29.082861328125" Y="-2.793860351562" />
                  <Point X="-29.21171484375" Y="-2.577792236328" />
                  <Point X="-29.306142578125" Y="-2.419450439453" />
                  <Point X="-28.696330078125" Y="-1.951523925781" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.084578125" Y="-1.475594726562" />
                  <Point X="-28.06661328125" Y="-1.448464477539" />
                  <Point X="-28.053857421875" Y="-1.419835449219" />
                  <Point X="-28.04615234375" Y="-1.390088256836" />
                  <Point X="-28.04334765625" Y="-1.359663330078" />
                  <Point X="-28.0455546875" Y="-1.327992431641" />
                  <Point X="-28.0525546875" Y="-1.298243286133" />
                  <Point X="-28.068638671875" Y="-1.272256469727" />
                  <Point X="-28.08947265625" Y="-1.248299438477" />
                  <Point X="-28.112970703125" Y="-1.228767700195" />
                  <Point X="-28.139453125" Y="-1.213181152344" />
                  <Point X="-28.16871484375" Y="-1.201957397461" />
                  <Point X="-28.2006015625" Y="-1.195475219727" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-29.12188671875" Y="-1.311549194336" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.7617890625" Y="-1.275654052734" />
                  <Point X="-29.834078125" Y="-0.992650390625" />
                  <Point X="-29.86816796875" Y="-0.75429095459" />
                  <Point X="-29.892423828125" Y="-0.584698364258" />
                  <Point X="-29.205400390625" Y="-0.400611053467" />
                  <Point X="-28.532875" Y="-0.220408279419" />
                  <Point X="-28.510626953125" Y="-0.211297363281" />
                  <Point X="-28.482478515625" Y="-0.195346466064" />
                  <Point X="-28.469212890625" Y="-0.186265762329" />
                  <Point X="-28.442220703125" Y="-0.164215179443" />
                  <Point X="-28.42601171875" Y="-0.147226165771" />
                  <Point X="-28.414462890625" Y="-0.126780334473" />
                  <Point X="-28.40284375" Y="-0.098530632019" />
                  <Point X="-28.398216796875" Y="-0.084101593018" />
                  <Point X="-28.3909140625" Y="-0.052987453461" />
                  <Point X="-28.388400390625" Y="-0.031613124847" />
                  <Point X="-28.390763671875" Y="-0.010221735001" />
                  <Point X="-28.397203125" Y="0.018105836868" />
                  <Point X="-28.401701171875" Y="0.032495716095" />
                  <Point X="-28.41418359375" Y="0.063532138824" />
                  <Point X="-28.425572265625" Y="0.084069480896" />
                  <Point X="-28.441650390625" Y="0.10118585968" />
                  <Point X="-28.466048828125" Y="0.12143560791" />
                  <Point X="-28.47921484375" Y="0.130602981567" />
                  <Point X="-28.50995703125" Y="0.148354721069" />
                  <Point X="-28.532875" Y="0.157848297119" />
                  <Point X="-29.344115234375" Y="0.375219512939" />
                  <Point X="-29.89181640625" Y="0.521975463867" />
                  <Point X="-29.87107421875" Y="0.66214440918" />
                  <Point X="-29.82448828125" Y="0.97696862793" />
                  <Point X="-29.755861328125" Y="1.230224731445" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-29.247748046875" Y="1.363260253906" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744982421875" Y="1.299341796875" />
                  <Point X="-28.723421875" Y="1.301228271484" />
                  <Point X="-28.70313671875" Y="1.305263427734" />
                  <Point X="-28.675294921875" Y="1.314041992188" />
                  <Point X="-28.6417109375" Y="1.324631225586" />
                  <Point X="-28.622775390625" Y="1.332962890625" />
                  <Point X="-28.60403125" Y="1.343785400391" />
                  <Point X="-28.5873515625" Y="1.356015014648" />
                  <Point X="-28.57371484375" Y="1.371564331055" />
                  <Point X="-28.561298828125" Y="1.389294921875" />
                  <Point X="-28.551349609375" Y="1.407431762695" />
                  <Point X="-28.540177734375" Y="1.43440246582" />
                  <Point X="-28.526703125" Y="1.466936035156" />
                  <Point X="-28.520912109375" Y="1.486806274414" />
                  <Point X="-28.51715625" Y="1.508121704102" />
                  <Point X="-28.515806640625" Y="1.528755981445" />
                  <Point X="-28.518951171875" Y="1.549193725586" />
                  <Point X="-28.524552734375" Y="1.570100219727" />
                  <Point X="-28.532048828125" Y="1.589375976562" />
                  <Point X="-28.54552734375" Y="1.615270751953" />
                  <Point X="-28.5617890625" Y="1.646505981445" />
                  <Point X="-28.57328515625" Y="1.663711303711" />
                  <Point X="-28.587197265625" Y="1.680289550781" />
                  <Point X="-28.60213671875" Y="1.694590332031" />
                  <Point X="-29.06746484375" Y="2.051650146484" />
                  <Point X="-29.351859375" Y="2.269873779297" />
                  <Point X="-29.262171875" Y="2.423531005859" />
                  <Point X="-29.0811484375" Y="2.733665283203" />
                  <Point X="-28.899369140625" Y="2.967318603516" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.50273828125" Y="3.015614013672" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.108017578125" Y="2.822404052734" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.04056640625" Y="2.818762939453" />
                  <Point X="-28.019107421875" Y="2.821587890625" />
                  <Point X="-27.999015625" Y="2.826504150391" />
                  <Point X="-27.980462890625" Y="2.835652832031" />
                  <Point X="-27.962208984375" Y="2.847281738281" />
                  <Point X="-27.946076171875" Y="2.860228515625" />
                  <Point X="-27.918552734375" Y="2.887751708984" />
                  <Point X="-27.8853515625" Y="2.920951904297" />
                  <Point X="-27.872404296875" Y="2.937085205078" />
                  <Point X="-27.860775390625" Y="2.955339599609" />
                  <Point X="-27.85162890625" Y="2.973888427734" />
                  <Point X="-27.8467109375" Y="2.993977294922" />
                  <Point X="-27.843884765625" Y="3.015436279297" />
                  <Point X="-27.84343359375" Y="3.036120849609" />
                  <Point X="-27.846826171875" Y="3.074896728516" />
                  <Point X="-27.85091796875" Y="3.121670410156" />
                  <Point X="-27.854955078125" Y="3.141962158203" />
                  <Point X="-27.86146484375" Y="3.162604736328" />
                  <Point X="-27.869794921875" Y="3.181532958984" />
                  <Point X="-28.07599609375" Y="3.538684326172" />
                  <Point X="-28.18333203125" Y="3.724595947266" />
                  <Point X="-28.0158984375" Y="3.852966552734" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.414322265625" Y="4.253747558594" />
                  <Point X="-27.167037109375" Y="4.391133789062" />
                  <Point X="-27.1338359375" Y="4.347864746094" />
                  <Point X="-27.0431953125" Y="4.229740722656" />
                  <Point X="-27.028890625" Y="4.214797851563" />
                  <Point X="-27.012310546875" Y="4.200885742188" />
                  <Point X="-26.99511328125" Y="4.189395019531" />
                  <Point X="-26.95195703125" Y="4.166928710938" />
                  <Point X="-26.899896484375" Y="4.139828125" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.85970703125" Y="4.126728515625" />
                  <Point X="-26.839263671875" Y="4.12358203125" />
                  <Point X="-26.818623046875" Y="4.124935546875" />
                  <Point X="-26.79730859375" Y="4.128694335938" />
                  <Point X="-26.777451171875" Y="4.134481933594" />
                  <Point X="-26.7325" Y="4.1531015625" />
                  <Point X="-26.67827734375" Y="4.175561523437" />
                  <Point X="-26.66014453125" Y="4.185509765625" />
                  <Point X="-26.6424140625" Y="4.197924316406" />
                  <Point X="-26.626859375" Y="4.211565429688" />
                  <Point X="-26.614626953125" Y="4.22825" />
                  <Point X="-26.603806640625" Y="4.246994140625" />
                  <Point X="-26.595478515625" Y="4.265921386719" />
                  <Point X="-26.58084765625" Y="4.31232421875" />
                  <Point X="-26.56319921875" Y="4.368297851562" />
                  <Point X="-26.5591640625" Y="4.388583984375" />
                  <Point X="-26.55727734375" Y="4.410145019531" />
                  <Point X="-26.557728515625" Y="4.430826660156" />
                  <Point X="-26.581171875" Y="4.608892578125" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.3577734375" Y="4.695380371094" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.602552734375" Y="4.850429199219" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.2214921875" Y="4.613203125" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.748126953125" Y="4.680626464844" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.512330078125" Y="4.869060546875" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.868802734375" Y="4.762411132812" />
                  <Point X="-23.51897265625" Y="4.677950195312" />
                  <Point X="-23.332873046875" Y="4.610451660156" />
                  <Point X="-23.105359375" Y="4.527930175781" />
                  <Point X="-22.9246171875" Y="4.443403808594" />
                  <Point X="-22.705423828125" Y="4.340894042969" />
                  <Point X="-22.530796875" Y="4.23915625" />
                  <Point X="-22.319017578125" Y="4.1157734375" />
                  <Point X="-22.154349609375" Y="3.998669921875" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.46187890625" Y="3.227529296875" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857921875" Y="2.53993359375" />
                  <Point X="-22.866921875" Y="2.516059326172" />
                  <Point X="-22.876654296875" Y="2.479669189453" />
                  <Point X="-22.888392578125" Y="2.435772949219" />
                  <Point X="-22.891380859375" Y="2.417935791016" />
                  <Point X="-22.892271484375" Y="2.380952392578" />
                  <Point X="-22.8884765625" Y="2.349485351562" />
                  <Point X="-22.883900390625" Y="2.311527587891" />
                  <Point X="-22.878556640625" Y="2.289603515625" />
                  <Point X="-22.8702890625" Y="2.267512695312" />
                  <Point X="-22.859927734375" Y="2.247470458984" />
                  <Point X="-22.84045703125" Y="2.218775390625" />
                  <Point X="-22.81696875" Y="2.184161865234" />
                  <Point X="-22.805533203125" Y="2.170327636719" />
                  <Point X="-22.778400390625" Y="2.145592529297" />
                  <Point X="-22.749705078125" Y="2.126121582031" />
                  <Point X="-22.715091796875" Y="2.102635009766" />
                  <Point X="-22.695046875" Y="2.092272705078" />
                  <Point X="-22.672958984375" Y="2.084006591797" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.619568359375" Y="2.074869140625" />
                  <Point X="-22.581611328125" Y="2.070291992188" />
                  <Point X="-22.563533203125" Y="2.069845703125" />
                  <Point X="-22.52679296875" Y="2.074171142578" />
                  <Point X="-22.49040234375" Y="2.083902587891" />
                  <Point X="-22.446505859375" Y="2.095640869141" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.595513671875" Y="2.581234863281" />
                  <Point X="-21.032673828125" Y="2.906190673828" />
                  <Point X="-21.00234765625" Y="2.864044433594" />
                  <Point X="-20.87671875" Y="2.689449462891" />
                  <Point X="-20.78492578125" Y="2.537758789062" />
                  <Point X="-20.73780078125" Y="2.459883789063" />
                  <Point X="-21.25503515625" Y="2.062995361328" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.778572265625" Y="1.660243896484" />
                  <Point X="-21.79602734375" Y="1.641626708984" />
                  <Point X="-21.822216796875" Y="1.607459838867" />
                  <Point X="-21.85380859375" Y="1.566245605469" />
                  <Point X="-21.863392578125" Y="1.550911621094" />
                  <Point X="-21.878369140625" Y="1.517087646484" />
                  <Point X="-21.888125" Y="1.482203125" />
                  <Point X="-21.89989453125" Y="1.440123168945" />
                  <Point X="-21.90334765625" Y="1.417825561523" />
                  <Point X="-21.9041640625" Y="1.394254272461" />
                  <Point X="-21.902259765625" Y="1.371765625" />
                  <Point X="-21.89425" Y="1.332951904297" />
                  <Point X="-21.88458984375" Y="1.28613269043" />
                  <Point X="-21.8793203125" Y="1.268979003906" />
                  <Point X="-21.863716796875" Y="1.235740356445" />
                  <Point X="-21.84193359375" Y="1.202632080078" />
                  <Point X="-21.81566015625" Y="1.162694946289" />
                  <Point X="-21.801109375" Y="1.145453491211" />
                  <Point X="-21.783865234375" Y="1.129362426758" />
                  <Point X="-21.76565234375" Y="1.116034545898" />
                  <Point X="-21.7340859375" Y="1.09826574707" />
                  <Point X="-21.696009765625" Y="1.07683215332" />
                  <Point X="-21.6794765625" Y="1.069500976563" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.601201171875" Y="1.053797851562" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.736697265625" Y="1.149028198242" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.208017578125" Y="1.154431030273" />
                  <Point X="-20.15405859375" Y="0.932786193848" />
                  <Point X="-20.1251328125" Y="0.746999938965" />
                  <Point X="-20.109134765625" Y="0.644238769531" />
                  <Point X="-20.694158203125" Y="0.487481842041" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.295208984375" Y="0.32558605957" />
                  <Point X="-21.318455078125" Y="0.315067596436" />
                  <Point X="-21.360384765625" Y="0.2908309021" />
                  <Point X="-21.41096484375" Y="0.261595336914" />
                  <Point X="-21.4256875" Y="0.251097213745" />
                  <Point X="-21.45246875" Y="0.225576385498" />
                  <Point X="-21.477626953125" Y="0.193518722534" />
                  <Point X="-21.507974609375" Y="0.154848754883" />
                  <Point X="-21.51969921875" Y="0.135566680908" />
                  <Point X="-21.52947265625" Y="0.114102310181" />
                  <Point X="-21.536318359375" Y="0.092604698181" />
                  <Point X="-21.544705078125" Y="0.048815151215" />
                  <Point X="-21.5548203125" Y="-0.00400592041" />
                  <Point X="-21.556515625" Y="-0.021875896454" />
                  <Point X="-21.5548203125" Y="-0.058554214478" />
                  <Point X="-21.54643359375" Y="-0.102343452454" />
                  <Point X="-21.536318359375" Y="-0.155164825439" />
                  <Point X="-21.52947265625" Y="-0.176662445068" />
                  <Point X="-21.51969921875" Y="-0.198126815796" />
                  <Point X="-21.507974609375" Y="-0.217408889771" />
                  <Point X="-21.48281640625" Y="-0.249466567993" />
                  <Point X="-21.45246875" Y="-0.288136535645" />
                  <Point X="-21.439998046875" Y="-0.301238006592" />
                  <Point X="-21.410962890625" Y="-0.324155914307" />
                  <Point X="-21.36903125" Y="-0.348392578125" />
                  <Point X="-21.318453125" Y="-0.377628173828" />
                  <Point X="-21.30729296875" Y="-0.383137237549" />
                  <Point X="-21.283419921875" Y="-0.392149810791" />
                  <Point X="-20.572619140625" Y="-0.582608581543" />
                  <Point X="-20.10852734375" Y="-0.706961791992" />
                  <Point X="-20.1148515625" Y="-0.748914245605" />
                  <Point X="-20.144974609375" Y="-0.948724731445" />
                  <Point X="-20.1820390625" Y="-1.111143554688" />
                  <Point X="-20.19882421875" Y="-1.18469909668" />
                  <Point X="-20.892240234375" Y="-1.093409057617" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710266113" />
                  <Point X="-21.62533984375" Y="-1.005508666992" />
                  <Point X="-21.707634765625" Y="-1.023395996094" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.836025390625" Y="-1.056597412109" />
                  <Point X="-21.8638515625" Y="-1.073489868164" />
                  <Point X="-21.8876015625" Y="-1.093960327148" />
                  <Point X="-21.93734375" Y="-1.153784667969" />
                  <Point X="-21.997345703125" Y="-1.225948364258" />
                  <Point X="-22.012064453125" Y="-1.250329345703" />
                  <Point X="-22.023408203125" Y="-1.277714599609" />
                  <Point X="-22.030240234375" Y="-1.305365966797" />
                  <Point X="-22.037369140625" Y="-1.382841186523" />
                  <Point X="-22.04596875" Y="-1.476296142578" />
                  <Point X="-22.04365234375" Y="-1.507561401367" />
                  <Point X="-22.035921875" Y="-1.539182495117" />
                  <Point X="-22.023548828125" Y="-1.567996582031" />
                  <Point X="-21.978005859375" Y="-1.638836181641" />
                  <Point X="-21.923068359375" Y="-1.724287231445" />
                  <Point X="-21.9130625" Y="-1.737242919922" />
                  <Point X="-21.889369140625" Y="-1.760909301758" />
                  <Point X="-21.2297421875" Y="-2.267059570312" />
                  <Point X="-20.786875" Y="-2.606883056641" />
                  <Point X="-20.790267578125" Y="-2.612372314453" />
                  <Point X="-20.875177734375" Y="-2.749765625" />
                  <Point X="-20.95183984375" Y="-2.858696044922" />
                  <Point X="-20.971017578125" Y="-2.885945068359" />
                  <Point X="-21.590404296875" Y="-2.528341552734" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.245775390625" Y="-2.159825195312" />
                  <Point X="-22.34371875" Y="-2.14213671875" />
                  <Point X="-22.461865234375" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135177001953" />
                  <Point X="-22.636533203125" Y="-2.178000244141" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.757615234375" Y="-2.246549804688" />
                  <Point X="-22.77857421875" Y="-2.267509521484" />
                  <Point X="-22.795466796875" Y="-2.290438476562" />
                  <Point X="-22.838291015625" Y="-2.371806152344" />
                  <Point X="-22.889947265625" Y="-2.469956542969" />
                  <Point X="-22.899771484375" Y="-2.499735351563" />
                  <Point X="-22.904728515625" Y="-2.531908447266" />
                  <Point X="-22.90432421875" Y="-2.563259277344" />
                  <Point X="-22.886634765625" Y="-2.661203369141" />
                  <Point X="-22.865296875" Y="-2.779349609375" />
                  <Point X="-22.86101171875" Y="-2.795140136719" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.42430078125" Y="-3.560255615234" />
                  <Point X="-22.138712890625" Y="-4.054906494141" />
                  <Point X="-22.218134765625" Y="-4.111634277344" />
                  <Point X="-22.298232421875" Y="-4.163480957031" />
                  <Point X="-22.77700390625" Y="-3.539535400391" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.374673828125" Y="-2.838452636719" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.688546875" Y="-2.750838623047" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-23.97698046875" Y="-2.863291015625" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968860839844" />
                  <Point X="-24.11275" Y="-2.996687255859" />
                  <Point X="-24.124375" Y="-3.025809082031" />
                  <Point X="-24.148765625" Y="-3.138029785156" />
                  <Point X="-24.178189453125" Y="-3.273396972656" />
                  <Point X="-24.180275390625" Y="-3.289627197266" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.0601328125" Y="-4.235549316406" />
                  <Point X="-23.97793359375" Y="-4.859915527344" />
                  <Point X="-24.024318359375" Y="-4.870083007812" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.058427734375" Y="-4.752636230469" />
                  <Point X="-26.14124609375" Y="-4.731328125" />
                  <Point X="-26.1207734375" Y="-4.575836914062" />
                  <Point X="-26.120076171875" Y="-4.568098632812" />
                  <Point X="-26.11944921875" Y="-4.541030761719" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497688964844" />
                  <Point X="-26.15076953125" Y="-4.359767089844" />
                  <Point X="-26.18386328125" Y="-4.193397460938" />
                  <Point X="-26.188125" Y="-4.178468261719" />
                  <Point X="-26.19902734375" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135462890625" />
                  <Point X="-26.221740234375" Y="-4.107625976562" />
                  <Point X="-26.23057421875" Y="-4.094861083984" />
                  <Point X="-26.25020703125" Y="-4.070937744141" />
                  <Point X="-26.261005859375" Y="-4.059779296875" />
                  <Point X="-26.366732421875" Y="-3.967059326172" />
                  <Point X="-26.494265625" Y="-3.85521484375" />
                  <Point X="-26.50673828125" Y="-3.845965087891" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822527587891" />
                  <Point X="-26.576529296875" Y="-3.810226318359" />
                  <Point X="-26.5913125" Y="-3.805476318359" />
                  <Point X="-26.621455078125" Y="-3.798447998047" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.777138671875" Y="-3.786972412109" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166015625" />
                  <Point X="-27.008005859375" Y="-3.781945556641" />
                  <Point X="-27.0390546875" Y="-3.790265136719" />
                  <Point X="-27.053673828125" Y="-3.79549609375" />
                  <Point X="-27.081865234375" Y="-3.80826953125" />
                  <Point X="-27.0954375" Y="-3.815812011719" />
                  <Point X="-27.212361328125" Y="-3.893938720703" />
                  <Point X="-27.35340234375" Y="-3.988180175781" />
                  <Point X="-27.35968359375" Y="-3.992759765625" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032770507812" />
                  <Point X="-27.410470703125" Y="-4.041629394531" />
                  <Point X="-27.503201171875" Y="-4.162478027344" />
                  <Point X="-27.517755859375" Y="-4.153466308594" />
                  <Point X="-27.747591796875" Y="-4.011157470703" />
                  <Point X="-27.923486328125" Y="-3.875725097656" />
                  <Point X="-27.98086328125" Y="-3.831546386719" />
                  <Point X="-27.67891015625" Y="-3.308547363281" />
                  <Point X="-27.341490234375" Y="-2.724119628906" />
                  <Point X="-27.3348515625" Y="-2.7100859375" />
                  <Point X="-27.32394921875" Y="-2.681121582031" />
                  <Point X="-27.319685546875" Y="-2.666190917969" />
                  <Point X="-27.3134140625" Y="-2.634665527344" />
                  <Point X="-27.311638671875" Y="-2.619241455078" />
                  <Point X="-27.310625" Y="-2.588309326172" />
                  <Point X="-27.3146640625" Y="-2.557625244141" />
                  <Point X="-27.323646484375" Y="-2.528008789062" />
                  <Point X="-27.3293515625" Y="-2.513568359375" />
                  <Point X="-27.34356640625" Y="-2.484739746094" />
                  <Point X="-27.351546875" Y="-2.471422119141" />
                  <Point X="-27.36957421875" Y="-2.44626171875" />
                  <Point X="-27.37962109375" Y="-2.434418945313" />
                  <Point X="-27.39696875" Y="-2.4170703125" />
                  <Point X="-27.408810546875" Y="-2.407024169922" />
                  <Point X="-27.43396875" Y="-2.388995361328" />
                  <Point X="-27.44728515625" Y="-2.381012695312" />
                  <Point X="-27.476115234375" Y="-2.366794677734" />
                  <Point X="-27.490556640625" Y="-2.361087890625" />
                  <Point X="-27.520173828125" Y="-2.352103027344" />
                  <Point X="-27.550859375" Y="-2.348062744141" />
                  <Point X="-27.581794921875" Y="-2.349074951172" />
                  <Point X="-27.597220703125" Y="-2.350849365234" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.391654296875" Y="-2.785940185547" />
                  <Point X="-28.793087890625" Y="-3.017708251953" />
                  <Point X="-28.82244140625" Y="-2.979144042969" />
                  <Point X="-29.0040234375" Y="-2.740584228516" />
                  <Point X="-29.130123046875" Y="-2.529134033203" />
                  <Point X="-29.181265625" Y="-2.443374023438" />
                  <Point X="-28.638498046875" Y="-2.026892456055" />
                  <Point X="-28.048123046875" Y="-1.573881958008" />
                  <Point X="-28.036484375" Y="-1.563311279297" />
                  <Point X="-28.015107421875" Y="-1.540392578125" />
                  <Point X="-28.005369140625" Y="-1.528044433594" />
                  <Point X="-27.987404296875" Y="-1.50091418457" />
                  <Point X="-27.979837890625" Y="-1.487128051758" />
                  <Point X="-27.96708203125" Y="-1.458499145508" />
                  <Point X="-27.961892578125" Y="-1.443656005859" />
                  <Point X="-27.9541875" Y="-1.413908935547" />
                  <Point X="-27.951552734375" Y="-1.39880871582" />
                  <Point X="-27.948748046875" Y="-1.368383789063" />
                  <Point X="-27.948578125" Y="-1.353059082031" />
                  <Point X="-27.95078515625" Y="-1.321388183594" />
                  <Point X="-27.953080078125" Y="-1.306233032227" />
                  <Point X="-27.960080078125" Y="-1.276484008789" />
                  <Point X="-27.971775390625" Y="-1.248246582031" />
                  <Point X="-27.987859375" Y="-1.222259765625" />
                  <Point X="-27.996953125" Y="-1.209916503906" />
                  <Point X="-28.017787109375" Y="-1.185959350586" />
                  <Point X="-28.02874609375" Y="-1.17524206543" />
                  <Point X="-28.052244140625" Y="-1.155710327148" />
                  <Point X="-28.064783203125" Y="-1.146895629883" />
                  <Point X="-28.091265625" Y="-1.131309082031" />
                  <Point X="-28.105431640625" Y="-1.124482177734" />
                  <Point X="-28.134693359375" Y="-1.113258422852" />
                  <Point X="-28.1497890625" Y="-1.108861572266" />
                  <Point X="-28.18167578125" Y="-1.102379394531" />
                  <Point X="-28.197294921875" Y="-1.100532836914" />
                  <Point X="-28.22862109375" Y="-1.09944152832" />
                  <Point X="-28.244328125" Y="-1.100196655273" />
                  <Point X="-29.134287109375" Y="-1.217361938477" />
                  <Point X="-29.66091796875" Y="-1.286694213867" />
                  <Point X="-29.669744140625" Y="-1.252142700195" />
                  <Point X="-29.740763671875" Y="-0.974112854004" />
                  <Point X="-29.774125" Y="-0.740841125488" />
                  <Point X="-29.786451171875" Y="-0.654654541016" />
                  <Point X="-29.1808125" Y="-0.492374084473" />
                  <Point X="-28.508287109375" Y="-0.312171173096" />
                  <Point X="-28.496873046875" Y="-0.308322143555" />
                  <Point X="-28.474625" Y="-0.299211273193" />
                  <Point X="-28.463791015625" Y="-0.293949279785" />
                  <Point X="-28.435642578125" Y="-0.277998474121" />
                  <Point X="-28.42881640625" Y="-0.273738952637" />
                  <Point X="-28.409111328125" Y="-0.25983706665" />
                  <Point X="-28.382119140625" Y="-0.237786468506" />
                  <Point X="-28.373486328125" Y="-0.229793823242" />
                  <Point X="-28.35727734375" Y="-0.212804885864" />
                  <Point X="-28.343294921875" Y="-0.193948501587" />
                  <Point X="-28.33174609375" Y="-0.173502746582" />
                  <Point X="-28.326603515625" Y="-0.162916793823" />
                  <Point X="-28.314984375" Y="-0.134666992188" />
                  <Point X="-28.312380859375" Y="-0.12753918457" />
                  <Point X="-28.30573046875" Y="-0.105808883667" />
                  <Point X="-28.298427734375" Y="-0.074694831848" />
                  <Point X="-28.296564453125" Y="-0.064083152771" />
                  <Point X="-28.29405078125" Y="-0.042708950043" />
                  <Point X="-28.293974609375" Y="-0.021181221008" />
                  <Point X="-28.296337890625" Y="0.000210221902" />
                  <Point X="-28.298126953125" Y="0.010836462021" />
                  <Point X="-28.30456640625" Y="0.039164134979" />
                  <Point X="-28.306529296875" Y="0.046448886871" />
                  <Point X="-28.3135625" Y="0.067943916321" />
                  <Point X="-28.326044921875" Y="0.098980247498" />
                  <Point X="-28.331103515625" Y="0.109603363037" />
                  <Point X="-28.3424921875" Y="0.130140823364" />
                  <Point X="-28.356330078125" Y="0.149111785889" />
                  <Point X="-28.372408203125" Y="0.166228103638" />
                  <Point X="-28.380978515625" Y="0.174287918091" />
                  <Point X="-28.405376953125" Y="0.194537780762" />
                  <Point X="-28.411763671875" Y="0.199398208618" />
                  <Point X="-28.431708984375" Y="0.212872207642" />
                  <Point X="-28.462451171875" Y="0.230624023438" />
                  <Point X="-28.473599609375" Y="0.236122467041" />
                  <Point X="-28.496517578125" Y="0.245615905762" />
                  <Point X="-28.508287109375" Y="0.249611190796" />
                  <Point X="-29.31952734375" Y="0.466982452393" />
                  <Point X="-29.7854453125" Y="0.591824707031" />
                  <Point X="-29.77709765625" Y="0.648237731934" />
                  <Point X="-29.73133203125" Y="0.9575234375" />
                  <Point X="-29.66416796875" Y="1.205377929688" />
                  <Point X="-29.6335859375" Y="1.318237060547" />
                  <Point X="-29.2601484375" Y="1.269072998047" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.7470546875" Y="1.204364379883" />
                  <Point X="-28.736701171875" Y="1.204703369141" />
                  <Point X="-28.715140625" Y="1.20658984375" />
                  <Point X="-28.70488671875" Y="1.208053833008" />
                  <Point X="-28.6846015625" Y="1.212088989258" />
                  <Point X="-28.6745703125" Y="1.214660400391" />
                  <Point X="-28.646728515625" Y="1.223438964844" />
                  <Point X="-28.61314453125" Y="1.234028198242" />
                  <Point X="-28.603451171875" Y="1.237676391602" />
                  <Point X="-28.584515625" Y="1.246008056641" />
                  <Point X="-28.5752734375" Y="1.25069152832" />
                  <Point X="-28.556529296875" Y="1.261514038086" />
                  <Point X="-28.547857421875" Y="1.267172241211" />
                  <Point X="-28.531177734375" Y="1.279401855469" />
                  <Point X="-28.515927734375" Y="1.293376342773" />
                  <Point X="-28.502291015625" Y="1.30892565918" />
                  <Point X="-28.495896484375" Y="1.317071899414" />
                  <Point X="-28.48348046875" Y="1.334802612305" />
                  <Point X="-28.4780078125" Y="1.343604492188" />
                  <Point X="-28.46805859375" Y="1.361741333008" />
                  <Point X="-28.46358203125" Y="1.371076171875" />
                  <Point X="-28.45241015625" Y="1.39804675293" />
                  <Point X="-28.438935546875" Y="1.430580444336" />
                  <Point X="-28.435498046875" Y="1.440354980469" />
                  <Point X="-28.42970703125" Y="1.460225219727" />
                  <Point X="-28.427353515625" Y="1.470320922852" />
                  <Point X="-28.42359765625" Y="1.491636230469" />
                  <Point X="-28.422359375" Y="1.501921386719" />
                  <Point X="-28.421009765625" Y="1.522555664062" />
                  <Point X="-28.421912109375" Y="1.543202636719" />
                  <Point X="-28.425056640625" Y="1.563640380859" />
                  <Point X="-28.4271875" Y="1.573780273438" />
                  <Point X="-28.4327890625" Y="1.594686767578" />
                  <Point X="-28.43601171875" Y="1.604532470703" />
                  <Point X="-28.4435078125" Y="1.623808227539" />
                  <Point X="-28.44778125" Y="1.63323840332" />
                  <Point X="-28.461259765625" Y="1.659133300781" />
                  <Point X="-28.477521484375" Y="1.690368408203" />
                  <Point X="-28.482798828125" Y="1.699284667969" />
                  <Point X="-28.494294921875" Y="1.716489990234" />
                  <Point X="-28.500513671875" Y="1.724779541016" />
                  <Point X="-28.51442578125" Y="1.741357788086" />
                  <Point X="-28.521505859375" Y="1.748915649414" />
                  <Point X="-28.5364453125" Y="1.763216186523" />
                  <Point X="-28.5443046875" Y="1.769958862305" />
                  <Point X="-29.0096328125" Y="2.127018554688" />
                  <Point X="-29.22761328125" Y="2.29428125" />
                  <Point X="-29.180125" Y="2.375641601562" />
                  <Point X="-29.00228515625" Y="2.680322753906" />
                  <Point X="-28.824388671875" Y="2.908984375" />
                  <Point X="-28.726337890625" Y="3.035013427734" />
                  <Point X="-28.55023828125" Y="2.933341796875" />
                  <Point X="-28.25415625" Y="2.762398681641" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.116296875" Y="2.727765625" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.038494140625" Y="2.723785644531" />
                  <Point X="-28.02816796875" Y="2.724575683594" />
                  <Point X="-28.006708984375" Y="2.727400634766" />
                  <Point X="-27.99652734375" Y="2.729310302734" />
                  <Point X="-27.976435546875" Y="2.7342265625" />
                  <Point X="-27.957" Y="2.741300292969" />
                  <Point X="-27.938447265625" Y="2.750448974609" />
                  <Point X="-27.929419921875" Y="2.755530517578" />
                  <Point X="-27.911166015625" Y="2.767159423828" />
                  <Point X="-27.90275" Y="2.773190185547" />
                  <Point X="-27.8866171875" Y="2.786136962891" />
                  <Point X="-27.878900390625" Y="2.793052978516" />
                  <Point X="-27.851376953125" Y="2.820576171875" />
                  <Point X="-27.81817578125" Y="2.853776367188" />
                  <Point X="-27.811259765625" Y="2.8614921875" />
                  <Point X="-27.7983125" Y="2.877625488281" />
                  <Point X="-27.79228125" Y="2.88604296875" />
                  <Point X="-27.78065234375" Y="2.904297363281" />
                  <Point X="-27.7755703125" Y="2.913325195312" />
                  <Point X="-27.766423828125" Y="2.931874023438" />
                  <Point X="-27.759353515625" Y="2.951298583984" />
                  <Point X="-27.754435546875" Y="2.971387451172" />
                  <Point X="-27.7525234375" Y="2.981572753906" />
                  <Point X="-27.749697265625" Y="3.003031738281" />
                  <Point X="-27.748908203125" Y="3.013364746094" />
                  <Point X="-27.74845703125" Y="3.034049316406" />
                  <Point X="-27.748794921875" Y="3.044400878906" />
                  <Point X="-27.7521875" Y="3.083176757812" />
                  <Point X="-27.756279296875" Y="3.129950439453" />
                  <Point X="-27.757744140625" Y="3.140207763672" />
                  <Point X="-27.76178125" Y="3.160499511719" />
                  <Point X="-27.764353515625" Y="3.170533935547" />
                  <Point X="-27.77086328125" Y="3.191176513672" />
                  <Point X="-27.774513671875" Y="3.200871337891" />
                  <Point X="-27.78284375" Y="3.219799560547" />
                  <Point X="-27.7875234375" Y="3.229032958984" />
                  <Point X="-27.993724609375" Y="3.586184326172" />
                  <Point X="-28.05938671875" Y="3.699915283203" />
                  <Point X="-27.958095703125" Y="3.777574951172" />
                  <Point X="-27.6483671875" Y="4.015041015625" />
                  <Point X="-27.368185546875" Y="4.170703613281" />
                  <Point X="-27.192525390625" Y="4.268296386719" />
                  <Point X="-27.118564453125" Y="4.171908691406" />
                  <Point X="-27.1118203125" Y="4.164046875" />
                  <Point X="-27.097515625" Y="4.149104003906" />
                  <Point X="-27.089955078125" Y="4.142022949219" />
                  <Point X="-27.073375" Y="4.128110839844" />
                  <Point X="-27.06508984375" Y="4.121895996094" />
                  <Point X="-27.047892578125" Y="4.110405273438" />
                  <Point X="-27.03898046875" Y="4.105129394531" />
                  <Point X="-26.99582421875" Y="4.082663085938" />
                  <Point X="-26.943763671875" Y="4.0555625" />
                  <Point X="-26.934326171875" Y="4.051286621094" />
                  <Point X="-26.915044921875" Y="4.0437890625" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.88429296875" Y="4.034965332031" />
                  <Point X="-26.874158203125" Y="4.032834228516" />
                  <Point X="-26.85371484375" Y="4.029687744141" />
                  <Point X="-26.833046875" Y="4.028785644531" />
                  <Point X="-26.81240625" Y="4.030139160156" />
                  <Point X="-26.802125" Y="4.031379150391" />
                  <Point X="-26.780810546875" Y="4.035137939453" />
                  <Point X="-26.7707265625" Y="4.037489257812" />
                  <Point X="-26.750869140625" Y="4.043276855469" />
                  <Point X="-26.741095703125" Y="4.046713623047" />
                  <Point X="-26.69614453125" Y="4.065333251953" />
                  <Point X="-26.641921875" Y="4.087793212891" />
                  <Point X="-26.63258203125" Y="4.092272949219" />
                  <Point X="-26.61444921875" Y="4.102221191406" />
                  <Point X="-26.60565625" Y="4.107689453125" />
                  <Point X="-26.58792578125" Y="4.120104003906" />
                  <Point X="-26.579775390625" Y="4.126499511719" />
                  <Point X="-26.564220703125" Y="4.140140625" />
                  <Point X="-26.550244140625" Y="4.15539453125" />
                  <Point X="-26.53801171875" Y="4.172079101562" />
                  <Point X="-26.5323515625" Y="4.180755371094" />
                  <Point X="-26.52153125" Y="4.199499511719" />
                  <Point X="-26.5168515625" Y="4.208733398438" />
                  <Point X="-26.5085234375" Y="4.227660644531" />
                  <Point X="-26.504875" Y="4.237354003906" />
                  <Point X="-26.490244140625" Y="4.283756835938" />
                  <Point X="-26.472595703125" Y="4.33973046875" />
                  <Point X="-26.470025390625" Y="4.349764160156" />
                  <Point X="-26.465990234375" Y="4.370050292969" />
                  <Point X="-26.464525390625" Y="4.380302734375" />
                  <Point X="-26.462638671875" Y="4.401863769531" />
                  <Point X="-26.46230078125" Y="4.412216796875" />
                  <Point X="-26.462751953125" Y="4.4328984375" />
                  <Point X="-26.463541015625" Y="4.443227050781" />
                  <Point X="-26.479263671875" Y="4.562655273438" />
                  <Point X="-26.332126953125" Y="4.603907226562" />
                  <Point X="-25.931173828125" Y="4.7163203125" />
                  <Point X="-25.591509765625" Y="4.756073242188" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.313255859375" Y="4.588615234375" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.65636328125" Y="4.656038574219" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.522224609375" Y="4.774577148438" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.89109765625" Y="4.670064453125" />
                  <Point X="-23.54640234375" Y="4.586843261719" />
                  <Point X="-23.365265625" Y="4.52114453125" />
                  <Point X="-23.141748046875" Y="4.440072753906" />
                  <Point X="-22.964861328125" Y="4.357349121094" />
                  <Point X="-22.74954296875" Y="4.256651367187" />
                  <Point X="-22.578619140625" Y="4.157071289063" />
                  <Point X="-22.370564453125" Y="4.035858398438" />
                  <Point X="-22.20940625" Y="3.921250488281" />
                  <Point X="-22.182216796875" Y="3.901915527344" />
                  <Point X="-22.544150390625" Y="3.275029296875" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.937619140625" Y="2.593116699219" />
                  <Point X="-22.946814453125" Y="2.573444091797" />
                  <Point X="-22.955814453125" Y="2.549569824219" />
                  <Point X="-22.958697265625" Y="2.540604003906" />
                  <Point X="-22.9684296875" Y="2.504213867188" />
                  <Point X="-22.98016796875" Y="2.460317626953" />
                  <Point X="-22.9820859375" Y="2.451469726562" />
                  <Point X="-22.986353515625" Y="2.420222900391" />
                  <Point X="-22.987244140625" Y="2.383239501953" />
                  <Point X="-22.986587890625" Y="2.369577880859" />
                  <Point X="-22.98279296875" Y="2.338110839844" />
                  <Point X="-22.978216796875" Y="2.300153076172" />
                  <Point X="-22.97619921875" Y="2.289031005859" />
                  <Point X="-22.97085546875" Y="2.267106933594" />
                  <Point X="-22.967529296875" Y="2.256304931641" />
                  <Point X="-22.95926171875" Y="2.234214111328" />
                  <Point X="-22.9546796875" Y="2.223885253906" />
                  <Point X="-22.944318359375" Y="2.203843017578" />
                  <Point X="-22.9385390625" Y="2.194129638672" />
                  <Point X="-22.919068359375" Y="2.165434570313" />
                  <Point X="-22.895580078125" Y="2.130821044922" />
                  <Point X="-22.89019140625" Y="2.123635253906" />
                  <Point X="-22.86953515625" Y="2.100122070312" />
                  <Point X="-22.84240234375" Y="2.075386962891" />
                  <Point X="-22.8317421875" Y="2.066981201172" />
                  <Point X="-22.803046875" Y="2.047510253906" />
                  <Point X="-22.76843359375" Y="2.024023681641" />
                  <Point X="-22.75871875" Y="2.018244384766" />
                  <Point X="-22.738673828125" Y="2.007882080078" />
                  <Point X="-22.72834375" Y="2.003299194336" />
                  <Point X="-22.706255859375" Y="1.995033081055" />
                  <Point X="-22.695453125" Y="1.991708007812" />
                  <Point X="-22.673529296875" Y="1.986365112305" />
                  <Point X="-22.662408203125" Y="1.984346801758" />
                  <Point X="-22.63094140625" Y="1.980552368164" />
                  <Point X="-22.592984375" Y="1.975975219727" />
                  <Point X="-22.583955078125" Y="1.975320922852" />
                  <Point X="-22.55242578125" Y="1.975497314453" />
                  <Point X="-22.515685546875" Y="1.979822875977" />
                  <Point X="-22.50225" Y="1.982395996094" />
                  <Point X="-22.465859375" Y="1.992127441406" />
                  <Point X="-22.421962890625" Y="2.003865722656" />
                  <Point X="-22.416" Y="2.005671875" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572753906" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.548013671875" Y="2.498962402344" />
                  <Point X="-21.059595703125" Y="2.780950683594" />
                  <Point X="-20.95603515625" Y="2.637025390625" />
                  <Point X="-20.866203125" Y="2.488575195312" />
                  <Point X="-20.863115234375" Y="2.483471435547" />
                  <Point X="-21.3128671875" Y="2.138363769531" />
                  <Point X="-21.827046875" Y="1.743819946289" />
                  <Point X="-21.831857421875" Y="1.739871337891" />
                  <Point X="-21.847875" Y="1.725221191406" />
                  <Point X="-21.865330078125" Y="1.706604003906" />
                  <Point X="-21.87142578125" Y="1.699420410156" />
                  <Point X="-21.897615234375" Y="1.665253662109" />
                  <Point X="-21.92920703125" Y="1.624039428711" />
                  <Point X="-21.9343671875" Y="1.616596435547" />
                  <Point X="-21.9502578125" Y="1.589374023438" />
                  <Point X="-21.965234375" Y="1.555550048828" />
                  <Point X="-21.969859375" Y="1.542673828125" />
                  <Point X="-21.979615234375" Y="1.507789306641" />
                  <Point X="-21.991384765625" Y="1.465709350586" />
                  <Point X="-21.993775390625" Y="1.454662231445" />
                  <Point X="-21.997228515625" Y="1.432364624023" />
                  <Point X="-21.998291015625" Y="1.421114013672" />
                  <Point X="-21.999107421875" Y="1.397542724609" />
                  <Point X="-21.998826171875" Y="1.386238525391" />
                  <Point X="-21.996921875" Y="1.36374987793" />
                  <Point X="-21.995298828125" Y="1.352565551758" />
                  <Point X="-21.9872890625" Y="1.313751708984" />
                  <Point X="-21.97762890625" Y="1.266932617188" />
                  <Point X="-21.97540234375" Y="1.258235717773" />
                  <Point X="-21.96531640625" Y="1.22860925293" />
                  <Point X="-21.949712890625" Y="1.195370605469" />
                  <Point X="-21.943080078125" Y="1.183524414062" />
                  <Point X="-21.921296875" Y="1.150416137695" />
                  <Point X="-21.8950234375" Y="1.110479003906" />
                  <Point X="-21.88826171875" Y="1.101424072266" />
                  <Point X="-21.8737109375" Y="1.084182617188" />
                  <Point X="-21.865921875" Y="1.075996337891" />
                  <Point X="-21.848677734375" Y="1.059905151367" />
                  <Point X="-21.839966796875" Y="1.052697265625" />
                  <Point X="-21.82175390625" Y="1.039369384766" />
                  <Point X="-21.812251953125" Y="1.033249267578" />
                  <Point X="-21.780685546875" Y="1.015480285645" />
                  <Point X="-21.742609375" Y="0.994046630859" />
                  <Point X="-21.73451953125" Y="0.989987182617" />
                  <Point X="-21.705318359375" Y="0.978083374023" />
                  <Point X="-21.66972265625" Y="0.968020996094" />
                  <Point X="-21.656328125" Y="0.9652578125" />
                  <Point X="-21.6136484375" Y="0.959616821289" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.555966796875" Y="0.952199829102" />
                  <Point X="-21.53428125" Y="0.951222900391" />
                  <Point X="-21.50878125" Y="0.952032287598" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-20.724296875" Y="1.054841064453" />
                  <Point X="-20.295296875" Y="1.111319702148" />
                  <Point X="-20.247310546875" Y="0.914207946777" />
                  <Point X="-20.219001953125" Y="0.732385070801" />
                  <Point X="-20.216126953125" Y="0.713921386719" />
                  <Point X="-20.71874609375" Y="0.579244750977" />
                  <Point X="-21.3080078125" Y="0.42135269165" />
                  <Point X="-21.31396875" Y="0.419543792725" />
                  <Point X="-21.334373046875" Y="0.412138061523" />
                  <Point X="-21.357619140625" Y="0.401619567871" />
                  <Point X="-21.365998046875" Y="0.397315765381" />
                  <Point X="-21.407927734375" Y="0.373079071045" />
                  <Point X="-21.4585078125" Y="0.343843475342" />
                  <Point X="-21.466119140625" Y="0.338944854736" />
                  <Point X="-21.491224609375" Y="0.319871185303" />
                  <Point X="-21.518005859375" Y="0.294350402832" />
                  <Point X="-21.527203125" Y="0.284226196289" />
                  <Point X="-21.552361328125" Y="0.252168548584" />
                  <Point X="-21.582708984375" Y="0.213498657227" />
                  <Point X="-21.589146484375" Y="0.204205856323" />
                  <Point X="-21.60087109375" Y="0.184923812866" />
                  <Point X="-21.606158203125" Y="0.174934432983" />
                  <Point X="-21.615931640625" Y="0.153470016479" />
                  <Point X="-21.619994140625" Y="0.142927886963" />
                  <Point X="-21.62683984375" Y="0.121430328369" />
                  <Point X="-21.629623046875" Y="0.110474594116" />
                  <Point X="-21.638009765625" Y="0.066685081482" />
                  <Point X="-21.648125" Y="0.013864059448" />
                  <Point X="-21.649396484375" Y="0.004966452122" />
                  <Point X="-21.6514140625" Y="-0.026262191772" />
                  <Point X="-21.64971875" Y="-0.062940540314" />
                  <Point X="-21.648125" Y="-0.076424201965" />
                  <Point X="-21.63973828125" Y="-0.120213562012" />
                  <Point X="-21.629623046875" Y="-0.173034881592" />
                  <Point X="-21.62683984375" Y="-0.183990478516" />
                  <Point X="-21.619994140625" Y="-0.205488037109" />
                  <Point X="-21.615931640625" Y="-0.216030151367" />
                  <Point X="-21.606158203125" Y="-0.237494567871" />
                  <Point X="-21.60087109375" Y="-0.247483963013" />
                  <Point X="-21.589146484375" Y="-0.266765991211" />
                  <Point X="-21.582708984375" Y="-0.276058654785" />
                  <Point X="-21.55755078125" Y="-0.308116455078" />
                  <Point X="-21.527203125" Y="-0.346786346436" />
                  <Point X="-21.521279296875" Y="-0.353634735107" />
                  <Point X="-21.498857421875" Y="-0.375807647705" />
                  <Point X="-21.469822265625" Y="-0.398725463867" />
                  <Point X="-21.45850390625" Y="-0.406405090332" />
                  <Point X="-21.416572265625" Y="-0.43064163208" />
                  <Point X="-21.365994140625" Y="-0.459877227783" />
                  <Point X="-21.36050390625" Y="-0.462814483643" />
                  <Point X="-21.340845703125" Y="-0.472014678955" />
                  <Point X="-21.31697265625" Y="-0.481027160645" />
                  <Point X="-21.3080078125" Y="-0.483912689209" />
                  <Point X="-20.59720703125" Y="-0.674371582031" />
                  <Point X="-20.215123046875" Y="-0.776750854492" />
                  <Point X="-20.238384765625" Y="-0.931058044434" />
                  <Point X="-20.272197265625" Y="-1.079219726563" />
                  <Point X="-20.87983984375" Y="-0.999221862793" />
                  <Point X="-21.563216796875" Y="-0.909253601074" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042480469" />
                  <Point X="-21.63327734375" Y="-0.910840881348" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.7278125" Y="-0.930563598633" />
                  <Point X="-21.82708203125" Y="-0.952140075684" />
                  <Point X="-21.842125" Y="-0.956742614746" />
                  <Point X="-21.87124609375" Y="-0.968367614746" />
                  <Point X="-21.88532421875" Y="-0.975390014648" />
                  <Point X="-21.913150390625" Y="-0.992282531738" />
                  <Point X="-21.925875" Y="-1.001530395508" />
                  <Point X="-21.949625" Y="-1.022000854492" />
                  <Point X="-21.960650390625" Y="-1.033223266602" />
                  <Point X="-22.010392578125" Y="-1.093047485352" />
                  <Point X="-22.07039453125" Y="-1.165211181641" />
                  <Point X="-22.078673828125" Y="-1.176850341797" />
                  <Point X="-22.093392578125" Y="-1.201231323242" />
                  <Point X="-22.09983203125" Y="-1.213973388672" />
                  <Point X="-22.11117578125" Y="-1.241358642578" />
                  <Point X="-22.115634765625" Y="-1.254927368164" />
                  <Point X="-22.122466796875" Y="-1.282578857422" />
                  <Point X="-22.12483984375" Y="-1.296661376953" />
                  <Point X="-22.13196875" Y="-1.374136474609" />
                  <Point X="-22.140568359375" Y="-1.467591430664" />
                  <Point X="-22.140708984375" Y="-1.483315307617" />
                  <Point X="-22.138392578125" Y="-1.514580566406" />
                  <Point X="-22.135935546875" Y="-1.530121948242" />
                  <Point X="-22.128205078125" Y="-1.561742919922" />
                  <Point X="-22.12321484375" Y="-1.576666625977" />
                  <Point X="-22.110841796875" Y="-1.605480712891" />
                  <Point X="-22.103458984375" Y="-1.61937109375" />
                  <Point X="-22.057916015625" Y="-1.690210693359" />
                  <Point X="-22.002978515625" Y="-1.775661743164" />
                  <Point X="-21.998255859375" Y="-1.78235534668" />
                  <Point X="-21.98019921875" Y="-1.804456298828" />
                  <Point X="-21.956505859375" Y="-1.828122680664" />
                  <Point X="-21.947201171875" Y="-1.836277832031" />
                  <Point X="-21.28757421875" Y="-2.342427978516" />
                  <Point X="-20.912828125" Y="-2.629980224609" />
                  <Point X="-20.95450390625" Y="-2.697416748047" />
                  <Point X="-20.998724609375" Y="-2.760251220703" />
                  <Point X="-21.542904296875" Y="-2.446069091797" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.18497265625" Y="-2.079513427734" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228892578125" Y="-2.066337646484" />
                  <Point X="-22.3268359375" Y="-2.048649169922" />
                  <Point X="-22.444982421875" Y="-2.027311889648" />
                  <Point X="-22.460640625" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.55515625" Y="-2.03513659668" />
                  <Point X="-22.584931640625" Y="-2.044960571289" />
                  <Point X="-22.59941015625" Y="-2.051109130859" />
                  <Point X="-22.68077734375" Y="-2.093932373047" />
                  <Point X="-22.778927734375" Y="-2.145588134766" />
                  <Point X="-22.79103125" Y="-2.153170654297" />
                  <Point X="-22.813962890625" Y="-2.170064453125" />
                  <Point X="-22.824791015625" Y="-2.179375732422" />
                  <Point X="-22.84575" Y="-2.200335449219" />
                  <Point X="-22.85505859375" Y="-2.211160888672" />
                  <Point X="-22.871951171875" Y="-2.23408984375" />
                  <Point X="-22.87953515625" Y="-2.246193359375" />
                  <Point X="-22.922359375" Y="-2.327561035156" />
                  <Point X="-22.974015625" Y="-2.425711425781" />
                  <Point X="-22.9801640625" Y="-2.440193359375" />
                  <Point X="-22.98998828125" Y="-2.469972167969" />
                  <Point X="-22.9936640625" Y="-2.485269042969" />
                  <Point X="-22.99862109375" Y="-2.517442138672" />
                  <Point X="-22.999720703125" Y="-2.533133544922" />
                  <Point X="-22.99931640625" Y="-2.564484375" />
                  <Point X="-22.9978125" Y="-2.580143798828" />
                  <Point X="-22.980123046875" Y="-2.678087890625" />
                  <Point X="-22.95878515625" Y="-2.796234130859" />
                  <Point X="-22.95698046875" Y="-2.80423046875" />
                  <Point X="-22.94876171875" Y="-2.831540771484" />
                  <Point X="-22.935927734375" Y="-2.862479248047" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.506572265625" Y="-3.607755615234" />
                  <Point X="-22.264103515625" Y="-4.027722900391" />
                  <Point X="-22.2762421875" Y="-4.036082763672" />
                  <Point X="-22.701634765625" Y="-3.481702880859" />
                  <Point X="-23.16608203125" Y="-2.876422607422" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849625732422" />
                  <Point X="-23.21674609375" Y="-2.828004638672" />
                  <Point X="-23.22669921875" Y="-2.820646972656" />
                  <Point X="-23.323298828125" Y="-2.758542480469" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.453716796875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663873535156" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.697251953125" Y="-2.65623828125" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920125" Y="-2.699414794922" />
                  <Point X="-23.944505859375" Y="-2.714135742188" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-24.037716796875" Y="-2.790243164062" />
                  <Point X="-24.136123046875" Y="-2.872063720703" />
                  <Point X="-24.147345703125" Y="-2.883087158203" />
                  <Point X="-24.16781640625" Y="-2.906836425781" />
                  <Point X="-24.177064453125" Y="-2.919562255859" />
                  <Point X="-24.19395703125" Y="-2.947388671875" />
                  <Point X="-24.20098046875" Y="-2.961467041016" />
                  <Point X="-24.21260546875" Y="-2.990588867188" />
                  <Point X="-24.21720703125" Y="-3.005632324219" />
                  <Point X="-24.24159765625" Y="-3.117853027344" />
                  <Point X="-24.271021484375" Y="-3.253220214844" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677001953" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520263672" />
                  <Point X="-24.166908203125" Y="-4.152331542969" />
                  <Point X="-24.344931640625" Y="-3.487936767578" />
                  <Point X="-24.347392578125" Y="-3.480121582031" />
                  <Point X="-24.357853515625" Y="-3.453578613281" />
                  <Point X="-24.3732109375" Y="-3.423815429688" />
                  <Point X="-24.37958984375" Y="-3.413209716797" />
                  <Point X="-24.45380078125" Y="-3.306286376953" />
                  <Point X="-24.543318359375" Y="-3.177309326172" />
                  <Point X="-24.553328125" Y="-3.165172119141" />
                  <Point X="-24.575212890625" Y="-3.142716308594" />
                  <Point X="-24.587087890625" Y="-3.132397705078" />
                  <Point X="-24.61334375" Y="-3.113153808594" />
                  <Point X="-24.6267578125" Y="-3.104936767578" />
                  <Point X="-24.6547578125" Y="-3.090829345703" />
                  <Point X="-24.66934375" Y="-3.084938964844" />
                  <Point X="-24.7841796875" Y="-3.049297851562" />
                  <Point X="-24.922703125" Y="-3.006305664062" />
                  <Point X="-24.936623046875" Y="-3.003109619141" />
                  <Point X="-24.964783203125" Y="-2.998840087891" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.1798203125" Y="-3.041946044922" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.3329296875" Y="-3.090829589844" />
                  <Point X="-25.3609296875" Y="-3.104937255859" />
                  <Point X="-25.37434375" Y="-3.113153808594" />
                  <Point X="-25.400599609375" Y="-3.132397705078" />
                  <Point X="-25.412474609375" Y="-3.142717773438" />
                  <Point X="-25.434359375" Y="-3.165174560547" />
                  <Point X="-25.444369140625" Y="-3.177311279297" />
                  <Point X="-25.518578125" Y="-3.284234375" />
                  <Point X="-25.608095703125" Y="-3.413211669922" />
                  <Point X="-25.61246875" Y="-3.420131835938" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213378906" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.86176953125" Y="-4.305313476563" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.137055667101" Y="-4.69950155076" />
                  <Point X="-25.958070913524" Y="-4.664710439029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.334366749765" Y="-3.960333703454" />
                  <Point X="-22.306174650764" Y="-3.954853714535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.123978762153" Y="-4.600181571913" />
                  <Point X="-25.930714092674" Y="-4.562614725714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.39898887945" Y="-3.876116886978" />
                  <Point X="-22.356411730895" Y="-3.86784072768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.122596269179" Y="-4.503134756481" />
                  <Point X="-25.903357271823" Y="-4.4605190124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.17438164186" Y="-4.124440194956" />
                  <Point X="-24.170675087214" Y="-4.123719713718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.463611009135" Y="-3.791900070503" />
                  <Point X="-22.406648811025" Y="-3.780827740825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.140798091487" Y="-4.409894746307" />
                  <Point X="-25.876000450972" Y="-4.358423299085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.199029412318" Y="-4.032453150176" />
                  <Point X="-24.183098457185" Y="-4.029356486193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.52823313882" Y="-3.707683254028" />
                  <Point X="-22.456885891155" Y="-3.69381475397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.159332454113" Y="-4.316719375423" />
                  <Point X="-25.848643802531" Y="-4.256327619283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.223677182775" Y="-3.940466105397" />
                  <Point X="-24.195521827156" Y="-3.934993258667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.592855268505" Y="-3.623466437552" />
                  <Point X="-22.50712296982" Y="-3.606801766831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.177866628073" Y="-4.223543967866" />
                  <Point X="-25.821287341013" Y="-4.154231975816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.248324953233" Y="-3.848479060617" />
                  <Point X="-24.207945197127" Y="-3.840630031142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.65747739819" Y="-3.539249621077" />
                  <Point X="-22.55735991631" Y="-3.519788753999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.207377393498" Y="-4.132502193552" />
                  <Point X="-25.793930879495" Y="-4.052136332348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.272972723691" Y="-3.756492015837" />
                  <Point X="-24.220368567099" Y="-3.746266803617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.72209942307" Y="-3.455032784229" />
                  <Point X="-22.607596862801" Y="-3.432775741167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.273728829135" Y="-4.048621520102" />
                  <Point X="-25.766574417977" Y="-3.95004068888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.297620494149" Y="-3.664504971058" />
                  <Point X="-24.23279193707" Y="-3.651903576092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.786721221806" Y="-3.370815903424" />
                  <Point X="-22.657833809291" Y="-3.345762728335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.364060885722" Y="-3.969402207166" />
                  <Point X="-25.739217956459" Y="-3.847945045413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.322268264606" Y="-3.572517926278" />
                  <Point X="-24.245215307041" Y="-3.557540348567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.851343020543" Y="-3.286599022619" />
                  <Point X="-22.708070755781" Y="-3.258749715503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.587310503475" Y="-4.110399759996" />
                  <Point X="-27.441490557846" Y="-4.082055233886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.454392591546" Y="-3.890182826048" />
                  <Point X="-25.711861494941" Y="-3.745849401945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.34724364076" Y="-3.480594561595" />
                  <Point X="-24.257638677012" Y="-3.463177121041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.91596481928" Y="-3.202382141813" />
                  <Point X="-22.758307702271" Y="-3.171736702671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.706267378661" Y="-4.036744548148" />
                  <Point X="-27.311136715399" Y="-3.959938927674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.565159986004" Y="-3.814935740404" />
                  <Point X="-25.684505033423" Y="-3.643753758478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.393720232955" Y="-3.392850609933" />
                  <Point X="-24.270062046983" Y="-3.368813893516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.980586618017" Y="-3.118165261008" />
                  <Point X="-22.808544648761" Y="-3.08472368984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.03412675721" Y="-2.73981179154" />
                  <Point X="-20.976449983639" Y="-2.728600562464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.813085403253" Y="-3.960729782769" />
                  <Point X="-27.106879247403" Y="-3.82345721188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.883363222421" Y="-3.780010097847" />
                  <Point X="-25.657148571905" Y="-3.54165811501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.452905088076" Y="-3.307576894347" />
                  <Point X="-24.273566036626" Y="-3.272716914085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.045208416754" Y="-3.033948380203" />
                  <Point X="-22.858781595251" Y="-2.997710677008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.159530785049" Y="-2.667409779218" />
                  <Point X="-20.923771788079" Y="-2.621582872505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.91344181426" Y="-3.883459006944" />
                  <Point X="-25.622109261147" Y="-3.438069076933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.512089999934" Y="-3.222303189789" />
                  <Point X="-24.253370062343" Y="-3.17201312834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.109830215491" Y="-2.949731499398" />
                  <Point X="-22.909018541741" Y="-2.910697664176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.284934812888" Y="-2.595007766896" />
                  <Point X="-21.024403480005" Y="-2.54436560587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.960208660667" Y="-3.795771474985" />
                  <Point X="-25.548208657962" Y="-3.326926168819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.579841205128" Y="-3.138694603979" />
                  <Point X="-24.231406955184" Y="-3.07096584676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.175602154454" Y="-2.865738183205" />
                  <Point X="-22.951575207206" Y="-2.822191755944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.410338840727" Y="-2.522605754573" />
                  <Point X="-21.125035171931" Y="-2.467148339235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.897270661337" Y="-3.686759481198" />
                  <Point X="-25.470565341644" Y="-3.21505575097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.720111708897" Y="-3.069182341843" />
                  <Point X="-24.203926044586" Y="-2.968846012842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.27656818187" Y="-2.788585904806" />
                  <Point X="-22.970897409746" Y="-2.729169525626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.535742868566" Y="-2.450203742251" />
                  <Point X="-21.225666863857" Y="-2.3899310726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.834332662008" Y="-3.577747487411" />
                  <Point X="-25.335173606251" Y="-3.091960177569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.91184928677" Y="-3.009674265483" />
                  <Point X="-24.115442077741" Y="-2.854868385992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.392153983315" Y="-2.714275422602" />
                  <Point X="-22.987783367803" Y="-2.635673737353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.661146949787" Y="-2.377801740305" />
                  <Point X="-21.326298461583" Y="-2.312713787654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.771394662678" Y="-3.468735493624" />
                  <Point X="-23.96353404854" Y="-2.728562370295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.550431749242" Y="-2.648263417652" />
                  <Point X="-22.99961673285" Y="-2.541195824488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.78655103424" Y="-2.305399738988" />
                  <Point X="-21.426929908715" Y="-2.235496473436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.708456663349" Y="-3.359723499837" />
                  <Point X="-22.980320282789" Y="-2.440666888539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.911955118694" Y="-2.23299773767" />
                  <Point X="-21.527561355846" Y="-2.158279159217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.645518555553" Y="-3.250711484967" />
                  <Point X="-22.925326687271" Y="-2.333199130421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.037359203148" Y="-2.160595736353" />
                  <Point X="-21.628192802977" Y="-2.081061844999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.582580351781" Y="-3.14169945144" />
                  <Point X="-22.865034375427" Y="-2.224701406185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.164308863747" Y="-2.088494164604" />
                  <Point X="-21.728824250108" Y="-2.00384453078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.51964214801" Y="-3.032687417914" />
                  <Point X="-22.675278988082" Y="-2.091038609312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.394415523601" Y="-2.03644428226" />
                  <Point X="-21.829455697239" Y="-1.926627216562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.456703944238" Y="-2.923675384388" />
                  <Point X="-21.930087144371" Y="-1.849409902344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.393765740466" Y="-2.814663350861" />
                  <Point X="-22.008042673198" Y="-1.767784836115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.811535679156" Y="-2.993471823809" />
                  <Point X="-28.720439201486" Y="-2.975764462318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.333368239537" Y="-2.706145179939" />
                  <Point X="-22.06335058416" Y="-1.681757518925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.87570508157" Y="-2.909167006067" />
                  <Point X="-28.467735532097" Y="-2.829865758921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.311173701411" Y="-2.605052912736" />
                  <Point X="-22.11530838964" Y="-1.595079007196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.939874481488" Y="-2.824862187839" />
                  <Point X="-28.215031044036" Y="-2.683966896391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.330143534822" Y="-2.511962188798" />
                  <Point X="-22.139253869259" Y="-1.502955450906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.004039914781" Y="-2.740556598578" />
                  <Point X="-27.962326203326" Y="-2.538067965312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.387672906055" Y="-2.426366679742" />
                  <Point X="-22.13483813035" Y="-1.405319032191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.382497101387" Y="-1.064698441266" />
                  <Point X="-20.263609429088" Y="-1.041589018772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.055758860735" Y="-2.65383165726" />
                  <Point X="-27.709621362617" Y="-2.392169034234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.513662211493" Y="-2.35407843386" />
                  <Point X="-22.125770768943" Y="-1.306778429657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.679332223414" Y="-1.025619258028" />
                  <Point X="-20.240498138951" Y="-0.94031855303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.107477806689" Y="-2.567106715942" />
                  <Point X="-22.094779812055" Y="-1.203976311856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.976167452095" Y="-0.986540095521" />
                  <Point X="-20.224729477302" Y="-0.840475349683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.159196281411" Y="-2.480381683026" />
                  <Point X="-22.008158655641" Y="-1.090360778673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.27300290278" Y="-0.947460976168" />
                  <Point X="-20.290655446509" Y="-0.756511973937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.084432288416" Y="-2.369070948934" />
                  <Point X="-21.86442027952" Y="-0.965642782667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.570861645776" Y="-0.90858076469" />
                  <Point X="-20.499982319441" Y="-0.700422910188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.915520123552" Y="-2.23945966409" />
                  <Point X="-20.709309169356" Y="-0.644333841964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.746607958689" Y="-2.109848379246" />
                  <Point X="-20.918635999309" Y="-0.588244769861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.5776954976" Y="-1.980237036822" />
                  <Point X="-21.127962829262" Y="-0.532155697757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.408782509806" Y="-1.850625592016" />
                  <Point X="-21.332551956485" Y="-0.475145709532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.239869522012" Y="-1.721014147211" />
                  <Point X="-21.462576451829" Y="-0.403641825112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.070956534219" Y="-1.591402702405" />
                  <Point X="-21.54583329265" Y="-0.323047229548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.974854894551" Y="-1.475944349957" />
                  <Point X="-21.605917988958" Y="-0.23794842537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.949283815906" Y="-1.374195749765" />
                  <Point X="-21.634655367406" Y="-0.146756319855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.959397793821" Y="-1.279383621898" />
                  <Point X="-21.650178415376" Y="-0.052995608698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.011839038663" Y="-1.192799081261" />
                  <Point X="-21.642094711401" Y="0.045353790201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.123199123652" Y="-1.117667202986" />
                  <Point X="-21.618536214632" Y="0.146711184106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.13328637824" Y="-1.217230189768" />
                  <Point X="-21.548486929075" Y="0.257105471908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.676415429263" Y="-1.226025696587" />
                  <Point X="-21.386745943117" Y="0.38532282078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.699966897298" Y="-1.133825552203" />
                  <Point X="-20.27423807953" Y="0.698350529243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.723518365333" Y="-1.04162540782" />
                  <Point X="-20.230106290505" Y="0.803706966057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.744369433817" Y="-0.948900358937" />
                  <Point X="-20.244731375647" Y="0.897642223507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.757835785292" Y="-0.854739866479" />
                  <Point X="-20.265839181259" Y="0.990317367747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.771302136766" Y="-0.760579374021" />
                  <Point X="-20.288335013236" Y="1.082722706994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.784768662898" Y="-0.666418915512" />
                  <Point X="-21.360658271213" Y="0.971062266634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.635334372329" Y="-0.346213436757" />
                  <Point X="-21.741962912218" Y="0.99372223866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.342482819126" Y="-0.192510775293" />
                  <Point X="-21.85741542062" Y="1.068058630407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.301495548769" Y="-0.08776557099" />
                  <Point X="-21.922465460222" Y="1.15219226962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.297936348279" Y="0.009704353522" />
                  <Point X="-21.969158217995" Y="1.239894202951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.326933365152" Y="0.100845990439" />
                  <Point X="-21.991137385722" Y="1.332399971554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.393504407306" Y="0.184683976706" />
                  <Point X="-21.997648987205" Y="1.427912330466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.528920385744" Y="0.255139862976" />
                  <Point X="-21.973580250439" Y="1.52936890498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.738247415362" Y="0.311228896269" />
                  <Point X="-21.919539600168" Y="1.636651429306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.94757444498" Y="0.367317929562" />
                  <Point X="-21.813802471782" Y="1.75398273103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.156901474597" Y="0.423406962854" />
                  <Point X="-21.644889496931" Y="1.883594173319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.366228535418" Y="0.479495990082" />
                  <Point X="-21.47597652208" Y="2.013205615609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.575555704894" Y="0.535584996189" />
                  <Point X="-21.307063568989" Y="2.142817053669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.784882874371" Y="0.591674002296" />
                  <Point X="-22.636361008044" Y="1.98120589255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.347562042192" Y="2.037342724811" />
                  <Point X="-21.138151227445" Y="2.272428372856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.770740276092" Y="0.691201130942" />
                  <Point X="-22.800839662279" Y="2.046012566914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.094858097961" Y="2.183241481632" />
                  <Point X="-20.9692388859" Y="2.402039692044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.755995734011" Y="0.790845265611" />
                  <Point X="-22.891361118327" Y="2.125195064325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.842154153729" Y="2.329140238453" />
                  <Point X="-20.882593034744" Y="2.515660025398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.74125119193" Y="0.890489400279" />
                  <Point X="-22.9479995953" Y="2.210963745683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.589450209498" Y="2.475038995274" />
                  <Point X="-20.934993005366" Y="2.60225258893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.722272246576" Y="0.990956619565" />
                  <Point X="-22.97841883711" Y="2.301828930076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.336746160005" Y="2.620937772555" />
                  <Point X="-20.992594263827" Y="2.687834124525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.694588968241" Y="1.093115789786" />
                  <Point X="-28.979469063926" Y="1.232121017858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.488563814088" Y="1.327543332079" />
                  <Point X="-22.986913829186" Y="2.396955756911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.084042089868" Y="2.766836553849" />
                  <Point X="-21.053685761226" Y="2.772737226395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.666905689907" Y="1.195274960008" />
                  <Point X="-29.276304967552" Y="1.271200049169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.437656733433" Y="1.434216752174" />
                  <Point X="-22.970372334442" Y="2.496949183794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.639223050402" Y="1.297434006053" />
                  <Point X="-29.573140188131" Y="1.31027921325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.421515706027" Y="1.534132336092" />
                  <Point X="-22.933344043017" Y="2.600924840549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.444685352158" Y="1.626406699135" />
                  <Point X="-22.87040606007" Y="2.709936831151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.4925513842" Y="1.71388057105" />
                  <Point X="-22.807468077122" Y="2.818948821754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.57616460228" Y="1.794405893893" />
                  <Point X="-22.744530094174" Y="2.927960812357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.676795947707" Y="1.871623227881" />
                  <Point X="-22.681592111227" Y="3.036972802959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.777427293133" Y="1.948840561868" />
                  <Point X="-22.618654128279" Y="3.145984793562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.878058638559" Y="2.026057895856" />
                  <Point X="-22.555716145332" Y="3.254996784164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.978689983986" Y="2.103275229844" />
                  <Point X="-22.492777947319" Y="3.364008816571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.079321331359" Y="2.180492563453" />
                  <Point X="-22.429839700888" Y="3.47302085839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.179952679597" Y="2.257709896895" />
                  <Point X="-22.366901454457" Y="3.582032900208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.194074060248" Y="2.351743064579" />
                  <Point X="-22.303963208026" Y="3.691044942027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.130356543575" Y="2.460906581188" />
                  <Point X="-22.241024961595" Y="3.800056983846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.066638670263" Y="2.570070167121" />
                  <Point X="-28.195932206255" Y="2.739318358763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.869106427616" Y="2.802846854649" />
                  <Point X="-22.189230259446" Y="3.906902940082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.002920796952" Y="2.679233753054" />
                  <Point X="-28.334987743341" Y="2.809066786498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.773126300219" Y="2.918281587504" />
                  <Point X="-22.296105265042" Y="3.982906629476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.914462855056" Y="2.793206321166" />
                  <Point X="-28.460391682308" Y="2.881468816095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.748767958294" Y="3.019794455559" />
                  <Point X="-22.408342598864" Y="4.057867987851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.825755622768" Y="2.907227346421" />
                  <Point X="-28.585795508901" Y="2.953870867535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.755002964907" Y="3.115360579066" />
                  <Point X="-22.532899099686" Y="4.130434742737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.737046897998" Y="3.021248661786" />
                  <Point X="-28.711199051546" Y="3.02627297417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.77754402312" Y="3.207757127223" />
                  <Point X="-22.657455798088" Y="4.203001459217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.825709265487" Y="3.295172838543" />
                  <Point X="-22.787649253767" Y="4.274472501076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.875946189552" Y="3.382185855734" />
                  <Point X="-22.933828794921" Y="4.342836162697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.926183113617" Y="3.469198872925" />
                  <Point X="-23.080009150636" Y="4.411199665984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.976420037682" Y="3.556211890116" />
                  <Point X="-25.106423301145" Y="4.114082742988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.811082536314" Y="4.171491172157" />
                  <Point X="-23.242098562202" Y="4.476470762077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.026656779326" Y="3.643224942766" />
                  <Point X="-25.192611477023" Y="4.194107544738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.757474520638" Y="4.278689600837" />
                  <Point X="-23.415820170048" Y="4.539480788261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.000458501177" Y="3.745095458192" />
                  <Point X="-25.231457504782" Y="4.283334727874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.730117823226" Y="4.380785290158" />
                  <Point X="-23.601547402172" Y="4.600157157486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.831358445973" Y="3.874743265218" />
                  <Point X="-26.930591906918" Y="4.049834543541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.595165604178" Y="4.11503481196" />
                  <Point X="-25.256104979634" Y="4.375321830113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.702761125815" Y="4.482880979478" />
                  <Point X="-23.823610030596" Y="4.653770641145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.662257836692" Y="4.004391179946" />
                  <Point X="-27.063435314633" Y="4.120790486903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.508119865554" Y="4.228732875564" />
                  <Point X="-25.280752454487" Y="4.467308932353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.675404428403" Y="4.584976668799" />
                  <Point X="-24.045673890059" Y="4.707383885515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.402439652937" Y="4.151672804845" />
                  <Point X="-27.141897621948" Y="4.202317045372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.475052468614" Y="4.331938612424" />
                  <Point X="-25.30539992934" Y="4.559296034592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.648047851108" Y="4.687072334771" />
                  <Point X="-24.311439373686" Y="4.752502394671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.462713052465" Y="4.431115237971" />
                  <Point X="-25.33004776854" Y="4.651283066009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.474388505896" Y="4.525623845744" />
                  <Point X="-25.354695778201" Y="4.743270064293" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.34689453125" Y="-4.214725097656" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.609890625" Y="-3.414620849609" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.8405" Y="-3.230758789062" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.1235" Y="-3.223407470703" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643798828" />
                  <Point X="-25.36248828125" Y="-3.392566894531" />
                  <Point X="-25.452005859375" Y="-3.521544189453" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.6782421875" Y="-4.354489257813" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.93783984375" Y="-4.969588867188" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.2328125" Y="-4.90395703125" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.331005859375" Y="-4.717053710938" />
                  <Point X="-26.3091484375" Y="-4.5510390625" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.3371171875" Y="-4.396835449219" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.492009765625" Y="-4.109908691406" />
                  <Point X="-26.61954296875" Y="-3.998064208984" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.789564453125" Y="-3.976565673828" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791015625" />
                  <Point X="-27.106802734375" Y="-4.051917724609" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.382693359375" Y="-4.3175390625" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.617779296875" Y="-4.315007324219" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.039400390625" Y="-4.026270019531" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.843453125" Y="-3.213547363281" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597594238281" />
                  <Point X="-27.5139765625" Y="-2.568765625" />
                  <Point X="-27.53132421875" Y="-2.551416992188" />
                  <Point X="-27.560154296875" Y="-2.537198974609" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.296654296875" Y="-2.950485107422" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-28.97362890625" Y="-3.094220458984" />
                  <Point X="-29.16169921875" Y="-2.847135742188" />
                  <Point X="-29.293306640625" Y="-2.626450683594" />
                  <Point X="-29.43101953125" Y="-2.395526855469" />
                  <Point X="-28.754162109375" Y="-1.876155395508" />
                  <Point X="-28.163787109375" Y="-1.423144897461" />
                  <Point X="-28.145822265625" Y="-1.396014770508" />
                  <Point X="-28.1381171875" Y="-1.366267578125" />
                  <Point X="-28.14032421875" Y="-1.334596679688" />
                  <Point X="-28.161158203125" Y="-1.310639770508" />
                  <Point X="-28.187640625" Y="-1.295053222656" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.109486328125" Y="-1.405736450195" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.853833984375" Y="-1.299165161133" />
                  <Point X="-29.927392578125" Y="-1.011187744141" />
                  <Point X="-29.9622109375" Y="-0.767741210938" />
                  <Point X="-29.99839453125" Y="-0.514742248535" />
                  <Point X="-29.22998828125" Y="-0.308848114014" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.529314453125" Y="-0.112694488525" />
                  <Point X="-28.502322265625" Y="-0.090643913269" />
                  <Point X="-28.490703125" Y="-0.062394203186" />
                  <Point X="-28.483400390625" Y="-0.031280038834" />
                  <Point X="-28.48983984375" Y="-0.002952492237" />
                  <Point X="-28.502322265625" Y="0.028083814621" />
                  <Point X="-28.526720703125" Y="0.048333656311" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.368703125" Y="0.283456573486" />
                  <Point X="-29.998185546875" Y="0.452126037598" />
                  <Point X="-29.96505078125" Y="0.676050415039" />
                  <Point X="-29.91764453125" Y="0.996414916992" />
                  <Point X="-29.8475546875" Y="1.255071411133" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.23534765625" Y="1.457447631836" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866699219" />
                  <Point X="-28.703861328125" Y="1.404645141602" />
                  <Point X="-28.67027734375" Y="1.41523425293" />
                  <Point X="-28.651533203125" Y="1.426056762695" />
                  <Point X="-28.6391171875" Y="1.443787353516" />
                  <Point X="-28.6279453125" Y="1.470758056641" />
                  <Point X="-28.614470703125" Y="1.503291625977" />
                  <Point X="-28.61071484375" Y="1.524607055664" />
                  <Point X="-28.61631640625" Y="1.545513427734" />
                  <Point X="-28.629794921875" Y="1.571407958984" />
                  <Point X="-28.646056640625" Y="1.602643310547" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-29.125296875" Y="1.976281616211" />
                  <Point X="-29.47610546875" Y="2.245466308594" />
                  <Point X="-29.34421875" Y="2.471420410156" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-28.974349609375" Y="3.025652587891" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.45523828125" Y="3.097886474609" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.09973828125" Y="2.917042480469" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775146484" />
                  <Point X="-28.013251953125" Y="2.927404052734" />
                  <Point X="-27.985728515625" Y="2.954927246094" />
                  <Point X="-27.95252734375" Y="2.988127441406" />
                  <Point X="-27.9408984375" Y="3.006381835938" />
                  <Point X="-27.938072265625" Y="3.027840820312" />
                  <Point X="-27.94146484375" Y="3.066616699219" />
                  <Point X="-27.945556640625" Y="3.113390380859" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.158267578125" Y="3.491184326172" />
                  <Point X="-28.30727734375" Y="3.749276611328" />
                  <Point X="-28.073701171875" Y="3.928358154297" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.460458984375" Y="4.336791503906" />
                  <Point X="-27.141548828125" Y="4.513971679688" />
                  <Point X="-27.058466796875" Y="4.405696777344" />
                  <Point X="-26.967826171875" Y="4.287572753906" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.90808984375" Y="4.251194335938" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.83512109375" Y="4.218491699219" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.76885546875" Y="4.240870117188" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.69690234375" Y="4.275744628906" />
                  <Point X="-26.68608203125" Y="4.294488769531" />
                  <Point X="-26.671451171875" Y="4.340891601563" />
                  <Point X="-26.653802734375" Y="4.396865234375" />
                  <Point X="-26.651916015625" Y="4.418426269531" />
                  <Point X="-26.675359375" Y="4.5964921875" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.383419921875" Y="4.786853027344" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.613595703125" Y="4.94478515625" />
                  <Point X="-25.22419921875" Y="4.990358398438" />
                  <Point X="-25.129728515625" Y="4.637791015625" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.839890625" Y="4.705214355469" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.502435546875" Y="4.963543945312" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.8465078125" Y="4.8547578125" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.30048046875" Y="4.699758789062" />
                  <Point X="-23.068970703125" Y="4.615788085938" />
                  <Point X="-22.884373046875" Y="4.529458496094" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.482974609375" Y="4.321241699219" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-22.09929296875" Y="4.076089355469" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.379607421875" Y="3.180029296875" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491514648438" />
                  <Point X="-22.78487890625" Y="2.455124511719" />
                  <Point X="-22.7966171875" Y="2.411228271484" />
                  <Point X="-22.797955078125" Y="2.392326904297" />
                  <Point X="-22.79416015625" Y="2.360859863281" />
                  <Point X="-22.789583984375" Y="2.322902099609" />
                  <Point X="-22.78131640625" Y="2.300811279297" />
                  <Point X="-22.761845703125" Y="2.272116210938" />
                  <Point X="-22.738357421875" Y="2.237502685547" />
                  <Point X="-22.72505859375" Y="2.224203857422" />
                  <Point X="-22.69636328125" Y="2.204732910156" />
                  <Point X="-22.66175" Y="2.181246337891" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.6081953125" Y="2.169185791016" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5513359375" Y="2.165946289062" />
                  <Point X="-22.5149453125" Y="2.175677734375" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.643013671875" Y="2.663507324219" />
                  <Point X="-21.005751953125" Y="3.031430664062" />
                  <Point X="-20.925234375" Y="2.919530273438" />
                  <Point X="-20.79740234375" Y="2.741874267578" />
                  <Point X="-20.7036484375" Y="2.586942138672" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.197203125" Y="1.987626831055" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583832885742" />
                  <Point X="-21.746818359375" Y="1.549666015625" />
                  <Point X="-21.77841015625" Y="1.508451782227" />
                  <Point X="-21.78687890625" Y="1.491501586914" />
                  <Point X="-21.796634765625" Y="1.456616943359" />
                  <Point X="-21.808404296875" Y="1.414537109375" />
                  <Point X="-21.809220703125" Y="1.390965698242" />
                  <Point X="-21.8012109375" Y="1.352151977539" />
                  <Point X="-21.79155078125" Y="1.305332763672" />
                  <Point X="-21.784353515625" Y="1.287956298828" />
                  <Point X="-21.7625703125" Y="1.254848022461" />
                  <Point X="-21.736296875" Y="1.214910888672" />
                  <Point X="-21.719052734375" Y="1.198819946289" />
                  <Point X="-21.687486328125" Y="1.181051147461" />
                  <Point X="-21.64941015625" Y="1.159617553711" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.58875390625" Y="1.147978881836" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.74909765625" Y="1.243215576172" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.115712890625" Y="1.176901000977" />
                  <Point X="-20.060806640625" Y="0.951366577148" />
                  <Point X="-20.031263671875" Y="0.761614746094" />
                  <Point X="-20.002140625" Y="0.574556396484" />
                  <Point X="-20.6695703125" Y="0.395718841553" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819503784" />
                  <Point X="-21.312841796875" Y="0.208582809448" />
                  <Point X="-21.363421875" Y="0.179347137451" />
                  <Point X="-21.377734375" Y="0.166926696777" />
                  <Point X="-21.402892578125" Y="0.134868942261" />
                  <Point X="-21.433240234375" Y="0.096199020386" />
                  <Point X="-21.443013671875" Y="0.074734649658" />
                  <Point X="-21.451400390625" Y="0.03094524765" />
                  <Point X="-21.461515625" Y="-0.021875907898" />
                  <Point X="-21.461515625" Y="-0.040684169769" />
                  <Point X="-21.45312890625" Y="-0.084473419189" />
                  <Point X="-21.443013671875" Y="-0.137294723511" />
                  <Point X="-21.433240234375" Y="-0.158759094238" />
                  <Point X="-21.40808203125" Y="-0.190816833496" />
                  <Point X="-21.377734375" Y="-0.22948677063" />
                  <Point X="-21.363421875" Y="-0.24190675354" />
                  <Point X="-21.321490234375" Y="-0.266143432617" />
                  <Point X="-21.270912109375" Y="-0.295379119873" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.54803125" Y="-0.49084564209" />
                  <Point X="-20.001931640625" Y="-0.637172424316" />
                  <Point X="-20.0209140625" Y="-0.763077148438" />
                  <Point X="-20.051568359375" Y="-0.966412475586" />
                  <Point X="-20.089419921875" Y="-1.132279541016" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-20.904640625" Y="-1.187596435547" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.68745703125" Y="-1.116228393555" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.814552734375" Y="-1.154697265625" />
                  <Point X="-21.864294921875" Y="-1.214521728516" />
                  <Point X="-21.924296875" Y="-1.286685424805" />
                  <Point X="-21.935640625" Y="-1.314070678711" />
                  <Point X="-21.94276953125" Y="-1.391545898438" />
                  <Point X="-21.951369140625" Y="-1.485000976562" />
                  <Point X="-21.943638671875" Y="-1.516622070312" />
                  <Point X="-21.898095703125" Y="-1.587461669922" />
                  <Point X="-21.843158203125" Y="-1.672912719727" />
                  <Point X="-21.831537109375" Y="-1.685540771484" />
                  <Point X="-21.17191015625" Y="-2.191691162109" />
                  <Point X="-20.660923828125" Y="-2.583784179688" />
                  <Point X="-20.709455078125" Y="-2.662314453125" />
                  <Point X="-20.7958671875" Y="-2.802139160156" />
                  <Point X="-20.874150390625" Y="-2.913371826172" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.637904296875" Y="-2.610614013672" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.3606015625" Y="-2.235624267578" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.5922890625" Y="-2.262068115234" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.33468359375" />
                  <Point X="-22.75422265625" Y="-2.416051269531" />
                  <Point X="-22.80587890625" Y="-2.514201660156" />
                  <Point X="-22.8108359375" Y="-2.546374755859" />
                  <Point X="-22.793146484375" Y="-2.644318847656" />
                  <Point X="-22.77180859375" Y="-2.762465087891" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.342029296875" Y="-3.512755615234" />
                  <Point X="-22.01332421875" Y="-4.082088867188" />
                  <Point X="-22.0621640625" Y="-4.116974121094" />
                  <Point X="-22.164685546875" Y="-4.190202148438" />
                  <Point X="-22.2522265625" Y="-4.246866210938" />
                  <Point X="-22.32022265625" Y="-4.29087890625" />
                  <Point X="-22.852373046875" Y="-3.597367675781" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.426048828125" Y="-2.918362792969" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.679841796875" Y="-2.845438964844" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.916244140625" Y="-2.936338867188" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045985839844" />
                  <Point X="-24.05593359375" Y="-3.158206542969" />
                  <Point X="-24.085357421875" Y="-3.293573730469" />
                  <Point X="-24.0860703125" Y="-3.310719970703" />
                  <Point X="-23.9659453125" Y="-4.223149414062" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.908646484375" Y="-4.941983398438" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.08651171875" Y="-4.977936035156" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#162" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.087589240046" Y="4.682011984302" Z="1.15" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.15" />
                  <Point X="-0.625565786334" Y="5.025726147262" Z="1.15" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.15" />
                  <Point X="-1.403075624132" Y="4.866276683211" Z="1.15" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.15" />
                  <Point X="-1.731088682022" Y="4.621246401114" Z="1.15" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.15" />
                  <Point X="-1.725294383833" Y="4.38720683628" Z="1.15" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.15" />
                  <Point X="-1.794147891476" Y="4.318344114707" Z="1.15" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.15" />
                  <Point X="-1.891157919395" Y="4.326824904047" Z="1.15" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.15" />
                  <Point X="-2.02495484578" Y="4.467415259401" Z="1.15" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.15" />
                  <Point X="-2.490898777771" Y="4.411779151341" Z="1.15" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.15" />
                  <Point X="-3.1102790809" Y="3.999318244864" Z="1.15" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.15" />
                  <Point X="-3.207726253463" Y="3.497464479469" Z="1.15" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.15" />
                  <Point X="-2.997432485308" Y="3.093539306272" Z="1.15" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.15" />
                  <Point X="-3.02724025057" Y="3.021563317317" Z="1.15" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.15" />
                  <Point X="-3.101537159334" Y="2.998132213865" Z="1.15" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.15" />
                  <Point X="-3.436394868828" Y="3.172467695282" Z="1.15" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.15" />
                  <Point X="-4.019969030777" Y="3.087634909834" Z="1.15" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.15" />
                  <Point X="-4.393556798386" Y="2.527905477937" Z="1.15" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.15" />
                  <Point X="-4.161891733669" Y="1.967893921913" Z="1.15" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.15" />
                  <Point X="-3.680302199361" Y="1.579598773737" Z="1.15" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.15" />
                  <Point X="-3.680298294316" Y="1.521170772872" Z="1.15" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.15" />
                  <Point X="-3.725054299057" Y="1.483611002958" Z="1.15" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.15" />
                  <Point X="-4.234978979452" Y="1.538299972978" Z="1.15" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.15" />
                  <Point X="-4.901971065093" Y="1.299428578334" Z="1.15" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.15" />
                  <Point X="-5.020668846551" Y="0.714649400672" Z="1.15" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.15" />
                  <Point X="-4.387801197235" Y="0.266440261951" Z="1.15" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.15" />
                  <Point X="-3.561387014679" Y="0.038537603163" Z="1.15" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.15" />
                  <Point X="-3.54374986551" Y="0.013510176796" Z="1.15" />
                  <Point X="-3.539556741714" Y="0" Z="1.15" />
                  <Point X="-3.544614735625" Y="-0.016296774265" Z="1.15" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.15" />
                  <Point X="-3.563981580444" Y="-0.040338379971" Z="1.15" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.15" />
                  <Point X="-4.249086591538" Y="-0.229271794805" Z="1.15" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.15" />
                  <Point X="-5.017863993632" Y="-0.743540257442" Z="1.15" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.15" />
                  <Point X="-4.908436621557" Y="-1.28025932209" Z="1.15" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.15" />
                  <Point X="-4.109118534658" Y="-1.42402869573" Z="1.15" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.15" />
                  <Point X="-3.204679692677" Y="-1.31538513216" Z="1.15" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.15" />
                  <Point X="-3.196888693617" Y="-1.338714000332" Z="1.15" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.15" />
                  <Point X="-3.790755601627" Y="-1.805207667793" Z="1.15" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.15" />
                  <Point X="-4.342406211938" Y="-2.62078031245" Z="1.15" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.15" />
                  <Point X="-4.019571260284" Y="-3.093223691376" Z="1.15" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.15" />
                  <Point X="-3.277810989185" Y="-2.962506444866" Z="1.15" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.15" />
                  <Point X="-2.563354293456" Y="-2.564976175676" Z="1.15" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.15" />
                  <Point X="-2.892910568303" Y="-3.157267393307" Z="1.15" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.15" />
                  <Point X="-3.076061385528" Y="-4.034607213638" Z="1.15" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.15" />
                  <Point X="-2.65025925522" Y="-4.326238096227" Z="1.15" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.15" />
                  <Point X="-2.349182549246" Y="-4.316697069993" Z="1.15" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.15" />
                  <Point X="-2.085180738284" Y="-4.062211097272" Z="1.15" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.15" />
                  <Point X="-1.798989960659" Y="-3.995178661957" Z="1.15" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.15" />
                  <Point X="-1.531132847586" Y="-4.116220404554" Z="1.15" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.15" />
                  <Point X="-1.392312957069" Y="-4.375310207644" Z="1.15" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.15" />
                  <Point X="-1.386734768903" Y="-4.679246736002" Z="1.15" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.15" />
                  <Point X="-1.251428347995" Y="-4.92109971651" Z="1.15" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.15" />
                  <Point X="-0.95352417825" Y="-4.987392803613" Z="1.15" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.15" />
                  <Point X="-0.636102237626" Y="-4.336149952577" Z="1.15" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.15" />
                  <Point X="-0.327569627717" Y="-3.389795928804" Z="1.15" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.15" />
                  <Point X="-0.114836500658" Y="-3.23988038038" Z="1.15" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.15" />
                  <Point X="0.138522578703" Y="-3.247231664908" Z="1.15" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.15" />
                  <Point X="0.342876231715" Y="-3.411849826964" Z="1.15" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.15" />
                  <Point X="0.598652622775" Y="-4.196386077239" Z="1.15" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.15" />
                  <Point X="0.916269045352" Y="-4.995850754123" Z="1.15" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.15" />
                  <Point X="1.095900759719" Y="-4.95954381364" Z="1.15" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.15" />
                  <Point X="1.077469391407" Y="-4.185342972699" Z="1.15" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.15" />
                  <Point X="0.986768397081" Y="-3.137546593711" Z="1.15" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.15" />
                  <Point X="1.109565059097" Y="-2.943505447159" Z="1.15" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.15" />
                  <Point X="1.318582529136" Y="-2.863948407564" Z="1.15" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.15" />
                  <Point X="1.540754437526" Y="-2.929140742709" Z="1.15" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.15" />
                  <Point X="2.101801955131" Y="-3.596525639874" Z="1.15" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.15" />
                  <Point X="2.768785828473" Y="-4.257560419713" Z="1.15" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.15" />
                  <Point X="2.960738896786" Y="-4.126381136638" Z="1.15" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.15" />
                  <Point X="2.69511430578" Y="-3.4564755239" Z="1.15" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.15" />
                  <Point X="2.249899780098" Y="-2.604152451245" Z="1.15" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.15" />
                  <Point X="2.283866993571" Y="-2.408057784782" Z="1.15" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.15" />
                  <Point X="2.42484059359" Y="-2.275034316396" Z="1.15" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.15" />
                  <Point X="2.624354352267" Y="-2.253548229393" Z="1.15" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.15" />
                  <Point X="3.330938073207" Y="-2.622635194495" Z="1.15" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.15" />
                  <Point X="4.160580471297" Y="-2.910869283072" Z="1.15" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.15" />
                  <Point X="4.326920440091" Y="-2.657319907355" Z="1.15" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.15" />
                  <Point X="3.852370824581" Y="-2.120743463248" Z="1.15" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.15" />
                  <Point X="3.137805804721" Y="-1.52914210515" Z="1.15" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.15" />
                  <Point X="3.100862217177" Y="-1.364847385313" Z="1.15" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.15" />
                  <Point X="3.167993379489" Y="-1.215208546291" Z="1.15" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.15" />
                  <Point X="3.317004753485" Y="-1.133807606309" Z="1.15" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.15" />
                  <Point X="4.082676917651" Y="-1.205888714104" Z="1.15" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.15" />
                  <Point X="4.953169245067" Y="-1.112123391744" Z="1.15" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.15" />
                  <Point X="5.022371380808" Y="-0.739250782891" Z="1.15" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.15" />
                  <Point X="4.45875414032" Y="-0.411269300136" Z="1.15" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.15" />
                  <Point X="3.697373412327" Y="-0.191574902187" Z="1.15" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.15" />
                  <Point X="3.62509514229" Y="-0.128668268591" Z="1.15" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.15" />
                  <Point X="3.589820866242" Y="-0.043789294484" Z="1.15" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.15" />
                  <Point X="3.591550584182" Y="0.05282123673" Z="1.15" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.15" />
                  <Point X="3.63028429611" Y="0.135280469984" Z="1.15" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.15" />
                  <Point X="3.706022002026" Y="0.196573942055" Z="1.15" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.15" />
                  <Point X="4.337213676987" Y="0.378702632633" Z="1.15" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.15" />
                  <Point X="5.011983461319" Y="0.800586742641" Z="1.15" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.15" />
                  <Point X="4.926712717109" Y="1.220007811904" Z="1.15" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.15" />
                  <Point X="4.238220852428" Y="1.324067891471" Z="1.15" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.15" />
                  <Point X="3.41163955611" Y="1.228828037834" Z="1.15" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.15" />
                  <Point X="3.33088423061" Y="1.255902287985" Z="1.15" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.15" />
                  <Point X="3.273043367322" Y="1.313608156799" Z="1.15" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.15" />
                  <Point X="3.241600595094" Y="1.393535631651" Z="1.15" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.15" />
                  <Point X="3.245360127151" Y="1.474429089023" Z="1.15" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.15" />
                  <Point X="3.286708047418" Y="1.550528036371" Z="1.15" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.15" />
                  <Point X="3.827077884251" Y="1.979239067042" Z="1.15" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.15" />
                  <Point X="4.332972433677" Y="2.644108468985" Z="1.15" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.15" />
                  <Point X="4.109194571834" Y="2.980013403414" Z="1.15" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.15" />
                  <Point X="3.32582963409" Y="2.73808868267" Z="1.15" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.15" />
                  <Point X="2.465981879757" Y="2.255260394789" Z="1.15" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.15" />
                  <Point X="2.391634019816" Y="2.250106162645" Z="1.15" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.15" />
                  <Point X="2.325553122027" Y="2.277387311336" Z="1.15" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.15" />
                  <Point X="2.273371288899" Y="2.331471738355" Z="1.15" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.15" />
                  <Point X="2.249323540114" Y="2.398124424016" Z="1.15" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.15" />
                  <Point X="2.257267547869" Y="2.473487696125" Z="1.15" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.15" />
                  <Point X="2.65753667019" Y="3.186309447865" Z="1.15" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.15" />
                  <Point X="2.923527160991" Y="4.148116541173" Z="1.15" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.15" />
                  <Point X="2.536038514268" Y="4.3957243076" Z="1.15" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.15" />
                  <Point X="2.130650966854" Y="4.606030640736" Z="1.15" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.15" />
                  <Point X="1.710410957197" Y="4.778042248186" Z="1.15" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.15" />
                  <Point X="1.159069124916" Y="4.934641203982" Z="1.15" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.15" />
                  <Point X="0.496615135178" Y="5.044552030988" Z="1.15" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.15" />
                  <Point X="0.105655321507" Y="4.749435521649" Z="1.15" />
                  <Point X="0" Y="4.355124473572" Z="1.15" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>