<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#133" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="806" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000473632812" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.373412109375" Y="-3.748703369141" />
                  <Point X="-24.4366953125" Y="-3.512524414063" />
                  <Point X="-24.44227734375" Y="-3.497140625" />
                  <Point X="-24.457634765625" Y="-3.467377929688" />
                  <Point X="-24.4835" Y="-3.430110107422" />
                  <Point X="-24.62136328125" Y="-3.231477539062" />
                  <Point X="-24.643248046875" Y="-3.209020263672" />
                  <Point X="-24.66950390625" Y="-3.189776611328" />
                  <Point X="-24.69750390625" Y="-3.175669433594" />
                  <Point X="-24.737529296875" Y="-3.163246826172" />
                  <Point X="-24.95086328125" Y="-3.097036132812" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.076849609375" Y="-3.109458251953" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.18977734375" />
                  <Point X="-25.344439453125" Y="-3.209021484375" />
                  <Point X="-25.36632421875" Y="-3.231477783203" />
                  <Point X="-25.392189453125" Y="-3.268745361328" />
                  <Point X="-25.53005078125" Y="-3.467378173828" />
                  <Point X="-25.5381875" Y="-3.481571289062" />
                  <Point X="-25.550990234375" Y="-3.512524169922" />
                  <Point X="-25.888287109375" Y="-4.771334960938" />
                  <Point X="-25.916583984375" Y="-4.87694140625" />
                  <Point X="-26.0793359375" Y="-4.845351074219" />
                  <Point X="-26.121828125" Y="-4.83441796875" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.222580078125" Y="-4.621303222656" />
                  <Point X="-26.2149609375" Y="-4.563439453125" />
                  <Point X="-26.21419921875" Y="-4.547930175781" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.2260703125" Y="-4.468150878906" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.182964355469" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131205078125" />
                  <Point X="-26.360494140625" Y="-4.098887695313" />
                  <Point X="-26.556904296875" Y="-3.926640380859" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.691935546875" Y="-3.887760498047" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708740234" />
                  <Point X="-27.014466796875" Y="-3.882028564453" />
                  <Point X="-27.042658203125" Y="-3.894801513672" />
                  <Point X="-27.083412109375" Y="-3.922032226562" />
                  <Point X="-27.300623046875" Y="-4.067169433594" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.3351015625" Y="-4.099461914062" />
                  <Point X="-27.4801484375" Y="-4.288489746094" />
                  <Point X="-27.801708984375" Y="-4.08938671875" />
                  <Point X="-27.86054296875" Y="-4.044086914063" />
                  <Point X="-28.10472265625" Y="-3.856077392578" />
                  <Point X="-27.5413671875" Y="-2.880319335938" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.411279296875" Y="-2.646978271484" />
                  <Point X="-27.40586328125" Y="-2.6244453125" />
                  <Point X="-27.4060625" Y="-2.601271484375" />
                  <Point X="-27.41020703125" Y="-2.569790283203" />
                  <Point X="-27.4165703125" Y="-2.545973144531" />
                  <Point X="-27.428845703125" Y="-2.524594726562" />
                  <Point X="-27.441169921875" Y="-2.508428710938" />
                  <Point X="-27.44954296875" Y="-2.498850097656" />
                  <Point X="-27.464150390625" Y="-2.4842421875" />
                  <Point X="-27.489306640625" Y="-2.466214355469" />
                  <Point X="-27.518134765625" Y="-2.451997070313" />
                  <Point X="-27.547751953125" Y="-2.443011962891" />
                  <Point X="-27.5786875" Y="-2.444024169922" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.7248828125" Y="-3.088026367188" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-29.082857421875" Y="-2.793861328125" />
                  <Point X="-29.125037109375" Y="-2.723135253906" />
                  <Point X="-29.306142578125" Y="-2.419449707031" />
                  <Point X="-28.3117265625" Y="-1.656408203125" />
                  <Point X="-28.105955078125" Y="-1.498513305664" />
                  <Point X="-28.08307421875" Y="-1.475893066406" />
                  <Point X="-28.064375" Y="-1.446474487305" />
                  <Point X="-28.0564375" Y="-1.426698608398" />
                  <Point X="-28.052634765625" Y="-1.415127563477" />
                  <Point X="-28.0461484375" Y="-1.390079711914" />
                  <Point X="-28.04203515625" Y="-1.358597290039" />
                  <Point X="-28.042736328125" Y="-1.335729003906" />
                  <Point X="-28.0488828125" Y="-1.31369128418" />
                  <Point X="-28.060888671875" Y="-1.284709838867" />
                  <Point X="-28.073193359375" Y="-1.26335925293" />
                  <Point X="-28.09057421875" Y="-1.245891601562" />
                  <Point X="-28.107259765625" Y="-1.23300012207" />
                  <Point X="-28.11715625" Y="-1.226304443359" />
                  <Point X="-28.139455078125" Y="-1.213180297852" />
                  <Point X="-28.168716796875" Y="-1.201956665039" />
                  <Point X="-28.200603515625" Y="-1.195474975586" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-29.602521484375" Y="-1.374825927734" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.834078125" Y="-0.992647888184" />
                  <Point X="-29.845234375" Y="-0.914634887695" />
                  <Point X="-29.892421875" Y="-0.584698242188" />
                  <Point X="-28.76728125" Y="-0.283217193604" />
                  <Point X="-28.532875" Y="-0.220408309937" />
                  <Point X="-28.5140078125" Y="-0.213123596191" />
                  <Point X="-28.494056640625" Y="-0.202860733032" />
                  <Point X="-28.483345703125" Y="-0.19642741394" />
                  <Point X="-28.4599765625" Y="-0.180208297729" />
                  <Point X="-28.4360234375" Y="-0.158684570312" />
                  <Point X="-28.415916015625" Y="-0.13012991333" />
                  <Point X="-28.407029296875" Y="-0.110682685852" />
                  <Point X="-28.402705078125" Y="-0.099355934143" />
                  <Point X="-28.394916015625" Y="-0.074257850647" />
                  <Point X="-28.390732421875" Y="-0.042062957764" />
                  <Point X="-28.392193359375" Y="-0.007714312077" />
                  <Point X="-28.396376953125" Y="0.016406698227" />
                  <Point X="-28.404166015625" Y="0.04150478363" />
                  <Point X="-28.417482421875" Y="0.070831138611" />
                  <Point X="-28.438826171875" Y="0.098748733521" />
                  <Point X="-28.45503125" Y="0.1133099823" />
                  <Point X="-28.464361328125" Y="0.120691551208" />
                  <Point X="-28.48773046875" Y="0.136910507202" />
                  <Point X="-28.501923828125" Y="0.145046325684" />
                  <Point X="-28.532875" Y="0.157848327637" />
                  <Point X="-29.782234375" Y="0.492613372803" />
                  <Point X="-29.89181640625" Y="0.521975769043" />
                  <Point X="-29.82448828125" Y="0.976969238281" />
                  <Point X="-29.8020234375" Y="1.059873779297" />
                  <Point X="-29.70355078125" Y="1.423268066406" />
                  <Point X="-28.933693359375" Y="1.32191418457" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228027344" />
                  <Point X="-28.7031328125" Y="1.305264648438" />
                  <Point X="-28.6934296875" Y="1.308324462891" />
                  <Point X="-28.64170703125" Y="1.324632202148" />
                  <Point X="-28.6227734375" Y="1.332963500977" />
                  <Point X="-28.60402734375" Y="1.343787475586" />
                  <Point X="-28.587345703125" Y="1.356021362305" />
                  <Point X="-28.573705078125" Y="1.371576660156" />
                  <Point X="-28.561291015625" Y="1.389309814453" />
                  <Point X="-28.551345703125" Y="1.407443237305" />
                  <Point X="-28.547455078125" Y="1.416838134766" />
                  <Point X="-28.526701171875" Y="1.466941894531" />
                  <Point X="-28.52091796875" Y="1.486786010742" />
                  <Point X="-28.517158203125" Y="1.508098999023" />
                  <Point X="-28.515802734375" Y="1.528739257812" />
                  <Point X="-28.518947265625" Y="1.549183349609" />
                  <Point X="-28.524546875" Y="1.570088134766" />
                  <Point X="-28.532046875" Y="1.58937512207" />
                  <Point X="-28.536748046875" Y="1.598405639648" />
                  <Point X="-28.561791015625" Y="1.646510131836" />
                  <Point X="-28.573283203125" Y="1.663708374023" />
                  <Point X="-28.5871953125" Y="1.680287719727" />
                  <Point X="-28.60213671875" Y="1.694590454102" />
                  <Point X="-29.318771484375" Y="2.244484375" />
                  <Point X="-29.351859375" Y="2.269873291016" />
                  <Point X="-29.081146484375" Y="2.733668212891" />
                  <Point X="-29.021650390625" Y="2.810142089844" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.309853515625" Y="2.904252441406" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796142578" />
                  <Point X="-28.13327734375" Y="2.824613769531" />
                  <Point X="-28.061244140625" Y="2.818311523438" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.999013671875" Y="2.826504638672" />
                  <Point X="-27.980462890625" Y="2.835653564453" />
                  <Point X="-27.962208984375" Y="2.847282714844" />
                  <Point X="-27.946080078125" Y="2.860226806641" />
                  <Point X="-27.936486328125" Y="2.869819824219" />
                  <Point X="-27.88535546875" Y="2.920950195312" />
                  <Point X="-27.87240234375" Y="2.937089599609" />
                  <Point X="-27.8607734375" Y="2.955345947266" />
                  <Point X="-27.851625" Y="2.973897705078" />
                  <Point X="-27.8467109375" Y="2.993989501953" />
                  <Point X="-27.84388671875" Y="3.015450683594" />
                  <Point X="-27.843435546875" Y="3.036123046875" />
                  <Point X="-27.8446171875" Y="3.049633056641" />
                  <Point X="-27.85091796875" Y="3.121667480469" />
                  <Point X="-27.854955078125" Y="3.141961669922" />
                  <Point X="-27.86146484375" Y="3.162604248047" />
                  <Point X="-27.869794921875" Y="3.181532714844" />
                  <Point X="-28.183333984375" Y="3.724595947266" />
                  <Point X="-27.700623046875" Y="4.094684814453" />
                  <Point X="-27.606912109375" Y="4.146748046875" />
                  <Point X="-27.16703515625" Y="4.391133789062" />
                  <Point X="-27.074787109375" Y="4.270912597656" />
                  <Point X="-27.0431953125" Y="4.229741210938" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.995111328125" Y="4.189394042969" />
                  <Point X="-26.980068359375" Y="4.181563476563" />
                  <Point X="-26.89989453125" Y="4.139827148438" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.77744921875" Y="4.134483398438" />
                  <Point X="-26.761783203125" Y="4.140973144531" />
                  <Point X="-26.678275390625" Y="4.175562988281" />
                  <Point X="-26.66014453125" Y="4.185509765625" />
                  <Point X="-26.6424140625" Y="4.197924316406" />
                  <Point X="-26.626859375" Y="4.211565429688" />
                  <Point X="-26.614626953125" Y="4.22825" />
                  <Point X="-26.603806640625" Y="4.246994140625" />
                  <Point X="-26.595478515625" Y="4.265921386719" />
                  <Point X="-26.59037890625" Y="4.282095214844" />
                  <Point X="-26.56319921875" Y="4.368297851562" />
                  <Point X="-26.5591640625" Y="4.388583984375" />
                  <Point X="-26.55727734375" Y="4.410145019531" />
                  <Point X="-26.557728515625" Y="4.430826171875" />
                  <Point X="-26.584201171875" Y="4.6318984375" />
                  <Point X="-26.58412109375" Y="4.631920898438" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.836033203125" Y="4.823103515625" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.164431640625" Y="4.400250976562" />
                  <Point X="-25.133904296875" Y="4.286315917969" />
                  <Point X="-25.12112890625" Y="4.258122558594" />
                  <Point X="-25.10326953125" Y="4.231395507813" />
                  <Point X="-25.08211328125" Y="4.208808105469" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-24.061966796875" Y="4.809046386719" />
                  <Point X="-23.5189765625" Y="4.677951660156" />
                  <Point X="-23.45904296875" Y="4.656213867188" />
                  <Point X="-23.105353515625" Y="4.527928222656" />
                  <Point X="-23.04619140625" Y="4.500260253906" />
                  <Point X="-22.705421875" Y="4.340893066406" />
                  <Point X="-22.64823828125" Y="4.307577636719" />
                  <Point X="-22.319015625" Y="4.115770996094" />
                  <Point X="-22.26511328125" Y="4.077440185547" />
                  <Point X="-22.056736328125" Y="3.929254638672" />
                  <Point X="-22.716296875" Y="2.786864746094" />
                  <Point X="-22.852416015625" Y="2.55109765625" />
                  <Point X="-22.8601171875" Y="2.53409375" />
                  <Point X="-22.86851171875" Y="2.509327392578" />
                  <Point X="-22.870314453125" Y="2.503373535156" />
                  <Point X="-22.888392578125" Y="2.435770751953" />
                  <Point X="-22.8916171875" Y="2.411278320312" />
                  <Point X="-22.8916328125" Y="2.381407958984" />
                  <Point X="-22.89094921875" Y="2.369985351562" />
                  <Point X="-22.883900390625" Y="2.311528320312" />
                  <Point X="-22.87855859375" Y="2.289607177734" />
                  <Point X="-22.87029296875" Y="2.267518066406" />
                  <Point X="-22.859927734375" Y="2.247467285156" />
                  <Point X="-22.853140625" Y="2.237465820312" />
                  <Point X="-22.81696875" Y="2.184158691406" />
                  <Point X="-22.800388671875" Y="2.165549072266" />
                  <Point X="-22.777087890625" Y="2.145462402344" />
                  <Point X="-22.7683984375" Y="2.1388046875" />
                  <Point X="-22.71508984375" Y="2.102633789063" />
                  <Point X="-22.695044921875" Y="2.092271728516" />
                  <Point X="-22.67295703125" Y="2.084006103516" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.640068359375" Y="2.077341064453" />
                  <Point X="-22.581611328125" Y="2.070291992188" />
                  <Point X="-22.556169921875" Y="2.070656005859" />
                  <Point X="-22.524583984375" Y="2.075385498047" />
                  <Point X="-22.514111328125" Y="2.077562744141" />
                  <Point X="-22.4465078125" Y="2.095640625" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.154849609375" Y="2.835652587891" />
                  <Point X="-21.032673828125" Y="2.906191162109" />
                  <Point X="-20.876716796875" Y="2.689448242188" />
                  <Point X="-20.84667578125" Y="2.639802978516" />
                  <Point X="-20.73780078125" Y="2.459884277344" />
                  <Point X="-21.59" Y="1.805967651367" />
                  <Point X="-21.76921484375" Y="1.668451171875" />
                  <Point X="-21.783111328125" Y="1.655373535156" />
                  <Point X="-21.801486328125" Y="1.63421496582" />
                  <Point X="-21.80515625" Y="1.629717529297" />
                  <Point X="-21.85380859375" Y="1.566244995117" />
                  <Point X="-21.86637890625" Y="1.544319946289" />
                  <Point X="-21.878248046875" Y="1.515210693359" />
                  <Point X="-21.881767578125" Y="1.504929931641" />
                  <Point X="-21.899892578125" Y="1.440124389648" />
                  <Point X="-21.90334765625" Y="1.41781628418" />
                  <Point X="-21.904162109375" Y="1.394239501953" />
                  <Point X="-21.9022578125" Y="1.371756225586" />
                  <Point X="-21.899466796875" Y="1.358234130859" />
                  <Point X="-21.88458984375" Y="1.286129638672" />
                  <Point X="-21.876130859375" Y="1.262076538086" />
                  <Point X="-21.861341796875" Y="1.233159057617" />
                  <Point X="-21.856125" Y="1.224201416016" />
                  <Point X="-21.81566015625" Y="1.162695800781" />
                  <Point X="-21.801107421875" Y="1.145449951172" />
                  <Point X="-21.78386328125" Y="1.129360229492" />
                  <Point X="-21.7656484375" Y="1.116032226562" />
                  <Point X="-21.75464453125" Y="1.109838867188" />
                  <Point X="-21.696005859375" Y="1.076829833984" />
                  <Point X="-21.67179296875" Y="1.067291870117" />
                  <Point X="-21.63894140625" Y="1.059327880859" />
                  <Point X="-21.629005859375" Y="1.05747265625" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.318095703125" Y="1.204138427734" />
                  <Point X="-20.223162109375" Y="1.21663659668" />
                  <Point X="-20.15405859375" Y="0.932787353516" />
                  <Point X="-20.144591796875" Y="0.871983825684" />
                  <Point X="-20.109134765625" Y="0.644238891602" />
                  <Point X="-21.07803515625" Y="0.384622406006" />
                  <Point X="-21.283419921875" Y="0.329589874268" />
                  <Point X="-21.30158984375" Y="0.322660339355" />
                  <Point X="-21.32828515625" Y="0.309205200195" />
                  <Point X="-21.333068359375" Y="0.30662020874" />
                  <Point X="-21.410962890625" Y="0.261595214844" />
                  <Point X="-21.431330078125" Y="0.245780944824" />
                  <Point X="-21.454412109375" Y="0.222186798096" />
                  <Point X="-21.46123828125" Y="0.214402084351" />
                  <Point X="-21.507974609375" Y="0.15484803772" />
                  <Point X="-21.51969921875" Y="0.135567016602" />
                  <Point X="-21.52947265625" Y="0.114103088379" />
                  <Point X="-21.53631640625" Y="0.092608657837" />
                  <Point X="-21.539240234375" Y="0.077345985413" />
                  <Point X="-21.554818359375" Y="-0.004001974583" />
                  <Point X="-21.55616796875" Y="-0.029992891312" />
                  <Point X="-21.55324609375" Y="-0.064064826965" />
                  <Point X="-21.5518984375" Y="-0.073817810059" />
                  <Point X="-21.536318359375" Y="-0.155165771484" />
                  <Point X="-21.52947265625" Y="-0.176665817261" />
                  <Point X="-21.51969921875" Y="-0.198128677368" />
                  <Point X="-21.50797265625" Y="-0.217411056519" />
                  <Point X="-21.499203125" Y="-0.22858480835" />
                  <Point X="-21.452466796875" Y="-0.288139007568" />
                  <Point X="-21.433314453125" Y="-0.306531616211" />
                  <Point X="-21.40438671875" Y="-0.327399597168" />
                  <Point X="-21.39634765625" Y="-0.332602966309" />
                  <Point X="-21.318453125" Y="-0.3776277771" />
                  <Point X="-21.307291015625" Y="-0.383137786865" />
                  <Point X="-21.283419921875" Y="-0.392149871826" />
                  <Point X="-20.1887421875" Y="-0.685468078613" />
                  <Point X="-20.108525390625" Y="-0.706961914062" />
                  <Point X="-20.1449765625" Y="-0.948730224609" />
                  <Point X="-20.157109375" Y="-1.001892028809" />
                  <Point X="-20.19882421875" Y="-1.18469909668" />
                  <Point X="-21.3374296875" Y="-1.034798828125" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710144043" />
                  <Point X="-21.62533984375" Y="-1.005508728027" />
                  <Point X="-21.6540234375" Y="-1.011743225098" />
                  <Point X="-21.806904296875" Y="-1.044972412109" />
                  <Point X="-21.8360234375" Y="-1.056595947266" />
                  <Point X="-21.8638515625" Y="-1.07348840332" />
                  <Point X="-21.8876015625" Y="-1.093959472656" />
                  <Point X="-21.904939453125" Y="-1.114810913086" />
                  <Point X="-21.997345703125" Y="-1.225947387695" />
                  <Point X="-22.012068359375" Y="-1.250334594727" />
                  <Point X="-22.02341015625" Y="-1.277719726562" />
                  <Point X="-22.030240234375" Y="-1.3053671875" />
                  <Point X="-22.032724609375" Y="-1.332370849609" />
                  <Point X="-22.04596875" Y="-1.476297485352" />
                  <Point X="-22.04365234375" Y="-1.507561157227" />
                  <Point X="-22.035921875" Y="-1.539182373047" />
                  <Point X="-22.02355078125" Y="-1.567994750977" />
                  <Point X="-22.007677734375" Y="-1.592685668945" />
                  <Point X="-21.9230703125" Y="-1.724285400391" />
                  <Point X="-21.9130625" Y="-1.737243286133" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-20.873501953125" Y="-2.540412353516" />
                  <Point X="-20.786875" Y="-2.606883056641" />
                  <Point X="-20.875185546875" Y="-2.749782226562" />
                  <Point X="-20.90028125" Y="-2.785439697266" />
                  <Point X="-20.971017578125" Y="-2.885945556641" />
                  <Point X="-21.98690625" Y="-2.299421630859" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.2457734375" Y="-2.159825195312" />
                  <Point X="-22.27991015625" Y="-2.153659912109" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.5551640625" Y="-2.135176025391" />
                  <Point X="-22.583525390625" Y="-2.150101806641" />
                  <Point X="-22.734681640625" Y="-2.229655029297" />
                  <Point X="-22.757615234375" Y="-2.246549560547" />
                  <Point X="-22.77857421875" Y="-2.267509033203" />
                  <Point X="-22.795466796875" Y="-2.290439208984" />
                  <Point X="-22.810392578125" Y="-2.318799560547" />
                  <Point X="-22.889947265625" Y="-2.469957275391" />
                  <Point X="-22.899771484375" Y="-2.499735107422" />
                  <Point X="-22.904728515625" Y="-2.531908447266" />
                  <Point X="-22.904322265625" Y="-2.563260498047" />
                  <Point X="-22.89815625" Y="-2.5973984375" />
                  <Point X="-22.865294921875" Y="-2.779350830078" />
                  <Point X="-22.86101171875" Y="-2.795140136719" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.195380859375" Y="-3.956756347656" />
                  <Point X="-22.13871484375" Y="-4.054905029297" />
                  <Point X="-22.2181484375" Y="-4.111642578125" />
                  <Point X="-22.246203125" Y="-4.129801757813" />
                  <Point X="-22.298234375" Y="-4.163480957031" />
                  <Point X="-23.0795703125" Y="-3.145222900391" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900556640625" />
                  <Point X="-23.311744140625" Y="-2.878910400391" />
                  <Point X="-23.49119921875" Y="-2.763537841797" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.61972265625" Y="-2.744505126953" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.895404296875" Y="-2.795461425781" />
                  <Point X="-23.923837890625" Y="-2.819103271484" />
                  <Point X="-24.075388671875" Y="-2.945111816406" />
                  <Point X="-24.095857421875" Y="-2.968861572266" />
                  <Point X="-24.11275" Y="-2.996688476562" />
                  <Point X="-24.124375" Y="-3.025808105469" />
                  <Point X="-24.132876953125" Y="-3.064922119141" />
                  <Point X="-24.178189453125" Y="-3.273395996094" />
                  <Point X="-24.180275390625" Y="-3.289627685547" />
                  <Point X="-24.1802578125" Y="-3.323119873047" />
                  <Point X="-23.995259765625" Y="-4.728317382812" />
                  <Point X="-23.97793359375" Y="-4.859915039062" />
                  <Point X="-24.024328125" Y="-4.870084960938" />
                  <Point X="-24.050240234375" Y="-4.874791992188" />
                  <Point X="-24.07068359375" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.058427734375" Y="-4.75263671875" />
                  <Point X="-26.09815625" Y="-4.742414550781" />
                  <Point X="-26.141244140625" Y="-4.731328125" />
                  <Point X="-26.128392578125" Y="-4.633702636719" />
                  <Point X="-26.1207734375" Y="-4.575838867188" />
                  <Point X="-26.120076171875" Y="-4.568099609375" />
                  <Point X="-26.11944921875" Y="-4.54103125" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497688964844" />
                  <Point X="-26.132896484375" Y="-4.449616699219" />
                  <Point X="-26.18386328125" Y="-4.193397460938" />
                  <Point X="-26.188125" Y="-4.178468261719" />
                  <Point X="-26.19902734375" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135462890625" />
                  <Point X="-26.221740234375" Y="-4.107625976562" />
                  <Point X="-26.230576171875" Y="-4.094859619141" />
                  <Point X="-26.250208984375" Y="-4.070937255859" />
                  <Point X="-26.261005859375" Y="-4.059781494141" />
                  <Point X="-26.29785546875" Y="-4.027464111328" />
                  <Point X="-26.494265625" Y="-3.855216796875" />
                  <Point X="-26.506736328125" Y="-3.845967529297" />
                  <Point X="-26.53301953125" Y="-3.829622558594" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.68572265625" Y="-3.792963867188" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166259766" />
                  <Point X="-27.008005859375" Y="-3.781946044922" />
                  <Point X="-27.0390546875" Y="-3.790265869141" />
                  <Point X="-27.053673828125" Y="-3.79549609375" />
                  <Point X="-27.081865234375" Y="-3.808269042969" />
                  <Point X="-27.0954375" Y="-3.815811767578" />
                  <Point X="-27.13619140625" Y="-3.843042480469" />
                  <Point X="-27.35340234375" Y="-3.9881796875" />
                  <Point X="-27.359685546875" Y="-3.992760253906" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032770507812" />
                  <Point X="-27.410470703125" Y="-4.041629394531" />
                  <Point X="-27.503201171875" Y="-4.162479003906" />
                  <Point X="-27.747587890625" Y="-4.011161132812" />
                  <Point X="-27.8025859375" Y="-3.968814208984" />
                  <Point X="-27.98086328125" Y="-3.831546386719" />
                  <Point X="-27.459095703125" Y="-2.927819335938" />
                  <Point X="-27.341490234375" Y="-2.724119628906" />
                  <Point X="-27.336208984375" Y="-2.713489746094" />
                  <Point X="-27.3237265625" Y="-2.683848388672" />
                  <Point X="-27.31891015625" Y="-2.669180175781" />
                  <Point X="-27.313494140625" Y="-2.646647216797" />
                  <Point X="-27.3108671875" Y="-2.623628662109" />
                  <Point X="-27.31106640625" Y="-2.600454833984" />
                  <Point X="-27.311875" Y="-2.588871582031" />
                  <Point X="-27.31601953125" Y="-2.557390380859" />
                  <Point X="-27.31842578125" Y="-2.545269042969" />
                  <Point X="-27.3247890625" Y="-2.521451904297" />
                  <Point X="-27.334185546875" Y="-2.498668212891" />
                  <Point X="-27.3464609375" Y="-2.477289794922" />
                  <Point X="-27.353296875" Y="-2.466999267578" />
                  <Point X="-27.36562109375" Y="-2.450833251953" />
                  <Point X="-27.3823671875" Y="-2.431676025391" />
                  <Point X="-27.396974609375" Y="-2.417068115234" />
                  <Point X="-27.4088125" Y="-2.4070234375" />
                  <Point X="-27.43396875" Y="-2.388995605469" />
                  <Point X="-27.447287109375" Y="-2.381012451172" />
                  <Point X="-27.476115234375" Y="-2.366795166016" />
                  <Point X="-27.4905546875" Y="-2.361088378906" />
                  <Point X="-27.520171875" Y="-2.352103271484" />
                  <Point X="-27.550859375" Y="-2.348062744141" />
                  <Point X="-27.581794921875" Y="-2.349074951172" />
                  <Point X="-27.597220703125" Y="-2.350849365234" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.7723828125" Y="-3.00575390625" />
                  <Point X="-28.793087890625" Y="-3.017708251953" />
                  <Point X="-29.004013671875" Y="-2.740592529297" />
                  <Point X="-29.0434453125" Y="-2.674475341797" />
                  <Point X="-29.181265625" Y="-2.443372802734" />
                  <Point X="-28.25389453125" Y="-1.731776855469" />
                  <Point X="-28.048123046875" Y="-1.573881958008" />
                  <Point X="-28.039166015625" Y="-1.566072021484" />
                  <Point X="-28.01628515625" Y="-1.543452026367" />
                  <Point X="-28.002900390625" Y="-1.526854125977" />
                  <Point X="-27.984201171875" Y="-1.497435546875" />
                  <Point X="-27.9762109375" Y="-1.481860961914" />
                  <Point X="-27.9682734375" Y="-1.462085083008" />
                  <Point X="-27.96066796875" Y="-1.438942993164" />
                  <Point X="-27.954181640625" Y="-1.413895141602" />
                  <Point X="-27.95194921875" Y="-1.402387207031" />
                  <Point X="-27.9478359375" Y="-1.370904785156" />
                  <Point X="-27.947080078125" Y="-1.355685791016" />
                  <Point X="-27.94778125" Y="-1.332817504883" />
                  <Point X="-27.951228515625" Y="-1.31020690918" />
                  <Point X="-27.957375" Y="-1.288169189453" />
                  <Point X="-27.961115234375" Y="-1.277332885742" />
                  <Point X="-27.97312109375" Y="-1.24835144043" />
                  <Point X="-27.978580078125" Y="-1.237273681641" />
                  <Point X="-27.990884765625" Y="-1.215923095703" />
                  <Point X="-28.0058515625" Y="-1.1963515625" />
                  <Point X="-28.023232421875" Y="-1.178883911133" />
                  <Point X="-28.0324921875" Y="-1.170715332031" />
                  <Point X="-28.049177734375" Y="-1.157823974609" />
                  <Point X="-28.068970703125" Y="-1.144432250977" />
                  <Point X="-28.09126953125" Y="-1.131308105469" />
                  <Point X="-28.10543359375" Y="-1.124481323242" />
                  <Point X="-28.1346953125" Y="-1.113257568359" />
                  <Point X="-28.14979296875" Y="-1.108860595703" />
                  <Point X="-28.1816796875" Y="-1.10237902832" />
                  <Point X="-28.197296875" Y="-1.100532592773" />
                  <Point X="-28.22862109375" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-29.614921875" Y="-1.280638671875" />
                  <Point X="-29.66091796875" Y="-1.286694213867" />
                  <Point X="-29.740763671875" Y="-0.974109375" />
                  <Point X="-29.75119140625" Y="-0.901186279297" />
                  <Point X="-29.786451171875" Y="-0.654654418945" />
                  <Point X="-28.742693359375" Y="-0.374980133057" />
                  <Point X="-28.508287109375" Y="-0.312171234131" />
                  <Point X="-28.49865625" Y="-0.309031860352" />
                  <Point X="-28.4797890625" Y="-0.30174710083" />
                  <Point X="-28.470552734375" Y="-0.297602020264" />
                  <Point X="-28.4506015625" Y="-0.287339172363" />
                  <Point X="-28.4291796875" Y="-0.274472290039" />
                  <Point X="-28.405810546875" Y="-0.258253234863" />
                  <Point X="-28.39648046875" Y="-0.250871276855" />
                  <Point X="-28.37252734375" Y="-0.229347564697" />
                  <Point X="-28.358349609375" Y="-0.213380859375" />
                  <Point X="-28.3382421875" Y="-0.18482623291" />
                  <Point X="-28.329509765625" Y="-0.169614395142" />
                  <Point X="-28.320623046875" Y="-0.150167236328" />
                  <Point X="-28.311974609375" Y="-0.127513839722" />
                  <Point X="-28.304185546875" Y="-0.102415748596" />
                  <Point X="-28.300708984375" Y="-0.086499725342" />
                  <Point X="-28.296525390625" Y="-0.054304889679" />
                  <Point X="-28.295818359375" Y="-0.038026077271" />
                  <Point X="-28.297279296875" Y="-0.003677401304" />
                  <Point X="-28.29858984375" Y="0.008520226479" />
                  <Point X="-28.3027734375" Y="0.032641273499" />
                  <Point X="-28.305646484375" Y="0.044564689636" />
                  <Point X="-28.313435546875" Y="0.069662780762" />
                  <Point X="-28.317666015625" Y="0.080782447815" />
                  <Point X="-28.330982421875" Y="0.110108718872" />
                  <Point X="-28.34201171875" Y="0.12853036499" />
                  <Point X="-28.36335546875" Y="0.156447982788" />
                  <Point X="-28.375330078125" Y="0.169412216187" />
                  <Point X="-28.39153515625" Y="0.183973388672" />
                  <Point X="-28.4101953125" Y="0.198736694336" />
                  <Point X="-28.433564453125" Y="0.214955764771" />
                  <Point X="-28.440486328125" Y="0.219330184937" />
                  <Point X="-28.46561328125" Y="0.232833312988" />
                  <Point X="-28.496564453125" Y="0.245635391235" />
                  <Point X="-28.508287109375" Y="0.249611343384" />
                  <Point X="-29.757646484375" Y="0.584376342773" />
                  <Point X="-29.7854453125" Y="0.591825195312" />
                  <Point X="-29.73133203125" Y="0.95752355957" />
                  <Point X="-29.710330078125" Y="1.03502746582" />
                  <Point X="-29.6335859375" Y="1.318237182617" />
                  <Point X="-28.94609375" Y="1.227727050781" />
                  <Point X="-28.778064453125" Y="1.20560546875" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.20470324707" />
                  <Point X="-28.71514453125" Y="1.206589355469" />
                  <Point X="-28.704888671875" Y="1.208053833008" />
                  <Point X="-28.68459765625" Y="1.212090332031" />
                  <Point X="-28.664859375" Y="1.21772265625" />
                  <Point X="-28.61313671875" Y="1.234030273438" />
                  <Point X="-28.6034453125" Y="1.237678100586" />
                  <Point X="-28.58451171875" Y="1.246009399414" />
                  <Point X="-28.57526953125" Y="1.250692749023" />
                  <Point X="-28.5565234375" Y="1.261516845703" />
                  <Point X="-28.547845703125" Y="1.267180541992" />
                  <Point X="-28.5311640625" Y="1.279414428711" />
                  <Point X="-28.51591796875" Y="1.293386108398" />
                  <Point X="-28.50227734375" Y="1.30894140625" />
                  <Point X="-28.49587890625" Y="1.317095214844" />
                  <Point X="-28.48346484375" Y="1.334828369141" />
                  <Point X="-28.47799609375" Y="1.343626586914" />
                  <Point X="-28.46805078125" Y="1.361760009766" />
                  <Point X="-28.45968359375" Y="1.380490112305" />
                  <Point X="-28.4389296875" Y="1.43059375" />
                  <Point X="-28.43549609375" Y="1.440361694336" />
                  <Point X="-28.429712890625" Y="1.460205810547" />
                  <Point X="-28.42736328125" Y="1.470282104492" />
                  <Point X="-28.423603515625" Y="1.491594970703" />
                  <Point X="-28.42236328125" Y="1.501873657227" />
                  <Point X="-28.4210078125" Y="1.522513916016" />
                  <Point X="-28.42190625" Y="1.543181518555" />
                  <Point X="-28.42505078125" Y="1.563625488281" />
                  <Point X="-28.427181640625" Y="1.573763793945" />
                  <Point X="-28.43278125" Y="1.594668579102" />
                  <Point X="-28.436005859375" Y="1.604518554688" />
                  <Point X="-28.443505859375" Y="1.623805419922" />
                  <Point X="-28.452482421875" Y="1.642273071289" />
                  <Point X="-28.477525390625" Y="1.690377563477" />
                  <Point X="-28.482802734375" Y="1.699291503906" />
                  <Point X="-28.494294921875" Y="1.716489868164" />
                  <Point X="-28.500509765625" Y="1.724774169922" />
                  <Point X="-28.514421875" Y="1.741353515625" />
                  <Point X="-28.521501953125" Y="1.748913574219" />
                  <Point X="-28.536443359375" Y="1.763216308594" />
                  <Point X="-28.5443046875" Y="1.769958984375" />
                  <Point X="-29.22761328125" Y="2.294281005859" />
                  <Point X="-29.002283203125" Y="2.680324951172" />
                  <Point X="-28.946669921875" Y="2.751807617188" />
                  <Point X="-28.726337890625" Y="3.035013183594" />
                  <Point X="-28.357353515625" Y="2.821979980469" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656982422" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.141556640625" Y="2.729975097656" />
                  <Point X="-28.0695234375" Y="2.723672851562" />
                  <Point X="-28.059169921875" Y="2.723334228516" />
                  <Point X="-28.038490234375" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310791016" />
                  <Point X="-27.97643359375" Y="2.734227294922" />
                  <Point X="-27.956994140625" Y="2.741302978516" />
                  <Point X="-27.938443359375" Y="2.750451904297" />
                  <Point X="-27.929419921875" Y="2.755531738281" />
                  <Point X="-27.911166015625" Y="2.767160888672" />
                  <Point X="-27.902748046875" Y="2.773192138672" />
                  <Point X="-27.886619140625" Y="2.786136230469" />
                  <Point X="-27.869314453125" Y="2.802642089844" />
                  <Point X="-27.81818359375" Y="2.853772460938" />
                  <Point X="-27.811265625" Y="2.861487792969" />
                  <Point X="-27.7983125" Y="2.877627197266" />
                  <Point X="-27.79227734375" Y="2.886051269531" />
                  <Point X="-27.7806484375" Y="2.904307617188" />
                  <Point X="-27.7755703125" Y="2.913329589844" />
                  <Point X="-27.766421875" Y="2.931881347656" />
                  <Point X="-27.759345703125" Y="2.951327880859" />
                  <Point X="-27.754431640625" Y="2.971419677734" />
                  <Point X="-27.7525234375" Y="2.981594726562" />
                  <Point X="-27.74969921875" Y="3.003055908203" />
                  <Point X="-27.74891015625" Y="3.013377929688" />
                  <Point X="-27.748458984375" Y="3.034050292969" />
                  <Point X="-27.749978515625" Y="3.057910644531" />
                  <Point X="-27.756279296875" Y="3.129945068359" />
                  <Point X="-27.757744140625" Y="3.140202636719" />
                  <Point X="-27.76178125" Y="3.160496826172" />
                  <Point X="-27.764353515625" Y="3.170533447266" />
                  <Point X="-27.77086328125" Y="3.191176025391" />
                  <Point X="-27.77451171875" Y="3.200870361328" />
                  <Point X="-27.782841796875" Y="3.219798828125" />
                  <Point X="-27.7875234375" Y="3.229032958984" />
                  <Point X="-28.059388671875" Y="3.699915039062" />
                  <Point X="-27.648369140625" Y="4.015039306641" />
                  <Point X="-27.560775390625" Y="4.063703613281" />
                  <Point X="-27.1925234375" Y="4.268295898437" />
                  <Point X="-27.15015625" Y="4.213080566406" />
                  <Point X="-27.118564453125" Y="4.171909179688" />
                  <Point X="-27.1118203125" Y="4.164048828125" />
                  <Point X="-27.097517578125" Y="4.149107421875" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.06508984375" Y="4.121896972656" />
                  <Point X="-27.047888671875" Y="4.110403808594" />
                  <Point X="-27.02393359375" Y="4.097296875" />
                  <Point X="-26.943759765625" Y="4.055560302734" />
                  <Point X="-26.93432421875" Y="4.05128515625" />
                  <Point X="-26.915044921875" Y="4.043788574219" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031378173828" />
                  <Point X="-26.780818359375" Y="4.035136474609" />
                  <Point X="-26.7707265625" Y="4.037489257812" />
                  <Point X="-26.75086328125" Y="4.043279296875" />
                  <Point X="-26.72542578125" Y="4.053205810547" />
                  <Point X="-26.64191796875" Y="4.087795654297" />
                  <Point X="-26.63258203125" Y="4.092273681641" />
                  <Point X="-26.614451171875" Y="4.102220214844" />
                  <Point X="-26.60565625" Y="4.107689453125" />
                  <Point X="-26.58792578125" Y="4.120104003906" />
                  <Point X="-26.579775390625" Y="4.126499511719" />
                  <Point X="-26.564220703125" Y="4.140140625" />
                  <Point X="-26.550244140625" Y="4.15539453125" />
                  <Point X="-26.53801171875" Y="4.172079101562" />
                  <Point X="-26.5323515625" Y="4.180755371094" />
                  <Point X="-26.52153125" Y="4.199499511719" />
                  <Point X="-26.5168515625" Y="4.208733398438" />
                  <Point X="-26.5085234375" Y="4.227660644531" />
                  <Point X="-26.499775390625" Y="4.253527832031" />
                  <Point X="-26.472595703125" Y="4.33973046875" />
                  <Point X="-26.470025390625" Y="4.349764160156" />
                  <Point X="-26.465990234375" Y="4.370050292969" />
                  <Point X="-26.464525390625" Y="4.380302734375" />
                  <Point X="-26.462638671875" Y="4.401863769531" />
                  <Point X="-26.46230078125" Y="4.412216796875" />
                  <Point X="-26.462751953125" Y="4.432897949219" />
                  <Point X="-26.463541015625" Y="4.4432265625" />
                  <Point X="-26.479265625" Y="4.562655273438" />
                  <Point X="-25.931173828125" Y="4.7163203125" />
                  <Point X="-25.824990234375" Y="4.728747558594" />
                  <Point X="-25.36522265625" Y="4.782556152344" />
                  <Point X="-25.2561953125" Y="4.375663085938" />
                  <Point X="-25.22566796875" Y="4.261728027344" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912597656" />
                  <Point X="-25.2001171875" Y="4.205341308594" />
                  <Point X="-25.1822578125" Y="4.178614257812" />
                  <Point X="-25.17260546875" Y="4.166452636719" />
                  <Point X="-25.15144921875" Y="4.143865234375" />
                  <Point X="-25.126896484375" Y="4.125025390625" />
                  <Point X="-25.0996015625" Y="4.110436035156" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-24.08426171875" Y="4.716699707031" />
                  <Point X="-23.54640234375" Y="4.58684375" />
                  <Point X="-23.49143359375" Y="4.566906738281" />
                  <Point X="-23.14175" Y="4.44007421875" />
                  <Point X="-23.086435546875" Y="4.414205566406" />
                  <Point X="-22.749541015625" Y="4.256650390625" />
                  <Point X="-22.696060546875" Y="4.225492675781" />
                  <Point X="-22.370564453125" Y="4.035856933594" />
                  <Point X="-22.32016796875" Y="4.00001953125" />
                  <Point X="-22.182216796875" Y="3.901916015625" />
                  <Point X="-22.798568359375" Y="2.834364746094" />
                  <Point X="-22.9346875" Y="2.59859765625" />
                  <Point X="-22.938955078125" Y="2.590291259766" />
                  <Point X="-22.95008984375" Y="2.56458984375" />
                  <Point X="-22.958484375" Y="2.539823486328" />
                  <Point X="-22.96208984375" Y="2.527915771484" />
                  <Point X="-22.98016796875" Y="2.460312988281" />
                  <Point X="-22.982580078125" Y="2.448171142578" />
                  <Point X="-22.9858046875" Y="2.423678710938" />
                  <Point X="-22.9866171875" Y="2.411328125" />
                  <Point X="-22.9866328125" Y="2.381457763672" />
                  <Point X="-22.985265625" Y="2.358612548828" />
                  <Point X="-22.978216796875" Y="2.300155517578" />
                  <Point X="-22.97619921875" Y="2.289036621094" />
                  <Point X="-22.970857421875" Y="2.267115478516" />
                  <Point X="-22.967533203125" Y="2.256313232422" />
                  <Point X="-22.959267578125" Y="2.234224121094" />
                  <Point X="-22.95468359375" Y="2.223892333984" />
                  <Point X="-22.944318359375" Y="2.203841552734" />
                  <Point X="-22.93175" Y="2.18412109375" />
                  <Point X="-22.895578125" Y="2.130813964844" />
                  <Point X="-22.887900390625" Y="2.120962890625" />
                  <Point X="-22.8713203125" Y="2.102353271484" />
                  <Point X="-22.86241796875" Y="2.093594726562" />
                  <Point X="-22.8391171875" Y="2.073508056641" />
                  <Point X="-22.82173828125" Y="2.060192626953" />
                  <Point X="-22.7684296875" Y="2.024021728516" />
                  <Point X="-22.75871484375" Y="2.018242797852" />
                  <Point X="-22.738669921875" Y="2.007880737305" />
                  <Point X="-22.72833984375" Y="2.003297485352" />
                  <Point X="-22.706251953125" Y="1.995031860352" />
                  <Point X="-22.695451171875" Y="1.991707519531" />
                  <Point X="-22.673529296875" Y="1.986364990234" />
                  <Point X="-22.65144140625" Y="1.983024414062" />
                  <Point X="-22.592984375" Y="1.975975341797" />
                  <Point X="-22.580251953125" Y="1.975301879883" />
                  <Point X="-22.554810546875" Y="1.975665771484" />
                  <Point X="-22.5421015625" Y="1.976703369141" />
                  <Point X="-22.510515625" Y="1.981432861328" />
                  <Point X="-22.4895703125" Y="1.985787475586" />
                  <Point X="-22.421966796875" Y="2.003865356445" />
                  <Point X="-22.416005859375" Y="2.005670654297" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572875977" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.107349609375" Y="2.753380126953" />
                  <Point X="-21.05959375" Y="2.780951904297" />
                  <Point X="-20.956037109375" Y="2.637033447266" />
                  <Point X="-20.927953125" Y="2.590620605469" />
                  <Point X="-20.86311328125" Y="2.483472412109" />
                  <Point X="-21.64783203125" Y="1.881336181641" />
                  <Point X="-21.827046875" Y="1.743819824219" />
                  <Point X="-21.8343203125" Y="1.737633666992" />
                  <Point X="-21.854837890625" Y="1.717664672852" />
                  <Point X="-21.873212890625" Y="1.696506103516" />
                  <Point X="-21.8805546875" Y="1.687511230469" />
                  <Point X="-21.92920703125" Y="1.624038452148" />
                  <Point X="-21.936224609375" Y="1.613496337891" />
                  <Point X="-21.948794921875" Y="1.591571289062" />
                  <Point X="-21.95434765625" Y="1.580188720703" />
                  <Point X="-21.966216796875" Y="1.551079345703" />
                  <Point X="-21.973255859375" Y="1.530517822266" />
                  <Point X="-21.991380859375" Y="1.465712402344" />
                  <Point X="-21.9937734375" Y="1.454664672852" />
                  <Point X="-21.997228515625" Y="1.432356567383" />
                  <Point X="-21.998291015625" Y="1.421096069336" />
                  <Point X="-21.99910546875" Y="1.397519287109" />
                  <Point X="-21.998822265625" Y="1.386221801758" />
                  <Point X="-21.99691796875" Y="1.363738647461" />
                  <Point X="-21.992505859375" Y="1.339030517578" />
                  <Point X="-21.97762890625" Y="1.266926025391" />
                  <Point X="-21.974208984375" Y="1.254612304688" />
                  <Point X="-21.96575" Y="1.230559204102" />
                  <Point X="-21.9607109375" Y="1.218820068359" />
                  <Point X="-21.945921875" Y="1.189902587891" />
                  <Point X="-21.93548828125" Y="1.171987304688" />
                  <Point X="-21.8950234375" Y="1.110481689453" />
                  <Point X="-21.888263671875" Y="1.101429199219" />
                  <Point X="-21.8737109375" Y="1.084183349609" />
                  <Point X="-21.86591796875" Y="1.075989990234" />
                  <Point X="-21.848673828125" Y="1.059900268555" />
                  <Point X="-21.839962890625" Y="1.052692504883" />
                  <Point X="-21.821748046875" Y="1.039364501953" />
                  <Point X="-21.801240234375" Y="1.027051025391" />
                  <Point X="-21.7426015625" Y="0.994041931152" />
                  <Point X="-21.73082421875" Y="0.988440490723" />
                  <Point X="-21.706611328125" Y="0.978902587891" />
                  <Point X="-21.69417578125" Y="0.974966003418" />
                  <Point X="-21.66132421875" Y="0.967002075195" />
                  <Point X="-21.641453125" Y="0.963291687012" />
                  <Point X="-21.562166015625" Y="0.952813049316" />
                  <Point X="-21.555966796875" Y="0.952199829102" />
                  <Point X="-21.53428125" Y="0.951222900391" />
                  <Point X="-21.50878125" Y="0.952032287598" />
                  <Point X="-21.49939453125" Y="0.952797302246" />
                  <Point X="-20.3056953125" Y="1.109951049805" />
                  <Point X="-20.295298828125" Y="1.111319946289" />
                  <Point X="-20.247310546875" Y="0.914205078125" />
                  <Point X="-20.2384609375" Y="0.857368896484" />
                  <Point X="-20.21612890625" Y="0.713921386719" />
                  <Point X="-21.102623046875" Y="0.476385314941" />
                  <Point X="-21.3080078125" Y="0.421352813721" />
                  <Point X="-21.317271484375" Y="0.418353729248" />
                  <Point X="-21.34434765625" Y="0.407493865967" />
                  <Point X="-21.37104296875" Y="0.394038757324" />
                  <Point X="-21.380609375" Y="0.388868591309" />
                  <Point X="-21.45850390625" Y="0.343843597412" />
                  <Point X="-21.469224609375" Y="0.33663168335" />
                  <Point X="-21.489591796875" Y="0.32081729126" />
                  <Point X="-21.49923828125" Y="0.312215026855" />
                  <Point X="-21.5223203125" Y="0.28862097168" />
                  <Point X="-21.53597265625" Y="0.273051544189" />
                  <Point X="-21.582708984375" Y="0.213497467041" />
                  <Point X="-21.58914453125" Y="0.204207199097" />
                  <Point X="-21.600869140625" Y="0.184926208496" />
                  <Point X="-21.606158203125" Y="0.174935333252" />
                  <Point X="-21.615931640625" Y="0.153471511841" />
                  <Point X="-21.619994140625" Y="0.142925094604" />
                  <Point X="-21.626837890625" Y="0.121430656433" />
                  <Point X="-21.63254296875" Y="0.095219825745" />
                  <Point X="-21.64812109375" Y="0.01387183857" />
                  <Point X="-21.64969140625" Y="0.000924408793" />
                  <Point X="-21.651041015625" Y="-0.025066610336" />
                  <Point X="-21.6508203125" Y="-0.038109901428" />
                  <Point X="-21.6478984375" Y="-0.072181846619" />
                  <Point X="-21.645203125" Y="-0.091687850952" />
                  <Point X="-21.629623046875" Y="-0.173035690308" />
                  <Point X="-21.62683984375" Y="-0.183988449097" />
                  <Point X="-21.619994140625" Y="-0.205488388062" />
                  <Point X="-21.615931640625" Y="-0.216035858154" />
                  <Point X="-21.606158203125" Y="-0.237498626709" />
                  <Point X="-21.6008671875" Y="-0.247491287231" />
                  <Point X="-21.589140625" Y="-0.266773620605" />
                  <Point X="-21.573935546875" Y="-0.287237060547" />
                  <Point X="-21.52719921875" Y="-0.346791290283" />
                  <Point X="-21.51826953125" Y="-0.356659393311" />
                  <Point X="-21.4991171875" Y="-0.375052062988" />
                  <Point X="-21.48889453125" Y="-0.383576782227" />
                  <Point X="-21.459966796875" Y="-0.404444763184" />
                  <Point X="-21.443888671875" Y="-0.414851348877" />
                  <Point X="-21.365994140625" Y="-0.459876190186" />
                  <Point X="-21.36050390625" Y="-0.462814178467" />
                  <Point X="-21.340845703125" Y="-0.472014831543" />
                  <Point X="-21.316974609375" Y="-0.481027008057" />
                  <Point X="-21.3080078125" Y="-0.483912841797" />
                  <Point X="-20.21512109375" Y="-0.776751342773" />
                  <Point X="-20.2383828125" Y="-0.931037414551" />
                  <Point X="-20.249728515625" Y="-0.980754211426" />
                  <Point X="-20.272197265625" Y="-1.079219604492" />
                  <Point X="-21.325029296875" Y="-0.940611572266" />
                  <Point X="-21.563216796875" Y="-0.909253662109" />
                  <Point X="-21.571375" Y="-0.908535644531" />
                  <Point X="-21.59990234375" Y="-0.908042358398" />
                  <Point X="-21.63327734375" Y="-0.910840942383" />
                  <Point X="-21.645517578125" Y="-0.912676269531" />
                  <Point X="-21.674201171875" Y="-0.918910705566" />
                  <Point X="-21.82708203125" Y="-0.952139953613" />
                  <Point X="-21.842123046875" Y="-0.956741943359" />
                  <Point X="-21.8712421875" Y="-0.96836541748" />
                  <Point X="-21.8853203125" Y="-0.975386962891" />
                  <Point X="-21.9131484375" Y="-0.992279418945" />
                  <Point X="-21.925875" Y="-1.001529907227" />
                  <Point X="-21.949625" Y="-1.022000915527" />
                  <Point X="-21.9606484375" Y="-1.033221191406" />
                  <Point X="-21.977986328125" Y="-1.054072631836" />
                  <Point X="-22.070392578125" Y="-1.165209106445" />
                  <Point X="-22.078673828125" Y="-1.176848999023" />
                  <Point X="-22.093396484375" Y="-1.201236206055" />
                  <Point X="-22.099837890625" Y="-1.213983764648" />
                  <Point X="-22.1111796875" Y="-1.241368896484" />
                  <Point X="-22.11563671875" Y="-1.254935791016" />
                  <Point X="-22.122466796875" Y="-1.282583129883" />
                  <Point X="-22.12483984375" Y="-1.296663818359" />
                  <Point X="-22.12732421875" Y="-1.323667480469" />
                  <Point X="-22.140568359375" Y="-1.467594116211" />
                  <Point X="-22.140708984375" Y="-1.483317016602" />
                  <Point X="-22.138392578125" Y="-1.514580688477" />
                  <Point X="-22.135935546875" Y="-1.530121337891" />
                  <Point X="-22.128205078125" Y="-1.561742675781" />
                  <Point X="-22.12321484375" Y="-1.576663330078" />
                  <Point X="-22.11084375" Y="-1.605475830078" />
                  <Point X="-22.103462890625" Y="-1.619367553711" />
                  <Point X="-22.08758984375" Y="-1.644058349609" />
                  <Point X="-22.002982421875" Y="-1.775658081055" />
                  <Point X="-21.998255859375" Y="-1.782354370117" />
                  <Point X="-21.98019921875" Y="-1.804457397461" />
                  <Point X="-21.956505859375" Y="-1.828123291016" />
                  <Point X="-21.947201171875" Y="-1.836277709961" />
                  <Point X="-20.931333984375" Y="-2.615781005859" />
                  <Point X="-20.912826171875" Y="-2.629982177734" />
                  <Point X="-20.954505859375" Y="-2.697424804688" />
                  <Point X="-20.97796875" Y="-2.730762939453" />
                  <Point X="-20.998724609375" Y="-2.760252929688" />
                  <Point X="-21.93940625" Y="-2.217149169922" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.184970703125" Y="-2.079513916016" />
                  <Point X="-22.216876953125" Y="-2.069326416016" />
                  <Point X="-22.228888671875" Y="-2.066337646484" />
                  <Point X="-22.263025390625" Y="-2.060172363281" />
                  <Point X="-22.444978515625" Y="-2.027312011719" />
                  <Point X="-22.460638671875" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503295898" />
                  <Point X="-22.539859375" Y="-2.03146105957" />
                  <Point X="-22.55515625" Y="-2.035136230469" />
                  <Point X="-22.5849296875" Y="-2.044959228516" />
                  <Point X="-22.59940625" Y="-2.051107177734" />
                  <Point X="-22.627767578125" Y="-2.066032958984" />
                  <Point X="-22.778923828125" Y="-2.145586181641" />
                  <Point X="-22.79102734375" Y="-2.153168457031" />
                  <Point X="-22.8139609375" Y="-2.170062988281" />
                  <Point X="-22.824791015625" Y="-2.179375244141" />
                  <Point X="-22.84575" Y="-2.200334716797" />
                  <Point X="-22.855060546875" Y="-2.211162353516" />
                  <Point X="-22.871953125" Y="-2.234092529297" />
                  <Point X="-22.87953515625" Y="-2.246195068359" />
                  <Point X="-22.8944609375" Y="-2.274555419922" />
                  <Point X="-22.974015625" Y="-2.425713134766" />
                  <Point X="-22.9801640625" Y="-2.440193115234" />
                  <Point X="-22.98998828125" Y="-2.469970947266" />
                  <Point X="-22.9936640625" Y="-2.485268798828" />
                  <Point X="-22.99862109375" Y="-2.517442138672" />
                  <Point X="-22.999720703125" Y="-2.533139404297" />
                  <Point X="-22.999314453125" Y="-2.564491455078" />
                  <Point X="-22.99780859375" Y="-2.580146240234" />
                  <Point X="-22.991642578125" Y="-2.614284179688" />
                  <Point X="-22.95878125" Y="-2.796236572266" />
                  <Point X="-22.95698046875" Y="-2.804222900391" />
                  <Point X="-22.94876171875" Y="-2.831540771484" />
                  <Point X="-22.935927734375" Y="-2.862479248047" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.27765234375" Y="-4.004256347656" />
                  <Point X="-22.264103515625" Y="-4.02772265625" />
                  <Point X="-22.276244140625" Y="-4.036082763672" />
                  <Point X="-23.004201171875" Y="-3.087390625" />
                  <Point X="-23.16608203125" Y="-2.876422851562" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849626464844" />
                  <Point X="-23.21674609375" Y="-2.828004882812" />
                  <Point X="-23.22669921875" Y="-2.820645996094" />
                  <Point X="-23.260369140625" Y="-2.798999755859" />
                  <Point X="-23.43982421875" Y="-2.683627197266" />
                  <Point X="-23.45371875" Y="-2.676244140625" />
                  <Point X="-23.48253125" Y="-2.663873046875" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.628427734375" Y="-2.649904785156" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920123046875" Y="-2.699413330078" />
                  <Point X="-23.944505859375" Y="-2.714134521484" />
                  <Point X="-23.956142578125" Y="-2.722413818359" />
                  <Point X="-23.984576171875" Y="-2.746055664062" />
                  <Point X="-24.136126953125" Y="-2.872064208984" />
                  <Point X="-24.147349609375" Y="-2.883091552734" />
                  <Point X="-24.167818359375" Y="-2.906841308594" />
                  <Point X="-24.177064453125" Y="-2.919563720703" />
                  <Point X="-24.19395703125" Y="-2.947390625" />
                  <Point X="-24.200978515625" Y="-2.961466064453" />
                  <Point X="-24.212603515625" Y="-2.990585693359" />
                  <Point X="-24.21720703125" Y="-3.005629882812" />
                  <Point X="-24.225708984375" Y="-3.044743896484" />
                  <Point X="-24.271021484375" Y="-3.253217773438" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677490234" />
                  <Point X="-24.2752578125" Y="-3.323169677734" />
                  <Point X="-24.2744453125" Y="-3.335519775391" />
                  <Point X="-24.16691015625" Y="-4.15232421875" />
                  <Point X="-24.2816484375" Y="-3.724115722656" />
                  <Point X="-24.344931640625" Y="-3.487936767578" />
                  <Point X="-24.347392578125" Y="-3.480120849609" />
                  <Point X="-24.357853515625" Y="-3.453578369141" />
                  <Point X="-24.3732109375" Y="-3.423815673828" />
                  <Point X="-24.37958984375" Y="-3.413211914062" />
                  <Point X="-24.405455078125" Y="-3.375944091797" />
                  <Point X="-24.543318359375" Y="-3.177311523438" />
                  <Point X="-24.553326171875" Y="-3.165175292969" />
                  <Point X="-24.5752109375" Y="-3.142718017578" />
                  <Point X="-24.587087890625" Y="-3.132396972656" />
                  <Point X="-24.61334375" Y="-3.113153320312" />
                  <Point X="-24.626759765625" Y="-3.104936279297" />
                  <Point X="-24.654759765625" Y="-3.090829101562" />
                  <Point X="-24.66934375" Y="-3.084938964844" />
                  <Point X="-24.709369140625" Y="-3.072516357422" />
                  <Point X="-24.922703125" Y="-3.006305664062" />
                  <Point X="-24.936623046875" Y="-3.003109619141" />
                  <Point X="-24.964783203125" Y="-2.998840087891" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.105009765625" Y="-3.018727539062" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.332931640625" Y="-3.090830078125" />
                  <Point X="-25.360931640625" Y="-3.104938232422" />
                  <Point X="-25.37434375" Y="-3.113154785156" />
                  <Point X="-25.400599609375" Y="-3.132398925781" />
                  <Point X="-25.412474609375" Y="-3.142717773438" />
                  <Point X="-25.434359375" Y="-3.165174072266" />
                  <Point X="-25.444369140625" Y="-3.177311523438" />
                  <Point X="-25.470234375" Y="-3.214579101563" />
                  <Point X="-25.608095703125" Y="-3.413211914062" />
                  <Point X="-25.61246875" Y="-3.420129638672" />
                  <Point X="-25.625974609375" Y="-3.445260742188" />
                  <Point X="-25.63877734375" Y="-3.476213623047" />
                  <Point X="-25.64275390625" Y="-3.487936279297" />
                  <Point X="-25.98005078125" Y="-4.746747070312" />
                  <Point X="-25.98542578125" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.330962717995" Y="-1.071482982245" />
                  <Point X="-20.308601544183" Y="-0.751703298447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.425325985934" Y="-1.059059816445" />
                  <Point X="-20.402081994615" Y="-0.72665525412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.302974274583" Y="0.690651173527" />
                  <Point X="-20.278409382789" Y="1.041945492694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.519689253873" Y="-1.046636650645" />
                  <Point X="-20.495562445048" Y="-0.701607209793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.400024677294" Y="0.664646521755" />
                  <Point X="-20.369473093054" Y="1.101554531597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.614052521812" Y="-1.034213484845" />
                  <Point X="-20.58904289548" Y="-0.676559165467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.497075080006" Y="0.638641869983" />
                  <Point X="-20.465589930252" Y="1.088900488659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.708415789751" Y="-1.021790319046" />
                  <Point X="-20.682523345913" Y="-0.65151112114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.594125482718" Y="0.612637218212" />
                  <Point X="-20.561706767451" Y="1.076246445721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.916521641334" Y="-2.635961881113" />
                  <Point X="-20.915936608268" Y="-2.627595518488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.80277905769" Y="-1.009367153246" />
                  <Point X="-20.776003796345" Y="-0.626463076813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.69117588543" Y="0.58663256644" />
                  <Point X="-20.657823604649" Y="1.063592402783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.019602028279" Y="-2.748199324928" />
                  <Point X="-21.006318982493" Y="-2.558242920276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.897142325629" Y="-0.996943987446" />
                  <Point X="-20.869484246778" Y="-0.601415032487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.788226288142" Y="0.560627914668" />
                  <Point X="-20.753940441848" Y="1.050938359844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.111138468742" Y="-2.69535064284" />
                  <Point X="-21.096701335178" Y="-2.488890014034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.991505593568" Y="-0.984520821646" />
                  <Point X="-20.96296469721" Y="-0.57636698816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.885276690854" Y="0.534623262897" />
                  <Point X="-20.850057279047" Y="1.038284316906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.202674909206" Y="-2.642501960752" />
                  <Point X="-21.187083687863" Y="-2.419537107792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.085868861508" Y="-0.972097655847" />
                  <Point X="-21.056445147643" Y="-0.551318943833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.982327093565" Y="0.508618611125" />
                  <Point X="-20.946174116245" Y="1.025630273968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.29421134967" Y="-2.589653278664" />
                  <Point X="-21.277466040548" Y="-2.350184201549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.180232129447" Y="-0.959674490047" />
                  <Point X="-21.149925598075" Y="-0.526270899507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.079377496277" Y="0.482613959353" />
                  <Point X="-21.042290953444" Y="1.01297623103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.9437928478" Y="2.421564766754" />
                  <Point X="-20.931555057639" Y="2.596573319568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.385747790134" Y="-2.536804596576" />
                  <Point X="-21.367848393234" Y="-2.280831295307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.274595397386" Y="-0.947251324247" />
                  <Point X="-21.243406048508" Y="-0.50122285518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.176427890307" Y="0.45660943174" />
                  <Point X="-21.138407790642" Y="1.000322188092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.04442439084" Y="2.344347422334" />
                  <Point X="-21.017941816342" Y="2.723065881844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.477284230598" Y="-2.483955914487" />
                  <Point X="-21.458230745919" Y="-2.211478389065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.368958666398" Y="-0.934828173797" />
                  <Point X="-21.336704920791" Y="-0.473578122345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.273478281602" Y="0.430604943232" />
                  <Point X="-21.234524627841" Y="0.987668145154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.14505593388" Y="2.267130077913" />
                  <Point X="-21.111209865513" Y="2.751151405729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.568820671062" Y="-2.431107232399" />
                  <Point X="-21.548613098604" Y="-2.142125482823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.463321936643" Y="-0.922405040969" />
                  <Point X="-21.428454176787" Y="-0.423772844146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.371276026432" Y="0.393912801257" />
                  <Point X="-21.330641465039" Y="0.975014102216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.24568747692" Y="2.189912733492" />
                  <Point X="-21.210448329617" Y="2.693856018238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.660357111526" Y="-2.378258550311" />
                  <Point X="-21.638995451289" Y="-2.072772576581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.557685206888" Y="-0.909981908142" />
                  <Point X="-21.518941222271" Y="-0.355917114681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.470587491291" Y="0.335573454323" />
                  <Point X="-21.426758302238" Y="0.962360059278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.346319019959" Y="2.112695389071" />
                  <Point X="-21.309686793721" Y="2.636560630748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.75189355199" Y="-2.325409868223" />
                  <Point X="-21.729377803974" Y="-2.003419670339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.653222703437" Y="-0.914350993812" />
                  <Point X="-21.605923562877" Y="-0.237941770428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.57353874471" Y="0.225182705963" />
                  <Point X="-21.522743460167" Y="0.951589117625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.446950562999" Y="2.03547804465" />
                  <Point X="-21.408925257825" Y="2.579265243258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.843429992453" Y="-2.272561186135" />
                  <Point X="-21.81976015666" Y="-1.934066764097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.749924437472" Y="-0.935369451192" />
                  <Point X="-21.617379594442" Y="0.960110113024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.547582106039" Y="1.958260700229" />
                  <Point X="-21.508163721929" Y="2.521969855767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.934966432917" Y="-2.219712504047" />
                  <Point X="-21.910142509345" Y="-1.864713857854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.846780942251" Y="-0.958601233338" />
                  <Point X="-21.711171855104" Y="0.980699063324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.648213649062" Y="1.881043356056" />
                  <Point X="-21.607402186033" Y="2.464674468277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.026502873946" Y="-2.166863830042" />
                  <Point X="-21.99949281116" Y="-1.780601936565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.946242381267" Y="-1.019085310636" />
                  <Point X="-21.803085130572" Y="1.028158753779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.748845187535" Y="1.803826076934" />
                  <Point X="-21.706640650137" Y="2.407379080786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.118039315004" Y="-2.114015156449" />
                  <Point X="-22.085413449837" Y="-1.647443547362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.049975221183" Y="-1.140653266647" />
                  <Point X="-21.892771418603" Y="1.107465848342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.849756323099" Y="1.722610373145" />
                  <Point X="-21.805879114241" Y="2.350083693296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.341709473357" Y="-3.95076667515" />
                  <Point X="-22.338125516532" Y="-3.899513704715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.210293348088" Y="-2.071428526928" />
                  <Point X="-21.977008948494" Y="1.26469381456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.955071539572" Y="1.578413378095" />
                  <Point X="-21.905117578345" Y="2.292788305806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.428987749316" Y="-3.837023403604" />
                  <Point X="-22.423069366823" Y="-3.752386590793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.304218014155" Y="-2.052733062137" />
                  <Point X="-22.004356042449" Y="2.235492918315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.516266025274" Y="-3.723280132059" />
                  <Point X="-22.508013217113" Y="-3.605259476871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.39826234296" Y="-2.035748854216" />
                  <Point X="-22.103594506553" Y="2.178197530825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.603544301233" Y="-3.609536860514" />
                  <Point X="-22.592957067404" Y="-3.45813236295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.492774720388" Y="-2.025458053457" />
                  <Point X="-22.202832970657" Y="2.120902143335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.690822577191" Y="-3.495793588969" />
                  <Point X="-22.677900917695" Y="-3.311005249028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.58950626507" Y="-2.046902822963" />
                  <Point X="-22.302071434761" Y="2.063606755844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.77810085315" Y="-3.382050317424" />
                  <Point X="-22.762844767986" Y="-3.163878135106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.688303830363" Y="-2.097893063697" />
                  <Point X="-22.40097379674" Y="2.011117852674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.280682100102" Y="3.731369259748" />
                  <Point X="-22.264656718688" Y="3.960542890974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.865379129108" Y="-3.268307045879" />
                  <Point X="-22.847788618276" Y="-3.016751021185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.787234912165" Y="-2.150792679483" />
                  <Point X="-22.498101070274" Y="1.984013896918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.389037758339" Y="3.543691921764" />
                  <Point X="-22.355377412766" Y="4.025057289879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.952657405067" Y="-3.154563774333" />
                  <Point X="-22.932691363167" Y="-2.869036072656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.890609495471" Y="-2.267237327279" />
                  <Point X="-22.593887545445" Y="1.976084251086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.497393416575" Y="3.356014583781" />
                  <Point X="-22.446750413015" Y="4.080243275935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.039935673731" Y="-3.040820398473" />
                  <Point X="-22.688151440253" Y="1.989928518873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.605749074812" Y="3.168337245798" />
                  <Point X="-22.538254539935" Y="4.133554063237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.127213931874" Y="-2.927076872148" />
                  <Point X="-22.780430015432" Y="2.032164180085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.714104733049" Y="2.980659907815" />
                  <Point X="-22.629758666854" Y="4.18686485054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.215586658296" Y="-2.828984971415" />
                  <Point X="-22.87079035593" Y="2.10183187526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.822460419673" Y="2.792982163888" />
                  <Point X="-22.721262802909" Y="4.240175507201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.306641628525" Y="-2.769250944194" />
                  <Point X="-22.957105351112" Y="2.229350703702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.930816206648" Y="2.605302984856" />
                  <Point X="-22.813259002546" Y="4.286449326786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.397776521809" Y="-2.710659869897" />
                  <Point X="-22.905475270046" Y="4.329576029324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.489572223511" Y="-2.661518796256" />
                  <Point X="-22.997691537546" Y="4.372702731862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.583750254746" Y="-2.64644662216" />
                  <Point X="-23.08990780594" Y="4.415829421611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.679553034193" Y="-2.65460943001" />
                  <Point X="-23.182413090555" Y="4.454822986834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.775401781536" Y="-2.663429609403" />
                  <Point X="-23.275289455917" Y="4.488509850142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.871716205079" Y="-2.678909268714" />
                  <Point X="-23.368165821279" Y="4.522196713451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.970845164289" Y="-2.734638663259" />
                  <Point X="-23.461042186642" Y="4.555883576759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.071955878245" Y="-2.818708471026" />
                  <Point X="-23.553981269502" Y="4.58867353805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.241085748223" Y="-3.875497528133" />
                  <Point X="-24.227997336422" Y="-3.688324519144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.173939686871" Y="-2.915264114285" />
                  <Point X="-23.647632187811" Y="4.611283778178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.316608444741" Y="-3.593641638352" />
                  <Point X="-23.741283106119" Y="4.633894018306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.397426444776" Y="-3.387512116905" />
                  <Point X="-23.834934024427" Y="4.656504258433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.483941948317" Y="-3.262860691581" />
                  <Point X="-23.928584942736" Y="4.679114498561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.571069880602" Y="-3.146967405338" />
                  <Point X="-24.022235861044" Y="4.701724738689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.662167091823" Y="-3.087837452442" />
                  <Point X="-24.11588676894" Y="4.724335127711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.755330241303" Y="-3.058251793094" />
                  <Point X="-24.209892743796" Y="4.74186782265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.848539341551" Y="-3.029323260339" />
                  <Point X="-24.304432388707" Y="4.751768680251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.941882524393" Y="-3.00231219801" />
                  <Point X="-24.398972033617" Y="4.761669537851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.037021354905" Y="-3.000980093731" />
                  <Point X="-24.493511678527" Y="4.771570395451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.134126263613" Y="-3.027764217556" />
                  <Point X="-24.588051323437" Y="4.781471253051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.231470883326" Y="-3.057976368473" />
                  <Point X="-24.704657652042" Y="4.47580383174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.328885963666" Y="-3.089196153296" />
                  <Point X="-24.822041157801" Y="4.159022259333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.429049864368" Y="-3.159725900719" />
                  <Point X="-24.921558839094" Y="4.097733880009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.53460277842" Y="-3.307322129513" />
                  <Point X="-25.017577997399" Y="4.086476710324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.642389388868" Y="-3.48686170497" />
                  <Point X="-25.110719046498" Y="4.11637841985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.77122325596" Y="-3.967391073324" />
                  <Point X="-25.199766802509" Y="4.204816947721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.900084167754" Y="-4.448307198937" />
                  <Point X="-25.277493812965" Y="4.455149679539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.017157089437" Y="-4.760647212138" />
                  <Point X="-25.353016681103" Y="4.737003115047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.110885100082" Y="-4.739139443793" />
                  <Point X="-25.445722076973" Y="4.773134955998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.172092811292" Y="-4.252569726646" />
                  <Point X="-25.541739851208" Y="4.761897579436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.254326329014" Y="-4.066683051221" />
                  <Point X="-25.637757625444" Y="4.750660202873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.343986879709" Y="-3.987007895614" />
                  <Point X="-25.733775399679" Y="4.739422826311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.433716272304" Y="-3.908317225045" />
                  <Point X="-25.829793174296" Y="4.728185444295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.523844376377" Y="-3.835328394252" />
                  <Point X="-25.925810956155" Y="4.716947958706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.616576945202" Y="-3.799585144663" />
                  <Point X="-26.022884814063" Y="4.690607881997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.711229020711" Y="-3.791292119531" />
                  <Point X="-26.120021146256" Y="4.663374381393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.806026528013" Y="-3.785078865935" />
                  <Point X="-26.217157478449" Y="4.63614088079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.900824035315" Y="-3.778865612339" />
                  <Point X="-26.314293810642" Y="4.608907380187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.996120173422" Y="-3.779783111469" />
                  <Point X="-26.411430142835" Y="4.581673879584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.09380820598" Y="-3.814906294871" />
                  <Point X="-26.534978672363" Y="4.176728359779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.193694436497" Y="-3.881465173644" />
                  <Point X="-26.636238954975" Y="4.09051962058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.293594138946" Y="-3.948216710024" />
                  <Point X="-26.734322953955" Y="4.049733853535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.394126407596" Y="-4.024014364528" />
                  <Point X="-26.831010367739" Y="4.028920185278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.498623621606" Y="-4.156513379345" />
                  <Point X="-27.406311929782" Y="-2.836394682984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.37833388811" Y="-2.436290046509" />
                  <Point X="-26.924933767211" Y="4.047633763218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.590493285066" Y="-4.108430008122" />
                  <Point X="-27.514667631522" Y="-3.024072643084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.468953300125" Y="-2.370327246631" />
                  <Point X="-27.016947281727" Y="4.0936599685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.681773134962" Y="-4.051911909959" />
                  <Point X="-27.623023288134" Y="-3.211749957833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.562655383987" Y="-2.348448708006" />
                  <Point X="-27.107567834115" Y="4.159606460292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.772798179877" Y="-3.991749930792" />
                  <Point X="-27.731378944746" Y="-3.399427272582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.659200408663" Y="-2.36722611715" />
                  <Point X="-27.195307675402" Y="4.266749040125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.863164693456" Y="-3.922170514779" />
                  <Point X="-27.839734601359" Y="-3.587104587331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.758135178468" Y="-2.420178473827" />
                  <Point X="-27.294388935183" Y="4.211701779186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.953531214085" Y="-3.852591199586" />
                  <Point X="-27.948090257971" Y="-3.77478190208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.857373635051" Y="-2.477473753774" />
                  <Point X="-27.393470194965" Y="4.156654518248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.956612091635" Y="-2.53476903372" />
                  <Point X="-27.492551454746" Y="4.101607257309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.055850548219" Y="-2.592064313667" />
                  <Point X="-27.978534236387" Y="-1.486389541953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.963511143273" Y="-1.271549301199" />
                  <Point X="-27.591632695074" Y="4.046560274563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.155089004803" Y="-2.649359593614" />
                  <Point X="-28.081685123812" Y="-1.599635189621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.050717810334" Y="-1.156781974701" />
                  <Point X="-27.764427324619" Y="2.937362713979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.753323586721" Y="3.096153563862" />
                  <Point X="-27.691374442312" Y="3.982067602949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.254327461386" Y="-2.70665487356" />
                  <Point X="-28.182316695432" Y="-1.676852942754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.142742387651" Y="-1.110913974836" />
                  <Point X="-27.869062253326" Y="2.802894287234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.833674137409" Y="3.308967922423" />
                  <Point X="-27.792001240304" Y="3.904918115868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.35356591797" Y="-2.763950153507" />
                  <Point X="-28.282948255696" Y="-1.754070533488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.237200971504" Y="-1.099853890114" />
                  <Point X="-27.968904258127" Y="2.736967865667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.918618035969" Y="3.456094346057" />
                  <Point X="-27.892628038296" Y="3.827768628787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.452804374554" Y="-2.821245433453" />
                  <Point X="-28.383579787982" Y="-1.83128772413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.333275767485" Y="-1.111906715626" />
                  <Point X="-28.065076087631" Y="2.723527396123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.003561934529" Y="3.603220769691" />
                  <Point X="-27.993254836289" Y="3.750619141707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.552042831138" Y="-2.8785407134" />
                  <Point X="-28.484211320269" Y="-1.908504914773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.429392603422" Y="-1.124560740525" />
                  <Point X="-28.366303033763" Y="-0.222337860554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.341793432129" Y="0.128165772494" />
                  <Point X="-28.159715820508" Y="2.731996929106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.651281287722" Y="-2.935835993346" />
                  <Point X="-28.584842852555" Y="-1.985722105415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.525509439359" Y="-1.137214765424" />
                  <Point X="-28.466657898689" Y="-0.295598523602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.431077177113" Y="0.213229500828" />
                  <Point X="-28.252867583563" Y="2.761745421922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.750519744305" Y="-2.993131273293" />
                  <Point X="-28.685474384842" Y="-2.062939296057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.621626275296" Y="-1.149868790323" />
                  <Point X="-28.564094406711" Y="-0.32712473856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.523480416572" Y="0.25368237977" />
                  <Point X="-28.44164036109" Y="1.42404969965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.430345413607" Y="1.585574973985" />
                  <Point X="-28.344410112741" Y="2.814507031351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.842894559756" Y="-2.952271912083" />
                  <Point X="-28.786105917128" Y="-2.1401564867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.717743111233" Y="-1.162522815222" />
                  <Point X="-28.661144802136" Y="-0.353129286122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.616960868437" Y="0.278730403611" />
                  <Point X="-28.547841485189" Y="1.267183635318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.514666064176" Y="1.741614259143" />
                  <Point X="-28.435946555014" Y="2.867355687568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.93011369061" Y="-2.837682826136" />
                  <Point X="-28.886737449415" Y="-2.217373677342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.813859947171" Y="-1.175176840121" />
                  <Point X="-28.75819519811" Y="-0.379133841538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.710441320302" Y="0.303778427452" />
                  <Point X="-28.646118939994" Y="1.223631321082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.604676622092" Y="1.816284078301" />
                  <Point X="-28.527483002184" Y="2.920204273754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.017030259669" Y="-2.718766904947" />
                  <Point X="-28.987368981702" Y="-2.294590867984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.909976783108" Y="-1.18783086502" />
                  <Point X="-28.855245596973" Y="-0.405138438267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.803921772167" Y="0.328826451294" />
                  <Point X="-28.74268819622" Y="1.204507384619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.695058972734" Y="1.885637013752" />
                  <Point X="-28.619019449354" Y="2.973052859939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.102267782417" Y="-2.575839502819" />
                  <Point X="-29.088000513988" Y="-2.371808058626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.006093619045" Y="-1.200484889919" />
                  <Point X="-28.952295995835" Y="-0.431143034997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.897402224032" Y="0.353874475135" />
                  <Point X="-28.837298081299" Y="1.213403761006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.785441323377" Y="1.954989949203" />
                  <Point X="-28.710555896524" Y="3.025901446125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.102210454982" Y="-1.213138914817" />
                  <Point X="-29.049346394698" Y="-0.457147631726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.990882675897" Y="0.378922498977" />
                  <Point X="-28.931661345544" Y="1.225826979633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.87582367402" Y="2.024342884654" />
                  <Point X="-28.812934073216" Y="2.923706076769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.198327290919" Y="-1.225792939716" />
                  <Point X="-29.146396793561" Y="-0.483152228455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.084363127762" Y="0.403970522818" />
                  <Point X="-29.026024612714" Y="1.23825015643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.966206024662" Y="2.093695820105" />
                  <Point X="-28.917570917279" Y="2.789210259153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.294444126856" Y="-1.238446964615" />
                  <Point X="-29.243447192424" Y="-0.509156825185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.177843579627" Y="0.42901854666" />
                  <Point X="-29.120387880412" Y="1.250673325675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.056588375305" Y="2.163048755556" />
                  <Point X="-29.022885023661" Y="2.64502913915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.390560962793" Y="-1.251100989514" />
                  <Point X="-29.340497591287" Y="-0.535161421914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.271324031492" Y="0.454066570501" />
                  <Point X="-29.214751148111" Y="1.26309649492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.146970725948" Y="2.232401691007" />
                  <Point X="-29.131078745866" Y="2.45966759432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.486677798731" Y="-1.263755014413" />
                  <Point X="-29.43754799015" Y="-0.561166018643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.364804483357" Y="0.479114594343" />
                  <Point X="-29.309114415809" Y="1.275519664165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.582794634668" Y="-1.276409039312" />
                  <Point X="-29.534598389013" Y="-0.587170615373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.458284935222" Y="0.504162618184" />
                  <Point X="-29.403477683507" Y="1.28794283341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.674914281301" Y="-1.231900594018" />
                  <Point X="-29.631648787876" Y="-0.613175212102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.551765387087" Y="0.529210642025" />
                  <Point X="-29.497840951205" Y="1.300366002655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.748390257316" Y="-0.9207752374" />
                  <Point X="-29.728699186738" Y="-0.639179808831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.645245838952" Y="0.554258665867" />
                  <Point X="-29.592204218903" Y="1.3127891719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.738726290817" Y="0.579306689708" />
                  <Point X="-29.705652005389" Y="1.052291007297" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.46517578125" Y="-3.773291259766" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521543945312" />
                  <Point X="-24.561544921875" Y="-3.484276123047" />
                  <Point X="-24.699408203125" Y="-3.285643554688" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.765689453125" Y="-3.253977294922" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.048689453125" Y="-3.200188964844" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285644042969" />
                  <Point X="-25.31414453125" Y="-3.322911621094" />
                  <Point X="-25.452005859375" Y="-3.521544433594" />
                  <Point X="-25.4592265625" Y="-3.537112060547" />
                  <Point X="-25.7965234375" Y="-4.795922851563" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.8493359375" Y="-4.986767578125" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.1455" Y="-4.926421386719" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.316767578125" Y="-4.608903320312" />
                  <Point X="-26.3091484375" Y="-4.551039550781" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.319244140625" Y="-4.486685058594" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.4231328125" Y="-4.170311523438" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.6981484375" Y="-3.982557128906" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791259766" />
                  <Point X="-27.0306328125" Y="-4.001021972656" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.449099609375" Y="-4.404081054688" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.488048828125" Y="-4.395333984375" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-27.9185" Y="-4.119359375" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.623638671875" Y="-2.832819335938" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.50025" Y="-2.613671386719" />
                  <Point X="-27.50439453125" Y="-2.582190185547" />
                  <Point X="-27.51671875" Y="-2.566024169922" />
                  <Point X="-27.531326171875" Y="-2.551416259766" />
                  <Point X="-27.560154296875" Y="-2.537198974609" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.6773828125" Y="-3.170298828125" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-28.871134765625" Y="-3.228877685547" />
                  <Point X="-29.161697265625" Y="-2.847135742188" />
                  <Point X="-29.20662890625" Y="-2.771794677734" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-28.36955859375" Y="-1.581039672852" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.1525390625" Y="-1.411088012695" />
                  <Point X="-28.1446015625" Y="-1.391312133789" />
                  <Point X="-28.138115234375" Y="-1.366264282227" />
                  <Point X="-28.136650390625" Y="-1.350049682617" />
                  <Point X="-28.14865625" Y="-1.321068115234" />
                  <Point X="-28.165341796875" Y="-1.308176757812" />
                  <Point X="-28.187640625" Y="-1.295052734375" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.59012109375" Y="-1.469013183594" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.813748046875" Y="-1.456100952148" />
                  <Point X="-29.927392578125" Y="-1.011187866211" />
                  <Point X="-29.93927734375" Y="-0.92808404541" />
                  <Point X="-29.99839453125" Y="-0.5147421875" />
                  <Point X="-28.791869140625" Y="-0.191454208374" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.53751171875" Y="-0.118382423401" />
                  <Point X="-28.514142578125" Y="-0.102163330078" />
                  <Point X="-28.502322265625" Y="-0.090645225525" />
                  <Point X="-28.493435546875" Y="-0.07119808197" />
                  <Point X="-28.485646484375" Y="-0.046099964142" />
                  <Point X="-28.487107421875" Y="-0.011751200676" />
                  <Point X="-28.494896484375" Y="0.013346917152" />
                  <Point X="-28.502322265625" Y="0.028085149765" />
                  <Point X="-28.51852734375" Y="0.042646335602" />
                  <Point X="-28.541896484375" Y="0.058865276337" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.806822265625" Y="0.400850463867" />
                  <Point X="-29.998185546875" Y="0.45212612915" />
                  <Point X="-29.99088671875" Y="0.501456359863" />
                  <Point X="-29.91764453125" Y="0.996414855957" />
                  <Point X="-29.893716796875" Y="1.084719726562" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-28.92129296875" Y="1.41610144043" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.722" Y="1.398926391602" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.65153125" Y="1.426058105469" />
                  <Point X="-28.6391171875" Y="1.443791259766" />
                  <Point X="-28.6352265625" Y="1.453186157227" />
                  <Point X="-28.61447265625" Y="1.503289916992" />
                  <Point X="-28.610712890625" Y="1.524602905273" />
                  <Point X="-28.6163125" Y="1.54550769043" />
                  <Point X="-28.621013671875" Y="1.554538208008" />
                  <Point X="-28.646056640625" Y="1.602642578125" />
                  <Point X="-28.65996875" Y="1.619221923828" />
                  <Point X="-29.376603515625" Y="2.169115966797" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.44460546875" Y="2.299432617188" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-29.096630859375" Y="2.868475830078" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.262353515625" Y="2.986524902344" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.124998046875" Y="2.919252441406" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927404541016" />
                  <Point X="-28.003658203125" Y="2.936997558594" />
                  <Point X="-27.95252734375" Y="2.988127929688" />
                  <Point X="-27.9408984375" Y="3.006384277344" />
                  <Point X="-27.93807421875" Y="3.027845458984" />
                  <Point X="-27.939255859375" Y="3.04135546875" />
                  <Point X="-27.945556640625" Y="3.113389892578" />
                  <Point X="-27.95206640625" Y="3.134032470703" />
                  <Point X="-28.269630859375" Y="3.684067871094" />
                  <Point X="-28.30727734375" Y="3.749276611328" />
                  <Point X="-28.248544921875" Y="3.794307373047" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.653048828125" Y="4.229792480469" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-26.99941796875" Y="4.328744628906" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.936203125" Y="4.265830078125" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.798140625" Y="4.228740234375" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.69690234375" Y="4.275744628906" />
                  <Point X="-26.68608203125" Y="4.294488769531" />
                  <Point X="-26.680982421875" Y="4.310662597656" />
                  <Point X="-26.653802734375" Y="4.396865234375" />
                  <Point X="-26.651916015625" Y="4.418426269531" />
                  <Point X="-26.68801953125" Y="4.692658691406" />
                  <Point X="-26.68913671875" Y="4.701141113281" />
                  <Point X="-26.609767578125" Y="4.723393554688" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.847076171875" Y="4.917459472656" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.07266796875" Y="4.424838867188" />
                  <Point X="-25.042140625" Y="4.310903808594" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.782830078125" Y="4.918166992188" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.70006640625" Y="4.984241210938" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-24.039671875" Y="4.901393066406" />
                  <Point X="-23.491544921875" Y="4.76905859375" />
                  <Point X="-23.426650390625" Y="4.745520996094" />
                  <Point X="-23.06896484375" Y="4.615785644531" />
                  <Point X="-23.005947265625" Y="4.586314941406" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.600416015625" Y="4.389663085938" />
                  <Point X="-22.26747265625" Y="4.195688964844" />
                  <Point X="-22.21005859375" Y="4.154860351562" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.634025390625" Y="2.739364746094" />
                  <Point X="-22.77014453125" Y="2.50359765625" />
                  <Point X="-22.7785390625" Y="2.478831298828" />
                  <Point X="-22.7966171875" Y="2.411228515625" />
                  <Point X="-22.7966328125" Y="2.381358154297" />
                  <Point X="-22.789583984375" Y="2.322901123047" />
                  <Point X="-22.781318359375" Y="2.300812011719" />
                  <Point X="-22.77453125" Y="2.290810546875" />
                  <Point X="-22.738359375" Y="2.237503417969" />
                  <Point X="-22.71505859375" Y="2.217416748047" />
                  <Point X="-22.66175" Y="2.181245849609" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.6286953125" Y="2.171657714844" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.53865234375" Y="2.169338134766" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.202349609375" Y="2.917925048828" />
                  <Point X="-21.005751953125" Y="3.031430664062" />
                  <Point X="-20.994896484375" Y="3.016344970703" />
                  <Point X="-20.79740234375" Y="2.741874511719" />
                  <Point X="-20.7653984375" Y="2.688985351562" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.53216796875" Y="1.730599243164" />
                  <Point X="-21.7113828125" Y="1.593082641602" />
                  <Point X="-21.7297578125" Y="1.571923950195" />
                  <Point X="-21.77841015625" Y="1.508451416016" />
                  <Point X="-21.790279296875" Y="1.479342041016" />
                  <Point X="-21.808404296875" Y="1.414536499023" />
                  <Point X="-21.80921875" Y="1.390959716797" />
                  <Point X="-21.806427734375" Y="1.3774375" />
                  <Point X="-21.79155078125" Y="1.305333007812" />
                  <Point X="-21.77676171875" Y="1.276415527344" />
                  <Point X="-21.736296875" Y="1.214909912109" />
                  <Point X="-21.719052734375" Y="1.19882019043" />
                  <Point X="-21.708048828125" Y="1.192626831055" />
                  <Point X="-21.64941015625" Y="1.159617675781" />
                  <Point X="-21.61655859375" Y="1.151653564453" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.33049609375" Y="1.298325683594" />
                  <Point X="-20.151025390625" Y="1.321953125" />
                  <Point X="-20.1456328125" Y="1.299801269531" />
                  <Point X="-20.060806640625" Y="0.951366333008" />
                  <Point X="-20.05072265625" Y="0.886598754883" />
                  <Point X="-20.002140625" Y="0.574556396484" />
                  <Point X="-21.053447265625" Y="0.29285949707" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.28552734375" Y="0.22437171936" />
                  <Point X="-21.363421875" Y="0.179346832275" />
                  <Point X="-21.38650390625" Y="0.155752685547" />
                  <Point X="-21.433240234375" Y="0.096198562622" />
                  <Point X="-21.443013671875" Y="0.074734649658" />
                  <Point X="-21.4459375" Y="0.059472068787" />
                  <Point X="-21.461515625" Y="-0.021875907898" />
                  <Point X="-21.45859375" Y="-0.055947822571" />
                  <Point X="-21.443013671875" Y="-0.137295791626" />
                  <Point X="-21.433240234375" Y="-0.158758636475" />
                  <Point X="-21.424470703125" Y="-0.169932342529" />
                  <Point X="-21.377734375" Y="-0.229486465454" />
                  <Point X="-21.348806640625" Y="-0.250354537964" />
                  <Point X="-21.270912109375" Y="-0.295379272461" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.164154296875" Y="-0.59370513916" />
                  <Point X="-20.001931640625" Y="-0.637172607422" />
                  <Point X="-20.00420703125" Y="-0.652271240234" />
                  <Point X="-20.051568359375" Y="-0.966412902832" />
                  <Point X="-20.064490234375" Y="-1.023030456543" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-21.349830078125" Y="-1.128986083984" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.633845703125" Y="-1.104575683594" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.8145546875" Y="-1.154697387695" />
                  <Point X="-21.831892578125" Y="-1.175548950195" />
                  <Point X="-21.924298828125" Y="-1.286685424805" />
                  <Point X="-21.935640625" Y="-1.314070556641" />
                  <Point X="-21.938125" Y="-1.34107421875" />
                  <Point X="-21.951369140625" Y="-1.485000854492" />
                  <Point X="-21.943638671875" Y="-1.516622070312" />
                  <Point X="-21.927765625" Y="-1.541312988281" />
                  <Point X="-21.843158203125" Y="-1.672912719727" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-20.815669921875" Y="-2.465043701172" />
                  <Point X="-20.660923828125" Y="-2.583784179688" />
                  <Point X="-20.66236328125" Y="-2.586112060547" />
                  <Point X="-20.795865234375" Y="-2.802138427734" />
                  <Point X="-20.82259375" Y="-2.840116210938" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-22.03440625" Y="-2.381694091797" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.296794921875" Y="-2.247147460938" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.539283203125" Y="-2.234170654297" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.334683349609" />
                  <Point X="-22.72632421875" Y="-2.363043701172" />
                  <Point X="-22.80587890625" Y="-2.514201416016" />
                  <Point X="-22.8108359375" Y="-2.546374755859" />
                  <Point X="-22.804669921875" Y="-2.580512695312" />
                  <Point X="-22.77180859375" Y="-2.762465087891" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.113109375" Y="-3.909256347656" />
                  <Point X="-22.01332421875" Y="-4.082087646484" />
                  <Point X="-22.16470703125" Y="-4.190215820312" />
                  <Point X="-22.19458203125" Y="-4.209553222656" />
                  <Point X="-22.320224609375" Y="-4.29087890625" />
                  <Point X="-23.154939453125" Y="-3.203055175781" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.363119140625" Y="-2.958821044922" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.611017578125" Y="-2.83910546875" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.863099609375" Y="-2.892150878906" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045986328125" />
                  <Point X="-24.040044921875" Y="-3.085100341797" />
                  <Point X="-24.085357421875" Y="-3.29357421875" />
                  <Point X="-24.0860703125" Y="-3.310719970703" />
                  <Point X="-23.901072265625" Y="-4.715917480469" />
                  <Point X="-23.872357421875" Y="-4.934028320312" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.03326171875" Y="-4.968262207031" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#132" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.030528837156" Y="4.469059650747" Z="0.4" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.4" />
                  <Point X="-0.859045964258" Y="4.998400702517" Z="0.4" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.4" />
                  <Point X="-1.629421755161" Y="4.802817165748" Z="0.4" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.4" />
                  <Point X="-1.743749236756" Y="4.717412958834" Z="0.4" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.4" />
                  <Point X="-1.73482565947" Y="4.356977565837" Z="0.4" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.4" />
                  <Point X="-1.823431524828" Y="4.306214334649" Z="0.4" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.4" />
                  <Point X="-1.919272926231" Y="4.341460754392" Z="0.4" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.4" />
                  <Point X="-1.965907245416" Y="4.390462892805" Z="0.4" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.4" />
                  <Point X="-2.683489695037" Y="4.304779848199" Z="0.4" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.4" />
                  <Point X="-3.285123066913" Y="3.865267279419" Z="0.4" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.4" />
                  <Point X="-3.319087842277" Y="3.690348398154" Z="0.4" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.4" />
                  <Point X="-2.995222443605" Y="3.068278669706" Z="0.4" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.4" />
                  <Point X="-3.045170430908" Y="3.003633136978" Z="0.4" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.4" />
                  <Point X="-3.126797795901" Y="3.000342255568" Z="0.4" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.4" />
                  <Point X="-3.243510950143" Y="3.061106106468" Z="0.4" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.4" />
                  <Point X="-4.1422512232" Y="2.93045837439" Z="0.4" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.4" />
                  <Point X="-4.49394420981" Y="2.355917798171" Z="0.4" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.4" />
                  <Point X="-4.413198389528" Y="2.16072827957" Z="0.4" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.4" />
                  <Point X="-3.671520745474" Y="1.562729755556" Z="0.4" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.4" />
                  <Point X="-3.687576091951" Y="1.50360061398" Z="0.4" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.4" />
                  <Point X="-3.743191945802" Y="1.477892230536" Z="0.4" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.4" />
                  <Point X="-3.920923908327" Y="1.496953825991" Z="0.4" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.4" />
                  <Point X="-4.94813293892" Y="1.12907711649" Z="0.4" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.4" />
                  <Point X="-5.046504362885" Y="0.540055446174" Z="0.4" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.4" />
                  <Point X="-4.825921179415" Y="0.38383415917" Z="0.4" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.4" />
                  <Point X="-3.553192267622" Y="0.032849980732" Z="0.4" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.4" />
                  <Point X="-3.541018236225" Y="0.004708911587" Z="0.4" />
                  <Point X="-3.539556741714" Y="0" Z="0.4" />
                  <Point X="-3.54734636491" Y="-0.025098039474" Z="0.4" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.4" />
                  <Point X="-3.572176327501" Y="-0.046026002402" Z="0.4" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.4" />
                  <Point X="-3.810966609357" Y="-0.111877897586" Z="0.4" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.4" />
                  <Point X="-4.994931317408" Y="-0.903883110755" Z="0.4" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.4" />
                  <Point X="-4.868350479233" Y="-1.437195074712" Z="0.4" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.4" />
                  <Point X="-4.589751741561" Y="-1.487305245705" Z="0.4" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.4" />
                  <Point X="-3.196860032493" Y="-1.319987443863" Z="0.4" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.4" />
                  <Point X="-3.199163643371" Y="-1.347497671794" Z="0.4" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.4" />
                  <Point X="-3.40615329321" Y="-1.510091949363" Z="0.4" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.4" />
                  <Point X="-4.255729323935" Y="-2.766124222808" Z="0.4" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.4" />
                  <Point X="-3.917076378774" Y="-3.227880864227" Z="0.4" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.4" />
                  <Point X="-3.658539159899" Y="-3.182319953662" Z="0.4" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.4" />
                  <Point X="-2.558231696556" Y="-2.570098772576" Z="0.4" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.4" />
                  <Point X="-2.673097059506" Y="-2.776539222593" Z="0.4" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.4" />
                  <Point X="-2.955160636368" Y="-4.127696763658" Z="0.4" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.4" />
                  <Point X="-2.520527631943" Y="-4.406564695894" Z="0.4" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.4" />
                  <Point X="-2.415588695114" Y="-4.403239213951" Z="0.4" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.4" />
                  <Point X="-2.009009591475" Y="-4.011315089263" Z="0.4" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.4" />
                  <Point X="-1.70757584091" Y="-4.001170266431" Z="0.4" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.4" />
                  <Point X="-1.462256605416" Y="-4.176623330081" Z="0.4" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.4" />
                  <Point X="-1.374440680063" Y="-4.46516012784" Z="0.4" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.4" />
                  <Point X="-1.372496427572" Y="-4.571095842946" Z="0.4" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.4" />
                  <Point X="-1.164116177721" Y="-4.943564378934" Z="0.4" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.4" />
                  <Point X="-0.865020114528" Y="-5.004571754218" Z="0.4" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.4" />
                  <Point X="-0.7543841194" Y="-4.777583976523" Z="0.4" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.4" />
                  <Point X="-0.279224833474" Y="-3.320140297226" Z="0.4" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.4" />
                  <Point X="-0.040025747755" Y="-3.216661883454" Z="0.4" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.4" />
                  <Point X="0.213333331606" Y="-3.270450161834" Z="0.4" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.4" />
                  <Point X="0.391221025958" Y="-3.481505458542" Z="0.4" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.4" />
                  <Point X="0.480370741001" Y="-3.754952053292" Z="0.4" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.4" />
                  <Point X="0.969519659796" Y="-4.986177060929" Z="0.4" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.4" />
                  <Point X="1.148767733789" Y="-4.947955403336" Z="0.4" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.4" />
                  <Point X="1.14234356295" Y="-4.678111169199" Z="0.4" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.4" />
                  <Point X="1.002658426557" Y="-3.064439993946" Z="0.4" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.4" />
                  <Point X="1.162709863562" Y="-2.899317284961" Z="0.4" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.4" />
                  <Point X="1.387407344699" Y="-2.857615085432" Z="0.4" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.4" />
                  <Point X="1.603684588297" Y="-2.969599015139" Z="0.4" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.4" />
                  <Point X="1.799235186664" Y="-3.202213030684" Z="0.4" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.4" />
                  <Point X="2.82643156981" Y="-4.220247442195" Z="0.4" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.4" />
                  <Point X="3.016616562522" Y="-4.086469021107" Z="0.4" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.4" />
                  <Point X="2.924034301514" Y="-3.852976421443" Z="0.4" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.4" />
                  <Point X="2.238376418643" Y="-2.540346217782" Z="0.4" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.4" />
                  <Point X="2.311764361318" Y="-2.355050505131" Z="0.4" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.4" />
                  <Point X="2.477847873242" Y="-2.247136948649" Z="0.4" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.4" />
                  <Point X="2.688160585731" Y="-2.265071590848" Z="0.4" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.4" />
                  <Point X="2.934437175664" Y="-2.39371519876" Z="0.4" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.4" />
                  <Point X="4.212137738877" Y="-2.837613507429" Z="0.4" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.4" />
                  <Point X="4.374012850382" Y="-2.58111729211" Z="0.4" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.4" />
                  <Point X="4.208610708218" Y="-2.394095976762" Z="0.4" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.4" />
                  <Point X="3.108136419019" Y="-1.482993325571" Z="0.4" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.4" />
                  <Point X="3.105506592394" Y="-1.314375849207" Z="0.4" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.4" />
                  <Point X="3.200398125633" Y="-1.176235696724" Z="0.4" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.4" />
                  <Point X="3.370616269366" Y="-1.122154921547" Z="0.4" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.4" />
                  <Point X="3.637487871427" Y="-1.147278468694" Z="0.4" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.4" />
                  <Point X="4.978099850241" Y="-1.002874077761" Z="0.4" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.4" />
                  <Point X="5.039077108073" Y="-0.628445224462" Z="0.4" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.4" />
                  <Point X="4.842630844309" Y="-0.514128748239" Z="0.4" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.4" />
                  <Point X="3.670057471473" Y="-0.175785798747" Z="0.4" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.4" />
                  <Point X="3.608705577778" Y="-0.107784111933" Z="0.4" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.4" />
                  <Point X="3.584357678071" Y="-0.015262562411" Z="0.4" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.4" />
                  <Point X="3.597013772353" Y="0.081347968827" Z="0.4" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.4" />
                  <Point X="3.646673860622" Y="0.156164626642" Z="0.4" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.4" />
                  <Point X="3.73333794288" Y="0.212363045495" Z="0.4" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.4" />
                  <Point X="3.953336972998" Y="0.275843184529" Z="0.4" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.4" />
                  <Point X="4.992524140223" Y="0.925570785492" Z="0.4" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.4" />
                  <Point X="4.896793203092" Y="1.342908179594" Z="0.4" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.4" />
                  <Point X="4.656822448593" Y="1.379177854064" Z="0.4" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.4" />
                  <Point X="3.38383617128" Y="1.232502593841" Z="0.4" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.4" />
                  <Point X="3.310320627854" Y="1.267477815238" Z="0.4" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.4" />
                  <Point X="3.258853143836" Y="1.335176672761" Z="0.4" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.4" />
                  <Point X="3.236383381685" Y="1.418820907933" Z="0.4" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.4" />
                  <Point X="3.251715625027" Y="1.497154826835" Z="0.4" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.4" />
                  <Point X="3.303769712976" Y="1.572786323576" Z="0.4" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.4" />
                  <Point X="3.492113198361" Y="1.722211632384" Z="0.4" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.4" />
                  <Point X="4.271222067695" Y="2.746151598906" Z="0.4" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.4" />
                  <Point X="4.039532322732" Y="3.076828126456" Z="0.4" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.4" />
                  <Point X="3.766493983716" Y="2.992506349036" Z="0.4" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.4" />
                  <Point X="2.442275305261" Y="2.248920947927" Z="0.4" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.4" />
                  <Point X="2.37113455072" Y="2.252578018927" Z="0.4" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.4" />
                  <Point X="2.306859603296" Y="2.290071625797" Z="0.4" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.4" />
                  <Point X="2.260686974438" Y="2.350165257086" Z="0.4" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.4" />
                  <Point X="2.246851683832" Y="2.418623893113" Z="0.4" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.4" />
                  <Point X="2.26360699473" Y="2.497194270621" Z="0.4" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.4" />
                  <Point X="2.403119003823" Y="2.745645098239" Z="0.4" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.4" />
                  <Point X="2.81276079731" Y="4.226887459588" Z="0.4" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.4" />
                  <Point X="2.418597040398" Y="4.464145982215" Z="0.4" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.4" />
                  <Point X="2.009076708665" Y="4.662886855183" Z="0.4" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.4" />
                  <Point X="1.584241568022" Y="4.823804817821" Z="0.4" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.4" />
                  <Point X="0.965906055851" Y="4.981276723544" Z="0.4" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.4" />
                  <Point X="0.298983015673" Y="5.065249392201" Z="0.4" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.4" />
                  <Point X="0.162715724397" Y="4.962387855204" Z="0.4" />
                  <Point X="0" Y="4.355124473572" Z="0.4" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>