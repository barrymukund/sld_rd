<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#127" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="605" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000473632812" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.397068359375" Y="-3.660416748047" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140869141" />
                  <Point X="-24.45763671875" Y="-3.467376220703" />
                  <Point X="-24.473833984375" Y="-3.444039550781" />
                  <Point X="-24.621365234375" Y="-3.231475830078" />
                  <Point X="-24.643248046875" Y="-3.209020751953" />
                  <Point X="-24.66950390625" Y="-3.189776855469" />
                  <Point X="-24.69750390625" Y="-3.175669677734" />
                  <Point X="-24.72256640625" Y="-3.167890869141" />
                  <Point X="-24.95086328125" Y="-3.097036376953" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097036376953" />
                  <Point X="-25.06188671875" Y="-3.104815185547" />
                  <Point X="-25.29018359375" Y="-3.175669677734" />
                  <Point X="-25.31818359375" Y="-3.189776855469" />
                  <Point X="-25.344439453125" Y="-3.209020751953" />
                  <Point X="-25.366326171875" Y="-3.231479980469" />
                  <Point X="-25.382521484375" Y="-3.25481640625" />
                  <Point X="-25.530052734375" Y="-3.467380371094" />
                  <Point X="-25.5381875" Y="-3.481571044922" />
                  <Point X="-25.550990234375" Y="-3.512523925781" />
                  <Point X="-25.911943359375" Y="-4.859621582031" />
                  <Point X="-25.916583984375" Y="-4.876941894531" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.104365234375" Y="-4.838911132812" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.219732421875" Y="-4.599673339844" />
                  <Point X="-26.2149609375" Y="-4.563439941406" />
                  <Point X="-26.21419921875" Y="-4.547930664062" />
                  <Point X="-26.2165078125" Y="-4.516222167969" />
                  <Point X="-26.22249609375" Y="-4.486120117188" />
                  <Point X="-26.277037109375" Y="-4.211930664062" />
                  <Point X="-26.287939453125" Y="-4.182964355469" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.323642578125" Y="-4.131206054688" />
                  <Point X="-26.346716796875" Y="-4.110969238281" />
                  <Point X="-26.55690234375" Y="-3.926641113281" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.673654296875" Y="-3.888958984375" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708740234" />
                  <Point X="-27.014466796875" Y="-3.882028564453" />
                  <Point X="-27.042658203125" Y="-3.894801513672" />
                  <Point X="-27.068177734375" Y="-3.911853027344" />
                  <Point X="-27.300623046875" Y="-4.067169433594" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.3351015625" Y="-4.099461914062" />
                  <Point X="-27.4801484375" Y="-4.288489257813" />
                  <Point X="-27.801708984375" Y="-4.08938671875" />
                  <Point X="-27.83636328125" Y="-4.062704589844" />
                  <Point X="-28.104720703125" Y="-3.856077148438" />
                  <Point X="-27.497404296875" Y="-2.804173339844" />
                  <Point X="-27.42376171875" Y="-2.676619384766" />
                  <Point X="-27.412857421875" Y="-2.647646972656" />
                  <Point X="-27.406587890625" Y="-2.6161171875" />
                  <Point X="-27.40572265625" Y="-2.584121582031" />
                  <Point X="-27.415533203125" Y="-2.553655029297" />
                  <Point X="-27.431462890625" Y="-2.523114990234" />
                  <Point X="-27.448515625" Y="-2.499877685547" />
                  <Point X="-27.464146484375" Y="-2.484245361328" />
                  <Point X="-27.4893046875" Y="-2.466215332031" />
                  <Point X="-27.518134765625" Y="-2.451997070313" />
                  <Point X="-27.547751953125" Y="-2.443011962891" />
                  <Point X="-27.5786875" Y="-2.444024169922" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.80102734375" Y="-3.131989013672" />
                  <Point X="-28.8180234375" Y="-3.141801513672" />
                  <Point X="-29.082857421875" Y="-2.793862304688" />
                  <Point X="-29.107701171875" Y="-2.752204833984" />
                  <Point X="-29.306142578125" Y="-2.419449951172" />
                  <Point X="-28.234806640625" Y="-1.597385009766" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.08314453125" Y="-1.473361938477" />
                  <Point X="-28.064416015625" Y="-1.443286254883" />
                  <Point X="-28.053091796875" Y="-1.416885009766" />
                  <Point X="-28.046150390625" Y="-1.390080444336" />
                  <Point X="-28.04334765625" Y="-1.359659545898" />
                  <Point X="-28.0455546875" Y="-1.3279921875" />
                  <Point X="-28.0530703125" Y="-1.297026123047" />
                  <Point X="-28.07040234375" Y="-1.270286987305" />
                  <Point X="-28.09385546875" Y="-1.244787109375" />
                  <Point X="-28.115591796875" Y="-1.227224609375" />
                  <Point X="-28.139455078125" Y="-1.213179931641" />
                  <Point X="-28.168716796875" Y="-1.201956176758" />
                  <Point X="-28.200603515625" Y="-1.195474731445" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-29.698646484375" Y="-1.387481079102" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.834078125" Y="-0.992647338867" />
                  <Point X="-29.8406484375" Y="-0.94670300293" />
                  <Point X="-29.892421875" Y="-0.584698364258" />
                  <Point X="-28.67965625" Y="-0.259738372803" />
                  <Point X="-28.532875" Y="-0.220408279419" />
                  <Point X="-28.515091796875" Y="-0.213672729492" />
                  <Point X="-28.496779296875" Y="-0.204547088623" />
                  <Point X="-28.484984375" Y="-0.197564437866" />
                  <Point X="-28.4599765625" Y="-0.180207809448" />
                  <Point X="-28.435953125" Y="-0.156119598389" />
                  <Point X="-28.4157890625" Y="-0.12689919281" />
                  <Point X="-28.403248046875" Y="-0.101100296021" />
                  <Point X="-28.39491796875" Y="-0.074258598328" />
                  <Point X="-28.390685546875" Y="-0.04343756485" />
                  <Point X="-28.391599609375" Y="-0.010846960068" />
                  <Point X="-28.39583203125" Y="0.014649418831" />
                  <Point X="-28.40416796875" Y="0.041507659912" />
                  <Point X="-28.420025390625" Y="0.071819892883" />
                  <Point X="-28.44201953125" Y="0.099983375549" />
                  <Point X="-28.4627265625" Y="0.119556137085" />
                  <Point X="-28.48773046875" Y="0.136909866333" />
                  <Point X="-28.501923828125" Y="0.145046295166" />
                  <Point X="-28.532875" Y="0.157848144531" />
                  <Point X="-29.869859375" Y="0.516092224121" />
                  <Point X="-29.891814453125" Y="0.521975158691" />
                  <Point X="-29.824486328125" Y="0.976974609375" />
                  <Point X="-29.811255859375" Y="1.025804199219" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-28.870880859375" Y="1.313644897461" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744982421875" Y="1.299341796875" />
                  <Point X="-28.723421875" Y="1.301228271484" />
                  <Point X="-28.703134765625" Y="1.305263916016" />
                  <Point X="-28.69705859375" Y="1.30717980957" />
                  <Point X="-28.641708984375" Y="1.324631347656" />
                  <Point X="-28.622775390625" Y="1.332963256836" />
                  <Point X="-28.604029296875" Y="1.343787109375" />
                  <Point X="-28.587345703125" Y="1.356020996094" />
                  <Point X="-28.57370703125" Y="1.371576293945" />
                  <Point X="-28.56129296875" Y="1.389309326172" />
                  <Point X="-28.551345703125" Y="1.407444213867" />
                  <Point X="-28.54891015625" Y="1.413325561523" />
                  <Point X="-28.526701171875" Y="1.466943481445" />
                  <Point X="-28.52091796875" Y="1.486786376953" />
                  <Point X="-28.517158203125" Y="1.508099243164" />
                  <Point X="-28.515802734375" Y="1.528739501953" />
                  <Point X="-28.518947265625" Y="1.54918371582" />
                  <Point X="-28.524546875" Y="1.570088378906" />
                  <Point X="-28.53205078125" Y="1.589383911133" />
                  <Point X="-28.53499609375" Y="1.595039916992" />
                  <Point X="-28.561791015625" Y="1.646509643555" />
                  <Point X="-28.573283203125" Y="1.663707275391" />
                  <Point X="-28.5871953125" Y="1.680286865234" />
                  <Point X="-28.60213671875" Y="1.694590454102" />
                  <Point X="-29.351859375" Y="2.269874023438" />
                  <Point X="-29.08115234375" Y="2.733657226562" />
                  <Point X="-29.04610546875" Y="2.778707519531" />
                  <Point X="-28.75050390625" Y="3.158661621094" />
                  <Point X="-28.27127734375" Y="2.881979980469" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.146794921875" Y="2.825796386719" />
                  <Point X="-28.13833203125" Y="2.825055908203" />
                  <Point X="-28.06124609375" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.999013671875" Y="2.826504638672" />
                  <Point X="-27.980462890625" Y="2.835653564453" />
                  <Point X="-27.962208984375" Y="2.847282714844" />
                  <Point X="-27.946080078125" Y="2.860225341797" />
                  <Point X="-27.940072265625" Y="2.866232421875" />
                  <Point X="-27.88535546875" Y="2.920948730469" />
                  <Point X="-27.872404296875" Y="2.937085693359" />
                  <Point X="-27.860775390625" Y="2.955340087891" />
                  <Point X="-27.85162890625" Y="2.973888916016" />
                  <Point X="-27.8467109375" Y="2.993977783203" />
                  <Point X="-27.843884765625" Y="3.015436523438" />
                  <Point X="-27.84343359375" Y="3.036118652344" />
                  <Point X="-27.844173828125" Y="3.044581787109" />
                  <Point X="-27.85091796875" Y="3.121668212891" />
                  <Point X="-27.854955078125" Y="3.141963134766" />
                  <Point X="-27.86146484375" Y="3.162605224609" />
                  <Point X="-27.869794921875" Y="3.181532714844" />
                  <Point X="-28.183333984375" Y="3.724595947266" />
                  <Point X="-27.700619140625" Y="4.094687255859" />
                  <Point X="-27.645431640625" Y="4.125348144531" />
                  <Point X="-27.167037109375" Y="4.391133789062" />
                  <Point X="-27.062978515625" Y="4.255521484375" />
                  <Point X="-27.0431953125" Y="4.229740234375" />
                  <Point X="-27.028890625" Y="4.214797851563" />
                  <Point X="-27.012310546875" Y="4.200885742188" />
                  <Point X="-26.995109375" Y="4.189393066406" />
                  <Point X="-26.985689453125" Y="4.184489746094" />
                  <Point X="-26.899892578125" Y="4.139826171875" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.77744921875" Y="4.134482910156" />
                  <Point X="-26.767638671875" Y="4.138546875" />
                  <Point X="-26.678275390625" Y="4.1755625" />
                  <Point X="-26.66014453125" Y="4.185509765625" />
                  <Point X="-26.6424140625" Y="4.197924316406" />
                  <Point X="-26.626859375" Y="4.211565429688" />
                  <Point X="-26.614626953125" Y="4.22825" />
                  <Point X="-26.603806640625" Y="4.246994140625" />
                  <Point X="-26.595478515625" Y="4.265921386719" />
                  <Point X="-26.59228515625" Y="4.276049316406" />
                  <Point X="-26.56319921875" Y="4.368297851562" />
                  <Point X="-26.5591640625" Y="4.388583496094" />
                  <Point X="-26.55727734375" Y="4.410143066406" />
                  <Point X="-26.557728515625" Y="4.430824707031" />
                  <Point X="-26.584201171875" Y="4.6318984375" />
                  <Point X="-25.94963671875" Y="4.809808105469" />
                  <Point X="-25.88273046875" Y="4.817638671875" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.15301953125" Y="4.35766015625" />
                  <Point X="-25.133904296875" Y="4.28631640625" />
                  <Point X="-25.12112890625" Y="4.258123046875" />
                  <Point X="-25.10326953125" Y="4.231395996094" />
                  <Point X="-25.08211328125" Y="4.20880859375" />
                  <Point X="-25.0548203125" Y="4.194219726563" />
                  <Point X="-25.0243828125" Y="4.18388671875" />
                  <Point X="-24.993845703125" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183885742188" />
                  <Point X="-24.932869140625" Y="4.194217773438" />
                  <Point X="-24.905576171875" Y="4.208806640625" />
                  <Point X="-24.88441796875" Y="4.23139453125" />
                  <Point X="-24.86655859375" Y="4.25812109375" />
                  <Point X="-24.853783203125" Y="4.286314941406" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.155958984375" Y="4.831738769531" />
                  <Point X="-24.100599609375" Y="4.818373535156" />
                  <Point X="-23.5189765625" Y="4.677951660156" />
                  <Point X="-23.48427734375" Y="4.665366210938" />
                  <Point X="-23.105357421875" Y="4.527929199219" />
                  <Point X="-23.070509765625" Y="4.511631835938" />
                  <Point X="-22.705423828125" Y="4.340893066406" />
                  <Point X="-22.6717265625" Y="4.32126171875" />
                  <Point X="-22.31901171875" Y="4.11576953125" />
                  <Point X="-22.28726953125" Y="4.093195556641" />
                  <Point X="-22.056736328125" Y="3.929254394531" />
                  <Point X="-22.7671796875" Y="2.698732177734" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.85964453125" Y="2.535450439453" />
                  <Point X="-22.866771484375" Y="2.515424804688" />
                  <Point X="-22.869046875" Y="2.508114013672" />
                  <Point X="-22.888392578125" Y="2.435770019531" />
                  <Point X="-22.89159765625" Y="2.413107177734" />
                  <Point X="-22.892107421875" Y="2.387337158203" />
                  <Point X="-22.891443359375" Y="2.374085693359" />
                  <Point X="-22.883900390625" Y="2.311528564453" />
                  <Point X="-22.87855859375" Y="2.289607177734" />
                  <Point X="-22.87029296875" Y="2.267518066406" />
                  <Point X="-22.85992578125" Y="2.247466796875" />
                  <Point X="-22.85567578125" Y="2.241204101562" />
                  <Point X="-22.816966796875" Y="2.184158203125" />
                  <Point X="-22.801798828125" Y="2.1667890625" />
                  <Point X="-22.782236328125" Y="2.149239257812" />
                  <Point X="-22.77213671875" Y="2.141341796875" />
                  <Point X="-22.71508984375" Y="2.102634033203" />
                  <Point X="-22.695044921875" Y="2.092271728516" />
                  <Point X="-22.67295703125" Y="2.084006103516" />
                  <Point X="-22.6510390625" Y="2.078663818359" />
                  <Point X="-22.644171875" Y="2.077835449219" />
                  <Point X="-22.581615234375" Y="2.070292236328" />
                  <Point X="-22.55808984375" Y="2.070388671875" />
                  <Point X="-22.531244140625" Y="2.073850341797" />
                  <Point X="-22.5188515625" Y="2.076295166016" />
                  <Point X="-22.4465078125" Y="2.095640869141" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.066716796875" Y="2.886536132812" />
                  <Point X="-21.032671875" Y="2.90619140625" />
                  <Point X="-20.87672265625" Y="2.689458740234" />
                  <Point X="-20.859025390625" Y="2.660211914062" />
                  <Point X="-20.737798828125" Y="2.459884277344" />
                  <Point X="-21.6569921875" Y="1.754562133789" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.782154296875" Y="1.656458862305" />
                  <Point X="-21.797115234375" Y="1.639752319336" />
                  <Point X="-21.801740234375" Y="1.634171142578" />
                  <Point X="-21.853806640625" Y="1.566247070312" />
                  <Point X="-21.865638671875" Y="1.546084106445" />
                  <Point X="-21.876236328125" Y="1.52151940918" />
                  <Point X="-21.88049609375" Y="1.509475708008" />
                  <Point X="-21.899892578125" Y="1.440125" />
                  <Point X="-21.90334765625" Y="1.417825195312" />
                  <Point X="-21.9041640625" Y="1.394253173828" />
                  <Point X="-21.902259765625" Y="1.371766235352" />
                  <Point X="-21.90051171875" Y="1.363294799805" />
                  <Point X="-21.88458984375" Y="1.286133300781" />
                  <Point X="-21.877001953125" Y="1.263822753906" />
                  <Point X="-21.86505078125" Y="1.239219848633" />
                  <Point X="-21.85896484375" Y="1.228515136719" />
                  <Point X="-21.815662109375" Y="1.162695800781" />
                  <Point X="-21.801107421875" Y="1.145449829102" />
                  <Point X="-21.78386328125" Y="1.129360107422" />
                  <Point X="-21.765646484375" Y="1.116031005859" />
                  <Point X="-21.758755859375" Y="1.112152954102" />
                  <Point X="-21.69600390625" Y="1.076828613281" />
                  <Point X="-21.673736328125" Y="1.067784912109" />
                  <Point X="-21.6464453125" Y="1.060555664062" />
                  <Point X="-21.63456640625" Y="1.058207519531" />
                  <Point X="-21.54971875" Y="1.046993896484" />
                  <Point X="-21.537294921875" Y="1.046174926758" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.234375" Y="1.21516027832" />
                  <Point X="-20.22316015625" Y="1.21663671875" />
                  <Point X="-20.154056640625" Y="0.932782775879" />
                  <Point X="-20.148484375" Y="0.89698248291" />
                  <Point X="-20.109134765625" Y="0.644238891602" />
                  <Point X="-21.154810546875" Y="0.364050476074" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.3002890625" Y="0.323303588867" />
                  <Point X="-21.32151953125" Y="0.313006256104" />
                  <Point X="-21.327603515625" Y="0.309778594971" />
                  <Point X="-21.410962890625" Y="0.261595947266" />
                  <Point X="-21.42993359375" Y="0.247179794312" />
                  <Point X="-21.449736328125" Y="0.227762924194" />
                  <Point X="-21.457958984375" Y="0.218580673218" />
                  <Point X="-21.507974609375" Y="0.15484967041" />
                  <Point X="-21.51969921875" Y="0.135567443848" />
                  <Point X="-21.52947265625" Y="0.114103370667" />
                  <Point X="-21.536318359375" Y="0.09260181427" />
                  <Point X="-21.5381484375" Y="0.083044784546" />
                  <Point X="-21.5548203125" Y="-0.004008500576" />
                  <Point X="-21.556318359375" Y="-0.027991376877" />
                  <Point X="-21.55448828125" Y="-0.056358264923" />
                  <Point X="-21.552990234375" Y="-0.068111083984" />
                  <Point X="-21.536318359375" Y="-0.155164382935" />
                  <Point X="-21.52947265625" Y="-0.176663650513" />
                  <Point X="-21.51969921875" Y="-0.198127578735" />
                  <Point X="-21.507978515625" Y="-0.217404647827" />
                  <Point X="-21.50248828125" Y="-0.224401428223" />
                  <Point X="-21.45247265625" Y="-0.288132568359" />
                  <Point X="-21.434962890625" Y="-0.305314483643" />
                  <Point X="-21.411498046875" Y="-0.323024169922" />
                  <Point X="-21.401810546875" Y="-0.329445068359" />
                  <Point X="-21.318453125" Y="-0.377627716064" />
                  <Point X="-21.307291015625" Y="-0.3831378479" />
                  <Point X="-21.283419921875" Y="-0.392149963379" />
                  <Point X="-20.111966796875" Y="-0.706039978027" />
                  <Point X="-20.108525390625" Y="-0.706961975098" />
                  <Point X="-20.1449765625" Y="-0.948736694336" />
                  <Point X="-20.15212109375" Y="-0.980041015625" />
                  <Point X="-20.198822265625" Y="-1.18469921875" />
                  <Point X="-21.426466796875" Y="-1.023076904297" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.600404296875" Y="-1.003439331055" />
                  <Point X="-21.635509765625" Y="-1.008056213379" />
                  <Point X="-21.64330078125" Y="-1.009412597656" />
                  <Point X="-21.806904296875" Y="-1.044972412109" />
                  <Point X="-21.837275390625" Y="-1.055693969727" />
                  <Point X="-21.869896484375" Y="-1.077422973633" />
                  <Point X="-21.893255859375" Y="-1.101254150391" />
                  <Point X="-21.8984609375" Y="-1.107017578125" />
                  <Point X="-21.99734765625" Y="-1.225948608398" />
                  <Point X="-22.013376953125" Y="-1.250425292969" />
                  <Point X="-22.025796875" Y="-1.284165161133" />
                  <Point X="-22.03094921875" Y="-1.315626953125" />
                  <Point X="-22.031796875" Y="-1.322275756836" />
                  <Point X="-22.04596875" Y="-1.47629675293" />
                  <Point X="-22.044888671875" Y="-1.508485717773" />
                  <Point X="-22.03383984375" Y="-1.546411254883" />
                  <Point X="-22.017390625" Y="-1.577035766602" />
                  <Point X="-22.013609375" Y="-1.583458007812" />
                  <Point X="-21.923068359375" Y="-1.724287841797" />
                  <Point X="-21.9130625" Y="-1.737242797852" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-20.80225390625" Y="-2.595083007812" />
                  <Point X="-20.786876953125" Y="-2.606881835938" />
                  <Point X="-20.875201171875" Y="-2.749802001953" />
                  <Point X="-20.889970703125" Y="-2.770788574219" />
                  <Point X="-20.971017578125" Y="-2.8859453125" />
                  <Point X="-22.06620703125" Y="-2.253637451172" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.245775390625" Y="-2.159824951172" />
                  <Point X="-22.26715234375" Y="-2.155964355469" />
                  <Point X="-22.461865234375" Y="-2.120799316406" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.5551640625" Y="-2.135175537109" />
                  <Point X="-22.572923828125" Y="-2.144521728516" />
                  <Point X="-22.734681640625" Y="-2.229654541016" />
                  <Point X="-22.757615234375" Y="-2.246549804688" />
                  <Point X="-22.77857421875" Y="-2.267509521484" />
                  <Point X="-22.795462890625" Y="-2.290434326172" />
                  <Point X="-22.804810546875" Y="-2.308193115234" />
                  <Point X="-22.889943359375" Y="-2.469952392578" />
                  <Point X="-22.899771484375" Y="-2.499735351563" />
                  <Point X="-22.904728515625" Y="-2.531908447266" />
                  <Point X="-22.904322265625" Y="-2.563261474609" />
                  <Point X="-22.9004609375" Y="-2.584638183594" />
                  <Point X="-22.865294921875" Y="-2.779351806641" />
                  <Point X="-22.86101171875" Y="-2.795139892578" />
                  <Point X="-22.848177734375" Y="-2.826078857422" />
                  <Point X="-22.14959765625" Y="-4.036056884766" />
                  <Point X="-22.13871484375" Y="-4.054905761719" />
                  <Point X="-22.218138671875" Y="-4.111636230469" />
                  <Point X="-22.234673828125" Y="-4.122339355469" />
                  <Point X="-22.298234375" Y="-4.163480957031" />
                  <Point X="-23.140083984375" Y="-3.066360351562" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.278076171875" Y="-2.900556640625" />
                  <Point X="-23.29916015625" Y="-2.887001953125" />
                  <Point X="-23.491201171875" Y="-2.763537841797" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.60595703125" Y="-2.743238525391" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.895404296875" Y="-2.795461425781" />
                  <Point X="-23.913208984375" Y="-2.810265625" />
                  <Point X="-24.075388671875" Y="-2.945111816406" />
                  <Point X="-24.095857421875" Y="-2.968860839844" />
                  <Point X="-24.11275" Y="-2.996687255859" />
                  <Point X="-24.124376953125" Y="-3.025812988281" />
                  <Point X="-24.12969921875" Y="-3.050305664062" />
                  <Point X="-24.17819140625" Y="-3.273400878906" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-23.98228515625" Y="-4.82687109375" />
                  <Point X="-23.977935546875" Y="-4.859915527344" />
                  <Point X="-24.024326171875" Y="-4.870084472656" />
                  <Point X="-24.03958984375" Y="-4.872857421875" />
                  <Point X="-24.07068359375" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.058427734375" Y="-4.752636230469" />
                  <Point X="-26.0806953125" Y="-4.746907226563" />
                  <Point X="-26.141244140625" Y="-4.731328613281" />
                  <Point X="-26.125544921875" Y="-4.612072753906" />
                  <Point X="-26.1207734375" Y="-4.575839355469" />
                  <Point X="-26.120076171875" Y="-4.568100097656" />
                  <Point X="-26.11944921875" Y="-4.541032226562" />
                  <Point X="-26.1217578125" Y="-4.509323730469" />
                  <Point X="-26.123333984375" Y="-4.497687011719" />
                  <Point X="-26.129322265625" Y="-4.467584960938" />
                  <Point X="-26.18386328125" Y="-4.193395507813" />
                  <Point X="-26.188126953125" Y="-4.178466308594" />
                  <Point X="-26.199029296875" Y="-4.1495" />
                  <Point X="-26.20566796875" Y="-4.135462890625" />
                  <Point X="-26.221740234375" Y="-4.107625976562" />
                  <Point X="-26.23057421875" Y="-4.094861816406" />
                  <Point X="-26.250205078125" Y="-4.070940429688" />
                  <Point X="-26.261001953125" Y="-4.059783203125" />
                  <Point X="-26.284076171875" Y="-4.039546386719" />
                  <Point X="-26.49426171875" Y="-3.855218261719" />
                  <Point X="-26.506734375" Y="-3.845967773438" />
                  <Point X="-26.53301953125" Y="-3.829622070312" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.66744140625" Y="-3.794162353516" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166259766" />
                  <Point X="-27.008005859375" Y="-3.781946044922" />
                  <Point X="-27.0390546875" Y="-3.790265869141" />
                  <Point X="-27.053673828125" Y="-3.79549609375" />
                  <Point X="-27.081865234375" Y="-3.808269042969" />
                  <Point X="-27.0954375" Y="-3.815811767578" />
                  <Point X="-27.12095703125" Y="-3.83286328125" />
                  <Point X="-27.35340234375" Y="-3.9881796875" />
                  <Point X="-27.359685546875" Y="-3.992760253906" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032770507812" />
                  <Point X="-27.410470703125" Y="-4.041629394531" />
                  <Point X="-27.503201171875" Y="-4.162478515625" />
                  <Point X="-27.7475859375" Y="-4.011162353516" />
                  <Point X="-27.77840625" Y="-3.987431640625" />
                  <Point X="-27.980861328125" Y="-3.831546386719" />
                  <Point X="-27.4151328125" Y="-2.851673339844" />
                  <Point X="-27.341490234375" Y="-2.724119384766" />
                  <Point X="-27.334849609375" Y="-2.710082763672" />
                  <Point X="-27.3239453125" Y="-2.681110351562" />
                  <Point X="-27.319681640625" Y="-2.666174560547" />
                  <Point X="-27.313412109375" Y="-2.634644775391" />
                  <Point X="-27.311623046875" Y="-2.618685302734" />
                  <Point X="-27.3107578125" Y="-2.586689697266" />
                  <Point X="-27.315294921875" Y="-2.555002929688" />
                  <Point X="-27.32510546875" Y="-2.524536376953" />
                  <Point X="-27.331302734375" Y="-2.509720458984" />
                  <Point X="-27.347232421875" Y="-2.479180419922" />
                  <Point X="-27.354873046875" Y="-2.466909667969" />
                  <Point X="-27.37192578125" Y="-2.443672363281" />
                  <Point X="-27.381337890625" Y="-2.432705810547" />
                  <Point X="-27.39696875" Y="-2.417073486328" />
                  <Point X="-27.408806640625" Y="-2.407027832031" />
                  <Point X="-27.43396484375" Y="-2.388997802734" />
                  <Point X="-27.44728515625" Y="-2.381013427734" />
                  <Point X="-27.476115234375" Y="-2.366795166016" />
                  <Point X="-27.4905546875" Y="-2.361088378906" />
                  <Point X="-27.520171875" Y="-2.352103271484" />
                  <Point X="-27.550859375" Y="-2.348062744141" />
                  <Point X="-27.581794921875" Y="-2.349074951172" />
                  <Point X="-27.597220703125" Y="-2.350849365234" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.793087890625" Y="-3.017708496094" />
                  <Point X="-29.004015625" Y="-2.740592285156" />
                  <Point X="-29.026109375" Y="-2.703544921875" />
                  <Point X="-29.181265625" Y="-2.443372802734" />
                  <Point X="-28.176974609375" Y="-1.672753540039" />
                  <Point X="-28.048123046875" Y="-1.573881958008" />
                  <Point X="-28.0355859375" Y="-1.562333862305" />
                  <Point X="-28.012775390625" Y="-1.537182373047" />
                  <Point X="-28.002501953125" Y="-1.523579101562" />
                  <Point X="-27.9837734375" Y="-1.493503417969" />
                  <Point X="-27.977109375" Y="-1.480734863281" />
                  <Point X="-27.96578515625" Y="-1.454333740234" />
                  <Point X="-27.961125" Y="-1.440700927734" />
                  <Point X="-27.95418359375" Y="-1.413896362305" />
                  <Point X="-27.95155078125" Y="-1.398796020508" />
                  <Point X="-27.948748046875" Y="-1.36837512207" />
                  <Point X="-27.948578125" Y="-1.3530546875" />
                  <Point X="-27.95078515625" Y="-1.321387207031" />
                  <Point X="-27.953234375" Y="-1.305585693359" />
                  <Point X="-27.96075" Y="-1.274619628906" />
                  <Point X="-27.9733515625" Y="-1.245353759766" />
                  <Point X="-27.99068359375" Y="-1.218614746094" />
                  <Point X="-28.00048046875" Y="-1.20597668457" />
                  <Point X="-28.02393359375" Y="-1.180476928711" />
                  <Point X="-28.034150390625" Y="-1.170893066406" />
                  <Point X="-28.05588671875" Y="-1.153330566406" />
                  <Point X="-28.06740625" Y="-1.145352050781" />
                  <Point X="-28.09126953125" Y="-1.131307373047" />
                  <Point X="-28.10543359375" Y="-1.124480957031" />
                  <Point X="-28.1346953125" Y="-1.113257324219" />
                  <Point X="-28.14979296875" Y="-1.108859863281" />
                  <Point X="-28.1816796875" Y="-1.102378417969" />
                  <Point X="-28.197296875" Y="-1.100532226562" />
                  <Point X="-28.22862109375" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-29.66091796875" Y="-1.286694213867" />
                  <Point X="-29.740763671875" Y="-0.974108886719" />
                  <Point X="-29.74660546875" Y="-0.933254333496" />
                  <Point X="-29.786451171875" Y="-0.654654602051" />
                  <Point X="-28.655068359375" Y="-0.351501281738" />
                  <Point X="-28.508287109375" Y="-0.312171234131" />
                  <Point X="-28.4992265625" Y="-0.309249298096" />
                  <Point X="-28.472720703125" Y="-0.298700042725" />
                  <Point X="-28.454408203125" Y="-0.289574462891" />
                  <Point X="-28.4483828125" Y="-0.286295837402" />
                  <Point X="-28.430818359375" Y="-0.275609100342" />
                  <Point X="-28.405810546875" Y="-0.258252471924" />
                  <Point X="-28.3927109375" Y="-0.247292434692" />
                  <Point X="-28.3686875" Y="-0.223204238892" />
                  <Point X="-28.357763671875" Y="-0.210076080322" />
                  <Point X="-28.337599609375" Y="-0.180855636597" />
                  <Point X="-28.330349609375" Y="-0.168432250977" />
                  <Point X="-28.31780859375" Y="-0.142633239746" />
                  <Point X="-28.312517578125" Y="-0.12925793457" />
                  <Point X="-28.3041875" Y="-0.10241619873" />
                  <Point X="-28.30080078125" Y="-0.087182945251" />
                  <Point X="-28.296568359375" Y="-0.056361980438" />
                  <Point X="-28.29572265625" Y="-0.040774116516" />
                  <Point X="-28.29663671875" Y="-0.008183498383" />
                  <Point X="-28.2978828125" Y="0.004710278988" />
                  <Point X="-28.302115234375" Y="0.030206682205" />
                  <Point X="-28.3051015625" Y="0.042809307098" />
                  <Point X="-28.3134375" Y="0.069667541504" />
                  <Point X="-28.319990234375" Y="0.085543884277" />
                  <Point X="-28.33584765625" Y="0.115856109619" />
                  <Point X="-28.34515234375" Y="0.130291854858" />
                  <Point X="-28.367146484375" Y="0.158455444336" />
                  <Point X="-28.37676171875" Y="0.169022827148" />
                  <Point X="-28.39746875" Y="0.188595565796" />
                  <Point X="-28.408560546875" Y="0.197601074219" />
                  <Point X="-28.433564453125" Y="0.214954727173" />
                  <Point X="-28.440484375" Y="0.219327941895" />
                  <Point X="-28.46561328125" Y="0.232833450317" />
                  <Point X="-28.496564453125" Y="0.245635238647" />
                  <Point X="-28.508287109375" Y="0.249611053467" />
                  <Point X="-29.785443359375" Y="0.591824523926" />
                  <Point X="-29.731328125" Y="0.957533691406" />
                  <Point X="-29.7195625" Y="1.000959533691" />
                  <Point X="-29.6335859375" Y="1.318237182617" />
                  <Point X="-28.88328125" Y="1.219457641602" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815551758" />
                  <Point X="-28.7470546875" Y="1.204364379883" />
                  <Point X="-28.736701171875" Y="1.204703369141" />
                  <Point X="-28.715140625" Y="1.20658984375" />
                  <Point X="-28.70488671875" Y="1.208053955078" />
                  <Point X="-28.684599609375" Y="1.212089599609" />
                  <Point X="-28.668490234375" Y="1.216577026367" />
                  <Point X="-28.613140625" Y="1.234028564453" />
                  <Point X="-28.6034453125" Y="1.237678344727" />
                  <Point X="-28.58451171875" Y="1.246010253906" />
                  <Point X="-28.5752734375" Y="1.250692382812" />
                  <Point X="-28.55652734375" Y="1.261516235352" />
                  <Point X="-28.5478515625" Y="1.267177001953" />
                  <Point X="-28.53116796875" Y="1.279410888672" />
                  <Point X="-28.5159140625" Y="1.293390869141" />
                  <Point X="-28.502275390625" Y="1.308946166992" />
                  <Point X="-28.4958828125" Y="1.317094726562" />
                  <Point X="-28.48346875" Y="1.334827636719" />
                  <Point X="-28.478" Y="1.343622070313" />
                  <Point X="-28.468052734375" Y="1.361756835938" />
                  <Point X="-28.461138671875" Y="1.376978149414" />
                  <Point X="-28.4389296875" Y="1.430596069336" />
                  <Point X="-28.43549609375" Y="1.440361694336" />
                  <Point X="-28.429712890625" Y="1.460204589844" />
                  <Point X="-28.42736328125" Y="1.470282470703" />
                  <Point X="-28.423603515625" Y="1.491595336914" />
                  <Point X="-28.42236328125" Y="1.501873901367" />
                  <Point X="-28.4210078125" Y="1.522514282227" />
                  <Point X="-28.42190625" Y="1.543181518555" />
                  <Point X="-28.42505078125" Y="1.563625854492" />
                  <Point X="-28.427181640625" Y="1.573764282227" />
                  <Point X="-28.43278125" Y="1.594668945312" />
                  <Point X="-28.436005859375" Y="1.604521118164" />
                  <Point X="-28.443509765625" Y="1.623816650391" />
                  <Point X="-28.45073046875" Y="1.638907958984" />
                  <Point X="-28.477525390625" Y="1.690377685547" />
                  <Point X="-28.4828046875" Y="1.699292358398" />
                  <Point X="-28.494296875" Y="1.716489990234" />
                  <Point X="-28.500509765625" Y="1.724772583008" />
                  <Point X="-28.514421875" Y="1.741352172852" />
                  <Point X="-28.5215" Y="1.748910644531" />
                  <Point X="-28.53644140625" Y="1.763214355469" />
                  <Point X="-28.5443046875" Y="1.769958984375" />
                  <Point X="-29.22761328125" Y="2.29428125" />
                  <Point X="-29.0022890625" Y="2.680313476562" />
                  <Point X="-28.971123046875" Y="2.720375" />
                  <Point X="-28.726337890625" Y="3.035012451172" />
                  <Point X="-28.31877734375" Y="2.799707519531" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.185615234375" Y="2.736657226562" />
                  <Point X="-28.165330078125" Y="2.732622070312" />
                  <Point X="-28.14661328125" Y="2.730417480469" />
                  <Point X="-28.06952734375" Y="2.723673339844" />
                  <Point X="-28.059173828125" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310791016" />
                  <Point X="-27.97643359375" Y="2.734227294922" />
                  <Point X="-27.956994140625" Y="2.741302978516" />
                  <Point X="-27.938443359375" Y="2.750451904297" />
                  <Point X="-27.929419921875" Y="2.755531738281" />
                  <Point X="-27.911166015625" Y="2.767160888672" />
                  <Point X="-27.902751953125" Y="2.773188720703" />
                  <Point X="-27.886623046875" Y="2.786131347656" />
                  <Point X="-27.872900390625" Y="2.799053222656" />
                  <Point X="-27.81818359375" Y="2.85376953125" />
                  <Point X="-27.811265625" Y="2.861486328125" />
                  <Point X="-27.798314453125" Y="2.877623291016" />
                  <Point X="-27.79228125" Y="2.886043457031" />
                  <Point X="-27.78065234375" Y="2.904297851562" />
                  <Point X="-27.7755703125" Y="2.913325683594" />
                  <Point X="-27.766423828125" Y="2.931874511719" />
                  <Point X="-27.759353515625" Y="2.951299072266" />
                  <Point X="-27.754435546875" Y="2.971387939453" />
                  <Point X="-27.7525234375" Y="2.981573242188" />
                  <Point X="-27.749697265625" Y="3.003031982422" />
                  <Point X="-27.748908203125" Y="3.013364746094" />
                  <Point X="-27.74845703125" Y="3.034046875" />
                  <Point X="-27.74953515625" Y="3.052859375" />
                  <Point X="-27.756279296875" Y="3.129945800781" />
                  <Point X="-27.757744140625" Y="3.140202636719" />
                  <Point X="-27.76178125" Y="3.160497558594" />
                  <Point X="-27.764353515625" Y="3.170535644531" />
                  <Point X="-27.77086328125" Y="3.191177734375" />
                  <Point X="-27.774513671875" Y="3.200873046875" />
                  <Point X="-27.78284375" Y="3.219800537109" />
                  <Point X="-27.7875234375" Y="3.229032958984" />
                  <Point X="-28.059388671875" Y="3.699914794922" />
                  <Point X="-27.64836328125" Y="4.015043212891" />
                  <Point X="-27.599294921875" Y="4.042303955078" />
                  <Point X="-27.192525390625" Y="4.268295898437" />
                  <Point X="-27.13834765625" Y="4.197689453125" />
                  <Point X="-27.118564453125" Y="4.171908203125" />
                  <Point X="-27.111818359375" Y="4.164045410156" />
                  <Point X="-27.097513671875" Y="4.149103027344" />
                  <Point X="-27.089955078125" Y="4.142022949219" />
                  <Point X="-27.073375" Y="4.128110839844" />
                  <Point X="-27.065087890625" Y="4.12189453125" />
                  <Point X="-27.04788671875" Y="4.110401855469" />
                  <Point X="-27.029552734375" Y="4.100222167969" />
                  <Point X="-26.943755859375" Y="4.055558837891" />
                  <Point X="-26.9343203125" Y="4.051283935547" />
                  <Point X="-26.91504296875" Y="4.043788330078" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031378173828" />
                  <Point X="-26.780818359375" Y="4.035136474609" />
                  <Point X="-26.770728515625" Y="4.037488525391" />
                  <Point X="-26.750865234375" Y="4.043278076172" />
                  <Point X="-26.73128125" Y="4.050779296875" />
                  <Point X="-26.64191796875" Y="4.087794921875" />
                  <Point X="-26.632580078125" Y="4.092274169922" />
                  <Point X="-26.61444921875" Y="4.102221191406" />
                  <Point X="-26.60565625" Y="4.107689453125" />
                  <Point X="-26.58792578125" Y="4.120104003906" />
                  <Point X="-26.579775390625" Y="4.126499511719" />
                  <Point X="-26.564220703125" Y="4.140140625" />
                  <Point X="-26.550244140625" Y="4.15539453125" />
                  <Point X="-26.53801171875" Y="4.172079101562" />
                  <Point X="-26.5323515625" Y="4.180755371094" />
                  <Point X="-26.52153125" Y="4.199499511719" />
                  <Point X="-26.5168515625" Y="4.208733398438" />
                  <Point X="-26.5085234375" Y="4.227660644531" />
                  <Point X="-26.501681640625" Y="4.247481933594" />
                  <Point X="-26.472595703125" Y="4.33973046875" />
                  <Point X="-26.470025390625" Y="4.349763671875" />
                  <Point X="-26.465990234375" Y="4.370049316406" />
                  <Point X="-26.464525390625" Y="4.380301757812" />
                  <Point X="-26.462638671875" Y="4.401861328125" />
                  <Point X="-26.46230078125" Y="4.41221484375" />
                  <Point X="-26.462751953125" Y="4.432896484375" />
                  <Point X="-26.463541015625" Y="4.443225097656" />
                  <Point X="-26.479265625" Y="4.562655273438" />
                  <Point X="-25.93117578125" Y="4.7163203125" />
                  <Point X="-25.8716875" Y="4.723282714844" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.244783203125" Y="4.333072265625" />
                  <Point X="-25.22566796875" Y="4.261728515625" />
                  <Point X="-25.220435546875" Y="4.247106445313" />
                  <Point X="-25.20766015625" Y="4.218913085938" />
                  <Point X="-25.2001171875" Y="4.205341796875" />
                  <Point X="-25.1822578125" Y="4.178614746094" />
                  <Point X="-25.17260546875" Y="4.166453125" />
                  <Point X="-25.15144921875" Y="4.143865722656" />
                  <Point X="-25.126896484375" Y="4.125026855469" />
                  <Point X="-25.099603515625" Y="4.110437988281" />
                  <Point X="-25.085359375" Y="4.104262207031" />
                  <Point X="-25.054921875" Y="4.093929199219" />
                  <Point X="-25.039859375" Y="4.090156005859" />
                  <Point X="-25.009322265625" Y="4.085113525391" />
                  <Point X="-24.978373046875" Y="4.085112792969" />
                  <Point X="-24.947833984375" Y="4.090154296875" />
                  <Point X="-24.93276953125" Y="4.093927246094" />
                  <Point X="-24.90233203125" Y="4.104259277344" />
                  <Point X="-24.8880859375" Y="4.110436035156" />
                  <Point X="-24.86079296875" Y="4.125024902344" />
                  <Point X="-24.8362421875" Y="4.143861328125" />
                  <Point X="-24.815083984375" Y="4.16644921875" />
                  <Point X="-24.8054296875" Y="4.178612792969" />
                  <Point X="-24.7875703125" Y="4.205339355469" />
                  <Point X="-24.78002734375" Y="4.218911621094" />
                  <Point X="-24.767251953125" Y="4.24710546875" />
                  <Point X="-24.76201953125" Y="4.261727050781" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.172123046875" Y="4.737912109375" />
                  <Point X="-24.12289453125" Y="4.726026855469" />
                  <Point X="-23.546408203125" Y="4.586845214844" />
                  <Point X="-23.516669921875" Y="4.576059082031" />
                  <Point X="-23.141751953125" Y="4.440073242188" />
                  <Point X="-23.110755859375" Y="4.425577636719" />
                  <Point X="-22.749546875" Y="4.256651855469" />
                  <Point X="-22.719548828125" Y="4.23917578125" />
                  <Point X="-22.370548828125" Y="4.035847900391" />
                  <Point X="-22.342326171875" Y="4.015776855469" />
                  <Point X="-22.18221484375" Y="3.901916015625" />
                  <Point X="-22.849451171875" Y="2.746232177734" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.938658203125" Y="2.590938476562" />
                  <Point X="-22.94914453125" Y="2.567302978516" />
                  <Point X="-22.956271484375" Y="2.54727734375" />
                  <Point X="-22.960822265625" Y="2.532655761719" />
                  <Point X="-22.98016796875" Y="2.460311767578" />
                  <Point X="-22.98245703125" Y="2.449072998047" />
                  <Point X="-22.985662109375" Y="2.42641015625" />
                  <Point X="-22.986578125" Y="2.414986083984" />
                  <Point X="-22.987087890625" Y="2.389216064453" />
                  <Point X="-22.985759765625" Y="2.362713134766" />
                  <Point X="-22.978216796875" Y="2.300156005859" />
                  <Point X="-22.97619921875" Y="2.289037109375" />
                  <Point X="-22.970857421875" Y="2.267115722656" />
                  <Point X="-22.967533203125" Y="2.256313232422" />
                  <Point X="-22.959267578125" Y="2.234224121094" />
                  <Point X="-22.954681640625" Y="2.22388671875" />
                  <Point X="-22.944314453125" Y="2.203835449219" />
                  <Point X="-22.934283203125" Y="2.187858886719" />
                  <Point X="-22.89557421875" Y="2.130812988281" />
                  <Point X="-22.8885234375" Y="2.121670410156" />
                  <Point X="-22.87335546875" Y="2.104301269531" />
                  <Point X="-22.86523828125" Y="2.096074707031" />
                  <Point X="-22.84567578125" Y="2.078524902344" />
                  <Point X="-22.8254765625" Y="2.062729980469" />
                  <Point X="-22.7684296875" Y="2.024022216797" />
                  <Point X="-22.758716796875" Y="2.018243408203" />
                  <Point X="-22.738671875" Y="2.007881103516" />
                  <Point X="-22.72833984375" Y="2.003297485352" />
                  <Point X="-22.706251953125" Y="1.995031860352" />
                  <Point X="-22.695453125" Y="1.991708251953" />
                  <Point X="-22.67353515625" Y="1.986365966797" />
                  <Point X="-22.655548828125" Y="1.983519165039" />
                  <Point X="-22.5929921875" Y="1.975975952148" />
                  <Point X="-22.5812265625" Y="1.97529309082" />
                  <Point X="-22.557701171875" Y="1.975389526367" />
                  <Point X="-22.54594140625" Y="1.976168701172" />
                  <Point X="-22.519095703125" Y="1.979630371094" />
                  <Point X="-22.494310546875" Y="1.984519897461" />
                  <Point X="-22.421966796875" Y="2.003865600586" />
                  <Point X="-22.4160078125" Y="2.005670288086" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572875977" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.05959375" Y="2.780951904297" />
                  <Point X="-20.956044921875" Y="2.637042724609" />
                  <Point X="-20.9403046875" Y="2.611030273438" />
                  <Point X="-20.86311328125" Y="2.483471679688" />
                  <Point X="-21.71482421875" Y="1.829930664062" />
                  <Point X="-21.827046875" Y="1.743819946289" />
                  <Point X="-21.83379296875" Y="1.738127563477" />
                  <Point X="-21.85292578125" Y="1.719834960938" />
                  <Point X="-21.86788671875" Y="1.703128417969" />
                  <Point X="-21.87713671875" Y="1.691965942383" />
                  <Point X="-21.929203125" Y="1.624041870117" />
                  <Point X="-21.935740234375" Y="1.614327758789" />
                  <Point X="-21.947572265625" Y="1.594164916992" />
                  <Point X="-21.9528671875" Y="1.583716064453" />
                  <Point X="-21.96346484375" Y="1.559151367188" />
                  <Point X="-21.971984375" Y="1.535063964844" />
                  <Point X="-21.991380859375" Y="1.465713256836" />
                  <Point X="-21.993771484375" Y="1.454670532227" />
                  <Point X="-21.9972265625" Y="1.432370727539" />
                  <Point X="-21.998291015625" Y="1.421113647461" />
                  <Point X="-21.999107421875" Y="1.397541503906" />
                  <Point X="-21.998826171875" Y="1.386236816406" />
                  <Point X="-21.996921875" Y="1.36374987793" />
                  <Point X="-21.99355078125" Y="1.344096435547" />
                  <Point X="-21.97762890625" Y="1.266934936523" />
                  <Point X="-21.97453125" Y="1.255544189453" />
                  <Point X="-21.966943359375" Y="1.233233642578" />
                  <Point X="-21.962453125" Y="1.222313354492" />
                  <Point X="-21.950501953125" Y="1.197710571289" />
                  <Point X="-21.938330078125" Y="1.176301147461" />
                  <Point X="-21.89502734375" Y="1.110481811523" />
                  <Point X="-21.888263671875" Y="1.101424560547" />
                  <Point X="-21.873708984375" Y="1.084178710938" />
                  <Point X="-21.86591796875" Y="1.075989868164" />
                  <Point X="-21.848673828125" Y="1.059900146484" />
                  <Point X="-21.8399609375" Y="1.052691650391" />
                  <Point X="-21.821744140625" Y="1.039362548828" />
                  <Point X="-21.805349609375" Y="1.029363891602" />
                  <Point X="-21.74259765625" Y="0.994039672852" />
                  <Point X="-21.731751953125" Y="0.988810852051" />
                  <Point X="-21.709484375" Y="0.979767150879" />
                  <Point X="-21.6980625" Y="0.975952270508" />
                  <Point X="-21.670771484375" Y="0.968722961426" />
                  <Point X="-21.647013671875" Y="0.964026489258" />
                  <Point X="-21.562166015625" Y="0.952812866211" />
                  <Point X="-21.555966796875" Y="0.952199645996" />
                  <Point X="-21.53428125" Y="0.95122277832" />
                  <Point X="-21.50878125" Y="0.952032287598" />
                  <Point X="-21.49939453125" Y="0.952797302246" />
                  <Point X="-20.295296875" Y="1.111319946289" />
                  <Point X="-20.247306640625" Y="0.914195068359" />
                  <Point X="-20.242353515625" Y="0.882371765137" />
                  <Point X="-20.216126953125" Y="0.713921508789" />
                  <Point X="-21.1793984375" Y="0.45581338501" />
                  <Point X="-21.3080078125" Y="0.421352813721" />
                  <Point X="-21.31659375" Y="0.418609832764" />
                  <Point X="-21.341748046875" Y="0.408779907227" />
                  <Point X="-21.362978515625" Y="0.398482574463" />
                  <Point X="-21.37514453125" Y="0.392027435303" />
                  <Point X="-21.45850390625" Y="0.343844787598" />
                  <Point X="-21.46844140625" Y="0.337234344482" />
                  <Point X="-21.487412109375" Y="0.322818206787" />
                  <Point X="-21.4964453125" Y="0.315012542725" />
                  <Point X="-21.516248046875" Y="0.295595703125" />
                  <Point X="-21.532693359375" Y="0.277231109619" />
                  <Point X="-21.582708984375" Y="0.213500152588" />
                  <Point X="-21.589146484375" Y="0.204206619263" />
                  <Point X="-21.60087109375" Y="0.184924285889" />
                  <Point X="-21.606158203125" Y="0.174935638428" />
                  <Point X="-21.615931640625" Y="0.153471511841" />
                  <Point X="-21.61999609375" Y="0.142924194336" />
                  <Point X="-21.626841796875" Y="0.12142263031" />
                  <Point X="-21.631453125" Y="0.100911781311" />
                  <Point X="-21.648125" Y="0.013858463287" />
                  <Point X="-21.649634765625" Y="0.001913939834" />
                  <Point X="-21.6511328125" Y="-0.022068887711" />
                  <Point X="-21.65112109375" Y="-0.034107486725" />
                  <Point X="-21.649291015625" Y="-0.062474391937" />
                  <Point X="-21.646294921875" Y="-0.085980140686" />
                  <Point X="-21.629623046875" Y="-0.173033462524" />
                  <Point X="-21.62683984375" Y="-0.183988006592" />
                  <Point X="-21.619994140625" Y="-0.205487197876" />
                  <Point X="-21.615931640625" Y="-0.216031997681" />
                  <Point X="-21.606158203125" Y="-0.237495956421" />
                  <Point X="-21.600873046875" Y="-0.247482070923" />
                  <Point X="-21.58915234375" Y="-0.266759216309" />
                  <Point X="-21.5772265625" Y="-0.283046630859" />
                  <Point X="-21.5272109375" Y="-0.346777770996" />
                  <Point X="-21.519009765625" Y="-0.355939483643" />
                  <Point X="-21.5015" Y="-0.373121459961" />
                  <Point X="-21.49219140625" Y="-0.381141906738" />
                  <Point X="-21.4687265625" Y="-0.398851501465" />
                  <Point X="-21.4493515625" Y="-0.41169342041" />
                  <Point X="-21.365994140625" Y="-0.459876037598" />
                  <Point X="-21.36050390625" Y="-0.462813720703" />
                  <Point X="-21.340845703125" Y="-0.472014831543" />
                  <Point X="-21.316974609375" Y="-0.481027008057" />
                  <Point X="-21.3080078125" Y="-0.483912963867" />
                  <Point X="-20.215119140625" Y="-0.776751342773" />
                  <Point X="-20.238380859375" Y="-0.931042419434" />
                  <Point X="-20.244740234375" Y="-0.958902893066" />
                  <Point X="-20.2721953125" Y="-1.079219726563" />
                  <Point X="-21.41406640625" Y="-0.928889709473" />
                  <Point X="-21.563216796875" Y="-0.909253662109" />
                  <Point X="-21.575611328125" Y="-0.908440856934" />
                  <Point X="-21.6003984375" Y="-0.908439331055" />
                  <Point X="-21.612791015625" Y="-0.909250366211" />
                  <Point X="-21.647896484375" Y="-0.9138671875" />
                  <Point X="-21.663478515625" Y="-0.91658013916" />
                  <Point X="-21.82708203125" Y="-0.952139953613" />
                  <Point X="-21.838529296875" Y="-0.95539050293" />
                  <Point X="-21.868900390625" Y="-0.966111999512" />
                  <Point X="-21.88994140625" Y="-0.976628540039" />
                  <Point X="-21.9225625" Y="-0.998357666016" />
                  <Point X="-21.937740234375" Y="-1.010922668457" />
                  <Point X="-21.961099609375" Y="-1.03475390625" />
                  <Point X="-21.971509765625" Y="-1.046280761719" />
                  <Point X="-22.070396484375" Y="-1.165211791992" />
                  <Point X="-22.076822265625" Y="-1.17390234375" />
                  <Point X="-22.0928515625" Y="-1.19837902832" />
                  <Point X="-22.102529296875" Y="-1.217607788086" />
                  <Point X="-22.11494921875" Y="-1.25134765625" />
                  <Point X="-22.119548828125" Y="-1.268812011719" />
                  <Point X="-22.124701171875" Y="-1.300273681641" />
                  <Point X="-22.126396484375" Y="-1.313571289062" />
                  <Point X="-22.140568359375" Y="-1.467592285156" />
                  <Point X="-22.140916015625" Y="-1.479482666016" />
                  <Point X="-22.1398359375" Y="-1.511671630859" />
                  <Point X="-22.13609765625" Y="-1.535057250977" />
                  <Point X="-22.125048828125" Y="-1.572982910156" />
                  <Point X="-22.11753125" Y="-1.591364013672" />
                  <Point X="-22.10108203125" Y="-1.62198840332" />
                  <Point X="-22.09351953125" Y="-1.634833007812" />
                  <Point X="-22.002978515625" Y="-1.775662841797" />
                  <Point X="-21.99825390625" Y="-1.782357910156" />
                  <Point X="-21.98019921875" Y="-1.804456176758" />
                  <Point X="-21.956505859375" Y="-1.828122558594" />
                  <Point X="-21.947201171875" Y="-1.836277709961" />
                  <Point X="-20.912828125" Y="-2.629980712891" />
                  <Point X="-20.954517578125" Y="-2.697440185547" />
                  <Point X="-20.96766015625" Y="-2.716113769531" />
                  <Point X="-20.998724609375" Y="-2.760252197266" />
                  <Point X="-22.01870703125" Y="-2.171364990234" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.18497265625" Y="-2.079513671875" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228892578125" Y="-2.066337158203" />
                  <Point X="-22.25026953125" Y="-2.0624765625" />
                  <Point X="-22.444982421875" Y="-2.027311767578" />
                  <Point X="-22.460640625" Y="-2.025807250977" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503295898" />
                  <Point X="-22.539859375" Y="-2.03146105957" />
                  <Point X="-22.555154296875" Y="-2.035135864258" />
                  <Point X="-22.584927734375" Y="-2.044958251953" />
                  <Point X="-22.59940625" Y="-2.051106201172" />
                  <Point X="-22.617166015625" Y="-2.060452392578" />
                  <Point X="-22.778923828125" Y="-2.145585205078" />
                  <Point X="-22.791029296875" Y="-2.153169189453" />
                  <Point X="-22.813962890625" Y="-2.170064453125" />
                  <Point X="-22.824791015625" Y="-2.179375732422" />
                  <Point X="-22.84575" Y="-2.200335449219" />
                  <Point X="-22.855060546875" Y="-2.211162841797" />
                  <Point X="-22.87194921875" Y="-2.234087646484" />
                  <Point X="-22.87952734375" Y="-2.246185058594" />
                  <Point X="-22.888875" Y="-2.263943847656" />
                  <Point X="-22.9740078125" Y="-2.425703125" />
                  <Point X="-22.980158203125" Y="-2.440182128906" />
                  <Point X="-22.989986328125" Y="-2.469965087891" />
                  <Point X="-22.9936640625" Y="-2.485269042969" />
                  <Point X="-22.99862109375" Y="-2.517442138672" />
                  <Point X="-22.999720703125" Y="-2.533139404297" />
                  <Point X="-22.999314453125" Y="-2.564492431641" />
                  <Point X="-22.99780859375" Y="-2.580148193359" />
                  <Point X="-22.993947265625" Y="-2.601524902344" />
                  <Point X="-22.95878125" Y="-2.796238525391" />
                  <Point X="-22.95698046875" Y="-2.804225585938" />
                  <Point X="-22.94876171875" Y="-2.831540039062" />
                  <Point X="-22.935927734375" Y="-2.862479003906" />
                  <Point X="-22.93044921875" Y="-2.873578857422" />
                  <Point X="-22.26410546875" Y="-4.027723388672" />
                  <Point X="-22.27624609375" Y="-4.036082519531" />
                  <Point X="-23.06471484375" Y="-3.008528076172" />
                  <Point X="-23.16608203125" Y="-2.876422851562" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.191169921875" Y="-2.849624267578" />
                  <Point X="-23.21675" Y="-2.828002685547" />
                  <Point X="-23.226703125" Y="-2.820645996094" />
                  <Point X="-23.247787109375" Y="-2.807091308594" />
                  <Point X="-23.439828125" Y="-2.683627197266" />
                  <Point X="-23.45371875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663874023438" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.614662109375" Y="-2.648638183594" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920123046875" Y="-2.699413330078" />
                  <Point X="-23.944505859375" Y="-2.714134521484" />
                  <Point X="-23.956142578125" Y="-2.722413818359" />
                  <Point X="-23.973947265625" Y="-2.737218017578" />
                  <Point X="-24.136126953125" Y="-2.872064208984" />
                  <Point X="-24.147349609375" Y="-2.883090576172" />
                  <Point X="-24.167818359375" Y="-2.906839599609" />
                  <Point X="-24.177064453125" Y="-2.919562255859" />
                  <Point X="-24.19395703125" Y="-2.947388671875" />
                  <Point X="-24.20098046875" Y="-2.961466064453" />
                  <Point X="-24.212607421875" Y="-2.990591796875" />
                  <Point X="-24.2172109375" Y="-3.005640136719" />
                  <Point X="-24.222533203125" Y="-3.0301328125" />
                  <Point X="-24.271025390625" Y="-3.253228027344" />
                  <Point X="-24.27241796875" Y="-3.261299316406" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520019531" />
                  <Point X="-24.16691015625" Y="-4.152323242188" />
                  <Point X="-24.3053046875" Y="-3.635829101562" />
                  <Point X="-24.344931640625" Y="-3.487937011719" />
                  <Point X="-24.347392578125" Y="-3.48012109375" />
                  <Point X="-24.35785546875" Y="-3.453576660156" />
                  <Point X="-24.37321484375" Y="-3.423812011719" />
                  <Point X="-24.37959375" Y="-3.413208251953" />
                  <Point X="-24.395791015625" Y="-3.389871582031" />
                  <Point X="-24.543322265625" Y="-3.177307861328" />
                  <Point X="-24.553328125" Y="-3.165173339844" />
                  <Point X="-24.5752109375" Y="-3.142718261719" />
                  <Point X="-24.587087890625" Y="-3.132397705078" />
                  <Point X="-24.61334375" Y="-3.113153808594" />
                  <Point X="-24.626759765625" Y="-3.104936523438" />
                  <Point X="-24.654759765625" Y="-3.090829345703" />
                  <Point X="-24.66934375" Y="-3.084939453125" />
                  <Point X="-24.69440625" Y="-3.077160644531" />
                  <Point X="-24.922703125" Y="-3.006306152344" />
                  <Point X="-24.93662109375" Y="-3.003109863281" />
                  <Point X="-24.96478125" Y="-2.998840087891" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.02290625" Y="-2.998840087891" />
                  <Point X="-25.05106640625" Y="-3.003109863281" />
                  <Point X="-25.064984375" Y="-3.006306152344" />
                  <Point X="-25.090046875" Y="-3.014084960938" />
                  <Point X="-25.31834375" Y="-3.084939453125" />
                  <Point X="-25.332927734375" Y="-3.090829345703" />
                  <Point X="-25.360927734375" Y="-3.104936523438" />
                  <Point X="-25.37434375" Y="-3.113153808594" />
                  <Point X="-25.400599609375" Y="-3.132397705078" />
                  <Point X="-25.4124765625" Y="-3.142718261719" />
                  <Point X="-25.43436328125" Y="-3.165177490234" />
                  <Point X="-25.444373046875" Y="-3.177316162109" />
                  <Point X="-25.460568359375" Y="-3.200652587891" />
                  <Point X="-25.608099609375" Y="-3.413216552734" />
                  <Point X="-25.612470703125" Y="-3.420134277344" />
                  <Point X="-25.625974609375" Y="-3.445260498047" />
                  <Point X="-25.63877734375" Y="-3.476213378906" />
                  <Point X="-25.64275390625" Y="-3.487936035156" />
                  <Point X="-25.98542578125" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.853753120457" Y="2.871237801725" />
                  <Point X="-29.150414845965" Y="2.235044678322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.581148375749" Y="1.311333642805" />
                  <Point X="-29.710800505216" Y="1.033293753904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.683969398575" Y="3.01055101789" />
                  <Point X="-29.073216410679" Y="2.175808106645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.48239029168" Y="1.298331887147" />
                  <Point X="-29.773173194231" Y="0.674745940261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.60138269862" Y="2.962869617087" />
                  <Point X="-28.996017975394" Y="2.116571534967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.383632207612" Y="1.285330131489" />
                  <Point X="-29.715729692555" Y="0.573144776744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.518795998665" Y="2.915188216283" />
                  <Point X="-28.918819540109" Y="2.05733496329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.284874123543" Y="1.272328375831" />
                  <Point X="-29.622551162245" Y="0.548177629436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.041742270805" Y="3.713444086735" />
                  <Point X="-28.053116716642" Y="3.68905150892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.43620929871" Y="2.867506815479" />
                  <Point X="-28.841621104823" Y="1.998098391612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.186116039474" Y="1.259326620173" />
                  <Point X="-29.529372631936" Y="0.523210482128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.878593674278" Y="3.838528230659" />
                  <Point X="-27.995129782667" Y="3.588615739729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.353622598755" Y="2.819825414675" />
                  <Point X="-28.764422669538" Y="1.938861819935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.087357955406" Y="1.246324864515" />
                  <Point X="-29.436194101627" Y="0.49824333482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.715445077751" Y="3.963612374583" />
                  <Point X="-27.937142848691" Y="3.488179970538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.271035930653" Y="2.772143945562" />
                  <Point X="-28.687224234252" Y="1.879625248257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.988599871337" Y="1.233323108857" />
                  <Point X="-29.343015571317" Y="0.473276187511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.565060605111" Y="4.061323766497" />
                  <Point X="-27.879155914716" Y="3.387744201348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.183004889337" Y="2.736137972485" />
                  <Point X="-28.610025798967" Y="1.82038867658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.889841787268" Y="1.220321353199" />
                  <Point X="-29.249837041008" Y="0.448309040203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.760974162011" Y="-0.647828053117" />
                  <Point X="-29.781218399376" Y="-0.691241960247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.423588537151" Y="4.139922444896" />
                  <Point X="-27.821168980741" Y="3.287308432157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.083429192482" Y="2.724889593104" />
                  <Point X="-28.533277919283" Y="1.760185885298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.791083651962" Y="1.207319707419" />
                  <Point X="-29.156658510699" Y="0.423341892895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.641186121812" Y="-0.615730922315" />
                  <Point X="-29.75661490133" Y="-0.863268738817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.282116469191" Y="4.218521123295" />
                  <Point X="-27.76695438628" Y="3.178782854772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.973808491446" Y="2.735182794708" />
                  <Point X="-28.468812984825" Y="1.673642232974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.683954748423" Y="1.212269232046" />
                  <Point X="-29.063479980389" Y="0.398374745587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.521398081613" Y="-0.583633791513" />
                  <Point X="-29.727568261068" Y="-1.025767168159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.168615485537" Y="4.237135617826" />
                  <Point X="-27.751905318804" Y="2.986266533723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.808462820597" Y="2.86497857972" />
                  <Point X="-28.422630670334" Y="1.547891375604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.556013142507" Y="1.261851740955" />
                  <Point X="-28.97030145008" Y="0.373407598278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.401610041413" Y="-0.551536660711" />
                  <Point X="-29.690470510613" Y="-1.17099993597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.102443538816" Y="4.154252665114" />
                  <Point X="-28.877122919771" Y="0.34844045097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.281822001214" Y="-0.519439529909" />
                  <Point X="-29.638204332395" Y="-1.283703905474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.024133072614" Y="4.097400851433" />
                  <Point X="-28.783944389462" Y="0.323473303662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.162033961015" Y="-0.487342399107" />
                  <Point X="-29.526527524123" Y="-1.269001367673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.939683728534" Y="4.053713903844" />
                  <Point X="-28.690765859152" Y="0.298506156354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.042245920816" Y="-0.455245268304" />
                  <Point X="-29.41485071585" Y="-1.254298829871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.846218798377" Y="4.029360942991" />
                  <Point X="-28.597587328843" Y="0.273539009045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.922457880616" Y="-0.423148137502" />
                  <Point X="-29.303173907578" Y="-1.23959629207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.731438426839" Y="4.050719093694" />
                  <Point X="-28.504519990139" Y="0.24833341057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.802669840417" Y="-0.3910510067" />
                  <Point X="-29.191497099305" Y="-1.224893754268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.374156149868" Y="4.592124258835" />
                  <Point X="-26.462620720936" Y="4.40241137396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.597334882046" Y="4.113515923169" />
                  <Point X="-28.419738414636" Y="0.205358935568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.682881800218" Y="-0.358953875898" />
                  <Point X="-29.079820291032" Y="-1.210191216467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.253570313281" Y="4.625932269512" />
                  <Point X="-28.348138691862" Y="0.134115886163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.563093725611" Y="-0.326856671309" />
                  <Point X="-28.96814348276" Y="-1.195488678666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.132984476693" Y="4.659740280189" />
                  <Point X="-28.299399666519" Y="0.013847912911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.435784722677" Y="-0.278630783874" />
                  <Point X="-28.856466674487" Y="-1.180786140864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.012398640106" Y="4.693548290867" />
                  <Point X="-28.744789866215" Y="-1.166083603063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.894983760644" Y="4.720556162045" />
                  <Point X="-28.633113057942" Y="-1.151381065261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.784112092983" Y="4.733532070233" />
                  <Point X="-28.52143624967" Y="-1.13667852746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.102616236131" Y="-2.383023030489" />
                  <Point X="-29.152921063608" Y="-2.490902081146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.673240466528" Y="4.746507890053" />
                  <Point X="-28.409759441397" Y="-1.121975989659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.939392034082" Y="-2.257776749998" />
                  <Point X="-29.094096469007" Y="-2.58954148133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.562368840074" Y="4.759483709873" />
                  <Point X="-28.298082633125" Y="-1.107273451857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.776167832032" Y="-2.132530469506" />
                  <Point X="-29.035271874407" Y="-2.688180881513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.45149721362" Y="4.772459529693" />
                  <Point X="-28.190493291522" Y="-1.101336514617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.612943629982" Y="-2.007284189015" />
                  <Point X="-28.973553147441" Y="-2.780613794808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.356736395424" Y="4.750885609708" />
                  <Point X="-28.098110582481" Y="-1.128010306143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.449719427932" Y="-1.882037908524" />
                  <Point X="-28.908553320997" Y="-2.866010367567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.31848445829" Y="4.608128003216" />
                  <Point X="-28.019833841804" Y="-1.184934444445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.286495225882" Y="-1.756791628033" />
                  <Point X="-28.843553494554" Y="-2.951406940327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.280232521155" Y="4.465370396724" />
                  <Point X="-27.959409438475" Y="-1.280143043738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.123270919734" Y="-1.631545124303" />
                  <Point X="-28.761014676802" Y="-2.999191024848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.241980708941" Y="4.322612522339" />
                  <Point X="-28.617577122446" Y="-2.91637734727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.19524444803" Y="4.198049606904" />
                  <Point X="-28.47413956809" Y="-2.833563669692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.124957832112" Y="4.123990590758" />
                  <Point X="-28.330702013734" Y="-2.750749992113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.036196310599" Y="4.089551137518" />
                  <Point X="-28.187264459378" Y="-2.667936314535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.607765042643" Y="4.783535806212" />
                  <Point X="-24.641701005138" Y="4.710759899789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.928688874433" Y="4.095312427983" />
                  <Point X="-28.043826905022" Y="-2.585122636957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.507824748109" Y="4.77306930908" />
                  <Point X="-27.900389350666" Y="-2.502308959379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.407884453575" Y="4.762602811947" />
                  <Point X="-27.75695179631" Y="-2.419495281801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.307944159041" Y="4.752136314814" />
                  <Point X="-27.622461844012" Y="-2.355869798759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.208003864506" Y="4.741669817681" />
                  <Point X="-27.516415920916" Y="-2.353242733188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.111733874314" Y="4.723332327486" />
                  <Point X="-27.42969485535" Y="-2.392057958326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.017519706636" Y="4.700586111682" />
                  <Point X="-27.35787216436" Y="-2.462822850848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.923305538958" Y="4.677839895878" />
                  <Point X="-27.310770383094" Y="-2.586601905355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.515182068133" Y="-3.024964178553" />
                  <Point X="-27.914959317494" Y="-3.882289256471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.82909137128" Y="4.655093680074" />
                  <Point X="-27.83783096581" Y="-3.941676122916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.734877203602" Y="4.63234746427" />
                  <Point X="-27.760702585984" Y="-4.001062929012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.640663035924" Y="4.609601248466" />
                  <Point X="-27.680081354149" Y="-4.052959289799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.546448868246" Y="4.586855032662" />
                  <Point X="-27.598744422167" Y="-4.103320826672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.45678992233" Y="4.554340112265" />
                  <Point X="-27.517407490186" Y="-4.153682363545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.367133018063" Y="4.521820813537" />
                  <Point X="-27.327268715927" Y="-3.970717596687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.277476113797" Y="4.489301514808" />
                  <Point X="-27.175005781417" Y="-3.868977830293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.18781920953" Y="4.45678221608" />
                  <Point X="-27.03268498493" Y="-3.788559047695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.099913673366" Y="4.420507096336" />
                  <Point X="-26.92267610804" Y="-3.777433400286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.013859262175" Y="4.380262226276" />
                  <Point X="-26.820963876835" Y="-3.784099966967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.927804850984" Y="4.340017356217" />
                  <Point X="-26.719251645631" Y="-3.790766533647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.841750439792" Y="4.299772486157" />
                  <Point X="-26.618350139037" Y="-3.799171704868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.755696028601" Y="4.259527616098" />
                  <Point X="-26.528917868718" Y="-3.83217273265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.673008911081" Y="4.212061561457" />
                  <Point X="-26.452089321294" Y="-3.892202531406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.590581252988" Y="4.164039094281" />
                  <Point X="-26.37769241128" Y="-3.957446993417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.508153594894" Y="4.116016627105" />
                  <Point X="-26.303295501267" Y="-4.022691455429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.4257259368" Y="4.067994159929" />
                  <Point X="-26.231565130898" Y="-4.09365433016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.344525136281" Y="4.017340688192" />
                  <Point X="-26.180690957517" Y="-4.209343463669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.265807618194" Y="3.961361800096" />
                  <Point X="-22.760962802297" Y="2.899498081061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.986512435273" Y="2.415805332226" />
                  <Point X="-26.149347142296" Y="-4.366915585413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.187090075173" Y="3.90538296547" />
                  <Point X="-22.215968122696" Y="3.843453792707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.962427038051" Y="2.242667482853" />
                  <Point X="-25.582834455015" Y="-3.376814357381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.653699080159" Y="-3.528784036422" />
                  <Point X="-26.120295832508" Y="-4.529404000922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.90398489751" Y="2.143207907292" />
                  <Point X="-25.348258294225" Y="-3.098553307581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.79529417303" Y="-4.057224843394" />
                  <Point X="-26.113020136037" Y="-4.738590369889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.833694823262" Y="2.069156307561" />
                  <Point X="-25.22333981665" Y="-3.055453918322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.936889265901" Y="-4.585665650366" />
                  <Point X="-26.018374356989" Y="-4.760410992121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.753800191133" Y="2.015701748673" />
                  <Point X="-25.100781860681" Y="-3.01741668398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.663406347826" Y="1.984762820817" />
                  <Point X="-24.986797974464" Y="-2.997766601563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.562966348861" Y="1.975367943294" />
                  <Point X="-24.890604556975" Y="-3.016268302449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.448117711684" Y="1.996872490133" />
                  <Point X="-24.79903582932" Y="-3.044687682689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.315901477657" Y="2.055621968607" />
                  <Point X="-24.707467101664" Y="-3.07310706293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.172463878107" Y="2.138435743104" />
                  <Point X="-24.619548226838" Y="-3.109353577821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.029026278557" Y="2.221249517601" />
                  <Point X="-24.545297762671" Y="-3.174912093963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.885588679007" Y="2.304063292098" />
                  <Point X="-24.482474858736" Y="-3.264977092107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.742151079456" Y="2.386877066596" />
                  <Point X="-24.419777616986" Y="-3.355311573678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.598713479906" Y="2.469690841093" />
                  <Point X="-24.060161785686" Y="-2.808902085129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.272775962113" Y="-3.264854657875" />
                  <Point X="-24.359390945261" Y="-3.450601088655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.455275880356" Y="2.55250461559" />
                  <Point X="-21.836018220025" Y="1.736000033239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.998840825931" Y="1.386825828057" />
                  <Point X="-23.899822375743" Y="-2.689842261275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.258254285048" Y="-3.458501971312" />
                  <Point X="-24.31830999935" Y="-3.587291866248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.311838280806" Y="2.635318390087" />
                  <Point X="-21.672114898947" Y="1.862702689184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.966333151193" Y="1.231749611104" />
                  <Point X="-23.783011605574" Y="-2.664129906657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.23517577267" Y="-3.6337990922" />
                  <Point X="-24.280058246563" Y="-3.730049868075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.168400681256" Y="2.718132164584" />
                  <Point X="-21.508890625449" Y="1.987949122898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.908506565684" Y="1.130969973517" />
                  <Point X="-23.673491159992" Y="-2.654051703567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.212097260291" Y="-3.809096213089" />
                  <Point X="-24.241806345535" Y="-3.872807551997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.044237948589" Y="2.759610853659" />
                  <Point X="-21.34566635195" Y="2.113195556613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.840124387117" Y="1.052826878294" />
                  <Point X="-23.565451767558" Y="-2.647149629205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.189018747913" Y="-3.984393333977" />
                  <Point X="-24.203554444507" Y="-4.015565235919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.980635384674" Y="2.671217841736" />
                  <Point X="-21.182442078451" Y="2.238441990327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.758533389218" Y="1.003010187541" />
                  <Point X="-23.470781783041" Y="-2.668918342645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.919732647682" Y="2.577035032293" />
                  <Point X="-21.019217804952" Y="2.363688424041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.669791242011" Y="0.968529185968" />
                  <Point X="-23.388274938469" Y="-2.716770993868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.571710755933" Y="0.954074316729" />
                  <Point X="-23.307630539903" Y="-2.768617673444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.465398283966" Y="0.957272998199" />
                  <Point X="-23.226986031786" Y="-2.820464118086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.3537214709" Y="0.97197554628" />
                  <Point X="-22.822169526724" Y="-2.177121471843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.999408784025" Y="-2.557212285711" />
                  <Point X="-23.154996117118" Y="-2.890870398273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.242044657834" Y="0.986678094361" />
                  <Point X="-22.677833842324" Y="-2.092381748171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.971977012942" Y="-2.72317381318" />
                  <Point X="-23.089797160064" Y="-2.97583993406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.130367844768" Y="1.001380642443" />
                  <Point X="-21.429040564846" Y="0.360874927268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.643708957773" Y="-0.099482926979" />
                  <Point X="-22.545203943112" Y="-2.032745161844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.933967326161" Y="-2.866450927232" />
                  <Point X="-23.024598124776" Y="-3.060809302073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.018691031702" Y="1.016083190524" />
                  <Point X="-21.294306427558" Y="0.425024066711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.604609290832" Y="-0.240422571034" />
                  <Point X="-21.991723651878" Y="-1.070591997325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.131649785631" Y="-1.370664559519" />
                  <Point X="-22.438403450997" Y="-2.028499917787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.876247434396" Y="-2.96745937029" />
                  <Point X="-22.959399040572" Y="-3.145778565187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.907014218636" Y="1.030785738605" />
                  <Point X="-21.174518496842" Y="0.457120962726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.541118830977" Y="-0.329055990888" />
                  <Point X="-21.832367516462" Y="-0.9536408125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.123237119323" Y="-1.5774126888" />
                  <Point X="-22.34172431605" Y="-2.045959994225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.81826071553" Y="-3.067895600785" />
                  <Point X="-22.894199956369" Y="-3.230747828302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.79533740557" Y="1.045488286686" />
                  <Point X="-21.054730448082" Y="0.489218111887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.468813509867" Y="-0.398785879778" />
                  <Point X="-21.715541735878" Y="-0.927896267942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.065508411801" Y="-1.678402226408" />
                  <Point X="-22.245045176556" Y="-2.06342006091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.760273996665" Y="-3.16833183128" />
                  <Point X="-22.829000872166" Y="-3.315717091416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.683660592504" Y="1.060190834768" />
                  <Point X="-20.934942399322" Y="0.521315261048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.386856291059" Y="-0.447817207257" />
                  <Point X="-21.601687246269" Y="-0.908523677444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.004753483044" Y="-1.772902011633" />
                  <Point X="-22.154160836118" Y="-2.093307114275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.702287277799" Y="-3.268768061774" />
                  <Point X="-22.763801787962" Y="-3.400686354531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.571983779437" Y="1.074893382849" />
                  <Point X="-20.815154350561" Y="0.553412410209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.299882364057" Y="-0.486090169296" />
                  <Point X="-21.501024751432" Y="-0.917441411031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.934153685139" Y="-1.846289406838" />
                  <Point X="-22.071520313033" Y="-2.140873091003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.644300558934" Y="-3.369204292269" />
                  <Point X="-22.698602703759" Y="-3.485655617645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.460306966371" Y="1.08959593093" />
                  <Point X="-20.695366301801" Y="0.58550955937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.206703809967" Y="-0.511057265608" />
                  <Point X="-21.402266670079" Y="-0.930443172512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.856955222222" Y="-1.905525919259" />
                  <Point X="-21.988933683542" Y="-2.188554642918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.586313840068" Y="-3.469640522764" />
                  <Point X="-22.633403619555" Y="-3.570624880759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.348630153305" Y="1.104298479011" />
                  <Point X="-20.575578253041" Y="0.617606708532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.113525255878" Y="-0.536024361919" />
                  <Point X="-21.303508576589" Y="-0.943444907965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.779756759305" Y="-1.96476243168" />
                  <Point X="-21.906347005344" Y="-2.236236090381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.528327121203" Y="-3.570076753259" />
                  <Point X="-22.568204535352" Y="-3.655594143874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.276513293735" Y="1.034164433045" />
                  <Point X="-20.455790204281" Y="0.649703857693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.020346701788" Y="-0.560991458231" />
                  <Point X="-21.204750483098" Y="-0.956446643418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.702558296387" Y="-2.0239989441" />
                  <Point X="-21.823760327147" Y="-2.283917537844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.470340402337" Y="-3.670512983753" />
                  <Point X="-22.503005451149" Y="-3.740563406988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.242383785894" Y="0.882566248405" />
                  <Point X="-20.33600215552" Y="0.681801006854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.927168147699" Y="-0.585958554542" />
                  <Point X="-21.105992389608" Y="-0.969448378871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.62535983347" Y="-2.083235456521" />
                  <Point X="-21.741173648949" Y="-2.331598985307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.412353683472" Y="-3.770949214248" />
                  <Point X="-22.437806366945" Y="-3.825532670102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.216146042803" Y="0.714044119642" />
                  <Point X="-20.21621410676" Y="0.713898156015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.833989593609" Y="-0.610925650854" />
                  <Point X="-21.007234296117" Y="-0.982450114324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.548161370553" Y="-2.142471968942" />
                  <Point X="-21.658586970752" Y="-2.37928043277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.354366964606" Y="-3.871385444743" />
                  <Point X="-22.372607282742" Y="-3.910501933217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.74081103952" Y="-0.635892747165" />
                  <Point X="-20.908476202627" Y="-0.995451849777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.470962907636" Y="-2.201708481363" />
                  <Point X="-21.576000292554" Y="-2.426961880233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.296380245741" Y="-3.971821675238" />
                  <Point X="-22.307408198538" Y="-3.995471196331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.64763248543" Y="-0.660859843477" />
                  <Point X="-20.809718109136" Y="-1.008453585229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.393764444718" Y="-2.260944993784" />
                  <Point X="-21.493413614357" Y="-2.474643327696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.554453931341" Y="-0.685826939788" />
                  <Point X="-20.710960015646" Y="-1.021455320682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.316565981801" Y="-2.320181506204" />
                  <Point X="-21.410826936159" Y="-2.522324775159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.461275377251" Y="-0.7107940361" />
                  <Point X="-20.612201922155" Y="-1.034457056135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.239367518884" Y="-2.379418018625" />
                  <Point X="-21.328240257962" Y="-2.570006222622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.368096823162" Y="-0.735761132411" />
                  <Point X="-20.513443828665" Y="-1.047458791588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.162169055967" Y="-2.438654531046" />
                  <Point X="-21.245653579764" Y="-2.617687670085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.274918269072" Y="-0.760728228722" />
                  <Point X="-20.414685735174" Y="-1.060460527041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.08497059305" Y="-2.497891043467" />
                  <Point X="-21.163066901566" Y="-2.665369117548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.233060440981" Y="-0.895752977102" />
                  <Point X="-20.315927641684" Y="-1.073462262494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.007772130132" Y="-2.557127555888" />
                  <Point X="-21.080480223369" Y="-2.713050565011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.930573667215" Y="-2.616364068308" />
                  <Point X="-20.995598696606" Y="-2.755810693844" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.48883203125" Y="-3.685004638672" />
                  <Point X="-24.528458984375" Y="-3.537112548828" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.551876953125" Y="-3.498207519531" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.7507265625" Y="-3.25862109375" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.0337265625" Y="-3.195545410156" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643798828" />
                  <Point X="-25.304474609375" Y="-3.308980224609" />
                  <Point X="-25.452005859375" Y="-3.521544189453" />
                  <Point X="-25.4592265625" Y="-3.537111816406" />
                  <Point X="-25.8201796875" Y="-4.884209472656" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.128037109375" Y="-4.930914550781" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.313919921875" Y="-4.5872734375" />
                  <Point X="-26.3091484375" Y="-4.551040039062" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.315669921875" Y="-4.504655273438" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.409357421875" Y="-4.182392089844" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.6798671875" Y="-3.983755615234" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791259766" />
                  <Point X="-27.0153984375" Y="-3.990842773438" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.4621015625" Y="-4.411399414062" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-27.8943203125" Y="-4.137977539062" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.57967578125" Y="-2.756673339844" />
                  <Point X="-27.506033203125" Y="-2.629119384766" />
                  <Point X="-27.499763671875" Y="-2.597589599609" />
                  <Point X="-27.515693359375" Y="-2.567049560547" />
                  <Point X="-27.53132421875" Y="-2.551417236328" />
                  <Point X="-27.560154296875" Y="-2.537198974609" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.75352734375" Y="-3.214261474609" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-28.850634765625" Y="-3.255809082031" />
                  <Point X="-29.161697265625" Y="-2.847136962891" />
                  <Point X="-29.18929296875" Y="-2.80086328125" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-28.292638671875" Y="-1.522016479492" />
                  <Point X="-28.163787109375" Y="-1.423144897461" />
                  <Point X="-28.14505859375" Y="-1.393069091797" />
                  <Point X="-28.1381171875" Y="-1.366264526367" />
                  <Point X="-28.14032421875" Y="-1.334597167969" />
                  <Point X="-28.16377734375" Y="-1.309097167969" />
                  <Point X="-28.187640625" Y="-1.295052490234" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.68624609375" Y="-1.481668334961" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.80573046875" Y="-1.48748815918" />
                  <Point X="-29.927392578125" Y="-1.01118737793" />
                  <Point X="-29.93469140625" Y="-0.960152709961" />
                  <Point X="-29.99839453125" Y="-0.5147421875" />
                  <Point X="-28.704244140625" Y="-0.167975448608" />
                  <Point X="-28.557462890625" Y="-0.128645339966" />
                  <Point X="-28.539150390625" Y="-0.119519729614" />
                  <Point X="-28.514142578125" Y="-0.102163116455" />
                  <Point X="-28.493978515625" Y="-0.072942703247" />
                  <Point X="-28.4856484375" Y="-0.046100971222" />
                  <Point X="-28.4865625" Y="-0.013510365486" />
                  <Point X="-28.4948984375" Y="0.0133478899" />
                  <Point X="-28.516892578125" Y="0.041511356354" />
                  <Point X="-28.541896484375" Y="0.058865184784" />
                  <Point X="-28.557462890625" Y="0.066085227966" />
                  <Point X="-29.894447265625" Y="0.424329223633" />
                  <Point X="-29.998185546875" Y="0.452125823975" />
                  <Point X="-29.996052734375" Y="0.466537689209" />
                  <Point X="-29.91764453125" Y="0.996414916992" />
                  <Point X="-29.90294921875" Y="1.050649414062" />
                  <Point X="-29.773515625" Y="1.528298706055" />
                  <Point X="-28.85848046875" Y="1.40783215332" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866699219" />
                  <Point X="-28.725626953125" Y="1.397782592773" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.65153125" Y="1.426057983398" />
                  <Point X="-28.6391171875" Y="1.443791015625" />
                  <Point X="-28.636681640625" Y="1.449672485352" />
                  <Point X="-28.61447265625" Y="1.503290283203" />
                  <Point X="-28.610712890625" Y="1.524603149414" />
                  <Point X="-28.6163125" Y="1.5455078125" />
                  <Point X="-28.6192578125" Y="1.5511640625" />
                  <Point X="-28.646056640625" Y="1.602642211914" />
                  <Point X="-28.65996875" Y="1.619221923828" />
                  <Point X="-29.426865234375" Y="2.207682861328" />
                  <Point X="-29.47610546875" Y="2.245466308594" />
                  <Point X="-29.46468359375" Y="2.26503515625" />
                  <Point X="-29.16001171875" Y="2.787007568359" />
                  <Point X="-29.121087890625" Y="2.837040527344" />
                  <Point X="-28.774669921875" Y="3.282310791016" />
                  <Point X="-28.22377734375" Y="2.964252441406" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.13005078125" Y="2.919694335938" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927404541016" />
                  <Point X="-28.007244140625" Y="2.933411621094" />
                  <Point X="-27.95252734375" Y="2.988127929688" />
                  <Point X="-27.9408984375" Y="3.006382324219" />
                  <Point X="-27.938072265625" Y="3.027841064453" />
                  <Point X="-27.9388125" Y="3.036304199219" />
                  <Point X="-27.945556640625" Y="3.113390625" />
                  <Point X="-27.95206640625" Y="3.134032714844" />
                  <Point X="-28.29190234375" Y="3.722644775391" />
                  <Point X="-28.30727734375" Y="3.749276855469" />
                  <Point X="-28.283513671875" Y="3.767497070312" />
                  <Point X="-27.752876953125" Y="4.174331054688" />
                  <Point X="-27.691568359375" Y="4.208392578125" />
                  <Point X="-27.141548828125" Y="4.513971679688" />
                  <Point X="-26.987609375" Y="4.313354003906" />
                  <Point X="-26.967826171875" Y="4.287572753906" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.941826171875" Y="4.268757324219" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.80399609375" Y="4.226314453125" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.69690234375" Y="4.275744628906" />
                  <Point X="-26.68608203125" Y="4.294488769531" />
                  <Point X="-26.682888671875" Y="4.304616699219" />
                  <Point X="-26.653802734375" Y="4.396865234375" />
                  <Point X="-26.651916015625" Y="4.418424804688" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.65503515625" Y="4.710701660156" />
                  <Point X="-25.96809375" Y="4.903296386719" />
                  <Point X="-25.8937734375" Y="4.911994628906" />
                  <Point X="-25.22419921875" Y="4.990358398438" />
                  <Point X="-25.061255859375" Y="4.382248046875" />
                  <Point X="-25.042140625" Y="4.310904296875" />
                  <Point X="-25.02428125" Y="4.284177246094" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176269531" />
                  <Point X="-24.945546875" Y="4.310902832031" />
                  <Point X="-24.77141796875" Y="4.960756835938" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.73959375" Y="4.988380859375" />
                  <Point X="-24.139794921875" Y="4.925565429688" />
                  <Point X="-24.0783046875" Y="4.910720214844" />
                  <Point X="-23.491544921875" Y="4.769058105469" />
                  <Point X="-23.451884765625" Y="4.754673339844" />
                  <Point X="-23.06896484375" Y="4.615785644531" />
                  <Point X="-23.030263671875" Y="4.597686035156" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.623904296875" Y="4.40334765625" />
                  <Point X="-22.26747265625" Y="4.195689941406" />
                  <Point X="-22.232212890625" Y="4.170614746094" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.684908203125" Y="2.651232177734" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.777271484375" Y="2.483572265625" />
                  <Point X="-22.7966171875" Y="2.411228271484" />
                  <Point X="-22.797126953125" Y="2.385458251953" />
                  <Point X="-22.789583984375" Y="2.322901123047" />
                  <Point X="-22.781318359375" Y="2.300812011719" />
                  <Point X="-22.777068359375" Y="2.294549316406" />
                  <Point X="-22.738359375" Y="2.237503417969" />
                  <Point X="-22.718796875" Y="2.219953613281" />
                  <Point X="-22.66175" Y="2.181245849609" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.632794921875" Y="2.172151855469" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.543392578125" Y="2.1680703125" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.114216796875" Y="2.96880859375" />
                  <Point X="-21.005751953125" Y="3.031430419922" />
                  <Point X="-20.79740234375" Y="2.741874511719" />
                  <Point X="-20.777748046875" Y="2.709394042969" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.59916015625" Y="1.679193725586" />
                  <Point X="-21.7113828125" Y="1.593082763672" />
                  <Point X="-21.72634375" Y="1.576376220703" />
                  <Point X="-21.77841015625" Y="1.508452148438" />
                  <Point X="-21.7890078125" Y="1.483887451172" />
                  <Point X="-21.808404296875" Y="1.414536743164" />
                  <Point X="-21.809220703125" Y="1.39096484375" />
                  <Point X="-21.80747265625" Y="1.382493530273" />
                  <Point X="-21.79155078125" Y="1.30533203125" />
                  <Point X="-21.779599609375" Y="1.280729125977" />
                  <Point X="-21.736296875" Y="1.214909790039" />
                  <Point X="-21.719052734375" Y="1.198820068359" />
                  <Point X="-21.712162109375" Y="1.194942016602" />
                  <Point X="-21.64941015625" Y="1.159617675781" />
                  <Point X="-21.622119140625" Y="1.152388549805" />
                  <Point X="-21.537271484375" Y="1.141174926758" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.246775390625" Y="1.30934753418" />
                  <Point X="-20.1510234375" Y="1.321953369141" />
                  <Point X="-20.060806640625" Y="0.951367370605" />
                  <Point X="-20.054615234375" Y="0.911595458984" />
                  <Point X="-20.002140625" Y="0.57455645752" />
                  <Point X="-21.13022265625" Y="0.272287506104" />
                  <Point X="-21.25883203125" Y="0.237826919556" />
                  <Point X="-21.2800625" Y="0.227529724121" />
                  <Point X="-21.363421875" Y="0.179347045898" />
                  <Point X="-21.383224609375" Y="0.159930160522" />
                  <Point X="-21.433240234375" Y="0.096199226379" />
                  <Point X="-21.443013671875" Y="0.074735168457" />
                  <Point X="-21.44484375" Y="0.065178009033" />
                  <Point X="-21.461515625" Y="-0.021875238419" />
                  <Point X="-21.459685546875" Y="-0.050242031097" />
                  <Point X="-21.443013671875" Y="-0.137295272827" />
                  <Point X="-21.433240234375" Y="-0.158759185791" />
                  <Point X="-21.42775" Y="-0.16575592041" />
                  <Point X="-21.377734375" Y="-0.229487014771" />
                  <Point X="-21.35426953125" Y="-0.247196685791" />
                  <Point X="-21.270912109375" Y="-0.295379364014" />
                  <Point X="-21.25883203125" Y="-0.300387023926" />
                  <Point X="-20.08737890625" Y="-0.614276977539" />
                  <Point X="-20.001931640625" Y="-0.637172363281" />
                  <Point X="-20.051568359375" Y="-0.966412475586" />
                  <Point X="-20.05950390625" Y="-1.001180419922" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-21.4388671875" Y="-1.117264160156" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.623123046875" Y="-1.102245117188" />
                  <Point X="-21.7867265625" Y="-1.13780480957" />
                  <Point X="-21.802052734375" Y="-1.143923217773" />
                  <Point X="-21.825412109375" Y="-1.167754394531" />
                  <Point X="-21.924298828125" Y="-1.286685424805" />
                  <Point X="-21.932044921875" Y="-1.299518310547" />
                  <Point X="-21.937197265625" Y="-1.330980224609" />
                  <Point X="-21.951369140625" Y="-1.485001220703" />
                  <Point X="-21.9501484375" Y="-1.501458496094" />
                  <Point X="-21.93369921875" Y="-1.532083007812" />
                  <Point X="-21.843158203125" Y="-1.672912597656" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-20.744421875" Y="-2.519714355469" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.7958671875" Y="-2.802140380859" />
                  <Point X="-20.812283203125" Y="-2.825465332031" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-22.11370703125" Y="-2.335909912109" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.28403515625" Y="-2.249452148438" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.528681640625" Y="-2.228591064453" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.33468359375" />
                  <Point X="-22.72074609375" Y="-2.352442382812" />
                  <Point X="-22.80587890625" Y="-2.514201660156" />
                  <Point X="-22.8108359375" Y="-2.546374755859" />
                  <Point X="-22.806974609375" Y="-2.567751464844" />
                  <Point X="-22.77180859375" Y="-2.762465087891" />
                  <Point X="-22.76590625" Y="-2.778578857422" />
                  <Point X="-22.067326171875" Y="-3.988556884766" />
                  <Point X="-22.013326171875" Y="-4.082087890625" />
                  <Point X="-22.164705078125" Y="-4.19021484375" />
                  <Point X="-22.183052734375" Y="-4.202090820312" />
                  <Point X="-22.320224609375" Y="-4.29087890625" />
                  <Point X="-23.215453125" Y="-3.124192626953" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.350533203125" Y="-2.966912597656" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.597251953125" Y="-2.837838867188" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.852470703125" Y="-2.883313232422" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045985839844" />
                  <Point X="-24.036865234375" Y="-3.070478515625" />
                  <Point X="-24.085357421875" Y="-3.293573730469" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-23.88809765625" Y="-4.814471191406" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-24.00564453125" Y="-4.963245605469" />
                  <Point X="-24.022611328125" Y="-4.966327636719" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#126" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.019116756578" Y="4.426469184036" Z="0.25" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.25" />
                  <Point X="-0.905741999842" Y="4.992935613568" Z="0.25" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.25" />
                  <Point X="-1.674690981367" Y="4.790125262255" Z="0.25" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.25" />
                  <Point X="-1.746281347703" Y="4.736646270378" Z="0.25" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.25" />
                  <Point X="-1.736731914597" Y="4.350931711748" Z="0.25" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.25" />
                  <Point X="-1.829288251499" Y="4.303788378637" Z="0.25" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.25" />
                  <Point X="-1.924895927598" Y="4.344387924461" Z="0.25" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.25" />
                  <Point X="-1.954097725343" Y="4.375072419486" Z="0.25" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.25" />
                  <Point X="-2.72200787849" Y="4.283379987571" Z="0.25" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.25" />
                  <Point X="-3.320091864116" Y="3.83845708633" Z="0.25" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.25" />
                  <Point X="-3.34136016004" Y="3.728925181891" Z="0.25" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.25" />
                  <Point X="-2.994780435264" Y="3.063226542393" Z="0.25" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.25" />
                  <Point X="-3.048756466976" Y="3.000047100911" Z="0.25" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.25" />
                  <Point X="-3.131849923214" Y="3.000784263909" Z="0.25" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.25" />
                  <Point X="-3.204934166406" Y="3.038833788706" Z="0.25" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.25" />
                  <Point X="-4.166707661684" Y="2.899023067301" Z="0.25" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.25" />
                  <Point X="-4.514021692094" Y="2.321520262217" Z="0.25" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.25" />
                  <Point X="-4.4634597207" Y="2.199295151102" Z="0.25" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.25" />
                  <Point X="-3.669764454696" Y="1.55935595192" Z="0.25" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.25" />
                  <Point X="-3.689031651478" Y="1.500086582201" Z="0.25" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.25" />
                  <Point X="-3.746819475151" Y="1.476748476052" Z="0.25" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.25" />
                  <Point X="-3.858112894101" Y="1.488684596593" Z="0.25" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.25" />
                  <Point X="-4.957365313686" Y="1.095006824121" Z="0.25" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.25" />
                  <Point X="-5.051671466152" Y="0.505136655274" Z="0.25" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.25" />
                  <Point X="-4.913545175851" Y="0.407312938614" Z="0.25" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.25" />
                  <Point X="-3.551553318211" Y="0.031712456245" Z="0.25" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.25" />
                  <Point X="-3.540471910368" Y="0.002948658545" Z="0.25" />
                  <Point X="-3.539556741714" Y="0" Z="0.25" />
                  <Point X="-3.547892690767" Y="-0.026858292516" Z="0.25" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.25" />
                  <Point X="-3.573815276912" Y="-0.047163526889" Z="0.25" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.25" />
                  <Point X="-3.723342612921" Y="-0.088399118142" Z="0.25" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.25" />
                  <Point X="-4.990344782163" Y="-0.935951681418" Z="0.25" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.25" />
                  <Point X="-4.860333250768" Y="-1.468582225236" Z="0.25" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.25" />
                  <Point X="-4.685878382942" Y="-1.499960555699" Z="0.25" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.25" />
                  <Point X="-3.195296100457" Y="-1.320907906204" Z="0.25" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.25" />
                  <Point X="-3.199618633322" Y="-1.349254406087" Z="0.25" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.25" />
                  <Point X="-3.329232831526" Y="-1.451068805677" Z="0.25" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.25" />
                  <Point X="-4.238393946334" Y="-2.795193004879" Z="0.25" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.25" />
                  <Point X="-3.896577402472" Y="-3.254812298797" Z="0.25" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.25" />
                  <Point X="-3.734684794042" Y="-3.226282655422" Z="0.25" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.25" />
                  <Point X="-2.557207177176" Y="-2.571123291956" Z="0.25" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.25" />
                  <Point X="-2.629134357747" Y="-2.700393588451" Z="0.25" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.25" />
                  <Point X="-2.930980486536" Y="-4.146314673662" Z="0.25" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.25" />
                  <Point X="-2.494581307287" Y="-4.422630015827" Z="0.25" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.25" />
                  <Point X="-2.428869924288" Y="-4.420547642742" Z="0.25" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.25" />
                  <Point X="-1.993775362114" Y="-4.001135887661" Z="0.25" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.25" />
                  <Point X="-1.68929301696" Y="-4.002368587326" Z="0.25" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.25" />
                  <Point X="-1.448481356982" Y="-4.188703915187" Z="0.25" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.25" />
                  <Point X="-1.370866224661" Y="-4.483130111879" Z="0.25" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.25" />
                  <Point X="-1.369648759305" Y="-4.549465664335" Z="0.25" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.25" />
                  <Point X="-1.146653743667" Y="-4.948057311419" Z="0.25" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.25" />
                  <Point X="-0.847319301783" Y="-5.00800754434" Z="0.25" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.25" />
                  <Point X="-0.778040495755" Y="-4.865870781312" Z="0.25" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.25" />
                  <Point X="-0.269555874626" Y="-3.30620917091" Z="0.25" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.25" />
                  <Point X="-0.025063597175" Y="-3.212018184068" Z="0.25" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.25" />
                  <Point X="0.228295482187" Y="-3.27509386122" Z="0.25" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.25" />
                  <Point X="0.400889984807" Y="-3.495436584858" Z="0.25" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.25" />
                  <Point X="0.456714364646" Y="-3.666665248503" Z="0.25" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.25" />
                  <Point X="0.980169782685" Y="-4.984242322291" Z="0.25" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.25" />
                  <Point X="1.159341128603" Y="-4.945637721276" Z="0.25" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.25" />
                  <Point X="1.155318397258" Y="-4.776664808499" Z="0.25" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.25" />
                  <Point X="1.005836432452" Y="-3.049818673994" Z="0.25" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.25" />
                  <Point X="1.173338824455" Y="-2.890479652521" Z="0.25" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.25" />
                  <Point X="1.401172307812" Y="-2.856348421006" Z="0.25" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.25" />
                  <Point X="1.616270618451" Y="-2.977690669625" Z="0.25" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.25" />
                  <Point X="1.738721832971" Y="-3.123350508847" Z="0.25" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.25" />
                  <Point X="2.837960718078" Y="-4.212784846692" Z="0.25" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.25" />
                  <Point X="3.027792095669" Y="-4.078486598001" Z="0.25" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.25" />
                  <Point X="2.969818300661" Y="-3.932276600952" Z="0.25" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.25" />
                  <Point X="2.236071746353" Y="-2.527584971089" Z="0.25" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.25" />
                  <Point X="2.317343834867" Y="-2.3444490492" Z="0.25" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.25" />
                  <Point X="2.488449329172" Y="-2.2415574751" Z="0.25" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.25" />
                  <Point X="2.700921832423" Y="-2.267376263138" Z="0.25" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.25" />
                  <Point X="2.855136996155" Y="-2.347931199613" Z="0.25" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.25" />
                  <Point X="4.222449192394" Y="-2.8229623523" Z="0.25" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.25" />
                  <Point X="4.383431332441" Y="-2.565876769061" Z="0.25" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.25" />
                  <Point X="4.279858684945" Y="-2.448766479465" Z="0.25" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.25" />
                  <Point X="3.102202541878" Y="-1.473763569655" Z="0.25" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.25" />
                  <Point X="3.106435467438" Y="-1.304281541985" Z="0.25" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.25" />
                  <Point X="3.206879074862" Y="-1.16844112681" Z="0.25" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.25" />
                  <Point X="3.381338572542" Y="-1.119824384595" Z="0.25" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.25" />
                  <Point X="3.548450062182" Y="-1.135556419612" Z="0.25" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.25" />
                  <Point X="4.983085971276" Y="-0.981024214964" Z="0.25" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.25" />
                  <Point X="5.042418253526" Y="-0.606284112777" Z="0.25" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.25" />
                  <Point X="4.919406185107" Y="-0.53470063786" Z="0.25" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.25" />
                  <Point X="3.664594283302" Y="-0.172627978059" Z="0.25" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.25" />
                  <Point X="3.605427664875" Y="-0.103607280602" Z="0.25" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.25" />
                  <Point X="3.583265040437" Y="-0.009557215996" Z="0.25" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.25" />
                  <Point X="3.598106409987" Y="0.087053315246" Z="0.25" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.25" />
                  <Point X="3.649951773525" Y="0.160341457973" Z="0.25" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.25" />
                  <Point X="3.738801131051" Y="0.215520866183" Z="0.25" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.25" />
                  <Point X="3.8765616322" Y="0.255271294909" Z="0.25" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.25" />
                  <Point X="4.988632276004" Y="0.950567594062" Z="0.25" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.25" />
                  <Point X="4.890809300289" Y="1.367488253132" Z="0.25" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.25" />
                  <Point X="4.740542767826" Y="1.390199846582" Z="0.25" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.25" />
                  <Point X="3.378275494314" Y="1.233237505042" Z="0.25" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.25" />
                  <Point X="3.306207907303" Y="1.269792920688" Z="0.25" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.25" />
                  <Point X="3.256015099138" Y="1.339490375954" Z="0.25" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.25" />
                  <Point X="3.235339939004" Y="1.423877963189" Z="0.25" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.25" />
                  <Point X="3.252986724602" Y="1.501699974397" Z="0.25" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.25" />
                  <Point X="3.307182046088" Y="1.577237981017" Z="0.25" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.25" />
                  <Point X="3.425120261183" Y="1.670806145453" Z="0.25" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.25" />
                  <Point X="4.258871994499" Y="2.76656022489" Z="0.25" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.25" />
                  <Point X="4.025599872912" Y="3.096191071064" Z="0.25" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.25" />
                  <Point X="3.854626853641" Y="3.043389882309" Z="0.25" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.25" />
                  <Point X="2.437533990362" Y="2.247653058555" Z="0.25" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.25" />
                  <Point X="2.3670346569" Y="2.253072390184" Z="0.25" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.25" />
                  <Point X="2.303120899549" Y="2.292608488689" Z="0.25" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.25" />
                  <Point X="2.258150111546" Y="2.353903960833" Z="0.25" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.25" />
                  <Point X="2.246357312575" Y="2.422723786932" Z="0.25" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.25" />
                  <Point X="2.264874884102" Y="2.50193558552" Z="0.25" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.25" />
                  <Point X="2.35223547055" Y="2.657512228314" Z="0.25" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.25" />
                  <Point X="2.790607524574" Y="4.242641643271" Z="0.25" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.25" />
                  <Point X="2.395108745624" Y="4.477830317138" Z="0.25" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.25" />
                  <Point X="1.984761857027" Y="4.674258098072" Z="0.25" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.25" />
                  <Point X="1.559007690187" Y="4.832957331747" Z="0.25" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.25" />
                  <Point X="0.927273442038" Y="4.990603827457" Z="0.25" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.25" />
                  <Point X="0.259456591772" Y="5.069388864444" Z="0.25" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.25" />
                  <Point X="0.174127804975" Y="5.004978321915" Z="0.25" />
                  <Point X="0" Y="4.355124473572" Z="0.25" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>