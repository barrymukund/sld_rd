<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#169" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2012" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000473632812" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.23147265625" Y="-4.278424804688" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497139648438" />
                  <Point X="-24.457634765625" Y="-3.467376708984" />
                  <Point X="-24.541515625" Y="-3.346522216797" />
                  <Point X="-24.62136328125" Y="-3.231476318359" />
                  <Point X="-24.643248046875" Y="-3.209021484375" />
                  <Point X="-24.66950390625" Y="-3.18977734375" />
                  <Point X="-24.69750390625" Y="-3.175669189453" />
                  <Point X="-24.827302734375" Y="-3.135384277344" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.166623046875" Y="-3.137320556641" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.18977734375" />
                  <Point X="-25.344439453125" Y="-3.209021484375" />
                  <Point X="-25.36632421875" Y="-3.231477294922" />
                  <Point X="-25.450203125" Y="-3.352331542969" />
                  <Point X="-25.53005078125" Y="-3.467377685547" />
                  <Point X="-25.5381875" Y="-3.481571777344" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.746349609375" Y="-4.241614257813" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-25.937439453125" Y="-4.872893554688" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.226603515625" Y="-4.807460449219" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.239666015625" Y="-4.751083007812" />
                  <Point X="-26.2149609375" Y="-4.563438476562" />
                  <Point X="-26.21419921875" Y="-4.5479296875" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.247517578125" Y="-4.360331054688" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.182964355469" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.443146484375" Y="-4.026403564453" />
                  <Point X="-26.556904296875" Y="-3.926639648438" />
                  <Point X="-26.5831875" Y="-3.910295898438" />
                  <Point X="-26.612884765625" Y="-3.897994628906" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.8016328125" Y="-3.880570800781" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708496094" />
                  <Point X="-27.014466796875" Y="-3.882028076172" />
                  <Point X="-27.042658203125" Y="-3.894801513672" />
                  <Point X="-27.17481640625" Y="-3.983107421875" />
                  <Point X="-27.300623046875" Y="-4.067169677734" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.3351015625" Y="-4.099461914062" />
                  <Point X="-27.44478125" Y="-4.242397949219" />
                  <Point X="-27.4801484375" Y="-4.288488769531" />
                  <Point X="-27.59371484375" Y="-4.218171386719" />
                  <Point X="-27.80171484375" Y="-4.0893828125" />
                  <Point X="-28.005623046875" Y="-3.932379638672" />
                  <Point X="-28.10472265625" Y="-3.856077148438" />
                  <Point X="-27.80514453125" Y="-3.337193115234" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647654052734" />
                  <Point X="-27.406587890625" Y="-2.616126708984" />
                  <Point X="-27.405576171875" Y="-2.585190429688" />
                  <Point X="-27.414560546875" Y="-2.555570800781" />
                  <Point X="-27.428779296875" Y="-2.526741455078" />
                  <Point X="-27.44680078125" Y="-2.501591552734" />
                  <Point X="-27.464142578125" Y="-2.484247802734" />
                  <Point X="-27.489298828125" Y="-2.466219482422" />
                  <Point X="-27.51812890625" Y="-2.451999267578" />
                  <Point X="-27.54774609375" Y="-2.443012207031" />
                  <Point X="-27.57868359375" Y="-2.444023925781" />
                  <Point X="-27.610212890625" Y="-2.450294677734" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.2680078125" Y="-2.82425" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-28.91853515625" Y="-3.009750732422" />
                  <Point X="-29.082859375" Y="-2.793861083984" />
                  <Point X="-29.229048828125" Y="-2.548723144531" />
                  <Point X="-29.306142578125" Y="-2.419449951172" />
                  <Point X="-28.77325" Y="-2.01054699707" />
                  <Point X="-28.105955078125" Y="-1.498513305664" />
                  <Point X="-28.084578125" Y="-1.475594726562" />
                  <Point X="-28.06661328125" Y="-1.44846484375" />
                  <Point X="-28.053857421875" Y="-1.419835327148" />
                  <Point X="-28.04615234375" Y="-1.390088134766" />
                  <Point X="-28.04334765625" Y="-1.359663574219" />
                  <Point X="-28.0455546875" Y="-1.327991699219" />
                  <Point X="-28.0525546875" Y="-1.298242553711" />
                  <Point X="-28.068638671875" Y="-1.272255859375" />
                  <Point X="-28.08947265625" Y="-1.248298706055" />
                  <Point X="-28.112970703125" Y="-1.228766601562" />
                  <Point X="-28.139453125" Y="-1.213180053711" />
                  <Point X="-28.16871875" Y="-1.201955566406" />
                  <Point X="-28.20060546875" Y="-1.195474609375" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-29.02576171875" Y="-1.298893798828" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.769806640625" Y="-1.244266967773" />
                  <Point X="-29.834078125" Y="-0.992650268555" />
                  <Point X="-29.87275390625" Y="-0.722222351074" />
                  <Point X="-29.892421875" Y="-0.584698364258" />
                  <Point X="-29.2930234375" Y="-0.42408984375" />
                  <Point X="-28.532875" Y="-0.220408432007" />
                  <Point X="-28.5101875" Y="-0.211047363281" />
                  <Point X="-28.48040234375" Y="-0.193959106445" />
                  <Point X="-28.46723046875" Y="-0.184844848633" />
                  <Point X="-28.441876953125" Y="-0.163933303833" />
                  <Point X="-28.425736328125" Y="-0.146853469849" />
                  <Point X="-28.414283203125" Y="-0.126334373474" />
                  <Point X="-28.402119140625" Y="-0.096326332092" />
                  <Point X="-28.39758203125" Y="-0.081954216003" />
                  <Point X="-28.390822265625" Y="-0.052596595764" />
                  <Point X="-28.388400390625" Y="-0.031193738937" />
                  <Point X="-28.390861328125" Y="-0.00979535675" />
                  <Point X="-28.39784765625" Y="0.020296171188" />
                  <Point X="-28.402419921875" Y="0.034683013916" />
                  <Point X="-28.414357421875" Y="0.063957149506" />
                  <Point X="-28.425853515625" Y="0.08445135498" />
                  <Point X="-28.44202734375" Y="0.101497032166" />
                  <Point X="-28.468064453125" Y="0.122882926941" />
                  <Point X="-28.48126171875" Y="0.131973648071" />
                  <Point X="-28.51036328125" Y="0.148587554932" />
                  <Point X="-28.532875" Y="0.157848297119" />
                  <Point X="-29.256490234375" Y="0.351740600586" />
                  <Point X="-29.89181640625" Y="0.521975585938" />
                  <Point X="-29.865908203125" Y="0.697063354492" />
                  <Point X="-29.82448828125" Y="0.976967895508" />
                  <Point X="-29.74662890625" Y="1.264294799805" />
                  <Point X="-29.70355078125" Y="1.423268066406" />
                  <Point X="-29.31055859375" Y="1.371529663086" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228149414" />
                  <Point X="-28.703134765625" Y="1.305263671875" />
                  <Point X="-28.671666015625" Y="1.315185913086" />
                  <Point X="-28.641708984375" Y="1.324631225586" />
                  <Point X="-28.622775390625" Y="1.332962524414" />
                  <Point X="-28.60403125" Y="1.343784790039" />
                  <Point X="-28.587353515625" Y="1.356014282227" />
                  <Point X="-28.573716796875" Y="1.371563232422" />
                  <Point X="-28.56130078125" Y="1.389293457031" />
                  <Point X="-28.55134765625" Y="1.407432495117" />
                  <Point X="-28.538720703125" Y="1.437917358398" />
                  <Point X="-28.526701171875" Y="1.466936889648" />
                  <Point X="-28.5209140625" Y="1.486796875" />
                  <Point X="-28.51715625" Y="1.508111572266" />
                  <Point X="-28.515802734375" Y="1.528754638672" />
                  <Point X="-28.518951171875" Y="1.549200927734" />
                  <Point X="-28.5245546875" Y="1.570107055664" />
                  <Point X="-28.53205078125" Y="1.589378662109" />
                  <Point X="-28.547287109375" Y="1.618646728516" />
                  <Point X="-28.561791015625" Y="1.646508300781" />
                  <Point X="-28.57328125" Y="1.663705810547" />
                  <Point X="-28.587193359375" Y="1.680285888672" />
                  <Point X="-28.60213671875" Y="1.694590087891" />
                  <Point X="-29.017203125" Y="2.013083129883" />
                  <Point X="-29.351859375" Y="2.269873535156" />
                  <Point X="-29.24209375" Y="2.457928222656" />
                  <Point X="-29.0811484375" Y="2.733665039062" />
                  <Point X="-28.874912109375" Y="2.998753662109" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.541314453125" Y="3.037886230469" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.10296484375" Y="2.821961914062" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.04056640625" Y="2.818762939453" />
                  <Point X="-28.019107421875" Y="2.821587890625" />
                  <Point X="-27.999015625" Y="2.826504150391" />
                  <Point X="-27.980462890625" Y="2.835652832031" />
                  <Point X="-27.962208984375" Y="2.847281738281" />
                  <Point X="-27.946076171875" Y="2.860229003906" />
                  <Point X="-27.914966796875" Y="2.891338378906" />
                  <Point X="-27.8853515625" Y="2.920952392578" />
                  <Point X="-27.872404296875" Y="2.937085693359" />
                  <Point X="-27.860775390625" Y="2.955340332031" />
                  <Point X="-27.85162890625" Y="2.973889160156" />
                  <Point X="-27.8467109375" Y="2.993977783203" />
                  <Point X="-27.843884765625" Y="3.015437255859" />
                  <Point X="-27.84343359375" Y="3.036120361328" />
                  <Point X="-27.847267578125" Y="3.079948242188" />
                  <Point X="-27.85091796875" Y="3.121669921875" />
                  <Point X="-27.854955078125" Y="3.141963867188" />
                  <Point X="-27.86146484375" Y="3.162605712891" />
                  <Point X="-27.869794921875" Y="3.181532958984" />
                  <Point X="-28.053724609375" Y="3.500107666016" />
                  <Point X="-28.18333203125" Y="3.724595703125" />
                  <Point X="-27.9809296875" Y="3.879776855469" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.3758046875" Y="4.275147460938" />
                  <Point X="-27.16703515625" Y="4.391134277344" />
                  <Point X="-27.14564453125" Y="4.363255859375" />
                  <Point X="-27.0431953125" Y="4.229741210938" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.99511328125" Y="4.189395019531" />
                  <Point X="-26.946333984375" Y="4.164001464844" />
                  <Point X="-26.899896484375" Y="4.139828125" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.797314453125" Y="4.128692871094" />
                  <Point X="-26.777451171875" Y="4.134481933594" />
                  <Point X="-26.72664453125" Y="4.155527832031" />
                  <Point X="-26.67827734375" Y="4.175561523437" />
                  <Point X="-26.660146484375" Y="4.185509277344" />
                  <Point X="-26.642416015625" Y="4.197923828125" />
                  <Point X="-26.62686328125" Y="4.2115625" />
                  <Point X="-26.6146328125" Y="4.228244140625" />
                  <Point X="-26.603810546875" Y="4.246988769531" />
                  <Point X="-26.595478515625" Y="4.265921386719" />
                  <Point X="-26.57894140625" Y="4.318370117188" />
                  <Point X="-26.56319921875" Y="4.368297851562" />
                  <Point X="-26.5591640625" Y="4.388583984375" />
                  <Point X="-26.55727734375" Y="4.410145019531" />
                  <Point X="-26.557728515625" Y="4.430827148438" />
                  <Point X="-26.578640625" Y="4.589659667969" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.312505859375" Y="4.708072265625" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.555857421875" Y="4.855894042969" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.232904296875" Y="4.655793457031" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.7595390625" Y="4.638036132812" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.472802734375" Y="4.864920898438" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.830169921875" Y="4.753083984375" />
                  <Point X="-23.5189765625" Y="4.677952148438" />
                  <Point X="-23.307640625" Y="4.601298828125" />
                  <Point X="-23.105349609375" Y="4.527926269531" />
                  <Point X="-22.900302734375" Y="4.432033203125" />
                  <Point X="-22.705423828125" Y="4.340893554688" />
                  <Point X="-22.50730859375" Y="4.225472167969" />
                  <Point X="-22.31902734375" Y="4.115777832031" />
                  <Point X="-22.1321953125" Y="3.982915527344" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.41099609375" Y="3.315662353516" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857919921875" Y="2.539934326172" />
                  <Point X="-22.866921875" Y="2.51605859375" />
                  <Point X="-22.877921875" Y="2.474927001953" />
                  <Point X="-22.888392578125" Y="2.435772216797" />
                  <Point X="-22.891380859375" Y="2.417936035156" />
                  <Point X="-22.892271484375" Y="2.380952880859" />
                  <Point X="-22.887982421875" Y="2.345385742188" />
                  <Point X="-22.883900390625" Y="2.311528076172" />
                  <Point X="-22.878556640625" Y="2.289603027344" />
                  <Point X="-22.8702890625" Y="2.267512451172" />
                  <Point X="-22.859927734375" Y="2.247469970703" />
                  <Point X="-22.837919921875" Y="2.215036132812" />
                  <Point X="-22.81696875" Y="2.184161376953" />
                  <Point X="-22.805533203125" Y="2.170328125" />
                  <Point X="-22.778400390625" Y="2.145592529297" />
                  <Point X="-22.745966796875" Y="2.123584716797" />
                  <Point X="-22.715091796875" Y="2.102635009766" />
                  <Point X="-22.695046875" Y="2.092272216797" />
                  <Point X="-22.672958984375" Y="2.084006347656" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.61546875" Y="2.074374755859" />
                  <Point X="-22.581611328125" Y="2.070291992188" />
                  <Point X="-22.563533203125" Y="2.069845703125" />
                  <Point X="-22.526794921875" Y="2.074170898438" />
                  <Point X="-22.485662109375" Y="2.085170166016" />
                  <Point X="-22.4465078125" Y="2.095640625" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.683646484375" Y="2.530351318359" />
                  <Point X="-21.032673828125" Y="2.906190185547" />
                  <Point X="-20.988416015625" Y="2.844681396484" />
                  <Point X="-20.87671875" Y="2.689449462891" />
                  <Point X="-20.772576171875" Y="2.517350341797" />
                  <Point X="-20.73780078125" Y="2.459883300781" />
                  <Point X="-21.188041015625" Y="2.114400634766" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.7785703125" Y="1.660244873047" />
                  <Point X="-21.79602734375" Y="1.641626708984" />
                  <Point X="-21.82562890625" Y="1.603008056641" />
                  <Point X="-21.85380859375" Y="1.566245361328" />
                  <Point X="-21.86339453125" Y="1.550911499023" />
                  <Point X="-21.878369140625" Y="1.517088256836" />
                  <Point X="-21.889396484375" Y="1.477658325195" />
                  <Point X="-21.89989453125" Y="1.440123657227" />
                  <Point X="-21.90334765625" Y="1.417825195312" />
                  <Point X="-21.9041640625" Y="1.394253417969" />
                  <Point X="-21.902259765625" Y="1.371766357422" />
                  <Point X="-21.89320703125" Y="1.327895629883" />
                  <Point X="-21.88458984375" Y="1.286133544922" />
                  <Point X="-21.8793203125" Y="1.268977905273" />
                  <Point X="-21.863716796875" Y="1.235740722656" />
                  <Point X="-21.839095703125" Y="1.198318725586" />
                  <Point X="-21.81566015625" Y="1.1626953125" />
                  <Point X="-21.801109375" Y="1.145452880859" />
                  <Point X="-21.783865234375" Y="1.129362182617" />
                  <Point X="-21.76565234375" Y="1.116034545898" />
                  <Point X="-21.72997265625" Y="1.095950561523" />
                  <Point X="-21.696009765625" Y="1.07683203125" />
                  <Point X="-21.679478515625" Y="1.069501342773" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.595640625" Y="1.053063232422" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.820416015625" Y="1.138006347656" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.202033203125" Y="1.129850585938" />
                  <Point X="-20.15405859375" Y="0.932785949707" />
                  <Point X="-20.121240234375" Y="0.722002990723" />
                  <Point X="-20.1091328125" Y="0.644238952637" />
                  <Point X="-20.6173828125" Y="0.50805368042" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.2952109375" Y="0.325585296631" />
                  <Point X="-21.318453125" Y="0.315068023682" />
                  <Point X="-21.36584765625" Y="0.287673370361" />
                  <Point X="-21.410962890625" Y="0.261595489502" />
                  <Point X="-21.42568359375" Y="0.251099029541" />
                  <Point X="-21.45246875" Y="0.225576080322" />
                  <Point X="-21.480904296875" Y="0.189341445923" />
                  <Point X="-21.507974609375" Y="0.154848464966" />
                  <Point X="-21.51969921875" Y="0.135567596436" />
                  <Point X="-21.52947265625" Y="0.114103523254" />
                  <Point X="-21.536318359375" Y="0.092603782654" />
                  <Point X="-21.545796875" Y="0.043109046936" />
                  <Point X="-21.5548203125" Y="-0.004006831169" />
                  <Point X="-21.556515625" Y="-0.021875137329" />
                  <Point X="-21.5548203125" Y="-0.058553302765" />
                  <Point X="-21.545341796875" Y="-0.108047889709" />
                  <Point X="-21.536318359375" Y="-0.155163772583" />
                  <Point X="-21.52947265625" Y="-0.176663650513" />
                  <Point X="-21.51969921875" Y="-0.198127731323" />
                  <Point X="-21.507974609375" Y="-0.217408584595" />
                  <Point X="-21.4795390625" Y="-0.253643234253" />
                  <Point X="-21.45246875" Y="-0.288136383057" />
                  <Point X="-21.43999609375" Y="-0.301239532471" />
                  <Point X="-21.410962890625" Y="-0.324155609131" />
                  <Point X="-21.363568359375" Y="-0.351550140381" />
                  <Point X="-21.318453125" Y="-0.377628173828" />
                  <Point X="-21.307291015625" Y="-0.383137695312" />
                  <Point X="-21.283419921875" Y="-0.392149810791" />
                  <Point X="-20.64939453125" Y="-0.562036621094" />
                  <Point X="-20.108525390625" Y="-0.706961975098" />
                  <Point X="-20.11819140625" Y="-0.771075744629" />
                  <Point X="-20.144974609375" Y="-0.948723388672" />
                  <Point X="-20.187025390625" Y="-1.132993408203" />
                  <Point X="-20.19882421875" Y="-1.18469909668" />
                  <Point X="-20.803203125" Y="-1.105131225586" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710266113" />
                  <Point X="-21.62533984375" Y="-1.005508666992" />
                  <Point X="-21.718357421875" Y="-1.02572644043" />
                  <Point X="-21.806904296875" Y="-1.044972412109" />
                  <Point X="-21.836025390625" Y="-1.056597045898" />
                  <Point X="-21.8638515625" Y="-1.073489013672" />
                  <Point X="-21.8876015625" Y="-1.093960327148" />
                  <Point X="-21.94382421875" Y="-1.161579223633" />
                  <Point X="-21.997345703125" Y="-1.225948242188" />
                  <Point X="-22.012064453125" Y="-1.250329956055" />
                  <Point X="-22.023408203125" Y="-1.277716064453" />
                  <Point X="-22.030240234375" Y="-1.305365478516" />
                  <Point X="-22.038298828125" Y="-1.392935180664" />
                  <Point X="-22.04596875" Y="-1.476295898438" />
                  <Point X="-22.04365234375" Y="-1.507562133789" />
                  <Point X="-22.035921875" Y="-1.539182739258" />
                  <Point X="-22.023548828125" Y="-1.567996582031" />
                  <Point X="-21.972072265625" Y="-1.648065795898" />
                  <Point X="-21.923068359375" Y="-1.724286987305" />
                  <Point X="-21.9130625" Y="-1.737242919922" />
                  <Point X="-21.889369140625" Y="-1.760909301758" />
                  <Point X="-21.300990234375" Y="-2.212389160156" />
                  <Point X="-20.786876953125" Y="-2.606882080078" />
                  <Point X="-20.799689453125" Y="-2.627614013672" />
                  <Point X="-20.875197265625" Y="-2.749797607422" />
                  <Point X="-20.962150390625" Y="-2.873346435547" />
                  <Point X="-20.971017578125" Y="-2.8859453125" />
                  <Point X="-21.51110546875" Y="-2.574125488281" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.245775390625" Y="-2.159825195312" />
                  <Point X="-22.35648046875" Y="-2.13983203125" />
                  <Point X="-22.461865234375" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135177001953" />
                  <Point X="-22.647134765625" Y="-2.183579589844" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.757615234375" Y="-2.246549072266" />
                  <Point X="-22.77857421875" Y="-2.267508300781" />
                  <Point X="-22.795466796875" Y="-2.290438964844" />
                  <Point X="-22.843869140625" Y="-2.382407958984" />
                  <Point X="-22.889947265625" Y="-2.46995703125" />
                  <Point X="-22.899771484375" Y="-2.499735107422" />
                  <Point X="-22.904728515625" Y="-2.531908691406" />
                  <Point X="-22.90432421875" Y="-2.563259277344" />
                  <Point X="-22.884330078125" Y="-2.67396484375" />
                  <Point X="-22.865296875" Y="-2.779349609375" />
                  <Point X="-22.86101171875" Y="-2.795140136719" />
                  <Point X="-22.8481796875" Y="-2.826078369141" />
                  <Point X="-22.470087890625" Y="-3.480955078125" />
                  <Point X="-22.13871484375" Y="-4.054907226563" />
                  <Point X="-22.21813671875" Y="-4.111635253906" />
                  <Point X="-22.298232421875" Y="-4.163480957031" />
                  <Point X="-22.716490234375" Y="-3.618397949219" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.387259765625" Y="-2.830360839844" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.7023125" Y="-2.752105224609" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-23.987609375" Y="-2.872128662109" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968860595703" />
                  <Point X="-24.11275" Y="-2.996686767578" />
                  <Point X="-24.124375" Y="-3.025809082031" />
                  <Point X="-24.151943359375" Y="-3.152651123047" />
                  <Point X="-24.178189453125" Y="-3.273396972656" />
                  <Point X="-24.180275390625" Y="-3.289627197266" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.073107421875" Y="-4.13699609375" />
                  <Point X="-23.97793359375" Y="-4.859915527344" />
                  <Point X="-24.024318359375" Y="-4.870083007812" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.05842578125" Y="-4.75263671875" />
                  <Point X="-26.14124609375" Y="-4.731328125" />
                  <Point X="-26.1207734375" Y="-4.575836425781" />
                  <Point X="-26.120076171875" Y="-4.568098632812" />
                  <Point X="-26.11944921875" Y="-4.541030761719" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497688964844" />
                  <Point X="-26.15434375" Y="-4.341796875" />
                  <Point X="-26.18386328125" Y="-4.193397460938" />
                  <Point X="-26.188125" Y="-4.178468261719" />
                  <Point X="-26.19902734375" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135462890625" />
                  <Point X="-26.221740234375" Y="-4.107625976562" />
                  <Point X="-26.23057421875" Y="-4.094861083984" />
                  <Point X="-26.25020703125" Y="-4.070937744141" />
                  <Point X="-26.261005859375" Y="-4.059779296875" />
                  <Point X="-26.3805078125" Y="-3.954978759766" />
                  <Point X="-26.494265625" Y="-3.85521484375" />
                  <Point X="-26.50673828125" Y="-3.845965087891" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822527587891" />
                  <Point X="-26.576529296875" Y="-3.810226318359" />
                  <Point X="-26.5913125" Y="-3.805476318359" />
                  <Point X="-26.621455078125" Y="-3.798447998047" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.795419921875" Y="-3.785774169922" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166015625" />
                  <Point X="-27.008005859375" Y="-3.781945556641" />
                  <Point X="-27.0390546875" Y="-3.790265136719" />
                  <Point X="-27.053673828125" Y="-3.79549609375" />
                  <Point X="-27.081865234375" Y="-3.80826953125" />
                  <Point X="-27.0954375" Y="-3.815812011719" />
                  <Point X="-27.227595703125" Y="-3.904117919922" />
                  <Point X="-27.35340234375" Y="-3.988180175781" />
                  <Point X="-27.35968359375" Y="-3.992759765625" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032770507812" />
                  <Point X="-27.410470703125" Y="-4.041629150391" />
                  <Point X="-27.503201171875" Y="-4.162478027344" />
                  <Point X="-27.543703125" Y="-4.137400878906" />
                  <Point X="-27.74759375" Y="-4.011156494141" />
                  <Point X="-27.947666015625" Y="-3.857107177734" />
                  <Point X="-27.98086328125" Y="-3.831546142578" />
                  <Point X="-27.722873046875" Y="-3.384693115234" />
                  <Point X="-27.341490234375" Y="-2.724119628906" />
                  <Point X="-27.3348515625" Y="-2.710084716797" />
                  <Point X="-27.32394921875" Y="-2.681119140625" />
                  <Point X="-27.319685546875" Y="-2.666188476562" />
                  <Point X="-27.3134140625" Y="-2.634661132812" />
                  <Point X="-27.311638671875" Y="-2.619231933594" />
                  <Point X="-27.310626953125" Y="-2.588295654297" />
                  <Point X="-27.314666015625" Y="-2.557615234375" />
                  <Point X="-27.323650390625" Y="-2.527995605469" />
                  <Point X="-27.329359375" Y="-2.513549316406" />
                  <Point X="-27.343578125" Y="-2.484719970703" />
                  <Point X="-27.35155859375" Y="-2.471407470703" />
                  <Point X="-27.369580078125" Y="-2.446257568359" />
                  <Point X="-27.37962109375" Y="-2.434420166016" />
                  <Point X="-27.396962890625" Y="-2.417076416016" />
                  <Point X="-27.4088046875" Y="-2.407029785156" />
                  <Point X="-27.4339609375" Y="-2.389001464844" />
                  <Point X="-27.447275390625" Y="-2.381019775391" />
                  <Point X="-27.47610546875" Y="-2.366799560547" />
                  <Point X="-27.490544921875" Y="-2.361092285156" />
                  <Point X="-27.520162109375" Y="-2.352105224609" />
                  <Point X="-27.5508515625" Y="-2.348062988281" />
                  <Point X="-27.5817890625" Y="-2.349074707031" />
                  <Point X="-27.59721484375" Y="-2.350848876953" />
                  <Point X="-27.628744140625" Y="-2.357119628906" />
                  <Point X="-27.64367578125" Y="-2.361383056641" />
                  <Point X="-27.67264453125" Y="-2.372285644531" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.3155078125" Y="-2.741977539062" />
                  <Point X="-28.793087890625" Y="-3.017708251953" />
                  <Point X="-28.84294140625" Y="-2.952212158203" />
                  <Point X="-29.0040234375" Y="-2.740583007812" />
                  <Point X="-29.14745703125" Y="-2.500064941406" />
                  <Point X="-29.181265625" Y="-2.443373291016" />
                  <Point X="-28.71541796875" Y="-2.085915527344" />
                  <Point X="-28.048123046875" Y="-1.573881835938" />
                  <Point X="-28.036484375" Y="-1.563311279297" />
                  <Point X="-28.015107421875" Y="-1.540392822266" />
                  <Point X="-28.005369140625" Y="-1.528044921875" />
                  <Point X="-27.987404296875" Y="-1.500914916992" />
                  <Point X="-27.9798359375" Y="-1.487128173828" />
                  <Point X="-27.967080078125" Y="-1.458498535156" />
                  <Point X="-27.961892578125" Y="-1.44365612793" />
                  <Point X="-27.9541875" Y="-1.413908813477" />
                  <Point X="-27.951552734375" Y="-1.39880871582" />
                  <Point X="-27.948748046875" Y="-1.368384033203" />
                  <Point X="-27.948578125" Y="-1.353059570313" />
                  <Point X="-27.95078515625" Y="-1.321387695312" />
                  <Point X="-27.953080078125" Y="-1.306232421875" />
                  <Point X="-27.960080078125" Y="-1.276483276367" />
                  <Point X="-27.971775390625" Y="-1.248245483398" />
                  <Point X="-27.987859375" Y="-1.222259033203" />
                  <Point X="-27.996953125" Y="-1.209916015625" />
                  <Point X="-28.017787109375" Y="-1.185958862305" />
                  <Point X="-28.02874609375" Y="-1.175241943359" />
                  <Point X="-28.052244140625" Y="-1.155709838867" />
                  <Point X="-28.064783203125" Y="-1.14689453125" />
                  <Point X="-28.091265625" Y="-1.131307983398" />
                  <Point X="-28.10543359375" Y="-1.124480224609" />
                  <Point X="-28.13469921875" Y="-1.113255737305" />
                  <Point X="-28.149796875" Y="-1.108859008789" />
                  <Point X="-28.18168359375" Y="-1.102377929688" />
                  <Point X="-28.197298828125" Y="-1.100532104492" />
                  <Point X="-28.22862109375" Y="-1.09944128418" />
                  <Point X="-28.244328125" Y="-1.100196655273" />
                  <Point X="-29.038162109375" Y="-1.204706665039" />
                  <Point X="-29.66091796875" Y="-1.286694335938" />
                  <Point X="-29.67776171875" Y="-1.220755493164" />
                  <Point X="-29.740763671875" Y="-0.974112060547" />
                  <Point X="-29.7787109375" Y="-0.708772583008" />
                  <Point X="-29.786451171875" Y="-0.654654602051" />
                  <Point X="-29.268435546875" Y="-0.515852844238" />
                  <Point X="-28.508287109375" Y="-0.312171417236" />
                  <Point X="-28.496640625" Y="-0.30822668457" />
                  <Point X="-28.473953125" Y="-0.298865661621" />
                  <Point X="-28.462912109375" Y="-0.29344909668" />
                  <Point X="-28.433126953125" Y="-0.276360900879" />
                  <Point X="-28.426345703125" Y="-0.272080566406" />
                  <Point X="-28.406783203125" Y="-0.258132598877" />
                  <Point X="-28.3814296875" Y="-0.237221054077" />
                  <Point X="-28.372830078125" Y="-0.229183242798" />
                  <Point X="-28.356689453125" Y="-0.212103347778" />
                  <Point X="-28.342783203125" Y="-0.19315512085" />
                  <Point X="-28.331330078125" Y="-0.17263609314" />
                  <Point X="-28.3262421875" Y="-0.16202293396" />
                  <Point X="-28.314078125" Y="-0.132014938354" />
                  <Point X="-28.311525390625" Y="-0.124925476074" />
                  <Point X="-28.30500390625" Y="-0.103270683289" />
                  <Point X="-28.298244140625" Y="-0.07391305542" />
                  <Point X="-28.296423828125" Y="-0.063278347015" />
                  <Point X="-28.294001953125" Y="-0.041875465393" />
                  <Point X="-28.2940234375" Y="-0.02033971405" />
                  <Point X="-28.296484375" Y="0.001058710814" />
                  <Point X="-28.298322265625" Y="0.011689258575" />
                  <Point X="-28.30530859375" Y="0.041780784607" />
                  <Point X="-28.307310546875" Y="0.049069843292" />
                  <Point X="-28.314453125" Y="0.070554618835" />
                  <Point X="-28.326390625" Y="0.099828712463" />
                  <Point X="-28.331501953125" Y="0.110433998108" />
                  <Point X="-28.342998046875" Y="0.130928207397" />
                  <Point X="-28.356939453125" Y="0.149840911865" />
                  <Point X="-28.37311328125" Y="0.166886611938" />
                  <Point X="-28.38173046875" Y="0.17490838623" />
                  <Point X="-28.407767578125" Y="0.196294326782" />
                  <Point X="-28.414173828125" Y="0.201118179321" />
                  <Point X="-28.434162109375" Y="0.214475799561" />
                  <Point X="-28.463263671875" Y="0.231089614868" />
                  <Point X="-28.474220703125" Y="0.236444046021" />
                  <Point X="-28.496732421875" Y="0.245704742432" />
                  <Point X="-28.508287109375" Y="0.249611297607" />
                  <Point X="-29.23190234375" Y="0.443503509521" />
                  <Point X="-29.7854453125" Y="0.591825012207" />
                  <Point X="-29.771931640625" Y="0.683157287598" />
                  <Point X="-29.73133203125" Y="0.957520446777" />
                  <Point X="-29.654935546875" Y="1.239447998047" />
                  <Point X="-29.6335859375" Y="1.318237182617" />
                  <Point X="-29.322958984375" Y="1.277342407227" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815551758" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.204703369141" />
                  <Point X="-28.71514453125" Y="1.206589599609" />
                  <Point X="-28.704890625" Y="1.208053344727" />
                  <Point X="-28.6846015625" Y="1.212088867188" />
                  <Point X="-28.67456640625" Y="1.214660766602" />
                  <Point X="-28.64309765625" Y="1.224583007812" />
                  <Point X="-28.613140625" Y="1.234028320312" />
                  <Point X="-28.603447265625" Y="1.237677124023" />
                  <Point X="-28.584513671875" Y="1.246008422852" />
                  <Point X="-28.5752734375" Y="1.250690795898" />
                  <Point X="-28.556529296875" Y="1.261513061523" />
                  <Point X="-28.547853515625" Y="1.267174438477" />
                  <Point X="-28.53117578125" Y="1.279403808594" />
                  <Point X="-28.5159296875" Y="1.293374755859" />
                  <Point X="-28.50229296875" Y="1.308923583984" />
                  <Point X="-28.495900390625" Y="1.317069946289" />
                  <Point X="-28.483484375" Y="1.334800170898" />
                  <Point X="-28.478015625" Y="1.343593505859" />
                  <Point X="-28.4680625" Y="1.361732666016" />
                  <Point X="-28.463578125" Y="1.371078369141" />
                  <Point X="-28.450951171875" Y="1.401563110352" />
                  <Point X="-28.438931640625" Y="1.430582641602" />
                  <Point X="-28.435494140625" Y="1.440359741211" />
                  <Point X="-28.42970703125" Y="1.460219848633" />
                  <Point X="-28.427357421875" Y="1.470302612305" />
                  <Point X="-28.423599609375" Y="1.49161730957" />
                  <Point X="-28.422359375" Y="1.501895996094" />
                  <Point X="-28.421005859375" Y="1.52253894043" />
                  <Point X="-28.42191015625" Y="1.543212890625" />
                  <Point X="-28.42505859375" Y="1.563659179688" />
                  <Point X="-28.427189453125" Y="1.573795898438" />
                  <Point X="-28.43279296875" Y="1.594702026367" />
                  <Point X="-28.436017578125" Y="1.604545776367" />
                  <Point X="-28.443513671875" Y="1.623817504883" />
                  <Point X="-28.44778515625" Y="1.633245483398" />
                  <Point X="-28.463021484375" Y="1.662513549805" />
                  <Point X="-28.477525390625" Y="1.69037512207" />
                  <Point X="-28.482798828125" Y="1.69928503418" />
                  <Point X="-28.4942890625" Y="1.716482421875" />
                  <Point X="-28.500505859375" Y="1.724770019531" />
                  <Point X="-28.51441796875" Y="1.741350097656" />
                  <Point X="-28.521501953125" Y="1.748912597656" />
                  <Point X="-28.5364453125" Y="1.763216796875" />
                  <Point X="-28.5443046875" Y="1.769958496094" />
                  <Point X="-28.95937109375" Y="2.088451660156" />
                  <Point X="-29.22761328125" Y="2.294281005859" />
                  <Point X="-29.160046875" Y="2.410038574219" />
                  <Point X="-29.002283203125" Y="2.680322998047" />
                  <Point X="-28.799931640625" Y="2.940419433594" />
                  <Point X="-28.726337890625" Y="3.035013183594" />
                  <Point X="-28.588814453125" Y="2.955613769531" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.111244140625" Y="2.727323486328" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.038494140625" Y="2.723785644531" />
                  <Point X="-28.02816796875" Y="2.724575683594" />
                  <Point X="-28.006708984375" Y="2.727400634766" />
                  <Point X="-27.99652734375" Y="2.729310302734" />
                  <Point X="-27.976435546875" Y="2.7342265625" />
                  <Point X="-27.957" Y="2.741300292969" />
                  <Point X="-27.938447265625" Y="2.750448974609" />
                  <Point X="-27.929419921875" Y="2.755530517578" />
                  <Point X="-27.911166015625" Y="2.767159423828" />
                  <Point X="-27.902748046875" Y="2.773191162109" />
                  <Point X="-27.886615234375" Y="2.786138427734" />
                  <Point X="-27.878900390625" Y="2.793053955078" />
                  <Point X="-27.847791015625" Y="2.824163330078" />
                  <Point X="-27.81817578125" Y="2.85377734375" />
                  <Point X="-27.811259765625" Y="2.861492675781" />
                  <Point X="-27.7983125" Y="2.877625976562" />
                  <Point X="-27.79228125" Y="2.886043945312" />
                  <Point X="-27.78065234375" Y="2.904298583984" />
                  <Point X="-27.7755703125" Y="2.913325927734" />
                  <Point X="-27.766423828125" Y="2.931874755859" />
                  <Point X="-27.759353515625" Y="2.951299072266" />
                  <Point X="-27.754435546875" Y="2.971387695312" />
                  <Point X="-27.7525234375" Y="2.981573486328" />
                  <Point X="-27.749697265625" Y="3.003032958984" />
                  <Point X="-27.748908203125" Y="3.013365478516" />
                  <Point X="-27.74845703125" Y="3.034048583984" />
                  <Point X="-27.748794921875" Y="3.044399169922" />
                  <Point X="-27.75262890625" Y="3.088227050781" />
                  <Point X="-27.756279296875" Y="3.129948730469" />
                  <Point X="-27.757744140625" Y="3.140205322266" />
                  <Point X="-27.76178125" Y="3.160499267578" />
                  <Point X="-27.764353515625" Y="3.170536621094" />
                  <Point X="-27.77086328125" Y="3.191178466797" />
                  <Point X="-27.774513671875" Y="3.200874023438" />
                  <Point X="-27.78284375" Y="3.219801269531" />
                  <Point X="-27.7875234375" Y="3.229032958984" />
                  <Point X="-27.971453125" Y="3.547607666016" />
                  <Point X="-28.05938671875" Y="3.699915039062" />
                  <Point X="-27.923126953125" Y="3.804385253906" />
                  <Point X="-27.6483671875" Y="4.015041015625" />
                  <Point X="-27.32966796875" Y="4.192103515625" />
                  <Point X="-27.192525390625" Y="4.268296386719" />
                  <Point X="-27.11856640625" Y="4.171911132813" />
                  <Point X="-27.1118203125" Y="4.164048828125" />
                  <Point X="-27.097517578125" Y="4.149107421875" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.065091796875" Y="4.121897460938" />
                  <Point X="-27.047892578125" Y="4.110405273438" />
                  <Point X="-27.03898046875" Y="4.105129394531" />
                  <Point X="-26.990201171875" Y="4.079735839844" />
                  <Point X="-26.943763671875" Y="4.0555625" />
                  <Point X="-26.934326171875" Y="4.051286621094" />
                  <Point X="-26.915044921875" Y="4.0437890625" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031377929688" />
                  <Point X="-26.7808203125" Y="4.035135742188" />
                  <Point X="-26.770732421875" Y="4.037487548828" />
                  <Point X="-26.750869140625" Y="4.043276611328" />
                  <Point X="-26.74109375" Y="4.046714111328" />
                  <Point X="-26.690287109375" Y="4.067760009766" />
                  <Point X="-26.641919921875" Y="4.087793701172" />
                  <Point X="-26.632580078125" Y="4.092274169922" />
                  <Point X="-26.61444921875" Y="4.102221679688" />
                  <Point X="-26.605658203125" Y="4.107688964844" />
                  <Point X="-26.587927734375" Y="4.120103515625" />
                  <Point X="-26.579779296875" Y="4.126497558594" />
                  <Point X="-26.5642265625" Y="4.140136230469" />
                  <Point X="-26.550248046875" Y="4.155391113281" />
                  <Point X="-26.538017578125" Y="4.172072753906" />
                  <Point X="-26.532361328125" Y="4.180744140625" />
                  <Point X="-26.5215390625" Y="4.199488769531" />
                  <Point X="-26.516859375" Y="4.208722167969" />
                  <Point X="-26.50852734375" Y="4.227654785156" />
                  <Point X="-26.504875" Y="4.237354003906" />
                  <Point X="-26.488337890625" Y="4.289802734375" />
                  <Point X="-26.472595703125" Y="4.33973046875" />
                  <Point X="-26.470025390625" Y="4.349764160156" />
                  <Point X="-26.465990234375" Y="4.370050292969" />
                  <Point X="-26.464525390625" Y="4.380302734375" />
                  <Point X="-26.462638671875" Y="4.401863769531" />
                  <Point X="-26.46230078125" Y="4.412216796875" />
                  <Point X="-26.462751953125" Y="4.432898925781" />
                  <Point X="-26.463541015625" Y="4.443228027344" />
                  <Point X="-26.479265625" Y="4.562655273438" />
                  <Point X="-26.286859375" Y="4.616599609375" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.544814453125" Y="4.761538085938" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.32466796875" Y="4.631205566406" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.667775390625" Y="4.613448242188" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.482697265625" Y="4.7704375" />
                  <Point X="-24.172123046875" Y="4.737912109375" />
                  <Point X="-23.85246484375" Y="4.660737304688" />
                  <Point X="-23.546408203125" Y="4.586845214844" />
                  <Point X="-23.340033203125" Y="4.511991699219" />
                  <Point X="-23.14173828125" Y="4.440068847656" />
                  <Point X="-22.940546875" Y="4.345978515625" />
                  <Point X="-22.749546875" Y="4.256652832031" />
                  <Point X="-22.555130859375" Y="4.14338671875" />
                  <Point X="-22.370578125" Y="4.035865478516" />
                  <Point X="-22.187251953125" Y="3.905495605469" />
                  <Point X="-22.18221875" Y="3.901915527344" />
                  <Point X="-22.493267578125" Y="3.363162353516" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.937623046875" Y="2.593106933594" />
                  <Point X="-22.9468125" Y="2.573449462891" />
                  <Point X="-22.955814453125" Y="2.549573730469" />
                  <Point X="-22.958697265625" Y="2.540602294922" />
                  <Point X="-22.969697265625" Y="2.499470703125" />
                  <Point X="-22.98016796875" Y="2.460315917969" />
                  <Point X="-22.9820859375" Y="2.451469726562" />
                  <Point X="-22.986353515625" Y="2.420223144531" />
                  <Point X="-22.987244140625" Y="2.383239990234" />
                  <Point X="-22.986587890625" Y="2.369579101562" />
                  <Point X="-22.982298828125" Y="2.334011962891" />
                  <Point X="-22.978216796875" Y="2.300154296875" />
                  <Point X="-22.97619921875" Y="2.289032470703" />
                  <Point X="-22.97085546875" Y="2.267107421875" />
                  <Point X="-22.967529296875" Y="2.256304199219" />
                  <Point X="-22.95926171875" Y="2.234213623047" />
                  <Point X="-22.9546796875" Y="2.223885498047" />
                  <Point X="-22.944318359375" Y="2.203843017578" />
                  <Point X="-22.9385390625" Y="2.194128662109" />
                  <Point X="-22.91653125" Y="2.161694824219" />
                  <Point X="-22.895580078125" Y="2.130820068359" />
                  <Point X="-22.890189453125" Y="2.123632324219" />
                  <Point X="-22.86953515625" Y="2.100123291016" />
                  <Point X="-22.84240234375" Y="2.075387695312" />
                  <Point X="-22.8317421875" Y="2.066981445312" />
                  <Point X="-22.79930859375" Y="2.044973754883" />
                  <Point X="-22.76843359375" Y="2.024024047852" />
                  <Point X="-22.75871875" Y="2.018245117188" />
                  <Point X="-22.738673828125" Y="2.007882446289" />
                  <Point X="-22.72834375" Y="2.003298339844" />
                  <Point X="-22.706255859375" Y="1.995032470703" />
                  <Point X="-22.695451171875" Y="1.991707519531" />
                  <Point X="-22.67352734375" Y="1.986364868164" />
                  <Point X="-22.662408203125" Y="1.984346801758" />
                  <Point X="-22.626841796875" Y="1.980057983398" />
                  <Point X="-22.592984375" Y="1.975975219727" />
                  <Point X="-22.583955078125" Y="1.975320922852" />
                  <Point X="-22.55242578125" Y="1.975497314453" />
                  <Point X="-22.5156875" Y="1.979822387695" />
                  <Point X="-22.50225390625" Y="1.982395507812" />
                  <Point X="-22.46112109375" Y="1.993394775391" />
                  <Point X="-22.421966796875" Y="2.003865112305" />
                  <Point X="-22.416005859375" Y="2.005670654297" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572875977" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.636146484375" Y="2.448078857422" />
                  <Point X="-21.059595703125" Y="2.780950195312" />
                  <Point X="-20.956037109375" Y="2.637028564453" />
                  <Point X="-20.863115234375" Y="2.483470947266" />
                  <Point X="-21.245873046875" Y="2.189769042969" />
                  <Point X="-21.827046875" Y="1.743820068359" />
                  <Point X="-21.831861328125" Y="1.739868530273" />
                  <Point X="-21.84787109375" Y="1.725224121094" />
                  <Point X="-21.865328125" Y="1.706606079102" />
                  <Point X="-21.87142578125" Y="1.699420288086" />
                  <Point X="-21.90102734375" Y="1.660801635742" />
                  <Point X="-21.92920703125" Y="1.62403894043" />
                  <Point X="-21.93436328125" Y="1.616603881836" />
                  <Point X="-21.95026171875" Y="1.589370361328" />
                  <Point X="-21.965236328125" Y="1.555547119141" />
                  <Point X="-21.969859375" Y="1.542675048828" />
                  <Point X="-21.98088671875" Y="1.503245117188" />
                  <Point X="-21.991384765625" Y="1.465710571289" />
                  <Point X="-21.993775390625" Y="1.454661987305" />
                  <Point X="-21.997228515625" Y="1.432363525391" />
                  <Point X="-21.998291015625" Y="1.421113525391" />
                  <Point X="-21.999107421875" Y="1.397541748047" />
                  <Point X="-21.998826171875" Y="1.386237060547" />
                  <Point X="-21.996921875" Y="1.36375" />
                  <Point X="-21.995298828125" Y="1.352567626953" />
                  <Point X="-21.98624609375" Y="1.308696777344" />
                  <Point X="-21.97762890625" Y="1.266934814453" />
                  <Point X="-21.97540234375" Y="1.258239501953" />
                  <Point X="-21.96531640625" Y="1.228606689453" />
                  <Point X="-21.949712890625" Y="1.195369506836" />
                  <Point X="-21.943080078125" Y="1.183525146484" />
                  <Point X="-21.918458984375" Y="1.146103149414" />
                  <Point X="-21.8950234375" Y="1.110479736328" />
                  <Point X="-21.888263671875" Y="1.101426513672" />
                  <Point X="-21.873712890625" Y="1.084184204102" />
                  <Point X="-21.865921875" Y="1.075994873047" />
                  <Point X="-21.848677734375" Y="1.059904174805" />
                  <Point X="-21.839966796875" Y="1.052696411133" />
                  <Point X="-21.82175390625" Y="1.039368896484" />
                  <Point X="-21.812251953125" Y="1.033248901367" />
                  <Point X="-21.776572265625" Y="1.013164855957" />
                  <Point X="-21.742609375" Y="0.994046325684" />
                  <Point X="-21.73451953125" Y="0.989987731934" />
                  <Point X="-21.7053203125" Y="0.978083618164" />
                  <Point X="-21.66972265625" Y="0.968020935059" />
                  <Point X="-21.656328125" Y="0.965257629395" />
                  <Point X="-21.608087890625" Y="0.958882141113" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.555966796875" Y="0.952199768066" />
                  <Point X="-21.53428125" Y="0.951222839355" />
                  <Point X="-21.50878125" Y="0.952032409668" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-20.808015625" Y="1.043819091797" />
                  <Point X="-20.295296875" Y="1.111319702148" />
                  <Point X="-20.294337890625" Y="1.107380371094" />
                  <Point X="-20.247310546875" Y="0.914207641602" />
                  <Point X="-20.216126953125" Y="0.713921325684" />
                  <Point X="-20.641970703125" Y="0.599816711426" />
                  <Point X="-21.3080078125" Y="0.421352752686" />
                  <Point X="-21.313970703125" Y="0.419543548584" />
                  <Point X="-21.334376953125" Y="0.412136352539" />
                  <Point X="-21.357619140625" Y="0.40161920166" />
                  <Point X="-21.365994140625" Y="0.397316864014" />
                  <Point X="-21.413388671875" Y="0.369922241211" />
                  <Point X="-21.45850390625" Y="0.343844299316" />
                  <Point X="-21.4661171875" Y="0.338945678711" />
                  <Point X="-21.49121875" Y="0.31987512207" />
                  <Point X="-21.51800390625" Y="0.294352111816" />
                  <Point X="-21.527203125" Y="0.284225128174" />
                  <Point X="-21.555638671875" Y="0.24799041748" />
                  <Point X="-21.582708984375" Y="0.213497421265" />
                  <Point X="-21.58914453125" Y="0.204207901001" />
                  <Point X="-21.600869140625" Y="0.184927047729" />
                  <Point X="-21.606158203125" Y="0.17493572998" />
                  <Point X="-21.615931640625" Y="0.153471618652" />
                  <Point X="-21.619994140625" Y="0.142926528931" />
                  <Point X="-21.62683984375" Y="0.121426742554" />
                  <Point X="-21.629623046875" Y="0.110472045898" />
                  <Point X="-21.6391015625" Y="0.060977359772" />
                  <Point X="-21.648125" Y="0.013861531258" />
                  <Point X="-21.649396484375" Y="0.004966303349" />
                  <Point X="-21.6514140625" Y="-0.026261442184" />
                  <Point X="-21.64971875" Y="-0.06293963623" />
                  <Point X="-21.648125" Y="-0.076421661377" />
                  <Point X="-21.638646484375" Y="-0.12591619873" />
                  <Point X="-21.629623046875" Y="-0.173032180786" />
                  <Point X="-21.62683984375" Y="-0.183986572266" />
                  <Point X="-21.619994140625" Y="-0.20548651123" />
                  <Point X="-21.615931640625" Y="-0.21603175354" />
                  <Point X="-21.606158203125" Y="-0.237495864868" />
                  <Point X="-21.600869140625" Y="-0.247487182617" />
                  <Point X="-21.58914453125" Y="-0.266768035889" />
                  <Point X="-21.582708984375" Y="-0.276057556152" />
                  <Point X="-21.5542734375" Y="-0.312292266846" />
                  <Point X="-21.527203125" Y="-0.346785400391" />
                  <Point X="-21.521279296875" Y="-0.353635559082" />
                  <Point X="-21.49885546875" Y="-0.375809509277" />
                  <Point X="-21.469822265625" Y="-0.398725524902" />
                  <Point X="-21.45850390625" Y="-0.406404571533" />
                  <Point X="-21.411109375" Y="-0.433799041748" />
                  <Point X="-21.365994140625" Y="-0.45987713623" />
                  <Point X="-21.360501953125" Y="-0.462816009521" />
                  <Point X="-21.340845703125" Y="-0.47201473999" />
                  <Point X="-21.316974609375" Y="-0.481026916504" />
                  <Point X="-21.3080078125" Y="-0.483912719727" />
                  <Point X="-20.673982421875" Y="-0.653799499512" />
                  <Point X="-20.215119140625" Y="-0.776751403809" />
                  <Point X="-20.238380859375" Y="-0.931034729004" />
                  <Point X="-20.272197265625" Y="-1.079219604492" />
                  <Point X="-20.790802734375" Y="-1.010944030762" />
                  <Point X="-21.563216796875" Y="-0.909253540039" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042419434" />
                  <Point X="-21.63327734375" Y="-0.910840820312" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.73853515625" Y="-0.932893981934" />
                  <Point X="-21.82708203125" Y="-0.952140014648" />
                  <Point X="-21.842125" Y="-0.95674230957" />
                  <Point X="-21.87124609375" Y="-0.968366943359" />
                  <Point X="-21.885322265625" Y="-0.975388977051" />
                  <Point X="-21.9131484375" Y="-0.992281005859" />
                  <Point X="-21.925875" Y="-1.001530822754" />
                  <Point X="-21.949625" Y="-1.02200213623" />
                  <Point X="-21.960650390625" Y="-1.033223510742" />
                  <Point X="-22.016873046875" Y="-1.100842285156" />
                  <Point X="-22.07039453125" Y="-1.165211303711" />
                  <Point X="-22.07867578125" Y="-1.176851318359" />
                  <Point X="-22.09339453125" Y="-1.201233032227" />
                  <Point X="-22.09983203125" Y="-1.213974853516" />
                  <Point X="-22.11117578125" Y="-1.241361083984" />
                  <Point X="-22.115634765625" Y="-1.254927368164" />
                  <Point X="-22.122466796875" Y="-1.282576782227" />
                  <Point X="-22.12483984375" Y="-1.296659912109" />
                  <Point X="-22.1328984375" Y="-1.384229614258" />
                  <Point X="-22.140568359375" Y="-1.467590332031" />
                  <Point X="-22.140708984375" Y="-1.483314697266" />
                  <Point X="-22.138392578125" Y="-1.514580932617" />
                  <Point X="-22.135935546875" Y="-1.530122924805" />
                  <Point X="-22.128205078125" Y="-1.561743530273" />
                  <Point X="-22.12321484375" Y="-1.576667114258" />
                  <Point X="-22.110841796875" Y="-1.605480957031" />
                  <Point X="-22.103458984375" Y="-1.61937097168" />
                  <Point X="-22.051982421875" Y="-1.699440185547" />
                  <Point X="-22.002978515625" Y="-1.775661499023" />
                  <Point X="-21.998255859375" Y="-1.782354370117" />
                  <Point X="-21.98019921875" Y="-1.804456176758" />
                  <Point X="-21.956505859375" Y="-1.828122680664" />
                  <Point X="-21.947201171875" Y="-1.836277832031" />
                  <Point X="-21.358822265625" Y="-2.287757568359" />
                  <Point X="-20.912828125" Y="-2.629980712891" />
                  <Point X="-20.954521484375" Y="-2.697446777344" />
                  <Point X="-20.998724609375" Y="-2.760252441406" />
                  <Point X="-21.46360546875" Y="-2.491853027344" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.18497265625" Y="-2.079513427734" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228892578125" Y="-2.066337646484" />
                  <Point X="-22.33959765625" Y="-2.046344360352" />
                  <Point X="-22.444982421875" Y="-2.027311889648" />
                  <Point X="-22.460640625" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.55515625" Y="-2.03513659668" />
                  <Point X="-22.584931640625" Y="-2.044960571289" />
                  <Point X="-22.59941015625" Y="-2.051109130859" />
                  <Point X="-22.69137890625" Y="-2.09951171875" />
                  <Point X="-22.778927734375" Y="-2.145588134766" />
                  <Point X="-22.791029296875" Y="-2.153169433594" />
                  <Point X="-22.8139609375" Y="-2.1700625" />
                  <Point X="-22.824791015625" Y="-2.179374267578" />
                  <Point X="-22.84575" Y="-2.200333496094" />
                  <Point X="-22.855060546875" Y="-2.211162353516" />
                  <Point X="-22.871953125" Y="-2.234093017578" />
                  <Point X="-22.87953515625" Y="-2.246194824219" />
                  <Point X="-22.9279375" Y="-2.338163818359" />
                  <Point X="-22.974015625" Y="-2.425712890625" />
                  <Point X="-22.9801640625" Y="-2.440193115234" />
                  <Point X="-22.98998828125" Y="-2.469971191406" />
                  <Point X="-22.9936640625" Y="-2.485269042969" />
                  <Point X="-22.99862109375" Y="-2.517442626953" />
                  <Point X="-22.999720703125" Y="-2.533133789062" />
                  <Point X="-22.99931640625" Y="-2.564484375" />
                  <Point X="-22.9978125" Y="-2.580143798828" />
                  <Point X="-22.977818359375" Y="-2.690849365234" />
                  <Point X="-22.95878515625" Y="-2.796234130859" />
                  <Point X="-22.95698046875" Y="-2.80423046875" />
                  <Point X="-22.948763671875" Y="-2.831536132812" />
                  <Point X="-22.935931640625" Y="-2.862474365234" />
                  <Point X="-22.930453125" Y="-2.873578125" />
                  <Point X="-22.552361328125" Y="-3.528454833984" />
                  <Point X="-22.26410546875" Y="-4.027724853516" />
                  <Point X="-22.276244140625" Y="-4.036081787109" />
                  <Point X="-22.64112109375" Y="-3.560565429688" />
                  <Point X="-23.16608203125" Y="-2.876422607422" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849625732422" />
                  <Point X="-23.21674609375" Y="-2.828004638672" />
                  <Point X="-23.22669921875" Y="-2.820646972656" />
                  <Point X="-23.335884765625" Y="-2.750450683594" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.453716796875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663873535156" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.711017578125" Y="-2.657504882812" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920125" Y="-2.699414794922" />
                  <Point X="-23.944505859375" Y="-2.714135742188" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-24.048345703125" Y="-2.799080810547" />
                  <Point X="-24.136123046875" Y="-2.872063720703" />
                  <Point X="-24.14734375" Y="-2.883086914062" />
                  <Point X="-24.167814453125" Y="-2.9068359375" />
                  <Point X="-24.177064453125" Y="-2.919561767578" />
                  <Point X="-24.19395703125" Y="-2.947387939453" />
                  <Point X="-24.20098046875" Y="-2.961467041016" />
                  <Point X="-24.21260546875" Y="-2.990589355469" />
                  <Point X="-24.21720703125" Y="-3.005632568359" />
                  <Point X="-24.244775390625" Y="-3.132474609375" />
                  <Point X="-24.271021484375" Y="-3.253220458984" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677001953" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520263672" />
                  <Point X="-24.167294921875" Y="-4.149396484375" />
                  <Point X="-24.16691015625" Y="-4.152323730469" />
                  <Point X="-24.344931640625" Y="-3.487937255859" />
                  <Point X="-24.347392578125" Y="-3.480123291016" />
                  <Point X="-24.357853515625" Y="-3.453577636719" />
                  <Point X="-24.3732109375" Y="-3.423814697266" />
                  <Point X="-24.37958984375" Y="-3.413208984375" />
                  <Point X="-24.463470703125" Y="-3.292354492188" />
                  <Point X="-24.543318359375" Y="-3.17730859375" />
                  <Point X="-24.553330078125" Y="-3.165170410156" />
                  <Point X="-24.57521484375" Y="-3.142715576172" />
                  <Point X="-24.587087890625" Y="-3.132398925781" />
                  <Point X="-24.61334375" Y="-3.113154785156" />
                  <Point X="-24.626755859375" Y="-3.104938232422" />
                  <Point X="-24.654755859375" Y="-3.090830078125" />
                  <Point X="-24.66934375" Y="-3.084938476562" />
                  <Point X="-24.799142578125" Y="-3.044653564453" />
                  <Point X="-24.922703125" Y="-3.006305175781" />
                  <Point X="-24.936623046875" Y="-3.003109130859" />
                  <Point X="-24.964783203125" Y="-2.99883984375" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.194783203125" Y="-3.04658984375" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.332931640625" Y="-3.090830078125" />
                  <Point X="-25.360931640625" Y="-3.104938232422" />
                  <Point X="-25.37434375" Y="-3.113154785156" />
                  <Point X="-25.400599609375" Y="-3.132398925781" />
                  <Point X="-25.412474609375" Y="-3.142717041016" />
                  <Point X="-25.434359375" Y="-3.165172851562" />
                  <Point X="-25.444369140625" Y="-3.177310546875" />
                  <Point X="-25.528248046875" Y="-3.298164794922" />
                  <Point X="-25.608095703125" Y="-3.4132109375" />
                  <Point X="-25.61246875" Y="-3.420131591797" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213623047" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.83811328125" Y="-4.217026367187" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.150807660743" Y="4.213928766327" />
                  <Point X="-28.036590451489" Y="3.660430247941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.359898074386" Y="4.596121978653" />
                  <Point X="-26.474262773161" Y="4.524658983448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.088526540275" Y="4.14082438139" />
                  <Point X="-27.989061938667" Y="3.578107410626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.034730187577" Y="4.687287477025" />
                  <Point X="-26.462470689528" Y="4.420005546786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.999374076039" Y="4.084511075619" />
                  <Point X="-27.941533089394" Y="3.495784783548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.701692786229" Y="3.020784286439" />
                  <Point X="-28.771197423399" Y="2.977352968857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.780838096293" Y="4.733914915242" />
                  <Point X="-26.487545795732" Y="4.292314933104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.894860192973" Y="4.037796649676" />
                  <Point X="-27.894004042108" Y="3.413462280203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.608513639075" Y="2.966987131413" />
                  <Point X="-28.940800054491" Y="2.759351534365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.560250256728" Y="4.75973154727" />
                  <Point X="-27.846474994823" Y="3.331139776858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.515334453939" Y="2.913190000122" />
                  <Point X="-29.067898196165" Y="2.567909852629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.362243407096" Y="4.771438010754" />
                  <Point X="-27.798945947537" Y="3.248817273513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.42215525862" Y="2.859392875193" />
                  <Point X="-29.170825867834" Y="2.39157155682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.336531980459" Y="4.675482344934" />
                  <Point X="-27.761696167595" Y="3.160071571045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.328976063301" Y="2.805595750264" />
                  <Point X="-29.191543564054" Y="2.26660375509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.310820798894" Y="4.579526525976" />
                  <Point X="-27.749766368954" Y="3.055504188271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.234007918687" Y="2.752916484922" />
                  <Point X="-29.111079349108" Y="2.204861428616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.285109827299" Y="4.483570575814" />
                  <Point X="-27.765859030104" Y="2.933426429208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.097602673638" Y="2.726129993673" />
                  <Point X="-29.030615134162" Y="2.143119102142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.259398855704" Y="4.387614625652" />
                  <Point X="-28.950150946353" Y="2.081376758711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.46966566497" Y="4.769072744408" />
                  <Point X="-24.65753082693" Y="4.651681562408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.233687884109" Y="4.29165867549" />
                  <Point X="-28.869686968229" Y="2.019634284254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.31612596433" Y="4.75299304932" />
                  <Point X="-24.693583675192" Y="4.51713129416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.197889467859" Y="4.202006060333" />
                  <Point X="-28.789222990105" Y="1.957891809797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.164091174782" Y="4.735972981413" />
                  <Point X="-24.72963666436" Y="4.382580937865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.133590834958" Y="4.130162357082" />
                  <Point X="-28.70875901198" Y="1.89614933534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.034780146861" Y="4.704753531306" />
                  <Point X="-24.76728457513" Y="4.247033963942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.022839607484" Y="4.087345456498" />
                  <Point X="-28.628295033856" Y="1.834406860883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.485532294707" Y="1.298745569263" />
                  <Point X="-29.670123302023" Y="1.183400306153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.905469118939" Y="4.673534081198" />
                  <Point X="-28.547831055732" Y="1.772664386426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.337457302699" Y="1.279251145233" />
                  <Point X="-29.706666676659" Y="1.048543523008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.776158407217" Y="4.642314433508" />
                  <Point X="-28.483738723686" Y="1.700691772095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.189382322501" Y="1.259756713824" />
                  <Point X="-29.73726909905" Y="0.917399058843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.646847915134" Y="4.611094648571" />
                  <Point X="-28.440380695388" Y="1.615762926818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.041307343585" Y="1.240262281614" />
                  <Point X="-29.755534811012" Y="0.793963426927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.521082734204" Y="4.577659507352" />
                  <Point X="-28.421463332337" Y="1.515561858888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.89323236467" Y="1.220767849403" />
                  <Point X="-29.773800300281" Y="0.670527934166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.407651478103" Y="4.536517274519" />
                  <Point X="-28.460143827384" Y="1.379369654697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.7398321988" Y="1.204600963314" />
                  <Point X="-29.739976242979" Y="0.579641602611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.294220229323" Y="4.495375037111" />
                  <Point X="-29.61450626499" Y="0.546021998122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.180788991348" Y="4.454232792952" />
                  <Point X="-29.489036287001" Y="0.512402393633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.074503452573" Y="4.408625420364" />
                  <Point X="-29.363566309012" Y="0.478782789143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.971969495507" Y="4.360673799345" />
                  <Point X="-29.238096331023" Y="0.445163184654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.869436057549" Y="4.312721853951" />
                  <Point X="-29.112626343586" Y="0.411543586069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.766902848974" Y="4.264769765223" />
                  <Point X="-28.987156355658" Y="0.37792398779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.67247654656" Y="4.211751919297" />
                  <Point X="-28.861686367731" Y="0.344304389511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.579702132152" Y="4.157701859182" />
                  <Point X="-28.736216379803" Y="0.310684791232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.486928158856" Y="4.10365152343" />
                  <Point X="-28.610746391875" Y="0.277065192953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.394154344476" Y="4.049601088376" />
                  <Point X="-28.487644133747" Y="0.241966072889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.308037753981" Y="3.991390758149" />
                  <Point X="-28.396401644484" Y="0.186958759702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.71084331198" Y="-0.634395553189" />
                  <Point X="-29.782908143924" Y="-0.679426658022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.224189337279" Y="3.931763115632" />
                  <Point X="-28.333602253531" Y="0.114178226107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.396986337667" Y="-0.550297897378" />
                  <Point X="-29.768201378232" Y="-0.782258799194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.232750752796" Y="3.814391401147" />
                  <Point X="-28.300859212408" Y="0.022616400674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.08312907014" Y="-0.466200058345" />
                  <Point X="-29.753494913348" Y="-0.885091128333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.333928105038" Y="3.639146826304" />
                  <Point X="-28.301971398811" Y="-0.090100518842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.769271599204" Y="-0.382102092209" />
                  <Point X="-29.737449449146" Y="-0.987086757835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.435105457281" Y="3.463902251461" />
                  <Point X="-29.7127735062" Y="-1.083689465678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.536282669949" Y="3.288657763834" />
                  <Point X="-29.688097563254" Y="-1.180292173521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.637459693896" Y="3.113413394133" />
                  <Point X="-29.663421249892" Y="-1.276894649903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.738636717842" Y="2.938169024432" />
                  <Point X="-29.456833014592" Y="-1.259825941518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.839813741789" Y="2.762924654731" />
                  <Point X="-29.229707645362" Y="-1.229924207564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.939817025639" Y="2.588413719243" />
                  <Point X="-29.002582331365" Y="-1.200022508124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.982308500844" Y="2.44984015035" />
                  <Point X="-28.775457314718" Y="-1.170120994489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.982726339246" Y="2.33755710762" />
                  <Point X="-28.548332298071" Y="-1.140219480854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.961076757201" Y="2.239063319602" />
                  <Point X="-28.321207281424" Y="-1.110317967219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.913282139531" Y="2.156906763051" />
                  <Point X="-28.142841437518" Y="-1.110884566254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.851375704422" Y="2.083568248717" />
                  <Point X="-28.04497454308" Y="-1.161752491672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.767900879853" Y="2.023707159927" />
                  <Point X="-27.980795954527" Y="-1.233671206956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.653363466055" Y="1.983256131137" />
                  <Point X="-27.950414370522" Y="-1.326708634569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.455430268971" Y="1.994916571401" />
                  <Point X="-27.962897281178" Y="-1.446530771179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.046768863901" Y="2.763123962327" />
                  <Point X="-28.314347717736" Y="-1.778163326019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.991164627347" Y="2.685847397266" />
                  <Point X="-29.100697556647" Y="-2.381551188553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.937923146574" Y="2.607094418532" />
                  <Point X="-29.13758325642" Y="-2.516621880184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.888734939563" Y="2.52580867325" />
                  <Point X="-29.088914656568" Y="-2.598232312056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.239885296521" Y="2.194363628955" />
                  <Point X="-29.040246056717" Y="-2.679842743928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.888651187738" Y="1.67694775865" />
                  <Point X="-28.989246643021" Y="-2.759996721663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.979515713616" Y="1.508147352934" />
                  <Point X="-28.931463659201" Y="-2.835911854331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.998650985754" Y="1.384168359515" />
                  <Point X="-28.873680675381" Y="-2.911826987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.980982386867" Y="1.283186977131" />
                  <Point X="-28.815897320975" Y="-2.9877418881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.947777627797" Y="1.191913665092" />
                  <Point X="-27.626799064397" Y="-2.356732779475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.896131379956" Y="1.112163874189" />
                  <Point X="-27.4691371507" Y="-2.370236629962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.82709745757" Y="1.043279108211" />
                  <Point X="-27.384604181772" Y="-2.429436516772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.73366585964" Y="0.989639701938" />
                  <Point X="-27.331852049104" Y="-2.50849527414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.604396416968" Y="0.958394266483" />
                  <Point X="-27.31125989837" Y="-2.607649818576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.416647006953" Y="0.963691170321" />
                  <Point X="-27.354534329287" Y="-2.746712632497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.189521803979" Y="0.993592800386" />
                  <Point X="-27.45571213746" Y="-2.921957492237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.962396601006" Y="1.023494430451" />
                  <Point X="-27.556889945633" Y="-3.097202351977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.735271444341" Y="1.053396031579" />
                  <Point X="-27.658067753806" Y="-3.272447211717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.508146385952" Y="1.083297571297" />
                  <Point X="-27.759245376415" Y="-3.447691955503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.293809131556" Y="1.105208404223" />
                  <Point X="-27.860422668402" Y="-3.622936492695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.270138543725" Y="1.00797748078" />
                  <Point X="-21.1345574501" Y="0.467828598976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.607301198974" Y="0.172425518997" />
                  <Point X="-27.961599960389" Y="-3.798181029886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.246744744571" Y="0.910573600577" />
                  <Point X="-20.820700481962" Y="0.551926250929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.643549360908" Y="0.037753205222" />
                  <Point X="-27.915842309746" Y="-3.881610424703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.23084987571" Y="0.808483868661" />
                  <Point X="-20.506843318918" Y="0.636024024673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.648004202483" Y="-0.077052437065" />
                  <Point X="-27.835530495335" Y="-3.943447981601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.628624007062" Y="-0.176964295232" />
                  <Point X="-27.755218680925" Y="-4.005285538499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.590224713013" Y="-0.264991701564" />
                  <Point X="-27.666096459182" Y="-4.061617741878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.532022319302" Y="-0.340644757846" />
                  <Point X="-27.080107257159" Y="-3.807472997303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.124074548774" Y="-3.834946810319" />
                  <Point X="-27.576050016396" Y="-4.117372427851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.458235606892" Y="-0.406559651003" />
                  <Point X="-26.859398504161" Y="-3.781580810176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.365075498063" Y="-0.460368702494" />
                  <Point X="-26.697144862323" Y="-3.792215430475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.248849021331" Y="-0.499764287623" />
                  <Point X="-26.558588540375" Y="-3.817657779696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.123379056858" Y="-0.533383900559" />
                  <Point X="-21.775632886536" Y="-0.94095732839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.001052781127" Y="-1.08181531183" />
                  <Point X="-25.096566865065" Y="-3.016107191287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.513157315936" Y="-3.276421796334" />
                  <Point X="-26.471446194073" Y="-3.875227146558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.997909092386" Y="-0.567003513494" />
                  <Point X="-21.548685256658" Y="-0.91116665831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.118705536715" Y="-1.267354861284" />
                  <Point X="-24.908608373209" Y="-3.010679638614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.633604524518" Y="-3.463707513819" />
                  <Point X="-26.396857113897" Y="-3.940640664688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.872439127913" Y="-0.600623126429" />
                  <Point X="-21.400610352509" Y="-0.93066113724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.133290475774" Y="-1.388490491021" />
                  <Point X="-24.788828361083" Y="-3.047854728385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.672848802165" Y="-3.600252008478" />
                  <Point X="-26.322267740005" Y="-4.006053999284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.746969163441" Y="-0.634242739365" />
                  <Point X="-21.252535448361" Y="-0.95015561617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.139162998821" Y="-1.504181999011" />
                  <Point X="-24.669075529647" Y="-3.085046802536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.708901470539" Y="-3.734802164319" />
                  <Point X="-26.249062588381" Y="-4.072332291952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.621499163394" Y="-0.667862330071" />
                  <Point X="-21.104460544212" Y="-0.9696500951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.113202981242" Y="-1.59998232797" />
                  <Point X="-23.821925777776" Y="-2.667710834433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.097298264236" Y="-2.839782661581" />
                  <Point X="-24.578091762143" Y="-3.14021578302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.744954138912" Y="-3.869352320159" />
                  <Point X="-26.197934126219" Y="-4.152405631256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.496029113875" Y="-0.701481889863" />
                  <Point X="-20.956385640063" Y="-0.98914457403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.063765448842" Y="-1.681112277459" />
                  <Point X="-23.611693162188" Y="-2.6483648645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.222513534778" Y="-3.030047794853" />
                  <Point X="-24.517564066297" Y="-3.214415829264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.781006807285" Y="-4.003902476" />
                  <Point X="-26.172851718456" Y="-4.248754351692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.370559064356" Y="-0.735101449656" />
                  <Point X="-20.808310735914" Y="-1.00863905296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.012386088304" Y="-1.761028838057" />
                  <Point X="-22.439975229671" Y="-2.028216187707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.923989204489" Y="-2.330661686466" />
                  <Point X="-23.467539769016" Y="-2.670309775452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.250687843436" Y="-3.159675005164" />
                  <Point X="-24.463334184511" Y="-3.292551186498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.817059475659" Y="-4.13845263184" />
                  <Point X="-26.153031952334" Y="-4.3483915356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.245089014837" Y="-0.768721009448" />
                  <Point X="-20.660235732096" Y="-1.028133469609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.949838242857" Y="-1.833966554729" />
                  <Point X="-22.300898574565" Y="-2.053333396684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.993826410957" Y="-2.48632276473" />
                  <Point X="-23.375628745355" Y="-2.724899341983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.274994006609" Y="-3.286885129913" />
                  <Point X="-24.409103517313" Y="-3.370686052951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.853112284323" Y="-4.273002875344" />
                  <Point X="-26.133212294848" Y="-4.448028787391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.229311809578" Y="-0.870884265742" />
                  <Point X="-20.512160714912" Y="-1.047627877907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.869580833576" Y="-1.895838107725" />
                  <Point X="-22.172321786151" Y="-2.085011650557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.994454422738" Y="-2.598737138364" />
                  <Point X="-23.287267554158" Y="-2.781707090025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.266776210852" Y="-3.393772029523" />
                  <Point X="-24.358982785821" Y="-3.451389092266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.889165289912" Y="-4.4075532419" />
                  <Point X="-26.119694110449" Y="-4.551603636586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.253695160438" Y="-0.998142622711" />
                  <Point X="-20.364085697729" Y="-1.067122286205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.789116716327" Y="-1.957580495247" />
                  <Point X="-22.077199036934" Y="-2.137594308221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.976274262361" Y="-2.699398861652" />
                  <Point X="-23.202014624019" Y="-2.840457095141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.253149087624" Y="-3.497278806183" />
                  <Point X="-24.329624142484" Y="-3.54506572415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.925218295501" Y="-4.542103608457" />
                  <Point X="-26.13346508413" Y="-4.672230644304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.708652599078" Y="-2.019322882769" />
                  <Point X="-21.984019804389" Y="-2.191391409889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.957942891372" Y="-2.799966098061" />
                  <Point X="-23.138280887589" Y="-2.912653784882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.239521964397" Y="-3.600785582843" />
                  <Point X="-24.303912914444" Y="-3.641021514068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.96127130109" Y="-4.676653975013" />
                  <Point X="-26.075740042847" Y="-4.748181983489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.628188481829" Y="-2.081065270291" />
                  <Point X="-21.890840571844" Y="-2.245188511557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.921444610618" Y="-2.88918138934" />
                  <Point X="-23.08018109527" Y="-2.988370953629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.225894841169" Y="-3.704292359503" />
                  <Point X="-24.278201686403" Y="-3.736977303985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.54772436458" Y="-2.142807657813" />
                  <Point X="-21.797661339299" Y="-2.298985613224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.873915790968" Y="-2.971504034928" />
                  <Point X="-23.022081302951" Y="-3.064088122376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.212267717942" Y="-3.807799136163" />
                  <Point X="-24.252490458363" Y="-3.832933093902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.467260247331" Y="-2.204550045335" />
                  <Point X="-21.704482106754" Y="-2.352782714892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.826386971319" Y="-3.053826680516" />
                  <Point X="-22.963981510632" Y="-3.139805291123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.198640594714" Y="-3.911305912823" />
                  <Point X="-24.226779230323" Y="-3.928888883819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.386796130082" Y="-2.266292432857" />
                  <Point X="-21.611302874209" Y="-2.406579816559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.77885815167" Y="-3.136149326104" />
                  <Point X="-22.905881718313" Y="-3.21552245987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.185013471487" Y="-4.014812689482" />
                  <Point X="-24.201068002283" Y="-4.024844673736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.306331949968" Y="-2.328034781097" />
                  <Point X="-21.518123641664" Y="-2.460376918227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.73132933202" Y="-3.218471971692" />
                  <Point X="-22.847781925994" Y="-3.291239628617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.171386348259" Y="-4.118319466142" />
                  <Point X="-24.175356774243" Y="-4.120800463654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.225867736351" Y="-2.389777108401" />
                  <Point X="-21.424944415476" Y="-2.514174023867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.683800512371" Y="-3.300794617281" />
                  <Point X="-22.789682133675" Y="-3.366956797364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.145403522733" Y="-2.451519435706" />
                  <Point X="-21.331765198252" Y="-2.567971135108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.636271692721" Y="-3.383117262869" />
                  <Point X="-22.731582341356" Y="-3.442673966111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.064939309116" Y="-2.51326176301" />
                  <Point X="-21.238585981028" Y="-2.621768246349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.588742873072" Y="-3.465439908457" />
                  <Point X="-22.673482549036" Y="-3.518391134858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.984475095499" Y="-2.575004090315" />
                  <Point X="-21.145406763804" Y="-2.675565357591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.541213960133" Y="-3.547762495751" />
                  <Point X="-22.615382726353" Y="-3.594108284631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.925186508393" Y="-2.649978417634" />
                  <Point X="-21.05222754658" Y="-2.729362468832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.493684742723" Y="-3.630084892791" />
                  <Point X="-22.557282865491" Y="-3.669825410547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.446155525313" Y="-3.71240728983" />
                  <Point X="-22.499183004629" Y="-3.745542536464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.398626307904" Y="-3.79472968687" />
                  <Point X="-22.441083143767" Y="-3.82125966238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.351097090494" Y="-3.87705208391" />
                  <Point X="-22.382983282905" Y="-3.896976788297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.303567873084" Y="-3.95937448095" />
                  <Point X="-22.324883422043" Y="-3.972693914213" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.323236328125" Y="-4.303012207031" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544433594" />
                  <Point X="-24.619560546875" Y="-3.400689941406" />
                  <Point X="-24.699408203125" Y="-3.285644042969" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.855462890625" Y="-3.226114990234" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.138462890625" Y="-3.228051269531" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285644042969" />
                  <Point X="-25.372158203125" Y="-3.406498291016" />
                  <Point X="-25.452005859375" Y="-3.521544433594" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.6545859375" Y="-4.266202148438" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.955541015625" Y="-4.966152832031" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.250275390625" Y="-4.899463867188" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.333853515625" Y="-4.73868359375" />
                  <Point X="-26.3091484375" Y="-4.5510390625" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.34069140625" Y="-4.378865234375" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.50578515625" Y="-4.097828125" />
                  <Point X="-26.61954296875" Y="-3.998064208984" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.807845703125" Y="-3.975367431641" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791015625" />
                  <Point X="-27.122037109375" Y="-4.062096923828" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.369412109375" Y="-4.30023046875" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.6437265625" Y="-4.298941894531" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.063580078125" Y="-4.007652099609" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.887416015625" Y="-3.289693115234" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597592285156" />
                  <Point X="-27.51398046875" Y="-2.568762939453" />
                  <Point X="-27.531322265625" Y="-2.551419189453" />
                  <Point X="-27.56015234375" Y="-2.537198974609" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.2205078125" Y="-2.906522460938" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-28.99412890625" Y="-3.0672890625" />
                  <Point X="-29.161697265625" Y="-2.847136962891" />
                  <Point X="-29.310640625" Y="-2.597381591797" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-28.83108203125" Y="-1.935178466797" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.396014770508" />
                  <Point X="-28.1381171875" Y="-1.366267456055" />
                  <Point X="-28.14032421875" Y="-1.334595703125" />
                  <Point X="-28.161158203125" Y="-1.310638671875" />
                  <Point X="-28.187640625" Y="-1.295052124023" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.013361328125" Y="-1.393081054688" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.8618515625" Y="-1.267778198242" />
                  <Point X="-29.927392578125" Y="-1.011188232422" />
                  <Point X="-29.966796875" Y="-0.735672485352" />
                  <Point X="-29.99839453125" Y="-0.514742248535" />
                  <Point X="-29.317611328125" Y="-0.332326934814" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.527677734375" Y="-0.111557266235" />
                  <Point X="-28.50232421875" Y="-0.090645690918" />
                  <Point X="-28.49016015625" Y="-0.060637718201" />
                  <Point X="-28.483400390625" Y="-0.031280040741" />
                  <Point X="-28.49038671875" Y="-0.001188552022" />
                  <Point X="-28.50232421875" Y="0.028085613251" />
                  <Point X="-28.528361328125" Y="0.049471488953" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.281078125" Y="0.259977722168" />
                  <Point X="-29.998185546875" Y="0.452126068115" />
                  <Point X="-29.959884765625" Y="0.710968994141" />
                  <Point X="-29.91764453125" Y="0.996414978027" />
                  <Point X="-29.838322265625" Y="1.289141601563" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.298158203125" Y="1.465716918945" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.700234375" Y="1.405788818359" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.651533203125" Y="1.426056518555" />
                  <Point X="-28.6391171875" Y="1.443786743164" />
                  <Point X="-28.626490234375" Y="1.474271606445" />
                  <Point X="-28.614470703125" Y="1.503291137695" />
                  <Point X="-28.610712890625" Y="1.524605834961" />
                  <Point X="-28.61631640625" Y="1.54551184082" />
                  <Point X="-28.631552734375" Y="1.574780029297" />
                  <Point X="-28.646056640625" Y="1.602641601562" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-29.07503515625" Y="1.93771472168" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.324140625" Y="2.505817871094" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-28.949892578125" Y="3.057087890625" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.493814453125" Y="3.120158691406" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.094685546875" Y="2.916600341797" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775146484" />
                  <Point X="-28.013251953125" Y="2.927404052734" />
                  <Point X="-27.982142578125" Y="2.958513427734" />
                  <Point X="-27.95252734375" Y="2.988127441406" />
                  <Point X="-27.9408984375" Y="3.006382080078" />
                  <Point X="-27.938072265625" Y="3.027841552734" />
                  <Point X="-27.94190625" Y="3.071669433594" />
                  <Point X="-27.945556640625" Y="3.113391113281" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.13599609375" Y="3.452607666016" />
                  <Point X="-28.30727734375" Y="3.749276611328" />
                  <Point X="-28.038732421875" Y="3.955168457031" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.42194140625" Y="4.35819140625" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.070275390625" Y="4.421087890625" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.902466796875" Y="4.248267089844" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.81380859375" Y="4.22225" />
                  <Point X="-26.763001953125" Y="4.243295898438" />
                  <Point X="-26.714634765625" Y="4.263329589844" />
                  <Point X="-26.696904296875" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.294488769531" />
                  <Point X="-26.669544921875" Y="4.3469375" />
                  <Point X="-26.653802734375" Y="4.396865234375" />
                  <Point X="-26.651916015625" Y="4.418426269531" />
                  <Point X="-26.672828125" Y="4.577258789062" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.33815234375" Y="4.799544921875" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.566900390625" Y="4.95025" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.141140625" Y="4.680381347656" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.851302734375" Y="4.662624023438" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.462908203125" Y="4.959404296875" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.807875" Y="4.845430664062" />
                  <Point X="-23.491544921875" Y="4.76905859375" />
                  <Point X="-23.275248046875" Y="4.690605957031" />
                  <Point X="-23.06896484375" Y="4.615785644531" />
                  <Point X="-22.86005859375" Y="4.518087402344" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.459486328125" Y="4.307557128906" />
                  <Point X="-22.26747265625" Y="4.195688964844" />
                  <Point X="-22.077138671875" Y="4.060335449219" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.328724609375" Y="3.268162353516" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491514892578" />
                  <Point X="-22.786146484375" Y="2.450383300781" />
                  <Point X="-22.7966171875" Y="2.411228515625" />
                  <Point X="-22.797955078125" Y="2.392326660156" />
                  <Point X="-22.793666015625" Y="2.356759521484" />
                  <Point X="-22.789583984375" Y="2.322901855469" />
                  <Point X="-22.78131640625" Y="2.300811279297" />
                  <Point X="-22.75930859375" Y="2.268377441406" />
                  <Point X="-22.738357421875" Y="2.237502685547" />
                  <Point X="-22.72505859375" Y="2.224203613281" />
                  <Point X="-22.692625" Y="2.202195800781" />
                  <Point X="-22.66175" Y="2.18124609375" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.604095703125" Y="2.16869140625" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5513359375" Y="2.165946289062" />
                  <Point X="-22.510203125" Y="2.176945556641" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.731146484375" Y="2.612623779297" />
                  <Point X="-21.005751953125" Y="3.031430419922" />
                  <Point X="-20.911302734375" Y="2.900166992188" />
                  <Point X="-20.79740234375" Y="2.741874023438" />
                  <Point X="-20.691298828125" Y="2.566533447266" />
                  <Point X="-20.612486328125" Y="2.436295654297" />
                  <Point X="-21.130208984375" Y="2.039032226562" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583833129883" />
                  <Point X="-21.75023046875" Y="1.545214477539" />
                  <Point X="-21.77841015625" Y="1.508451782227" />
                  <Point X="-21.78687890625" Y="1.491501464844" />
                  <Point X="-21.79790625" Y="1.452071533203" />
                  <Point X="-21.808404296875" Y="1.414536865234" />
                  <Point X="-21.809220703125" Y="1.390965087891" />
                  <Point X="-21.80016796875" Y="1.347094360352" />
                  <Point X="-21.79155078125" Y="1.305332275391" />
                  <Point X="-21.784353515625" Y="1.287956298828" />
                  <Point X="-21.759732421875" Y="1.250534301758" />
                  <Point X="-21.736296875" Y="1.214910888672" />
                  <Point X="-21.719052734375" Y="1.19882019043" />
                  <Point X="-21.683373046875" Y="1.178736206055" />
                  <Point X="-21.64941015625" Y="1.159617675781" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.583193359375" Y="1.147244262695" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.83281640625" Y="1.232193481445" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.109728515625" Y="1.152320922852" />
                  <Point X="-20.060806640625" Y="0.951366699219" />
                  <Point X="-20.02737109375" Y="0.736618041992" />
                  <Point X="-20.002140625" Y="0.57455645752" />
                  <Point X="-20.592794921875" Y="0.416290740967" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819198608" />
                  <Point X="-21.318306640625" Y="0.205424575806" />
                  <Point X="-21.363421875" Y="0.179346679688" />
                  <Point X="-21.377734375" Y="0.166927154541" />
                  <Point X="-21.406169921875" Y="0.130692443848" />
                  <Point X="-21.433240234375" Y="0.09619947052" />
                  <Point X="-21.443013671875" Y="0.074735412598" />
                  <Point X="-21.4524921875" Y="0.025240736008" />
                  <Point X="-21.461515625" Y="-0.021875146866" />
                  <Point X="-21.461515625" Y="-0.040684932709" />
                  <Point X="-21.452037109375" Y="-0.090179611206" />
                  <Point X="-21.443013671875" Y="-0.13729548645" />
                  <Point X="-21.433240234375" Y="-0.158759552002" />
                  <Point X="-21.4048046875" Y="-0.194994094849" />
                  <Point X="-21.377734375" Y="-0.229487228394" />
                  <Point X="-21.363421875" Y="-0.24190675354" />
                  <Point X="-21.31602734375" Y="-0.269301361084" />
                  <Point X="-21.270912109375" Y="-0.295379272461" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.624806640625" Y="-0.470273742676" />
                  <Point X="-20.001931640625" Y="-0.637172546387" />
                  <Point X="-20.02425390625" Y="-0.785237915039" />
                  <Point X="-20.051568359375" Y="-0.966413330078" />
                  <Point X="-20.09440625" Y="-1.154129638672" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-20.815603515625" Y="-1.199318481445" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.6981796875" Y="-1.118558959961" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.814552734375" Y="-1.154697021484" />
                  <Point X="-21.870775390625" Y="-1.222316040039" />
                  <Point X="-21.924296875" Y="-1.286685058594" />
                  <Point X="-21.935640625" Y="-1.314071166992" />
                  <Point X="-21.94369921875" Y="-1.40164074707" />
                  <Point X="-21.951369140625" Y="-1.485001464844" />
                  <Point X="-21.943638671875" Y="-1.516622192383" />
                  <Point X="-21.892162109375" Y="-1.59669140625" />
                  <Point X="-21.843158203125" Y="-1.672912719727" />
                  <Point X="-21.831537109375" Y="-1.685540771484" />
                  <Point X="-21.243158203125" Y="-2.137020751953" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.718875" Y="-2.677555419922" />
                  <Point X="-20.795865234375" Y="-2.802138183594" />
                  <Point X="-20.884462890625" Y="-2.928023193359" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.55860546875" Y="-2.656397949219" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.37336328125" Y="-2.233319580078" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.602890625" Y="-2.267647460938" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.334683105469" />
                  <Point X="-22.75980078125" Y="-2.426652099609" />
                  <Point X="-22.80587890625" Y="-2.514201171875" />
                  <Point X="-22.8108359375" Y="-2.546374755859" />
                  <Point X="-22.790841796875" Y="-2.657080322266" />
                  <Point X="-22.77180859375" Y="-2.762465087891" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.387814453125" Y="-3.433455322266" />
                  <Point X="-22.01332421875" Y="-4.082088867188" />
                  <Point X="-22.07333984375" Y="-4.124956542969" />
                  <Point X="-22.16469921875" Y="-4.1902109375" />
                  <Point X="-22.26375390625" Y="-4.254328613281" />
                  <Point X="-22.32022265625" Y="-4.290879394531" />
                  <Point X="-22.791859375" Y="-3.676230224609" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.438634765625" Y="-2.910270996094" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.693607421875" Y="-2.846705566406" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.926873046875" Y="-2.945176513672" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045985595703" />
                  <Point X="-24.059111328125" Y="-3.172827636719" />
                  <Point X="-24.085357421875" Y="-3.293573486328" />
                  <Point X="-24.0860703125" Y="-3.310719970703" />
                  <Point X="-23.978919921875" Y="-4.124595703125" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.919220703125" Y="-4.944301269531" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.097162109375" Y="-4.979870605469" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#168" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.099001320624" Y="4.724602451013" Z="1.3" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.3" />
                  <Point X="-0.578869750749" Y="5.03119123621" Z="1.3" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.3" />
                  <Point X="-1.357806397926" Y="4.878968586704" Z="1.3" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.3" />
                  <Point X="-1.728556571075" Y="4.60201308957" Z="1.3" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.3" />
                  <Point X="-1.723388128706" Y="4.393252690368" Z="1.3" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.3" />
                  <Point X="-1.788291164805" Y="4.320770070718" Z="1.3" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.3" />
                  <Point X="-1.885534918028" Y="4.323897733978" Z="1.3" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.3" />
                  <Point X="-2.036764365853" Y="4.48280573272" Z="1.3" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.3" />
                  <Point X="-2.452380594318" Y="4.43317901197" Z="1.3" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.3" />
                  <Point X="-3.075310283697" Y="4.026128437953" Z="1.3" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.3" />
                  <Point X="-3.185453935701" Y="3.458887695732" Z="1.3" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.3" />
                  <Point X="-2.997874493648" Y="3.098591433586" Z="1.3" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.3" />
                  <Point X="-3.023654214502" Y="3.025149353384" Z="1.3" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.3" />
                  <Point X="-3.096485032021" Y="2.997690205525" Z="1.3" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.3" />
                  <Point X="-3.474971652565" Y="3.194740013045" Z="1.3" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.3" />
                  <Point X="-3.995512592293" Y="3.119070216922" Z="1.3" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.3" />
                  <Point X="-4.373479316101" Y="2.562303013891" Z="1.3" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.3" />
                  <Point X="-4.111630402498" Y="1.929327050382" Z="1.3" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.3" />
                  <Point X="-3.682058490139" Y="1.582972577373" Z="1.3" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.3" />
                  <Point X="-3.678842734788" Y="1.52468480465" Z="1.3" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.3" />
                  <Point X="-3.721426769708" Y="1.484754757443" Z="1.3" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.3" />
                  <Point X="-4.297789993677" Y="1.546569202375" Z="1.3" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.3" />
                  <Point X="-4.892738690327" Y="1.333498870703" Z="1.3" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.3" />
                  <Point X="-5.015501743284" Y="0.749568191571" Z="1.3" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.3" />
                  <Point X="-4.300177200798" Y="0.242961482507" Z="1.3" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.3" />
                  <Point X="-3.56302596409" Y="0.039675127649" Z="1.3" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.3" />
                  <Point X="-3.544296191367" Y="0.015270429838" Z="1.3" />
                  <Point X="-3.539556741714" Y="0" Z="1.3" />
                  <Point X="-3.544068409768" Y="-0.014536521223" Z="1.3" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.3" />
                  <Point X="-3.562342631033" Y="-0.039200855485" Z="1.3" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.3" />
                  <Point X="-4.336710587974" Y="-0.252750574249" Z="1.3" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.3" />
                  <Point X="-5.022450528877" Y="-0.711471686779" Z="1.3" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.3" />
                  <Point X="-4.916453850021" Y="-1.248872171566" Z="1.3" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.3" />
                  <Point X="-4.012991893278" Y="-1.411373385736" Z="1.3" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.3" />
                  <Point X="-3.206243624714" Y="-1.31446466982" Z="1.3" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.3" />
                  <Point X="-3.196433703667" Y="-1.33695726604" Z="1.3" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.3" />
                  <Point X="-3.867676063311" Y="-1.864230811479" Z="1.3" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.3" />
                  <Point X="-4.359741589539" Y="-2.591711530379" Z="1.3" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.3" />
                  <Point X="-4.040070236586" Y="-3.066292256805" Z="1.3" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.3" />
                  <Point X="-3.201665355042" Y="-2.918543743106" Z="1.3" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.3" />
                  <Point X="-2.564378812836" Y="-2.563951656295" Z="1.3" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.3" />
                  <Point X="-2.936873270062" Y="-3.23341302745" Z="1.3" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.3" />
                  <Point X="-3.10024153536" Y="-4.015989303634" Z="1.3" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.3" />
                  <Point X="-2.676205579876" Y="-4.310172776294" Z="1.3" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.3" />
                  <Point X="-2.335901320073" Y="-4.299388641202" Z="1.3" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.3" />
                  <Point X="-2.100414967645" Y="-4.072390298874" Z="1.3" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.3" />
                  <Point X="-1.817272784609" Y="-3.993980341062" Z="1.3" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.3" />
                  <Point X="-1.54490809602" Y="-4.104139819448" Z="1.3" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.3" />
                  <Point X="-1.39588741247" Y="-4.357340223605" Z="1.3" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.3" />
                  <Point X="-1.389582437169" Y="-4.700876914613" Z="1.3" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.3" />
                  <Point X="-1.26889078205" Y="-4.916606784025" Z="1.3" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.3" />
                  <Point X="-0.971224990995" Y="-4.983957013492" Z="1.3" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.3" />
                  <Point X="-0.612445861271" Y="-4.247863147788" Z="1.3" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.3" />
                  <Point X="-0.337238586566" Y="-3.40372705512" Z="1.3" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.3" />
                  <Point X="-0.129798651239" Y="-3.244524079766" Z="1.3" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.3" />
                  <Point X="0.123560428122" Y="-3.242587965522" Z="1.3" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.3" />
                  <Point X="0.333207272867" Y="-3.397918700648" Z="1.3" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.3" />
                  <Point X="0.62230899913" Y="-4.284672882028" Z="1.3" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.3" />
                  <Point X="0.905618922463" Y="-4.997785492762" Z="1.3" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.3" />
                  <Point X="1.085327364905" Y="-4.961861495701" Z="1.3" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.3" />
                  <Point X="1.064494557099" Y="-4.086789333399" Z="1.3" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.3" />
                  <Point X="0.983590391186" Y="-3.152167913664" Z="1.3" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.3" />
                  <Point X="1.098936098204" Y="-2.952343079598" Z="1.3" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.3" />
                  <Point X="1.304817566023" Y="-2.86521507199" Z="1.3" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.3" />
                  <Point X="1.528168407372" Y="-2.921049088223" Z="1.3" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.3" />
                  <Point X="2.162315308824" Y="-3.675388161711" Z="1.3" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.3" />
                  <Point X="2.757256680205" Y="-4.265023015216" Z="1.3" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.3" />
                  <Point X="2.949563363639" Y="-4.134363559744" Z="1.3" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.3" />
                  <Point X="2.649330306633" Y="-3.377175344392" Z="1.3" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.3" />
                  <Point X="2.252204452388" Y="-2.616913697938" Z="1.3" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.3" />
                  <Point X="2.278287520022" Y="-2.418659240712" Z="1.3" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.3" />
                  <Point X="2.41423913766" Y="-2.280613789945" Z="1.3" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.3" />
                  <Point X="2.611593105575" Y="-2.251243557102" Z="1.3" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.3" />
                  <Point X="3.410238252715" Y="-2.668419193642" Z="1.3" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.3" />
                  <Point X="4.150269017781" Y="-2.9255204382" Z="1.3" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.3" />
                  <Point X="4.317501958032" Y="-2.672560430404" Z="1.3" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.3" />
                  <Point X="3.781122847854" Y="-2.066072960546" Z="1.3" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.3" />
                  <Point X="3.143739681861" Y="-1.538371861065" Z="1.3" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.3" />
                  <Point X="3.099933342133" Y="-1.374941692535" Z="1.3" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.3" />
                  <Point X="3.16151243026" Y="-1.223003116205" Z="1.3" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.3" />
                  <Point X="3.306282450309" Y="-1.136138143262" Z="1.3" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.3" />
                  <Point X="4.171714726896" Y="-1.217610763186" Z="1.3" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.3" />
                  <Point X="4.948183124032" Y="-1.133973254541" Z="1.3" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.3" />
                  <Point X="5.019030235355" Y="-0.761411894577" Z="1.3" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.3" />
                  <Point X="4.381978799522" Y="-0.390697410515" Z="1.3" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.3" />
                  <Point X="3.702836600498" Y="-0.194732722875" Z="1.3" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.3" />
                  <Point X="3.628373055193" Y="-0.132845099923" Z="1.3" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.3" />
                  <Point X="3.590913503876" Y="-0.049494640899" Z="1.3" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.3" />
                  <Point X="3.590457946548" Y="0.047115890311" Z="1.3" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.3" />
                  <Point X="3.627006383207" Y="0.131103638652" Z="1.3" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.3" />
                  <Point X="3.700558813855" Y="0.193416121367" Z="1.3" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.3" />
                  <Point X="4.413989017784" Y="0.399274522254" Z="1.3" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.3" />
                  <Point X="5.015875325539" Y="0.775589934071" Z="1.3" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.3" />
                  <Point X="4.932696619912" Y="1.195427738366" Z="1.3" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.3" />
                  <Point X="4.154500533195" Y="1.313045898952" Z="1.3" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.3" />
                  <Point X="3.417200233077" Y="1.228093126633" Z="1.3" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.3" />
                  <Point X="3.334996951162" Y="1.253587182535" Z="1.3" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.3" />
                  <Point X="3.275881412019" Y="1.309294453606" Z="1.3" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.3" />
                  <Point X="3.242644037776" Y="1.388478576394" Z="1.3" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.3" />
                  <Point X="3.244089027575" Y="1.469883941461" Z="1.3" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.3" />
                  <Point X="3.283295714306" Y="1.54607637893" Z="1.3" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.3" />
                  <Point X="3.89407082143" Y="2.030644553973" Z="1.3" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.3" />
                  <Point X="4.345322506873" Y="2.623699843" Z="1.3" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.3" />
                  <Point X="4.123127021654" Y="2.960650458805" Z="1.3" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.3" />
                  <Point X="3.237696764165" Y="2.687205149396" Z="1.3" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.3" />
                  <Point X="2.470723194656" Y="2.256528284161" Z="1.3" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.3" />
                  <Point X="2.395733913635" Y="2.249611791389" Z="1.3" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.3" />
                  <Point X="2.329291825774" Y="2.274850448444" Z="1.3" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.3" />
                  <Point X="2.275908151791" Y="2.327733034608" Z="1.3" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.3" />
                  <Point X="2.24981791137" Y="2.394024530197" Z="1.3" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.3" />
                  <Point X="2.255999658497" Y="2.468746381226" Z="1.3" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.3" />
                  <Point X="2.708420203463" Y="3.27444231779" Z="1.3" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.3" />
                  <Point X="2.945680433728" Y="4.13236235749" Z="1.3" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.3" />
                  <Point X="2.559526809042" Y="4.382039972677" Z="1.3" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.3" />
                  <Point X="2.154965818492" Y="4.594659397847" Z="1.3" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.3" />
                  <Point X="1.735644835032" Y="4.76888973426" Z="1.3" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.3" />
                  <Point X="1.197701738729" Y="4.925314100069" Z="1.3" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.3" />
                  <Point X="0.536141559079" Y="5.040412558745" Z="1.3" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.3" />
                  <Point X="0.094243240929" Y="4.706845054938" Z="1.3" />
                  <Point X="0" Y="4.355124473572" Z="1.3" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>