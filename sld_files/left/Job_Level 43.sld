<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#201" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3084" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.105306640625" Y="-4.749287109375" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497141357422" />
                  <Point X="-24.457634765625" Y="-3.467376708984" />
                  <Point X="-24.59308203125" Y="-3.272222900391" />
                  <Point X="-24.62136328125" Y="-3.231476318359" />
                  <Point X="-24.643248046875" Y="-3.209020019531" />
                  <Point X="-24.66950390625" Y="-3.189776367188" />
                  <Point X="-24.69750390625" Y="-3.175669189453" />
                  <Point X="-24.907099609375" Y="-3.110617919922" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9790234375" Y="-3.092766357422" />
                  <Point X="-25.0086640625" Y="-3.092766357422" />
                  <Point X="-25.03682421875" Y="-3.097035644531" />
                  <Point X="-25.246419921875" Y="-3.162086669922" />
                  <Point X="-25.29018359375" Y="-3.175668945312" />
                  <Point X="-25.318185546875" Y="-3.189778320312" />
                  <Point X="-25.34444140625" Y="-3.2090234375" />
                  <Point X="-25.36632421875" Y="-3.231477783203" />
                  <Point X="-25.501771484375" Y="-3.426631347656" />
                  <Point X="-25.53005078125" Y="-3.467378173828" />
                  <Point X="-25.538185546875" Y="-3.481571044922" />
                  <Point X="-25.550990234375" Y="-3.512524658203" />
                  <Point X="-25.620181640625" Y="-3.770751708984" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.03184375" Y="-4.854569335938" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.246416015625" Y="-4.802362304688" />
                  <Point X="-26.2149609375" Y="-4.563438964844" />
                  <Point X="-26.21419921875" Y="-4.547929199219" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.26658203125" Y="-4.264491210938" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.18296484375" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.51661328125" Y="-3.961973388672" />
                  <Point X="-26.556904296875" Y="-3.926639404297" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.899142578125" Y="-3.8741796875" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.983419921875" Y="-3.873708984375" />
                  <Point X="-27.01446875" Y="-3.882029052734" />
                  <Point X="-27.042658203125" Y="-3.894802001953" />
                  <Point X="-27.25606640625" Y="-4.037397216797" />
                  <Point X="-27.300623046875" Y="-4.067170166016" />
                  <Point X="-27.31278515625" Y="-4.076822021484" />
                  <Point X="-27.335099609375" Y="-4.099459960938" />
                  <Point X="-27.373947265625" Y="-4.150084472656" />
                  <Point X="-27.4801484375" Y="-4.288489257813" />
                  <Point X="-27.732095703125" Y="-4.132489746094" />
                  <Point X="-27.80171484375" Y="-4.0893828125" />
                  <Point X="-28.104720703125" Y="-3.856077148438" />
                  <Point X="-28.039611328125" Y="-3.743303466797" />
                  <Point X="-27.42376171875" Y="-2.676619873047" />
                  <Point X="-27.412859375" Y="-2.647655761719" />
                  <Point X="-27.406587890625" Y="-2.616131347656" />
                  <Point X="-27.40557421875" Y="-2.585198730469" />
                  <Point X="-27.414556640625" Y="-2.555581542969" />
                  <Point X="-27.428771484375" Y="-2.526753417969" />
                  <Point X="-27.44680078125" Y="-2.501593017578" />
                  <Point X="-27.46115234375" Y="-2.487240966797" />
                  <Point X="-27.4641484375" Y="-2.484244384766" />
                  <Point X="-27.489302734375" Y="-2.466218017578" />
                  <Point X="-27.5181328125" Y="-2.451998535156" />
                  <Point X="-27.54775" Y="-2.443012207031" />
                  <Point X="-27.5786875" Y="-2.444024169922" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-27.8618984375" Y="-2.589782226562" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-29.027861328125" Y="-2.866116455078" />
                  <Point X="-29.082857421875" Y="-2.793862304688" />
                  <Point X="-29.306142578125" Y="-2.41944921875" />
                  <Point X="-29.1834921875" Y="-2.325337402344" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.084580078125" Y="-1.475596069336" />
                  <Point X="-28.066615234375" Y="-1.448467041016" />
                  <Point X="-28.053857421875" Y="-1.419839111328" />
                  <Point X="-28.047482421875" Y="-1.395229980469" />
                  <Point X="-28.04614453125" Y="-1.390062133789" />
                  <Point X="-28.043345703125" Y="-1.359643188477" />
                  <Point X="-28.045556640625" Y="-1.327976196289" />
                  <Point X="-28.05255859375" Y="-1.298232543945" />
                  <Point X="-28.068642578125" Y="-1.272250732422" />
                  <Point X="-28.089474609375" Y="-1.248297241211" />
                  <Point X="-28.112970703125" Y="-1.228767333984" />
                  <Point X="-28.13487890625" Y="-1.215873046875" />
                  <Point X="-28.139453125" Y="-1.213180786133" />
                  <Point X="-28.168716796875" Y="-1.20195703125" />
                  <Point X="-28.200603515625" Y="-1.195475097656" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-28.5130859375" Y="-1.231398925781" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.812564453125" Y="-1.076868896484" />
                  <Point X="-29.834076171875" Y="-0.992649719238" />
                  <Point X="-29.892423828125" Y="-0.584698608398" />
                  <Point X="-29.7603515625" Y="-0.549309997559" />
                  <Point X="-28.532875" Y="-0.220408309937" />
                  <Point X="-28.5174921875" Y="-0.214827438354" />
                  <Point X="-28.4877265625" Y="-0.199468978882" />
                  <Point X="-28.464767578125" Y="-0.183534179688" />
                  <Point X="-28.459978515625" Y="-0.180210571289" />
                  <Point X="-28.43752734375" Y="-0.1583309021" />
                  <Point X="-28.41827734375" Y="-0.132070251465" />
                  <Point X="-28.404166015625" Y="-0.1040625" />
                  <Point X="-28.396513671875" Y="-0.079403999329" />
                  <Point X="-28.394916015625" Y="-0.074255729675" />
                  <Point X="-28.390646484375" Y="-0.046098762512" />
                  <Point X="-28.390646484375" Y="-0.016461385727" />
                  <Point X="-28.394916015625" Y="0.011695732117" />
                  <Point X="-28.402568359375" Y="0.0363540802" />
                  <Point X="-28.404166015625" Y="0.041502658844" />
                  <Point X="-28.4182734375" Y="0.069503883362" />
                  <Point X="-28.437517578125" Y="0.095761192322" />
                  <Point X="-28.45997265625" Y="0.117646942139" />
                  <Point X="-28.482931640625" Y="0.133581893921" />
                  <Point X="-28.49340625" Y="0.139893341064" />
                  <Point X="-28.51376953125" Y="0.150440795898" />
                  <Point X="-28.532875" Y="0.157848175049" />
                  <Point X="-28.789162109375" Y="0.226520568848" />
                  <Point X="-29.891814453125" Y="0.521975463867" />
                  <Point X="-29.838349609375" Y="0.883296630859" />
                  <Point X="-29.82448828125" Y="0.976969055176" />
                  <Point X="-29.703548828125" Y="1.423267822266" />
                  <Point X="-29.64555078125" Y="1.415632202148" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.744984375" Y="1.299341674805" />
                  <Point X="-28.72342578125" Y="1.301227416992" />
                  <Point X="-28.70313671875" Y="1.305263183594" />
                  <Point X="-28.652322265625" Y="1.321285400391" />
                  <Point X="-28.6417109375" Y="1.324630615234" />
                  <Point X="-28.622779296875" Y="1.3329609375" />
                  <Point X="-28.60403515625" Y="1.343782714844" />
                  <Point X="-28.58735546875" Y="1.356012329102" />
                  <Point X="-28.57371875" Y="1.371561889648" />
                  <Point X="-28.561302734375" Y="1.389291992188" />
                  <Point X="-28.5513515625" Y="1.407429931641" />
                  <Point X="-28.5309609375" Y="1.45665612793" />
                  <Point X="-28.526701171875" Y="1.466940185547" />
                  <Point X="-28.52091015625" Y="1.486807983398" />
                  <Point X="-28.517154296875" Y="1.508119873047" />
                  <Point X="-28.5158046875" Y="1.528757324219" />
                  <Point X="-28.518951171875" Y="1.549198120117" />
                  <Point X="-28.524552734375" Y="1.570101196289" />
                  <Point X="-28.532046875" Y="1.589375" />
                  <Point X="-28.5566484375" Y="1.63663671875" />
                  <Point X="-28.561798828125" Y="1.646526123047" />
                  <Point X="-28.573291015625" Y="1.663720092773" />
                  <Point X="-28.587201171875" Y="1.680294067383" />
                  <Point X="-28.60213671875" Y="1.69458984375" />
                  <Point X="-28.749142578125" Y="1.807393066406" />
                  <Point X="-29.351859375" Y="2.269873291016" />
                  <Point X="-29.135013671875" Y="2.641381591797" />
                  <Point X="-29.0811484375" Y="2.733664550781" />
                  <Point X="-28.75050390625" Y="3.158662597656" />
                  <Point X="-28.747056640625" Y="3.156671875" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.07601953125" Y="2.819604492188" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.99901171875" Y="2.826504882812" />
                  <Point X="-27.9804609375" Y="2.835654541016" />
                  <Point X="-27.96220703125" Y="2.847284179688" />
                  <Point X="-27.946078125" Y="2.860229003906" />
                  <Point X="-27.895841796875" Y="2.910463867188" />
                  <Point X="-27.885353515625" Y="2.920952392578" />
                  <Point X="-27.872404296875" Y="2.937086181641" />
                  <Point X="-27.860775390625" Y="2.955340576172" />
                  <Point X="-27.85162890625" Y="2.973889404297" />
                  <Point X="-27.8467109375" Y="2.993978271484" />
                  <Point X="-27.843884765625" Y="3.015437255859" />
                  <Point X="-27.84343359375" Y="3.03612109375" />
                  <Point X="-27.849625" Y="3.106893798828" />
                  <Point X="-27.85091796875" Y="3.121670654297" />
                  <Point X="-27.854955078125" Y="3.141964355469" />
                  <Point X="-27.86146484375" Y="3.162605957031" />
                  <Point X="-27.869794921875" Y="3.181533447266" />
                  <Point X="-27.934939453125" Y="3.294365234375" />
                  <Point X="-28.18333203125" Y="3.724595947266" />
                  <Point X="-27.794427734375" Y="4.022764404297" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.170373046875" Y="4.389280273438" />
                  <Point X="-27.16703515625" Y="4.391134277344" />
                  <Point X="-27.043197265625" Y="4.229745117188" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.995111328125" Y="4.18939453125" />
                  <Point X="-26.916341796875" Y="4.148389648438" />
                  <Point X="-26.89989453125" Y="4.139827636719" />
                  <Point X="-26.8806171875" Y="4.132331054688" />
                  <Point X="-26.8597109375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.12358203125" />
                  <Point X="-26.81862890625" Y="4.124934570312" />
                  <Point X="-26.7973125" Y="4.128692871094" />
                  <Point X="-26.777451171875" Y="4.134481445312" />
                  <Point X="-26.695408203125" Y="4.168465332031" />
                  <Point X="-26.67827734375" Y="4.175561035156" />
                  <Point X="-26.660142578125" Y="4.185511230469" />
                  <Point X="-26.6424140625" Y="4.197925292969" />
                  <Point X="-26.62686328125" Y="4.211562011719" />
                  <Point X="-26.6146328125" Y="4.228240722656" />
                  <Point X="-26.603810546875" Y="4.246983886719" />
                  <Point X="-26.595478515625" Y="4.265920410156" />
                  <Point X="-26.568775390625" Y="4.350613769531" />
                  <Point X="-26.56319921875" Y="4.368296875" />
                  <Point X="-26.5591640625" Y="4.388583007812" />
                  <Point X="-26.55727734375" Y="4.410146484375" />
                  <Point X="-26.557728515625" Y="4.430827148438" />
                  <Point X="-26.565134765625" Y="4.48708203125" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.071068359375" Y="4.775762695312" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.3068125" Y="4.885041015625" />
                  <Point X="-25.29376953125" Y="4.882942871094" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.820404296875" Y="4.410887207031" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.26199609375" Y="4.84284375" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.624130859375" Y="4.703339355469" />
                  <Point X="-23.51897265625" Y="4.677950683594" />
                  <Point X="-23.17305859375" Y="4.552485839844" />
                  <Point X="-23.10535546875" Y="4.527928710938" />
                  <Point X="-22.770623046875" Y="4.37138671875" />
                  <Point X="-22.705423828125" Y="4.340894042969" />
                  <Point X="-22.382037109375" Y="4.152489257813" />
                  <Point X="-22.319021484375" Y="4.115775878906" />
                  <Point X="-22.05673828125" Y="3.929254150391" />
                  <Point X="-22.1396171875" Y="3.785704589844" />
                  <Point X="-22.852416015625" Y="2.551098144531" />
                  <Point X="-22.857919921875" Y="2.539937988281" />
                  <Point X="-22.866921875" Y="2.516059082031" />
                  <Point X="-22.88468359375" Y="2.449640625" />
                  <Point X="-22.88680078125" Y="2.43955859375" />
                  <Point X="-22.89184765625" Y="2.406789306641" />
                  <Point X="-22.892271484375" Y="2.380955078125" />
                  <Point X="-22.885345703125" Y="2.323521972656" />
                  <Point X="-22.883900390625" Y="2.311530273438" />
                  <Point X="-22.878560546875" Y="2.289612792969" />
                  <Point X="-22.870294921875" Y="2.267521484375" />
                  <Point X="-22.859927734375" Y="2.247469970703" />
                  <Point X="-22.824388671875" Y="2.195096435547" />
                  <Point X="-22.817986328125" Y="2.186704589844" />
                  <Point X="-22.797265625" Y="2.16246875" />
                  <Point X="-22.778400390625" Y="2.145592041016" />
                  <Point X="-22.72602734375" Y="2.110054443359" />
                  <Point X="-22.715091796875" Y="2.102634521484" />
                  <Point X="-22.695044921875" Y="2.092270996094" />
                  <Point X="-22.67295703125" Y="2.084005615234" />
                  <Point X="-22.65103515625" Y="2.078663330078" />
                  <Point X="-22.5936015625" Y="2.071737792969" />
                  <Point X="-22.582560546875" Y="2.071055175781" />
                  <Point X="-22.551666015625" Y="2.070947021484" />
                  <Point X="-22.52679296875" Y="2.074171142578" />
                  <Point X="-22.460373046875" Y="2.091932373047" />
                  <Point X="-22.454685546875" Y="2.093645507812" />
                  <Point X="-22.428736328125" Y="2.102355224609" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-22.1536875" Y="2.258972412109" />
                  <Point X="-21.032673828125" Y="2.906190429688" />
                  <Point X="-20.914109375" Y="2.741412109375" />
                  <Point X="-20.87671875" Y="2.689450683594" />
                  <Point X="-20.73780078125" Y="2.459883056641" />
                  <Point X="-20.83074609375" Y="2.388563476562" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.778572265625" Y="1.660243896484" />
                  <Point X="-21.79602734375" Y="1.641626953125" />
                  <Point X="-21.843828125" Y="1.579266235352" />
                  <Point X="-21.849328125" Y="1.571274780273" />
                  <Point X="-21.86777734375" Y="1.541305786133" />
                  <Point X="-21.878369140625" Y="1.51708996582" />
                  <Point X="-21.89617578125" Y="1.453419311523" />
                  <Point X="-21.89989453125" Y="1.440125488281" />
                  <Point X="-21.90334765625" Y="1.417826538086" />
                  <Point X="-21.9041640625" Y="1.394250488281" />
                  <Point X="-21.90226171875" Y="1.371765258789" />
                  <Point X="-21.88764453125" Y="1.300923583984" />
                  <Point X="-21.885119140625" Y="1.29127722168" />
                  <Point X="-21.874869140625" Y="1.259111572266" />
                  <Point X="-21.86371875" Y="1.235741577148" />
                  <Point X="-21.823962890625" Y="1.175313232422" />
                  <Point X="-21.815662109375" Y="1.162696166992" />
                  <Point X="-21.801107421875" Y="1.145451049805" />
                  <Point X="-21.78386328125" Y="1.129360839844" />
                  <Point X="-21.76565234375" Y="1.116034667969" />
                  <Point X="-21.7080390625" Y="1.083603637695" />
                  <Point X="-21.698638671875" Y="1.078974975586" />
                  <Point X="-21.6686328125" Y="1.066205688477" />
                  <Point X="-21.643880859375" Y="1.059438476562" />
                  <Point X="-21.565984375" Y="1.049143432617" />
                  <Point X="-21.56048828125" Y="1.048579101563" />
                  <Point X="-21.531146484375" Y="1.046426391602" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-21.26692578125" Y="1.079222290039" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.170119140625" Y="0.998756591797" />
                  <Point X="-20.15405859375" Y="0.932786132812" />
                  <Point X="-20.1091328125" Y="0.644238708496" />
                  <Point X="-20.2079140625" Y="0.617770324707" />
                  <Point X="-21.283419921875" Y="0.32958972168" />
                  <Point X="-21.295212890625" Y="0.325584594727" />
                  <Point X="-21.318453125" Y="0.315067657471" />
                  <Point X="-21.394984375" Y="0.270831512451" />
                  <Point X="-21.4026953125" Y="0.265862976074" />
                  <Point X="-21.432984375" Y="0.244207641602" />
                  <Point X="-21.452466796875" Y="0.225577194214" />
                  <Point X="-21.498384765625" Y="0.167066253662" />
                  <Point X="-21.50797265625" Y="0.154849395752" />
                  <Point X="-21.5196953125" Y="0.135571411133" />
                  <Point X="-21.529470703125" Y="0.114105369568" />
                  <Point X="-21.536318359375" Y="0.092603042603" />
                  <Point X="-21.551625" Y="0.012679788589" />
                  <Point X="-21.5529375" Y="0.00332843852" />
                  <Point X="-21.5561328125" Y="-0.032165313721" />
                  <Point X="-21.5548203125" Y="-0.058552555084" />
                  <Point X="-21.539513671875" Y="-0.138475662231" />
                  <Point X="-21.536318359375" Y="-0.155163192749" />
                  <Point X="-21.529470703125" Y="-0.176665512085" />
                  <Point X="-21.5196953125" Y="-0.198131561279" />
                  <Point X="-21.50797265625" Y="-0.217409393311" />
                  <Point X="-21.4620546875" Y="-0.275920501709" />
                  <Point X="-21.4555078125" Y="-0.283418029785" />
                  <Point X="-21.431609375" Y="-0.308053741455" />
                  <Point X="-21.410962890625" Y="-0.32415536499" />
                  <Point X="-21.334431640625" Y="-0.36839151001" />
                  <Point X="-21.32989453125" Y="-0.370852325439" />
                  <Point X="-21.3018359375" Y="-0.385096496582" />
                  <Point X="-21.283419921875" Y="-0.392149871826" />
                  <Point X="-21.058861328125" Y="-0.452319885254" />
                  <Point X="-20.108525390625" Y="-0.706961914062" />
                  <Point X="-20.13601171875" Y="-0.889268554688" />
                  <Point X="-20.144974609375" Y="-0.948725158691" />
                  <Point X="-20.19882421875" Y="-1.18469921875" />
                  <Point X="-20.328333984375" Y="-1.167648803711" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710144043" />
                  <Point X="-21.62533984375" Y="-1.005508728027" />
                  <Point X="-21.77554296875" Y="-1.03815625" />
                  <Point X="-21.806904296875" Y="-1.04497265625" />
                  <Point X="-21.8360234375" Y="-1.056596801758" />
                  <Point X="-21.8638515625" Y="-1.073489746094" />
                  <Point X="-21.8876015625" Y="-1.093960571289" />
                  <Point X="-21.978390625" Y="-1.203150634766" />
                  <Point X="-21.997345703125" Y="-1.225948608398" />
                  <Point X="-22.012068359375" Y="-1.250334594727" />
                  <Point X="-22.02341015625" Y="-1.277719238281" />
                  <Point X="-22.030240234375" Y="-1.305365722656" />
                  <Point X="-22.043251953125" Y="-1.446771606445" />
                  <Point X="-22.04596875" Y="-1.476296142578" />
                  <Point X="-22.04365234375" Y="-1.507561767578" />
                  <Point X="-22.035921875" Y="-1.539182373047" />
                  <Point X="-22.023548828125" Y="-1.567996582031" />
                  <Point X="-21.940423828125" Y="-1.697291503906" />
                  <Point X="-21.923068359375" Y="-1.724287109375" />
                  <Point X="-21.913064453125" Y="-1.737240966797" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.680978515625" Y="-1.920813110352" />
                  <Point X="-20.786876953125" Y="-2.606882324219" />
                  <Point X="-20.849919921875" Y="-2.708895996094" />
                  <Point X="-20.875203125" Y="-2.749804443359" />
                  <Point X="-20.971017578125" Y="-2.885945068359" />
                  <Point X="-21.088169921875" Y="-2.818306884766" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.245775390625" Y="-2.159825195312" />
                  <Point X="-22.424541015625" Y="-2.127540283203" />
                  <Point X="-22.461865234375" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135177001953" />
                  <Point X="-22.70367578125" Y="-2.213336914062" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.75761328125" Y="-2.246547851562" />
                  <Point X="-22.778572265625" Y="-2.267506347656" />
                  <Point X="-22.795466796875" Y="-2.290437744141" />
                  <Point X="-22.873626953125" Y="-2.438947998047" />
                  <Point X="-22.889947265625" Y="-2.469955810547" />
                  <Point X="-22.899771484375" Y="-2.499735351563" />
                  <Point X="-22.904728515625" Y="-2.531911132812" />
                  <Point X="-22.90432421875" Y="-2.563260253906" />
                  <Point X="-22.8720390625" Y="-2.742025634766" />
                  <Point X="-22.865296875" Y="-2.779350585938" />
                  <Point X="-22.86101171875" Y="-2.795144287109" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.714265625" Y="-3.058020996094" />
                  <Point X="-22.138712890625" Y="-4.054906005859" />
                  <Point X="-22.18816015625" Y="-4.090224121094" />
                  <Point X="-22.21815625" Y="-4.111648925781" />
                  <Point X="-22.298232421875" Y="-4.163481933594" />
                  <Point X="-22.393751953125" Y="-4.038998535156" />
                  <Point X="-23.241451171875" Y="-2.934255371094" />
                  <Point X="-23.25249609375" Y="-2.922177490234" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.45438671875" Y="-2.787205322266" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.520009765625" Y="-2.751167236328" />
                  <Point X="-23.551630859375" Y="-2.743435546875" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.7757265625" Y="-2.758860839844" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397949219" />
                  <Point X="-23.871021484375" Y="-2.780741455078" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-24.044298828125" Y="-2.919262695313" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.09585546875" Y="-2.968858886719" />
                  <Point X="-24.112748046875" Y="-2.996683349609" />
                  <Point X="-24.124375" Y="-3.025807373047" />
                  <Point X="-24.168892578125" Y="-3.230629638672" />
                  <Point X="-24.178189453125" Y="-3.273395263672" />
                  <Point X="-24.180275390625" Y="-3.289626220703" />
                  <Point X="-24.1802578125" Y="-3.323120605469" />
                  <Point X="-24.142306640625" Y="-3.611376708984" />
                  <Point X="-23.977935546875" Y="-4.859915527344" />
                  <Point X="-23.995953125" Y="-4.863865234375" />
                  <Point X="-24.024318359375" Y="-4.870083007812" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.0137421875" Y="-4.761310058594" />
                  <Point X="-26.05842578125" Y="-4.75263671875" />
                  <Point X="-26.141244140625" Y="-4.731328125" />
                  <Point X="-26.1207734375" Y="-4.575838867188" />
                  <Point X="-26.120076171875" Y="-4.568099121094" />
                  <Point X="-26.11944921875" Y="-4.541030273438" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497688964844" />
                  <Point X="-26.173408203125" Y="-4.24595703125" />
                  <Point X="-26.18386328125" Y="-4.193397460938" />
                  <Point X="-26.188126953125" Y="-4.178467773438" />
                  <Point X="-26.199029296875" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135463867188" />
                  <Point X="-26.221740234375" Y="-4.107626464844" />
                  <Point X="-26.23057421875" Y="-4.094861083984" />
                  <Point X="-26.25020703125" Y="-4.070937744141" />
                  <Point X="-26.261005859375" Y="-4.059779541016" />
                  <Point X="-26.453974609375" Y="-3.890548828125" />
                  <Point X="-26.494265625" Y="-3.85521484375" />
                  <Point X="-26.50673828125" Y="-3.845965332031" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.8929296875" Y="-3.779383056641" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.992732421875" Y="-3.779166503906" />
                  <Point X="-27.008009765625" Y="-3.781946533203" />
                  <Point X="-27.03905859375" Y="-3.790266601562" />
                  <Point X="-27.053677734375" Y="-3.795497558594" />
                  <Point X="-27.0818671875" Y="-3.808270507812" />
                  <Point X="-27.0954375" Y="-3.8158125" />
                  <Point X="-27.308845703125" Y="-3.958407714844" />
                  <Point X="-27.35340234375" Y="-3.988180664062" />
                  <Point X="-27.359677734375" Y="-3.992755859375" />
                  <Point X="-27.38044140625" Y="-4.010132080078" />
                  <Point X="-27.402755859375" Y="-4.032770019531" />
                  <Point X="-27.410466796875" Y="-4.041625732422" />
                  <Point X="-27.449314453125" Y="-4.092250244141" />
                  <Point X="-27.503203125" Y="-4.162478515625" />
                  <Point X="-27.682083984375" Y="-4.051719238281" />
                  <Point X="-27.74759375" Y="-4.01115625" />
                  <Point X="-27.980861328125" Y="-3.831547607422" />
                  <Point X="-27.957337890625" Y="-3.790803222656" />
                  <Point X="-27.34148828125" Y="-2.724119628906" />
                  <Point X="-27.3348515625" Y="-2.710086425781" />
                  <Point X="-27.32394921875" Y="-2.681122314453" />
                  <Point X="-27.319685546875" Y="-2.666191894531" />
                  <Point X="-27.3134140625" Y="-2.634667480469" />
                  <Point X="-27.311638671875" Y="-2.619242919922" />
                  <Point X="-27.310625" Y="-2.588310302734" />
                  <Point X="-27.3146640625" Y="-2.557626953125" />
                  <Point X="-27.323646484375" Y="-2.528009765625" />
                  <Point X="-27.3293515625" Y="-2.513567871094" />
                  <Point X="-27.34356640625" Y="-2.484739746094" />
                  <Point X="-27.35155078125" Y="-2.471418945312" />
                  <Point X="-27.369580078125" Y="-2.446258544922" />
                  <Point X="-27.379625" Y="-2.434418945313" />
                  <Point X="-27.3939765625" Y="-2.420066894531" />
                  <Point X="-27.408810546875" Y="-2.407025390625" />
                  <Point X="-27.43396484375" Y="-2.388999023438" />
                  <Point X="-27.44728125" Y="-2.381017578125" />
                  <Point X="-27.476111328125" Y="-2.366798095703" />
                  <Point X="-27.49055078125" Y="-2.361091064453" />
                  <Point X="-27.52016796875" Y="-2.352104736328" />
                  <Point X="-27.55085546875" Y="-2.348062988281" />
                  <Point X="-27.58179296875" Y="-2.349074951172" />
                  <Point X="-27.597220703125" Y="-2.350849365234" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-27.9093984375" Y="-2.507509765625" />
                  <Point X="-28.793087890625" Y="-3.017708251953" />
                  <Point X="-28.952267578125" Y="-2.808578369141" />
                  <Point X="-29.00401953125" Y="-2.740587402344" />
                  <Point X="-29.181265625" Y="-2.443373291016" />
                  <Point X="-29.12566015625" Y="-2.400706298828" />
                  <Point X="-28.048123046875" Y="-1.573882324219" />
                  <Point X="-28.036482421875" Y="-1.563310180664" />
                  <Point X="-28.015107421875" Y="-1.540392578125" />
                  <Point X="-28.005373046875" Y="-1.528047363281" />
                  <Point X="-27.987408203125" Y="-1.500918334961" />
                  <Point X="-27.979841796875" Y="-1.487137084961" />
                  <Point X="-27.967083984375" Y="-1.458509033203" />
                  <Point X="-27.961892578125" Y="-1.443662475586" />
                  <Point X="-27.955517578125" Y="-1.419053344727" />
                  <Point X="-27.955513671875" Y="-1.419039306641" />
                  <Point X="-27.951544921875" Y="-1.398766357422" />
                  <Point X="-27.94874609375" Y="-1.368347290039" />
                  <Point X="-27.948576171875" Y="-1.353026611328" />
                  <Point X="-27.950787109375" Y="-1.321359619141" />
                  <Point X="-27.953083984375" Y="-1.306207275391" />
                  <Point X="-27.9600859375" Y="-1.276463623047" />
                  <Point X="-27.971783203125" Y="-1.248228881836" />
                  <Point X="-27.9878671875" Y="-1.222247070313" />
                  <Point X="-27.996958984375" Y="-1.209908813477" />
                  <Point X="-28.017791015625" Y="-1.185955444336" />
                  <Point X="-28.02875" Y="-1.175239501953" />
                  <Point X="-28.05224609375" Y="-1.155709594727" />
                  <Point X="-28.064783203125" Y="-1.146895263672" />
                  <Point X="-28.08669140625" Y="-1.134000854492" />
                  <Point X="-28.10543359375" Y="-1.124480957031" />
                  <Point X="-28.134697265625" Y="-1.113257202148" />
                  <Point X="-28.14979296875" Y="-1.108861083984" />
                  <Point X="-28.1816796875" Y="-1.102379150391" />
                  <Point X="-28.197294921875" Y="-1.100532714844" />
                  <Point X="-28.228619140625" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-28.525486328125" Y="-1.137211669922" />
                  <Point X="-29.66091796875" Y="-1.286694213867" />
                  <Point X="-29.72051953125" Y="-1.053357910156" />
                  <Point X="-29.740759765625" Y="-0.974113586426" />
                  <Point X="-29.786451171875" Y="-0.654654663086" />
                  <Point X="-29.735763671875" Y="-0.641072937012" />
                  <Point X="-28.508287109375" Y="-0.312171325684" />
                  <Point X="-28.500474609375" Y="-0.309712677002" />
                  <Point X="-28.473931640625" Y="-0.299251556396" />
                  <Point X="-28.444166015625" Y="-0.283893005371" />
                  <Point X="-28.43355859375" Y="-0.277513366699" />
                  <Point X="-28.410599609375" Y="-0.261578613281" />
                  <Point X="-28.39367578125" Y="-0.24824609375" />
                  <Point X="-28.371224609375" Y="-0.226366424561" />
                  <Point X="-28.360908203125" Y="-0.214495620728" />
                  <Point X="-28.341658203125" Y="-0.18823500061" />
                  <Point X="-28.3334375" Y="-0.174815689087" />
                  <Point X="-28.319326171875" Y="-0.146807998657" />
                  <Point X="-28.313435546875" Y="-0.132219467163" />
                  <Point X="-28.305783203125" Y="-0.107560997009" />
                  <Point X="-28.300990234375" Y="-0.088498031616" />
                  <Point X="-28.296720703125" Y="-0.060341125488" />
                  <Point X="-28.295646484375" Y="-0.046098743439" />
                  <Point X="-28.295646484375" Y="-0.016461402893" />
                  <Point X="-28.296720703125" Y="-0.002219167948" />
                  <Point X="-28.300990234375" Y="0.025938035965" />
                  <Point X="-28.304185546875" Y="0.039852855682" />
                  <Point X="-28.311837890625" Y="0.064511177063" />
                  <Point X="-28.319326171875" Y="0.084246665955" />
                  <Point X="-28.33343359375" Y="0.112247810364" />
                  <Point X="-28.3416484375" Y="0.125662071228" />
                  <Point X="-28.360892578125" Y="0.151919433594" />
                  <Point X="-28.3712109375" Y="0.163793212891" />
                  <Point X="-28.393666015625" Y="0.185678970337" />
                  <Point X="-28.4058046875" Y="0.195691101074" />
                  <Point X="-28.428763671875" Y="0.21162600708" />
                  <Point X="-28.449712890625" Y="0.224249145508" />
                  <Point X="-28.470076171875" Y="0.234796615601" />
                  <Point X="-28.479427734375" Y="0.239016464233" />
                  <Point X="-28.498533203125" Y="0.246423828125" />
                  <Point X="-28.508287109375" Y="0.249611038208" />
                  <Point X="-28.76457421875" Y="0.318283447266" />
                  <Point X="-29.7854453125" Y="0.591824890137" />
                  <Point X="-29.744373046875" Y="0.869390808105" />
                  <Point X="-29.73133203125" Y="0.957521606445" />
                  <Point X="-29.633583984375" Y="1.318236938477" />
                  <Point X="-28.778064453125" Y="1.20560559082" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.747056640625" Y="1.204364257812" />
                  <Point X="-28.73670703125" Y="1.20470300293" />
                  <Point X="-28.7151484375" Y="1.206588745117" />
                  <Point X="-28.704892578125" Y="1.208052734375" />
                  <Point X="-28.684603515625" Y="1.212088623047" />
                  <Point X="-28.674568359375" Y="1.21466027832" />
                  <Point X="-28.62375390625" Y="1.230682495117" />
                  <Point X="-28.60344921875" Y="1.237676391602" />
                  <Point X="-28.584517578125" Y="1.246006713867" />
                  <Point X="-28.575279296875" Y="1.250688232422" />
                  <Point X="-28.55653515625" Y="1.261510009766" />
                  <Point X="-28.547861328125" Y="1.267169677734" />
                  <Point X="-28.531181640625" Y="1.279399169922" />
                  <Point X="-28.515931640625" Y="1.293374023438" />
                  <Point X="-28.502294921875" Y="1.308923706055" />
                  <Point X="-28.49590234375" Y="1.317068481445" />
                  <Point X="-28.483486328125" Y="1.334798339844" />
                  <Point X="-28.478013671875" Y="1.343596923828" />
                  <Point X="-28.4680625" Y="1.361734863281" />
                  <Point X="-28.463583984375" Y="1.37107421875" />
                  <Point X="-28.443193359375" Y="1.420300537109" />
                  <Point X="-28.43549609375" Y="1.440356079102" />
                  <Point X="-28.429705078125" Y="1.460223876953" />
                  <Point X="-28.4273515625" Y="1.470319946289" />
                  <Point X="-28.423595703125" Y="1.491631835938" />
                  <Point X="-28.422357421875" Y="1.501920532227" />
                  <Point X="-28.4210078125" Y="1.522557983398" />
                  <Point X="-28.42191015625" Y="1.543210449219" />
                  <Point X="-28.425056640625" Y="1.563651367188" />
                  <Point X="-28.427189453125" Y="1.573788330078" />
                  <Point X="-28.432791015625" Y="1.59469140625" />
                  <Point X="-28.436009765625" Y="1.604528808594" />
                  <Point X="-28.44350390625" Y="1.623802490234" />
                  <Point X="-28.447779296875" Y="1.633239257813" />
                  <Point X="-28.472380859375" Y="1.680500854492" />
                  <Point X="-28.472390625" Y="1.680518188477" />
                  <Point X="-28.48281640625" Y="1.69931652832" />
                  <Point X="-28.49430859375" Y="1.716510498047" />
                  <Point X="-28.5005234375" Y="1.724792480469" />
                  <Point X="-28.51443359375" Y="1.741366455078" />
                  <Point X="-28.52151171875" Y="1.748922973633" />
                  <Point X="-28.536447265625" Y="1.76321875" />
                  <Point X="-28.5443046875" Y="1.769958129883" />
                  <Point X="-28.691310546875" Y="1.882761230469" />
                  <Point X="-29.22761328125" Y="2.294280517578" />
                  <Point X="-29.052966796875" Y="2.593491943359" />
                  <Point X="-29.002283203125" Y="2.680322265625" />
                  <Point X="-28.726337890625" Y="3.035013427734" />
                  <Point X="-28.2541640625" Y="2.762403076172" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.084298828125" Y="2.724966064453" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310546875" />
                  <Point X="-27.976431640625" Y="2.734227294922" />
                  <Point X="-27.95698828125" Y="2.7413046875" />
                  <Point X="-27.9384375" Y="2.750454345703" />
                  <Point X="-27.929416015625" Y="2.755533691406" />
                  <Point X="-27.911162109375" Y="2.767163330078" />
                  <Point X="-27.902744140625" Y="2.773195068359" />
                  <Point X="-27.886615234375" Y="2.786139892578" />
                  <Point X="-27.878904296875" Y="2.793052978516" />
                  <Point X="-27.82866796875" Y="2.843287841797" />
                  <Point X="-27.811265625" Y="2.86148828125" />
                  <Point X="-27.79831640625" Y="2.877622070312" />
                  <Point X="-27.79228125" Y="2.886043945312" />
                  <Point X="-27.78065234375" Y="2.904298339844" />
                  <Point X="-27.7755703125" Y="2.913326171875" />
                  <Point X="-27.766423828125" Y="2.931875" />
                  <Point X="-27.759353515625" Y="2.951299560547" />
                  <Point X="-27.754435546875" Y="2.971388427734" />
                  <Point X="-27.7525234375" Y="2.981573730469" />
                  <Point X="-27.749697265625" Y="3.003032714844" />
                  <Point X="-27.748908203125" Y="3.013365478516" />
                  <Point X="-27.74845703125" Y="3.034049316406" />
                  <Point X="-27.748794921875" Y="3.044400390625" />
                  <Point X="-27.754986328125" Y="3.115173095703" />
                  <Point X="-27.757744140625" Y="3.140206298828" />
                  <Point X="-27.76178125" Y="3.1605" />
                  <Point X="-27.764353515625" Y="3.170537353516" />
                  <Point X="-27.77086328125" Y="3.191178955078" />
                  <Point X="-27.774513671875" Y="3.200873779297" />
                  <Point X="-27.78284375" Y="3.219801269531" />
                  <Point X="-27.7875234375" Y="3.229033935547" />
                  <Point X="-27.85266796875" Y="3.341865722656" />
                  <Point X="-28.05938671875" Y="3.699914794922" />
                  <Point X="-27.736625" Y="3.947372558594" />
                  <Point X="-27.648365234375" Y="4.015041992188" />
                  <Point X="-27.1925234375" Y="4.268296875" />
                  <Point X="-27.11856640625" Y="4.171913085938" />
                  <Point X="-27.111828125" Y="4.164057128906" />
                  <Point X="-27.0975234375" Y="4.149111816406" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.06508984375" Y="4.121895996094" />
                  <Point X="-27.047888671875" Y="4.110403320312" />
                  <Point X="-27.0389765625" Y="4.105128417969" />
                  <Point X="-26.96020703125" Y="4.064123535156" />
                  <Point X="-26.934326171875" Y="4.051286865234" />
                  <Point X="-26.915048828125" Y="4.043790283203" />
                  <Point X="-26.905205078125" Y="4.040568359375" />
                  <Point X="-26.884298828125" Y="4.034966308594" />
                  <Point X="-26.8741640625" Y="4.032834960938" />
                  <Point X="-26.853720703125" Y="4.029687988281" />
                  <Point X="-26.8330546875" Y="4.028785400391" />
                  <Point X="-26.812416015625" Y="4.030137939453" />
                  <Point X="-26.802134765625" Y="4.031377685547" />
                  <Point X="-26.780818359375" Y="4.035135986328" />
                  <Point X="-26.77073046875" Y="4.037487548828" />
                  <Point X="-26.750869140625" Y="4.043276123047" />
                  <Point X="-26.741095703125" Y="4.046713134766" />
                  <Point X="-26.659052734375" Y="4.080697021484" />
                  <Point X="-26.632580078125" Y="4.092274169922" />
                  <Point X="-26.6144453125" Y="4.102224121094" />
                  <Point X="-26.60565234375" Y="4.107692871094" />
                  <Point X="-26.587923828125" Y="4.120106933594" />
                  <Point X="-26.579779296875" Y="4.126498535156" />
                  <Point X="-26.564228515625" Y="4.140135253906" />
                  <Point X="-26.55025390625" Y="4.155384277344" />
                  <Point X="-26.5380234375" Y="4.172062988281" />
                  <Point X="-26.532361328125" Y="4.180737792969" />
                  <Point X="-26.5215390625" Y="4.199480957031" />
                  <Point X="-26.51685546875" Y="4.208723632812" />
                  <Point X="-26.5085234375" Y="4.22766015625" />
                  <Point X="-26.504875" Y="4.237354003906" />
                  <Point X="-26.478171875" Y="4.322047363281" />
                  <Point X="-26.470025390625" Y="4.349763183594" />
                  <Point X="-26.465990234375" Y="4.370049316406" />
                  <Point X="-26.464525390625" Y="4.380302734375" />
                  <Point X="-26.462638671875" Y="4.401866210938" />
                  <Point X="-26.46230078125" Y="4.41221875" />
                  <Point X="-26.462751953125" Y="4.432899414062" />
                  <Point X="-26.463541015625" Y="4.443227539062" />
                  <Point X="-26.470947265625" Y="4.499482421875" />
                  <Point X="-26.479263671875" Y="4.562655273438" />
                  <Point X="-26.045421875" Y="4.684290039062" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.728640625" Y="4.386299316406" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.271890625" Y="4.748360351562" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.64642578125" Y="4.610992675781" />
                  <Point X="-23.546404296875" Y="4.586844238281" />
                  <Point X="-23.205451171875" Y="4.463178710938" />
                  <Point X="-23.141744140625" Y="4.440070800781" />
                  <Point X="-22.8108671875" Y="4.28533203125" />
                  <Point X="-22.749546875" Y="4.256653320312" />
                  <Point X="-22.429859375" Y="4.070404052734" />
                  <Point X="-22.3705703125" Y="4.035861572266" />
                  <Point X="-22.18221875" Y="3.901915771484" />
                  <Point X="-22.221888671875" Y="3.833204833984" />
                  <Point X="-22.9346875" Y="2.598598388672" />
                  <Point X="-22.9376171875" Y="2.593117675781" />
                  <Point X="-22.9468125" Y="2.57344921875" />
                  <Point X="-22.955814453125" Y="2.5495703125" />
                  <Point X="-22.958697265625" Y="2.540601806641" />
                  <Point X="-22.976458984375" Y="2.474183349609" />
                  <Point X="-22.980693359375" Y="2.454019287109" />
                  <Point X="-22.985740234375" Y="2.42125" />
                  <Point X="-22.986833984375" Y="2.40834765625" />
                  <Point X="-22.9872578125" Y="2.382513427734" />
                  <Point X="-22.986587890625" Y="2.369581542969" />
                  <Point X="-22.979662109375" Y="2.3121484375" />
                  <Point X="-22.976201171875" Y="2.289042724609" />
                  <Point X="-22.970861328125" Y="2.267125244141" />
                  <Point X="-22.967537109375" Y="2.256321777344" />
                  <Point X="-22.959271484375" Y="2.23423046875" />
                  <Point X="-22.95468359375" Y="2.223890625" />
                  <Point X="-22.94431640625" Y="2.203839111328" />
                  <Point X="-22.938537109375" Y="2.194127441406" />
                  <Point X="-22.902998046875" Y="2.14175390625" />
                  <Point X="-22.890193359375" Y="2.124970214844" />
                  <Point X="-22.86947265625" Y="2.100734375" />
                  <Point X="-22.86060546875" Y="2.091665771484" />
                  <Point X="-22.841740234375" Y="2.0747890625" />
                  <Point X="-22.8317421875" Y="2.066980957031" />
                  <Point X="-22.779369140625" Y="2.031443481445" />
                  <Point X="-22.75871875" Y="2.018244262695" />
                  <Point X="-22.738671875" Y="2.007880737305" />
                  <Point X="-22.72833984375" Y="2.003296508789" />
                  <Point X="-22.706251953125" Y="1.99503112793" />
                  <Point X="-22.69544921875" Y="1.991706665039" />
                  <Point X="-22.67352734375" Y="1.986364501953" />
                  <Point X="-22.662408203125" Y="1.984346557617" />
                  <Point X="-22.604974609375" Y="1.977421020508" />
                  <Point X="-22.582892578125" Y="1.976055786133" />
                  <Point X="-22.551998046875" Y="1.975947631836" />
                  <Point X="-22.539453125" Y="1.976735229492" />
                  <Point X="-22.514580078125" Y="1.979959472656" />
                  <Point X="-22.502251953125" Y="1.982395751953" />
                  <Point X="-22.43583203125" Y="2.000156982422" />
                  <Point X="-22.42445703125" Y="2.003583374023" />
                  <Point X="-22.3985078125" Y="2.01229296875" />
                  <Point X="-22.389677734375" Y="2.015755737305" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-22.1061875" Y="2.176699951172" />
                  <Point X="-21.059595703125" Y="2.780950195312" />
                  <Point X="-20.99122265625" Y="2.685926513672" />
                  <Point X="-20.95603515625" Y="2.637025878906" />
                  <Point X="-20.86311328125" Y="2.483471191406" />
                  <Point X="-20.888578125" Y="2.463931884766" />
                  <Point X="-21.827046875" Y="1.743819946289" />
                  <Point X="-21.831857421875" Y="1.739871337891" />
                  <Point X="-21.847875" Y="1.725221679688" />
                  <Point X="-21.865330078125" Y="1.706604736328" />
                  <Point X="-21.87142578125" Y="1.699421020508" />
                  <Point X="-21.9192265625" Y="1.637060424805" />
                  <Point X="-21.9302265625" Y="1.621077270508" />
                  <Point X="-21.94867578125" Y="1.591108276367" />
                  <Point X="-21.95481640625" Y="1.579375610352" />
                  <Point X="-21.965408203125" Y="1.555159790039" />
                  <Point X="-21.969859375" Y="1.542676635742" />
                  <Point X="-21.987666015625" Y="1.479005981445" />
                  <Point X="-21.993775390625" Y="1.454663452148" />
                  <Point X="-21.997228515625" Y="1.432364624023" />
                  <Point X="-21.998291015625" Y="1.421114257812" />
                  <Point X="-21.999107421875" Y="1.397538208008" />
                  <Point X="-21.998826171875" Y="1.386241699219" />
                  <Point X="-21.996923828125" Y="1.363756469727" />
                  <Point X="-21.995302734375" Y="1.352567749023" />
                  <Point X="-21.980685546875" Y="1.281726074219" />
                  <Point X="-21.975634765625" Y="1.262433349609" />
                  <Point X="-21.965384765625" Y="1.230267700195" />
                  <Point X="-21.960609375" Y="1.218202636719" />
                  <Point X="-21.949458984375" Y="1.194832641602" />
                  <Point X="-21.943083984375" Y="1.183527832031" />
                  <Point X="-21.903328125" Y="1.123099365234" />
                  <Point X="-21.88826171875" Y="1.101423339844" />
                  <Point X="-21.87370703125" Y="1.084178222656" />
                  <Point X="-21.86591796875" Y="1.07599206543" />
                  <Point X="-21.848673828125" Y="1.059901855469" />
                  <Point X="-21.83996484375" Y="1.05269519043" />
                  <Point X="-21.82175390625" Y="1.039369018555" />
                  <Point X="-21.812251953125" Y="1.033249511719" />
                  <Point X="-21.754638671875" Y="1.000818359375" />
                  <Point X="-21.735837890625" Y="0.991561096191" />
                  <Point X="-21.70583203125" Y="0.978791870117" />
                  <Point X="-21.693685546875" Y="0.974568908691" />
                  <Point X="-21.66893359375" Y="0.967801635742" />
                  <Point X="-21.656328125" Y="0.965257507324" />
                  <Point X="-21.578431640625" Y="0.954962402344" />
                  <Point X="-21.567439453125" Y="0.953833740234" />
                  <Point X="-21.53809765625" Y="0.951680969238" />
                  <Point X="-21.528408203125" Y="0.951465881348" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-21.254525390625" Y="0.98503503418" />
                  <Point X="-20.295296875" Y="1.111319824219" />
                  <Point X="-20.262423828125" Y="0.976285949707" />
                  <Point X="-20.247310546875" Y="0.914204956055" />
                  <Point X="-20.216126953125" Y="0.713920959473" />
                  <Point X="-20.232501953125" Y="0.709533203125" />
                  <Point X="-21.3080078125" Y="0.42135269165" />
                  <Point X="-21.313970703125" Y="0.419543487549" />
                  <Point X="-21.33437890625" Y="0.41213494873" />
                  <Point X="-21.357619140625" Y="0.401617950439" />
                  <Point X="-21.365994140625" Y="0.397316345215" />
                  <Point X="-21.442525390625" Y="0.353080230713" />
                  <Point X="-21.457947265625" Y="0.343143157959" />
                  <Point X="-21.488236328125" Y="0.321487762451" />
                  <Point X="-21.498640625" Y="0.312867340088" />
                  <Point X="-21.518123046875" Y="0.294236846924" />
                  <Point X="-21.527201171875" Y="0.284226806641" />
                  <Point X="-21.573119140625" Y="0.225715896606" />
                  <Point X="-21.589142578125" Y="0.204208236694" />
                  <Point X="-21.600865234375" Y="0.184930206299" />
                  <Point X="-21.60615234375" Y="0.17494303894" />
                  <Point X="-21.615927734375" Y="0.153476989746" />
                  <Point X="-21.6199921875" Y="0.142932647705" />
                  <Point X="-21.62683984375" Y="0.121430328369" />
                  <Point X="-21.629623046875" Y="0.110472366333" />
                  <Point X="-21.6449296875" Y="0.030549060822" />
                  <Point X="-21.6475546875" Y="0.011846354485" />
                  <Point X="-21.65075" Y="-0.023647468567" />
                  <Point X="-21.651015625" Y="-0.036884716034" />
                  <Point X="-21.649703125" Y="-0.063271968842" />
                  <Point X="-21.648125" Y="-0.076421974182" />
                  <Point X="-21.632818359375" Y="-0.156344985962" />
                  <Point X="-21.62683984375" Y="-0.183990478516" />
                  <Point X="-21.6199921875" Y="-0.205492797852" />
                  <Point X="-21.615927734375" Y="-0.216037139893" />
                  <Point X="-21.60615234375" Y="-0.237503189087" />
                  <Point X="-21.600865234375" Y="-0.247490646362" />
                  <Point X="-21.589142578125" Y="-0.26676852417" />
                  <Point X="-21.58270703125" Y="-0.276058929443" />
                  <Point X="-21.5367890625" Y="-0.334570007324" />
                  <Point X="-21.5236953125" Y="-0.349565155029" />
                  <Point X="-21.499796875" Y="-0.374200744629" />
                  <Point X="-21.49003125" Y="-0.382966064453" />
                  <Point X="-21.469384765625" Y="-0.399067718506" />
                  <Point X="-21.45850390625" Y="-0.406404052734" />
                  <Point X="-21.38197265625" Y="-0.450640319824" />
                  <Point X="-21.3728984375" Y="-0.455561828613" />
                  <Point X="-21.34483984375" Y="-0.469805999756" />
                  <Point X="-21.335814453125" Y="-0.473812255859" />
                  <Point X="-21.3080078125" Y="-0.483912841797" />
                  <Point X="-21.08344921875" Y="-0.544082946777" />
                  <Point X="-20.21512109375" Y="-0.776751220703" />
                  <Point X="-20.22994921875" Y="-0.875105407715" />
                  <Point X="-20.238380859375" Y="-0.931038635254" />
                  <Point X="-20.2721953125" Y="-1.079219726563" />
                  <Point X="-20.31593359375" Y="-1.073461547852" />
                  <Point X="-21.563216796875" Y="-0.909253601074" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042297363" />
                  <Point X="-21.63327734375" Y="-0.910840881348" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.795720703125" Y="-0.945323791504" />
                  <Point X="-21.82708203125" Y="-0.952140197754" />
                  <Point X="-21.842125" Y="-0.956742736816" />
                  <Point X="-21.871244140625" Y="-0.968367004395" />
                  <Point X="-21.8853203125" Y="-0.975388549805" />
                  <Point X="-21.9131484375" Y="-0.992281494141" />
                  <Point X="-21.925875" Y="-1.001530883789" />
                  <Point X="-21.949625" Y="-1.022001586914" />
                  <Point X="-21.9606484375" Y="-1.033223266602" />
                  <Point X="-22.0514375" Y="-1.142413208008" />
                  <Point X="-22.070392578125" Y="-1.165211181641" />
                  <Point X="-22.078673828125" Y="-1.176848388672" />
                  <Point X="-22.093396484375" Y="-1.201234375" />
                  <Point X="-22.099837890625" Y="-1.213983154297" />
                  <Point X="-22.1111796875" Y="-1.241367797852" />
                  <Point X="-22.11563671875" Y="-1.254934448242" />
                  <Point X="-22.122466796875" Y="-1.282580932617" />
                  <Point X="-22.12483984375" Y="-1.296660888672" />
                  <Point X="-22.1378515625" Y="-1.438066772461" />
                  <Point X="-22.140568359375" Y="-1.467591308594" />
                  <Point X="-22.140708984375" Y="-1.483315307617" />
                  <Point X="-22.138392578125" Y="-1.514580932617" />
                  <Point X="-22.135935546875" Y="-1.530122436523" />
                  <Point X="-22.128205078125" Y="-1.561743164063" />
                  <Point X="-22.12321484375" Y="-1.576666381836" />
                  <Point X="-22.110841796875" Y="-1.60548059082" />
                  <Point X="-22.103458984375" Y="-1.619371582031" />
                  <Point X="-22.020333984375" Y="-1.748666503906" />
                  <Point X="-22.002978515625" Y="-1.775662109375" />
                  <Point X="-21.9982578125" Y="-1.782353027344" />
                  <Point X="-21.980201171875" Y="-1.804454711914" />
                  <Point X="-21.956505859375" Y="-1.828122802734" />
                  <Point X="-21.947201171875" Y="-1.836277587891" />
                  <Point X="-21.738810546875" Y="-1.996181640625" />
                  <Point X="-20.912828125" Y="-2.62998046875" />
                  <Point X="-20.930734375" Y="-2.658954345703" />
                  <Point X="-20.9545390625" Y="-2.697470947266" />
                  <Point X="-20.998724609375" Y="-2.760252197266" />
                  <Point X="-21.040669921875" Y="-2.736034667969" />
                  <Point X="-22.151544921875" Y="-2.094670898438" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.18497265625" Y="-2.079513427734" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228892578125" Y="-2.066337646484" />
                  <Point X="-22.407658203125" Y="-2.034052612305" />
                  <Point X="-22.444982421875" Y="-2.027311889648" />
                  <Point X="-22.460640625" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.55515625" Y="-2.03513659668" />
                  <Point X="-22.584931640625" Y="-2.044960571289" />
                  <Point X="-22.59941015625" Y="-2.051109130859" />
                  <Point X="-22.747919921875" Y="-2.129269042969" />
                  <Point X="-22.778927734375" Y="-2.145588134766" />
                  <Point X="-22.791029296875" Y="-2.153169677734" />
                  <Point X="-22.813958984375" Y="-2.170061523438" />
                  <Point X="-22.824787109375" Y="-2.179371826172" />
                  <Point X="-22.84574609375" Y="-2.200330322266" />
                  <Point X="-22.855056640625" Y="-2.211157470703" />
                  <Point X="-22.871951171875" Y="-2.234088867188" />
                  <Point X="-22.87953515625" Y="-2.246193115234" />
                  <Point X="-22.9576953125" Y="-2.394703369141" />
                  <Point X="-22.974015625" Y="-2.425711181641" />
                  <Point X="-22.9801640625" Y="-2.440193115234" />
                  <Point X="-22.98998828125" Y="-2.46997265625" />
                  <Point X="-22.9936640625" Y="-2.485270263672" />
                  <Point X="-22.99862109375" Y="-2.517446044922" />
                  <Point X="-22.999720703125" Y="-2.533136230469" />
                  <Point X="-22.99931640625" Y="-2.564485351562" />
                  <Point X="-22.9978125" Y="-2.580144287109" />
                  <Point X="-22.96552734375" Y="-2.758909667969" />
                  <Point X="-22.95878515625" Y="-2.796234619141" />
                  <Point X="-22.956982421875" Y="-2.8042265625" />
                  <Point X="-22.948759765625" Y="-2.831549072266" />
                  <Point X="-22.93592578125" Y="-2.862483398438" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.796537109375" Y="-3.105520996094" />
                  <Point X="-22.264103515625" Y="-4.027722167969" />
                  <Point X="-22.276244140625" Y="-4.036082763672" />
                  <Point X="-22.3183828125" Y="-3.981166259766" />
                  <Point X="-23.16608203125" Y="-2.876423095703" />
                  <Point X="-23.171345703125" Y="-2.870145019531" />
                  <Point X="-23.191169921875" Y="-2.849624023438" />
                  <Point X="-23.216748046875" Y="-2.828003662109" />
                  <Point X="-23.22669921875" Y="-2.820646728516" />
                  <Point X="-23.40301171875" Y="-2.707294921875" />
                  <Point X="-23.43982421875" Y="-2.683627929688" />
                  <Point X="-23.453716796875" Y="-2.676245605469" />
                  <Point X="-23.48252734375" Y="-2.663874511719" />
                  <Point X="-23.4974453125" Y="-2.658885742188" />
                  <Point X="-23.52906640625" Y="-2.651154052734" />
                  <Point X="-23.54460546875" Y="-2.648695800781" />
                  <Point X="-23.575875" Y="-2.646376953125" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.784431640625" Y="-2.664260498047" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.838775390625" Y="-2.670339355469" />
                  <Point X="-23.86642578125" Y="-2.677171630859" />
                  <Point X="-23.8799921875" Y="-2.681629882812" />
                  <Point X="-23.907376953125" Y="-2.692973388672" />
                  <Point X="-23.920123046875" Y="-2.6994140625" />
                  <Point X="-23.94450390625" Y="-2.714133789062" />
                  <Point X="-23.956138671875" Y="-2.722412841797" />
                  <Point X="-24.10503515625" Y="-2.846214355469" />
                  <Point X="-24.136123046875" Y="-2.872063232422" />
                  <Point X="-24.147345703125" Y="-2.883087646484" />
                  <Point X="-24.167814453125" Y="-2.906834960938" />
                  <Point X="-24.177060546875" Y="-2.919557861328" />
                  <Point X="-24.193953125" Y="-2.947382324219" />
                  <Point X="-24.2009765625" Y="-2.961460449219" />
                  <Point X="-24.212603515625" Y="-2.990584472656" />
                  <Point X="-24.21720703125" Y="-3.005630371094" />
                  <Point X="-24.261724609375" Y="-3.210452636719" />
                  <Point X="-24.271021484375" Y="-3.253218261719" />
                  <Point X="-24.2724140625" Y="-3.261285888672" />
                  <Point X="-24.275275390625" Y="-3.289676025391" />
                  <Point X="-24.2752578125" Y="-3.323170410156" />
                  <Point X="-24.2744453125" Y="-3.335520996094" />
                  <Point X="-24.236494140625" Y="-3.623777099609" />
                  <Point X="-24.16691015625" Y="-4.152321289062" />
                  <Point X="-24.344931640625" Y="-3.4879375" />
                  <Point X="-24.347392578125" Y="-3.480120117188" />
                  <Point X="-24.357853515625" Y="-3.453581542969" />
                  <Point X="-24.3732109375" Y="-3.423816894531" />
                  <Point X="-24.37958984375" Y="-3.413209716797" />
                  <Point X="-24.515037109375" Y="-3.218055908203" />
                  <Point X="-24.543318359375" Y="-3.177309326172" />
                  <Point X="-24.553328125" Y="-3.165172607422" />
                  <Point X="-24.575212890625" Y="-3.142716308594" />
                  <Point X="-24.587087890625" Y="-3.132396728516" />
                  <Point X="-24.61334375" Y="-3.113153076172" />
                  <Point X="-24.626759765625" Y="-3.104936035156" />
                  <Point X="-24.654759765625" Y="-3.090828857422" />
                  <Point X="-24.66934375" Y="-3.084938720703" />
                  <Point X="-24.878939453125" Y="-3.019887451172" />
                  <Point X="-24.922703125" Y="-3.006305419922" />
                  <Point X="-24.936623046875" Y="-3.003109375" />
                  <Point X="-24.964783203125" Y="-2.99883984375" />
                  <Point X="-24.9790234375" Y="-2.997766357422" />
                  <Point X="-25.0086640625" Y="-2.997766357422" />
                  <Point X="-25.022904296875" Y="-2.998839599609" />
                  <Point X="-25.051064453125" Y="-3.003108886719" />
                  <Point X="-25.064984375" Y="-3.006304931641" />
                  <Point X="-25.274580078125" Y="-3.071355957031" />
                  <Point X="-25.31834375" Y="-3.084938232422" />
                  <Point X="-25.332931640625" Y="-3.090830078125" />
                  <Point X="-25.36093359375" Y="-3.104939453125" />
                  <Point X="-25.37434765625" Y="-3.113156982422" />
                  <Point X="-25.400603515625" Y="-3.132402099609" />
                  <Point X="-25.4124765625" Y="-3.142719726562" />
                  <Point X="-25.434359375" Y="-3.165174072266" />
                  <Point X="-25.444369140625" Y="-3.177310791016" />
                  <Point X="-25.57981640625" Y="-3.372464355469" />
                  <Point X="-25.608095703125" Y="-3.413211181641" />
                  <Point X="-25.61247265625" Y="-3.420137451172" />
                  <Point X="-25.625970703125" Y="-3.445256591797" />
                  <Point X="-25.638775390625" Y="-3.476210205078" />
                  <Point X="-25.64275390625" Y="-3.487937011719" />
                  <Point X="-25.7119453125" Y="-3.7461640625" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.989248464096" Y="4.700038963015" />
                  <Point X="-26.469217395427" Y="4.48634302673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.133048294999" Y="4.190786468046" />
                  <Point X="-27.815377417001" Y="3.886993970109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.643491860596" Y="4.74998927457" />
                  <Point X="-26.464148577656" Y="4.384609363344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.060740025226" Y="4.118989737476" />
                  <Point X="-28.047651378485" Y="3.679588493149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.362190947354" Y="4.771242063891" />
                  <Point X="-26.495656980132" Y="4.266590472275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.955570417633" Y="4.061823817141" />
                  <Point X="-27.999889915008" Y="3.596862820283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.337296815218" Y="4.678335199156" />
                  <Point X="-26.583761637399" Y="4.123373305093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.782685146588" Y="4.03480685262" />
                  <Point X="-27.952128451532" Y="3.514137147416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.312402683082" Y="4.585428334421" />
                  <Point X="-27.904366988056" Y="3.43141147455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.287508550945" Y="4.492521469685" />
                  <Point X="-27.856605524579" Y="3.348685801684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.65440474321" Y="2.993482704433" />
                  <Point X="-28.813891774021" Y="2.922474503381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.439818231125" Y="4.76594706987" />
                  <Point X="-24.652257495578" Y="4.671363015449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.262614418809" Y="4.39961460495" />
                  <Point X="-27.808843324253" Y="3.265960456885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.552710587642" Y="2.934769413162" />
                  <Point X="-28.937669455919" Y="2.763374682341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.250729630847" Y="4.74614429232" />
                  <Point X="-24.683896349045" Y="4.553286043857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.237720286673" Y="4.306707740215" />
                  <Point X="-27.767462060482" Y="3.180394136092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.451016432074" Y="2.876056121891" />
                  <Point X="-29.04148483948" Y="2.613162649146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.083635573762" Y="4.716548913221" />
                  <Point X="-24.715535202513" Y="4.435209072265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.206407710801" Y="4.216658550746" />
                  <Point X="-27.752189529402" Y="3.083203458568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.349322276506" Y="2.81734283062" />
                  <Point X="-29.1234959814" Y="2.47265848979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.932191757354" Y="4.679985598041" />
                  <Point X="-24.747173906241" Y="4.317132167342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.145900770193" Y="4.139607529907" />
                  <Point X="-27.753037496667" Y="2.978835472759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.247143845044" Y="2.758845152869" />
                  <Point X="-29.205506612006" Y="2.332154558086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.780747940945" Y="4.64342228286" />
                  <Point X="-24.797257400254" Y="4.190843112688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.027856469527" Y="4.088173792242" />
                  <Point X="-27.832481760669" Y="2.839474161084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.088788851621" Y="2.725358891945" />
                  <Point X="-29.164969591566" Y="2.246212355944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.629304148368" Y="4.606858957069" />
                  <Point X="-29.079208176501" Y="2.180405351566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.488149126404" Y="4.565714775465" />
                  <Point X="-28.993446761436" Y="2.114598347188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.35943757311" Y="4.519030404664" />
                  <Point X="-28.90768534637" Y="2.048791342809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.230726019817" Y="4.472346033863" />
                  <Point X="-28.821923931305" Y="1.982984338431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.106583405157" Y="4.423627440521" />
                  <Point X="-28.73616251624" Y="1.917177334053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.992670116225" Y="4.370354457933" />
                  <Point X="-28.650401471266" Y="1.851370164899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.878756827293" Y="4.317081475345" />
                  <Point X="-28.56464083205" Y="1.78556281509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.618679222538" Y="1.316274688229" />
                  <Point X="-29.636233662331" Y="1.308458948078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.764844767785" Y="4.263807945383" />
                  <Point X="-28.492441770823" Y="1.713717461743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.438416048902" Y="1.292542577578" />
                  <Point X="-29.668279753557" Y="1.190200662554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.661959375715" Y="4.205625026773" />
                  <Point X="-28.446411800028" Y="1.630220878666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.258152875266" Y="1.268810466927" />
                  <Point X="-29.700325844783" Y="1.071942377031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.560784286946" Y="4.146680632074" />
                  <Point X="-28.421649956883" Y="1.537255115078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.07788970163" Y="1.245078356277" />
                  <Point X="-29.731866588124" Y="0.953909086881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.459609198176" Y="4.087736237375" />
                  <Point X="-28.441632293157" Y="1.424367959311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.897626527994" Y="1.221346245626" />
                  <Point X="-29.748339679655" Y="0.842584347537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.359783933313" Y="4.028190862352" />
                  <Point X="-28.528829114746" Y="1.281554986614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.685054703937" Y="1.211998872927" />
                  <Point X="-29.764812734517" Y="0.73125962452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.269856087952" Y="3.964238872257" />
                  <Point X="-29.781285789378" Y="0.619934901503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.184277042946" Y="3.898350671496" />
                  <Point X="-29.676450920331" Y="0.562619945966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.265088289684" Y="3.758380739895" />
                  <Point X="-29.530638115384" Y="0.523549542955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.345899882176" Y="3.618410654354" />
                  <Point X="-29.384825310436" Y="0.484479139945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.426711474667" Y="3.478440568814" />
                  <Point X="-29.239012505489" Y="0.445408736935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.507523067159" Y="3.338470483273" />
                  <Point X="-29.093199700542" Y="0.406338333925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.58833465965" Y="3.198500397732" />
                  <Point X="-28.947386895595" Y="0.367267930914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.669146252142" Y="3.058530312191" />
                  <Point X="-28.801574090647" Y="0.328197527904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.749957844633" Y="2.918560226651" />
                  <Point X="-28.655761595739" Y="0.289126986856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.830769437125" Y="2.77859014111" />
                  <Point X="-28.509949206253" Y="0.25005639887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.911581029617" Y="2.638620055569" />
                  <Point X="-28.403240187216" Y="0.193575868668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.966878250482" Y="2.510009700164" />
                  <Point X="-28.337484060003" Y="0.11886193628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.987019299266" Y="2.397051881035" />
                  <Point X="-28.302069726845" Y="0.030638966815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.97744183888" Y="2.297325594673" />
                  <Point X="-28.298449073001" Y="-0.071739460692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.337520787902" Y="-0.534363994259" />
                  <Point X="-29.775749656091" Y="-0.729476057107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.946100258923" Y="2.207289318655" />
                  <Point X="-28.348984806387" Y="-0.198229865287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.750929705593" Y="-0.377187264326" />
                  <Point X="-29.761766604011" Y="-0.82724084767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.892027106172" Y="2.127373790906" />
                  <Point X="-29.747783551931" Y="-0.925005638234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.81683079042" Y="2.056862901251" />
                  <Point X="-29.728890416827" Y="-1.020584318989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.034687807864" Y="2.746333631948" />
                  <Point X="-21.405536231399" Y="2.581221275888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.714884993816" Y="1.998261647788" />
                  <Point X="-29.705040781818" Y="-1.113956223807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.97801713843" Y="2.667574593137" />
                  <Point X="-22.192617797721" Y="2.126799538427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.525648567644" Y="1.978524686566" />
                  <Point X="-29.681190662946" Y="-1.207327913195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.925690763912" Y="2.586881349613" />
                  <Point X="-29.611175139983" Y="-1.280145440413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.876118214568" Y="2.504962024126" />
                  <Point X="-29.279547646367" Y="-1.236485813876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.101270711389" Y="2.300727227515" />
                  <Point X="-28.94792015275" Y="-1.192826187339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.424124337477" Y="2.052993085567" />
                  <Point X="-28.616292659133" Y="-1.149166560802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.746977963566" Y="1.805258943618" />
                  <Point X="-28.284665372506" Y="-1.105507026423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.93141069985" Y="1.619153752456" />
                  <Point X="-28.099959862382" Y="-1.127261281439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.984116724513" Y="1.49169707193" />
                  <Point X="-28.012328936735" Y="-1.192235926079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.998411646798" Y="1.381342116016" />
                  <Point X="-27.961309200768" Y="-1.273510922568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.981350317796" Y="1.284947862639" />
                  <Point X="-27.949087696955" Y="-1.37206000495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.949586373544" Y="1.195099635321" />
                  <Point X="-27.981735474309" Y="-1.490586178398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.89727976308" Y="1.114397592272" />
                  <Point X="-28.204137080051" Y="-1.693596199391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.825716880245" Y="1.042268994056" />
                  <Point X="-28.526989590725" Y="-1.941329844725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.720603564924" Y="0.985078010787" />
                  <Point X="-28.849842101399" Y="-2.189063490059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.558659772679" Y="0.953189586044" />
                  <Point X="-29.172696238309" Y="-2.43679785944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.253165136003" Y="0.985214115042" />
                  <Point X="-29.13356210592" Y="-2.523364667584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.921537809789" Y="1.028873667046" />
                  <Point X="-29.084557927177" Y="-2.605537047965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.589910483575" Y="1.072533219051" />
                  <Point X="-29.035553748433" Y="-2.687709428347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.292747631401" Y="1.100848198588" />
                  <Point X="-28.982943786535" Y="-2.768276410635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.269907482061" Y="1.007026841793" />
                  <Point X="-28.923825387144" Y="-2.845945649855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.247149253538" Y="0.913169011501" />
                  <Point X="-27.573672572609" Y="-2.348809333732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.839569970701" Y="-2.467194482711" />
                  <Point X="-28.864707197428" Y="-2.923614982428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.232007876417" Y="0.815919940472" />
                  <Point X="-20.831367210575" Y="0.549067972097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.5808079933" Y="0.215395437688" />
                  <Point X="-27.432588716042" Y="-2.389985200212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.626652056613" Y="-2.92161645151" />
                  <Point X="-28.805589007713" Y="-3.001284315001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.216866499296" Y="0.718670869443" />
                  <Point X="-20.24477809964" Y="0.706243824317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.633978186373" Y="0.087732096071" />
                  <Point X="-27.358897600695" Y="-2.461166248265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.650751582826" Y="-0.023726347638" />
                  <Point X="-27.317914404523" Y="-2.546909800172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.639279377333" Y="-0.122609039127" />
                  <Point X="-27.316519901185" Y="-2.650279373742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.615865063186" Y="-0.21617476128" />
                  <Point X="-27.373557041158" Y="-2.779664391044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.565641782282" Y="-0.29780436241" />
                  <Point X="-27.454368788363" Y="-2.919634545468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.500963467866" Y="-0.372998167972" />
                  <Point X="-27.535180535568" Y="-3.059604699892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.407997550556" Y="-0.435597521288" />
                  <Point X="-27.615992282773" Y="-3.199574854315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.292363974075" Y="-0.488104582512" />
                  <Point X="-27.696804029978" Y="-3.339545008739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.146551025662" Y="-0.527174921647" />
                  <Point X="-27.777615777183" Y="-3.479515163163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.000738235438" Y="-0.566245331212" />
                  <Point X="-27.858427524389" Y="-3.619485317586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.854925565898" Y="-0.60531579451" />
                  <Point X="-21.543431497248" Y="-0.911858385152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.045660239025" Y="-1.135465027578" />
                  <Point X="-27.939239271594" Y="-3.75945547201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.709112896359" Y="-0.644386257808" />
                  <Point X="-21.363168299476" Y="-0.935590485057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.119988265288" Y="-1.27254844345" />
                  <Point X="-27.939362176293" Y="-3.863500639166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.56330022682" Y="-0.683456721106" />
                  <Point X="-21.182905101704" Y="-0.959322584961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.132711202823" Y="-1.382203506661" />
                  <Point X="-27.853787206161" Y="-3.929390654177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.41748755728" Y="-0.722527184403" />
                  <Point X="-21.002641903931" Y="-0.983054684865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.14024713067" Y="-1.489549164366" />
                  <Point X="-27.768212236028" Y="-3.995280669187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.271674887741" Y="-0.761597647701" />
                  <Point X="-20.822378706159" Y="-1.00678678477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.119865922048" Y="-1.584465312105" />
                  <Point X="-27.673435879928" Y="-4.057073963221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.225408809698" Y="-0.844989109058" />
                  <Point X="-20.642115508387" Y="-1.030518884674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.072579449135" Y="-1.667402464395" />
                  <Point X="-25.113907273181" Y="-3.021488853087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.432619368217" Y="-3.163388620152" />
                  <Point X="-26.826016050076" Y="-3.783768792929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.413214467041" Y="-4.04520637213" />
                  <Point X="-27.57573793508" Y="-4.117566482136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.244404920716" Y="-0.95743716905" />
                  <Point X="-20.461852310615" Y="-1.054250984579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.02060128298" Y="-1.748250740271" />
                  <Point X="-24.877645855564" Y="-3.020288939191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.540091869201" Y="-3.31522890693" />
                  <Point X="-26.624428603746" Y="-3.798006725683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.270818859471" Y="-1.073187858734" />
                  <Point X="-20.281589010508" Y="-1.077983038921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.959565757447" Y="-1.825066419939" />
                  <Point X="-22.422810264366" Y="-2.031316162731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.868878816731" Y="-2.229918677858" />
                  <Point X="-24.740018472769" Y="-3.063003726944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.632156115893" Y="-3.460208996906" />
                  <Point X="-26.503677364227" Y="-3.848235256521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.875225832599" Y="-1.891506312538" />
                  <Point X="-22.256645795673" Y="-2.061325421247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.943088563177" Y="-2.366949432163" />
                  <Point X="-23.571432918823" Y="-2.646706363548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.173613295729" Y="-2.914814341077" />
                  <Point X="-24.615762301656" Y="-3.111671761696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.667391820635" Y="-3.579887389863" />
                  <Point X="-26.42406433014" Y="-3.916779696479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.789464556305" Y="-1.957313378701" />
                  <Point X="-22.128220512818" Y="-2.10813724786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.995018292077" Y="-2.494060483547" />
                  <Point X="-23.432037582832" Y="-2.688634007825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.225047728804" Y="-3.041704872552" />
                  <Point X="-24.540084643947" Y="-3.181968344105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.699030251491" Y="-3.697964173296" />
                  <Point X="-26.345415589743" Y="-3.985753467649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.703703245893" Y="-2.023120429674" />
                  <Point X="-22.026526198366" Y="-2.166850468391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.994611275098" Y="-2.597869714371" />
                  <Point X="-23.336469776397" Y="-2.750074925466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.250071289408" Y="-3.156836526" />
                  <Point X="-24.484947061698" Y="-3.261409957307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.730668952614" Y="-3.81604107706" />
                  <Point X="-26.266766849345" Y="-4.054727238818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.617941886256" Y="-2.088927458731" />
                  <Point X="-21.924831883913" Y="-2.225563688922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.977228269257" Y="-2.694120747992" />
                  <Point X="-23.240901874703" Y="-2.811515800695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.273415322665" Y="-3.271220405695" />
                  <Point X="-24.429810105332" Y="-3.340851849171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.762307840163" Y="-3.934118063826" />
                  <Point X="-26.207481801779" Y="-4.13232228149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.532180526618" Y="-2.154734487787" />
                  <Point X="-21.82313756946" Y="-2.284276909453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.959844269081" Y="-2.790371338906" />
                  <Point X="-23.162781280649" Y="-2.880724717767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.269452145246" Y="-3.373446331881" />
                  <Point X="-24.375191019094" Y="-3.42052431167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.793946727712" Y="-4.052195050592" />
                  <Point X="-26.17794274059" Y="-4.22316109057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.446419166981" Y="-2.220541516844" />
                  <Point X="-21.721443255007" Y="-2.342990129984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.926914689708" Y="-2.879700592032" />
                  <Point X="-23.103305629223" Y="-2.958234898133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.256519096545" Y="-3.471678614069" />
                  <Point X="-24.339400538511" Y="-3.508579809512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.825585615261" Y="-4.170272037359" />
                  <Point X="-26.158940049257" Y="-4.318690993748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.360657807344" Y="-2.2863485459" />
                  <Point X="-21.619748940555" Y="-2.401703350515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.879153014588" Y="-2.962426170669" />
                  <Point X="-23.043829977796" Y="-3.035745078499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.243586047845" Y="-3.569910896257" />
                  <Point X="-24.314506191037" Y="-3.601486578372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.857224502811" Y="-4.288349024125" />
                  <Point X="-26.139937354143" Y="-4.414220895243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.274896447707" Y="-2.352155574957" />
                  <Point X="-21.518054626102" Y="-2.460416571046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.831391339468" Y="-3.045151749305" />
                  <Point X="-22.98435432637" Y="-3.113255258864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.230653236583" Y="-3.66814328416" />
                  <Point X="-24.289611843563" Y="-3.694393347233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.88886339036" Y="-4.406426010891" />
                  <Point X="-26.121701880424" Y="-4.510092385712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.189135088069" Y="-2.417962604013" />
                  <Point X="-21.416360311649" Y="-2.519129791577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.783629663331" Y="-3.127877327489" />
                  <Point X="-22.924878674944" Y="-3.19076543923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.217720713603" Y="-3.766375800414" />
                  <Point X="-24.264717496089" Y="-3.787300116094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.920502277909" Y="-4.524502997657" />
                  <Point X="-26.126064083374" Y="-4.616025010054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.103373728432" Y="-2.48376963307" />
                  <Point X="-21.314665997196" Y="-2.577843012107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.735867984448" Y="-3.210602904449" />
                  <Point X="-22.865403023518" Y="-3.268275619596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.204788190623" Y="-3.864608316668" />
                  <Point X="-24.239823148615" Y="-3.880206884954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.952141165458" Y="-4.642579984424" />
                  <Point X="-26.140607249254" Y="-4.726490491137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.017612368795" Y="-2.549576662126" />
                  <Point X="-21.212971682744" Y="-2.636556232638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.688106305566" Y="-3.29332848141" />
                  <Point X="-22.805927372092" Y="-3.345785799962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.191855667643" Y="-3.962840832922" />
                  <Point X="-24.214928801141" Y="-3.973113653815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.983780053007" Y="-4.76065697119" />
                  <Point X="-25.993898352018" Y="-4.765161928156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.931851009158" Y="-2.615383691183" />
                  <Point X="-21.111277368291" Y="-2.695269453169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.640344626683" Y="-3.376054058371" />
                  <Point X="-22.746451720666" Y="-3.423295980327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.178923144662" Y="-4.061073349175" />
                  <Point X="-24.190034453667" Y="-4.066020422675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.987343186291" Y="-2.744080846711" />
                  <Point X="-21.00958334359" Y="-2.753982802706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.5925829478" Y="-3.458779635332" />
                  <Point X="-22.68697606924" Y="-3.500806160693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.544821268917" Y="-3.541505212293" />
                  <Point X="-22.627500417813" Y="-3.578316341059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.497059590035" Y="-3.624230789254" />
                  <Point X="-22.568024766387" Y="-3.655826521425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.449297911152" Y="-3.706956366215" />
                  <Point X="-22.508549114961" Y="-3.73333670179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.401536232269" Y="-3.789681943176" />
                  <Point X="-22.449073463535" Y="-3.810846882156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.353774553386" Y="-3.872407520137" />
                  <Point X="-22.389597812109" Y="-3.888357062522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.306012874504" Y="-3.955133097098" />
                  <Point X="-22.330122160683" Y="-3.965867242887" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.0001640625" Y="0.001626220703" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.1970703125" Y="-4.773875" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521543701172" />
                  <Point X="-24.671126953125" Y="-3.326389892578" />
                  <Point X="-24.699408203125" Y="-3.285643310547" />
                  <Point X="-24.7256640625" Y="-3.266399658203" />
                  <Point X="-24.935259765625" Y="-3.201348388672" />
                  <Point X="-24.9790234375" Y="-3.187766357422" />
                  <Point X="-25.0086640625" Y="-3.187766357422" />
                  <Point X="-25.218259765625" Y="-3.252817382812" />
                  <Point X="-25.2620234375" Y="-3.266399658203" />
                  <Point X="-25.288279296875" Y="-3.285644775391" />
                  <Point X="-25.4237265625" Y="-3.480798339844" />
                  <Point X="-25.452005859375" Y="-3.521545166016" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.52841796875" Y="-3.795339355469" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-26.0499453125" Y="-4.947828613281" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.343408203125" Y="-4.875501464844" />
                  <Point X="-26.349041015625" Y="-4.854044433594" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.359755859375" Y="-4.283025390625" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.202628417969" />
                  <Point X="-26.579251953125" Y="-4.033397949219" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.90535546875" Y="-3.968976318359" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791503906" />
                  <Point X="-27.203287109375" Y="-4.11638671875" />
                  <Point X="-27.24784375" Y="-4.146159667969" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.298580078125" Y="-4.207918945312" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.782107421875" Y="-4.213260253906" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.192541015625" Y="-3.908356445312" />
                  <Point X="-28.228580078125" Y="-3.880607666016" />
                  <Point X="-28.1218828125" Y="-3.695803222656" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597595214844" />
                  <Point X="-27.5139765625" Y="-2.568767089844" />
                  <Point X="-27.528328125" Y="-2.554415039062" />
                  <Point X="-27.53132421875" Y="-2.551418457031" />
                  <Point X="-27.560154296875" Y="-2.537198974609" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-27.8143984375" Y="-2.6720546875" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-29.103455078125" Y="-2.923654541016" />
                  <Point X="-29.161697265625" Y="-2.84713671875" />
                  <Point X="-29.403095703125" Y="-2.442347900391" />
                  <Point X="-29.43101953125" Y="-2.395526367188" />
                  <Point X="-29.24132421875" Y="-2.24996875" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.39601574707" />
                  <Point X="-28.139447265625" Y="-1.371406616211" />
                  <Point X="-28.138115234375" Y="-1.366259765625" />
                  <Point X="-28.140326171875" Y="-1.334592895508" />
                  <Point X="-28.161158203125" Y="-1.310639526367" />
                  <Point X="-28.18306640625" Y="-1.297745239258" />
                  <Point X="-28.187640625" Y="-1.295052978516" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.500685546875" Y="-1.325586181641" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.904609375" Y="-1.100379882812" />
                  <Point X="-29.927392578125" Y="-1.011186950684" />
                  <Point X="-29.991259765625" Y="-0.56464074707" />
                  <Point X="-29.998396484375" Y="-0.514742370605" />
                  <Point X="-29.784939453125" Y="-0.457547088623" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.54189453125" Y="-0.12142489624" />
                  <Point X="-28.518935546875" Y="-0.105489959717" />
                  <Point X="-28.514146484375" Y="-0.102166366577" />
                  <Point X="-28.494896484375" Y="-0.07590562439" />
                  <Point X="-28.487244140625" Y="-0.051247123718" />
                  <Point X="-28.485646484375" Y="-0.046098747253" />
                  <Point X="-28.485646484375" Y="-0.016461328506" />
                  <Point X="-28.493298828125" Y="0.008197021484" />
                  <Point X="-28.494896484375" Y="0.01334554863" />
                  <Point X="-28.514140625" Y="0.039602794647" />
                  <Point X="-28.537099609375" Y="0.055537731171" />
                  <Point X="-28.53710546875" Y="0.055541229248" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-28.81375" Y="0.134757598877" />
                  <Point X="-29.998185546875" Y="0.452126037598" />
                  <Point X="-29.932326171875" Y="0.897202636719" />
                  <Point X="-29.91764453125" Y="0.996416320801" />
                  <Point X="-29.78908203125" Y="1.470850463867" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.633150390625" Y="1.509819458008" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731705078125" Y="1.395866088867" />
                  <Point X="-28.680890625" Y="1.411888305664" />
                  <Point X="-28.670279296875" Y="1.415233642578" />
                  <Point X="-28.65153515625" Y="1.426055419922" />
                  <Point X="-28.639119140625" Y="1.443785522461" />
                  <Point X="-28.618728515625" Y="1.49301171875" />
                  <Point X="-28.61446875" Y="1.503295776367" />
                  <Point X="-28.610712890625" Y="1.524607788086" />
                  <Point X="-28.616314453125" Y="1.545510742188" />
                  <Point X="-28.640916015625" Y="1.592772460938" />
                  <Point X="-28.64605859375" Y="1.602647705078" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-28.806974609375" Y="1.732024780273" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.217060546875" Y="2.689271240234" />
                  <Point X="-29.16001171875" Y="2.787007080078" />
                  <Point X="-28.819458984375" Y="3.224742431641" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.699556640625" Y="3.238944335938" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.067740234375" Y="2.914242919922" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927405029297" />
                  <Point X="-27.963015625" Y="2.977639892578" />
                  <Point X="-27.95252734375" Y="2.988128417969" />
                  <Point X="-27.9408984375" Y="3.0063828125" />
                  <Point X="-27.938072265625" Y="3.027841796875" />
                  <Point X="-27.944263671875" Y="3.098614501953" />
                  <Point X="-27.945556640625" Y="3.113391357422" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.0172109375" Y="3.246864746094" />
                  <Point X="-28.30727734375" Y="3.749276855469" />
                  <Point X="-27.85223046875" Y="4.09815625" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.216509765625" Y="4.47232421875" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.133259765625" Y="4.503170410156" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.8724765625" Y="4.232655761719" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.218491699219" />
                  <Point X="-26.813806640625" Y="4.22225" />
                  <Point X="-26.731763671875" Y="4.256233886719" />
                  <Point X="-26.7146328125" Y="4.263329589844" />
                  <Point X="-26.696904296875" Y="4.275743652344" />
                  <Point X="-26.68608203125" Y="4.294486816406" />
                  <Point X="-26.65937890625" Y="4.379180175781" />
                  <Point X="-26.653802734375" Y="4.39686328125" />
                  <Point X="-26.651916015625" Y="4.418426757812" />
                  <Point X="-26.659322265625" Y="4.474681640625" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.09671484375" Y="4.867235351562" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.31785546875" Y="4.979396972656" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.202005859375" Y="4.907530761719" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.91216796875" Y="4.435475097656" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.2521015625" Y="4.937327148438" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.6018359375" Y="4.795686035156" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.140666015625" Y="4.64179296875" />
                  <Point X="-23.06896484375" Y="4.615786132813" />
                  <Point X="-22.73037890625" Y="4.45744140625" />
                  <Point X="-22.6613046875" Y="4.42513671875" />
                  <Point X="-22.33421484375" Y="4.23457421875" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-21.95898828125" Y="3.976312988281" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.057345703125" Y="3.738204345703" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491516357422" />
                  <Point X="-22.792908203125" Y="2.425097900391" />
                  <Point X="-22.797955078125" Y="2.392328613281" />
                  <Point X="-22.791029296875" Y="2.334895507812" />
                  <Point X="-22.789583984375" Y="2.322903808594" />
                  <Point X="-22.781318359375" Y="2.3008125" />
                  <Point X="-22.745779296875" Y="2.248438964844" />
                  <Point X="-22.72505859375" Y="2.224203125" />
                  <Point X="-22.672685546875" Y="2.188665527344" />
                  <Point X="-22.66175" Y="2.181245605469" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.582228515625" Y="2.1660546875" />
                  <Point X="-22.551333984375" Y="2.165946533203" />
                  <Point X="-22.4849140625" Y="2.183707763672" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-22.2011875" Y="2.341244873047" />
                  <Point X="-21.005751953125" Y="3.031430419922" />
                  <Point X="-20.83699609375" Y="2.796897705078" />
                  <Point X="-20.79740234375" Y="2.741873535156" />
                  <Point X="-20.625431640625" Y="2.457687011719" />
                  <Point X="-20.612486328125" Y="2.436295410156" />
                  <Point X="-20.7729140625" Y="2.313194824219" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583832885742" />
                  <Point X="-21.7684296875" Y="1.521472167969" />
                  <Point X="-21.78687890625" Y="1.491503295898" />
                  <Point X="-21.804685546875" Y="1.427832641602" />
                  <Point X="-21.808404296875" Y="1.414538818359" />
                  <Point X="-21.809220703125" Y="1.390962768555" />
                  <Point X="-21.794603515625" Y="1.32012109375" />
                  <Point X="-21.784353515625" Y="1.287955444336" />
                  <Point X="-21.74459765625" Y="1.227527099609" />
                  <Point X="-21.736296875" Y="1.21491003418" />
                  <Point X="-21.719052734375" Y="1.198819824219" />
                  <Point X="-21.661439453125" Y="1.166388793945" />
                  <Point X="-21.63143359375" Y="1.153619506836" />
                  <Point X="-21.553537109375" Y="1.143324462891" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-21.279326171875" Y="1.173409667969" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.077814453125" Y="1.021227294922" />
                  <Point X="-20.060806640625" Y="0.951366821289" />
                  <Point X="-20.006615234375" Y="0.603301879883" />
                  <Point X="-20.002140625" Y="0.574556396484" />
                  <Point X="-20.183326171875" Y="0.526007446289" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232818893433" />
                  <Point X="-21.347443359375" Y="0.188582717896" />
                  <Point X="-21.377732421875" Y="0.166927444458" />
                  <Point X="-21.423650390625" Y="0.108416542053" />
                  <Point X="-21.43323828125" Y="0.096199775696" />
                  <Point X="-21.443013671875" Y="0.07473374176" />
                  <Point X="-21.4583203125" Y="-0.00518951416" />
                  <Point X="-21.461515625" Y="-0.040683258057" />
                  <Point X="-21.446208984375" Y="-0.120606361389" />
                  <Point X="-21.443013671875" Y="-0.137293823242" />
                  <Point X="-21.43323828125" Y="-0.158759857178" />
                  <Point X="-21.3873203125" Y="-0.2172709198" />
                  <Point X="-21.363421875" Y="-0.241906600952" />
                  <Point X="-21.286890625" Y="-0.28614276123" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-21.0342734375" Y="-0.360556976318" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.04207421875" Y="-0.903431152344" />
                  <Point X="-20.051568359375" Y="-0.966411682129" />
                  <Point X="-20.120998046875" Y="-1.270661743164" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-20.340734375" Y="-1.26183605957" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.755365234375" Y="-1.130988647461" />
                  <Point X="-21.7867265625" Y="-1.137805053711" />
                  <Point X="-21.8145546875" Y="-1.154697998047" />
                  <Point X="-21.90534375" Y="-1.263888061523" />
                  <Point X="-21.924298828125" Y="-1.286686035156" />
                  <Point X="-21.935640625" Y="-1.314070678711" />
                  <Point X="-21.94865234375" Y="-1.45547644043" />
                  <Point X="-21.951369140625" Y="-1.485000976562" />
                  <Point X="-21.943638671875" Y="-1.516621582031" />
                  <Point X="-21.860513671875" Y="-1.645916381836" />
                  <Point X="-21.843158203125" Y="-1.672912109375" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-21.623146484375" Y="-1.845444580078" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.76910546875" Y="-2.758837402344" />
                  <Point X="-20.795869140625" Y="-2.802142089844" />
                  <Point X="-20.93945703125" Y="-3.006162353516" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.135669921875" Y="-2.900579345703" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.441423828125" Y="-2.221027832031" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.659431640625" Y="-2.297404785156" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.334682373047" />
                  <Point X="-22.78955859375" Y="-2.483192626953" />
                  <Point X="-22.80587890625" Y="-2.514200439453" />
                  <Point X="-22.8108359375" Y="-2.546376220703" />
                  <Point X="-22.77855078125" Y="-2.725141601562" />
                  <Point X="-22.77180859375" Y="-2.762466552734" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.631994140625" Y="-3.010520996094" />
                  <Point X="-22.01332421875" Y="-4.082088867188" />
                  <Point X="-22.132943359375" Y="-4.167529296875" />
                  <Point X="-22.16469921875" Y="-4.1902109375" />
                  <Point X="-22.32022265625" Y="-4.290879882812" />
                  <Point X="-22.46912109375" Y="-4.096830566406" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467529297" />
                  <Point X="-23.50576171875" Y="-2.867115722656" />
                  <Point X="-23.54257421875" Y="-2.843448730469" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.767021484375" Y="-2.853461181641" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509521484" />
                  <Point X="-23.9835625" Y="-2.992311035156" />
                  <Point X="-24.014650390625" Y="-3.018159912109" />
                  <Point X="-24.03154296875" Y="-3.045984375" />
                  <Point X="-24.076060546875" Y="-3.250806640625" />
                  <Point X="-24.085357421875" Y="-3.293572265625" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-24.048119140625" Y="-3.598976318359" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.975611328125" Y="-4.956662109375" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#200" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.159865750374" Y="4.951751606804" Z="2.1" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.1" />
                  <Point X="-0.329824227631" Y="5.060338377271" Z="2.1" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.1" />
                  <Point X="-1.116370524828" Y="4.946658738665" Z="2.1" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.1" />
                  <Point X="-1.715051979358" Y="4.499435428002" Z="2.1" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.1" />
                  <Point X="-1.713221434693" Y="4.425497245507" Z="2.1" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.1" />
                  <Point X="-1.757055289229" Y="4.33370850278" Z="2.1" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.1" />
                  <Point X="-1.855545577404" Y="4.308286160276" Z="2.1" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.1" />
                  <Point X="-2.099748472908" Y="4.564888257088" Z="2.1" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.1" />
                  <Point X="-2.246950282568" Y="4.547311601988" Z="2.1" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.1" />
                  <Point X="-2.88881003195" Y="4.169116134428" Z="2.1" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.1" />
                  <Point X="-3.066668240966" Y="3.253144849134" Z="2.1" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.1" />
                  <Point X="-3.000231871464" Y="3.12553611259" Z="2.1" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.1" />
                  <Point X="-3.004528688808" Y="3.044274879078" Z="2.1" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.1" />
                  <Point X="-3.069540353017" Y="2.995332827709" Z="2.1" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.1" />
                  <Point X="-3.680714499163" Y="3.31352570778" Z="2.1" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.1" />
                  <Point X="-3.865078253709" Y="3.286725188062" Z="2.1" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.1" />
                  <Point X="-4.266399410583" Y="2.745756538975" Z="2.1" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.1" />
                  <Point X="-3.843569969582" Y="1.723637068881" Z="2.1" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.1" />
                  <Point X="-3.691425374286" Y="1.600966196766" Z="2.1" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.1" />
                  <Point X="-3.671079750644" Y="1.543426307468" Z="2.1" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.1" />
                  <Point X="-3.702079946514" Y="1.49085478136" Z="2.1" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.1" />
                  <Point X="-4.632782069545" Y="1.590671759161" Z="2.1" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.1" />
                  <Point X="-4.843499358245" Y="1.515207096669" Z="2.1" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.1" />
                  <Point X="-4.987943859195" Y="0.935801743036" Z="2.1" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.1" />
                  <Point X="-3.832849219806" Y="0.117741325474" Z="2.1" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.1" />
                  <Point X="-3.571767027618" Y="0.04574192491" Z="2.1" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.1" />
                  <Point X="-3.547209929271" Y="0.024658446061" Z="2.1" />
                  <Point X="-3.539556741714" Y="0" Z="2.1" />
                  <Point X="-3.541154671864" Y="-0.005148505" Z="2.1" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.1" />
                  <Point X="-3.553601567506" Y="-0.033134058224" Z="2.1" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.1" />
                  <Point X="-4.804038568967" Y="-0.377970731282" Z="2.1" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.1" />
                  <Point X="-5.046912050183" Y="-0.540439309911" Z="2.1" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.1" />
                  <Point X="-4.959212401833" Y="-1.081474035436" Z="2.1" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.1" />
                  <Point X="-3.500316472582" Y="-1.343878399096" Z="2.1" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.1" />
                  <Point X="-3.214584595577" Y="-1.309555537337" Z="2.1" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.1" />
                  <Point X="-3.194007090596" Y="-1.32758801648" Z="2.1" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.1" />
                  <Point X="-4.277918525623" Y="-2.179020911138" Z="2.1" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.1" />
                  <Point X="-4.452196936742" Y="-2.436678025997" Z="2.1" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.1" />
                  <Point X="-4.149398110196" Y="-2.922657939097" Z="2.1" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.1" />
                  <Point X="-2.795555306281" Y="-2.684076000389" Z="2.1" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.1" />
                  <Point X="-2.569842916196" Y="-2.558487552935" Z="2.1" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.1" />
                  <Point X="-3.171341012779" Y="-3.639523076211" Z="2.1" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.1" />
                  <Point X="-3.229202334464" Y="-3.916693783613" Z="2.1" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.1" />
                  <Point X="-2.814585978039" Y="-4.224491069983" Z="2.1" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.1" />
                  <Point X="-2.265068097814" Y="-4.20707702098" Z="2.1" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.1" />
                  <Point X="-2.181664190908" Y="-4.126679374084" Z="2.1" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.1" />
                  <Point X="-1.914781179008" Y="-3.987589296289" Z="2.1" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.1" />
                  <Point X="-1.618376087668" Y="-4.039710032219" Z="2.1" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.1" />
                  <Point X="-1.41495117461" Y="-4.26150030873" Z="2.1" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.1" />
                  <Point X="-1.404770001256" Y="-4.816237867206" Z="2.1" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.1" />
                  <Point X="-1.362023763675" Y="-4.89264447744" Z="2.1" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.1" />
                  <Point X="-1.065629325632" Y="-4.965632799513" Z="2.1" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.1" />
                  <Point X="-0.486278520712" Y="-3.777000188912" Z="2.1" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.1" />
                  <Point X="-0.388806367091" Y="-3.47802639547" Z="2.1" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.1" />
                  <Point X="-0.209596787669" Y="-3.269290476488" Z="2.1" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.1" />
                  <Point X="0.043762291691" Y="-3.217821568801" Z="2.1" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.1" />
                  <Point X="0.281639492341" Y="-3.323619360298" Z="2.1" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.1" />
                  <Point X="0.748476339689" Y="-4.755535840904" Z="2.1" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.1" />
                  <Point X="0.848818267056" Y="-5.008104098836" Z="2.1" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.1" />
                  <Point X="1.028935925896" Y="-4.974222466691" Z="2.1" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.1" />
                  <Point X="0.995295440787" Y="-3.561169923798" Z="2.1" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.1" />
                  <Point X="0.966641026412" Y="-3.230148286745" Z="2.1" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.1" />
                  <Point X="1.042248306775" Y="-2.999477119276" Z="2.1" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.1" />
                  <Point X="1.231404429422" Y="-2.871970615598" Z="2.1" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.1" />
                  <Point X="1.461042913217" Y="-2.877893597631" Z="2.1" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.1" />
                  <Point X="2.485053195189" Y="-4.09598827818" Z="2.1" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.1" />
                  <Point X="2.695767889446" Y="-4.304823524569" Z="2.1" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.1" />
                  <Point X="2.889960520187" Y="-4.176936482978" Z="2.1" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.1" />
                  <Point X="2.405148977849" Y="-2.95424105368" Z="2.1" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.1" />
                  <Point X="2.26449603794" Y="-2.684973680299" Z="2.1" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.1" />
                  <Point X="2.24853032776" Y="-2.475200339007" Z="2.1" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.1" />
                  <Point X="2.357698039365" Y="-2.310370982207" Z="2.1" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.1" />
                  <Point X="2.543533123214" Y="-2.238951971551" Z="2.1" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.1" />
                  <Point X="3.833172543427" Y="-2.912600522426" Z="2.1" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.1" />
                  <Point X="4.095274599029" Y="-3.00365993222" Z="2.1" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.1" />
                  <Point X="4.267270053721" Y="-2.753843219999" Z="2.1" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.1" />
                  <Point X="3.401133638641" Y="-1.774496946131" Z="2.1" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.1" />
                  <Point X="3.175387026611" Y="-1.587597225949" Z="2.1" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.1" />
                  <Point X="3.094979341901" Y="-1.428777997715" Z="2.1" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.1" />
                  <Point X="3.126947367706" Y="-1.264574155744" Z="2.1" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.1" />
                  <Point X="3.24909683337" Y="-1.148567673675" Z="2.1" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.1" />
                  <Point X="4.646583042869" Y="-1.280128358289" Z="2.1" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.1" />
                  <Point X="4.921590478513" Y="-1.250505856123" Z="2.1" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.1" />
                  <Point X="5.001210792939" Y="-0.879604490234" Z="2.1" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.1" />
                  <Point X="3.972510315267" Y="-0.280980665871" Z="2.1" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.1" />
                  <Point X="3.731973604075" Y="-0.211574433211" Z="2.1" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.1" />
                  <Point X="3.64585525734" Y="-0.155121533692" Z="2.1" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.1" />
                  <Point X="3.596740904592" Y="-0.079923155111" Z="2.1" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.1" />
                  <Point X="3.584630545832" Y="0.016687376075" Z="2.1" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.1" />
                  <Point X="3.609524181061" Y="0.108827204883" Z="2.1" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.1" />
                  <Point X="3.671421810277" Y="0.176574411031" Z="2.1" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.1" />
                  <Point X="4.823457502039" Y="0.508991266898" Z="2.1" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.1" />
                  <Point X="5.036631934708" Y="0.642273621697" Z="2.1" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.1" />
                  <Point X="4.964610768197" Y="1.064334012829" Z="2.1" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.1" />
                  <Point X="3.707992163952" Y="1.254261938854" Z="2.1" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.1" />
                  <Point X="3.446857176896" Y="1.224173600226" Z="2.1" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.1" />
                  <Point X="3.356931460768" Y="1.241239953466" Z="2.1" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.1" />
                  <Point X="3.291017650405" Y="1.286288036579" Z="2.1" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.1" />
                  <Point X="3.248209065412" Y="1.361507615027" Z="2.1" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.1" />
                  <Point X="3.237309829841" Y="1.445643154462" Z="2.1" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.1" />
                  <Point X="3.265096604377" Y="1.522334205912" Z="2.1" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.1" />
                  <Point X="4.251366486379" Y="2.304807150941" Z="2.1" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.1" />
                  <Point X="4.411189563921" Y="2.514853837751" Z="2.1" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.1" />
                  <Point X="4.197433420696" Y="2.857381420894" Z="2.1" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.1" />
                  <Point X="2.767654791231" Y="2.415826305272" Z="2.1" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.1" />
                  <Point X="2.496010207451" Y="2.263290360813" Z="2.1" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.1" />
                  <Point X="2.417600014005" Y="2.246975144688" Z="2.1" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.1" />
                  <Point X="2.349231579087" Y="2.26132051302" Z="2.1" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.1" />
                  <Point X="2.289438087215" Y="2.307793281295" Z="2.1" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.1" />
                  <Point X="2.252454558071" Y="2.372158429828" Z="2.1" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.1" />
                  <Point X="2.249237581844" Y="2.443459368431" Z="2.1" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.1" />
                  <Point X="2.979799047587" Y="3.744484290724" Z="2.1" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.1" />
                  <Point X="3.063831221654" Y="4.048340044514" Z="2.1" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.1" />
                  <Point X="2.684797714503" Y="4.309056853089" Z="2.1" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.1" />
                  <Point X="2.284645027228" Y="4.534012769105" Z="2.1" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.1" />
                  <Point X="1.870225516819" Y="4.72007632665" Z="2.1" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.1" />
                  <Point X="1.403742345732" Y="4.875569545869" Z="2.1" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.1" />
                  <Point X="0.746949153218" Y="5.018335373451" Z="2.1" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.1" />
                  <Point X="0.033378811179" Y="4.479695899146" Z="2.1" />
                  <Point X="0" Y="4.355124473572" Z="2.1" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>