<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#213" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3486" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000473632812" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.4366953125" Y="-3.512524902344" />
                  <Point X="-24.442279296875" Y="-3.497135498047" />
                  <Point X="-24.45763671875" Y="-3.467375244141" />
                  <Point X="-24.612421875" Y="-3.244359130859" />
                  <Point X="-24.621365234375" Y="-3.231474853516" />
                  <Point X="-24.643251953125" Y="-3.209018066406" />
                  <Point X="-24.669505859375" Y="-3.189776123047" />
                  <Point X="-24.69750390625" Y="-3.175669189453" />
                  <Point X="-24.937025390625" Y="-3.101330566406" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.979021484375" Y="-3.092766601562" />
                  <Point X="-25.00866015625" Y="-3.092766113281" />
                  <Point X="-25.036822265625" Y="-3.097035644531" />
                  <Point X="-25.276341796875" Y="-3.171374023438" />
                  <Point X="-25.290181640625" Y="-3.175668945312" />
                  <Point X="-25.318181640625" Y="-3.189776611328" />
                  <Point X="-25.34444140625" Y="-3.2090234375" />
                  <Point X="-25.366326171875" Y="-3.231479248047" />
                  <Point X="-25.521111328125" Y="-3.454495117188" />
                  <Point X="-25.525669921875" Y="-3.461741699219" />
                  <Point X="-25.541830078125" Y="-3.490191894531" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.572869140625" Y="-3.594177734375" />
                  <Point X="-25.9165859375" Y="-4.876941894531" />
                  <Point X="-26.067244140625" Y="-4.847698730469" />
                  <Point X="-26.07933203125" Y="-4.845352050781" />
                  <Point X="-26.246416015625" Y="-4.802362304688" />
                  <Point X="-26.2149609375" Y="-4.563438476562" />
                  <Point X="-26.21419921875" Y="-4.547929199219" />
                  <Point X="-26.2165078125" Y="-4.516221191406" />
                  <Point X="-26.27373046875" Y="-4.228549316406" />
                  <Point X="-26.277037109375" Y="-4.2119296875" />
                  <Point X="-26.28794140625" Y="-4.182959960937" />
                  <Point X="-26.304013671875" Y="-4.155125" />
                  <Point X="-26.32364453125" Y="-4.131204589844" />
                  <Point X="-26.5441640625" Y="-3.937812255859" />
                  <Point X="-26.556904296875" Y="-3.926639404297" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.93570703125" Y="-3.871782958984" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.983421875" Y="-3.873709472656" />
                  <Point X="-27.01447265625" Y="-3.882030761719" />
                  <Point X="-27.04266015625" Y="-3.894803222656" />
                  <Point X="-27.286537109375" Y="-4.057756591797" />
                  <Point X="-27.29269921875" Y="-4.062240966797" />
                  <Point X="-27.318673828125" Y="-4.082789306641" />
                  <Point X="-27.335095703125" Y="-4.099456542969" />
                  <Point X="-27.347380859375" Y="-4.115464355469" />
                  <Point X="-27.4801484375" Y="-4.288490234375" />
                  <Point X="-27.78398828125" Y="-4.100359863281" />
                  <Point X="-27.801708984375" Y="-4.089387451172" />
                  <Point X="-28.104720703125" Y="-3.856078125" />
                  <Point X="-27.423759765625" Y="-2.676619140625" />
                  <Point X="-27.412859375" Y="-2.647655029297" />
                  <Point X="-27.406587890625" Y="-2.616129150391" />
                  <Point X="-27.40557421875" Y="-2.585194580078" />
                  <Point X="-27.41455859375" Y="-2.555576171875" />
                  <Point X="-27.428775390625" Y="-2.526747558594" />
                  <Point X="-27.446802734375" Y="-2.501591552734" />
                  <Point X="-27.463181640625" Y="-2.4852109375" />
                  <Point X="-27.46412890625" Y="-2.484262451172" />
                  <Point X="-27.489296875" Y="-2.466221435547" />
                  <Point X="-27.518126953125" Y="-2.452000488281" />
                  <Point X="-27.54774609375" Y="-2.443012695312" />
                  <Point X="-27.57868359375" Y="-2.444023925781" />
                  <Point X="-27.610212890625" Y="-2.450294433594" />
                  <Point X="-27.639181640625" Y="-2.461197021484" />
                  <Point X="-27.709607421875" Y="-2.501856689453" />
                  <Point X="-28.8180234375" Y="-3.141801025391" />
                  <Point X="-29.068859375" Y="-2.812253662109" />
                  <Point X="-29.082857421875" Y="-2.793862792969" />
                  <Point X="-29.306142578125" Y="-2.419451171875" />
                  <Point X="-28.105955078125" Y="-1.498514282227" />
                  <Point X="-28.084580078125" Y="-1.475596069336" />
                  <Point X="-28.066615234375" Y="-1.448467041016" />
                  <Point X="-28.053857421875" Y="-1.419839111328" />
                  <Point X="-28.046572265625" Y="-1.391716552734" />
                  <Point X="-28.046142578125" Y="-1.390053588867" />
                  <Point X="-28.043341796875" Y="-1.359593383789" />
                  <Point X="-28.0455625" Y="-1.327936279297" />
                  <Point X="-28.0525703125" Y="-1.298205322266" />
                  <Point X="-28.06865234375" Y="-1.272235229492" />
                  <Point X="-28.08948046875" Y="-1.248290893555" />
                  <Point X="-28.112970703125" Y="-1.228768554688" />
                  <Point X="-28.137986328125" Y="-1.214044555664" />
                  <Point X="-28.139443359375" Y="-1.213186035156" />
                  <Point X="-28.16873828125" Y="-1.201947875977" />
                  <Point X="-28.200615234375" Y="-1.195472412109" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-28.32083203125" Y="-1.206088378906" />
                  <Point X="-29.732099609375" Y="-1.391885375977" />
                  <Point X="-29.828599609375" Y="-1.014095703125" />
                  <Point X="-29.83407421875" Y="-0.99265625" />
                  <Point X="-29.892421875" Y="-0.584698303223" />
                  <Point X="-28.532875" Y="-0.220408370972" />
                  <Point X="-28.517490234375" Y="-0.214826126099" />
                  <Point X="-28.4877265625" Y="-0.199468887329" />
                  <Point X="-28.46148828125" Y="-0.181258895874" />
                  <Point X="-28.45997265625" Y="-0.180206848145" />
                  <Point X="-28.437513671875" Y="-0.158318222046" />
                  <Point X="-28.4182734375" Y="-0.132065170288" />
                  <Point X="-28.40416796875" Y="-0.104068946838" />
                  <Point X="-28.395421875" Y="-0.075889976501" />
                  <Point X="-28.39488671875" Y="-0.074164123535" />
                  <Point X="-28.39063671875" Y="-0.046026592255" />
                  <Point X="-28.3906484375" Y="-0.016421098709" />
                  <Point X="-28.39491796875" Y="0.011701253891" />
                  <Point X="-28.40365234375" Y="0.039843032837" />
                  <Point X="-28.404134765625" Y="0.041401161194" />
                  <Point X="-28.418240234375" Y="0.069438659668" />
                  <Point X="-28.4374921875" Y="0.095728752136" />
                  <Point X="-28.45997265625" Y="0.117646827698" />
                  <Point X="-28.4862109375" Y="0.135856811523" />
                  <Point X="-28.498970703125" Y="0.143313354492" />
                  <Point X="-28.532875" Y="0.157848205566" />
                  <Point X="-28.6139140625" Y="0.179562866211" />
                  <Point X="-29.89181640625" Y="0.521975402832" />
                  <Point X="-29.828015625" Y="0.953133605957" />
                  <Point X="-29.82448828125" Y="0.976973083496" />
                  <Point X="-29.70355078125" Y="1.423267822266" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744986328125" Y="1.299341430664" />
                  <Point X="-28.723427734375" Y="1.301226928711" />
                  <Point X="-28.70313671875" Y="1.305262573242" />
                  <Point X="-28.64506640625" Y="1.323572509766" />
                  <Point X="-28.641720703125" Y="1.324627319336" />
                  <Point X="-28.622779296875" Y="1.3329609375" />
                  <Point X="-28.60403515625" Y="1.343782958984" />
                  <Point X="-28.58735546875" Y="1.356013183594" />
                  <Point X="-28.573716796875" Y="1.371563476562" />
                  <Point X="-28.56130078125" Y="1.389294433594" />
                  <Point X="-28.551349609375" Y="1.407431762695" />
                  <Point X="-28.528048828125" Y="1.463686279297" />
                  <Point X="-28.5267109375" Y="1.466917358398" />
                  <Point X="-28.52091015625" Y="1.486808349609" />
                  <Point X="-28.517154296875" Y="1.508120605469" />
                  <Point X="-28.5158046875" Y="1.5287578125" />
                  <Point X="-28.518951171875" Y="1.549198364258" />
                  <Point X="-28.524552734375" Y="1.570101806641" />
                  <Point X="-28.532048828125" Y="1.589377563477" />
                  <Point X="-28.5601640625" Y="1.64338684082" />
                  <Point X="-28.56180078125" Y="1.646530517578" />
                  <Point X="-28.57329296875" Y="1.663723144531" />
                  <Point X="-28.587203125" Y="1.680296142578" />
                  <Point X="-28.60213671875" Y="1.69458972168" />
                  <Point X="-28.64862109375" Y="1.730259033203" />
                  <Point X="-29.351859375" Y="2.269873046875" />
                  <Point X="-29.094859375" Y="2.710175537109" />
                  <Point X="-29.081146484375" Y="2.733665283203" />
                  <Point X="-28.75050390625" Y="3.158661621094" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.065916015625" Y="2.818720458984" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818763183594" />
                  <Point X="-28.01910546875" Y="2.821588623047" />
                  <Point X="-27.999013671875" Y="2.826505126953" />
                  <Point X="-27.980462890625" Y="2.835653808594" />
                  <Point X="-27.962208984375" Y="2.847282958984" />
                  <Point X="-27.946078125" Y="2.860229003906" />
                  <Point X="-27.888669921875" Y="2.917635986328" />
                  <Point X="-27.885353515625" Y="2.920952392578" />
                  <Point X="-27.872404296875" Y="2.9370859375" />
                  <Point X="-27.860775390625" Y="2.955340332031" />
                  <Point X="-27.85162890625" Y="2.973889160156" />
                  <Point X="-27.8467109375" Y="2.993978027344" />
                  <Point X="-27.843884765625" Y="3.015436767578" />
                  <Point X="-27.84343359375" Y="3.036121582031" />
                  <Point X="-27.850509765625" Y="3.116998535156" />
                  <Point X="-27.85091796875" Y="3.121664794922" />
                  <Point X="-27.854953125" Y="3.141948974609" />
                  <Point X="-27.861462890625" Y="3.162597412109" />
                  <Point X="-27.869794921875" Y="3.181534423828" />
                  <Point X="-27.89039453125" Y="3.217212646484" />
                  <Point X="-28.18333203125" Y="3.724595458984" />
                  <Point X="-27.72448828125" Y="4.076385498047" />
                  <Point X="-27.700615234375" Y="4.094689697266" />
                  <Point X="-27.167037109375" Y="4.391134277344" />
                  <Point X="-27.0431953125" Y="4.229740722656" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.99511328125" Y="4.18939453125" />
                  <Point X="-26.90509765625" Y="4.14253515625" />
                  <Point X="-26.899896484375" Y="4.139827636719" />
                  <Point X="-26.880619140625" Y="4.132331054688" />
                  <Point X="-26.85971484375" Y="4.126729492187" />
                  <Point X="-26.8392734375" Y="4.123583007812" />
                  <Point X="-26.81863671875" Y="4.124934082031" />
                  <Point X="-26.797322265625" Y="4.128690429688" />
                  <Point X="-26.777455078125" Y="4.13448046875" />
                  <Point X="-26.68369921875" Y="4.17331640625" />
                  <Point X="-26.67828125" Y="4.175560058594" />
                  <Point X="-26.660146484375" Y="4.185508789062" />
                  <Point X="-26.642416015625" Y="4.197923828125" />
                  <Point X="-26.6268671875" Y="4.211560058594" />
                  <Point X="-26.61463671875" Y="4.22823828125" />
                  <Point X="-26.6038125" Y="4.246983886719" />
                  <Point X="-26.595478515625" Y="4.265922363281" />
                  <Point X="-26.564962890625" Y="4.362707519531" />
                  <Point X="-26.563205078125" Y="4.368283203125" />
                  <Point X="-26.559166015625" Y="4.388571777344" />
                  <Point X="-26.55727734375" Y="4.410139160156" />
                  <Point X="-26.557728515625" Y="4.430826171875" />
                  <Point X="-26.5600703125" Y="4.448614746094" />
                  <Point X="-26.584201171875" Y="4.6318984375" />
                  <Point X="-25.98053125" Y="4.801146484375" />
                  <Point X="-25.94963671875" Y="4.809808105469" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.843228515625" Y="4.325706054688" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.1829453125" Y="4.834564941406" />
                  <Point X="-24.15595703125" Y="4.83173828125" />
                  <Point X="-23.546865234375" Y="4.684685058594" />
                  <Point X="-23.5189765625" Y="4.677952148438" />
                  <Point X="-23.122591796875" Y="4.534180175781" />
                  <Point X="-23.10534375" Y="4.527924316406" />
                  <Point X="-22.721994140625" Y="4.34864453125" />
                  <Point X="-22.705421875" Y="4.34089453125" />
                  <Point X="-22.335060546875" Y="4.12512109375" />
                  <Point X="-22.3190390625" Y="4.115786621094" />
                  <Point X="-22.05673828125" Y="3.929254150391" />
                  <Point X="-22.852416015625" Y="2.551099365234" />
                  <Point X="-22.857921875" Y="2.53993359375" />
                  <Point X="-22.866921875" Y="2.516056396484" />
                  <Point X="-22.88721875" Y="2.440155273438" />
                  <Point X="-22.88989453125" Y="2.425802001953" />
                  <Point X="-22.89240625" Y="2.402517089844" />
                  <Point X="-22.892271484375" Y="2.380955810547" />
                  <Point X="-22.884357421875" Y="2.315322998047" />
                  <Point X="-22.883900390625" Y="2.311531005859" />
                  <Point X="-22.878560546875" Y="2.289612792969" />
                  <Point X="-22.870294921875" Y="2.267521484375" />
                  <Point X="-22.8599296875" Y="2.247471435547" />
                  <Point X="-22.819318359375" Y="2.187620605469" />
                  <Point X="-22.810140625" Y="2.176123535156" />
                  <Point X="-22.7944921875" Y="2.159365966797" />
                  <Point X="-22.778400390625" Y="2.145593261719" />
                  <Point X="-22.71855078125" Y="2.104981933594" />
                  <Point X="-22.71506640625" Y="2.102619140625" />
                  <Point X="-22.69503515625" Y="2.092265869141" />
                  <Point X="-22.67295703125" Y="2.084004882813" />
                  <Point X="-22.651037109375" Y="2.078663818359" />
                  <Point X="-22.585404296875" Y="2.070749511719" />
                  <Point X="-22.570349609375" Y="2.070137451172" />
                  <Point X="-22.54765234375" Y="2.071017822266" />
                  <Point X="-22.52679296875" Y="2.074171142578" />
                  <Point X="-22.450890625" Y="2.094468261719" />
                  <Point X="-22.442080078125" Y="2.097290283203" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-22.329953125" Y="2.157205566406" />
                  <Point X="-21.032671875" Y="2.906190673828" />
                  <Point X="-20.8862421875" Y="2.702685302734" />
                  <Point X="-20.876724609375" Y="2.689458496094" />
                  <Point X="-20.73780078125" Y="2.459883300781" />
                  <Point X="-21.769216796875" Y="1.668450317383" />
                  <Point X="-21.7785703125" Y="1.660244750977" />
                  <Point X="-21.796025390625" Y="1.64162890625" />
                  <Point X="-21.850650390625" Y="1.570364746094" />
                  <Point X="-21.858427734375" Y="1.558471679688" />
                  <Point X="-21.8700546875" Y="1.537402709961" />
                  <Point X="-21.8783671875" Y="1.517089599609" />
                  <Point X="-21.898716796875" Y="1.444328735352" />
                  <Point X="-21.899892578125" Y="1.44012512207" />
                  <Point X="-21.90334765625" Y="1.417826171875" />
                  <Point X="-21.9041640625" Y="1.394255859375" />
                  <Point X="-21.902259765625" Y="1.371768554688" />
                  <Point X="-21.8855546875" Y="1.290812744141" />
                  <Point X="-21.881609375" Y="1.277034667969" />
                  <Point X="-21.8734453125" Y="1.254978027344" />
                  <Point X="-21.86371484375" Y="1.235739746094" />
                  <Point X="-21.818310546875" Y="1.166727416992" />
                  <Point X="-21.8156875" Y="1.162737304688" />
                  <Point X="-21.801123046875" Y="1.145470825195" />
                  <Point X="-21.783869140625" Y="1.129367919922" />
                  <Point X="-21.765650390625" Y="1.116034301758" />
                  <Point X="-21.69981640625" Y="1.078976196289" />
                  <Point X="-21.6864765625" Y="1.072773803711" />
                  <Point X="-21.66469140625" Y="1.064631713867" />
                  <Point X="-21.64387890625" Y="1.059438354492" />
                  <Point X="-21.554861328125" Y="1.047673339844" />
                  <Point X="-21.545970703125" Y="1.046921020508" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-21.434365234375" Y="1.057178344727" />
                  <Point X="-20.22316015625" Y="1.21663659668" />
                  <Point X="-20.158150390625" Y="0.949598266602" />
                  <Point X="-20.154060546875" Y="0.932795532227" />
                  <Point X="-20.109134765625" Y="0.64423828125" />
                  <Point X="-21.283419921875" Y="0.32958972168" />
                  <Point X="-21.2952109375" Y="0.325585662842" />
                  <Point X="-21.318453125" Y="0.315067932129" />
                  <Point X="-21.40591015625" Y="0.264515991211" />
                  <Point X="-21.417361328125" Y="0.256731567383" />
                  <Point X="-21.436728515625" Y="0.241388305664" />
                  <Point X="-21.452470703125" Y="0.225574478149" />
                  <Point X="-21.5049453125" Y="0.158709762573" />
                  <Point X="-21.5079765625" Y="0.154846847534" />
                  <Point X="-21.519701171875" Y="0.135562789917" />
                  <Point X="-21.52947265625" Y="0.114101913452" />
                  <Point X="-21.536318359375" Y="0.092605056763" />
                  <Point X="-21.553810546875" Y="0.001271131039" />
                  <Point X="-21.555421875" Y="-0.012619204521" />
                  <Point X="-21.556431640625" Y="-0.036705905914" />
                  <Point X="-21.5548203125" Y="-0.058554153442" />
                  <Point X="-21.537330078125" Y="-0.149880493164" />
                  <Point X="-21.536322265625" Y="-0.155143676758" />
                  <Point X="-21.5294765625" Y="-0.176649932861" />
                  <Point X="-21.519703125" Y="-0.198118255615" />
                  <Point X="-21.5079765625" Y="-0.217406707764" />
                  <Point X="-21.455501953125" Y="-0.28427142334" />
                  <Point X="-21.44579296875" Y="-0.294878845215" />
                  <Point X="-21.428447265625" Y="-0.311164733887" />
                  <Point X="-21.410962890625" Y="-0.324155548096" />
                  <Point X="-21.323505859375" Y="-0.374707519531" />
                  <Point X="-21.315861328125" Y="-0.37867578125" />
                  <Point X="-21.283419921875" Y="-0.392149902344" />
                  <Point X="-21.212412109375" Y="-0.411176208496" />
                  <Point X="-20.108525390625" Y="-0.706961914062" />
                  <Point X="-20.142693359375" Y="-0.933590820313" />
                  <Point X="-20.144974609375" Y="-0.948727905273" />
                  <Point X="-20.198822265625" Y="-1.18469921875" />
                  <Point X="-21.5756171875" Y="-1.003440795898" />
                  <Point X="-21.59196484375" Y="-1.002710205078" />
                  <Point X="-21.625341796875" Y="-1.005508789063" />
                  <Point X="-21.796990234375" Y="-1.042817016602" />
                  <Point X="-21.80690625" Y="-1.04497265625" />
                  <Point X="-21.83602734375" Y="-1.056598022461" />
                  <Point X="-21.8638515625" Y="-1.073489624023" />
                  <Point X="-21.8876015625" Y="-1.093959594727" />
                  <Point X="-21.9913515625" Y="-1.218738769531" />
                  <Point X="-21.997345703125" Y="-1.225947753906" />
                  <Point X="-22.012064453125" Y="-1.250327636719" />
                  <Point X="-22.023408203125" Y="-1.277711791992" />
                  <Point X="-22.030240234375" Y="-1.305364379883" />
                  <Point X="-22.045109375" Y="-1.466958740234" />
                  <Point X="-22.04596875" Y="-1.476294677734" />
                  <Point X="-22.043650390625" Y="-1.507563598633" />
                  <Point X="-22.03591796875" Y="-1.539187011719" />
                  <Point X="-22.023546875" Y="-1.567997558594" />
                  <Point X="-21.9285546875" Y="-1.715751831055" />
                  <Point X="-21.922525390625" Y="-1.724098144531" />
                  <Point X="-21.90541796875" Y="-1.74526171875" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.823474609375" Y="-1.811472045898" />
                  <Point X="-20.786876953125" Y="-2.606882080078" />
                  <Point X="-20.8687578125" Y="-2.739377197266" />
                  <Point X="-20.87519921875" Y="-2.749800537109" />
                  <Point X="-20.971017578125" Y="-2.8859453125" />
                  <Point X="-22.199044921875" Y="-2.176942871094" />
                  <Point X="-22.21387109375" Y="-2.170011474609" />
                  <Point X="-22.245775390625" Y="-2.159825195312" />
                  <Point X="-22.4500625" Y="-2.122930908203" />
                  <Point X="-22.461865234375" Y="-2.120799560547" />
                  <Point X="-22.49321875" Y="-2.120395507812" />
                  <Point X="-22.525390625" Y="-2.125353271484" />
                  <Point X="-22.555166015625" Y="-2.135177001953" />
                  <Point X="-22.72487890625" Y="-2.224495849609" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.7576171875" Y="-2.24655078125" />
                  <Point X="-22.778576171875" Y="-2.267510986328" />
                  <Point X="-22.795466796875" Y="-2.290439208984" />
                  <Point X="-22.884787109375" Y="-2.46015234375" />
                  <Point X="-22.889947265625" Y="-2.469957275391" />
                  <Point X="-22.899771484375" Y="-2.499735595703" />
                  <Point X="-22.904728515625" Y="-2.531907958984" />
                  <Point X="-22.90432421875" Y="-2.563258544922" />
                  <Point X="-22.8674296875" Y="-2.767546386719" />
                  <Point X="-22.865234375" Y="-2.776939453125" />
                  <Point X="-22.85719921875" Y="-2.804855712891" />
                  <Point X="-22.8481796875" Y="-2.826078369141" />
                  <Point X="-22.8058359375" Y="-2.899420410156" />
                  <Point X="-22.13871484375" Y="-4.054906005859" />
                  <Point X="-22.210513671875" Y="-4.106189453125" />
                  <Point X="-22.2181640625" Y="-4.111653320313" />
                  <Point X="-22.298232421875" Y="-4.163481445312" />
                  <Point X="-23.241451171875" Y="-2.934254882812" />
                  <Point X="-23.252494140625" Y="-2.922178710938" />
                  <Point X="-23.27807421875" Y="-2.900556884766" />
                  <Point X="-23.47955859375" Y="-2.771021728516" />
                  <Point X="-23.49119921875" Y="-2.763538085938" />
                  <Point X="-23.52001171875" Y="-2.751166503906" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.803255859375" Y="-2.761394042969" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397949219" />
                  <Point X="-23.871021484375" Y="-2.780741455078" />
                  <Point X="-23.89540234375" Y="-2.795461425781" />
                  <Point X="-24.065556640625" Y="-2.936938232422" />
                  <Point X="-24.07538671875" Y="-2.945111816406" />
                  <Point X="-24.095857421875" Y="-2.968860351563" />
                  <Point X="-24.11275" Y="-2.996686035156" />
                  <Point X="-24.124375" Y="-3.025808105469" />
                  <Point X="-24.17525" Y="-3.259873046875" />
                  <Point X="-24.176751953125" Y="-3.268816650391" />
                  <Point X="-24.180404296875" Y="-3.299486328125" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.1682578125" Y="-3.414269042969" />
                  <Point X="-23.977935546875" Y="-4.859915039062" />
                  <Point X="-24.017103515625" Y="-4.868500976562" />
                  <Point X="-24.024328125" Y="-4.870084960938" />
                  <Point X="-24.07068359375" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.049142578125" Y="-4.754439453125" />
                  <Point X="-26.058421875" Y="-4.752637695312" />
                  <Point X="-26.141244140625" Y="-4.731328613281" />
                  <Point X="-26.1207734375" Y="-4.575838378906" />
                  <Point X="-26.120076171875" Y="-4.568098632812" />
                  <Point X="-26.11944921875" Y="-4.541030761719" />
                  <Point X="-26.1217578125" Y="-4.509322753906" />
                  <Point X="-26.123333984375" Y="-4.497687011719" />
                  <Point X="-26.180556640625" Y="-4.210015136719" />
                  <Point X="-26.188126953125" Y="-4.178463378906" />
                  <Point X="-26.19903125" Y="-4.149493652344" />
                  <Point X="-26.205671875" Y="-4.135456054688" />
                  <Point X="-26.221744140625" Y="-4.10762109375" />
                  <Point X="-26.230578125" Y="-4.094857910156" />
                  <Point X="-26.250208984375" Y="-4.0709375" />
                  <Point X="-26.261005859375" Y="-4.059780029297" />
                  <Point X="-26.481525390625" Y="-3.866387695312" />
                  <Point X="-26.50673828125" Y="-3.845965332031" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.929494140625" Y="-3.776986328125" />
                  <Point X="-26.961931640625" Y="-3.776132568359" />
                  <Point X="-26.992736328125" Y="-3.779167236328" />
                  <Point X="-27.008013671875" Y="-3.781947509766" />
                  <Point X="-27.039064453125" Y="-3.790268798828" />
                  <Point X="-27.053681640625" Y="-3.795499755859" />
                  <Point X="-27.081869140625" Y="-3.808272216797" />
                  <Point X="-27.095439453125" Y="-3.815813720703" />
                  <Point X="-27.33931640625" Y="-3.978767089844" />
                  <Point X="-27.351640625" Y="-3.987735839844" />
                  <Point X="-27.377615234375" Y="-4.008284179688" />
                  <Point X="-27.386345703125" Y="-4.016114013672" />
                  <Point X="-27.402767578125" Y="-4.03278125" />
                  <Point X="-27.410458984375" Y="-4.041618408203" />
                  <Point X="-27.422744140625" Y="-4.057626220703" />
                  <Point X="-27.503201171875" Y="-4.162479980469" />
                  <Point X="-27.7339765625" Y="-4.019589355469" />
                  <Point X="-27.747587890625" Y="-4.011161132812" />
                  <Point X="-27.98086328125" Y="-3.831547607422" />
                  <Point X="-27.34148828125" Y="-2.724119140625" />
                  <Point X="-27.33484765625" Y="-2.710080322266" />
                  <Point X="-27.323947265625" Y="-2.681116210938" />
                  <Point X="-27.319685546875" Y="-2.666190429688" />
                  <Point X="-27.3134140625" Y="-2.634664550781" />
                  <Point X="-27.311638671875" Y="-2.619240478516" />
                  <Point X="-27.310625" Y="-2.588305908203" />
                  <Point X="-27.3146640625" Y="-2.557618408203" />
                  <Point X="-27.3236484375" Y="-2.528" />
                  <Point X="-27.32935546875" Y="-2.51355859375" />
                  <Point X="-27.343572265625" Y="-2.484729980469" />
                  <Point X="-27.351556640625" Y="-2.471410644531" />
                  <Point X="-27.369583984375" Y="-2.446254638672" />
                  <Point X="-27.379625" Y="-2.434419921875" />
                  <Point X="-27.395962890625" Y="-2.418079833984" />
                  <Point X="-27.40878125" Y="-2.407050537109" />
                  <Point X="-27.43394921875" Y="-2.389009521484" />
                  <Point X="-27.447271484375" Y="-2.381022705078" />
                  <Point X="-27.4761015625" Y="-2.366801757812" />
                  <Point X="-27.490541015625" Y="-2.36109375" />
                  <Point X="-27.52016015625" Y="-2.352105957031" />
                  <Point X="-27.550849609375" Y="-2.348063476562" />
                  <Point X="-27.581787109375" Y="-2.349074707031" />
                  <Point X="-27.59721484375" Y="-2.350848632812" />
                  <Point X="-27.628744140625" Y="-2.357119140625" />
                  <Point X="-27.64367578125" Y="-2.3613828125" />
                  <Point X="-27.67264453125" Y="-2.372285400391" />
                  <Point X="-27.686681640625" Y="-2.378924316406" />
                  <Point X="-27.757107421875" Y="-2.419583984375" />
                  <Point X="-28.793087890625" Y="-3.017707763672" />
                  <Point X="-28.993265625" Y="-2.754715576172" />
                  <Point X="-29.004017578125" Y="-2.740589111328" />
                  <Point X="-29.181265625" Y="-2.443374267578" />
                  <Point X="-28.048123046875" Y="-1.5738828125" />
                  <Point X="-28.036482421875" Y="-1.563309570312" />
                  <Point X="-28.015107421875" Y="-1.540391479492" />
                  <Point X="-28.005373046875" Y="-1.528047363281" />
                  <Point X="-27.987408203125" Y="-1.500918334961" />
                  <Point X="-27.979841796875" Y="-1.487136962891" />
                  <Point X="-27.967083984375" Y="-1.458509155273" />
                  <Point X="-27.961892578125" Y="-1.443662353516" />
                  <Point X="-27.954607421875" Y="-1.415539794922" />
                  <Point X="-27.951541015625" Y="-1.398752197266" />
                  <Point X="-27.948740234375" Y="-1.368291870117" />
                  <Point X="-27.94857421875" Y="-1.352945556641" />
                  <Point X="-27.950794921875" Y="-1.321288452148" />
                  <Point X="-27.953095703125" Y="-1.306141357422" />
                  <Point X="-27.960103515625" Y="-1.276410400391" />
                  <Point X="-27.971802734375" Y="-1.248189697266" />
                  <Point X="-27.987884765625" Y="-1.222219482422" />
                  <Point X="-27.996974609375" Y="-1.209886352539" />
                  <Point X="-28.017802734375" Y="-1.185942016602" />
                  <Point X="-28.028759765625" Y="-1.175229125977" />
                  <Point X="-28.05225" Y="-1.155706787109" />
                  <Point X="-28.06478125" Y="-1.146897583008" />
                  <Point X="-28.089796875" Y="-1.132173583984" />
                  <Point X="-28.10541796875" Y="-1.124488647461" />
                  <Point X="-28.134712890625" Y="-1.113250488281" />
                  <Point X="-28.149826171875" Y="-1.108849365234" />
                  <Point X="-28.181703125" Y="-1.102373901367" />
                  <Point X="-28.197314453125" Y="-1.100529785156" />
                  <Point X="-28.228626953125" Y="-1.099441162109" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-28.333232421875" Y="-1.111901123047" />
                  <Point X="-29.66091796875" Y="-1.286694335938" />
                  <Point X="-29.7365546875" Y="-0.990584533691" />
                  <Point X="-29.7407578125" Y="-0.974121643066" />
                  <Point X="-29.78644921875" Y="-0.654654296875" />
                  <Point X="-28.508287109375" Y="-0.312171356201" />
                  <Point X="-28.50047265625" Y="-0.309711517334" />
                  <Point X="-28.4739296875" Y="-0.299250396729" />
                  <Point X="-28.444166015625" Y="-0.283893188477" />
                  <Point X="-28.433560546875" Y="-0.277514434814" />
                  <Point X="-28.407322265625" Y="-0.259304443359" />
                  <Point X="-28.393666015625" Y="-0.248240356445" />
                  <Point X="-28.37120703125" Y="-0.226351623535" />
                  <Point X="-28.360888671875" Y="-0.214475021362" />
                  <Point X="-28.3416484375" Y="-0.188221969604" />
                  <Point X="-28.33343359375" Y="-0.174810546875" />
                  <Point X="-28.319328125" Y="-0.146814300537" />
                  <Point X="-28.3134375" Y="-0.132229492187" />
                  <Point X="-28.30469140625" Y="-0.104050598145" />
                  <Point X="-28.300953125" Y="-0.088352310181" />
                  <Point X="-28.296703125" Y="-0.060214878082" />
                  <Point X="-28.29563671875" Y="-0.045988994598" />
                  <Point X="-28.2956484375" Y="-0.016383468628" />
                  <Point X="-28.296724609375" Y="-0.002161596537" />
                  <Point X="-28.300994140625" Y="0.025960674286" />
                  <Point X="-28.3041875" Y="0.039861225128" />
                  <Point X="-28.31290234375" Y="0.067940620422" />
                  <Point X="-28.31926953125" Y="0.084096298218" />
                  <Point X="-28.333375" Y="0.112133850098" />
                  <Point X="-28.34159375" Y="0.125566085815" />
                  <Point X="-28.360845703125" Y="0.151856292725" />
                  <Point X="-28.371173828125" Y="0.163749389648" />
                  <Point X="-28.393654296875" Y="0.185667388916" />
                  <Point X="-28.405806640625" Y="0.195692306519" />
                  <Point X="-28.432044921875" Y="0.213902328491" />
                  <Point X="-28.438279296875" Y="0.217878433228" />
                  <Point X="-28.4615390625" Y="0.230628036499" />
                  <Point X="-28.495443359375" Y="0.245162918091" />
                  <Point X="-28.508287109375" Y="0.249611053467" />
                  <Point X="-28.589326171875" Y="0.271325744629" />
                  <Point X="-29.7854453125" Y="0.591824768066" />
                  <Point X="-29.7340390625" Y="0.939227416992" />
                  <Point X="-29.73133203125" Y="0.957525878906" />
                  <Point X="-29.6335859375" Y="1.318236938477" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.76773828125" Y="1.204815551758" />
                  <Point X="-28.747060546875" Y="1.204364135742" />
                  <Point X="-28.736708984375" Y="1.204702636719" />
                  <Point X="-28.715150390625" Y="1.206588012695" />
                  <Point X="-28.704896484375" Y="1.208051879883" />
                  <Point X="-28.68460546875" Y="1.212087524414" />
                  <Point X="-28.674568359375" Y="1.214659667969" />
                  <Point X="-28.616498046875" Y="1.232969726562" />
                  <Point X="-28.603462890625" Y="1.237671386719" />
                  <Point X="-28.584521484375" Y="1.246004882812" />
                  <Point X="-28.575279296875" Y="1.250688842773" />
                  <Point X="-28.55653515625" Y="1.261510742188" />
                  <Point X="-28.547859375" Y="1.267171264648" />
                  <Point X="-28.5311796875" Y="1.279401367188" />
                  <Point X="-28.51593359375" Y="1.293371704102" />
                  <Point X="-28.502294921875" Y="1.308921875" />
                  <Point X="-28.4958984375" Y="1.317071777344" />
                  <Point X="-28.483482421875" Y="1.334802856445" />
                  <Point X="-28.478013671875" Y="1.343598022461" />
                  <Point X="-28.4680625" Y="1.361735351562" />
                  <Point X="-28.463580078125" Y="1.371077636719" />
                  <Point X="-28.440279296875" Y="1.42733215332" />
                  <Point X="-28.435509765625" Y="1.440320556641" />
                  <Point X="-28.429708984375" Y="1.460211547852" />
                  <Point X="-28.4273515625" Y="1.470320556641" />
                  <Point X="-28.423595703125" Y="1.4916328125" />
                  <Point X="-28.422357421875" Y="1.501921142578" />
                  <Point X="-28.4210078125" Y="1.522558349609" />
                  <Point X="-28.42191015625" Y="1.543211303711" />
                  <Point X="-28.425056640625" Y="1.563651855469" />
                  <Point X="-28.427189453125" Y="1.573788208008" />
                  <Point X="-28.432791015625" Y="1.59469152832" />
                  <Point X="-28.43601171875" Y="1.604533935547" />
                  <Point X="-28.4435078125" Y="1.623809814453" />
                  <Point X="-28.447783203125" Y="1.633243408203" />
                  <Point X="-28.4758984375" Y="1.687252685547" />
                  <Point X="-28.4828203125" Y="1.699323852539" />
                  <Point X="-28.4943125" Y="1.716516357422" />
                  <Point X="-28.50052734375" Y="1.724797485352" />
                  <Point X="-28.5144375" Y="1.741370605469" />
                  <Point X="-28.521515625" Y="1.74892565918" />
                  <Point X="-28.53644921875" Y="1.763219360352" />
                  <Point X="-28.5443046875" Y="1.769957763672" />
                  <Point X="-28.5907890625" Y="1.805627075195" />
                  <Point X="-29.22761328125" Y="2.294280273438" />
                  <Point X="-29.0128125" Y="2.662285888672" />
                  <Point X="-29.002283203125" Y="2.680320800781" />
                  <Point X="-28.726337890625" Y="3.035012451172" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.0741953125" Y="2.72408203125" />
                  <Point X="-28.059169921875" Y="2.723334472656" />
                  <Point X="-28.038490234375" Y="2.723785888672" />
                  <Point X="-28.0281640625" Y="2.724576171875" />
                  <Point X="-28.006705078125" Y="2.727401611328" />
                  <Point X="-27.996525390625" Y="2.729311279297" />
                  <Point X="-27.97643359375" Y="2.734227783203" />
                  <Point X="-27.956994140625" Y="2.741302978516" />
                  <Point X="-27.938443359375" Y="2.750451660156" />
                  <Point X="-27.929419921875" Y="2.755531982422" />
                  <Point X="-27.911166015625" Y="2.767161132812" />
                  <Point X="-27.902748046875" Y="2.773193115234" />
                  <Point X="-27.8866171875" Y="2.786139160156" />
                  <Point X="-27.878904296875" Y="2.793053222656" />
                  <Point X="-27.82149609375" Y="2.850460205078" />
                  <Point X="-27.811265625" Y="2.861487792969" />
                  <Point X="-27.79831640625" Y="2.877621337891" />
                  <Point X="-27.79228125" Y="2.886043701172" />
                  <Point X="-27.78065234375" Y="2.904298095703" />
                  <Point X="-27.7755703125" Y="2.913325927734" />
                  <Point X="-27.766423828125" Y="2.931874755859" />
                  <Point X="-27.759353515625" Y="2.951299316406" />
                  <Point X="-27.754435546875" Y="2.971388183594" />
                  <Point X="-27.7525234375" Y="2.981573486328" />
                  <Point X="-27.749697265625" Y="3.003032226562" />
                  <Point X="-27.748908203125" Y="3.013365234375" />
                  <Point X="-27.74845703125" Y="3.034050048828" />
                  <Point X="-27.748794921875" Y="3.044401855469" />
                  <Point X="-27.75587109375" Y="3.125278808594" />
                  <Point X="-27.757744140625" Y="3.140199951172" />
                  <Point X="-27.761779296875" Y="3.160484130859" />
                  <Point X="-27.764349609375" Y="3.170513427734" />
                  <Point X="-27.770859375" Y="3.191161865234" />
                  <Point X="-27.7745078125" Y="3.200856689453" />
                  <Point X="-27.78283984375" Y="3.219793701172" />
                  <Point X="-27.7875234375" Y="3.229035888672" />
                  <Point X="-27.808123046875" Y="3.264714111328" />
                  <Point X="-28.05938671875" Y="3.699914794922" />
                  <Point X="-27.666685546875" Y="4.000993652344" />
                  <Point X="-27.648359375" Y="4.015045654297" />
                  <Point X="-27.192525390625" Y="4.268296386719" />
                  <Point X="-27.118564453125" Y="4.171908203125" />
                  <Point X="-27.1118203125" Y="4.164047363281" />
                  <Point X="-27.097517578125" Y="4.149106445313" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.06509375" Y="4.121898925781" />
                  <Point X="-27.04789453125" Y="4.11040625" />
                  <Point X="-27.03898046875" Y="4.105128417969" />
                  <Point X="-26.94896484375" Y="4.058269287109" />
                  <Point X="-26.934328125" Y="4.051286865234" />
                  <Point X="-26.91505078125" Y="4.043790283203" />
                  <Point X="-26.905208984375" Y="4.040568359375" />
                  <Point X="-26.8843046875" Y="4.034966796875" />
                  <Point X="-26.87416796875" Y="4.032835205078" />
                  <Point X="-26.8537265625" Y="4.029688720703" />
                  <Point X="-26.83306640625" Y="4.028785888672" />
                  <Point X="-26.8124296875" Y="4.030136962891" />
                  <Point X="-26.8021484375" Y="4.031375976562" />
                  <Point X="-26.780833984375" Y="4.035132324219" />
                  <Point X="-26.7707421875" Y="4.037484863281" />
                  <Point X="-26.750875" Y="4.043274902344" />
                  <Point X="-26.741099609375" Y="4.046712158203" />
                  <Point X="-26.64734375" Y="4.085548095703" />
                  <Point X="-26.632587890625" Y="4.092270263672" />
                  <Point X="-26.614453125" Y="4.102219238281" />
                  <Point X="-26.60565625" Y="4.107689453125" />
                  <Point X="-26.58792578125" Y="4.120104492188" />
                  <Point X="-26.57977734375" Y="4.126499511719" />
                  <Point X="-26.564228515625" Y="4.140135742188" />
                  <Point X="-26.5502578125" Y="4.155381347656" />
                  <Point X="-26.53802734375" Y="4.172059570313" />
                  <Point X="-26.5323671875" Y="4.180733398438" />
                  <Point X="-26.52154296875" Y="4.199479003906" />
                  <Point X="-26.516859375" Y="4.208719726563" />
                  <Point X="-26.508525390625" Y="4.227658203125" />
                  <Point X="-26.504875" Y="4.237355957031" />
                  <Point X="-26.474359375" Y="4.334141113281" />
                  <Point X="-26.470033203125" Y="4.349734375" />
                  <Point X="-26.465994140625" Y="4.370022949219" />
                  <Point X="-26.46452734375" Y="4.380284179688" />
                  <Point X="-26.462638671875" Y="4.4018515625" />
                  <Point X="-26.46230078125" Y="4.412210449219" />
                  <Point X="-26.462751953125" Y="4.432897460938" />
                  <Point X="-26.4658828125" Y="4.461014160156" />
                  <Point X="-26.479265625" Y="4.562655273438" />
                  <Point X="-25.954884765625" Y="4.709673339844" />
                  <Point X="-25.93117578125" Y="4.7163203125" />
                  <Point X="-25.36522265625" Y="4.782556152344" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.75146484375" Y="4.301118164063" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.19283984375" Y="4.740081542969" />
                  <Point X="-24.17212109375" Y="4.737911621094" />
                  <Point X="-23.56916015625" Y="4.592338378906" />
                  <Point X="-23.546408203125" Y="4.586845703125" />
                  <Point X="-23.154984375" Y="4.444873046875" />
                  <Point X="-23.141728515625" Y="4.440064941406" />
                  <Point X="-22.76223828125" Y="4.262590332031" />
                  <Point X="-22.74954296875" Y="4.256653808594" />
                  <Point X="-22.3828828125" Y="4.043035888672" />
                  <Point X="-22.370587890625" Y="4.035873046875" />
                  <Point X="-22.18221875" Y="3.901915283203" />
                  <Point X="-22.9346875" Y="2.598599365234" />
                  <Point X="-22.93762109375" Y="2.593113769531" />
                  <Point X="-22.94681640625" Y="2.573440673828" />
                  <Point X="-22.95581640625" Y="2.549563476562" />
                  <Point X="-22.958697265625" Y="2.540598144531" />
                  <Point X="-22.978994140625" Y="2.464697021484" />
                  <Point X="-22.980609375" Y="2.457565429688" />
                  <Point X="-22.984345703125" Y="2.435990478516" />
                  <Point X="-22.986857421875" Y="2.412705566406" />
                  <Point X="-22.987404296875" Y="2.401923339844" />
                  <Point X="-22.98726953125" Y="2.380362060547" />
                  <Point X="-22.986587890625" Y="2.369583007812" />
                  <Point X="-22.978673828125" Y="2.303950195312" />
                  <Point X="-22.976201171875" Y="2.289044189453" />
                  <Point X="-22.970861328125" Y="2.267125976563" />
                  <Point X="-22.967537109375" Y="2.256321777344" />
                  <Point X="-22.959271484375" Y="2.23423046875" />
                  <Point X="-22.954685546875" Y="2.22389453125" />
                  <Point X="-22.9443203125" Y="2.203844482422" />
                  <Point X="-22.938541015625" Y="2.194130371094" />
                  <Point X="-22.8979296875" Y="2.134279541016" />
                  <Point X="-22.893564453125" Y="2.128353027344" />
                  <Point X="-22.87957421875" Y="2.111285400391" />
                  <Point X="-22.86392578125" Y="2.094527832031" />
                  <Point X="-22.856265625" Y="2.087191650391" />
                  <Point X="-22.840173828125" Y="2.073418945312" />
                  <Point X="-22.8317421875" Y="2.066982421875" />
                  <Point X="-22.771892578125" Y="2.02637121582" />
                  <Point X="-22.758685546875" Y="2.018225097656" />
                  <Point X="-22.738654296875" Y="2.007871948242" />
                  <Point X="-22.728328125" Y="2.003290283203" />
                  <Point X="-22.70625" Y="1.995029418945" />
                  <Point X="-22.695447265625" Y="1.991705322266" />
                  <Point X="-22.67352734375" Y="1.986364257812" />
                  <Point X="-22.66241015625" Y="1.984347045898" />
                  <Point X="-22.59677734375" Y="1.976432739258" />
                  <Point X="-22.589263671875" Y="1.975827758789" />
                  <Point X="-22.56666796875" Y="1.975208740234" />
                  <Point X="-22.543970703125" Y="1.976089233398" />
                  <Point X="-22.533453125" Y="1.977084960938" />
                  <Point X="-22.51259375" Y="1.98023828125" />
                  <Point X="-22.502251953125" Y="1.982395874023" />
                  <Point X="-22.426349609375" Y="2.002692993164" />
                  <Point X="-22.405302734375" Y="2.009698242188" />
                  <Point X="-22.3746875" Y="2.022552856445" />
                  <Point X="-22.36396484375" Y="2.027872436523" />
                  <Point X="-22.282453125" Y="2.074933105469" />
                  <Point X="-21.05959375" Y="2.780950683594" />
                  <Point X="-20.96335546875" Y="2.647199951172" />
                  <Point X="-20.95604296875" Y="2.637037841797" />
                  <Point X="-20.863115234375" Y="2.483471435547" />
                  <Point X="-21.827048828125" Y="1.743818847656" />
                  <Point X="-21.8318671875" Y="1.739864624023" />
                  <Point X="-21.84787109375" Y="1.725224731445" />
                  <Point X="-21.865326171875" Y="1.706608764648" />
                  <Point X="-21.871423828125" Y="1.699422729492" />
                  <Point X="-21.926048828125" Y="1.628158691406" />
                  <Point X="-21.930158203125" Y="1.622358642578" />
                  <Point X="-21.941603515625" Y="1.604372192383" />
                  <Point X="-21.95323046875" Y="1.583303222656" />
                  <Point X="-21.957978515625" Y="1.573382446289" />
                  <Point X="-21.966291015625" Y="1.553069335938" />
                  <Point X="-21.96985546875" Y="1.542677124023" />
                  <Point X="-21.990205078125" Y="1.469916259766" />
                  <Point X="-21.993771484375" Y="1.454671142578" />
                  <Point X="-21.9972265625" Y="1.432372192383" />
                  <Point X="-21.998291015625" Y="1.421114746094" />
                  <Point X="-21.999107421875" Y="1.397544433594" />
                  <Point X="-21.998826171875" Y="1.386239746094" />
                  <Point X="-21.996921875" Y="1.363752319336" />
                  <Point X="-21.995298828125" Y="1.352569946289" />
                  <Point X="-21.97859375" Y="1.271614135742" />
                  <Point X="-21.976884765625" Y="1.264660766602" />
                  <Point X="-21.970703125" Y="1.244057739258" />
                  <Point X="-21.9625390625" Y="1.222001098633" />
                  <Point X="-21.95821875" Y="1.212100585938" />
                  <Point X="-21.94848828125" Y="1.192862426758" />
                  <Point X="-21.943078125" Y="1.183525024414" />
                  <Point X="-21.897693359375" Y="1.114542236328" />
                  <Point X="-21.8883046875" Y="1.101484741211" />
                  <Point X="-21.873740234375" Y="1.084218261719" />
                  <Point X="-21.86594140625" Y="1.076019042969" />
                  <Point X="-21.8486875" Y="1.059916137695" />
                  <Point X="-21.839974609375" Y="1.052705688477" />
                  <Point X="-21.821755859375" Y="1.039372070312" />
                  <Point X="-21.81225" Y="1.033248901367" />
                  <Point X="-21.746416015625" Y="0.996190673828" />
                  <Point X="-21.739869140625" Y="0.992832275391" />
                  <Point X="-21.719734375" Y="0.983785888672" />
                  <Point X="-21.69794921875" Y="0.975643737793" />
                  <Point X="-21.68769140625" Y="0.972458007812" />
                  <Point X="-21.66687890625" Y="0.967264648438" />
                  <Point X="-21.656326171875" Y="0.965257385254" />
                  <Point X="-21.56730859375" Y="0.953492248535" />
                  <Point X="-21.545794921875" Y="0.951921142578" />
                  <Point X="-21.511619140625" Y="0.951984619141" />
                  <Point X="-21.49939453125" Y="0.952797302246" />
                  <Point X="-21.42196484375" Y="0.962991027832" />
                  <Point X="-20.295296875" Y="1.111319946289" />
                  <Point X="-20.250455078125" Y="0.92712713623" />
                  <Point X="-20.2473125" Y="0.914215698242" />
                  <Point X="-20.21612890625" Y="0.713920776367" />
                  <Point X="-21.3080078125" Y="0.421352661133" />
                  <Point X="-21.313966796875" Y="0.419544494629" />
                  <Point X="-21.334376953125" Y="0.412136108398" />
                  <Point X="-21.357619140625" Y="0.401618347168" />
                  <Point X="-21.365994140625" Y="0.397316467285" />
                  <Point X="-21.453451171875" Y="0.34676449585" />
                  <Point X="-21.459318359375" Y="0.343081604004" />
                  <Point X="-21.476353515625" Y="0.331195495605" />
                  <Point X="-21.495720703125" Y="0.315852264404" />
                  <Point X="-21.504056640625" Y="0.308410705566" />
                  <Point X="-21.519798828125" Y="0.292596954346" />
                  <Point X="-21.527205078125" Y="0.284224731445" />
                  <Point X="-21.5796796875" Y="0.217360031128" />
                  <Point X="-21.589150390625" Y="0.204200363159" />
                  <Point X="-21.600875" Y="0.184916259766" />
                  <Point X="-21.60616015625" Y="0.174929244995" />
                  <Point X="-21.615931640625" Y="0.153468399048" />
                  <Point X="-21.619994140625" Y="0.142928359985" />
                  <Point X="-21.62683984375" Y="0.121431549072" />
                  <Point X="-21.629623046875" Y="0.110474624634" />
                  <Point X="-21.647115234375" Y="0.019140668869" />
                  <Point X="-21.648177734375" Y="0.012218111992" />
                  <Point X="-21.650337890625" Y="-0.008640067101" />
                  <Point X="-21.65134765625" Y="-0.032726783752" />
                  <Point X="-21.651173828125" Y="-0.043693218231" />
                  <Point X="-21.6495625" Y="-0.065541526794" />
                  <Point X="-21.648125" Y="-0.076423240662" />
                  <Point X="-21.630634765625" Y="-0.167749465942" />
                  <Point X="-21.62684765625" Y="-0.183958724976" />
                  <Point X="-21.620001953125" Y="-0.205465057373" />
                  <Point X="-21.6159375" Y="-0.216011627197" />
                  <Point X="-21.6061640625" Y="-0.237479904175" />
                  <Point X="-21.60087890625" Y="-0.247469436646" />
                  <Point X="-21.58915234375" Y="-0.266758026123" />
                  <Point X="-21.5827109375" Y="-0.276056915283" />
                  <Point X="-21.530236328125" Y="-0.342921600342" />
                  <Point X="-21.525580078125" Y="-0.348413208008" />
                  <Point X="-21.510818359375" Y="-0.364136474609" />
                  <Point X="-21.49347265625" Y="-0.380422424316" />
                  <Point X="-21.48510546875" Y="-0.387420318604" />
                  <Point X="-21.46762109375" Y="-0.400411132812" />
                  <Point X="-21.45850390625" Y="-0.406404052734" />
                  <Point X="-21.371046875" Y="-0.456956054688" />
                  <Point X="-21.35230078125" Y="-0.466409515381" />
                  <Point X="-21.319859375" Y="-0.479883514404" />
                  <Point X="-21.3080078125" Y="-0.483912963867" />
                  <Point X="-21.237" Y="-0.502939239502" />
                  <Point X="-20.215119140625" Y="-0.776751220703" />
                  <Point X="-20.236630859375" Y="-0.919428100586" />
                  <Point X="-20.238380859375" Y="-0.931044250488" />
                  <Point X="-20.2721953125" Y="-1.079219848633" />
                  <Point X="-21.563216796875" Y="-0.909253479004" />
                  <Point X="-21.571375" Y="-0.908535522461" />
                  <Point X="-21.59990234375" Y="-0.908042358398" />
                  <Point X="-21.633279296875" Y="-0.910840942383" />
                  <Point X="-21.64551953125" Y="-0.912676269531" />
                  <Point X="-21.81716796875" Y="-0.94998449707" />
                  <Point X="-21.84212890625" Y="-0.956743286133" />
                  <Point X="-21.87125" Y="-0.968368713379" />
                  <Point X="-21.885326171875" Y="-0.975390991211" />
                  <Point X="-21.913150390625" Y="-0.992282714844" />
                  <Point X="-21.925873046875" Y="-1.001529418945" />
                  <Point X="-21.949623046875" Y="-1.021999389648" />
                  <Point X="-21.960650390625" Y="-1.03322253418" />
                  <Point X="-22.064400390625" Y="-1.158001708984" />
                  <Point X="-22.078673828125" Y="-1.176848144531" />
                  <Point X="-22.093392578125" Y="-1.201227905273" />
                  <Point X="-22.09983203125" Y="-1.213970336914" />
                  <Point X="-22.11117578125" Y="-1.241354492188" />
                  <Point X="-22.115634765625" Y="-1.25492565918" />
                  <Point X="-22.122466796875" Y="-1.28257824707" />
                  <Point X="-22.12483984375" Y="-1.296659667969" />
                  <Point X="-22.139708984375" Y="-1.45825402832" />
                  <Point X="-22.140708984375" Y="-1.483318969727" />
                  <Point X="-22.138390625" Y="-1.514587890625" />
                  <Point X="-22.135931640625" Y="-1.530127807617" />
                  <Point X="-22.12819921875" Y="-1.561751220703" />
                  <Point X="-22.1232109375" Y="-1.576670043945" />
                  <Point X="-22.11083984375" Y="-1.60548059082" />
                  <Point X="-22.10345703125" Y="-1.619372192383" />
                  <Point X="-22.00846484375" Y="-1.767126586914" />
                  <Point X="-21.99640625" Y="-1.783819091797" />
                  <Point X="-21.979298828125" Y="-1.804982788086" />
                  <Point X="-21.971736328125" Y="-1.813281982422" />
                  <Point X="-21.9556875" Y="-1.828929443359" />
                  <Point X="-21.947201171875" Y="-1.83627746582" />
                  <Point X="-21.881306640625" Y="-1.886840454102" />
                  <Point X="-20.912828125" Y="-2.629980957031" />
                  <Point X="-20.9495703125" Y="-2.689435302734" />
                  <Point X="-20.95451953125" Y="-2.697443359375" />
                  <Point X="-20.998724609375" Y="-2.760252441406" />
                  <Point X="-22.151544921875" Y="-2.094670410156" />
                  <Point X="-22.158810546875" Y="-2.090883300781" />
                  <Point X="-22.1849765625" Y="-2.079512207031" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228890625" Y="-2.066337646484" />
                  <Point X="-22.433177734375" Y="-2.029443237305" />
                  <Point X="-22.460640625" Y="-2.025807495117" />
                  <Point X="-22.491994140625" Y="-2.025403442383" />
                  <Point X="-22.5076875" Y="-2.02650378418" />
                  <Point X="-22.539859375" Y="-2.031461547852" />
                  <Point X="-22.55515625" Y="-2.03513659668" />
                  <Point X="-22.584931640625" Y="-2.044960327148" />
                  <Point X="-22.59941015625" Y="-2.051109130859" />
                  <Point X="-22.769123046875" Y="-2.140427978516" />
                  <Point X="-22.791029296875" Y="-2.153169921875" />
                  <Point X="-22.813962890625" Y="-2.170064697266" />
                  <Point X="-22.824794921875" Y="-2.179377685547" />
                  <Point X="-22.84575390625" Y="-2.200337890625" />
                  <Point X="-22.8550625" Y="-2.211165527344" />
                  <Point X="-22.871953125" Y="-2.23409375" />
                  <Point X="-22.87953515625" Y="-2.246194335938" />
                  <Point X="-22.96885546875" Y="-2.415907470703" />
                  <Point X="-22.9801640625" Y="-2.440193603516" />
                  <Point X="-22.98998828125" Y="-2.469971923828" />
                  <Point X="-22.9936640625" Y="-2.485269042969" />
                  <Point X="-22.99862109375" Y="-2.51744140625" />
                  <Point X="-22.999720703125" Y="-2.533133056641" />
                  <Point X="-22.99931640625" Y="-2.564483642578" />
                  <Point X="-22.9978125" Y="-2.580142578125" />
                  <Point X="-22.96091796875" Y="-2.784430419922" />
                  <Point X="-22.95652734375" Y="-2.803216552734" />
                  <Point X="-22.9484921875" Y="-2.8311328125" />
                  <Point X="-22.944630859375" Y="-2.842013671875" />
                  <Point X="-22.935611328125" Y="-2.863236328125" />
                  <Point X="-22.930453125" Y="-2.873578125" />
                  <Point X="-22.888109375" Y="-2.946920166016" />
                  <Point X="-22.26410546875" Y="-4.027723632812" />
                  <Point X="-22.276244140625" Y="-4.036082275391" />
                  <Point X="-23.16608203125" Y="-2.876422607422" />
                  <Point X="-23.17134375" Y="-2.870145751953" />
                  <Point X="-23.19116796875" Y="-2.849625" />
                  <Point X="-23.216748046875" Y="-2.828003173828" />
                  <Point X="-23.22669921875" Y="-2.820646728516" />
                  <Point X="-23.42818359375" Y="-2.691111572266" />
                  <Point X="-23.453716796875" Y="-2.676244873047" />
                  <Point X="-23.482529296875" Y="-2.663873291016" />
                  <Point X="-23.49744921875" Y="-2.658884765625" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.8119609375" Y="-2.666793701172" />
                  <Point X="-23.838775390625" Y="-2.670339355469" />
                  <Point X="-23.86642578125" Y="-2.677171630859" />
                  <Point X="-23.8799921875" Y="-2.681629882812" />
                  <Point X="-23.907376953125" Y="-2.692973388672" />
                  <Point X="-23.920123046875" Y="-2.699414550781" />
                  <Point X="-23.94450390625" Y="-2.714134521484" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-24.12629296875" Y="-2.863890136719" />
                  <Point X="-24.14734375" Y="-2.883086425781" />
                  <Point X="-24.167814453125" Y="-2.906834960938" />
                  <Point X="-24.177064453125" Y="-2.919560791016" />
                  <Point X="-24.19395703125" Y="-2.947386474609" />
                  <Point X="-24.20098046875" Y="-2.961466064453" />
                  <Point X="-24.21260546875" Y="-2.990588134766" />
                  <Point X="-24.21720703125" Y="-3.005630615234" />
                  <Point X="-24.26808203125" Y="-3.239695556641" />
                  <Point X="-24.2710859375" Y="-3.257582763672" />
                  <Point X="-24.27473828125" Y="-3.288252441406" />
                  <Point X="-24.27540234375" Y="-3.300075195312" />
                  <Point X="-24.275255859375" Y="-3.323708984375" />
                  <Point X="-24.2744453125" Y="-3.335520019531" />
                  <Point X="-24.2624453125" Y="-3.426668945312" />
                  <Point X="-24.166912109375" Y="-4.152318847656" />
                  <Point X="-24.344931640625" Y="-3.487937255859" />
                  <Point X="-24.347392578125" Y="-3.480121582031" />
                  <Point X="-24.357857421875" Y="-3.453570556641" />
                  <Point X="-24.37321484375" Y="-3.423810302734" />
                  <Point X="-24.379591796875" Y="-3.413208251953" />
                  <Point X="-24.534376953125" Y="-3.190192138672" />
                  <Point X="-24.55333203125" Y="-3.165168701172" />
                  <Point X="-24.57521875" Y="-3.142711914062" />
                  <Point X="-24.58709375" Y="-3.132394287109" />
                  <Point X="-24.61334765625" Y="-3.11315234375" />
                  <Point X="-24.626759765625" Y="-3.104936767578" />
                  <Point X="-24.6547578125" Y="-3.090829833984" />
                  <Point X="-24.66934375" Y="-3.084938476562" />
                  <Point X="-24.908865234375" Y="-3.010599853516" />
                  <Point X="-24.936623046875" Y="-3.003109375" />
                  <Point X="-24.96478125" Y="-2.998840087891" />
                  <Point X="-24.97901953125" Y="-2.997766601562" />
                  <Point X="-25.008658203125" Y="-2.997766113281" />
                  <Point X="-25.022900390625" Y="-2.998839355469" />
                  <Point X="-25.0510625" Y="-3.003108886719" />
                  <Point X="-25.064982421875" Y="-3.006305175781" />
                  <Point X="-25.304501953125" Y="-3.080643554688" />
                  <Point X="-25.332927734375" Y="-3.090829345703" />
                  <Point X="-25.360927734375" Y="-3.104937011719" />
                  <Point X="-25.374341796875" Y="-3.113153808594" />
                  <Point X="-25.4006015625" Y="-3.132400634766" />
                  <Point X="-25.4124765625" Y="-3.142718994141" />
                  <Point X="-25.434361328125" Y="-3.165174804688" />
                  <Point X="-25.44437109375" Y="-3.177312255859" />
                  <Point X="-25.59915625" Y="-3.400328125" />
                  <Point X="-25.6082734375" Y="-3.414821289062" />
                  <Point X="-25.62443359375" Y="-3.443271484375" />
                  <Point X="-25.629724609375" Y="-3.454140380859" />
                  <Point X="-25.638884765625" Y="-3.476472900391" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.6646328125" Y="-3.56958984375" />
                  <Point X="-25.985427734375" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.884128625244" Y="2.832193075219" />
                  <Point X="-29.150103133338" Y="2.234804549119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.562360241495" Y="1.308859923895" />
                  <Point X="-29.737577321092" Y="0.915315919704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.697301148464" Y="3.018248091563" />
                  <Point X="-29.072592985426" Y="2.175328824801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.464127723983" Y="1.295927403739" />
                  <Point X="-29.778449058241" Y="0.589950128174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.614575544256" Y="2.970486473879" />
                  <Point X="-28.995082837514" Y="2.115853100483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.365895206472" Y="1.282994883584" />
                  <Point X="-29.685542263835" Y="0.565055838076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.531849940047" Y="2.922724856195" />
                  <Point X="-28.917572689601" Y="2.056377376165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.26766268896" Y="1.270062363429" />
                  <Point X="-29.592635469429" Y="0.540161547979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.449124335839" Y="2.874963238512" />
                  <Point X="-28.840062541689" Y="1.996901651847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.169430171448" Y="1.257129843274" />
                  <Point X="-29.499728675023" Y="0.515267257881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.935580369374" Y="3.794835505328" />
                  <Point X="-28.01334624351" Y="3.620170492263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.36639873163" Y="2.827201620828" />
                  <Point X="-28.762552393777" Y="1.937425927529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.071197653937" Y="1.244197323119" />
                  <Point X="-29.406821880616" Y="0.490372967783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.777695898392" Y="3.915883466303" />
                  <Point X="-27.954632969808" Y="3.518476297235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.283673127421" Y="2.779440003145" />
                  <Point X="-28.685042245865" Y="1.877950203211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.972965136425" Y="1.231264802964" />
                  <Point X="-29.31391508621" Y="0.465478677686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.623375663571" Y="4.028926021788" />
                  <Point X="-27.895919696106" Y="3.416782102206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.197347333373" Y="2.739764544233" />
                  <Point X="-28.607532097953" Y="1.818474478892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.874732618913" Y="1.218332282809" />
                  <Point X="-29.221008291804" Y="0.440584387588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.698100015874" Y="-0.630981169198" />
                  <Point X="-29.767531204076" Y="-0.786926171155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.485208327975" Y="4.105688571611" />
                  <Point X="-27.837206422404" Y="3.315087907178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.099358988982" Y="2.726283602272" />
                  <Point X="-28.530624868088" Y="1.757644578469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.776462967471" Y="1.205483166827" />
                  <Point X="-29.128101497398" Y="0.415690097491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.58002313732" Y="-0.599342524697" />
                  <Point X="-29.742247669551" Y="-0.963704789718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.347040992378" Y="4.182451121434" />
                  <Point X="-27.779263074072" Y="3.211664431456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.993714266459" Y="2.729999167146" />
                  <Point X="-28.466237632775" Y="1.668694309871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.66737737823" Y="1.216927044885" />
                  <Point X="-29.035194702991" Y="0.390795807393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.461946258766" Y="-0.567703880197" />
                  <Point X="-29.705082178623" Y="-1.113796097253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.208873656781" Y="4.259213671257" />
                  <Point X="-27.748950586898" Y="3.046181025477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.847803812365" Y="2.824153045859" />
                  <Point X="-28.421571087084" Y="1.535450647177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.537700252121" Y="1.274620271983" />
                  <Point X="-28.942287908585" Y="0.365901517296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.343869380212" Y="-0.536065235696" />
                  <Point X="-29.667171294301" Y="-1.262213223815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.134504894344" Y="4.192682279641" />
                  <Point X="-28.849381114179" Y="0.341007227198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.225792501657" Y="-0.504426591195" />
                  <Point X="-29.568673590862" Y="-1.274550126625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.062731893649" Y="4.120320711696" />
                  <Point X="-28.756474319772" Y="0.316112937101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.107715623103" Y="-0.472787946694" />
                  <Point X="-29.458208157795" Y="-1.260007068591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.979330611108" Y="4.074076692394" />
                  <Point X="-28.663567525366" Y="0.291218647003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.989638744549" Y="-0.441149302194" />
                  <Point X="-29.347742724728" Y="-1.245464010557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.89185252158" Y="4.036989331505" />
                  <Point X="-28.570660762927" Y="0.266324285108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.871561865995" Y="-0.409510657693" />
                  <Point X="-29.237277291661" Y="-1.230920952523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.789357677697" Y="4.033630153122" />
                  <Point X="-28.479193576077" Y="0.238196583499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.753484987441" Y="-0.377872013192" />
                  <Point X="-29.126811858594" Y="-1.216377894489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.445631660197" Y="4.572085061696" />
                  <Point X="-26.472547429388" Y="4.511631254294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.66562301798" Y="4.077976382175" />
                  <Point X="-28.39726461573" Y="0.188645674405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.635408108887" Y="-0.346233368692" />
                  <Point X="-29.016346425528" Y="-1.201834836455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.326808987945" Y="4.605398786268" />
                  <Point X="-28.330173078966" Y="0.105769366315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.517331230333" Y="-0.314594724191" />
                  <Point X="-28.905880992461" Y="-1.187291778421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.207986315693" Y="4.638712510839" />
                  <Point X="-28.29601873392" Y="-0.051085085602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.376233552083" Y="-0.231250517009" />
                  <Point X="-28.795415559394" Y="-1.172748720387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.089163643442" Y="4.67202623541" />
                  <Point X="-28.684950126327" Y="-1.158205662353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.97034097119" Y="4.705339959982" />
                  <Point X="-28.57448469326" Y="-1.143662604319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.138565802931" Y="-2.410609520106" />
                  <Point X="-29.165170094192" Y="-2.470363736621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.857629482914" Y="4.724927740611" />
                  <Point X="-28.464019260193" Y="-1.129119546285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.980612938166" Y="-2.289407944179" />
                  <Point X="-29.105630380024" Y="-2.57020171597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.747922541429" Y="4.73776719866" />
                  <Point X="-28.353553827126" Y="-1.114576488251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.822660073401" Y="-2.168206368253" />
                  <Point X="-29.046090665855" Y="-2.670039695319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.638215599944" Y="4.750606656708" />
                  <Point X="-28.243135465902" Y="-1.100139155307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.664707208636" Y="-2.047004792326" />
                  <Point X="-28.984769854091" Y="-2.765877263973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.528508658459" Y="4.763446114757" />
                  <Point X="-28.143803873379" Y="-1.110603112569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.50675434387" Y="-1.925803216399" />
                  <Point X="-28.919158194248" Y="-2.852077430048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.418801716973" Y="4.776285572806" />
                  <Point X="-28.058072510313" Y="-1.151613685326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.348801479105" Y="-1.804601640472" />
                  <Point X="-28.853546534405" Y="-2.938277596123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.345233669316" Y="4.70795574635" />
                  <Point X="-27.986507801759" Y="-1.22444308508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.19084861434" Y="-1.683400064545" />
                  <Point X="-28.782094894682" Y="-3.011360952629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.306163414209" Y="4.562142609205" />
                  <Point X="-27.949306914987" Y="-1.374454892247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.030566211576" Y="-1.556966260607" />
                  <Point X="-28.64212469051" Y="-2.930549093686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.267093159103" Y="4.41632947206" />
                  <Point X="-28.502154486337" Y="-2.849737234742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.228022903996" Y="4.270516334916" />
                  <Point X="-28.362184282164" Y="-2.768925375799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.171086741078" Y="4.164830683717" />
                  <Point X="-28.222214077991" Y="-2.688113516856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.092654932103" Y="4.107425044037" />
                  <Point X="-28.082243873818" Y="-2.607301657912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.998598322458" Y="4.08511328125" />
                  <Point X="-27.942273669646" Y="-2.526489798969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.584724469176" Y="4.781122808599" />
                  <Point X="-24.680468761452" Y="4.566077607256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.881849007835" Y="4.113770168342" />
                  <Point X="-27.802303465473" Y="-2.445677940025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.485366857379" Y="4.770717291583" />
                  <Point X="-27.664225885846" Y="-2.369116985411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.386009245582" Y="4.760311774567" />
                  <Point X="-27.550861993547" Y="-2.348063881354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.286651633785" Y="4.749906257551" />
                  <Point X="-27.458975385784" Y="-2.375249548168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.187294032244" Y="4.739500717499" />
                  <Point X="-27.3808042019" Y="-2.433240561385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.09256294882" Y="4.718703847622" />
                  <Point X="-27.321766143644" Y="-2.534205278362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.624796684045" Y="-3.214823015718" />
                  <Point X="-27.920182224958" Y="-3.878269803089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.998665691707" Y="4.696034173186" />
                  <Point X="-27.842739801461" Y="-3.937897638934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.904768434594" Y="4.673364498751" />
                  <Point X="-27.765297377965" Y="-3.99752547478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.810871177482" Y="4.650694824315" />
                  <Point X="-27.684711539237" Y="-4.050093084422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.716973920369" Y="4.62802514988" />
                  <Point X="-27.603193561143" Y="-4.100567074768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.623076663256" Y="4.605355475445" />
                  <Point X="-27.521675583049" Y="-4.151041065115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.52998022219" Y="4.580887138718" />
                  <Point X="-27.341783025203" Y="-3.980562131719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.440448143087" Y="4.548413113949" />
                  <Point X="-27.193662442873" Y="-3.881444223715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.350916063984" Y="4.51593908918" />
                  <Point X="-27.050975954045" Y="-3.794531489547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.26138398488" Y="4.48346506441" />
                  <Point X="-26.939061780199" Y="-3.776734506446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.171851905777" Y="4.450991039641" />
                  <Point X="-26.837857589463" Y="-3.78299253926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.084617604762" Y="4.413356120787" />
                  <Point X="-26.736815745705" Y="-3.789615209355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.998548347239" Y="4.373104471405" />
                  <Point X="-26.635809874494" Y="-3.796318675115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.912479089717" Y="4.332852822023" />
                  <Point X="-26.544110511675" Y="-3.82392490096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.826409832195" Y="4.292601172641" />
                  <Point X="-26.465343919004" Y="-3.880578604146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.740714277903" Y="4.251510172062" />
                  <Point X="-26.390555339409" Y="-3.946167070987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.658142353755" Y="4.20340338331" />
                  <Point X="-26.315766759814" Y="-4.011755537829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.575570429607" Y="4.155296594559" />
                  <Point X="-26.242375571418" Y="-4.08048259669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.49299850546" Y="4.107189805807" />
                  <Point X="-26.185984532874" Y="-4.187392617281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.410426581312" Y="4.059083017055" />
                  <Point X="-26.153230212616" Y="-4.347391576357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.329712069133" Y="4.006804412718" />
                  <Point X="-26.121698552587" Y="-4.510136675269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.25072931175" Y="3.950636223424" />
                  <Point X="-22.576391244398" Y="3.219187546837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.979783575247" Y="2.313153537438" />
                  <Point X="-25.40663439626" Y="-3.137642651337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.763910906863" Y="-3.940098832603" />
                  <Point X="-26.118764211449" Y="-4.737112404044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.932650647991" Y="2.185449458437" />
                  <Point X="-25.272899370489" Y="-3.070835232376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.921089355423" Y="-4.526693775014" />
                  <Point X="-26.024608495757" Y="-4.759201571007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.867457327311" Y="2.098309687218" />
                  <Point X="-25.15223515487" Y="-3.033385333679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.790019572728" Y="2.038671364821" />
                  <Point X="-25.033585053352" Y="-3.000459209323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.705554989453" Y="1.994815558068" />
                  <Point X="-24.931401840997" Y="-3.004518323578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.60908816146" Y="1.977917234324" />
                  <Point X="-24.839679347753" Y="-3.032072597638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.503190913704" Y="1.982199980158" />
                  <Point X="-24.74831399757" Y="-3.060429028146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.382739448646" Y="2.019172033271" />
                  <Point X="-24.657386197296" Y="-3.089768211838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.244051274878" Y="2.09710440478" />
                  <Point X="-24.576479932299" Y="-3.141616132296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.104081126944" Y="2.177916137408" />
                  <Point X="-24.509850437992" Y="-3.225530204736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.964110979011" Y="2.258727870035" />
                  <Point X="-24.446499196338" Y="-3.316807353188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.824140831078" Y="2.339539602663" />
                  <Point X="-24.151604529957" Y="-2.888029454948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.22983872946" Y="-3.063746344007" />
                  <Point X="-24.383147954684" Y="-3.40808450164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.684170683144" Y="2.420351335291" />
                  <Point X="-23.984303326078" Y="-2.745831165595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.268147598721" Y="-3.383355840015" />
                  <Point X="-24.33357715112" Y="-3.530313020802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.544200535211" Y="2.501163067918" />
                  <Point X="-21.951303025215" Y="1.586795904622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.965972308646" Y="1.553848154589" />
                  <Point X="-23.847681562163" Y="-2.672540026608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.24441555363" Y="-3.563619160898" />
                  <Point X="-24.294506869661" Y="-3.676126098758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.404230387278" Y="2.581974800546" />
                  <Point X="-21.751638601882" Y="1.801683174987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.981458782478" Y="1.285498597984" />
                  <Point X="-23.738106854316" Y="-2.659997570172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.220683533801" Y="-3.743882538525" />
                  <Point X="-24.255436588202" Y="-3.821939176713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.264260239344" Y="2.662786533174" />
                  <Point X="-21.593685754981" Y="1.922884710791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.931025940216" Y="1.165206249436" />
                  <Point X="-23.629673863163" Y="-2.650019451418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.196951513973" Y="-3.924145916151" />
                  <Point X="-24.216366306742" Y="-3.967752254669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.124290091411" Y="2.743598265801" />
                  <Point X="-21.43573290808" Y="2.044086246594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.866488233773" Y="1.076593944532" />
                  <Point X="-23.526471277473" Y="-2.651789015675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.173219494145" Y="-4.104409293777" />
                  <Point X="-24.177296025283" Y="-4.113565332624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.02504630676" Y="2.73293708883" />
                  <Point X="-21.277780061179" Y="2.165287782398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.787899094698" Y="1.019541674044" />
                  <Point X="-23.43755815402" Y="-2.685653237596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.960805954284" Y="2.643656915978" />
                  <Point X="-21.119827214277" Y="2.286489318202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.70266804159" Y="0.977407386724" />
                  <Point X="-23.356512893282" Y="-2.737188968509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.900574089077" Y="2.545373533316" />
                  <Point X="-20.961874367376" Y="2.407690854005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.606990261052" Y="0.958736833379" />
                  <Point X="-23.275664455446" Y="-2.789166770895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.505834889019" Y="0.952369151964" />
                  <Point X="-23.19656184642" Y="-2.845065768992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.395568013376" Y="0.966466242721" />
                  <Point X="-22.782244521409" Y="-2.148060187831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.991119649068" Y="-2.617201405707" />
                  <Point X="-23.128397937398" Y="-2.925533489555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.285102571679" Y="0.981009320139" />
                  <Point X="-22.646007401633" Y="-2.075632973723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.961110273797" Y="-2.783365612165" />
                  <Point X="-23.062590924208" Y="-3.011294884827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.174637129982" Y="0.995552397557" />
                  <Point X="-21.467757492596" Y="0.337193283945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.648991395213" Y="-0.069864726012" />
                  <Point X="-22.521060782534" Y="-2.028564639331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.911726402338" Y="-2.906013987709" />
                  <Point X="-22.996783911017" Y="-3.0970562801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.064171688284" Y="1.010095474975" />
                  <Point X="-21.32963382831" Y="0.413857746397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.612970969279" Y="-0.222527891631" />
                  <Point X="-21.989206477106" Y="-1.067566677859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.133601952338" Y="-1.391884225216" />
                  <Point X="-22.418631181664" Y="-2.03207035592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.853013203103" Y="-3.007708349995" />
                  <Point X="-22.930976897827" Y="-3.182817675373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.953706246587" Y="1.024638552393" />
                  <Point X="-21.210697430208" Y="0.447426903411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.55087465111" Y="-0.316623644379" />
                  <Point X="-21.835016875061" Y="-0.954817528392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.117649730921" Y="-1.589621316167" />
                  <Point X="-22.322380150066" Y="-2.049453366305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.794299903145" Y="-3.109402486051" />
                  <Point X="-22.865169884636" Y="-3.268579070646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.84324080489" Y="1.039181629811" />
                  <Point X="-21.092620569591" Y="0.479065507624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.480070498079" Y="-0.391161279806" />
                  <Point X="-21.719414904462" Y="-0.928737618169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.058227691114" Y="-1.689723596459" />
                  <Point X="-22.226204629975" Y="-2.067005978309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.735586603187" Y="-3.211096622108" />
                  <Point X="-22.799362871446" Y="-3.354340465918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.732775363193" Y="1.053724707229" />
                  <Point X="-20.974543708975" Y="0.510704111837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.39834783002" Y="-0.441175528962" />
                  <Point X="-21.606454953074" Y="-0.908591780254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.996228926989" Y="-1.784038459179" />
                  <Point X="-22.138010300584" Y="-2.102484638127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.676873303229" Y="-3.312790758164" />
                  <Point X="-22.733555858255" Y="-3.440101861191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.622309921496" Y="1.068267784647" />
                  <Point X="-20.856466848358" Y="0.54234271605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.312678319077" Y="-0.482325023862" />
                  <Point X="-21.506106654889" Y="-0.916772179211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.923570072411" Y="-1.854410366725" />
                  <Point X="-22.055284730989" Y="-2.150246333554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.618160003271" Y="-3.41448489422" />
                  <Point X="-22.667748845064" Y="-3.525863256464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.511844479798" Y="1.082810862064" />
                  <Point X="-20.738389987741" Y="0.573981320263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.219905287849" Y="-0.507519750977" />
                  <Point X="-21.407874157905" Y="-0.929704745473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.846059973851" Y="-1.913886201891" />
                  <Point X="-21.972559161394" Y="-2.19800802898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.559446703313" Y="-3.516179030277" />
                  <Point X="-22.601941831874" Y="-3.611624651737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.401379038101" Y="1.097353939482" />
                  <Point X="-20.620313127124" Y="0.605619924476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.126998491451" Y="-0.532414036602" />
                  <Point X="-21.309641660922" Y="-0.942637311735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.76854981535" Y="-1.973361902426" />
                  <Point X="-21.889833591799" Y="-2.245769724407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.500733403355" Y="-3.617873166333" />
                  <Point X="-22.536134818683" Y="-3.697386047009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.293838196507" Y="1.105328257521" />
                  <Point X="-20.502236266507" Y="0.637258528689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.034091695054" Y="-0.557308322228" />
                  <Point X="-21.211409163938" Y="-0.955569877996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.69103965685" Y="-2.032837602962" />
                  <Point X="-21.807108022204" Y="-2.293531419834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.442020103396" Y="-3.719567302389" />
                  <Point X="-22.470327805493" Y="-3.783147442282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.257077228537" Y="0.954328376546" />
                  <Point X="-20.38415940589" Y="0.668897132902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.941184898657" Y="-0.582202607853" />
                  <Point X="-21.113176666954" Y="-0.968502444258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.613529498349" Y="-2.092313303498" />
                  <Point X="-21.724382452609" Y="-2.341293115261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.383306803438" Y="-3.821261438445" />
                  <Point X="-22.404520792302" Y="-3.868908837555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.227527137327" Y="0.787132601195" />
                  <Point X="-20.266082545273" Y="0.700535737116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.84827810226" Y="-0.607096893479" />
                  <Point X="-21.01494416997" Y="-0.98143501052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.536019339848" Y="-2.151789004033" />
                  <Point X="-21.641656883013" Y="-2.389054810688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.32459350348" Y="-3.922955574502" />
                  <Point X="-22.338713779112" Y="-3.954670232828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.755371305862" Y="-0.631991179105" />
                  <Point X="-20.916711672987" Y="-0.994367576781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.458509181347" Y="-2.211264704569" />
                  <Point X="-21.558931313418" Y="-2.436816506114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.265880203522" Y="-4.024649710558" />
                  <Point X="-22.268638573676" Y="-4.030845111359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.662464509465" Y="-0.65688546473" />
                  <Point X="-20.818479176003" Y="-1.007300143043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.380999022846" Y="-2.270740405104" />
                  <Point X="-21.476205743823" Y="-2.484578201541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.569557713068" Y="-0.681779750356" />
                  <Point X="-20.720246679019" Y="-1.020232709305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.303488864346" Y="-2.33021610564" />
                  <Point X="-21.393480174228" Y="-2.532339896968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.47665091667" Y="-0.706674035981" />
                  <Point X="-20.622014182035" Y="-1.033165275566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.225978705845" Y="-2.389691806175" />
                  <Point X="-21.310754604633" Y="-2.580101592395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.383744120273" Y="-0.731568321607" />
                  <Point X="-20.523781685052" Y="-1.046097841828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.148468547344" Y="-2.449167506711" />
                  <Point X="-21.228029035038" Y="-2.627863287821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.290837323876" Y="-0.756462607233" />
                  <Point X="-20.425549188068" Y="-1.05903040809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.070958388843" Y="-2.508643207247" />
                  <Point X="-21.145303465443" Y="-2.675624983248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.224970298963" Y="-0.842089213969" />
                  <Point X="-20.327316691084" Y="-1.071962974351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.993448230342" Y="-2.568118907782" />
                  <Point X="-21.062577895848" Y="-2.723386678675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.915938071841" Y="-2.627594608318" />
                  <Point X="-20.927753417136" Y="-2.654132308346" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001625976562" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.1497578125" Y="-4.950448730469" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.535681640625" Y="-3.521542236328" />
                  <Point X="-24.690466796875" Y="-3.298526123047" />
                  <Point X="-24.69941015625" Y="-3.285641845703" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.965185546875" Y="-3.192061279297" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.008662109375" Y="-3.187766113281" />
                  <Point X="-25.248181640625" Y="-3.262104492188" />
                  <Point X="-25.262021484375" Y="-3.266399414062" />
                  <Point X="-25.28828125" Y="-3.285646240234" />
                  <Point X="-25.44306640625" Y="-3.508662109375" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.48110546875" Y="-3.618765625" />
                  <Point X="-25.847744140625" Y="-4.987077148438" />
                  <Point X="-26.085345703125" Y="-4.940958007812" />
                  <Point X="-26.100240234375" Y="-4.93806640625" />
                  <Point X="-26.351587890625" Y="-4.873396484375" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.534755371094" />
                  <Point X="-26.366904296875" Y="-4.247083496094" />
                  <Point X="-26.3702109375" Y="-4.230463867188" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.606802734375" Y="-4.009236816406" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.941919921875" Y="-3.966579589844" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.989880859375" Y="-3.973792724609" />
                  <Point X="-27.2337578125" Y="-4.13674609375" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.272017578125" Y="-4.173302246094" />
                  <Point X="-27.457095703125" Y="-4.414500488281" />
                  <Point X="-27.834" Y="-4.181130371094" />
                  <Point X="-27.855830078125" Y="-4.167612792969" />
                  <Point X="-28.228580078125" Y="-3.880608154297" />
                  <Point X="-28.20980859375" Y="-3.848094482422" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.59759375" />
                  <Point X="-27.513978515625" Y="-2.568765136719" />
                  <Point X="-27.530375" Y="-2.552367919922" />
                  <Point X="-27.531322265625" Y="-2.551420166016" />
                  <Point X="-27.56015234375" Y="-2.53719921875" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-27.662107421875" Y="-2.584129394531" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-29.144453125" Y="-2.869791748047" />
                  <Point X="-29.161697265625" Y="-2.847136474609" />
                  <Point X="-29.43101953125" Y="-2.395526855469" />
                  <Point X="-29.3951640625" Y="-2.368014892578" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.39601574707" />
                  <Point X="-28.138537109375" Y="-1.367893188477" />
                  <Point X="-28.138109375" Y="-1.366241210938" />
                  <Point X="-28.140330078125" Y="-1.334584106445" />
                  <Point X="-28.161158203125" Y="-1.310639770508" />
                  <Point X="-28.186193359375" Y="-1.295904418945" />
                  <Point X="-28.187650390625" Y="-1.295046386719" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.308431640625" Y="-1.300275634766" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.92064453125" Y="-1.037606811523" />
                  <Point X="-29.927390625" Y="-1.01119152832" />
                  <Point X="-29.99839453125" Y="-0.5147421875" />
                  <Point X="-29.9601875" Y="-0.504504608154" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.541892578125" Y="-0.121423377991" />
                  <Point X="-28.515654296875" Y="-0.103213386536" />
                  <Point X="-28.514138671875" Y="-0.102161346436" />
                  <Point X="-28.4948984375" Y="-0.075908363342" />
                  <Point X="-28.48615234375" Y="-0.047729434967" />
                  <Point X="-28.48563671875" Y="-0.046064220428" />
                  <Point X="-28.4856484375" Y="-0.016458742142" />
                  <Point X="-28.4943828125" Y="0.011683167458" />
                  <Point X="-28.49488671875" Y="0.013311170578" />
                  <Point X="-28.514138671875" Y="0.039601272583" />
                  <Point X="-28.540376953125" Y="0.057811260223" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-28.638501953125" Y="0.087800071716" />
                  <Point X="-29.998185546875" Y="0.452126037598" />
                  <Point X="-29.9219921875" Y="0.967039672852" />
                  <Point X="-29.91764453125" Y="0.99642010498" />
                  <Point X="-29.773515625" Y="1.528298706055" />
                  <Point X="-29.7587734375" Y="1.526357910156" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731705078125" Y="1.395865722656" />
                  <Point X="-28.673634765625" Y="1.414175537109" />
                  <Point X="-28.670279296875" Y="1.415233154297" />
                  <Point X="-28.65153515625" Y="1.426055175781" />
                  <Point X="-28.639119140625" Y="1.443786132813" />
                  <Point X="-28.615818359375" Y="1.500040405273" />
                  <Point X="-28.61446875" Y="1.503296142578" />
                  <Point X="-28.610712890625" Y="1.524608398438" />
                  <Point X="-28.616314453125" Y="1.54551171875" />
                  <Point X="-28.6444296875" Y="1.599520996094" />
                  <Point X="-28.64605859375" Y="1.602648681641" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-28.706453125" Y="1.654890991211" />
                  <Point X="-29.47610546875" Y="2.245465820312" />
                  <Point X="-29.17690625" Y="2.758065185547" />
                  <Point X="-29.160009765625" Y="2.787010253906" />
                  <Point X="-28.774669921875" Y="3.282310791016" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.05763671875" Y="2.913358886719" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775634766" />
                  <Point X="-28.013251953125" Y="2.927404785156" />
                  <Point X="-27.95584375" Y="2.984811767578" />
                  <Point X="-27.95252734375" Y="2.988128173828" />
                  <Point X="-27.9408984375" Y="3.006382568359" />
                  <Point X="-27.938072265625" Y="3.027841308594" />
                  <Point X="-27.9451484375" Y="3.108718261719" />
                  <Point X="-27.945556640625" Y="3.113384521484" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-27.972666015625" Y="3.169711181641" />
                  <Point X="-28.30727734375" Y="3.749276123047" />
                  <Point X="-27.782291015625" Y="4.15177734375" />
                  <Point X="-27.75287109375" Y="4.174333984375" />
                  <Point X="-27.141548828125" Y="4.513971679688" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.86123046875" Y="4.226801269531" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835125" Y="4.2184921875" />
                  <Point X="-26.813810546875" Y="4.222248535156" />
                  <Point X="-26.7200546875" Y="4.261084472656" />
                  <Point X="-26.71463671875" Y="4.263328125" />
                  <Point X="-26.69690625" Y="4.275743164062" />
                  <Point X="-26.68608203125" Y="4.294488769531" />
                  <Point X="-26.65556640625" Y="4.391273925781" />
                  <Point X="-26.6538046875" Y="4.396859375" />
                  <Point X="-26.651916015625" Y="4.418426757812" />
                  <Point X="-26.6542578125" Y="4.436215332031" />
                  <Point X="-26.68913671875" Y="4.701141113281" />
                  <Point X="-26.006177734375" Y="4.892619140625" />
                  <Point X="-25.96809375" Y="4.903296386719" />
                  <Point X="-25.224462890625" Y="4.990327148438" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.9349921875" Y="4.350293945312" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.17305078125" Y="4.929048339844" />
                  <Point X="-24.13979296875" Y="4.925564941406" />
                  <Point X="-23.5245703125" Y="4.777031738281" />
                  <Point X="-23.491544921875" Y="4.76905859375" />
                  <Point X="-23.09019921875" Y="4.623487304688" />
                  <Point X="-23.0689609375" Y="4.615784179688" />
                  <Point X="-22.68175" Y="4.434698730469" />
                  <Point X="-22.661298828125" Y="4.425134277344" />
                  <Point X="-22.28723828125" Y="4.207206054688" />
                  <Point X="-22.267484375" Y="4.195696289062" />
                  <Point X="-21.9312578125" Y="3.956592285156" />
                  <Point X="-21.955578125" Y="3.914469970703" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491514648438" />
                  <Point X="-22.795443359375" Y="2.415613525391" />
                  <Point X="-22.795443359375" Y="2.41561328125" />
                  <Point X="-22.797955078125" Y="2.392328613281" />
                  <Point X="-22.790041015625" Y="2.326695800781" />
                  <Point X="-22.789583984375" Y="2.322903808594" />
                  <Point X="-22.781318359375" Y="2.3008125" />
                  <Point X="-22.74070703125" Y="2.240961669922" />
                  <Point X="-22.74069921875" Y="2.240949951172" />
                  <Point X="-22.72505859375" Y="2.224204101562" />
                  <Point X="-22.665208984375" Y="2.183592773438" />
                  <Point X="-22.6617421875" Y="2.181241455078" />
                  <Point X="-22.6396640625" Y="2.17298046875" />
                  <Point X="-22.57403125" Y="2.165066162109" />
                  <Point X="-22.551333984375" Y="2.165946533203" />
                  <Point X="-22.475431640625" Y="2.186243652344" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-22.377453125" Y="2.239478027344" />
                  <Point X="-21.00575" Y="3.031430908203" />
                  <Point X="-20.80912890625" Y="2.758170898438" />
                  <Point X="-20.79740625" Y="2.741879638672" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-20.638927734375" Y="2.416005859375" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.720626953125" Y="1.583835083008" />
                  <Point X="-21.775251953125" Y="1.512570922852" />
                  <Point X="-21.78687890625" Y="1.491502075195" />
                  <Point X="-21.807228515625" Y="1.418741210938" />
                  <Point X="-21.808404296875" Y="1.414537597656" />
                  <Point X="-21.809220703125" Y="1.390967163086" />
                  <Point X="-21.792515625" Y="1.310011352539" />
                  <Point X="-21.7843515625" Y="1.287954589844" />
                  <Point X="-21.738927734375" Y="1.218912719727" />
                  <Point X="-21.7363046875" Y="1.214922607422" />
                  <Point X="-21.71905078125" Y="1.198819702148" />
                  <Point X="-21.653216796875" Y="1.16176159668" />
                  <Point X="-21.653216796875" Y="1.161761108398" />
                  <Point X="-21.631431640625" Y="1.153619384766" />
                  <Point X="-21.5424140625" Y="1.141854370117" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-21.446765625" Y="1.151365600586" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.065845703125" Y="0.972069274902" />
                  <Point X="-20.06080859375" Y="0.951375061035" />
                  <Point X="-20.002140625" Y="0.574556152344" />
                  <Point X="-20.029775390625" Y="0.567151245117" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819351196" />
                  <Point X="-21.358369140625" Y="0.182267471313" />
                  <Point X="-21.377736328125" Y="0.166924255371" />
                  <Point X="-21.4302109375" Y="0.100059577942" />
                  <Point X="-21.4332421875" Y="0.096196586609" />
                  <Point X="-21.443013671875" Y="0.074735565186" />
                  <Point X="-21.460505859375" Y="-0.016598384857" />
                  <Point X="-21.4605078125" Y="-0.016605688095" />
                  <Point X="-21.461515625" Y="-0.040685081482" />
                  <Point X="-21.444025390625" Y="-0.132011474609" />
                  <Point X="-21.443015625" Y="-0.137288192749" />
                  <Point X="-21.4332421875" Y="-0.158756515503" />
                  <Point X="-21.380767578125" Y="-0.225621185303" />
                  <Point X="-21.363421875" Y="-0.241907058716" />
                  <Point X="-21.27596484375" Y="-0.292458953857" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-21.18782421875" Y="-0.31941317749" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.048755859375" Y="-0.947753295898" />
                  <Point X="-20.051568359375" Y="-0.966412902832" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-20.162658203125" Y="-1.285280273438" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.6051640625" Y="-1.098341186523" />
                  <Point X="-21.7768125" Y="-1.135649536133" />
                  <Point X="-21.786728515625" Y="-1.137805053711" />
                  <Point X="-21.814552734375" Y="-1.154696655273" />
                  <Point X="-21.918302734375" Y="-1.279475830078" />
                  <Point X="-21.924296875" Y="-1.286684814453" />
                  <Point X="-21.935640625" Y="-1.314069091797" />
                  <Point X="-21.950509765625" Y="-1.475663452148" />
                  <Point X="-21.951369140625" Y="-1.484999389648" />
                  <Point X="-21.94363671875" Y="-1.516622802734" />
                  <Point X="-21.84864453125" Y="-1.664377075195" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-21.765642578125" Y="-1.736103637695" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.787943359375" Y="-2.789318847656" />
                  <Point X="-20.795865234375" Y="-2.802136474609" />
                  <Point X="-20.943310546875" Y="-3.011638183594" />
                  <Point X="-20.9770703125" Y="-2.992147216797" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.26266015625" Y="-2.253312744141" />
                  <Point X="-22.466947265625" Y="-2.216418457031" />
                  <Point X="-22.47875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.680634765625" Y="-2.308563720703" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.334684082031" />
                  <Point X="-22.80071875" Y="-2.504397216797" />
                  <Point X="-22.80587890625" Y="-2.514202148438" />
                  <Point X="-22.8108359375" Y="-2.546374511719" />
                  <Point X="-22.77394140625" Y="-2.750662353516" />
                  <Point X="-22.77394140625" Y="-2.750667480469" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.7235625" Y="-2.851920654297" />
                  <Point X="-22.01332421875" Y="-4.082087890625" />
                  <Point X="-22.155296875" Y="-4.183494628906" />
                  <Point X="-22.164712890625" Y="-4.190220214844" />
                  <Point X="-22.32022265625" Y="-4.290879882812" />
                  <Point X="-22.348095703125" Y="-4.254555175781" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467041016" />
                  <Point X="-23.53093359375" Y="-2.850931884766" />
                  <Point X="-23.54257421875" Y="-2.843448242188" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.79455078125" Y="-2.855994384766" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509521484" />
                  <Point X="-24.0048203125" Y="-3.009986328125" />
                  <Point X="-24.014650390625" Y="-3.018159912109" />
                  <Point X="-24.03154296875" Y="-3.045985595703" />
                  <Point X="-24.08241796875" Y="-3.280050537109" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-24.0740703125" Y="-3.401869140625" />
                  <Point X="-23.872357421875" Y="-4.934028320312" />
                  <Point X="-23.99676171875" Y="-4.961297851562" />
                  <Point X="-24.005654296875" Y="-4.963247558594" />
                  <Point X="-24.139798828125" Y="-4.987616699219" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#212" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.18268991153" Y="5.036932540226" Z="2.4" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.4" />
                  <Point X="-0.236432156461" Y="5.071268555169" Z="2.4" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.4" />
                  <Point X="-1.025832072416" Y="4.97204254565" Z="2.4" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.4" />
                  <Point X="-1.709987757464" Y="4.460968804913" Z="2.4" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.4" />
                  <Point X="-1.709408924438" Y="4.437588953684" Z="2.4" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.4" />
                  <Point X="-1.745341835888" Y="4.338560414803" Z="2.4" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.4" />
                  <Point X="-1.84429957467" Y="4.302431820138" Z="2.4" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.4" />
                  <Point X="-2.123367513053" Y="4.595669203726" Z="2.4" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.4" />
                  <Point X="-2.169913915662" Y="4.590111323244" Z="2.4" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.4" />
                  <Point X="-2.818872437545" Y="4.222736520606" Z="2.4" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.4" />
                  <Point X="-3.02212360544" Y="3.17599128166" Z="2.4" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.4" />
                  <Point X="-3.001115888145" Y="3.135640367216" Z="2.4" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.4" />
                  <Point X="-2.997356616673" Y="3.051446951214" Z="2.4" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.4" />
                  <Point X="-3.05943609839" Y="2.994448811028" Z="2.4" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.4" />
                  <Point X="-3.757868066637" Y="3.358070343305" Z="2.4" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.4" />
                  <Point X="-3.81616537674" Y="3.34959580224" Z="2.4" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.4" />
                  <Point X="-4.226244446013" Y="2.814551610882" Z="2.4" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.4" />
                  <Point X="-3.743047307239" Y="1.646503325818" Z="2.4" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.4" />
                  <Point X="-3.694937955841" Y="1.607713804039" Z="2.4" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.4" />
                  <Point X="-3.66816863159" Y="1.550454371025" Z="2.4" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.4" />
                  <Point X="-3.694824887816" Y="1.493142290329" Z="2.4" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.4" />
                  <Point X="-4.758404097995" Y="1.607210217955" Z="2.4" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.4" />
                  <Point X="-4.825034608714" Y="1.583347681407" Z="2.4" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.4" />
                  <Point X="-4.977609652661" Y="1.005639324835" Z="2.4" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.4" />
                  <Point X="-3.657601226934" Y="0.070783766587" Z="2.4" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.4" />
                  <Point X="-3.57504492644" Y="0.048016973882" Z="2.4" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.4" />
                  <Point X="-3.548302580985" Y="0.028178952145" Z="2.4" />
                  <Point X="-3.539556741714" Y="0" Z="2.4" />
                  <Point X="-3.54006202015" Y="-0.001627998916" Z="2.4" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.4" />
                  <Point X="-3.550323668683" Y="-0.030859009252" Z="2.4" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.4" />
                  <Point X="-4.979286561839" Y="-0.424928290169" Z="2.4" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.4" />
                  <Point X="-5.056085120672" Y="-0.476302168585" Z="2.4" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.4" />
                  <Point X="-4.975246858762" Y="-1.018699734387" Z="2.4" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.4" />
                  <Point X="-3.308063189821" Y="-1.318567779107" Z="2.4" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.4" />
                  <Point X="-3.217712459651" Y="-1.307714612656" Z="2.4" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.4" />
                  <Point X="-3.193097110694" Y="-1.324074547895" Z="2.4" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.4" />
                  <Point X="-4.43175944899" Y="-2.29706719851" Z="2.4" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.4" />
                  <Point X="-4.486867691943" Y="-2.378540461854" Z="2.4" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.4" />
                  <Point X="-4.1903960628" Y="-2.868795069957" Z="2.4" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.4" />
                  <Point X="-2.643264037996" Y="-2.596150596871" Z="2.4" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.4" />
                  <Point X="-2.571891954956" Y="-2.556438514175" Z="2.4" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.4" />
                  <Point X="-3.259266416298" Y="-3.791814344497" Z="2.4" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.4" />
                  <Point X="-3.277562634128" Y="-3.879457963605" Z="2.4" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.4" />
                  <Point X="-2.86647862735" Y="-4.192360430116" Z="2.4" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.4" />
                  <Point X="-2.238505639467" Y="-4.172460163397" Z="2.4" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.4" />
                  <Point X="-2.212132649631" Y="-4.147037777288" Z="2.4" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.4" />
                  <Point X="-1.951346826908" Y="-3.985192654499" Z="2.4" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.4" />
                  <Point X="-1.645926584536" Y="-4.015548862008" Z="2.4" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.4" />
                  <Point X="-1.422100085413" Y="-4.225560340652" Z="2.4" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.4" />
                  <Point X="-1.410465337788" Y="-4.859498224429" Z="2.4" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.4" />
                  <Point X="-1.396948631784" Y="-4.883658612471" Z="2.4" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.4" />
                  <Point X="-1.101030951122" Y="-4.958761219271" Z="2.4" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.4" />
                  <Point X="-0.438965768003" Y="-3.600426579333" Z="2.4" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.4" />
                  <Point X="-0.408144284789" Y="-3.505888648101" Z="2.4" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.4" />
                  <Point X="-0.23952108883" Y="-3.278577875258" Z="2.4" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.4" />
                  <Point X="0.01383799053" Y="-3.20853417003" Z="2.4" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.4" />
                  <Point X="0.262301574644" Y="-3.295757107667" Z="2.4" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.4" />
                  <Point X="0.795789092398" Y="-4.932109450482" Z="2.4" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.4" />
                  <Point X="0.827518021279" Y="-5.011973576113" Z="2.4" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.4" />
                  <Point X="1.007789136268" Y="-4.978857830813" Z="2.4" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.4" />
                  <Point X="0.96934577217" Y="-3.364062645198" Z="2.4" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.4" />
                  <Point X="0.960285014622" Y="-3.259390926651" Z="2.4" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.4" />
                  <Point X="1.020990384989" Y="-3.017152384156" Z="2.4" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.4" />
                  <Point X="1.203874503196" Y="-2.874503944451" Z="2.4" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.4" />
                  <Point X="1.435870852908" Y="-2.861710288658" Z="2.4" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.4" />
                  <Point X="2.606079902576" Y="-4.253713321856" Z="2.4" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.4" />
                  <Point X="2.672709592911" Y="-4.319748715576" Z="2.4" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.4" />
                  <Point X="2.867609453893" Y="-4.19290132919" Z="2.4" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.4" />
                  <Point X="2.313580979555" Y="-2.795640694662" Z="2.4" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.4" />
                  <Point X="2.269105382521" Y="-2.710496173684" Z="2.4" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.4" />
                  <Point X="2.237371380661" Y="-2.496403250868" Z="2.4" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.4" />
                  <Point X="2.336495127504" Y="-2.321529929306" Z="2.4" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.4" />
                  <Point X="2.518010629829" Y="-2.23434262697" Z="2.4" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.4" />
                  <Point X="3.991772902444" Y="-3.00416852072" Z="2.4" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.4" />
                  <Point X="4.074651691996" Y="-3.032962242477" Z="2.4" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.4" />
                  <Point X="4.248433089604" Y="-2.784324266097" Z="2.4" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.4" />
                  <Point X="3.258637685187" Y="-1.665155940725" Z="2.4" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.4" />
                  <Point X="3.187254780891" Y="-1.60605673778" Z="2.4" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.4" />
                  <Point X="3.093121591814" Y="-1.448966612158" Z="2.4" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.4" />
                  <Point X="3.113985469248" Y="-1.280163295571" Z="2.4" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.4" />
                  <Point X="3.227652227018" Y="-1.15322874758" Z="2.4" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.4" />
                  <Point X="4.824658661359" Y="-1.303572456453" Z="2.4" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.4" />
                  <Point X="4.911618236444" Y="-1.294205581716" Z="2.4" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.4" />
                  <Point X="4.994528502033" Y="-0.923926713606" Z="2.4" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.4" />
                  <Point X="3.818959633672" Y="-0.239836886629" Z="2.4" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.4" />
                  <Point X="3.742899980417" Y="-0.217890074587" Z="2.4" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.4" />
                  <Point X="3.652411083145" Y="-0.163475196355" Z="2.4" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.4" />
                  <Point X="3.59892617986" Y="-0.09133384794" Z="2.4" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.4" />
                  <Point X="3.582445270564" Y="0.005276683236" Z="2.4" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.4" />
                  <Point X="3.602968355256" Y="0.10047354222" Z="2.4" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.4" />
                  <Point X="3.660495433935" Y="0.170258769655" Z="2.4" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.4" />
                  <Point X="4.977008183635" Y="0.550135046139" Z="2.4" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.4" />
                  <Point X="5.044415663147" Y="0.592280004557" Z="2.4" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.4" />
                  <Point X="4.976578573803" Y="1.015173865753" Z="2.4" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.4" />
                  <Point X="3.540551525485" Y="1.232217953817" Z="2.4" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.4" />
                  <Point X="3.457978530828" Y="1.222703777824" Z="2.4" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.4" />
                  <Point X="3.365156901871" Y="1.236609742565" Z="2.4" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.4" />
                  <Point X="3.2966937398" Y="1.277660630194" Z="2.4" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.4" />
                  <Point X="3.250295950775" Y="1.351393504514" Z="2.4" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.4" />
                  <Point X="3.23476763069" Y="1.436552859337" Z="2.4" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.4" />
                  <Point X="3.258271938154" Y="1.51343089103" Z="2.4" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.4" />
                  <Point X="4.385352360735" Y="2.407618124804" Z="2.4" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.4" />
                  <Point X="4.435889710313" Y="2.474036585782" Z="2.4" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.4" />
                  <Point X="4.225298320337" Y="2.818655531677" Z="2.4" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.4" />
                  <Point X="2.59138905138" Y="2.314059238726" Z="2.4" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.4" />
                  <Point X="2.50549283725" Y="2.265826139558" Z="2.4" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.4" />
                  <Point X="2.425799801643" Y="2.245986402175" Z="2.4" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.4" />
                  <Point X="2.35670898658" Y="2.256246787236" Z="2.4" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.4" />
                  <Point X="2.294511812999" Y="2.300315873802" Z="2.4" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.4" />
                  <Point X="2.253443300584" Y="2.363958642189" Z="2.4" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.4" />
                  <Point X="2.2467018031" Y="2.433976738632" Z="2.4" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.4" />
                  <Point X="3.081566114134" Y="3.920750030575" Z="2.4" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.4" />
                  <Point X="3.108137767126" Y="4.016831677149" Z="2.4" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.4" />
                  <Point X="2.731774304051" Y="4.281688183243" Z="2.4" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.4" />
                  <Point X="2.333274730504" Y="4.511270283326" Z="2.4" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.4" />
                  <Point X="1.92069327249" Y="4.701771298796" Z="2.4" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.4" />
                  <Point X="1.481007573357" Y="4.856915338043" Z="2.4" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.4" />
                  <Point X="0.82600200102" Y="5.010056428966" Z="2.4" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.4" />
                  <Point X="0.010554650023" Y="4.394514965725" Z="2.4" />
                  <Point X="0" Y="4.355124473572" Z="2.4" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>