<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#159" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1677" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000473632812" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.270900390625" Y="-4.131279296875" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140136719" />
                  <Point X="-24.457634765625" Y="-3.467376708984" />
                  <Point X="-24.525400390625" Y="-3.369740722656" />
                  <Point X="-24.62136328125" Y="-3.231476318359" />
                  <Point X="-24.643248046875" Y="-3.209020751953" />
                  <Point X="-24.66950390625" Y="-3.189776855469" />
                  <Point X="-24.69750390625" Y="-3.175669433594" />
                  <Point X="-24.802365234375" Y="-3.143124023438" />
                  <Point X="-24.95086328125" Y="-3.097036132812" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.141685546875" Y="-3.129581054688" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.189776855469" />
                  <Point X="-25.344439453125" Y="-3.209020751953" />
                  <Point X="-25.36632421875" Y="-3.231477294922" />
                  <Point X="-25.434087890625" Y="-3.329113037109" />
                  <Point X="-25.53005078125" Y="-3.467377685547" />
                  <Point X="-25.5381875" Y="-3.481572021484" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.78577734375" Y="-4.388759277344" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.197498046875" Y="-4.814948730469" />
                  <Point X="-26.246416015625" Y="-4.802362304688" />
                  <Point X="-26.234919921875" Y="-4.715032714844" />
                  <Point X="-26.2149609375" Y="-4.563438476562" />
                  <Point X="-26.21419921875" Y="-4.5479296875" />
                  <Point X="-26.2165078125" Y="-4.51622265625" />
                  <Point X="-26.241560546875" Y="-4.390280761719" />
                  <Point X="-26.277037109375" Y="-4.211931152344" />
                  <Point X="-26.287939453125" Y="-4.182964355469" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.4201875" Y="-4.046537841797" />
                  <Point X="-26.556904296875" Y="-3.926639648438" />
                  <Point X="-26.5831875" Y="-3.910295898438" />
                  <Point X="-26.612884765625" Y="-3.897994628906" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.771162109375" Y="-3.882567871094" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708496094" />
                  <Point X="-27.014466796875" Y="-3.882028076172" />
                  <Point X="-27.042658203125" Y="-3.894801757812" />
                  <Point X="-27.14942578125" Y="-3.966142333984" />
                  <Point X="-27.300623046875" Y="-4.067169921875" />
                  <Point X="-27.31278515625" Y="-4.076821533203" />
                  <Point X="-27.3351015625" Y="-4.099461425781" />
                  <Point X="-27.466916015625" Y="-4.271245117188" />
                  <Point X="-27.4801484375" Y="-4.288489257813" />
                  <Point X="-27.550470703125" Y="-4.244947265625" />
                  <Point X="-27.801712890625" Y="-4.089383789063" />
                  <Point X="-27.96532421875" Y="-3.963409423828" />
                  <Point X="-28.10472265625" Y="-3.856077392578" />
                  <Point X="-27.731873046875" Y="-3.210283691406" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647655029297" />
                  <Point X="-27.406587890625" Y="-2.616129150391" />
                  <Point X="-27.40557421875" Y="-2.585194824219" />
                  <Point X="-27.41455859375" Y="-2.555576660156" />
                  <Point X="-27.428775390625" Y="-2.526747558594" />
                  <Point X="-27.44680078125" Y="-2.501591308594" />
                  <Point X="-27.4641484375" Y="-2.484242675781" />
                  <Point X="-27.48930859375" Y="-2.466212890625" />
                  <Point X="-27.518138671875" Y="-2.451995605469" />
                  <Point X="-27.547755859375" Y="-2.443011474609" />
                  <Point X="-27.578689453125" Y="-2.444024169922" />
                  <Point X="-27.61021484375" Y="-2.450295166016" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.39491796875" Y="-2.897521240234" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-28.884369140625" Y="-3.05463671875" />
                  <Point X="-29.082859375" Y="-2.793861083984" />
                  <Point X="-29.20015625" Y="-2.597171386719" />
                  <Point X="-29.306142578125" Y="-2.419449951172" />
                  <Point X="-28.645048828125" Y="-1.912175170898" />
                  <Point X="-28.105955078125" Y="-1.498513305664" />
                  <Point X="-28.084576171875" Y="-1.475592651367" />
                  <Point X="-28.066611328125" Y="-1.44846105957" />
                  <Point X="-28.053857421875" Y="-1.419833618164" />
                  <Point X="-28.04615234375" Y="-1.390086425781" />
                  <Point X="-28.04334765625" Y="-1.359655761719" />
                  <Point X="-28.045556640625" Y="-1.327985595703" />
                  <Point X="-28.052556640625" Y="-1.298240966797" />
                  <Point X="-28.068638671875" Y="-1.272257568359" />
                  <Point X="-28.089470703125" Y="-1.248301513672" />
                  <Point X="-28.112970703125" Y="-1.228767456055" />
                  <Point X="-28.139453125" Y="-1.213180908203" />
                  <Point X="-28.168716796875" Y="-1.20195703125" />
                  <Point X="-28.200603515625" Y="-1.195475097656" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-29.18597265625" Y="-1.319986083984" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.7564453125" Y="-1.296578369141" />
                  <Point X="-29.834078125" Y="-0.992649414063" />
                  <Point X="-29.865109375" Y="-0.775670166016" />
                  <Point X="-29.892421875" Y="-0.584698303223" />
                  <Point X="-29.146984375" Y="-0.384958587646" />
                  <Point X="-28.532875" Y="-0.220408432007" />
                  <Point X="-28.510947265625" Y="-0.211478302002" />
                  <Point X="-28.48389453125" Y="-0.19628666687" />
                  <Point X="-28.470525390625" Y="-0.187201858521" />
                  <Point X="-28.4424375" Y="-0.164393829346" />
                  <Point X="-28.42619921875" Y="-0.1474818573" />
                  <Point X="-28.41459765625" Y="-0.127108207703" />
                  <Point X="-28.40334375" Y="-0.100032905579" />
                  <Point X="-28.398638671875" Y="-0.085519622803" />
                  <Point X="-28.390970703125" Y="-0.053229255676" />
                  <Point X="-28.38840234375" Y="-0.031902353287" />
                  <Point X="-28.39069140625" Y="-0.010543683052" />
                  <Point X="-28.396765625" Y="0.01661328125" />
                  <Point X="-28.401236328125" Y="0.031074197769" />
                  <Point X="-28.414083984375" Y="0.063283050537" />
                  <Point X="-28.425388671875" Y="0.083819145203" />
                  <Point X="-28.44137890625" Y="0.100961517334" />
                  <Point X="-28.464685546875" Y="0.120452003479" />
                  <Point X="-28.477876953125" Y="0.129703018188" />
                  <Point X="-28.5097109375" Y="0.148212341309" />
                  <Point X="-28.532875" Y="0.157848297119" />
                  <Point X="-29.40253125" Y="0.39087197876" />
                  <Point X="-29.89181640625" Y="0.521975524902" />
                  <Point X="-29.87451953125" Y="0.63886541748" />
                  <Point X="-29.82448828125" Y="0.976967773438" />
                  <Point X="-29.762015625" Y="1.207510742188" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-29.205873046875" Y="1.357747558594" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228149414" />
                  <Point X="-28.703134765625" Y="1.305263916016" />
                  <Point X="-28.677712890625" Y="1.313279785156" />
                  <Point X="-28.641708984375" Y="1.324631347656" />
                  <Point X="-28.622775390625" Y="1.332962402344" />
                  <Point X="-28.60403125" Y="1.343784545898" />
                  <Point X="-28.587353515625" Y="1.356014160156" />
                  <Point X="-28.573716796875" Y="1.371563598633" />
                  <Point X="-28.56130078125" Y="1.389294067383" />
                  <Point X="-28.55134765625" Y="1.407432250977" />
                  <Point X="-28.541146484375" Y="1.432060546875" />
                  <Point X="-28.526701171875" Y="1.466936645508" />
                  <Point X="-28.5209140625" Y="1.486797119141" />
                  <Point X="-28.51715625" Y="1.508112182617" />
                  <Point X="-28.515802734375" Y="1.528754882812" />
                  <Point X="-28.518951171875" Y="1.549200927734" />
                  <Point X="-28.5245546875" Y="1.570107177734" />
                  <Point X="-28.53205078125" Y="1.589377563477" />
                  <Point X="-28.544359375" Y="1.613022827148" />
                  <Point X="-28.561791015625" Y="1.646507324219" />
                  <Point X="-28.573283203125" Y="1.663707763672" />
                  <Point X="-28.5871953125" Y="1.680287231445" />
                  <Point X="-28.60213671875" Y="1.694590332031" />
                  <Point X="-29.10097265625" Y="2.077361328125" />
                  <Point X="-29.351859375" Y="2.269873535156" />
                  <Point X="-29.275556640625" Y="2.400599365234" />
                  <Point X="-29.0811484375" Y="2.733665283203" />
                  <Point X="-28.915673828125" Y="2.946361572266" />
                  <Point X="-28.75050390625" Y="3.158662353516" />
                  <Point X="-28.47701953125" Y="3.000765625" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.111384765625" Y="2.822698486328" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.04056640625" Y="2.818762939453" />
                  <Point X="-28.019107421875" Y="2.821587890625" />
                  <Point X="-27.999015625" Y="2.826504394531" />
                  <Point X="-27.980462890625" Y="2.835653320313" />
                  <Point X="-27.962208984375" Y="2.847282470703" />
                  <Point X="-27.946076171875" Y="2.860228759766" />
                  <Point X="-27.920943359375" Y="2.885361328125" />
                  <Point X="-27.8853515625" Y="2.920952148438" />
                  <Point X="-27.872404296875" Y="2.937085449219" />
                  <Point X="-27.860775390625" Y="2.95533984375" />
                  <Point X="-27.85162890625" Y="2.973888671875" />
                  <Point X="-27.8467109375" Y="2.993977539062" />
                  <Point X="-27.843884765625" Y="3.015436523438" />
                  <Point X="-27.84343359375" Y="3.036120605469" />
                  <Point X="-27.84653125" Y="3.071528320312" />
                  <Point X="-27.85091796875" Y="3.121670166016" />
                  <Point X="-27.854955078125" Y="3.141962646484" />
                  <Point X="-27.86146484375" Y="3.162604980469" />
                  <Point X="-27.869794921875" Y="3.181533203125" />
                  <Point X="-28.090845703125" Y="3.564402587891" />
                  <Point X="-28.18333203125" Y="3.724595947266" />
                  <Point X="-28.0392109375" Y="3.835093261719" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.44" Y="4.23948046875" />
                  <Point X="-27.167037109375" Y="4.391133300781" />
                  <Point X="-27.125962890625" Y="4.337604003906" />
                  <Point X="-27.0431953125" Y="4.229740234375" />
                  <Point X="-27.028890625" Y="4.214797851563" />
                  <Point X="-27.012310546875" Y="4.200885742188" />
                  <Point X="-26.99511328125" Y="4.189395019531" />
                  <Point X="-26.955705078125" Y="4.168879882812" />
                  <Point X="-26.899896484375" Y="4.139828125" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.85970703125" Y="4.126728515625" />
                  <Point X="-26.839263671875" Y="4.12358203125" />
                  <Point X="-26.818623046875" Y="4.124935546875" />
                  <Point X="-26.79730859375" Y="4.128694335938" />
                  <Point X="-26.777451171875" Y="4.134481933594" />
                  <Point X="-26.736404296875" Y="4.151484375" />
                  <Point X="-26.67827734375" Y="4.175561523437" />
                  <Point X="-26.660146484375" Y="4.185508789062" />
                  <Point X="-26.642416015625" Y="4.197922851562" />
                  <Point X="-26.626859375" Y="4.211563964844" />
                  <Point X="-26.614626953125" Y="4.228249511719" />
                  <Point X="-26.603806640625" Y="4.246993652344" />
                  <Point X="-26.595478515625" Y="4.265922363281" />
                  <Point X="-26.582119140625" Y="4.308294433594" />
                  <Point X="-26.56319921875" Y="4.368298828125" />
                  <Point X="-26.5591640625" Y="4.388583496094" />
                  <Point X="-26.55727734375" Y="4.410145507812" />
                  <Point X="-26.557728515625" Y="4.430826660156" />
                  <Point X="-26.582859375" Y="4.62171484375" />
                  <Point X="-26.58419921875" Y="4.631897460938" />
                  <Point X="-26.387953125" Y="4.686918457031" />
                  <Point X="-25.949630859375" Y="4.80980859375" />
                  <Point X="-25.63368359375" Y="4.846785644531" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.213884765625" Y="4.584809570312" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.74051953125" Y="4.709020507812" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.5386796875" Y="4.8718203125" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.89455859375" Y="4.76862890625" />
                  <Point X="-23.51897265625" Y="4.677950195312" />
                  <Point X="-23.3496953125" Y="4.616553222656" />
                  <Point X="-23.105359375" Y="4.527930175781" />
                  <Point X="-22.940828125" Y="4.450984863281" />
                  <Point X="-22.705423828125" Y="4.340893554688" />
                  <Point X="-22.546455078125" Y="4.248279296875" />
                  <Point X="-22.319021484375" Y="4.115775390625" />
                  <Point X="-22.1691171875" Y="4.009172607422" />
                  <Point X="-22.05673828125" Y="3.929254150391" />
                  <Point X="-22.49580078125" Y="3.168774169922" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857919921875" Y="2.539934326172" />
                  <Point X="-22.866921875" Y="2.516058837891" />
                  <Point X="-22.87580859375" Y="2.482829589844" />
                  <Point X="-22.888392578125" Y="2.435772460938" />
                  <Point X="-22.891380859375" Y="2.417935791016" />
                  <Point X="-22.892271484375" Y="2.380953125" />
                  <Point X="-22.888806640625" Y="2.352219238281" />
                  <Point X="-22.883900390625" Y="2.311528320312" />
                  <Point X="-22.87855859375" Y="2.289606933594" />
                  <Point X="-22.87029296875" Y="2.267518310547" />
                  <Point X="-22.859927734375" Y="2.247468261719" />
                  <Point X="-22.842146484375" Y="2.221265625" />
                  <Point X="-22.81696875" Y="2.184159667969" />
                  <Point X="-22.80553515625" Y="2.170329101562" />
                  <Point X="-22.77840234375" Y="2.145593505859" />
                  <Point X="-22.752201171875" Y="2.127813964844" />
                  <Point X="-22.71509375" Y="2.102635986328" />
                  <Point X="-22.695044921875" Y="2.092271728516" />
                  <Point X="-22.67295703125" Y="2.084006103516" />
                  <Point X="-22.65103515625" Y="2.078663330078" />
                  <Point X="-22.62230078125" Y="2.075198486328" />
                  <Point X="-22.581611328125" Y="2.070291748047" />
                  <Point X="-22.563533203125" Y="2.069845703125" />
                  <Point X="-22.526794921875" Y="2.074170898438" />
                  <Point X="-22.493564453125" Y="2.083056884766" />
                  <Point X="-22.4465078125" Y="2.095640625" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.5367578125" Y="2.615157226563" />
                  <Point X="-21.032673828125" Y="2.906190429688" />
                  <Point X="-21.01163671875" Y="2.876953369141" />
                  <Point X="-20.876720703125" Y="2.689451904297" />
                  <Point X="-20.793158203125" Y="2.551364257812" />
                  <Point X="-20.73780078125" Y="2.459883789063" />
                  <Point X="-21.299697265625" Y="2.028724853516" />
                  <Point X="-21.76921484375" Y="1.668451171875" />
                  <Point X="-21.7785703125" Y="1.66024621582" />
                  <Point X="-21.79602734375" Y="1.641626220703" />
                  <Point X="-21.81994140625" Y="1.610427124023" />
                  <Point X="-21.85380859375" Y="1.566244873047" />
                  <Point X="-21.86339453125" Y="1.550911621094" />
                  <Point X="-21.878369140625" Y="1.517087036133" />
                  <Point X="-21.88727734375" Y="1.485232666016" />
                  <Point X="-21.89989453125" Y="1.440122680664" />
                  <Point X="-21.90334765625" Y="1.417825805664" />
                  <Point X="-21.9041640625" Y="1.394254272461" />
                  <Point X="-21.902259765625" Y="1.371764648438" />
                  <Point X="-21.8949453125" Y="1.336322387695" />
                  <Point X="-21.88458984375" Y="1.286131835938" />
                  <Point X="-21.8793203125" Y="1.268980712891" />
                  <Point X="-21.863716796875" Y="1.235740722656" />
                  <Point X="-21.843826171875" Y="1.205508178711" />
                  <Point X="-21.81566015625" Y="1.16269543457" />
                  <Point X="-21.801109375" Y="1.145452270508" />
                  <Point X="-21.783865234375" Y="1.129361572266" />
                  <Point X="-21.76565234375" Y="1.116034545898" />
                  <Point X="-21.736828125" Y="1.099809204102" />
                  <Point X="-21.696009765625" Y="1.07683215332" />
                  <Point X="-21.6794765625" Y="1.069500976563" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.604908203125" Y="1.054287963867" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.6808828125" Y="1.156376220703" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.212005859375" Y="1.170817504883" />
                  <Point X="-20.154060546875" Y="0.932788574219" />
                  <Point X="-20.127728515625" Y="0.76366394043" />
                  <Point X="-20.109134765625" Y="0.644238586426" />
                  <Point X="-20.745341796875" Y="0.473767272949" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.2952109375" Y="0.325585296631" />
                  <Point X="-21.318453125" Y="0.315068023682" />
                  <Point X="-21.3567421875" Y="0.292936553955" />
                  <Point X="-21.410962890625" Y="0.26159564209" />
                  <Point X="-21.425685546875" Y="0.251097961426" />
                  <Point X="-21.45246875" Y="0.225576080322" />
                  <Point X="-21.47544140625" Y="0.196302856445" />
                  <Point X="-21.507974609375" Y="0.154848464966" />
                  <Point X="-21.51969921875" Y="0.135566680908" />
                  <Point X="-21.52947265625" Y="0.114102157593" />
                  <Point X="-21.536318359375" Y="0.092604545593" />
                  <Point X="-21.5439765625" Y="0.052618865967" />
                  <Point X="-21.5548203125" Y="-0.00400592041" />
                  <Point X="-21.556515625" Y="-0.021875896454" />
                  <Point X="-21.5548203125" Y="-0.05855406189" />
                  <Point X="-21.547162109375" Y="-0.098539894104" />
                  <Point X="-21.536318359375" Y="-0.155164672852" />
                  <Point X="-21.52947265625" Y="-0.176662139893" />
                  <Point X="-21.51969921875" Y="-0.198126663208" />
                  <Point X="-21.507974609375" Y="-0.217408584595" />
                  <Point X="-21.485001953125" Y="-0.24668182373" />
                  <Point X="-21.45246875" Y="-0.288136383057" />
                  <Point X="-21.439998046875" Y="-0.30123815918" />
                  <Point X="-21.410962890625" Y="-0.324155914307" />
                  <Point X="-21.372673828125" Y="-0.346287261963" />
                  <Point X="-21.318453125" Y="-0.377628173828" />
                  <Point X="-21.307291015625" Y="-0.383137695312" />
                  <Point X="-21.283419921875" Y="-0.392149810791" />
                  <Point X="-20.521435546875" Y="-0.596323181152" />
                  <Point X="-20.108525390625" Y="-0.706962036133" />
                  <Point X="-20.112623046875" Y="-0.73414074707" />
                  <Point X="-20.144974609375" Y="-0.948723144531" />
                  <Point X="-20.17871484375" Y="-1.096576782227" />
                  <Point X="-20.19882421875" Y="-1.18469909668" />
                  <Point X="-20.951599609375" Y="-1.085594482422" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710266113" />
                  <Point X="-21.62533984375" Y="-1.005508666992" />
                  <Point X="-21.700486328125" Y="-1.021842224121" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.8360234375" Y="-1.056596069336" />
                  <Point X="-21.8638515625" Y="-1.07348840332" />
                  <Point X="-21.8876015625" Y="-1.093959960937" />
                  <Point X="-21.9330234375" Y="-1.148587890625" />
                  <Point X="-21.997345703125" Y="-1.225947998047" />
                  <Point X="-22.012068359375" Y="-1.250334716797" />
                  <Point X="-22.02341015625" Y="-1.277719970703" />
                  <Point X="-22.030240234375" Y="-1.305365844727" />
                  <Point X="-22.03675" Y="-1.376111572266" />
                  <Point X="-22.04596875" Y="-1.476296142578" />
                  <Point X="-22.04365234375" Y="-1.507561157227" />
                  <Point X="-22.035921875" Y="-1.539182617188" />
                  <Point X="-22.023548828125" Y="-1.56799597168" />
                  <Point X="-21.981962890625" Y="-1.632682373047" />
                  <Point X="-21.923068359375" Y="-1.724286376953" />
                  <Point X="-21.9130625" Y="-1.737244140625" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.1822421875" Y="-2.303506835938" />
                  <Point X="-20.786876953125" Y="-2.606882080078" />
                  <Point X="-20.875197265625" Y="-2.749798339844" />
                  <Point X="-20.94496484375" Y="-2.848927490234" />
                  <Point X="-20.971017578125" Y="-2.885945068359" />
                  <Point X="-21.643271484375" Y="-2.497818847656" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.2457734375" Y="-2.159825195312" />
                  <Point X="-22.335208984375" Y="-2.143673095703" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135176513672" />
                  <Point X="-22.629466796875" Y="-2.174280029297" />
                  <Point X="-22.73468359375" Y="-2.229655517578" />
                  <Point X="-22.757615234375" Y="-2.246549072266" />
                  <Point X="-22.77857421875" Y="-2.267508300781" />
                  <Point X="-22.795466796875" Y="-2.290438720703" />
                  <Point X="-22.8345703125" Y="-2.364738769531" />
                  <Point X="-22.889947265625" Y="-2.469956787109" />
                  <Point X="-22.899771484375" Y="-2.499735107422" />
                  <Point X="-22.904728515625" Y="-2.531908935547" />
                  <Point X="-22.90432421875" Y="-2.563259033203" />
                  <Point X="-22.888171875" Y="-2.652695800781" />
                  <Point X="-22.865296875" Y="-2.779349365234" />
                  <Point X="-22.86101171875" Y="-2.795140869141" />
                  <Point X="-22.8481796875" Y="-2.826078613281" />
                  <Point X="-22.39378125" Y="-3.613122314453" />
                  <Point X="-22.138716796875" Y="-4.054904785156" />
                  <Point X="-22.218173828125" Y="-4.111659179688" />
                  <Point X="-22.296162109375" Y="-4.162140136719" />
                  <Point X="-22.298232421875" Y="-4.16348046875" />
                  <Point X="-22.817345703125" Y="-3.486960449219" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.366283203125" Y="-2.843846923828" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.67937109375" Y="-2.749994140625" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-23.96989453125" Y="-2.857399414062" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968860595703" />
                  <Point X="-24.11275" Y="-2.996686767578" />
                  <Point X="-24.124375" Y="-3.025809326172" />
                  <Point X="-24.146646484375" Y="-3.128282470703" />
                  <Point X="-24.178189453125" Y="-3.273397216797" />
                  <Point X="-24.180275390625" Y="-3.289627197266" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.051484375" Y="-4.301251953125" />
                  <Point X="-23.977935546875" Y="-4.859915527344" />
                  <Point X="-24.024318359375" Y="-4.870083007812" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.05842578125" Y="-4.75263671875" />
                  <Point X="-26.14124609375" Y="-4.731328125" />
                  <Point X="-26.140732421875" Y="-4.727431640625" />
                  <Point X="-26.1207734375" Y="-4.575837402344" />
                  <Point X="-26.120076171875" Y="-4.568098632812" />
                  <Point X="-26.11944921875" Y="-4.541030761719" />
                  <Point X="-26.1217578125" Y="-4.509323730469" />
                  <Point X="-26.123333984375" Y="-4.497687988281" />
                  <Point X="-26.14838671875" Y="-4.37174609375" />
                  <Point X="-26.18386328125" Y="-4.193396484375" />
                  <Point X="-26.188126953125" Y="-4.178467285156" />
                  <Point X="-26.199029296875" Y="-4.149500488281" />
                  <Point X="-26.20566796875" Y="-4.135462890625" />
                  <Point X="-26.221740234375" Y="-4.107625976562" />
                  <Point X="-26.23057421875" Y="-4.094861083984" />
                  <Point X="-26.25020703125" Y="-4.070937744141" />
                  <Point X="-26.261005859375" Y="-4.059779541016" />
                  <Point X="-26.357548828125" Y="-3.97511328125" />
                  <Point X="-26.494265625" Y="-3.855215087891" />
                  <Point X="-26.50673828125" Y="-3.845965087891" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822527587891" />
                  <Point X="-26.576529296875" Y="-3.810226318359" />
                  <Point X="-26.5913125" Y="-3.805476318359" />
                  <Point X="-26.621455078125" Y="-3.798447998047" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.76494921875" Y="-3.787771240234" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166015625" />
                  <Point X="-27.008005859375" Y="-3.781945556641" />
                  <Point X="-27.0390546875" Y="-3.790265136719" />
                  <Point X="-27.053673828125" Y="-3.795496337891" />
                  <Point X="-27.081865234375" Y="-3.808270019531" />
                  <Point X="-27.0954375" Y="-3.8158125" />
                  <Point X="-27.202205078125" Y="-3.887153076172" />
                  <Point X="-27.35340234375" Y="-3.988180664062" />
                  <Point X="-27.359677734375" Y="-3.992754882812" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032771484375" />
                  <Point X="-27.410470703125" Y="-4.041629150391" />
                  <Point X="-27.503201171875" Y="-4.162478515625" />
                  <Point X="-27.74758984375" Y="-4.011158203125" />
                  <Point X="-27.9073671875" Y="-3.88813671875" />
                  <Point X="-27.98086328125" Y="-3.831546630859" />
                  <Point X="-27.6496015625" Y="-3.257783691406" />
                  <Point X="-27.341490234375" Y="-2.724119628906" />
                  <Point X="-27.3348515625" Y="-2.710085693359" />
                  <Point X="-27.32394921875" Y="-2.68112109375" />
                  <Point X="-27.319685546875" Y="-2.666190429688" />
                  <Point X="-27.3134140625" Y="-2.634664550781" />
                  <Point X="-27.311638671875" Y="-2.619240478516" />
                  <Point X="-27.310625" Y="-2.588306152344" />
                  <Point X="-27.3146640625" Y="-2.557618408203" />
                  <Point X="-27.3236484375" Y="-2.528000244141" />
                  <Point X="-27.32935546875" Y="-2.513559570312" />
                  <Point X="-27.343572265625" Y="-2.48473046875" />
                  <Point X="-27.351552734375" Y="-2.471414794922" />
                  <Point X="-27.369578125" Y="-2.446258544922" />
                  <Point X="-27.379623046875" Y="-2.43441796875" />
                  <Point X="-27.396970703125" Y="-2.417069335938" />
                  <Point X="-27.4088125" Y="-2.407022705078" />
                  <Point X="-27.43397265625" Y="-2.388992919922" />
                  <Point X="-27.447291015625" Y="-2.381009765625" />
                  <Point X="-27.47612109375" Y="-2.366792480469" />
                  <Point X="-27.4905625" Y="-2.361086181641" />
                  <Point X="-27.5201796875" Y="-2.352102050781" />
                  <Point X="-27.550865234375" Y="-2.348062255859" />
                  <Point X="-27.581798828125" Y="-2.349074951172" />
                  <Point X="-27.59722265625" Y="-2.350849609375" />
                  <Point X="-27.628748046875" Y="-2.357120605469" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372285888672" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.44241796875" Y="-2.815248779297" />
                  <Point X="-28.793087890625" Y="-3.017708251953" />
                  <Point X="-28.808775390625" Y="-2.997098388672" />
                  <Point X="-29.004021484375" Y="-2.740584472656" />
                  <Point X="-29.118564453125" Y="-2.548513183594" />
                  <Point X="-29.181265625" Y="-2.443373291016" />
                  <Point X="-28.587216796875" Y="-1.987543701172" />
                  <Point X="-28.048123046875" Y="-1.573881835938" />
                  <Point X="-28.036484375" Y="-1.563311279297" />
                  <Point X="-28.01510546875" Y="-1.54039074707" />
                  <Point X="-28.005365234375" Y="-1.528040527344" />
                  <Point X="-27.987400390625" Y="-1.500908935547" />
                  <Point X="-27.979833984375" Y="-1.487121704102" />
                  <Point X="-27.967080078125" Y="-1.458494262695" />
                  <Point X="-27.961892578125" Y="-1.443654296875" />
                  <Point X="-27.9541875" Y="-1.413907104492" />
                  <Point X="-27.951552734375" Y="-1.398805297852" />
                  <Point X="-27.948748046875" Y="-1.368374633789" />
                  <Point X="-27.948578125" Y="-1.353045654297" />
                  <Point X="-27.950787109375" Y="-1.321375366211" />
                  <Point X="-27.95308203125" Y="-1.306223144531" />
                  <Point X="-27.96008203125" Y="-1.276478515625" />
                  <Point X="-27.97177734375" Y="-1.248243896484" />
                  <Point X="-27.987859375" Y="-1.222260498047" />
                  <Point X="-27.996953125" Y="-1.209919433594" />
                  <Point X="-28.01778515625" Y="-1.185963378906" />
                  <Point X="-28.028744140625" Y="-1.175245239258" />
                  <Point X="-28.052244140625" Y="-1.155711181641" />
                  <Point X="-28.064783203125" Y="-1.146895385742" />
                  <Point X="-28.091265625" Y="-1.131308959961" />
                  <Point X="-28.10543359375" Y="-1.124481201172" />
                  <Point X="-28.134697265625" Y="-1.113257324219" />
                  <Point X="-28.14979296875" Y="-1.108861083984" />
                  <Point X="-28.1816796875" Y="-1.102379150391" />
                  <Point X="-28.197296875" Y="-1.100532714844" />
                  <Point X="-28.22862109375" Y="-1.09944152832" />
                  <Point X="-28.244328125" Y="-1.100196655273" />
                  <Point X="-29.198373046875" Y="-1.225798828125" />
                  <Point X="-29.660919921875" Y="-1.286694335938" />
                  <Point X="-29.664400390625" Y="-1.273066040039" />
                  <Point X="-29.740763671875" Y="-0.97410949707" />
                  <Point X="-29.77106640625" Y="-0.76222064209" />
                  <Point X="-29.786451171875" Y="-0.65465447998" />
                  <Point X="-29.122396484375" Y="-0.476721466064" />
                  <Point X="-28.508287109375" Y="-0.312171417236" />
                  <Point X="-28.49704296875" Y="-0.308391937256" />
                  <Point X="-28.475115234375" Y="-0.299461791992" />
                  <Point X="-28.464431640625" Y="-0.294311401367" />
                  <Point X="-28.43737890625" Y="-0.279119781494" />
                  <Point X="-28.4305" Y="-0.274861572266" />
                  <Point X="-28.410640625" Y="-0.260949890137" />
                  <Point X="-28.382552734375" Y="-0.238141769409" />
                  <Point X="-28.373912109375" Y="-0.230190307617" />
                  <Point X="-28.357673828125" Y="-0.213278213501" />
                  <Point X="-28.34364453125" Y="-0.194491241455" />
                  <Point X="-28.33204296875" Y="-0.17411756897" />
                  <Point X="-28.326873046875" Y="-0.163570846558" />
                  <Point X="-28.315619140625" Y="-0.13649546814" />
                  <Point X="-28.312974609375" Y="-0.129329925537" />
                  <Point X="-28.306208984375" Y="-0.107468833923" />
                  <Point X="-28.298541015625" Y="-0.07517842865" />
                  <Point X="-28.29665234375" Y="-0.064587860107" />
                  <Point X="-28.294083984375" Y="-0.043260925293" />
                  <Point X="-28.293943359375" Y="-0.02177897644" />
                  <Point X="-28.296232421875" Y="-0.000420235425" />
                  <Point X="-28.297982421875" Y="0.010192626953" />
                  <Point X="-28.304056640625" Y="0.037349594116" />
                  <Point X="-28.30600390625" Y="0.044672836304" />
                  <Point X="-28.312998046875" Y="0.066271606445" />
                  <Point X="-28.325845703125" Y="0.098480407715" />
                  <Point X="-28.330859375" Y="0.109095947266" />
                  <Point X="-28.3421640625" Y="0.129632064819" />
                  <Point X="-28.355919921875" Y="0.148619232178" />
                  <Point X="-28.37191015625" Y="0.165761535645" />
                  <Point X="-28.380435546875" Y="0.173837402344" />
                  <Point X="-28.4037421875" Y="0.193327957153" />
                  <Point X="-28.410138671875" Y="0.198231765747" />
                  <Point X="-28.430125" Y="0.211829864502" />
                  <Point X="-28.461958984375" Y="0.23033921814" />
                  <Point X="-28.47322265625" Y="0.235925796509" />
                  <Point X="-28.49638671875" Y="0.245561767578" />
                  <Point X="-28.508287109375" Y="0.249611297607" />
                  <Point X="-29.377943359375" Y="0.482634857178" />
                  <Point X="-29.7854453125" Y="0.591824951172" />
                  <Point X="-29.78054296875" Y="0.624959106445" />
                  <Point X="-29.73133203125" Y="0.957520141602" />
                  <Point X="-29.670322265625" Y="1.182663696289" />
                  <Point X="-29.6335859375" Y="1.318237060547" />
                  <Point X="-29.2182734375" Y="1.263560424805" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815551758" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.204703369141" />
                  <Point X="-28.71514453125" Y="1.206589599609" />
                  <Point X="-28.704890625" Y="1.208053588867" />
                  <Point X="-28.6846015625" Y="1.212089355469" />
                  <Point X="-28.67456640625" Y="1.214661254883" />
                  <Point X="-28.64914453125" Y="1.222677246094" />
                  <Point X="-28.613140625" Y="1.234028686523" />
                  <Point X="-28.603447265625" Y="1.237677001953" />
                  <Point X="-28.584513671875" Y="1.246008056641" />
                  <Point X="-28.575275390625" Y="1.250690429688" />
                  <Point X="-28.55653125" Y="1.261512695312" />
                  <Point X="-28.547853515625" Y="1.267174438477" />
                  <Point X="-28.53117578125" Y="1.279404052734" />
                  <Point X="-28.5159296875" Y="1.293375732422" />
                  <Point X="-28.50229296875" Y="1.308925170898" />
                  <Point X="-28.495900390625" Y="1.317071044922" />
                  <Point X="-28.483484375" Y="1.334801391602" />
                  <Point X="-28.478015625" Y="1.343592407227" />
                  <Point X="-28.4680625" Y="1.36173059082" />
                  <Point X="-28.463578125" Y="1.371077880859" />
                  <Point X="-28.453376953125" Y="1.395706176758" />
                  <Point X="-28.438931640625" Y="1.430582275391" />
                  <Point X="-28.435494140625" Y="1.440360107422" />
                  <Point X="-28.42970703125" Y="1.460220581055" />
                  <Point X="-28.427357421875" Y="1.470303100586" />
                  <Point X="-28.423599609375" Y="1.491618164063" />
                  <Point X="-28.422359375" Y="1.501896484375" />
                  <Point X="-28.421005859375" Y="1.52253918457" />
                  <Point X="-28.42191015625" Y="1.543213378906" />
                  <Point X="-28.42505859375" Y="1.563659423828" />
                  <Point X="-28.427189453125" Y="1.573795654297" />
                  <Point X="-28.43279296875" Y="1.594701904297" />
                  <Point X="-28.436017578125" Y="1.604547729492" />
                  <Point X="-28.443513671875" Y="1.623817993164" />
                  <Point X="-28.44778515625" Y="1.633242675781" />
                  <Point X="-28.46009375" Y="1.656887939453" />
                  <Point X="-28.477525390625" Y="1.690372436523" />
                  <Point X="-28.482798828125" Y="1.699283935547" />
                  <Point X="-28.494291015625" Y="1.716484619141" />
                  <Point X="-28.500509765625" Y="1.72477331543" />
                  <Point X="-28.514421875" Y="1.741352783203" />
                  <Point X="-28.521501953125" Y="1.748912231445" />
                  <Point X="-28.536443359375" Y="1.763215332031" />
                  <Point X="-28.5443046875" Y="1.769958862305" />
                  <Point X="-29.043140625" Y="2.152729736328" />
                  <Point X="-29.22761328125" Y="2.294281005859" />
                  <Point X="-29.193509765625" Y="2.352709960938" />
                  <Point X="-29.002283203125" Y="2.680323486328" />
                  <Point X="-28.840693359375" Y="2.888027587891" />
                  <Point X="-28.726337890625" Y="3.035013671875" />
                  <Point X="-28.52451953125" Y="2.918493164063" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.1196640625" Y="2.728060058594" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.038494140625" Y="2.723785644531" />
                  <Point X="-28.02816796875" Y="2.724575683594" />
                  <Point X="-28.006708984375" Y="2.727400634766" />
                  <Point X="-27.99652734375" Y="2.729310546875" />
                  <Point X="-27.976435546875" Y="2.734227050781" />
                  <Point X="-27.957" Y="2.741301025391" />
                  <Point X="-27.938447265625" Y="2.750449951172" />
                  <Point X="-27.929419921875" Y="2.755531494141" />
                  <Point X="-27.911166015625" Y="2.767160644531" />
                  <Point X="-27.90275" Y="2.773189697266" />
                  <Point X="-27.8866171875" Y="2.786135986328" />
                  <Point X="-27.878900390625" Y="2.793053222656" />
                  <Point X="-27.853767578125" Y="2.818185791016" />
                  <Point X="-27.81817578125" Y="2.853776611328" />
                  <Point X="-27.811259765625" Y="2.861492431641" />
                  <Point X="-27.7983125" Y="2.877625732422" />
                  <Point X="-27.79228125" Y="2.886043212891" />
                  <Point X="-27.78065234375" Y="2.904297607422" />
                  <Point X="-27.7755703125" Y="2.913325439453" />
                  <Point X="-27.766423828125" Y="2.931874267578" />
                  <Point X="-27.759353515625" Y="2.951298828125" />
                  <Point X="-27.754435546875" Y="2.971387695312" />
                  <Point X="-27.7525234375" Y="2.981572998047" />
                  <Point X="-27.749697265625" Y="3.003031982422" />
                  <Point X="-27.748908203125" Y="3.013364746094" />
                  <Point X="-27.74845703125" Y="3.034048828125" />
                  <Point X="-27.748794921875" Y="3.044400146484" />
                  <Point X="-27.751892578125" Y="3.079807861328" />
                  <Point X="-27.756279296875" Y="3.129949707031" />
                  <Point X="-27.757744140625" Y="3.140206787109" />
                  <Point X="-27.76178125" Y="3.160499267578" />
                  <Point X="-27.764353515625" Y="3.170534667969" />
                  <Point X="-27.77086328125" Y="3.191177001953" />
                  <Point X="-27.774513671875" Y="3.200871582031" />
                  <Point X="-27.78284375" Y="3.219799804688" />
                  <Point X="-27.7875234375" Y="3.229033447266" />
                  <Point X="-28.00857421875" Y="3.611902832031" />
                  <Point X="-28.05938671875" Y="3.699915283203" />
                  <Point X="-27.981408203125" Y="3.759701904297" />
                  <Point X="-27.6483671875" Y="4.015040039062" />
                  <Point X="-27.39386328125" Y="4.156436035156" />
                  <Point X="-27.192525390625" Y="4.268295410156" />
                  <Point X="-27.118564453125" Y="4.171908203125" />
                  <Point X="-27.111818359375" Y="4.164045410156" />
                  <Point X="-27.097513671875" Y="4.149103027344" />
                  <Point X="-27.089955078125" Y="4.142022949219" />
                  <Point X="-27.073375" Y="4.128110839844" />
                  <Point X="-27.06508984375" Y="4.121895996094" />
                  <Point X="-27.047892578125" Y="4.110405273438" />
                  <Point X="-27.03898046875" Y="4.105129394531" />
                  <Point X="-26.999572265625" Y="4.084614257813" />
                  <Point X="-26.943763671875" Y="4.0555625" />
                  <Point X="-26.934326171875" Y="4.051286621094" />
                  <Point X="-26.915044921875" Y="4.0437890625" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.88429296875" Y="4.034965332031" />
                  <Point X="-26.874158203125" Y="4.032834228516" />
                  <Point X="-26.85371484375" Y="4.029687744141" />
                  <Point X="-26.833046875" Y="4.028785644531" />
                  <Point X="-26.81240625" Y="4.030139160156" />
                  <Point X="-26.802125" Y="4.031379150391" />
                  <Point X="-26.780810546875" Y="4.035137939453" />
                  <Point X="-26.7707265625" Y="4.037489257812" />
                  <Point X="-26.750869140625" Y="4.043276855469" />
                  <Point X="-26.741095703125" Y="4.046713623047" />
                  <Point X="-26.700048828125" Y="4.063716064453" />
                  <Point X="-26.641921875" Y="4.087793212891" />
                  <Point X="-26.63258203125" Y="4.092273193359" />
                  <Point X="-26.614451171875" Y="4.102220214844" />
                  <Point X="-26.60566015625" Y="4.1076875" />
                  <Point X="-26.5879296875" Y="4.1201015625" />
                  <Point X="-26.579783203125" Y="4.126494140625" />
                  <Point X="-26.5642265625" Y="4.140135253906" />
                  <Point X="-26.5502421875" Y="4.155395507812" />
                  <Point X="-26.538009765625" Y="4.172081054688" />
                  <Point X="-26.5323515625" Y="4.180754882812" />
                  <Point X="-26.52153125" Y="4.199499023438" />
                  <Point X="-26.5168515625" Y="4.208735351562" />
                  <Point X="-26.5085234375" Y="4.2276640625" />
                  <Point X="-26.504875" Y="4.237356445312" />
                  <Point X="-26.491515625" Y="4.279728515625" />
                  <Point X="-26.472595703125" Y="4.339732910156" />
                  <Point X="-26.470025390625" Y="4.349764160156" />
                  <Point X="-26.465990234375" Y="4.370048828125" />
                  <Point X="-26.464525390625" Y="4.380302246094" />
                  <Point X="-26.462638671875" Y="4.401864257812" />
                  <Point X="-26.46230078125" Y="4.412217285156" />
                  <Point X="-26.462751953125" Y="4.4328984375" />
                  <Point X="-26.463541015625" Y="4.4432265625" />
                  <Point X="-26.479263671875" Y="4.562654785156" />
                  <Point X="-26.362306640625" Y="4.595445800781" />
                  <Point X="-25.931173828125" Y="4.7163203125" />
                  <Point X="-25.622640625" Y="4.7524296875" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.3056484375" Y="4.560221679688" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.648755859375" Y="4.684432617188" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.54857421875" Y="4.777336914062" />
                  <Point X="-24.172123046875" Y="4.737912109375" />
                  <Point X="-23.916853515625" Y="4.676282226562" />
                  <Point X="-23.546404296875" Y="4.586843261719" />
                  <Point X="-23.382087890625" Y="4.52724609375" />
                  <Point X="-23.141748046875" Y="4.440072753906" />
                  <Point X="-22.981072265625" Y="4.364930175781" />
                  <Point X="-22.749546875" Y="4.25665234375" />
                  <Point X="-22.59427734375" Y="4.166193847656" />
                  <Point X="-22.370568359375" Y="4.035860107422" />
                  <Point X="-22.224173828125" Y="3.931752929688" />
                  <Point X="-22.182216796875" Y="3.901915771484" />
                  <Point X="-22.578072265625" Y="3.216274169922" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.937623046875" Y="2.593106933594" />
                  <Point X="-22.946810546875" Y="2.573449707031" />
                  <Point X="-22.9558125" Y="2.54957421875" />
                  <Point X="-22.958697265625" Y="2.540602783203" />
                  <Point X="-22.967583984375" Y="2.507373535156" />
                  <Point X="-22.98016796875" Y="2.46031640625" />
                  <Point X="-22.9820859375" Y="2.451469482422" />
                  <Point X="-22.986353515625" Y="2.420222900391" />
                  <Point X="-22.987244140625" Y="2.383240234375" />
                  <Point X="-22.986587890625" Y="2.369580078125" />
                  <Point X="-22.983123046875" Y="2.340846191406" />
                  <Point X="-22.978216796875" Y="2.300155273438" />
                  <Point X="-22.97619921875" Y="2.289036865234" />
                  <Point X="-22.970857421875" Y="2.267115478516" />
                  <Point X="-22.967533203125" Y="2.2563125" />
                  <Point X="-22.959267578125" Y="2.234223876953" />
                  <Point X="-22.95468359375" Y="2.223891357422" />
                  <Point X="-22.944318359375" Y="2.203841308594" />
                  <Point X="-22.938537109375" Y="2.194123779297" />
                  <Point X="-22.920755859375" Y="2.167921142578" />
                  <Point X="-22.895578125" Y="2.130815185547" />
                  <Point X="-22.8901875" Y="2.123629638672" />
                  <Point X="-22.869537109375" Y="2.100124267578" />
                  <Point X="-22.842404296875" Y="2.075388671875" />
                  <Point X="-22.83174609375" Y="2.066983642578" />
                  <Point X="-22.805544921875" Y="2.049204101563" />
                  <Point X="-22.7684375" Y="2.024026000977" />
                  <Point X="-22.75871875" Y="2.018245361328" />
                  <Point X="-22.738669921875" Y="2.007881103516" />
                  <Point X="-22.72833984375" Y="2.003297363281" />
                  <Point X="-22.706251953125" Y="1.995031860352" />
                  <Point X="-22.695451171875" Y="1.991707763672" />
                  <Point X="-22.673529296875" Y="1.986364990234" />
                  <Point X="-22.662408203125" Y="1.984346557617" />
                  <Point X="-22.633673828125" Y="1.980881713867" />
                  <Point X="-22.592984375" Y="1.975975097656" />
                  <Point X="-22.583955078125" Y="1.975320678711" />
                  <Point X="-22.55242578125" Y="1.975497314453" />
                  <Point X="-22.5156875" Y="1.979822387695" />
                  <Point X="-22.50225390625" Y="1.982395507812" />
                  <Point X="-22.4690234375" Y="1.991281616211" />
                  <Point X="-22.421966796875" Y="2.003865112305" />
                  <Point X="-22.416005859375" Y="2.005670654297" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572875977" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.4892578125" Y="2.532884765625" />
                  <Point X="-21.059595703125" Y="2.780950439453" />
                  <Point X="-20.956041015625" Y="2.637033935547" />
                  <Point X="-20.874435546875" Y="2.502180175781" />
                  <Point X="-20.863115234375" Y="2.483471679688" />
                  <Point X="-21.357529296875" Y="2.104093261719" />
                  <Point X="-21.827046875" Y="1.743819702148" />
                  <Point X="-21.83185546875" Y="1.739874267578" />
                  <Point X="-21.847875" Y="1.725222045898" />
                  <Point X="-21.86533203125" Y="1.706602172852" />
                  <Point X="-21.87142578125" Y="1.699419189453" />
                  <Point X="-21.89533984375" Y="1.668219970703" />
                  <Point X="-21.92920703125" Y="1.624037841797" />
                  <Point X="-21.934361328125" Y="1.616604736328" />
                  <Point X="-21.95026171875" Y="1.589369140625" />
                  <Point X="-21.965236328125" Y="1.555544555664" />
                  <Point X="-21.969859375" Y="1.542672485352" />
                  <Point X="-21.978767578125" Y="1.510818115234" />
                  <Point X="-21.991384765625" Y="1.465708129883" />
                  <Point X="-21.993775390625" Y="1.454661987305" />
                  <Point X="-21.997228515625" Y="1.432364990234" />
                  <Point X="-21.998291015625" Y="1.421114135742" />
                  <Point X="-21.999107421875" Y="1.397542602539" />
                  <Point X="-21.998826171875" Y="1.386238891602" />
                  <Point X="-21.996921875" Y="1.363749267578" />
                  <Point X="-21.995298828125" Y="1.352563476562" />
                  <Point X="-21.987984375" Y="1.31712121582" />
                  <Point X="-21.97762890625" Y="1.266930664062" />
                  <Point X="-21.975400390625" Y="1.258231079102" />
                  <Point X="-21.96531640625" Y="1.228612304688" />
                  <Point X="-21.949712890625" Y="1.195372314453" />
                  <Point X="-21.943080078125" Y="1.183525756836" />
                  <Point X="-21.923189453125" Y="1.153293212891" />
                  <Point X="-21.8950234375" Y="1.11048046875" />
                  <Point X="-21.888263671875" Y="1.101428100586" />
                  <Point X="-21.873712890625" Y="1.084184936523" />
                  <Point X="-21.865921875" Y="1.075994262695" />
                  <Point X="-21.848677734375" Y="1.059903686523" />
                  <Point X="-21.83996484375" Y="1.052694702148" />
                  <Point X="-21.821751953125" Y="1.039367675781" />
                  <Point X="-21.812251953125" Y="1.033249267578" />
                  <Point X="-21.783427734375" Y="1.017023864746" />
                  <Point X="-21.742609375" Y="0.994046936035" />
                  <Point X="-21.73451953125" Y="0.989987121582" />
                  <Point X="-21.705318359375" Y="0.978083312988" />
                  <Point X="-21.66972265625" Y="0.968021118164" />
                  <Point X="-21.656328125" Y="0.965257629395" />
                  <Point X="-21.61735546875" Y="0.960106933594" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.555966796875" Y="0.952199768066" />
                  <Point X="-21.53428125" Y="0.951222839355" />
                  <Point X="-21.50878125" Y="0.952032409668" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-20.668482421875" Y="1.062188964844" />
                  <Point X="-20.295296875" Y="1.111319702148" />
                  <Point X="-20.2473125" Y="0.914209228516" />
                  <Point X="-20.22159765625" Y="0.749048950195" />
                  <Point X="-20.21612890625" Y="0.713920898438" />
                  <Point X="-20.7699296875" Y="0.565530151367" />
                  <Point X="-21.3080078125" Y="0.421352752686" />
                  <Point X="-21.313970703125" Y="0.419543548584" />
                  <Point X="-21.334376953125" Y="0.412136352539" />
                  <Point X="-21.357619140625" Y="0.40161920166" />
                  <Point X="-21.365994140625" Y="0.397316864014" />
                  <Point X="-21.404283203125" Y="0.375185424805" />
                  <Point X="-21.45850390625" Y="0.343844573975" />
                  <Point X="-21.466115234375" Y="0.338946258545" />
                  <Point X="-21.49122265625" Y="0.319873046875" />
                  <Point X="-21.518005859375" Y="0.294351074219" />
                  <Point X="-21.527203125" Y="0.284225402832" />
                  <Point X="-21.55017578125" Y="0.254952056885" />
                  <Point X="-21.582708984375" Y="0.213497711182" />
                  <Point X="-21.589146484375" Y="0.204206115723" />
                  <Point X="-21.60087109375" Y="0.184924377441" />
                  <Point X="-21.606158203125" Y="0.17493409729" />
                  <Point X="-21.615931640625" Y="0.153469680786" />
                  <Point X="-21.619994140625" Y="0.142927719116" />
                  <Point X="-21.62683984375" Y="0.121430160522" />
                  <Point X="-21.629623046875" Y="0.11047442627" />
                  <Point X="-21.63728125" Y="0.070488777161" />
                  <Point X="-21.648125" Y="0.013864057541" />
                  <Point X="-21.649396484375" Y="0.004966452122" />
                  <Point X="-21.6514140625" Y="-0.02626218605" />
                  <Point X="-21.64971875" Y="-0.062940380096" />
                  <Point X="-21.648125" Y="-0.07642388916" />
                  <Point X="-21.640466796875" Y="-0.116409835815" />
                  <Point X="-21.629623046875" Y="-0.173034561157" />
                  <Point X="-21.62683984375" Y="-0.183990432739" />
                  <Point X="-21.619994140625" Y="-0.205487838745" />
                  <Point X="-21.615931640625" Y="-0.216029663086" />
                  <Point X="-21.606158203125" Y="-0.23749407959" />
                  <Point X="-21.60087109375" Y="-0.247484054565" />
                  <Point X="-21.589146484375" Y="-0.266766082764" />
                  <Point X="-21.582708984375" Y="-0.276057861328" />
                  <Point X="-21.559736328125" Y="-0.305331054688" />
                  <Point X="-21.527203125" Y="-0.346785552979" />
                  <Point X="-21.52128125" Y="-0.353633789062" />
                  <Point X="-21.498857421875" Y="-0.375808013916" />
                  <Point X="-21.469822265625" Y="-0.39872567749" />
                  <Point X="-21.45850390625" Y="-0.406404876709" />
                  <Point X="-21.42021484375" Y="-0.4285362854" />
                  <Point X="-21.365994140625" Y="-0.45987713623" />
                  <Point X="-21.360501953125" Y="-0.462816009521" />
                  <Point X="-21.340845703125" Y="-0.47201473999" />
                  <Point X="-21.316974609375" Y="-0.481026916504" />
                  <Point X="-21.3080078125" Y="-0.483912719727" />
                  <Point X="-20.5460234375" Y="-0.68808605957" />
                  <Point X="-20.215119140625" Y="-0.776751342773" />
                  <Point X="-20.238380859375" Y="-0.931036010742" />
                  <Point X="-20.271333984375" Y="-1.075440917969" />
                  <Point X="-20.2721953125" Y="-1.079219604492" />
                  <Point X="-20.93919921875" Y="-0.991407226562" />
                  <Point X="-21.563216796875" Y="-0.909253540039" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042419434" />
                  <Point X="-21.63327734375" Y="-0.910840820312" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.7206640625" Y="-0.92900970459" />
                  <Point X="-21.82708203125" Y="-0.952140014648" />
                  <Point X="-21.842123046875" Y="-0.956742126465" />
                  <Point X="-21.8712421875" Y="-0.968365600586" />
                  <Point X="-21.885318359375" Y="-0.975387023926" />
                  <Point X="-21.913146484375" Y="-0.99227935791" />
                  <Point X="-21.925876953125" Y="-1.001530517578" />
                  <Point X="-21.949626953125" Y="-1.02200213623" />
                  <Point X="-21.9606484375" Y="-1.03322253418" />
                  <Point X="-22.0060703125" Y="-1.087850341797" />
                  <Point X="-22.070392578125" Y="-1.165210571289" />
                  <Point X="-22.078673828125" Y="-1.176848876953" />
                  <Point X="-22.093396484375" Y="-1.201235473633" />
                  <Point X="-22.099837890625" Y="-1.213984008789" />
                  <Point X="-22.1111796875" Y="-1.241369140625" />
                  <Point X="-22.11563671875" Y="-1.254934692383" />
                  <Point X="-22.122466796875" Y="-1.282580566406" />
                  <Point X="-22.12483984375" Y="-1.296661010742" />
                  <Point X="-22.131349609375" Y="-1.367406738281" />
                  <Point X="-22.140568359375" Y="-1.467591308594" />
                  <Point X="-22.140708984375" Y="-1.483315551758" />
                  <Point X="-22.138392578125" Y="-1.514580444336" />
                  <Point X="-22.135935546875" Y="-1.530121337891" />
                  <Point X="-22.128205078125" Y="-1.561742797852" />
                  <Point X="-22.123212890625" Y="-1.576667602539" />
                  <Point X="-22.11083984375" Y="-1.605480957031" />
                  <Point X="-22.103458984375" Y="-1.619369506836" />
                  <Point X="-22.061873046875" Y="-1.684055908203" />
                  <Point X="-22.002978515625" Y="-1.775659912109" />
                  <Point X="-21.998259765625" Y="-1.782348632812" />
                  <Point X="-21.980197265625" Y="-1.804459472656" />
                  <Point X="-21.95650390625" Y="-1.828124511719" />
                  <Point X="-21.947201171875" Y="-1.836277709961" />
                  <Point X="-21.24007421875" Y="-2.378875488281" />
                  <Point X="-20.912828125" Y="-2.629980224609" />
                  <Point X="-20.954529296875" Y="-2.697457519531" />
                  <Point X="-20.998724609375" Y="-2.760251953125" />
                  <Point X="-21.595771484375" Y="-2.415546386719" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.184970703125" Y="-2.079513916016" />
                  <Point X="-22.216876953125" Y="-2.069326416016" />
                  <Point X="-22.228888671875" Y="-2.066337646484" />
                  <Point X="-22.31832421875" Y="-2.050185546875" />
                  <Point X="-22.444978515625" Y="-2.027311889648" />
                  <Point X="-22.460638671875" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.555154296875" Y="-2.035136108398" />
                  <Point X="-22.5849296875" Y="-2.044959594727" />
                  <Point X="-22.59941015625" Y="-2.051108154297" />
                  <Point X="-22.6737109375" Y="-2.090211669922" />
                  <Point X="-22.778927734375" Y="-2.145587158203" />
                  <Point X="-22.791029296875" Y="-2.153169677734" />
                  <Point X="-22.8139609375" Y="-2.170063232422" />
                  <Point X="-22.824791015625" Y="-2.179374267578" />
                  <Point X="-22.84575" Y="-2.200333496094" />
                  <Point X="-22.855060546875" Y="-2.211162109375" />
                  <Point X="-22.871953125" Y="-2.234092529297" />
                  <Point X="-22.87953515625" Y="-2.246194335938" />
                  <Point X="-22.918638671875" Y="-2.320494384766" />
                  <Point X="-22.974015625" Y="-2.425712402344" />
                  <Point X="-22.9801640625" Y="-2.440193115234" />
                  <Point X="-22.98998828125" Y="-2.469971435547" />
                  <Point X="-22.9936640625" Y="-2.485269042969" />
                  <Point X="-22.99862109375" Y="-2.517442871094" />
                  <Point X="-22.999720703125" Y="-2.533134033203" />
                  <Point X="-22.99931640625" Y="-2.564484130859" />
                  <Point X="-22.9978125" Y="-2.580143066406" />
                  <Point X="-22.98166015625" Y="-2.669579833984" />
                  <Point X="-22.95878515625" Y="-2.796233398438" />
                  <Point X="-22.95698046875" Y="-2.804228759766" />
                  <Point X="-22.948763671875" Y="-2.831537353516" />
                  <Point X="-22.935931640625" Y="-2.862475097656" />
                  <Point X="-22.930453125" Y="-2.873578613281" />
                  <Point X="-22.4760546875" Y="-3.660622314453" />
                  <Point X="-22.264107421875" Y="-4.027723632812" />
                  <Point X="-22.2762421875" Y="-4.036082275391" />
                  <Point X="-22.7419765625" Y="-3.429127929688" />
                  <Point X="-23.16608203125" Y="-2.876422607422" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849625732422" />
                  <Point X="-23.21674609375" Y="-2.828004638672" />
                  <Point X="-23.22669921875" Y="-2.820646972656" />
                  <Point X="-23.314908203125" Y="-2.763936767578" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.453716796875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663873535156" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.688076171875" Y="-2.655393798828" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920125" Y="-2.699414794922" />
                  <Point X="-23.944505859375" Y="-2.714135742188" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-24.030630859375" Y="-2.7843515625" />
                  <Point X="-24.136123046875" Y="-2.872063720703" />
                  <Point X="-24.14734375" Y="-2.883086914062" />
                  <Point X="-24.167814453125" Y="-2.9068359375" />
                  <Point X="-24.177064453125" Y="-2.919561767578" />
                  <Point X="-24.19395703125" Y="-2.947387939453" />
                  <Point X="-24.20098046875" Y="-2.961467285156" />
                  <Point X="-24.21260546875" Y="-2.99058984375" />
                  <Point X="-24.21720703125" Y="-3.005633056641" />
                  <Point X="-24.239478515625" Y="-3.108106201172" />
                  <Point X="-24.271021484375" Y="-3.253220947266" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677001953" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520019531" />
                  <Point X="-24.166912109375" Y="-4.152313964844" />
                  <Point X="-24.17913671875" Y="-4.106691894531" />
                  <Point X="-24.344931640625" Y="-3.487937011719" />
                  <Point X="-24.347392578125" Y="-3.480122314453" />
                  <Point X="-24.357853515625" Y="-3.453578857422" />
                  <Point X="-24.3732109375" Y="-3.423815429688" />
                  <Point X="-24.37958984375" Y="-3.413209228516" />
                  <Point X="-24.44735546875" Y="-3.315573242188" />
                  <Point X="-24.543318359375" Y="-3.177308837891" />
                  <Point X="-24.553328125" Y="-3.165171386719" />
                  <Point X="-24.575212890625" Y="-3.142715820312" />
                  <Point X="-24.587087890625" Y="-3.132397705078" />
                  <Point X="-24.61334375" Y="-3.113153808594" />
                  <Point X="-24.6267578125" Y="-3.104936767578" />
                  <Point X="-24.6547578125" Y="-3.090829345703" />
                  <Point X="-24.66934375" Y="-3.084938964844" />
                  <Point X="-24.774205078125" Y="-3.052393554688" />
                  <Point X="-24.922703125" Y="-3.006305664062" />
                  <Point X="-24.936623046875" Y="-3.003109619141" />
                  <Point X="-24.964783203125" Y="-2.998840087891" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.169845703125" Y="-3.038850341797" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.3329296875" Y="-3.090829589844" />
                  <Point X="-25.3609296875" Y="-3.104937255859" />
                  <Point X="-25.37434375" Y="-3.113153808594" />
                  <Point X="-25.400599609375" Y="-3.132397705078" />
                  <Point X="-25.412474609375" Y="-3.142717285156" />
                  <Point X="-25.434359375" Y="-3.165173828125" />
                  <Point X="-25.444369140625" Y="-3.177310791016" />
                  <Point X="-25.5121328125" Y="-3.274946533203" />
                  <Point X="-25.608095703125" Y="-3.413211181641" />
                  <Point X="-25.61246875" Y="-3.420132080078" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213378906" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.877541015625" Y="-4.364171386719" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.677122786386" Y="3.0065991661" />
                  <Point X="-29.168943376718" Y="2.249261870565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.594737587063" Y="2.95903379449" />
                  <Point X="-29.093341829753" Y="2.191250590363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.941765164375" Y="3.790095696941" />
                  <Point X="-28.031592768557" Y="3.651773316478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.512352378767" Y="2.911468436699" />
                  <Point X="-29.01774027078" Y="2.133239328652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.553827899441" Y="1.307736771742" />
                  <Point X="-29.695578942024" Y="1.089459307685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.716167170196" Y="3.9630586905" />
                  <Point X="-27.978282559588" Y="3.559436385899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.429967118682" Y="2.863903158654" />
                  <Point X="-28.942138688073" Y="2.075228103488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.449475099652" Y="1.293998538429" />
                  <Point X="-29.750025621582" Y="0.831191319854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.524413898256" Y="4.083905382074" />
                  <Point X="-27.924971930668" Y="3.467100101989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.347581858597" Y="2.81633788061" />
                  <Point X="-28.866537105366" Y="2.017216878323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.345122299863" Y="1.280260305116" />
                  <Point X="-29.783453087079" Y="0.605290083321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.34720219896" Y="4.182360015415" />
                  <Point X="-27.871661301748" Y="3.374763818078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.265196598513" Y="2.768772602565" />
                  <Point X="-28.790935522659" Y="1.959205653159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.240769500074" Y="1.266522071803" />
                  <Point X="-29.6947114183" Y="0.567512816321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.184723489436" Y="4.258127833993" />
                  <Point X="-27.818350672827" Y="3.282427534167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.174246593037" Y="2.734395875872" />
                  <Point X="-28.715333939951" Y="1.901194427995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.136416745823" Y="1.252783768369" />
                  <Point X="-29.598226175096" Y="0.541659608272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.123371966899" Y="4.17817344044" />
                  <Point X="-27.768729321888" Y="3.184410260353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.067968434206" Y="2.723622435491" />
                  <Point X="-28.639732357244" Y="1.843183202831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.032064004085" Y="1.239045445665" />
                  <Point X="-29.501740931892" Y="0.515806400224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.052226193021" Y="4.113300871375" />
                  <Point X="-27.748679271283" Y="3.040857177218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.93659472202" Y="2.751492758468" />
                  <Point X="-28.564130774537" Y="1.785171977666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.927711262348" Y="1.22530712296" />
                  <Point X="-29.405255688688" Y="0.489953192175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.968189962086" Y="4.068277865498" />
                  <Point X="-28.494918888291" Y="1.717321482793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.823358520611" Y="1.211568800255" />
                  <Point X="-29.308770413953" Y="0.464100032679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.877479302861" Y="4.033532577899" />
                  <Point X="-28.443083835813" Y="1.622713010418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.713130831487" Y="1.206877103196" />
                  <Point X="-29.212285126769" Y="0.438246892354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.407527598395" Y="4.582767288707" />
                  <Point X="-26.469373854249" Y="4.487532406175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.759512811388" Y="4.040757591439" />
                  <Point X="-28.426825274762" Y="1.473321545359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.569081068569" Y="1.254266832575" />
                  <Point X="-29.115799839585" Y="0.412393752029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.269038048295" Y="4.621595041177" />
                  <Point X="-26.491956169171" Y="4.278331237042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.600366608519" Y="4.111393799777" />
                  <Point X="-29.0193145524" Y="0.386540611703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.676308872919" Y="-0.625141923889" />
                  <Point X="-29.770030914859" Y="-0.769461212609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.130548611229" Y="4.660422619589" />
                  <Point X="-28.922829265216" Y="0.360687471378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.539171275075" Y="-0.58839599533" />
                  <Point X="-29.74958763935" Y="-0.912408782491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.992059174163" Y="4.699250198002" />
                  <Point X="-28.826343978032" Y="0.334834331053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.402033677231" Y="-0.551650066772" />
                  <Point X="-29.722589000519" Y="-1.045261978069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.862478058393" Y="4.724360164565" />
                  <Point X="-28.729858690847" Y="0.308981190728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.264896079386" Y="-0.514904138214" />
                  <Point X="-29.690612069301" Y="-1.170449275619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.739886087365" Y="4.738707792013" />
                  <Point X="-28.633373403663" Y="0.283128050403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.127758481542" Y="-0.478158209656" />
                  <Point X="-29.652071472424" Y="-1.285529414387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.617294118068" Y="4.753055416793" />
                  <Point X="-28.536888116479" Y="0.257274910078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.990620909285" Y="-0.441412320498" />
                  <Point X="-29.528207018097" Y="-1.269222334492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.494702186764" Y="4.767402983072" />
                  <Point X="-28.446821551916" Y="0.221537803675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.853483338069" Y="-0.404666432944" />
                  <Point X="-29.404342563771" Y="-1.252915254596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.372110255459" Y="4.78175054935" />
                  <Point X="-28.370647575279" Y="0.16440798787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.716345766853" Y="-0.36792054539" />
                  <Point X="-29.280478109444" Y="-1.2366081747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.333995474053" Y="4.666014712258" />
                  <Point X="-28.316082281819" Y="0.074003717925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.579208195637" Y="-0.331174657836" />
                  <Point X="-29.156613665698" Y="-1.220301111097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.300909432204" Y="4.542535295308" />
                  <Point X="-28.29810687865" Y="-0.072744042108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.428415068086" Y="-0.273401057519" />
                  <Point X="-29.032749242754" Y="-1.203994079526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.267823504145" Y="4.419055703138" />
                  <Point X="-28.90888481981" Y="-1.187687047956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.234737576086" Y="4.295576110968" />
                  <Point X="-28.785020396866" Y="-1.171380016385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.189946981063" Y="4.190120125368" />
                  <Point X="-28.661155973923" Y="-1.155072984814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.120994151039" Y="4.121870718894" />
                  <Point X="-28.537291550979" Y="-1.138765953244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.579922870205" Y="4.780619973593" />
                  <Point X="-24.653229102209" Y="4.667738275301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.029433574867" Y="4.088434188624" />
                  <Point X="-28.413427128035" Y="-1.122458921673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.473861666051" Y="4.769512452305" />
                  <Point X="-24.732797148781" Y="4.370786774561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.906884859388" Y="4.102715208367" />
                  <Point X="-28.289562705091" Y="-1.106151890103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.134820432333" Y="-2.407734649676" />
                  <Point X="-29.170111144437" Y="-2.462077580793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.367800466507" Y="4.758404923921" />
                  <Point X="-28.17475260211" Y="-1.103787288614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.909035528635" Y="-2.234483840698" />
                  <Point X="-29.115885661566" Y="-2.553005113159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.261739266962" Y="4.747297395536" />
                  <Point X="-28.082645573159" Y="-1.136382355396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.683250624938" Y="-2.061233031719" />
                  <Point X="-29.061660311404" Y="-2.643932849877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.156940114816" Y="4.734246484579" />
                  <Point X="-28.008501978465" Y="-1.196638685218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.457465478244" Y="-1.88798184856" />
                  <Point X="-29.007434961242" Y="-2.734860586596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.059018436601" Y="4.710605192476" />
                  <Point X="-27.956622520965" Y="-1.291178779857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.231680151701" Y="-1.714730388456" />
                  <Point X="-28.946745176862" Y="-2.815833967552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.961096758386" Y="4.686963900373" />
                  <Point X="-28.885621120033" Y="-2.896138627578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.863175175992" Y="4.663322460721" />
                  <Point X="-28.824497063204" Y="-2.976443287604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.765253672574" Y="4.639680899453" />
                  <Point X="-28.704988841177" Y="-2.966844217201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.667332169157" Y="4.616039338185" />
                  <Point X="-28.523768226417" Y="-2.862216395395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.56941066574" Y="4.592397776918" />
                  <Point X="-28.342547722995" Y="-2.757588745035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.476264035102" Y="4.56140355635" />
                  <Point X="-28.161327310265" Y="-2.652961234327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.384583763501" Y="4.528151340879" />
                  <Point X="-27.980106897535" Y="-2.54833372362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.292904009284" Y="4.494898328704" />
                  <Point X="-27.798886484805" Y="-2.443706212913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.201224269547" Y="4.461645294234" />
                  <Point X="-27.629527084962" Y="-2.357343060386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.111228185898" Y="4.425799656741" />
                  <Point X="-27.514055449462" Y="-2.353959788149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.024341622995" Y="4.385165777198" />
                  <Point X="-27.42684751594" Y="-2.394098800335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.937455183034" Y="4.34453170834" />
                  <Point X="-27.357968185434" Y="-2.462461386141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.850568865035" Y="4.303897451679" />
                  <Point X="-27.313286466209" Y="-2.568085025768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.763682547035" Y="4.263263195018" />
                  <Point X="-27.663174821616" Y="-3.28129329909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.680735253605" Y="4.21656337243" />
                  <Point X="-27.931781311649" Y="-3.869338475729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.598553324644" Y="4.168684991913" />
                  <Point X="-27.856265924311" Y="-3.927482430122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.516371903397" Y="4.120805829583" />
                  <Point X="-27.780750343115" Y="-3.985626086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.434190510016" Y="4.072926624343" />
                  <Point X="-27.702276894672" Y="-4.039215025736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.353068888612" Y="4.023415513369" />
                  <Point X="-27.62148768267" Y="-4.089238002304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.27558022644" Y="3.968310135755" />
                  <Point X="-27.540698470668" Y="-4.139260978872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.198091505185" Y="3.913204849123" />
                  <Point X="-27.310844315539" Y="-3.959744072185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.903878355407" Y="2.651960952961" />
                  <Point X="-27.110739439645" Y="-3.826037038291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.984614088495" Y="2.353211372667" />
                  <Point X="-26.965270087706" Y="-3.776461333516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.954942292869" Y="2.224474477582" />
                  <Point X="-26.855486680804" Y="-3.781837165203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.899110295755" Y="2.136020760214" />
                  <Point X="-26.746836751848" Y="-3.788958399867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.83099846966" Y="2.066476321255" />
                  <Point X="-26.638186880756" Y="-3.796079723633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.751476737374" Y="2.014501597081" />
                  <Point X="-26.543274488666" Y="-3.824354910004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.658120863811" Y="1.983829582363" />
                  <Point X="-26.46608831803" Y="-3.879926083735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.550077886492" Y="1.975773724139" />
                  <Point X="-26.393916830202" Y="-3.943219191827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.417726543474" Y="2.005149466582" />
                  <Point X="-26.32174531129" Y="-4.006512252052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.245146117784" Y="2.096472563959" />
                  <Point X="-26.250268174158" Y="-4.070874566453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.063925584716" Y="2.201100259969" />
                  <Point X="-26.195002279136" Y="-4.160200004599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.882705051649" Y="2.30572795598" />
                  <Point X="-25.439961062756" Y="-3.171965942842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.640128569491" Y="-3.480196873358" />
                  <Point X="-26.16496938497" Y="-4.288380856694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.701484518582" Y="2.41035565199" />
                  <Point X="-25.258009878008" Y="-3.066213141908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.720635677342" Y="-3.778594401659" />
                  <Point X="-26.138408375399" Y="-4.421907942236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.520263985515" Y="2.514983348" />
                  <Point X="-25.116141538692" Y="-3.022182510305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.800203895032" Y="-4.075546165897" />
                  <Point X="-26.120076971665" Y="-4.568107509472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.339043378127" Y="2.619611158454" />
                  <Point X="-24.98701114934" Y="-2.997766601563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.879772138899" Y="-4.372497970444" />
                  <Point X="-26.116871735225" Y="-4.737599331761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.157822755398" Y="2.724238992531" />
                  <Point X="-21.762201530331" Y="1.793577292138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.997305596501" Y="1.431548777793" />
                  <Point X="-24.886565514784" Y="-3.017521341724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.959341290146" Y="-4.669451172234" />
                  <Point X="-26.018407147556" Y="-4.760404616617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.032328590337" Y="2.743055606888" />
                  <Point X="-21.536416541961" Y="1.966828231502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.980837186271" Y="1.282480452133" />
                  <Point X="-24.792291881234" Y="-3.046780129993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.972789482278" Y="2.660310339781" />
                  <Point X="-21.310631417133" Y="2.14007938099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.937415692938" Y="1.174916234808" />
                  <Point X="-24.698018492078" Y="-3.076039294596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.916772220851" Y="2.572141904438" />
                  <Point X="-21.084845771805" Y="2.313331331979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.878996594943" Y="1.090446303445" />
                  <Point X="-24.610296731214" Y="-3.115387082061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.805378801407" Y="1.02938031084" />
                  <Point X="-24.540175431657" Y="-3.18183720324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.721209965101" Y="0.98456149933" />
                  <Point X="-24.48165600653" Y="-3.266152644368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.623305739837" Y="0.960893332039" />
                  <Point X="-24.084680584819" Y="-2.829291554563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.226265925243" Y="-3.047313859672" />
                  <Point X="-24.423136403608" Y="-3.350467811715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.515933027621" Y="0.951805356066" />
                  <Point X="-23.87429742551" Y="-2.67975735215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.275271440648" Y="-3.297203189462" />
                  <Point X="-24.366252351521" Y="-3.437301506491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.392910389069" Y="0.966816153345" />
                  <Point X="-23.748831755471" Y="-2.660984616279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.259685639726" Y="-3.447630614272" />
                  <Point X="-24.327677770062" Y="-3.552329313592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.269045938159" Y="0.983123227979" />
                  <Point X="-23.628357757964" Y="-2.649898382052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.240592486204" Y="-3.592657189699" />
                  <Point X="-24.294591601975" Y="-3.675808536151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.14518148725" Y="0.999430302613" />
                  <Point X="-23.517703253976" Y="-2.653932841857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.221499332681" Y="-3.737683765125" />
                  <Point X="-24.261505433888" Y="-3.799287758711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.02131703634" Y="1.015737377247" />
                  <Point X="-21.45713575916" Y="0.344635395402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.638164764829" Y="0.065875172139" />
                  <Point X="-23.42845839773" Y="-2.690935268106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.202406179159" Y="-3.882710340551" />
                  <Point X="-24.2284192658" Y="-3.922766981271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.897452585431" Y="1.032044451881" />
                  <Point X="-21.291097962912" Y="0.42588372693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.641104663527" Y="-0.113079328448" />
                  <Point X="-23.348547183914" Y="-2.742310243318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.183313025637" Y="-4.027736915977" />
                  <Point X="-24.195333097713" Y="-4.04624620383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.773588134521" Y="1.048351526515" />
                  <Point X="-21.153960420915" Y="0.462629569491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.607175350391" Y="-0.235260221486" />
                  <Point X="-23.268636111259" Y="-2.7936854359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.64972368729" Y="1.064658595484" />
                  <Point X="-21.016822878918" Y="0.499375412051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.548611248843" Y="-0.319506866959" />
                  <Point X="-22.711402819988" Y="-2.110048867584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.999456605033" Y="-2.55361279887" />
                  <Point X="-23.19150476837" Y="-2.849341036958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.525859260673" Y="1.080965632711" />
                  <Point X="-20.879685336921" Y="0.536121254612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.481034725548" Y="-0.389875599945" />
                  <Point X="-21.851625383335" Y="-0.960535169788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.133908245174" Y="-1.39521265862" />
                  <Point X="-22.548430206982" Y="-2.033520504339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.977271587372" Y="-2.693878321035" />
                  <Point X="-23.128025459035" Y="-2.926018926169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.401994834056" Y="1.097272669938" />
                  <Point X="-20.742547776219" Y="0.572867125978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.40033071859" Y="-0.440029780774" />
                  <Point X="-21.717420101614" Y="-0.928304612091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.128362224817" Y="-1.561099989766" />
                  <Point X="-22.432578121309" Y="-2.029551390211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.950229715827" Y="-2.826664944071" />
                  <Point X="-23.066674116863" Y="-3.005973597459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.291016519477" Y="1.093736834717" />
                  <Point X="-20.605410140537" Y="0.6096131128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.31424958724" Y="-0.481903916147" />
                  <Point X="-21.591086142807" Y="-0.908194828768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.078321197077" Y="-1.658471017981" />
                  <Point X="-22.331194167357" Y="-2.047861245211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.900788443117" Y="-2.924959514043" />
                  <Point X="-23.005322774692" Y="-3.08592826875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.260131669077" Y="0.966867880178" />
                  <Point X="-20.468272504855" Y="0.646359099623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.217950363499" Y="-0.50804356905" />
                  <Point X="-21.485171733502" Y="-0.919528394299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.021968545242" Y="-1.746122997386" />
                  <Point X="-22.229810280516" Y="-2.066171203553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.847478037208" Y="-3.017296141361" />
                  <Point X="-22.94397143252" Y="-3.165882940041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.234499010876" Y="0.831911258887" />
                  <Point X="-20.331134869174" Y="0.683105086446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.121465101257" Y="-0.533896747782" />
                  <Point X="-21.380818999023" Y="-0.933266728181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.959805148933" Y="-1.824827214961" />
                  <Point X="-22.13954334388" Y="-2.10159976402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.794167631299" Y="-3.109632768679" />
                  <Point X="-22.882620090348" Y="-3.245837611331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.024979839014" Y="-0.559749926514" />
                  <Point X="-21.276466264544" Y="-0.947005062062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.884974654628" Y="-1.884025812139" />
                  <Point X="-22.057158009507" Y="-2.149164927671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.740857225391" Y="-3.201969395997" />
                  <Point X="-22.821268748176" Y="-3.325792282622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.928494576772" Y="-0.585603105246" />
                  <Point X="-21.172113530065" Y="-0.960743395944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.809373041223" Y="-1.942036990033" />
                  <Point X="-21.974772675135" Y="-2.196730091322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.687546819482" Y="-3.294306023315" />
                  <Point X="-22.759917406005" Y="-3.405746953912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.832009314529" Y="-0.611456283978" />
                  <Point X="-21.067760795586" Y="-0.974481729826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.733771427818" Y="-2.000048167926" />
                  <Point X="-21.892387340762" Y="-2.244295254973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.634236413573" Y="-3.386642650633" />
                  <Point X="-22.698565970256" Y="-3.485701481107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.735524052286" Y="-0.63730946271" />
                  <Point X="-20.963408061107" Y="-0.988220063707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.658169814412" Y="-2.05805934582" />
                  <Point X="-21.810002006389" Y="-2.291860418624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.580926007664" Y="-3.478979277952" />
                  <Point X="-22.637214495834" Y="-3.56565594875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.639038790044" Y="-0.663162641442" />
                  <Point X="-20.85905528488" Y="-1.001958333301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.582568201007" Y="-2.116070523713" />
                  <Point X="-21.727616672017" Y="-2.339425582275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.527615601756" Y="-3.57131590527" />
                  <Point X="-22.575863021411" Y="-3.645610416392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.542553525707" Y="-0.689015816949" />
                  <Point X="-20.754702496041" Y="-1.015696583476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.506966587602" Y="-2.174081701607" />
                  <Point X="-21.645231337644" Y="-2.386990745926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.474305187547" Y="-3.663652519807" />
                  <Point X="-22.514511546989" Y="-3.725564884035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.446068205244" Y="-0.714868906029" />
                  <Point X="-20.650349707202" Y="-1.02943483365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.431364974197" Y="-2.2320928795" />
                  <Point X="-21.562846019406" Y="-2.434555934422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.420994528729" Y="-3.755988757679" />
                  <Point X="-22.453160072567" Y="-3.805519351677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.34958288478" Y="-0.740721995108" />
                  <Point X="-20.545996918363" Y="-1.043173083825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.355763360792" Y="-2.290104057394" />
                  <Point X="-21.480460725404" Y="-2.482121160239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.36768386991" Y="-3.84832499555" />
                  <Point X="-22.391808598144" Y="-3.88547381932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.253097564316" Y="-0.766575084188" />
                  <Point X="-20.441644129525" Y="-1.056911333999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.280161747387" Y="-2.348115235287" />
                  <Point X="-21.398075431402" Y="-2.529686386055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.314373211092" Y="-3.940661233421" />
                  <Point X="-22.330457123722" Y="-3.965428286962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.23588803004" Y="-0.914502178895" />
                  <Point X="-20.337291340686" Y="-1.070649584174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.204560128906" Y="-2.406126405364" />
                  <Point X="-21.3156901374" Y="-2.577251611872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.128958504694" Y="-2.464137566616" />
                  <Point X="-21.233304843398" Y="-2.624816837689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.053356880482" Y="-2.522148727868" />
                  <Point X="-21.150919549396" Y="-2.672382063505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.97775525627" Y="-2.580159889121" />
                  <Point X="-21.068534255394" Y="-2.719947289322" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.3626640625" Y="-4.1558671875" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.6034453125" Y="-3.423908203125" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.830525390625" Y="-3.233854492188" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.113525390625" Y="-3.220311767578" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643798828" />
                  <Point X="-25.35604296875" Y="-3.383279541016" />
                  <Point X="-25.452005859375" Y="-3.521544189453" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.694013671875" Y="-4.413347167969" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.9260390625" Y="-4.971879394531" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.221169921875" Y="-4.906952148438" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.329107421875" Y="-4.702633300781" />
                  <Point X="-26.3091484375" Y="-4.5510390625" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.334734375" Y="-4.408815429688" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.482826171875" Y="-4.117962402344" />
                  <Point X="-26.61954296875" Y="-3.998064208984" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.777375" Y="-3.977364501953" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791015625" />
                  <Point X="-27.096646484375" Y="-4.045131591797" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157293945313" />
                  <Point X="-27.391546875" Y="-4.329077636719" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.600482421875" Y="-4.325717773438" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.02328125" Y="-4.038682128906" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.81414453125" Y="-3.162783691406" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.59759375" />
                  <Point X="-27.513978515625" Y="-2.568764648438" />
                  <Point X="-27.531326171875" Y="-2.551416015625" />
                  <Point X="-27.56015625" Y="-2.537198730469" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.34741796875" Y="-2.979793701172" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-28.959962890625" Y="-3.112174804688" />
                  <Point X="-29.161697265625" Y="-2.847136962891" />
                  <Point X="-29.281748046875" Y="-2.645829589844" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-28.702880859375" Y="-1.836806518555" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.396013183594" />
                  <Point X="-28.1381171875" Y="-1.366265869141" />
                  <Point X="-28.140326171875" Y="-1.334595703125" />
                  <Point X="-28.161158203125" Y="-1.310639526367" />
                  <Point X="-28.187640625" Y="-1.295052978516" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.173572265625" Y="-1.414173339844" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.848490234375" Y="-1.320090087891" />
                  <Point X="-29.927392578125" Y="-1.011188293457" />
                  <Point X="-29.95915234375" Y="-0.789120056152" />
                  <Point X="-29.99839453125" Y="-0.5147421875" />
                  <Point X="-29.171572265625" Y="-0.293195648193" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.53041015625" Y="-0.113453697205" />
                  <Point X="-28.502322265625" Y="-0.090645721436" />
                  <Point X="-28.491068359375" Y="-0.063570365906" />
                  <Point X="-28.483400390625" Y="-0.031280040741" />
                  <Point X="-28.489474609375" Y="-0.004123178959" />
                  <Point X="-28.502322265625" Y="0.028085645676" />
                  <Point X="-28.52562890625" Y="0.047576118469" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.427119140625" Y="0.299109039307" />
                  <Point X="-29.998185546875" Y="0.452126068115" />
                  <Point X="-29.96849609375" Y="0.652771118164" />
                  <Point X="-29.91764453125" Y="0.996414978027" />
                  <Point X="-29.853708984375" Y="1.232357788086" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.19347265625" Y="1.451934814453" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.70628125" Y="1.403882446289" />
                  <Point X="-28.67027734375" Y="1.415234008789" />
                  <Point X="-28.651533203125" Y="1.426056274414" />
                  <Point X="-28.6391171875" Y="1.443786743164" />
                  <Point X="-28.628916015625" Y="1.468414916992" />
                  <Point X="-28.614470703125" Y="1.503291137695" />
                  <Point X="-28.610712890625" Y="1.524606201172" />
                  <Point X="-28.61631640625" Y="1.545512451172" />
                  <Point X="-28.628625" Y="1.569157714844" />
                  <Point X="-28.646056640625" Y="1.602642211914" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-29.1588046875" Y="2.001992797852" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.357603515625" Y="2.448488769531" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-28.990654296875" Y="3.004695556641" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.42951953125" Y="3.083038085938" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.10310546875" Y="2.917336914062" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775146484" />
                  <Point X="-28.013251953125" Y="2.927404296875" />
                  <Point X="-27.988119140625" Y="2.952536865234" />
                  <Point X="-27.95252734375" Y="2.988127685547" />
                  <Point X="-27.9408984375" Y="3.006382080078" />
                  <Point X="-27.938072265625" Y="3.027841064453" />
                  <Point X="-27.941169921875" Y="3.063248779297" />
                  <Point X="-27.945556640625" Y="3.113390625" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.1731171875" Y="3.51690234375" />
                  <Point X="-28.30727734375" Y="3.749276611328" />
                  <Point X="-28.097013671875" Y="3.910484863281" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.48613671875" Y="4.322524902344" />
                  <Point X="-27.141548828125" Y="4.513971679688" />
                  <Point X="-27.05059375" Y="4.395436523437" />
                  <Point X="-26.967826171875" Y="4.287572753906" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.911837890625" Y="4.253145507813" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.83512109375" Y="4.218491699219" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.772759765625" Y="4.239252929688" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.69690234375" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.29448828125" />
                  <Point X="-26.67272265625" Y="4.336860351562" />
                  <Point X="-26.653802734375" Y="4.396864746094" />
                  <Point X="-26.651916015625" Y="4.418426757812" />
                  <Point X="-26.677046875" Y="4.609314941406" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.413599609375" Y="4.778391601562" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.6447265625" Y="4.941141601562" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.12212109375" Y="4.609397460938" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.832283203125" Y="4.733608398438" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.52878515625" Y="4.966303710938" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.872263671875" Y="4.860975585938" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.317302734375" Y="4.705860351562" />
                  <Point X="-23.068970703125" Y="4.615788085938" />
                  <Point X="-22.900583984375" Y="4.537039550781" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.4986328125" Y="4.330364746094" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-22.114060546875" Y="4.086592285156" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.413529296875" Y="3.121274169922" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491514892578" />
                  <Point X="-22.784033203125" Y="2.458285644531" />
                  <Point X="-22.7966171875" Y="2.411228515625" />
                  <Point X="-22.797955078125" Y="2.392326171875" />
                  <Point X="-22.794490234375" Y="2.363592285156" />
                  <Point X="-22.789583984375" Y="2.322901367188" />
                  <Point X="-22.781318359375" Y="2.300812744141" />
                  <Point X="-22.763537109375" Y="2.274610107422" />
                  <Point X="-22.738359375" Y="2.237504150391" />
                  <Point X="-22.72505859375" Y="2.224203369141" />
                  <Point X="-22.698857421875" Y="2.206423828125" />
                  <Point X="-22.66175" Y="2.181245849609" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.610927734375" Y="2.169515380859" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5513359375" Y="2.165946289062" />
                  <Point X="-22.51810546875" Y="2.174832275391" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.5842578125" Y="2.6974296875" />
                  <Point X="-21.005751953125" Y="3.031430419922" />
                  <Point X="-20.9345234375" Y="2.932438964844" />
                  <Point X="-20.79740234375" Y="2.741874267578" />
                  <Point X="-20.711880859375" Y="2.600547851562" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.241865234375" Y="1.953356323242" />
                  <Point X="-21.7113828125" Y="1.593082641602" />
                  <Point X="-21.72062890625" Y="1.583833374023" />
                  <Point X="-21.74454296875" Y="1.552634155273" />
                  <Point X="-21.77841015625" Y="1.508452026367" />
                  <Point X="-21.78687890625" Y="1.491501708984" />
                  <Point X="-21.795787109375" Y="1.459647216797" />
                  <Point X="-21.808404296875" Y="1.414537231445" />
                  <Point X="-21.809220703125" Y="1.390965820312" />
                  <Point X="-21.80190625" Y="1.35552355957" />
                  <Point X="-21.79155078125" Y="1.305333007812" />
                  <Point X="-21.784353515625" Y="1.287955810547" />
                  <Point X="-21.764462890625" Y="1.257723266602" />
                  <Point X="-21.736296875" Y="1.214910400391" />
                  <Point X="-21.719052734375" Y="1.198819824219" />
                  <Point X="-21.690228515625" Y="1.182594482422" />
                  <Point X="-21.64941015625" Y="1.159617553711" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.5924609375" Y="1.148468994141" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.693283203125" Y="1.250563598633" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.119701171875" Y="1.193287841797" />
                  <Point X="-20.06080859375" Y="0.951367736816" />
                  <Point X="-20.033859375" Y="0.778279052734" />
                  <Point X="-20.002140625" Y="0.574556335449" />
                  <Point X="-20.72075390625" Y="0.382004333496" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819198608" />
                  <Point X="-21.309201171875" Y="0.210687652588" />
                  <Point X="-21.363421875" Y="0.179346832275" />
                  <Point X="-21.377734375" Y="0.166926849365" />
                  <Point X="-21.40070703125" Y="0.137653594971" />
                  <Point X="-21.433240234375" Y="0.096199172974" />
                  <Point X="-21.443013671875" Y="0.074734649658" />
                  <Point X="-21.450671875" Y="0.034748916626" />
                  <Point X="-21.461515625" Y="-0.021875907898" />
                  <Point X="-21.461515625" Y="-0.040684169769" />
                  <Point X="-21.453857421875" Y="-0.080669906616" />
                  <Point X="-21.443013671875" Y="-0.137294723511" />
                  <Point X="-21.433240234375" Y="-0.158759246826" />
                  <Point X="-21.410267578125" Y="-0.188032348633" />
                  <Point X="-21.377734375" Y="-0.229486923218" />
                  <Point X="-21.363421875" Y="-0.241906906128" />
                  <Point X="-21.3251328125" Y="-0.264038299561" />
                  <Point X="-21.270912109375" Y="-0.295379272461" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.49684765625" Y="-0.504560272217" />
                  <Point X="-20.001931640625" Y="-0.637172546387" />
                  <Point X="-20.018685546875" Y="-0.748302856445" />
                  <Point X="-20.051568359375" Y="-0.966412963867" />
                  <Point X="-20.086095703125" Y="-1.117712890625" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-20.964" Y="-1.179781738281" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.68030859375" Y="-1.114674682617" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.8145546875" Y="-1.154697387695" />
                  <Point X="-21.8599765625" Y="-1.209325439453" />
                  <Point X="-21.924298828125" Y="-1.286685424805" />
                  <Point X="-21.935640625" Y="-1.314070678711" />
                  <Point X="-21.942150390625" Y="-1.38481628418" />
                  <Point X="-21.951369140625" Y="-1.485000976562" />
                  <Point X="-21.943638671875" Y="-1.516622436523" />
                  <Point X="-21.902052734375" Y="-1.581308837891" />
                  <Point X="-21.843158203125" Y="-1.672912963867" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-21.12441015625" Y="-2.228138183594" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.703177734375" Y="-2.652154541016" />
                  <Point X="-20.795865234375" Y="-2.802138183594" />
                  <Point X="-20.86727734375" Y="-2.903604492188" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.690771484375" Y="-2.580091308594" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.35209375" Y="-2.237160644531" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.58522265625" Y="-2.258348388672" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.334683105469" />
                  <Point X="-22.750501953125" Y="-2.408983154297" />
                  <Point X="-22.80587890625" Y="-2.514201171875" />
                  <Point X="-22.8108359375" Y="-2.546375" />
                  <Point X="-22.79468359375" Y="-2.635811767578" />
                  <Point X="-22.77180859375" Y="-2.762465332031" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.3115078125" Y="-3.565622314453" />
                  <Point X="-22.01332421875" Y="-4.082087646484" />
                  <Point X="-22.054716796875" Y="-4.11165234375" />
                  <Point X="-22.16470703125" Y="-4.190215820312" />
                  <Point X="-22.244541015625" Y="-4.241891113281" />
                  <Point X="-22.32022265625" Y="-4.29087890625" />
                  <Point X="-22.89271484375" Y="-3.544792724609" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.417658203125" Y="-2.923757080078" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.670666015625" Y="-2.844594482422" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.909158203125" Y="-2.930447265625" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045985595703" />
                  <Point X="-24.053814453125" Y="-3.148458740234" />
                  <Point X="-24.085357421875" Y="-3.293573486328" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-23.957296875" Y="-4.288852050781" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.90159765625" Y="-4.940438476562" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.079412109375" Y="-4.976645996094" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#158" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.079981186327" Y="4.653618339828" Z="1.05" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.05" />
                  <Point X="-0.656696476724" Y="5.022082754629" Z="1.05" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.05" />
                  <Point X="-1.433255108269" Y="4.857815414216" Z="1.05" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.05" />
                  <Point X="-1.732776755986" Y="4.63406860881" Z="1.05" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.05" />
                  <Point X="-1.726565220585" Y="4.383176266887" Z="1.05" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.05" />
                  <Point X="-1.798052375923" Y="4.316726810699" Z="1.05" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.05" />
                  <Point X="-1.894906586973" Y="4.328776350759" Z="1.05" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.05" />
                  <Point X="-2.017081832398" Y="4.457154943855" Z="1.05" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.05" />
                  <Point X="-2.51657756674" Y="4.397512577589" Z="1.05" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.05" />
                  <Point X="-3.133591612369" Y="3.981444782805" Z="1.05" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.05" />
                  <Point X="-3.222574465305" Y="3.523182335294" Z="1.05" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.05" />
                  <Point X="-2.99713781308" Y="3.090171221397" Z="1.05" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.05" />
                  <Point X="-3.029630941282" Y="3.019172626605" Z="1.05" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.05" />
                  <Point X="-3.10490524421" Y="2.998426886092" Z="1.05" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.05" />
                  <Point X="-3.410677013004" Y="3.15761948344" Z="1.05" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.05" />
                  <Point X="-4.0362733231" Y="3.066678038441" Z="1.05" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.05" />
                  <Point X="-4.406941786576" Y="2.504973787302" Z="1.05" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.05" />
                  <Point X="-4.195399287784" Y="1.993605169601" Z="1.05" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.05" />
                  <Point X="-3.679131338843" Y="1.577349571313" Z="1.05" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.05" />
                  <Point X="-3.681268667334" Y="1.518828085019" Z="1.05" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.05" />
                  <Point X="-3.727472651956" Y="1.482848499969" Z="1.05" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.05" />
                  <Point X="-4.193104969969" Y="1.532787153379" Z="1.05" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.05" />
                  <Point X="-4.908125981603" Y="1.276715050088" Z="1.05" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.05" />
                  <Point X="-5.024113582062" Y="0.691370206739" Z="1.05" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.05" />
                  <Point X="-4.446217194859" Y="0.28209278158" Z="1.05" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.05" />
                  <Point X="-3.560294381738" Y="0.037779253506" Z="1.05" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.05" />
                  <Point X="-3.543385648272" Y="0.012336674768" Z="1.05" />
                  <Point X="-3.539556741714" Y="0" Z="1.05" />
                  <Point X="-3.544978952863" Y="-0.017470276292" Z="1.05" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.05" />
                  <Point X="-3.565074213385" Y="-0.041096729628" Z="1.05" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.05" />
                  <Point X="-4.190670593914" Y="-0.213619275176" Z="1.05" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.05" />
                  <Point X="-5.014806303469" Y="-0.76491930455" Z="1.05" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.05" />
                  <Point X="-4.90309180258" Y="-1.301184089106" Z="1.05" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.05" />
                  <Point X="-4.173202962245" Y="-1.43246556906" Z="1.05" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.05" />
                  <Point X="-3.20363707132" Y="-1.315998773721" Z="1.05" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.05" />
                  <Point X="-3.197192020251" Y="-1.339885156527" Z="1.05" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.05" />
                  <Point X="-3.739475293838" Y="-1.765858905336" Z="1.05" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.05" />
                  <Point X="-4.330849293538" Y="-2.640159500498" Z="1.05" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.05" />
                  <Point X="-4.005905276083" Y="-3.111177981089" Z="1.05" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.05" />
                  <Point X="-3.32857474528" Y="-2.991814912705" Z="1.05" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.05" />
                  <Point X="-2.562671280536" Y="-2.565659188596" Z="1.05" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.05" />
                  <Point X="-2.863602100463" Y="-3.106503637212" Z="1.05" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.05" />
                  <Point X="-3.05994128564" Y="-4.047019153641" Z="1.05" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.05" />
                  <Point X="-2.63296170545" Y="-4.336948309516" Z="1.05" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.05" />
                  <Point X="-2.358036702029" Y="-4.328236022521" Z="1.05" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.05" />
                  <Point X="-2.075024585376" Y="-4.055424962871" Z="1.05" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.05" />
                  <Point X="-1.786801411359" Y="-3.995977542553" Z="1.05" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.05" />
                  <Point X="-1.52194934863" Y="-4.124274127957" Z="1.05" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.05" />
                  <Point X="-1.389929986802" Y="-4.387290197004" Z="1.05" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.05" />
                  <Point X="-1.384836323392" Y="-4.664826616928" Z="1.05" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.05" />
                  <Point X="-1.239786725292" Y="-4.924095004833" Z="1.05" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.05" />
                  <Point X="-0.941723636421" Y="-4.989683330361" Z="1.05" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.05" />
                  <Point X="-0.651873155196" Y="-4.395007822436" Z="1.05" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.05" />
                  <Point X="-0.321123655151" Y="-3.38050851126" Z="1.05" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.05" />
                  <Point X="-0.104861733605" Y="-3.23678458079" Z="1.05" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.05" />
                  <Point X="0.148497345756" Y="-3.250327464498" Z="1.05" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.05" />
                  <Point X="0.349322204281" Y="-3.421137244508" Z="1.05" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.05" />
                  <Point X="0.582881705205" Y="-4.137528207379" Z="1.05" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.05" />
                  <Point X="0.923369127278" Y="-4.994560928364" Z="1.05" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.05" />
                  <Point X="1.102949689595" Y="-4.957998692266" Z="1.05" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.05" />
                  <Point X="1.086119280946" Y="-4.251045398899" Z="1.05" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.05" />
                  <Point X="0.988887067678" Y="-3.127799047075" Z="1.05" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.05" />
                  <Point X="1.116651033026" Y="-2.937613692199" Z="1.05" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.05" />
                  <Point X="1.327759171211" Y="-2.863103964613" Z="1.05" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.05" />
                  <Point X="1.549145124295" Y="-2.934535179033" Z="1.05" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.05" />
                  <Point X="2.061459719335" Y="-3.543950625315" Z="1.05" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.05" />
                  <Point X="2.776471927318" Y="-4.252585356044" Z="1.05" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.05" />
                  <Point X="2.968189252217" Y="-4.121059521234" Z="1.05" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.05" />
                  <Point X="2.725636971877" Y="-3.509342310239" Z="1.05" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.05" />
                  <Point X="2.248363331904" Y="-2.59564495345" Z="1.05" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.05" />
                  <Point X="2.287586642604" Y="-2.400990147495" Z="1.05" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.05" />
                  <Point X="2.431908230877" Y="-2.271314667363" Z="1.05" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.05" />
                  <Point X="2.632861850062" Y="-2.255084677587" Z="1.05" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.05" />
                  <Point X="3.278071286868" Y="-2.592112528397" Z="1.05" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.05" />
                  <Point X="4.167454773641" Y="-2.901101846319" Z="1.05" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.05" />
                  <Point X="4.33319942813" Y="-2.647159558656" Z="1.05" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.05" />
                  <Point X="3.899869475733" Y="-2.15719046505" Z="1.05" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.05" />
                  <Point X="3.133849886627" Y="-1.522988934539" Z="1.05" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.05" />
                  <Point X="3.101481467206" Y="-1.358117847166" Z="1.05" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.05" />
                  <Point X="3.172314012308" Y="-1.210012166349" Z="1.05" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.05" />
                  <Point X="3.324152955603" Y="-1.132253915008" Z="1.05" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.05" />
                  <Point X="4.023318378155" Y="-1.198074014716" Z="1.05" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.05" />
                  <Point X="4.956493325757" Y="-1.097556816546" Z="1.05" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.05" />
                  <Point X="5.02459881111" Y="-0.724476708434" Z="1.05" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.05" />
                  <Point X="4.509937700852" Y="-0.424983893216" Z="1.05" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.05" />
                  <Point X="3.693731286879" Y="-0.189469688395" Z="1.05" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.05" />
                  <Point X="3.622909867022" Y="-0.12588371437" Z="1.05" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.05" />
                  <Point X="3.589092441153" Y="-0.039985730208" Z="1.05" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.05" />
                  <Point X="3.592279009271" Y="0.05662480101" Z="1.05" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.05" />
                  <Point X="3.632469571378" Y="0.138065024205" Z="1.05" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.05" />
                  <Point X="3.709664127473" Y="0.198679155847" Z="1.05" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.05" />
                  <Point X="4.286030116455" Y="0.364988039553" Z="1.05" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.05" />
                  <Point X="5.009388885173" Y="0.817251281688" Z="1.05" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.05" />
                  <Point X="4.922723448573" Y="1.236394527596" Z="1.05" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.05" />
                  <Point X="4.294034398583" Y="1.331415886483" Z="1.05" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.05" />
                  <Point X="3.407932438133" Y="1.229317978635" Z="1.05" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.05" />
                  <Point X="3.32814241691" Y="1.257445691619" Z="1.05" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.05" />
                  <Point X="3.271151337524" Y="1.316483958927" Z="1.05" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.05" />
                  <Point X="3.24090496664" Y="1.396907001822" Z="1.05" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.05" />
                  <Point X="3.246207526867" Y="1.477459187398" Z="1.05" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.05" />
                  <Point X="3.288982936159" Y="1.553495807999" Z="1.05" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.05" />
                  <Point X="3.782415926133" Y="1.944968742421" Z="1.05" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.05" />
                  <Point X="4.324739051546" Y="2.657714219641" Z="1.05" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.05" />
                  <Point X="4.099906271954" Y="2.992922033153" Z="1.05" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.05" />
                  <Point X="3.384584880707" Y="2.772011038185" Z="1.05" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.05" />
                  <Point X="2.462821003157" Y="2.254415135207" Z="1.05" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.05" />
                  <Point X="2.38890075727" Y="2.250435743483" Z="1.05" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.05" />
                  <Point X="2.323060652863" Y="2.279078553264" Z="1.05" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.05" />
                  <Point X="2.271680046971" Y="2.333964207519" Z="1.05" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.05" />
                  <Point X="2.248993959276" Y="2.400857686563" Z="1.05" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.05" />
                  <Point X="2.25811280745" Y="2.476648572725" Z="1.05" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.05" />
                  <Point X="2.623614314674" Y="3.127554201248" Z="1.05" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.05" />
                  <Point X="2.908758312501" Y="4.158619330295" Z="1.05" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.05" />
                  <Point X="2.520379651085" Y="4.404847197549" Z="1.05" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.05" />
                  <Point X="2.114441065762" Y="4.613611469329" Z="1.05" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.05" />
                  <Point X="1.693588371974" Y="4.784143924138" Z="1.05" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.05" />
                  <Point X="1.133314049041" Y="4.940859273257" Z="1.05" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.05" />
                  <Point X="0.470264185911" Y="5.04731167915" Z="1.05" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.05" />
                  <Point X="0.113263375226" Y="4.777829166123" Z="1.05" />
                  <Point X="0" Y="4.355124473572" Z="1.05" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>