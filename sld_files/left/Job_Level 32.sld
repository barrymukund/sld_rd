<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#179" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2347" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.192046875" Y="-4.425568847656" />
                  <Point X="-24.4366953125" Y="-3.512524414063" />
                  <Point X="-24.44227734375" Y="-3.497140625" />
                  <Point X="-24.457634765625" Y="-3.467376953125" />
                  <Point X="-24.55762890625" Y="-3.323303955078" />
                  <Point X="-24.62136328125" Y="-3.2314765625" />
                  <Point X="-24.643248046875" Y="-3.209020507813" />
                  <Point X="-24.66950390625" Y="-3.189776611328" />
                  <Point X="-24.69750390625" Y="-3.175669189453" />
                  <Point X="-24.85223828125" Y="-3.127644775391" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9790234375" Y="-3.092766357422" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097036132812" />
                  <Point X="-25.19155859375" Y="-3.145060302734" />
                  <Point X="-25.29018359375" Y="-3.175669433594" />
                  <Point X="-25.31818359375" Y="-3.18977734375" />
                  <Point X="-25.344439453125" Y="-3.209021484375" />
                  <Point X="-25.36632421875" Y="-3.231477050781" />
                  <Point X="-25.466318359375" Y="-3.375549804688" />
                  <Point X="-25.53005078125" Y="-3.467377441406" />
                  <Point X="-25.5381875" Y="-3.481571777344" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.706921875" Y="-4.094469726562" />
                  <Point X="-25.9165859375" Y="-4.876941894531" />
                  <Point X="-25.96694140625" Y="-4.867167480469" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.24641796875" Y="-4.802362304688" />
                  <Point X="-26.244412109375" Y="-4.787132324219" />
                  <Point X="-26.2149609375" Y="-4.5634375" />
                  <Point X="-26.21419921875" Y="-4.547929199219" />
                  <Point X="-26.2165078125" Y="-4.516223632812" />
                  <Point X="-26.253474609375" Y="-4.330381835937" />
                  <Point X="-26.277037109375" Y="-4.211932128906" />
                  <Point X="-26.287939453125" Y="-4.18296484375" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.466103515625" Y="-4.006269287109" />
                  <Point X="-26.556904296875" Y="-3.926639648438" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.83210546875" Y="-3.878573486328" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708740234" />
                  <Point X="-27.014466796875" Y="-3.882028564453" />
                  <Point X="-27.042658203125" Y="-3.894801757812" />
                  <Point X="-27.20020703125" Y="-4.000072998047" />
                  <Point X="-27.300623046875" Y="-4.067169677734" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.335099609375" Y="-4.099461425781" />
                  <Point X="-27.42264453125" Y="-4.213550292969" />
                  <Point X="-27.480146484375" Y="-4.288489746094" />
                  <Point X="-27.636958984375" Y="-4.191395996094" />
                  <Point X="-27.80171484375" Y="-4.0893828125" />
                  <Point X="-28.045923828125" Y="-3.901349853516" />
                  <Point X="-28.104720703125" Y="-3.856078125" />
                  <Point X="-27.8784140625" Y="-3.464102294922" />
                  <Point X="-27.423759765625" Y="-2.676619384766" />
                  <Point X="-27.412859375" Y="-2.647654541016" />
                  <Point X="-27.406587890625" Y="-2.616127929688" />
                  <Point X="-27.40557421875" Y="-2.585193847656" />
                  <Point X="-27.41455859375" Y="-2.555575927734" />
                  <Point X="-27.428775390625" Y="-2.526746826172" />
                  <Point X="-27.44680078125" Y="-2.501591064453" />
                  <Point X="-27.46414453125" Y="-2.48424609375" />
                  <Point X="-27.489298828125" Y="-2.46621875" />
                  <Point X="-27.51812890625" Y="-2.451999023438" />
                  <Point X="-27.547748046875" Y="-2.443012451172" />
                  <Point X="-27.57868359375" Y="-2.444023925781" />
                  <Point X="-27.610212890625" Y="-2.450294433594" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.141099609375" Y="-2.750979003906" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-28.95269921875" Y="-2.964865234375" />
                  <Point X="-29.082859375" Y="-2.793861816406" />
                  <Point X="-29.25794140625" Y="-2.500275390625" />
                  <Point X="-29.306142578125" Y="-2.419449951172" />
                  <Point X="-28.901451171875" Y="-2.108919189453" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.084580078125" Y="-1.475598388672" />
                  <Point X="-28.066615234375" Y="-1.448470947266" />
                  <Point X="-28.053857421875" Y="-1.419837646484" />
                  <Point X="-28.046150390625" Y="-1.390082275391" />
                  <Point X="-28.043345703125" Y="-1.359645751953" />
                  <Point X="-28.045556640625" Y="-1.327977416992" />
                  <Point X="-28.05255859375" Y="-1.298234619141" />
                  <Point X="-28.068642578125" Y="-1.272253173828" />
                  <Point X="-28.089474609375" Y="-1.248298828125" />
                  <Point X="-28.112970703125" Y="-1.228767211914" />
                  <Point X="-28.139453125" Y="-1.213180664062" />
                  <Point X="-28.168716796875" Y="-1.201956665039" />
                  <Point X="-28.200603515625" Y="-1.195474975586" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-28.865548828125" Y="-1.277801757812" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.78316796875" Y="-1.191955200195" />
                  <Point X="-29.834078125" Y="-0.99265045166" />
                  <Point X="-29.8803984375" Y="-0.668774841309" />
                  <Point X="-29.892423828125" Y="-0.584698425293" />
                  <Point X="-29.439064453125" Y="-0.463221252441" />
                  <Point X="-28.532875" Y="-0.220408279419" />
                  <Point X="-28.5174921875" Y="-0.214827850342" />
                  <Point X="-28.48773046875" Y="-0.199470458984" />
                  <Point X="-28.47078125" Y="-0.18770640564" />
                  <Point X="-28.4599765625" Y="-0.1802084198" />
                  <Point X="-28.437525390625" Y="-0.15832875061" />
                  <Point X="-28.418279296875" Y="-0.132072967529" />
                  <Point X="-28.404166015625" Y="-0.104065216064" />
                  <Point X="-28.3949140625" Y="-0.074251617432" />
                  <Point X="-28.390646484375" Y="-0.046089038849" />
                  <Point X="-28.3906484375" Y="-0.016451818466" />
                  <Point X="-28.39491796875" Y="0.011698917389" />
                  <Point X="-28.404166015625" Y="0.041499160767" />
                  <Point X="-28.41826953125" Y="0.069496589661" />
                  <Point X="-28.437513671875" Y="0.095756622314" />
                  <Point X="-28.4599765625" Y="0.117648887634" />
                  <Point X="-28.476927734375" Y="0.129412948608" />
                  <Point X="-28.484791015625" Y="0.134320022583" />
                  <Point X="-28.511162109375" Y="0.149038833618" />
                  <Point X="-28.532875" Y="0.157848297119" />
                  <Point X="-29.110451171875" Y="0.312609344482" />
                  <Point X="-29.891814453125" Y="0.521975402832" />
                  <Point X="-29.857294921875" Y="0.755261169434" />
                  <Point X="-29.82448828125" Y="0.976968811035" />
                  <Point X="-29.7312421875" Y="1.321078735352" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-29.415244140625" Y="1.385311523438" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228149414" />
                  <Point X="-28.703134765625" Y="1.305263671875" />
                  <Point X="-28.66562109375" Y="1.317092041016" />
                  <Point X="-28.641708984375" Y="1.324631225586" />
                  <Point X="-28.622775390625" Y="1.332962402344" />
                  <Point X="-28.60403125" Y="1.343784545898" />
                  <Point X="-28.587353515625" Y="1.356014160156" />
                  <Point X="-28.573716796875" Y="1.371563598633" />
                  <Point X="-28.56130078125" Y="1.389294067383" />
                  <Point X="-28.55134765625" Y="1.407432617188" />
                  <Point X="-28.536294921875" Y="1.443774291992" />
                  <Point X="-28.526701171875" Y="1.466937011719" />
                  <Point X="-28.520912109375" Y="1.486806152344" />
                  <Point X="-28.51715625" Y="1.508122192383" />
                  <Point X="-28.515806640625" Y="1.528755981445" />
                  <Point X="-28.518951171875" Y="1.549193115234" />
                  <Point X="-28.524552734375" Y="1.570100219727" />
                  <Point X="-28.532048828125" Y="1.589378051758" />
                  <Point X="-28.5502109375" Y="1.624269287109" />
                  <Point X="-28.5617890625" Y="1.6465078125" />
                  <Point X="-28.57328515625" Y="1.663712768555" />
                  <Point X="-28.587197265625" Y="1.680290649414" />
                  <Point X="-28.60213671875" Y="1.694590332031" />
                  <Point X="-28.933435546875" Y="1.948805175781" />
                  <Point X="-29.351859375" Y="2.269873291016" />
                  <Point X="-29.208630859375" Y="2.515257568359" />
                  <Point X="-29.081146484375" Y="2.733665771484" />
                  <Point X="-28.83415234375" Y="3.051145996094" />
                  <Point X="-28.75050390625" Y="3.158662353516" />
                  <Point X="-28.605609375" Y="3.075006835938" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.094544921875" Y="2.821225341797" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.019107421875" Y="2.821587890625" />
                  <Point X="-27.999015625" Y="2.826504150391" />
                  <Point X="-27.98046484375" Y="2.83565234375" />
                  <Point X="-27.9622109375" Y="2.847281005859" />
                  <Point X="-27.946078125" Y="2.860228759766" />
                  <Point X="-27.9089921875" Y="2.897314697266" />
                  <Point X="-27.885353515625" Y="2.920952148438" />
                  <Point X="-27.87241015625" Y="2.937077392578" />
                  <Point X="-27.860779296875" Y="2.955331054688" />
                  <Point X="-27.85162890625" Y="2.973883056641" />
                  <Point X="-27.8467109375" Y="2.993976074219" />
                  <Point X="-27.843884765625" Y="3.015434326172" />
                  <Point X="-27.84343359375" Y="3.036120849609" />
                  <Point X="-27.848005859375" Y="3.088369140625" />
                  <Point X="-27.85091796875" Y="3.121670410156" />
                  <Point X="-27.854955078125" Y="3.141958251953" />
                  <Point X="-27.86146484375" Y="3.162602539062" />
                  <Point X="-27.869794921875" Y="3.181532958984" />
                  <Point X="-28.016603515625" Y="3.435812988281" />
                  <Point X="-28.18333203125" Y="3.724595703125" />
                  <Point X="-27.922646484375" Y="3.924460449219" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.311607421875" Y="4.310813964844" />
                  <Point X="-27.16703515625" Y="4.391134277344" />
                  <Point X="-27.165326171875" Y="4.38890625" />
                  <Point X="-27.0431953125" Y="4.229741210938" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.995111328125" Y="4.18939453125" />
                  <Point X="-26.936958984375" Y="4.159122558594" />
                  <Point X="-26.89989453125" Y="4.139827636719" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.777451171875" Y="4.134482421875" />
                  <Point X="-26.7168828125" Y="4.159571289062" />
                  <Point X="-26.67827734375" Y="4.175562011719" />
                  <Point X="-26.660142578125" Y="4.185510253906" />
                  <Point X="-26.6424140625" Y="4.197923828125" />
                  <Point X="-26.62686328125" Y="4.211561523438" />
                  <Point X="-26.6146328125" Y="4.228241699219" />
                  <Point X="-26.603810546875" Y="4.246985351563" />
                  <Point X="-26.595478515625" Y="4.265921875" />
                  <Point X="-26.575765625" Y="4.328447265625" />
                  <Point X="-26.56319921875" Y="4.368298339844" />
                  <Point X="-26.5591640625" Y="4.388583007812" />
                  <Point X="-26.55727734375" Y="4.410145507812" />
                  <Point X="-26.557728515625" Y="4.430826660156" />
                  <Point X="-26.574419921875" Y="4.557604003906" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.237056640625" Y="4.729225585938" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.47803125" Y="4.865002441406" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.25192578125" Y="4.726778320312" />
                  <Point X="-25.133904296875" Y="4.286315917969" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286314941406" />
                  <Point X="-24.77855859375" Y="4.567051757812" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.40692578125" Y="4.858021972656" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.765783203125" Y="4.737538574219" />
                  <Point X="-23.5189765625" Y="4.677952148438" />
                  <Point X="-23.265583984375" Y="4.586044433594" />
                  <Point X="-23.105349609375" Y="4.527926757812" />
                  <Point X="-22.85977734375" Y="4.413081054688" />
                  <Point X="-22.705423828125" Y="4.340894042969" />
                  <Point X="-22.468162109375" Y="4.202665039063" />
                  <Point X="-22.31902734375" Y="4.115777832031" />
                  <Point X="-22.0952734375" Y="3.956658691406" />
                  <Point X="-22.05673828125" Y="3.929254150391" />
                  <Point X="-22.326189453125" Y="3.462550292969" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857921875" Y="2.53993359375" />
                  <Point X="-22.866921875" Y="2.516058349609" />
                  <Point X="-22.88003515625" Y="2.467024658203" />
                  <Point X="-22.888392578125" Y="2.435771972656" />
                  <Point X="-22.891380859375" Y="2.417936035156" />
                  <Point X="-22.892271484375" Y="2.380953125" />
                  <Point X="-22.887158203125" Y="2.338552978516" />
                  <Point X="-22.883900390625" Y="2.311528320312" />
                  <Point X="-22.878556640625" Y="2.289604492188" />
                  <Point X="-22.8702890625" Y="2.267513427734" />
                  <Point X="-22.859927734375" Y="2.247469482422" />
                  <Point X="-22.83369140625" Y="2.2088046875" />
                  <Point X="-22.81696875" Y="2.184160888672" />
                  <Point X="-22.80553515625" Y="2.170329589844" />
                  <Point X="-22.778400390625" Y="2.145592773438" />
                  <Point X="-22.739736328125" Y="2.119356933594" />
                  <Point X="-22.715091796875" Y="2.102635253906" />
                  <Point X="-22.695044921875" Y="2.092271728516" />
                  <Point X="-22.67295703125" Y="2.084006103516" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.608634765625" Y="2.07355078125" />
                  <Point X="-22.581611328125" Y="2.070291992188" />
                  <Point X="-22.563533203125" Y="2.069845458984" />
                  <Point X="-22.52679296875" Y="2.074170898438" />
                  <Point X="-22.4777578125" Y="2.087283203125" />
                  <Point X="-22.446505859375" Y="2.095640625" />
                  <Point X="-22.4347109375" Y="2.099639648438" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.83053515625" Y="2.445545410156" />
                  <Point X="-21.032673828125" Y="2.906190185547" />
                  <Point X="-20.9651953125" Y="2.812409912109" />
                  <Point X="-20.87671875" Y="2.689450927734" />
                  <Point X="-20.7519921875" Y="2.483335693359" />
                  <Point X="-20.73780078125" Y="2.459883544922" />
                  <Point X="-21.07638671875" Y="2.200076416016" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.778572265625" Y="1.660243896484" />
                  <Point X="-21.79602734375" Y="1.641626831055" />
                  <Point X="-21.83131640625" Y="1.595588745117" />
                  <Point X="-21.85380859375" Y="1.566245483398" />
                  <Point X="-21.86339453125" Y="1.550909912109" />
                  <Point X="-21.8783671875" Y="1.517088500977" />
                  <Point X="-21.891513671875" Y="1.470083496094" />
                  <Point X="-21.899892578125" Y="1.440123901367" />
                  <Point X="-21.90334765625" Y="1.41782434082" />
                  <Point X="-21.9041640625" Y="1.394252197266" />
                  <Point X="-21.90226171875" Y="1.371766845703" />
                  <Point X="-21.891470703125" Y="1.319467529297" />
                  <Point X="-21.884591796875" Y="1.286133911133" />
                  <Point X="-21.8793203125" Y="1.26897668457" />
                  <Point X="-21.86371875" Y="1.235741577148" />
                  <Point X="-21.834369140625" Y="1.191130249023" />
                  <Point X="-21.815662109375" Y="1.162696166992" />
                  <Point X="-21.801107421875" Y="1.145450195312" />
                  <Point X="-21.78386328125" Y="1.129360107422" />
                  <Point X="-21.765654296875" Y="1.116034545898" />
                  <Point X="-21.72312109375" Y="1.092092163086" />
                  <Point X="-21.69601171875" Y="1.07683215332" />
                  <Point X="-21.6794765625" Y="1.069500366211" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.586373046875" Y="1.051838134766" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.959951171875" Y="1.119636474609" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.192060546875" Y="1.088883789062" />
                  <Point X="-20.15405859375" Y="0.932785400391" />
                  <Point X="-20.11475390625" Y="0.680341796875" />
                  <Point X="-20.1091328125" Y="0.644239013672" />
                  <Point X="-20.489423828125" Y="0.542340270996" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.295208984375" Y="0.325586517334" />
                  <Point X="-21.318453125" Y="0.315067901611" />
                  <Point X="-21.374951171875" Y="0.282410339355" />
                  <Point X="-21.410962890625" Y="0.261595336914" />
                  <Point X="-21.425689453125" Y="0.251094924927" />
                  <Point X="-21.452466796875" Y="0.225576690674" />
                  <Point X="-21.4863671875" Y="0.18238079834" />
                  <Point X="-21.50797265625" Y="0.154849060059" />
                  <Point X="-21.51969921875" Y="0.135566070557" />
                  <Point X="-21.52947265625" Y="0.114101852417" />
                  <Point X="-21.536318359375" Y="0.09260408783" />
                  <Point X="-21.547619140625" Y="0.033600597382" />
                  <Point X="-21.5548203125" Y="-0.00400637579" />
                  <Point X="-21.556515625" Y="-0.021876653671" />
                  <Point X="-21.5548203125" Y="-0.058553150177" />
                  <Point X="-21.543521484375" Y="-0.117556793213" />
                  <Point X="-21.536318359375" Y="-0.155163619995" />
                  <Point X="-21.529470703125" Y="-0.176668060303" />
                  <Point X="-21.519697265625" Y="-0.198129852295" />
                  <Point X="-21.50797265625" Y="-0.217409347534" />
                  <Point X="-21.474072265625" Y="-0.260605102539" />
                  <Point X="-21.452466796875" Y="-0.288136993408" />
                  <Point X="-21.44" Y="-0.301234832764" />
                  <Point X="-21.410962890625" Y="-0.324155609131" />
                  <Point X="-21.354462890625" Y="-0.356813171387" />
                  <Point X="-21.318453125" Y="-0.37762802124" />
                  <Point X="-21.30729296875" Y="-0.383137237549" />
                  <Point X="-21.283419921875" Y="-0.392149963379" />
                  <Point X="-20.7773515625" Y="-0.527750366211" />
                  <Point X="-20.108525390625" Y="-0.706961975098" />
                  <Point X="-20.123759765625" Y="-0.808010925293" />
                  <Point X="-20.144974609375" Y="-0.948723937988" />
                  <Point X="-20.1953359375" Y="-1.169409790039" />
                  <Point X="-20.19882421875" Y="-1.18469921875" />
                  <Point X="-20.654806640625" Y="-1.12466809082" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710266113" />
                  <Point X="-21.62533984375" Y="-1.005508666992" />
                  <Point X="-21.736228515625" Y="-1.029610839844" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.8360234375" Y="-1.056596313477" />
                  <Point X="-21.8638515625" Y="-1.073488891602" />
                  <Point X="-21.8876015625" Y="-1.093960083008" />
                  <Point X="-21.954626953125" Y="-1.174569702148" />
                  <Point X="-21.997345703125" Y="-1.225947998047" />
                  <Point X="-22.012068359375" Y="-1.250334106445" />
                  <Point X="-22.02341015625" Y="-1.27771875" />
                  <Point X="-22.030240234375" Y="-1.305365844727" />
                  <Point X="-22.039845703125" Y="-1.409759399414" />
                  <Point X="-22.04596875" Y="-1.476296142578" />
                  <Point X="-22.04365234375" Y="-1.507560913086" />
                  <Point X="-22.035921875" Y="-1.539182128906" />
                  <Point X="-22.023548828125" Y="-1.567996826172" />
                  <Point X="-21.962181640625" Y="-1.66344909668" />
                  <Point X="-21.923068359375" Y="-1.724287353516" />
                  <Point X="-21.9130625" Y="-1.737241943359" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.419736328125" Y="-2.121271484375" />
                  <Point X="-20.786876953125" Y="-2.606882080078" />
                  <Point X="-20.81538671875" Y="-2.653014892578" />
                  <Point X="-20.875197265625" Y="-2.749797607422" />
                  <Point X="-20.971017578125" Y="-2.8859453125" />
                  <Point X="-21.3789375" Y="-2.650432128906" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.245775390625" Y="-2.159825195312" />
                  <Point X="-22.37775" Y="-2.135990722656" />
                  <Point X="-22.461865234375" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395507812" />
                  <Point X="-22.525390625" Y="-2.125353515625" />
                  <Point X="-22.555166015625" Y="-2.135177001953" />
                  <Point X="-22.6648046875" Y="-2.192878662109" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.757615234375" Y="-2.246549804688" />
                  <Point X="-22.77857421875" Y="-2.267509277344" />
                  <Point X="-22.795466796875" Y="-2.290438476562" />
                  <Point X="-22.853169921875" Y="-2.400076660156" />
                  <Point X="-22.889947265625" Y="-2.469956542969" />
                  <Point X="-22.899771484375" Y="-2.499735351563" />
                  <Point X="-22.904728515625" Y="-2.531908203125" />
                  <Point X="-22.90432421875" Y="-2.563259277344" />
                  <Point X="-22.88048828125" Y="-2.695233398438" />
                  <Point X="-22.865296875" Y="-2.779349609375" />
                  <Point X="-22.86101171875" Y="-2.795139648438" />
                  <Point X="-22.8481796875" Y="-2.826078369141" />
                  <Point X="-22.54639453125" Y="-3.348788085938" />
                  <Point X="-22.13871484375" Y="-4.054905517578" />
                  <Point X="-22.147185546875" Y="-4.060955322266" />
                  <Point X="-22.218166015625" Y="-4.111653320313" />
                  <Point X="-22.298234375" Y="-4.163480957031" />
                  <Point X="-22.615634765625" Y="-3.749835449219" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922177490234" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.40823828125" Y="-2.816874755859" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.520009765625" Y="-2.751167236328" />
                  <Point X="-23.551630859375" Y="-2.743435546875" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.72525390625" Y="-2.754216308594" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.843634765625" Y="-2.769396972656" />
                  <Point X="-23.87101953125" Y="-2.780739746094" />
                  <Point X="-23.89540234375" Y="-2.7954609375" />
                  <Point X="-24.00532421875" Y="-2.886857910156" />
                  <Point X="-24.07538671875" Y="-2.945111328125" />
                  <Point X="-24.095857421875" Y="-2.968860595703" />
                  <Point X="-24.11275" Y="-2.996687011719" />
                  <Point X="-24.124375" Y="-3.02580859375" />
                  <Point X="-24.157240234375" Y="-3.17701953125" />
                  <Point X="-24.178189453125" Y="-3.273396484375" />
                  <Point X="-24.180275390625" Y="-3.289627197266" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.094732421875" Y="-3.972739501953" />
                  <Point X="-23.977935546875" Y="-4.859915527344" />
                  <Point X="-24.024318359375" Y="-4.870083007812" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.05842578125" Y="-4.75263671875" />
                  <Point X="-26.141244140625" Y="-4.731328125" />
                  <Point X="-26.1207734375" Y="-4.575842285156" />
                  <Point X="-26.120076171875" Y="-4.568098144531" />
                  <Point X="-26.11944921875" Y="-4.541030273438" />
                  <Point X="-26.1217578125" Y="-4.509324707031" />
                  <Point X="-26.123333984375" Y="-4.497689941406" />
                  <Point X="-26.16030078125" Y="-4.311848144531" />
                  <Point X="-26.18386328125" Y="-4.1933984375" />
                  <Point X="-26.188125" Y="-4.17846875" />
                  <Point X="-26.19902734375" Y="-4.149501464844" />
                  <Point X="-26.20566796875" Y="-4.135463867188" />
                  <Point X="-26.221740234375" Y="-4.107626464844" />
                  <Point X="-26.23057421875" Y="-4.094861083984" />
                  <Point X="-26.25020703125" Y="-4.070937744141" />
                  <Point X="-26.261005859375" Y="-4.059779785156" />
                  <Point X="-26.40346484375" Y="-3.934844970703" />
                  <Point X="-26.494265625" Y="-3.855215332031" />
                  <Point X="-26.506736328125" Y="-3.845965820312" />
                  <Point X="-26.53301953125" Y="-3.829621582031" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.825892578125" Y="-3.783776855469" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166259766" />
                  <Point X="-27.008005859375" Y="-3.781946044922" />
                  <Point X="-27.0390546875" Y="-3.790265869141" />
                  <Point X="-27.053673828125" Y="-3.795496337891" />
                  <Point X="-27.081865234375" Y="-3.80826953125" />
                  <Point X="-27.0954375" Y="-3.815812255859" />
                  <Point X="-27.252986328125" Y="-3.921083496094" />
                  <Point X="-27.35340234375" Y="-3.988180175781" />
                  <Point X="-27.35968359375" Y="-3.992759765625" />
                  <Point X="-27.380443359375" Y="-4.010133789063" />
                  <Point X="-27.4027578125" Y="-4.032772216797" />
                  <Point X="-27.410466796875" Y="-4.041628417969" />
                  <Point X="-27.49801171875" Y="-4.155717285156" />
                  <Point X="-27.503201171875" Y="-4.162479003906" />
                  <Point X="-27.586947265625" Y="-4.110625488281" />
                  <Point X="-27.747595703125" Y="-4.011155517578" />
                  <Point X="-27.980861328125" Y="-3.831548583984" />
                  <Point X="-27.796140625" Y="-3.511602294922" />
                  <Point X="-27.341486328125" Y="-2.724119384766" />
                  <Point X="-27.33484765625" Y="-2.710079833984" />
                  <Point X="-27.323947265625" Y="-2.681114990234" />
                  <Point X="-27.319685546875" Y="-2.666189453125" />
                  <Point X="-27.3134140625" Y="-2.634662841797" />
                  <Point X="-27.311638671875" Y="-2.619239257812" />
                  <Point X="-27.310625" Y="-2.588305175781" />
                  <Point X="-27.3146640625" Y="-2.5576171875" />
                  <Point X="-27.3236484375" Y="-2.527999267578" />
                  <Point X="-27.32935546875" Y="-2.513558837891" />
                  <Point X="-27.343572265625" Y="-2.484729736328" />
                  <Point X="-27.351552734375" Y="-2.471413330078" />
                  <Point X="-27.369578125" Y="-2.446257568359" />
                  <Point X="-27.379623046875" Y="-2.434418212891" />
                  <Point X="-27.396966796875" Y="-2.417073242188" />
                  <Point X="-27.4088046875" Y="-2.407028564453" />
                  <Point X="-27.433958984375" Y="-2.389001220703" />
                  <Point X="-27.447275390625" Y="-2.381018554688" />
                  <Point X="-27.47610546875" Y="-2.366798828125" />
                  <Point X="-27.490546875" Y="-2.361091064453" />
                  <Point X="-27.520166015625" Y="-2.352104492188" />
                  <Point X="-27.5508515625" Y="-2.348063232422" />
                  <Point X="-27.581787109375" Y="-2.349074707031" />
                  <Point X="-27.59721484375" Y="-2.350848632812" />
                  <Point X="-27.628744140625" Y="-2.357119140625" />
                  <Point X="-27.64367578125" Y="-2.361383056641" />
                  <Point X="-27.67264453125" Y="-2.372285888672" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.188599609375" Y="-2.668706542969" />
                  <Point X="-28.793087890625" Y="-3.017708251953" />
                  <Point X="-28.87710546875" Y="-2.907326904297" />
                  <Point X="-29.004021484375" Y="-2.7405859375" />
                  <Point X="-29.176349609375" Y="-2.4516171875" />
                  <Point X="-29.181265625" Y="-2.443373535156" />
                  <Point X="-28.843619140625" Y="-2.184287841797" />
                  <Point X="-28.048123046875" Y="-1.573881958008" />
                  <Point X="-28.036486328125" Y="-1.563313598633" />
                  <Point X="-28.015111328125" Y="-1.54039855957" />
                  <Point X="-28.005373046875" Y="-1.528051879883" />
                  <Point X="-27.987408203125" Y="-1.500924438477" />
                  <Point X="-27.97983984375" Y="-1.487134765625" />
                  <Point X="-27.96708203125" Y="-1.458501464844" />
                  <Point X="-27.961892578125" Y="-1.443657836914" />
                  <Point X="-27.954185546875" Y="-1.41390246582" />
                  <Point X="-27.95155078125" Y="-1.398799438477" />
                  <Point X="-27.94874609375" Y="-1.368362915039" />
                  <Point X="-27.948576171875" Y="-1.353029418945" />
                  <Point X="-27.950787109375" Y="-1.321361083984" />
                  <Point X="-27.953083984375" Y="-1.306207763672" />
                  <Point X="-27.9600859375" Y="-1.276465087891" />
                  <Point X="-27.971783203125" Y="-1.24823046875" />
                  <Point X="-27.9878671875" Y="-1.222248901367" />
                  <Point X="-27.996958984375" Y="-1.209912475586" />
                  <Point X="-28.017791015625" Y="-1.185958129883" />
                  <Point X="-28.02874609375" Y="-1.175243896484" />
                  <Point X="-28.0522421875" Y="-1.155712158203" />
                  <Point X="-28.064783203125" Y="-1.146895263672" />
                  <Point X="-28.091265625" Y="-1.13130859375" />
                  <Point X="-28.10543359375" Y="-1.124481079102" />
                  <Point X="-28.134697265625" Y="-1.113257080078" />
                  <Point X="-28.14979296875" Y="-1.108860473633" />
                  <Point X="-28.1816796875" Y="-1.10237890625" />
                  <Point X="-28.197296875" Y="-1.100532592773" />
                  <Point X="-28.22862109375" Y="-1.09944152832" />
                  <Point X="-28.244328125" Y="-1.100196655273" />
                  <Point X="-28.87794921875" Y="-1.183614379883" />
                  <Point X="-29.66091796875" Y="-1.286694213867" />
                  <Point X="-29.691123046875" Y="-1.168444091797" />
                  <Point X="-29.740763671875" Y="-0.974113342285" />
                  <Point X="-29.78635546875" Y="-0.655324829102" />
                  <Point X="-29.786451171875" Y="-0.654654418945" />
                  <Point X="-29.4144765625" Y="-0.554984130859" />
                  <Point X="-28.508287109375" Y="-0.312171173096" />
                  <Point X="-28.500478515625" Y="-0.309713409424" />
                  <Point X="-28.4739296875" Y="-0.299250793457" />
                  <Point X="-28.44416796875" Y="-0.283893463135" />
                  <Point X="-28.4335625" Y="-0.277514099121" />
                  <Point X="-28.41661328125" Y="-0.26575" />
                  <Point X="-28.393673828125" Y="-0.248243865967" />
                  <Point X="-28.37122265625" Y="-0.226364196777" />
                  <Point X="-28.36090625" Y="-0.214492797852" />
                  <Point X="-28.34166015625" Y="-0.188237075806" />
                  <Point X="-28.33344140625" Y="-0.174823120117" />
                  <Point X="-28.319328125" Y="-0.146815429688" />
                  <Point X="-28.31343359375" Y="-0.132221694946" />
                  <Point X="-28.304181640625" Y="-0.102408096313" />
                  <Point X="-28.300986328125" Y="-0.088484802246" />
                  <Point X="-28.29671875" Y="-0.060322250366" />
                  <Point X="-28.295646484375" Y="-0.046082839966" />
                  <Point X="-28.2956484375" Y="-0.016445501328" />
                  <Point X="-28.29672265625" Y="-0.002206386566" />
                  <Point X="-28.3009921875" Y="0.02594427681" />
                  <Point X="-28.3041875" Y="0.039855976105" />
                  <Point X="-28.313435546875" Y="0.069656204224" />
                  <Point X="-28.319322265625" Y="0.084238342285" />
                  <Point X="-28.33342578125" Y="0.112235771179" />
                  <Point X="-28.341642578125" Y="0.125651069641" />
                  <Point X="-28.36088671875" Y="0.151911102295" />
                  <Point X="-28.371208984375" Y="0.163790390015" />
                  <Point X="-28.393671875" Y="0.185682540894" />
                  <Point X="-28.4058125" Y="0.195695404053" />
                  <Point X="-28.422763671875" Y="0.207459503174" />
                  <Point X="-28.438490234375" Y="0.217273666382" />
                  <Point X="-28.464861328125" Y="0.231992538452" />
                  <Point X="-28.4754453125" Y="0.237069198608" />
                  <Point X="-28.497158203125" Y="0.245878677368" />
                  <Point X="-28.508287109375" Y="0.249611190796" />
                  <Point X="-29.08586328125" Y="0.4043722229" />
                  <Point X="-29.7854453125" Y="0.591824829102" />
                  <Point X="-29.763318359375" Y="0.74135534668" />
                  <Point X="-29.73133203125" Y="0.957523010254" />
                  <Point X="-29.639548828125" Y="1.296231933594" />
                  <Point X="-29.6335859375" Y="1.318237060547" />
                  <Point X="-29.42764453125" Y="1.291124267578" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.204703369141" />
                  <Point X="-28.71514453125" Y="1.20658972168" />
                  <Point X="-28.704890625" Y="1.208053344727" />
                  <Point X="-28.6846015625" Y="1.212088989258" />
                  <Point X="-28.67456640625" Y="1.214660766602" />
                  <Point X="-28.637052734375" Y="1.226489135742" />
                  <Point X="-28.613140625" Y="1.234028320312" />
                  <Point X="-28.603447265625" Y="1.237677001953" />
                  <Point X="-28.584513671875" Y="1.246008300781" />
                  <Point X="-28.575275390625" Y="1.250690429688" />
                  <Point X="-28.55653125" Y="1.261512573242" />
                  <Point X="-28.547853515625" Y="1.267174438477" />
                  <Point X="-28.53117578125" Y="1.279404174805" />
                  <Point X="-28.5159296875" Y="1.293375732422" />
                  <Point X="-28.50229296875" Y="1.308925170898" />
                  <Point X="-28.495900390625" Y="1.317070922852" />
                  <Point X="-28.483484375" Y="1.334801391602" />
                  <Point X="-28.478015625" Y="1.343593139648" />
                  <Point X="-28.4680625" Y="1.361731689453" />
                  <Point X="-28.463578125" Y="1.371078735352" />
                  <Point X="-28.448525390625" Y="1.407420410156" />
                  <Point X="-28.438931640625" Y="1.430583129883" />
                  <Point X="-28.435494140625" Y="1.440362915039" />
                  <Point X="-28.429705078125" Y="1.460231933594" />
                  <Point X="-28.427353515625" Y="1.470321289062" />
                  <Point X="-28.42359765625" Y="1.491637207031" />
                  <Point X="-28.422359375" Y="1.50192175293" />
                  <Point X="-28.421009765625" Y="1.522555541992" />
                  <Point X="-28.421912109375" Y="1.543203125" />
                  <Point X="-28.425056640625" Y="1.563640258789" />
                  <Point X="-28.4271875" Y="1.573778930664" />
                  <Point X="-28.4327890625" Y="1.594686035156" />
                  <Point X="-28.43601171875" Y="1.604529296875" />
                  <Point X="-28.4435078125" Y="1.623807128906" />
                  <Point X="-28.44778125" Y="1.63324206543" />
                  <Point X="-28.465943359375" Y="1.668133300781" />
                  <Point X="-28.477521484375" Y="1.690371826172" />
                  <Point X="-28.482798828125" Y="1.699287231445" />
                  <Point X="-28.494294921875" Y="1.71649230957" />
                  <Point X="-28.500513671875" Y="1.724781738281" />
                  <Point X="-28.51442578125" Y="1.741359619141" />
                  <Point X="-28.5215078125" Y="1.748919189453" />
                  <Point X="-28.536447265625" Y="1.76321887207" />
                  <Point X="-28.5443046875" Y="1.769958862305" />
                  <Point X="-28.875603515625" Y="2.024173706055" />
                  <Point X="-29.22761328125" Y="2.294280761719" />
                  <Point X="-29.126583984375" Y="2.467367919922" />
                  <Point X="-29.00228125" Y="2.680324951172" />
                  <Point X="-28.759171875" Y="2.992812011719" />
                  <Point X="-28.726337890625" Y="3.035013916016" />
                  <Point X="-28.653109375" Y="2.992734619141" />
                  <Point X="-28.25415625" Y="2.762398681641" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.10282421875" Y="2.726586914062" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.00670703125" Y="2.727400634766" />
                  <Point X="-27.99652734375" Y="2.729310302734" />
                  <Point X="-27.976435546875" Y="2.7342265625" />
                  <Point X="-27.956998046875" Y="2.741301269531" />
                  <Point X="-27.938447265625" Y="2.750449462891" />
                  <Point X="-27.929421875" Y="2.755529541016" />
                  <Point X="-27.91116796875" Y="2.767158203125" />
                  <Point X="-27.902748046875" Y="2.773191650391" />
                  <Point X="-27.886615234375" Y="2.786139404297" />
                  <Point X="-27.87890234375" Y="2.793053710938" />
                  <Point X="-27.84181640625" Y="2.830139648438" />
                  <Point X="-27.818177734375" Y="2.853777099609" />
                  <Point X="-27.811267578125" Y="2.861485351562" />
                  <Point X="-27.79832421875" Y="2.877610595703" />
                  <Point X="-27.792291015625" Y="2.886027587891" />
                  <Point X="-27.78066015625" Y="2.90428125" />
                  <Point X="-27.775580078125" Y="2.913307861328" />
                  <Point X="-27.7664296875" Y="2.931859863281" />
                  <Point X="-27.759353515625" Y="2.951297607422" />
                  <Point X="-27.754435546875" Y="2.971390625" />
                  <Point X="-27.7525234375" Y="2.981571289062" />
                  <Point X="-27.749697265625" Y="3.003029541016" />
                  <Point X="-27.748908203125" Y="3.013362792969" />
                  <Point X="-27.74845703125" Y="3.034049316406" />
                  <Point X="-27.748794921875" Y="3.044402587891" />
                  <Point X="-27.7533671875" Y="3.096650878906" />
                  <Point X="-27.756279296875" Y="3.129952148438" />
                  <Point X="-27.757744140625" Y="3.140211181641" />
                  <Point X="-27.76178125" Y="3.160499023438" />
                  <Point X="-27.764353515625" Y="3.170527832031" />
                  <Point X="-27.77086328125" Y="3.191172119141" />
                  <Point X="-27.77451171875" Y="3.200865478516" />
                  <Point X="-27.782841796875" Y="3.219795898438" />
                  <Point X="-27.7875234375" Y="3.229032958984" />
                  <Point X="-27.93433203125" Y="3.483312988281" />
                  <Point X="-28.05938671875" Y="3.699914550781" />
                  <Point X="-27.86484375" Y="3.849068603516" />
                  <Point X="-27.648365234375" Y="4.015041992188" />
                  <Point X="-27.265470703125" Y="4.227770019531" />
                  <Point X="-27.192525390625" Y="4.268296875" />
                  <Point X="-27.118564453125" Y="4.171909179688" />
                  <Point X="-27.1118203125" Y="4.164048828125" />
                  <Point X="-27.097517578125" Y="4.149107421875" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.06508984375" Y="4.121895996094" />
                  <Point X="-27.047888671875" Y="4.110403320312" />
                  <Point X="-27.0389765625" Y="4.105128417969" />
                  <Point X="-26.98082421875" Y="4.074856445312" />
                  <Point X="-26.943759765625" Y="4.055561523438" />
                  <Point X="-26.93432421875" Y="4.051286621094" />
                  <Point X="-26.915044921875" Y="4.043789550781" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031378173828" />
                  <Point X="-26.780818359375" Y="4.035136474609" />
                  <Point X="-26.770728515625" Y="4.037488525391" />
                  <Point X="-26.7508671875" Y="4.043277587891" />
                  <Point X="-26.741095703125" Y="4.046714111328" />
                  <Point X="-26.68052734375" Y="4.071802978516" />
                  <Point X="-26.641921875" Y="4.087793701172" />
                  <Point X="-26.6325859375" Y="4.092271240234" />
                  <Point X="-26.614451171875" Y="4.102219726562" />
                  <Point X="-26.60565234375" Y="4.107690429687" />
                  <Point X="-26.587923828125" Y="4.120104003906" />
                  <Point X="-26.579775390625" Y="4.126499023438" />
                  <Point X="-26.564224609375" Y="4.14013671875" />
                  <Point X="-26.550251953125" Y="4.15538671875" />
                  <Point X="-26.538021484375" Y="4.172066894531" />
                  <Point X="-26.532361328125" Y="4.180739746094" />
                  <Point X="-26.5215390625" Y="4.199483398437" />
                  <Point X="-26.51685546875" Y="4.208725097656" />
                  <Point X="-26.5085234375" Y="4.227661621094" />
                  <Point X="-26.504875" Y="4.237356445312" />
                  <Point X="-26.485162109375" Y="4.299881835937" />
                  <Point X="-26.472595703125" Y="4.339732910156" />
                  <Point X="-26.470025390625" Y="4.349763671875" />
                  <Point X="-26.465990234375" Y="4.370048339844" />
                  <Point X="-26.464525390625" Y="4.380302246094" />
                  <Point X="-26.462638671875" Y="4.401864746094" />
                  <Point X="-26.46230078125" Y="4.412217285156" />
                  <Point X="-26.462751953125" Y="4.4328984375" />
                  <Point X="-26.463541015625" Y="4.443227050781" />
                  <Point X="-26.479265625" Y="4.562655273438" />
                  <Point X="-26.21141015625" Y="4.637752929688" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.46698828125" Y="4.770646484375" />
                  <Point X="-25.36522265625" Y="4.782556152344" />
                  <Point X="-25.343689453125" Y="4.702190917969" />
                  <Point X="-25.22566796875" Y="4.261728515625" />
                  <Point X="-25.220435546875" Y="4.247106933594" />
                  <Point X="-25.20766015625" Y="4.218912597656" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218911621094" />
                  <Point X="-24.767251953125" Y="4.247104980469" />
                  <Point X="-24.76201953125" Y="4.2617265625" />
                  <Point X="-24.686794921875" Y="4.542463378906" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.4168203125" Y="4.763538574219" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.788078125" Y="4.645191894531" />
                  <Point X="-23.54640625" Y="4.586845214844" />
                  <Point X="-23.2979765625" Y="4.496737304688" />
                  <Point X="-23.141734375" Y="4.440067382812" />
                  <Point X="-22.900021484375" Y="4.327026855469" />
                  <Point X="-22.749546875" Y="4.256653808594" />
                  <Point X="-22.515984375" Y="4.120580078125" />
                  <Point X="-22.370580078125" Y="4.035865478516" />
                  <Point X="-22.18221875" Y="3.901915283203" />
                  <Point X="-22.4084609375" Y="3.510050292969" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.937619140625" Y="2.593116699219" />
                  <Point X="-22.94681640625" Y="2.573442871094" />
                  <Point X="-22.95581640625" Y="2.549567626953" />
                  <Point X="-22.958697265625" Y="2.540602050781" />
                  <Point X="-22.971810546875" Y="2.491568359375" />
                  <Point X="-22.98016796875" Y="2.460315673828" />
                  <Point X="-22.9820859375" Y="2.451469726562" />
                  <Point X="-22.986353515625" Y="2.420223144531" />
                  <Point X="-22.987244140625" Y="2.383240234375" />
                  <Point X="-22.986587890625" Y="2.369578857422" />
                  <Point X="-22.981474609375" Y="2.327178710938" />
                  <Point X="-22.978216796875" Y="2.300154052734" />
                  <Point X="-22.976197265625" Y="2.289031494141" />
                  <Point X="-22.970853515625" Y="2.267107666016" />
                  <Point X="-22.967529296875" Y="2.256306396484" />
                  <Point X="-22.95926171875" Y="2.234215332031" />
                  <Point X="-22.9546796875" Y="2.223888916016" />
                  <Point X="-22.944318359375" Y="2.203844970703" />
                  <Point X="-22.9385390625" Y="2.194127441406" />
                  <Point X="-22.912302734375" Y="2.155462646484" />
                  <Point X="-22.895580078125" Y="2.130818847656" />
                  <Point X="-22.890189453125" Y="2.1236328125" />
                  <Point X="-22.869537109375" Y="2.100124023438" />
                  <Point X="-22.84240234375" Y="2.075387207031" />
                  <Point X="-22.8317421875" Y="2.066982177734" />
                  <Point X="-22.793078125" Y="2.04074621582" />
                  <Point X="-22.76843359375" Y="2.024024414062" />
                  <Point X="-22.75871875" Y="2.018244995117" />
                  <Point X="-22.738671875" Y="2.007881469727" />
                  <Point X="-22.72833984375" Y="2.003297485352" />
                  <Point X="-22.706251953125" Y="1.995031860352" />
                  <Point X="-22.695451171875" Y="1.991707397461" />
                  <Point X="-22.673529296875" Y="1.986365112305" />
                  <Point X="-22.662408203125" Y="1.984346801758" />
                  <Point X="-22.6200078125" Y="1.979234008789" />
                  <Point X="-22.592984375" Y="1.975975219727" />
                  <Point X="-22.58395703125" Y="1.975320922852" />
                  <Point X="-22.55242578125" Y="1.975496948242" />
                  <Point X="-22.515685546875" Y="1.979822509766" />
                  <Point X="-22.502251953125" Y="1.982395507812" />
                  <Point X="-22.453216796875" Y="1.99550769043" />
                  <Point X="-22.42196484375" Y="2.003865234375" />
                  <Point X="-22.416001953125" Y="2.005671142578" />
                  <Point X="-22.395587890625" Y="2.013069458008" />
                  <Point X="-22.372341796875" Y="2.023574829102" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.78303515625" Y="2.363272949219" />
                  <Point X="-21.059595703125" Y="2.780949951172" />
                  <Point X="-21.04230859375" Y="2.756924316406" />
                  <Point X="-20.95603515625" Y="2.637025878906" />
                  <Point X="-20.86311328125" Y="2.483471679688" />
                  <Point X="-21.13421875" Y="2.275444824219" />
                  <Point X="-21.827046875" Y="1.743819824219" />
                  <Point X="-21.831857421875" Y="1.739871337891" />
                  <Point X="-21.847875" Y="1.725221435547" />
                  <Point X="-21.865330078125" Y="1.706604492188" />
                  <Point X="-21.87142578125" Y="1.699420776367" />
                  <Point X="-21.90671484375" Y="1.65338269043" />
                  <Point X="-21.92920703125" Y="1.624039428711" />
                  <Point X="-21.934365234375" Y="1.616600097656" />
                  <Point X="-21.950263671875" Y="1.589366333008" />
                  <Point X="-21.965236328125" Y="1.555544921875" />
                  <Point X="-21.96985546875" Y="1.542676391602" />
                  <Point X="-21.983001953125" Y="1.495671386719" />
                  <Point X="-21.991380859375" Y="1.465711791992" />
                  <Point X="-21.993771484375" Y="1.454669555664" />
                  <Point X="-21.9972265625" Y="1.432369873047" />
                  <Point X="-21.998291015625" Y="1.421112670898" />
                  <Point X="-21.999107421875" Y="1.397540649414" />
                  <Point X="-21.998826171875" Y="1.386243530273" />
                  <Point X="-21.996923828125" Y="1.363758056641" />
                  <Point X="-21.995302734375" Y="1.352569702148" />
                  <Point X="-21.98451171875" Y="1.300270263672" />
                  <Point X="-21.9776328125" Y="1.266936767578" />
                  <Point X="-21.97540234375" Y="1.258232788086" />
                  <Point X="-21.96531640625" Y="1.228607421875" />
                  <Point X="-21.94971484375" Y="1.195372436523" />
                  <Point X="-21.943083984375" Y="1.183527954102" />
                  <Point X="-21.913734375" Y="1.138916625977" />
                  <Point X="-21.89502734375" Y="1.110482543945" />
                  <Point X="-21.888263671875" Y="1.101425048828" />
                  <Point X="-21.873708984375" Y="1.084178955078" />
                  <Point X="-21.86591796875" Y="1.075990966797" />
                  <Point X="-21.848673828125" Y="1.059900878906" />
                  <Point X="-21.839966796875" Y="1.052696044922" />
                  <Point X="-21.8217578125" Y="1.039370605469" />
                  <Point X="-21.812255859375" Y="1.033249389648" />
                  <Point X="-21.76972265625" Y="1.009306945801" />
                  <Point X="-21.74261328125" Y="0.994046936035" />
                  <Point X="-21.73451953125" Y="0.989986572266" />
                  <Point X="-21.70531640625" Y="0.978082336426" />
                  <Point X="-21.669720703125" Y="0.968020690918" />
                  <Point X="-21.656328125" Y="0.9652578125" />
                  <Point X="-21.5988203125" Y="0.957657226563" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.555966796875" Y="0.952199829102" />
                  <Point X="-21.53428125" Y="0.951222900391" />
                  <Point X="-21.50878125" Y="0.952032287598" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-20.94755078125" Y="1.02544921875" />
                  <Point X="-20.295296875" Y="1.111319702148" />
                  <Point X="-20.284365234375" Y="1.066413574219" />
                  <Point X="-20.247310546875" Y="0.914205444336" />
                  <Point X="-20.216126953125" Y="0.713921264648" />
                  <Point X="-20.51401171875" Y="0.634103271484" />
                  <Point X="-21.3080078125" Y="0.42135269165" />
                  <Point X="-21.313966796875" Y="0.419544830322" />
                  <Point X="-21.334375" Y="0.412137023926" />
                  <Point X="-21.357619140625" Y="0.401618377686" />
                  <Point X="-21.365994140625" Y="0.397316040039" />
                  <Point X="-21.4224921875" Y="0.364658538818" />
                  <Point X="-21.45850390625" Y="0.343843597412" />
                  <Point X="-21.466115234375" Y="0.338946044922" />
                  <Point X="-21.491228515625" Y="0.319867462158" />
                  <Point X="-21.518005859375" Y="0.294349212646" />
                  <Point X="-21.52719921875" Y="0.28422769165" />
                  <Point X="-21.561099609375" Y="0.241031936646" />
                  <Point X="-21.582705078125" Y="0.213500137329" />
                  <Point X="-21.589142578125" Y="0.204210464478" />
                  <Point X="-21.600869140625" Y="0.184927536011" />
                  <Point X="-21.606158203125" Y="0.17493397522" />
                  <Point X="-21.615931640625" Y="0.153469863892" />
                  <Point X="-21.619994140625" Y="0.14292729187" />
                  <Point X="-21.62683984375" Y="0.12142943573" />
                  <Point X="-21.629623046875" Y="0.110474441528" />
                  <Point X="-21.640923828125" Y="0.051470855713" />
                  <Point X="-21.648125" Y="0.013863910675" />
                  <Point X="-21.649396484375" Y="0.004965708733" />
                  <Point X="-21.6514140625" Y="-0.026263233185" />
                  <Point X="-21.64971875" Y="-0.062939647675" />
                  <Point X="-21.648125" Y="-0.076420341492" />
                  <Point X="-21.636826171875" Y="-0.135424072266" />
                  <Point X="-21.629623046875" Y="-0.17303086853" />
                  <Point X="-21.62683984375" Y="-0.183988388062" />
                  <Point X="-21.6199921875" Y="-0.205492797852" />
                  <Point X="-21.615927734375" Y="-0.216039672852" />
                  <Point X="-21.606154296875" Y="-0.237501403809" />
                  <Point X="-21.600865234375" Y="-0.247491836548" />
                  <Point X="-21.589140625" Y="-0.266771362305" />
                  <Point X="-21.582705078125" Y="-0.276060577393" />
                  <Point X="-21.5488046875" Y="-0.319256347656" />
                  <Point X="-21.52719921875" Y="-0.346788146973" />
                  <Point X="-21.521279296875" Y="-0.353634002686" />
                  <Point X="-21.498861328125" Y="-0.375802734375" />
                  <Point X="-21.46982421875" Y="-0.398723510742" />
                  <Point X="-21.45850390625" Y="-0.406404510498" />
                  <Point X="-21.40200390625" Y="-0.439062011719" />
                  <Point X="-21.365994140625" Y="-0.459876922607" />
                  <Point X="-21.360505859375" Y="-0.462813873291" />
                  <Point X="-21.340845703125" Y="-0.472014373779" />
                  <Point X="-21.31697265625" Y="-0.481027160645" />
                  <Point X="-21.3080078125" Y="-0.483912963867" />
                  <Point X="-20.801939453125" Y="-0.619513244629" />
                  <Point X="-20.215119140625" Y="-0.776751403809" />
                  <Point X="-20.217697265625" Y="-0.793848510742" />
                  <Point X="-20.238380859375" Y="-0.931034790039" />
                  <Point X="-20.272197265625" Y="-1.079219848633" />
                  <Point X="-20.64240625" Y="-1.030480834961" />
                  <Point X="-21.563216796875" Y="-0.909253601074" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042480469" />
                  <Point X="-21.63327734375" Y="-0.910840881348" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.75640625" Y="-0.936778381348" />
                  <Point X="-21.82708203125" Y="-0.952140075684" />
                  <Point X="-21.842125" Y="-0.95674230957" />
                  <Point X="-21.871244140625" Y="-0.968366088867" />
                  <Point X="-21.8853203125" Y="-0.975387512207" />
                  <Point X="-21.9131484375" Y="-0.992280151367" />
                  <Point X="-21.925875" Y="-1.001530578613" />
                  <Point X="-21.949625" Y="-1.02200177002" />
                  <Point X="-21.9606484375" Y="-1.03322253418" />
                  <Point X="-22.027673828125" Y="-1.11383203125" />
                  <Point X="-22.070392578125" Y="-1.165210327148" />
                  <Point X="-22.078673828125" Y="-1.176847900391" />
                  <Point X="-22.093396484375" Y="-1.201234008789" />
                  <Point X="-22.099837890625" Y="-1.213982666016" />
                  <Point X="-22.1111796875" Y="-1.24136730957" />
                  <Point X="-22.11563671875" Y="-1.254934448242" />
                  <Point X="-22.122466796875" Y="-1.282581542969" />
                  <Point X="-22.12483984375" Y="-1.296661499023" />
                  <Point X="-22.1344453125" Y="-1.401055053711" />
                  <Point X="-22.140568359375" Y="-1.467591796875" />
                  <Point X="-22.140708984375" Y="-1.483315429688" />
                  <Point X="-22.138392578125" Y="-1.514580200195" />
                  <Point X="-22.135935546875" Y="-1.53012109375" />
                  <Point X="-22.128205078125" Y="-1.561742431641" />
                  <Point X="-22.12321484375" Y="-1.576665649414" />
                  <Point X="-22.110841796875" Y="-1.60548034668" />
                  <Point X="-22.103458984375" Y="-1.619371826172" />
                  <Point X="-22.042091796875" Y="-1.71482409668" />
                  <Point X="-22.002978515625" Y="-1.775662353516" />
                  <Point X="-21.99825390625" Y="-1.782358642578" />
                  <Point X="-21.980201171875" Y="-1.804454101562" />
                  <Point X="-21.9565078125" Y="-1.828121337891" />
                  <Point X="-21.947201171875" Y="-1.836277587891" />
                  <Point X="-21.477568359375" Y="-2.196640136719" />
                  <Point X="-20.912828125" Y="-2.629980957031" />
                  <Point X="-20.954517578125" Y="-2.697439453125" />
                  <Point X="-20.998724609375" Y="-2.760252197266" />
                  <Point X="-21.3314375" Y="-2.568159667969" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.18497265625" Y="-2.079513427734" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228892578125" Y="-2.066337646484" />
                  <Point X="-22.3608671875" Y="-2.042503051758" />
                  <Point X="-22.444982421875" Y="-2.027311889648" />
                  <Point X="-22.460640625" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403442383" />
                  <Point X="-22.507685546875" Y="-2.02650378418" />
                  <Point X="-22.539859375" Y="-2.031461914062" />
                  <Point X="-22.555154296875" Y="-2.03513659668" />
                  <Point X="-22.5849296875" Y="-2.044960083008" />
                  <Point X="-22.59941015625" Y="-2.051108886719" />
                  <Point X="-22.709048828125" Y="-2.108810546875" />
                  <Point X="-22.778927734375" Y="-2.145587890625" />
                  <Point X="-22.79103125" Y="-2.153170654297" />
                  <Point X="-22.813962890625" Y="-2.170064453125" />
                  <Point X="-22.824791015625" Y="-2.179375488281" />
                  <Point X="-22.84575" Y="-2.200334960938" />
                  <Point X="-22.85505859375" Y="-2.211161132813" />
                  <Point X="-22.871951171875" Y="-2.234090332031" />
                  <Point X="-22.87953515625" Y="-2.246193359375" />
                  <Point X="-22.93723828125" Y="-2.355831542969" />
                  <Point X="-22.974015625" Y="-2.425711425781" />
                  <Point X="-22.9801640625" Y="-2.440193359375" />
                  <Point X="-22.98998828125" Y="-2.469972167969" />
                  <Point X="-22.9936640625" Y="-2.485269042969" />
                  <Point X="-22.99862109375" Y="-2.517441894531" />
                  <Point X="-22.999720703125" Y="-2.533133300781" />
                  <Point X="-22.99931640625" Y="-2.564484375" />
                  <Point X="-22.9978125" Y="-2.580144042969" />
                  <Point X="-22.9739765625" Y="-2.712118164062" />
                  <Point X="-22.95878515625" Y="-2.796234375" />
                  <Point X="-22.95698046875" Y="-2.804230957031" />
                  <Point X="-22.948763671875" Y="-2.83153515625" />
                  <Point X="-22.935931640625" Y="-2.862473876953" />
                  <Point X="-22.930453125" Y="-2.873578125" />
                  <Point X="-22.62866796875" Y="-3.396287841797" />
                  <Point X="-22.264107421875" Y="-4.027722900391" />
                  <Point X="-22.276244140625" Y="-4.036082763672" />
                  <Point X="-22.540265625" Y="-3.692003173828" />
                  <Point X="-23.16608203125" Y="-2.876422851562" />
                  <Point X="-23.171345703125" Y="-2.870144042969" />
                  <Point X="-23.191169921875" Y="-2.849624023438" />
                  <Point X="-23.216748046875" Y="-2.828003662109" />
                  <Point X="-23.22669921875" Y="-2.820646728516" />
                  <Point X="-23.35686328125" Y="-2.736964355469" />
                  <Point X="-23.43982421875" Y="-2.683627929688" />
                  <Point X="-23.453716796875" Y="-2.676245605469" />
                  <Point X="-23.48252734375" Y="-2.663874511719" />
                  <Point X="-23.4974453125" Y="-2.658885742188" />
                  <Point X="-23.52906640625" Y="-2.651154052734" />
                  <Point X="-23.54460546875" Y="-2.648695800781" />
                  <Point X="-23.575875" Y="-2.646376953125" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.733958984375" Y="-2.659615966797" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670339111328" />
                  <Point X="-23.866421875" Y="-2.677170410156" />
                  <Point X="-23.87998828125" Y="-2.681627929688" />
                  <Point X="-23.907373046875" Y="-2.692970703125" />
                  <Point X="-23.92012109375" Y="-2.699412841797" />
                  <Point X="-23.94450390625" Y="-2.714134033203" />
                  <Point X="-23.956138671875" Y="-2.722413085938" />
                  <Point X="-24.066060546875" Y="-2.813810058594" />
                  <Point X="-24.136123046875" Y="-2.872063476562" />
                  <Point X="-24.147345703125" Y="-2.883086914062" />
                  <Point X="-24.16781640625" Y="-2.906836181641" />
                  <Point X="-24.177064453125" Y="-2.919562011719" />
                  <Point X="-24.19395703125" Y="-2.947388427734" />
                  <Point X="-24.20098046875" Y="-2.961466552734" />
                  <Point X="-24.21260546875" Y="-2.990588134766" />
                  <Point X="-24.21720703125" Y="-3.005631591797" />
                  <Point X="-24.250072265625" Y="-3.156842529297" />
                  <Point X="-24.271021484375" Y="-3.253219482422" />
                  <Point X="-24.2724140625" Y="-3.261286865234" />
                  <Point X="-24.275275390625" Y="-3.289677001953" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520263672" />
                  <Point X="-24.188919921875" Y="-3.985139648438" />
                  <Point X="-24.16691015625" Y="-4.152326660156" />
                  <Point X="-24.344931640625" Y="-3.487936767578" />
                  <Point X="-24.347392578125" Y="-3.480120849609" />
                  <Point X="-24.357853515625" Y="-3.453579589844" />
                  <Point X="-24.3732109375" Y="-3.423815917969" />
                  <Point X="-24.37958984375" Y="-3.413209960938" />
                  <Point X="-24.479583984375" Y="-3.269136962891" />
                  <Point X="-24.543318359375" Y="-3.177309570312" />
                  <Point X="-24.553328125" Y="-3.165172363281" />
                  <Point X="-24.575212890625" Y="-3.142716308594" />
                  <Point X="-24.587087890625" Y="-3.132397460938" />
                  <Point X="-24.61334375" Y="-3.113153564453" />
                  <Point X="-24.6267578125" Y="-3.104936523438" />
                  <Point X="-24.6547578125" Y="-3.090829101562" />
                  <Point X="-24.66934375" Y="-3.084938720703" />
                  <Point X="-24.824078125" Y="-3.036914306641" />
                  <Point X="-24.922703125" Y="-3.006305419922" />
                  <Point X="-24.936623046875" Y="-3.003109375" />
                  <Point X="-24.964783203125" Y="-2.99883984375" />
                  <Point X="-24.9790234375" Y="-2.997766357422" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.998840087891" />
                  <Point X="-25.051064453125" Y="-3.003109619141" />
                  <Point X="-25.064984375" Y="-3.006305664062" />
                  <Point X="-25.21971875" Y="-3.054329833984" />
                  <Point X="-25.31834375" Y="-3.084938964844" />
                  <Point X="-25.3329296875" Y="-3.090830078125" />
                  <Point X="-25.3609296875" Y="-3.104937988281" />
                  <Point X="-25.37434375" Y="-3.113154785156" />
                  <Point X="-25.400599609375" Y="-3.132398925781" />
                  <Point X="-25.412474609375" Y="-3.142716552734" />
                  <Point X="-25.434359375" Y="-3.165172119141" />
                  <Point X="-25.444369140625" Y="-3.177310058594" />
                  <Point X="-25.54436328125" Y="-3.3213828125" />
                  <Point X="-25.608095703125" Y="-3.413210449219" />
                  <Point X="-25.61246875" Y="-3.420131835938" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213623047" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.798685546875" Y="-4.069881835938" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.605854093918" Y="4.783335672048" />
                  <Point X="-24.622812204963" Y="4.78125347783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.341237978311" Y="4.693041884708" />
                  <Point X="-26.478054703933" Y="4.553458342225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.185157148802" Y="4.739277328309" />
                  <Point X="-24.649330626994" Y="4.682283991606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.316408542896" Y="4.600377122614" />
                  <Point X="-26.465653012878" Y="4.459267644997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.917471444518" Y="4.67643156655" />
                  <Point X="-24.675849049024" Y="4.583314505383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.291579107481" Y="4.50771236052" />
                  <Point X="-26.467322874546" Y="4.363349178347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.654677779115" Y="4.612985137946" />
                  <Point X="-24.702367997866" Y="4.484344954475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.266749672067" Y="4.415047598426" />
                  <Point X="-26.496456394046" Y="4.264058598529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.129619190414" Y="4.186315982597" />
                  <Point X="-27.399794407011" Y="4.15314263726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.430484357443" Y="4.544799195365" />
                  <Point X="-24.728887317001" Y="4.385375358101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.241920236652" Y="4.322382836333" />
                  <Point X="-26.545132699821" Y="4.162368466279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.033697375252" Y="4.102380267134" />
                  <Point X="-27.620946764437" Y="4.030275108742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.233337499612" Y="4.473292352319" />
                  <Point X="-24.755406636136" Y="4.286405761727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.212795307679" Y="4.230245494529" />
                  <Point X="-26.762033107492" Y="4.040023011545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.848173462029" Y="4.029446305937" />
                  <Point X="-27.778579997312" Y="3.915206748041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.05495124374" Y="4.399481996999" />
                  <Point X="-24.801107841358" Y="4.185080925892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.14942652115" Y="4.14231276974" />
                  <Point X="-27.927224301902" Y="3.801242088953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.89284856753" Y="4.3236722695" />
                  <Point X="-28.053664713808" Y="3.690003725078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.733811171316" Y="4.247486172942" />
                  <Point X="-28.002062704894" Y="3.600626221666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.598121631038" Y="4.168433320146" />
                  <Point X="-27.950460695981" Y="3.511248718253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.462433093509" Y="4.089380344227" />
                  <Point X="-27.898858623322" Y="3.421871222668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.333501606635" Y="4.009497706811" />
                  <Point X="-27.84725652168" Y="3.332493730641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.21872612726" Y="3.92787693023" />
                  <Point X="-27.795654420038" Y="3.243116238614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.222776911785" Y="3.831666123012" />
                  <Point X="-27.760045255045" Y="3.151775060883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.72364606208" Y="3.033459758906" />
                  <Point X="-28.727959076085" Y="3.032930187375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.282253024474" Y="3.728649941213" />
                  <Point X="-27.749923960854" Y="3.057304366128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.586940067113" Y="2.954531711052" />
                  <Point X="-28.810288444064" Y="2.92710797866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.341729137163" Y="3.625633759413" />
                  <Point X="-27.757048224198" Y="2.960716183163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.450233041688" Y="2.875603789723" />
                  <Point X="-28.892616074214" Y="2.821285983323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.401205249853" Y="3.522617577614" />
                  <Point X="-27.814430532759" Y="2.857957088184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.313526016262" Y="2.796675868393" />
                  <Point X="-28.974943704365" Y="2.715463987986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.460681576142" Y="3.419601369588" />
                  <Point X="-27.947717057085" Y="2.745878127402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.103996265748" Y="2.726689453388" />
                  <Point X="-29.042478616331" Y="2.611458310056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.52015793211" Y="3.316585157918" />
                  <Point X="-29.1026595369" Y="2.508355588731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.579634288079" Y="3.213568946247" />
                  <Point X="-29.16283989852" Y="2.405252936036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.639110644047" Y="3.110552734577" />
                  <Point X="-29.223019891308" Y="2.302150328628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.698587000015" Y="3.007536522907" />
                  <Point X="-29.128291197968" Y="2.218068116226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.758063355983" Y="2.904520311237" />
                  <Point X="-29.020761650388" Y="2.135557651091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.817539711951" Y="2.801504099566" />
                  <Point X="-28.913232102808" Y="2.053047185956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.877016067919" Y="2.698487887896" />
                  <Point X="-28.805702525974" Y="1.970536724413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.936350234686" Y="2.595489134865" />
                  <Point X="-28.698172933393" Y="1.888026264804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.026051406642" Y="2.734330904241" />
                  <Point X="-21.171214146839" Y="2.716507160926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.970745038273" Y="2.49555255059" />
                  <Point X="-28.590643340811" Y="1.805515805194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.962771208689" Y="2.646387302142" />
                  <Point X="-21.381774933525" Y="2.594940113771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.98689215068" Y="2.397856501065" />
                  <Point X="-28.497825838717" Y="1.721198928014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.907860055952" Y="2.557416110501" />
                  <Point X="-21.592335720212" Y="2.473373066616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.978579611046" Y="2.303163719175" />
                  <Point X="-28.447089637544" Y="1.631715116779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.888363380721" Y="2.464096567789" />
                  <Point X="-21.802896412815" Y="2.351806031012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.948115638589" Y="2.211190791238" />
                  <Point X="-28.421733450908" Y="1.539115031602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.036861381957" Y="2.350149872494" />
                  <Point X="-22.013456202078" Y="2.230239106325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.889362383785" Y="2.122691350412" />
                  <Point X="-28.435086486424" Y="1.441762051581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.540705614798" Y="1.306009092378" />
                  <Point X="-29.640210143245" Y="1.293791472545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.18535955698" Y="2.23620315586" />
                  <Point X="-22.224015991341" Y="2.108672181638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.790649486708" Y="2.039098336717" />
                  <Point X="-28.479911815265" Y="1.340544759844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.16452810886" Y="1.256484448847" />
                  <Point X="-29.667039188536" Y="1.194783846581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.333858062843" Y="2.122256398605" />
                  <Point X="-28.641999077436" Y="1.224929513111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.788350208434" Y="1.206959853754" />
                  <Point X="-29.693868233827" Y="1.095776220617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.482356568705" Y="2.008309641349" />
                  <Point X="-29.720697279118" Y="0.996768594652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.630855074568" Y="1.894362884093" />
                  <Point X="-29.740038987078" Y="0.898680298115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.779353580431" Y="1.780416126838" />
                  <Point X="-29.754463779193" Y="0.80119572293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.893445415142" Y="1.670693977591" />
                  <Point X="-29.768888808015" Y="0.703711118682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.960264259047" Y="1.566776221764" />
                  <Point X="-29.783314213116" Y="0.606226468232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.990937638629" Y="1.467296570902" />
                  <Point X="-29.576675503929" Y="0.535885077986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.997517503811" Y="1.370775231626" />
                  <Point X="-29.331717461487" Y="0.470248710249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.979759506482" Y="1.277242206112" />
                  <Point X="-29.086759419044" Y="0.404612342511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.944395634142" Y="1.185870910231" />
                  <Point X="-28.84180118878" Y="0.338975997835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.884917775171" Y="1.097460439609" />
                  <Point X="-28.596842957826" Y="0.273339653244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.779324926916" Y="1.014712177698" />
                  <Point X="-28.412407154726" Y="0.200272088924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.292908321575" Y="1.101507754485" />
                  <Point X="-21.434825899097" Y="0.961297906142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.5108137122" Y="0.951967775876" />
                  <Point X="-28.334576810923" Y="0.114115020094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.270283908964" Y="1.008572249635" />
                  <Point X="-28.300483115051" Y="0.022587766152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.247659007978" Y="0.915636804749" />
                  <Point X="-28.2986243154" Y="-0.072897435367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.232913036548" Y="0.821733948958" />
                  <Point X="-28.332400829024" Y="-0.17275810318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.218290311943" Y="0.727815960359" />
                  <Point X="-28.440619746109" Y="-0.281759148814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.777924339576" Y="0.563388108591" />
                  <Point X="-29.015365104915" Y="-0.448042438745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.370051867875" Y="0.394970556612" />
                  <Point X="-29.674708491205" Y="-0.624713060333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.53090409381" Y="0.279506953261" />
                  <Point X="-29.775278872272" Y="-0.732774983831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.60634171869" Y="0.174530944196" />
                  <Point X="-29.761826562581" Y="-0.826836681311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.63639290522" Y="0.075127689035" />
                  <Point X="-29.74837425289" Y="-0.920898378791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.65116443948" Y="-0.022399460732" />
                  <Point X="-29.730469274624" Y="-1.014413357315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.640394379525" Y="-0.116790497068" />
                  <Point X="-29.706763377346" Y="-1.107216072546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.618334140767" Y="-0.209795273757" />
                  <Point X="-29.683057792474" Y="-1.200018826135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.564756785372" Y="-0.298930235119" />
                  <Point X="-28.154054428377" Y="-1.107994252874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.48707594662" Y="-0.385105660861" />
                  <Point X="-28.016990716183" Y="-1.186878378575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.356334348123" Y="-0.464766044516" />
                  <Point X="-27.960425098729" Y="-1.275646427492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.12723797121" Y="-0.532349979891" />
                  <Point X="-27.948891765308" Y="-1.369943745631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.882279374466" Y="-0.597986279569" />
                  <Point X="-27.97150758056" Y="-1.468434051995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.637321143904" Y="-0.663622624209" />
                  <Point X="-28.047706350084" Y="-1.573503517871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.392363092053" Y="-0.729258990791" />
                  <Point X="-28.196113472989" Y="-1.68743905471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.2191838864" Y="-0.803708691486" />
                  <Point X="-21.329439829619" Y="-0.940030979964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.941749000824" Y="-1.015213092687" />
                  <Point X="-28.344611664027" Y="-1.80138577331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.233886797496" Y="-0.901227415388" />
                  <Point X="-20.953262940025" Y="-0.989555699172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.034752772759" Y="-1.122345953405" />
                  <Point X="-28.493109855066" Y="-1.91533249191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.253983999996" Y="-0.99940847499" />
                  <Point X="-20.577086040412" Y="-1.03908041715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.105103913735" Y="-1.226697420777" />
                  <Point X="-28.641608046105" Y="-2.02927921051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.12746168623" Y="-1.325156043475" />
                  <Point X="-28.790106237143" Y="-2.143225929111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.136369386083" Y="-1.421963204908" />
                  <Point X="-28.938604287555" Y="-2.257172630444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.137873823386" Y="-1.517861360001" />
                  <Point X="-29.087102258739" Y="-2.371119322049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.108459350939" Y="-1.609963150336" />
                  <Point X="-29.161805691881" Y="-2.476005183704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.052398249989" Y="-1.69879314609" />
                  <Point X="-27.480078617424" Y="-2.365228496727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.712357557147" Y="-2.393748764348" />
                  <Point X="-29.108620821718" Y="-2.565188336193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.994170035882" Y="-1.787357053808" />
                  <Point X="-27.368842720618" Y="-2.4472838794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.922918407804" Y="-2.515315819358" />
                  <Point X="-29.055435951554" Y="-2.654371488682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.901110558339" Y="-1.871644220138" />
                  <Point X="-27.320885110985" Y="-2.537108858778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.133479258461" Y="-2.636882874368" />
                  <Point X="-29.001803651515" Y="-2.74349970369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.79358101085" Y="-1.954154685284" />
                  <Point X="-22.422486731907" Y="-2.031374598094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.60433990339" Y="-2.053703359903" />
                  <Point X="-27.31309205517" Y="-2.63186542526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.344039473511" Y="-2.758449851335" />
                  <Point X="-28.935177491382" Y="-2.831032473292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.686051463362" Y="-2.03666515043" />
                  <Point X="-22.152864876849" Y="-2.093982630429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.820869340711" Y="-2.176003265206" />
                  <Point X="-27.345802497595" Y="-2.731595195988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.55459946317" Y="-2.880016800627" />
                  <Point X="-28.868551324905" Y="-2.918565242115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.578521915873" Y="-2.119175615577" />
                  <Point X="-22.01605190981" Y="-2.172897543764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.897949287795" Y="-2.281180926081" />
                  <Point X="-27.405279057758" Y="-2.834611432731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.765159452829" Y="-3.00158374992" />
                  <Point X="-28.80192511536" Y="-3.00609800565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.47099236387" Y="-2.201686080168" />
                  <Point X="-21.879344711192" Y="-2.251825443828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.951803616153" Y="-2.38350683956" />
                  <Point X="-27.46475561792" Y="-2.937627669473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.363462742561" Y="-2.28419653625" />
                  <Point X="-21.742637512574" Y="-2.330753343892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.993439004146" Y="-2.484332455811" />
                  <Point X="-27.524232178082" Y="-3.040643906215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.255933121251" Y="-2.366706992332" />
                  <Point X="-21.605930313956" Y="-2.409681243956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.997734959627" Y="-2.580573366237" />
                  <Point X="-23.549557091507" Y="-2.648328604397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.904944131459" Y="-2.691964646048" />
                  <Point X="-27.583708738244" Y="-3.143660142957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.148403499942" Y="-2.449217448414" />
                  <Point X="-21.469223115338" Y="-2.48860914402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.980823130254" Y="-2.674210288112" />
                  <Point X="-23.378519619056" Y="-2.723041276862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.057101209477" Y="-2.806360619479" />
                  <Point X="-27.643185298407" Y="-3.246676379699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.040873878632" Y="-2.531727904497" />
                  <Point X="-21.33251591672" Y="-2.567537044084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.963911871852" Y="-2.767847280094" />
                  <Point X="-23.253516059047" Y="-2.803406203054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.174863553453" Y="-2.916533450594" />
                  <Point X="-24.91796751948" Y="-3.007775144767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.16905096579" Y="-3.038604315473" />
                  <Point X="-27.702661858569" Y="-3.349692616441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.933344257323" Y="-2.614238360579" />
                  <Point X="-21.195809151559" Y="-2.64646499737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.936858575692" Y="-2.860238986422" />
                  <Point X="-23.157694130734" Y="-2.88735418308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.219845264878" Y="-3.017769943698" />
                  <Point X="-24.696995291104" Y="-3.076356600153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.435938900126" Y="-3.167087466719" />
                  <Point X="-27.762138418731" Y="-3.452708853183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.966159353314" Y="-2.713980981149" />
                  <Point X="-21.059102389845" Y="-2.725392951079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.886466773228" Y="-2.949765084502" />
                  <Point X="-23.090574416336" Y="-2.974826351838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.241218675603" Y="-3.116107701968" />
                  <Point X="-24.562678410203" Y="-3.155577994328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.510017202929" Y="-3.27189657202" />
                  <Point X="-27.821614873676" Y="-3.555725077006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.834864885138" Y="-3.03914260275" />
                  <Point X="-23.023454701938" Y="-3.062298520596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.262593304048" Y="-3.214445609754" />
                  <Point X="-24.497522608617" Y="-3.243291301259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.582635353499" Y="-3.376526393169" />
                  <Point X="-27.881091188179" Y="-3.658741283585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.783262997048" Y="-3.128520120998" />
                  <Point X="-22.95633498754" Y="-3.149770689355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.27526382451" Y="-3.311714787464" />
                  <Point X="-24.436308702643" Y="-3.331488612112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.639810742706" Y="-3.479260081646" />
                  <Point X="-27.940567502683" Y="-3.761757490165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.731661108959" Y="-3.217897639245" />
                  <Point X="-22.889215273142" Y="-3.237242858113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.265141920153" Y="-3.406185407301" />
                  <Point X="-24.37565363558" Y="-3.419754539754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.666968880136" Y="-3.578308115044" />
                  <Point X="-27.946283080632" Y="-3.858172708312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.680059220869" Y="-3.307275157493" />
                  <Point X="-22.822095558744" Y="-3.324715026871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.252741263363" Y="-3.500376231521" />
                  <Point X="-24.338768249702" Y="-3.510939017264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.693487662798" Y="-3.677277645548" />
                  <Point X="-26.653094916803" Y="-3.79510260087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.157049825856" Y="-3.856980483093" />
                  <Point X="-27.839071408764" Y="-3.940722203676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.628457331115" Y="-3.396652675536" />
                  <Point X="-22.754975844346" Y="-3.41218719563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.240340606573" Y="-3.59456705574" />
                  <Point X="-24.313938970836" Y="-3.603603798579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.72000644546" Y="-3.776247176051" />
                  <Point X="-26.478162769076" Y="-3.869337067342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.332542149433" Y="-3.974241664404" />
                  <Point X="-27.728661707268" Y="-4.022879030377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.576855035302" Y="-3.486030143722" />
                  <Point X="-22.687856129947" Y="-3.499659364388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.227939949783" Y="-3.68875787996" />
                  <Point X="-24.289109691969" Y="-3.696268579895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.746525228121" Y="-3.875216706554" />
                  <Point X="-26.382426279874" Y="-3.953295537972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.44256948127" Y="-4.083464755449" />
                  <Point X="-27.599661344568" Y="-4.102753210906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.525252739489" Y="-3.575407611908" />
                  <Point X="-22.620736415549" Y="-3.587131533146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.215539292993" Y="-3.78294870418" />
                  <Point X="-24.264280413102" Y="-3.78893336121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.773044010783" Y="-3.974186237058" />
                  <Point X="-26.28669106894" Y="-4.037254165553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.473650443677" Y="-3.664785080093" />
                  <Point X="-22.553616701151" Y="-3.674603701904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.203138636203" Y="-3.877139528399" />
                  <Point X="-24.239451134235" Y="-3.881598142526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.799562803366" Y="-4.073155768779" />
                  <Point X="-26.212377402712" Y="-4.123843028094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.422048147864" Y="-3.754162548279" />
                  <Point X="-22.486496969653" Y="-3.762075868563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.190737979413" Y="-3.971330352619" />
                  <Point X="-24.214621855369" Y="-3.974262923841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.826081885947" Y="-4.172125336108" />
                  <Point X="-26.17946373026" Y="-4.215515170693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.370445852051" Y="-3.843540016465" />
                  <Point X="-22.41937723391" Y="-3.849548034701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.178337879129" Y="-4.065521245169" />
                  <Point X="-24.189792576502" Y="-4.066927705156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.852600968527" Y="-4.271094903437" />
                  <Point X="-26.160877976216" Y="-4.308946560462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.318843556238" Y="-3.93291748465" />
                  <Point X="-22.352257498166" Y="-3.937020200838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.879120051107" Y="-4.370064470766" />
                  <Point X="-26.142292989265" Y="-4.402378044419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.267241260426" Y="-4.022294952836" />
                  <Point X="-22.285137762423" Y="-4.024492366975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.905639133687" Y="-4.469034038094" />
                  <Point X="-26.1237080269" Y="-4.495809531394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.932158216267" Y="-4.568003605423" />
                  <Point X="-26.122823601994" Y="-4.591414371089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.958677298847" Y="-4.666973172752" />
                  <Point X="-26.135631937848" Y="-4.688700470401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.985196381428" Y="-4.765942740081" />
                  <Point X="-25.988062722915" Y="-4.766294682562" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.283810546875" Y="-4.450156738281" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521543945312" />
                  <Point X="-24.635673828125" Y="-3.377470947266" />
                  <Point X="-24.699408203125" Y="-3.285643554688" />
                  <Point X="-24.7256640625" Y="-3.266399658203" />
                  <Point X="-24.8803984375" Y="-3.218375244141" />
                  <Point X="-24.9790234375" Y="-3.187766357422" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.1633984375" Y="-3.235790771484" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285644042969" />
                  <Point X="-25.3882734375" Y="-3.429716796875" />
                  <Point X="-25.452005859375" Y="-3.521544433594" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.615158203125" Y="-4.119057617188" />
                  <Point X="-25.847744140625" Y="-4.987077148438" />
                  <Point X="-25.98504296875" Y="-4.960426757812" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.27937890625" Y="-4.891975585938" />
                  <Point X="-26.351587890625" Y="-4.873396484375" />
                  <Point X="-26.338599609375" Y="-4.774733398438" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.3466484375" Y="-4.348915527344" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.202628417969" />
                  <Point X="-26.5287421875" Y="-4.077693603516" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.838318359375" Y="-3.973370117188" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791259766" />
                  <Point X="-27.147427734375" Y="-4.0790625" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.34727734375" Y="-4.271383300781" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.686970703125" Y="-4.272166503906" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.103880859375" Y="-3.976622314453" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.9606875" Y="-3.416602539062" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597593017578" />
                  <Point X="-27.513978515625" Y="-2.568763916016" />
                  <Point X="-27.531322265625" Y="-2.551418945312" />
                  <Point X="-27.56015234375" Y="-2.53719921875" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.093599609375" Y="-2.833251464844" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-29.02829296875" Y="-3.022403320313" />
                  <Point X="-29.161697265625" Y="-2.847136962891" />
                  <Point X="-29.339533203125" Y="-2.548933837891" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-28.959283203125" Y="-2.033550537109" />
                  <Point X="-28.163787109375" Y="-1.423144897461" />
                  <Point X="-28.145822265625" Y="-1.396017456055" />
                  <Point X="-28.138115234375" Y="-1.366262084961" />
                  <Point X="-28.140326171875" Y="-1.33459375" />
                  <Point X="-28.161158203125" Y="-1.310639282227" />
                  <Point X="-28.187640625" Y="-1.295052734375" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.8531484375" Y="-1.371989013672" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.875212890625" Y="-1.215466186523" />
                  <Point X="-29.927392578125" Y="-1.011187805176" />
                  <Point X="-29.97444140625" Y="-0.682225097656" />
                  <Point X="-29.99839453125" Y="-0.514742248535" />
                  <Point X="-29.46365234375" Y="-0.371458251953" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.5418984375" Y="-0.121426872253" />
                  <Point X="-28.52494921875" Y="-0.109662811279" />
                  <Point X="-28.51414453125" Y="-0.102164848328" />
                  <Point X="-28.4948984375" Y="-0.075908973694" />
                  <Point X="-28.485646484375" Y="-0.046095249176" />
                  <Point X="-28.4856484375" Y="-0.016458133698" />
                  <Point X="-28.494896484375" Y="0.013342202187" />
                  <Point X="-28.514140625" Y="0.039602336884" />
                  <Point X="-28.531091796875" Y="0.051366401672" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.1350390625" Y="0.220846435547" />
                  <Point X="-29.998185546875" Y="0.45212600708" />
                  <Point X="-29.951271484375" Y="0.769167297363" />
                  <Point X="-29.91764453125" Y="0.996414489746" />
                  <Point X="-29.822935546875" Y="1.345925292969" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.40284375" Y="1.479498779297" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.694189453125" Y="1.407694946289" />
                  <Point X="-28.67027734375" Y="1.415234008789" />
                  <Point X="-28.651533203125" Y="1.426056274414" />
                  <Point X="-28.6391171875" Y="1.443786743164" />
                  <Point X="-28.624064453125" Y="1.480128295898" />
                  <Point X="-28.614470703125" Y="1.503291137695" />
                  <Point X="-28.61071484375" Y="1.524607177734" />
                  <Point X="-28.61631640625" Y="1.545514038086" />
                  <Point X="-28.634478515625" Y="1.580405273438" />
                  <Point X="-28.646056640625" Y="1.602643798828" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-28.991267578125" Y="1.873436645508" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.290677734375" Y="2.563147216797" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-28.9091328125" Y="3.109479980469" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.558109375" Y="3.157279296875" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.086265625" Y="2.915863769531" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.0315078125" Y="2.915775146484" />
                  <Point X="-28.01325390625" Y="2.927403808594" />
                  <Point X="-27.97616796875" Y="2.964489746094" />
                  <Point X="-27.952529296875" Y="2.988127197266" />
                  <Point X="-27.9408984375" Y="3.006380859375" />
                  <Point X="-27.938072265625" Y="3.027839111328" />
                  <Point X="-27.94264453125" Y="3.080087402344" />
                  <Point X="-27.945556640625" Y="3.113388671875" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.098875" Y="3.388312988281" />
                  <Point X="-28.30727734375" Y="3.749276855469" />
                  <Point X="-27.98044921875" Y="3.999852294922" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.357744140625" Y="4.393857910156" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.08995703125" Y="4.44673828125" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.89309375" Y="4.243388671875" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.75323828125" Y="4.247339355469" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.696904296875" Y="4.275743652344" />
                  <Point X="-26.68608203125" Y="4.294487304688" />
                  <Point X="-26.666369140625" Y="4.357012695312" />
                  <Point X="-26.653802734375" Y="4.396863769531" />
                  <Point X="-26.651916015625" Y="4.418426269531" />
                  <Point X="-26.668607421875" Y="4.545203613281" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.262703125" Y="4.820698242188" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.48907421875" Y="4.959358398438" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.160162109375" Y="4.751365722656" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.870322265625" Y="4.591640136719" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.39703125" Y="4.952505371094" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.74348828125" Y="4.829885253906" />
                  <Point X="-23.491544921875" Y="4.76905859375" />
                  <Point X="-23.23319140625" Y="4.6753515625" />
                  <Point X="-23.0689609375" Y="4.615784179688" />
                  <Point X="-22.819533203125" Y="4.499135742188" />
                  <Point X="-22.6613046875" Y="4.42513671875" />
                  <Point X="-22.42033984375" Y="4.28475" />
                  <Point X="-22.26747265625" Y="4.195688964844" />
                  <Point X="-22.040216796875" Y="4.034078369141" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.24391796875" Y="3.415050292969" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491514648438" />
                  <Point X="-22.788259765625" Y="2.442480957031" />
                  <Point X="-22.7966171875" Y="2.411228271484" />
                  <Point X="-22.797955078125" Y="2.392327392578" />
                  <Point X="-22.792841796875" Y="2.349927246094" />
                  <Point X="-22.789583984375" Y="2.322902587891" />
                  <Point X="-22.78131640625" Y="2.300811523438" />
                  <Point X="-22.755080078125" Y="2.262146728516" />
                  <Point X="-22.738357421875" Y="2.237502929688" />
                  <Point X="-22.72505859375" Y="2.224203369141" />
                  <Point X="-22.68639453125" Y="2.197967529297" />
                  <Point X="-22.66175" Y="2.181245849609" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.59726171875" Y="2.167867431641" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.551333984375" Y="2.165946289062" />
                  <Point X="-22.502298828125" Y="2.17905859375" />
                  <Point X="-22.471046875" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.87803515625" Y="2.527817871094" />
                  <Point X="-21.005751953125" Y="3.031430419922" />
                  <Point X="-20.88808203125" Y="2.867895507812" />
                  <Point X="-20.79740234375" Y="2.741873291016" />
                  <Point X="-20.67071484375" Y="2.532519287109" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.0185546875" Y="2.124708007812" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583832885742" />
                  <Point X="-21.75591796875" Y="1.537794799805" />
                  <Point X="-21.77841015625" Y="1.508451538086" />
                  <Point X="-21.78687890625" Y="1.491500610352" />
                  <Point X="-21.800025390625" Y="1.444495605469" />
                  <Point X="-21.808404296875" Y="1.414536010742" />
                  <Point X="-21.809220703125" Y="1.390963867188" />
                  <Point X="-21.7984296875" Y="1.338664672852" />
                  <Point X="-21.79155078125" Y="1.305331054688" />
                  <Point X="-21.784353515625" Y="1.287955078125" />
                  <Point X="-21.75500390625" Y="1.24334362793" />
                  <Point X="-21.736296875" Y="1.214909667969" />
                  <Point X="-21.719052734375" Y="1.198819702148" />
                  <Point X="-21.67651953125" Y="1.174877319336" />
                  <Point X="-21.64941015625" Y="1.15961730957" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.57392578125" Y="1.146019165039" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.9723515625" Y="1.213823730469" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.099755859375" Y="1.111354003906" />
                  <Point X="-20.060806640625" Y="0.951366210938" />
                  <Point X="-20.020884765625" Y="0.694956787109" />
                  <Point X="-20.002140625" Y="0.57455645752" />
                  <Point X="-20.4648359375" Y="0.450577270508" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819656372" />
                  <Point X="-21.32741015625" Y="0.200162124634" />
                  <Point X="-21.363421875" Y="0.179347137451" />
                  <Point X="-21.377734375" Y="0.16692578125" />
                  <Point X="-21.411634765625" Y="0.123729782104" />
                  <Point X="-21.433240234375" Y="0.096198104858" />
                  <Point X="-21.443013671875" Y="0.074733894348" />
                  <Point X="-21.454314453125" Y="0.015730275154" />
                  <Point X="-21.461515625" Y="-0.02187666893" />
                  <Point X="-21.461515625" Y="-0.040685844421" />
                  <Point X="-21.450216796875" Y="-0.099689460754" />
                  <Point X="-21.443013671875" Y="-0.137296401978" />
                  <Point X="-21.433240234375" Y="-0.158758178711" />
                  <Point X="-21.39933984375" Y="-0.201954025269" />
                  <Point X="-21.377734375" Y="-0.229485855103" />
                  <Point X="-21.363421875" Y="-0.24190675354" />
                  <Point X="-21.306921875" Y="-0.274564300537" />
                  <Point X="-21.270912109375" Y="-0.295379119873" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.752763671875" Y="-0.435987335205" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.029822265625" Y="-0.822173095703" />
                  <Point X="-20.051568359375" Y="-0.966414001465" />
                  <Point X="-20.102716796875" Y="-1.190546020508" />
                  <Point X="-20.125453125" Y="-1.290178588867" />
                  <Point X="-20.66720703125" Y="-1.21885534668" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.71605078125" Y="-1.122443359375" />
                  <Point X="-21.7867265625" Y="-1.137805053711" />
                  <Point X="-21.8145546875" Y="-1.154697631836" />
                  <Point X="-21.881580078125" Y="-1.235307373047" />
                  <Point X="-21.924298828125" Y="-1.286685546875" />
                  <Point X="-21.935640625" Y="-1.31407019043" />
                  <Point X="-21.94524609375" Y="-1.418463745117" />
                  <Point X="-21.951369140625" Y="-1.485000610352" />
                  <Point X="-21.943638671875" Y="-1.516621826172" />
                  <Point X="-21.882271484375" Y="-1.61207409668" />
                  <Point X="-21.843158203125" Y="-1.672912353516" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-21.361904296875" Y="-2.045903076172" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.734572265625" Y="-2.702956298828" />
                  <Point X="-20.795865234375" Y="-2.802138183594" />
                  <Point X="-20.9016484375" Y="-2.952441650391" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.4264375" Y="-2.732704589844" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.3946328125" Y="-2.229478271484" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219245117188" />
                  <Point X="-22.620560546875" Y="-2.276946777344" />
                  <Point X="-22.690439453125" Y="-2.313724121094" />
                  <Point X="-22.7113984375" Y="-2.33468359375" />
                  <Point X="-22.7691015625" Y="-2.444321777344" />
                  <Point X="-22.80587890625" Y="-2.514201660156" />
                  <Point X="-22.8108359375" Y="-2.546374511719" />
                  <Point X="-22.787" Y="-2.678348632812" />
                  <Point X="-22.77180859375" Y="-2.76246484375" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.46412109375" Y="-3.301288330078" />
                  <Point X="-22.01332421875" Y="-4.082087890625" />
                  <Point X="-22.09196875" Y="-4.138260742187" />
                  <Point X="-22.164720703125" Y="-4.190224609375" />
                  <Point X="-22.28296875" Y="-4.266766113281" />
                  <Point X="-22.32022265625" Y="-4.290879882812" />
                  <Point X="-22.69100390625" Y="-3.807667724609" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467529297" />
                  <Point X="-23.45961328125" Y="-2.89678515625" />
                  <Point X="-23.54257421875" Y="-2.843448730469" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.716548828125" Y="-2.848816650391" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868508789062" />
                  <Point X="-23.944587890625" Y="-2.959905761719" />
                  <Point X="-24.014650390625" Y="-3.018159179688" />
                  <Point X="-24.03154296875" Y="-3.045985595703" />
                  <Point X="-24.064408203125" Y="-3.197196533203" />
                  <Point X="-24.085357421875" Y="-3.293573486328" />
                  <Point X="-24.0860703125" Y="-3.310719970703" />
                  <Point X="-24.000544921875" Y="-3.960339355469" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.93684375" Y="-4.9481640625" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.114912109375" Y="-4.983095214844" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#178" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.118021454921" Y="4.795586562198" Z="1.55" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.55" />
                  <Point X="-0.501043024775" Y="5.040299717792" Z="1.55" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.55" />
                  <Point X="-1.282357687583" Y="4.900121759192" Z="1.55" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.55" />
                  <Point X="-1.724336386163" Y="4.56995757033" Z="1.55" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.55" />
                  <Point X="-1.720211036827" Y="4.403329113849" Z="1.55" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.55" />
                  <Point X="-1.778529953687" Y="4.324813330738" Z="1.55" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.55" />
                  <Point X="-1.876163249083" Y="4.319019117196" Z="1.55" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.55" />
                  <Point X="-2.056446899308" Y="4.508456521585" Z="1.55" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.55" />
                  <Point X="-2.388183621896" Y="4.46884544635" Z="1.55" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.55" />
                  <Point X="-3.017028955026" Y="4.070812093102" Z="1.55" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.55" />
                  <Point X="-3.148333406096" Y="3.39459305617" Z="1.55" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.55" />
                  <Point X="-2.998611174216" Y="3.107011645774" Z="1.55" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.55" />
                  <Point X="-3.017677487723" Y="3.031126080164" Z="1.55" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.55" />
                  <Point X="-3.088064819832" Y="2.996953524957" Z="1.55" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.55" />
                  <Point X="-3.539266292127" Y="3.23186054265" Z="1.55" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.55" />
                  <Point X="-3.954751861485" Y="3.171462395404" Z="1.55" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.55" />
                  <Point X="-4.340016845627" Y="2.619632240479" Z="1.55" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.55" />
                  <Point X="-4.027861517212" Y="1.865048931163" Z="1.55" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.55" />
                  <Point X="-3.684985641435" Y="1.588595583434" Z="1.55" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.55" />
                  <Point X="-3.676416802243" Y="1.530541524281" Z="1.55" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.55" />
                  <Point X="-3.71538088746" Y="1.486661014917" Z="1.55" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.55" />
                  <Point X="-4.402475017386" Y="1.56035125137" Z="1.55" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.55" />
                  <Point X="-4.877351399051" Y="1.390282691317" Z="1.55" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.55" />
                  <Point X="-5.006889904506" Y="0.807766176404" Z="1.55" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.55" />
                  <Point X="-4.154137206738" Y="0.203830183435" Z="1.55" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.55" />
                  <Point X="-3.565757546443" Y="0.041571001793" Z="1.55" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.55" />
                  <Point X="-3.545206734462" Y="0.018204184908" Z="1.55" />
                  <Point X="-3.539556741714" Y="0" Z="1.55" />
                  <Point X="-3.543157866673" Y="-0.011602766153" Z="1.55" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.55" />
                  <Point X="-3.559611048681" Y="-0.037304981341" Z="1.55" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.55" />
                  <Point X="-4.482750582034" Y="-0.291881873321" Z="1.55" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.55" />
                  <Point X="-5.030094754285" Y="-0.658024069008" Z="1.55" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.55" />
                  <Point X="-4.929815897462" Y="-1.196560254025" Z="1.55" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.55" />
                  <Point X="-3.85278082431" Y="-1.390281202411" Z="1.55" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.55" />
                  <Point X="-3.208850178109" Y="-1.312930565919" Z="1.55" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.55" />
                  <Point X="-3.195675387082" Y="-1.334029375552" Z="1.55" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.55" />
                  <Point X="-3.995876832783" Y="-1.962602717623" Z="1.55" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.55" />
                  <Point X="-4.38863388554" Y="-2.543263560259" Z="1.55" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.55" />
                  <Point X="-4.074235197089" Y="-3.021406532522" Z="1.55" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.55" />
                  <Point X="-3.074755964804" Y="-2.845272573507" Z="1.55" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.55" />
                  <Point X="-2.566086345136" Y="-2.562244123995" Z="1.55" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.55" />
                  <Point X="-3.010144439661" Y="-3.360322417688" Z="1.55" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.55" />
                  <Point X="-3.14054178508" Y="-3.984959453628" Z="1.55" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.55" />
                  <Point X="-2.719449454302" Y="-4.283397243072" Z="1.55" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.55" />
                  <Point X="-2.313765938117" Y="-4.270541259882" Z="1.55" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.55" />
                  <Point X="-2.125805349915" Y="-4.089355634877" Z="1.55" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.55" />
                  <Point X="-1.847744157858" Y="-3.99198313957" Z="1.55" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.55" />
                  <Point X="-1.56786684341" Y="-4.084005510939" Z="1.55" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.55" />
                  <Point X="-1.401844838139" Y="-4.327390250207" Z="1.55" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.55" />
                  <Point X="-1.394328550946" Y="-4.736927212299" Z="1.55" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.55" />
                  <Point X="-1.297994838808" Y="-4.909118563218" Z="1.55" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.55" />
                  <Point X="-1.000726345569" Y="-4.978230696624" Z="1.55" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.55" />
                  <Point X="-0.573018567347" Y="-4.100718473139" Z="1.55" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.55" />
                  <Point X="-0.35335351798" Y="-3.426945598979" Z="1.55" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.55" />
                  <Point X="-0.154735568874" Y="-3.252263578741" Z="1.55" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.55" />
                  <Point X="0.098623510487" Y="-3.234848466547" Z="1.55" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.55" />
                  <Point X="0.317092341452" Y="-3.374700156789" Z="1.55" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.55" />
                  <Point X="0.661736293054" Y="-4.431817556677" Z="1.55" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.55" />
                  <Point X="0.887868717649" Y="-5.00101005716" Z="1.55" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.55" />
                  <Point X="1.067705040214" Y="-4.965724299135" Z="1.55" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.55" />
                  <Point X="1.042869833251" Y="-3.922533267899" Z="1.55" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.55" />
                  <Point X="0.978293714694" Y="-3.176536780252" Z="1.55" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.55" />
                  <Point X="1.081221163383" Y="-2.967072466998" Z="1.55" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.55" />
                  <Point X="1.281875960835" Y="-2.867326179368" Z="1.55" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.55" />
                  <Point X="1.507191690448" Y="-2.907562997413" Z="1.55" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.55" />
                  <Point X="2.263170898313" Y="-3.806825698108" Z="1.55" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.55" />
                  <Point X="2.738041433093" Y="-4.277460674389" Z="1.55" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.55" />
                  <Point X="2.93093747506" Y="-4.147667598255" Z="1.55" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.55" />
                  <Point X="2.573023641388" Y="-3.245008378544" Z="1.55" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.55" />
                  <Point X="2.256045572873" Y="-2.638182442426" Z="1.55" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.55" />
                  <Point X="2.26898839744" Y="-2.43632833393" Z="1.55" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.55" />
                  <Point X="2.396570044443" Y="-2.289912912527" Z="1.55" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.55" />
                  <Point X="2.590324361087" Y="-2.247402436618" Z="1.55" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.55" />
                  <Point X="3.542405218563" Y="-2.744725858887" Z="1.55" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.55" />
                  <Point X="4.133083261921" Y="-2.949939030082" Z="1.55" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.55" />
                  <Point X="4.301804487935" Y="-2.697961302153" Z="1.55" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.55" />
                  <Point X="3.662376219975" Y="-1.974955456041" Z="1.55" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.55" />
                  <Point X="3.153629477095" Y="-1.553754787591" Z="1.55" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.55" />
                  <Point X="3.098385217061" Y="-1.391765537903" Z="1.55" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.55" />
                  <Point X="3.150710848212" Y="-1.235994066061" Z="1.55" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.55" />
                  <Point X="3.288411945016" Y="-1.140022371516" Z="1.55" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.55" />
                  <Point X="4.320111075638" Y="-1.237147511655" Z="1.55" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.55" />
                  <Point X="4.939872922308" Y="-1.170389692535" Z="1.55" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.55" />
                  <Point X="5.0134616596" Y="-0.79834708072" Z="1.55" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.55" />
                  <Point X="4.254019898193" Y="-0.356410927814" Z="1.55" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.55" />
                  <Point X="3.711941914116" Y="-0.199995757355" Z="1.55" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.55" />
                  <Point X="3.633836243364" Y="-0.139806485476" Z="1.55" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.55" />
                  <Point X="3.5927345666" Y="-0.05900355159" Z="1.55" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.55" />
                  <Point X="3.588636883824" Y="0.037606979612" Z="1.55" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.55" />
                  <Point X="3.621543195037" Y="0.124142253099" Z="1.55" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.55" />
                  <Point X="3.691453500237" Y="0.188153086887" Z="1.55" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.55" />
                  <Point X="4.541947919114" Y="0.433561004955" Z="1.55" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.55" />
                  <Point X="5.022361765904" Y="0.733928586454" Z="1.55" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.55" />
                  <Point X="4.942669791251" Y="1.154460949136" Z="1.55" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.55" />
                  <Point X="4.014966667806" Y="1.294675911422" Z="1.55" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.55" />
                  <Point X="3.42646802802" Y="1.226868274631" Z="1.55" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.55" />
                  <Point X="3.341851485414" Y="1.249728673451" Z="1.55" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.55" />
                  <Point X="3.280611486515" Y="1.302104948285" Z="1.55" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.55" />
                  <Point X="3.244383108912" Y="1.380050150967" Z="1.55" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.55" />
                  <Point X="3.241970528283" Y="1.462308695524" Z="1.55" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.55" />
                  <Point X="3.277608492453" Y="1.538656949862" Z="1.55" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.55" />
                  <Point X="4.005725716726" Y="2.116320365526" Z="1.55" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.55" />
                  <Point X="4.365905962201" Y="2.58968546636" Z="1.55" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.55" />
                  <Point X="4.146347771355" Y="2.928378884458" Z="1.55" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.55" />
                  <Point X="3.090808647623" Y="2.602399260608" Z="1.55" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.55" />
                  <Point X="2.478625386154" Y="2.258641433115" Z="1.55" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.55" />
                  <Point X="2.402567070001" Y="2.248787839295" Z="1.55" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.55" />
                  <Point X="2.335522998684" Y="2.270622343624" Z="1.55" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.55" />
                  <Point X="2.280136256611" Y="2.321501861698" Z="1.55" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.55" />
                  <Point X="2.250641863464" Y="2.387191373832" Z="1.55" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.55" />
                  <Point X="2.253886509543" Y="2.460844189727" Z="1.55" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.55" />
                  <Point X="2.793226092252" Y="3.421330434332" Z="1.55" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.55" />
                  <Point X="2.982602554955" Y="4.106105384685" Z="1.55" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.55" />
                  <Point X="2.598673966999" Y="4.359232747806" Z="1.55" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.55" />
                  <Point X="2.195490571222" Y="4.575707326365" Z="1.55" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.55" />
                  <Point X="1.777701298091" Y="4.753635544382" Z="1.55" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.55" />
                  <Point X="1.262089428417" Y="4.909768926881" Z="1.55" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.55" />
                  <Point X="0.602018932247" Y="5.033513438341" Z="1.55" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.55" />
                  <Point X="0.075223106632" Y="4.635860943753" Z="1.55" />
                  <Point X="0" Y="4.355124473572" Z="1.55" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>