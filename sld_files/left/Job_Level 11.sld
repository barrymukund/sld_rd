<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#137" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="940" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000473632812" Y="0.004715576172" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.357640625" Y="-3.807561279297" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140136719" />
                  <Point X="-24.45763671875" Y="-3.467376464844" />
                  <Point X="-24.48994921875" Y="-3.420821289063" />
                  <Point X="-24.621365234375" Y="-3.231476074219" />
                  <Point X="-24.643248046875" Y="-3.209020751953" />
                  <Point X="-24.66950390625" Y="-3.189776855469" />
                  <Point X="-24.69750390625" Y="-3.175669433594" />
                  <Point X="-24.74750390625" Y="-3.160151123047" />
                  <Point X="-24.95086328125" Y="-3.097036132812" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097036132812" />
                  <Point X="-25.08682421875" Y="-3.112554443359" />
                  <Point X="-25.29018359375" Y="-3.175669433594" />
                  <Point X="-25.31818359375" Y="-3.189776855469" />
                  <Point X="-25.344439453125" Y="-3.209020751953" />
                  <Point X="-25.36632421875" Y="-3.231478027344" />
                  <Point X="-25.398634765625" Y="-3.278032958984" />
                  <Point X="-25.53005078125" Y="-3.467378417969" />
                  <Point X="-25.5381875" Y="-3.481572021484" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.872517578125" Y="-4.712477539062" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.0793359375" Y="-4.845351074219" />
                  <Point X="-26.133470703125" Y="-4.831422851562" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.224478515625" Y="-4.63572265625" />
                  <Point X="-26.2149609375" Y="-4.563438964844" />
                  <Point X="-26.21419921875" Y="-4.547930175781" />
                  <Point X="-26.2165078125" Y="-4.516223632812" />
                  <Point X="-26.228453125" Y="-4.456171386719" />
                  <Point X="-26.277037109375" Y="-4.211932128906" />
                  <Point X="-26.287939453125" Y="-4.182964355469" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131205078125" />
                  <Point X="-26.369677734375" Y="-4.090833740234" />
                  <Point X="-26.556904296875" Y="-3.926639892578" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.704125" Y="-3.886961669922" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708740234" />
                  <Point X="-27.014466796875" Y="-3.882028564453" />
                  <Point X="-27.042658203125" Y="-3.894801513672" />
                  <Point X="-27.093568359375" Y="-3.928818359375" />
                  <Point X="-27.300623046875" Y="-4.067169433594" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.3351015625" Y="-4.099461914062" />
                  <Point X="-27.4801484375" Y="-4.288489746094" />
                  <Point X="-27.8017109375" Y="-4.089386230469" />
                  <Point X="-27.876662109375" Y="-4.031675537109" />
                  <Point X="-28.10472265625" Y="-3.856077392578" />
                  <Point X="-27.57067578125" Y="-2.931083007812" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647655273438" />
                  <Point X="-27.406587890625" Y="-2.616129882812" />
                  <Point X="-27.40557421875" Y="-2.585197753906" />
                  <Point X="-27.414556640625" Y="-2.555581298828" />
                  <Point X="-27.428771484375" Y="-2.526752685547" />
                  <Point X="-27.446798828125" Y="-2.501592285156" />
                  <Point X="-27.464146484375" Y="-2.484243652344" />
                  <Point X="-27.4893046875" Y="-2.46621484375" />
                  <Point X="-27.518134765625" Y="-2.451996826172" />
                  <Point X="-27.547751953125" Y="-2.443011962891" />
                  <Point X="-27.5786875" Y="-2.444024169922" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.674119140625" Y="-3.058717773438" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-29.082857421875" Y="-2.793860839844" />
                  <Point X="-29.13659375" Y="-2.703756347656" />
                  <Point X="-29.306142578125" Y="-2.419449951172" />
                  <Point X="-28.3630078125" Y="-1.695756958008" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.08306640625" Y="-1.475880493164" />
                  <Point X="-28.0641328125" Y="-1.445860961914" />
                  <Point X="-28.05589453125" Y="-1.424918212891" />
                  <Point X="-28.052333984375" Y="-1.413959350586" />
                  <Point X="-28.046150390625" Y="-1.390082641602" />
                  <Point X="-28.042037109375" Y="-1.358597900391" />
                  <Point X="-28.04273828125" Y="-1.335732788086" />
                  <Point X="-28.0488828125" Y="-1.313697631836" />
                  <Point X="-28.06088671875" Y="-1.284715698242" />
                  <Point X="-28.073388671875" Y="-1.26310534668" />
                  <Point X="-28.091087890625" Y="-1.245498046875" />
                  <Point X="-28.10881640625" Y="-1.231992797852" />
                  <Point X="-28.118197265625" Y="-1.22569152832" />
                  <Point X="-28.139453125" Y="-1.213180908203" />
                  <Point X="-28.168716796875" Y="-1.201956787109" />
                  <Point X="-28.200603515625" Y="-1.195474975586" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-29.538435546875" Y="-1.366388916016" />
                  <Point X="-29.7321015625" Y="-1.391885498047" />
                  <Point X="-29.834078125" Y="-0.992646728516" />
                  <Point X="-29.84829296875" Y="-0.893254882812" />
                  <Point X="-29.892421875" Y="-0.584698364258" />
                  <Point X="-28.825697265625" Y="-0.298869781494" />
                  <Point X="-28.532875" Y="-0.220408279419" />
                  <Point X="-28.51338671875" Y="-0.212802215576" />
                  <Point X="-28.49234375" Y="-0.201781326294" />
                  <Point X="-28.482255859375" Y="-0.195670394897" />
                  <Point X="-28.459978515625" Y="-0.180209625244" />
                  <Point X="-28.436013671875" Y="-0.158673614502" />
                  <Point X="-28.41563671875" Y="-0.129513626099" />
                  <Point X="-28.40638671875" Y="-0.108886680603" />
                  <Point X="-28.40233984375" Y="-0.098174682617" />
                  <Point X="-28.39491796875" Y="-0.074261932373" />
                  <Point X="-28.389474609375" Y="-0.045538040161" />
                  <Point X="-28.390185546875" Y="-0.01295730114" />
                  <Point X="-28.3942578125" Y="0.007752975464" />
                  <Point X="-28.3967421875" Y="0.017585195541" />
                  <Point X="-28.40416796875" Y="0.041509784698" />
                  <Point X="-28.417482421875" Y="0.070831130981" />
                  <Point X="-28.439341796875" Y="0.099208946228" />
                  <Point X="-28.45663671875" Y="0.114524894714" />
                  <Point X="-28.465451171875" Y="0.121447898865" />
                  <Point X="-28.4877265625" Y="0.136908508301" />
                  <Point X="-28.501923828125" Y="0.145046447754" />
                  <Point X="-28.532875" Y="0.157848297119" />
                  <Point X="-29.723818359375" Y="0.47696081543" />
                  <Point X="-29.89181640625" Y="0.521975524902" />
                  <Point X="-29.82448828125" Y="0.976967102051" />
                  <Point X="-29.7958671875" Y="1.08258605957" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-28.97556640625" Y="1.327427124023" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228149414" />
                  <Point X="-28.7031328125" Y="1.305264282227" />
                  <Point X="-28.69101171875" Y="1.309086425781" />
                  <Point X="-28.64170703125" Y="1.324631835938" />
                  <Point X="-28.62277734375" Y="1.332961914062" />
                  <Point X="-28.604033203125" Y="1.343783935547" />
                  <Point X="-28.5873515625" Y="1.356015625" />
                  <Point X="-28.573712890625" Y="1.371567993164" />
                  <Point X="-28.561298828125" Y="1.389297607422" />
                  <Point X="-28.55134765625" Y="1.407436401367" />
                  <Point X="-28.546484375" Y="1.41917956543" />
                  <Point X="-28.526701171875" Y="1.466940917969" />
                  <Point X="-28.520916015625" Y="1.486788085938" />
                  <Point X="-28.51715625" Y="1.508103271484" />
                  <Point X="-28.515802734375" Y="1.52874987793" />
                  <Point X="-28.518951171875" Y="1.549199829102" />
                  <Point X="-28.5245546875" Y="1.570106445312" />
                  <Point X="-28.53205078125" Y="1.589378173828" />
                  <Point X="-28.537919921875" Y="1.600652709961" />
                  <Point X="-28.561791015625" Y="1.64650793457" />
                  <Point X="-28.57328125" Y="1.663706054688" />
                  <Point X="-28.587193359375" Y="1.680286132812" />
                  <Point X="-28.60213671875" Y="1.694590454102" />
                  <Point X="-29.285263671875" Y="2.218773193359" />
                  <Point X="-29.351859375" Y="2.269873779297" />
                  <Point X="-29.349173828125" Y="2.274474609375" />
                  <Point X="-29.081146484375" Y="2.733665771484" />
                  <Point X="-29.005345703125" Y="2.831098876953" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.335572265625" Y="2.919100585938" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.12991015625" Y="2.824319335938" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.999013671875" Y="2.826504638672" />
                  <Point X="-27.980462890625" Y="2.835653076172" />
                  <Point X="-27.962208984375" Y="2.847281982422" />
                  <Point X="-27.946078125" Y="2.860227783203" />
                  <Point X="-27.93409375" Y="2.872211669922" />
                  <Point X="-27.885353515625" Y="2.920951171875" />
                  <Point X="-27.872404296875" Y="2.9370859375" />
                  <Point X="-27.860775390625" Y="2.955340576172" />
                  <Point X="-27.85162890625" Y="2.973889160156" />
                  <Point X="-27.8467109375" Y="2.993977783203" />
                  <Point X="-27.843884765625" Y="3.015436767578" />
                  <Point X="-27.84343359375" Y="3.036118164062" />
                  <Point X="-27.84491015625" Y="3.053001464844" />
                  <Point X="-27.85091796875" Y="3.121667724609" />
                  <Point X="-27.854955078125" Y="3.141963378906" />
                  <Point X="-27.86146484375" Y="3.16260546875" />
                  <Point X="-27.869794921875" Y="3.181532958984" />
                  <Point X="-28.172509765625" Y="3.705850585938" />
                  <Point X="-28.18333203125" Y="3.724596435547" />
                  <Point X="-28.1674296875" Y="3.736789306641" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.581234375" Y="4.161015136719" />
                  <Point X="-27.16703515625" Y="4.391134277344" />
                  <Point X="-27.08266015625" Y="4.281173339844" />
                  <Point X="-27.0431953125" Y="4.229741210938" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.995111328125" Y="4.189394042969" />
                  <Point X="-26.9763203125" Y="4.179612304688" />
                  <Point X="-26.89989453125" Y="4.139827148438" />
                  <Point X="-26.8806171875" Y="4.132331054688" />
                  <Point X="-26.8597109375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.12358203125" />
                  <Point X="-26.81862890625" Y="4.124934570312" />
                  <Point X="-26.7973125" Y="4.128692871094" />
                  <Point X="-26.777447265625" Y="4.134483398438" />
                  <Point X="-26.757876953125" Y="4.142590820312" />
                  <Point X="-26.6782734375" Y="4.175562988281" />
                  <Point X="-26.660140625" Y="4.185512207031" />
                  <Point X="-26.642412109375" Y="4.197926757812" />
                  <Point X="-26.62686328125" Y="4.211563476563" />
                  <Point X="-26.6146328125" Y="4.2282421875" />
                  <Point X="-26.603810546875" Y="4.246985839844" />
                  <Point X="-26.5954765625" Y="4.26592578125" />
                  <Point X="-26.589107421875" Y="4.286129882813" />
                  <Point X="-26.563197265625" Y="4.368302246094" />
                  <Point X="-26.5591640625" Y="4.388583007812" />
                  <Point X="-26.55727734375" Y="4.410145019531" />
                  <Point X="-26.557728515625" Y="4.430826660156" />
                  <Point X="-26.584201171875" Y="4.6318984375" />
                  <Point X="-26.55394140625" Y="4.640382324219" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.80490234375" Y="4.826747070312" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.172041015625" Y="4.42864453125" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183885742188" />
                  <Point X="-24.932869140625" Y="4.194217773438" />
                  <Point X="-24.905576171875" Y="4.208806640625" />
                  <Point X="-24.88441796875" Y="4.23139453125" />
                  <Point X="-24.86655859375" Y="4.25812109375" />
                  <Point X="-24.853783203125" Y="4.286314941406" />
                  <Point X="-24.69867578125" Y="4.865185058594" />
                  <Point X="-24.692578125" Y="4.887937011719" />
                  <Point X="-24.683611328125" Y="4.886998046875" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-24.0362109375" Y="4.802828613281" />
                  <Point X="-23.5189765625" Y="4.677952148438" />
                  <Point X="-23.442220703125" Y="4.650112304688" />
                  <Point X="-23.105349609375" Y="4.527926269531" />
                  <Point X="-23.029982421875" Y="4.4926796875" />
                  <Point X="-22.705427734375" Y="4.340895996094" />
                  <Point X="-22.632580078125" Y="4.298455078125" />
                  <Point X="-22.319015625" Y="4.115770996094" />
                  <Point X="-22.25034375" Y="4.066937255859" />
                  <Point X="-22.056736328125" Y="3.929254638672" />
                  <Point X="-22.682373046875" Y="2.845620361328" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.8603359375" Y="2.533438964844" />
                  <Point X="-22.869576171875" Y="2.505511230469" />
                  <Point X="-22.87116015625" Y="2.500211914062" />
                  <Point X="-22.888392578125" Y="2.435770019531" />
                  <Point X="-22.89161328125" Y="2.410312011719" />
                  <Point X="-22.891298828125" Y="2.377708984375" />
                  <Point X="-22.890619140625" Y="2.367253173828" />
                  <Point X="-22.883900390625" Y="2.311529296875" />
                  <Point X="-22.87855859375" Y="2.289607421875" />
                  <Point X="-22.87029296875" Y="2.267518310547" />
                  <Point X="-22.859927734375" Y="2.247467285156" />
                  <Point X="-22.85144921875" Y="2.234973388672" />
                  <Point X="-22.81696875" Y="2.184158691406" />
                  <Point X="-22.79964453125" Y="2.1649140625" />
                  <Point X="-22.773849609375" Y="2.143136474609" />
                  <Point X="-22.76590625" Y="2.137114746094" />
                  <Point X="-22.715091796875" Y="2.102635009766" />
                  <Point X="-22.695046875" Y="2.092272705078" />
                  <Point X="-22.672958984375" Y="2.084006591797" />
                  <Point X="-22.65103515625" Y="2.078663330078" />
                  <Point X="-22.637333984375" Y="2.077011230469" />
                  <Point X="-22.581611328125" Y="2.070291748047" />
                  <Point X="-22.555189453125" Y="2.070808105469" />
                  <Point X="-22.52044140625" Y="2.0763828125" />
                  <Point X="-22.510947265625" Y="2.078408203125" />
                  <Point X="-22.446505859375" Y="2.095640869141" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.213603515625" Y="2.801730224609" />
                  <Point X="-21.032671875" Y="2.906191162109" />
                  <Point X="-20.876724609375" Y="2.689457763672" />
                  <Point X="-20.83844140625" Y="2.6261953125" />
                  <Point X="-20.73780078125" Y="2.459883789063" />
                  <Point X="-21.545337890625" Y="1.84023828125" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.78355859375" Y="1.654854492188" />
                  <Point X="-21.80420703125" Y="1.630728393555" />
                  <Point X="-21.8074296875" Y="1.626751342773" />
                  <Point X="-21.85380859375" Y="1.566246459961" />
                  <Point X="-21.86674609375" Y="1.543403930664" />
                  <Point X="-21.879462890625" Y="1.511263916016" />
                  <Point X="-21.882615234375" Y="1.501899536133" />
                  <Point X="-21.899892578125" Y="1.440124023438" />
                  <Point X="-21.90334765625" Y="1.417824829102" />
                  <Point X="-21.9041640625" Y="1.394254272461" />
                  <Point X="-21.902259765625" Y="1.371761474609" />
                  <Point X="-21.898771484375" Y="1.354861694336" />
                  <Point X="-21.88458984375" Y="1.286128540039" />
                  <Point X="-21.875673828125" Y="1.261193237305" />
                  <Point X="-21.8589921875" Y="1.229400634766" />
                  <Point X="-21.854234375" Y="1.221326782227" />
                  <Point X="-21.815662109375" Y="1.162696655273" />
                  <Point X="-21.801107421875" Y="1.145451049805" />
                  <Point X="-21.78386328125" Y="1.129360717773" />
                  <Point X="-21.76565234375" Y="1.116034301758" />
                  <Point X="-21.751908203125" Y="1.108297729492" />
                  <Point X="-21.696009765625" Y="1.07683203125" />
                  <Point X="-21.6708125" Y="1.067059692383" />
                  <Point X="-21.634251953125" Y="1.058605834961" />
                  <Point X="-21.625296875" Y="1.056982788086" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.373908203125" Y="1.196790283203" />
                  <Point X="-20.22316015625" Y="1.21663659668" />
                  <Point X="-20.15405859375" Y="0.932790527344" />
                  <Point X="-20.141998046875" Y="0.855320251465" />
                  <Point X="-20.109134765625" Y="0.644238708496" />
                  <Point X="-21.0268515625" Y="0.398336883545" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.295208984375" Y="0.32558605957" />
                  <Point X="-21.318455078125" Y="0.315066680908" />
                  <Point X="-21.3367109375" Y="0.304513763428" />
                  <Point X="-21.41096484375" Y="0.261594268799" />
                  <Point X="-21.43202734375" Y="0.245061416626" />
                  <Point X="-21.457294921875" Y="0.218682525635" />
                  <Point X="-21.463423828125" Y="0.211617294312" />
                  <Point X="-21.507974609375" Y="0.154847854614" />
                  <Point X="-21.51969921875" Y="0.135566680908" />
                  <Point X="-21.52947265625" Y="0.114102615356" />
                  <Point X="-21.53631640625" Y="0.092607887268" />
                  <Point X="-21.53996875" Y="0.073541801453" />
                  <Point X="-21.554818359375" Y="-0.004002580643" />
                  <Point X="-21.556078125" Y="-0.030989843369" />
                  <Point X="-21.552427734375" Y="-0.068865180969" />
                  <Point X="-21.551169921875" Y="-0.077621208191" />
                  <Point X="-21.536318359375" Y="-0.155165740967" />
                  <Point X="-21.52947265625" Y="-0.176664718628" />
                  <Point X="-21.51969921875" Y="-0.198128326416" />
                  <Point X="-21.5079765625" Y="-0.217406005859" />
                  <Point X="-21.4970234375" Y="-0.231364196777" />
                  <Point X="-21.452470703125" Y="-0.288133636475" />
                  <Point X="-21.439998046875" Y="-0.301239074707" />
                  <Point X="-21.4109609375" Y="-0.324156219482" />
                  <Point X="-21.392703125" Y="-0.334709136963" />
                  <Point X="-21.318451171875" Y="-0.37762878418" />
                  <Point X="-21.307291015625" Y="-0.383137695312" />
                  <Point X="-21.283419921875" Y="-0.392149810791" />
                  <Point X="-20.23992578125" Y="-0.671753417969" />
                  <Point X="-20.108525390625" Y="-0.706961853027" />
                  <Point X="-20.144974609375" Y="-0.948726135254" />
                  <Point X="-20.160431640625" Y="-1.016461120605" />
                  <Point X="-20.198822265625" Y="-1.18469921875" />
                  <Point X="-21.2780703125" Y="-1.042613647461" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710266113" />
                  <Point X="-21.62533984375" Y="-1.005508666992" />
                  <Point X="-21.661171875" Y="-1.013296875" />
                  <Point X="-21.806904296875" Y="-1.044972412109" />
                  <Point X="-21.836025390625" Y="-1.056597412109" />
                  <Point X="-21.8638515625" Y="-1.073489868164" />
                  <Point X="-21.887599609375" Y="-1.093959960937" />
                  <Point X="-21.9092578125" Y="-1.12000793457" />
                  <Point X="-21.99734375" Y="-1.225947998047" />
                  <Point X="-22.012064453125" Y="-1.250329223633" />
                  <Point X="-22.023408203125" Y="-1.277714355469" />
                  <Point X="-22.030240234375" Y="-1.3053671875" />
                  <Point X="-22.03334375" Y="-1.339100341797" />
                  <Point X="-22.04596875" Y="-1.476297363281" />
                  <Point X="-22.04365234375" Y="-1.507561401367" />
                  <Point X="-22.035921875" Y="-1.539182373047" />
                  <Point X="-22.023548828125" Y="-1.567996948242" />
                  <Point X="-22.00371875" Y="-1.598841064453" />
                  <Point X="-21.923068359375" Y="-1.724287475586" />
                  <Point X="-21.9130625" Y="-1.737241943359" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-20.921" Y="-2.503965332031" />
                  <Point X="-20.786876953125" Y="-2.606882324219" />
                  <Point X="-20.8751875" Y="-2.749784179688" />
                  <Point X="-20.90715625" Y="-2.795206298828" />
                  <Point X="-20.971017578125" Y="-2.885945068359" />
                  <Point X="-21.9340390625" Y="-2.329944091797" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.2457734375" Y="-2.159825195312" />
                  <Point X="-22.28841796875" Y="-2.152123535156" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135177001953" />
                  <Point X="-22.59059375" Y="-2.153822509766" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.757615234375" Y="-2.246549804688" />
                  <Point X="-22.77857421875" Y="-2.267509521484" />
                  <Point X="-22.79546484375" Y="-2.290437255859" />
                  <Point X="-22.814111328125" Y="-2.325865234375" />
                  <Point X="-22.8899453125" Y="-2.469955322266" />
                  <Point X="-22.899771484375" Y="-2.499735351563" />
                  <Point X="-22.904728515625" Y="-2.531908203125" />
                  <Point X="-22.904322265625" Y="-2.563261230469" />
                  <Point X="-22.896619140625" Y="-2.605906738281" />
                  <Point X="-22.865294921875" Y="-2.7793515625" />
                  <Point X="-22.861013671875" Y="-2.795139160156" />
                  <Point X="-22.848177734375" Y="-2.826078857422" />
                  <Point X="-22.225904296875" Y="-3.903889892578" />
                  <Point X="-22.13871484375" Y="-4.054904785156" />
                  <Point X="-22.218158203125" Y="-4.111648925781" />
                  <Point X="-22.253888671875" Y="-4.13477734375" />
                  <Point X="-22.298232421875" Y="-4.163480957031" />
                  <Point X="-23.039228515625" Y="-3.197797851562" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900556884766" />
                  <Point X="-23.320134765625" Y="-2.873516113281" />
                  <Point X="-23.49119921875" Y="-2.763538085938" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.628900390625" Y="-2.745349609375" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-23.930921875" Y="-2.824994628906" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968860839844" />
                  <Point X="-24.11275" Y="-2.996687255859" />
                  <Point X="-24.124375" Y="-3.025810302734" />
                  <Point X="-24.134994140625" Y="-3.074671875" />
                  <Point X="-24.178189453125" Y="-3.273398193359" />
                  <Point X="-24.180275390625" Y="-3.289627197266" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.003908203125" Y="-4.662614746094" />
                  <Point X="-23.97793359375" Y="-4.859915527344" />
                  <Point X="-24.024310546875" Y="-4.870081054688" />
                  <Point X="-24.057341796875" Y="-4.87608203125" />
                  <Point X="-24.07068359375" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.058427734375" Y="-4.752636230469" />
                  <Point X="-26.109798828125" Y="-4.739419433594" />
                  <Point X="-26.141244140625" Y="-4.731328613281" />
                  <Point X="-26.130291015625" Y="-4.648122070312" />
                  <Point X="-26.1207734375" Y="-4.575838378906" />
                  <Point X="-26.120076171875" Y="-4.568099121094" />
                  <Point X="-26.11944921875" Y="-4.54103125" />
                  <Point X="-26.1217578125" Y="-4.509324707031" />
                  <Point X="-26.123333984375" Y="-4.497689941406" />
                  <Point X="-26.135279296875" Y="-4.437637695313" />
                  <Point X="-26.18386328125" Y="-4.1933984375" />
                  <Point X="-26.188125" Y="-4.178469238281" />
                  <Point X="-26.19902734375" Y="-4.149501464844" />
                  <Point X="-26.20566796875" Y="-4.135462890625" />
                  <Point X="-26.221740234375" Y="-4.107625976562" />
                  <Point X="-26.230576171875" Y="-4.094859619141" />
                  <Point X="-26.250208984375" Y="-4.070937255859" />
                  <Point X="-26.261005859375" Y="-4.05978125" />
                  <Point X="-26.3070390625" Y="-4.019409912109" />
                  <Point X="-26.494265625" Y="-3.855216064453" />
                  <Point X="-26.506736328125" Y="-3.845966308594" />
                  <Point X="-26.53301953125" Y="-3.829621826172" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.697912109375" Y="-3.792165039062" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166259766" />
                  <Point X="-27.008005859375" Y="-3.781946044922" />
                  <Point X="-27.0390546875" Y="-3.790265869141" />
                  <Point X="-27.053673828125" Y="-3.79549609375" />
                  <Point X="-27.081865234375" Y="-3.808269042969" />
                  <Point X="-27.0954375" Y="-3.815811767578" />
                  <Point X="-27.14634765625" Y="-3.849828613281" />
                  <Point X="-27.35340234375" Y="-3.9881796875" />
                  <Point X="-27.359685546875" Y="-3.992760253906" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032770507812" />
                  <Point X="-27.410470703125" Y="-4.041629394531" />
                  <Point X="-27.503203125" Y="-4.162479003906" />
                  <Point X="-27.747591796875" Y="-4.011159179687" />
                  <Point X="-27.818705078125" Y="-3.956403320312" />
                  <Point X="-27.98086328125" Y="-3.831546875" />
                  <Point X="-27.488404296875" Y="-2.978583007812" />
                  <Point X="-27.341490234375" Y="-2.724119628906" />
                  <Point X="-27.3348515625" Y="-2.7100859375" />
                  <Point X="-27.32394921875" Y="-2.681121582031" />
                  <Point X="-27.319685546875" Y="-2.666190917969" />
                  <Point X="-27.3134140625" Y="-2.634665527344" />
                  <Point X="-27.311638671875" Y="-2.619241455078" />
                  <Point X="-27.310625" Y="-2.588309326172" />
                  <Point X="-27.3146640625" Y="-2.557625244141" />
                  <Point X="-27.323646484375" Y="-2.528008789062" />
                  <Point X="-27.3293515625" Y="-2.513568359375" />
                  <Point X="-27.34356640625" Y="-2.484739746094" />
                  <Point X="-27.351546875" Y="-2.471422119141" />
                  <Point X="-27.36957421875" Y="-2.44626171875" />
                  <Point X="-27.37962109375" Y="-2.434418945313" />
                  <Point X="-27.39696875" Y="-2.4170703125" />
                  <Point X="-27.408810546875" Y="-2.407024169922" />
                  <Point X="-27.43396875" Y="-2.388995361328" />
                  <Point X="-27.44728515625" Y="-2.381012695312" />
                  <Point X="-27.476115234375" Y="-2.366794677734" />
                  <Point X="-27.490556640625" Y="-2.361087890625" />
                  <Point X="-27.520173828125" Y="-2.352103027344" />
                  <Point X="-27.550859375" Y="-2.348062744141" />
                  <Point X="-27.581794921875" Y="-2.349074951172" />
                  <Point X="-27.597220703125" Y="-2.350849365234" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.721619140625" Y="-2.9764453125" />
                  <Point X="-28.793087890625" Y="-3.017708007813" />
                  <Point X="-29.004015625" Y="-2.740591064453" />
                  <Point X="-29.055001953125" Y="-2.655096679688" />
                  <Point X="-29.181265625" Y="-2.443373046875" />
                  <Point X="-28.30517578125" Y="-1.771125488281" />
                  <Point X="-28.048123046875" Y="-1.573881958008" />
                  <Point X="-28.039158203125" Y="-1.566064941406" />
                  <Point X="-28.01626953125" Y="-1.543432006836" />
                  <Point X="-28.002712890625" Y="-1.526559936523" />
                  <Point X="-27.983779296875" Y="-1.496540283203" />
                  <Point X="-27.9757265625" Y="-1.480637329102" />
                  <Point X="-27.96748828125" Y="-1.459694580078" />
                  <Point X="-27.9603671875" Y="-1.437776733398" />
                  <Point X="-27.95418359375" Y="-1.413900024414" />
                  <Point X="-27.951951171875" Y="-1.402389160156" />
                  <Point X="-27.947837890625" Y="-1.370904418945" />
                  <Point X="-27.94708203125" Y="-1.355686035156" />
                  <Point X="-27.947783203125" Y="-1.332820922852" />
                  <Point X="-27.951228515625" Y="-1.310215576172" />
                  <Point X="-27.957373046875" Y="-1.288180297852" />
                  <Point X="-27.96111328125" Y="-1.277344726562" />
                  <Point X="-27.9731171875" Y="-1.248362792969" />
                  <Point X="-27.97865625" Y="-1.237143798828" />
                  <Point X="-27.991158203125" Y="-1.215533325195" />
                  <Point X="-28.006388671875" Y="-1.195755493164" />
                  <Point X="-28.024087890625" Y="-1.178148193359" />
                  <Point X="-28.03351953125" Y="-1.169927368164" />
                  <Point X="-28.051248046875" Y="-1.156422241211" />
                  <Point X="-28.070009765625" Y="-1.143819824219" />
                  <Point X="-28.091265625" Y="-1.131309204102" />
                  <Point X="-28.105431640625" Y="-1.124481567383" />
                  <Point X="-28.1346953125" Y="-1.113257324219" />
                  <Point X="-28.14979296875" Y="-1.108860595703" />
                  <Point X="-28.1816796875" Y="-1.10237890625" />
                  <Point X="-28.197296875" Y="-1.100532592773" />
                  <Point X="-28.22862109375" Y="-1.09944152832" />
                  <Point X="-28.244328125" Y="-1.100196655273" />
                  <Point X="-29.5508359375" Y="-1.272201660156" />
                  <Point X="-29.660919921875" Y="-1.286694458008" />
                  <Point X="-29.74076171875" Y="-0.974110595703" />
                  <Point X="-29.75425" Y="-0.879804992676" />
                  <Point X="-29.786451171875" Y="-0.654654541016" />
                  <Point X="-28.801109375" Y="-0.390632751465" />
                  <Point X="-28.508287109375" Y="-0.312171173096" />
                  <Point X="-28.4983359375" Y="-0.308906829834" />
                  <Point X="-28.47884765625" Y="-0.301300750732" />
                  <Point X="-28.469310546875" Y="-0.296958862305" />
                  <Point X="-28.448267578125" Y="-0.285938049316" />
                  <Point X="-28.428091796875" Y="-0.273716339111" />
                  <Point X="-28.405814453125" Y="-0.258255554199" />
                  <Point X="-28.39648046875" Y="-0.250870025635" />
                  <Point X="-28.372515625" Y="-0.229333984375" />
                  <Point X="-28.358142578125" Y="-0.213089645386" />
                  <Point X="-28.337765625" Y="-0.1839296875" />
                  <Point X="-28.328953125" Y="-0.168385971069" />
                  <Point X="-28.319703125" Y="-0.147759048462" />
                  <Point X="-28.311609375" Y="-0.126334915161" />
                  <Point X="-28.3041875" Y="-0.102422233582" />
                  <Point X="-28.301580078125" Y="-0.091950271606" />
                  <Point X="-28.29613671875" Y="-0.063226371765" />
                  <Point X="-28.294498046875" Y="-0.043465621948" />
                  <Point X="-28.295208984375" Y="-0.010884814262" />
                  <Point X="-28.296970703125" Y="0.005371555328" />
                  <Point X="-28.30104296875" Y="0.026081855774" />
                  <Point X="-28.30601171875" Y="0.045746295929" />
                  <Point X="-28.3134375" Y="0.069670860291" />
                  <Point X="-28.31766796875" Y="0.080788154602" />
                  <Point X="-28.330982421875" Y="0.110109512329" />
                  <Point X="-28.34222265625" Y="0.128804046631" />
                  <Point X="-28.36408203125" Y="0.157181945801" />
                  <Point X="-28.376359375" Y="0.170329711914" />
                  <Point X="-28.393654296875" Y="0.185645751953" />
                  <Point X="-28.411283203125" Y="0.199491897583" />
                  <Point X="-28.43355859375" Y="0.214952392578" />
                  <Point X="-28.440482421875" Y="0.219328445435" />
                  <Point X="-28.46561328125" Y="0.23283366394" />
                  <Point X="-28.496564453125" Y="0.245635437012" />
                  <Point X="-28.508287109375" Y="0.249611251831" />
                  <Point X="-29.69923046875" Y="0.568723754883" />
                  <Point X="-29.7854453125" Y="0.591825012207" />
                  <Point X="-29.73133203125" Y="0.957518859863" />
                  <Point X="-29.704173828125" Y="1.057738647461" />
                  <Point X="-29.6335859375" Y="1.318237060547" />
                  <Point X="-28.987966796875" Y="1.233239868164" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815551758" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.204703491211" />
                  <Point X="-28.71514453125" Y="1.20658972168" />
                  <Point X="-28.704890625" Y="1.208053588867" />
                  <Point X="-28.684599609375" Y="1.21208972168" />
                  <Point X="-28.66244140625" Y="1.21848425293" />
                  <Point X="-28.61313671875" Y="1.234029541016" />
                  <Point X="-28.603443359375" Y="1.237678588867" />
                  <Point X="-28.584513671875" Y="1.246008666992" />
                  <Point X="-28.57527734375" Y="1.250689697266" />
                  <Point X="-28.556533203125" Y="1.26151171875" />
                  <Point X="-28.547857421875" Y="1.267172119141" />
                  <Point X="-28.53117578125" Y="1.279403808594" />
                  <Point X="-28.51592578125" Y="1.29337878418" />
                  <Point X="-28.502287109375" Y="1.308931152344" />
                  <Point X="-28.495892578125" Y="1.317079223633" />
                  <Point X="-28.483478515625" Y="1.334808837891" />
                  <Point X="-28.478009765625" Y="1.343604125977" />
                  <Point X="-28.46805859375" Y="1.361742919922" />
                  <Point X="-28.458712890625" Y="1.382830322266" />
                  <Point X="-28.4389296875" Y="1.430591674805" />
                  <Point X="-28.43549609375" Y="1.440356201172" />
                  <Point X="-28.4297109375" Y="1.460203369141" />
                  <Point X="-28.427359375" Y="1.470285766602" />
                  <Point X="-28.423599609375" Y="1.491601074219" />
                  <Point X="-28.422359375" Y="1.501888793945" />
                  <Point X="-28.421005859375" Y="1.522535400391" />
                  <Point X="-28.421908203125" Y="1.543205566406" />
                  <Point X="-28.425056640625" Y="1.563655517578" />
                  <Point X="-28.427189453125" Y="1.573794189453" />
                  <Point X="-28.43279296875" Y="1.594700805664" />
                  <Point X="-28.436017578125" Y="1.604544921875" />
                  <Point X="-28.443513671875" Y="1.623816650391" />
                  <Point X="-28.453654296875" Y="1.644518798828" />
                  <Point X="-28.477525390625" Y="1.690374023438" />
                  <Point X="-28.482798828125" Y="1.699283325195" />
                  <Point X="-28.4942890625" Y="1.716481445313" />
                  <Point X="-28.500505859375" Y="1.724770263672" />
                  <Point X="-28.51441796875" Y="1.741350341797" />
                  <Point X="-28.521501953125" Y="1.748912597656" />
                  <Point X="-28.5364453125" Y="1.763216918945" />
                  <Point X="-28.5443046875" Y="1.769958984375" />
                  <Point X="-29.227431640625" Y="2.294141601562" />
                  <Point X="-29.22761328125" Y="2.294281738281" />
                  <Point X="-29.00228515625" Y="2.680319335938" />
                  <Point X="-28.930365234375" Y="2.772765136719" />
                  <Point X="-28.726337890625" Y="3.035013183594" />
                  <Point X="-28.383072265625" Y="2.836828125" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.138189453125" Y="2.729680908203" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310791016" />
                  <Point X="-27.97643359375" Y="2.734227294922" />
                  <Point X="-27.95699609375" Y="2.741302001953" />
                  <Point X="-27.9384453125" Y="2.750450439453" />
                  <Point X="-27.929419921875" Y="2.755530761719" />
                  <Point X="-27.911166015625" Y="2.767159667969" />
                  <Point X="-27.902748046875" Y="2.773191650391" />
                  <Point X="-27.8866171875" Y="2.786137451172" />
                  <Point X="-27.866919921875" Y="2.80503515625" />
                  <Point X="-27.8181796875" Y="2.853774658203" />
                  <Point X="-27.811263671875" Y="2.861489257812" />
                  <Point X="-27.798314453125" Y="2.877624023438" />
                  <Point X="-27.79228125" Y="2.886044189453" />
                  <Point X="-27.78065234375" Y="2.904298828125" />
                  <Point X="-27.7755703125" Y="2.913325683594" />
                  <Point X="-27.766423828125" Y="2.931874267578" />
                  <Point X="-27.759353515625" Y="2.951299072266" />
                  <Point X="-27.754435546875" Y="2.971387695312" />
                  <Point X="-27.7525234375" Y="2.981573242188" />
                  <Point X="-27.749697265625" Y="3.003032226562" />
                  <Point X="-27.748908203125" Y="3.013364746094" />
                  <Point X="-27.74845703125" Y="3.034046142578" />
                  <Point X="-27.750271484375" Y="3.061278320313" />
                  <Point X="-27.756279296875" Y="3.129944580078" />
                  <Point X="-27.757744140625" Y="3.140201416016" />
                  <Point X="-27.76178125" Y="3.160497070312" />
                  <Point X="-27.764353515625" Y="3.170535888672" />
                  <Point X="-27.77086328125" Y="3.191177978516" />
                  <Point X="-27.774513671875" Y="3.200873291016" />
                  <Point X="-27.78284375" Y="3.21980078125" />
                  <Point X="-27.7875234375" Y="3.229032958984" />
                  <Point X="-28.05938671875" Y="3.699915771484" />
                  <Point X="-27.648369140625" Y="4.015038330078" />
                  <Point X="-27.535095703125" Y="4.077971191406" />
                  <Point X="-27.1925234375" Y="4.268295898437" />
                  <Point X="-27.158029296875" Y="4.223341308594" />
                  <Point X="-27.118564453125" Y="4.171909179688" />
                  <Point X="-27.1118203125" Y="4.164048828125" />
                  <Point X="-27.097517578125" Y="4.149107421875" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.06508984375" Y="4.121896972656" />
                  <Point X="-27.047888671875" Y="4.110403808594" />
                  <Point X="-27.0389765625" Y="4.105127441406" />
                  <Point X="-27.020185546875" Y="4.095345703125" />
                  <Point X="-26.943759765625" Y="4.055560546875" />
                  <Point X="-26.93432421875" Y="4.051285644531" />
                  <Point X="-26.915046875" Y="4.043789550781" />
                  <Point X="-26.905205078125" Y="4.040568359375" />
                  <Point X="-26.884298828125" Y="4.034966308594" />
                  <Point X="-26.8741640625" Y="4.032834960938" />
                  <Point X="-26.853720703125" Y="4.029687988281" />
                  <Point X="-26.8330546875" Y="4.028785400391" />
                  <Point X="-26.812416015625" Y="4.030137939453" />
                  <Point X="-26.802134765625" Y="4.031377685547" />
                  <Point X="-26.780818359375" Y="4.035135986328" />
                  <Point X="-26.7707265625" Y="4.037488525391" />
                  <Point X="-26.750861328125" Y="4.043279052734" />
                  <Point X="-26.741087890625" Y="4.046716552734" />
                  <Point X="-26.721517578125" Y="4.054823974609" />
                  <Point X="-26.6419140625" Y="4.087796142578" />
                  <Point X="-26.632576171875" Y="4.092276367188" />
                  <Point X="-26.614443359375" Y="4.102225585938" />
                  <Point X="-26.6056484375" Y="4.107694824219" />
                  <Point X="-26.587919921875" Y="4.120109375" />
                  <Point X="-26.579771484375" Y="4.12650390625" />
                  <Point X="-26.56422265625" Y="4.140140625" />
                  <Point X="-26.55025390625" Y="4.155385742188" />
                  <Point X="-26.5380234375" Y="4.172064453125" />
                  <Point X="-26.532361328125" Y="4.180740234375" />
                  <Point X="-26.5215390625" Y="4.199483886719" />
                  <Point X="-26.51685546875" Y="4.208724121094" />
                  <Point X="-26.508521484375" Y="4.2276640625" />
                  <Point X="-26.50487109375" Y="4.237363769531" />
                  <Point X="-26.498501953125" Y="4.257567871094" />
                  <Point X="-26.472591796875" Y="4.339740234375" />
                  <Point X="-26.470021484375" Y="4.349772460938" />
                  <Point X="-26.46598828125" Y="4.370053222656" />
                  <Point X="-26.464525390625" Y="4.380301757812" />
                  <Point X="-26.462638671875" Y="4.401863769531" />
                  <Point X="-26.46230078125" Y="4.412216796875" />
                  <Point X="-26.462751953125" Y="4.4328984375" />
                  <Point X="-26.463541015625" Y="4.443227050781" />
                  <Point X="-26.479265625" Y="4.562655761719" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.793859375" Y="4.732391113281" />
                  <Point X="-25.36522265625" Y="4.782556152344" />
                  <Point X="-25.2638046875" Y="4.404056640625" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.978369140625" Y="4.085113037109" />
                  <Point X="-24.94783203125" Y="4.090154541016" />
                  <Point X="-24.93276953125" Y="4.093927246094" />
                  <Point X="-24.90233203125" Y="4.104259277344" />
                  <Point X="-24.8880859375" Y="4.110436035156" />
                  <Point X="-24.86079296875" Y="4.125024902344" />
                  <Point X="-24.8362421875" Y="4.143861328125" />
                  <Point X="-24.815083984375" Y="4.16644921875" />
                  <Point X="-24.8054296875" Y="4.178612792969" />
                  <Point X="-24.7875703125" Y="4.205339355469" />
                  <Point X="-24.78002734375" Y="4.218911621094" />
                  <Point X="-24.767251953125" Y="4.24710546875" />
                  <Point X="-24.76201953125" Y="4.261727050781" />
                  <Point X="-24.62180859375" Y="4.785005859375" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-24.058505859375" Y="4.710481933594" />
                  <Point X="-23.546408203125" Y="4.586845703125" />
                  <Point X="-23.47461328125" Y="4.560805175781" />
                  <Point X="-23.14173828125" Y="4.440068847656" />
                  <Point X="-23.0702265625" Y="4.406625" />
                  <Point X="-22.74955078125" Y="4.256655273438" />
                  <Point X="-22.68040234375" Y="4.216369628906" />
                  <Point X="-22.3705625" Y="4.03585546875" />
                  <Point X="-22.3053984375" Y="3.989516601562" />
                  <Point X="-22.182216796875" Y="3.901916015625" />
                  <Point X="-22.76464453125" Y="2.893120361328" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.93909765625" Y="2.589973876953" />
                  <Point X="-22.95052734375" Y="2.563280029297" />
                  <Point X="-22.959767578125" Y="2.535352294922" />
                  <Point X="-22.962935546875" Y="2.524753662109" />
                  <Point X="-22.98016796875" Y="2.460311767578" />
                  <Point X="-22.982640625" Y="2.447693359375" />
                  <Point X="-22.985861328125" Y="2.422235351562" />
                  <Point X="-22.986609375" Y="2.409395751953" />
                  <Point X="-22.986294921875" Y="2.376792724609" />
                  <Point X="-22.984935546875" Y="2.355881103516" />
                  <Point X="-22.978216796875" Y="2.300157226562" />
                  <Point X="-22.97619921875" Y="2.289038330078" />
                  <Point X="-22.970857421875" Y="2.267116455078" />
                  <Point X="-22.967533203125" Y="2.256313476562" />
                  <Point X="-22.959267578125" Y="2.234224365234" />
                  <Point X="-22.95468359375" Y="2.223893066406" />
                  <Point X="-22.944318359375" Y="2.203842041016" />
                  <Point X="-22.93005859375" Y="2.181628417969" />
                  <Point X="-22.895578125" Y="2.130813720703" />
                  <Point X="-22.88757421875" Y="2.120598876953" />
                  <Point X="-22.87025" Y="2.101354248047" />
                  <Point X="-22.8609296875" Y="2.092324462891" />
                  <Point X="-22.835134765625" Y="2.070546875" />
                  <Point X="-22.819248046875" Y="2.058503417969" />
                  <Point X="-22.76843359375" Y="2.024023681641" />
                  <Point X="-22.75871875" Y="2.018244384766" />
                  <Point X="-22.738673828125" Y="2.007882080078" />
                  <Point X="-22.72834375" Y="2.003299194336" />
                  <Point X="-22.706255859375" Y="1.995033081055" />
                  <Point X="-22.695453125" Y="1.991708251953" />
                  <Point X="-22.673529296875" Y="1.986364868164" />
                  <Point X="-22.64870703125" Y="1.982694335938" />
                  <Point X="-22.592984375" Y="1.975974975586" />
                  <Point X="-22.579755859375" Y="1.975309936523" />
                  <Point X="-22.553333984375" Y="1.975826171875" />
                  <Point X="-22.540140625" Y="1.97700769043" />
                  <Point X="-22.505392578125" Y="1.982582275391" />
                  <Point X="-22.486404296875" Y="1.986633056641" />
                  <Point X="-22.421962890625" Y="2.003865722656" />
                  <Point X="-22.416" Y="2.005671875" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572753906" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.166103515625" Y="2.719457763672" />
                  <Point X="-21.05959375" Y="2.780950927734" />
                  <Point X="-20.956046875" Y="2.637043212891" />
                  <Point X="-20.91971875" Y="2.577010742188" />
                  <Point X="-20.863115234375" Y="2.483471435547" />
                  <Point X="-21.603169921875" Y="1.915606811523" />
                  <Point X="-21.827046875" Y="1.743819946289" />
                  <Point X="-21.8345703125" Y="1.737397338867" />
                  <Point X="-21.855734375" Y="1.716626098633" />
                  <Point X="-21.8763828125" Y="1.6925" />
                  <Point X="-21.882828125" Y="1.684545898438" />
                  <Point X="-21.92920703125" Y="1.624040893555" />
                  <Point X="-21.936470703125" Y="1.613064575195" />
                  <Point X="-21.949408203125" Y="1.590222045898" />
                  <Point X="-21.95508203125" Y="1.578355957031" />
                  <Point X="-21.967798828125" Y="1.546215942383" />
                  <Point X="-21.974103515625" Y="1.527487182617" />
                  <Point X="-21.991380859375" Y="1.465711669922" />
                  <Point X="-21.993771484375" Y="1.454669921875" />
                  <Point X="-21.9972265625" Y="1.432370849609" />
                  <Point X="-21.998291015625" Y="1.42111340332" />
                  <Point X="-21.999107421875" Y="1.39754284668" />
                  <Point X="-21.998826171875" Y="1.386239990234" />
                  <Point X="-21.996921875" Y="1.363747192383" />
                  <Point X="-21.991810546875" Y="1.335657592773" />
                  <Point X="-21.97762890625" Y="1.266924438477" />
                  <Point X="-21.97404296875" Y="1.254143188477" />
                  <Point X="-21.965126953125" Y="1.229207763672" />
                  <Point X="-21.959796875" Y="1.217053710938" />
                  <Point X="-21.943115234375" Y="1.185260986328" />
                  <Point X="-21.933599609375" Y="1.16911340332" />
                  <Point X="-21.89502734375" Y="1.110483276367" />
                  <Point X="-21.88826171875" Y="1.101424804688" />
                  <Point X="-21.87370703125" Y="1.084179199219" />
                  <Point X="-21.86591796875" Y="1.07599230957" />
                  <Point X="-21.848673828125" Y="1.059901977539" />
                  <Point X="-21.83996484375" Y="1.052695556641" />
                  <Point X="-21.82175390625" Y="1.039369140625" />
                  <Point X="-21.7985078125" Y="1.025512207031" />
                  <Point X="-21.742609375" Y="0.994046569824" />
                  <Point X="-21.730361328125" Y="0.988260070801" />
                  <Point X="-21.7051640625" Y="0.978487670898" />
                  <Point X="-21.69221484375" Y="0.97450177002" />
                  <Point X="-21.655654296875" Y="0.966047973633" />
                  <Point X="-21.637744140625" Y="0.962801757812" />
                  <Point X="-21.562166015625" Y="0.952812927246" />
                  <Point X="-21.555966796875" Y="0.952199707031" />
                  <Point X="-21.53428125" Y="0.951222961426" />
                  <Point X="-21.50878125" Y="0.952032348633" />
                  <Point X="-21.49939453125" Y="0.952797180176" />
                  <Point X="-20.3615078125" Y="1.102603149414" />
                  <Point X="-20.295296875" Y="1.111319946289" />
                  <Point X="-20.247310546875" Y="0.914207580566" />
                  <Point X="-20.2358671875" Y="0.840706726074" />
                  <Point X="-20.216126953125" Y="0.713921142578" />
                  <Point X="-21.051439453125" Y="0.490099853516" />
                  <Point X="-21.3080078125" Y="0.42135269165" />
                  <Point X="-21.31396875" Y="0.419543823242" />
                  <Point X="-21.334375" Y="0.412136749268" />
                  <Point X="-21.35762109375" Y="0.401617370605" />
                  <Point X="-21.365998046875" Y="0.397313995361" />
                  <Point X="-21.38425390625" Y="0.386761016846" />
                  <Point X="-21.4585078125" Y="0.343841583252" />
                  <Point X="-21.469623046875" Y="0.33632244873" />
                  <Point X="-21.490685546875" Y="0.319789642334" />
                  <Point X="-21.5006328125" Y="0.310775939941" />
                  <Point X="-21.525900390625" Y="0.284397033691" />
                  <Point X="-21.538158203125" Y="0.270266571045" />
                  <Point X="-21.582708984375" Y="0.213497085571" />
                  <Point X="-21.58914453125" Y="0.204206665039" />
                  <Point X="-21.600869140625" Y="0.184925521851" />
                  <Point X="-21.606158203125" Y="0.174934799194" />
                  <Point X="-21.615931640625" Y="0.153470825195" />
                  <Point X="-21.619994140625" Y="0.142924255371" />
                  <Point X="-21.626837890625" Y="0.121429519653" />
                  <Point X="-21.633271484375" Y="0.091415283203" />
                  <Point X="-21.64812109375" Y="0.013870851517" />
                  <Point X="-21.64971484375" Y="0.00042716977" />
                  <Point X="-21.650974609375" Y="-0.02656006813" />
                  <Point X="-21.650640625" Y="-0.040103626251" />
                  <Point X="-21.646990234375" Y="-0.077978973389" />
                  <Point X="-21.644474609375" Y="-0.09549105835" />
                  <Point X="-21.629623046875" Y="-0.173035644531" />
                  <Point X="-21.62683984375" Y="-0.183989593506" />
                  <Point X="-21.619994140625" Y="-0.205488632202" />
                  <Point X="-21.615931640625" Y="-0.216033569336" />
                  <Point X="-21.606158203125" Y="-0.237497238159" />
                  <Point X="-21.600869140625" Y="-0.247487670898" />
                  <Point X="-21.589146484375" Y="-0.266765380859" />
                  <Point X="-21.571759765625" Y="-0.290010742188" />
                  <Point X="-21.52720703125" Y="-0.34678024292" />
                  <Point X="-21.521287109375" Y="-0.353626831055" />
                  <Point X="-21.498853515625" Y="-0.37581149292" />
                  <Point X="-21.46981640625" Y="-0.398728668213" />
                  <Point X="-21.4585" Y="-0.406405639648" />
                  <Point X="-21.4402421875" Y="-0.416958618164" />
                  <Point X="-21.365990234375" Y="-0.459878234863" />
                  <Point X="-21.360501953125" Y="-0.462815612793" />
                  <Point X="-21.340845703125" Y="-0.472014770508" />
                  <Point X="-21.316974609375" Y="-0.481026824951" />
                  <Point X="-21.3080078125" Y="-0.483912780762" />
                  <Point X="-20.264513671875" Y="-0.763516418457" />
                  <Point X="-20.215119140625" Y="-0.776751403809" />
                  <Point X="-20.238380859375" Y="-0.931038574219" />
                  <Point X="-20.25305078125" Y="-0.995325561523" />
                  <Point X="-20.2721953125" Y="-1.079219726563" />
                  <Point X="-21.265669921875" Y="-0.948426452637" />
                  <Point X="-21.563216796875" Y="-0.909253601074" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042480469" />
                  <Point X="-21.63327734375" Y="-0.910840881348" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.681349609375" Y="-0.920464355469" />
                  <Point X="-21.82708203125" Y="-0.952139892578" />
                  <Point X="-21.842125" Y="-0.956742614746" />
                  <Point X="-21.87124609375" Y="-0.968367614746" />
                  <Point X="-21.88532421875" Y="-0.975390014648" />
                  <Point X="-21.913150390625" Y="-0.992282531738" />
                  <Point X="-21.925876953125" Y="-1.001532348633" />
                  <Point X="-21.949625" Y="-1.022002502441" />
                  <Point X="-21.960646484375" Y="-1.03322265625" />
                  <Point X="-21.9823046875" Y="-1.059270751953" />
                  <Point X="-22.070390625" Y="-1.165210693359" />
                  <Point X="-22.078669921875" Y="-1.176845458984" />
                  <Point X="-22.093390625" Y="-1.201226806641" />
                  <Point X="-22.09983203125" Y="-1.213973022461" />
                  <Point X="-22.11117578125" Y="-1.241358154297" />
                  <Point X="-22.115634765625" Y="-1.254928344727" />
                  <Point X="-22.122466796875" Y="-1.282581176758" />
                  <Point X="-22.12483984375" Y="-1.296663818359" />
                  <Point X="-22.127943359375" Y="-1.330396972656" />
                  <Point X="-22.140568359375" Y="-1.467594116211" />
                  <Point X="-22.140708984375" Y="-1.483316894531" />
                  <Point X="-22.138392578125" Y="-1.514580932617" />
                  <Point X="-22.135935546875" Y="-1.530121948242" />
                  <Point X="-22.128205078125" Y="-1.561742919922" />
                  <Point X="-22.12321484375" Y="-1.576666015625" />
                  <Point X="-22.110841796875" Y="-1.60548059082" />
                  <Point X="-22.103458984375" Y="-1.619372070312" />
                  <Point X="-22.08362890625" Y="-1.650216186523" />
                  <Point X="-22.002978515625" Y="-1.775662719727" />
                  <Point X="-21.998251953125" Y="-1.782359008789" />
                  <Point X="-21.980201171875" Y="-1.804454101562" />
                  <Point X="-21.9565078125" Y="-1.828121337891" />
                  <Point X="-21.947201171875" Y="-1.836277709961" />
                  <Point X="-20.97883203125" Y="-2.579333984375" />
                  <Point X="-20.912828125" Y="-2.629980712891" />
                  <Point X="-20.954513671875" Y="-2.697435791016" />
                  <Point X="-20.98484375" Y="-2.740528564453" />
                  <Point X="-20.998724609375" Y="-2.760251708984" />
                  <Point X="-21.8865390625" Y="-2.247671630859" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.184970703125" Y="-2.079513916016" />
                  <Point X="-22.216876953125" Y="-2.069326416016" />
                  <Point X="-22.228888671875" Y="-2.066337646484" />
                  <Point X="-22.271533203125" Y="-2.058635986328" />
                  <Point X="-22.444978515625" Y="-2.027312011719" />
                  <Point X="-22.460638671875" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.55515625" Y="-2.03513659668" />
                  <Point X="-22.584931640625" Y="-2.044960571289" />
                  <Point X="-22.59941015625" Y="-2.051109130859" />
                  <Point X="-22.634837890625" Y="-2.069754638672" />
                  <Point X="-22.778927734375" Y="-2.145588134766" />
                  <Point X="-22.79103125" Y="-2.153170654297" />
                  <Point X="-22.813962890625" Y="-2.170064453125" />
                  <Point X="-22.824791015625" Y="-2.179375732422" />
                  <Point X="-22.84575" Y="-2.200335449219" />
                  <Point X="-22.855060546875" Y="-2.211163085938" />
                  <Point X="-22.871951171875" Y="-2.234090820312" />
                  <Point X="-22.87953125" Y="-2.246190917969" />
                  <Point X="-22.898177734375" Y="-2.281618896484" />
                  <Point X="-22.97401171875" Y="-2.425708984375" />
                  <Point X="-22.980162109375" Y="-2.440187744141" />
                  <Point X="-22.98998828125" Y="-2.469967773438" />
                  <Point X="-22.9936640625" Y="-2.485269042969" />
                  <Point X="-22.99862109375" Y="-2.517441894531" />
                  <Point X="-22.999720703125" Y="-2.533139160156" />
                  <Point X="-22.999314453125" Y="-2.5644921875" />
                  <Point X="-22.99780859375" Y="-2.580147949219" />
                  <Point X="-22.99010546875" Y="-2.622793457031" />
                  <Point X="-22.95878125" Y="-2.79623828125" />
                  <Point X="-22.956984375" Y="-2.804215576172" />
                  <Point X="-22.94876171875" Y="-2.831543212891" />
                  <Point X="-22.93592578125" Y="-2.862482910156" />
                  <Point X="-22.93044921875" Y="-2.873578857422" />
                  <Point X="-22.30817578125" Y="-3.951389892578" />
                  <Point X="-22.26410546875" Y="-4.027722412109" />
                  <Point X="-22.276244140625" Y="-4.036081787109" />
                  <Point X="-22.963859375" Y="-3.139965332031" />
                  <Point X="-23.16608203125" Y="-2.876422607422" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849626220703" />
                  <Point X="-23.21674609375" Y="-2.828004882812" />
                  <Point X="-23.22669921875" Y="-2.820646484375" />
                  <Point X="-23.268759765625" Y="-2.793605712891" />
                  <Point X="-23.43982421875" Y="-2.683627685547" />
                  <Point X="-23.453716796875" Y="-2.676244628906" />
                  <Point X="-23.482529296875" Y="-2.663873291016" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.63760546875" Y="-2.650749267578" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920125" Y="-2.699414794922" />
                  <Point X="-23.944505859375" Y="-2.714135742188" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-23.991658203125" Y="-2.751946777344" />
                  <Point X="-24.136123046875" Y="-2.872063720703" />
                  <Point X="-24.147345703125" Y="-2.883087158203" />
                  <Point X="-24.16781640625" Y="-2.906836425781" />
                  <Point X="-24.177064453125" Y="-2.919562255859" />
                  <Point X="-24.19395703125" Y="-2.947388671875" />
                  <Point X="-24.20098046875" Y="-2.961468505859" />
                  <Point X="-24.21260546875" Y="-2.990591552734" />
                  <Point X="-24.21720703125" Y="-3.005634765625" />
                  <Point X="-24.227826171875" Y="-3.054496337891" />
                  <Point X="-24.271021484375" Y="-3.25322265625" />
                  <Point X="-24.2724140625" Y="-3.261287353516" />
                  <Point X="-24.275275390625" Y="-3.289677001953" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520263672" />
                  <Point X="-24.16691015625" Y="-4.152327636719" />
                  <Point X="-24.265876953125" Y="-3.782973632812" />
                  <Point X="-24.344931640625" Y="-3.487937011719" />
                  <Point X="-24.347392578125" Y="-3.480122314453" />
                  <Point X="-24.35785546875" Y="-3.453574707031" />
                  <Point X="-24.37321484375" Y="-3.423811035156" />
                  <Point X="-24.37959375" Y="-3.413208740234" />
                  <Point X="-24.41190625" Y="-3.366653564453" />
                  <Point X="-24.543322265625" Y="-3.177308349609" />
                  <Point X="-24.553328125" Y="-3.165173828125" />
                  <Point X="-24.5752109375" Y="-3.142718505859" />
                  <Point X="-24.587087890625" Y="-3.132397705078" />
                  <Point X="-24.61334375" Y="-3.113153808594" />
                  <Point X="-24.6267578125" Y="-3.104936767578" />
                  <Point X="-24.6547578125" Y="-3.090829345703" />
                  <Point X="-24.66934375" Y="-3.084938964844" />
                  <Point X="-24.71934375" Y="-3.069420654297" />
                  <Point X="-24.922703125" Y="-3.006305664062" />
                  <Point X="-24.936623046875" Y="-3.003109619141" />
                  <Point X="-24.964783203125" Y="-2.998840087891" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.998840087891" />
                  <Point X="-25.051064453125" Y="-3.003109619141" />
                  <Point X="-25.064984375" Y="-3.006305664062" />
                  <Point X="-25.114984375" Y="-3.021823974609" />
                  <Point X="-25.31834375" Y="-3.084938964844" />
                  <Point X="-25.3329296875" Y="-3.090829345703" />
                  <Point X="-25.3609296875" Y="-3.104936767578" />
                  <Point X="-25.37434375" Y="-3.113153808594" />
                  <Point X="-25.400599609375" Y="-3.132397705078" />
                  <Point X="-25.4124765625" Y="-3.142718505859" />
                  <Point X="-25.434361328125" Y="-3.16517578125" />
                  <Point X="-25.444369140625" Y="-3.177312255859" />
                  <Point X="-25.4766796875" Y="-3.2238671875" />
                  <Point X="-25.608095703125" Y="-3.413212646484" />
                  <Point X="-25.61246875" Y="-3.420131103516" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213378906" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.96428125" Y="-4.687889648438" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.315937348398" Y="4.199730171459" />
                  <Point X="-28.011154078659" Y="3.616374069509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.713100877849" Y="3.027370769202" />
                  <Point X="-28.768353044505" Y="2.981008696539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.150969609559" Y="4.214140847789" />
                  <Point X="-27.962921438568" Y="3.532832367533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.625548504669" Y="2.97682224076" />
                  <Point X="-29.024726672223" Y="2.641871987591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.089538764788" Y="4.141673754492" />
                  <Point X="-27.914688798477" Y="3.449290665558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.537996131489" Y="2.926273712317" />
                  <Point X="-29.166598547585" Y="2.398813656814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.420354530208" Y="4.579172306432" />
                  <Point X="-26.475362937161" Y="4.533014772446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.005850192192" Y="4.087883112405" />
                  <Point X="-27.866456158386" Y="3.365748963582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.450443758309" Y="2.875725183874" />
                  <Point X="-29.183616002526" Y="2.260520624163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.19840041637" Y="4.641400229004" />
                  <Point X="-26.462466696233" Y="4.419822310965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.911851477711" Y="4.04274370657" />
                  <Point X="-27.818223518296" Y="3.282207261606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.362891361011" Y="2.82517667567" />
                  <Point X="-29.10641758418" Y="2.201284096038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.976446302533" Y="4.703628151577" />
                  <Point X="-26.495070380436" Y="4.268450879089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.770104500172" Y="4.037669850657" />
                  <Point X="-27.772796162982" Y="3.196311646209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.275338883195" Y="2.774628235027" />
                  <Point X="-29.029219165834" Y="2.142047567913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.794457618377" Y="4.732321096844" />
                  <Point X="-27.752710238373" Y="3.089152045654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.175253498562" Y="2.734596151872" />
                  <Point X="-28.952020747487" Y="2.082811039789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.622709128346" Y="4.752421498998" />
                  <Point X="-27.756807050585" Y="2.961700719551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.04039260818" Y="2.723744182765" />
                  <Point X="-28.874822329141" Y="2.023574511664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.45096064062" Y="4.772521899218" />
                  <Point X="-28.797623910795" Y="1.964337983539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.57656363536" Y="1.310729947947" />
                  <Point X="-29.652999629918" Y="1.246592533105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.35163638283" Y="4.731851154809" />
                  <Point X="-28.720425492448" Y="1.905101455415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.448813487384" Y="1.29391135751" />
                  <Point X="-29.69649317518" Y="1.08608342283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.324506883262" Y="4.630601815404" />
                  <Point X="-28.643227074102" Y="1.84586492729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.321063339408" Y="1.277092767073" />
                  <Point X="-29.735502178564" Y="0.929337289992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.297377383694" Y="4.529352475999" />
                  <Point X="-28.566028655756" Y="1.786628399165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.193313191432" Y="1.260274176636" />
                  <Point X="-29.756454571762" Y="0.7877424521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.270247884126" Y="4.428103136594" />
                  <Point X="-28.49720286566" Y="1.720366401764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.065563043456" Y="1.243455586199" />
                  <Point X="-29.777406964961" Y="0.646147614208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.243118383328" Y="4.326853798221" />
                  <Point X="-28.449618860451" Y="1.636280430497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.937812906797" Y="1.226636986266" />
                  <Point X="-29.716400464967" Y="0.573324453366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.557363458626" Y="4.77825681013" />
                  <Point X="-24.642833583441" Y="4.706538859921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.212151633664" Y="4.228824293956" />
                  <Point X="-28.421584496815" Y="1.535790362198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.810062787649" Y="1.209818371639" />
                  <Point X="-29.604378607653" Y="0.543308260035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.425968682318" Y="4.764496425982" />
                  <Point X="-24.685700648767" Y="4.54655542873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.157680372105" Y="4.150517416954" />
                  <Point X="-28.460270862759" Y="1.379314954317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.64562130395" Y="1.223787467475" />
                  <Point X="-29.49235674212" Y="0.513292073602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.294573906011" Y="4.750736041833" />
                  <Point X="-24.728567714094" Y="4.386571997538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.070872434017" Y="4.0993442333" />
                  <Point X="-29.380334876586" Y="0.483275887168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.164310436124" Y="4.736026378885" />
                  <Point X="-24.779685969358" Y="4.219664995913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.9273364953" Y="4.095771494052" />
                  <Point X="-29.268313011053" Y="0.453259700734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.049539277663" Y="4.708317123133" />
                  <Point X="-29.156291145519" Y="0.423243514301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.934768170656" Y="4.680607824206" />
                  <Point X="-29.044269279986" Y="0.393227327867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.819997063649" Y="4.652898525278" />
                  <Point X="-28.932247414452" Y="0.363211141434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.705225956642" Y="4.625189226351" />
                  <Point X="-28.820225548919" Y="0.333194955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.590454849636" Y="4.597479927424" />
                  <Point X="-28.708203683385" Y="0.303178768566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.482820654592" Y="4.5637820483" />
                  <Point X="-28.596181817852" Y="0.273162582133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.662250121476" Y="-0.621374938248" />
                  <Point X="-29.777392738689" Y="-0.717991065884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.379631321703" Y="4.526354486983" />
                  <Point X="-28.486268664333" Y="0.241376976226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.445120378868" Y="-0.563195143795" />
                  <Point X="-29.76155667885" Y="-0.8287167264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.276441993015" Y="4.488926922139" />
                  <Point X="-28.399459210949" Y="0.190205064056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.227990636261" Y="-0.505015349342" />
                  <Point X="-29.745720263703" Y="-0.939442088778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.173252664328" Y="4.451499357295" />
                  <Point X="-28.336393770353" Y="0.119109559514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.010860893654" Y="-0.44683555489" />
                  <Point X="-29.722843762752" Y="-1.044260117754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.075820380753" Y="4.409241058021" />
                  <Point X="-28.300819645535" Y="0.024946102041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.793731150419" Y="-0.38865575991" />
                  <Point X="-29.696758352763" Y="-1.14638555234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.980919265891" Y="4.364858856014" />
                  <Point X="-28.304021098108" Y="-0.101753928119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.576601389333" Y="-0.330475949951" />
                  <Point X="-29.670672942775" Y="-1.248510986926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.886018143736" Y="4.320476660126" />
                  <Point X="-29.551164114066" Y="-1.27224486532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.791117021581" Y="4.276094464238" />
                  <Point X="-29.375866646701" Y="-1.249166517595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.700527450342" Y="4.228094447566" />
                  <Point X="-29.200569179199" Y="-1.226088169754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.613298446093" Y="4.177274580373" />
                  <Point X="-29.025271711696" Y="-1.203009821913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.526069557014" Y="4.126454616541" />
                  <Point X="-28.849974244194" Y="-1.179931474073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.438840667936" Y="4.075634652708" />
                  <Point X="-28.674676776692" Y="-1.156853126232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.353182715401" Y="4.023496516601" />
                  <Point X="-28.49937930919" Y="-1.133774778391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.27318554974" Y="3.966608416315" />
                  <Point X="-28.324081841688" Y="-1.11069643055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.193189583584" Y="3.909719309526" />
                  <Point X="-28.16936007272" Y="-1.104883143761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.302047407736" Y="3.694363056943" />
                  <Point X="-28.068876461345" Y="-1.144581075003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.44092778919" Y="3.4538148876" />
                  <Point X="-27.996842860148" Y="-1.208151399293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.579808170645" Y="3.213266718257" />
                  <Point X="-27.954917668745" Y="-1.296985679136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.718688552099" Y="2.972718548914" />
                  <Point X="-27.956328847175" Y="-1.422183490923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.088549779045" Y="-2.372229657266" />
                  <Point X="-29.17862070871" Y="-2.447808141128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.857569520163" Y="2.732169887348" />
                  <Point X="-29.129329451977" Y="-2.53046155827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.964662872476" Y="2.518294202434" />
                  <Point X="-29.080038195244" Y="-2.613114975411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.986254011718" Y="2.376163392973" />
                  <Point X="-29.030746675442" Y="-2.695768171812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.970356677886" Y="2.265489147442" />
                  <Point X="-28.977650822055" Y="-2.775229153304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.927317850721" Y="2.177589318955" />
                  <Point X="-28.920047733076" Y="-2.850908115074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.870304868519" Y="2.101415198806" />
                  <Point X="-28.862444644097" Y="-2.926587076843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.794037777996" Y="2.041397193849" />
                  <Point X="-28.804841555118" Y="-3.002266038613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.702775271003" Y="1.993961837321" />
                  <Point X="-28.415974671658" Y="-2.799981672612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.577149283917" Y="1.975360864264" />
                  <Point X="-27.942188363776" Y="-2.526441448897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.371195652203" Y="2.024162788188" />
                  <Point X="-27.583211654019" Y="-2.349237916626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.899184507881" Y="2.296213472814" />
                  <Point X="-27.4636609885" Y="-2.372936689768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.425397098939" Y="2.569754620427" />
                  <Point X="-27.383963524775" Y="-2.430076269837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.04691271388" Y="2.76332703585" />
                  <Point X="-27.331201221286" Y="-2.509817132926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.991273320596" Y="2.686000337747" />
                  <Point X="-27.311577155231" Y="-2.617364278823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.93778633794" Y="2.60686755268" />
                  <Point X="-27.388926367901" Y="-2.806281667133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.88801411494" Y="2.524617714156" />
                  <Point X="-21.72750628478" Y="1.820200144067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.86794761117" Y="1.702355878891" />
                  <Point X="-27.527806512139" Y="-3.046829637428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.988070798233" Y="1.477546864443" />
                  <Point X="-27.666687257275" Y="-3.287378111936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.994137043205" Y="1.348442988038" />
                  <Point X="-27.805568002411" Y="-3.527926586444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.970507856198" Y="1.244256537654" />
                  <Point X="-27.944448747547" Y="-3.768475060952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.926011627118" Y="1.157579614578" />
                  <Point X="-27.923999690002" Y="-3.875329956794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.87014941834" Y="1.080439880873" />
                  <Point X="-27.846927864927" Y="-3.934672709286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.792108718773" Y="1.02191011061" />
                  <Point X="-27.769856461744" Y="-3.994015815787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.698506151012" Y="0.976438298209" />
                  <Point X="-27.687117631901" Y="-4.048603386669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.576595272331" Y="0.954719979061" />
                  <Point X="-27.602076359925" Y="-4.101258979205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.418382393703" Y="0.963462654678" />
                  <Point X="-27.18705672531" Y="-3.877029849355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.459741640374" Y="-4.105839661013" />
                  <Point X="-27.517035087949" Y="-4.153914571742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.243084849771" Y="0.986541066651" />
                  <Point X="-26.920721215747" Y="-3.777561513998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.067787305839" Y="1.009619478624" />
                  <Point X="-26.783635343021" Y="-3.786546501241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.892489761908" Y="1.032697890597" />
                  <Point X="-26.646549590452" Y="-3.795531589307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.717192217976" Y="1.05577630257" />
                  <Point X="-26.536966694567" Y="-3.827594314273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.541894674044" Y="1.078854714542" />
                  <Point X="-21.33889677731" Y="0.410090543644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.594233813019" Y="0.195837331155" />
                  <Point X="-26.458975007409" Y="-3.88616521083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.366597130112" Y="1.101933126515" />
                  <Point X="-21.1165401668" Y="0.472656201027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.645215247529" Y="0.029045135774" />
                  <Point X="-26.386709286146" Y="-3.949540763259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.280424061676" Y="1.050227223971" />
                  <Point X="-20.899410582137" Y="0.530835862948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.644621264306" Y="-0.094470145609" />
                  <Point X="-25.144060077709" Y="-3.030847963263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.61614781275" Y="-3.426976607619" />
                  <Point X="-26.314443564884" Y="-4.012916315687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.255354468608" Y="0.947249417782" />
                  <Point X="-20.682280945736" Y="0.589015568284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.621921312904" Y="-0.199436317246" />
                  <Point X="-24.95914040499" Y="-2.999695626573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.672266757918" Y="-3.598079686298" />
                  <Point X="-26.244331453681" Y="-4.078098961522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.235713061718" Y="0.839716822572" />
                  <Point X="-20.465151309335" Y="0.647195273619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.575735421894" Y="-0.28469544562" />
                  <Point X="-24.847164145387" Y="-3.029750080926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.715134445371" Y="-3.758063639516" />
                  <Point X="-26.194852281672" Y="-4.160594699026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.218635516766" Y="0.730032891756" />
                  <Point X="-20.248021672933" Y="0.705374978955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.516147759573" Y="-0.358709152631" />
                  <Point X="-24.739275629443" Y="-3.063234559476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.758002132824" Y="-3.918047592734" />
                  <Point X="-26.169886888802" Y="-4.263659939562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.438780151778" Y="-0.417803713952" />
                  <Point X="-24.635769539918" Y="-3.100396330417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.800869820277" Y="-4.078031545952" />
                  <Point X="-26.148746709281" Y="-4.36993491521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.350312546939" Y="-0.467584271847" />
                  <Point X="-24.558657330874" Y="-3.159705196736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.84373750773" Y="-4.238015499169" />
                  <Point X="-26.127606672027" Y="-4.476210010233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.242800857864" Y="-0.501384945683" />
                  <Point X="-21.763526819" Y="-0.938325907617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.103988472783" Y="-1.224007155736" />
                  <Point X="-23.824865859505" Y="-2.667994736236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.213739331237" Y="-2.994298322941" />
                  <Point X="-24.502293014849" Y="-3.236423612434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.886605195183" Y="-4.397999452387" />
                  <Point X="-26.123534317561" Y="-4.59680659159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.130778997903" Y="-0.531401136792" />
                  <Point X="-21.580051593642" Y="-0.908385606175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.131705266922" Y="-1.371277999962" />
                  <Point X="-23.658849537391" Y="-2.652704194067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.247933846687" Y="-3.147004620729" />
                  <Point X="-24.447898835036" Y="-3.314795168702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.929472882636" Y="-4.557983405605" />
                  <Point X="-26.137274799457" Y="-4.732349917367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.018757137941" Y="-0.561417327902" />
                  <Point X="-21.450912533946" Y="-0.9240387613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.139346681744" Y="-1.501703600808" />
                  <Point X="-23.513689558271" Y="-2.654914201613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.275273143453" Y="-3.29395870705" />
                  <Point X="-24.393504424193" Y="-3.393166531112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.972340970063" Y="-4.717967694441" />
                  <Point X="-26.022068326878" Y="-4.759693901204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.90673527798" Y="-0.591433519011" />
                  <Point X="-21.323162450216" Y="-0.940857405647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.112003991177" Y="-1.602774051724" />
                  <Point X="-22.725215985122" Y="-2.117320009677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.880110931369" Y="-2.247292301944" />
                  <Point X="-23.417341258593" Y="-2.698082071375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.264752459838" Y="-3.409144497795" />
                  <Point X="-24.347876679161" Y="-3.478893999571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.794713418019" Y="-0.621449710121" />
                  <Point X="-21.195412341152" Y="-0.957676028735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.061594558393" Y="-1.684489207753" />
                  <Point X="-22.468244964577" Y="-2.0257094136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.987038938704" Y="-2.461029245948" />
                  <Point X="-23.333661511217" Y="-2.751880118701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.250049877175" Y="-3.520821258592" />
                  <Point X="-24.320320912519" Y="-3.579785658431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.682691558058" Y="-0.65146590123" />
                  <Point X="-21.067662211357" Y="-0.974494634427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.009804272614" Y="-1.765045690545" />
                  <Point X="-22.344077851113" Y="-2.045534526975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.995623291921" Y="-2.592246066052" />
                  <Point X="-23.249981726528" Y="-2.805678134718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.235347294512" Y="-3.632498019389" />
                  <Point X="-24.293191305024" Y="-3.681034907274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.570669698097" Y="-0.681482092339" />
                  <Point X="-20.939912081561" Y="-0.991313240119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.947044654144" Y="-1.83639781032" />
                  <Point X="-22.222863072082" Y="-2.067836943083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.976172949545" Y="-2.699938983426" />
                  <Point X="-23.174779577538" Y="-2.866589731724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.22064471185" Y="-3.744174780186" />
                  <Point X="-24.266061697529" Y="-3.782284156118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.458647838136" Y="-0.711498283449" />
                  <Point X="-20.812161951766" Y="-1.008131845812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.869846199345" Y="-1.895634307857" />
                  <Point X="-22.125185380054" Y="-2.109889320216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.956110879439" Y="-2.807118600285" />
                  <Point X="-23.116191162647" Y="-2.941441906884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.205942129187" Y="-3.855851540982" />
                  <Point X="-24.238932343182" Y="-3.883533617378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.346625978175" Y="-0.741514474558" />
                  <Point X="-20.68441182197" Y="-1.024950451504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.792647744546" Y="-1.954870805395" />
                  <Point X="-22.037632853363" Y="-2.160437719847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.916439651789" Y="-2.897844180282" />
                  <Point X="-23.058303868246" Y="-3.016882391989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.191239546524" Y="-3.967528301779" />
                  <Point X="-24.21180299057" Y="-3.984783080094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.234603981428" Y="-0.771530550891" />
                  <Point X="-20.556661692175" Y="-1.041769057196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.715449289747" Y="-2.014107302932" />
                  <Point X="-21.950080326671" Y="-2.210986119478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.868206942747" Y="-2.981385824401" />
                  <Point X="-23.000416573845" Y="-3.092322877094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.176536963861" Y="-4.079205062576" />
                  <Point X="-24.184673637959" Y="-4.086032542811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.232801366255" Y="-0.89403166965" />
                  <Point X="-20.428911562379" Y="-1.058587662889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.638250834947" Y="-2.073343800469" />
                  <Point X="-21.862527817688" Y="-2.261534533968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.819974233706" Y="-3.064927468521" />
                  <Point X="-22.942529211959" Y="-3.167763305571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.264258948807" Y="-1.044441408054" />
                  <Point X="-20.301161432584" Y="-1.075406268581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.561052380148" Y="-2.132580298006" />
                  <Point X="-21.774975355564" Y="-2.312082987777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.771741524664" Y="-3.14846911264" />
                  <Point X="-22.88464173441" Y="-3.243203636997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.483853925349" Y="-2.191816795543" />
                  <Point X="-21.68742289344" Y="-2.362631441587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.723508815623" Y="-3.232010756759" />
                  <Point X="-22.826754256861" Y="-3.318643968422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.40665547055" Y="-2.25105329308" />
                  <Point X="-21.599870431316" Y="-2.413179895397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.675276106581" Y="-3.315552400878" />
                  <Point X="-22.768866779312" Y="-3.394084299848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.329457015751" Y="-2.310289790617" />
                  <Point X="-21.512317969192" Y="-2.463728349207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.62704339754" Y="-3.399094044997" />
                  <Point X="-22.710979301763" Y="-3.469524631273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.252258560951" Y="-2.369526288154" />
                  <Point X="-21.424765507069" Y="-2.514276803017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.578810688498" Y="-3.482635689116" />
                  <Point X="-22.653091824214" Y="-3.544964962699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.175060106152" Y="-2.428762785691" />
                  <Point X="-21.337213044945" Y="-2.564825256826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.530577979457" Y="-3.566177333235" />
                  <Point X="-22.595204346665" Y="-3.620405294124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.097861651353" Y="-2.487999283228" />
                  <Point X="-21.249660582821" Y="-2.615373710636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.482345270415" Y="-3.649718977355" />
                  <Point X="-22.537316869117" Y="-3.69584562555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.020663196554" Y="-2.547235780765" />
                  <Point X="-21.162108120697" Y="-2.665922164446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.434112561374" Y="-3.733260621474" />
                  <Point X="-22.479429391568" Y="-3.771285956976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.943464780434" Y="-2.606472310759" />
                  <Point X="-21.074555658573" Y="-2.716470618256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.385879852332" Y="-3.816802265593" />
                  <Point X="-22.421541914019" Y="-3.846726288401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.337647143291" Y="-3.900343909712" />
                  <Point X="-22.36365443647" Y="-3.922166619827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.289414495497" Y="-3.983885605224" />
                  <Point X="-22.305766958921" Y="-3.997606951252" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.449404296875" Y="-3.832148925781" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.5679921875" Y="-3.474989013672" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.7756640625" Y="-3.250881591797" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.0586640625" Y="-3.203284912109" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643798828" />
                  <Point X="-25.32058984375" Y="-3.332198730469" />
                  <Point X="-25.452005859375" Y="-3.521544189453" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.78075390625" Y="-4.737065429688" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.86113671875" Y="-4.984477050781" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.157142578125" Y="-4.923426269531" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.318666015625" Y="-4.623323242188" />
                  <Point X="-26.3091484375" Y="-4.551039550781" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.321626953125" Y="-4.474705078125" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.43231640625" Y="-4.1622578125" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.710337890625" Y="-3.981758300781" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791259766" />
                  <Point X="-27.0407890625" Y="-4.007808105469" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.44024609375" Y="-4.392542480469" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.505345703125" Y="-4.384624023438" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-27.934619140625" Y="-4.106947753906" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.652947265625" Y="-2.883583007812" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597594238281" />
                  <Point X="-27.5139765625" Y="-2.568765625" />
                  <Point X="-27.53132421875" Y="-2.551416992188" />
                  <Point X="-27.560154296875" Y="-2.537198974609" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.626619140625" Y="-3.140990234375" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-28.88480078125" Y="-3.210923339844" />
                  <Point X="-29.161697265625" Y="-2.847135742188" />
                  <Point X="-29.218185546875" Y="-2.752415527344" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-28.42083984375" Y="-1.620388427734" />
                  <Point X="-28.163787109375" Y="-1.423144897461" />
                  <Point X="-28.1525390625" Y="-1.411084838867" />
                  <Point X="-28.14430078125" Y="-1.390141967773" />
                  <Point X="-28.1381171875" Y="-1.366265258789" />
                  <Point X="-28.13665234375" Y="-1.350050537109" />
                  <Point X="-28.14865625" Y="-1.321068603516" />
                  <Point X="-28.166384765625" Y="-1.307563354492" />
                  <Point X="-28.187640625" Y="-1.295052856445" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.52603515625" Y="-1.460576171875" />
                  <Point X="-29.80328125" Y="-1.497076416016" />
                  <Point X="-29.81909375" Y="-1.435176269531" />
                  <Point X="-29.927392578125" Y="-1.011188049316" />
                  <Point X="-29.9423359375" Y="-0.90670501709" />
                  <Point X="-29.99839453125" Y="-0.514742248535" />
                  <Point X="-28.85028515625" Y="-0.207106811523" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.536419921875" Y="-0.117624572754" />
                  <Point X="-28.514142578125" Y="-0.102163780212" />
                  <Point X="-28.5023203125" Y="-0.090641342163" />
                  <Point X="-28.4930703125" Y="-0.070014373779" />
                  <Point X="-28.4856484375" Y="-0.046101638794" />
                  <Point X="-28.483400390625" Y="-0.031286182404" />
                  <Point X="-28.48747265625" Y="-0.010575950623" />
                  <Point X="-28.4948984375" Y="0.013348590851" />
                  <Point X="-28.50232421875" Y="0.028088119507" />
                  <Point X="-28.519619140625" Y="0.043404026031" />
                  <Point X="-28.54189453125" Y="0.058864665985" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.74840625" Y="0.385197845459" />
                  <Point X="-29.998185546875" Y="0.452126037598" />
                  <Point X="-29.98744140625" Y="0.524735656738" />
                  <Point X="-29.91764453125" Y="0.996414733887" />
                  <Point X="-29.887560546875" Y="1.107433349609" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-28.963166015625" Y="1.421614379883" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.71958203125" Y="1.399688720703" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.651533203125" Y="1.426056152344" />
                  <Point X="-28.639119140625" Y="1.443786010742" />
                  <Point X="-28.634255859375" Y="1.455529052734" />
                  <Point X="-28.61447265625" Y="1.503290283203" />
                  <Point X="-28.610712890625" Y="1.52460546875" />
                  <Point X="-28.61631640625" Y="1.545512084961" />
                  <Point X="-28.622185546875" Y="1.556786621094" />
                  <Point X="-28.646056640625" Y="1.602641845703" />
                  <Point X="-28.65996875" Y="1.619221923828" />
                  <Point X="-29.343095703125" Y="2.143404785156" />
                  <Point X="-29.47610546875" Y="2.245466308594" />
                  <Point X="-29.431220703125" Y="2.322364257812" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-29.080326171875" Y="2.889432861328" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.288072265625" Y="3.001373046875" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.121630859375" Y="2.918957763672" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927404296875" />
                  <Point X="-28.001267578125" Y="2.939388183594" />
                  <Point X="-27.95252734375" Y="2.988127685547" />
                  <Point X="-27.9408984375" Y="3.006382324219" />
                  <Point X="-27.938072265625" Y="3.027841308594" />
                  <Point X="-27.939548828125" Y="3.044724609375" />
                  <Point X="-27.945556640625" Y="3.113390869141" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.25478125" Y="3.658350585938" />
                  <Point X="-28.30727734375" Y="3.749276855469" />
                  <Point X="-28.225232421875" Y="3.812180664062" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.62737109375" Y="4.244059082031" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.007291015625" Y="4.339005371094" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.932455078125" Y="4.26387890625" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.218491699219" />
                  <Point X="-26.813806640625" Y="4.22225" />
                  <Point X="-26.794236328125" Y="4.230357421875" />
                  <Point X="-26.7146328125" Y="4.263329589844" />
                  <Point X="-26.696904296875" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.294487792969" />
                  <Point X="-26.679712890625" Y="4.314691894531" />
                  <Point X="-26.653802734375" Y="4.396864257812" />
                  <Point X="-26.651916015625" Y="4.418426269531" />
                  <Point X="-26.68633203125" Y="4.679836425781" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.579587890625" Y="4.731854980469" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.8159453125" Y="4.921103027344" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.08027734375" Y="4.453232421875" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176269531" />
                  <Point X="-24.945546875" Y="4.310902832031" />
                  <Point X="-24.790439453125" Y="4.889772949219" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.673716796875" Y="4.981481445312" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-24.013916015625" Y="4.895175292969" />
                  <Point X="-23.491544921875" Y="4.76905859375" />
                  <Point X="-23.409828125" Y="4.739419433594" />
                  <Point X="-23.06896484375" Y="4.615785644531" />
                  <Point X="-22.98973828125" Y="4.578733886719" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.5847578125" Y="4.380540527344" />
                  <Point X="-22.26747265625" Y="4.195688964844" />
                  <Point X="-22.1952890625" Y="4.144357421875" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.6001015625" Y="2.798120361328" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.779384765625" Y="2.475670166016" />
                  <Point X="-22.7966171875" Y="2.411228271484" />
                  <Point X="-22.796302734375" Y="2.378625244141" />
                  <Point X="-22.789583984375" Y="2.322901367188" />
                  <Point X="-22.781318359375" Y="2.300812255859" />
                  <Point X="-22.77283984375" Y="2.288318359375" />
                  <Point X="-22.738359375" Y="2.237503662109" />
                  <Point X="-22.712564453125" Y="2.215726074219" />
                  <Point X="-22.66175" Y="2.181246337891" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.6259609375" Y="2.171328125" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.535490234375" Y="2.170183349609" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.261103515625" Y="2.884002685547" />
                  <Point X="-21.005751953125" Y="3.031430908203" />
                  <Point X="-20.985607421875" Y="3.003436523438" />
                  <Point X="-20.797404296875" Y="2.741876220703" />
                  <Point X="-20.7571640625" Y="2.675379638672" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.487505859375" Y="1.764869750977" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.73203125" Y="1.568956787109" />
                  <Point X="-21.77841015625" Y="1.508451904297" />
                  <Point X="-21.791126953125" Y="1.476311889648" />
                  <Point X="-21.808404296875" Y="1.414536376953" />
                  <Point X="-21.809220703125" Y="1.390965698242" />
                  <Point X="-21.805732421875" Y="1.374066040039" />
                  <Point X="-21.79155078125" Y="1.305332763672" />
                  <Point X="-21.774869140625" Y="1.273540161133" />
                  <Point X="-21.736296875" Y="1.21491003418" />
                  <Point X="-21.719052734375" Y="1.198819824219" />
                  <Point X="-21.70530859375" Y="1.191083251953" />
                  <Point X="-21.64941015625" Y="1.159617553711" />
                  <Point X="-21.612849609375" Y="1.151163696289" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.38630859375" Y="1.290977539062" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.141642578125" Y="1.283414672852" />
                  <Point X="-20.060806640625" Y="0.951367370605" />
                  <Point X="-20.04812890625" Y="0.869934143066" />
                  <Point X="-20.002140625" Y="0.574556396484" />
                  <Point X="-21.002263671875" Y="0.306573974609" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819503784" />
                  <Point X="-21.28916796875" Y="0.222266586304" />
                  <Point X="-21.363421875" Y="0.179346984863" />
                  <Point X="-21.388689453125" Y="0.152968048096" />
                  <Point X="-21.433240234375" Y="0.096198562622" />
                  <Point X="-21.443013671875" Y="0.07473449707" />
                  <Point X="-21.446666015625" Y="0.055668399811" />
                  <Point X="-21.461515625" Y="-0.021876060486" />
                  <Point X="-21.457865234375" Y="-0.059751335144" />
                  <Point X="-21.443013671875" Y="-0.137295791626" />
                  <Point X="-21.433240234375" Y="-0.158759399414" />
                  <Point X="-21.422287109375" Y="-0.172717590332" />
                  <Point X="-21.377734375" Y="-0.229487075806" />
                  <Point X="-21.363421875" Y="-0.24190675354" />
                  <Point X="-21.3451640625" Y="-0.252459671021" />
                  <Point X="-21.270912109375" Y="-0.295379272461" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.215337890625" Y="-0.579990478516" />
                  <Point X="-20.001931640625" Y="-0.637172424316" />
                  <Point X="-20.006435546875" Y="-0.667045410156" />
                  <Point X="-20.051568359375" Y="-0.966412475586" />
                  <Point X="-20.0678125" Y="-1.037596679688" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-21.290470703125" Y="-1.13680090332" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.640994140625" Y="-1.106129394531" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.814552734375" Y="-1.154697265625" />
                  <Point X="-21.8362109375" Y="-1.180745239258" />
                  <Point X="-21.924296875" Y="-1.286685424805" />
                  <Point X="-21.935640625" Y="-1.314070556641" />
                  <Point X="-21.938744140625" Y="-1.347803710938" />
                  <Point X="-21.951369140625" Y="-1.485000854492" />
                  <Point X="-21.943638671875" Y="-1.516621826172" />
                  <Point X="-21.92380859375" Y="-1.547465942383" />
                  <Point X="-21.843158203125" Y="-1.672912353516" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-20.86316796875" Y="-2.428596679688" />
                  <Point X="-20.660923828125" Y="-2.583783935547" />
                  <Point X="-20.668642578125" Y="-2.596272460938" />
                  <Point X="-20.795865234375" Y="-2.802138183594" />
                  <Point X="-20.82946875" Y="-2.849883544922" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.9815390625" Y="-2.412216552734" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.305302734375" Y="-2.245611083984" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.546349609375" Y="-2.237890380859" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.33468359375" />
                  <Point X="-22.730044921875" Y="-2.370111572266" />
                  <Point X="-22.80587890625" Y="-2.514201660156" />
                  <Point X="-22.8108359375" Y="-2.546374511719" />
                  <Point X="-22.8031328125" Y="-2.589020019531" />
                  <Point X="-22.77180859375" Y="-2.76246484375" />
                  <Point X="-22.76590625" Y="-2.778578857422" />
                  <Point X="-22.1436328125" Y="-3.856389892578" />
                  <Point X="-22.01332421875" Y="-4.082087646484" />
                  <Point X="-22.013740234375" Y="-4.082384033203" />
                  <Point X="-22.16470703125" Y="-4.190215820312" />
                  <Point X="-22.202267578125" Y="-4.214528320312" />
                  <Point X="-22.32022265625" Y="-4.29087890625" />
                  <Point X="-23.11459765625" Y="-3.255630126953" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.371509765625" Y="-2.953426513672" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.6201953125" Y="-2.839949951172" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.870185546875" Y="-2.898042480469" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045985839844" />
                  <Point X="-24.042162109375" Y="-3.094847412109" />
                  <Point X="-24.085357421875" Y="-3.293573730469" />
                  <Point X="-24.0860703125" Y="-3.310719970703" />
                  <Point X="-23.909720703125" Y="-4.65021484375" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-24.005650390625" Y="-4.96324609375" />
                  <Point X="-24.040361328125" Y="-4.969552246094" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#136" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.038136890874" Y="4.497453295221" Z="0.5" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.5" />
                  <Point X="-0.827915273868" Y="5.00204409515" Z="0.5" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.5" />
                  <Point X="-1.599242271024" Y="4.811278434743" Z="0.5" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.5" />
                  <Point X="-1.742061162791" Y="4.704590751138" Z="0.5" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.5" />
                  <Point X="-1.733554822718" Y="4.361008135229" Z="0.5" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.5" />
                  <Point X="-1.819527040381" Y="4.307831638657" Z="0.5" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.5" />
                  <Point X="-1.915524258653" Y="4.339509307679" Z="0.5" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.5" />
                  <Point X="-1.973780258798" Y="4.400723208351" Z="0.5" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.5" />
                  <Point X="-2.657810906068" Y="4.319046421952" Z="0.5" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.5" />
                  <Point X="-3.261810535445" Y="3.883140741479" Z="0.5" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.5" />
                  <Point X="-3.304239630436" Y="3.66463054233" Z="0.5" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.5" />
                  <Point X="-2.995517115832" Y="3.071646754582" Z="0.5" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.5" />
                  <Point X="-3.042779740196" Y="3.00602382769" Z="0.5" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.5" />
                  <Point X="-3.123429711025" Y="3.000047583341" Z="0.5" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.5" />
                  <Point X="-3.269228805968" Y="3.07595431831" Z="0.5" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.5" />
                  <Point X="-4.125946930877" Y="2.951415245782" Z="0.5" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.5" />
                  <Point X="-4.48055922162" Y="2.378849488806" Z="0.5" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.5" />
                  <Point X="-4.379690835413" Y="2.135017031883" Z="0.5" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.5" />
                  <Point X="-3.672691605992" Y="1.56497895798" Z="0.5" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.5" />
                  <Point X="-3.686605718933" Y="1.505943301832" Z="0.5" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.5" />
                  <Point X="-3.740773592902" Y="1.478654733526" Z="0.5" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.5" />
                  <Point X="-3.96279791781" Y="1.502466645589" Z="0.5" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.5" />
                  <Point X="-4.94197802241" Y="1.151790644736" Z="0.5" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.5" />
                  <Point X="-5.043059627374" Y="0.563334640107" Z="0.5" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.5" />
                  <Point X="-4.767505181791" Y="0.368181639541" Z="0.5" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.5" />
                  <Point X="-3.554284900563" Y="0.033608330389" Z="0.5" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.5" />
                  <Point X="-3.541382453463" Y="0.005882413615" Z="0.5" />
                  <Point X="-3.539556741714" Y="0" Z="0.5" />
                  <Point X="-3.546982147672" Y="-0.023924537446" Z="0.5" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.5" />
                  <Point X="-3.57108369456" Y="-0.045267652745" Z="0.5" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.5" />
                  <Point X="-3.869382606982" Y="-0.127530417215" Z="0.5" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.5" />
                  <Point X="-4.997989007571" Y="-0.882504063647" Z="0.5" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.5" />
                  <Point X="-4.87369529821" Y="-1.416270307696" Z="0.5" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.5" />
                  <Point X="-4.525667313974" Y="-1.478868372375" Z="0.5" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.5" />
                  <Point X="-3.197902653851" Y="-1.319373802303" Z="0.5" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.5" />
                  <Point X="-3.198860316737" Y="-1.346326515599" Z="0.5" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.5" />
                  <Point X="-3.457433600999" Y="-1.54944071182" Z="0.5" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.5" />
                  <Point X="-4.267286242335" Y="-2.74674503476" Z="0.5" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.5" />
                  <Point X="-3.930742362975" Y="-3.209926574513" Z="0.5" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.5" />
                  <Point X="-3.607775403804" Y="-3.153011485823" Z="0.5" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.5" />
                  <Point X="-2.558914709476" Y="-2.569415759656" Z="0.5" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.5" />
                  <Point X="-2.702405527345" Y="-2.827302978688" Z="0.5" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.5" />
                  <Point X="-2.971280736256" Y="-4.115284823655" Z="0.5" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.5" />
                  <Point X="-2.537825181713" Y="-4.395854482605" Z="0.5" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.5" />
                  <Point X="-2.406734542332" Y="-4.391700261423" Z="0.5" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.5" />
                  <Point X="-2.019165744383" Y="-4.018101223664" Z="0.5" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.5" />
                  <Point X="-1.71976439021" Y="-4.000371385834" Z="0.5" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.5" />
                  <Point X="-1.471440104372" Y="-4.168569606677" Z="0.5" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.5" />
                  <Point X="-1.37682365033" Y="-4.453180138481" Z="0.5" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.5" />
                  <Point X="-1.374394873083" Y="-4.58551596202" Z="0.5" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.5" />
                  <Point X="-1.175757800425" Y="-4.940569090611" Z="0.5" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.5" />
                  <Point X="-0.876820656357" Y="-5.002281227471" Z="0.5" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.5" />
                  <Point X="-0.73861320183" Y="-4.718726106664" Z="0.5" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.5" />
                  <Point X="-0.28567080604" Y="-3.329427714769" Z="0.5" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.5" />
                  <Point X="-0.050000514809" Y="-3.219757683044" Z="0.5" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.5" />
                  <Point X="0.203358564552" Y="-3.267354362244" Z="0.5" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.5" />
                  <Point X="0.384775053392" Y="-3.472218040999" Z="0.5" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.5" />
                  <Point X="0.496141658571" Y="-3.813809923152" Z="0.5" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.5" />
                  <Point X="0.96241957787" Y="-4.987466886689" Z="0.5" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.5" />
                  <Point X="1.141718803913" Y="-4.94950052471" Z="0.5" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.5" />
                  <Point X="1.133693673411" Y="-4.612408742999" Z="0.5" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.5" />
                  <Point X="1.00053975596" Y="-3.074187540582" Z="0.5" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.5" />
                  <Point X="1.155623889634" Y="-2.90520903992" Z="0.5" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.5" />
                  <Point X="1.378230702624" Y="-2.858459528383" Z="0.5" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.5" />
                  <Point X="1.595293901527" Y="-2.964204578815" Z="0.5" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.5" />
                  <Point X="1.83957742246" Y="-3.254788045243" Z="0.5" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.5" />
                  <Point X="2.818745470965" Y="-4.225222505864" Z="0.5" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.5" />
                  <Point X="3.00916620709" Y="-4.091790636511" Z="0.5" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.5" />
                  <Point X="2.893511635416" Y="-3.800109635104" Z="0.5" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.5" />
                  <Point X="2.239912866837" Y="-2.548853715577" Z="0.5" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.5" />
                  <Point X="2.308044712285" Y="-2.362118142417" Z="0.5" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.5" />
                  <Point X="2.470780235955" Y="-2.250856597682" Z="0.5" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.5" />
                  <Point X="2.679653087935" Y="-2.263535142654" Z="0.5" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.5" />
                  <Point X="2.987303962003" Y="-2.424237864858" Z="0.5" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.5" />
                  <Point X="4.205263436533" Y="-2.847380944181" Z="0.5" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.5" />
                  <Point X="4.367733862343" Y="-2.591277640809" Z="0.5" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.5" />
                  <Point X="4.161112057066" Y="-2.35764897496" Z="0.5" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.5" />
                  <Point X="3.112092337112" Y="-1.489146496182" Z="0.5" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.5" />
                  <Point X="3.104887342365" Y="-1.321105387354" Z="0.5" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.5" />
                  <Point X="3.196077492814" Y="-1.181432076666" Z="0.5" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.5" />
                  <Point X="3.363468067248" Y="-1.123708612849" Z="0.5" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.5" />
                  <Point X="3.696846410924" Y="-1.155093168082" Z="0.5" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.5" />
                  <Point X="4.974775769551" Y="-1.017440652959" Z="0.5" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.5" />
                  <Point X="5.036849677771" Y="-0.64321929892" Z="0.5" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.5" />
                  <Point X="4.791447283777" Y="-0.500414155159" Z="0.5" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.5" />
                  <Point X="3.67369959692" Y="-0.177891012539" Z="0.5" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.5" />
                  <Point X="3.610890853046" Y="-0.110568666154" Z="0.5" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.5" />
                  <Point X="3.585086103161" Y="-0.019066126687" Z="0.5" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.5" />
                  <Point X="3.596285347263" Y="0.077544404547" Z="0.5" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.5" />
                  <Point X="3.644488585354" Y="0.153380072421" Z="0.5" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.5" />
                  <Point X="3.729695817433" Y="0.210257831703" Z="0.5" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.5" />
                  <Point X="4.004520533529" Y="0.28955777761" Z="0.5" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.5" />
                  <Point X="4.995118716369" Y="0.908906246445" Z="0.5" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.5" />
                  <Point X="4.900782471628" Y="1.326521463902" Z="0.5" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.5" />
                  <Point X="4.601008902438" Y="1.371829859051" Z="0.5" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.5" />
                  <Point X="3.387543289257" Y="1.23201265304" Z="0.5" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.5" />
                  <Point X="3.313062441555" Y="1.265934411604" Z="0.5" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.5" />
                  <Point X="3.260745173634" Y="1.332300870633" Z="0.5" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.5" />
                  <Point X="3.23707901014" Y="1.415449537762" Z="0.5" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.5" />
                  <Point X="3.25086822531" Y="1.49412472846" Z="0.5" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.5" />
                  <Point X="3.301494824235" Y="1.569818551949" Z="0.5" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.5" />
                  <Point X="3.53677515648" Y="1.756481957005" Z="0.5" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.5" />
                  <Point X="4.279455449826" Y="2.73254584825" Z="0.5" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.5" />
                  <Point X="4.048820622612" Y="3.063919496717" Z="0.5" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.5" />
                  <Point X="3.707738737099" Y="2.958583993521" Z="0.5" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.5" />
                  <Point X="2.445436181861" Y="2.249766207509" Z="0.5" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.5" />
                  <Point X="2.373867813266" Y="2.25224843809" Z="0.5" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.5" />
                  <Point X="2.30935207246" Y="2.288380383869" Z="0.5" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.5" />
                  <Point X="2.262378216366" Y="2.347672787922" Z="0.5" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.5" />
                  <Point X="2.247181264669" Y="2.415890630567" Z="0.5" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.5" />
                  <Point X="2.262761735149" Y="2.494033394021" Z="0.5" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.5" />
                  <Point X="2.437041359339" Y="2.804400344856" Z="0.5" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.5" />
                  <Point X="2.827529645801" Y="4.216384670466" Z="0.5" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.5" />
                  <Point X="2.434255903581" Y="4.455023092266" Z="0.5" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.5" />
                  <Point X="2.025286609756" Y="4.65530602659" Z="0.5" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.5" />
                  <Point X="1.601064153245" Y="4.817703141869" Z="0.5" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.5" />
                  <Point X="0.991661131727" Y="4.975058654269" Z="0.5" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.5" />
                  <Point X="0.32533396494" Y="5.062489744039" Z="0.5" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.5" />
                  <Point X="0.155107670679" Y="4.93399421073" Z="0.5" />
                  <Point X="0" Y="4.355124473572" Z="0.5" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>