<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#187" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2615" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.16050390625" Y="-4.54328515625" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140625" />
                  <Point X="-24.457634765625" Y="-3.467376708984" />
                  <Point X="-24.570521484375" Y="-3.304728759766" />
                  <Point X="-24.62136328125" Y="-3.231476318359" />
                  <Point X="-24.643248046875" Y="-3.209020263672" />
                  <Point X="-24.66950390625" Y="-3.189776611328" />
                  <Point X="-24.69750390625" Y="-3.175669189453" />
                  <Point X="-24.872189453125" Y="-3.121453369141" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097036132812" />
                  <Point X="-25.2115078125" Y="-3.151251708984" />
                  <Point X="-25.29018359375" Y="-3.175669433594" />
                  <Point X="-25.31818359375" Y="-3.189776855469" />
                  <Point X="-25.344439453125" Y="-3.209020751953" />
                  <Point X="-25.36632421875" Y="-3.231477294922" />
                  <Point X="-25.479208984375" Y="-3.394125" />
                  <Point X="-25.53005078125" Y="-3.467377685547" />
                  <Point X="-25.5381875" Y="-3.481572021484" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.675380859375" Y="-3.97675390625" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-25.990541015625" Y="-4.862586425781" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.24641796875" Y="-4.802362792969" />
                  <Point X="-26.2149609375" Y="-4.563437011719" />
                  <Point X="-26.21419921875" Y="-4.547929199219" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.258240234375" Y="-4.306421386719" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.182963867188" />
                  <Point X="-26.30401171875" Y="-4.155126953125" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.484470703125" Y="-3.990161621094" />
                  <Point X="-26.556904296875" Y="-3.926639404297" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.856482421875" Y="-3.876975830078" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708740234" />
                  <Point X="-27.014466796875" Y="-3.882028564453" />
                  <Point X="-27.042658203125" Y="-3.894801757812" />
                  <Point X="-27.22051953125" Y="-4.013645263672" />
                  <Point X="-27.300623046875" Y="-4.067169677734" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.3351015625" Y="-4.099461425781" />
                  <Point X="-27.4049375" Y="-4.190472167969" />
                  <Point X="-27.4801484375" Y="-4.288488769531" />
                  <Point X="-27.671552734375" Y="-4.169975585938" />
                  <Point X="-27.801712890625" Y="-4.089384033203" />
                  <Point X="-28.0781640625" Y="-3.876525878906" />
                  <Point X="-28.104720703125" Y="-3.856077392578" />
                  <Point X="-27.93703125" Y="-3.565629882812" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647655273438" />
                  <Point X="-27.406587890625" Y="-2.616130126953" />
                  <Point X="-27.40557421875" Y="-2.585197998047" />
                  <Point X="-27.414556640625" Y="-2.555581542969" />
                  <Point X="-27.428771484375" Y="-2.526752929688" />
                  <Point X="-27.446798828125" Y="-2.501592529297" />
                  <Point X="-27.464146484375" Y="-2.484243896484" />
                  <Point X="-27.4893046875" Y="-2.466215576172" />
                  <Point X="-27.518134765625" Y="-2.451997070313" />
                  <Point X="-27.547751953125" Y="-2.44301171875" />
                  <Point X="-27.5786875" Y="-2.444023925781" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.039572265625" Y="-2.692362060547" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-28.98003125" Y="-2.928956787109" />
                  <Point X="-29.082859375" Y="-2.793861328125" />
                  <Point X="-29.2810546875" Y="-2.461517089844" />
                  <Point X="-29.306142578125" Y="-2.419449462891" />
                  <Point X="-29.00401171875" Y="-2.187616699219" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.084578125" Y="-1.47559375" />
                  <Point X="-28.06661328125" Y="-1.448462890625" />
                  <Point X="-28.053857421875" Y="-1.419834838867" />
                  <Point X="-28.048544921875" Y="-1.399324829102" />
                  <Point X="-28.04615234375" Y="-1.390087524414" />
                  <Point X="-28.04334765625" Y="-1.35965637207" />
                  <Point X="-28.045556640625" Y="-1.327986572266" />
                  <Point X="-28.052556640625" Y="-1.298244262695" />
                  <Point X="-28.06863671875" Y="-1.272262573242" />
                  <Point X="-28.089466796875" Y="-1.248306152344" />
                  <Point X="-28.112970703125" Y="-1.228767700195" />
                  <Point X="-28.13123046875" Y="-1.218020996094" />
                  <Point X="-28.139453125" Y="-1.213180908203" />
                  <Point X="-28.168712890625" Y="-1.201957885742" />
                  <Point X="-28.2006015625" Y="-1.195475219727" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-28.737380859375" Y="-1.260927856445" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.793857421875" Y="-1.15010559082" />
                  <Point X="-29.834078125" Y="-0.99265057373" />
                  <Point X="-29.886513671875" Y="-0.626016723633" />
                  <Point X="-29.892423828125" Y="-0.584698364258" />
                  <Point X="-29.555896484375" Y="-0.494526184082" />
                  <Point X="-28.532875" Y="-0.220408279419" />
                  <Point X="-28.5174921875" Y="-0.214827850342" />
                  <Point X="-28.48773046875" Y="-0.199471069336" />
                  <Point X="-28.468595703125" Y="-0.186190475464" />
                  <Point X="-28.4599765625" Y="-0.180209030151" />
                  <Point X="-28.4375234375" Y="-0.158326782227" />
                  <Point X="-28.418275390625" Y="-0.132066894531" />
                  <Point X="-28.4041640625" Y="-0.104058685303" />
                  <Point X="-28.397787109375" Y="-0.083507484436" />
                  <Point X="-28.394916015625" Y="-0.074256622314" />
                  <Point X="-28.3906484375" Y="-0.046108921051" />
                  <Point X="-28.390646484375" Y="-0.016468515396" />
                  <Point X="-28.3949140625" Y="0.011691631317" />
                  <Point X="-28.401291015625" Y="0.032242832184" />
                  <Point X="-28.404169921875" Y="0.041516765594" />
                  <Point X="-28.418279296875" Y="0.069514343262" />
                  <Point X="-28.437525390625" Y="0.095769523621" />
                  <Point X="-28.4599765625" Y="0.117649040222" />
                  <Point X="-28.479111328125" Y="0.130929641724" />
                  <Point X="-28.487765625" Y="0.136272659302" />
                  <Point X="-28.51194921875" Y="0.14947265625" />
                  <Point X="-28.532875" Y="0.157848297119" />
                  <Point X="-28.993619140625" Y="0.281304260254" />
                  <Point X="-29.89181640625" Y="0.521975463867" />
                  <Point X="-29.85040625" Y="0.801819335938" />
                  <Point X="-29.82448828125" Y="0.976968200684" />
                  <Point X="-29.718931640625" Y="1.366505371094" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-29.4989921875" Y="1.396337280273" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228149414" />
                  <Point X="-28.70313671875" Y="1.305263427734" />
                  <Point X="-28.66078515625" Y="1.31861706543" />
                  <Point X="-28.6417109375" Y="1.324631225586" />
                  <Point X="-28.6227734375" Y="1.332963745117" />
                  <Point X="-28.604029296875" Y="1.343786865234" />
                  <Point X="-28.5873515625" Y="1.356016845703" />
                  <Point X="-28.57371484375" Y="1.37156640625" />
                  <Point X="-28.561298828125" Y="1.389297851562" />
                  <Point X="-28.551349609375" Y="1.407433227539" />
                  <Point X="-28.53435546875" Y="1.448460083008" />
                  <Point X="-28.526703125" Y="1.4669375" />
                  <Point X="-28.520912109375" Y="1.486807495117" />
                  <Point X="-28.51715625" Y="1.508122924805" />
                  <Point X="-28.515806640625" Y="1.528757202148" />
                  <Point X="-28.518951171875" Y="1.549194946289" />
                  <Point X="-28.524552734375" Y="1.57010144043" />
                  <Point X="-28.532048828125" Y="1.58937890625" />
                  <Point X="-28.552552734375" Y="1.628768554688" />
                  <Point X="-28.5617890625" Y="1.646508666992" />
                  <Point X="-28.573287109375" Y="1.663714233398" />
                  <Point X="-28.58719921875" Y="1.680291748047" />
                  <Point X="-28.60213671875" Y="1.694590087891" />
                  <Point X="-28.866419921875" Y="1.897382446289" />
                  <Point X="-29.351859375" Y="2.269873291016" />
                  <Point X="-29.181861328125" Y="2.561120849609" />
                  <Point X="-29.0811484375" Y="2.733664794922" />
                  <Point X="-28.80154296875" Y="3.093059570312" />
                  <Point X="-28.75050390625" Y="3.158662353516" />
                  <Point X="-28.657044921875" Y="3.104703369141" />
                  <Point X="-28.20665625" Y="2.844671142578" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.08780859375" Y="2.820635986328" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.99901171875" Y="2.826504882812" />
                  <Point X="-27.9804609375" Y="2.835654541016" />
                  <Point X="-27.96220703125" Y="2.847284179688" />
                  <Point X="-27.946078125" Y="2.860228515625" />
                  <Point X="-27.904208984375" Y="2.902095947266" />
                  <Point X="-27.885353515625" Y="2.920951904297" />
                  <Point X="-27.872404296875" Y="2.937086181641" />
                  <Point X="-27.860775390625" Y="2.955340576172" />
                  <Point X="-27.85162890625" Y="2.973889404297" />
                  <Point X="-27.8467109375" Y="2.993978027344" />
                  <Point X="-27.843884765625" Y="3.015436523438" />
                  <Point X="-27.84343359375" Y="3.036120605469" />
                  <Point X="-27.84859375" Y="3.095104980469" />
                  <Point X="-27.85091796875" Y="3.121670166016" />
                  <Point X="-27.854955078125" Y="3.141963378906" />
                  <Point X="-27.86146484375" Y="3.16260546875" />
                  <Point X="-27.869794921875" Y="3.181533203125" />
                  <Point X="-27.986908203125" Y="3.384377441406" />
                  <Point X="-28.18333203125" Y="3.724595703125" />
                  <Point X="-27.876021484375" Y="3.960207519531" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.26025" Y="4.339347167969" />
                  <Point X="-27.16703515625" Y="4.391134765625" />
                  <Point X="-27.0431953125" Y="4.229741210938" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.99511328125" Y="4.189395019531" />
                  <Point X="-26.92946484375" Y="4.155220214844" />
                  <Point X="-26.899896484375" Y="4.139828125" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.797314453125" Y="4.128692871094" />
                  <Point X="-26.777453125" Y="4.134481933594" />
                  <Point X="-26.709076171875" Y="4.162805664062" />
                  <Point X="-26.678279296875" Y="4.175561523437" />
                  <Point X="-26.660146484375" Y="4.185509277344" />
                  <Point X="-26.642416015625" Y="4.197923828125" />
                  <Point X="-26.62686328125" Y="4.2115625" />
                  <Point X="-26.6146328125" Y="4.228244140625" />
                  <Point X="-26.603810546875" Y="4.246988769531" />
                  <Point X="-26.595478515625" Y="4.265921875" />
                  <Point X="-26.57322265625" Y="4.336508300781" />
                  <Point X="-26.56319921875" Y="4.368298339844" />
                  <Point X="-26.5591640625" Y="4.388583984375" />
                  <Point X="-26.55727734375" Y="4.410145507812" />
                  <Point X="-26.557728515625" Y="4.430826660156" />
                  <Point X="-26.57104296875" Y="4.531959472656" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.176697265625" Y="4.746147949219" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.41576953125" Y="4.872289550781" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.267140625" Y="4.783564941406" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.793775390625" Y="4.510264648438" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.354224609375" Y="4.852502441406" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.7142734375" Y="4.725102539062" />
                  <Point X="-23.51897265625" Y="4.677950195312" />
                  <Point X="-23.2319375" Y="4.573841796875" />
                  <Point X="-23.105353515625" Y="4.527928710938" />
                  <Point X="-22.827359375" Y="4.397919433594" />
                  <Point X="-22.705423828125" Y="4.340893554688" />
                  <Point X="-22.43684375" Y="4.184419433594" />
                  <Point X="-22.319021484375" Y="4.115775878906" />
                  <Point X="-22.065736328125" Y="3.935653076172" />
                  <Point X="-22.05673828125" Y="3.929253662109" />
                  <Point X="-22.25834375" Y="3.580060791016" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857919921875" Y="2.539938720703" />
                  <Point X="-22.866921875" Y="2.516058349609" />
                  <Point X="-22.881724609375" Y="2.460702880859" />
                  <Point X="-22.888392578125" Y="2.435771972656" />
                  <Point X="-22.891380859375" Y="2.417936767578" />
                  <Point X="-22.892271484375" Y="2.380953613281" />
                  <Point X="-22.8865" Y="2.333086914062" />
                  <Point X="-22.883900390625" Y="2.311528808594" />
                  <Point X="-22.878556640625" Y="2.289600341797" />
                  <Point X="-22.8702890625" Y="2.267510986328" />
                  <Point X="-22.859927734375" Y="2.247469482422" />
                  <Point X="-22.83030859375" Y="2.203819580078" />
                  <Point X="-22.81696875" Y="2.184160888672" />
                  <Point X="-22.80553125" Y="2.170326660156" />
                  <Point X="-22.7783984375" Y="2.145592285156" />
                  <Point X="-22.73475" Y="2.115973876953" />
                  <Point X="-22.71508984375" Y="2.102634765625" />
                  <Point X="-22.69504296875" Y="2.092270996094" />
                  <Point X="-22.67295703125" Y="2.084006103516" />
                  <Point X="-22.65103515625" Y="2.078663330078" />
                  <Point X="-22.60316796875" Y="2.072891357422" />
                  <Point X="-22.581611328125" Y="2.070291748047" />
                  <Point X="-22.563533203125" Y="2.069845458984" />
                  <Point X="-22.52679296875" Y="2.074170898438" />
                  <Point X="-22.4714375" Y="2.088973876953" />
                  <Point X="-22.446505859375" Y="2.095640625" />
                  <Point X="-22.434716796875" Y="2.099637939453" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.948044921875" Y="2.377700683594" />
                  <Point X="-21.032671875" Y="2.90619140625" />
                  <Point X="-20.946615234375" Y="2.786592285156" />
                  <Point X="-20.876732421875" Y="2.689469238281" />
                  <Point X="-20.73780078125" Y="2.459883056641" />
                  <Point X="-20.9870625" Y="2.2686171875" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.778576171875" Y="1.660239379883" />
                  <Point X="-21.796025390625" Y="1.641626708984" />
                  <Point X="-21.835865234375" Y="1.589653198242" />
                  <Point X="-21.853806640625" Y="1.566245605469" />
                  <Point X="-21.863390625" Y="1.550916748047" />
                  <Point X="-21.878369140625" Y="1.517089111328" />
                  <Point X="-21.893208984375" Y="1.464023803711" />
                  <Point X="-21.89989453125" Y="1.440124511719" />
                  <Point X="-21.90334765625" Y="1.417827148438" />
                  <Point X="-21.9041640625" Y="1.39425390625" />
                  <Point X="-21.902259765625" Y="1.371766601562" />
                  <Point X="-21.890076171875" Y="1.312724731445" />
                  <Point X="-21.88458984375" Y="1.286133789062" />
                  <Point X="-21.8793203125" Y="1.268979003906" />
                  <Point X="-21.863716796875" Y="1.235741821289" />
                  <Point X="-21.83058203125" Y="1.185378662109" />
                  <Point X="-21.81566015625" Y="1.162696411133" />
                  <Point X="-21.801109375" Y="1.145453613281" />
                  <Point X="-21.783865234375" Y="1.129362792969" />
                  <Point X="-21.76565234375" Y="1.116034790039" />
                  <Point X="-21.717634765625" Y="1.089005493164" />
                  <Point X="-21.696009765625" Y="1.07683215332" />
                  <Point X="-21.679478515625" Y="1.069501342773" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.578958984375" Y="1.050858398438" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-21.071578125" Y="1.104940307617" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.18408203125" Y="1.056110229492" />
                  <Point X="-20.15405859375" Y="0.932784851074" />
                  <Point X="-20.10956640625" Y="0.647012756348" />
                  <Point X="-20.109134765625" Y="0.644238586426" />
                  <Point X="-20.387056640625" Y="0.569769348145" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.2952109375" Y="0.325585754395" />
                  <Point X="-21.318453125" Y="0.315067901611" />
                  <Point X="-21.382236328125" Y="0.278199981689" />
                  <Point X="-21.410962890625" Y="0.261595489502" />
                  <Point X="-21.425685546875" Y="0.251098571777" />
                  <Point X="-21.45246875" Y="0.225576690674" />
                  <Point X="-21.49073828125" Y="0.176811462402" />
                  <Point X="-21.507974609375" Y="0.154848907471" />
                  <Point X="-21.51969921875" Y="0.135569107056" />
                  <Point X="-21.52947265625" Y="0.114105949402" />
                  <Point X="-21.536318359375" Y="0.09260408783" />
                  <Point X="-21.54907421875" Y="0.025993318558" />
                  <Point X="-21.5548203125" Y="-0.00400637579" />
                  <Point X="-21.556515625" Y="-0.021874074936" />
                  <Point X="-21.5548203125" Y="-0.058553760529" />
                  <Point X="-21.542064453125" Y="-0.125164375305" />
                  <Point X="-21.536318359375" Y="-0.155164077759" />
                  <Point X="-21.52947265625" Y="-0.176665939331" />
                  <Point X="-21.51969921875" Y="-0.198129089355" />
                  <Point X="-21.507974609375" Y="-0.217409194946" />
                  <Point X="-21.469705078125" Y="-0.266174133301" />
                  <Point X="-21.45246875" Y="-0.28813684082" />
                  <Point X="-21.439998046875" Y="-0.301238769531" />
                  <Point X="-21.410962890625" Y="-0.324155609131" />
                  <Point X="-21.3471796875" Y="-0.361023529053" />
                  <Point X="-21.318453125" Y="-0.37762802124" />
                  <Point X="-21.307291015625" Y="-0.383138305664" />
                  <Point X="-21.283419921875" Y="-0.392149963379" />
                  <Point X="-20.87971875" Y="-0.500321075439" />
                  <Point X="-20.108525390625" Y="-0.706961914062" />
                  <Point X="-20.12821484375" Y="-0.837559020996" />
                  <Point X="-20.144974609375" Y="-0.94872454834" />
                  <Point X="-20.19882421875" Y="-1.18469921875" />
                  <Point X="-20.53608984375" Y="-1.140297485352" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710266113" />
                  <Point X="-21.62533984375" Y="-1.005508666992" />
                  <Point X="-21.7505234375" Y="-1.032718139648" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.836025390625" Y="-1.056597412109" />
                  <Point X="-21.8638515625" Y="-1.073489868164" />
                  <Point X="-21.8876015625" Y="-1.093960083008" />
                  <Point X="-21.963267578125" Y="-1.184962890625" />
                  <Point X="-21.997345703125" Y="-1.225948242188" />
                  <Point X="-22.01206640625" Y="-1.250330688477" />
                  <Point X="-22.02341015625" Y="-1.277717163086" />
                  <Point X="-22.030240234375" Y="-1.305366088867" />
                  <Point X="-22.0410859375" Y="-1.423218505859" />
                  <Point X="-22.04596875" Y="-1.476296386719" />
                  <Point X="-22.043650390625" Y="-1.507568237305" />
                  <Point X="-22.03591796875" Y="-1.539188964844" />
                  <Point X="-22.023546875" Y="-1.567996948242" />
                  <Point X="-21.95426953125" Y="-1.675755615234" />
                  <Point X="-21.92306640625" Y="-1.724287475586" />
                  <Point X="-21.913064453125" Y="-1.737239868164" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.514734375" Y="-2.048377685547" />
                  <Point X="-20.786876953125" Y="-2.606882568359" />
                  <Point X="-20.827943359375" Y="-2.673335205078" />
                  <Point X="-20.87519921875" Y="-2.749799316406" />
                  <Point X="-20.971017578125" Y="-2.885945556641" />
                  <Point X="-21.273205078125" Y="-2.711477539062" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.2457734375" Y="-2.159825195312" />
                  <Point X="-22.39476171875" Y="-2.13291796875" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135176757812" />
                  <Point X="-22.678939453125" Y="-2.200317871094" />
                  <Point X="-22.73468359375" Y="-2.229655761719" />
                  <Point X="-22.757615234375" Y="-2.246550292969" />
                  <Point X="-22.77857421875" Y="-2.267510253906" />
                  <Point X="-22.795466796875" Y="-2.290438720703" />
                  <Point X="-22.860609375" Y="-2.414212158203" />
                  <Point X="-22.889947265625" Y="-2.469956787109" />
                  <Point X="-22.899771484375" Y="-2.499735595703" />
                  <Point X="-22.904728515625" Y="-2.531908691406" />
                  <Point X="-22.90432421875" Y="-2.563259277344" />
                  <Point X="-22.877416015625" Y="-2.712248535156" />
                  <Point X="-22.865296875" Y="-2.779349609375" />
                  <Point X="-22.86101171875" Y="-2.795140869141" />
                  <Point X="-22.8481796875" Y="-2.826078369141" />
                  <Point X="-22.607439453125" Y="-3.2430546875" />
                  <Point X="-22.13871484375" Y="-4.054907226563" />
                  <Point X="-22.162083984375" Y="-4.071598632812" />
                  <Point X="-22.218150390625" Y="-4.111644042969" />
                  <Point X="-22.298232421875" Y="-4.163481445312" />
                  <Point X="-22.534951171875" Y="-3.854985351562" />
                  <Point X="-23.241451171875" Y="-2.934254882812" />
                  <Point X="-23.25249609375" Y="-2.922177490234" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.42501953125" Y="-2.8060859375" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.520009765625" Y="-2.751167236328" />
                  <Point X="-23.551630859375" Y="-2.743435546875" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.743607421875" Y="-2.755905273438" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.843634765625" Y="-2.769396972656" />
                  <Point X="-23.87101953125" Y="-2.780739746094" />
                  <Point X="-23.89540234375" Y="-2.7954609375" />
                  <Point X="-24.01949609375" Y="-2.898641357422" />
                  <Point X="-24.07538671875" Y="-2.945111328125" />
                  <Point X="-24.095857421875" Y="-2.968861328125" />
                  <Point X="-24.11275" Y="-2.996688232422" />
                  <Point X="-24.124375" Y="-3.02580859375" />
                  <Point X="-24.161478515625" Y="-3.196514648438" />
                  <Point X="-24.178189453125" Y="-3.273396484375" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323120361328" />
                  <Point X="-24.112033203125" Y="-3.841334960938" />
                  <Point X="-23.977935546875" Y="-4.859915527344" />
                  <Point X="-24.024318359375" Y="-4.870083007812" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.058419921875" Y="-4.752637695312" />
                  <Point X="-26.14124609375" Y="-4.731327636719" />
                  <Point X="-26.1207734375" Y="-4.575837890625" />
                  <Point X="-26.120076171875" Y="-4.56809765625" />
                  <Point X="-26.11944921875" Y="-4.541030273438" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497689453125" />
                  <Point X="-26.16506640625" Y="-4.287887695313" />
                  <Point X="-26.18386328125" Y="-4.193397949219" />
                  <Point X="-26.188125" Y="-4.17846875" />
                  <Point X="-26.19902734375" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135462402344" />
                  <Point X="-26.221740234375" Y="-4.107625488281" />
                  <Point X="-26.230576171875" Y="-4.094859863281" />
                  <Point X="-26.250208984375" Y="-4.070937011719" />
                  <Point X="-26.261005859375" Y="-4.059779785156" />
                  <Point X="-26.42183203125" Y="-3.918737304688" />
                  <Point X="-26.494265625" Y="-3.855215087891" />
                  <Point X="-26.50673828125" Y="-3.845965332031" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.85026953125" Y="-3.782179199219" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166259766" />
                  <Point X="-27.008005859375" Y="-3.781946044922" />
                  <Point X="-27.0390546875" Y="-3.790265869141" />
                  <Point X="-27.053673828125" Y="-3.795496337891" />
                  <Point X="-27.081865234375" Y="-3.80826953125" />
                  <Point X="-27.0954375" Y="-3.815812255859" />
                  <Point X="-27.273298828125" Y="-3.934655761719" />
                  <Point X="-27.35340234375" Y="-3.988180175781" />
                  <Point X="-27.35968359375" Y="-3.992759765625" />
                  <Point X="-27.380439453125" Y="-4.010130859375" />
                  <Point X="-27.402755859375" Y="-4.032769287109" />
                  <Point X="-27.410470703125" Y="-4.041628662109" />
                  <Point X="-27.480306640625" Y="-4.132639160156" />
                  <Point X="-27.503203125" Y="-4.162477539063" />
                  <Point X="-27.621541015625" Y="-4.089205078125" />
                  <Point X="-27.747591796875" Y="-4.011157958984" />
                  <Point X="-27.980861328125" Y="-3.831548339844" />
                  <Point X="-27.8547578125" Y="-3.613129882813" />
                  <Point X="-27.34148828125" Y="-2.724119628906" />
                  <Point X="-27.3348515625" Y="-2.7100859375" />
                  <Point X="-27.32394921875" Y="-2.681121582031" />
                  <Point X="-27.319685546875" Y="-2.666190917969" />
                  <Point X="-27.3134140625" Y="-2.634665771484" />
                  <Point X="-27.311638671875" Y="-2.619241699219" />
                  <Point X="-27.310625" Y="-2.588309570312" />
                  <Point X="-27.3146640625" Y="-2.557625488281" />
                  <Point X="-27.323646484375" Y="-2.528009033203" />
                  <Point X="-27.3293515625" Y="-2.513568603516" />
                  <Point X="-27.34356640625" Y="-2.484739990234" />
                  <Point X="-27.351546875" Y="-2.471422363281" />
                  <Point X="-27.36957421875" Y="-2.446261962891" />
                  <Point X="-27.37962109375" Y="-2.434419189453" />
                  <Point X="-27.39696875" Y="-2.417070556641" />
                  <Point X="-27.408810546875" Y="-2.407023681641" />
                  <Point X="-27.43396875" Y="-2.388995361328" />
                  <Point X="-27.44728515625" Y="-2.381013916016" />
                  <Point X="-27.476115234375" Y="-2.366795410156" />
                  <Point X="-27.4905546875" Y="-2.361088623047" />
                  <Point X="-27.520171875" Y="-2.352103271484" />
                  <Point X="-27.550859375" Y="-2.3480625" />
                  <Point X="-27.581794921875" Y="-2.349074707031" />
                  <Point X="-27.597220703125" Y="-2.350849121094" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.087072265625" Y="-2.610089599609" />
                  <Point X="-28.793087890625" Y="-3.017708251953" />
                  <Point X="-28.9044375" Y="-2.871418457031" />
                  <Point X="-29.004021484375" Y="-2.740585449219" />
                  <Point X="-29.181265625" Y="-2.443372802734" />
                  <Point X="-28.9461796875" Y="-2.262985351562" />
                  <Point X="-28.048123046875" Y="-1.573882080078" />
                  <Point X="-28.036482421875" Y="-1.563309814453" />
                  <Point X="-28.01510546875" Y="-1.540390136719" />
                  <Point X="-28.005369140625" Y="-1.528042602539" />
                  <Point X="-27.987404296875" Y="-1.500911743164" />
                  <Point X="-27.979837890625" Y="-1.487127685547" />
                  <Point X="-27.96708203125" Y="-1.458499633789" />
                  <Point X="-27.961892578125" Y="-1.443655639648" />
                  <Point X="-27.956580078125" Y="-1.423145629883" />
                  <Point X="-27.951552734375" Y="-1.398806274414" />
                  <Point X="-27.948748046875" Y="-1.36837512207" />
                  <Point X="-27.948578125" Y="-1.353046142578" />
                  <Point X="-27.950787109375" Y="-1.321376220703" />
                  <Point X="-27.953083984375" Y="-1.30622253418" />
                  <Point X="-27.960083984375" Y="-1.276480224609" />
                  <Point X="-27.971775390625" Y="-1.248249145508" />
                  <Point X="-27.98785546875" Y="-1.222267333984" />
                  <Point X="-27.996947265625" Y="-1.209928466797" />
                  <Point X="-28.01777734375" Y="-1.185971923828" />
                  <Point X="-28.02873828125" Y="-1.175251586914" />
                  <Point X="-28.0522421875" Y="-1.155713134766" />
                  <Point X="-28.06478515625" Y="-1.146895019531" />
                  <Point X="-28.083044921875" Y="-1.13614831543" />
                  <Point X="-28.105431640625" Y="-1.124481933594" />
                  <Point X="-28.13469140625" Y="-1.113258911133" />
                  <Point X="-28.149787109375" Y="-1.108862060547" />
                  <Point X="-28.18167578125" Y="-1.102379394531" />
                  <Point X="-28.197294921875" Y="-1.100532836914" />
                  <Point X="-28.22862109375" Y="-1.09944152832" />
                  <Point X="-28.244328125" Y="-1.100196655273" />
                  <Point X="-28.74978125" Y="-1.166740478516" />
                  <Point X="-29.66091796875" Y="-1.286694213867" />
                  <Point X="-29.7018125" Y="-1.126594604492" />
                  <Point X="-29.740763671875" Y="-0.974113342285" />
                  <Point X="-29.786451171875" Y="-0.654654418945" />
                  <Point X="-29.53130859375" Y="-0.58628918457" />
                  <Point X="-28.508287109375" Y="-0.312171173096" />
                  <Point X="-28.500478515625" Y="-0.309713409424" />
                  <Point X="-28.4739296875" Y="-0.299251556396" />
                  <Point X="-28.44416796875" Y="-0.283894775391" />
                  <Point X="-28.4335625" Y="-0.277515441895" />
                  <Point X="-28.414427734375" Y="-0.264234802246" />
                  <Point X="-28.393671875" Y="-0.24824357605" />
                  <Point X="-28.37121875" Y="-0.226361236572" />
                  <Point X="-28.36090234375" Y="-0.214488937378" />
                  <Point X="-28.341654296875" Y="-0.188228897095" />
                  <Point X="-28.333435546875" Y="-0.174811828613" />
                  <Point X="-28.31932421875" Y="-0.14680368042" />
                  <Point X="-28.313431640625" Y="-0.132212478638" />
                  <Point X="-28.3070546875" Y="-0.111661354065" />
                  <Point X="-28.300990234375" Y="-0.088497138977" />
                  <Point X="-28.29672265625" Y="-0.060349449158" />
                  <Point X="-28.2956484375" Y="-0.04611523819" />
                  <Point X="-28.295646484375" Y="-0.016474779129" />
                  <Point X="-28.29671875" Y="-0.00223417902" />
                  <Point X="-28.300986328125" Y="0.02592599678" />
                  <Point X="-28.304181640625" Y="0.039845573425" />
                  <Point X="-28.31055859375" Y="0.060396701813" />
                  <Point X="-28.3105625" Y="0.060407848358" />
                  <Point X="-28.319333984375" Y="0.084269851685" />
                  <Point X="-28.333443359375" Y="0.112267433167" />
                  <Point X="-28.34166015625" Y="0.12567931366" />
                  <Point X="-28.36090625" Y="0.151934432983" />
                  <Point X="-28.37122265625" Y="0.163805252075" />
                  <Point X="-28.393673828125" Y="0.185684768677" />
                  <Point X="-28.40580859375" Y="0.195693328857" />
                  <Point X="-28.424943359375" Y="0.208973968506" />
                  <Point X="-28.44225" Y="0.219659805298" />
                  <Point X="-28.46643359375" Y="0.23285975647" />
                  <Point X="-28.4766484375" Y="0.237670227051" />
                  <Point X="-28.49757421875" Y="0.246045883179" />
                  <Point X="-28.508287109375" Y="0.249611190796" />
                  <Point X="-28.96903125" Y="0.373067169189" />
                  <Point X="-29.7854453125" Y="0.591824768066" />
                  <Point X="-29.7564296875" Y="0.787913085938" />
                  <Point X="-29.73133203125" Y="0.957521972656" />
                  <Point X="-29.6335859375" Y="1.318236938477" />
                  <Point X="-29.511392578125" Y="1.302149902344" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.204703369141" />
                  <Point X="-28.71514453125" Y="1.20658972168" />
                  <Point X="-28.704890625" Y="1.208053588867" />
                  <Point X="-28.684603515625" Y="1.212088745117" />
                  <Point X="-28.674568359375" Y="1.214660522461" />
                  <Point X="-28.632216796875" Y="1.228014160156" />
                  <Point X="-28.603451171875" Y="1.237676269531" />
                  <Point X="-28.584513671875" Y="1.246008789062" />
                  <Point X="-28.57526953125" Y="1.250693603516" />
                  <Point X="-28.556525390625" Y="1.261516723633" />
                  <Point X="-28.5478515625" Y="1.267177612305" />
                  <Point X="-28.531173828125" Y="1.279407592773" />
                  <Point X="-28.515927734375" Y="1.293378662109" />
                  <Point X="-28.502291015625" Y="1.308928222656" />
                  <Point X="-28.495896484375" Y="1.317075683594" />
                  <Point X="-28.48348046875" Y="1.334807128906" />
                  <Point X="-28.478009765625" Y="1.343604614258" />
                  <Point X="-28.468060546875" Y="1.361739990234" />
                  <Point X="-28.46358203125" Y="1.371077880859" />
                  <Point X="-28.446587890625" Y="1.412104736328" />
                  <Point X="-28.435498046875" Y="1.440356079102" />
                  <Point X="-28.42970703125" Y="1.460226074219" />
                  <Point X="-28.427353515625" Y="1.470322143555" />
                  <Point X="-28.42359765625" Y="1.491637573242" />
                  <Point X="-28.422359375" Y="1.501922607422" />
                  <Point X="-28.421009765625" Y="1.522556884766" />
                  <Point X="-28.421912109375" Y="1.543203857422" />
                  <Point X="-28.425056640625" Y="1.563641601562" />
                  <Point X="-28.4271875" Y="1.573781494141" />
                  <Point X="-28.4327890625" Y="1.594687988281" />
                  <Point X="-28.43601171875" Y="1.604531005859" />
                  <Point X="-28.4435078125" Y="1.623808349609" />
                  <Point X="-28.44778125" Y="1.633243286133" />
                  <Point X="-28.46828515625" Y="1.67263293457" />
                  <Point X="-28.477521484375" Y="1.690373046875" />
                  <Point X="-28.482802734375" Y="1.69929309082" />
                  <Point X="-28.49430078125" Y="1.716498657227" />
                  <Point X="-28.500517578125" Y="1.724784057617" />
                  <Point X="-28.5144296875" Y="1.741361572266" />
                  <Point X="-28.5215078125" Y="1.748919067383" />
                  <Point X="-28.5364453125" Y="1.763217407227" />
                  <Point X="-28.5443046875" Y="1.769958496094" />
                  <Point X="-28.808587890625" Y="1.972750854492" />
                  <Point X="-29.22761328125" Y="2.294280517578" />
                  <Point X="-29.099814453125" Y="2.513231201172" />
                  <Point X="-29.002283203125" Y="2.680322753906" />
                  <Point X="-28.7265625" Y="3.034725341797" />
                  <Point X="-28.726337890625" Y="3.035013671875" />
                  <Point X="-28.704544921875" Y="3.022431152344" />
                  <Point X="-28.25415625" Y="2.762398925781" />
                  <Point X="-28.24491796875" Y="2.757716796875" />
                  <Point X="-28.225984375" Y="2.749385742188" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.096087890625" Y="2.725997558594" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310546875" />
                  <Point X="-27.976431640625" Y="2.734227294922" />
                  <Point X="-27.95698828125" Y="2.7413046875" />
                  <Point X="-27.9384375" Y="2.750454345703" />
                  <Point X="-27.929416015625" Y="2.755533691406" />
                  <Point X="-27.911162109375" Y="2.767163330078" />
                  <Point X="-27.90274609375" Y="2.773194091797" />
                  <Point X="-27.8866171875" Y="2.786138427734" />
                  <Point X="-27.878904296875" Y="2.793052001953" />
                  <Point X="-27.83703515625" Y="2.834919433594" />
                  <Point X="-27.8181796875" Y="2.853775390625" />
                  <Point X="-27.811265625" Y="2.861489013672" />
                  <Point X="-27.79831640625" Y="2.877623291016" />
                  <Point X="-27.79228125" Y="2.886043945312" />
                  <Point X="-27.78065234375" Y="2.904298339844" />
                  <Point X="-27.7755703125" Y="2.913326171875" />
                  <Point X="-27.766423828125" Y="2.931875" />
                  <Point X="-27.759353515625" Y="2.951299316406" />
                  <Point X="-27.754435546875" Y="2.971387939453" />
                  <Point X="-27.7525234375" Y="2.981573242188" />
                  <Point X="-27.749697265625" Y="3.003031738281" />
                  <Point X="-27.748908203125" Y="3.013364746094" />
                  <Point X="-27.74845703125" Y="3.034048828125" />
                  <Point X="-27.748794921875" Y="3.044399902344" />
                  <Point X="-27.753955078125" Y="3.103384277344" />
                  <Point X="-27.756279296875" Y="3.129949462891" />
                  <Point X="-27.757744140625" Y="3.140206054688" />
                  <Point X="-27.76178125" Y="3.160499267578" />
                  <Point X="-27.764353515625" Y="3.170535888672" />
                  <Point X="-27.77086328125" Y="3.191177978516" />
                  <Point X="-27.774513671875" Y="3.200872802734" />
                  <Point X="-27.78284375" Y="3.219800537109" />
                  <Point X="-27.7875234375" Y="3.229033447266" />
                  <Point X="-27.90463671875" Y="3.431877685547" />
                  <Point X="-28.05938671875" Y="3.699914550781" />
                  <Point X="-27.81821875" Y="3.884815673828" />
                  <Point X="-27.648365234375" Y="4.015041992188" />
                  <Point X="-27.21411328125" Y="4.256303222656" />
                  <Point X="-27.192525390625" Y="4.268297363281" />
                  <Point X="-27.118564453125" Y="4.171909179688" />
                  <Point X="-27.1118203125" Y="4.164048828125" />
                  <Point X="-27.097517578125" Y="4.149107421875" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.065091796875" Y="4.121897460938" />
                  <Point X="-27.047892578125" Y="4.110405273438" />
                  <Point X="-27.03898046875" Y="4.105129394531" />
                  <Point X="-26.97333203125" Y="4.070954345703" />
                  <Point X="-26.943763671875" Y="4.055562255859" />
                  <Point X="-26.934326171875" Y="4.051286621094" />
                  <Point X="-26.915044921875" Y="4.0437890625" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031377929688" />
                  <Point X="-26.7808203125" Y="4.035135742188" />
                  <Point X="-26.77073046875" Y="4.037488037109" />
                  <Point X="-26.750869140625" Y="4.043277099609" />
                  <Point X="-26.74109765625" Y="4.046713867188" />
                  <Point X="-26.672720703125" Y="4.075037597656" />
                  <Point X="-26.641923828125" Y="4.087793457031" />
                  <Point X="-26.6325859375" Y="4.092271972656" />
                  <Point X="-26.614453125" Y="4.102219726562" />
                  <Point X="-26.605658203125" Y="4.107688964844" />
                  <Point X="-26.587927734375" Y="4.120103515625" />
                  <Point X="-26.579779296875" Y="4.126497558594" />
                  <Point X="-26.5642265625" Y="4.140136230469" />
                  <Point X="-26.550248046875" Y="4.155391113281" />
                  <Point X="-26.538017578125" Y="4.172072753906" />
                  <Point X="-26.532361328125" Y="4.180744140625" />
                  <Point X="-26.5215390625" Y="4.199488769531" />
                  <Point X="-26.516857421875" Y="4.208723144531" />
                  <Point X="-26.508525390625" Y="4.22765625" />
                  <Point X="-26.504875" Y="4.237354980469" />
                  <Point X="-26.482619140625" Y="4.30794140625" />
                  <Point X="-26.472595703125" Y="4.339731445312" />
                  <Point X="-26.470025390625" Y="4.349764160156" />
                  <Point X="-26.465990234375" Y="4.370049804688" />
                  <Point X="-26.464525390625" Y="4.380302734375" />
                  <Point X="-26.462638671875" Y="4.401864257812" />
                  <Point X="-26.46230078125" Y="4.412217285156" />
                  <Point X="-26.462751953125" Y="4.4328984375" />
                  <Point X="-26.463541015625" Y="4.4432265625" />
                  <Point X="-26.47685546875" Y="4.544359375" />
                  <Point X="-26.479263671875" Y="4.562655273438" />
                  <Point X="-26.15105078125" Y="4.654675292969" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.4047265625" Y="4.77793359375" />
                  <Point X="-25.36522265625" Y="4.782557128906" />
                  <Point X="-25.358904296875" Y="4.758977050781" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.70201171875" Y="4.485676757813" />
                  <Point X="-24.621806640625" Y="4.785005859375" />
                  <Point X="-24.364119140625" Y="4.758019042969" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.736568359375" Y="4.632755859375" />
                  <Point X="-23.54640234375" Y="4.586843261719" />
                  <Point X="-23.264330078125" Y="4.484534667969" />
                  <Point X="-23.141736328125" Y="4.440069335938" />
                  <Point X="-22.867603515625" Y="4.311865234375" />
                  <Point X="-22.74955078125" Y="4.256654785156" />
                  <Point X="-22.484666015625" Y="4.102333984375" />
                  <Point X="-22.3705703125" Y="4.035861572266" />
                  <Point X="-22.18221875" Y="3.901916259766" />
                  <Point X="-22.3406171875" Y="3.627560546875" />
                  <Point X="-22.934689453125" Y="2.59859765625" />
                  <Point X="-22.9376171875" Y="2.593120361328" />
                  <Point X="-22.946814453125" Y="2.573448242188" />
                  <Point X="-22.95581640625" Y="2.549567871094" />
                  <Point X="-22.958697265625" Y="2.540600097656" />
                  <Point X="-22.9735" Y="2.485244628906" />
                  <Point X="-22.98016796875" Y="2.460313720703" />
                  <Point X="-22.9820859375" Y="2.451470458984" />
                  <Point X="-22.986353515625" Y="2.420223876953" />
                  <Point X="-22.987244140625" Y="2.383240722656" />
                  <Point X="-22.986587890625" Y="2.369581542969" />
                  <Point X="-22.98081640625" Y="2.32171484375" />
                  <Point X="-22.978216796875" Y="2.300156738281" />
                  <Point X="-22.97619921875" Y="2.289036376953" />
                  <Point X="-22.97085546875" Y="2.267107910156" />
                  <Point X="-22.967529296875" Y="2.256299804688" />
                  <Point X="-22.95926171875" Y="2.234210449219" />
                  <Point X="-22.954677734375" Y="2.223882324219" />
                  <Point X="-22.94431640625" Y="2.203840820312" />
                  <Point X="-22.9385390625" Y="2.194127441406" />
                  <Point X="-22.908919921875" Y="2.150477539062" />
                  <Point X="-22.895580078125" Y="2.130818847656" />
                  <Point X="-22.890185546875" Y="2.123628173828" />
                  <Point X="-22.86953125" Y="2.100120117188" />
                  <Point X="-22.8423984375" Y="2.075385742188" />
                  <Point X="-22.831740234375" Y="2.066981933594" />
                  <Point X="-22.788091796875" Y="2.037363525391" />
                  <Point X="-22.768431640625" Y="2.024024414062" />
                  <Point X="-22.758716796875" Y="2.018244995117" />
                  <Point X="-22.738669921875" Y="2.007881225586" />
                  <Point X="-22.728337890625" Y="2.00329675293" />
                  <Point X="-22.706251953125" Y="1.995031860352" />
                  <Point X="-22.695451171875" Y="1.991707763672" />
                  <Point X="-22.673529296875" Y="1.986365112305" />
                  <Point X="-22.662408203125" Y="1.984346557617" />
                  <Point X="-22.614541015625" Y="1.978574584961" />
                  <Point X="-22.592984375" Y="1.975974975586" />
                  <Point X="-22.583955078125" Y="1.975320800781" />
                  <Point X="-22.55242578125" Y="1.975496948242" />
                  <Point X="-22.515685546875" Y="1.979822509766" />
                  <Point X="-22.50225" Y="1.982395751953" />
                  <Point X="-22.44689453125" Y="1.997198730469" />
                  <Point X="-22.421962890625" Y="2.003865356445" />
                  <Point X="-22.416" Y="2.00567175293" />
                  <Point X="-22.39559765625" Y="2.01306640625" />
                  <Point X="-22.372345703125" Y="2.023573486328" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.900544921875" Y="2.295428222656" />
                  <Point X="-21.05959375" Y="2.780951416016" />
                  <Point X="-21.023728515625" Y="2.731106689453" />
                  <Point X="-20.95605078125" Y="2.637048095703" />
                  <Point X="-20.863115234375" Y="2.483470458984" />
                  <Point X="-21.04489453125" Y="2.343985595703" />
                  <Point X="-21.827046875" Y="1.743819946289" />
                  <Point X="-21.83186328125" Y="1.7398671875" />
                  <Point X="-21.8478828125" Y="1.725213500977" />
                  <Point X="-21.86533203125" Y="1.706600830078" />
                  <Point X="-21.871421875" Y="1.69942175293" />
                  <Point X="-21.91126171875" Y="1.647448242188" />
                  <Point X="-21.929203125" Y="1.624040649414" />
                  <Point X="-21.934357421875" Y="1.616608520508" />
                  <Point X="-21.950255859375" Y="1.589379760742" />
                  <Point X="-21.965234375" Y="1.555552124023" />
                  <Point X="-21.969859375" Y="1.542674438477" />
                  <Point X="-21.98469921875" Y="1.489609008789" />
                  <Point X="-21.991384765625" Y="1.465709960938" />
                  <Point X="-21.993775390625" Y="1.454663452148" />
                  <Point X="-21.997228515625" Y="1.432366210938" />
                  <Point X="-21.998291015625" Y="1.421115356445" />
                  <Point X="-21.999107421875" Y="1.397541992188" />
                  <Point X="-21.998826171875" Y="1.386237670898" />
                  <Point X="-21.996921875" Y="1.363750488281" />
                  <Point X="-21.995298828125" Y="1.352567504883" />
                  <Point X="-21.983115234375" Y="1.293525512695" />
                  <Point X="-21.97762890625" Y="1.266934448242" />
                  <Point X="-21.97540234375" Y="1.258238525391" />
                  <Point X="-21.96531640625" Y="1.228607788086" />
                  <Point X="-21.949712890625" Y="1.195370605469" />
                  <Point X="-21.943080078125" Y="1.183526977539" />
                  <Point X="-21.9099453125" Y="1.133163818359" />
                  <Point X="-21.8950234375" Y="1.110481567383" />
                  <Point X="-21.888263671875" Y="1.101428344727" />
                  <Point X="-21.873712890625" Y="1.084185546875" />
                  <Point X="-21.865921875" Y="1.075995849609" />
                  <Point X="-21.848677734375" Y="1.059905029297" />
                  <Point X="-21.83996875" Y="1.052697875977" />
                  <Point X="-21.821755859375" Y="1.039369750977" />
                  <Point X="-21.812251953125" Y="1.033249389648" />
                  <Point X="-21.764234375" Y="1.006220031738" />
                  <Point X="-21.742609375" Y="0.994046813965" />
                  <Point X="-21.734521484375" Y="0.989988037109" />
                  <Point X="-21.7053203125" Y="0.978083496094" />
                  <Point X="-21.66972265625" Y="0.968020996094" />
                  <Point X="-21.656328125" Y="0.965257629395" />
                  <Point X="-21.59140625" Y="0.956677368164" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.555966796875" Y="0.952199829102" />
                  <Point X="-21.53428125" Y="0.951222900391" />
                  <Point X="-21.50878125" Y="0.952032287598" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-21.059177734375" Y="1.010753051758" />
                  <Point X="-20.295296875" Y="1.111319702148" />
                  <Point X="-20.27638671875" Y="1.033639892578" />
                  <Point X="-20.247310546875" Y="0.914203674316" />
                  <Point X="-20.216126953125" Y="0.713921020508" />
                  <Point X="-20.41164453125" Y="0.661532348633" />
                  <Point X="-21.3080078125" Y="0.42135269165" />
                  <Point X="-21.313966796875" Y="0.419544525146" />
                  <Point X="-21.334376953125" Y="0.412136138916" />
                  <Point X="-21.357619140625" Y="0.401618225098" />
                  <Point X="-21.365994140625" Y="0.397316497803" />
                  <Point X="-21.42977734375" Y="0.360448516846" />
                  <Point X="-21.45850390625" Y="0.343844055176" />
                  <Point X="-21.46611328125" Y="0.338947967529" />
                  <Point X="-21.49122265625" Y="0.319873565674" />
                  <Point X="-21.518005859375" Y="0.294351745605" />
                  <Point X="-21.527203125" Y="0.284226196289" />
                  <Point X="-21.56547265625" Y="0.235460952759" />
                  <Point X="-21.582708984375" Y="0.213498352051" />
                  <Point X="-21.58914453125" Y="0.20421031189" />
                  <Point X="-21.600869140625" Y="0.184930511475" />
                  <Point X="-21.606158203125" Y="0.174938583374" />
                  <Point X="-21.615931640625" Y="0.153475509644" />
                  <Point X="-21.61999609375" Y="0.14292640686" />
                  <Point X="-21.626841796875" Y="0.121424530029" />
                  <Point X="-21.629623046875" Y="0.11047177124" />
                  <Point X="-21.64237890625" Y="0.043861064911" />
                  <Point X="-21.648125" Y="0.013861235619" />
                  <Point X="-21.649396484375" Y="0.004967046261" />
                  <Point X="-21.6514140625" Y="-0.026260259628" />
                  <Point X="-21.64971875" Y="-0.062939945221" />
                  <Point X="-21.648125" Y="-0.076421531677" />
                  <Point X="-21.635369140625" Y="-0.143032089233" />
                  <Point X="-21.629623046875" Y="-0.173031768799" />
                  <Point X="-21.626841796875" Y="-0.183984527588" />
                  <Point X="-21.61999609375" Y="-0.205486404419" />
                  <Point X="-21.615931640625" Y="-0.216035507202" />
                  <Point X="-21.606158203125" Y="-0.237498580933" />
                  <Point X="-21.600869140625" Y="-0.24748991394" />
                  <Point X="-21.58914453125" Y="-0.266770019531" />
                  <Point X="-21.582708984375" Y="-0.276058807373" />
                  <Point X="-21.544439453125" Y="-0.324823760986" />
                  <Point X="-21.527203125" Y="-0.346786499023" />
                  <Point X="-21.52128125" Y="-0.353633850098" />
                  <Point X="-21.49885546875" Y="-0.3758097229" />
                  <Point X="-21.4698203125" Y="-0.398726501465" />
                  <Point X="-21.45850390625" Y="-0.406404205322" />
                  <Point X="-21.394720703125" Y="-0.443272033691" />
                  <Point X="-21.365994140625" Y="-0.459876647949" />
                  <Point X="-21.360505859375" Y="-0.462813568115" />
                  <Point X="-21.34084375" Y="-0.472015869141" />
                  <Point X="-21.31697265625" Y="-0.481027618408" />
                  <Point X="-21.3080078125" Y="-0.483912963867" />
                  <Point X="-20.904306640625" Y="-0.592083984375" />
                  <Point X="-20.215119140625" Y="-0.776751281738" />
                  <Point X="-20.22215234375" Y="-0.823396362305" />
                  <Point X="-20.238380859375" Y="-0.931036376953" />
                  <Point X="-20.2721953125" Y="-1.079219848633" />
                  <Point X="-20.523689453125" Y="-1.046110107422" />
                  <Point X="-21.563216796875" Y="-0.909253601074" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042480469" />
                  <Point X="-21.63327734375" Y="-0.910840881348" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.770701171875" Y="-0.939885803223" />
                  <Point X="-21.82708203125" Y="-0.952140075684" />
                  <Point X="-21.842125" Y="-0.956742614746" />
                  <Point X="-21.87124609375" Y="-0.968367614746" />
                  <Point X="-21.88532421875" Y="-0.975390014648" />
                  <Point X="-21.913150390625" Y="-0.992282531738" />
                  <Point X="-21.925873046875" Y="-1.001529968262" />
                  <Point X="-21.949623046875" Y="-1.022000244141" />
                  <Point X="-21.960650390625" Y="-1.033223022461" />
                  <Point X="-22.03631640625" Y="-1.124225708008" />
                  <Point X="-22.07039453125" Y="-1.165211181641" />
                  <Point X="-22.078673828125" Y="-1.17684753418" />
                  <Point X="-22.09339453125" Y="-1.201229980469" />
                  <Point X="-22.0998359375" Y="-1.213976074219" />
                  <Point X="-22.1111796875" Y="-1.241362548828" />
                  <Point X="-22.115638671875" Y="-1.254934204102" />
                  <Point X="-22.12246875" Y="-1.282583374023" />
                  <Point X="-22.12483984375" Y="-1.29666027832" />
                  <Point X="-22.135685546875" Y="-1.414512695312" />
                  <Point X="-22.140568359375" Y="-1.467590576172" />
                  <Point X="-22.140708984375" Y="-1.483319946289" />
                  <Point X="-22.138390625" Y="-1.514591796875" />
                  <Point X="-22.135931640625" Y="-1.530134277344" />
                  <Point X="-22.12819921875" Y="-1.561754882812" />
                  <Point X="-22.123208984375" Y="-1.576674804688" />
                  <Point X="-22.110837890625" Y="-1.605482788086" />
                  <Point X="-22.10345703125" Y="-1.61937097168" />
                  <Point X="-22.0341796875" Y="-1.727129516602" />
                  <Point X="-22.0029765625" Y="-1.775661499023" />
                  <Point X="-21.9982578125" Y="-1.782350585938" />
                  <Point X="-21.980203125" Y="-1.804452026367" />
                  <Point X="-21.9565078125" Y="-1.82812121582" />
                  <Point X="-21.947201171875" Y="-1.836277587891" />
                  <Point X="-21.57256640625" Y="-2.12374609375" />
                  <Point X="-20.912828125" Y="-2.629980712891" />
                  <Point X="-20.95453515625" Y="-2.697464355469" />
                  <Point X="-20.998724609375" Y="-2.760252685547" />
                  <Point X="-21.225705078125" Y="-2.629205078125" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.184970703125" Y="-2.079513916016" />
                  <Point X="-22.216876953125" Y="-2.069326416016" />
                  <Point X="-22.228888671875" Y="-2.066337646484" />
                  <Point X="-22.377876953125" Y="-2.039430297852" />
                  <Point X="-22.444978515625" Y="-2.027311889648" />
                  <Point X="-22.460638671875" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.55515625" Y="-2.035136474609" />
                  <Point X="-22.584931640625" Y="-2.044960083008" />
                  <Point X="-22.59941015625" Y="-2.051108642578" />
                  <Point X="-22.72318359375" Y="-2.116249755859" />
                  <Point X="-22.778927734375" Y="-2.145587646484" />
                  <Point X="-22.79103125" Y="-2.153171630859" />
                  <Point X="-22.813962890625" Y="-2.170066162109" />
                  <Point X="-22.824791015625" Y="-2.179376708984" />
                  <Point X="-22.84575" Y="-2.200336669922" />
                  <Point X="-22.85505859375" Y="-2.211160888672" />
                  <Point X="-22.871951171875" Y="-2.234089355469" />
                  <Point X="-22.87953515625" Y="-2.246193603516" />
                  <Point X="-22.944677734375" Y="-2.369967041016" />
                  <Point X="-22.974015625" Y="-2.425711669922" />
                  <Point X="-22.9801640625" Y="-2.440193603516" />
                  <Point X="-22.98998828125" Y="-2.469972412109" />
                  <Point X="-22.9936640625" Y="-2.485269287109" />
                  <Point X="-22.99862109375" Y="-2.517442382812" />
                  <Point X="-22.999720703125" Y="-2.533133789062" />
                  <Point X="-22.99931640625" Y="-2.564484375" />
                  <Point X="-22.9978125" Y="-2.580143554688" />
                  <Point X="-22.970904296875" Y="-2.7291328125" />
                  <Point X="-22.95878515625" Y="-2.796233886719" />
                  <Point X="-22.95698046875" Y="-2.804229248047" />
                  <Point X="-22.948763671875" Y="-2.831537597656" />
                  <Point X="-22.935931640625" Y="-2.862475097656" />
                  <Point X="-22.930453125" Y="-2.873578125" />
                  <Point X="-22.689712890625" Y="-3.290554443359" />
                  <Point X="-22.26410546875" Y="-4.027724853516" />
                  <Point X="-22.276244140625" Y="-4.036082275391" />
                  <Point X="-22.45958203125" Y="-3.797152832031" />
                  <Point X="-23.16608203125" Y="-2.876422363281" />
                  <Point X="-23.171345703125" Y="-2.870143066406" />
                  <Point X="-23.191169921875" Y="-2.849624023438" />
                  <Point X="-23.216748046875" Y="-2.828003662109" />
                  <Point X="-23.22669921875" Y="-2.820646728516" />
                  <Point X="-23.37364453125" Y="-2.726175537109" />
                  <Point X="-23.43982421875" Y="-2.683627929688" />
                  <Point X="-23.453716796875" Y="-2.676245605469" />
                  <Point X="-23.48252734375" Y="-2.663874511719" />
                  <Point X="-23.4974453125" Y="-2.658885742188" />
                  <Point X="-23.52906640625" Y="-2.651154052734" />
                  <Point X="-23.54460546875" Y="-2.648695800781" />
                  <Point X="-23.575875" Y="-2.646376953125" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.7523125" Y="-2.661304931641" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670339111328" />
                  <Point X="-23.866421875" Y="-2.677170410156" />
                  <Point X="-23.87998828125" Y="-2.681627929688" />
                  <Point X="-23.907373046875" Y="-2.692970703125" />
                  <Point X="-23.92012109375" Y="-2.699412841797" />
                  <Point X="-23.94450390625" Y="-2.714134033203" />
                  <Point X="-23.956138671875" Y="-2.722413085938" />
                  <Point X="-24.080232421875" Y="-2.825593505859" />
                  <Point X="-24.136123046875" Y="-2.872063476562" />
                  <Point X="-24.147345703125" Y="-2.883088134766" />
                  <Point X="-24.16781640625" Y="-2.906838134766" />
                  <Point X="-24.177064453125" Y="-2.919563476563" />
                  <Point X="-24.19395703125" Y="-2.947390380859" />
                  <Point X="-24.200978515625" Y="-2.961466552734" />
                  <Point X="-24.212603515625" Y="-2.990586914062" />
                  <Point X="-24.21720703125" Y="-3.005631103516" />
                  <Point X="-24.254310546875" Y="-3.176337158203" />
                  <Point X="-24.271021484375" Y="-3.253218994141" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323170166016" />
                  <Point X="-24.2744453125" Y="-3.335520507812" />
                  <Point X="-24.206220703125" Y="-3.853735107422" />
                  <Point X="-24.166912109375" Y="-4.152314941406" />
                  <Point X="-24.344931640625" Y="-3.487937255859" />
                  <Point X="-24.347392578125" Y="-3.480121337891" />
                  <Point X="-24.357853515625" Y="-3.453579833984" />
                  <Point X="-24.3732109375" Y="-3.423815917969" />
                  <Point X="-24.37958984375" Y="-3.413209472656" />
                  <Point X="-24.4924765625" Y="-3.250561523438" />
                  <Point X="-24.543318359375" Y="-3.177309082031" />
                  <Point X="-24.553328125" Y="-3.165172119141" />
                  <Point X="-24.575212890625" Y="-3.142716064453" />
                  <Point X="-24.587087890625" Y="-3.132396972656" />
                  <Point X="-24.61334375" Y="-3.113153320312" />
                  <Point X="-24.6267578125" Y="-3.104936523438" />
                  <Point X="-24.6547578125" Y="-3.090829101562" />
                  <Point X="-24.66934375" Y="-3.084938476562" />
                  <Point X="-24.844029296875" Y="-3.03072265625" />
                  <Point X="-24.922703125" Y="-3.006305175781" />
                  <Point X="-24.936623046875" Y="-3.003109130859" />
                  <Point X="-24.964783203125" Y="-2.99883984375" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.998840087891" />
                  <Point X="-25.051064453125" Y="-3.003109619141" />
                  <Point X="-25.064984375" Y="-3.006305664062" />
                  <Point X="-25.23966796875" Y="-3.060521240234" />
                  <Point X="-25.31834375" Y="-3.084938964844" />
                  <Point X="-25.3329296875" Y="-3.090829345703" />
                  <Point X="-25.3609296875" Y="-3.104936767578" />
                  <Point X="-25.37434375" Y="-3.113153808594" />
                  <Point X="-25.400599609375" Y="-3.132397705078" />
                  <Point X="-25.412474609375" Y="-3.142717285156" />
                  <Point X="-25.434359375" Y="-3.165173828125" />
                  <Point X="-25.444369140625" Y="-3.177310791016" />
                  <Point X="-25.55725390625" Y="-3.339958496094" />
                  <Point X="-25.608095703125" Y="-3.413211181641" />
                  <Point X="-25.61246875" Y="-3.420132080078" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213378906" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.76714453125" Y="-3.952166015625" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.309305502381" Y="0.688953953661" />
                  <Point X="-20.345677429908" Y="1.104686987649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.256131896328" Y="-1.008825902986" />
                  <Point X="-20.27790771755" Y="-0.75992712749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.402484051637" Y="0.663986886814" />
                  <Point X="-20.439954424388" Y="1.092275207163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.346188365806" Y="-1.069478504996" />
                  <Point X="-20.375559813802" Y="-0.73376131819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.49566259831" Y="0.639019790445" />
                  <Point X="-20.534231418868" Y="1.079863426678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.442662445534" Y="-1.056777486176" />
                  <Point X="-20.473211910054" Y="-0.70759550889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.588841144702" Y="0.614052690856" />
                  <Point X="-20.628508413348" Y="1.067451646192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.539136525933" Y="-1.044076459697" />
                  <Point X="-20.570864006307" Y="-0.681429699591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.682019691093" Y="0.589085591268" />
                  <Point X="-20.722785407828" Y="1.055039865706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.635610609846" Y="-1.031375393047" />
                  <Point X="-20.668516102559" Y="-0.655263890291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.775198237485" Y="0.564118491679" />
                  <Point X="-20.817062402308" Y="1.042628085221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.938086732993" Y="2.425942514854" />
                  <Point X="-20.956626022807" Y="2.637847567086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.732084693759" Y="-1.018674326397" />
                  <Point X="-20.766168198811" Y="-0.629098080991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.868376783876" Y="0.539151392091" />
                  <Point X="-20.911339396788" Y="1.030216304735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.027450389463" Y="2.357371023934" />
                  <Point X="-21.064272538829" Y="2.778250117094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.828558777673" Y="-1.005973259747" />
                  <Point X="-20.863820295064" Y="-0.602932271691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.961555330268" Y="0.514184292502" />
                  <Point X="-21.005616391268" Y="1.017804524249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.116814075915" Y="2.288799875716" />
                  <Point X="-21.155050104797" Y="2.725839685683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.925032861586" Y="-0.993272193097" />
                  <Point X="-20.961472385993" Y="-0.576766523229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.054733876659" Y="0.489217192914" />
                  <Point X="-21.099893383594" Y="1.005392719146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.20617776964" Y="2.22022881062" />
                  <Point X="-21.245827670765" Y="2.673429254271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.021506945499" Y="-0.980571126447" />
                  <Point X="-21.059124473154" Y="-0.550600817854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.147912423051" Y="0.464250093325" />
                  <Point X="-21.194170373087" Y="0.992980881659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.295541463365" Y="2.151657745523" />
                  <Point X="-21.336605236732" Y="2.62101882286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.965320403381" Y="-2.712788999902" />
                  <Point X="-20.976864074386" Y="-2.580844236548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.117981029412" Y="-0.967870059797" />
                  <Point X="-21.156776560314" Y="-0.524435112479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.241090969442" Y="0.439282993737" />
                  <Point X="-21.28844736258" Y="0.980569044172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.38490515709" Y="2.083086680427" />
                  <Point X="-21.4273828027" Y="2.568608391448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.05960597781" Y="-2.725102711129" />
                  <Point X="-21.079089600129" Y="-2.502403888972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.214455113325" Y="-0.955168993147" />
                  <Point X="-21.254428647474" Y="-0.498269407104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.334087988395" Y="0.412241026027" />
                  <Point X="-21.382724352073" Y="0.968157206684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.474268850815" Y="2.014515615331" />
                  <Point X="-21.518160368667" Y="2.516197960037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.16004206459" Y="-2.66711574448" />
                  <Point X="-21.181315125872" Y="-2.423963541395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.310929197238" Y="-0.942467926497" />
                  <Point X="-21.352568508269" Y="-0.466528423568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.425153447011" Y="0.363121222646" />
                  <Point X="-21.477001341566" Y="0.955745369197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.563632544539" Y="1.945944550235" />
                  <Point X="-21.608937934635" Y="2.463787528625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.260478147982" Y="-2.609128816571" />
                  <Point X="-21.283540651614" Y="-2.345523193819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.407403281152" Y="-0.929766859847" />
                  <Point X="-21.452908627026" Y="-0.409638376452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.514769584641" Y="0.297435604585" />
                  <Point X="-21.572223970885" Y="0.954142244383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.652996238264" Y="1.877373485139" />
                  <Point X="-21.699715500603" Y="2.411377097214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.360914224973" Y="-2.551141961816" />
                  <Point X="-21.385766177357" Y="-2.267082846242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.503877365065" Y="-0.917065793197" />
                  <Point X="-21.557103666298" Y="-0.30868638622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.600362444885" Y="0.185763715586" />
                  <Point X="-21.668784149404" Y="0.967827376883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.742359931989" Y="1.808802420043" />
                  <Point X="-21.79049306657" Y="2.358966665802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.461350301964" Y="-2.49315510706" />
                  <Point X="-21.487991703099" Y="-2.188642498666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.600028759854" Y="-0.908053080107" />
                  <Point X="-21.7676754201" Y="1.008157014886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.831703255182" Y="1.739998518707" />
                  <Point X="-21.881270632538" Y="2.306556234391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.561786378955" Y="-2.435168252305" />
                  <Point X="-21.590217234803" Y="-2.110202082951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.694063998902" Y="-0.923228137828" />
                  <Point X="-21.86928249521" Y="1.079528439386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.918179426761" Y="1.638422924451" />
                  <Point X="-21.972048196536" Y="2.254145780466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.662222455946" Y="-2.37718139755" />
                  <Point X="-21.692442795071" Y="-2.031761340747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.787647287812" Y="-0.943569009264" />
                  <Point X="-21.983553517635" Y="1.295649444054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.996133363442" Y="1.43943773959" />
                  <Point X="-22.062825760003" Y="2.201735320473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.207709552143" Y="3.857764642461" />
                  <Point X="-22.213519778763" Y="3.92417583661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.762658532938" Y="-2.319194542795" />
                  <Point X="-21.794668355339" Y="-1.953320598544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.880439373607" Y="-0.972953373685" />
                  <Point X="-22.15360332347" Y="2.14932486048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.290523212521" Y="3.714326353621" />
                  <Point X="-22.315209507898" Y="3.996492000948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.863094609929" Y="-2.26120768804" />
                  <Point X="-21.896893915607" Y="-1.87487985634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.969588829795" Y="-1.043973185032" />
                  <Point X="-22.244380886937" Y="2.096914400487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.37333689947" Y="3.570888368494" />
                  <Point X="-22.416350236318" Y="4.062533058387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.96353068692" Y="-2.203220833285" />
                  <Point X="-22.000648563233" Y="-1.778961565658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.05587277096" Y="-1.147745982969" />
                  <Point X="-22.335158450404" Y="2.044503940494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.4561506271" Y="3.427450848353" />
                  <Point X="-22.516834917117" Y="4.121075457207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.063966763911" Y="-2.14523397853" />
                  <Point X="-22.111278675656" Y="-1.604456352743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.131692595614" Y="-1.371124179917" />
                  <Point X="-22.426851553519" Y="2.002558146562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.53896435473" Y="3.284013328212" />
                  <Point X="-22.617319558525" Y="4.179617405785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.164293148013" Y="-2.088500919244" />
                  <Point X="-22.520179042986" Y="1.979293474113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.62177808236" Y="3.140575808071" />
                  <Point X="-22.717804199933" Y="4.238159354363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.262120141219" Y="-2.060336028614" />
                  <Point X="-22.615489034167" Y="1.978688899952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.70459180999" Y="2.997138287931" />
                  <Point X="-22.817568252497" Y="4.288464934766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.359013996253" Y="-2.042836956097" />
                  <Point X="-22.712485837271" Y="1.997364674288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.78740553762" Y="2.85370076779" />
                  <Point X="-22.916999481044" Y="4.334966319245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.280472631128" Y="-4.030571625744" />
                  <Point X="-22.283689312282" Y="-3.993804791912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.45582631967" Y="-2.02626979423" />
                  <Point X="-22.812815966605" Y="2.054140541785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.87021926525" Y="2.710263247649" />
                  <Point X="-23.016430688297" Y="4.381467460342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.388107736595" Y="-3.890299498976" />
                  <Point X="-22.396083798476" Y="-3.799132694508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.550511112872" Y="-2.034020413988" />
                  <Point X="-22.917745098987" Y="2.163483254656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.952331854989" Y="2.558811684747" />
                  <Point X="-23.115861895551" Y="4.427968601438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.495742873221" Y="-3.750027016067" />
                  <Point X="-22.508478284671" Y="-3.604460597104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.642399538589" Y="-2.073733660364" />
                  <Point X="-23.214595463463" Y="4.466495688377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.603378071433" Y="-3.609753829226" />
                  <Point X="-22.620872770865" Y="-3.4097884997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.733564747688" Y="-2.121713310511" />
                  <Point X="-23.313083623348" Y="4.502217748725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.711013269645" Y="-3.469480642384" />
                  <Point X="-22.733267328879" Y="-3.21511558139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.823946285125" Y="-2.178650368744" />
                  <Point X="-23.411571777673" Y="4.537939745525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.818648467857" Y="-3.329207455542" />
                  <Point X="-22.845662000409" Y="-3.020441365594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.908572903331" Y="-2.301366454774" />
                  <Point X="-23.510059931999" Y="4.573661742325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.926283666069" Y="-3.188934268701" />
                  <Point X="-22.962383609819" Y="-2.776310023513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.989353118392" Y="-2.468047129941" />
                  <Point X="-23.607874512166" Y="4.601684751279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.033918864281" Y="-3.048661081859" />
                  <Point X="-23.705295190703" Y="4.625205443987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.141554062493" Y="-2.908387895017" />
                  <Point X="-23.802715854082" Y="4.648725963432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.245659768875" Y="-2.808456984389" />
                  <Point X="-23.900136510293" Y="4.672246400962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.346706158333" Y="-2.743494226224" />
                  <Point X="-23.997557166505" Y="4.695766838491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.447671486766" Y="-2.679457999801" />
                  <Point X="-24.094977822717" Y="4.719287276021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.545733030494" Y="-2.648612184436" />
                  <Point X="-24.192153712363" Y="4.740010018904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.64088255241" Y="-2.651050930697" />
                  <Point X="-24.288398412245" Y="4.750089214088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.735483813826" Y="-2.659756323141" />
                  <Point X="-24.384643116179" Y="4.760168455572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.830049483403" Y="-2.668868532173" />
                  <Point X="-24.480887835057" Y="4.770247867877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.922608688587" Y="-2.700914734154" />
                  <Point X="-24.577132553935" Y="4.780327280183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.01202527727" Y="-2.768881207117" />
                  <Point X="-24.660327309027" Y="4.641244923859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.100921512838" Y="-2.842795343393" />
                  <Point X="-24.732217364288" Y="4.372949257203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.187993040727" Y="-2.937565983879" />
                  <Point X="-24.810066901876" Y="4.172770785234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.182008225898" Y="-4.095975488729" />
                  <Point X="-24.197220405417" Y="-3.922099481191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.260125562853" Y="-3.203090241588" />
                  <Point X="-24.899541758635" Y="4.105470319436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.32360375044" Y="-3.567533995718" />
                  <Point X="-24.993123633137" Y="4.08511328125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.440095244782" Y="-3.326032880906" />
                  <Point X="-25.090351171231" Y="4.106426368596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.549081092669" Y="-3.170321697635" />
                  <Point X="-25.193501721611" Y="4.195439796153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.651243779726" Y="-3.092599599521" />
                  <Point X="-25.328114334728" Y="4.644066246358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.749452123503" Y="-3.060075851915" />
                  <Point X="-25.434880340181" Y="4.774404514497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.847476687914" Y="-3.029652712085" />
                  <Point X="-25.529276669744" Y="4.763356740259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.945276615216" Y="-3.001797186159" />
                  <Point X="-25.623672999308" Y="4.75230896602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.040662653365" Y="-3.001532539499" />
                  <Point X="-25.718069328871" Y="4.741261191782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.13374097035" Y="-3.02764526644" />
                  <Point X="-25.812465658435" Y="4.730213417544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.226582884057" Y="-3.05646009522" />
                  <Point X="-25.906861987999" Y="4.719165643306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.319416295839" Y="-3.085372101451" />
                  <Point X="-26.000280885923" Y="4.696945774304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.409953783217" Y="-3.140526643691" />
                  <Point X="-26.093360645974" Y="4.670849541688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.495635967857" Y="-3.251177550182" />
                  <Point X="-26.186440397975" Y="4.644753217038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.580323523274" Y="-3.373197120718" />
                  <Point X="-26.279520136849" Y="4.61865674236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.660012803472" Y="-3.552347238418" />
                  <Point X="-26.372599875723" Y="4.592560267682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.73190284909" Y="-3.820643015295" />
                  <Point X="-26.465679614597" Y="4.566463793004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.803792886232" Y="-4.088938889056" />
                  <Point X="-26.527962912358" Y="4.188362385657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.875682915223" Y="-4.357234855981" />
                  <Point X="-26.615728096353" Y="4.101520270741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.947572944214" Y="-4.625530822906" />
                  <Point X="-26.707513156255" Y="4.060625547693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.031355805916" Y="-4.757891089918" />
                  <Point X="-26.800344815006" Y="4.031693504231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.128761671464" Y="-4.734539710444" />
                  <Point X="-26.133914415932" Y="-4.67564357167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.170015051303" Y="-4.263011421223" />
                  <Point X="-26.896274816464" Y="4.038175679961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.284999333741" Y="-4.038737817296" />
                  <Point X="-26.995515813332" Y="4.082502706406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.388287121595" Y="-3.948155758224" />
                  <Point X="-27.096633385915" Y="4.148279091421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.491574808028" Y="-3.857574858409" />
                  <Point X="-27.202034296635" Y="4.263014255364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.591499583256" Y="-3.805432209543" />
                  <Point X="-27.292976737189" Y="4.212488349098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.687966151815" Y="-3.792817043785" />
                  <Point X="-27.383919194721" Y="4.161962636897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.783879026349" Y="-3.786530629696" />
                  <Point X="-27.474861652254" Y="4.111436924696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.879791902717" Y="-3.780244194634" />
                  <Point X="-27.565804109786" Y="4.060911212496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.975398458162" Y="-3.777459023751" />
                  <Point X="-27.656601491155" Y="4.008727272156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.068591927557" Y="-3.802255552628" />
                  <Point X="-27.745969785849" Y="3.940208796379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.159050075334" Y="-3.858316950664" />
                  <Point X="-27.756009387493" Y="2.964959209928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.777179001644" Y="3.206929006906" />
                  <Point X="-27.835338091195" Y="3.871690442359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.249146104939" Y="-3.918517378351" />
                  <Point X="-27.352042963551" Y="-2.742400902625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.378915060646" Y="-2.435251427347" />
                  <Point X="-27.8397571334" Y="2.832197567547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.890005178763" Y="3.406535354157" />
                  <Point X="-27.924706441498" Y="3.803172602193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.339242080698" Y="-3.978718421489" />
                  <Point X="-27.434856669821" Y="-2.88583866691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.480415772212" Y="-2.365095743716" />
                  <Point X="-27.928465764327" Y="2.756139100412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.002399859756" Y="3.60120967812" />
                  <Point X="-28.014074791801" Y="3.734654762028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.427194283244" Y="-4.06342290459" />
                  <Point X="-27.517670376091" Y="-3.029276431195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.577193488012" Y="-2.34892414872" />
                  <Point X="-28.021148021114" Y="2.725499384683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.514503146336" Y="-4.155480791296" />
                  <Point X="-27.600484082361" Y="-3.17271419548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.670580431404" Y="-2.371509259686" />
                  <Point X="-28.116712353429" Y="2.727801942991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.615327828449" Y="-4.093052159666" />
                  <Point X="-27.683297788632" Y="-3.316151959765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.761514619819" Y="-2.422129488336" />
                  <Point X="-28.213569311998" Y="2.744879286973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.716152440455" Y="-4.030624329378" />
                  <Point X="-27.766111494902" Y="-3.459589724051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.852292199924" Y="-2.474539758153" />
                  <Point X="-27.94891273425" Y="-1.370161997291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.955348503832" Y="-1.296600814364" />
                  <Point X="-28.313460522618" Y="2.796638290616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.817958548379" Y="-3.956977949405" />
                  <Point X="-27.848925201172" Y="-3.603027488336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.94306978003" Y="-2.52695002797" />
                  <Point X="-28.028158206747" Y="-1.554384860228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.063744969128" Y="-1.14762630493" />
                  <Point X="-28.413896598429" Y="2.854625131873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.920209386909" Y="-3.878248275337" />
                  <Point X="-27.931738887507" Y="-3.746465480479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.033847360135" Y="-2.579360297787" />
                  <Point X="-28.117179385792" Y="-1.626870886031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.162729473594" Y="-1.106231000056" />
                  <Point X="-28.514332674239" Y="2.912611973131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.124624932166" Y="-2.631770659894" />
                  <Point X="-28.206543069171" Y="-1.695442069386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.258457551597" Y="-1.102056819984" />
                  <Point X="-28.338812950074" Y="-0.18359041258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.368980402771" Y="0.16122514959" />
                  <Point X="-28.473193301753" Y="1.352384035576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.506385852633" Y="1.731776628189" />
                  <Point X="-28.614768750049" Y="2.970598814388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.215402492753" Y="-2.684181152805" />
                  <Point X="-28.295906752549" Y="-1.764013252742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.352734549769" Y="-1.114468558268" />
                  <Point X="-28.426393761083" Y="-0.272539920367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.470789991231" Y="0.234911312268" />
                  <Point X="-28.560410083099" Y="1.259273649698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.609463501826" Y="1.819956791374" />
                  <Point X="-28.715204840878" Y="3.028585827303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.30618005334" Y="-2.736591645717" />
                  <Point X="-28.385270435928" Y="-1.832584436097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.447011547941" Y="-1.126880296551" />
                  <Point X="-28.518060253038" Y="-0.314789881253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.568858903335" Y="0.265841348549" />
                  <Point X="-28.652479167691" Y="1.221625343717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.711689059202" Y="1.898397500522" />
                  <Point X="-28.80255870042" Y="2.937042252376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.396957613927" Y="-2.789002138629" />
                  <Point X="-28.474634119306" Y="-1.901155619453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.541288546113" Y="-1.139292034835" />
                  <Point X="-28.611238796207" Y="-0.339757017678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.666510997986" Y="0.292007139548" />
                  <Point X="-28.746333984046" Y="1.204388045181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.813914615414" Y="1.97683819637" />
                  <Point X="-28.888281604298" Y="2.826856768908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.487735174514" Y="-2.84141263154" />
                  <Point X="-28.563997802684" Y="-1.969726802809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.635565544285" Y="-1.151703773119" />
                  <Point X="-28.704417339375" Y="-0.364724154102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.764163092637" Y="0.318172930546" />
                  <Point X="-28.842546104684" Y="1.214094857889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.91614015046" Y="2.055278650284" />
                  <Point X="-28.974004508176" Y="2.71667128544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.578512735101" Y="-2.893823124452" />
                  <Point X="-28.653361486063" Y="-2.038297986164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.729842542458" Y="-1.164115511402" />
                  <Point X="-28.797595882544" Y="-0.389691290527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.861815187288" Y="0.344338721545" />
                  <Point X="-28.939020184112" Y="1.226795873279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.018365685506" Y="2.133719104197" />
                  <Point X="-29.0578574757" Y="2.585112331641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.669290295688" Y="-2.946233617363" />
                  <Point X="-28.742725169441" Y="-2.10686916952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.824119534073" Y="-1.176527324628" />
                  <Point X="-28.890774425713" Y="-0.414658426951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.959467281939" Y="0.370504512543" />
                  <Point X="-29.035494263541" Y="1.239496888669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.120591220552" Y="2.212159558111" />
                  <Point X="-29.14078980165" Y="2.443030396504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.760067856275" Y="-2.998644110275" />
                  <Point X="-28.83208885282" Y="-2.175440352875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.91839652393" Y="-1.188939157955" />
                  <Point X="-28.983952968882" Y="-0.439625563376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.057119378375" Y="0.396670323934" />
                  <Point X="-29.131968342969" Y="1.252197904059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.222816755598" Y="2.290600012025" />
                  <Point X="-29.223722017669" Y="2.300947204852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.86164263426" Y="-2.927641843606" />
                  <Point X="-28.921452536198" Y="-2.244011536231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.012673513787" Y="-1.201350991282" />
                  <Point X="-29.07713151205" Y="-0.4645926998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.154771475004" Y="0.422836137539" />
                  <Point X="-29.228442422398" Y="1.264898919449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.969390240454" Y="-2.786083827646" />
                  <Point X="-29.010816228116" Y="-2.312582621983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.106950503644" Y="-1.213762824609" />
                  <Point X="-29.170310055219" Y="-0.489559836225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.252423571633" Y="0.449001951144" />
                  <Point X="-29.324916501826" Y="1.27759993484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.079859607153" Y="-2.61341594677" />
                  <Point X="-29.1001799233" Y="-2.381153670396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.201227493501" Y="-1.226174657936" />
                  <Point X="-29.263488598388" Y="-0.514526972649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.350075668262" Y="0.475167764749" />
                  <Point X="-29.421390581255" Y="1.29030095023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.295504483358" Y="-1.238586491262" />
                  <Point X="-29.356667141557" Y="-0.539494109073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.44772776489" Y="0.501333578354" />
                  <Point X="-29.517864660793" Y="1.303001966877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.389781473215" Y="-1.250998324589" />
                  <Point X="-29.449845684725" Y="-0.564461245498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.545379861519" Y="0.527499391959" />
                  <Point X="-29.614338741862" Y="1.315703001008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.484058463072" Y="-1.263410157916" />
                  <Point X="-29.543024228173" Y="-0.589428378738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.643031958148" Y="0.553665205564" />
                  <Point X="-29.691292181144" Y="1.105282078543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.578335452929" Y="-1.275821991243" />
                  <Point X="-29.636202773557" Y="-0.614395489836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.740684054777" Y="0.579831019169" />
                  <Point X="-29.757975131848" Y="0.777468934462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.678909456215" Y="-1.21625863172" />
                  <Point X="-29.729381318942" Y="-0.639362600934" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.252267578125" Y="-4.567872558594" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521543945312" />
                  <Point X="-24.64856640625" Y="-3.358895996094" />
                  <Point X="-24.699408203125" Y="-3.285643554688" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.900349609375" Y="-3.212184082031" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.18334765625" Y="-3.241982177734" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643798828" />
                  <Point X="-25.4011640625" Y="-3.448291503906" />
                  <Point X="-25.452005859375" Y="-3.521544189453" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.5836171875" Y="-4.001341796875" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-26.008642578125" Y="-4.955845703125" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.302662109375" Y="-4.885985351562" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.342396484375" Y="-4.803573730469" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.534756835938" />
                  <Point X="-26.3514140625" Y="-4.324955078125" />
                  <Point X="-26.3702109375" Y="-4.230465332031" />
                  <Point X="-26.386283203125" Y="-4.202628417969" />
                  <Point X="-26.547109375" Y="-4.061586181641" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.8626953125" Y="-3.971772460938" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791259766" />
                  <Point X="-27.167740234375" Y="-4.092634765625" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.329568359375" Y="-4.248305175781" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.721564453125" Y="-4.25074609375" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.13612109375" Y="-3.951798339844" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-28.019302734375" Y="-3.518129882812" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597594482422" />
                  <Point X="-27.5139765625" Y="-2.568765869141" />
                  <Point X="-27.53132421875" Y="-2.551417236328" />
                  <Point X="-27.560154296875" Y="-2.537198730469" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-27.992072265625" Y="-2.774634521484" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-29.055625" Y="-2.986494873047" />
                  <Point X="-29.161697265625" Y="-2.847136962891" />
                  <Point X="-29.362646484375" Y="-2.510175292969" />
                  <Point X="-29.43101953125" Y="-2.395526367188" />
                  <Point X="-29.06184375" Y="-2.112248046875" />
                  <Point X="-28.163787109375" Y="-1.423144897461" />
                  <Point X="-28.145822265625" Y="-1.396014038086" />
                  <Point X="-28.140509765625" Y="-1.37550402832" />
                  <Point X="-28.1381171875" Y="-1.366266601562" />
                  <Point X="-28.140326171875" Y="-1.334596801758" />
                  <Point X="-28.16115625" Y="-1.310640380859" />
                  <Point X="-28.179416015625" Y="-1.299893676758" />
                  <Point X="-28.187638671875" Y="-1.295053710938" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.72498046875" Y="-1.355115112305" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.88590234375" Y="-1.173616455078" />
                  <Point X="-29.927392578125" Y="-1.011187744141" />
                  <Point X="-29.980556640625" Y="-0.63946685791" />
                  <Point X="-29.99839453125" Y="-0.5147421875" />
                  <Point X="-29.580484375" Y="-0.402763305664" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.5418984375" Y="-0.121426872253" />
                  <Point X="-28.522763671875" Y="-0.108146209717" />
                  <Point X="-28.51414453125" Y="-0.102164848328" />
                  <Point X="-28.494896484375" Y="-0.07590486908" />
                  <Point X="-28.48851953125" Y="-0.055353652954" />
                  <Point X="-28.4856484375" Y="-0.046102703094" />
                  <Point X="-28.485646484375" Y="-0.016462242126" />
                  <Point X="-28.4920234375" Y="0.004088969707" />
                  <Point X="-28.4948984375" Y="0.013349503517" />
                  <Point X="-28.51414453125" Y="0.03960477066" />
                  <Point X="-28.533279296875" Y="0.052885433197" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.01820703125" Y="0.189541366577" />
                  <Point X="-29.998185546875" Y="0.452126037598" />
                  <Point X="-29.9443828125" Y="0.815725341797" />
                  <Point X="-29.91764453125" Y="0.996415039063" />
                  <Point X="-29.810625" Y="1.391352416992" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.486591796875" Y="1.490524536133" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.6893515625" Y="1.409220092773" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.651533203125" Y="1.426057128906" />
                  <Point X="-28.6391171875" Y="1.443788574219" />
                  <Point X="-28.622123046875" Y="1.484815429688" />
                  <Point X="-28.614470703125" Y="1.50329284668" />
                  <Point X="-28.61071484375" Y="1.524608276367" />
                  <Point X="-28.61631640625" Y="1.545514526367" />
                  <Point X="-28.6368203125" Y="1.584904174805" />
                  <Point X="-28.646056640625" Y="1.602644287109" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-28.924251953125" Y="1.822014160156" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.263908203125" Y="2.609010498047" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-28.8765234375" Y="3.151393798828" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.609544921875" Y="3.186975585938" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.079529296875" Y="2.915274414062" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927405029297" />
                  <Point X="-27.9713828125" Y="2.969272460938" />
                  <Point X="-27.95252734375" Y="2.988128417969" />
                  <Point X="-27.9408984375" Y="3.0063828125" />
                  <Point X="-27.938072265625" Y="3.027841308594" />
                  <Point X="-27.943232421875" Y="3.086825683594" />
                  <Point X="-27.945556640625" Y="3.113390869141" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.0691796875" Y="3.336877197266" />
                  <Point X="-28.30727734375" Y="3.749276855469" />
                  <Point X="-27.93382421875" Y="4.035599365234" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.30638671875" Y="4.422391113281" />
                  <Point X="-27.141546875" Y="4.513972167969" />
                  <Point X="-27.105703125" Y="4.467259277344" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.88559765625" Y="4.239485839844" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.81380859375" Y="4.22225" />
                  <Point X="-26.745431640625" Y="4.250573730469" />
                  <Point X="-26.714634765625" Y="4.263329589844" />
                  <Point X="-26.696904296875" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.294488769531" />
                  <Point X="-26.663826171875" Y="4.365075195313" />
                  <Point X="-26.653802734375" Y="4.396865234375" />
                  <Point X="-26.651916015625" Y="4.418426757812" />
                  <Point X="-26.66523046875" Y="4.519559570312" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.20234375" Y="4.837620605469" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.4268125" Y="4.966645507812" />
                  <Point X="-25.22419921875" Y="4.990358398438" />
                  <Point X="-25.175376953125" Y="4.808152832031" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.8855390625" Y="4.534852539062" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.344330078125" Y="4.946985839844" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.691978515625" Y="4.81744921875" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.199544921875" Y="4.663148925781" />
                  <Point X="-23.068970703125" Y="4.615788085938" />
                  <Point X="-22.787115234375" Y="4.483973632812" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.389021484375" Y="4.266504394531" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-22.0106796875" Y="4.013072753906" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.176072265625" Y="3.532560791016" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491516601562" />
                  <Point X="-22.78994921875" Y="2.436161132812" />
                  <Point X="-22.7966171875" Y="2.411230224609" />
                  <Point X="-22.797955078125" Y="2.392325683594" />
                  <Point X="-22.79218359375" Y="2.344458984375" />
                  <Point X="-22.789583984375" Y="2.322900878906" />
                  <Point X="-22.78131640625" Y="2.300811523438" />
                  <Point X="-22.751697265625" Y="2.257161621094" />
                  <Point X="-22.738357421875" Y="2.237502929688" />
                  <Point X="-22.725056640625" Y="2.224202636719" />
                  <Point X="-22.681408203125" Y="2.194584228516" />
                  <Point X="-22.661748046875" Y="2.181245117188" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.591794921875" Y="2.167208251953" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5513359375" Y="2.165946044922" />
                  <Point X="-22.49598046875" Y="2.180749023438" />
                  <Point X="-22.471048828125" Y="2.187415771484" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.995544921875" Y="2.459973144531" />
                  <Point X="-21.005751953125" Y="3.031430664062" />
                  <Point X="-20.86950390625" Y="2.842078369141" />
                  <Point X="-20.797404296875" Y="2.741875244141" />
                  <Point X="-20.654248046875" Y="2.505307373047" />
                  <Point X="-20.612486328125" Y="2.436295654297" />
                  <Point X="-20.92923046875" Y="2.193248779297" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583831665039" />
                  <Point X="-21.76046875" Y="1.531858154297" />
                  <Point X="-21.77841015625" Y="1.508450561523" />
                  <Point X="-21.78687890625" Y="1.491503540039" />
                  <Point X="-21.80171875" Y="1.438438354492" />
                  <Point X="-21.808404296875" Y="1.4145390625" />
                  <Point X="-21.809220703125" Y="1.390965820312" />
                  <Point X="-21.797037109375" Y="1.331923950195" />
                  <Point X="-21.79155078125" Y="1.305333007812" />
                  <Point X="-21.784353515625" Y="1.287956665039" />
                  <Point X="-21.75121875" Y="1.237593505859" />
                  <Point X="-21.736296875" Y="1.214911132812" />
                  <Point X="-21.719052734375" Y="1.19882019043" />
                  <Point X="-21.67103515625" Y="1.171790893555" />
                  <Point X="-21.64941015625" Y="1.159617675781" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.56651171875" Y="1.145039428711" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-21.083978515625" Y="1.199127563477" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.09177734375" Y="1.078580566406" />
                  <Point X="-20.060806640625" Y="0.951366760254" />
                  <Point X="-20.015697265625" Y="0.661627197266" />
                  <Point X="-20.002140625" Y="0.574556274414" />
                  <Point X="-20.36246875" Y="0.478006439209" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819351196" />
                  <Point X="-21.3346953125" Y="0.195951385498" />
                  <Point X="-21.363421875" Y="0.179346984863" />
                  <Point X="-21.377734375" Y="0.166927154541" />
                  <Point X="-21.41600390625" Y="0.118162017822" />
                  <Point X="-21.433240234375" Y="0.09619947052" />
                  <Point X="-21.443013671875" Y="0.074736328125" />
                  <Point X="-21.45576953125" Y="0.008125679016" />
                  <Point X="-21.461515625" Y="-0.021874082565" />
                  <Point X="-21.461515625" Y="-0.040685997009" />
                  <Point X="-21.448759765625" Y="-0.107296646118" />
                  <Point X="-21.443013671875" Y="-0.137296401978" />
                  <Point X="-21.433240234375" Y="-0.158759552002" />
                  <Point X="-21.394970703125" Y="-0.207524520874" />
                  <Point X="-21.377734375" Y="-0.229487228394" />
                  <Point X="-21.363421875" Y="-0.241907058716" />
                  <Point X="-21.299638671875" Y="-0.278775024414" />
                  <Point X="-21.270912109375" Y="-0.295379425049" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.855130859375" Y="-0.408558044434" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.03427734375" Y="-0.851721252441" />
                  <Point X="-20.051568359375" Y="-0.96641418457" />
                  <Point X="-20.109365234375" Y="-1.219679321289" />
                  <Point X="-20.125453125" Y="-1.290178588867" />
                  <Point X="-20.548490234375" Y="-1.234484741211" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.730345703125" Y="-1.125550537109" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.814552734375" Y="-1.154697265625" />
                  <Point X="-21.89021875" Y="-1.245699951172" />
                  <Point X="-21.924296875" Y="-1.286685424805" />
                  <Point X="-21.935640625" Y="-1.314071899414" />
                  <Point X="-21.946486328125" Y="-1.431924316406" />
                  <Point X="-21.951369140625" Y="-1.485002197266" />
                  <Point X="-21.94363671875" Y="-1.516622924805" />
                  <Point X="-21.874359375" Y="-1.624381591797" />
                  <Point X="-21.84315625" Y="-1.672913452148" />
                  <Point X="-21.831537109375" Y="-1.685540771484" />
                  <Point X="-21.45690234375" Y="-1.973009277344" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.74712890625" Y="-2.723276367188" />
                  <Point X="-20.7958671875" Y="-2.802140136719" />
                  <Point X="-20.915396484375" Y="-2.971976318359" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.320705078125" Y="-2.79375" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.411646484375" Y="-2.226405517578" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.6346953125" Y="-2.284385986328" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.334683837891" />
                  <Point X="-22.776541015625" Y="-2.458457275391" />
                  <Point X="-22.80587890625" Y="-2.514201904297" />
                  <Point X="-22.8108359375" Y="-2.546375" />
                  <Point X="-22.783927734375" Y="-2.695364257812" />
                  <Point X="-22.77180859375" Y="-2.762465332031" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.525166015625" Y="-3.195554931641" />
                  <Point X="-22.01332421875" Y="-4.082088867188" />
                  <Point X="-22.1068671875" Y="-4.148903808594" />
                  <Point X="-22.16469921875" Y="-4.1902109375" />
                  <Point X="-22.298341796875" Y="-4.276716308594" />
                  <Point X="-22.32022265625" Y="-4.290879394531" />
                  <Point X="-22.6103203125" Y="-3.912817871094" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467529297" />
                  <Point X="-23.47639453125" Y="-2.885996337891" />
                  <Point X="-23.54257421875" Y="-2.843448730469" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.73490234375" Y="-2.850505615234" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868508789062" />
                  <Point X="-23.958759765625" Y="-2.971689208984" />
                  <Point X="-24.014650390625" Y="-3.018159179688" />
                  <Point X="-24.03154296875" Y="-3.045986083984" />
                  <Point X="-24.068646484375" Y="-3.216692138672" />
                  <Point X="-24.085357421875" Y="-3.293573974609" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-24.017845703125" Y="-3.828934814453" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.95094140625" Y="-4.951254394531" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.129111328125" Y="-4.985674804688" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#186" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.133237562358" Y="4.852373851146" Z="1.75" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.75" />
                  <Point X="-0.438781643995" Y="5.047586503057" Z="1.75" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.75" />
                  <Point X="-1.221998719308" Y="4.917044297182" Z="1.75" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.75" />
                  <Point X="-1.720960238234" Y="4.544313154938" Z="1.75" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.75" />
                  <Point X="-1.717669363323" Y="4.411390252634" Z="1.75" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.75" />
                  <Point X="-1.770720984793" Y="4.328047938753" Z="1.75" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.75" />
                  <Point X="-1.868665913927" Y="4.31511622377" Z="1.75" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.75" />
                  <Point X="-2.072192926071" Y="4.528977152677" Z="1.75" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.75" />
                  <Point X="-2.336826043959" Y="4.497378593855" Z="1.75" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.75" />
                  <Point X="-2.97040389209" Y="4.10655901722" Z="1.75" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.75" />
                  <Point X="-3.118636982412" Y="3.343157344521" Z="1.75" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.75" />
                  <Point X="-2.99920051867" Y="3.113747815525" Z="1.75" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.75" />
                  <Point X="-3.012896106299" Y="3.035907461587" Z="1.75" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.75" />
                  <Point X="-3.081328650081" Y="2.996364180503" Z="1.75" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.75" />
                  <Point X="-3.590702003777" Y="3.261556966333" Z="1.75" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.75" />
                  <Point X="-3.92214327684" Y="3.213376138189" Z="1.75" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.75" />
                  <Point X="-4.313246869247" Y="2.665495621751" Z="1.75" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.75" />
                  <Point X="-3.960846408983" Y="1.813626435788" Z="1.75" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.75" />
                  <Point X="-3.687327362472" Y="1.593093988282" Z="1.75" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.75" />
                  <Point X="-3.674476056207" Y="1.535226899985" Z="1.75" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.75" />
                  <Point X="-3.710544181661" Y="1.488186020896" Z="1.75" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.75" />
                  <Point X="-4.486223036353" Y="1.571376890567" Z="1.75" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.75" />
                  <Point X="-4.865041566031" Y="1.435709747809" Z="1.75" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.75" />
                  <Point X="-5.000000433484" Y="0.85432456427" Z="1.75" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.75" />
                  <Point X="-4.03730521149" Y="0.172525144176" Z="1.75" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.75" />
                  <Point X="-3.567942812324" Y="0.043087701108" Z="1.75" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.75" />
                  <Point X="-3.545935168938" Y="0.020551188964" Z="1.75" />
                  <Point X="-3.539556741714" Y="0" Z="1.75" />
                  <Point X="-3.542429432197" Y="-0.009255762097" Z="1.75" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.75" />
                  <Point X="-3.557425782799" Y="-0.035788282026" Z="1.75" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.75" />
                  <Point X="-4.599582577282" Y="-0.32318691258" Z="1.75" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.75" />
                  <Point X="-5.036210134611" Y="-0.615265974791" Z="1.75" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.75" />
                  <Point X="-4.940505535415" Y="-1.154710719993" Z="1.75" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.75" />
                  <Point X="-3.724611969136" Y="-1.373407455751" Z="1.75" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.75" />
                  <Point X="-3.210935420825" Y="-1.311703282798" Z="1.75" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.75" />
                  <Point X="-3.195068733814" Y="-1.331687063162" Z="1.75" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.75" />
                  <Point X="-4.098437448361" Y="-2.041300242537" Z="1.75" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.75" />
                  <Point X="-4.41174772234" Y="-2.504505184164" Z="1.75" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.75" />
                  <Point X="-4.101567165492" Y="-2.985497953095" Z="1.75" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.75" />
                  <Point X="-2.973228452614" Y="-2.786655637828" Z="1.75" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.75" />
                  <Point X="-2.567452370976" Y="-2.560878098155" Z="1.75" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.75" />
                  <Point X="-3.06876137534" Y="-3.461849929878" Z="1.75" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.75" />
                  <Point X="-3.172781984856" Y="-3.960135573622" Z="1.75" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.75" />
                  <Point X="-2.754044553842" Y="-4.261976816494" Z="1.75" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.75" />
                  <Point X="-2.296057632552" Y="-4.247463354827" Z="1.75" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.75" />
                  <Point X="-2.14611765573" Y="-4.10292790368" Z="1.75" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.75" />
                  <Point X="-1.872121256458" Y="-3.990385378377" Z="1.75" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.75" />
                  <Point X="-1.586233841322" Y="-4.067898064132" Z="1.75" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.75" />
                  <Point X="-1.406610778674" Y="-4.303430271488" Z="1.75" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.75" />
                  <Point X="-1.398125441968" Y="-4.765767450447" Z="1.75" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.75" />
                  <Point X="-1.321278084214" Y="-4.903127986571" Z="1.75" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.75" />
                  <Point X="-1.024327429229" Y="-4.973649643129" Z="1.75" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.75" />
                  <Point X="-0.541476732207" Y="-3.98300273342" Z="1.75" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.75" />
                  <Point X="-0.366245463111" Y="-3.445520434067" Z="1.75" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.75" />
                  <Point X="-0.174685102981" Y="-3.258455177922" Z="1.75" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.75" />
                  <Point X="0.07867397638" Y="-3.228656867366" Z="1.75" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.75" />
                  <Point X="0.304200396321" Y="-3.356125321701" Z="1.75" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.75" />
                  <Point X="0.693278128194" Y="-4.549533296396" Z="1.75" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.75" />
                  <Point X="0.873668553797" Y="-5.003589708679" Z="1.75" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.75" />
                  <Point X="1.053607180462" Y="-4.968814541883" Z="1.75" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.75" />
                  <Point X="1.025570054173" Y="-3.791128415499" Z="1.75" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.75" />
                  <Point X="0.9740563735" Y="-3.196031873522" Z="1.75" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.75" />
                  <Point X="1.067049215526" Y="-2.978855976917" Z="1.75" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.75" />
                  <Point X="1.263522676685" Y="-2.86901506527" Z="1.75" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.75" />
                  <Point X="1.490410316909" Y="-2.896774124765" Z="1.75" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.75" />
                  <Point X="2.343855369905" Y="-3.911975727225" Z="1.75" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.75" />
                  <Point X="2.722669235403" Y="-4.287410801727" Z="1.75" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.75" />
                  <Point X="2.916036764197" Y="-4.158310829063" Z="1.75" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.75" />
                  <Point X="2.511978309192" Y="-3.139274805866" Z="1.75" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.75" />
                  <Point X="2.259118469261" Y="-2.655197438016" Z="1.75" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.75" />
                  <Point X="2.261549099374" Y="-2.450463608503" Z="1.75" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.75" />
                  <Point X="2.382434769869" Y="-2.297352210593" Z="1.75" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.75" />
                  <Point X="2.573309365497" Y="-2.24432954023" Z="1.75" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.75" />
                  <Point X="3.648138791241" Y="-2.805771191083" Z="1.75" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.75" />
                  <Point X="4.119334657233" Y="-2.969473903586" Z="1.75" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.75" />
                  <Point X="4.289246511857" Y="-2.718281999551" Z="1.75" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.75" />
                  <Point X="3.567378917672" Y="-1.902061452437" Z="1.75" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.75" />
                  <Point X="3.161541313283" Y="-1.566061128812" Z="1.75" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.75" />
                  <Point X="3.097146717003" Y="-1.405224614199" Z="1.75" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.75" />
                  <Point X="3.142069582573" Y="-1.246386825945" Z="1.75" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.75" />
                  <Point X="3.274115540781" Y="-1.143129754119" Z="1.75" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.75" />
                  <Point X="4.438828154631" Y="-1.252776910431" Z="1.75" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.75" />
                  <Point X="4.933224760928" Y="-1.199522842931" Z="1.75" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.75" />
                  <Point X="5.009006798996" Y="-0.827895229634" Z="1.75" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.75" />
                  <Point X="4.151652777129" Y="-0.328981741653" Z="1.75" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.75" />
                  <Point X="3.71922616501" Y="-0.204206184939" Z="1.75" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.75" />
                  <Point X="3.6382067939" Y="-0.145375593918" Z="1.75" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.75" />
                  <Point X="3.594191416779" Y="-0.066610680143" Z="1.75" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.75" />
                  <Point X="3.587180033645" Y="0.029999851053" Z="1.75" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.75" />
                  <Point X="3.6171726445" Y="0.118573144657" Z="1.75" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.75" />
                  <Point X="3.684169249342" Y="0.183942659303" Z="1.75" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.75" />
                  <Point X="4.644315040178" Y="0.460990191116" Z="1.75" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.75" />
                  <Point X="5.027550918197" Y="0.700599508361" Z="1.75" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.75" />
                  <Point X="4.950648328322" Y="1.121687517752" Z="1.75" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.75" />
                  <Point X="3.903339575496" Y="1.279979921397" Z="1.75" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.75" />
                  <Point X="3.433882263975" Y="1.225888393029" Z="1.75" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.75" />
                  <Point X="3.347335112815" Y="1.246641866184" Z="1.75" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.75" />
                  <Point X="3.284395546111" Y="1.296353344029" Z="1.75" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.75" />
                  <Point X="3.245774365821" Y="1.373307410625" Z="1.75" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.75" />
                  <Point X="3.24027572885" Y="1.456248498774" Z="1.75" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.75" />
                  <Point X="3.273058714971" Y="1.532721406608" Z="1.75" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.75" />
                  <Point X="4.095049632964" Y="2.184861014768" Z="1.75" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.75" />
                  <Point X="4.382372726462" Y="2.562473965047" Z="1.75" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.75" />
                  <Point X="4.164924371116" Y="2.90256162498" Z="1.75" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.75" />
                  <Point X="2.973298154389" Y="2.534554549576" Z="1.75" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.75" />
                  <Point X="2.484947139353" Y="2.260331952278" Z="1.75" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.75" />
                  <Point X="2.408033595093" Y="2.248128677619" Z="1.75" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.75" />
                  <Point X="2.340507937013" Y="2.267239859768" Z="1.75" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.75" />
                  <Point X="2.283518740467" Y="2.316516923369" Z="1.75" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.75" />
                  <Point X="2.251301025139" Y="2.381724848739" Z="1.75" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.75" />
                  <Point X="2.25219599038" Y="2.454522436529" Z="1.75" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.75" />
                  <Point X="2.861070803283" Y="3.538840927566" Z="1.75" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.75" />
                  <Point X="3.012140251936" Y="4.085099806441" Z="1.75" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.75" />
                  <Point X="2.629991693364" Y="4.340986967909" Z="1.75" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.75" />
                  <Point X="2.227910373406" Y="4.56054566918" Z="1.75" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.75" />
                  <Point X="1.811346468538" Y="4.741432192479" Z="1.75" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.75" />
                  <Point X="1.313599580168" Y="4.897332788331" Z="1.75" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.75" />
                  <Point X="0.654720830782" Y="5.027994142017" Z="1.75" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.75" />
                  <Point X="0.060006999195" Y="4.579073654805" Z="1.75" />
                  <Point X="0" Y="4.355124473572" Z="1.75" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>