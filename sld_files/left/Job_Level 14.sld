<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#143" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1141" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004716064453" />
                  <Width Value="9.78389453125" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.333984375" Y="-3.895848144531" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140625" />
                  <Point X="-24.457634765625" Y="-3.467377197266" />
                  <Point X="-24.499615234375" Y="-3.406890869141" />
                  <Point X="-24.62136328125" Y="-3.231476806641" />
                  <Point X="-24.643248046875" Y="-3.209020263672" />
                  <Point X="-24.66950390625" Y="-3.189776611328" />
                  <Point X="-24.69750390625" Y="-3.175669189453" />
                  <Point X="-24.762466796875" Y="-3.155507080078" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.036822265625" Y="-3.097035888672" />
                  <Point X="-25.10178515625" Y="-3.117197753906" />
                  <Point X="-25.290181640625" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.18977734375" />
                  <Point X="-25.344439453125" Y="-3.209021484375" />
                  <Point X="-25.36632421875" Y="-3.231477050781" />
                  <Point X="-25.4083046875" Y="-3.291963134766" />
                  <Point X="-25.53005078125" Y="-3.467377441406" />
                  <Point X="-25.5381875" Y="-3.481571777344" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.848861328125" Y="-4.624190429688" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.079341796875" Y="-4.845349609375" />
                  <Point X="-26.150931640625" Y="-4.8269296875" />
                  <Point X="-26.246416015625" Y="-4.802362304688" />
                  <Point X="-26.227326171875" Y="-4.657352539062" />
                  <Point X="-26.2149609375" Y="-4.563438476562" />
                  <Point X="-26.21419921875" Y="-4.5479296875" />
                  <Point X="-26.2165078125" Y="-4.516223632812" />
                  <Point X="-26.23202734375" Y="-4.438201660156" />
                  <Point X="-26.277037109375" Y="-4.211932128906" />
                  <Point X="-26.287939453125" Y="-4.182964355469" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131204589844" />
                  <Point X="-26.383453125" Y="-4.078752929688" />
                  <Point X="-26.556904296875" Y="-3.926639648438" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.722408203125" Y="-3.885763427734" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708496094" />
                  <Point X="-27.014466796875" Y="-3.882028076172" />
                  <Point X="-27.042658203125" Y="-3.894802001953" />
                  <Point X="-27.10880078125" Y="-3.938998046875" />
                  <Point X="-27.300623046875" Y="-4.067170166016" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.3351015625" Y="-4.099461914062" />
                  <Point X="-27.4801484375" Y="-4.288489746094" />
                  <Point X="-27.481283203125" Y="-4.287787597656" />
                  <Point X="-27.80170703125" Y="-4.089388671875" />
                  <Point X="-27.90084375" Y="-4.013056884766" />
                  <Point X="-28.104720703125" Y="-3.856077392578" />
                  <Point X="-27.614638671875" Y="-3.007228759766" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647653076172" />
                  <Point X="-27.406587890625" Y="-2.616124023438" />
                  <Point X="-27.40557421875" Y="-2.585186279297" />
                  <Point X="-27.4145625" Y="-2.555565429688" />
                  <Point X="-27.428783203125" Y="-2.526735351562" />
                  <Point X="-27.446806640625" Y="-2.501586181641" />
                  <Point X="-27.4641484375" Y="-2.484243896484" />
                  <Point X="-27.489302734375" Y="-2.466217529297" />
                  <Point X="-27.5181328125" Y="-2.451998291016" />
                  <Point X="-27.54775" Y="-2.443012207031" />
                  <Point X="-27.5786875" Y="-2.444024169922" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.59797265625" Y="-3.014755126953" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-28.829705078125" Y="-3.126454101562" />
                  <Point X="-29.082861328125" Y="-2.793860595703" />
                  <Point X="-29.1539296875" Y="-2.674688476562" />
                  <Point X="-29.306142578125" Y="-2.419450439453" />
                  <Point X="-28.439927734375" Y="-1.754780029297" />
                  <Point X="-28.105955078125" Y="-1.498513305664" />
                  <Point X="-28.083068359375" Y="-1.475884399414" />
                  <Point X="-28.063818359375" Y="-1.445054199219" />
                  <Point X="-28.055126953125" Y="-1.422354980469" />
                  <Point X="-28.05187890625" Y="-1.412201904297" />
                  <Point X="-28.046150390625" Y="-1.39008190918" />
                  <Point X="-28.042037109375" Y="-1.358598266602" />
                  <Point X="-28.04273828125" Y="-1.335732666016" />
                  <Point X="-28.0488828125" Y="-1.313697265625" />
                  <Point X="-28.06088671875" Y="-1.284715820312" />
                  <Point X="-28.0736484375" Y="-1.262768920898" />
                  <Point X="-28.091767578125" Y="-1.244985595703" />
                  <Point X="-28.111060546875" Y="-1.230559936523" />
                  <Point X="-28.11976171875" Y="-1.224772094727" />
                  <Point X="-28.139453125" Y="-1.213182006836" />
                  <Point X="-28.16871484375" Y="-1.201957397461" />
                  <Point X="-28.2006015625" Y="-1.195475097656" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-29.44230859375" Y="-1.353733642578" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.735064453125" Y="-1.380278076172" />
                  <Point X="-29.834078125" Y="-0.992650695801" />
                  <Point X="-29.85287890625" Y="-0.861186584473" />
                  <Point X="-29.892421875" Y="-0.584698303223" />
                  <Point X="-28.9133203125" Y="-0.322348449707" />
                  <Point X="-28.532875" Y="-0.220408309937" />
                  <Point X="-28.512583984375" Y="-0.212376022339" />
                  <Point X="-28.4898984375" Y="-0.200216705322" />
                  <Point X="-28.480609375" Y="-0.194530181885" />
                  <Point X="-28.45997265625" Y="-0.180207092285" />
                  <Point X="-28.436015625" Y="-0.15867729187" />
                  <Point X="-28.415291015625" Y="-0.128735580444" />
                  <Point X="-28.40549609375" Y="-0.106357574463" />
                  <Point X="-28.401794921875" Y="-0.096424255371" />
                  <Point X="-28.394916015625" Y="-0.074259979248" />
                  <Point X="-28.38947265625" Y="-0.045519985199" />
                  <Point X="-28.390345703125" Y="-0.012158127785" />
                  <Point X="-28.394962890625" Y="0.010307911873" />
                  <Point X="-28.397287109375" Y="0.019342475891" />
                  <Point X="-28.404166015625" Y="0.041506755829" />
                  <Point X="-28.417482421875" Y="0.07083265686" />
                  <Point X="-28.44000390625" Y="0.099789558411" />
                  <Point X="-28.458939453125" Y="0.11624621582" />
                  <Point X="-28.46708984375" Y="0.122585899353" />
                  <Point X="-28.4877265625" Y="0.136908981323" />
                  <Point X="-28.501923828125" Y="0.145046920776" />
                  <Point X="-28.532875" Y="0.157848327637" />
                  <Point X="-29.6361953125" Y="0.453482116699" />
                  <Point X="-29.89181640625" Y="0.521975769043" />
                  <Point X="-29.888298828125" Y="0.545749084473" />
                  <Point X="-29.82448828125" Y="0.976966308594" />
                  <Point X="-29.786634765625" Y="1.116656616211" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-29.038376953125" Y="1.335696289062" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.744982421875" Y="1.299341796875" />
                  <Point X="-28.723421875" Y="1.301228271484" />
                  <Point X="-28.70313671875" Y="1.305263549805" />
                  <Point X="-28.68738671875" Y="1.310229614258" />
                  <Point X="-28.6417109375" Y="1.324631103516" />
                  <Point X="-28.62277734375" Y="1.332961547852" />
                  <Point X="-28.604033203125" Y="1.343783325195" />
                  <Point X="-28.5873515625" Y="1.356014770508" />
                  <Point X="-28.573712890625" Y="1.371567016602" />
                  <Point X="-28.561298828125" Y="1.389296508789" />
                  <Point X="-28.55134765625" Y="1.40743737793" />
                  <Point X="-28.545029296875" Y="1.422694580078" />
                  <Point X="-28.526701171875" Y="1.466941650391" />
                  <Point X="-28.520916015625" Y="1.486788085938" />
                  <Point X="-28.51715625" Y="1.508103637695" />
                  <Point X="-28.515802734375" Y="1.528749755859" />
                  <Point X="-28.518951171875" Y="1.54919934082" />
                  <Point X="-28.5245546875" Y="1.570106445312" />
                  <Point X="-28.532048828125" Y="1.589376342773" />
                  <Point X="-28.539673828125" Y="1.604024658203" />
                  <Point X="-28.5617890625" Y="1.646506103516" />
                  <Point X="-28.573283203125" Y="1.663707763672" />
                  <Point X="-28.5871953125" Y="1.680287231445" />
                  <Point X="-28.60213671875" Y="1.694590087891" />
                  <Point X="-29.23500390625" Y="2.180206542969" />
                  <Point X="-29.351859375" Y="2.269873046875" />
                  <Point X="-29.329095703125" Y="2.308872070312" />
                  <Point X="-29.081146484375" Y="2.733668212891" />
                  <Point X="-28.980890625" Y="2.862534423828" />
                  <Point X="-28.75050390625" Y="3.158662353516" />
                  <Point X="-28.3741484375" Y="2.941372802734" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.124857421875" Y="2.823877197266" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.999013671875" Y="2.826504638672" />
                  <Point X="-27.980462890625" Y="2.835653076172" />
                  <Point X="-27.962208984375" Y="2.847281982422" />
                  <Point X="-27.946078125" Y="2.860228027344" />
                  <Point X="-27.9305078125" Y="2.875797851562" />
                  <Point X="-27.885353515625" Y="2.920951416016" />
                  <Point X="-27.872404296875" Y="2.937084960938" />
                  <Point X="-27.860775390625" Y="2.955339111328" />
                  <Point X="-27.85162890625" Y="2.973888183594" />
                  <Point X="-27.8467109375" Y="2.993977050781" />
                  <Point X="-27.843884765625" Y="3.015435546875" />
                  <Point X="-27.84343359375" Y="3.036123535156" />
                  <Point X="-27.845353515625" Y="3.058059082031" />
                  <Point X="-27.85091796875" Y="3.121673095703" />
                  <Point X="-27.854955078125" Y="3.141961425781" />
                  <Point X="-27.86146484375" Y="3.162604248047" />
                  <Point X="-27.869794921875" Y="3.181532714844" />
                  <Point X="-28.15023828125" Y="3.6672734375" />
                  <Point X="-28.18333203125" Y="3.724595947266" />
                  <Point X="-28.1324609375" Y="3.763599609375" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.542716796875" Y="4.182414550781" />
                  <Point X="-27.167037109375" Y="4.391134277344" />
                  <Point X="-27.094470703125" Y="4.296562988281" />
                  <Point X="-27.0431953125" Y="4.229740722656" />
                  <Point X="-27.028890625" Y="4.214797851563" />
                  <Point X="-27.012310546875" Y="4.200885742188" />
                  <Point X="-26.995111328125" Y="4.18939453125" />
                  <Point X="-26.970697265625" Y="4.176685546875" />
                  <Point X="-26.89989453125" Y="4.139827636719" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.77744921875" Y="4.134482910156" />
                  <Point X="-26.752021484375" Y="4.145016113281" />
                  <Point X="-26.678275390625" Y="4.1755625" />
                  <Point X="-26.660142578125" Y="4.18551171875" />
                  <Point X="-26.6424140625" Y="4.19792578125" />
                  <Point X="-26.62686328125" Y="4.2115625" />
                  <Point X="-26.6146328125" Y="4.2282421875" />
                  <Point X="-26.603810546875" Y="4.246985839844" />
                  <Point X="-26.595478515625" Y="4.265924316406" />
                  <Point X="-26.587203125" Y="4.292174316406" />
                  <Point X="-26.56319921875" Y="4.36830078125" />
                  <Point X="-26.5591640625" Y="4.388583007812" />
                  <Point X="-26.55727734375" Y="4.41014453125" />
                  <Point X="-26.557728515625" Y="4.430826171875" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.508671875" Y="4.65307421875" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.75820703125" Y="4.832211914062" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.183453125" Y="4.471234863281" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183885742188" />
                  <Point X="-24.932869140625" Y="4.194217773438" />
                  <Point X="-24.905576171875" Y="4.208806640625" />
                  <Point X="-24.88441796875" Y="4.23139453125" />
                  <Point X="-24.86655859375" Y="4.25812109375" />
                  <Point X="-24.853783203125" Y="4.286314941406" />
                  <Point X="-24.710087890625" Y="4.822594238281" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.644083984375" Y="4.882858886719" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.997578125" Y="4.793501464844" />
                  <Point X="-23.5189765625" Y="4.677951660156" />
                  <Point X="-23.416986328125" Y="4.640959472656" />
                  <Point X="-23.105349609375" Y="4.527926757812" />
                  <Point X="-23.00566796875" Y="4.48130859375" />
                  <Point X="-22.705423828125" Y="4.34089453125" />
                  <Point X="-22.609091796875" Y="4.284770996094" />
                  <Point X="-22.31902734375" Y="4.115777832031" />
                  <Point X="-22.228193359375" Y="4.05118359375" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.631490234375" Y="2.933753173828" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857919921875" Y="2.539934326172" />
                  <Point X="-22.866919921875" Y="2.516060546875" />
                  <Point X="-22.87242578125" Y="2.495474609375" />
                  <Point X="-22.888390625" Y="2.435774169922" />
                  <Point X="-22.89159375" Y="2.409136230469" />
                  <Point X="-22.89078515625" Y="2.372433105469" />
                  <Point X="-22.890125" Y="2.363153076172" />
                  <Point X="-22.883900390625" Y="2.311529052734" />
                  <Point X="-22.87855859375" Y="2.289607421875" />
                  <Point X="-22.87029296875" Y="2.267518310547" />
                  <Point X="-22.859927734375" Y="2.247467529297" />
                  <Point X="-22.848912109375" Y="2.231234863281" />
                  <Point X="-22.81696875" Y="2.184158935547" />
                  <Point X="-22.7987421875" Y="2.164162109375" />
                  <Point X="-22.769208984375" Y="2.139847412109" />
                  <Point X="-22.762166015625" Y="2.134577392578" />
                  <Point X="-22.71508984375" Y="2.102634521484" />
                  <Point X="-22.695046875" Y="2.092272216797" />
                  <Point X="-22.672958984375" Y="2.084006347656" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.633234375" Y="2.076517089844" />
                  <Point X="-22.581611328125" Y="2.070291992188" />
                  <Point X="-22.563533203125" Y="2.069845703125" />
                  <Point X="-22.52679296875" Y="2.074171142578" />
                  <Point X="-22.50620703125" Y="2.079676269531" />
                  <Point X="-22.446505859375" Y="2.095640869141" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.301736328125" Y="2.750846679688" />
                  <Point X="-21.032671875" Y="2.90619140625" />
                  <Point X="-20.8767265625" Y="2.689463867188" />
                  <Point X="-20.826091796875" Y="2.605787597656" />
                  <Point X="-20.73780078125" Y="2.459883789063" />
                  <Point X="-21.47834375" Y="1.891643676758" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.7785703125" Y="1.660245605469" />
                  <Point X="-21.796029296875" Y="1.64162512207" />
                  <Point X="-21.81084375" Y="1.622296875" />
                  <Point X="-21.853810546875" Y="1.566243774414" />
                  <Point X="-21.86339453125" Y="1.550909423828" />
                  <Point X="-21.8783671875" Y="1.517089599609" />
                  <Point X="-21.88388671875" Y="1.49735559082" />
                  <Point X="-21.899892578125" Y="1.440125" />
                  <Point X="-21.90334765625" Y="1.417824462891" />
                  <Point X="-21.9041640625" Y="1.39425378418" />
                  <Point X="-21.902259765625" Y="1.371764526367" />
                  <Point X="-21.897728515625" Y="1.349807739258" />
                  <Point X="-21.88458984375" Y="1.286131713867" />
                  <Point X="-21.8793203125" Y="1.268979248047" />
                  <Point X="-21.863716796875" Y="1.235740600586" />
                  <Point X="-21.85139453125" Y="1.217011474609" />
                  <Point X="-21.81566015625" Y="1.162695068359" />
                  <Point X="-21.801107421875" Y="1.145450805664" />
                  <Point X="-21.78386328125" Y="1.129360717773" />
                  <Point X="-21.76565234375" Y="1.116034057617" />
                  <Point X="-21.747794921875" Y="1.105982177734" />
                  <Point X="-21.696009765625" Y="1.076831665039" />
                  <Point X="-21.6794765625" Y="1.069501098633" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.619736328125" Y="1.056247680664" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.45762890625" Y="1.185768310547" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.15405859375" Y="0.932788391113" />
                  <Point X="-20.13810546875" Y="0.830322692871" />
                  <Point X="-20.109134765625" Y="0.644238891602" />
                  <Point X="-20.950076171875" Y="0.418908813477" />
                  <Point X="-21.283419921875" Y="0.329589874268" />
                  <Point X="-21.2952109375" Y="0.325585357666" />
                  <Point X="-21.318451171875" Y="0.31506854248" />
                  <Point X="-21.342171875" Y="0.301357940674" />
                  <Point X="-21.4109609375" Y="0.261596130371" />
                  <Point X="-21.4256875" Y="0.251096954346" />
                  <Point X="-21.452466796875" Y="0.225577346802" />
                  <Point X="-21.46669921875" Y="0.207442337036" />
                  <Point X="-21.50797265625" Y="0.154849700928" />
                  <Point X="-21.51969921875" Y="0.135567306519" />
                  <Point X="-21.52947265625" Y="0.114103546143" />
                  <Point X="-21.536318359375" Y="0.092604255676" />
                  <Point X="-21.5410625" Y="0.067832977295" />
                  <Point X="-21.5548203125" Y="-0.004006072998" />
                  <Point X="-21.556515625" Y="-0.021875444412" />
                  <Point X="-21.5548203125" Y="-0.058553920746" />
                  <Point X="-21.550076171875" Y="-0.08332535553" />
                  <Point X="-21.536318359375" Y="-0.155164398193" />
                  <Point X="-21.52947265625" Y="-0.176663696289" />
                  <Point X="-21.51969921875" Y="-0.198127456665" />
                  <Point X="-21.50797265625" Y="-0.217409851074" />
                  <Point X="-21.493740234375" Y="-0.235544708252" />
                  <Point X="-21.452466796875" Y="-0.288137481689" />
                  <Point X="-21.44" Y="-0.301236541748" />
                  <Point X="-21.4109609375" Y="-0.324156280518" />
                  <Point X="-21.387240234375" Y="-0.337866760254" />
                  <Point X="-21.318451171875" Y="-0.377628540039" />
                  <Point X="-21.307291015625" Y="-0.383137786865" />
                  <Point X="-21.283419921875" Y="-0.392149871826" />
                  <Point X="-20.316701171875" Y="-0.651181518555" />
                  <Point X="-20.10852734375" Y="-0.706961791992" />
                  <Point X="-20.144978515625" Y="-0.948746704102" />
                  <Point X="-20.16541796875" Y="-1.038310668945" />
                  <Point X="-20.19882421875" Y="-1.18469921875" />
                  <Point X="-21.189033203125" Y="-1.054335571289" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710144043" />
                  <Point X="-21.62533984375" Y="-1.005508728027" />
                  <Point X="-21.67189453125" Y="-1.015627380371" />
                  <Point X="-21.806904296875" Y="-1.044972412109" />
                  <Point X="-21.836025390625" Y="-1.056597167969" />
                  <Point X="-21.8638515625" Y="-1.073489379883" />
                  <Point X="-21.8876015625" Y="-1.093960449219" />
                  <Point X="-21.915740234375" Y="-1.127802856445" />
                  <Point X="-21.997345703125" Y="-1.225948486328" />
                  <Point X="-22.012064453125" Y="-1.250329589844" />
                  <Point X="-22.023408203125" Y="-1.277715209961" />
                  <Point X="-22.030240234375" Y="-1.305365234375" />
                  <Point X="-22.0342734375" Y="-1.349192626953" />
                  <Point X="-22.04596875" Y="-1.476295776367" />
                  <Point X="-22.04365234375" Y="-1.507562011719" />
                  <Point X="-22.035921875" Y="-1.539182617188" />
                  <Point X="-22.023548828125" Y="-1.567996337891" />
                  <Point X="-21.99778515625" Y="-1.6080703125" />
                  <Point X="-21.923068359375" Y="-1.724287109375" />
                  <Point X="-21.9130625" Y="-1.737242431641" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-20.992248046875" Y="-2.449294921875" />
                  <Point X="-20.786875" Y="-2.606882568359" />
                  <Point X="-20.875193359375" Y="-2.749793457031" />
                  <Point X="-20.917466796875" Y="-2.809858642578" />
                  <Point X="-20.971017578125" Y="-2.885945556641" />
                  <Point X="-21.85473828125" Y="-2.375728271484" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.2457734375" Y="-2.159825195312" />
                  <Point X="-22.3011796875" Y="-2.149818847656" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135176757812" />
                  <Point X="-22.6011953125" Y="-2.159401611328" />
                  <Point X="-22.73468359375" Y="-2.229655761719" />
                  <Point X="-22.757615234375" Y="-2.246549072266" />
                  <Point X="-22.77857421875" Y="-2.267508300781" />
                  <Point X="-22.795466796875" Y="-2.290439208984" />
                  <Point X="-22.81969140625" Y="-2.33646875" />
                  <Point X="-22.889947265625" Y="-2.469957275391" />
                  <Point X="-22.899771484375" Y="-2.499734863281" />
                  <Point X="-22.904728515625" Y="-2.531908203125" />
                  <Point X="-22.904322265625" Y="-2.563260498047" />
                  <Point X="-22.894314453125" Y="-2.618667236328" />
                  <Point X="-22.865294921875" Y="-2.779350830078" />
                  <Point X="-22.86101171875" Y="-2.795139648438" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.2716875" Y="-3.824589599609" />
                  <Point X="-22.13871484375" Y="-4.054905761719" />
                  <Point X="-22.2181484375" Y="-4.111643554688" />
                  <Point X="-22.26541796875" Y="-4.142240234375" />
                  <Point X="-22.298232421875" Y="-4.163480957031" />
                  <Point X="-22.97871484375" Y="-3.276660400391" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900556884766" />
                  <Point X="-23.332720703125" Y="-2.865424560547" />
                  <Point X="-23.49119921875" Y="-2.763538085938" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.6426640625" Y="-2.746616455078" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-23.94155078125" Y="-2.833832275391" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968860839844" />
                  <Point X="-24.11275" Y="-2.996687255859" />
                  <Point X="-24.124375" Y="-3.025810302734" />
                  <Point X="-24.138171875" Y="-3.089293212891" />
                  <Point X="-24.178189453125" Y="-3.273398193359" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.016884765625" Y="-4.564061523438" />
                  <Point X="-23.977935546875" Y="-4.859915527344" />
                  <Point X="-24.02430859375" Y="-4.870081054688" />
                  <Point X="-24.0679921875" Y="-4.878016601562" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.05843359375" Y="-4.752635253906" />
                  <Point X="-26.127259765625" Y="-4.734926269531" />
                  <Point X="-26.141244140625" Y="-4.731328125" />
                  <Point X="-26.133138671875" Y="-4.669751953125" />
                  <Point X="-26.1207734375" Y="-4.575837890625" />
                  <Point X="-26.120076171875" Y="-4.568098632812" />
                  <Point X="-26.11944921875" Y="-4.541030761719" />
                  <Point X="-26.1217578125" Y="-4.509324707031" />
                  <Point X="-26.123333984375" Y="-4.497689941406" />
                  <Point X="-26.138853515625" Y="-4.41966796875" />
                  <Point X="-26.18386328125" Y="-4.1933984375" />
                  <Point X="-26.188125" Y="-4.178469238281" />
                  <Point X="-26.19902734375" Y="-4.149501464844" />
                  <Point X="-26.20566796875" Y="-4.135462890625" />
                  <Point X="-26.221740234375" Y="-4.107625976562" />
                  <Point X="-26.230576171875" Y="-4.094860351562" />
                  <Point X="-26.250208984375" Y="-4.0709375" />
                  <Point X="-26.261005859375" Y="-4.059780273438" />
                  <Point X="-26.320814453125" Y="-4.007328613281" />
                  <Point X="-26.494265625" Y="-3.855215332031" />
                  <Point X="-26.506736328125" Y="-3.845965820312" />
                  <Point X="-26.53301953125" Y="-3.829621582031" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.7161953125" Y="-3.790966796875" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166015625" />
                  <Point X="-27.008005859375" Y="-3.781945556641" />
                  <Point X="-27.0390546875" Y="-3.790265136719" />
                  <Point X="-27.05367578125" Y="-3.795496582031" />
                  <Point X="-27.0818671875" Y="-3.808270507812" />
                  <Point X="-27.0954375" Y="-3.815812988281" />
                  <Point X="-27.161580078125" Y="-3.860009033203" />
                  <Point X="-27.35340234375" Y="-3.988181152344" />
                  <Point X="-27.359681640625" Y="-3.992758789062" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032770507812" />
                  <Point X="-27.410470703125" Y="-4.041629394531" />
                  <Point X="-27.503203125" Y="-4.162479003906" />
                  <Point X="-27.7475859375" Y="-4.011161865234" />
                  <Point X="-27.84288671875" Y="-3.937784423828" />
                  <Point X="-27.98086328125" Y="-3.831547119141" />
                  <Point X="-27.5323671875" Y="-3.054728759766" />
                  <Point X="-27.341490234375" Y="-2.724119628906" />
                  <Point X="-27.3348515625" Y="-2.710083740234" />
                  <Point X="-27.32394921875" Y="-2.6811171875" />
                  <Point X="-27.319685546875" Y="-2.666186523438" />
                  <Point X="-27.3134140625" Y="-2.634657470703" />
                  <Point X="-27.311638671875" Y="-2.619235107422" />
                  <Point X="-27.310625" Y="-2.588297363281" />
                  <Point X="-27.31466796875" Y="-2.557601074219" />
                  <Point X="-27.32365625" Y="-2.527980224609" />
                  <Point X="-27.32936328125" Y="-2.513540283203" />
                  <Point X="-27.343583984375" Y="-2.484710205078" />
                  <Point X="-27.351564453125" Y="-2.471396240234" />
                  <Point X="-27.369587890625" Y="-2.446247070312" />
                  <Point X="-27.379630859375" Y="-2.434411865234" />
                  <Point X="-27.39697265625" Y="-2.417069580078" />
                  <Point X="-27.408810546875" Y="-2.407024902344" />
                  <Point X="-27.43396484375" Y="-2.388998535156" />
                  <Point X="-27.44728125" Y="-2.381016845703" />
                  <Point X="-27.476111328125" Y="-2.366797607422" />
                  <Point X="-27.49055078125" Y="-2.361090576172" />
                  <Point X="-27.52016796875" Y="-2.352104492188" />
                  <Point X="-27.55085546875" Y="-2.348062988281" />
                  <Point X="-27.58179296875" Y="-2.349074951172" />
                  <Point X="-27.597220703125" Y="-2.350849365234" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.64547265625" Y="-2.932482666016" />
                  <Point X="-28.793087890625" Y="-3.017708496094" />
                  <Point X="-29.004015625" Y="-2.740594482422" />
                  <Point X="-29.0723359375" Y="-2.626030517578" />
                  <Point X="-29.181265625" Y="-2.443373046875" />
                  <Point X="-28.382095703125" Y="-1.83014855957" />
                  <Point X="-28.048123046875" Y="-1.573881835938" />
                  <Point X="-28.039162109375" Y="-1.566067749023" />
                  <Point X="-28.016275390625" Y="-1.543438964844" />
                  <Point X="-28.002486328125" Y="-1.526198852539" />
                  <Point X="-27.983236328125" Y="-1.495368652344" />
                  <Point X="-27.975099609375" Y="-1.479024169922" />
                  <Point X="-27.966408203125" Y="-1.456324951172" />
                  <Point X="-27.959912109375" Y="-1.436018798828" />
                  <Point X="-27.95418359375" Y="-1.413898803711" />
                  <Point X="-27.951951171875" Y="-1.402388916016" />
                  <Point X="-27.947837890625" Y="-1.370905151367" />
                  <Point X="-27.94708203125" Y="-1.355686523438" />
                  <Point X="-27.947783203125" Y="-1.332820922852" />
                  <Point X="-27.951228515625" Y="-1.310215454102" />
                  <Point X="-27.957373046875" Y="-1.288180297852" />
                  <Point X="-27.96111328125" Y="-1.27734387207" />
                  <Point X="-27.9731171875" Y="-1.248362426758" />
                  <Point X="-27.97876171875" Y="-1.236961547852" />
                  <Point X="-27.9915234375" Y="-1.215014648438" />
                  <Point X="-28.00710546875" Y="-1.194968505859" />
                  <Point X="-28.025224609375" Y="-1.177184936523" />
                  <Point X="-28.03487890625" Y="-1.16890222168" />
                  <Point X="-28.054171875" Y="-1.1544765625" />
                  <Point X="-28.07157421875" Y="-1.142900878906" />
                  <Point X="-28.091265625" Y="-1.131310791016" />
                  <Point X="-28.1054296875" Y="-1.124483886719" />
                  <Point X="-28.13469140625" Y="-1.113259277344" />
                  <Point X="-28.1497890625" Y="-1.108861572266" />
                  <Point X="-28.18167578125" Y="-1.102379272461" />
                  <Point X="-28.197294921875" Y="-1.100532714844" />
                  <Point X="-28.22862109375" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-29.454708984375" Y="-1.259546386719" />
                  <Point X="-29.66091796875" Y="-1.286694335938" />
                  <Point X="-29.740763671875" Y="-0.974111328125" />
                  <Point X="-29.7588359375" Y="-0.847737487793" />
                  <Point X="-29.786451171875" Y="-0.654654541016" />
                  <Point X="-28.888732421875" Y="-0.414111328125" />
                  <Point X="-28.508287109375" Y="-0.312171203613" />
                  <Point X="-28.497908203125" Y="-0.308739349365" />
                  <Point X="-28.4776171875" Y="-0.30070703125" />
                  <Point X="-28.467705078125" Y="-0.296106872559" />
                  <Point X="-28.44501953125" Y="-0.283947601318" />
                  <Point X="-28.42644140625" Y="-0.272574371338" />
                  <Point X="-28.4058046875" Y="-0.258251281738" />
                  <Point X="-28.39647265625" Y="-0.250866226196" />
                  <Point X="-28.372515625" Y="-0.229336425781" />
                  <Point X="-28.35790234375" Y="-0.212744766235" />
                  <Point X="-28.337177734375" Y="-0.18280305481" />
                  <Point X="-28.32826171875" Y="-0.166828186035" />
                  <Point X="-28.318466796875" Y="-0.144450180054" />
                  <Point X="-28.311064453125" Y="-0.124583473206" />
                  <Point X="-28.304185546875" Y="-0.102419197083" />
                  <Point X="-28.301576171875" Y="-0.091938766479" />
                  <Point X="-28.2961328125" Y="-0.063198673248" />
                  <Point X="-28.294505859375" Y="-0.043034713745" />
                  <Point X="-28.29537890625" Y="-0.009672898293" />
                  <Point X="-28.297291015625" Y="0.006966469288" />
                  <Point X="-28.301908203125" Y="0.029432447433" />
                  <Point X="-28.306556640625" Y="0.047501716614" />
                  <Point X="-28.313435546875" Y="0.069665985107" />
                  <Point X="-28.317666015625" Y="0.080784912109" />
                  <Point X="-28.330982421875" Y="0.110110870361" />
                  <Point X="-28.342494140625" Y="0.129156143188" />
                  <Point X="-28.365015625" Y="0.158113082886" />
                  <Point X="-28.377685546875" Y="0.171494033813" />
                  <Point X="-28.39662109375" Y="0.187950744629" />
                  <Point X="-28.412921875" Y="0.200630203247" />
                  <Point X="-28.43355859375" Y="0.214953292847" />
                  <Point X="-28.440482421875" Y="0.219328887939" />
                  <Point X="-28.465615234375" Y="0.23283454895" />
                  <Point X="-28.49656640625" Y="0.245635879517" />
                  <Point X="-28.508287109375" Y="0.249611236572" />
                  <Point X="-29.611607421875" Y="0.545245117188" />
                  <Point X="-29.785447265625" Y="0.591825195312" />
                  <Point X="-29.73133203125" Y="0.957517089844" />
                  <Point X="-29.69494140625" Y="1.091809448242" />
                  <Point X="-29.6335859375" Y="1.318236938477" />
                  <Point X="-29.05077734375" Y="1.241509155273" />
                  <Point X="-28.778064453125" Y="1.20560559082" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.7470546875" Y="1.204364379883" />
                  <Point X="-28.736701171875" Y="1.204703369141" />
                  <Point X="-28.715140625" Y="1.20658984375" />
                  <Point X="-28.70488671875" Y="1.208053955078" />
                  <Point X="-28.6846015625" Y="1.212089233398" />
                  <Point X="-28.658818359375" Y="1.219626708984" />
                  <Point X="-28.613142578125" Y="1.234028198242" />
                  <Point X="-28.603451171875" Y="1.23767565918" />
                  <Point X="-28.584517578125" Y="1.246006103516" />
                  <Point X="-28.57527734375" Y="1.250688842773" />
                  <Point X="-28.556533203125" Y="1.261510620117" />
                  <Point X="-28.547859375" Y="1.267171020508" />
                  <Point X="-28.531177734375" Y="1.27940246582" />
                  <Point X="-28.51592578125" Y="1.293377685547" />
                  <Point X="-28.502287109375" Y="1.308929931641" />
                  <Point X="-28.495892578125" Y="1.31707800293" />
                  <Point X="-28.483478515625" Y="1.334807495117" />
                  <Point X="-28.4780078125" Y="1.343606933594" />
                  <Point X="-28.468056640625" Y="1.361747802734" />
                  <Point X="-28.4572578125" Y="1.386346313477" />
                  <Point X="-28.4389296875" Y="1.430593505859" />
                  <Point X="-28.43549609375" Y="1.440355957031" />
                  <Point X="-28.4297109375" Y="1.460202392578" />
                  <Point X="-28.427359375" Y="1.470286132812" />
                  <Point X="-28.423599609375" Y="1.49160168457" />
                  <Point X="-28.422359375" Y="1.501889038086" />
                  <Point X="-28.421005859375" Y="1.52253515625" />
                  <Point X="-28.421908203125" Y="1.543205688477" />
                  <Point X="-28.425056640625" Y="1.563655151367" />
                  <Point X="-28.427189453125" Y="1.573793212891" />
                  <Point X="-28.43279296875" Y="1.594700317383" />
                  <Point X="-28.436015625" Y="1.604540039062" />
                  <Point X="-28.443509765625" Y="1.623810058594" />
                  <Point X="-28.45540625" Y="1.647888793945" />
                  <Point X="-28.477521484375" Y="1.690370239258" />
                  <Point X="-28.48280078125" Y="1.699286376953" />
                  <Point X="-28.494294921875" Y="1.716488037109" />
                  <Point X="-28.500509765625" Y="1.72477331543" />
                  <Point X="-28.514421875" Y="1.741352783203" />
                  <Point X="-28.521501953125" Y="1.748912841797" />
                  <Point X="-28.536443359375" Y="1.763215698242" />
                  <Point X="-28.5443046875" Y="1.769958618164" />
                  <Point X="-29.177171875" Y="2.255575195312" />
                  <Point X="-29.22761328125" Y="2.294280029297" />
                  <Point X="-29.00228125" Y="2.680328125" />
                  <Point X="-28.90591015625" Y="2.804200439453" />
                  <Point X="-28.726337890625" Y="3.035013427734" />
                  <Point X="-28.4216484375" Y="2.859100341797" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.13313671875" Y="2.729238769531" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310791016" />
                  <Point X="-27.97643359375" Y="2.734227294922" />
                  <Point X="-27.95699609375" Y="2.741302001953" />
                  <Point X="-27.9384453125" Y="2.750450439453" />
                  <Point X="-27.929419921875" Y="2.755530761719" />
                  <Point X="-27.911166015625" Y="2.767159667969" />
                  <Point X="-27.902748046875" Y="2.773192138672" />
                  <Point X="-27.8866171875" Y="2.786138183594" />
                  <Point X="-27.878904296875" Y="2.793051757812" />
                  <Point X="-27.863333984375" Y="2.808621582031" />
                  <Point X="-27.8181796875" Y="2.853775146484" />
                  <Point X="-27.811265625" Y="2.861486816406" />
                  <Point X="-27.79831640625" Y="2.877620361328" />
                  <Point X="-27.79228125" Y="2.886042236328" />
                  <Point X="-27.78065234375" Y="2.904296386719" />
                  <Point X="-27.7755703125" Y="2.913324951172" />
                  <Point X="-27.766423828125" Y="2.931874023438" />
                  <Point X="-27.759353515625" Y="2.951298339844" />
                  <Point X="-27.754435546875" Y="2.971387207031" />
                  <Point X="-27.7525234375" Y="2.981572265625" />
                  <Point X="-27.749697265625" Y="3.003030761719" />
                  <Point X="-27.748908203125" Y="3.013364257812" />
                  <Point X="-27.74845703125" Y="3.034052246094" />
                  <Point X="-27.748794921875" Y="3.044406738281" />
                  <Point X="-27.75071484375" Y="3.066342285156" />
                  <Point X="-27.756279296875" Y="3.129956298828" />
                  <Point X="-27.757744140625" Y="3.140213378906" />
                  <Point X="-27.76178125" Y="3.160501708984" />
                  <Point X="-27.764353515625" Y="3.170532958984" />
                  <Point X="-27.77086328125" Y="3.19117578125" />
                  <Point X="-27.77451171875" Y="3.200870361328" />
                  <Point X="-27.782841796875" Y="3.219798828125" />
                  <Point X="-27.7875234375" Y="3.229032714844" />
                  <Point X="-28.059388671875" Y="3.699915771484" />
                  <Point X="-27.648369140625" Y="4.015039306641" />
                  <Point X="-27.496580078125" Y="4.099370605469" />
                  <Point X="-27.192525390625" Y="4.268296386719" />
                  <Point X="-27.16983984375" Y="4.238730957031" />
                  <Point X="-27.118564453125" Y="4.171908691406" />
                  <Point X="-27.1118203125" Y="4.164046875" />
                  <Point X="-27.097515625" Y="4.149104003906" />
                  <Point X="-27.089955078125" Y="4.142022949219" />
                  <Point X="-27.073375" Y="4.128110839844" />
                  <Point X="-27.0650859375" Y="4.121894042969" />
                  <Point X="-27.04788671875" Y="4.110402832031" />
                  <Point X="-27.0389765625" Y="4.105128417969" />
                  <Point X="-27.0145625" Y="4.092419189453" />
                  <Point X="-26.943759765625" Y="4.055561279297" />
                  <Point X="-26.93432421875" Y="4.051286621094" />
                  <Point X="-26.915044921875" Y="4.043789550781" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031378173828" />
                  <Point X="-26.780818359375" Y="4.035136474609" />
                  <Point X="-26.770728515625" Y="4.037488525391" />
                  <Point X="-26.750865234375" Y="4.043278076172" />
                  <Point X="-26.741091796875" Y="4.046715087891" />
                  <Point X="-26.7156640625" Y="4.057248291016" />
                  <Point X="-26.64191796875" Y="4.087794677734" />
                  <Point X="-26.632578125" Y="4.092275878906" />
                  <Point X="-26.6144453125" Y="4.102225097656" />
                  <Point X="-26.60565234375" Y="4.107693359375" />
                  <Point X="-26.587923828125" Y="4.120107421875" />
                  <Point X="-26.579779296875" Y="4.126499023438" />
                  <Point X="-26.564228515625" Y="4.140135742188" />
                  <Point X="-26.550251953125" Y="4.15538671875" />
                  <Point X="-26.538021484375" Y="4.17206640625" />
                  <Point X="-26.532361328125" Y="4.180740234375" />
                  <Point X="-26.5215390625" Y="4.199483886719" />
                  <Point X="-26.516853515625" Y="4.208729003906" />
                  <Point X="-26.508521484375" Y="4.227667480469" />
                  <Point X="-26.504875" Y="4.237360839844" />
                  <Point X="-26.496599609375" Y="4.263610839844" />
                  <Point X="-26.472595703125" Y="4.339737304688" />
                  <Point X="-26.470025390625" Y="4.349763671875" />
                  <Point X="-26.465990234375" Y="4.370045898437" />
                  <Point X="-26.464525390625" Y="4.380301757812" />
                  <Point X="-26.462638671875" Y="4.40186328125" />
                  <Point X="-26.46230078125" Y="4.412216308594" />
                  <Point X="-26.462751953125" Y="4.432897949219" />
                  <Point X="-26.463541015625" Y="4.4432265625" />
                  <Point X="-26.479265625" Y="4.562655761719" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.7471640625" Y="4.737855957031" />
                  <Point X="-25.36522265625" Y="4.782556152344" />
                  <Point X="-25.275216796875" Y="4.446646972656" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.978369140625" Y="4.085113037109" />
                  <Point X="-24.94783203125" Y="4.090154541016" />
                  <Point X="-24.93276953125" Y="4.093927246094" />
                  <Point X="-24.90233203125" Y="4.104259277344" />
                  <Point X="-24.8880859375" Y="4.110436035156" />
                  <Point X="-24.86079296875" Y="4.125024902344" />
                  <Point X="-24.8362421875" Y="4.143861328125" />
                  <Point X="-24.815083984375" Y="4.16644921875" />
                  <Point X="-24.8054296875" Y="4.178612792969" />
                  <Point X="-24.7875703125" Y="4.205339355469" />
                  <Point X="-24.78002734375" Y="4.218911621094" />
                  <Point X="-24.767251953125" Y="4.24710546875" />
                  <Point X="-24.76201953125" Y="4.261727050781" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-24.019873046875" Y="4.701154785156" />
                  <Point X="-23.546408203125" Y="4.586844726562" />
                  <Point X="-23.44937890625" Y="4.55165234375" />
                  <Point X="-23.141734375" Y="4.440067871094" />
                  <Point X="-23.045912109375" Y="4.395254394531" />
                  <Point X="-22.749546875" Y="4.256654296875" />
                  <Point X="-22.6569140625" Y="4.202686035156" />
                  <Point X="-22.370572265625" Y="4.035861328125" />
                  <Point X="-22.283248046875" Y="3.973763427734" />
                  <Point X="-22.182216796875" Y="3.901915771484" />
                  <Point X="-22.71376171875" Y="2.981253173828" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.937623046875" Y="2.593106933594" />
                  <Point X="-22.9468125" Y="2.573445556641" />
                  <Point X="-22.9558125" Y="2.549571777344" />
                  <Point X="-22.958693359375" Y="2.540606201172" />
                  <Point X="-22.96419921875" Y="2.520020263672" />
                  <Point X="-22.9801640625" Y="2.460319824219" />
                  <Point X="-22.9827109375" Y="2.447115966797" />
                  <Point X="-22.9859140625" Y="2.420478027344" />
                  <Point X="-22.9865703125" Y="2.407043945312" />
                  <Point X="-22.98576171875" Y="2.370340820313" />
                  <Point X="-22.98444140625" Y="2.351780761719" />
                  <Point X="-22.978216796875" Y="2.300156738281" />
                  <Point X="-22.97619921875" Y="2.289037841797" />
                  <Point X="-22.970857421875" Y="2.267116210938" />
                  <Point X="-22.967533203125" Y="2.256313476562" />
                  <Point X="-22.959267578125" Y="2.234224365234" />
                  <Point X="-22.95468359375" Y="2.223892578125" />
                  <Point X="-22.944318359375" Y="2.203841796875" />
                  <Point X="-22.927521484375" Y="2.177890136719" />
                  <Point X="-22.895578125" Y="2.130814208984" />
                  <Point X="-22.8871796875" Y="2.120163574219" />
                  <Point X="-22.868953125" Y="2.100166748047" />
                  <Point X="-22.859125" Y="2.090820556641" />
                  <Point X="-22.829591796875" Y="2.066505859375" />
                  <Point X="-22.815505859375" Y="2.055965820313" />
                  <Point X="-22.7684296875" Y="2.024023071289" />
                  <Point X="-22.75871875" Y="2.018245605469" />
                  <Point X="-22.73867578125" Y="2.007883422852" />
                  <Point X="-22.72834375" Y="2.003298339844" />
                  <Point X="-22.706255859375" Y="1.995032470703" />
                  <Point X="-22.695451171875" Y="1.991707519531" />
                  <Point X="-22.67352734375" Y="1.986364746094" />
                  <Point X="-22.644607421875" Y="1.982200317383" />
                  <Point X="-22.592984375" Y="1.975975219727" />
                  <Point X="-22.583955078125" Y="1.975320922852" />
                  <Point X="-22.55242578125" Y="1.975497314453" />
                  <Point X="-22.515685546875" Y="1.979822753906" />
                  <Point X="-22.50225" Y="1.982395996094" />
                  <Point X="-22.4816640625" Y="1.987901245117" />
                  <Point X="-22.421962890625" Y="2.003865844727" />
                  <Point X="-22.416" Y="2.005671875" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572875977" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.254236328125" Y="2.66857421875" />
                  <Point X="-21.05959375" Y="2.780951416016" />
                  <Point X="-20.956041015625" Y="2.637037597656" />
                  <Point X="-20.907369140625" Y="2.556604492188" />
                  <Point X="-20.86311328125" Y="2.483471679688" />
                  <Point X="-21.53617578125" Y="1.967012207031" />
                  <Point X="-21.827046875" Y="1.743819946289" />
                  <Point X="-21.831857421875" Y="1.739871337891" />
                  <Point X="-21.847873046875" Y="1.725224487305" />
                  <Point X="-21.86533203125" Y="1.706604125977" />
                  <Point X="-21.8714296875" Y="1.699416625977" />
                  <Point X="-21.886244140625" Y="1.680088378906" />
                  <Point X="-21.9292109375" Y="1.62403527832" />
                  <Point X="-21.93437109375" Y="1.61659375" />
                  <Point X="-21.95026171875" Y="1.58936730957" />
                  <Point X="-21.965234375" Y="1.555547485352" />
                  <Point X="-21.96985546875" Y="1.542678710938" />
                  <Point X="-21.975375" Y="1.522944702148" />
                  <Point X="-21.991380859375" Y="1.465714111328" />
                  <Point X="-21.9937734375" Y="1.454670043945" />
                  <Point X="-21.997228515625" Y="1.432369506836" />
                  <Point X="-21.998291015625" Y="1.421112915039" />
                  <Point X="-21.999107421875" Y="1.397542236328" />
                  <Point X="-21.998826171875" Y="1.38623828125" />
                  <Point X="-21.996921875" Y="1.363749023438" />
                  <Point X="-21.995298828125" Y="1.352563842773" />
                  <Point X="-21.990767578125" Y="1.330607055664" />
                  <Point X="-21.97762890625" Y="1.266931030273" />
                  <Point X="-21.975400390625" Y="1.258233032227" />
                  <Point X="-21.96531640625" Y="1.22860949707" />
                  <Point X="-21.949712890625" Y="1.19537097168" />
                  <Point X="-21.943080078125" Y="1.183525634766" />
                  <Point X="-21.9307578125" Y="1.164796508789" />
                  <Point X="-21.8950234375" Y="1.110480102539" />
                  <Point X="-21.88826171875" Y="1.101425292969" />
                  <Point X="-21.873708984375" Y="1.084181030273" />
                  <Point X="-21.86591796875" Y="1.075991577148" />
                  <Point X="-21.848673828125" Y="1.059901489258" />
                  <Point X="-21.839966796875" Y="1.052696166992" />
                  <Point X="-21.821755859375" Y="1.039369384766" />
                  <Point X="-21.812251953125" Y="1.033248413086" />
                  <Point X="-21.79439453125" Y="1.023196533203" />
                  <Point X="-21.742609375" Y="0.994045959473" />
                  <Point X="-21.734515625" Y="0.989985473633" />
                  <Point X="-21.705318359375" Y="0.978083557129" />
                  <Point X="-21.66972265625" Y="0.968021240234" />
                  <Point X="-21.656328125" Y="0.965257751465" />
                  <Point X="-21.63218359375" Y="0.962066650391" />
                  <Point X="-21.562166015625" Y="0.952812927246" />
                  <Point X="-21.555966796875" Y="0.952199707031" />
                  <Point X="-21.53428125" Y="0.951222839355" />
                  <Point X="-21.50878125" Y="0.952032348633" />
                  <Point X="-21.49939453125" Y="0.952797180176" />
                  <Point X="-20.445228515625" Y="1.091581054688" />
                  <Point X="-20.295296875" Y="1.111319824219" />
                  <Point X="-20.247310546875" Y="0.914207519531" />
                  <Point X="-20.231974609375" Y="0.815708068848" />
                  <Point X="-20.216126953125" Y="0.713921386719" />
                  <Point X="-20.9746640625" Y="0.510671722412" />
                  <Point X="-21.3080078125" Y="0.421352813721" />
                  <Point X="-21.313970703125" Y="0.419543640137" />
                  <Point X="-21.334376953125" Y="0.41213583374" />
                  <Point X="-21.3576171875" Y="0.401619110107" />
                  <Point X="-21.3659921875" Y="0.397317687988" />
                  <Point X="-21.389712890625" Y="0.383607208252" />
                  <Point X="-21.458501953125" Y="0.343845397949" />
                  <Point X="-21.466109375" Y="0.338949920654" />
                  <Point X="-21.4912265625" Y="0.319870178223" />
                  <Point X="-21.518005859375" Y="0.294350585938" />
                  <Point X="-21.52719921875" Y="0.28422833252" />
                  <Point X="-21.541431640625" Y="0.266093231201" />
                  <Point X="-21.582705078125" Y="0.213500656128" />
                  <Point X="-21.589140625" Y="0.204212310791" />
                  <Point X="-21.6008671875" Y="0.184929840088" />
                  <Point X="-21.606158203125" Y="0.17493598938" />
                  <Point X="-21.615931640625" Y="0.153472183228" />
                  <Point X="-21.619994140625" Y="0.142927093506" />
                  <Point X="-21.62683984375" Y="0.121427757263" />
                  <Point X="-21.629623046875" Y="0.110473655701" />
                  <Point X="-21.6343671875" Y="0.085702392578" />
                  <Point X="-21.648125" Y="0.013863312721" />
                  <Point X="-21.649396484375" Y="0.004966600418" />
                  <Point X="-21.6514140625" Y="-0.026261734009" />
                  <Point X="-21.64971875" Y="-0.062940216064" />
                  <Point X="-21.648125" Y="-0.076423278809" />
                  <Point X="-21.643380859375" Y="-0.101194694519" />
                  <Point X="-21.629623046875" Y="-0.173033782959" />
                  <Point X="-21.62683984375" Y="-0.183987869263" />
                  <Point X="-21.619994140625" Y="-0.205487213135" />
                  <Point X="-21.615931640625" Y="-0.216032302856" />
                  <Point X="-21.606158203125" Y="-0.237496109009" />
                  <Point X="-21.6008671875" Y="-0.247489959717" />
                  <Point X="-21.589140625" Y="-0.26677243042" />
                  <Point X="-21.582705078125" Y="-0.276061065674" />
                  <Point X="-21.56847265625" Y="-0.294196014404" />
                  <Point X="-21.52719921875" Y="-0.346788757324" />
                  <Point X="-21.52128125" Y="-0.353631347656" />
                  <Point X="-21.498857421875" Y="-0.375807647705" />
                  <Point X="-21.469818359375" Y="-0.398727508545" />
                  <Point X="-21.4585" Y="-0.406405670166" />
                  <Point X="-21.434779296875" Y="-0.420116119385" />
                  <Point X="-21.365990234375" Y="-0.459877929688" />
                  <Point X="-21.36050390625" Y="-0.462814300537" />
                  <Point X="-21.340845703125" Y="-0.472014923096" />
                  <Point X="-21.316974609375" Y="-0.481026977539" />
                  <Point X="-21.3080078125" Y="-0.483912780762" />
                  <Point X="-20.3412890625" Y="-0.742944396973" />
                  <Point X="-20.21512109375" Y="-0.776751220703" />
                  <Point X="-20.238384765625" Y="-0.931056762695" />
                  <Point X="-20.258037109375" Y="-1.017174072266" />
                  <Point X="-20.2721953125" Y="-1.079219848633" />
                  <Point X="-21.1766328125" Y="-0.960148376465" />
                  <Point X="-21.563216796875" Y="-0.909253662109" />
                  <Point X="-21.571375" Y="-0.908535705566" />
                  <Point X="-21.59990234375" Y="-0.908042419434" />
                  <Point X="-21.63327734375" Y="-0.910840942383" />
                  <Point X="-21.645517578125" Y="-0.912676147461" />
                  <Point X="-21.692072265625" Y="-0.92279486084" />
                  <Point X="-21.82708203125" Y="-0.952139831543" />
                  <Point X="-21.842125" Y="-0.956742370605" />
                  <Point X="-21.87124609375" Y="-0.968367248535" />
                  <Point X="-21.88532421875" Y="-0.975389526367" />
                  <Point X="-21.913150390625" Y="-0.992281677246" />
                  <Point X="-21.925875" Y="-1.001530761719" />
                  <Point X="-21.949625" Y="-1.022001953125" />
                  <Point X="-21.960650390625" Y="-1.033223754883" />
                  <Point X="-21.9887890625" Y="-1.067066040039" />
                  <Point X="-22.07039453125" Y="-1.165211669922" />
                  <Point X="-22.078673828125" Y="-1.176850585938" />
                  <Point X="-22.093392578125" Y="-1.201231689453" />
                  <Point X="-22.09983203125" Y="-1.213973999023" />
                  <Point X="-22.11117578125" Y="-1.241359619141" />
                  <Point X="-22.115634765625" Y="-1.254927124023" />
                  <Point X="-22.122466796875" Y="-1.282577026367" />
                  <Point X="-22.12483984375" Y="-1.296659667969" />
                  <Point X="-22.128873046875" Y="-1.340487060547" />
                  <Point X="-22.140568359375" Y="-1.467590209961" />
                  <Point X="-22.140708984375" Y="-1.483314697266" />
                  <Point X="-22.138392578125" Y="-1.514580932617" />
                  <Point X="-22.135935546875" Y="-1.530122802734" />
                  <Point X="-22.128205078125" Y="-1.561743530273" />
                  <Point X="-22.12321484375" Y="-1.576667114258" />
                  <Point X="-22.110841796875" Y="-1.605480834961" />
                  <Point X="-22.103458984375" Y="-1.619370849609" />
                  <Point X="-22.0776953125" Y="-1.659444824219" />
                  <Point X="-22.002978515625" Y="-1.775661743164" />
                  <Point X="-21.99825390625" Y="-1.782356201172" />
                  <Point X="-21.98019921875" Y="-1.804455322266" />
                  <Point X="-21.956505859375" Y="-1.828122070312" />
                  <Point X="-21.947201171875" Y="-1.836277709961" />
                  <Point X="-21.050080078125" Y="-2.524663574219" />
                  <Point X="-20.912826171875" Y="-2.629981445312" />
                  <Point X="-20.954513671875" Y="-2.697437744141" />
                  <Point X="-20.995154296875" Y="-2.755182128906" />
                  <Point X="-20.99872265625" Y="-2.760252929688" />
                  <Point X="-21.80723828125" Y="-2.293455810547" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.184970703125" Y="-2.079513916016" />
                  <Point X="-22.216876953125" Y="-2.069326416016" />
                  <Point X="-22.228888671875" Y="-2.066337646484" />
                  <Point X="-22.284294921875" Y="-2.056331298828" />
                  <Point X="-22.444978515625" Y="-2.027312011719" />
                  <Point X="-22.460638671875" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.55515625" Y="-2.035136352539" />
                  <Point X="-22.584931640625" Y="-2.044960083008" />
                  <Point X="-22.59941015625" Y="-2.051108642578" />
                  <Point X="-22.645439453125" Y="-2.075333496094" />
                  <Point X="-22.778927734375" Y="-2.145587646484" />
                  <Point X="-22.791029296875" Y="-2.153169677734" />
                  <Point X="-22.8139609375" Y="-2.170062988281" />
                  <Point X="-22.824791015625" Y="-2.179374267578" />
                  <Point X="-22.84575" Y="-2.200333496094" />
                  <Point X="-22.855060546875" Y="-2.211162841797" />
                  <Point X="-22.871953125" Y="-2.23409375" />
                  <Point X="-22.87953515625" Y="-2.2461953125" />
                  <Point X="-22.903759765625" Y="-2.292224853516" />
                  <Point X="-22.974015625" Y="-2.425713378906" />
                  <Point X="-22.9801640625" Y="-2.440192871094" />
                  <Point X="-22.98998828125" Y="-2.469970458984" />
                  <Point X="-22.9936640625" Y="-2.485268554688" />
                  <Point X="-22.99862109375" Y="-2.517441894531" />
                  <Point X="-22.999720703125" Y="-2.533139160156" />
                  <Point X="-22.999314453125" Y="-2.564491455078" />
                  <Point X="-22.99780859375" Y="-2.580146484375" />
                  <Point X="-22.98780078125" Y="-2.635553222656" />
                  <Point X="-22.95878125" Y="-2.796236816406" />
                  <Point X="-22.95698046875" Y="-2.804223632812" />
                  <Point X="-22.94876171875" Y="-2.831539794922" />
                  <Point X="-22.935927734375" Y="-2.862478759766" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.353958984375" Y="-3.872089599609" />
                  <Point X="-22.264103515625" Y="-4.027723632812" />
                  <Point X="-22.2762421875" Y="-4.036082763672" />
                  <Point X="-22.903345703125" Y="-3.218828125" />
                  <Point X="-23.16608203125" Y="-2.876422851562" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849626220703" />
                  <Point X="-23.21674609375" Y="-2.828004882812" />
                  <Point X="-23.22669921875" Y="-2.820646484375" />
                  <Point X="-23.281345703125" Y="-2.785514160156" />
                  <Point X="-23.43982421875" Y="-2.683627685547" />
                  <Point X="-23.453716796875" Y="-2.676244628906" />
                  <Point X="-23.482529296875" Y="-2.663873291016" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.651369140625" Y="-2.652016113281" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920125" Y="-2.699414794922" />
                  <Point X="-23.944505859375" Y="-2.714135742188" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-24.002287109375" Y="-2.760784423828" />
                  <Point X="-24.136123046875" Y="-2.872063720703" />
                  <Point X="-24.147345703125" Y="-2.883087158203" />
                  <Point X="-24.16781640625" Y="-2.906836425781" />
                  <Point X="-24.177064453125" Y="-2.919562255859" />
                  <Point X="-24.19395703125" Y="-2.947388671875" />
                  <Point X="-24.20098046875" Y="-2.961468505859" />
                  <Point X="-24.21260546875" Y="-2.990591552734" />
                  <Point X="-24.21720703125" Y="-3.005634765625" />
                  <Point X="-24.23100390625" Y="-3.069117675781" />
                  <Point X="-24.271021484375" Y="-3.25322265625" />
                  <Point X="-24.2724140625" Y="-3.261287597656" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520019531" />
                  <Point X="-24.166912109375" Y="-4.152318847656" />
                  <Point X="-24.242220703125" Y="-3.871260498047" />
                  <Point X="-24.344931640625" Y="-3.487937011719" />
                  <Point X="-24.347392578125" Y="-3.480121337891" />
                  <Point X="-24.357853515625" Y="-3.453579345703" />
                  <Point X="-24.3732109375" Y="-3.423815917969" />
                  <Point X="-24.37958984375" Y="-3.413210449219" />
                  <Point X="-24.4215703125" Y="-3.352724121094" />
                  <Point X="-24.543318359375" Y="-3.177310058594" />
                  <Point X="-24.553328125" Y="-3.165173339844" />
                  <Point X="-24.575212890625" Y="-3.142716796875" />
                  <Point X="-24.587087890625" Y="-3.132396972656" />
                  <Point X="-24.61334375" Y="-3.113153320312" />
                  <Point X="-24.6267578125" Y="-3.104936523438" />
                  <Point X="-24.6547578125" Y="-3.090829101562" />
                  <Point X="-24.66934375" Y="-3.084938476562" />
                  <Point X="-24.734306640625" Y="-3.064776367188" />
                  <Point X="-24.922703125" Y="-3.006305175781" />
                  <Point X="-24.936623046875" Y="-3.003109130859" />
                  <Point X="-24.964783203125" Y="-2.99883984375" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.998840087891" />
                  <Point X="-25.0510625" Y="-3.003109375" />
                  <Point X="-25.06498046875" Y="-3.006305175781" />
                  <Point X="-25.129943359375" Y="-3.026467041016" />
                  <Point X="-25.31833984375" Y="-3.084938476562" />
                  <Point X="-25.33292578125" Y="-3.090828857422" />
                  <Point X="-25.360927734375" Y="-3.104937011719" />
                  <Point X="-25.37434375" Y="-3.113154785156" />
                  <Point X="-25.400599609375" Y="-3.132398925781" />
                  <Point X="-25.412474609375" Y="-3.142716552734" />
                  <Point X="-25.434359375" Y="-3.165172119141" />
                  <Point X="-25.444369140625" Y="-3.177310058594" />
                  <Point X="-25.486349609375" Y="-3.237796142578" />
                  <Point X="-25.608095703125" Y="-3.413210449219" />
                  <Point X="-25.61246875" Y="-3.420131835938" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213623047" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.940625" Y="-4.599602539062" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.07063977972" Y="-2.718731523032" />
                  <Point X="-20.989031955251" Y="-2.571507110487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.15292470815" Y="-2.671224256194" />
                  <Point X="-21.065237701997" Y="-2.513032709577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.23520963658" Y="-2.623716989356" />
                  <Point X="-21.141443301209" Y="-2.45455804251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.371811297381" Y="-1.066105153619" />
                  <Point X="-20.216503891657" Y="-0.785923176946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.317494565009" Y="-2.576209722518" />
                  <Point X="-21.217648900422" Y="-2.396083375444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.473042469481" Y="-1.052777815145" />
                  <Point X="-20.306470517101" Y="-0.752274058364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.399779493439" Y="-2.52870245568" />
                  <Point X="-21.293854499634" Y="-2.337608708378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.574273641581" Y="-1.039450476671" />
                  <Point X="-20.40104263534" Y="-0.726933468719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.315554187379" Y="-3.984850529351" />
                  <Point X="-22.302477066887" Y="-3.961258779483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.482064421869" Y="-2.481195188841" />
                  <Point X="-21.370060098846" Y="-2.279134041312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.675504813681" Y="-1.026123138197" />
                  <Point X="-20.495614780793" Y="-0.701592928169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.378617026743" Y="-3.902665695883" />
                  <Point X="-22.357892185133" Y="-3.865277091897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.564349350299" Y="-2.433687922003" />
                  <Point X="-21.446265698059" Y="-2.220659374245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.776735985781" Y="-1.012795799723" />
                  <Point X="-20.590186926245" Y="-0.676252387619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.441679866108" Y="-3.820480862415" />
                  <Point X="-22.413307254909" Y="-3.769295316871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.646634278729" Y="-2.386180655165" />
                  <Point X="-21.522471297271" Y="-2.162184707179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.877967157881" Y="-0.999468461249" />
                  <Point X="-20.684759071697" Y="-0.650911847069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.504742705473" Y="-3.738296028948" />
                  <Point X="-22.468722324686" Y="-3.673313541846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.728919207158" Y="-2.338673388327" />
                  <Point X="-21.598676896484" Y="-2.103710040113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.979198329981" Y="-0.986141122774" />
                  <Point X="-20.77933121715" Y="-0.625571306519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.567805544837" Y="-3.65611119548" />
                  <Point X="-22.524137394463" Y="-3.57733176682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.811204138239" Y="-2.291166126271" />
                  <Point X="-21.674882495696" Y="-2.045235373047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.080429502081" Y="-0.9728137843" />
                  <Point X="-20.873903362602" Y="-0.600230765968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.630868384202" Y="-3.573926362012" />
                  <Point X="-22.57955246424" Y="-3.481349991794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.893489121668" Y="-2.243658958653" />
                  <Point X="-21.751088094909" Y="-1.98676070598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.181660674448" Y="-0.959486446307" />
                  <Point X="-20.968475508054" Y="-0.574890225418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.260695138009" Y="0.701979362387" />
                  <Point X="-20.224448600115" Y="0.767369847712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.693931223567" Y="-3.491741528545" />
                  <Point X="-22.634967534016" Y="-3.385368216769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.975774105097" Y="-2.196151791036" />
                  <Point X="-21.827293694121" Y="-1.928286038914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.28289185192" Y="-0.946159117526" />
                  <Point X="-21.063047653506" Y="-0.549549684868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.388260683257" Y="0.667798234097" />
                  <Point X="-20.248642052132" Y="0.919676912172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.756994062932" Y="-3.409556695077" />
                  <Point X="-22.690382603793" Y="-3.289386441743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.058059088525" Y="-2.148644623419" />
                  <Point X="-21.903499293334" Y="-1.869811371848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.384123029393" Y="-0.932831788745" />
                  <Point X="-21.157619798959" Y="-0.524209144318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.515826228505" Y="0.633617105806" />
                  <Point X="-20.281788593497" Y="1.055832175892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.820056902296" Y="-3.327371861609" />
                  <Point X="-22.74579767357" Y="-3.193404666718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.140344071954" Y="-2.101137455801" />
                  <Point X="-21.977425983039" Y="-1.807225443201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.485354206866" Y="-0.919504459964" />
                  <Point X="-21.252191944411" Y="-0.498868603767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.643391773754" Y="0.599435977515" />
                  <Point X="-20.364715826453" Y="1.102180694692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.883119741661" Y="-3.245187028142" />
                  <Point X="-22.801212743347" Y="-3.097422891692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.229601499371" Y="-2.06620891011" />
                  <Point X="-22.038189773425" Y="-1.720893015584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.587735944743" Y="-0.908252797097" />
                  <Point X="-21.344878977712" Y="-0.470127230873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.770957319002" Y="0.565254849225" />
                  <Point X="-20.481884994259" Y="1.086755127789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.946182491678" Y="-3.163002033487" />
                  <Point X="-22.856627813124" Y="-3.001441116666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.328336035079" Y="-2.048377520357" />
                  <Point X="-22.096518135292" Y="-1.630166958614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.706106209789" Y="-0.92584520078" />
                  <Point X="-21.427960709182" Y="-0.42405743477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.898522864251" Y="0.531073720934" />
                  <Point X="-20.599054208963" Y="1.071329476279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.009245199509" Y="-3.080816962726" />
                  <Point X="-22.9120428829" Y="-2.905459341641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.427070589438" Y="-2.030546164251" />
                  <Point X="-22.138707815053" Y="-1.510325948419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.829753221576" Y="-0.952957107576" />
                  <Point X="-21.505947396822" Y="-0.368795936284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.026088369982" Y="0.496892663934" />
                  <Point X="-20.716223423667" Y="1.05590382477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.07230790734" Y="-2.998631891964" />
                  <Point X="-22.959109869037" Y="-2.794417225063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.535854337296" Y="-2.03084403312" />
                  <Point X="-22.123071136177" Y="-1.286163425727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.027291392864" Y="-1.113372194803" />
                  <Point X="-21.571251747725" Y="-0.290654896676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.153653817201" Y="0.462711712492" />
                  <Point X="-20.833392638372" Y="1.04047817326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.13537061517" Y="-2.916446821203" />
                  <Point X="-22.985802374109" Y="-2.646618571657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.678893399245" Y="-2.09294012448" />
                  <Point X="-21.624615724114" Y="-0.190972851228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.281219264421" Y="0.428530761049" />
                  <Point X="-20.950561853076" Y="1.025052521751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.201914558624" Y="-2.840542065753" />
                  <Point X="-22.980409141635" Y="-2.440935715454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.849384305396" Y="-2.204560653752" />
                  <Point X="-21.650684373229" Y="-0.042048731882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.426538539838" Y="0.3623210557" />
                  <Point X="-21.06773106778" Y="1.009626870241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.280376220021" Y="-2.786137442607" />
                  <Point X="-21.184900282484" Y="0.994201218732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.36045675592" Y="-2.734653346372" />
                  <Point X="-21.302069497189" Y="0.978775567222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.195584585301" Y="-4.045310625066" />
                  <Point X="-24.183799088525" Y="-4.024049026063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.440571323541" Y="-2.683230644988" />
                  <Point X="-21.419238711893" Y="0.963349915713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.230979884866" Y="-3.91321222853" />
                  <Point X="-24.204645550732" Y="-3.865703832148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.53122074708" Y="-2.650813326776" />
                  <Point X="-21.534572230764" Y="0.95123594715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.266375305071" Y="-3.781114049633" />
                  <Point X="-24.225492012939" Y="-3.707358638233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.639922186079" Y="-2.650962706532" />
                  <Point X="-21.636845755057" Y="0.96268283249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.301770781418" Y="-3.649015972019" />
                  <Point X="-24.246338475146" Y="-3.549013444318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.754379049517" Y="-2.661495146828" />
                  <Point X="-21.731101693655" Y="0.988593825307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.931612669747" Y="2.430910204252" />
                  <Point X="-20.883658257157" Y="2.51742225464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.337166257766" Y="-3.516917894405" />
                  <Point X="-24.267184937352" Y="-3.390668250403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.87285853649" Y="-2.679284592083" />
                  <Point X="-21.814253376503" Y="1.034537425782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.120625314546" Y="2.285875573949" />
                  <Point X="-20.940348139548" Y="2.61110420683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.384431823404" Y="-3.406234024732" />
                  <Point X="-24.252575110556" Y="-3.168358217902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.047483787463" Y="-2.79836367685" />
                  <Point X="-21.88677317845" Y="1.099661447131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.309637959345" Y="2.140840943646" />
                  <Point X="-21.000410924774" Y="2.698701281232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.444820646996" Y="-3.319225139113" />
                  <Point X="-21.946007355607" Y="1.188753370061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.498650604144" Y="1.995806313343" />
                  <Point X="-21.065246681461" Y="2.777687687185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.505209756649" Y="-3.23221676956" />
                  <Point X="-21.986760355657" Y="1.311186219064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.687662658788" Y="1.850772747707" />
                  <Point X="-21.224987299356" Y="2.68546119131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.56820421952" Y="-3.149908581632" />
                  <Point X="-21.972702068685" Y="1.532501247384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.903035805211" Y="1.658182513622" />
                  <Point X="-21.384727741253" Y="2.593235012944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.646406835658" Y="-3.095036628469" />
                  <Point X="-21.544468143701" Y="2.501008905746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.73767282765" Y="-3.06373162919" />
                  <Point X="-21.704208546149" Y="2.408782798549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.830347942573" Y="-3.034968754971" />
                  <Point X="-21.863948948597" Y="2.316556691351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.923035762641" Y="-3.006228801441" />
                  <Point X="-22.023689351045" Y="2.224330584153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.005408529104" Y="-4.762927753881" />
                  <Point X="-25.964719340315" Y="-4.689522514182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.027985837" Y="-2.99961054024" />
                  <Point X="-22.183429753493" Y="2.132104476955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.102094810857" Y="-4.741401216178" />
                  <Point X="-25.863082786404" Y="-4.31021210998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.155968588273" Y="-3.034544328122" />
                  <Point X="-22.343170155941" Y="2.039878369758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.121344543538" Y="-4.580175445947" />
                  <Point X="-25.761446751319" Y="-3.930902641766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.287156445882" Y="-3.075260280897" />
                  <Point X="-22.480415045003" Y="1.988235242987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.139418383585" Y="-4.416828309249" />
                  <Point X="-25.659810716234" Y="-3.551593173552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.483968463356" Y="-3.234365351966" />
                  <Point X="-22.595651261225" Y="1.97629681305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.168103549854" Y="-4.2726245118" />
                  <Point X="-22.695687321469" Y="1.991780190385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.203376950625" Y="-4.140306204017" />
                  <Point X="-22.781512694189" Y="2.03290032665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.26528105636" Y="-4.056030959747" />
                  <Point X="-22.858370121428" Y="2.090199064828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.338369643887" Y="-3.991933054745" />
                  <Point X="-22.922490569974" Y="2.170475920825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.411458471972" Y="-3.927835583724" />
                  <Point X="-22.972898236141" Y="2.275491291093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.484547300058" Y="-3.863738112703" />
                  <Point X="-22.980750764895" Y="2.457278161486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.565935871911" Y="-3.814613775794" />
                  <Point X="-22.258516782038" Y="3.956175964305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.663366123131" Y="-3.794429394538" />
                  <Point X="-22.336425184045" Y="4.011578693812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.768176860225" Y="-3.787559762258" />
                  <Point X="-22.416690210158" Y="4.06272996089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.872987600496" Y="-3.780690135707" />
                  <Point X="-22.498793922445" Y="4.110564150303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.980070202178" Y="-3.777919055634" />
                  <Point X="-22.580897634733" Y="4.158398339716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.118080393261" Y="-3.830942823799" />
                  <Point X="-22.663001354018" Y="4.206232516505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.290595204076" Y="-3.946214573735" />
                  <Point X="-22.745105160687" Y="4.254066535648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.51503147491" Y="-4.15515511707" />
                  <Point X="-22.831138289626" Y="4.294811869773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.595895993053" Y="-4.105085362244" />
                  <Point X="-22.917396147598" Y="4.335151781989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.676760511197" Y="-4.055015607417" />
                  <Point X="-23.00365400557" Y="4.375491694205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.757036976728" Y="-4.003884977594" />
                  <Point X="-23.089911740019" Y="4.415831829262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.833164752548" Y="-3.945269913412" />
                  <Point X="-23.177837537243" Y="4.453162699413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.909292271341" Y="-3.88665438554" />
                  <Point X="-23.268273850057" Y="4.48596447955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.311404326414" Y="-2.612082773326" />
                  <Point X="-23.358710162872" Y="4.518766259687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.346615634288" Y="-2.479652446991" />
                  <Point X="-23.449146475686" Y="4.551568039824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.413223273783" Y="-2.403862602243" />
                  <Point X="-23.539583093664" Y="4.584369269431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.497041388025" Y="-2.359121275826" />
                  <Point X="-23.634976586054" Y="4.608228060881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.601552405969" Y="-2.351710935885" />
                  <Point X="-23.730774702226" Y="4.631356891707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.743411069937" Y="-2.411677532918" />
                  <Point X="-23.826572818398" Y="4.654485722532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.903151466058" Y="-2.503903628703" />
                  <Point X="-23.92237093457" Y="4.677614553357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.06289186218" Y="-2.596129724488" />
                  <Point X="-24.018169050742" Y="4.700743384182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.222632258302" Y="-2.688355820273" />
                  <Point X="-24.113967283309" Y="4.723872005027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.382372654424" Y="-2.780581916058" />
                  <Point X="-24.212461552843" Y="4.74213684643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.542113050546" Y="-2.872808011843" />
                  <Point X="-24.315120707442" Y="4.752888036282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.701853507309" Y="-2.965034217028" />
                  <Point X="-24.417779862041" Y="4.763639226135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.820041312309" Y="-2.982297454074" />
                  <Point X="-28.02998000668" Y="-1.556989129127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.957270098785" Y="-1.425816983003" />
                  <Point X="-24.888598008094" Y="4.110214013871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.722164177451" Y="4.410468592444" />
                  <Point X="-24.52043901664" Y="4.774390415988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.882890453763" Y="-2.899727099371" />
                  <Point X="-28.221858602648" Y="-1.707194072204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.970725215144" Y="-1.254137448203" />
                  <Point X="-25.010978451395" Y="4.085387057109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.945739595217" Y="-2.817156744667" />
                  <Point X="-28.410871346791" Y="-1.852228881728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.03299371359" Y="-1.170519585785" />
                  <Point X="-25.104315340644" Y="4.11295605884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.008111735378" Y="-2.733725856852" />
                  <Point X="-28.599883832309" Y="-1.99726322468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.114224342811" Y="-1.121110312825" />
                  <Point X="-25.178890945043" Y="4.174371314391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.064405357554" Y="-2.639329032309" />
                  <Point X="-28.788896317826" Y="-2.142297567631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.21116872124" Y="-1.100049393851" />
                  <Point X="-25.230040892542" Y="4.278047573689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.120699355747" Y="-2.544932886121" />
                  <Point X="-28.977908803343" Y="-2.287331910582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.32581556697" Y="-1.110924571275" />
                  <Point X="-25.265436345213" Y="4.410145694016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.176993415601" Y="-2.45053685117" />
                  <Point X="-29.166921288861" Y="-2.432366253534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.442984773705" Y="-1.126350208409" />
                  <Point X="-25.300831721447" Y="4.542243952237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.560153980441" Y="-1.141775845542" />
                  <Point X="-25.336227068497" Y="4.67434226311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.677323187176" Y="-1.157201482675" />
                  <Point X="-25.386224202018" Y="4.780098253875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.794492393912" Y="-1.172627119809" />
                  <Point X="-25.50237810198" Y="4.766504278648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.911661600647" Y="-1.188052756942" />
                  <Point X="-28.387900331669" Y="-0.243162415345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.30027884132" Y="-0.085089062367" />
                  <Point X="-25.618532001941" Y="4.752910303421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.028830807382" Y="-1.203478394075" />
                  <Point X="-28.539390913271" Y="-0.320505451763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.317375410957" Y="0.080021116822" />
                  <Point X="-25.734685901902" Y="4.739316328195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.146000014118" Y="-1.218904031209" />
                  <Point X="-28.666956454296" Y="-0.354686572435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.376174420168" Y="0.169898103506" />
                  <Point X="-25.850839883864" Y="4.725722205036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.263169220853" Y="-1.234329668342" />
                  <Point X="-28.794521995321" Y="-0.388867693107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.453512004099" Y="0.226330616084" />
                  <Point X="-25.970833119106" Y="4.705201885614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.380338427589" Y="-1.249755305475" />
                  <Point X="-28.922087534861" Y="-0.4230488111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.543931794135" Y="0.259162204101" />
                  <Point X="-26.099438018023" Y="4.66914571367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.497507632955" Y="-1.265180940139" />
                  <Point X="-29.049653070208" Y="-0.457229921527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.638503940795" Y="0.284502742473" />
                  <Point X="-26.228042916941" Y="4.633089541725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.614676835944" Y="-1.280606570513" />
                  <Point X="-29.177218605554" Y="-0.491411031954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.733076087454" Y="0.309843280845" />
                  <Point X="-26.637838473426" Y="4.089751995192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.462501430045" Y="4.40606839472" />
                  <Point X="-26.356647815858" Y="4.597033369781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.681659728947" Y="-1.205493701013" />
                  <Point X="-29.304784140901" Y="-0.525592142381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.827648234114" Y="0.335183819216" />
                  <Point X="-26.776124463757" Y="4.036230672015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.715923956137" Y="-1.071354795897" />
                  <Point X="-29.432349676247" Y="-0.559773252809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.922220380774" Y="0.360524357588" />
                  <Point X="-26.885295598443" Y="4.03523393881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.746890679105" Y="-0.93126703569" />
                  <Point X="-29.559915211594" Y="-0.593954363236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.016792527434" Y="0.38586489596" />
                  <Point X="-28.510011953085" Y="1.300121253529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.442595271096" Y="1.42174416734" />
                  <Point X="-26.97393834502" Y="4.071271398091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.769167268382" Y="-0.775501859306" />
                  <Point X="-29.68748074694" Y="-0.628135473663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.111364674094" Y="0.411205434331" />
                  <Point X="-28.664106346694" Y="1.218080815903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.441735644631" Y="1.619248181799" />
                  <Point X="-27.057358299994" Y="4.116731022839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.205936820754" Y="0.436545972703" />
                  <Point X="-28.779532947535" Y="1.205798923022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.495536129551" Y="1.71814274501" />
                  <Point X="-27.915634517629" Y="2.764312946277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.750374207483" Y="3.062450437832" />
                  <Point X="-27.128334089885" Y="4.184640515673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.300508967414" Y="0.461886511075" />
                  <Point X="-28.880764076231" Y="1.219126339799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.566143787133" Y="1.78671636611" />
                  <Point X="-28.04681849804" Y="2.723603988153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.777943233703" Y="3.208667805228" />
                  <Point X="-27.191396817938" Y="4.266825549952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.395081114073" Y="0.487227049446" />
                  <Point X="-28.981995204927" Y="1.232453756576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.642349383787" Y="1.845191037791" />
                  <Point X="-28.151426701636" Y="2.730839000538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.832291808824" Y="3.306573587543" />
                  <Point X="-27.346671064971" Y="4.182656600405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.489653260733" Y="0.512567587818" />
                  <Point X="-29.083226360029" Y="1.245781125716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.718554980441" Y="1.903665709473" />
                  <Point X="-28.245096454854" Y="2.757807499773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.887706972556" Y="3.40255519307" />
                  <Point X="-27.503625673716" Y="4.095456198084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.584225407393" Y="0.53790812619" />
                  <Point X="-29.184457571103" Y="1.259108393879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.794760577096" Y="1.962140381154" />
                  <Point X="-28.327649994954" Y="2.80483017833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.943122136288" Y="3.498536798596" />
                  <Point X="-27.663066033051" Y="4.00377138299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.67879758808" Y="0.563248603175" />
                  <Point X="-29.285688782177" Y="1.272435662042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.87096617375" Y="2.020615052835" />
                  <Point X="-28.409934890217" Y="2.852337505003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.998537300019" Y="3.594518404123" />
                  <Point X="-27.851962036618" Y="3.858947179041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.773369782634" Y="0.588589055144" />
                  <Point X="-29.386919993251" Y="1.285762930205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.947171770404" Y="2.079089724516" />
                  <Point X="-28.49221977547" Y="2.899844849733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.053952463751" Y="3.69050000965" />
                  <Point X="-28.040858040184" Y="3.714122975092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.750941334194" Y="0.825004254471" />
                  <Point X="-29.488151204325" Y="1.299090198369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.023377367059" Y="2.137564396197" />
                  <Point X="-28.574504659062" Y="2.947352197461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.678945333828" Y="1.150841684584" />
                  <Point X="-29.589382415399" Y="1.312417466532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.099582963713" Y="2.196039067878" />
                  <Point X="-28.656789542654" Y="2.994859545189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.175788560367" Y="2.25451373956" />
                  <Point X="-28.784812620165" Y="2.959853006847" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.425748046875" Y="-3.920435791016" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521543945312" />
                  <Point X="-24.57766015625" Y="-3.461057617188" />
                  <Point X="-24.699408203125" Y="-3.285643554688" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.790626953125" Y="-3.246237792969" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.073626953125" Y="-3.207928466797" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285644042969" />
                  <Point X="-25.330259765625" Y="-3.346130126953" />
                  <Point X="-25.452005859375" Y="-3.521544433594" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.75709765625" Y="-4.648778320312" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.878837890625" Y="-4.981041503906" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.174603515625" Y="-4.918933105469" />
                  <Point X="-26.351587890625" Y="-4.873396484375" />
                  <Point X="-26.321513671875" Y="-4.644953125" />
                  <Point X="-26.3091484375" Y="-4.5510390625" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.325201171875" Y="-4.456735351562" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.446091796875" Y="-4.150177246094" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.72862109375" Y="-3.980560058594" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791015625" />
                  <Point X="-27.056021484375" Y="-4.017987060547" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.42696484375" Y="-4.375233886719" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.53129296875" Y="-4.36855859375" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-27.95880078125" Y="-4.088329589844" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.69691015625" Y="-2.959728759766" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597590576172" />
                  <Point X="-27.513982421875" Y="-2.568760498047" />
                  <Point X="-27.53132421875" Y="-2.551418212891" />
                  <Point X="-27.560154296875" Y="-2.537198974609" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.55047265625" Y="-3.097027587891" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-28.905298828125" Y="-3.183991943359" />
                  <Point X="-29.16169921875" Y="-2.847135742188" />
                  <Point X="-29.235521484375" Y="-2.723346679688" />
                  <Point X="-29.43101953125" Y="-2.395526855469" />
                  <Point X="-28.497759765625" Y="-1.679411499023" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.152537109375" Y="-1.411084228516" />
                  <Point X="-28.143845703125" Y="-1.388385009766" />
                  <Point X="-28.1381171875" Y="-1.366265014648" />
                  <Point X="-28.13665234375" Y="-1.350050537109" />
                  <Point X="-28.14865625" Y="-1.321068969727" />
                  <Point X="-28.16794921875" Y="-1.306643310547" />
                  <Point X="-28.187640625" Y="-1.295053222656" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.429908203125" Y="-1.447920898438" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.827109375" Y="-1.403788818359" />
                  <Point X="-29.927392578125" Y="-1.011187805176" />
                  <Point X="-29.946921875" Y="-0.874636352539" />
                  <Point X="-29.99839453125" Y="-0.5147421875" />
                  <Point X="-28.937908203125" Y="-0.230585510254" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.53477734375" Y="-0.116485984802" />
                  <Point X="-28.514140625" Y="-0.102162872314" />
                  <Point X="-28.5023203125" Y="-0.090643028259" />
                  <Point X="-28.492525390625" Y="-0.068264976501" />
                  <Point X="-28.485646484375" Y="-0.046100727081" />
                  <Point X="-28.483400390625" Y="-0.031282693863" />
                  <Point X="-28.488017578125" Y="-0.008816726685" />
                  <Point X="-28.494896484375" Y="0.013347525597" />
                  <Point X="-28.502322265625" Y="0.028085069656" />
                  <Point X="-28.5212578125" Y="0.044541702271" />
                  <Point X="-28.54189453125" Y="0.058864818573" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.660783203125" Y="0.361719177246" />
                  <Point X="-29.998185546875" Y="0.452126098633" />
                  <Point X="-29.982275390625" Y="0.55965423584" />
                  <Point X="-29.91764453125" Y="0.996414916992" />
                  <Point X="-29.878328125" Y="1.14150378418" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.0259765625" Y="1.429883544922" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866699219" />
                  <Point X="-28.715953125" Y="1.400832641602" />
                  <Point X="-28.67027734375" Y="1.41523425293" />
                  <Point X="-28.651533203125" Y="1.426056030273" />
                  <Point X="-28.639119140625" Y="1.443785522461" />
                  <Point X="-28.63280078125" Y="1.459042724609" />
                  <Point X="-28.61447265625" Y="1.503289794922" />
                  <Point X="-28.610712890625" Y="1.52460546875" />
                  <Point X="-28.61631640625" Y="1.545512451172" />
                  <Point X="-28.62394140625" Y="1.560160888672" />
                  <Point X="-28.646056640625" Y="1.602642211914" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-29.2928359375" Y="2.104837890625" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.411142578125" Y="2.356761962891" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-29.05587109375" Y="2.920868164062" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.3266484375" Y="3.023645263672" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.116578125" Y="2.918515625" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927404296875" />
                  <Point X="-27.997681640625" Y="2.942974121094" />
                  <Point X="-27.95252734375" Y="2.988127685547" />
                  <Point X="-27.9408984375" Y="3.006381835938" />
                  <Point X="-27.938072265625" Y="3.027840332031" />
                  <Point X="-27.9399921875" Y="3.049775878906" />
                  <Point X="-27.945556640625" Y="3.113389892578" />
                  <Point X="-27.95206640625" Y="3.134032714844" />
                  <Point X="-28.232509765625" Y="3.6197734375" />
                  <Point X="-28.30727734375" Y="3.749276611328" />
                  <Point X="-28.190263671875" Y="3.838990966797" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.588853515625" Y="4.265458496094" />
                  <Point X="-27.141548828125" Y="4.513971679688" />
                  <Point X="-27.0191015625" Y="4.354395019531" />
                  <Point X="-26.967826171875" Y="4.287572753906" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.92683203125" Y="4.260951660156" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.78837890625" Y="4.232783691406" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.696904296875" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.294487792969" />
                  <Point X="-26.677806640625" Y="4.320737792969" />
                  <Point X="-26.653802734375" Y="4.396864257812" />
                  <Point X="-26.651916015625" Y="4.41842578125" />
                  <Point X="-26.68380078125" Y="4.660603027344" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.534318359375" Y="4.744546875" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.76925" Y="4.926567871094" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.091689453125" Y="4.495822753906" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176269531" />
                  <Point X="-24.945546875" Y="4.310902832031" />
                  <Point X="-24.8018515625" Y="4.847182128906" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.634189453125" Y="4.977342285156" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.975283203125" Y="4.885848144531" />
                  <Point X="-23.491544921875" Y="4.76905859375" />
                  <Point X="-23.38459375" Y="4.730266601562" />
                  <Point X="-23.06896484375" Y="4.615785644531" />
                  <Point X="-22.965423828125" Y="4.567362792969" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.56126953125" Y="4.366855957031" />
                  <Point X="-22.26747265625" Y="4.195688964844" />
                  <Point X="-22.17313671875" Y="4.128603515625" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.54921875" Y="2.886253173828" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491514892578" />
                  <Point X="-22.78065234375" Y="2.470928955078" />
                  <Point X="-22.7966171875" Y="2.411228515625" />
                  <Point X="-22.79580859375" Y="2.374525390625" />
                  <Point X="-22.789583984375" Y="2.322901367188" />
                  <Point X="-22.781318359375" Y="2.300812255859" />
                  <Point X="-22.770302734375" Y="2.284579589844" />
                  <Point X="-22.738359375" Y="2.237503662109" />
                  <Point X="-22.708826171875" Y="2.213188964844" />
                  <Point X="-22.66175" Y="2.18124609375" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.621861328125" Y="2.170833740234" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5513359375" Y="2.165946289062" />
                  <Point X="-22.53075" Y="2.171451416016" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.349236328125" Y="2.833119140625" />
                  <Point X="-21.005751953125" Y="3.031430664062" />
                  <Point X="-20.97167578125" Y="2.984073486328" />
                  <Point X="-20.79740234375" Y="2.741874267578" />
                  <Point X="-20.744814453125" Y="2.654970947266" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.42051171875" Y="1.816275146484" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583833374023" />
                  <Point X="-21.735443359375" Y="1.564505249023" />
                  <Point X="-21.77841015625" Y="1.508452026367" />
                  <Point X="-21.78687890625" Y="1.491500610352" />
                  <Point X="-21.7923984375" Y="1.471766479492" />
                  <Point X="-21.808404296875" Y="1.414536010742" />
                  <Point X="-21.809220703125" Y="1.390965209961" />
                  <Point X="-21.804689453125" Y="1.369008422852" />
                  <Point X="-21.79155078125" Y="1.305332397461" />
                  <Point X="-21.784353515625" Y="1.287955566406" />
                  <Point X="-21.77203125" Y="1.26922644043" />
                  <Point X="-21.736296875" Y="1.21491003418" />
                  <Point X="-21.719052734375" Y="1.198819946289" />
                  <Point X="-21.7011953125" Y="1.188768188477" />
                  <Point X="-21.64941015625" Y="1.159617553711" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.6072890625" Y="1.150428710938" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.470029296875" Y="1.279955566406" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.135658203125" Y="1.258834594727" />
                  <Point X="-20.060806640625" Y="0.951366943359" />
                  <Point X="-20.044236328125" Y="0.8449375" />
                  <Point X="-20.002140625" Y="0.574556396484" />
                  <Point X="-20.92548828125" Y="0.327145874023" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819198608" />
                  <Point X="-21.2946328125" Y="0.219108657837" />
                  <Point X="-21.363421875" Y="0.179346832275" />
                  <Point X="-21.377734375" Y="0.166926391602" />
                  <Point X="-21.391966796875" Y="0.148791397095" />
                  <Point X="-21.433240234375" Y="0.09619871521" />
                  <Point X="-21.443013671875" Y="0.074734954834" />
                  <Point X="-21.4477578125" Y="0.0499635849" />
                  <Point X="-21.461515625" Y="-0.021875450134" />
                  <Point X="-21.461515625" Y="-0.040684627533" />
                  <Point X="-21.456771484375" Y="-0.065456001282" />
                  <Point X="-21.443013671875" Y="-0.137295028687" />
                  <Point X="-21.433240234375" Y="-0.158758789063" />
                  <Point X="-21.4190078125" Y="-0.176893783569" />
                  <Point X="-21.377734375" Y="-0.229486465454" />
                  <Point X="-21.363421875" Y="-0.241906906128" />
                  <Point X="-21.339701171875" Y="-0.255617446899" />
                  <Point X="-21.270912109375" Y="-0.295379272461" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.29211328125" Y="-0.559418579102" />
                  <Point X="-20.001931640625" Y="-0.637172546387" />
                  <Point X="-20.009775390625" Y="-0.689206298828" />
                  <Point X="-20.051568359375" Y="-0.966412841797" />
                  <Point X="-20.072798828125" Y="-1.059446533203" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-21.20143359375" Y="-1.148522827148" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.651716796875" Y="-1.108459960938" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.814552734375" Y="-1.154697143555" />
                  <Point X="-21.84269140625" Y="-1.188539672852" />
                  <Point X="-21.924296875" Y="-1.286685180664" />
                  <Point X="-21.935640625" Y="-1.314070800781" />
                  <Point X="-21.939673828125" Y="-1.35789831543" />
                  <Point X="-21.951369140625" Y="-1.485001220703" />
                  <Point X="-21.943638671875" Y="-1.516621826172" />
                  <Point X="-21.917875" Y="-1.556695678711" />
                  <Point X="-21.843158203125" Y="-1.672912475586" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-20.934416015625" Y="-2.373926269531" />
                  <Point X="-20.660923828125" Y="-2.583784179688" />
                  <Point X="-20.678060546875" Y="-2.611512939453" />
                  <Point X="-20.795865234375" Y="-2.802138427734" />
                  <Point X="-20.839779296875" Y="-2.864534912109" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.90223828125" Y="-2.458000732422" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.318064453125" Y="-2.243306396484" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.556951171875" Y="-2.243469726562" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.334683105469" />
                  <Point X="-22.735623046875" Y="-2.380712646484" />
                  <Point X="-22.80587890625" Y="-2.514201171875" />
                  <Point X="-22.8108359375" Y="-2.546374511719" />
                  <Point X="-22.800828125" Y="-2.60178125" />
                  <Point X="-22.77180859375" Y="-2.76246484375" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.189416015625" Y="-3.777089599609" />
                  <Point X="-22.01332421875" Y="-4.082089111328" />
                  <Point X="-22.024912109375" Y="-4.090366210938" />
                  <Point X="-22.164685546875" Y="-4.190202148438" />
                  <Point X="-22.213796875" Y="-4.221991210938" />
                  <Point X="-22.32022265625" Y="-4.29087890625" />
                  <Point X="-23.054083984375" Y="-3.334492675781" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.384095703125" Y="-2.945334960938" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.633958984375" Y="-2.841216796875" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.880814453125" Y="-2.906880126953" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045985839844" />
                  <Point X="-24.04533984375" Y="-3.10946875" />
                  <Point X="-24.085357421875" Y="-3.293573730469" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-23.922697265625" Y="-4.551661621094" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.87340234375" Y="-4.9342578125" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.05101171875" Y="-4.971486816406" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#142" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.049548971452" Y="4.540043761932" Z="0.65" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.65" />
                  <Point X="-0.781219238283" Y="5.007509184099" Z="0.65" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.65" />
                  <Point X="-1.553973044818" Y="4.823970338236" Z="0.65" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.65" />
                  <Point X="-1.739529051844" Y="4.685357439594" Z="0.65" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.65" />
                  <Point X="-1.731648567591" Y="4.367053989318" Z="0.65" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.65" />
                  <Point X="-1.813670313711" Y="4.310257594668" Z="0.65" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.65" />
                  <Point X="-1.909901257286" Y="4.33658213761" Z="0.65" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.65" />
                  <Point X="-1.985589778871" Y="4.41611368167" Z="0.65" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.65" />
                  <Point X="-2.619292722615" Y="4.34044628258" Z="0.65" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.65" />
                  <Point X="-3.226841738242" Y="3.909950934568" Z="0.65" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.65" />
                  <Point X="-3.281967312673" Y="3.626053758592" Z="0.65" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.65" />
                  <Point X="-2.995959124172" Y="3.076698881895" Z="0.65" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.65" />
                  <Point X="-3.039193704129" Y="3.009609863758" Z="0.65" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.65" />
                  <Point X="-3.118377583712" Y="2.999605575001" Z="0.65" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.65" />
                  <Point X="-3.307805589705" Y="3.098226636073" Z="0.65" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.65" />
                  <Point X="-4.101490492392" Y="2.982850552871" Z="0.65" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.65" />
                  <Point X="-4.460481739335" Y="2.413247024759" Z="0.65" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.65" />
                  <Point X="-4.329429504242" Y="2.096450160351" Z="0.65" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.65" />
                  <Point X="-3.674447896769" Y="1.568352761616" Z="0.65" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.65" />
                  <Point X="-3.685150159406" Y="1.50945733361" Z="0.65" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.65" />
                  <Point X="-3.737146063554" Y="1.47979848801" Z="0.65" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.65" />
                  <Point X="-4.025608932035" Y="1.510735874986" Z="0.65" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.65" />
                  <Point X="-4.932745647644" Y="1.185860937105" Z="0.65" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.65" />
                  <Point X="-5.037892524107" Y="0.598253431006" Z="0.65" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.65" />
                  <Point X="-4.679881185355" Y="0.344702860097" Z="0.65" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.65" />
                  <Point X="-3.555923849975" Y="0.034745854875" Z="0.65" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.65" />
                  <Point X="-3.54192877932" Y="0.007642666657" Z="0.65" />
                  <Point X="-3.539556741714" Y="0" Z="0.65" />
                  <Point X="-3.546435821815" Y="-0.022164284404" Z="0.65" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.65" />
                  <Point X="-3.569444745149" Y="-0.044130128259" Z="0.65" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.65" />
                  <Point X="-3.957006603418" Y="-0.151009196659" Z="0.65" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.65" />
                  <Point X="-5.002575542816" Y="-0.850435492984" Z="0.65" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.65" />
                  <Point X="-4.881712526674" Y="-1.384883157171" Z="0.65" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.65" />
                  <Point X="-4.429540672593" Y="-1.46621306238" Z="0.65" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.65" />
                  <Point X="-3.199466585888" Y="-1.318453339962" Z="0.65" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.65" />
                  <Point X="-3.198405326786" Y="-1.344569781307" Z="0.65" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.65" />
                  <Point X="-3.534354062682" Y="-1.608463855506" Z="0.65" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.65" />
                  <Point X="-4.284621619936" Y="-2.717676252689" Z="0.65" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.65" />
                  <Point X="-3.951241339277" Y="-3.182995139943" Z="0.65" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.65" />
                  <Point X="-3.531629769661" Y="-3.109048784063" Z="0.65" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.65" />
                  <Point X="-2.559939228856" Y="-2.568391240276" Z="0.65" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.65" />
                  <Point X="-2.746368229105" Y="-2.903448612831" Z="0.65" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.65" />
                  <Point X="-2.995460886088" Y="-4.096666913651" Z="0.65" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.65" />
                  <Point X="-2.563771506369" Y="-4.379789162672" Z="0.65" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.65" />
                  <Point X="-2.393453313158" Y="-4.374391832631" Z="0.65" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.65" />
                  <Point X="-2.034399973745" Y="-4.028280425266" Z="0.65" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.65" />
                  <Point X="-1.738047214159" Y="-3.99917306494" Z="0.65" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.65" />
                  <Point X="-1.485215352806" Y="-4.156489021572" Z="0.65" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.65" />
                  <Point X="-1.380398105731" Y="-4.435210154441" Z="0.65" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.65" />
                  <Point X="-1.377242541349" Y="-4.607146140631" Z="0.65" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.65" />
                  <Point X="-1.193220234479" Y="-4.936076158126" Z="0.65" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.65" />
                  <Point X="-0.894521469102" Y="-4.99884543735" Z="0.65" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.65" />
                  <Point X="-0.714956825476" Y="-4.630439301874" Z="0.65" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.65" />
                  <Point X="-0.295339764888" Y="-3.343358841085" Z="0.65" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.65" />
                  <Point X="-0.06496266539" Y="-3.224401382429" Z="0.65" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.65" />
                  <Point X="0.188396413972" Y="-3.262710662859" Z="0.65" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.65" />
                  <Point X="0.375106094544" Y="-3.458286914683" Z="0.65" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.65" />
                  <Point X="0.519798034926" Y="-3.902096727941" Z="0.65" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.65" />
                  <Point X="0.951769454981" Y="-4.989401625327" Z="0.65" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.65" />
                  <Point X="1.131145409099" Y="-4.951818206771" Z="0.65" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.65" />
                  <Point X="1.120718839102" Y="-4.513855103699" Z="0.65" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.65" />
                  <Point X="0.997361750065" Y="-3.088808860535" Z="0.65" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.65" />
                  <Point X="1.144994928741" Y="-2.91404667236" Z="0.65" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.65" />
                  <Point X="1.364465739511" Y="-2.859726192809" Z="0.65" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.65" />
                  <Point X="1.582707871373" Y="-2.956112924329" Z="0.65" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.65" />
                  <Point X="1.900090776153" Y="-3.333650567081" Z="0.65" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.65" />
                  <Point X="2.807216322698" Y="-4.232685101368" Z="0.65" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.65" />
                  <Point X="2.997990673943" Y="-4.099773059617" Z="0.65" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.65" />
                  <Point X="2.847727636269" Y="-3.720809455595" Z="0.65" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.65" />
                  <Point X="2.242217539128" Y="-2.56161496227" Z="0.65" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.65" />
                  <Point X="2.302465238736" Y="-2.372719598348" Z="0.65" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.65" />
                  <Point X="2.460178780024" Y="-2.256436071231" Z="0.65" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.65" />
                  <Point X="2.666891841243" Y="-2.261230470363" Z="0.65" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.65" />
                  <Point X="3.066604141511" Y="-2.470021864005" Z="0.65" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.65" />
                  <Point X="4.194951983017" Y="-2.86203209931" Z="0.65" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.65" />
                  <Point X="4.358315380285" Y="-2.606518163858" Z="0.65" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.65" />
                  <Point X="4.089864080339" Y="-2.302978472258" Z="0.65" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.65" />
                  <Point X="3.118026214253" Y="-1.498376252097" Z="0.65" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.65" />
                  <Point X="3.103958467322" Y="-1.331199694576" Z="0.65" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.65" />
                  <Point X="3.189596543585" Y="-1.18922664658" Z="0.65" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.65" />
                  <Point X="3.352745764072" Y="-1.126039149801" Z="0.65" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.65" />
                  <Point X="3.785884220169" Y="-1.166815217164" Z="0.65" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.65" />
                  <Point X="4.969789648516" Y="-1.039290515755" Z="0.65" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.65" />
                  <Point X="5.033508532318" Y="-0.665380410605" Z="0.65" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.65" />
                  <Point X="4.714671942979" Y="-0.479842265538" Z="0.65" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.65" />
                  <Point X="3.679162785091" Y="-0.181048833227" Z="0.65" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.65" />
                  <Point X="3.614168765949" Y="-0.114745497486" Z="0.65" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.65" />
                  <Point X="3.586178740795" Y="-0.024771473102" Z="0.65" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.65" />
                  <Point X="3.595192709629" Y="0.071839058128" Z="0.65" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.65" />
                  <Point X="3.641210672452" Y="0.149203241089" Z="0.65" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.65" />
                  <Point X="3.724232629262" Y="0.207100011015" Z="0.65" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.65" />
                  <Point X="4.081295874327" Y="0.310129667231" Z="0.65" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.65" />
                  <Point X="4.999010580588" Y="0.883909437875" Z="0.65" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.65" />
                  <Point X="4.906766374431" Y="1.301941390364" Z="0.65" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.65" />
                  <Point X="4.517288583205" Y="1.360807866533" Z="0.65" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.65" />
                  <Point X="3.393103966223" Y="1.231277741839" Z="0.65" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.65" />
                  <Point X="3.317175162106" Y="1.263619306154" Z="0.65" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.65" />
                  <Point X="3.263583218331" Y="1.32798716744" Z="0.65" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.65" />
                  <Point X="3.238122452822" Y="1.410392482505" Z="0.65" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.65" />
                  <Point X="3.249597125735" Y="1.489579580898" Z="0.65" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.65" />
                  <Point X="3.298082491123" Y="1.565366894508" Z="0.65" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.65" />
                  <Point X="3.603768093658" Y="1.807887443937" Z="0.65" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.65" />
                  <Point X="4.291805523022" Y="2.712137222266" Z="0.65" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.65" />
                  <Point X="4.062753072433" Y="3.044556552108" Z="0.65" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.65" />
                  <Point X="3.619605867174" Y="2.907700460247" Z="0.65" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.65" />
                  <Point X="2.45017749676" Y="2.251034096881" Z="0.65" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.65" />
                  <Point X="2.377967707085" Y="2.251754066833" Z="0.65" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.65" />
                  <Point X="2.313090776206" Y="2.285843520977" Z="0.65" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.65" />
                  <Point X="2.264915079258" Y="2.343934084176" Z="0.65" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.65" />
                  <Point X="2.247675635926" Y="2.411790736747" Z="0.65" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.65" />
                  <Point X="2.261493845776" Y="2.489292079122" Z="0.65" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.65" />
                  <Point X="2.487924892612" Y="2.892533214781" Z="0.65" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.65" />
                  <Point X="2.849682918537" Y="4.200630486783" Z="0.65" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.65" />
                  <Point X="2.457744198355" Y="4.441338757343" Z="0.65" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.65" />
                  <Point X="2.049601461394" Y="4.643934783701" Z="0.65" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.65" />
                  <Point X="1.62629803108" Y="4.808550627943" Z="0.65" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.65" />
                  <Point X="1.03029374554" Y="4.965731550357" Z="0.65" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.65" />
                  <Point X="0.364860388841" Y="5.058350271797" Z="0.65" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.65" />
                  <Point X="0.143695590101" Y="4.891403744019" Z="0.65" />
                  <Point X="0" Y="4.355124473572" Z="0.65" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>