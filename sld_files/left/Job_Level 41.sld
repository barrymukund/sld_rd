<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#197" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2950" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.121076171875" Y="-4.6904296875" />
                  <Point X="-24.4366953125" Y="-3.512525146484" />
                  <Point X="-24.44227734375" Y="-3.497140136719" />
                  <Point X="-24.457634765625" Y="-3.467376953125" />
                  <Point X="-24.58663671875" Y="-3.281510498047" />
                  <Point X="-24.62136328125" Y="-3.2314765625" />
                  <Point X="-24.643248046875" Y="-3.209021240234" />
                  <Point X="-24.66950390625" Y="-3.189777099609" />
                  <Point X="-24.69750390625" Y="-3.175669189453" />
                  <Point X="-24.897125" Y="-3.113713867188" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9790234375" Y="-3.092766357422" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.2364453125" Y="-3.158990966797" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.189776855469" />
                  <Point X="-25.344439453125" Y="-3.209020751953" />
                  <Point X="-25.36632421875" Y="-3.231477050781" />
                  <Point X="-25.49532421875" Y="-3.417343261719" />
                  <Point X="-25.53005078125" Y="-3.467377441406" />
                  <Point X="-25.5381875" Y="-3.481572021484" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.635953125" Y="-3.829609375" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.02004296875" Y="-4.856859863281" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.24641796875" Y="-4.802362304688" />
                  <Point X="-26.2149609375" Y="-4.563436035156" />
                  <Point X="-26.21419921875" Y="-4.547929199219" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.26419921875" Y="-4.276471191406" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.18296484375" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.5074296875" Y="-3.970027099609" />
                  <Point X="-26.556904296875" Y="-3.926639404297" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.886953125" Y="-3.874978515625" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.983419921875" Y="-3.873708984375" />
                  <Point X="-27.01446875" Y="-3.882029052734" />
                  <Point X="-27.042658203125" Y="-3.894802001953" />
                  <Point X="-27.24591015625" Y="-4.030610595703" />
                  <Point X="-27.300623046875" Y="-4.067169677734" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.335099609375" Y="-4.0994609375" />
                  <Point X="-27.38280078125" Y="-4.161624511719" />
                  <Point X="-27.480146484375" Y="-4.288489257813" />
                  <Point X="-27.714796875" Y="-4.143200195312" />
                  <Point X="-27.801712890625" Y="-4.089384033203" />
                  <Point X="-28.10472265625" Y="-3.856077636719" />
                  <Point X="-28.010302734375" Y="-3.692539306641" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647654296875" />
                  <Point X="-27.406587890625" Y="-2.616127197266" />
                  <Point X="-27.40557421875" Y="-2.585190917969" />
                  <Point X="-27.414560546875" Y="-2.555571289062" />
                  <Point X="-27.428779296875" Y="-2.526741699219" />
                  <Point X="-27.44680078125" Y="-2.501591552734" />
                  <Point X="-27.46413671875" Y="-2.484253662109" />
                  <Point X="-27.489283203125" Y="-2.466230224609" />
                  <Point X="-27.518115234375" Y="-2.452004882812" />
                  <Point X="-27.547736328125" Y="-2.443014160156" />
                  <Point X="-27.57867578125" Y="-2.444023681641" />
                  <Point X="-27.610208984375" Y="-2.450293701172" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-27.912662109375" Y="-2.619090820312" />
                  <Point X="-28.818021484375" Y="-3.141800537109" />
                  <Point X="-29.0141953125" Y="-2.884070800781" />
                  <Point X="-29.082865234375" Y="-2.793850097656" />
                  <Point X="-29.306142578125" Y="-2.419449707031" />
                  <Point X="-29.1322109375" Y="-2.285988525391" />
                  <Point X="-28.105955078125" Y="-1.498513549805" />
                  <Point X="-28.084578125" Y="-1.475592773438" />
                  <Point X="-28.06661328125" Y="-1.448461303711" />
                  <Point X="-28.05385546875" Y="-1.419829101562" />
                  <Point X="-28.047787109375" Y="-1.396395751953" />
                  <Point X="-28.046162109375" Y="-1.390126342773" />
                  <Point X="-28.04334765625" Y="-1.359667114258" />
                  <Point X="-28.0455546875" Y="-1.327993774414" />
                  <Point X="-28.0525546875" Y="-1.298245849609" />
                  <Point X="-28.06863671875" Y="-1.272259765625" />
                  <Point X="-28.089470703125" Y="-1.248301147461" />
                  <Point X="-28.112970703125" Y="-1.228766967773" />
                  <Point X="-28.1338359375" Y="-1.216486328125" />
                  <Point X="-28.139453125" Y="-1.213180419922" />
                  <Point X="-28.16871875" Y="-1.201955810547" />
                  <Point X="-28.20060546875" Y="-1.195474609375" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-28.577169921875" Y="-1.23983581543" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.807220703125" Y="-1.097793945313" />
                  <Point X="-29.83407421875" Y="-0.99265234375" />
                  <Point X="-29.892423828125" Y="-0.584698547363" />
                  <Point X="-29.701935546875" Y="-0.533657592773" />
                  <Point X="-28.532875" Y="-0.220408432007" />
                  <Point X="-28.5174921875" Y="-0.214827392578" />
                  <Point X="-28.48773046875" Y="-0.199471069336" />
                  <Point X="-28.46586328125" Y="-0.184294616699" />
                  <Point X="-28.4599765625" Y="-0.180209030151" />
                  <Point X="-28.43751953125" Y="-0.158324188232" />
                  <Point X="-28.418275390625" Y="-0.132068557739" />
                  <Point X="-28.40416796875" Y="-0.104068252563" />
                  <Point X="-28.39687890625" Y="-0.080583244324" />
                  <Point X="-28.3949140625" Y="-0.074252830505" />
                  <Point X="-28.39064453125" Y="-0.04608008194" />
                  <Point X="-28.3906484375" Y="-0.016446353912" />
                  <Point X="-28.39491796875" Y="0.011704078674" />
                  <Point X="-28.402205078125" Y="0.03518012619" />
                  <Point X="-28.404150390625" Y="0.04145665741" />
                  <Point X="-28.41826953125" Y="0.069495521545" />
                  <Point X="-28.437517578125" Y="0.095759658813" />
                  <Point X="-28.4599765625" Y="0.117648742676" />
                  <Point X="-28.48184375" Y="0.1328253479" />
                  <Point X="-28.491720703125" Y="0.138825164795" />
                  <Point X="-28.513173828125" Y="0.150130203247" />
                  <Point X="-28.532875" Y="0.157848297119" />
                  <Point X="-28.847578125" Y="0.242172988892" />
                  <Point X="-29.89181640625" Y="0.521975524902" />
                  <Point X="-29.841794921875" Y="0.860017272949" />
                  <Point X="-29.82448828125" Y="0.976967834473" />
                  <Point X="-29.70355078125" Y="1.423268066406" />
                  <Point X="-29.603677734375" Y="1.410119506836" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744984375" Y="1.299341674805" />
                  <Point X="-28.723423828125" Y="1.301227783203" />
                  <Point X="-28.703134765625" Y="1.305263427734" />
                  <Point X="-28.65473828125" Y="1.320523071289" />
                  <Point X="-28.641708984375" Y="1.324630859375" />
                  <Point X="-28.6227734375" Y="1.332963867188" />
                  <Point X="-28.604029296875" Y="1.343787109375" />
                  <Point X="-28.5873515625" Y="1.356017578125" />
                  <Point X="-28.573712890625" Y="1.371567749023" />
                  <Point X="-28.561296875" Y="1.389299926758" />
                  <Point X="-28.551349609375" Y="1.407434204102" />
                  <Point X="-28.5319296875" Y="1.454317871094" />
                  <Point X="-28.526703125" Y="1.466938598633" />
                  <Point X="-28.520912109375" Y="1.486807739258" />
                  <Point X="-28.51715625" Y="1.508122802734" />
                  <Point X="-28.515806640625" Y="1.52875769043" />
                  <Point X="-28.518951171875" Y="1.549196166992" />
                  <Point X="-28.524552734375" Y="1.57010168457" />
                  <Point X="-28.53205078125" Y="1.589379394531" />
                  <Point X="-28.555482421875" Y="1.634392211914" />
                  <Point X="-28.561791015625" Y="1.646509277344" />
                  <Point X="-28.57328515625" Y="1.663713134766" />
                  <Point X="-28.587197265625" Y="1.680290893555" />
                  <Point X="-28.60213671875" Y="1.69458996582" />
                  <Point X="-28.782650390625" Y="1.833104248047" />
                  <Point X="-29.351859375" Y="2.269873291016" />
                  <Point X="-29.1483984375" Y="2.618450195312" />
                  <Point X="-29.0811484375" Y="2.733665039062" />
                  <Point X="-28.760783203125" Y="3.145451416016" />
                  <Point X="-28.75050390625" Y="3.158662597656" />
                  <Point X="-28.72133984375" Y="3.141823974609" />
                  <Point X="-28.20665625" Y="2.844671142578" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.079388671875" Y="2.819899169922" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.99901171875" Y="2.826504882812" />
                  <Point X="-27.9804609375" Y="2.835654541016" />
                  <Point X="-27.96220703125" Y="2.847284179688" />
                  <Point X="-27.946078125" Y="2.860228759766" />
                  <Point X="-27.898232421875" Y="2.908072998047" />
                  <Point X="-27.885353515625" Y="2.920952148438" />
                  <Point X="-27.872404296875" Y="2.937086669922" />
                  <Point X="-27.860775390625" Y="2.955341308594" />
                  <Point X="-27.85162890625" Y="2.973889892578" />
                  <Point X="-27.8467109375" Y="2.993978515625" />
                  <Point X="-27.843884765625" Y="3.0154375" />
                  <Point X="-27.84343359375" Y="3.036123535156" />
                  <Point X="-27.84933203125" Y="3.103528076172" />
                  <Point X="-27.85091796875" Y="3.12166796875" />
                  <Point X="-27.854953125" Y="3.141953369141" />
                  <Point X="-27.861462890625" Y="3.162599853516" />
                  <Point X="-27.869794921875" Y="3.181533203125" />
                  <Point X="-27.949787109375" Y="3.320083007812" />
                  <Point X="-28.18333203125" Y="3.724595703125" />
                  <Point X="-27.817740234375" Y="4.004890869141" />
                  <Point X="-27.70062109375" Y="4.094686279297" />
                  <Point X="-27.196052734375" Y="4.375013671875" />
                  <Point X="-27.167037109375" Y="4.391134277344" />
                  <Point X="-27.043197265625" Y="4.229743164063" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.995111328125" Y="4.18939453125" />
                  <Point X="-26.92008984375" Y="4.150340820313" />
                  <Point X="-26.89989453125" Y="4.139827636719" />
                  <Point X="-26.8806171875" Y="4.132331054688" />
                  <Point X="-26.8597109375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.12358203125" />
                  <Point X="-26.81862890625" Y="4.124934570312" />
                  <Point X="-26.7973125" Y="4.128692871094" />
                  <Point X="-26.777451171875" Y="4.134481445312" />
                  <Point X="-26.6993125" Y="4.166848144531" />
                  <Point X="-26.67827734375" Y="4.175561035156" />
                  <Point X="-26.660142578125" Y="4.185509765625" />
                  <Point X="-26.6424140625" Y="4.197923339844" />
                  <Point X="-26.626865234375" Y="4.211560058594" />
                  <Point X="-26.614634765625" Y="4.228239257812" />
                  <Point X="-26.6038125" Y="4.246981933594" />
                  <Point X="-26.595478515625" Y="4.265919921875" />
                  <Point X="-26.570046875" Y="4.346582519531" />
                  <Point X="-26.56319921875" Y="4.368296386719" />
                  <Point X="-26.5591640625" Y="4.38858203125" />
                  <Point X="-26.55727734375" Y="4.410145996094" />
                  <Point X="-26.557728515625" Y="4.430828125" />
                  <Point X="-26.56682421875" Y="4.499905273438" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.101248046875" Y="4.767301269531" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.337943359375" Y="4.881397949219" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.286162109375" Y="4.854550292969" />
                  <Point X="-25.133904296875" Y="4.28631640625" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286314941406" />
                  <Point X="-24.812794921875" Y="4.439280273438" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.288345703125" Y="4.845603515625" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.649884765625" Y="4.709557617188" />
                  <Point X="-23.51897265625" Y="4.677950195312" />
                  <Point X="-23.189880859375" Y="4.558587402344" />
                  <Point X="-23.105353515625" Y="4.527928710938" />
                  <Point X="-22.786833984375" Y="4.378967285156" />
                  <Point X="-22.705423828125" Y="4.340893554688" />
                  <Point X="-22.397697265625" Y="4.161612304688" />
                  <Point X="-22.319021484375" Y="4.115775878906" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.1735390625" Y="3.72694921875" />
                  <Point X="-22.852416015625" Y="2.551098144531" />
                  <Point X="-22.85791796875" Y="2.539940429688" />
                  <Point X="-22.866921875" Y="2.516059326172" />
                  <Point X="-22.883837890625" Y="2.452801513672" />
                  <Point X="-22.885810546875" Y="2.443633056641" />
                  <Point X="-22.891703125" Y="2.407699951172" />
                  <Point X="-22.892271484375" Y="2.380953613281" />
                  <Point X="-22.88567578125" Y="2.326253662109" />
                  <Point X="-22.883900390625" Y="2.311528808594" />
                  <Point X="-22.87855859375" Y="2.289608398438" />
                  <Point X="-22.87029296875" Y="2.267518798828" />
                  <Point X="-22.8599296875" Y="2.247469726562" />
                  <Point X="-22.82608203125" Y="2.197588623047" />
                  <Point X="-22.820265625" Y="2.189889892578" />
                  <Point X="-22.797853515625" Y="2.163162353516" />
                  <Point X="-22.778400390625" Y="2.145592285156" />
                  <Point X="-22.72851953125" Y="2.111745849609" />
                  <Point X="-22.715091796875" Y="2.102634765625" />
                  <Point X="-22.695044921875" Y="2.092271728516" />
                  <Point X="-22.67295703125" Y="2.084006103516" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.5963359375" Y="2.072067626953" />
                  <Point X="-22.586201171875" Y="2.071392333984" />
                  <Point X="-22.55257421875" Y="2.070954101563" />
                  <Point X="-22.526794921875" Y="2.074170898438" />
                  <Point X="-22.463537109375" Y="2.091086914063" />
                  <Point X="-22.458453125" Y="2.092599609375" />
                  <Point X="-22.42933984375" Y="2.102155029297" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-22.09493359375" Y="2.292894775391" />
                  <Point X="-21.032673828125" Y="2.906190673828" />
                  <Point X="-20.923396484375" Y="2.754321044922" />
                  <Point X="-20.87671875" Y="2.689450927734" />
                  <Point X="-20.73780078125" Y="2.459883789063" />
                  <Point X="-20.875408203125" Y="2.35429296875" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.778576171875" Y="1.660239868164" />
                  <Point X="-21.796025390625" Y="1.641627319336" />
                  <Point X="-21.84155078125" Y="1.58223425293" />
                  <Point X="-21.853806640625" Y="1.566245849609" />
                  <Point X="-21.86338671875" Y="1.550922485352" />
                  <Point X="-21.878369140625" Y="1.51708972168" />
                  <Point X="-21.895328125" Y="1.45644909668" />
                  <Point X="-21.89989453125" Y="1.440125244141" />
                  <Point X="-21.90334765625" Y="1.417826904297" />
                  <Point X="-21.9041640625" Y="1.394252075195" />
                  <Point X="-21.902259765625" Y="1.371766235352" />
                  <Point X="-21.888337890625" Y="1.304296020508" />
                  <Point X="-21.88608984375" Y="1.295529663086" />
                  <Point X="-21.87514453125" Y="1.259993652344" />
                  <Point X="-21.863716796875" Y="1.235742797852" />
                  <Point X="-21.8258515625" Y="1.178190063477" />
                  <Point X="-21.81566015625" Y="1.162697265625" />
                  <Point X="-21.801109375" Y="1.145455200195" />
                  <Point X="-21.78386328125" Y="1.129362426758" />
                  <Point X="-21.76565234375" Y="1.116034667969" />
                  <Point X="-21.71078125" Y="1.085146850586" />
                  <Point X="-21.702224609375" Y="1.08088293457" />
                  <Point X="-21.669478515625" Y="1.066570922852" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.56969140625" Y="1.049633544922" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-21.211111328125" Y="1.0865703125" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.174107421875" Y="1.015143371582" />
                  <Point X="-20.154060546875" Y="0.932789733887" />
                  <Point X="-20.109134765625" Y="0.644238525391" />
                  <Point X="-20.25909765625" Y="0.604055908203" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.2952109375" Y="0.325585754395" />
                  <Point X="-21.318453125" Y="0.315067901611" />
                  <Point X="-21.391341796875" Y="0.272936950684" />
                  <Point X="-21.410962890625" Y="0.261595489502" />
                  <Point X="-21.4256875" Y="0.25109614563" />
                  <Point X="-21.452466796875" Y="0.225577148438" />
                  <Point X="-21.49619921875" Y="0.169850814819" />
                  <Point X="-21.50797265625" Y="0.154849517822" />
                  <Point X="-21.5196953125" Y="0.135571990967" />
                  <Point X="-21.529470703125" Y="0.11410610199" />
                  <Point X="-21.536318359375" Y="0.092603485107" />
                  <Point X="-21.550896484375" Y="0.016483804703" />
                  <Point X="-21.5548203125" Y="-0.004007134438" />
                  <Point X="-21.556515625" Y="-0.021876350403" />
                  <Point X="-21.5548203125" Y="-0.058553001404" />
                  <Point X="-21.5402421875" Y="-0.134672531128" />
                  <Point X="-21.536318359375" Y="-0.155163619995" />
                  <Point X="-21.529470703125" Y="-0.176666244507" />
                  <Point X="-21.5196953125" Y="-0.198132125854" />
                  <Point X="-21.50797265625" Y="-0.217409500122" />
                  <Point X="-21.464240234375" Y="-0.273135986328" />
                  <Point X="-21.458337890625" Y="-0.279963531494" />
                  <Point X="-21.43225390625" Y="-0.307383850098" />
                  <Point X="-21.410962890625" Y="-0.324155456543" />
                  <Point X="-21.33807421875" Y="-0.366286560059" />
                  <Point X="-21.318453125" Y="-0.37762802124" />
                  <Point X="-21.307291015625" Y="-0.383138305664" />
                  <Point X="-21.283419921875" Y="-0.392149963379" />
                  <Point X="-21.007677734375" Y="-0.466034667969" />
                  <Point X="-20.108525390625" Y="-0.706961914062" />
                  <Point X="-20.133783203125" Y="-0.874494018555" />
                  <Point X="-20.144974609375" Y="-0.94872467041" />
                  <Point X="-20.19882421875" Y="-1.18469934082" />
                  <Point X="-20.387693359375" Y="-1.159834228516" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710266113" />
                  <Point X="-21.62533984375" Y="-1.005508666992" />
                  <Point X="-21.76839453125" Y="-1.036602539062" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.8360234375" Y="-1.056596435547" />
                  <Point X="-21.863849609375" Y="-1.073488037109" />
                  <Point X="-21.8876015625" Y="-1.093959716797" />
                  <Point X="-21.974068359375" Y="-1.197953369141" />
                  <Point X="-21.997345703125" Y="-1.225947753906" />
                  <Point X="-22.012064453125" Y="-1.250329589844" />
                  <Point X="-22.023408203125" Y="-1.277715820312" />
                  <Point X="-22.030240234375" Y="-1.305365966797" />
                  <Point X="-22.0426328125" Y="-1.440042236328" />
                  <Point X="-22.04596875" Y="-1.476296386719" />
                  <Point X="-22.043650390625" Y="-1.507566772461" />
                  <Point X="-22.03591796875" Y="-1.539188232422" />
                  <Point X="-22.023546875" Y="-1.567997192383" />
                  <Point X="-21.94437890625" Y="-1.691138793945" />
                  <Point X="-21.92306640625" Y="-1.724287841797" />
                  <Point X="-21.913064453125" Y="-1.737239746094" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.63348046875" Y="-1.957260131836" />
                  <Point X="-20.786876953125" Y="-2.606881835938" />
                  <Point X="-20.843642578125" Y="-2.698736328125" />
                  <Point X="-20.875185546875" Y="-2.749775878906" />
                  <Point X="-20.971017578125" Y="-2.885944580078" />
                  <Point X="-21.141037109375" Y="-2.787784179688" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.245775390625" Y="-2.159825195312" />
                  <Point X="-22.416033203125" Y="-2.129076904297" />
                  <Point X="-22.461865234375" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395507812" />
                  <Point X="-22.525390625" Y="-2.125353515625" />
                  <Point X="-22.555166015625" Y="-2.135177001953" />
                  <Point X="-22.696609375" Y="-2.2096171875" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.757615234375" Y="-2.246549804688" />
                  <Point X="-22.77857421875" Y="-2.267509277344" />
                  <Point X="-22.795466796875" Y="-2.290438720703" />
                  <Point X="-22.869908203125" Y="-2.431881347656" />
                  <Point X="-22.889947265625" Y="-2.469956787109" />
                  <Point X="-22.899771484375" Y="-2.499735351563" />
                  <Point X="-22.904728515625" Y="-2.531908203125" />
                  <Point X="-22.90432421875" Y="-2.563259033203" />
                  <Point X="-22.87357421875" Y="-2.733517089844" />
                  <Point X="-22.865296875" Y="-2.779349365234" />
                  <Point X="-22.86101171875" Y="-2.795139648438" />
                  <Point X="-22.8481796875" Y="-2.826078369141" />
                  <Point X="-22.68374609375" Y="-3.110887695312" />
                  <Point X="-22.13871484375" Y="-4.054907226563" />
                  <Point X="-22.180708984375" Y="-4.084902587891" />
                  <Point X="-22.218154296875" Y="-4.111646972656" />
                  <Point X="-22.298232421875" Y="-4.163481445312" />
                  <Point X="-22.434095703125" Y="-3.986422851562" />
                  <Point X="-23.241451171875" Y="-2.934254882812" />
                  <Point X="-23.25249609375" Y="-2.922177490234" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.44599609375" Y="-2.792599853516" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.520009765625" Y="-2.751167236328" />
                  <Point X="-23.551630859375" Y="-2.743435546875" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.766548828125" Y="-2.758016357422" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397705078" />
                  <Point X="-23.871021484375" Y="-2.780740966797" />
                  <Point X="-23.89540234375" Y="-2.7954609375" />
                  <Point X="-24.037212890625" Y="-2.913370849609" />
                  <Point X="-24.07538671875" Y="-2.945111328125" />
                  <Point X="-24.095857421875" Y="-2.968861083984" />
                  <Point X="-24.11275" Y="-2.9966875" />
                  <Point X="-24.124375" Y="-3.02580859375" />
                  <Point X="-24.166775390625" Y="-3.220883300781" />
                  <Point X="-24.178189453125" Y="-3.273396484375" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.133658203125" Y="-3.677078613281" />
                  <Point X="-23.977935546875" Y="-4.859915527344" />
                  <Point X="-23.988904296875" Y="-4.8623203125" />
                  <Point X="-24.024318359375" Y="-4.870083007812" />
                  <Point X="-24.070681640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.00194140625" Y="-4.763600585938" />
                  <Point X="-26.05842578125" Y="-4.752636230469" />
                  <Point X="-26.14124609375" Y="-4.731328125" />
                  <Point X="-26.1207734375" Y="-4.575836914062" />
                  <Point X="-26.120076171875" Y="-4.568097167969" />
                  <Point X="-26.11944921875" Y="-4.541030273438" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497688964844" />
                  <Point X="-26.171025390625" Y="-4.257937011719" />
                  <Point X="-26.18386328125" Y="-4.193397460938" />
                  <Point X="-26.188126953125" Y="-4.178467773438" />
                  <Point X="-26.199029296875" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135463867188" />
                  <Point X="-26.221740234375" Y="-4.107626464844" />
                  <Point X="-26.23057421875" Y="-4.094861083984" />
                  <Point X="-26.25020703125" Y="-4.070937744141" />
                  <Point X="-26.261005859375" Y="-4.059779541016" />
                  <Point X="-26.444791015625" Y="-3.898602539062" />
                  <Point X="-26.494265625" Y="-3.85521484375" />
                  <Point X="-26.50673828125" Y="-3.845965332031" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.880740234375" Y="-3.780181884766" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.992732421875" Y="-3.779166503906" />
                  <Point X="-27.008009765625" Y="-3.781946533203" />
                  <Point X="-27.03905859375" Y="-3.790266601562" />
                  <Point X="-27.053677734375" Y="-3.795497558594" />
                  <Point X="-27.0818671875" Y="-3.808270507812" />
                  <Point X="-27.0954375" Y="-3.8158125" />
                  <Point X="-27.298689453125" Y="-3.95162109375" />
                  <Point X="-27.35340234375" Y="-3.988180175781" />
                  <Point X="-27.35968359375" Y="-3.992759765625" />
                  <Point X="-27.38044140625" Y="-4.010133056641" />
                  <Point X="-27.402755859375" Y="-4.032770996094" />
                  <Point X="-27.410466796875" Y="-4.041627441406" />
                  <Point X="-27.45816796875" Y="-4.103791015625" />
                  <Point X="-27.50319921875" Y="-4.162478515625" />
                  <Point X="-27.66478515625" Y="-4.062429443359" />
                  <Point X="-27.747591796875" Y="-4.011157714844" />
                  <Point X="-27.98086328125" Y="-3.831547119141" />
                  <Point X="-27.92803125" Y="-3.740039794922" />
                  <Point X="-27.341490234375" Y="-2.724120117188" />
                  <Point X="-27.3348515625" Y="-2.710084960938" />
                  <Point X="-27.32394921875" Y="-2.681119628906" />
                  <Point X="-27.319685546875" Y="-2.666188964844" />
                  <Point X="-27.3134140625" Y="-2.634661865234" />
                  <Point X="-27.311638671875" Y="-2.61923828125" />
                  <Point X="-27.310625" Y="-2.588302001953" />
                  <Point X="-27.314666015625" Y="-2.557610107422" />
                  <Point X="-27.32365234375" Y="-2.527990478516" />
                  <Point X="-27.329359375" Y="-2.513550048828" />
                  <Point X="-27.343578125" Y="-2.484720458984" />
                  <Point X="-27.35155859375" Y="-2.471407958984" />
                  <Point X="-27.369580078125" Y="-2.4462578125" />
                  <Point X="-27.37962109375" Y="-2.434420166016" />
                  <Point X="-27.39695703125" Y="-2.417082275391" />
                  <Point X="-27.40879296875" Y="-2.407038574219" />
                  <Point X="-27.433939453125" Y="-2.389015136719" />
                  <Point X="-27.44725" Y="-2.381035400391" />
                  <Point X="-27.47608203125" Y="-2.366810058594" />
                  <Point X="-27.4905234375" Y="-2.361100097656" />
                  <Point X="-27.52014453125" Y="-2.352109375" />
                  <Point X="-27.550833984375" Y="-2.348064697266" />
                  <Point X="-27.5817734375" Y="-2.34907421875" />
                  <Point X="-27.597203125" Y="-2.35084765625" />
                  <Point X="-27.628736328125" Y="-2.357117675781" />
                  <Point X="-27.643669921875" Y="-2.361381591797" />
                  <Point X="-27.672642578125" Y="-2.37228515625" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-27.960162109375" Y="-2.536818359375" />
                  <Point X="-28.7930859375" Y="-3.017707519531" />
                  <Point X="-28.9386015625" Y="-2.826532226562" />
                  <Point X="-29.00402734375" Y="-2.740572998047" />
                  <Point X="-29.181265625" Y="-2.443373779297" />
                  <Point X="-29.07437890625" Y="-2.361357421875" />
                  <Point X="-28.048123046875" Y="-1.573882324219" />
                  <Point X="-28.03648046875" Y="-1.563308227539" />
                  <Point X="-28.015103515625" Y="-1.540387451172" />
                  <Point X="-28.005369140625" Y="-1.528040771484" />
                  <Point X="-27.987404296875" Y="-1.500909301758" />
                  <Point X="-27.979837890625" Y="-1.487126464844" />
                  <Point X="-27.967080078125" Y="-1.458494262695" />
                  <Point X="-27.961888671875" Y="-1.443644897461" />
                  <Point X="-27.9558203125" Y="-1.420211547852" />
                  <Point X="-27.9558203125" Y="-1.420211425781" />
                  <Point X="-27.951564453125" Y="-1.3988671875" />
                  <Point X="-27.94875" Y="-1.368407958984" />
                  <Point X="-27.948578125" Y="-1.353063476562" />
                  <Point X="-27.95078515625" Y="-1.321390136719" />
                  <Point X="-27.953080078125" Y="-1.306233642578" />
                  <Point X="-27.960080078125" Y="-1.276485717773" />
                  <Point X="-27.9717734375" Y="-1.248252441406" />
                  <Point X="-27.98785546875" Y="-1.222266357422" />
                  <Point X="-27.99694921875" Y="-1.209922119141" />
                  <Point X="-28.017783203125" Y="-1.185963623047" />
                  <Point X="-28.028744140625" Y="-1.175244995117" />
                  <Point X="-28.052244140625" Y="-1.15571081543" />
                  <Point X="-28.064783203125" Y="-1.146895263672" />
                  <Point X="-28.0856484375" Y="-1.134614501953" />
                  <Point X="-28.10543359375" Y="-1.124480712891" />
                  <Point X="-28.13469921875" Y="-1.113256103516" />
                  <Point X="-28.149796875" Y="-1.108859375" />
                  <Point X="-28.18168359375" Y="-1.102378173828" />
                  <Point X="-28.197298828125" Y="-1.100532104492" />
                  <Point X="-28.22862109375" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196655273" />
                  <Point X="-28.5895703125" Y="-1.14564855957" />
                  <Point X="-29.66091796875" Y="-1.286694213867" />
                  <Point X="-29.71517578125" Y="-1.074282470703" />
                  <Point X="-29.7407578125" Y="-0.974116394043" />
                  <Point X="-29.786451171875" Y="-0.654654663086" />
                  <Point X="-29.67734765625" Y="-0.625420532227" />
                  <Point X="-28.508287109375" Y="-0.312171478271" />
                  <Point X="-28.500474609375" Y="-0.309712524414" />
                  <Point X="-28.473931640625" Y="-0.299251556396" />
                  <Point X="-28.444169921875" Y="-0.283895233154" />
                  <Point X="-28.433564453125" Y="-0.277516479492" />
                  <Point X="-28.411697265625" Y="-0.262339996338" />
                  <Point X="-28.393673828125" Y="-0.248245361328" />
                  <Point X="-28.371216796875" Y="-0.226360488892" />
                  <Point X="-28.360896484375" Y="-0.214484771729" />
                  <Point X="-28.34165234375" Y="-0.188229202271" />
                  <Point X="-28.333435546875" Y="-0.174813598633" />
                  <Point X="-28.319328125" Y="-0.146813354492" />
                  <Point X="-28.3134375" Y="-0.132228393555" />
                  <Point X="-28.3061484375" Y="-0.108743293762" />
                  <Point X="-28.300986328125" Y="-0.088487327576" />
                  <Point X="-28.296716796875" Y="-0.060314670563" />
                  <Point X="-28.29564453125" Y="-0.046067531586" />
                  <Point X="-28.2956484375" Y="-0.016433759689" />
                  <Point X="-28.29672265625" Y="-0.002200738907" />
                  <Point X="-28.3009921875" Y="0.025949626923" />
                  <Point X="-28.3041875" Y="0.03986712265" />
                  <Point X="-28.311474609375" Y="0.063343151093" />
                  <Point X="-28.311474609375" Y="0.063343044281" />
                  <Point X="-28.31930078125" Y="0.084183204651" />
                  <Point X="-28.333419921875" Y="0.112222099304" />
                  <Point X="-28.34164453125" Y="0.125651664734" />
                  <Point X="-28.360892578125" Y="0.151915863037" />
                  <Point X="-28.3712109375" Y="0.16379246521" />
                  <Point X="-28.393669921875" Y="0.185681503296" />
                  <Point X="-28.405810546875" Y="0.195693771362" />
                  <Point X="-28.427677734375" Y="0.210870407104" />
                  <Point X="-28.447431640625" Y="0.222869918823" />
                  <Point X="-28.468884765625" Y="0.234174926758" />
                  <Point X="-28.478521484375" Y="0.238584716797" />
                  <Point X="-28.49822265625" Y="0.246302703857" />
                  <Point X="-28.508287109375" Y="0.249611190796" />
                  <Point X="-28.822990234375" Y="0.333935974121" />
                  <Point X="-29.7854453125" Y="0.591824951172" />
                  <Point X="-29.747818359375" Y="0.84611114502" />
                  <Point X="-29.73133203125" Y="0.95752166748" />
                  <Point X="-29.6335859375" Y="1.318237182617" />
                  <Point X="-29.616078125" Y="1.315932250977" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.747056640625" Y="1.204364257812" />
                  <Point X="-28.736705078125" Y="1.204703125" />
                  <Point X="-28.71514453125" Y="1.206589355469" />
                  <Point X="-28.704890625" Y="1.208053100586" />
                  <Point X="-28.6846015625" Y="1.212088745117" />
                  <Point X="-28.67456640625" Y="1.214660522461" />
                  <Point X="-28.626169921875" Y="1.229920166016" />
                  <Point X="-28.603443359375" Y="1.237678222656" />
                  <Point X="-28.5845078125" Y="1.246011230469" />
                  <Point X="-28.57526953125" Y="1.250694091797" />
                  <Point X="-28.556525390625" Y="1.261517211914" />
                  <Point X="-28.547849609375" Y="1.267178955078" />
                  <Point X="-28.531171875" Y="1.279409423828" />
                  <Point X="-28.5159296875" Y="1.293375732422" />
                  <Point X="-28.502291015625" Y="1.308926025391" />
                  <Point X="-28.495892578125" Y="1.317078491211" />
                  <Point X="-28.4834765625" Y="1.334810791016" />
                  <Point X="-28.47800390625" Y="1.343611450195" />
                  <Point X="-28.468056640625" Y="1.361745605469" />
                  <Point X="-28.46358203125" Y="1.371079223633" />
                  <Point X="-28.444162109375" Y="1.417962890625" />
                  <Point X="-28.435498046875" Y="1.440356201172" />
                  <Point X="-28.42970703125" Y="1.460225219727" />
                  <Point X="-28.427353515625" Y="1.470322021484" />
                  <Point X="-28.42359765625" Y="1.491636962891" />
                  <Point X="-28.422359375" Y="1.501922607422" />
                  <Point X="-28.421009765625" Y="1.522557373047" />
                  <Point X="-28.421912109375" Y="1.543203857422" />
                  <Point X="-28.425056640625" Y="1.563642333984" />
                  <Point X="-28.4271875" Y="1.573783813477" />
                  <Point X="-28.4327890625" Y="1.594689331055" />
                  <Point X="-28.436013671875" Y="1.604538696289" />
                  <Point X="-28.44351171875" Y="1.62381640625" />
                  <Point X="-28.44778515625" Y="1.633244750977" />
                  <Point X="-28.471216796875" Y="1.678257568359" />
                  <Point X="-28.482798828125" Y="1.699284912109" />
                  <Point X="-28.49429296875" Y="1.716488769531" />
                  <Point X="-28.500513671875" Y="1.724782348633" />
                  <Point X="-28.51442578125" Y="1.741360107422" />
                  <Point X="-28.521509765625" Y="1.748920898438" />
                  <Point X="-28.53644921875" Y="1.763219970703" />
                  <Point X="-28.5443046875" Y="1.769958251953" />
                  <Point X="-28.724818359375" Y="1.90847253418" />
                  <Point X="-29.22761328125" Y="2.294280517578" />
                  <Point X="-29.0663515625" Y="2.570560546875" />
                  <Point X="-29.002283203125" Y="2.680322998047" />
                  <Point X="-28.72633984375" Y="3.035013427734" />
                  <Point X="-28.254158203125" Y="2.762399658203" />
                  <Point X="-28.24491796875" Y="2.757716796875" />
                  <Point X="-28.225984375" Y="2.749385742188" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.08766796875" Y="2.725260742188" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310546875" />
                  <Point X="-27.976431640625" Y="2.734227294922" />
                  <Point X="-27.95698828125" Y="2.7413046875" />
                  <Point X="-27.9384375" Y="2.750454345703" />
                  <Point X="-27.929416015625" Y="2.755533691406" />
                  <Point X="-27.911162109375" Y="2.767163330078" />
                  <Point X="-27.902744140625" Y="2.773194580078" />
                  <Point X="-27.886615234375" Y="2.786139160156" />
                  <Point X="-27.878904296875" Y="2.793052490234" />
                  <Point X="-27.83105859375" Y="2.840896728516" />
                  <Point X="-27.811263671875" Y="2.861489746094" />
                  <Point X="-27.798314453125" Y="2.877624267578" />
                  <Point X="-27.79228125" Y="2.886044921875" />
                  <Point X="-27.78065234375" Y="2.904299560547" />
                  <Point X="-27.7755703125" Y="2.913326416016" />
                  <Point X="-27.766423828125" Y="2.931875" />
                  <Point X="-27.759353515625" Y="2.951299804688" />
                  <Point X="-27.754435546875" Y="2.971388427734" />
                  <Point X="-27.7525234375" Y="2.981573974609" />
                  <Point X="-27.749697265625" Y="3.003032958984" />
                  <Point X="-27.748908203125" Y="3.013365966797" />
                  <Point X="-27.74845703125" Y="3.034052001953" />
                  <Point X="-27.748794921875" Y="3.044405029297" />
                  <Point X="-27.754693359375" Y="3.111809570312" />
                  <Point X="-27.757744140625" Y="3.140202148438" />
                  <Point X="-27.761779296875" Y="3.160487548828" />
                  <Point X="-27.764349609375" Y="3.170520263672" />
                  <Point X="-27.770859375" Y="3.191166748047" />
                  <Point X="-27.774509765625" Y="3.200865234375" />
                  <Point X="-27.782841796875" Y="3.219798583984" />
                  <Point X="-27.7875234375" Y="3.229033447266" />
                  <Point X="-27.867515625" Y="3.367583251953" />
                  <Point X="-28.05938671875" Y="3.699914550781" />
                  <Point X="-27.7599375" Y="3.929499023438" />
                  <Point X="-27.648365234375" Y="4.015041748047" />
                  <Point X="-27.192525390625" Y="4.268296386719" />
                  <Point X="-27.11856640625" Y="4.171910644531" />
                  <Point X="-27.111822265625" Y="4.16405078125" />
                  <Point X="-27.097517578125" Y="4.149107421875" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.06508984375" Y="4.121895996094" />
                  <Point X="-27.047888671875" Y="4.110403320312" />
                  <Point X="-27.0389765625" Y="4.105128417969" />
                  <Point X="-26.963955078125" Y="4.066074707031" />
                  <Point X="-26.943759765625" Y="4.055561523438" />
                  <Point X="-26.934326171875" Y="4.051286865234" />
                  <Point X="-26.915048828125" Y="4.043790283203" />
                  <Point X="-26.905205078125" Y="4.040568359375" />
                  <Point X="-26.884298828125" Y="4.034966308594" />
                  <Point X="-26.8741640625" Y="4.032834960938" />
                  <Point X="-26.853720703125" Y="4.029687988281" />
                  <Point X="-26.8330546875" Y="4.028785400391" />
                  <Point X="-26.812416015625" Y="4.030137939453" />
                  <Point X="-26.802134765625" Y="4.031377685547" />
                  <Point X="-26.780818359375" Y="4.035135986328" />
                  <Point X="-26.77073046875" Y="4.037487548828" />
                  <Point X="-26.750869140625" Y="4.043276123047" />
                  <Point X="-26.741095703125" Y="4.046713134766" />
                  <Point X="-26.66295703125" Y="4.079079833984" />
                  <Point X="-26.641921875" Y="4.087792724609" />
                  <Point X="-26.632583984375" Y="4.092271240234" />
                  <Point X="-26.61444921875" Y="4.102220214844" />
                  <Point X="-26.60565234375" Y="4.107689941406" />
                  <Point X="-26.587923828125" Y="4.120103515625" />
                  <Point X="-26.5797734375" Y="4.126500488281" />
                  <Point X="-26.564224609375" Y="4.140137207031" />
                  <Point X="-26.55025390625" Y="4.155383300781" />
                  <Point X="-26.5380234375" Y="4.1720625" />
                  <Point X="-26.532365234375" Y="4.180735351563" />
                  <Point X="-26.52154296875" Y="4.199478027344" />
                  <Point X="-26.516859375" Y="4.208716796875" />
                  <Point X="-26.508525390625" Y="4.227654785156" />
                  <Point X="-26.504875" Y="4.237354003906" />
                  <Point X="-26.479443359375" Y="4.318016601563" />
                  <Point X="-26.472595703125" Y="4.33973046875" />
                  <Point X="-26.470025390625" Y="4.349762207031" />
                  <Point X="-26.465990234375" Y="4.370047851562" />
                  <Point X="-26.464525390625" Y="4.380301757812" />
                  <Point X="-26.462638671875" Y="4.401865722656" />
                  <Point X="-26.46230078125" Y="4.412217773438" />
                  <Point X="-26.462751953125" Y="4.432899902344" />
                  <Point X="-26.463541015625" Y="4.443229980469" />
                  <Point X="-26.47263671875" Y="4.512307128906" />
                  <Point X="-26.479265625" Y="4.562655273438" />
                  <Point X="-26.0756015625" Y="4.675828613281" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.36522265625" Y="4.782557128906" />
                  <Point X="-25.22566796875" Y="4.26173046875" />
                  <Point X="-25.220435546875" Y="4.247107910156" />
                  <Point X="-25.20766015625" Y="4.218913085938" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218911621094" />
                  <Point X="-24.767251953125" Y="4.247104980469" />
                  <Point X="-24.76201953125" Y="4.2617265625" />
                  <Point X="-24.72103125" Y="4.414691894531" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.298240234375" Y="4.751120117188" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.6721796875" Y="4.6172109375" />
                  <Point X="-23.546404296875" Y="4.586843261719" />
                  <Point X="-23.2222734375" Y="4.469280273438" />
                  <Point X="-23.141736328125" Y="4.440068847656" />
                  <Point X="-22.827078125" Y="4.292913085938" />
                  <Point X="-22.74955078125" Y="4.256655273438" />
                  <Point X="-22.44551953125" Y="4.079527099609" />
                  <Point X="-22.3705703125" Y="4.035861572266" />
                  <Point X="-22.18221875" Y="3.901915527344" />
                  <Point X="-22.255810546875" Y="3.77444921875" />
                  <Point X="-22.9346875" Y="2.598598144531" />
                  <Point X="-22.93762109375" Y="2.593113037109" />
                  <Point X="-22.946810546875" Y="2.573455322266" />
                  <Point X="-22.955814453125" Y="2.54957421875" />
                  <Point X="-22.958697265625" Y="2.540601318359" />
                  <Point X="-22.97561328125" Y="2.477343505859" />
                  <Point X="-22.97955859375" Y="2.459006591797" />
                  <Point X="-22.985451171875" Y="2.423073486328" />
                  <Point X="-22.986681640625" Y="2.409718261719" />
                  <Point X="-22.98725" Y="2.382971923828" />
                  <Point X="-22.986587890625" Y="2.369580810547" />
                  <Point X="-22.9799921875" Y="2.314880859375" />
                  <Point X="-22.97619921875" Y="2.289036376953" />
                  <Point X="-22.970857421875" Y="2.267115966797" />
                  <Point X="-22.967533203125" Y="2.256315185547" />
                  <Point X="-22.959267578125" Y="2.234225585938" />
                  <Point X="-22.954685546875" Y="2.223896728516" />
                  <Point X="-22.944322265625" Y="2.20384765625" />
                  <Point X="-22.938541015625" Y="2.194127441406" />
                  <Point X="-22.904693359375" Y="2.144246337891" />
                  <Point X="-22.893060546875" Y="2.128848876953" />
                  <Point X="-22.8706484375" Y="2.102121337891" />
                  <Point X="-22.861529296875" Y="2.092661621094" />
                  <Point X="-22.842076171875" Y="2.075091552734" />
                  <Point X="-22.8317421875" Y="2.066981201172" />
                  <Point X="-22.781861328125" Y="2.033134765625" />
                  <Point X="-22.758716796875" Y="2.018243774414" />
                  <Point X="-22.738669921875" Y="2.007880737305" />
                  <Point X="-22.72833984375" Y="2.003297485352" />
                  <Point X="-22.706251953125" Y="1.995031860352" />
                  <Point X="-22.695451171875" Y="1.991707397461" />
                  <Point X="-22.673529296875" Y="1.986365112305" />
                  <Point X="-22.662408203125" Y="1.984346801758" />
                  <Point X="-22.607708984375" Y="1.977750854492" />
                  <Point X="-22.587439453125" Y="1.976400390625" />
                  <Point X="-22.5538125" Y="1.975962158203" />
                  <Point X="-22.540810546875" Y="1.976685302734" />
                  <Point X="-22.51503125" Y="1.979902099609" />
                  <Point X="-22.50225390625" Y="1.982395629883" />
                  <Point X="-22.43899609375" Y="1.999311645508" />
                  <Point X="-22.428828125" Y="2.002336914062" />
                  <Point X="-22.39971484375" Y="2.011892578125" />
                  <Point X="-22.390572265625" Y="2.015425170898" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-22.04743359375" Y="2.210622314453" />
                  <Point X="-21.059595703125" Y="2.780950683594" />
                  <Point X="-21.000509765625" Y="2.698835205078" />
                  <Point X="-20.956033203125" Y="2.637024902344" />
                  <Point X="-20.86311328125" Y="2.483471435547" />
                  <Point X="-20.933240234375" Y="2.429661376953" />
                  <Point X="-21.827046875" Y="1.743819824219" />
                  <Point X="-21.831861328125" Y="1.739869018555" />
                  <Point X="-21.8478828125" Y="1.725214233398" />
                  <Point X="-21.86533203125" Y="1.7066015625" />
                  <Point X="-21.871423828125" Y="1.699420898438" />
                  <Point X="-21.91694921875" Y="1.640027832031" />
                  <Point X="-21.929205078125" Y="1.624039428711" />
                  <Point X="-21.934359375" Y="1.616607055664" />
                  <Point X="-21.95025" Y="1.589389038086" />
                  <Point X="-21.965232421875" Y="1.555556274414" />
                  <Point X="-21.969859375" Y="1.542676025391" />
                  <Point X="-21.986818359375" Y="1.482035400391" />
                  <Point X="-21.993775390625" Y="1.454663696289" />
                  <Point X="-21.997228515625" Y="1.432365356445" />
                  <Point X="-21.998291015625" Y="1.421114868164" />
                  <Point X="-21.999107421875" Y="1.397540039063" />
                  <Point X="-21.998826171875" Y="1.386235351562" />
                  <Point X="-21.996921875" Y="1.363749511719" />
                  <Point X="-21.995298828125" Y="1.352568237305" />
                  <Point X="-21.981376953125" Y="1.285098022461" />
                  <Point X="-21.976880859375" Y="1.267565429688" />
                  <Point X="-21.965935546875" Y="1.232029418945" />
                  <Point X="-21.961080078125" Y="1.219497802734" />
                  <Point X="-21.94965234375" Y="1.195246948242" />
                  <Point X="-21.943080078125" Y="1.183527832031" />
                  <Point X="-21.90521484375" Y="1.125975097656" />
                  <Point X="-21.88826171875" Y="1.101427612305" />
                  <Point X="-21.8737109375" Y="1.084185546875" />
                  <Point X="-21.865921875" Y="1.075997802734" />
                  <Point X="-21.84867578125" Y="1.059905029297" />
                  <Point X="-21.83996875" Y="1.052699951172" />
                  <Point X="-21.8217578125" Y="1.039372192383" />
                  <Point X="-21.81225390625" Y="1.033249755859" />
                  <Point X="-21.7573828125" Y="1.002361816406" />
                  <Point X="-21.74026953125" Y="0.993833984375" />
                  <Point X="-21.7075234375" Y="0.979521911621" />
                  <Point X="-21.6949765625" Y="0.97505682373" />
                  <Point X="-21.66937890625" Y="0.967924560547" />
                  <Point X="-21.656328125" Y="0.965257629395" />
                  <Point X="-21.582138671875" Y="0.955452575684" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.555966796875" Y="0.952199829102" />
                  <Point X="-21.53428125" Y="0.951222900391" />
                  <Point X="-21.50878125" Y="0.952032287598" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-21.1987109375" Y="0.992383117676" />
                  <Point X="-20.295296875" Y="1.111319824219" />
                  <Point X="-20.266412109375" Y="0.992672363281" />
                  <Point X="-20.2473125" Y="0.914208251953" />
                  <Point X="-20.21612890625" Y="0.713920837402" />
                  <Point X="-20.283685546875" Y="0.695818908691" />
                  <Point X="-21.3080078125" Y="0.42135269165" />
                  <Point X="-21.313966796875" Y="0.419544525146" />
                  <Point X="-21.334376953125" Y="0.412136138916" />
                  <Point X="-21.357619140625" Y="0.401618225098" />
                  <Point X="-21.365994140625" Y="0.397316497803" />
                  <Point X="-21.4388828125" Y="0.35518560791" />
                  <Point X="-21.45850390625" Y="0.343844055176" />
                  <Point X="-21.4661171875" Y="0.33894543457" />
                  <Point X="-21.491224609375" Y="0.319870147705" />
                  <Point X="-21.51800390625" Y="0.294351135254" />
                  <Point X="-21.527201171875" Y="0.284226654053" />
                  <Point X="-21.57093359375" Y="0.228500350952" />
                  <Point X="-21.589142578125" Y="0.204209121704" />
                  <Point X="-21.600865234375" Y="0.184931686401" />
                  <Point X="-21.60615234375" Y="0.174943939209" />
                  <Point X="-21.615927734375" Y="0.153478027344" />
                  <Point X="-21.6199921875" Y="0.14293309021" />
                  <Point X="-21.62683984375" Y="0.121430480957" />
                  <Point X="-21.629623046875" Y="0.110472663879" />
                  <Point X="-21.644201171875" Y="0.034353065491" />
                  <Point X="-21.648125" Y="0.013862127304" />
                  <Point X="-21.649396484375" Y="0.004965560436" />
                  <Point X="-21.6514140625" Y="-0.026262935638" />
                  <Point X="-21.64971875" Y="-0.062939498901" />
                  <Point X="-21.648125" Y="-0.076422271729" />
                  <Point X="-21.633546875" Y="-0.152541870117" />
                  <Point X="-21.629623046875" Y="-0.173032958984" />
                  <Point X="-21.62683984375" Y="-0.183990631104" />
                  <Point X="-21.6199921875" Y="-0.205493240356" />
                  <Point X="-21.615927734375" Y="-0.21603817749" />
                  <Point X="-21.60615234375" Y="-0.237504089355" />
                  <Point X="-21.600865234375" Y="-0.247492141724" />
                  <Point X="-21.589142578125" Y="-0.26676940918" />
                  <Point X="-21.58270703125" Y="-0.276058807373" />
                  <Point X="-21.538974609375" Y="-0.331785400391" />
                  <Point X="-21.527169921875" Y="-0.345440429688" />
                  <Point X="-21.5010859375" Y="-0.372860748291" />
                  <Point X="-21.4910390625" Y="-0.382010864258" />
                  <Point X="-21.469748046875" Y="-0.398782531738" />
                  <Point X="-21.45850390625" Y="-0.406404052734" />
                  <Point X="-21.385615234375" Y="-0.448535064697" />
                  <Point X="-21.365994140625" Y="-0.459876647949" />
                  <Point X="-21.360505859375" Y="-0.462813568115" />
                  <Point X="-21.34084375" Y="-0.472015869141" />
                  <Point X="-21.31697265625" Y="-0.481027618408" />
                  <Point X="-21.3080078125" Y="-0.483912963867" />
                  <Point X="-21.032265625" Y="-0.557797607422" />
                  <Point X="-20.215119140625" Y="-0.776751220703" />
                  <Point X="-20.227720703125" Y="-0.860331542969" />
                  <Point X="-20.238380859375" Y="-0.931036437988" />
                  <Point X="-20.2721953125" Y="-1.079219970703" />
                  <Point X="-20.37529296875" Y="-1.065646972656" />
                  <Point X="-21.563216796875" Y="-0.909253601074" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042480469" />
                  <Point X="-21.63327734375" Y="-0.910840881348" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.788572265625" Y="-0.943770202637" />
                  <Point X="-21.82708203125" Y="-0.952140075684" />
                  <Point X="-21.842125" Y="-0.956742492676" />
                  <Point X="-21.871244140625" Y="-0.968366271973" />
                  <Point X="-21.8853203125" Y="-0.975387939453" />
                  <Point X="-21.913146484375" Y="-0.992279541016" />
                  <Point X="-21.92587109375" Y="-1.001527893066" />
                  <Point X="-21.949623046875" Y="-1.021999511719" />
                  <Point X="-21.960650390625" Y="-1.033222900391" />
                  <Point X="-22.0471171875" Y="-1.137216552734" />
                  <Point X="-22.07039453125" Y="-1.1652109375" />
                  <Point X="-22.07867578125" Y="-1.176850952148" />
                  <Point X="-22.09339453125" Y="-1.201232788086" />
                  <Point X="-22.09983203125" Y="-1.213974731445" />
                  <Point X="-22.11117578125" Y="-1.241360839844" />
                  <Point X="-22.115634765625" Y="-1.254927734375" />
                  <Point X="-22.122466796875" Y="-1.282577758789" />
                  <Point X="-22.12483984375" Y="-1.296661132812" />
                  <Point X="-22.137232421875" Y="-1.431337402344" />
                  <Point X="-22.140568359375" Y="-1.467591430664" />
                  <Point X="-22.140708984375" Y="-1.48332019043" />
                  <Point X="-22.138390625" Y="-1.514590698242" />
                  <Point X="-22.135931640625" Y="-1.530132324219" />
                  <Point X="-22.12819921875" Y="-1.56175378418" />
                  <Point X="-22.123208984375" Y="-1.576672973633" />
                  <Point X="-22.110837890625" Y="-1.605481933594" />
                  <Point X="-22.10345703125" Y="-1.619371704102" />
                  <Point X="-22.0242890625" Y="-1.742513183594" />
                  <Point X="-22.0029765625" Y="-1.775662353516" />
                  <Point X="-21.998255859375" Y="-1.782352294922" />
                  <Point X="-21.980203125" Y="-1.804451538086" />
                  <Point X="-21.9565078125" Y="-1.82812097168" />
                  <Point X="-21.947201171875" Y="-1.836277587891" />
                  <Point X="-21.6913125" Y="-2.032628662109" />
                  <Point X="-20.912828125" Y="-2.62998046875" />
                  <Point X="-20.924455078125" Y="-2.648794189453" />
                  <Point X="-20.954498046875" Y="-2.697406738281" />
                  <Point X="-20.9987265625" Y="-2.760250732422" />
                  <Point X="-21.093537109375" Y="-2.70551171875" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.18497265625" Y="-2.079513427734" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228892578125" Y="-2.066337646484" />
                  <Point X="-22.399150390625" Y="-2.035589233398" />
                  <Point X="-22.444982421875" Y="-2.027311889648" />
                  <Point X="-22.460640625" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403442383" />
                  <Point X="-22.507685546875" Y="-2.02650378418" />
                  <Point X="-22.539859375" Y="-2.031461914062" />
                  <Point X="-22.555154296875" Y="-2.03513659668" />
                  <Point X="-22.5849296875" Y="-2.044960083008" />
                  <Point X="-22.59941015625" Y="-2.051108886719" />
                  <Point X="-22.740853515625" Y="-2.125549072266" />
                  <Point X="-22.778927734375" Y="-2.145587890625" />
                  <Point X="-22.79103125" Y="-2.153170654297" />
                  <Point X="-22.813962890625" Y="-2.170064453125" />
                  <Point X="-22.824791015625" Y="-2.179375488281" />
                  <Point X="-22.84575" Y="-2.200334960938" />
                  <Point X="-22.85505859375" Y="-2.211161376953" />
                  <Point X="-22.871951171875" Y="-2.234090820312" />
                  <Point X="-22.87953515625" Y="-2.246193847656" />
                  <Point X="-22.9539765625" Y="-2.387636474609" />
                  <Point X="-22.974015625" Y="-2.425711914062" />
                  <Point X="-22.9801640625" Y="-2.440193359375" />
                  <Point X="-22.98998828125" Y="-2.469971923828" />
                  <Point X="-22.9936640625" Y="-2.485269042969" />
                  <Point X="-22.99862109375" Y="-2.517441894531" />
                  <Point X="-22.999720703125" Y="-2.533133300781" />
                  <Point X="-22.99931640625" Y="-2.564484130859" />
                  <Point X="-22.9978125" Y="-2.580143554688" />
                  <Point X="-22.9670625" Y="-2.750401611328" />
                  <Point X="-22.95878515625" Y="-2.796233886719" />
                  <Point X="-22.95698046875" Y="-2.80423046875" />
                  <Point X="-22.948763671875" Y="-2.83153515625" />
                  <Point X="-22.935931640625" Y="-2.862473876953" />
                  <Point X="-22.930453125" Y="-2.873578125" />
                  <Point X="-22.76601953125" Y="-3.158387451172" />
                  <Point X="-22.26410546875" Y="-4.027724609375" />
                  <Point X="-22.276244140625" Y="-4.036082519531" />
                  <Point X="-22.3587265625" Y="-3.928590087891" />
                  <Point X="-23.16608203125" Y="-2.876422119141" />
                  <Point X="-23.171345703125" Y="-2.870143066406" />
                  <Point X="-23.191169921875" Y="-2.849624023438" />
                  <Point X="-23.216748046875" Y="-2.828003662109" />
                  <Point X="-23.22669921875" Y="-2.820646728516" />
                  <Point X="-23.39462109375" Y="-2.712689453125" />
                  <Point X="-23.43982421875" Y="-2.683627929688" />
                  <Point X="-23.453716796875" Y="-2.676245605469" />
                  <Point X="-23.48252734375" Y="-2.663874511719" />
                  <Point X="-23.4974453125" Y="-2.658885742188" />
                  <Point X="-23.52906640625" Y="-2.651154052734" />
                  <Point X="-23.54460546875" Y="-2.648695800781" />
                  <Point X="-23.575875" Y="-2.646376953125" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.77525390625" Y="-2.663416015625" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670339355469" />
                  <Point X="-23.866423828125" Y="-2.677171386719" />
                  <Point X="-23.8799921875" Y="-2.681629394531" />
                  <Point X="-23.907376953125" Y="-2.69297265625" />
                  <Point X="-23.920123046875" Y="-2.6994140625" />
                  <Point X="-23.94450390625" Y="-2.714134033203" />
                  <Point X="-23.956138671875" Y="-2.722412597656" />
                  <Point X="-24.09794921875" Y="-2.840322509766" />
                  <Point X="-24.136123046875" Y="-2.872062988281" />
                  <Point X="-24.147345703125" Y="-2.883087646484" />
                  <Point X="-24.16781640625" Y="-2.906837402344" />
                  <Point X="-24.177064453125" Y="-2.9195625" />
                  <Point X="-24.19395703125" Y="-2.947388916016" />
                  <Point X="-24.20098046875" Y="-2.961466552734" />
                  <Point X="-24.21260546875" Y="-2.990587646484" />
                  <Point X="-24.21720703125" Y="-3.005631103516" />
                  <Point X="-24.259607421875" Y="-3.200705810547" />
                  <Point X="-24.271021484375" Y="-3.253218994141" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520019531" />
                  <Point X="-24.227845703125" Y="-3.689478515625" />
                  <Point X="-24.166912109375" Y="-4.152311035156" />
                  <Point X="-24.344931640625" Y="-3.4879375" />
                  <Point X="-24.347392578125" Y="-3.480123779297" />
                  <Point X="-24.357853515625" Y="-3.453578613281" />
                  <Point X="-24.3732109375" Y="-3.423815429688" />
                  <Point X="-24.37958984375" Y="-3.413209716797" />
                  <Point X="-24.508591796875" Y="-3.227343261719" />
                  <Point X="-24.543318359375" Y="-3.177309326172" />
                  <Point X="-24.553330078125" Y="-3.165171386719" />
                  <Point X="-24.57521484375" Y="-3.142716064453" />
                  <Point X="-24.587087890625" Y="-3.132398681641" />
                  <Point X="-24.61334375" Y="-3.113154541016" />
                  <Point X="-24.6267578125" Y="-3.104937744141" />
                  <Point X="-24.6547578125" Y="-3.090829833984" />
                  <Point X="-24.66934375" Y="-3.084938720703" />
                  <Point X="-24.86896484375" Y="-3.022983398438" />
                  <Point X="-24.922703125" Y="-3.006305419922" />
                  <Point X="-24.936623046875" Y="-3.003109375" />
                  <Point X="-24.964783203125" Y="-2.99883984375" />
                  <Point X="-24.9790234375" Y="-2.997766357422" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.26460546875" Y="-3.068260253906" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.3329296875" Y="-3.090829589844" />
                  <Point X="-25.3609296875" Y="-3.104937255859" />
                  <Point X="-25.37434375" Y="-3.113153808594" />
                  <Point X="-25.400599609375" Y="-3.132397705078" />
                  <Point X="-25.412474609375" Y="-3.142717041016" />
                  <Point X="-25.434359375" Y="-3.165173339844" />
                  <Point X="-25.444369140625" Y="-3.177310302734" />
                  <Point X="-25.573369140625" Y="-3.363176513672" />
                  <Point X="-25.608095703125" Y="-3.413210693359" />
                  <Point X="-25.612470703125" Y="-3.420132568359" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213378906" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.727716796875" Y="-3.805021484375" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.970582083062" Y="-2.720260434853" />
                  <Point X="-20.932288023255" Y="-2.615048370235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.368669416719" Y="-1.066518975591" />
                  <Point X="-20.258929912784" Y="-0.765012166509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.071038671778" Y="-2.71850122597" />
                  <Point X="-21.011314168844" Y="-2.55440950277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.465143494879" Y="-1.053817908884" />
                  <Point X="-20.351043390616" Y="-0.740330448862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.154580261777" Y="-2.670268440061" />
                  <Point X="-21.090340314432" Y="-2.493770635305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.561617571119" Y="-1.041116836899" />
                  <Point X="-20.443156868447" Y="-0.715648731216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.23812186046" Y="-2.622035678013" />
                  <Point X="-21.169366460021" Y="-2.43313176784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.658091647358" Y="-1.028415764914" />
                  <Point X="-20.535270346278" Y="-0.690967013569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.321663459143" Y="-2.573802915964" />
                  <Point X="-21.248392605609" Y="-2.372492900376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.754565723598" Y="-1.01571469293" />
                  <Point X="-20.62738382411" Y="-0.666285295923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.405205057826" Y="-2.525570153916" />
                  <Point X="-21.327418751197" Y="-2.311854032911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.851039799837" Y="-1.003013620945" />
                  <Point X="-20.719497301941" Y="-0.641603578277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.227207189045" Y="0.710952390725" />
                  <Point X="-20.219124317039" Y="0.733159899047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.488746656509" Y="-2.477337391868" />
                  <Point X="-21.406444896786" Y="-2.251215165446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.947513876077" Y="-0.99031254896" />
                  <Point X="-20.811610779772" Y="-0.61692186063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.339229129695" Y="0.680936056322" />
                  <Point X="-20.250122842546" Y="0.925753568197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.572288255192" Y="-2.429104629819" />
                  <Point X="-21.485471042374" Y="-2.190576297981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.043987952316" Y="-0.977611476976" />
                  <Point X="-20.903724257604" Y="-0.592240142984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.451251006803" Y="0.650919896499" />
                  <Point X="-20.290640558803" Y="1.092193475706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.655829853876" Y="-2.380871867771" />
                  <Point X="-21.564497187963" Y="-2.129937430516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.140462028556" Y="-0.964910404991" />
                  <Point X="-20.995837735435" Y="-0.567558425338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.563272883911" Y="0.620903736676" />
                  <Point X="-20.389279443436" Y="1.098946785513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.332629008371" Y="-3.962600844237" />
                  <Point X="-22.320671924961" Y="-3.929749027565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.739371452559" Y="-2.332639105722" />
                  <Point X="-21.643523333551" Y="-2.069298563051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.236936104795" Y="-0.952209333006" />
                  <Point X="-21.087951227327" Y="-0.542876746322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.675294761019" Y="0.590887576852" />
                  <Point X="-20.495464461548" Y="1.084967263982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.401200210452" Y="-3.873237255563" />
                  <Point X="-22.382678867705" Y="-3.82235028459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.822913051242" Y="-2.284406343674" />
                  <Point X="-21.722549448675" Y="-2.008659611886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.333410181035" Y="-0.939508261021" />
                  <Point X="-21.180064728417" Y="-0.518195092578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.787316638126" Y="0.560871417029" />
                  <Point X="-20.60164947966" Y="1.070987742451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.469771339028" Y="-3.783873464938" />
                  <Point X="-22.44468581045" Y="-3.714951541614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.906454649925" Y="-2.236173581625" />
                  <Point X="-21.801575517192" Y="-1.948020532669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.429884257274" Y="-0.926807189037" />
                  <Point X="-21.272178229507" Y="-0.493513438833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.899338515234" Y="0.530855257206" />
                  <Point X="-20.707834497772" Y="1.057008220919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.538342467605" Y="-3.694509674313" />
                  <Point X="-22.506692753194" Y="-3.607552798638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.989996248608" Y="-2.187940819577" />
                  <Point X="-21.880601585709" Y="-1.887381453452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.526358333514" Y="-0.914106117052" />
                  <Point X="-21.361841195571" Y="-0.462098995442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.011360392342" Y="0.500839097382" />
                  <Point X="-20.814019515884" Y="1.043028699388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.606913596181" Y="-3.605145883688" />
                  <Point X="-22.568699695939" Y="-3.500154055662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.073537847291" Y="-2.139708057528" />
                  <Point X="-21.959163783905" Y="-1.825467901002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.6260460907" Y="-0.910234560901" />
                  <Point X="-21.445419500606" Y="-0.413967083265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.12338226945" Y="0.470822937559" />
                  <Point X="-20.920204533995" Y="1.029049177857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.675484724758" Y="-3.515782093063" />
                  <Point X="-22.630706638683" Y="-3.392755312687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.157174436913" Y="-2.091736280939" />
                  <Point X="-22.027978756146" Y="-1.736774065341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.735120281296" Y="-0.932152018594" />
                  <Point X="-21.523123107443" Y="-0.349694570445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.235404146558" Y="0.440806777736" />
                  <Point X="-21.026389552107" Y="1.015069656325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.744055853334" Y="-3.426418302438" />
                  <Point X="-22.692713581428" Y="-3.285356569711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.247785120397" Y="-2.062925669756" />
                  <Point X="-22.092530552198" Y="-1.636367249363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.845684533688" Y="-0.958163387424" />
                  <Point X="-21.592204847782" Y="-0.261733674108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.349412847774" Y="0.40533186354" />
                  <Point X="-21.132574570219" Y="1.001090134794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.812626981911" Y="-3.337054511813" />
                  <Point X="-22.754720524172" Y="-3.177957826735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.342646512567" Y="-2.045793784708" />
                  <Point X="-22.140237504645" Y="-1.489679605946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.984572830494" Y="-1.061994428709" />
                  <Point X="-21.640446551502" Y="-0.116515247739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.477942386661" Y="0.329961275728" />
                  <Point X="-21.238759595158" Y="0.987110594507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.881198110487" Y="-3.247690721188" />
                  <Point X="-22.816727251189" Y="-3.070558491054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.437507870809" Y="-2.028661806439" />
                  <Point X="-21.34494463137" Y="0.973131023245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.949769239064" Y="-3.158326930563" />
                  <Point X="-22.878733930137" Y="-2.963159023302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.539609923425" Y="-2.031423472466" />
                  <Point X="-21.451129667582" Y="0.959151451984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.909191396145" Y="2.448114615496" />
                  <Point X="-20.883849984173" Y="2.517739572665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.01834036764" Y="-3.068963139938" />
                  <Point X="-22.939794533357" Y="-2.853160233852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.659354231549" Y="-2.082656837131" />
                  <Point X="-21.554776260726" Y="0.952146195732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.049464214271" Y="2.340479633147" />
                  <Point X="-20.946977394246" Y="2.622059856958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.086911496217" Y="-2.979599349314" />
                  <Point X="-22.979143710648" Y="-2.683509791919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.784664207711" Y="-2.149181749051" />
                  <Point X="-21.651340879821" Y="0.964598503268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.189737069351" Y="2.232844549269" />
                  <Point X="-21.013539485079" Y="2.716943433417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.155482624793" Y="-2.890235558689" />
                  <Point X="-21.741562427579" Y="0.99447825607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.33000992443" Y="2.125209465391" />
                  <Point X="-21.099784935601" Y="2.757747423593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.230388069835" Y="-2.81827515952" />
                  <Point X="-21.825359789068" Y="1.042008315584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.470282779509" Y="2.017574381513" />
                  <Point X="-21.227778083212" Y="2.683850558701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.312314426796" Y="-2.765604557312" />
                  <Point X="-21.899111711415" Y="1.117137992309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.610555634589" Y="1.909939297635" />
                  <Point X="-21.355771230824" Y="2.609953693809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.394240783757" Y="-2.712933955104" />
                  <Point X="-21.96204564263" Y="1.221989855393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.750828489668" Y="1.802304213758" />
                  <Point X="-21.483764378435" Y="2.536056828918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.4781635056" Y="-2.665748320331" />
                  <Point X="-21.999092693616" Y="1.397965337369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.905600151888" Y="1.654833984654" />
                  <Point X="-21.611757526046" Y="2.462159964026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.572306120062" Y="-2.646641609759" />
                  <Point X="-21.739750673658" Y="2.388263099134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.190025657208" Y="-4.06605067161" />
                  <Point X="-24.181391654028" Y="-4.042328942832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.676190441126" Y="-2.654300018104" />
                  <Point X="-21.867743821269" Y="2.314366234243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.232893358045" Y="-3.906067293667" />
                  <Point X="-24.208246241343" Y="-3.838349897073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.780790726613" Y="-2.663925522532" />
                  <Point X="-21.995736968881" Y="2.240469369351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.275761058882" Y="-3.746083915725" />
                  <Point X="-24.235100791951" Y="-3.634370750464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.889811719634" Y="-2.685696821088" />
                  <Point X="-22.123730159415" Y="2.166572386529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.318628759719" Y="-3.586100537783" />
                  <Point X="-24.2619552434" Y="-3.430391331416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.025159676348" Y="-2.779800857912" />
                  <Point X="-22.251723379033" Y="2.0926753238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.365850587995" Y="-3.438080026662" />
                  <Point X="-24.230186108347" Y="-3.065344932205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.177217255221" Y="-2.919814204308" />
                  <Point X="-22.378959745512" Y="2.020857697982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.430957653484" Y="-3.339198800923" />
                  <Point X="-22.493171704969" Y="1.984824336357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.497276372011" Y="-3.243646564547" />
                  <Point X="-22.597100384829" Y="1.977044053225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.565329380618" Y="-3.152859251005" />
                  <Point X="-22.69307133944" Y="1.991127440523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.64553986668" Y="-3.09547433225" />
                  <Point X="-22.779444721459" Y="2.031579941799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.735346235681" Y="-3.064453885189" />
                  <Point X="-22.85910624576" Y="2.090473120597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.229776812959" Y="3.819541526618" />
                  <Point X="-22.196181143175" Y="3.911844870741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.82618200013" Y="-3.036261678874" />
                  <Point X="-22.928084644605" Y="2.178717945357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.50331957058" Y="3.345750394813" />
                  <Point X="-22.276490889291" Y="3.968957074739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.917017905534" Y="-3.008069859832" />
                  <Point X="-22.979885894239" Y="2.314156599702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.776861842839" Y="2.87196059653" />
                  <Point X="-22.356800635407" Y="4.026069278738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.014525501513" Y="-2.998208359996" />
                  <Point X="-22.439678959201" Y="4.076124373567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.125393373565" Y="-3.025053916985" />
                  <Point X="-22.523088887737" Y="4.124718896372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.23936482256" Y="-3.060426481547" />
                  <Point X="-22.6064988214" Y="4.173313405091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.355703996289" Y="-3.102304316348" />
                  <Point X="-22.689908755063" Y="4.221907913809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.057535479146" Y="-4.752809049744" />
                  <Point X="-25.769988007587" Y="-3.962778864616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.527912233026" Y="-3.297681140211" />
                  <Point X="-22.774168236519" Y="4.268168309151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.135823302597" Y="-4.69014165888" />
                  <Point X="-22.860559626825" Y="4.308571333063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.138720292297" Y="-4.420339654649" />
                  <Point X="-22.946951167436" Y="4.348973944018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.174446883727" Y="-4.240736239861" />
                  <Point X="-23.033342708047" Y="4.389376554973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.225249882704" Y="-4.102554914375" />
                  <Point X="-23.119734248658" Y="4.429779165928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.298734839301" Y="-4.026691755281" />
                  <Point X="-23.20829877218" Y="4.464211555402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.375370023279" Y="-3.95948377478" />
                  <Point X="-23.297605941835" Y="4.496603541396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.452005241154" Y="-3.89227588741" />
                  <Point X="-23.38691313581" Y="4.52899546057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.530800433209" Y="-3.831002480329" />
                  <Point X="-23.476220329785" Y="4.561387379744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.62015841022" Y="-3.7987500864" />
                  <Point X="-23.566303340329" Y="4.591647760413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.718370509617" Y="-3.790824193793" />
                  <Point X="-23.659233667642" Y="4.614085202555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.817111830193" Y="-3.784352324429" />
                  <Point X="-23.752164344562" Y="4.636521684157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.915853179903" Y="-3.777880535109" />
                  <Point X="-23.845095078069" Y="4.658958010289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.019556109684" Y="-3.785040574997" />
                  <Point X="-23.938025811576" Y="4.681394336421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.14355513122" Y="-3.847963668685" />
                  <Point X="-24.030956545083" Y="4.703830662553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.277139302818" Y="-3.937221745733" />
                  <Point X="-24.12388727859" Y="4.726266988685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.421443010806" Y="-4.055931506957" />
                  <Point X="-24.21896030518" Y="4.742817412943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.550630713278" Y="-4.133110384356" />
                  <Point X="-24.31634514054" Y="4.753016194812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.633134590495" Y="-4.082026506009" />
                  <Point X="-24.413729952301" Y="4.763215041515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.715638396876" Y="-4.030942433045" />
                  <Point X="-24.511114764063" Y="4.773413888217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.795975108724" Y="-3.973904316785" />
                  <Point X="-27.339757683529" Y="-2.720457242702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.319299745488" Y="-2.664249519885" />
                  <Point X="-24.84332900932" Y="4.138424168966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.660354545637" Y="4.641142376274" />
                  <Point X="-24.608499575824" Y="4.78361273492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.874942005865" Y="-3.913102665549" />
                  <Point X="-27.61395161581" Y="-3.19603746218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.350718257319" Y="-2.472809753677" />
                  <Point X="-24.962899967625" Y="4.087667079018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.953908903006" Y="-3.852301014312" />
                  <Point X="-27.887493598612" Y="-3.669826465186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.423928079004" Y="-2.396190667623" />
                  <Point X="-25.060970198078" Y="4.095982753342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.510091307081" Y="-2.355160773138" />
                  <Point X="-25.14612605238" Y="4.139780384529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.610586955158" Y="-2.353508878967" />
                  <Point X="-25.213627560965" Y="4.232082931927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.730047228453" Y="-2.403961864353" />
                  <Point X="-25.258854456124" Y="4.385584476743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.858040281374" Y="-2.477858469084" />
                  <Point X="-25.301721923461" Y="4.545568496219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.986033359884" Y="-2.55175514412" />
                  <Point X="-25.344589390799" Y="4.705552515694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.114026539399" Y="-2.625652096667" />
                  <Point X="-25.419991939777" Y="4.776147133024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.242019718915" Y="-2.699549049215" />
                  <Point X="-25.525586936379" Y="4.763788682267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.370012898431" Y="-2.773446001763" />
                  <Point X="-25.631181932982" Y="4.751430231511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.498006077946" Y="-2.847342954311" />
                  <Point X="-28.026927591835" Y="-1.553065450931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.949470679432" Y="-1.340254333122" />
                  <Point X="-25.736776929584" Y="4.739071780754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.625999257462" Y="-2.921239906859" />
                  <Point X="-28.169499890157" Y="-1.667018203194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.001306681301" Y="-1.204911159757" />
                  <Point X="-25.842371926186" Y="4.726713329998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.753992436977" Y="-2.995136859407" />
                  <Point X="-28.309772865675" Y="-1.774653617975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.078375677609" Y="-1.138895068836" />
                  <Point X="-25.949078397904" Y="4.711301126457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.840589363577" Y="-2.955298541819" />
                  <Point X="-28.450045841194" Y="-1.882289032755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.167249342235" Y="-1.105312037565" />
                  <Point X="-26.061663977268" Y="4.679736207415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.908982350036" Y="-2.865445309747" />
                  <Point X="-28.590318816712" Y="-1.989924447536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.267599489169" Y="-1.10326038229" />
                  <Point X="-26.174249611693" Y="4.648171137094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.977374716803" Y="-2.775590375088" />
                  <Point X="-28.73059179223" Y="-2.097559862316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.373784496969" Y="-1.117239875489" />
                  <Point X="-26.286835253897" Y="4.616606045399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.042342128804" Y="-2.676325454546" />
                  <Point X="-28.870864767748" Y="-2.205195277097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.479969504769" Y="-1.131219368689" />
                  <Point X="-26.559460211427" Y="4.145336548619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.462302059656" Y="4.412276376727" />
                  <Point X="-26.399420896101" Y="4.585040953705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.10512283671" Y="-2.571052613879" />
                  <Point X="-29.011137743266" Y="-2.312830691877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.586154512569" Y="-1.145198861888" />
                  <Point X="-26.688527715803" Y="4.068487912777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.167903544616" Y="-2.465779773211" />
                  <Point X="-29.151410413112" Y="-2.420465266829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.692339533018" Y="-1.159178389841" />
                  <Point X="-28.334974700627" Y="-0.17732658234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.299806644477" Y="-0.080703142183" />
                  <Point X="-26.803177381333" Y="4.031251963601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.798524553887" Y="-1.173157918948" />
                  <Point X="-28.481540045075" Y="-0.302250138672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.331832138366" Y="0.109068954526" />
                  <Point X="-26.901267417701" Y="4.039513221622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.904709574757" Y="-1.187137448056" />
                  <Point X="-28.594672824477" Y="-0.335318477461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.40241837629" Y="0.192896277722" />
                  <Point X="-26.988118458536" Y="4.078653366087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.010894595626" Y="-1.201116977164" />
                  <Point X="-28.706694714973" Y="-0.365334674068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.485842199051" Y="0.241452626457" />
                  <Point X="-27.071677697305" Y="4.126837662397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.117079616496" Y="-1.215096506272" />
                  <Point X="-28.818716605468" Y="-0.395350870674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.577244481817" Y="0.268088336486" />
                  <Point X="-27.144207040196" Y="4.205326348572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.223264637365" Y="-1.229076035379" />
                  <Point X="-28.930738495964" Y="-0.425367067281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.669357922359" Y="0.292770156583" />
                  <Point X="-27.229953114951" Y="4.247502362391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.329449658235" Y="-1.243055564487" />
                  <Point X="-29.04276038646" Y="-0.455383263888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.761471362902" Y="0.317451976679" />
                  <Point X="-27.849852969235" Y="2.822102928439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.752699866744" Y="3.089028883763" />
                  <Point X="-27.356674911325" Y="4.177098506316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.435634679104" Y="-1.257035093595" />
                  <Point X="-29.154782276956" Y="-0.485399460494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.853584815133" Y="0.342133764662" />
                  <Point X="-28.50119115065" Y="1.310327400588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.421324516565" Y="1.529759174306" />
                  <Point X="-27.983569687162" Y="2.732480683345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.796917276223" Y="3.245303967688" />
                  <Point X="-27.483396707699" Y="4.106694650241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.541819699973" Y="-1.271014622703" />
                  <Point X="-29.266804167452" Y="-0.515415657101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.945698290868" Y="0.366815488068" />
                  <Point X="-28.632251850675" Y="1.228002504706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.469542836935" Y="1.675041845898" />
                  <Point X="-28.08730338917" Y="2.725236097458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.85892424849" Y="3.35270262955" />
                  <Point X="-27.610118504073" Y="4.036290794166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.648004720843" Y="-1.28499415181" />
                  <Point X="-29.378826057948" Y="-0.545431853708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.037811766603" Y="0.391497211475" />
                  <Point X="-28.741890807061" Y="1.20453336576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.538046751402" Y="1.764590305773" />
                  <Point X="-28.184335974337" Y="2.736402678776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.920931061486" Y="3.460101729009" />
                  <Point X="-27.746270566906" Y="3.939977493937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.697539143187" Y="-1.14332744067" />
                  <Point X="-29.490847948444" Y="-0.575448050314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.129925242337" Y="0.416178934882" />
                  <Point X="-28.839646529753" Y="1.213713143056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.616911692974" Y="1.825672077634" />
                  <Point X="-28.272183034352" Y="2.772806283034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.982937848863" Y="3.567500898851" />
                  <Point X="-27.886498377155" Y="3.832466169714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.73922817073" Y="-0.980105684468" />
                  <Point X="-29.60286983894" Y="-0.605464246921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.222038718072" Y="0.440860658289" />
                  <Point X="-28.936120614748" Y="1.226414190986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.695937672741" Y="1.886311400689" />
                  <Point X="-28.355724655066" Y="2.821038984554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.044944636241" Y="3.674900068693" />
                  <Point X="-28.026726096064" Y="3.724955096444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.768232104598" Y="-0.782031919831" />
                  <Point X="-29.714891717085" Y="-0.635480409593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.314152193807" Y="0.465542381696" />
                  <Point X="-29.032594699742" Y="1.239115238916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.774963756566" Y="1.946950437845" />
                  <Point X="-28.43926627578" Y="2.869271686074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.406265669542" Y="0.490224105103" />
                  <Point X="-29.129068784737" Y="1.251816286846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.853989900324" Y="2.00758931034" />
                  <Point X="-28.522807896493" Y="2.917504387594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.498379145276" Y="0.51490582851" />
                  <Point X="-29.225542869732" Y="1.264517334776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.933016044081" Y="2.068228182835" />
                  <Point X="-28.606349517207" Y="2.965737089114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.590492621011" Y="0.539587551916" />
                  <Point X="-29.322016954727" Y="1.277218382706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.012042187839" Y="2.12886705533" />
                  <Point X="-28.689891137921" Y="3.013969790634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.682606096746" Y="0.564269275323" />
                  <Point X="-29.418491039721" Y="1.289919430636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.091068331596" Y="2.189505927826" />
                  <Point X="-28.833429251043" Y="2.897363484015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.77471957248" Y="0.58895099873" />
                  <Point X="-29.514965124716" Y="1.302620478566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.170094475354" Y="2.250144800321" />
                  <Point X="-29.032139539088" Y="2.629172872614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.701213870293" Y="1.068667673706" />
                  <Point X="-29.611439209711" Y="1.315321526496" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.21283984375" Y="-4.715017089844" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.664681640625" Y="-3.335677734375" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399658203" />
                  <Point X="-24.92528515625" Y="-3.204444335938" />
                  <Point X="-24.9790234375" Y="-3.187766357422" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.20828515625" Y="-3.249721679688" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643798828" />
                  <Point X="-25.417279296875" Y="-3.471510009766" />
                  <Point X="-25.452005859375" Y="-3.521544189453" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.544189453125" Y="-3.854197265625" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-26.03814453125" Y="-4.950119140625" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.331765625" Y="-4.878497070312" />
                  <Point X="-26.351587890625" Y="-4.873396484375" />
                  <Point X="-26.347142578125" Y="-4.839624023438" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.357373046875" Y="-4.295005371094" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.202628417969" />
                  <Point X="-26.570068359375" Y="-4.041451660156" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.893166015625" Y="-3.969775146484" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791503906" />
                  <Point X="-27.193130859375" Y="-4.109600097656" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.30743359375" Y="-4.219458007813" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.76480859375" Y="-4.223970703125" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.176421875" Y="-3.920768554688" />
                  <Point X="-28.228580078125" Y="-3.880608154297" />
                  <Point X="-28.09257421875" Y="-3.645039306641" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597592529297" />
                  <Point X="-27.51398046875" Y="-2.568762939453" />
                  <Point X="-27.53131640625" Y="-2.551425048828" />
                  <Point X="-27.5601484375" Y="-2.537199707031" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-27.865162109375" Y="-2.70136328125" />
                  <Point X="-28.84295703125" Y="-3.265893554688" />
                  <Point X="-29.0897890625" Y="-2.941609375" />
                  <Point X="-29.161703125" Y="-2.847127685547" />
                  <Point X="-29.3915390625" Y="-2.461727050781" />
                  <Point X="-29.43101953125" Y="-2.395526367188" />
                  <Point X="-29.19004296875" Y="-2.210619873047" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.396013305664" />
                  <Point X="-28.13975390625" Y="-1.372579956055" />
                  <Point X="-28.1381171875" Y="-1.366270874023" />
                  <Point X="-28.14032421875" Y="-1.334597412109" />
                  <Point X="-28.161158203125" Y="-1.310638793945" />
                  <Point X="-28.1820234375" Y="-1.298358154297" />
                  <Point X="-28.187640625" Y="-1.295052246094" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.56476953125" Y="-1.334023071289" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.899265625" Y="-1.121305053711" />
                  <Point X="-29.927390625" Y="-1.011189025879" />
                  <Point X="-29.988201171875" Y="-0.586019287109" />
                  <Point X="-29.99839453125" Y="-0.514742248535" />
                  <Point X="-29.7265234375" Y="-0.441894622803" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.541896484375" Y="-0.121425811768" />
                  <Point X="-28.520029296875" Y="-0.106249320984" />
                  <Point X="-28.514142578125" Y="-0.102163780212" />
                  <Point X="-28.4948984375" Y="-0.075908058167" />
                  <Point X="-28.487609375" Y="-0.05242313385" />
                  <Point X="-28.48564453125" Y="-0.046092662811" />
                  <Point X="-28.4856484375" Y="-0.01645889473" />
                  <Point X="-28.492935546875" Y="0.007017140865" />
                  <Point X="-28.49489453125" Y="0.013339464188" />
                  <Point X="-28.514142578125" Y="0.03960370636" />
                  <Point X="-28.536009765625" Y="0.054780345917" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-28.872166015625" Y="0.150410064697" />
                  <Point X="-29.998185546875" Y="0.452126037598" />
                  <Point X="-29.935771484375" Y="0.873923217773" />
                  <Point X="-29.91764453125" Y="0.996414794922" />
                  <Point X="-29.79523828125" Y="1.448136230469" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.59127734375" Y="1.504306762695" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866333008" />
                  <Point X="-28.683306640625" Y="1.411126098633" />
                  <Point X="-28.67027734375" Y="1.415233764648" />
                  <Point X="-28.651533203125" Y="1.426057006836" />
                  <Point X="-28.6391171875" Y="1.44378918457" />
                  <Point X="-28.619697265625" Y="1.490672851562" />
                  <Point X="-28.614470703125" Y="1.503293579102" />
                  <Point X="-28.61071484375" Y="1.524608520508" />
                  <Point X="-28.61631640625" Y="1.545514038086" />
                  <Point X="-28.639748046875" Y="1.590526855469" />
                  <Point X="-28.646056640625" Y="1.602643920898" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-28.840482421875" Y="1.757735961914" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.2304453125" Y="2.66633984375" />
                  <Point X="-29.160013671875" Y="2.787006591797" />
                  <Point X="-28.835763671875" Y="3.203785644531" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.67383984375" Y="3.224096191406" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.071109375" Y="2.914537597656" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927405029297" />
                  <Point X="-27.96540625" Y="2.975249267578" />
                  <Point X="-27.95252734375" Y="2.988128417969" />
                  <Point X="-27.9408984375" Y="3.006383056641" />
                  <Point X="-27.938072265625" Y="3.027842041016" />
                  <Point X="-27.943970703125" Y="3.095246582031" />
                  <Point X="-27.945556640625" Y="3.113386474609" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.03205859375" Y="3.272582763672" />
                  <Point X="-28.30727734375" Y="3.749276855469" />
                  <Point X="-27.87554296875" Y="4.080282714844" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.242189453125" Y="4.458057617188" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.12538671875" Y="4.49291015625" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.876224609375" Y="4.234606933594" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.218491699219" />
                  <Point X="-26.813806640625" Y="4.22225" />
                  <Point X="-26.73566796875" Y="4.254616699219" />
                  <Point X="-26.7146328125" Y="4.263329589844" />
                  <Point X="-26.696904296875" Y="4.275743164062" />
                  <Point X="-26.68608203125" Y="4.294485839844" />
                  <Point X="-26.660650390625" Y="4.3751484375" />
                  <Point X="-26.653802734375" Y="4.396862304687" />
                  <Point X="-26.651916015625" Y="4.418426269531" />
                  <Point X="-26.66101171875" Y="4.487503417969" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.12689453125" Y="4.858773925781" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.348986328125" Y="4.97575390625" />
                  <Point X="-25.22419921875" Y="4.990358398438" />
                  <Point X="-25.1943984375" Y="4.879137207031" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.90455859375" Y="4.463868652344" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.278451171875" Y="4.940086914062" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.62758984375" Y="4.801904296875" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.15748828125" Y="4.64789453125" />
                  <Point X="-23.068970703125" Y="4.615788085938" />
                  <Point X="-22.74658984375" Y="4.465021484375" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.349875" Y="4.243697265625" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-21.9737578125" Y="3.986815673828" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.091267578125" Y="3.679448974609" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491517333984" />
                  <Point X="-22.7920625" Y="2.428259521484" />
                  <Point X="-22.797955078125" Y="2.392326416016" />
                  <Point X="-22.791359375" Y="2.337626464844" />
                  <Point X="-22.789583984375" Y="2.322901611328" />
                  <Point X="-22.781318359375" Y="2.300812011719" />
                  <Point X="-22.747470703125" Y="2.250930908203" />
                  <Point X="-22.72505859375" Y="2.224203369141" />
                  <Point X="-22.675177734375" Y="2.190356933594" />
                  <Point X="-22.66175" Y="2.181245849609" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.584962890625" Y="2.166384277344" />
                  <Point X="-22.5513359375" Y="2.165946044922" />
                  <Point X="-22.488078125" Y="2.182862060547" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-22.14243359375" Y="2.375167236328" />
                  <Point X="-21.005751953125" Y="3.031430664062" />
                  <Point X="-20.846283203125" Y="2.809806884766" />
                  <Point X="-20.79740234375" Y="2.741874267578" />
                  <Point X="-20.6336640625" Y="2.471293212891" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-20.817576171875" Y="2.278924560547" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.720626953125" Y="1.583833740234" />
                  <Point X="-21.76615234375" Y="1.524440673828" />
                  <Point X="-21.778408203125" Y="1.508452392578" />
                  <Point X="-21.78687890625" Y="1.491503417969" />
                  <Point X="-21.803837890625" Y="1.430862792969" />
                  <Point X="-21.808404296875" Y="1.41453894043" />
                  <Point X="-21.809220703125" Y="1.390964111328" />
                  <Point X="-21.795298828125" Y="1.323493896484" />
                  <Point X="-21.784353515625" Y="1.287957885742" />
                  <Point X="-21.74648828125" Y="1.230405273438" />
                  <Point X="-21.736296875" Y="1.214912475586" />
                  <Point X="-21.71905078125" Y="1.198819580078" />
                  <Point X="-21.6641796875" Y="1.167931762695" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.557244140625" Y="1.143814575195" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-21.22351171875" Y="1.180757568359" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.081802734375" Y="1.037614379883" />
                  <Point X="-20.06080859375" Y="0.951369750977" />
                  <Point X="-20.0092109375" Y="0.619965759277" />
                  <Point X="-20.002140625" Y="0.574556274414" />
                  <Point X="-20.234509765625" Y="0.51229296875" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819351196" />
                  <Point X="-21.34380078125" Y="0.190688308716" />
                  <Point X="-21.363421875" Y="0.179346832275" />
                  <Point X="-21.377732421875" Y="0.166927749634" />
                  <Point X="-21.42146484375" Y="0.111201332092" />
                  <Point X="-21.43323828125" Y="0.096200080872" />
                  <Point X="-21.443013671875" Y="0.074734199524" />
                  <Point X="-21.457591796875" Y="-0.001385390401" />
                  <Point X="-21.461515625" Y="-0.021876363754" />
                  <Point X="-21.461515625" Y="-0.04068371582" />
                  <Point X="-21.4469375" Y="-0.11680329895" />
                  <Point X="-21.443013671875" Y="-0.137294281006" />
                  <Point X="-21.43323828125" Y="-0.158760162354" />
                  <Point X="-21.389505859375" Y="-0.214486587524" />
                  <Point X="-21.363421875" Y="-0.241906906128" />
                  <Point X="-21.290533203125" Y="-0.28403793335" />
                  <Point X="-21.270912109375" Y="-0.295379425049" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.98308984375" Y="-0.374271636963" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.039845703125" Y="-0.88865625" />
                  <Point X="-20.051568359375" Y="-0.966412353516" />
                  <Point X="-20.117673828125" Y="-1.256095092773" />
                  <Point X="-20.125451171875" Y="-1.290178710938" />
                  <Point X="-20.40009375" Y="-1.254021484375" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.748216796875" Y="-1.129434814453" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.814552734375" Y="-1.154696533203" />
                  <Point X="-21.90101953125" Y="-1.258690185547" />
                  <Point X="-21.924296875" Y="-1.286684570312" />
                  <Point X="-21.935640625" Y="-1.314070800781" />
                  <Point X="-21.948033203125" Y="-1.448747192383" />
                  <Point X="-21.951369140625" Y="-1.485001220703" />
                  <Point X="-21.94363671875" Y="-1.516622680664" />
                  <Point X="-21.86446875" Y="-1.639764282227" />
                  <Point X="-21.84315625" Y="-1.672913330078" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-21.5756484375" Y="-1.881891601562" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.762828125" Y="-2.748677978516" />
                  <Point X="-20.79586328125" Y="-2.802133544922" />
                  <Point X="-20.93258203125" Y="-2.996394775391" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.188537109375" Y="-2.870056640625" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.432916015625" Y="-2.222564453125" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219245117188" />
                  <Point X="-22.652365234375" Y="-2.293685302734" />
                  <Point X="-22.690439453125" Y="-2.313724121094" />
                  <Point X="-22.7113984375" Y="-2.33468359375" />
                  <Point X="-22.78583984375" Y="-2.476126220703" />
                  <Point X="-22.80587890625" Y="-2.514201660156" />
                  <Point X="-22.8108359375" Y="-2.546374511719" />
                  <Point X="-22.7800859375" Y="-2.716632568359" />
                  <Point X="-22.77180859375" Y="-2.76246484375" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.60147265625" Y="-3.063387939453" />
                  <Point X="-22.01332421875" Y="-4.082088623047" />
                  <Point X="-22.1254921875" Y="-4.162207519531" />
                  <Point X="-22.164705078125" Y="-4.190215332031" />
                  <Point X="-22.317556640625" Y="-4.289153808594" />
                  <Point X="-22.32022265625" Y="-4.290879882812" />
                  <Point X="-22.50946484375" Y="-4.044255371094" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467529297" />
                  <Point X="-23.49737109375" Y="-2.872510253906" />
                  <Point X="-23.54257421875" Y="-2.843448730469" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.75784375" Y="-2.852616699219" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509277344" />
                  <Point X="-23.9764765625" Y="-2.986419189453" />
                  <Point X="-24.014650390625" Y="-3.018159667969" />
                  <Point X="-24.03154296875" Y="-3.045986083984" />
                  <Point X="-24.073943359375" Y="-3.241060791016" />
                  <Point X="-24.085357421875" Y="-3.293573974609" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-24.039470703125" Y="-3.664678710938" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.9685625" Y="-4.9551171875" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#196" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.152257696655" Y="4.92335796233" Z="2" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2" />
                  <Point X="-0.360954918021" Y="5.056694984639" Z="2" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2" />
                  <Point X="-1.146550008965" Y="4.93819746967" Z="2" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2" />
                  <Point X="-1.716740053323" Y="4.512257635698" Z="2" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2" />
                  <Point X="-1.714492271444" Y="4.421466676115" Z="2" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2" />
                  <Point X="-1.760959773676" Y="4.332091198772" Z="2" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2" />
                  <Point X="-1.859294244982" Y="4.310237606989" Z="2" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2" />
                  <Point X="-2.091875459526" Y="4.554627941542" Z="2" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2" />
                  <Point X="-2.272629071537" Y="4.533045028235" Z="2" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2" />
                  <Point X="-2.912122563418" Y="4.151242672369" Z="2" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2" />
                  <Point X="-3.081516452808" Y="3.278862704959" Z="2" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2" />
                  <Point X="-2.999937199237" Y="3.122168027714" Z="2" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2" />
                  <Point X="-3.00691937952" Y="3.041884188367" Z="2" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2" />
                  <Point X="-3.072908437892" Y="2.995627499936" Z="2" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2" />
                  <Point X="-3.654996643338" Y="3.298677495938" Z="2" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2" />
                  <Point X="-3.881382546032" Y="3.26576831667" Z="2" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2" />
                  <Point X="-4.279784398772" Y="2.722824848339" Z="2" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2" />
                  <Point X="-3.877077523697" Y="1.749348316568" Z="2" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2" />
                  <Point X="-3.690254513767" Y="1.598716994342" Z="2" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2" />
                  <Point X="-3.672050123662" Y="1.541083619616" Z="2" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2" />
                  <Point X="-3.704498299413" Y="1.49009227837" Z="2" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2" />
                  <Point X="-4.590908060061" Y="1.585158939563" Z="2" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2" />
                  <Point X="-4.849654274755" Y="1.492493568424" Z="2" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2" />
                  <Point X="-4.991388594706" Y="0.912522549103" Z="2" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2" />
                  <Point X="-3.89126521743" Y="0.133393845103" Z="2" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2" />
                  <Point X="-3.570674394677" Y="0.044983575252" Z="2" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2" />
                  <Point X="-3.546845712033" Y="0.023484944033" Z="2" />
                  <Point X="-3.539556741714" Y="0" Z="2" />
                  <Point X="-3.541518889102" Y="-0.006322007027" Z="2" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2" />
                  <Point X="-3.554694200447" Y="-0.033892407882" Z="2" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2" />
                  <Point X="-4.745622571343" Y="-0.362318211653" Z="2" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2" />
                  <Point X="-5.04385436002" Y="-0.561818357019" Z="2" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2" />
                  <Point X="-4.953867582857" Y="-1.102398802452" Z="2" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2" />
                  <Point X="-3.564400900169" Y="-1.352315272426" Z="2" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2" />
                  <Point X="-3.213541974219" Y="-1.310169178897" Z="2" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2" />
                  <Point X="-3.19431041723" Y="-1.328759172675" Z="2" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2" />
                  <Point X="-4.226638217834" Y="-2.139672148681" Z="2" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2" />
                  <Point X="-4.440640018341" Y="-2.456057214045" Z="2" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2" />
                  <Point X="-4.135732125995" Y="-2.940612228811" Z="2" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2" />
                  <Point X="-2.846319062376" Y="-2.713384468229" Z="2" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2" />
                  <Point X="-2.569159903276" Y="-2.559170565855" Z="2" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2" />
                  <Point X="-3.142032544939" Y="-3.588759320116" Z="2" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2" />
                  <Point X="-3.213082234576" Y="-3.929105723616" Z="2" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2" />
                  <Point X="-2.797288428268" Y="-4.235201283272" Z="2" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2" />
                  <Point X="-2.273922250596" Y="-4.218615973508" Z="2" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2" />
                  <Point X="-2.171508038" Y="-4.119893239683" Z="2" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2" />
                  <Point X="-1.902592629708" Y="-3.988388176886" Z="2" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2" />
                  <Point X="-1.609192588712" Y="-4.047763755623" Z="2" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2" />
                  <Point X="-1.412568204343" Y="-4.27348029809" Z="2" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2" />
                  <Point X="-1.402871555745" Y="-4.801817748132" Z="2" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2" />
                  <Point X="-1.350382140972" Y="-4.895639765763" Z="2" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2" />
                  <Point X="-1.053828783803" Y="-4.96792332626" Z="2" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2" />
                  <Point X="-0.502049438282" Y="-3.835858058771" Z="2" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2" />
                  <Point X="-0.382360394526" Y="-3.468738977926" Z="2" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2" />
                  <Point X="-0.199622020615" Y="-3.266194676897" Z="2" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2" />
                  <Point X="0.053737058745" Y="-3.220917368391" Z="2" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2" />
                  <Point X="0.288085464907" Y="-3.332906777842" Z="2" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2" />
                  <Point X="0.732705422119" Y="-4.696677971044" Z="2" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2" />
                  <Point X="0.855918348982" Y="-5.006814273076" Z="2" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2" />
                  <Point X="1.035984855772" Y="-4.972677345318" Z="2" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2" />
                  <Point X="1.003945330326" Y="-3.626872349998" Z="2" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2" />
                  <Point X="0.968759697009" Y="-3.22040074011" Z="2" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2" />
                  <Point X="1.049334280704" Y="-2.993585364317" Z="2" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2" />
                  <Point X="1.240581071497" Y="-2.871126172647" Z="2" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2" />
                  <Point X="1.469433599986" Y="-2.883288033955" Z="2" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2" />
                  <Point X="2.444710959394" Y="-4.043413263621" Z="2" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2" />
                  <Point X="2.703453988291" Y="-4.2998484609" Z="2" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2" />
                  <Point X="2.897410875619" Y="-4.171614867574" Z="2" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2" />
                  <Point X="2.435671643947" Y="-3.007107840019" Z="2" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2" />
                  <Point X="2.262959589746" Y="-2.676466182504" Z="2" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2" />
                  <Point X="2.252249976792" Y="-2.46813270172" Z="2" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2" />
                  <Point X="2.364765676652" Y="-2.306651333175" Z="2" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2" />
                  <Point X="2.552040621009" Y="-2.240488419745" Z="2" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2" />
                  <Point X="3.780305757088" Y="-2.882077856328" Z="2" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2" />
                  <Point X="4.102148901373" Y="-2.993892495468" Z="2" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2" />
                  <Point X="4.27354904176" Y="-2.7436828713" Z="2" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2" />
                  <Point X="3.448632289793" Y="-1.810943947933" Z="2" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2" />
                  <Point X="3.171431108517" Y="-1.581444055339" Z="2" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2" />
                  <Point X="3.09559859193" Y="-1.422048459567" Z="2" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2" />
                  <Point X="3.131268000525" Y="-1.259377775801" Z="2" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2" />
                  <Point X="3.256245035487" Y="-1.147013982373" Z="2" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2" />
                  <Point X="4.587224503372" Y="-1.272313658901" Z="2" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2" />
                  <Point X="4.924914559203" Y="-1.235939280925" Z="2" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2" />
                  <Point X="5.003438223241" Y="-0.864830415777" Z="2" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2" />
                  <Point X="4.023693875799" Y="-0.294695258951" Z="2" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2" />
                  <Point X="3.728331478628" Y="-0.209469219419" Z="2" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2" />
                  <Point X="3.643669982071" Y="-0.152336979471" Z="2" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2" />
                  <Point X="3.596012479502" Y="-0.076119590834" Z="2" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2" />
                  <Point X="3.585358970922" Y="0.020490940354" Z="2" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2" />
                  <Point X="3.611709456329" Y="0.111611759104" Z="2" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2" />
                  <Point X="3.675063935724" Y="0.178679624823" Z="2" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2" />
                  <Point X="4.772273941507" Y="0.495276673817" Z="2" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2" />
                  <Point X="5.034037358562" Y="0.658938160744" Z="2" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2" />
                  <Point X="4.960621499661" Y="1.080720728522" Z="2" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2" />
                  <Point X="3.763805710107" Y="1.261609933866" Z="2" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2" />
                  <Point X="3.443150058918" Y="1.224663541027" Z="2" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2" />
                  <Point X="3.354189647068" Y="1.242783357099" Z="2" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2" />
                  <Point X="3.289125620607" Y="1.289163838708" Z="2" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2" />
                  <Point X="3.247513436957" Y="1.364878985198" Z="2" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2" />
                  <Point X="3.238157229558" Y="1.448673252837" Z="2" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2" />
                  <Point X="3.267371493118" Y="1.525301977539" Z="2" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2" />
                  <Point X="4.20670452826" Y="2.27053682632" Z="2" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2" />
                  <Point X="4.40295618179" Y="2.528459588407" Z="2" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2" />
                  <Point X="4.188145120816" Y="2.870290050633" Z="2" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2" />
                  <Point X="2.826410037847" Y="2.449748660788" Z="2" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2" />
                  <Point X="2.492849330852" Y="2.262445101232" Z="2" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2" />
                  <Point X="2.414866751458" Y="2.247304725525" Z="2" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2" />
                  <Point X="2.346739109923" Y="2.263011754948" Z="2" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2" />
                  <Point X="2.287746845287" Y="2.310285750459" Z="2" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2" />
                  <Point X="2.252124977233" Y="2.374891692374" Z="2" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2" />
                  <Point X="2.250082841426" Y="2.44662024503" Z="2" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2" />
                  <Point X="2.945876692072" Y="3.685729044108" Z="2" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2" />
                  <Point X="3.049062373163" Y="4.058842833636" Z="2" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2" />
                  <Point X="2.66913885132" Y="4.318179743037" Z="2" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2" />
                  <Point X="2.268435126136" Y="4.541593597698" Z="2" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2" />
                  <Point X="1.853402931596" Y="4.726178002601" Z="2" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2" />
                  <Point X="1.377987269856" Y="4.881787615144" Z="2" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2" />
                  <Point X="0.720598203951" Y="5.021095021613" Z="2" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2" />
                  <Point X="0.040986864898" Y="4.50808954362" Z="2" />
                  <Point X="0" Y="4.355124473572" Z="2" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>