<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#125" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="538" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000473632812" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.404953125" Y="-3.630987792969" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140136719" />
                  <Point X="-24.45763671875" Y="-3.467375" />
                  <Point X="-24.470611328125" Y="-3.448682128906" />
                  <Point X="-24.621365234375" Y="-3.231474609375" />
                  <Point X="-24.643248046875" Y="-3.209020751953" />
                  <Point X="-24.66950390625" Y="-3.189776855469" />
                  <Point X="-24.69750390625" Y="-3.175669433594" />
                  <Point X="-24.717580078125" Y="-3.169438476562" />
                  <Point X="-24.95086328125" Y="-3.097036132812" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.036822265625" Y="-3.097035644531" />
                  <Point X="-25.0568984375" Y="-3.103266357422" />
                  <Point X="-25.290181640625" Y="-3.175668945312" />
                  <Point X="-25.31818359375" Y="-3.189776855469" />
                  <Point X="-25.344439453125" Y="-3.209020751953" />
                  <Point X="-25.366326171875" Y="-3.231479736328" />
                  <Point X="-25.379298828125" Y="-3.250172363281" />
                  <Point X="-25.530052734375" Y="-3.467380126953" />
                  <Point X="-25.5381875" Y="-3.481572021484" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.098544921875" Y="-4.840408691406" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.218783203125" Y="-4.592463867188" />
                  <Point X="-26.2149609375" Y="-4.563440429688" />
                  <Point X="-26.21419921875" Y="-4.547931152344" />
                  <Point X="-26.2165078125" Y="-4.516221191406" />
                  <Point X="-26.2213046875" Y="-4.492109375" />
                  <Point X="-26.277037109375" Y="-4.2119296875" />
                  <Point X="-26.287939453125" Y="-4.182964355469" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.323642578125" Y="-4.131206542969" />
                  <Point X="-26.342125" Y="-4.114996582031" />
                  <Point X="-26.55690234375" Y="-3.926641601562" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.66755859375" Y="-3.889358398438" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708496094" />
                  <Point X="-27.014466796875" Y="-3.882028076172" />
                  <Point X="-27.042658203125" Y="-3.894801269531" />
                  <Point X="-27.063099609375" Y="-3.908459716797" />
                  <Point X="-27.300623046875" Y="-4.067169433594" />
                  <Point X="-27.312787109375" Y="-4.076824462891" />
                  <Point X="-27.3351015625" Y="-4.099462402344" />
                  <Point X="-27.4801484375" Y="-4.288489257813" />
                  <Point X="-27.801712890625" Y="-4.089384765625" />
                  <Point X="-27.828302734375" Y="-4.068910888672" />
                  <Point X="-28.104720703125" Y="-3.856077392578" />
                  <Point X="-27.48275" Y="-2.778791503906" />
                  <Point X="-27.42376171875" Y="-2.676619384766" />
                  <Point X="-27.412859375" Y="-2.647653320312" />
                  <Point X="-27.406587890625" Y="-2.616125" />
                  <Point X="-27.405693359375" Y="-2.584325683594" />
                  <Point X="-27.415345703125" Y="-2.554014160156" />
                  <Point X="-27.430935546875" Y="-2.523813964844" />
                  <Point X="-27.448173828125" Y="-2.500218261719" />
                  <Point X="-27.464146484375" Y="-2.484244384766" />
                  <Point X="-27.4893046875" Y="-2.46621484375" />
                  <Point X="-27.518134765625" Y="-2.451996826172" />
                  <Point X="-27.547751953125" Y="-2.443011962891" />
                  <Point X="-27.5786875" Y="-2.444024169922" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.8180234375" Y="-3.141801513672" />
                  <Point X="-29.082859375" Y="-2.793861328125" />
                  <Point X="-29.101923828125" Y="-2.761894287109" />
                  <Point X="-29.306142578125" Y="-2.419450195312" />
                  <Point X="-28.209166015625" Y="-1.577710449219" />
                  <Point X="-28.105955078125" Y="-1.498513183594" />
                  <Point X="-28.08340625" Y="-1.473780029297" />
                  <Point X="-28.064830078125" Y="-1.444291259766" />
                  <Point X="-28.05324609375" Y="-1.417473876953" />
                  <Point X="-28.04615234375" Y="-1.390083618164" />
                  <Point X="-28.04334765625" Y="-1.359660522461" />
                  <Point X="-28.0455546875" Y="-1.327994384766" />
                  <Point X="-28.05297265625" Y="-1.297256347656" />
                  <Point X="-28.07006640625" Y="-1.270655395508" />
                  <Point X="-28.092998046875" Y="-1.245459594727" />
                  <Point X="-28.1150703125" Y="-1.227531005859" />
                  <Point X="-28.139455078125" Y="-1.21317956543" />
                  <Point X="-28.16871875" Y="-1.201956054688" />
                  <Point X="-28.20060546875" Y="-1.195474731445" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-29.730689453125" Y="-1.391699584961" />
                  <Point X="-29.732099609375" Y="-1.391885375977" />
                  <Point X="-29.834078125" Y="-0.992649047852" />
                  <Point X="-29.839119140625" Y="-0.957393615723" />
                  <Point X="-29.892421875" Y="-0.584698303223" />
                  <Point X="-28.65044921875" Y="-0.251912139893" />
                  <Point X="-28.532875" Y="-0.220408340454" />
                  <Point X="-28.515501953125" Y="-0.213876495361" />
                  <Point X="-28.48553125" Y="-0.197944580078" />
                  <Point X="-28.4599765625" Y="-0.180208786011" />
                  <Point X="-28.43624609375" Y="-0.156543289185" />
                  <Point X="-28.416267578125" Y="-0.127925041199" />
                  <Point X="-28.40343359375" Y="-0.101703704834" />
                  <Point X="-28.394916015625" Y="-0.074258636475" />
                  <Point X="-28.390671875" Y="-0.043926670074" />
                  <Point X="-28.391404296875" Y="-0.011924245834" />
                  <Point X="-28.3956484375" Y="0.014060599327" />
                  <Point X="-28.404166015625" Y="0.041505516052" />
                  <Point X="-28.419693359375" Y="0.071395469666" />
                  <Point X="-28.441138671875" Y="0.099178123474" />
                  <Point X="-28.46217578125" Y="0.119175140381" />
                  <Point X="-28.48773046875" Y="0.13691078186" />
                  <Point X="-28.501923828125" Y="0.145046905518" />
                  <Point X="-28.532875" Y="0.157848297119" />
                  <Point X="-29.89181640625" Y="0.521975402832" />
                  <Point X="-29.82448828125" Y="0.976966491699" />
                  <Point X="-29.81433203125" Y="1.014445617676" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-28.8499453125" Y="1.310888549805" />
                  <Point X="-28.7656640625" Y="1.29979309082" />
                  <Point X="-28.744982421875" Y="1.299341796875" />
                  <Point X="-28.723421875" Y="1.301228149414" />
                  <Point X="-28.703134765625" Y="1.305263916016" />
                  <Point X="-28.698267578125" Y="1.306798461914" />
                  <Point X="-28.641708984375" Y="1.324631347656" />
                  <Point X="-28.622775390625" Y="1.332962524414" />
                  <Point X="-28.60403125" Y="1.343784790039" />
                  <Point X="-28.5873515625" Y="1.356016357422" />
                  <Point X="-28.573712890625" Y="1.371568725586" />
                  <Point X="-28.561298828125" Y="1.389298828125" />
                  <Point X="-28.5513515625" Y="1.407431396484" />
                  <Point X="-28.5493984375" Y="1.412146606445" />
                  <Point X="-28.526705078125" Y="1.466935668945" />
                  <Point X="-28.52091796875" Y="1.486784667969" />
                  <Point X="-28.517158203125" Y="1.508094482422" />
                  <Point X="-28.515802734375" Y="1.528733154297" />
                  <Point X="-28.518947265625" Y="1.54917578125" />
                  <Point X="-28.524544921875" Y="1.570078125" />
                  <Point X="-28.532041015625" Y="1.589363891602" />
                  <Point X="-28.53440234375" Y="1.593900512695" />
                  <Point X="-28.561787109375" Y="1.646503417969" />
                  <Point X="-28.573283203125" Y="1.663708007812" />
                  <Point X="-28.5871953125" Y="1.680287475586" />
                  <Point X="-28.60213671875" Y="1.694590454102" />
                  <Point X="-29.351859375" Y="2.269874023438" />
                  <Point X="-29.081154296875" Y="2.73365625" />
                  <Point X="-29.0542578125" Y="2.768229980469" />
                  <Point X="-28.75050390625" Y="3.158661865234" />
                  <Point X="-28.25841796875" Y="2.874556152344" />
                  <Point X="-28.20665625" Y="2.844671142578" />
                  <Point X="-28.187724609375" Y="2.836340820312" />
                  <Point X="-28.16708203125" Y="2.82983203125" />
                  <Point X="-28.14679296875" Y="2.825796142578" />
                  <Point X="-28.140013671875" Y="2.825203125" />
                  <Point X="-28.061244140625" Y="2.818311523438" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.999013671875" Y="2.826504638672" />
                  <Point X="-27.980462890625" Y="2.835653564453" />
                  <Point X="-27.962208984375" Y="2.847282714844" />
                  <Point X="-27.94608203125" Y="2.860224365234" />
                  <Point X="-27.94126953125" Y="2.865036132812" />
                  <Point X="-27.885357421875" Y="2.920947753906" />
                  <Point X="-27.87240234375" Y="2.937090087891" />
                  <Point X="-27.8607734375" Y="2.955346679688" />
                  <Point X="-27.851625" Y="2.973898193359" />
                  <Point X="-27.8467109375" Y="2.993989746094" />
                  <Point X="-27.84388671875" Y="3.015451171875" />
                  <Point X="-27.843435546875" Y="3.036114257812" />
                  <Point X="-27.84402734375" Y="3.0428984375" />
                  <Point X="-27.85091796875" Y="3.121668945312" />
                  <Point X="-27.854955078125" Y="3.141961669922" />
                  <Point X="-27.86146484375" Y="3.162604492188" />
                  <Point X="-27.869794921875" Y="3.181532958984" />
                  <Point X="-28.18333203125" Y="3.724596191406" />
                  <Point X="-27.700623046875" Y="4.094684570313" />
                  <Point X="-27.65826953125" Y="4.11821484375" />
                  <Point X="-27.16703515625" Y="4.391133789062" />
                  <Point X="-27.059041015625" Y="4.250392578125" />
                  <Point X="-27.0431953125" Y="4.229741699219" />
                  <Point X="-27.028892578125" Y="4.214801269531" />
                  <Point X="-27.0123125" Y="4.200888183594" />
                  <Point X="-26.99511328125" Y="4.189395019531" />
                  <Point X="-26.987568359375" Y="4.185467285156" />
                  <Point X="-26.899896484375" Y="4.139828125" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.7774453125" Y="4.134484863281" />
                  <Point X="-26.769587890625" Y="4.137740234375" />
                  <Point X="-26.678271484375" Y="4.175564453125" />
                  <Point X="-26.66013671875" Y="4.185514648437" />
                  <Point X="-26.64240625" Y="4.197931640625" />
                  <Point X="-26.626857421875" Y="4.211570800781" />
                  <Point X="-26.614626953125" Y="4.228252441406" />
                  <Point X="-26.6038046875" Y="4.247" />
                  <Point X="-26.59547265625" Y="4.265938964844" />
                  <Point X="-26.592921875" Y="4.274032226563" />
                  <Point X="-26.56319921875" Y="4.368296386719" />
                  <Point X="-26.5591640625" Y="4.388583007812" />
                  <Point X="-26.55727734375" Y="4.410143066406" />
                  <Point X="-26.557728515625" Y="4.430824707031" />
                  <Point X="-26.584201171875" Y="4.6318984375" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.898294921875" Y="4.815816894531" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.149216796875" Y="4.343463378906" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.96330859375" Y="4.183885253906" />
                  <Point X="-24.93287109375" Y="4.194216796875" />
                  <Point X="-24.905576171875" Y="4.208805664062" />
                  <Point X="-24.88441796875" Y="4.231393554688" />
                  <Point X="-24.86655859375" Y="4.258119628906" />
                  <Point X="-24.853783203125" Y="4.286313964844" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.155958984375" Y="4.831738769531" />
                  <Point X="-24.1134765625" Y="4.821482421875" />
                  <Point X="-23.518974609375" Y="4.677951660156" />
                  <Point X="-23.4926875" Y="4.668416992188" />
                  <Point X="-23.105353515625" Y="4.527928222656" />
                  <Point X="-23.078611328125" Y="4.515421875" />
                  <Point X="-22.705423828125" Y="4.340894042969" />
                  <Point X="-22.679556640625" Y="4.325823730469" />
                  <Point X="-22.319021484375" Y="4.115774902344" />
                  <Point X="-22.294650390625" Y="4.098444335938" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.784140625" Y="2.669354492188" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.859431640625" Y="2.536046875" />
                  <Point X="-22.868623046875" Y="2.509696777344" />
                  <Point X="-22.888392578125" Y="2.435772216797" />
                  <Point X="-22.891580078125" Y="2.413850341797" />
                  <Point X="-22.89225390625" Y="2.389439697266" />
                  <Point X="-22.891607421875" Y="2.375445556641" />
                  <Point X="-22.883900390625" Y="2.311529052734" />
                  <Point X="-22.878556640625" Y="2.289602050781" />
                  <Point X="-22.8702890625" Y="2.267512207031" />
                  <Point X="-22.85992578125" Y="2.247467529297" />
                  <Point X="-22.856521484375" Y="2.242450927734" />
                  <Point X="-22.816966796875" Y="2.184158935547" />
                  <Point X="-22.802375" Y="2.167312988281" />
                  <Point X="-22.784060546875" Y="2.150609130859" />
                  <Point X="-22.7733828125" Y="2.142187744141" />
                  <Point X="-22.71508984375" Y="2.102634277344" />
                  <Point X="-22.695046875" Y="2.092272216797" />
                  <Point X="-22.672958984375" Y="2.084006347656" />
                  <Point X="-22.651041015625" Y="2.078664306641" />
                  <Point X="-22.645541015625" Y="2.078000732422" />
                  <Point X="-22.5816171875" Y="2.070292724609" />
                  <Point X="-22.558892578125" Y="2.070288574219" />
                  <Point X="-22.533626953125" Y="2.073327636719" />
                  <Point X="-22.5204296875" Y="2.075872558594" />
                  <Point X="-22.446505859375" Y="2.095640869141" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.037337890625" Y="2.903497314453" />
                  <Point X="-21.032671875" Y="2.906191162109" />
                  <Point X="-20.876720703125" Y="2.689453125" />
                  <Point X="-20.86314453125" Y="2.667016601562" />
                  <Point X="-20.73780078125" Y="2.459883544922" />
                  <Point X="-21.67932421875" Y="1.737427124023" />
                  <Point X="-21.76921484375" Y="1.668451171875" />
                  <Point X="-21.7817109375" Y="1.65694934082" />
                  <Point X="-21.79553515625" Y="1.64172644043" />
                  <Point X="-21.80060546875" Y="1.635654174805" />
                  <Point X="-21.85380859375" Y="1.56624621582" />
                  <Point X="-21.865318359375" Y="1.546817871094" />
                  <Point X="-21.875494140625" Y="1.523767333984" />
                  <Point X="-21.880076171875" Y="1.510987304688" />
                  <Point X="-21.89989453125" Y="1.440121459961" />
                  <Point X="-21.90334765625" Y="1.417823974609" />
                  <Point X="-21.9041640625" Y="1.394253662109" />
                  <Point X="-21.902259765625" Y="1.371764404297" />
                  <Point X="-21.900859375" Y="1.364978637695" />
                  <Point X="-21.88458984375" Y="1.286131469727" />
                  <Point X="-21.877359375" Y="1.264563964844" />
                  <Point X="-21.866353515625" Y="1.241399169922" />
                  <Point X="-21.85991015625" Y="1.229953979492" />
                  <Point X="-21.815662109375" Y="1.162696655273" />
                  <Point X="-21.801107421875" Y="1.145450683594" />
                  <Point X="-21.78386328125" Y="1.129360717773" />
                  <Point X="-21.765646484375" Y="1.11603112793" />
                  <Point X="-21.760126953125" Y="1.112924804688" />
                  <Point X="-21.69600390625" Y="1.076828857422" />
                  <Point X="-21.6745625" Y="1.06800769043" />
                  <Point X="-21.649125" Y="1.061023681641" />
                  <Point X="-21.636419921875" Y="1.058452392578" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046174926758" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.223162109375" Y="1.216636352539" />
                  <Point X="-20.15405859375" Y="0.93278326416" />
                  <Point X="-20.14978125" Y="0.905313659668" />
                  <Point X="-20.109134765625" Y="0.644238891602" />
                  <Point X="-21.18040234375" Y="0.357193237305" />
                  <Point X="-21.283419921875" Y="0.32958984375" />
                  <Point X="-21.299681640625" Y="0.323595489502" />
                  <Point X="-21.319091796875" Y="0.314350860596" />
                  <Point X="-21.325783203125" Y="0.310830841064" />
                  <Point X="-21.410962890625" Y="0.261595672607" />
                  <Point X="-21.429330078125" Y="0.247764663696" />
                  <Point X="-21.448041015625" Y="0.229739868164" />
                  <Point X="-21.4568671875" Y="0.219972000122" />
                  <Point X="-21.507974609375" Y="0.154848770142" />
                  <Point X="-21.51969921875" Y="0.135566986084" />
                  <Point X="-21.52947265625" Y="0.114102912903" />
                  <Point X="-21.53631640625" Y="0.09261151886" />
                  <Point X="-21.537783203125" Y="0.084955970764" />
                  <Point X="-21.554818359375" Y="-0.00399896574" />
                  <Point X="-21.556369140625" Y="-0.02713318634" />
                  <Point X="-21.55490234375" Y="-0.053597080231" />
                  <Point X="-21.553353515625" Y="-0.066207824707" />
                  <Point X="-21.536318359375" Y="-0.155162918091" />
                  <Point X="-21.52947265625" Y="-0.176663116455" />
                  <Point X="-21.51969921875" Y="-0.198127029419" />
                  <Point X="-21.50797265625" Y="-0.217410476685" />
                  <Point X="-21.50357421875" Y="-0.223014892578" />
                  <Point X="-21.452466796875" Y="-0.288138122559" />
                  <Point X="-21.435677734375" Y="-0.304770111084" />
                  <Point X="-21.41403515625" Y="-0.321427734375" />
                  <Point X="-21.4036328125" Y="-0.328392791748" />
                  <Point X="-21.318453125" Y="-0.377628112793" />
                  <Point X="-21.307291015625" Y="-0.383138275146" />
                  <Point X="-21.283419921875" Y="-0.392149902344" />
                  <Point X="-20.108525390625" Y="-0.706962341309" />
                  <Point X="-20.1449765625" Y="-0.948734558105" />
                  <Point X="-20.150458984375" Y="-0.972756469727" />
                  <Point X="-20.198822265625" Y="-1.18469921875" />
                  <Point X="-21.456146484375" Y="-1.019169555664" />
                  <Point X="-21.5756171875" Y="-1.003440917969" />
                  <Point X="-21.59950390625" Y="-1.003324951172" />
                  <Point X="-21.63103515625" Y="-1.007165405273" />
                  <Point X="-21.6397265625" Y="-1.008635925293" />
                  <Point X="-21.806904296875" Y="-1.044972412109" />
                  <Point X="-21.8372734375" Y="-1.055693359375" />
                  <Point X="-21.869287109375" Y="-1.076807250977" />
                  <Point X="-21.890482421875" Y="-1.098040039062" />
                  <Point X="-21.896294921875" Y="-1.104418457031" />
                  <Point X="-21.99734375" Y="-1.225947753906" />
                  <Point X="-22.01337109375" Y="-1.250415527344" />
                  <Point X="-22.025666015625" Y="-1.283385253906" />
                  <Point X="-22.0305078125" Y="-1.311482299805" />
                  <Point X="-22.031486328125" Y="-1.31891015625" />
                  <Point X="-22.04596875" Y="-1.476296020508" />
                  <Point X="-22.044888671875" Y="-1.508485595703" />
                  <Point X="-22.034251953125" Y="-1.545636352539" />
                  <Point X="-22.01978125" Y="-1.573184814453" />
                  <Point X="-22.015587890625" Y="-1.580382080078" />
                  <Point X="-21.923068359375" Y="-1.724288208008" />
                  <Point X="-21.9130625" Y="-1.737243286133" />
                  <Point X="-21.889369140625" Y="-1.760909301758" />
                  <Point X="-20.786875" Y="-2.606882568359" />
                  <Point X="-20.875177734375" Y="-2.749766845703" />
                  <Point X="-20.88653515625" Y="-2.765905029297" />
                  <Point X="-20.971017578125" Y="-2.885944824219" />
                  <Point X="-22.092638671875" Y="-2.238376220703" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2217265625" Y="-2.167514648437" />
                  <Point X="-22.254962890625" Y="-2.15851953125" />
                  <Point X="-22.262896484375" Y="-2.156732910156" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.494025390625" Y="-2.119082275391" />
                  <Point X="-22.532287109375" Y="-2.126591552734" />
                  <Point X="-22.56218359375" Y="-2.139248535156" />
                  <Point X="-22.569390625" Y="-2.142663330078" />
                  <Point X="-22.73468359375" Y="-2.229655761719" />
                  <Point X="-22.75885546875" Y="-2.246129638672" />
                  <Point X="-22.783146484375" Y="-2.272389648438" />
                  <Point X="-22.7995234375" Y="-2.298683349609" />
                  <Point X="-22.802953125" Y="-2.304663330078" />
                  <Point X="-22.889947265625" Y="-2.469956298828" />
                  <Point X="-22.901267578125" Y="-2.500111816406" />
                  <Point X="-22.905591796875" Y="-2.539170898438" />
                  <Point X="-22.90228515625" Y="-2.572795166016" />
                  <Point X="-22.90123046875" Y="-2.580381347656" />
                  <Point X="-22.865296875" Y="-2.779348632812" />
                  <Point X="-22.861013671875" Y="-2.795139160156" />
                  <Point X="-22.848177734375" Y="-2.826078857422" />
                  <Point X="-22.13871484375" Y="-4.054905517578" />
                  <Point X="-22.21814453125" Y="-4.111640136719" />
                  <Point X="-22.230830078125" Y="-4.1198515625" />
                  <Point X="-22.298234375" Y="-4.163480957031" />
                  <Point X="-23.160255859375" Y="-3.040072998047" />
                  <Point X="-23.241451171875" Y="-2.934255371094" />
                  <Point X="-23.252494140625" Y="-2.922178955078" />
                  <Point X="-23.278076171875" Y="-2.900556152344" />
                  <Point X="-23.29496484375" Y="-2.889698730469" />
                  <Point X="-23.491201171875" Y="-2.763537353516" />
                  <Point X="-23.520255859375" Y="-2.749644287109" />
                  <Point X="-23.55904296875" Y="-2.741946289062" />
                  <Point X="-23.59396875" Y="-2.74242578125" />
                  <Point X="-23.601369140625" Y="-2.74281640625" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84462109375" Y="-2.76853515625" />
                  <Point X="-23.87752734375" Y="-2.783798583984" />
                  <Point X="-23.90462109375" Y="-2.803404296875" />
                  <Point X="-23.9096640625" Y="-2.807319335938" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968861083984" />
                  <Point X="-24.11275" Y="-2.9966875" />
                  <Point X="-24.124376953125" Y="-3.025811279297" />
                  <Point X="-24.128640625" Y="-3.045430175781" />
                  <Point X="-24.17819140625" Y="-3.273399169922" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323119873047" />
                  <Point X="-23.977958984375" Y="-4.859721679688" />
                  <Point X="-23.97793359375" Y="-4.859915039062" />
                  <Point X="-24.024318359375" Y="-4.870083007812" />
                  <Point X="-24.036041015625" Y="-4.872212402344" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.05842578125" Y="-4.75263671875" />
                  <Point X="-26.074875" Y="-4.748404785156" />
                  <Point X="-26.141244140625" Y="-4.731328613281" />
                  <Point X="-26.124595703125" Y="-4.60486328125" />
                  <Point X="-26.1207734375" Y="-4.57583984375" />
                  <Point X="-26.120076171875" Y="-4.568100585938" />
                  <Point X="-26.11944921875" Y="-4.541033203125" />
                  <Point X="-26.1217578125" Y="-4.509323242188" />
                  <Point X="-26.123333984375" Y="-4.497685058594" />
                  <Point X="-26.128130859375" Y="-4.473573242188" />
                  <Point X="-26.18386328125" Y="-4.193393554688" />
                  <Point X="-26.188126953125" Y="-4.178464355469" />
                  <Point X="-26.199029296875" Y="-4.149499023438" />
                  <Point X="-26.20566796875" Y="-4.135462890625" />
                  <Point X="-26.221740234375" Y="-4.107625976562" />
                  <Point X="-26.23057421875" Y="-4.094861083984" />
                  <Point X="-26.250205078125" Y="-4.070940185547" />
                  <Point X="-26.261001953125" Y="-4.059784179688" />
                  <Point X="-26.279484375" Y="-4.04357421875" />
                  <Point X="-26.49426171875" Y="-3.855219238281" />
                  <Point X="-26.506734375" Y="-3.84596875" />
                  <Point X="-26.53301953125" Y="-3.829622558594" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.661345703125" Y="-3.794561767578" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166015625" />
                  <Point X="-27.008005859375" Y="-3.781945556641" />
                  <Point X="-27.0390546875" Y="-3.790265136719" />
                  <Point X="-27.053673828125" Y="-3.795495849609" />
                  <Point X="-27.081865234375" Y="-3.808269042969" />
                  <Point X="-27.0954375" Y="-3.815811523438" />
                  <Point X="-27.11587890625" Y="-3.829469970703" />
                  <Point X="-27.35340234375" Y="-3.9881796875" />
                  <Point X="-27.35968359375" Y="-3.992760009766" />
                  <Point X="-27.380443359375" Y="-4.010134521484" />
                  <Point X="-27.4027578125" Y="-4.032772460938" />
                  <Point X="-27.410470703125" Y="-4.041629638672" />
                  <Point X="-27.503201171875" Y="-4.162478515625" />
                  <Point X="-27.7475859375" Y="-4.011162109375" />
                  <Point X="-27.77034375" Y="-3.993639160156" />
                  <Point X="-27.980861328125" Y="-3.831546386719" />
                  <Point X="-27.400478515625" Y="-2.826291503906" />
                  <Point X="-27.341490234375" Y="-2.724119384766" />
                  <Point X="-27.3348515625" Y="-2.710083984375" />
                  <Point X="-27.32394921875" Y="-2.681117919922" />
                  <Point X="-27.319685546875" Y="-2.666187255859" />
                  <Point X="-27.3134140625" Y="-2.634658935547" />
                  <Point X="-27.311625" Y="-2.618796386719" />
                  <Point X="-27.31073046875" Y="-2.586997070312" />
                  <Point X="-27.315171875" Y="-2.555500244141" />
                  <Point X="-27.32482421875" Y="-2.525188720703" />
                  <Point X="-27.3309296875" Y="-2.510437255859" />
                  <Point X="-27.34651953125" Y="-2.480237060547" />
                  <Point X="-27.3542265625" Y="-2.467772460938" />
                  <Point X="-27.37146484375" Y="-2.444176757812" />
                  <Point X="-27.38099609375" Y="-2.433045654297" />
                  <Point X="-27.39696875" Y="-2.417071777344" />
                  <Point X="-27.40880859375" Y="-2.407026123047" />
                  <Point X="-27.433966796875" Y="-2.388996582031" />
                  <Point X="-27.44728515625" Y="-2.381012695312" />
                  <Point X="-27.476115234375" Y="-2.366794677734" />
                  <Point X="-27.490556640625" Y="-2.361087890625" />
                  <Point X="-27.520173828125" Y="-2.352103027344" />
                  <Point X="-27.550859375" Y="-2.348062744141" />
                  <Point X="-27.581794921875" Y="-2.349074951172" />
                  <Point X="-27.597220703125" Y="-2.350849365234" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.793087890625" Y="-3.017708496094" />
                  <Point X="-29.004017578125" Y="-2.740590087891" />
                  <Point X="-29.02033203125" Y="-2.713234619141" />
                  <Point X="-29.181265625" Y="-2.443373291016" />
                  <Point X="-28.151333984375" Y="-1.653078979492" />
                  <Point X="-28.048123046875" Y="-1.573881713867" />
                  <Point X="-28.035751953125" Y="-1.562516723633" />
                  <Point X="-28.013203125" Y="-1.537783569336" />
                  <Point X="-28.003025390625" Y="-1.524415161133" />
                  <Point X="-27.98444921875" Y="-1.494926391602" />
                  <Point X="-27.977619140625" Y="-1.481963012695" />
                  <Point X="-27.96603515625" Y="-1.455145629883" />
                  <Point X="-27.96128125" Y="-1.441291870117" />
                  <Point X="-27.9541875" Y="-1.413901611328" />
                  <Point X="-27.951552734375" Y="-1.3988046875" />
                  <Point X="-27.948748046875" Y="-1.368381591797" />
                  <Point X="-27.948578125" Y="-1.353055297852" />
                  <Point X="-27.95078515625" Y="-1.321389038086" />
                  <Point X="-27.953205078125" Y="-1.305708007812" />
                  <Point X="-27.960623046875" Y="-1.274969970703" />
                  <Point X="-27.97305078125" Y="-1.245898925781" />
                  <Point X="-27.99014453125" Y="-1.219297973633" />
                  <Point X="-27.99980859375" Y="-1.206711181641" />
                  <Point X="-28.022740234375" Y="-1.181515380859" />
                  <Point X="-28.0331015625" Y="-1.171720336914" />
                  <Point X="-28.055173828125" Y="-1.153791625977" />
                  <Point X="-28.066884765625" Y="-1.145658203125" />
                  <Point X="-28.09126953125" Y="-1.131306884766" />
                  <Point X="-28.105435546875" Y="-1.124479492188" />
                  <Point X="-28.13469921875" Y="-1.113255981445" />
                  <Point X="-28.149796875" Y="-1.108859741211" />
                  <Point X="-28.18168359375" Y="-1.102378417969" />
                  <Point X="-28.197298828125" Y="-1.100532348633" />
                  <Point X="-28.22862109375" Y="-1.09944128418" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-29.66091796875" Y="-1.286694335938" />
                  <Point X="-29.740763671875" Y="-0.974109069824" />
                  <Point X="-29.745076171875" Y="-0.943946716309" />
                  <Point X="-29.786451171875" Y="-0.654654541016" />
                  <Point X="-28.625861328125" Y="-0.343675018311" />
                  <Point X="-28.508287109375" Y="-0.312171264648" />
                  <Point X="-28.49944140625" Y="-0.309331085205" />
                  <Point X="-28.47091015625" Y="-0.297760955811" />
                  <Point X="-28.440939453125" Y="-0.28182901001" />
                  <Point X="-28.431365234375" Y="-0.275989776611" />
                  <Point X="-28.405810546875" Y="-0.25825402832" />
                  <Point X="-28.39289453125" Y="-0.247475891113" />
                  <Point X="-28.3691640625" Y="-0.223810516357" />
                  <Point X="-28.358349609375" Y="-0.210922988892" />
                  <Point X="-28.33837109375" Y="-0.182304763794" />
                  <Point X="-28.330939453125" Y="-0.169688461304" />
                  <Point X="-28.31810546875" Y="-0.143467224121" />
                  <Point X="-28.312703125" Y="-0.129861984253" />
                  <Point X="-28.304185546875" Y="-0.102416992188" />
                  <Point X="-28.30083203125" Y="-0.087423027039" />
                  <Point X="-28.296587890625" Y="-0.057091175079" />
                  <Point X="-28.295697265625" Y="-0.041752998352" />
                  <Point X="-28.2964296875" Y="-0.009750630379" />
                  <Point X="-28.297646484375" Y="0.003389266014" />
                  <Point X="-28.301890625" Y="0.029374191284" />
                  <Point X="-28.30491796875" Y="0.042219070435" />
                  <Point X="-28.313435546875" Y="0.069664070129" />
                  <Point X="-28.31986328125" Y="0.085299789429" />
                  <Point X="-28.335390625" Y="0.115189781189" />
                  <Point X="-28.344490234375" Y="0.129443756104" />
                  <Point X="-28.365935546875" Y="0.157226425171" />
                  <Point X="-28.3756875" Y="0.168033828735" />
                  <Point X="-28.396724609375" Y="0.188030883789" />
                  <Point X="-28.408009765625" Y="0.197220535278" />
                  <Point X="-28.433564453125" Y="0.21495614624" />
                  <Point X="-28.440484375" Y="0.219329681396" />
                  <Point X="-28.465615234375" Y="0.232834442139" />
                  <Point X="-28.49656640625" Y="0.245635925293" />
                  <Point X="-28.508287109375" Y="0.249611297607" />
                  <Point X="-29.7854453125" Y="0.591824768066" />
                  <Point X="-29.73133203125" Y="0.957518615723" />
                  <Point X="-29.722638671875" Y="0.989598327637" />
                  <Point X="-29.6335859375" Y="1.318237060547" />
                  <Point X="-28.862345703125" Y="1.216701416016" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815673828" />
                  <Point X="-28.7470546875" Y="1.204364379883" />
                  <Point X="-28.736703125" Y="1.204703369141" />
                  <Point X="-28.715142578125" Y="1.206589599609" />
                  <Point X="-28.70488671875" Y="1.208053833008" />
                  <Point X="-28.684599609375" Y="1.21208972168" />
                  <Point X="-28.669701171875" Y="1.216194946289" />
                  <Point X="-28.613142578125" Y="1.234027832031" />
                  <Point X="-28.603447265625" Y="1.237677124023" />
                  <Point X="-28.584513671875" Y="1.246008300781" />
                  <Point X="-28.5752734375" Y="1.250690795898" />
                  <Point X="-28.556529296875" Y="1.261513061523" />
                  <Point X="-28.5478515625" Y="1.26717590332" />
                  <Point X="-28.531171875" Y="1.279407470703" />
                  <Point X="-28.51592578125" Y="1.293379516602" />
                  <Point X="-28.502287109375" Y="1.308931762695" />
                  <Point X="-28.495892578125" Y="1.317080932617" />
                  <Point X="-28.483478515625" Y="1.334811035156" />
                  <Point X="-28.4780078125" Y="1.343607055664" />
                  <Point X="-28.468060546875" Y="1.361739624023" />
                  <Point X="-28.461630859375" Y="1.375791503906" />
                  <Point X="-28.4389375" Y="1.430580322266" />
                  <Point X="-28.435501953125" Y="1.440344848633" />
                  <Point X="-28.42971484375" Y="1.460193725586" />
                  <Point X="-28.42736328125" Y="1.470278442383" />
                  <Point X="-28.423603515625" Y="1.491588256836" />
                  <Point X="-28.42236328125" Y="1.501868652344" />
                  <Point X="-28.4210078125" Y="1.522507324219" />
                  <Point X="-28.42190625" Y="1.543176269531" />
                  <Point X="-28.42505078125" Y="1.563618896484" />
                  <Point X="-28.427181640625" Y="1.573750854492" />
                  <Point X="-28.432779296875" Y="1.594653198242" />
                  <Point X="-28.435998046875" Y="1.604494873047" />
                  <Point X="-28.443494140625" Y="1.623780639648" />
                  <Point X="-28.450134765625" Y="1.637762451172" />
                  <Point X="-28.47751953125" Y="1.690365478516" />
                  <Point X="-28.482798828125" Y="1.699283691406" />
                  <Point X="-28.494294921875" Y="1.71648828125" />
                  <Point X="-28.500509765625" Y="1.72477355957" />
                  <Point X="-28.514421875" Y="1.741353027344" />
                  <Point X="-28.521501953125" Y="1.748912719727" />
                  <Point X="-28.536443359375" Y="1.763215698242" />
                  <Point X="-28.5443046875" Y="1.769958862305" />
                  <Point X="-29.22761328125" Y="2.29428125" />
                  <Point X="-29.002291015625" Y="2.680313232422" />
                  <Point X="-28.979275390625" Y="2.709897705078" />
                  <Point X="-28.726337890625" Y="3.035012695312" />
                  <Point X="-28.30591796875" Y="2.792283691406" />
                  <Point X="-28.25415625" Y="2.762398681641" />
                  <Point X="-28.24491796875" Y="2.757716796875" />
                  <Point X="-28.225986328125" Y="2.749386474609" />
                  <Point X="-28.21629296875" Y="2.745738037109" />
                  <Point X="-28.195650390625" Y="2.739229248047" />
                  <Point X="-28.1856171875" Y="2.736657470703" />
                  <Point X="-28.165328125" Y="2.732621582031" />
                  <Point X="-28.14829296875" Y="2.730564453125" />
                  <Point X="-28.0695234375" Y="2.723672851562" />
                  <Point X="-28.059169921875" Y="2.723334228516" />
                  <Point X="-28.038490234375" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310791016" />
                  <Point X="-27.97643359375" Y="2.734227294922" />
                  <Point X="-27.956994140625" Y="2.741302978516" />
                  <Point X="-27.938443359375" Y="2.750451904297" />
                  <Point X="-27.929419921875" Y="2.755531738281" />
                  <Point X="-27.911166015625" Y="2.767160888672" />
                  <Point X="-27.90275" Y="2.773190185547" />
                  <Point X="-27.886623046875" Y="2.786131835938" />
                  <Point X="-27.874099609375" Y="2.797855957031" />
                  <Point X="-27.8181875" Y="2.853767578125" />
                  <Point X="-27.811267578125" Y="2.861486572266" />
                  <Point X="-27.7983125" Y="2.87762890625" />
                  <Point X="-27.79227734375" Y="2.886052246094" />
                  <Point X="-27.7806484375" Y="2.904308837891" />
                  <Point X="-27.7755703125" Y="2.913329833984" />
                  <Point X="-27.766421875" Y="2.931881347656" />
                  <Point X="-27.759345703125" Y="2.951328125" />
                  <Point X="-27.754431640625" Y="2.971419677734" />
                  <Point X="-27.7525234375" Y="2.981594970703" />
                  <Point X="-27.74969921875" Y="3.003056396484" />
                  <Point X="-27.74891015625" Y="3.013377441406" />
                  <Point X="-27.748458984375" Y="3.034040527344" />
                  <Point X="-27.749388671875" Y="3.051177246094" />
                  <Point X="-27.756279296875" Y="3.129947753906" />
                  <Point X="-27.757744140625" Y="3.140205322266" />
                  <Point X="-27.76178125" Y="3.160498046875" />
                  <Point X="-27.764353515625" Y="3.170533203125" />
                  <Point X="-27.77086328125" Y="3.191176025391" />
                  <Point X="-27.77451171875" Y="3.200870605469" />
                  <Point X="-27.782841796875" Y="3.219799072266" />
                  <Point X="-27.7875234375" Y="3.229032958984" />
                  <Point X="-28.05938671875" Y="3.699915039062" />
                  <Point X="-27.648369140625" Y="4.0150390625" />
                  <Point X="-27.6121328125" Y="4.035170410156" />
                  <Point X="-27.1925234375" Y="4.268295898437" />
                  <Point X="-27.13441015625" Y="4.192560546875" />
                  <Point X="-27.118564453125" Y="4.171909667969" />
                  <Point X="-27.111818359375" Y="4.164046875" />
                  <Point X="-27.097515625" Y="4.149106445313" />
                  <Point X="-27.089958984375" Y="4.142028808594" />
                  <Point X="-27.07337890625" Y="4.128115722656" />
                  <Point X="-27.065095703125" Y="4.121900878906" />
                  <Point X="-27.047896484375" Y="4.110407714844" />
                  <Point X="-27.031435546875" Y="4.101201660156" />
                  <Point X="-26.943763671875" Y="4.0555625" />
                  <Point X="-26.934326171875" Y="4.051286621094" />
                  <Point X="-26.915044921875" Y="4.0437890625" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031378173828" />
                  <Point X="-26.780818359375" Y="4.035136474609" />
                  <Point X="-26.770724609375" Y="4.037489501953" />
                  <Point X="-26.750857421875" Y="4.043281005859" />
                  <Point X="-26.7332265625" Y="4.049974609375" />
                  <Point X="-26.64191015625" Y="4.087798828125" />
                  <Point X="-26.63257421875" Y="4.092277587891" />
                  <Point X="-26.614439453125" Y="4.102227539063" />
                  <Point X="-26.605640625" Y="4.10769921875" />
                  <Point X="-26.58791015625" Y="4.120116210938" />
                  <Point X="-26.579759765625" Y="4.126514160156" />
                  <Point X="-26.5642109375" Y="4.140153320312" />
                  <Point X="-26.5502421875" Y="4.155399414062" />
                  <Point X="-26.53801171875" Y="4.172081054688" />
                  <Point X="-26.5323515625" Y="4.1807578125" />
                  <Point X="-26.521529296875" Y="4.199505371094" />
                  <Point X="-26.51684765625" Y="4.208744140625" />
                  <Point X="-26.508515625" Y="4.227683105469" />
                  <Point X="-26.502318359375" Y="4.245463867188" />
                  <Point X="-26.472595703125" Y="4.339728027344" />
                  <Point X="-26.470025390625" Y="4.349763183594" />
                  <Point X="-26.465990234375" Y="4.370049804688" />
                  <Point X="-26.464525390625" Y="4.380301269531" />
                  <Point X="-26.462638671875" Y="4.401861328125" />
                  <Point X="-26.46230078125" Y="4.41221484375" />
                  <Point X="-26.462751953125" Y="4.432896484375" />
                  <Point X="-26.463541015625" Y="4.443225097656" />
                  <Point X="-26.479265625" Y="4.562655273438" />
                  <Point X="-25.931173828125" Y="4.7163203125" />
                  <Point X="-25.887251953125" Y="4.7214609375" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.24098046875" Y="4.318875488281" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.978369140625" Y="4.085113037109" />
                  <Point X="-24.947833984375" Y="4.090154052734" />
                  <Point X="-24.9327734375" Y="4.093926269531" />
                  <Point X="-24.9023359375" Y="4.1042578125" />
                  <Point X="-24.88808984375" Y="4.11043359375" />
                  <Point X="-24.860794921875" Y="4.125022460938" />
                  <Point X="-24.8362421875" Y="4.143860351562" />
                  <Point X="-24.815083984375" Y="4.166448242188" />
                  <Point X="-24.8054296875" Y="4.178611328125" />
                  <Point X="-24.7875703125" Y="4.205337402344" />
                  <Point X="-24.78002734375" Y="4.218910644531" />
                  <Point X="-24.767251953125" Y="4.247104980469" />
                  <Point X="-24.76201953125" Y="4.261726074219" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.172123046875" Y="4.737912109375" />
                  <Point X="-24.135771484375" Y="4.729135742188" />
                  <Point X="-23.54640234375" Y="4.586844238281" />
                  <Point X="-23.525080078125" Y="4.579110351562" />
                  <Point X="-23.14174609375" Y="4.440072753906" />
                  <Point X="-23.11885546875" Y="4.4293671875" />
                  <Point X="-22.74954296875" Y="4.256651367187" />
                  <Point X="-22.72737890625" Y="4.243738769531" />
                  <Point X="-22.370568359375" Y="4.035860107422" />
                  <Point X="-22.349705078125" Y="4.021023681641" />
                  <Point X="-22.182216796875" Y="3.901915527344" />
                  <Point X="-22.866412109375" Y="2.716854492188" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.938521484375" Y="2.591233642578" />
                  <Point X="-22.949130859375" Y="2.567335693359" />
                  <Point X="-22.958322265625" Y="2.540985595703" />
                  <Point X="-22.9603984375" Y="2.534239990234" />
                  <Point X="-22.98016796875" Y="2.460315429688" />
                  <Point X="-22.982404296875" Y="2.449441650391" />
                  <Point X="-22.985591796875" Y="2.427519775391" />
                  <Point X="-22.98654296875" Y="2.416471679688" />
                  <Point X="-22.987216796875" Y="2.392061035156" />
                  <Point X="-22.98715234375" Y="2.385055664062" />
                  <Point X="-22.985923828125" Y="2.364072753906" />
                  <Point X="-22.978216796875" Y="2.30015625" />
                  <Point X="-22.97619921875" Y="2.289035400391" />
                  <Point X="-22.97085546875" Y="2.267108398438" />
                  <Point X="-22.967529296875" Y="2.256302246094" />
                  <Point X="-22.95926171875" Y="2.234212402344" />
                  <Point X="-22.954677734375" Y="2.223882568359" />
                  <Point X="-22.944314453125" Y="2.203837890625" />
                  <Point X="-22.935130859375" Y="2.189106445312" />
                  <Point X="-22.895576171875" Y="2.130814453125" />
                  <Point X="-22.8887734375" Y="2.121959960938" />
                  <Point X="-22.874181640625" Y="2.105114013672" />
                  <Point X="-22.866392578125" Y="2.097122558594" />
                  <Point X="-22.848078125" Y="2.080418701172" />
                  <Point X="-22.842890625" Y="2.076016845703" />
                  <Point X="-22.82672265625" Y="2.063575927734" />
                  <Point X="-22.7684296875" Y="2.024022583008" />
                  <Point X="-22.75871875" Y="2.018244995117" />
                  <Point X="-22.73867578125" Y="2.00788293457" />
                  <Point X="-22.72834375" Y="2.003298339844" />
                  <Point X="-22.706255859375" Y="1.995032470703" />
                  <Point X="-22.695455078125" Y="1.991708251953" />
                  <Point X="-22.673537109375" Y="1.986366210938" />
                  <Point X="-22.656919921875" Y="1.983684692383" />
                  <Point X="-22.59299609375" Y="1.97597668457" />
                  <Point X="-22.581634765625" Y="1.975292724609" />
                  <Point X="-22.55891015625" Y="1.975288574219" />
                  <Point X="-22.547546875" Y="1.975968505859" />
                  <Point X="-22.52228125" Y="1.979007568359" />
                  <Point X="-22.515638671875" Y="1.980046142578" />
                  <Point X="-22.49588671875" Y="1.984097290039" />
                  <Point X="-22.421962890625" Y="2.003865600586" />
                  <Point X="-22.416" Y="2.005671875" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572875977" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.05959375" Y="2.780951171875" />
                  <Point X="-20.95604296875" Y="2.637037841797" />
                  <Point X="-20.944423828125" Y="2.617835449219" />
                  <Point X="-20.863115234375" Y="2.483471191406" />
                  <Point X="-21.73715625" Y="1.812795654297" />
                  <Point X="-21.827046875" Y="1.743819702148" />
                  <Point X="-21.83355078125" Y="1.738349487305" />
                  <Point X="-21.8520390625" Y="1.720815917969" />
                  <Point X="-21.86586328125" Y="1.705593017578" />
                  <Point X="-21.87600390625" Y="1.693448608398" />
                  <Point X="-21.92920703125" Y="1.624040649414" />
                  <Point X="-21.93554296875" Y="1.614666992188" />
                  <Point X="-21.947052734375" Y="1.595238769531" />
                  <Point X="-21.9522265625" Y="1.585183959961" />
                  <Point X="-21.96240234375" Y="1.562133422852" />
                  <Point X="-21.964919921875" Y="1.555829345703" />
                  <Point X="-21.97156640625" Y="1.536573242188" />
                  <Point X="-21.991384765625" Y="1.465707519531" />
                  <Point X="-21.993775390625" Y="1.454660400391" />
                  <Point X="-21.997228515625" Y="1.432362915039" />
                  <Point X="-21.998291015625" Y="1.421112548828" />
                  <Point X="-21.999107421875" Y="1.397542236328" />
                  <Point X="-21.998826171875" Y="1.38623815918" />
                  <Point X="-21.996921875" Y="1.363748779297" />
                  <Point X="-21.9938984375" Y="1.345777954102" />
                  <Point X="-21.97762890625" Y="1.266930786133" />
                  <Point X="-21.974662109375" Y="1.255934692383" />
                  <Point X="-21.967431640625" Y="1.23436706543" />
                  <Point X="-21.96316796875" Y="1.223795776367" />
                  <Point X="-21.952162109375" Y="1.200630859375" />
                  <Point X="-21.939275390625" Y="1.177740600586" />
                  <Point X="-21.89502734375" Y="1.110483276367" />
                  <Point X="-21.888263671875" Y="1.101425537109" />
                  <Point X="-21.873708984375" Y="1.08417956543" />
                  <Point X="-21.86591796875" Y="1.075991210938" />
                  <Point X="-21.848673828125" Y="1.059901245117" />
                  <Point X="-21.839962890625" Y="1.052693237305" />
                  <Point X="-21.82174609375" Y="1.039363647461" />
                  <Point X="-21.806720703125" Y="1.030135253906" />
                  <Point X="-21.74259765625" Y="0.994039306641" />
                  <Point X="-21.7321484375" Y="0.988973388672" />
                  <Point X="-21.71070703125" Y="0.980152160645" />
                  <Point X="-21.69971484375" Y="0.976397766113" />
                  <Point X="-21.67427734375" Y="0.969413696289" />
                  <Point X="-21.6488671875" Y="0.964271362305" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.55596875" Y="0.952199768066" />
                  <Point X="-21.53428125" Y="0.951222717285" />
                  <Point X="-21.50878125" Y="0.952032409668" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-20.295298828125" Y="1.111319580078" />
                  <Point X="-20.247310546875" Y="0.914204101562" />
                  <Point X="-20.243650390625" Y="0.89069708252" />
                  <Point X="-20.21612890625" Y="0.713921264648" />
                  <Point X="-21.204990234375" Y="0.448956176758" />
                  <Point X="-21.3080078125" Y="0.421352752686" />
                  <Point X="-21.31627734375" Y="0.418726745605" />
                  <Point X="-21.34053125" Y="0.409364379883" />
                  <Point X="-21.35994140625" Y="0.400119750977" />
                  <Point X="-21.37332421875" Y="0.393079620361" />
                  <Point X="-21.45850390625" Y="0.343844421387" />
                  <Point X="-21.468109375" Y="0.337485168457" />
                  <Point X="-21.4864765625" Y="0.323654174805" />
                  <Point X="-21.49523828125" Y="0.31618258667" />
                  <Point X="-21.51394921875" Y="0.298157775879" />
                  <Point X="-21.5316015625" Y="0.278621887207" />
                  <Point X="-21.582708984375" Y="0.21349861145" />
                  <Point X="-21.589146484375" Y="0.20420640564" />
                  <Point X="-21.60087109375" Y="0.184924667358" />
                  <Point X="-21.606158203125" Y="0.174935134888" />
                  <Point X="-21.615931640625" Y="0.15347102356" />
                  <Point X="-21.619994140625" Y="0.142928604126" />
                  <Point X="-21.626837890625" Y="0.121437141418" />
                  <Point X="-21.6310859375" Y="0.102832679749" />
                  <Point X="-21.64812109375" Y="0.01387773037" />
                  <Point X="-21.64960546875" Y="0.002354999542" />
                  <Point X="-21.65115625" Y="-0.020779190063" />
                  <Point X="-21.65122265625" Y="-0.032390651703" />
                  <Point X="-21.649755859375" Y="-0.058854587555" />
                  <Point X="-21.646658203125" Y="-0.084075889587" />
                  <Point X="-21.629623046875" Y="-0.173030990601" />
                  <Point X="-21.62683984375" Y="-0.18398538208" />
                  <Point X="-21.619994140625" Y="-0.205485610962" />
                  <Point X="-21.615931640625" Y="-0.216031600952" />
                  <Point X="-21.606158203125" Y="-0.237495422363" />
                  <Point X="-21.600869140625" Y="-0.247487625122" />
                  <Point X="-21.589142578125" Y="-0.266770996094" />
                  <Point X="-21.578306640625" Y="-0.281666442871" />
                  <Point X="-21.52719921875" Y="-0.346789703369" />
                  <Point X="-21.519326171875" Y="-0.355628295898" />
                  <Point X="-21.502537109375" Y="-0.372260253906" />
                  <Point X="-21.49362109375" Y="-0.380053436279" />
                  <Point X="-21.471978515625" Y="-0.396711090088" />
                  <Point X="-21.451173828125" Y="-0.41064151001" />
                  <Point X="-21.365994140625" Y="-0.459876708984" />
                  <Point X="-21.360505859375" Y="-0.462813934326" />
                  <Point X="-21.34084375" Y="-0.472015930176" />
                  <Point X="-21.31697265625" Y="-0.481027526855" />
                  <Point X="-21.3080078125" Y="-0.483912872314" />
                  <Point X="-20.215119140625" Y="-0.776751586914" />
                  <Point X="-20.238380859375" Y="-0.931041137695" />
                  <Point X="-20.243078125" Y="-0.951618530273" />
                  <Point X="-20.2721953125" Y="-1.079219604492" />
                  <Point X="-21.44374609375" Y="-0.924982299805" />
                  <Point X="-21.563216796875" Y="-0.909253723145" />
                  <Point X="-21.57515625" Y="-0.908442077637" />
                  <Point X="-21.59904296875" Y="-0.90832598877" />
                  <Point X="-21.610990234375" Y="-0.909021850586" />
                  <Point X="-21.642521484375" Y="-0.912862243652" />
                  <Point X="-21.659904296875" Y="-0.915803344727" />
                  <Point X="-21.82708203125" Y="-0.952139892578" />
                  <Point X="-21.838529296875" Y="-0.955390563965" />
                  <Point X="-21.8688984375" Y="-0.966111450195" />
                  <Point X="-21.889578125" Y="-0.976388305664" />
                  <Point X="-21.921591796875" Y="-0.997502075195" />
                  <Point X="-21.936521484375" Y="-1.00969140625" />
                  <Point X="-21.957716796875" Y="-1.030924316406" />
                  <Point X="-21.969341796875" Y="-1.043681030273" />
                  <Point X="-22.070390625" Y="-1.165210327148" />
                  <Point X="-22.0768125" Y="-1.173892578125" />
                  <Point X="-22.09283984375" Y="-1.198360473633" />
                  <Point X="-22.1023828125" Y="-1.217221557617" />
                  <Point X="-22.114677734375" Y="-1.25019128418" />
                  <Point X="-22.119287109375" Y="-1.267252197266" />
                  <Point X="-22.12412890625" Y="-1.295349121094" />
                  <Point X="-22.1260859375" Y="-1.310205200195" />
                  <Point X="-22.140568359375" Y="-1.467591064453" />
                  <Point X="-22.140916015625" Y="-1.479481811523" />
                  <Point X="-22.1398359375" Y="-1.511671386719" />
                  <Point X="-22.13621875" Y="-1.534634643555" />
                  <Point X="-22.12558203125" Y="-1.57178527832" />
                  <Point X="-22.11835546875" Y="-1.589814086914" />
                  <Point X="-22.103884765625" Y="-1.617362426758" />
                  <Point X="-22.095498046875" Y="-1.631757324219" />
                  <Point X="-22.002978515625" Y="-1.775663574219" />
                  <Point X="-21.99825390625" Y="-1.782358032227" />
                  <Point X="-21.98019921875" Y="-1.80445715332" />
                  <Point X="-21.956505859375" Y="-1.828123291016" />
                  <Point X="-21.947201171875" Y="-1.836277832031" />
                  <Point X="-20.912826171875" Y="-2.629980957031" />
                  <Point X="-20.95449609375" Y="-2.697406982422" />
                  <Point X="-20.964224609375" Y="-2.711230224609" />
                  <Point X="-20.998724609375" Y="-2.760251220703" />
                  <Point X="-22.045138671875" Y="-2.156103759766" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.162580078125" Y="-2.089220214844" />
                  <Point X="-22.18526171875" Y="-2.079791748047" />
                  <Point X="-22.196908203125" Y="-2.075813720703" />
                  <Point X="-22.23014453125" Y="-2.066818603516" />
                  <Point X="-22.24601171875" Y="-2.063245361328" />
                  <Point X="-22.444978515625" Y="-2.027311889648" />
                  <Point X="-22.456798828125" Y="-2.025934570313" />
                  <Point X="-22.4889609375" Y="-2.024217407227" />
                  <Point X="-22.5123203125" Y="-2.025860717773" />
                  <Point X="-22.55058203125" Y="-2.033369873047" />
                  <Point X="-22.56932421875" Y="-2.039108520508" />
                  <Point X="-22.599220703125" Y="-2.051765625" />
                  <Point X="-22.613634765625" Y="-2.058595214844" />
                  <Point X="-22.778927734375" Y="-2.145587646484" />
                  <Point X="-22.788185546875" Y="-2.151153564453" />
                  <Point X="-22.812357421875" Y="-2.167627441406" />
                  <Point X="-22.82859375" Y="-2.181619873047" />
                  <Point X="-22.852884765625" Y="-2.207879882812" />
                  <Point X="-22.86378515625" Y="-2.222164550781" />
                  <Point X="-22.880162109375" Y="-2.248458251953" />
                  <Point X="-22.887021484375" Y="-2.260418212891" />
                  <Point X="-22.974015625" Y="-2.425711181641" />
                  <Point X="-22.97888671875" Y="-2.436568603516" />
                  <Point X="-22.99020703125" Y="-2.466724121094" />
                  <Point X="-22.99569140625" Y="-2.489658203125" />
                  <Point X="-23.000015625" Y="-2.528717285156" />
                  <Point X="-23.000134765625" Y="-2.548468505859" />
                  <Point X="-22.996828125" Y="-2.582092773438" />
                  <Point X="-22.99471875" Y="-2.597265136719" />
                  <Point X="-22.95878515625" Y="-2.796232421875" />
                  <Point X="-22.956984375" Y="-2.80421875" />
                  <Point X="-22.94876171875" Y="-2.831543212891" />
                  <Point X="-22.93592578125" Y="-2.862482910156" />
                  <Point X="-22.93044921875" Y="-2.873578857422" />
                  <Point X="-22.264103515625" Y="-4.027722900391" />
                  <Point X="-22.276244140625" Y="-4.036082763672" />
                  <Point X="-23.08488671875" Y="-2.982240722656" />
                  <Point X="-23.16608203125" Y="-2.876423095703" />
                  <Point X="-23.17134375" Y="-2.870146972656" />
                  <Point X="-23.19116796875" Y="-2.849624267578" />
                  <Point X="-23.21675" Y="-2.828001464844" />
                  <Point X="-23.226703125" Y="-2.820645019531" />
                  <Point X="-23.243591796875" Y="-2.809787597656" />
                  <Point X="-23.439828125" Y="-2.683626220703" />
                  <Point X="-23.45021875" Y="-2.677831542969" />
                  <Point X="-23.4792734375" Y="-2.663938476562" />
                  <Point X="-23.50176171875" Y="-2.656461669922" />
                  <Point X="-23.540548828125" Y="-2.648763671875" />
                  <Point X="-23.56034765625" Y="-2.646955322266" />
                  <Point X="-23.5952734375" Y="-2.647434814453" />
                  <Point X="-23.61007421875" Y="-2.648216064453" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.835375" Y="-2.669565185547" />
                  <Point X="-23.864009765625" Y="-2.675534667969" />
                  <Point X="-23.884595703125" Y="-2.682354736328" />
                  <Point X="-23.917501953125" Y="-2.697618164062" />
                  <Point X="-23.933220703125" Y="-2.706835449219" />
                  <Point X="-23.960314453125" Y="-2.726441162109" />
                  <Point X="-23.970400390625" Y="-2.734271240234" />
                  <Point X="-24.136123046875" Y="-2.872063476562" />
                  <Point X="-24.147345703125" Y="-2.883087646484" />
                  <Point X="-24.16781640625" Y="-2.906837158203" />
                  <Point X="-24.177064453125" Y="-2.9195625" />
                  <Point X="-24.19395703125" Y="-2.947388916016" />
                  <Point X="-24.200978515625" Y="-2.961464355469" />
                  <Point X="-24.21260546875" Y="-2.990588134766" />
                  <Point X="-24.2172109375" Y="-3.005636474609" />
                  <Point X="-24.221474609375" Y="-3.025255371094" />
                  <Point X="-24.271025390625" Y="-3.253224365234" />
                  <Point X="-24.27241796875" Y="-3.261298828125" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323169677734" />
                  <Point X="-24.2744453125" Y="-3.335520019531" />
                  <Point X="-24.16691015625" Y="-4.152324707031" />
                  <Point X="-24.313189453125" Y="-3.606400146484" />
                  <Point X="-24.344931640625" Y="-3.487937011719" />
                  <Point X="-24.347392578125" Y="-3.480122314453" />
                  <Point X="-24.35785546875" Y="-3.453576416016" />
                  <Point X="-24.37321484375" Y="-3.423811279297" />
                  <Point X="-24.37959375" Y="-3.413205810547" />
                  <Point X="-24.392568359375" Y="-3.394512939453" />
                  <Point X="-24.543322265625" Y="-3.177305419922" />
                  <Point X="-24.553330078125" Y="-3.165170166016" />
                  <Point X="-24.575212890625" Y="-3.142716308594" />
                  <Point X="-24.587087890625" Y="-3.132397705078" />
                  <Point X="-24.61334375" Y="-3.113153808594" />
                  <Point X="-24.6267578125" Y="-3.104936767578" />
                  <Point X="-24.6547578125" Y="-3.090829345703" />
                  <Point X="-24.66934375" Y="-3.084938964844" />
                  <Point X="-24.689419921875" Y="-3.078708007812" />
                  <Point X="-24.922703125" Y="-3.006305664062" />
                  <Point X="-24.936623046875" Y="-3.003109619141" />
                  <Point X="-24.964783203125" Y="-2.998840087891" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.0510625" Y="-3.003108886719" />
                  <Point X="-25.06498046875" Y="-3.0063046875" />
                  <Point X="-25.085056640625" Y="-3.012535400391" />
                  <Point X="-25.31833984375" Y="-3.084937988281" />
                  <Point X="-25.33292578125" Y="-3.090828369141" />
                  <Point X="-25.360927734375" Y="-3.104936279297" />
                  <Point X="-25.37434375" Y="-3.113153808594" />
                  <Point X="-25.400599609375" Y="-3.132397705078" />
                  <Point X="-25.4124765625" Y="-3.142718017578" />
                  <Point X="-25.43436328125" Y="-3.165177001953" />
                  <Point X="-25.444373046875" Y="-3.177315673828" />
                  <Point X="-25.457345703125" Y="-3.196008300781" />
                  <Point X="-25.608099609375" Y="-3.413216064453" />
                  <Point X="-25.61247265625" Y="-3.420136962891" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213378906" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.863822521384" Y="4.663479052241" />
                  <Point X="-24.647041275188" Y="4.690829653805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.347195942299" Y="4.715279593543" />
                  <Point X="-25.803820920168" Y="4.73122528914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.456381614355" Y="4.554192995537" />
                  <Point X="-24.672275909752" Y="4.596652959953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.321484840526" Y="4.619323835377" />
                  <Point X="-26.171292445004" Y="4.648999770845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.166381177117" Y="4.449008050407" />
                  <Point X="-24.697510544315" Y="4.502476266101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.295773738754" Y="4.52336807721" />
                  <Point X="-26.472791309312" Y="4.564470436479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.940745512476" Y="4.346070772665" />
                  <Point X="-24.722745178879" Y="4.40829957225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.270062636981" Y="4.427412319043" />
                  <Point X="-26.466962146461" Y="4.469208970918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.727056036804" Y="4.243550665034" />
                  <Point X="-24.747979813442" Y="4.314122878398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.244351535209" Y="4.331456560876" />
                  <Point X="-26.465411935974" Y="4.374096929667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.553491851736" Y="4.142431763427" />
                  <Point X="-24.779459306785" Y="4.220164259821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.215121253456" Y="4.235377910237" />
                  <Point X="-26.491445047941" Y="4.279948119261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.379927666669" Y="4.04131286182" />
                  <Point X="-24.857149534287" Y="4.127819355639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.143573008397" Y="4.137821483755" />
                  <Point X="-26.529204901865" Y="4.186208815707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.146065378367" Y="4.207750058216" />
                  <Point X="-27.292309501961" Y="4.212857015545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.237576113137" Y="3.941283929324" />
                  <Point X="-26.628319406616" Y="4.094612063773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.045715297256" Y="4.109187849456" />
                  <Point X="-27.45328867274" Y="4.123420625353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.214827788358" Y="3.845431633609" />
                  <Point X="-27.614267917113" Y="4.03398423773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.26862486614" Y="3.752252362253" />
                  <Point X="-27.741833045886" Y="3.943381003479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.322421943921" Y="3.659073090898" />
                  <Point X="-27.86041632143" Y="3.852464116001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.376219021702" Y="3.565893819542" />
                  <Point X="-27.978999596973" Y="3.761547228524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.430016099484" Y="3.472714548186" />
                  <Point X="-28.041345398529" Y="3.66866648518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.483813177265" Y="3.37953527683" />
                  <Point X="-27.985334553565" Y="3.571652636666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.537610255046" Y="3.286356005474" />
                  <Point X="-27.9293237086" Y="3.474638788151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.591407332827" Y="3.193176734118" />
                  <Point X="-27.873312863636" Y="3.377624939637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.645204410609" Y="3.099997462762" />
                  <Point X="-27.817302018671" Y="3.280611091122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.69900148839" Y="3.006818191407" />
                  <Point X="-27.768553265861" Y="3.183850840454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.752798566171" Y="2.913638920051" />
                  <Point X="-27.752630547368" Y="3.088236900163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.043726350169" Y="2.758898896443" />
                  <Point X="-21.094705951507" Y="2.76067914335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.806595643952" Y="2.820459648695" />
                  <Point X="-27.75100649987" Y="2.993122280467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.711900786629" Y="3.026677448361" />
                  <Point X="-28.732269287985" Y="3.027388732101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.973565887263" Y="2.661390932382" />
                  <Point X="-21.249960645887" Y="2.671042850036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.860392721734" Y="2.727280377339" />
                  <Point X="-27.783894433039" Y="2.899212845692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.5366556414" Y="2.925499846331" />
                  <Point X="-28.804267702071" Y="2.934845065415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.911955950037" Y="2.564181559257" />
                  <Point X="-21.405215340267" Y="2.581406556723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.914189741233" Y="2.634101103948" />
                  <Point X="-27.864969349228" Y="2.806986137443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.361410496172" Y="2.824322244302" />
                  <Point X="-28.876266116157" Y="2.842301398729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.883125371811" Y="2.468116866572" />
                  <Point X="-21.560470034646" Y="2.491770263409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.958444451737" Y="2.540588605784" />
                  <Point X="-28.948264530243" Y="2.749757732042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.001614788879" Y="2.377196701484" />
                  <Point X="-21.715724729026" Y="2.402133970096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.982849043656" Y="2.446382926204" />
                  <Point X="-29.015863399248" Y="2.657060429856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.120104205947" Y="2.286276536396" />
                  <Point X="-21.870979423406" Y="2.312497676782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.984393212542" Y="2.351378943062" />
                  <Point X="-29.070239232322" Y="2.563901369081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.238593623016" Y="2.195356371309" />
                  <Point X="-22.026234117786" Y="2.222861383469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.967313095532" Y="2.255724585524" />
                  <Point X="-29.124615065396" Y="2.470742308305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.357083040084" Y="2.104436206221" />
                  <Point X="-22.181488812166" Y="2.133225090156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.914583255644" Y="2.158825312232" />
                  <Point X="-29.178990898471" Y="2.377583247529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.475572457152" Y="2.013516041133" />
                  <Point X="-22.336743506545" Y="2.043588796842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.822253024877" Y="2.060543162818" />
                  <Point X="-29.213880529494" Y="2.283743713584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.594061874221" Y="1.922595876046" />
                  <Point X="-29.084092353901" Y="2.184153503913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.712551291289" Y="1.831675710958" />
                  <Point X="-28.954304178308" Y="2.084563294242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.830704034996" Y="1.740743788977" />
                  <Point X="-28.824516002715" Y="1.984973084571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.910479939604" Y="1.648471718245" />
                  <Point X="-28.694727827122" Y="1.8853828749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.965095375254" Y="1.555321024575" />
                  <Point X="-28.564939651529" Y="1.785792665229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.992356934068" Y="1.461215112478" />
                  <Point X="-28.47609665395" Y="1.687632292681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.997139952485" Y="1.366324232453" />
                  <Point X="-28.431808415504" Y="1.591027806606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.97838839583" Y="1.270611506958" />
                  <Point X="-28.423111549765" Y="1.495666198654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.936883032923" Y="1.174104201038" />
                  <Point X="-28.450949176881" Y="1.401580403306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.273298761878" Y="1.020952651471" />
                  <Point X="-20.833191923893" Y="1.040504551521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.866485767934" Y="1.076587967666" />
                  <Point X="-28.502811694517" Y="1.308333575621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.249958233947" Y="0.925079675567" />
                  <Point X="-21.403858788468" Y="0.965374770847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.696829340209" Y="0.975605527953" />
                  <Point X="-28.661396018231" Y="1.218813555526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.956724915429" Y="1.229126667869" />
                  <Point X="-29.651161384207" Y="1.253376923722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.234117987806" Y="0.829468615274" />
                  <Point X="-29.676678201711" Y="1.159210083916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.219237923722" Y="0.733891085278" />
                  <Point X="-29.702195019214" Y="1.06504324411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.464408831253" Y="0.647394735317" />
                  <Point X="-29.727712168404" Y="0.970876415886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.778265476007" Y="0.563296944154" />
                  <Point X="-29.743340786184" Y="0.876364272537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.092122120761" Y="0.47919915299" />
                  <Point X="-29.757334599535" Y="0.781795040559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.37175300038" Y="0.393906171772" />
                  <Point X="-29.771328412886" Y="0.687225808581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.50828321808" Y="0.303616005325" />
                  <Point X="-29.785322226237" Y="0.592656576603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.584292828381" Y="0.211212412697" />
                  <Point X="-29.381109176329" Y="0.483483239153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.627698003626" Y="0.117670248108" />
                  <Point X="-28.973185036165" Y="0.374180307576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.646323828843" Y="0.023262769548" />
                  <Point X="-28.565260896002" Y="0.264877375998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.648174450618" Y="-0.071730512024" />
                  <Point X="-28.37118329025" Y="0.163042129956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.630701669288" Y="-0.167398581701" />
                  <Point X="-28.312275783761" Y="0.065927127792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.590921538727" Y="-0.263845641179" />
                  <Point X="-28.295973114605" Y="-0.029700080668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.513283181107" Y="-0.361614739078" />
                  <Point X="-28.31095670738" Y="-0.124234748787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.362117393565" Y="-0.461951471408" />
                  <Point X="-28.363823753197" Y="-0.217446497574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.986218606578" Y="-0.570136053009" />
                  <Point X="-28.495893740344" Y="-0.307892418705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.578294643646" Y="-0.679438978397" />
                  <Point X="-28.806587982327" Y="-0.392100643407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.216683051118" Y="-0.787124640174" />
                  <Point X="-29.120444777071" Y="-0.476198429332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.230939554414" Y="-0.881684698817" />
                  <Point X="-29.434301571815" Y="-0.560296215258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.248669882845" Y="-0.976123448813" />
                  <Point X="-21.346485435898" Y="-0.93778688494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.703661209857" Y="-0.92531403207" />
                  <Point X="-29.748158366559" Y="-0.644394001184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.270189590165" Y="-1.070429870782" />
                  <Point X="-20.363786740419" Y="-1.067161386273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.93898013755" Y="-1.012154420747" />
                  <Point X="-29.774454660272" Y="-0.738533621081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.019819382242" Y="-1.104389358826" />
                  <Point X="-29.76079109455" Y="-0.834068670018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.09190286838" Y="-1.196930054731" />
                  <Point X="-29.747127528829" Y="-0.929603718956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.123360392666" Y="-1.290889440485" />
                  <Point X="-29.727677203529" Y="-1.02534084599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.133024566951" Y="-1.385609866791" />
                  <Point X="-28.024474497185" Y="-1.179875901805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.67658143547" Y="-1.157103825729" />
                  <Point X="-29.703177400184" Y="-1.121254304684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.140885432989" Y="-1.480393266008" />
                  <Point X="-27.960089174143" Y="-1.277182193538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.247248164133" Y="-1.232233611149" />
                  <Point X="-29.678677596838" Y="-1.217167763378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.123874506131" Y="-1.576045207372" />
                  <Point X="-27.949139012658" Y="-1.372622488311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.068969355326" Y="-1.673020444196" />
                  <Point X="-27.971118109496" Y="-1.466912868045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.006451579142" Y="-1.770261519755" />
                  <Point X="-28.033275338028" Y="-1.559800196504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.904723796116" Y="-1.868871838926" />
                  <Point X="-28.148412749091" Y="-1.650837416221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.774935168332" Y="-1.968462064387" />
                  <Point X="-28.266902191817" Y="-1.741757580412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.645146540547" Y="-2.068052289849" />
                  <Point X="-22.357048190908" Y="-2.043192136416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.559564227079" Y="-2.036120120599" />
                  <Point X="-28.385391660333" Y="-1.832677743703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.515357912762" Y="-2.167642515311" />
                  <Point X="-22.057972902265" Y="-2.14869398234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.739602281539" Y="-2.124890959908" />
                  <Point X="-28.50388112885" Y="-1.923597906995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.385569284978" Y="-2.267232740773" />
                  <Point X="-21.882728059275" Y="-2.249871573815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.858915024031" Y="-2.215782373838" />
                  <Point X="-28.622370597367" Y="-2.014518070286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.255780657193" Y="-2.366822966235" />
                  <Point X="-21.707483343848" Y="-2.351049160836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.912572589381" Y="-2.308966517075" />
                  <Point X="-28.740860065884" Y="-2.105438233577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.125992029408" Y="-2.466413191697" />
                  <Point X="-21.532238628421" Y="-2.452226747856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.961698937095" Y="-2.402308893919" />
                  <Point X="-28.859349534401" Y="-2.196358396868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.996203401624" Y="-2.566003417159" />
                  <Point X="-21.356993912994" Y="-2.553404334877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.996410624941" Y="-2.496154641778" />
                  <Point X="-28.977839002918" Y="-2.287278560159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.933389737435" Y="-2.663254825355" />
                  <Point X="-21.181749197567" Y="-2.654581921897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.995556077134" Y="-2.591242389953" />
                  <Point X="-27.376535285396" Y="-2.438255224873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.765894743955" Y="-2.424658492971" />
                  <Point X="-29.096328471435" Y="-2.37819872345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.995825875369" Y="-2.756132414083" />
                  <Point X="-21.00650448214" Y="-2.755759508918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.978531585938" Y="-2.686894804994" />
                  <Point X="-23.466932652626" Y="-2.669839463925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.741038191394" Y="-2.660267487589" />
                  <Point X="-27.321626505532" Y="-2.535230588426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.921149421159" Y="-2.514294786885" />
                  <Point X="-29.164871930892" Y="-2.470863039811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.9612551216" Y="-2.782556019131" />
                  <Point X="-23.304598270129" Y="-2.770566212185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.985456826243" Y="-2.746790107491" />
                  <Point X="-27.312955293022" Y="-2.630591300548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.076404098363" Y="-2.603931080798" />
                  <Point X="-29.106977859955" Y="-2.567942652025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.927437760162" Y="-2.878794854123" />
                  <Point X="-23.171231045847" Y="-2.87028140499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.095174856171" Y="-2.838016576167" />
                  <Point X="-27.341792080272" Y="-2.724642204456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.231658775567" Y="-2.693567374711" />
                  <Point X="-29.049083789019" Y="-2.66502226424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.871426757759" Y="-2.975808708136" />
                  <Point X="-23.095835155798" Y="-2.967972194196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.183396920424" Y="-2.929993700505" />
                  <Point X="-27.395588425312" Y="-2.8178215014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.386913452771" Y="-2.783203668624" />
                  <Point X="-28.987546977467" Y="-2.762229083759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.815415755355" Y="-3.072822562148" />
                  <Point X="-23.02088645938" Y="-3.065647367056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.221143859346" Y="-3.023733455061" />
                  <Point X="-27.449385323422" Y="-2.91100077903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.542168129975" Y="-2.872839962538" />
                  <Point X="-28.913217625486" Y="-2.859882628635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.759404752952" Y="-3.16983641616" />
                  <Point X="-22.945937551969" Y="-3.163322547283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.241649717354" Y="-3.118075281428" />
                  <Point X="-24.627418394248" Y="-3.104603942385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.305418447786" Y="-3.0809276588" />
                  <Point X="-27.503182276833" Y="-3.004180054729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.697422807179" Y="-2.962476256451" />
                  <Point X="-28.838888273505" Y="-2.95753617351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.703393750548" Y="-3.266850270173" />
                  <Point X="-22.870988644558" Y="-3.260997727511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.262155624892" Y="-3.212417106066" />
                  <Point X="-24.525331405507" Y="-3.203226805296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.439417539078" Y="-3.17130621413" />
                  <Point X="-27.556979230244" Y="-3.097359330428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.647382748145" Y="-3.363864124185" />
                  <Point X="-22.796039737146" Y="-3.358672907739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.275266289957" Y="-3.307017178262" />
                  <Point X="-24.457717256694" Y="-3.300645850109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.504596697301" Y="-3.264088014478" />
                  <Point X="-27.610776183655" Y="-3.190538606127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.591371745741" Y="-3.460877978197" />
                  <Point X="-22.721090829735" Y="-3.456348087967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.265638846835" Y="-3.402411282692" />
                  <Point X="-24.390102964988" Y="-3.398064899912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.5690108145" Y="-3.356896530648" />
                  <Point X="-27.664573137066" Y="-3.283717881826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.535360743338" Y="-3.55789183221" />
                  <Point X="-22.646141922324" Y="-3.554023268194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.253066344006" Y="-3.497908230874" />
                  <Point X="-24.343102321456" Y="-3.49476410526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.627892699666" Y="-3.449898236617" />
                  <Point X="-27.718370090478" Y="-3.376897157525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.479349740934" Y="-3.654905686222" />
                  <Point X="-22.571193014913" Y="-3.651698448422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.240493841177" Y="-3.593405179056" />
                  <Point X="-24.317390981489" Y="-3.590719871744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.657752941636" Y="-3.543913400698" />
                  <Point X="-27.772167043889" Y="-3.470076433224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.423338738531" Y="-3.751919540234" />
                  <Point X="-22.496244107501" Y="-3.74937362865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.227921338348" Y="-3.688902127237" />
                  <Point X="-24.291679811558" Y="-3.686675632291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.682987638533" Y="-3.638090092373" />
                  <Point X="-27.8259639973" Y="-3.563255708923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.367327736127" Y="-3.848933394247" />
                  <Point X="-22.42129520009" Y="-3.847048808878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.21534883552" Y="-3.784399075419" />
                  <Point X="-24.265968674839" Y="-3.782631391678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.70822233543" Y="-3.732266784048" />
                  <Point X="-27.879760950711" Y="-3.656434984622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.311316733724" Y="-3.945947248259" />
                  <Point X="-22.346346292679" Y="-3.944723989105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.202776332691" Y="-3.8798960236" />
                  <Point X="-24.240257538121" Y="-3.878587151065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.733457032327" Y="-3.826443475723" />
                  <Point X="-26.678355513725" Y="-3.793446893661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.007958337335" Y="-3.781936909434" />
                  <Point X="-27.933557904122" Y="-3.74961426032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.190203829862" Y="-3.975392971782" />
                  <Point X="-24.214546401402" Y="-3.974542910453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.758691729224" Y="-3.920620167398" />
                  <Point X="-26.447098339474" Y="-3.896580478845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.178111570131" Y="-3.871052934322" />
                  <Point X="-27.965251669882" Y="-3.84356539634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.177631327034" Y="-4.070889919964" />
                  <Point X="-24.188835264683" Y="-4.07049866984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.783926426121" Y="-4.014796859073" />
                  <Point X="-26.33421062074" Y="-3.995580511557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.313308638998" Y="-3.961389655352" />
                  <Point X="-27.835930309411" Y="-3.943139304468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.809161123018" Y="-4.108973550748" />
                  <Point X="-26.231084184378" Y="-4.094239672779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.419008584499" Y="-4.052756438628" />
                  <Point X="-27.696032579916" Y="-4.043082547541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.834395819915" Y="-4.203150242422" />
                  <Point X="-26.184569162376" Y="-4.190921919848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.490045497377" Y="-4.145333681677" />
                  <Point X="-27.533332414022" Y="-4.143822069239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.859630516812" Y="-4.297326934097" />
                  <Point X="-26.165312561986" Y="-4.28665228186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.88486521371" Y="-4.391503625772" />
                  <Point X="-26.146271691781" Y="-4.382375110408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.910099910607" Y="-4.485680317447" />
                  <Point X="-26.127230702189" Y="-4.478097943125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.935334607504" Y="-4.579857009122" />
                  <Point X="-26.120552634405" Y="-4.573389053099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.960569304401" Y="-4.674033700797" />
                  <Point X="-26.132909325482" Y="-4.668015454646" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000162109375" Y="0.001370849609" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.977974121094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.496716796875" Y="-3.655575439453" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.548654296875" Y="-3.502851318359" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.745740234375" Y="-3.260168945312" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.028740234375" Y="-3.193997314453" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643798828" />
                  <Point X="-25.301251953125" Y="-3.304336425781" />
                  <Point X="-25.452005859375" Y="-3.521544189453" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.82806640625" Y="-4.913638671875" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.122216796875" Y="-4.932412109375" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.312970703125" Y="-4.580063964844" />
                  <Point X="-26.3091484375" Y="-4.551040527344" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.314478515625" Y="-4.510645507812" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.404765625" Y="-4.186418945312" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.673771484375" Y="-3.984155029297" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791015625" />
                  <Point X="-27.0103203125" Y="-3.987449462891" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294921875" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-27.886259765625" Y="-4.14418359375" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.565021484375" Y="-2.731291503906" />
                  <Point X="-27.506033203125" Y="-2.629119384766" />
                  <Point X="-27.49976171875" Y="-2.597591064453" />
                  <Point X="-27.5153515625" Y="-2.567390869141" />
                  <Point X="-27.53132421875" Y="-2.551416992188" />
                  <Point X="-27.560154296875" Y="-2.537198974609" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.77891015625" Y="-3.228915771484" />
                  <Point X="-28.842958984375" Y="-3.26589453125" />
                  <Point X="-28.843802734375" Y="-3.264786376953" />
                  <Point X="-29.16169921875" Y="-2.847135498047" />
                  <Point X="-29.183515625" Y="-2.810552978516" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-28.266998046875" Y="-1.502341918945" />
                  <Point X="-28.163787109375" Y="-1.42314465332" />
                  <Point X="-28.1452109375" Y="-1.393656005859" />
                  <Point X="-28.1381171875" Y="-1.36626574707" />
                  <Point X="-28.14032421875" Y="-1.334599609375" />
                  <Point X="-28.163255859375" Y="-1.309403808594" />
                  <Point X="-28.187640625" Y="-1.295052368164" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.7182890625" Y="-1.48588684082" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.927392578125" Y="-1.011187805176" />
                  <Point X="-29.933162109375" Y="-0.970842102051" />
                  <Point X="-29.99839453125" Y="-0.5147421875" />
                  <Point X="-28.675037109375" Y="-0.160149139404" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.539697265625" Y="-0.119899169922" />
                  <Point X="-28.514142578125" Y="-0.102163482666" />
                  <Point X="-28.4941640625" Y="-0.07354524231" />
                  <Point X="-28.485646484375" Y="-0.046100269318" />
                  <Point X="-28.48637890625" Y="-0.014097898483" />
                  <Point X="-28.494896484375" Y="0.01334706974" />
                  <Point X="-28.516341796875" Y="0.041129737854" />
                  <Point X="-28.541896484375" Y="0.058865428925" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.923654296875" Y="0.432155395508" />
                  <Point X="-29.998185546875" Y="0.452125946045" />
                  <Point X="-29.997775390625" Y="0.454897979736" />
                  <Point X="-29.91764453125" Y="0.996414550781" />
                  <Point X="-29.906025390625" Y="1.039292724609" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-28.837544921875" Y="1.405075927734" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866699219" />
                  <Point X="-28.7268359375" Y="1.397401367188" />
                  <Point X="-28.67027734375" Y="1.41523425293" />
                  <Point X="-28.651533203125" Y="1.426056518555" />
                  <Point X="-28.639119140625" Y="1.443786621094" />
                  <Point X="-28.637166015625" Y="1.448501831055" />
                  <Point X="-28.61447265625" Y="1.503290893555" />
                  <Point X="-28.610712890625" Y="1.524600708008" />
                  <Point X="-28.616310546875" Y="1.545502929688" />
                  <Point X="-28.618671875" Y="1.550039672852" />
                  <Point X="-28.646056640625" Y="1.602642456055" />
                  <Point X="-28.65996875" Y="1.619221923828" />
                  <Point X="-29.443619140625" Y="2.220538574219" />
                  <Point X="-29.47610546875" Y="2.245465820312" />
                  <Point X="-29.471375" Y="2.253569335938" />
                  <Point X="-29.16001171875" Y="2.787007568359" />
                  <Point X="-29.129240234375" Y="2.826562255859" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.21091796875" Y="2.956828613281" />
                  <Point X="-28.15915625" Y="2.926943603516" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.131734375" Y="2.919841796875" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927404541016" />
                  <Point X="-28.008439453125" Y="2.932216308594" />
                  <Point X="-27.95252734375" Y="2.988127929688" />
                  <Point X="-27.9408984375" Y="3.006384521484" />
                  <Point X="-27.93807421875" Y="3.027845947266" />
                  <Point X="-27.938666015625" Y="3.034619628906" />
                  <Point X="-27.945556640625" Y="3.113390136719" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.299326171875" Y="3.735504150391" />
                  <Point X="-28.295169921875" Y="3.758560302734" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.70440625" Y="4.201259277344" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-26.983671875" Y="4.308224609375" />
                  <Point X="-26.967826171875" Y="4.287573730469" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.943701171875" Y="4.269732910156" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.80594921875" Y="4.225505859375" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.69690234375" Y="4.275747070312" />
                  <Point X="-26.686080078125" Y="4.294494628906" />
                  <Point X="-26.683525390625" Y="4.302600585938" />
                  <Point X="-26.653802734375" Y="4.396864746094" />
                  <Point X="-26.651916015625" Y="4.418424804688" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.670125" Y="4.706471191406" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.909337890625" Y="4.910172851562" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.057453125" Y="4.368051269531" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.28417578125" />
                  <Point X="-24.945546875" Y="4.310901855469" />
                  <Point X="-24.767615234375" Y="4.974952636719" />
                  <Point X="-24.75276953125" Y="4.989760742188" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-24.091181640625" Y="4.913829101562" />
                  <Point X="-23.491544921875" Y="4.76905859375" />
                  <Point X="-23.460294921875" Y="4.757724121094" />
                  <Point X="-23.06896484375" Y="4.615785644531" />
                  <Point X="-23.0383671875" Y="4.6014765625" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.631734375" Y="4.407909179688" />
                  <Point X="-22.26747265625" Y="4.195688964844" />
                  <Point X="-22.239595703125" Y="4.175865722656" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.701869140625" Y="2.621854492188" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.77684765625" Y="2.485153564453" />
                  <Point X="-22.7966171875" Y="2.411229003906" />
                  <Point X="-22.797291015625" Y="2.386818359375" />
                  <Point X="-22.789583984375" Y="2.322901855469" />
                  <Point X="-22.78131640625" Y="2.300812011719" />
                  <Point X="-22.777912109375" Y="2.295795410156" />
                  <Point X="-22.738357421875" Y="2.237503417969" />
                  <Point X="-22.72004296875" Y="2.220799560547" />
                  <Point X="-22.66175" Y="2.18124609375" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.634162109375" Y="2.172316650391" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.54497265625" Y="2.167647705078" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.084837890625" Y="2.985769775391" />
                  <Point X="-21.00575" Y="3.031430908203" />
                  <Point X="-20.79740234375" Y="2.741874755859" />
                  <Point X="-20.781865234375" Y="2.716196777344" />
                  <Point X="-20.612486328125" Y="2.436295654297" />
                  <Point X="-21.6214921875" Y="1.66205859375" />
                  <Point X="-21.7113828125" Y="1.593082641602" />
                  <Point X="-21.72520703125" Y="1.577859741211" />
                  <Point X="-21.77841015625" Y="1.508451782227" />
                  <Point X="-21.7885859375" Y="1.485401245117" />
                  <Point X="-21.808404296875" Y="1.414535400391" />
                  <Point X="-21.809220703125" Y="1.390965087891" />
                  <Point X="-21.8078203125" Y="1.384179321289" />
                  <Point X="-21.79155078125" Y="1.30533215332" />
                  <Point X="-21.780544921875" Y="1.282167358398" />
                  <Point X="-21.736296875" Y="1.21491003418" />
                  <Point X="-21.719052734375" Y="1.198819946289" />
                  <Point X="-21.713533203125" Y="1.195713500977" />
                  <Point X="-21.64941015625" Y="1.159617553711" />
                  <Point X="-21.62397265625" Y="1.152633422852" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.2188671875" Y="1.313021484375" />
                  <Point X="-20.151025390625" Y="1.32195300293" />
                  <Point X="-20.060806640625" Y="0.951366516113" />
                  <Point X="-20.055912109375" Y="0.919927734375" />
                  <Point X="-20.002140625" Y="0.574556396484" />
                  <Point X="-21.155814453125" Y="0.265430328369" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.2782421875" Y="0.22858215332" />
                  <Point X="-21.363421875" Y="0.179346984863" />
                  <Point X="-21.3821328125" Y="0.161322113037" />
                  <Point X="-21.433240234375" Y="0.096198867798" />
                  <Point X="-21.443013671875" Y="0.074734802246" />
                  <Point X="-21.44448046875" Y="0.067079246521" />
                  <Point X="-21.461515625" Y="-0.02187575531" />
                  <Point X="-21.460048828125" Y="-0.048339725494" />
                  <Point X="-21.443013671875" Y="-0.137294876099" />
                  <Point X="-21.433240234375" Y="-0.15875894165" />
                  <Point X="-21.428841796875" Y="-0.164363357544" />
                  <Point X="-21.377734375" Y="-0.229486618042" />
                  <Point X="-21.356091796875" Y="-0.246144104004" />
                  <Point X="-21.270912109375" Y="-0.295379425049" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.061787109375" Y="-0.621134277344" />
                  <Point X="-20.0019296875" Y="-0.637172668457" />
                  <Point X="-20.051568359375" Y="-0.966412963867" />
                  <Point X="-20.057841796875" Y="-0.993897155762" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-21.468546875" Y="-1.113356811523" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.619548828125" Y="-1.101468505859" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.802052734375" Y="-1.143923095703" />
                  <Point X="-21.823248046875" Y="-1.165155883789" />
                  <Point X="-21.924296875" Y="-1.286685180664" />
                  <Point X="-21.932044921875" Y="-1.299517944336" />
                  <Point X="-21.93688671875" Y="-1.327615234375" />
                  <Point X="-21.951369140625" Y="-1.485000976562" />
                  <Point X="-21.9501484375" Y="-1.501458374023" />
                  <Point X="-21.935677734375" Y="-1.529006713867" />
                  <Point X="-21.843158203125" Y="-1.672912719727" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-20.720671875" Y="-2.5379375" />
                  <Point X="-20.660923828125" Y="-2.583784179688" />
                  <Point X="-20.7958671875" Y="-2.802140380859" />
                  <Point X="-20.808845703125" Y="-2.820581298828" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-22.140138671875" Y="-2.320648681641" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.27978125" Y="-2.250220458984" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.49525" Y="-2.214074462891" />
                  <Point X="-22.525146484375" Y="-2.226731445312" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7025078125" Y="-2.322614746094" />
                  <Point X="-22.718884765625" Y="-2.348908447266" />
                  <Point X="-22.80587890625" Y="-2.514201416016" />
                  <Point X="-22.811048828125" Y="-2.529873291016" />
                  <Point X="-22.8077421875" Y="-2.563497558594" />
                  <Point X="-22.77180859375" Y="-2.76246484375" />
                  <Point X="-22.76590625" Y="-2.778578857422" />
                  <Point X="-22.052064453125" Y="-4.014990234375" />
                  <Point X="-22.013326171875" Y="-4.082087646484" />
                  <Point X="-22.16470703125" Y="-4.190216308594" />
                  <Point X="-22.179208984375" Y="-4.199603027344" />
                  <Point X="-22.320224609375" Y="-4.29087890625" />
                  <Point X="-23.235625" Y="-3.097905273438" />
                  <Point X="-23.3168203125" Y="-2.992087646484" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.346337890625" Y="-2.969609863281" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.55773828125" Y="-2.836937255859" />
                  <Point X="-23.5926640625" Y="-2.837416748047" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.821833984375" Y="-2.86076171875" />
                  <Point X="-23.848927734375" Y="-2.880367431641" />
                  <Point X="-24.014650390625" Y="-3.018159667969" />
                  <Point X="-24.03154296875" Y="-3.045986083984" />
                  <Point X="-24.035806640625" Y="-3.065604980469" />
                  <Point X="-24.085357421875" Y="-3.293573974609" />
                  <Point X="-24.0860703125" Y="-3.310719726562" />
                  <Point X="-23.883771484375" Y="-4.847321777344" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.019060546875" Y="-4.965682617188" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#124" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.015312729718" Y="4.412272361799" Z="0.2" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.2" />
                  <Point X="-0.921307345037" Y="4.991113917252" Z="0.2" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.2" />
                  <Point X="-1.689780723436" Y="4.785894627758" Z="0.2" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.2" />
                  <Point X="-1.747125384685" Y="4.743057374226" Z="0.2" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.2" />
                  <Point X="-1.737367332973" Y="4.348916427052" Z="0.2" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.2" />
                  <Point X="-1.831240493722" Y="4.302979726633" Z="0.2" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.2" />
                  <Point X="-1.926770261387" Y="4.345363647817" Z="0.2" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.2" />
                  <Point X="-1.950161218653" Y="4.369942261713" Z="0.2" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.2" />
                  <Point X="-2.734847272975" Y="4.276246700695" Z="0.2" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.2" />
                  <Point X="-3.33174812985" Y="3.829520355301" Z="0.2" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.2" />
                  <Point X="-3.348784265961" Y="3.741784109804" Z="0.2" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.2" />
                  <Point X="-2.994633099151" Y="3.061542499955" Z="0.2" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.2" />
                  <Point X="-3.049951812332" Y="2.998851755555" Z="0.2" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.2" />
                  <Point X="-3.133533965652" Y="3.000931600022" Z="0.2" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.2" />
                  <Point X="-3.192075238493" Y="3.031409682785" Z="0.2" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.2" />
                  <Point X="-4.174859807845" Y="2.888544631605" Z="0.2" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.2" />
                  <Point X="-4.520714186189" Y="2.310054416899" Z="0.2" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.2" />
                  <Point X="-4.480213497757" Y="2.212150774946" Z="0.2" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.2" />
                  <Point X="-3.669179024437" Y="1.558231350708" Z="0.2" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.2" />
                  <Point X="-3.689516837987" Y="1.498915238275" Z="0.2" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.2" />
                  <Point X="-3.7480286516" Y="1.476367224557" Z="0.2" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.2" />
                  <Point X="-3.83717588936" Y="1.485928186794" Z="0.2" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.2" />
                  <Point X="-4.960442771941" Y="1.083650059998" Z="0.2" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.2" />
                  <Point X="-5.053393833907" Y="0.493497058308" Z="0.2" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.2" />
                  <Point X="-4.942753174663" Y="0.415139198428" Z="0.2" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.2" />
                  <Point X="-3.551007001741" Y="0.031333281416" Z="0.2" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.2" />
                  <Point X="-3.540289801749" Y="0.002361907531" Z="0.2" />
                  <Point X="-3.539556741714" Y="0" Z="0.2" />
                  <Point X="-3.548074799386" Y="-0.02744504353" Z="0.2" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.2" />
                  <Point X="-3.574361593383" Y="-0.047542701717" Z="0.2" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.2" />
                  <Point X="-3.694134614109" Y="-0.080572858328" Z="0.2" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.2" />
                  <Point X="-4.988815937082" Y="-0.946641204972" Z="0.2" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.2" />
                  <Point X="-4.85766084128" Y="-1.479044608744" Z="0.2" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.2" />
                  <Point X="-4.717920596735" Y="-1.504178992364" Z="0.2" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.2" />
                  <Point X="-3.194774789778" Y="-1.321214726984" Z="0.2" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.2" />
                  <Point X="-3.199770296639" Y="-1.349839984184" Z="0.2" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.2" />
                  <Point X="-3.303592677632" Y="-1.431394424448" Z="0.2" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.2" />
                  <Point X="-4.232615487134" Y="-2.804882598903" Z="0.2" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.2" />
                  <Point X="-3.889744410372" Y="-3.263789443654" Z="0.2" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.2" />
                  <Point X="-3.760066672089" Y="-3.240936889342" Z="0.2" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.2" />
                  <Point X="-2.556865670716" Y="-2.571464798416" Z="0.2" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.2" />
                  <Point X="-2.614480123827" Y="-2.675011710403" Z="0.2" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.2" />
                  <Point X="-2.922920436592" Y="-4.152520643663" Z="0.2" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.2" />
                  <Point X="-2.485932532402" Y="-4.427985122472" Z="0.2" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.2" />
                  <Point X="-2.433297000679" Y="-4.426317119006" Z="0.2" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.2" />
                  <Point X="-1.98869728566" Y="-3.99774282046" Z="0.2" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.2" />
                  <Point X="-1.68319874231" Y="-4.002768027624" Z="0.2" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.2" />
                  <Point X="-1.443889607504" Y="-4.192730776888" Z="0.2" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.2" />
                  <Point X="-1.369674739528" Y="-4.489120106559" Z="0.2" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.2" />
                  <Point X="-1.36869953655" Y="-4.542255604798" Z="0.2" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.2" />
                  <Point X="-1.140832932315" Y="-4.94955495558" Z="0.2" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.2" />
                  <Point X="-0.841419030868" Y="-5.009152807713" Z="0.2" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.2" />
                  <Point X="-0.78592595454" Y="-4.895299716242" Z="0.2" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.2" />
                  <Point X="-0.266332888343" Y="-3.301565462138" Z="0.2" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.2" />
                  <Point X="-0.020076213648" Y="-3.210470284273" Z="0.2" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.2" />
                  <Point X="0.233282865714" Y="-3.276641761015" Z="0.2" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.2" />
                  <Point X="0.404112971089" Y="-3.50008029363" Z="0.2" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.2" />
                  <Point X="0.448828905861" Y="-3.637236313573" Z="0.2" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.2" />
                  <Point X="0.983719823648" Y="-4.983597409411" Z="0.2" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.2" />
                  <Point X="1.162865593541" Y="-4.944865160589" Z="0.2" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.2" />
                  <Point X="1.159643342027" Y="-4.809516021599" Z="0.2" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.2" />
                  <Point X="1.00689576775" Y="-3.044944900676" Z="0.2" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.2" />
                  <Point X="1.17688181142" Y="-2.887533775041" Z="0.2" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.2" />
                  <Point X="1.405760628849" Y="-2.85592619953" Z="0.2" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.2" />
                  <Point X="1.620465961836" Y="-2.980387887787" Z="0.2" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.2" />
                  <Point X="1.718550715073" Y="-3.097063001567" Z="0.2" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.2" />
                  <Point X="2.8418037675" Y="-4.210297314857" Z="0.2" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.2" />
                  <Point X="3.031517273384" Y="-4.075825790299" Z="0.2" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.2" />
                  <Point X="2.98507963371" Y="-3.958709994121" Z="0.2" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.2" />
                  <Point X="2.235303522256" Y="-2.523331222192" Z="0.2" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.2" />
                  <Point X="2.319203659383" Y="-2.340915230557" Z="0.2" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.2" />
                  <Point X="2.491983147815" Y="-2.239697650584" Z="0.2" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.2" />
                  <Point X="2.705175581321" Y="-2.268144487235" Z="0.2" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.2" />
                  <Point X="2.828703602986" Y="-2.332669866564" Z="0.2" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.2" />
                  <Point X="4.225886343566" Y="-2.818078633924" Z="0.2" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.2" />
                  <Point X="4.38657082646" Y="-2.560796594711" Z="0.2" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.2" />
                  <Point X="4.303608010521" Y="-2.466989980366" Z="0.2" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.2" />
                  <Point X="3.100224582831" Y="-1.47068698435" Z="0.2" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.2" />
                  <Point X="3.106745092452" Y="-1.300916772912" Z="0.2" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.2" />
                  <Point X="3.209039391271" Y="-1.165842936839" Z="0.2" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.2" />
                  <Point X="3.3849126736" Y="-1.119047538944" Z="0.2" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.2" />
                  <Point X="3.518770792434" Y="-1.131649069918" Z="0.2" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.2" />
                  <Point X="4.984748011621" Y="-0.973740927365" Z="0.2" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.2" />
                  <Point X="5.043531968677" Y="-0.598897075548" Z="0.2" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.2" />
                  <Point X="4.944997965373" Y="-0.5415579344" Z="0.2" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.2" />
                  <Point X="3.662773220578" Y="-0.171575371163" Z="0.2" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.2" />
                  <Point X="3.604335027241" Y="-0.102215003491" Z="0.2" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.2" />
                  <Point X="3.582900827892" Y="-0.007655433858" Z="0.2" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.2" />
                  <Point X="3.598470622532" Y="0.088955097386" Z="0.2" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.2" />
                  <Point X="3.651044411159" Y="0.161733735084" Z="0.2" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.2" />
                  <Point X="3.740622193774" Y="0.216573473079" Z="0.2" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.2" />
                  <Point X="3.850969851934" Y="0.248413998369" Z="0.2" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.2" />
                  <Point X="4.98733498793" Y="0.958899863585" Z="0.2" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.2" />
                  <Point X="4.888814666021" Y="1.375681610978" Z="0.2" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.2" />
                  <Point X="4.768449540904" Y="1.393873844088" Z="0.2" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.2" />
                  <Point X="3.376421935325" Y="1.233482475443" Z="0.2" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.2" />
                  <Point X="3.304837000452" Y="1.270564622505" Z="0.2" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.2" />
                  <Point X="3.255069084239" Y="1.340928277018" Z="0.2" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.2" />
                  <Point X="3.234992124776" Y="1.425563648275" Z="0.2" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.2" />
                  <Point X="3.25341042446" Y="1.503215023585" Z="0.2" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.2" />
                  <Point X="3.308319490458" Y="1.578721866831" Z="0.2" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.2" />
                  <Point X="3.402789282124" Y="1.653670983142" Z="0.2" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.2" />
                  <Point X="4.254755303433" Y="2.773363100219" Z="0.2" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.2" />
                  <Point X="4.020955722971" Y="3.102645385934" Z="0.2" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.2" />
                  <Point X="3.88400447695" Y="3.060351060067" Z="0.2" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.2" />
                  <Point X="2.435953552062" Y="2.247230428764" Z="0.2" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.2" />
                  <Point X="2.365668025627" Y="2.253237180602" Z="0.2" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.2" />
                  <Point X="2.301874664967" Y="2.293454109653" Z="0.2" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.2" />
                  <Point X="2.257304490582" Y="2.355150195415" Z="0.2" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.2" />
                  <Point X="2.246192522156" Y="2.424090418205" Z="0.2" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.2" />
                  <Point X="2.265297513893" Y="2.50351602382" Z="0.2" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.2" />
                  <Point X="2.335274292792" Y="2.628134605005" Z="0.2" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.2" />
                  <Point X="2.783223100329" Y="4.247893037832" Z="0.2" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.2" />
                  <Point X="2.387279314033" Y="4.482391762112" Z="0.2" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.2" />
                  <Point X="1.976656906481" Y="4.678048512368" Z="0.2" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.2" />
                  <Point X="1.550596397575" Y="4.836008169723" Z="0.2" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.2" />
                  <Point X="0.914395904101" Y="4.993712862095" Z="0.2" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.2" />
                  <Point X="0.246281117138" Y="5.070768688525" Z="0.2" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.2" />
                  <Point X="0.177931831835" Y="5.019175144152" Z="0.2" />
                  <Point X="0" Y="4.355124473572" Z="0.2" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>