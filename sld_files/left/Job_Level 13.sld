<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#141" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1074" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004716064453" />
                  <Width Value="9.78389453125" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.341869140625" Y="-3.866419189453" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140136719" />
                  <Point X="-24.457634765625" Y="-3.467376953125" />
                  <Point X="-24.496392578125" Y="-3.411534423828" />
                  <Point X="-24.62136328125" Y="-3.2314765625" />
                  <Point X="-24.643248046875" Y="-3.209020751953" />
                  <Point X="-24.66950390625" Y="-3.189776855469" />
                  <Point X="-24.69750390625" Y="-3.175669433594" />
                  <Point X="-24.757478515625" Y="-3.157055175781" />
                  <Point X="-24.95086328125" Y="-3.097036132812" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097036132812" />
                  <Point X="-25.096798828125" Y="-3.115650146484" />
                  <Point X="-25.29018359375" Y="-3.175669433594" />
                  <Point X="-25.31818359375" Y="-3.189776855469" />
                  <Point X="-25.344439453125" Y="-3.209020751953" />
                  <Point X="-25.36632421875" Y="-3.231478271484" />
                  <Point X="-25.405080078125" Y="-3.287320556641" />
                  <Point X="-25.53005078125" Y="-3.467378662109" />
                  <Point X="-25.5381875" Y="-3.481572021484" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.85674609375" Y="-4.653619628906" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.145111328125" Y="-4.828427734375" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.226376953125" Y="-4.650142578125" />
                  <Point X="-26.2149609375" Y="-4.563438476562" />
                  <Point X="-26.21419921875" Y="-4.5479296875" />
                  <Point X="-26.2165078125" Y="-4.516223632812" />
                  <Point X="-26.2308359375" Y="-4.44419140625" />
                  <Point X="-26.277037109375" Y="-4.211932128906" />
                  <Point X="-26.287939453125" Y="-4.182964355469" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131204589844" />
                  <Point X="-26.378861328125" Y="-4.082780029297" />
                  <Point X="-26.556904296875" Y="-3.926639892578" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.716314453125" Y="-3.886162841797" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708496094" />
                  <Point X="-27.014466796875" Y="-3.882028076172" />
                  <Point X="-27.042658203125" Y="-3.894802001953" />
                  <Point X="-27.10372265625" Y="-3.935604980469" />
                  <Point X="-27.300623046875" Y="-4.067170166016" />
                  <Point X="-27.31278515625" Y="-4.076821533203" />
                  <Point X="-27.3351015625" Y="-4.099461425781" />
                  <Point X="-27.4801484375" Y="-4.288488769531" />
                  <Point X="-27.80171484375" Y="-4.089383544922" />
                  <Point X="-27.892783203125" Y="-4.019263183594" />
                  <Point X="-28.10472265625" Y="-3.856077392578" />
                  <Point X="-27.599984375" Y="-2.981846679688" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647653808594" />
                  <Point X="-27.406587890625" Y="-2.616125976562" />
                  <Point X="-27.405576171875" Y="-2.585190185547" />
                  <Point X="-27.414560546875" Y="-2.555570556641" />
                  <Point X="-27.428779296875" Y="-2.526740966797" />
                  <Point X="-27.4468046875" Y="-2.501588378906" />
                  <Point X="-27.4641484375" Y="-2.484243896484" />
                  <Point X="-27.489302734375" Y="-2.466216308594" />
                  <Point X="-27.5181328125" Y="-2.451997558594" />
                  <Point X="-27.547751953125" Y="-2.443011962891" />
                  <Point X="-27.5786875" Y="-2.444024169922" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.62335546875" Y="-3.029409423828" />
                  <Point X="-28.8180234375" Y="-3.141801513672" />
                  <Point X="-28.822873046875" Y="-3.135430664062" />
                  <Point X="-29.082859375" Y="-2.793861083984" />
                  <Point X="-29.148150390625" Y="-2.684377685547" />
                  <Point X="-29.306142578125" Y="-2.419450195312" />
                  <Point X="-28.414287109375" Y="-1.73510559082" />
                  <Point X="-28.105955078125" Y="-1.498513305664" />
                  <Point X="-28.083064453125" Y="-1.475880004883" />
                  <Point X="-28.063912109375" Y="-1.445301391602" />
                  <Point X="-28.055375" Y="-1.423191894531" />
                  <Point X="-28.052033203125" Y="-1.412791503906" />
                  <Point X="-28.04615234375" Y="-1.3900859375" />
                  <Point X="-28.042037109375" Y="-1.358610229492" />
                  <Point X="-28.042734375" Y="-1.335736816406" />
                  <Point X="-28.0488828125" Y="-1.313694091797" />
                  <Point X="-28.060888671875" Y="-1.284711669922" />
                  <Point X="-28.07356640625" Y="-1.262873901367" />
                  <Point X="-28.09155078125" Y="-1.245147827148" />
                  <Point X="-28.110322265625" Y="-1.231028686523" />
                  <Point X="-28.119240234375" Y="-1.225078491211" />
                  <Point X="-28.139453125" Y="-1.213181640625" />
                  <Point X="-28.16871484375" Y="-1.20195715332" />
                  <Point X="-28.2006015625" Y="-1.195475097656" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-29.4743515625" Y="-1.357952026367" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.732392578125" Y="-1.390740112305" />
                  <Point X="-29.834076171875" Y="-0.992650268555" />
                  <Point X="-29.8513515625" Y="-0.871875061035" />
                  <Point X="-29.892421875" Y="-0.584698425293" />
                  <Point X="-28.88411328125" Y="-0.314522155762" />
                  <Point X="-28.532875" Y="-0.220408309937" />
                  <Point X="-28.512833984375" Y="-0.212510360718" />
                  <Point X="-28.490697265625" Y="-0.200730514526" />
                  <Point X="-28.48116015625" Y="-0.194910263062" />
                  <Point X="-28.4599765625" Y="-0.180207992554" />
                  <Point X="-28.436021484375" Y="-0.158684127808" />
                  <Point X="-28.415404296875" Y="-0.128991195679" />
                  <Point X="-28.4057890625" Y="-0.107196975708" />
                  <Point X="-28.4019765625" Y="-0.097008346558" />
                  <Point X="-28.394916015625" Y="-0.074257392883" />
                  <Point X="-28.38947265625" Y="-0.045515735626" />
                  <Point X="-28.390294921875" Y="-0.012401903152" />
                  <Point X="-28.39473046875" Y="0.009473824501" />
                  <Point X="-28.39710546875" Y="0.018753225327" />
                  <Point X="-28.404166015625" Y="0.041504177094" />
                  <Point X="-28.417482421875" Y="0.070830841064" />
                  <Point X="-28.439794921875" Y="0.099606651306" />
                  <Point X="-28.458185546875" Y="0.115684440613" />
                  <Point X="-28.466546875" Y="0.122207633972" />
                  <Point X="-28.48773046875" Y="0.136909896851" />
                  <Point X="-28.501923828125" Y="0.14504586792" />
                  <Point X="-28.532875" Y="0.157848327637" />
                  <Point X="-29.66540234375" Y="0.461308410645" />
                  <Point X="-29.891814453125" Y="0.521975341797" />
                  <Point X="-29.89001953125" Y="0.534108459473" />
                  <Point X="-29.82448828125" Y="0.976969299316" />
                  <Point X="-29.789712890625" Y="1.105300048828" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-29.01744140625" Y="1.332939941406" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228027344" />
                  <Point X="-28.7031328125" Y="1.305264282227" />
                  <Point X="-28.68859375" Y="1.309848876953" />
                  <Point X="-28.64170703125" Y="1.324631713867" />
                  <Point X="-28.62277734375" Y="1.332962036133" />
                  <Point X="-28.604033203125" Y="1.343784179688" />
                  <Point X="-28.5873515625" Y="1.35601574707" />
                  <Point X="-28.573712890625" Y="1.371567993164" />
                  <Point X="-28.561298828125" Y="1.389298095703" />
                  <Point X="-28.551349609375" Y="1.407434814453" />
                  <Point X="-28.545515625" Y="1.421520751953" />
                  <Point X="-28.526703125" Y="1.466938964844" />
                  <Point X="-28.520916015625" Y="1.486788696289" />
                  <Point X="-28.51715625" Y="1.508104125977" />
                  <Point X="-28.515802734375" Y="1.528750488281" />
                  <Point X="-28.518951171875" Y="1.549200317383" />
                  <Point X="-28.5245546875" Y="1.570107055664" />
                  <Point X="-28.532048828125" Y="1.589374145508" />
                  <Point X="-28.539087890625" Y="1.602897949219" />
                  <Point X="-28.5617890625" Y="1.64650390625" />
                  <Point X="-28.573283203125" Y="1.663708984375" />
                  <Point X="-28.5871953125" Y="1.680288208008" />
                  <Point X="-28.60213671875" Y="1.694590087891" />
                  <Point X="-29.251755859375" Y="2.193061767578" />
                  <Point X="-29.351859375" Y="2.269873779297" />
                  <Point X="-29.3357890625" Y="2.297406738281" />
                  <Point X="-29.081146484375" Y="2.733668212891" />
                  <Point X="-28.98904296875" Y="2.852056152344" />
                  <Point X="-28.75050390625" Y="3.158662353516" />
                  <Point X="-28.3612890625" Y="2.933948730469" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.126541015625" Y="2.824024658203" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.999013671875" Y="2.826504638672" />
                  <Point X="-27.980462890625" Y="2.835653076172" />
                  <Point X="-27.962208984375" Y="2.847281982422" />
                  <Point X="-27.946078125" Y="2.860228027344" />
                  <Point X="-27.931703125" Y="2.874602539062" />
                  <Point X="-27.885353515625" Y="2.920951416016" />
                  <Point X="-27.872404296875" Y="2.9370859375" />
                  <Point X="-27.860775390625" Y="2.955340576172" />
                  <Point X="-27.85162890625" Y="2.973889160156" />
                  <Point X="-27.8467109375" Y="2.993977783203" />
                  <Point X="-27.843884765625" Y="3.015436767578" />
                  <Point X="-27.84343359375" Y="3.036119873047" />
                  <Point X="-27.845205078125" Y="3.056371337891" />
                  <Point X="-27.85091796875" Y="3.121669433594" />
                  <Point X="-27.854955078125" Y="3.141963378906" />
                  <Point X="-27.86146484375" Y="3.16260546875" />
                  <Point X="-27.869794921875" Y="3.181532958984" />
                  <Point X="-28.157662109375" Y="3.680132568359" />
                  <Point X="-28.18333203125" Y="3.724596191406" />
                  <Point X="-28.1441171875" Y="3.754662841797" />
                  <Point X="-27.700623046875" Y="4.094684814453" />
                  <Point X="-27.5555546875" Y="4.175280761719" />
                  <Point X="-27.16703515625" Y="4.391133789062" />
                  <Point X="-27.090533203125" Y="4.29143359375" />
                  <Point X="-27.0431953125" Y="4.229741210938" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.99511328125" Y="4.189395019531" />
                  <Point X="-26.97257421875" Y="4.177661621094" />
                  <Point X="-26.899896484375" Y="4.139828125" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.777451171875" Y="4.134482421875" />
                  <Point X="-26.753974609375" Y="4.14420703125" />
                  <Point X="-26.67827734375" Y="4.175562011719" />
                  <Point X="-26.660146484375" Y="4.185508789062" />
                  <Point X="-26.642416015625" Y="4.197922851562" />
                  <Point X="-26.626859375" Y="4.211563964844" />
                  <Point X="-26.614626953125" Y="4.228249511719" />
                  <Point X="-26.603806640625" Y="4.246993652344" />
                  <Point X="-26.595478515625" Y="4.265923339844" />
                  <Point X="-26.587837890625" Y="4.290158203125" />
                  <Point X="-26.56319921875" Y="4.368299804688" />
                  <Point X="-26.5591640625" Y="4.388583496094" />
                  <Point X="-26.55727734375" Y="4.41014453125" />
                  <Point X="-26.557728515625" Y="4.430826171875" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.52376171875" Y="4.648843261719" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.773771484375" Y="4.830390136719" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.1796484375" Y="4.457038085938" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183885742188" />
                  <Point X="-24.932869140625" Y="4.194217773438" />
                  <Point X="-24.905576171875" Y="4.208806640625" />
                  <Point X="-24.88441796875" Y="4.23139453125" />
                  <Point X="-24.86655859375" Y="4.25812109375" />
                  <Point X="-24.853783203125" Y="4.286314941406" />
                  <Point X="-24.706283203125" Y="4.836791015625" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.657259765625" Y="4.884238769531" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-24.01045703125" Y="4.796610351562" />
                  <Point X="-23.51897265625" Y="4.677951171875" />
                  <Point X="-23.4253984375" Y="4.644010742188" />
                  <Point X="-23.105357421875" Y="4.527930175781" />
                  <Point X="-23.013771484375" Y="4.485098632812" />
                  <Point X="-22.705421875" Y="4.340893066406" />
                  <Point X="-22.616919921875" Y="4.28933203125" />
                  <Point X="-22.3190234375" Y="4.115775878906" />
                  <Point X="-22.235578125" Y="4.056435302734" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.648451171875" Y="2.904375488281" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.860509765625" Y="2.532909912109" />
                  <Point X="-22.87059375" Y="2.501822265625" />
                  <Point X="-22.87200390625" Y="2.497053955078" />
                  <Point X="-22.888392578125" Y="2.435772949219" />
                  <Point X="-22.8916015625" Y="2.409496826172" />
                  <Point X="-22.89095703125" Y="2.37416015625" />
                  <Point X="-22.8902890625" Y="2.364521484375" />
                  <Point X="-22.883900390625" Y="2.311530761719" />
                  <Point X="-22.87855859375" Y="2.289608642578" />
                  <Point X="-22.87029296875" Y="2.267519042969" />
                  <Point X="-22.859927734375" Y="2.247467285156" />
                  <Point X="-22.8497578125" Y="2.232480957031" />
                  <Point X="-22.81696875" Y="2.184158691406" />
                  <Point X="-22.79901953125" Y="2.164391113281" />
                  <Point X="-22.770732421875" Y="2.140922119141" />
                  <Point X="-22.7634140625" Y="2.135423339844" />
                  <Point X="-22.715091796875" Y="2.102634765625" />
                  <Point X="-22.695046875" Y="2.092272216797" />
                  <Point X="-22.672958984375" Y="2.084006347656" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.6346015625" Y="2.076681884766" />
                  <Point X="-22.581611328125" Y="2.070291992188" />
                  <Point X="-22.554375" Y="2.070942382812" />
                  <Point X="-22.516466796875" Y="2.077362548828" />
                  <Point X="-22.5077890625" Y="2.079253417969" />
                  <Point X="-22.4465078125" Y="2.095640625" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.272359375" Y="2.767807861328" />
                  <Point X="-21.032671875" Y="2.906191162109" />
                  <Point X="-20.8767265625" Y="2.689464355469" />
                  <Point X="-20.830208984375" Y="2.612590820312" />
                  <Point X="-20.73780078125" Y="2.459884033203" />
                  <Point X="-21.50067578125" Y="1.874508544922" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.778572265625" Y="1.660243896484" />
                  <Point X="-21.79602734375" Y="1.641626342773" />
                  <Point X="-21.809705078125" Y="1.623782104492" />
                  <Point X="-21.85380859375" Y="1.566245239258" />
                  <Point X="-21.867046875" Y="1.542637084961" />
                  <Point X="-21.880611328125" Y="1.507467041016" />
                  <Point X="-21.88346484375" Y="1.498869018555" />
                  <Point X="-21.89989453125" Y="1.440123535156" />
                  <Point X="-21.90334765625" Y="1.417824707031" />
                  <Point X="-21.9041640625" Y="1.39425378418" />
                  <Point X="-21.902259765625" Y="1.371763671875" />
                  <Point X="-21.898076171875" Y="1.351492553711" />
                  <Point X="-21.88458984375" Y="1.286130859375" />
                  <Point X="-21.8793203125" Y="1.268979248047" />
                  <Point X="-21.86371484375" Y="1.235738525391" />
                  <Point X="-21.852337890625" Y="1.218447387695" />
                  <Point X="-21.815658203125" Y="1.162693359375" />
                  <Point X="-21.801109375" Y="1.145452270508" />
                  <Point X="-21.783865234375" Y="1.129361694336" />
                  <Point X="-21.76565234375" Y="1.116033935547" />
                  <Point X="-21.749166015625" Y="1.10675402832" />
                  <Point X="-21.696009765625" Y="1.076831542969" />
                  <Point X="-21.6794765625" Y="1.069501098633" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.62158984375" Y="1.056492675781" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.42972265625" Y="1.189442260742" />
                  <Point X="-20.223162109375" Y="1.216636474609" />
                  <Point X="-20.15405859375" Y="0.932786010742" />
                  <Point X="-20.13940234375" Y="0.83865447998" />
                  <Point X="-20.109134765625" Y="0.644238891602" />
                  <Point X="-20.97566796875" Y="0.412051544189" />
                  <Point X="-21.283419921875" Y="0.329589874268" />
                  <Point X="-21.2952109375" Y="0.325585662842" />
                  <Point X="-21.318453125" Y="0.315067657471" />
                  <Point X="-21.3403515625" Y="0.302409698486" />
                  <Point X="-21.410962890625" Y="0.261595214844" />
                  <Point X="-21.425685546875" Y="0.251097854614" />
                  <Point X="-21.452470703125" Y="0.225574920654" />
                  <Point X="-21.465609375" Y="0.208832290649" />
                  <Point X="-21.5079765625" Y="0.15484727478" />
                  <Point X="-21.51969921875" Y="0.13556640625" />
                  <Point X="-21.52947265625" Y="0.114101722717" />
                  <Point X="-21.53631640625" Y="0.092607444763" />
                  <Point X="-21.540697265625" Y="0.069737640381" />
                  <Point X="-21.554818359375" Y="-0.004003189087" />
                  <Point X="-21.556515625" Y="-0.021876050949" />
                  <Point X="-21.5548203125" Y="-0.058550582886" />
                  <Point X="-21.55044140625" Y="-0.08142023468" />
                  <Point X="-21.536318359375" Y="-0.155161224365" />
                  <Point X="-21.52947265625" Y="-0.176664901733" />
                  <Point X="-21.51969921875" Y="-0.198128372192" />
                  <Point X="-21.5079765625" Y="-0.217407272339" />
                  <Point X="-21.494837890625" Y="-0.234150054932" />
                  <Point X="-21.452470703125" Y="-0.288135070801" />
                  <Point X="-21.439998046875" Y="-0.301237762451" />
                  <Point X="-21.41096484375" Y="-0.324155059814" />
                  <Point X="-21.38906640625" Y="-0.336813171387" />
                  <Point X="-21.318455078125" Y="-0.377627471924" />
                  <Point X="-21.307291015625" Y="-0.383138244629" />
                  <Point X="-21.283419921875" Y="-0.392149871826" />
                  <Point X="-20.291109375" Y="-0.658039001465" />
                  <Point X="-20.10852734375" Y="-0.706961730957" />
                  <Point X="-20.14498046875" Y="-0.948747558594" />
                  <Point X="-20.163755859375" Y="-1.03102746582" />
                  <Point X="-20.19882421875" Y="-1.18469921875" />
                  <Point X="-21.218712890625" Y="-1.050428344727" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710144043" />
                  <Point X="-21.62533984375" Y="-1.005508728027" />
                  <Point X="-21.6683203125" Y="-1.014850524902" />
                  <Point X="-21.806904296875" Y="-1.044972412109" />
                  <Point X="-21.836025390625" Y="-1.056597412109" />
                  <Point X="-21.8638515625" Y="-1.073489746094" />
                  <Point X="-21.8876015625" Y="-1.093960327148" />
                  <Point X="-21.913580078125" Y="-1.125204589844" />
                  <Point X="-21.997345703125" Y="-1.225948486328" />
                  <Point X="-22.012064453125" Y="-1.250329223633" />
                  <Point X="-22.023408203125" Y="-1.277714111328" />
                  <Point X="-22.030240234375" Y="-1.305366943359" />
                  <Point X="-22.033962890625" Y="-1.345829711914" />
                  <Point X="-22.04596875" Y="-1.476297241211" />
                  <Point X="-22.04365234375" Y="-1.507561035156" />
                  <Point X="-22.035921875" Y="-1.539182373047" />
                  <Point X="-22.023548828125" Y="-1.56799597168" />
                  <Point X="-21.999763671875" Y="-1.604993164063" />
                  <Point X="-21.923068359375" Y="-1.724286621094" />
                  <Point X="-21.9130625" Y="-1.737242919922" />
                  <Point X="-21.889369140625" Y="-1.760909423828" />
                  <Point X="-20.9685" Y="-2.467518066406" />
                  <Point X="-20.786876953125" Y="-2.6068828125" />
                  <Point X="-20.8751796875" Y="-2.749772216797" />
                  <Point X="-20.91403125" Y="-2.804975097656" />
                  <Point X="-20.971017578125" Y="-2.885945068359" />
                  <Point X="-21.881171875" Y="-2.360466796875" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.245775390625" Y="-2.159825195312" />
                  <Point X="-22.296927734375" Y="-2.150587158203" />
                  <Point X="-22.461865234375" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135176513672" />
                  <Point X="-22.597662109375" Y="-2.157541503906" />
                  <Point X="-22.73468359375" Y="-2.229655517578" />
                  <Point X="-22.757615234375" Y="-2.246549560547" />
                  <Point X="-22.77857421875" Y="-2.267509033203" />
                  <Point X="-22.795466796875" Y="-2.290438720703" />
                  <Point X="-22.81783203125" Y="-2.332934326172" />
                  <Point X="-22.889947265625" Y="-2.469956787109" />
                  <Point X="-22.899771484375" Y="-2.499735351563" />
                  <Point X="-22.904728515625" Y="-2.531908935547" />
                  <Point X="-22.90432421875" Y="-2.563259033203" />
                  <Point X="-22.8950859375" Y="-2.614412109375" />
                  <Point X="-22.865296875" Y="-2.779349365234" />
                  <Point X="-22.86101171875" Y="-2.795140869141" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.25642578125" Y="-3.851022949219" />
                  <Point X="-22.138712890625" Y="-4.054905273438" />
                  <Point X="-22.21815234375" Y="-4.111645996094" />
                  <Point X="-22.26157421875" Y="-4.139752441406" />
                  <Point X="-22.298232421875" Y="-4.163480957031" />
                  <Point X="-22.99888671875" Y="-3.250372802734" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.328525390625" Y="-2.868121826172" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.638076171875" Y="-2.746194091797" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-23.9380078125" Y="-2.830886474609" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968861083984" />
                  <Point X="-24.11275" Y="-2.996687744141" />
                  <Point X="-24.124375" Y="-3.025809570312" />
                  <Point X="-24.13711328125" Y="-3.084418701172" />
                  <Point X="-24.178189453125" Y="-3.273397460938" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.01255859375" Y="-4.596912597656" />
                  <Point X="-23.97793359375" Y="-4.859915527344" />
                  <Point X="-24.024318359375" Y="-4.870083007812" />
                  <Point X="-24.06444140625" Y="-4.877371582031" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.05841796875" Y="-4.752638183594" />
                  <Point X="-26.121439453125" Y="-4.736423828125" />
                  <Point X="-26.141244140625" Y="-4.731328125" />
                  <Point X="-26.132189453125" Y="-4.662541992188" />
                  <Point X="-26.1207734375" Y="-4.575837890625" />
                  <Point X="-26.120076171875" Y="-4.568098632812" />
                  <Point X="-26.11944921875" Y="-4.541030761719" />
                  <Point X="-26.1217578125" Y="-4.509324707031" />
                  <Point X="-26.123333984375" Y="-4.497689941406" />
                  <Point X="-26.137662109375" Y="-4.425657714844" />
                  <Point X="-26.18386328125" Y="-4.1933984375" />
                  <Point X="-26.188125" Y="-4.178469238281" />
                  <Point X="-26.19902734375" Y="-4.149501464844" />
                  <Point X="-26.20566796875" Y="-4.135462890625" />
                  <Point X="-26.221740234375" Y="-4.107625976562" />
                  <Point X="-26.230576171875" Y="-4.094860351562" />
                  <Point X="-26.250208984375" Y="-4.0709375" />
                  <Point X="-26.261005859375" Y="-4.059780273438" />
                  <Point X="-26.31622265625" Y="-4.011355712891" />
                  <Point X="-26.494265625" Y="-3.855215576172" />
                  <Point X="-26.506736328125" Y="-3.845966308594" />
                  <Point X="-26.53301953125" Y="-3.829621826172" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.7101015625" Y="-3.791366210938" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166015625" />
                  <Point X="-27.008005859375" Y="-3.781945556641" />
                  <Point X="-27.0390546875" Y="-3.790265136719" />
                  <Point X="-27.05367578125" Y="-3.795496582031" />
                  <Point X="-27.0818671875" Y="-3.808270507812" />
                  <Point X="-27.0954375" Y="-3.815812988281" />
                  <Point X="-27.156501953125" Y="-3.856615966797" />
                  <Point X="-27.35340234375" Y="-3.988181152344" />
                  <Point X="-27.35967578125" Y="-3.992754394531" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032771484375" />
                  <Point X="-27.410470703125" Y="-4.04162890625" />
                  <Point X="-27.503203125" Y="-4.162478027344" />
                  <Point X="-27.74759765625" Y="-4.011154541016" />
                  <Point X="-27.834826171875" Y="-3.943990966797" />
                  <Point X="-27.98086328125" Y="-3.831546875" />
                  <Point X="-27.517712890625" Y="-3.029346679688" />
                  <Point X="-27.341490234375" Y="-2.724119628906" />
                  <Point X="-27.3348515625" Y="-2.710084472656" />
                  <Point X="-27.32394921875" Y="-2.681118652344" />
                  <Point X="-27.319685546875" Y="-2.666187988281" />
                  <Point X="-27.3134140625" Y="-2.63466015625" />
                  <Point X="-27.311638671875" Y="-2.619231201172" />
                  <Point X="-27.310626953125" Y="-2.588295410156" />
                  <Point X="-27.314666015625" Y="-2.557614990234" />
                  <Point X="-27.323650390625" Y="-2.527995361328" />
                  <Point X="-27.329359375" Y="-2.513549316406" />
                  <Point X="-27.343578125" Y="-2.484719726562" />
                  <Point X="-27.351560546875" Y="-2.471402832031" />
                  <Point X="-27.3695859375" Y="-2.446250244141" />
                  <Point X="-27.37962890625" Y="-2.434414550781" />
                  <Point X="-27.39697265625" Y="-2.417070068359" />
                  <Point X="-27.40880859375" Y="-2.407026855469" />
                  <Point X="-27.433962890625" Y="-2.388999267578" />
                  <Point X="-27.44728125" Y="-2.381014892578" />
                  <Point X="-27.476111328125" Y="-2.366796142578" />
                  <Point X="-27.4905546875" Y="-2.361088867188" />
                  <Point X="-27.520173828125" Y="-2.352103271484" />
                  <Point X="-27.550859375" Y="-2.348062744141" />
                  <Point X="-27.581794921875" Y="-2.349074951172" />
                  <Point X="-27.597220703125" Y="-2.350849365234" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.67085546875" Y="-2.947136962891" />
                  <Point X="-28.793087890625" Y="-3.017708496094" />
                  <Point X="-29.0040234375" Y="-2.740581787109" />
                  <Point X="-29.06655859375" Y="-2.635719482422" />
                  <Point X="-29.181265625" Y="-2.443373535156" />
                  <Point X="-28.356455078125" Y="-1.810473999023" />
                  <Point X="-28.048123046875" Y="-1.573881835938" />
                  <Point X="-28.03916015625" Y="-1.566067260742" />
                  <Point X="-28.01626953125" Y="-1.543433837891" />
                  <Point X="-28.002552734375" Y="-1.526307006836" />
                  <Point X="-27.983400390625" Y="-1.495728271484" />
                  <Point X="-27.9752890625" Y="-1.479521362305" />
                  <Point X="-27.966751953125" Y="-1.457411743164" />
                  <Point X="-27.960068359375" Y="-1.436610961914" />
                  <Point X="-27.9541875" Y="-1.413905395508" />
                  <Point X="-27.951953125" Y="-1.402401855469" />
                  <Point X="-27.947837890625" Y="-1.370926025391" />
                  <Point X="-27.94708203125" Y="-1.355715454102" />
                  <Point X="-27.947779296875" Y="-1.332842163086" />
                  <Point X="-27.9512265625" Y="-1.310212524414" />
                  <Point X="-27.957375" Y="-1.288169799805" />
                  <Point X="-27.961115234375" Y="-1.277336669922" />
                  <Point X="-27.97312109375" Y="-1.248354248047" />
                  <Point X="-27.97873046875" Y="-1.237015136719" />
                  <Point X="-27.991408203125" Y="-1.215177246094" />
                  <Point X="-28.00687890625" Y="-1.195214599609" />
                  <Point X="-28.02486328125" Y="-1.177488525391" />
                  <Point X="-28.0344453125" Y="-1.1692265625" />
                  <Point X="-28.053216796875" Y="-1.155107421875" />
                  <Point X="-28.071052734375" Y="-1.14320703125" />
                  <Point X="-28.091265625" Y="-1.131310180664" />
                  <Point X="-28.1054296875" Y="-1.124483276367" />
                  <Point X="-28.13469140625" Y="-1.113258911133" />
                  <Point X="-28.1497890625" Y="-1.108861083984" />
                  <Point X="-28.18167578125" Y="-1.102379150391" />
                  <Point X="-28.197294921875" Y="-1.100532714844" />
                  <Point X="-28.22862109375" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-29.486751953125" Y="-1.263764770508" />
                  <Point X="-29.66091796875" Y="-1.286694335938" />
                  <Point X="-29.740759765625" Y="-0.97411541748" />
                  <Point X="-29.75730859375" Y="-0.858423461914" />
                  <Point X="-29.786451171875" Y="-0.654654663086" />
                  <Point X="-28.859525390625" Y="-0.40628515625" />
                  <Point X="-28.508287109375" Y="-0.312171203613" />
                  <Point X="-28.49804296875" Y="-0.308792572021" />
                  <Point X="-28.478001953125" Y="-0.300894592285" />
                  <Point X="-28.468205078125" Y="-0.296375427246" />
                  <Point X="-28.446068359375" Y="-0.284595581055" />
                  <Point X="-28.426994140625" Y="-0.27295513916" />
                  <Point X="-28.405810546875" Y="-0.258252929688" />
                  <Point X="-28.396482421875" Y="-0.250873352051" />
                  <Point X="-28.37252734375" Y="-0.229349502563" />
                  <Point X="-28.35798828125" Y="-0.212866638184" />
                  <Point X="-28.33737109375" Y="-0.183173721313" />
                  <Point X="-28.32848828125" Y="-0.167337509155" />
                  <Point X="-28.318873046875" Y="-0.145543304443" />
                  <Point X="-28.31124609375" Y="-0.125165924072" />
                  <Point X="-28.304185546875" Y="-0.102415039062" />
                  <Point X="-28.301576171875" Y="-0.091935195923" />
                  <Point X="-28.2961328125" Y="-0.063193470001" />
                  <Point X="-28.294501953125" Y="-0.043157474518" />
                  <Point X="-28.29532421875" Y="-0.010043711662" />
                  <Point X="-28.297189453125" Y="0.006476311207" />
                  <Point X="-28.301625" Y="0.028351959229" />
                  <Point X="-28.306375" Y="0.04691078949" />
                  <Point X="-28.313435546875" Y="0.069661827087" />
                  <Point X="-28.317666015625" Y="0.080781494141" />
                  <Point X="-28.330982421875" Y="0.110108192444" />
                  <Point X="-28.34240625" Y="0.129043487549" />
                  <Point X="-28.36471875" Y="0.157819244385" />
                  <Point X="-28.377267578125" Y="0.171128417969" />
                  <Point X="-28.395658203125" Y="0.187206298828" />
                  <Point X="-28.412380859375" Y="0.200252563477" />
                  <Point X="-28.433564453125" Y="0.214954772949" />
                  <Point X="-28.440486328125" Y="0.219329193115" />
                  <Point X="-28.46561328125" Y="0.232832473755" />
                  <Point X="-28.496564453125" Y="0.245634841919" />
                  <Point X="-28.508287109375" Y="0.249611236572" />
                  <Point X="-29.640814453125" Y="0.553071289062" />
                  <Point X="-29.7854453125" Y="0.591825012207" />
                  <Point X="-29.73133203125" Y="0.957517211914" />
                  <Point X="-29.69801953125" Y="1.080452880859" />
                  <Point X="-29.6335859375" Y="1.318237060547" />
                  <Point X="-29.029841796875" Y="1.238752685547" />
                  <Point X="-28.778064453125" Y="1.20560559082" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.20470324707" />
                  <Point X="-28.71514453125" Y="1.206589477539" />
                  <Point X="-28.704890625" Y="1.208053710938" />
                  <Point X="-28.684599609375" Y="1.21208984375" />
                  <Point X="-28.6600234375" Y="1.219246582031" />
                  <Point X="-28.61313671875" Y="1.234029418945" />
                  <Point X="-28.60344140625" Y="1.237678955078" />
                  <Point X="-28.58451171875" Y="1.246009277344" />
                  <Point X="-28.57527734375" Y="1.250690185547" />
                  <Point X="-28.556533203125" Y="1.261512207031" />
                  <Point X="-28.547859375" Y="1.267172119141" />
                  <Point X="-28.531177734375" Y="1.279403564453" />
                  <Point X="-28.51592578125" Y="1.293378662109" />
                  <Point X="-28.502287109375" Y="1.308930908203" />
                  <Point X="-28.495892578125" Y="1.317080200195" />
                  <Point X="-28.483478515625" Y="1.334810302734" />
                  <Point X="-28.4780078125" Y="1.343607543945" />
                  <Point X="-28.46805859375" Y="1.361744140625" />
                  <Point X="-28.45774609375" Y="1.385169189453" />
                  <Point X="-28.43893359375" Y="1.430587280273" />
                  <Point X="-28.4355" Y="1.440349121094" />
                  <Point X="-28.429712890625" Y="1.460198852539" />
                  <Point X="-28.427359375" Y="1.470286621094" />
                  <Point X="-28.423599609375" Y="1.491602050781" />
                  <Point X="-28.422359375" Y="1.501889526367" />
                  <Point X="-28.421005859375" Y="1.522535888672" />
                  <Point X="-28.421908203125" Y="1.543206298828" />
                  <Point X="-28.425056640625" Y="1.56365612793" />
                  <Point X="-28.427189453125" Y="1.573794555664" />
                  <Point X="-28.43279296875" Y="1.594701293945" />
                  <Point X="-28.436015625" Y="1.604544921875" />
                  <Point X="-28.443509765625" Y="1.623812133789" />
                  <Point X="-28.4548203125" Y="1.646759277344" />
                  <Point X="-28.477521484375" Y="1.690365234375" />
                  <Point X="-28.482794921875" Y="1.699276977539" />
                  <Point X="-28.4942890625" Y="1.716481933594" />
                  <Point X="-28.500509765625" Y="1.724775024414" />
                  <Point X="-28.514421875" Y="1.741354125977" />
                  <Point X="-28.521505859375" Y="1.748916015625" />
                  <Point X="-28.536447265625" Y="1.763217895508" />
                  <Point X="-28.5443046875" Y="1.769958618164" />
                  <Point X="-29.193923828125" Y="2.268430175781" />
                  <Point X="-29.22761328125" Y="2.294281494141" />
                  <Point X="-29.002283203125" Y="2.680324707031" />
                  <Point X="-28.9140625" Y="2.793722412109" />
                  <Point X="-28.726337890625" Y="3.035013671875" />
                  <Point X="-28.4087890625" Y="2.851676269531" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.1348203125" Y="2.729386230469" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310791016" />
                  <Point X="-27.97643359375" Y="2.734227294922" />
                  <Point X="-27.95699609375" Y="2.741302001953" />
                  <Point X="-27.9384453125" Y="2.750450439453" />
                  <Point X="-27.929419921875" Y="2.755530761719" />
                  <Point X="-27.911166015625" Y="2.767159667969" />
                  <Point X="-27.902748046875" Y="2.773192138672" />
                  <Point X="-27.8866171875" Y="2.786138183594" />
                  <Point X="-27.878904296875" Y="2.793051757812" />
                  <Point X="-27.864529296875" Y="2.807426269531" />
                  <Point X="-27.8181796875" Y="2.853775146484" />
                  <Point X="-27.811263671875" Y="2.861489013672" />
                  <Point X="-27.798314453125" Y="2.877623535156" />
                  <Point X="-27.79228125" Y="2.886044189453" />
                  <Point X="-27.78065234375" Y="2.904298828125" />
                  <Point X="-27.7755703125" Y="2.913325683594" />
                  <Point X="-27.766423828125" Y="2.931874267578" />
                  <Point X="-27.759353515625" Y="2.951299072266" />
                  <Point X="-27.754435546875" Y="2.971387695312" />
                  <Point X="-27.7525234375" Y="2.981573242188" />
                  <Point X="-27.749697265625" Y="3.003032226562" />
                  <Point X="-27.748908203125" Y="3.013364990234" />
                  <Point X="-27.74845703125" Y="3.034048095703" />
                  <Point X="-27.748794921875" Y="3.0443984375" />
                  <Point X="-27.75056640625" Y="3.064649902344" />
                  <Point X="-27.756279296875" Y="3.129947998047" />
                  <Point X="-27.757744140625" Y="3.140204833984" />
                  <Point X="-27.76178125" Y="3.160498779297" />
                  <Point X="-27.764353515625" Y="3.170535888672" />
                  <Point X="-27.77086328125" Y="3.191177978516" />
                  <Point X="-27.774513671875" Y="3.200873291016" />
                  <Point X="-27.78284375" Y="3.21980078125" />
                  <Point X="-27.7875234375" Y="3.229032958984" />
                  <Point X="-28.059388671875" Y="3.699916015625" />
                  <Point X="-27.648373046875" Y="4.015036621094" />
                  <Point X="-27.50941796875" Y="4.092236328125" />
                  <Point X="-27.1925234375" Y="4.268295898437" />
                  <Point X="-27.16590234375" Y="4.2336015625" />
                  <Point X="-27.118564453125" Y="4.171909179688" />
                  <Point X="-27.1118203125" Y="4.164048828125" />
                  <Point X="-27.097517578125" Y="4.149107421875" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.065091796875" Y="4.121897460938" />
                  <Point X="-27.047892578125" Y="4.110405273438" />
                  <Point X="-27.03898046875" Y="4.105129394531" />
                  <Point X="-27.01644140625" Y="4.093395996094" />
                  <Point X="-26.943763671875" Y="4.0555625" />
                  <Point X="-26.934326171875" Y="4.051286621094" />
                  <Point X="-26.915044921875" Y="4.0437890625" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031378173828" />
                  <Point X="-26.780818359375" Y="4.035136474609" />
                  <Point X="-26.770728515625" Y="4.037488525391" />
                  <Point X="-26.7508671875" Y="4.043277587891" />
                  <Point X="-26.741095703125" Y="4.046714355469" />
                  <Point X="-26.717619140625" Y="4.056438964844" />
                  <Point X="-26.641921875" Y="4.087793945312" />
                  <Point X="-26.632583984375" Y="4.092272705078" />
                  <Point X="-26.614453125" Y="4.102219238281" />
                  <Point X="-26.60566015625" Y="4.1076875" />
                  <Point X="-26.5879296875" Y="4.1201015625" />
                  <Point X="-26.579783203125" Y="4.126494140625" />
                  <Point X="-26.5642265625" Y="4.140135253906" />
                  <Point X="-26.5502421875" Y="4.155395507812" />
                  <Point X="-26.538009765625" Y="4.172081054688" />
                  <Point X="-26.5323515625" Y="4.180754882812" />
                  <Point X="-26.52153125" Y="4.199499023438" />
                  <Point X="-26.516849609375" Y="4.208737304688" />
                  <Point X="-26.508521484375" Y="4.227666992188" />
                  <Point X="-26.504875" Y="4.237358398438" />
                  <Point X="-26.497234375" Y="4.261593261719" />
                  <Point X="-26.472595703125" Y="4.339734863281" />
                  <Point X="-26.470025390625" Y="4.349764160156" />
                  <Point X="-26.465990234375" Y="4.370047851562" />
                  <Point X="-26.464525390625" Y="4.380302246094" />
                  <Point X="-26.462638671875" Y="4.40186328125" />
                  <Point X="-26.46230078125" Y="4.412216308594" />
                  <Point X="-26.462751953125" Y="4.432897949219" />
                  <Point X="-26.463541015625" Y="4.4432265625" />
                  <Point X="-26.479265625" Y="4.562655761719" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.762728515625" Y="4.736034179688" />
                  <Point X="-25.36522265625" Y="4.782556152344" />
                  <Point X="-25.271412109375" Y="4.432450195312" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.978369140625" Y="4.085113037109" />
                  <Point X="-24.94783203125" Y="4.090154541016" />
                  <Point X="-24.93276953125" Y="4.093927246094" />
                  <Point X="-24.90233203125" Y="4.104259277344" />
                  <Point X="-24.8880859375" Y="4.110436035156" />
                  <Point X="-24.86079296875" Y="4.125024902344" />
                  <Point X="-24.8362421875" Y="4.143861328125" />
                  <Point X="-24.815083984375" Y="4.16644921875" />
                  <Point X="-24.8054296875" Y="4.178612792969" />
                  <Point X="-24.7875703125" Y="4.205339355469" />
                  <Point X="-24.78002734375" Y="4.218911621094" />
                  <Point X="-24.767251953125" Y="4.24710546875" />
                  <Point X="-24.76201953125" Y="4.261727050781" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-24.032751953125" Y="4.704263671875" />
                  <Point X="-23.546400390625" Y="4.58684375" />
                  <Point X="-23.457791015625" Y="4.554704101562" />
                  <Point X="-23.14175" Y="4.440074707031" />
                  <Point X="-23.054015625" Y="4.399043945312" />
                  <Point X="-22.749544921875" Y="4.256652832031" />
                  <Point X="-22.6647421875" Y="4.207246582031" />
                  <Point X="-22.37057421875" Y="4.03586328125" />
                  <Point X="-22.290634765625" Y="3.979015380859" />
                  <Point X="-22.18221875" Y="3.901915039062" />
                  <Point X="-22.73072265625" Y="2.951875488281" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.9392109375" Y="2.589721679688" />
                  <Point X="-22.950875" Y="2.562221923828" />
                  <Point X="-22.960958984375" Y="2.531134277344" />
                  <Point X="-22.963779296875" Y="2.52159765625" />
                  <Point X="-22.98016796875" Y="2.460316650391" />
                  <Point X="-22.98269140625" Y="2.447289306641" />
                  <Point X="-22.985900390625" Y="2.421013183594" />
                  <Point X="-22.9865859375" Y="2.407764404297" />
                  <Point X="-22.98594140625" Y="2.372427734375" />
                  <Point X="-22.98460546875" Y="2.353150390625" />
                  <Point X="-22.978216796875" Y="2.300159667969" />
                  <Point X="-22.97619921875" Y="2.289040039062" />
                  <Point X="-22.970857421875" Y="2.267117919922" />
                  <Point X="-22.967533203125" Y="2.256315429688" />
                  <Point X="-22.959267578125" Y="2.234225830078" />
                  <Point X="-22.954685546875" Y="2.223895019531" />
                  <Point X="-22.9443203125" Y="2.203843261719" />
                  <Point X="-22.9283671875" Y="2.179135986328" />
                  <Point X="-22.895578125" Y="2.130813720703" />
                  <Point X="-22.88730078125" Y="2.120296386719" />
                  <Point X="-22.8693515625" Y="2.100528808594" />
                  <Point X="-22.8596796875" Y="2.091278564453" />
                  <Point X="-22.831392578125" Y="2.067809570312" />
                  <Point X="-22.816755859375" Y="2.056812011719" />
                  <Point X="-22.76843359375" Y="2.0240234375" />
                  <Point X="-22.75871875" Y="2.018244628906" />
                  <Point X="-22.738673828125" Y="2.007882080078" />
                  <Point X="-22.72834375" Y="2.003298339844" />
                  <Point X="-22.706255859375" Y="1.995032470703" />
                  <Point X="-22.695451171875" Y="1.991707519531" />
                  <Point X="-22.67352734375" Y="1.986364746094" />
                  <Point X="-22.645974609375" Y="1.982365112305" />
                  <Point X="-22.592984375" Y="1.975975219727" />
                  <Point X="-22.57934375" Y="1.975319091797" />
                  <Point X="-22.552107421875" Y="1.975969482422" />
                  <Point X="-22.53851171875" Y="1.977276245117" />
                  <Point X="-22.500603515625" Y="1.983696411133" />
                  <Point X="-22.483248046875" Y="1.987478271484" />
                  <Point X="-22.421966796875" Y="2.003865356445" />
                  <Point X="-22.416005859375" Y="2.005670654297" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572875977" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.224859375" Y="2.685535400391" />
                  <Point X="-21.059591796875" Y="2.780951904297" />
                  <Point X="-20.956048828125" Y="2.637051269531" />
                  <Point X="-20.911486328125" Y="2.563408203125" />
                  <Point X="-20.86311328125" Y="2.483472167969" />
                  <Point X="-21.5585078125" Y="1.949877075195" />
                  <Point X="-21.827046875" Y="1.743819946289" />
                  <Point X="-21.831857421875" Y="1.739871337891" />
                  <Point X="-21.847876953125" Y="1.725220581055" />
                  <Point X="-21.86533203125" Y="1.706603027344" />
                  <Point X="-21.87142578125" Y="1.699419799805" />
                  <Point X="-21.885103515625" Y="1.681575561523" />
                  <Point X="-21.92920703125" Y="1.624038696289" />
                  <Point X="-21.936669921875" Y="1.612709838867" />
                  <Point X="-21.949908203125" Y="1.58910168457" />
                  <Point X="-21.95568359375" Y="1.576822387695" />
                  <Point X="-21.969248046875" Y="1.54165234375" />
                  <Point X="-21.974955078125" Y="1.524456176758" />
                  <Point X="-21.991384765625" Y="1.46571081543" />
                  <Point X="-21.993775390625" Y="1.454661621094" />
                  <Point X="-21.997228515625" Y="1.432362792969" />
                  <Point X="-21.998291015625" Y="1.42111315918" />
                  <Point X="-21.999107421875" Y="1.397542236328" />
                  <Point X="-21.998826171875" Y="1.386238525391" />
                  <Point X="-21.996921875" Y="1.363748413086" />
                  <Point X="-21.995298828125" Y="1.352562011719" />
                  <Point X="-21.991115234375" Y="1.332290893555" />
                  <Point X="-21.97762890625" Y="1.266929199219" />
                  <Point X="-21.975400390625" Y="1.258230834961" />
                  <Point X="-21.965314453125" Y="1.228607421875" />
                  <Point X="-21.949708984375" Y="1.195366821289" />
                  <Point X="-21.943076171875" Y="1.183521118164" />
                  <Point X="-21.93169921875" Y="1.166229980469" />
                  <Point X="-21.89501953125" Y="1.110475952148" />
                  <Point X="-21.88826171875" Y="1.101426513672" />
                  <Point X="-21.873712890625" Y="1.084185424805" />
                  <Point X="-21.865921875" Y="1.075994140625" />
                  <Point X="-21.848677734375" Y="1.059903442383" />
                  <Point X="-21.839966796875" Y="1.052696289062" />
                  <Point X="-21.82175390625" Y="1.039368530273" />
                  <Point X="-21.795765625" Y="1.023967895508" />
                  <Point X="-21.742609375" Y="0.994045532227" />
                  <Point X="-21.734515625" Y="0.989985168457" />
                  <Point X="-21.705318359375" Y="0.978083557129" />
                  <Point X="-21.66972265625" Y="0.968021240234" />
                  <Point X="-21.656328125" Y="0.965257751465" />
                  <Point X="-21.634037109375" Y="0.962311584473" />
                  <Point X="-21.562166015625" Y="0.952812927246" />
                  <Point X="-21.555966796875" Y="0.952199707031" />
                  <Point X="-21.53428125" Y="0.951222839355" />
                  <Point X="-21.50878125" Y="0.952032348633" />
                  <Point X="-21.49939453125" Y="0.952797180176" />
                  <Point X="-20.417322265625" Y="1.095255004883" />
                  <Point X="-20.295298828125" Y="1.111319824219" />
                  <Point X="-20.247310546875" Y="0.914204711914" />
                  <Point X="-20.233271484375" Y="0.824039123535" />
                  <Point X="-20.216126953125" Y="0.713921386719" />
                  <Point X="-21.000255859375" Y="0.503814422607" />
                  <Point X="-21.3080078125" Y="0.421352813721" />
                  <Point X="-21.31396875" Y="0.419544219971" />
                  <Point X="-21.33437890625" Y="0.41213583374" />
                  <Point X="-21.35762109375" Y="0.401617797852" />
                  <Point X="-21.365994140625" Y="0.397315887451" />
                  <Point X="-21.387892578125" Y="0.384657836914" />
                  <Point X="-21.45850390625" Y="0.343843475342" />
                  <Point X="-21.466115234375" Y="0.338946655273" />
                  <Point X="-21.491220703125" Y="0.319874023438" />
                  <Point X="-21.518005859375" Y="0.294351043701" />
                  <Point X="-21.52720703125" Y="0.284223114014" />
                  <Point X="-21.540345703125" Y="0.26748046875" />
                  <Point X="-21.582712890625" Y="0.213495452881" />
                  <Point X="-21.589150390625" Y="0.20420072937" />
                  <Point X="-21.600873046875" Y="0.184919876099" />
                  <Point X="-21.606158203125" Y="0.174933609009" />
                  <Point X="-21.615931640625" Y="0.153468917847" />
                  <Point X="-21.619994140625" Y="0.142923828125" />
                  <Point X="-21.626837890625" Y="0.1214296875" />
                  <Point X="-21.629619140625" Y="0.11048034668" />
                  <Point X="-21.634" Y="0.087610565186" />
                  <Point X="-21.64812109375" Y="0.013869703293" />
                  <Point X="-21.649392578125" Y="0.004977895737" />
                  <Point X="-21.6514140625" Y="-0.026262773514" />
                  <Point X="-21.64971875" Y="-0.062937393188" />
                  <Point X="-21.648125" Y="-0.076415847778" />
                  <Point X="-21.64374609375" Y="-0.099285636902" />
                  <Point X="-21.629623046875" Y="-0.173026489258" />
                  <Point X="-21.626841796875" Y="-0.183979400635" />
                  <Point X="-21.61999609375" Y="-0.205483047485" />
                  <Point X="-21.615931640625" Y="-0.216033935547" />
                  <Point X="-21.606158203125" Y="-0.237497451782" />
                  <Point X="-21.60087109375" Y="-0.24748550415" />
                  <Point X="-21.5891484375" Y="-0.266764404297" />
                  <Point X="-21.582712890625" Y="-0.276055114746" />
                  <Point X="-21.56957421875" Y="-0.292797912598" />
                  <Point X="-21.52720703125" Y="-0.346782958984" />
                  <Point X="-21.521279296875" Y="-0.353635498047" />
                  <Point X="-21.498859375" Y="-0.375806152344" />
                  <Point X="-21.469826171875" Y="-0.398723510742" />
                  <Point X="-21.4585078125" Y="-0.406402984619" />
                  <Point X="-21.436609375" Y="-0.419061065674" />
                  <Point X="-21.365998046875" Y="-0.459875427246" />
                  <Point X="-21.36050390625" Y="-0.462814422607" />
                  <Point X="-21.34084375" Y="-0.472015960693" />
                  <Point X="-21.31697265625" Y="-0.481027557373" />
                  <Point X="-21.3080078125" Y="-0.483912780762" />
                  <Point X="-20.315697265625" Y="-0.749801879883" />
                  <Point X="-20.21512109375" Y="-0.776750976562" />
                  <Point X="-20.23838671875" Y="-0.931063537598" />
                  <Point X="-20.256375" Y="-1.00989276123" />
                  <Point X="-20.2721953125" Y="-1.079219970703" />
                  <Point X="-21.2063125" Y="-0.956241088867" />
                  <Point X="-21.563216796875" Y="-0.909253662109" />
                  <Point X="-21.571375" Y="-0.908535705566" />
                  <Point X="-21.59990234375" Y="-0.908042419434" />
                  <Point X="-21.63327734375" Y="-0.910840942383" />
                  <Point X="-21.645517578125" Y="-0.912676147461" />
                  <Point X="-21.688498046875" Y="-0.922018005371" />
                  <Point X="-21.82708203125" Y="-0.952139831543" />
                  <Point X="-21.842125" Y="-0.956742675781" />
                  <Point X="-21.87124609375" Y="-0.968367675781" />
                  <Point X="-21.88532421875" Y="-0.975389770508" />
                  <Point X="-21.913150390625" Y="-0.992282104492" />
                  <Point X="-21.925875" Y="-1.001530456543" />
                  <Point X="-21.949625" Y="-1.022001037598" />
                  <Point X="-21.960650390625" Y="-1.033223388672" />
                  <Point X="-21.98662890625" Y="-1.064467651367" />
                  <Point X="-22.07039453125" Y="-1.165211547852" />
                  <Point X="-22.078673828125" Y="-1.176850097656" />
                  <Point X="-22.093392578125" Y="-1.201230834961" />
                  <Point X="-22.09983203125" Y="-1.21397265625" />
                  <Point X="-22.11117578125" Y="-1.241357666016" />
                  <Point X="-22.115634765625" Y="-1.254928100586" />
                  <Point X="-22.122466796875" Y="-1.282580932617" />
                  <Point X="-22.12483984375" Y="-1.296663452148" />
                  <Point X="-22.1285625" Y="-1.337126220703" />
                  <Point X="-22.140568359375" Y="-1.46759375" />
                  <Point X="-22.140708984375" Y="-1.483316894531" />
                  <Point X="-22.138392578125" Y="-1.514580566406" />
                  <Point X="-22.135935546875" Y="-1.530121337891" />
                  <Point X="-22.128205078125" Y="-1.561742675781" />
                  <Point X="-22.12321484375" Y="-1.576666992188" />
                  <Point X="-22.110841796875" Y="-1.605480712891" />
                  <Point X="-22.103458984375" Y="-1.619369873047" />
                  <Point X="-22.079673828125" Y="-1.65636706543" />
                  <Point X="-22.002978515625" Y="-1.775660522461" />
                  <Point X="-21.998255859375" Y="-1.782352783203" />
                  <Point X="-21.98019921875" Y="-1.804456176758" />
                  <Point X="-21.956505859375" Y="-1.828122680664" />
                  <Point X="-21.947201171875" Y="-1.836277954102" />
                  <Point X="-21.02633203125" Y="-2.54288671875" />
                  <Point X="-20.912828125" Y="-2.629981445312" />
                  <Point X="-20.95451171875" Y="-2.697432373047" />
                  <Point X="-20.99171875" Y="-2.750298339844" />
                  <Point X="-20.998724609375" Y="-2.760251953125" />
                  <Point X="-21.833671875" Y="-2.278194335938" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.18497265625" Y="-2.079513427734" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228892578125" Y="-2.066337646484" />
                  <Point X="-22.280044921875" Y="-2.057099609375" />
                  <Point X="-22.444982421875" Y="-2.027312011719" />
                  <Point X="-22.460640625" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.555154296875" Y="-2.035136108398" />
                  <Point X="-22.5849296875" Y="-2.044959594727" />
                  <Point X="-22.59941015625" Y="-2.051108154297" />
                  <Point X="-22.64190625" Y="-2.073473144531" />
                  <Point X="-22.778927734375" Y="-2.145587158203" />
                  <Point X="-22.79103125" Y="-2.153170410156" />
                  <Point X="-22.813962890625" Y="-2.170064453125" />
                  <Point X="-22.824791015625" Y="-2.179375244141" />
                  <Point X="-22.84575" Y="-2.200334716797" />
                  <Point X="-22.85505859375" Y="-2.211161621094" />
                  <Point X="-22.871951171875" Y="-2.234091308594" />
                  <Point X="-22.87953515625" Y="-2.246194091797" />
                  <Point X="-22.901900390625" Y="-2.288689697266" />
                  <Point X="-22.974015625" Y="-2.425712158203" />
                  <Point X="-22.9801640625" Y="-2.440193359375" />
                  <Point X="-22.98998828125" Y="-2.469971923828" />
                  <Point X="-22.9936640625" Y="-2.485269287109" />
                  <Point X="-22.99862109375" Y="-2.517442871094" />
                  <Point X="-22.999720703125" Y="-2.533134033203" />
                  <Point X="-22.99931640625" Y="-2.564484130859" />
                  <Point X="-22.9978125" Y="-2.580143066406" />
                  <Point X="-22.98857421875" Y="-2.631296142578" />
                  <Point X="-22.95878515625" Y="-2.796233398438" />
                  <Point X="-22.95698046875" Y="-2.804228759766" />
                  <Point X="-22.94876171875" Y="-2.831542236328" />
                  <Point X="-22.935927734375" Y="-2.862479980469" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.338697265625" Y="-3.898522949219" />
                  <Point X="-22.264103515625" Y="-4.027722167969" />
                  <Point X="-22.2762421875" Y="-4.036083007813" />
                  <Point X="-22.923517578125" Y="-3.192540283203" />
                  <Point X="-23.16608203125" Y="-2.876422607422" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849625732422" />
                  <Point X="-23.21674609375" Y="-2.828004638672" />
                  <Point X="-23.22669921875" Y="-2.820646972656" />
                  <Point X="-23.277150390625" Y="-2.788211669922" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.453716796875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663873535156" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.64678125" Y="-2.65159375" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920125" Y="-2.699414794922" />
                  <Point X="-23.944505859375" Y="-2.714135742188" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-23.998744140625" Y="-2.757838623047" />
                  <Point X="-24.136123046875" Y="-2.872063720703" />
                  <Point X="-24.147345703125" Y="-2.883087646484" />
                  <Point X="-24.16781640625" Y="-2.906837158203" />
                  <Point X="-24.177064453125" Y="-2.919562744141" />
                  <Point X="-24.19395703125" Y="-2.947389404297" />
                  <Point X="-24.20098046875" Y="-2.961467529297" />
                  <Point X="-24.21260546875" Y="-2.990589355469" />
                  <Point X="-24.21720703125" Y="-3.005633056641" />
                  <Point X="-24.2299453125" Y="-3.0642421875" />
                  <Point X="-24.271021484375" Y="-3.253220947266" />
                  <Point X="-24.2724140625" Y="-3.261287353516" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520263672" />
                  <Point X="-24.16691015625" Y="-4.152321777344" />
                  <Point X="-24.25010546875" Y="-3.841831542969" />
                  <Point X="-24.344931640625" Y="-3.487937011719" />
                  <Point X="-24.347392578125" Y="-3.480122314453" />
                  <Point X="-24.357853515625" Y="-3.453578613281" />
                  <Point X="-24.3732109375" Y="-3.423815429688" />
                  <Point X="-24.37958984375" Y="-3.413209716797" />
                  <Point X="-24.41834765625" Y="-3.3573671875" />
                  <Point X="-24.543318359375" Y="-3.177309326172" />
                  <Point X="-24.553328125" Y="-3.165172119141" />
                  <Point X="-24.575212890625" Y="-3.142716308594" />
                  <Point X="-24.587087890625" Y="-3.132397705078" />
                  <Point X="-24.61334375" Y="-3.113153808594" />
                  <Point X="-24.6267578125" Y="-3.104936767578" />
                  <Point X="-24.6547578125" Y="-3.090829345703" />
                  <Point X="-24.66934375" Y="-3.084938964844" />
                  <Point X="-24.729318359375" Y="-3.066324707031" />
                  <Point X="-24.922703125" Y="-3.006305664062" />
                  <Point X="-24.936623046875" Y="-3.003109619141" />
                  <Point X="-24.964783203125" Y="-2.998840087891" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.998840087891" />
                  <Point X="-25.051064453125" Y="-3.003109619141" />
                  <Point X="-25.064984375" Y="-3.006305664062" />
                  <Point X="-25.124958984375" Y="-3.024919677734" />
                  <Point X="-25.31834375" Y="-3.084938964844" />
                  <Point X="-25.3329296875" Y="-3.090829345703" />
                  <Point X="-25.3609296875" Y="-3.104936767578" />
                  <Point X="-25.37434375" Y="-3.113153808594" />
                  <Point X="-25.400599609375" Y="-3.132397705078" />
                  <Point X="-25.4124765625" Y="-3.14271875" />
                  <Point X="-25.434361328125" Y="-3.165176269531" />
                  <Point X="-25.444369140625" Y="-3.177312744141" />
                  <Point X="-25.483125" Y="-3.233155029297" />
                  <Point X="-25.608095703125" Y="-3.413213134766" />
                  <Point X="-25.61246875" Y="-3.420130615234" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213378906" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.948509765625" Y="-4.629031738281" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.131321355076" Y="-4.655948828351" />
                  <Point X="-25.960532499841" Y="-4.673899460373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.119926186718" Y="-4.561623222249" />
                  <Point X="-25.9356377358" Y="-4.580992718951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.129824579262" Y="-4.465059572711" />
                  <Point X="-25.910743411192" Y="-4.488085931343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.149231571622" Y="-4.367496529062" />
                  <Point X="-25.885849086584" Y="-4.395179143734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.56703246632" Y="-4.122956363707" />
                  <Point X="-27.479903562879" Y="-4.132113980473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.168638884678" Y="-4.269933451706" />
                  <Point X="-25.860954761976" Y="-4.302272356126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.751658053579" Y="-4.00802814599" />
                  <Point X="-27.412075094615" Y="-4.043719753201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.190518159018" Y="-4.17211056075" />
                  <Point X="-25.836060437368" Y="-4.209365568518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.895330822505" Y="-3.897404242925" />
                  <Point X="-27.309669565045" Y="-3.958959721515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.250881184792" Y="-4.070242864529" />
                  <Point X="-25.81116611276" Y="-4.11645878091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.959822197131" Y="-3.795102639755" />
                  <Point X="-27.186140460291" Y="-3.876419867044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.372583793455" Y="-3.961928118357" />
                  <Point X="-25.786271788152" Y="-4.023551993302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.907826985342" Y="-3.705044270169" />
                  <Point X="-27.052212409552" Y="-3.794972985839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.496779105752" Y="-3.853351378476" />
                  <Point X="-25.761377463544" Y="-3.930645205694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.181823832585" Y="-4.096662982137" />
                  <Point X="-24.174131414228" Y="-4.097471487886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.855831773552" Y="-3.614985900583" />
                  <Point X="-25.736483138936" Y="-3.837738418086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.208160845292" Y="-3.998371563999" />
                  <Point X="-24.186883894493" Y="-4.000607861641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.803836561763" Y="-3.524927530997" />
                  <Point X="-25.711588814328" Y="-3.744831630477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.234497857999" Y="-3.90008014586" />
                  <Point X="-24.199636374757" Y="-3.903744235397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.751841349973" Y="-3.434869161411" />
                  <Point X="-25.68669448972" Y="-3.651924842869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.26083495853" Y="-3.801788718491" />
                  <Point X="-24.212388855022" Y="-3.806880609152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.297696848495" Y="-4.008122848267" />
                  <Point X="-22.27398008053" Y="-4.010615581027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.699846138184" Y="-3.344810791825" />
                  <Point X="-25.661800165112" Y="-3.559018055261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.287172186816" Y="-3.703497277695" />
                  <Point X="-24.225141335286" Y="-3.710016982908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.377424725392" Y="-3.904219824178" />
                  <Point X="-22.332693767219" Y="-3.90892123733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.647850926395" Y="-3.254752422239" />
                  <Point X="-25.634694957091" Y="-3.466343640863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.313509415103" Y="-3.605205836898" />
                  <Point X="-24.237893815551" Y="-3.613153356663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.457152602288" Y="-3.800316800089" />
                  <Point X="-22.391407049108" Y="-3.807226936178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.595855714605" Y="-3.164694052653" />
                  <Point X="-25.582481554163" Y="-3.37630820409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.339846643389" Y="-3.506914396101" />
                  <Point X="-24.250646295815" Y="-3.516289730419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.536880479184" Y="-3.696413776" />
                  <Point X="-22.450120284891" Y="-3.705532639873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.854618694979" Y="-2.936869445662" />
                  <Point X="-28.68411160488" Y="-2.954790462974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.543860502816" Y="-3.074635683067" />
                  <Point X="-25.520690449686" Y="-3.287279424314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.384078706011" Y="-3.406742132427" />
                  <Point X="-24.26339877608" Y="-3.419426104174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.61660835608" Y="-3.592510751911" />
                  <Point X="-22.508833520674" Y="-3.603838343567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.933648872708" Y="-2.83303975271" />
                  <Point X="-28.544141348747" Y="-2.873978643146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.491865321803" Y="-2.984577310247" />
                  <Point X="-25.458900455029" Y="-3.198250527891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.455594260765" Y="-3.303702258176" />
                  <Point X="-24.275258082038" Y="-3.322656354332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.696336232976" Y="-3.488607727822" />
                  <Point X="-22.567546756457" Y="-3.502144047262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.010679705801" Y="-2.729420199347" />
                  <Point X="-28.404170978334" Y="-2.79316683533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.439870171923" Y="-2.894518934154" />
                  <Point X="-25.372198903263" Y="-3.111839941627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.52710998143" Y="-3.200662366488" />
                  <Point X="-24.265572331623" Y="-3.228151081164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.776064109873" Y="-3.384704703733" />
                  <Point X="-22.62625999224" Y="-3.400449750956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.071454869318" Y="-2.627509185704" />
                  <Point X="-28.264200607922" Y="-2.712355027513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.387875022044" Y="-2.804460558061" />
                  <Point X="-25.166798579247" Y="-3.037905099047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.652517194804" Y="-3.091958250671" />
                  <Point X="-24.24527325592" Y="-3.134761313434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.855791986769" Y="-3.280801679644" />
                  <Point X="-22.684973228023" Y="-3.298755454651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.13223032919" Y="-2.525598140913" />
                  <Point X="-28.12423023751" Y="-2.631543219697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.336845829992" Y="-2.714300655709" />
                  <Point X="-24.224974531771" Y="-3.041371508754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.935519754408" Y="-3.176898667038" />
                  <Point X="-22.743686463806" Y="-3.197061158345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.16011496705" Y="-2.427144060817" />
                  <Point X="-27.984259867097" Y="-2.55073141188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.311888291781" Y="-2.621400512118" />
                  <Point X="-24.194771842759" Y="-2.949022652727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.015246905543" Y="-3.07299571923" />
                  <Point X="-22.802399699589" Y="-3.09536686204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.050624081169" Y="-2.343128730087" />
                  <Point X="-27.844289496685" Y="-2.469919604064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.325033469236" Y="-2.524495611736" />
                  <Point X="-24.122883042487" Y="-2.861055183545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.094974056677" Y="-2.969092771421" />
                  <Point X="-22.861112935373" Y="-2.993672565734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.941133195288" Y="-2.259113399358" />
                  <Point X="-27.704319126273" Y="-2.389107796247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.392121880433" Y="-2.421921049023" />
                  <Point X="-24.020889757876" Y="-2.776251823168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.176293599132" Y="-2.865022456541" />
                  <Point X="-22.919826171156" Y="-2.891978269429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.831642309407" Y="-2.175098068628" />
                  <Point X="-23.906775731446" Y="-2.69272240409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.331655860835" Y="-2.753169938277" />
                  <Point X="-22.959498219344" Y="-2.792285282584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.722151423526" Y="-2.091082737899" />
                  <Point X="-22.977084365044" Y="-2.69491361763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.612660537645" Y="-2.007067407169" />
                  <Point X="-22.994670243934" Y="-2.59754198072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.503169651764" Y="-1.92305207644" />
                  <Point X="-22.996219578944" Y="-2.50185585249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.101321229786" Y="-2.701017694384" />
                  <Point X="-20.966973103832" Y="-2.715138251422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.393678765883" Y="-1.83903674571" />
                  <Point X="-22.965514596501" Y="-2.40955978963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.303595335015" Y="-2.584234542682" />
                  <Point X="-20.919888599164" Y="-2.62456374572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.284188046724" Y="-1.755021397458" />
                  <Point X="-22.917875694505" Y="-2.319043553435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.505869440243" Y="-2.467451390979" />
                  <Point X="-21.064134829653" Y="-2.513879569415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.174697413442" Y="-1.671006040179" />
                  <Point X="-22.868023491498" Y="-2.228759944549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.708143545472" Y="-2.350668239276" />
                  <Point X="-21.208381046339" Y="-2.403195394562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.06520678016" Y="-1.5869906829" />
                  <Point X="-22.774198319823" Y="-2.143098080908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.910417973623" Y="-2.233885053633" />
                  <Point X="-21.352627263026" Y="-2.292511219709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.985949193778" Y="-1.499797704348" />
                  <Point X="-22.622910165823" Y="-2.063475820081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.112692929961" Y="-2.117101812475" />
                  <Point X="-21.496873479712" Y="-2.181827044856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.676263942453" Y="-1.226615178771" />
                  <Point X="-29.413972283319" Y="-1.254183143021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.952989700591" Y="-1.407738600115" />
                  <Point X="-21.641119696398" Y="-2.071142870002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.701336498614" Y="-1.128456660371" />
                  <Point X="-29.010506357327" Y="-1.20106583407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.950887816236" Y="-1.312436230504" />
                  <Point X="-21.785365913085" Y="-1.960458695149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.726409054775" Y="-1.030298141971" />
                  <Point X="-28.607040431335" Y="-1.147948525118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.993537013115" Y="-1.212430332723" />
                  <Point X="-21.929612129771" Y="-1.849774520296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.746692066555" Y="-0.932643024971" />
                  <Point X="-22.023057176006" Y="-1.744429763613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.760563925532" Y="-0.835661747283" />
                  <Point X="-22.088920599899" Y="-1.641983952254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.774433953074" Y="-0.738680662086" />
                  <Point X="-22.133075627334" Y="-1.541819785304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.752246071382" Y="-0.645489415865" />
                  <Point X="-22.138555546437" Y="-1.445720536039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.496188511071" Y="-0.576878863367" />
                  <Point X="-22.12984952348" Y="-1.351112289366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.240130950759" Y="-0.508268310869" />
                  <Point X="-22.116153697664" Y="-1.257028492106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.984073390448" Y="-0.439657758371" />
                  <Point X="-22.071124678601" Y="-1.166237946161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.728015581507" Y="-0.371047232005" />
                  <Point X="-21.998196396487" Y="-1.078379730923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.479818566527" Y="-0.301610502901" />
                  <Point X="-21.912466615283" Y="-0.991867007457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.362852230397" Y="-0.218380873653" />
                  <Point X="-21.681879336475" Y="-0.9205794205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.666826790528" Y="-1.027265742096" />
                  <Point X="-20.269860531621" Y="-1.068988577165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.31236811033" Y="-0.128163681928" />
                  <Point X="-20.248573064067" Y="-0.975702693604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.294717040843" Y="-0.034495597529" />
                  <Point X="-20.230993519164" Y="-0.882027091669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.311296831968" Y="0.062770295297" />
                  <Point X="-20.216816183966" Y="-0.787993903085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.371091635354" Y="0.164578268938" />
                  <Point X="-20.731577276271" Y="-0.638367045575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.661255361505" Y="0.290598992035" />
                  <Point X="-21.315640752584" Y="-0.481456213992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.247844905303" Y="0.44777532401" />
                  <Point X="-21.509190342581" Y="-0.365590045791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.784282894912" Y="0.599680515233" />
                  <Point X="-21.592462269584" Y="-0.261314527025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.77036431512" Y="0.693740900107" />
                  <Point X="-21.631800636522" Y="-0.161656611493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.756445735327" Y="0.78780128498" />
                  <Point X="-21.649561581946" Y="-0.064266574348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.742527155535" Y="0.881861669854" />
                  <Point X="-21.644885660423" Y="0.030765253054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.726407466864" Y="0.975690708862" />
                  <Point X="-21.625925340702" Y="0.124295729708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.701239828857" Y="1.068568770074" />
                  <Point X="-21.58142113128" Y="0.215141435369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.676072186905" Y="1.161446830872" />
                  <Point X="-21.508886172465" Y="0.303040990552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.650904544375" Y="1.254324891609" />
                  <Point X="-21.386151348891" Y="0.385664327338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.599632671364" Y="1.239355051898" />
                  <Point X="-21.169631631239" Y="0.458430474553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.491396084646" Y="1.323502214782" />
                  <Point X="-20.913573371755" Y="0.527040953565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.445711443449" Y="1.414223852064" />
                  <Point X="-20.657515671787" Y="0.595651491384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.422007580145" Y="1.507255762197" />
                  <Point X="-20.401457971818" Y="0.664262029204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.435916090198" Y="1.604240892069" />
                  <Point X="-20.220303223471" Y="0.740745184472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.486685274079" Y="1.705100234874" />
                  <Point X="-21.747466776441" Y="0.996779828392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.42375071398" Y="0.962755899203" />
                  <Point X="-20.235423068932" Y="0.837857630825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.599755576232" Y="1.812507689072" />
                  <Point X="-21.89286036565" Y="1.107584596956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.020285222509" Y="1.015873253825" />
                  <Point X="-20.25241381143" Y="0.935166716381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.744001612155" Y="1.923191844926" />
                  <Point X="-21.956482508121" Y="1.209794840145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.616819731039" Y="1.068990608446" />
                  <Point X="-20.276279934947" Y="1.0331984336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.888247648079" Y="2.03387600078" />
                  <Point X="-21.986194184336" Y="1.308440949711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.032493684002" Y="2.144560156635" />
                  <Point X="-21.998838956341" Y="1.405293255361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.176739719926" Y="2.255244312489" />
                  <Point X="-21.982059752135" Y="1.499052976493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.19360798942" Y="2.352540525613" />
                  <Point X="-21.948798648807" Y="1.591080380222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.141074709147" Y="2.442542341923" />
                  <Point X="-21.886285705864" Y="1.680033291719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.088541428874" Y="2.532544158232" />
                  <Point X="-21.797806294592" Y="1.766257017419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.0360081486" Y="2.622545974541" />
                  <Point X="-21.688315320938" Y="1.850272338923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.977687628764" Y="2.711939527462" />
                  <Point X="-22.828496416125" Y="2.065633487555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.382907842366" Y="2.018800241267" />
                  <Point X="-21.578824347284" Y="1.934287660427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.908989951369" Y="2.800242397174" />
                  <Point X="-22.922900123679" Y="2.171079003603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.240205706574" Y="2.099324928973" />
                  <Point X="-21.469333442269" Y="2.018302989145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.840290463392" Y="2.888545076585" />
                  <Point X="-28.390824368654" Y="2.84130428642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.884448583588" Y="2.788082046774" />
                  <Point X="-22.971988988183" Y="2.271761737725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.100235515415" Y="2.180136755629" />
                  <Point X="-21.359842552892" Y="2.102318319507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.771590975415" Y="2.976847755997" />
                  <Point X="-28.59309802827" Y="2.958087391287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.800587630617" Y="2.874791192001" />
                  <Point X="-22.985684769347" Y="2.368724508889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.960265324256" Y="2.260948582286" />
                  <Point X="-21.250351663515" Y="2.186333649869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.75584944106" Y="2.965612305359" />
                  <Point X="-22.979296216672" Y="2.463576331504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.820295133097" Y="2.341760408943" />
                  <Point X="-21.140860774138" Y="2.270348980231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.750207119014" Y="3.060542559974" />
                  <Point X="-22.952791417505" Y="2.556313851415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.680324941938" Y="2.4225722356" />
                  <Point X="-21.031369884761" Y="2.354364310593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.761127733463" Y="3.157213649363" />
                  <Point X="-22.906744064552" Y="2.646997366156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.540354750779" Y="2.503384062257" />
                  <Point X="-20.921878995384" Y="2.438379640955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.803798315863" Y="3.257221794853" />
                  <Point X="-22.854748857358" Y="2.737055736225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.40038455962" Y="2.584195888913" />
                  <Point X="-20.891712613279" Y="2.530732312992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.86251172295" Y="3.358916109163" />
                  <Point X="-22.802753650164" Y="2.827114106294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.260414368461" Y="2.66500771557" />
                  <Point X="-20.953442219894" Y="2.632743642647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.921225130036" Y="3.460610423473" />
                  <Point X="-22.750758442969" Y="2.917172476363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.120443459119" Y="2.745819466743" />
                  <Point X="-21.027265652152" Y="2.736026084598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.979938537122" Y="3.562304737783" />
                  <Point X="-22.698763318433" Y="3.007230855119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.038651944208" Y="3.663999052094" />
                  <Point X="-22.646768245717" Y="3.097289239322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.988516435438" Y="3.754252884343" />
                  <Point X="-22.594773173001" Y="3.187347623525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.878945231936" Y="3.838259773351" />
                  <Point X="-22.542778100285" Y="3.277406007729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.769374028434" Y="3.922266662358" />
                  <Point X="-22.490783027569" Y="3.367464391932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.659802824931" Y="4.006273551365" />
                  <Point X="-22.438787954853" Y="3.457522776135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.518871259991" Y="4.086984333566" />
                  <Point X="-22.386792882137" Y="3.547581160338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.374288353196" Y="4.167311344273" />
                  <Point X="-27.083693079339" Y="4.136768550243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.637573630145" Y="4.089879506698" />
                  <Point X="-22.334797809421" Y="3.637639544541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.229705513461" Y="4.247638362029" />
                  <Point X="-27.172020735589" Y="4.241575447564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.536265544789" Y="4.174754884419" />
                  <Point X="-22.282802736705" Y="3.727697928744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.495835803605" Y="4.266028833948" />
                  <Point X="-25.120194018969" Y="4.121443056175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.915186322336" Y="4.099895878996" />
                  <Point X="-22.230807663989" Y="3.817756312947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.468256986948" Y="4.358653470073" />
                  <Point X="-25.21110832122" Y="4.226521820946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.802138934478" Y="4.183537406305" />
                  <Point X="-22.192544095582" Y="3.90925793641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.464936823482" Y="4.453827793389" />
                  <Point X="-25.242719958691" Y="4.325367624486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.758601118477" Y="4.274484684008" />
                  <Point X="-22.350162221765" Y="4.021347555585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.477690358111" Y="4.550691530452" />
                  <Point X="-25.269056559659" Y="4.423658999349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.733706711024" Y="4.367391462909" />
                  <Point X="-22.54471677123" Y="4.137319349282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.262063181292" Y="4.623551487488" />
                  <Point X="-25.295393606859" Y="4.521950421113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.70881230357" Y="4.460298241809" />
                  <Point X="-22.744766073029" Y="4.253868664722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.014252075685" Y="4.693028777302" />
                  <Point X="-25.321730697891" Y="4.620241847483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.683917896117" Y="4.55320502071" />
                  <Point X="-23.006717543258" Y="4.376924160236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.645316884999" Y="4.74977541278" />
                  <Point X="-25.348067788922" Y="4.718533273854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.659023488663" Y="4.646111799611" />
                  <Point X="-23.322515661904" Y="4.505639166553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.63412908121" Y="4.739018578512" />
                  <Point X="-23.824043430216" Y="4.653875145664" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.4336328125" Y="-3.891006835938" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.5744375" Y="-3.465701660156" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.785638671875" Y="-3.247785644531" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.068638671875" Y="-3.206380615234" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643798828" />
                  <Point X="-25.32703515625" Y="-3.341486083984" />
                  <Point X="-25.452005859375" Y="-3.521544189453" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.764982421875" Y="-4.678207519531" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.8729375" Y="-4.982186523438" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.168783203125" Y="-4.920431152344" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.320564453125" Y="-4.637743164062" />
                  <Point X="-26.3091484375" Y="-4.5510390625" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.324009765625" Y="-4.462725097656" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.4415" Y="-4.154204101562" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.72252734375" Y="-3.980959472656" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791015625" />
                  <Point X="-27.050943359375" Y="-4.014593994141" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157293945313" />
                  <Point X="-27.431390625" Y="-4.381002929688" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.522642578125" Y="-4.373913574219" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-27.950740234375" Y="-4.094535644531" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.682255859375" Y="-2.934346679688" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597591796875" />
                  <Point X="-27.51398046875" Y="-2.568762207031" />
                  <Point X="-27.53132421875" Y="-2.551417724609" />
                  <Point X="-27.560154296875" Y="-2.537198974609" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.57585546875" Y="-3.111681884766" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-28.898466796875" Y="-3.192969238281" />
                  <Point X="-29.161697265625" Y="-2.847136962891" />
                  <Point X="-29.2297421875" Y="-2.733036132812" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-28.472119140625" Y="-1.659737060547" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.15253515625" Y="-1.411081542969" />
                  <Point X="-28.143998046875" Y="-1.388972045898" />
                  <Point X="-28.1381171875" Y="-1.366266479492" />
                  <Point X="-28.136650390625" Y="-1.350051513672" />
                  <Point X="-28.14865625" Y="-1.321069091797" />
                  <Point X="-28.167427734375" Y="-1.306949951172" />
                  <Point X="-28.187640625" Y="-1.295053100586" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.461951171875" Y="-1.452139282227" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.8244375" Y="-1.414251464844" />
                  <Point X="-29.927392578125" Y="-1.011187316895" />
                  <Point X="-29.94539453125" Y="-0.88532598877" />
                  <Point X="-29.99839453125" Y="-0.514742248535" />
                  <Point X="-28.908701171875" Y="-0.222759277344" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.535326171875" Y="-0.116865516663" />
                  <Point X="-28.514142578125" Y="-0.10216317749" />
                  <Point X="-28.502322265625" Y="-0.090645019531" />
                  <Point X="-28.49270703125" Y="-0.068850776672" />
                  <Point X="-28.485646484375" Y="-0.046099815369" />
                  <Point X="-28.483400390625" Y="-0.031280040741" />
                  <Point X="-28.4878359375" Y="-0.009404351234" />
                  <Point X="-28.494896484375" Y="0.01334661293" />
                  <Point X="-28.502322265625" Y="0.028084936142" />
                  <Point X="-28.520712890625" Y="0.044162780762" />
                  <Point X="-28.541896484375" Y="0.058865123749" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.689990234375" Y="0.369545410156" />
                  <Point X="-29.998185546875" Y="0.452125976563" />
                  <Point X="-29.98399609375" Y="0.548014892578" />
                  <Point X="-29.91764453125" Y="0.996414916992" />
                  <Point X="-29.88140625" Y="1.130146850586" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.005041015625" Y="1.427127197266" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.7171640625" Y="1.400451171875" />
                  <Point X="-28.67027734375" Y="1.415234008789" />
                  <Point X="-28.651533203125" Y="1.426056152344" />
                  <Point X="-28.639119140625" Y="1.443786132813" />
                  <Point X="-28.63328515625" Y="1.457872192383" />
                  <Point X="-28.61447265625" Y="1.503290527344" />
                  <Point X="-28.610712890625" Y="1.524606079102" />
                  <Point X="-28.61631640625" Y="1.545512817383" />
                  <Point X="-28.62335546875" Y="1.559036621094" />
                  <Point X="-28.646056640625" Y="1.602642578125" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-29.309587890625" Y="2.117693359375" />
                  <Point X="-29.47610546875" Y="2.245466308594" />
                  <Point X="-29.4178359375" Y="2.345296142578" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-29.0640234375" Y="2.910389648438" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.3137890625" Y="3.016221191406" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.11826171875" Y="2.918663085938" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927404296875" />
                  <Point X="-27.998876953125" Y="2.941778808594" />
                  <Point X="-27.95252734375" Y="2.988127685547" />
                  <Point X="-27.9408984375" Y="3.006382324219" />
                  <Point X="-27.938072265625" Y="3.027841308594" />
                  <Point X="-27.93984375" Y="3.048092773438" />
                  <Point X="-27.945556640625" Y="3.113390869141" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.23993359375" Y="3.632632568359" />
                  <Point X="-28.30727734375" Y="3.749276611328" />
                  <Point X="-28.201919921875" Y="3.830054199219" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.60169140625" Y="4.258325195312" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.0151640625" Y="4.349265625" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.92870703125" Y="4.261927246094" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.790330078125" Y="4.231975097656" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.69690234375" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.29448828125" />
                  <Point X="-26.67844140625" Y="4.318723144531" />
                  <Point X="-26.653802734375" Y="4.396864746094" />
                  <Point X="-26.651916015625" Y="4.41842578125" />
                  <Point X="-26.68464453125" Y="4.667013671875" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.549408203125" Y="4.740315917969" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.784814453125" Y="4.92474609375" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.087884765625" Y="4.481625976562" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176269531" />
                  <Point X="-24.945546875" Y="4.310902832031" />
                  <Point X="-24.798046875" Y="4.86137890625" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.647365234375" Y="4.978722167969" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.988162109375" Y="4.88895703125" />
                  <Point X="-23.491544921875" Y="4.76905859375" />
                  <Point X="-23.393005859375" Y="4.733317382812" />
                  <Point X="-23.06896484375" Y="4.615785644531" />
                  <Point X="-22.97352734375" Y="4.571153320312" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.56909765625" Y="4.371417480469" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-22.180521484375" Y="4.133854980469" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.5661796875" Y="2.856875488281" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.780228515625" Y="2.472510253906" />
                  <Point X="-22.7966171875" Y="2.411229248047" />
                  <Point X="-22.79597265625" Y="2.375892578125" />
                  <Point X="-22.789583984375" Y="2.322901855469" />
                  <Point X="-22.781318359375" Y="2.300812255859" />
                  <Point X="-22.7711484375" Y="2.285825927734" />
                  <Point X="-22.738359375" Y="2.237503662109" />
                  <Point X="-22.710072265625" Y="2.214034667969" />
                  <Point X="-22.66175" Y="2.18124609375" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.623228515625" Y="2.170998535156" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.532330078125" Y="2.171028808594" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.319859375" Y="2.850080322266" />
                  <Point X="-21.005751953125" Y="3.031430419922" />
                  <Point X="-20.9763203125" Y="2.990527587891" />
                  <Point X="-20.79740234375" Y="2.741874267578" />
                  <Point X="-20.748931640625" Y="2.661773925781" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.44284375" Y="1.799140014648" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583832885742" />
                  <Point X="-21.734306640625" Y="1.565988647461" />
                  <Point X="-21.77841015625" Y="1.508451782227" />
                  <Point X="-21.791974609375" Y="1.473281738281" />
                  <Point X="-21.808404296875" Y="1.414536254883" />
                  <Point X="-21.809220703125" Y="1.390965332031" />
                  <Point X="-21.805037109375" Y="1.370694213867" />
                  <Point X="-21.79155078125" Y="1.305332519531" />
                  <Point X="-21.784353515625" Y="1.287955810547" />
                  <Point X="-21.7729765625" Y="1.270664550781" />
                  <Point X="-21.736296875" Y="1.214910522461" />
                  <Point X="-21.719052734375" Y="1.198819946289" />
                  <Point X="-21.70256640625" Y="1.189540039062" />
                  <Point X="-21.64941015625" Y="1.159617553711" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.609142578125" Y="1.150673706055" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.442123046875" Y="1.283629516602" />
                  <Point X="-20.151025390625" Y="1.321953125" />
                  <Point X="-20.137654296875" Y="1.267027832031" />
                  <Point X="-20.060806640625" Y="0.951365905762" />
                  <Point X="-20.045533203125" Y="0.853269775391" />
                  <Point X="-20.002140625" Y="0.57455645752" />
                  <Point X="-20.951080078125" Y="0.320288635254" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819351196" />
                  <Point X="-21.292810546875" Y="0.220161453247" />
                  <Point X="-21.363421875" Y="0.179346984863" />
                  <Point X="-21.377734375" Y="0.166926849365" />
                  <Point X="-21.390873046875" Y="0.150184173584" />
                  <Point X="-21.433240234375" Y="0.096199172974" />
                  <Point X="-21.443013671875" Y="0.07473449707" />
                  <Point X="-21.44739453125" Y="0.05186473465" />
                  <Point X="-21.461515625" Y="-0.021876060486" />
                  <Point X="-21.461515625" Y="-0.04068523407" />
                  <Point X="-21.45713671875" Y="-0.063554851532" />
                  <Point X="-21.443013671875" Y="-0.137295791626" />
                  <Point X="-21.433240234375" Y="-0.158759246826" />
                  <Point X="-21.4201015625" Y="-0.175501922607" />
                  <Point X="-21.377734375" Y="-0.229486923218" />
                  <Point X="-21.363421875" Y="-0.241907058716" />
                  <Point X="-21.3415234375" Y="-0.254565109253" />
                  <Point X="-21.270912109375" Y="-0.295379425049" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.266521484375" Y="-0.566276000977" />
                  <Point X="-20.001931640625" Y="-0.637172546387" />
                  <Point X="-20.008662109375" Y="-0.681819396973" />
                  <Point X="-20.051568359375" Y="-0.966412658691" />
                  <Point X="-20.07113671875" Y="-1.052163208008" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-21.23111328125" Y="-1.144615600586" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.648142578125" Y="-1.107683105469" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.814552734375" Y="-1.154697265625" />
                  <Point X="-21.84053125" Y="-1.18594152832" />
                  <Point X="-21.924296875" Y="-1.286685424805" />
                  <Point X="-21.935640625" Y="-1.31407043457" />
                  <Point X="-21.93936328125" Y="-1.354533203125" />
                  <Point X="-21.951369140625" Y="-1.485000732422" />
                  <Point X="-21.943638671875" Y="-1.516622070312" />
                  <Point X="-21.919853515625" Y="-1.553619262695" />
                  <Point X="-21.843158203125" Y="-1.672912719727" />
                  <Point X="-21.831537109375" Y="-1.685540771484" />
                  <Point X="-20.91066796875" Y="-2.392149658203" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.674921875" Y="-2.606432861328" />
                  <Point X="-20.795865234375" Y="-2.802138427734" />
                  <Point X="-20.836341796875" Y="-2.859651123047" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.928671875" Y="-2.442739257812" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.313810546875" Y="-2.244074707031" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.55341796875" Y="-2.241609863281" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.334683349609" />
                  <Point X="-22.733763671875" Y="-2.377178955078" />
                  <Point X="-22.80587890625" Y="-2.514201416016" />
                  <Point X="-22.8108359375" Y="-2.546375" />
                  <Point X="-22.80159765625" Y="-2.597528076172" />
                  <Point X="-22.77180859375" Y="-2.762465332031" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.174154296875" Y="-3.803522949219" />
                  <Point X="-22.01332421875" Y="-4.082087646484" />
                  <Point X="-22.021189453125" Y="-4.087705078125" />
                  <Point X="-22.16470703125" Y="-4.190216308594" />
                  <Point X="-22.209953125" Y="-4.219503417969" />
                  <Point X="-22.32022265625" Y="-4.29087890625" />
                  <Point X="-23.074255859375" Y="-3.308205078125" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.379900390625" Y="-2.948031982422" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.62937109375" Y="-2.840794433594" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.877271484375" Y="-2.903934326172" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045986083984" />
                  <Point X="-24.04428125" Y="-3.104595214844" />
                  <Point X="-24.085357421875" Y="-3.293573974609" />
                  <Point X="-24.0860703125" Y="-3.310719970703" />
                  <Point X="-23.91837109375" Y="-4.584512695312" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.0474609375" Y="-4.970841796875" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#140" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.045744944593" Y="4.525846939695" Z="0.6" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.6" />
                  <Point X="-0.796784583478" Y="5.005687487782" Z="0.6" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.6" />
                  <Point X="-1.569062786887" Y="4.819739703738" Z="0.6" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.6" />
                  <Point X="-1.740373088827" Y="4.691768543442" Z="0.6" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.6" />
                  <Point X="-1.732283985967" Y="4.365038704621" Z="0.6" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.6" />
                  <Point X="-1.815622555934" Y="4.309448942664" Z="0.6" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.6" />
                  <Point X="-1.911775591075" Y="4.337557860966" Z="0.6" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.6" />
                  <Point X="-1.98165327218" Y="4.410983523897" Z="0.6" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.6" />
                  <Point X="-2.632132117099" Y="4.333312995704" Z="0.6" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.6" />
                  <Point X="-3.238498003977" Y="3.901014203538" Z="0.6" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.6" />
                  <Point X="-3.289391418594" Y="3.638912686505" Z="0.6" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.6" />
                  <Point X="-2.995811788059" Y="3.075014839457" Z="0.6" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.6" />
                  <Point X="-3.040389049484" Y="3.008414518402" Z="0.6" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.6" />
                  <Point X="-3.12006162615" Y="2.999752911114" Z="0.6" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.6" />
                  <Point X="-3.294946661792" Y="3.090802530152" Z="0.6" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.6" />
                  <Point X="-4.109642638554" Y="2.972372117175" Z="0.6" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.6" />
                  <Point X="-4.46717423343" Y="2.401781179442" Z="0.6" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.6" />
                  <Point X="-4.346183281299" Y="2.109305784195" Z="0.6" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.6" />
                  <Point X="-3.67386246651" Y="1.567228160404" Z="0.6" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.6" />
                  <Point X="-3.685635345915" Y="1.508285989684" Z="0.6" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.6" />
                  <Point X="-3.738355240003" Y="1.479417236516" Z="0.6" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.6" />
                  <Point X="-4.004671927293" Y="1.507979465187" Z="0.6" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.6" />
                  <Point X="-4.935823105899" Y="1.174504172982" Z="0.6" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.6" />
                  <Point X="-5.039614891863" Y="0.58661383404" Z="0.6" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.6" />
                  <Point X="-4.709089184167" Y="0.352529119912" Z="0.6" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.6" />
                  <Point X="-3.555377533504" Y="0.034366680047" Z="0.6" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.6" />
                  <Point X="-3.541746670701" Y="0.007055915643" Z="0.6" />
                  <Point X="-3.539556741714" Y="0" Z="0.6" />
                  <Point X="-3.546617930434" Y="-0.022751035418" Z="0.6" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.6" />
                  <Point X="-3.569991061619" Y="-0.044509303087" Z="0.6" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.6" />
                  <Point X="-3.927798604606" Y="-0.143182936844" Z="0.6" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.6" />
                  <Point X="-5.001046697734" Y="-0.861125016538" Z="0.6" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.6" />
                  <Point X="-4.879040117186" Y="-1.395345540679" Z="0.6" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.6" />
                  <Point X="-4.461582886387" Y="-1.470431499045" Z="0.6" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.6" />
                  <Point X="-3.198945275209" Y="-1.318760160742" Z="0.6" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.6" />
                  <Point X="-3.198556990103" Y="-1.345155359404" Z="0.6" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.6" />
                  <Point X="-3.508713908788" Y="-1.588789474277" Z="0.6" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.6" />
                  <Point X="-4.278843160736" Y="-2.727365846712" Z="0.6" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.6" />
                  <Point X="-3.944408347177" Y="-3.1919722848" Z="0.6" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.6" />
                  <Point X="-3.557011647708" Y="-3.123703017983" Z="0.6" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.6" />
                  <Point X="-2.559597722396" Y="-2.568732746736" Z="0.6" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.6" />
                  <Point X="-2.731713995185" Y="-2.878066734784" Z="0.6" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.6" />
                  <Point X="-2.987400836144" Y="-4.102872883653" Z="0.6" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.6" />
                  <Point X="-2.555122731483" Y="-4.385144269316" Z="0.6" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.6" />
                  <Point X="-2.39788038955" Y="-4.380161308895" Z="0.6" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.6" />
                  <Point X="-2.029321897291" Y="-4.024887358065" Z="0.6" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.6" />
                  <Point X="-1.73195293951" Y="-3.999572505238" Z="0.6" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.6" />
                  <Point X="-1.480623603328" Y="-4.160515883274" Z="0.6" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.6" />
                  <Point X="-1.379206620598" Y="-4.441200149121" Z="0.6" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.6" />
                  <Point X="-1.376293318593" Y="-4.599936081094" Z="0.6" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.6" />
                  <Point X="-1.187399423128" Y="-4.937573802288" Z="0.6" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.6" />
                  <Point X="-0.888621198187" Y="-4.999990700724" Z="0.6" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.6" />
                  <Point X="-0.722842284261" Y="-4.659868236804" Z="0.6" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.6" />
                  <Point X="-0.292116778606" Y="-3.338715132313" Z="0.6" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.6" />
                  <Point X="-0.059975281863" Y="-3.222853482634" Z="0.6" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.6" />
                  <Point X="0.193383797498" Y="-3.264258562654" Z="0.6" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.6" />
                  <Point X="0.378329080827" Y="-3.462930623455" Z="0.6" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.6" />
                  <Point X="0.511912576141" Y="-3.872667793011" Z="0.6" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.6" />
                  <Point X="0.955319495944" Y="-4.988756712448" Z="0.6" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.6" />
                  <Point X="1.134669874037" Y="-4.951045646084" Z="0.6" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.6" />
                  <Point X="1.125043783872" Y="-4.546706316799" Z="0.6" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.6" />
                  <Point X="0.998421085363" Y="-3.083935087217" Z="0.6" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.6" />
                  <Point X="1.148537915705" Y="-2.91110079488" Z="0.6" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.6" />
                  <Point X="1.369054060549" Y="-2.859303971334" Z="0.6" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.6" />
                  <Point X="1.586903214758" Y="-2.958810142491" Z="0.6" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.6" />
                  <Point X="1.879919658255" Y="-3.307363059802" Z="0.6" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.6" />
                  <Point X="2.81105937212" Y="-4.230197569533" Z="0.6" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.6" />
                  <Point X="3.001715851659" Y="-4.097112251915" Z="0.6" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.6" />
                  <Point X="2.862988969318" Y="-3.747242848765" Z="0.6" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.6" />
                  <Point X="2.241449315031" Y="-2.557361213372" Z="0.6" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.6" />
                  <Point X="2.304325063252" Y="-2.369185779704" Z="0.6" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.6" />
                  <Point X="2.463712598668" Y="-2.254576246715" Z="0.6" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.6" />
                  <Point X="2.67114559014" Y="-2.26199869446" Z="0.6" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.6" />
                  <Point X="3.040170748342" Y="-2.454760530956" Z="0.6" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.6" />
                  <Point X="4.198389134189" Y="-2.857148380933" Z="0.6" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.6" />
                  <Point X="4.361454874305" Y="-2.601437989508" Z="0.6" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.6" />
                  <Point X="4.113613405915" Y="-2.321201973159" Z="0.6" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.6" />
                  <Point X="3.116048255206" Y="-1.495299666792" Z="0.6" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.6" />
                  <Point X="3.104268092336" Y="-1.327834925502" Z="0.6" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.6" />
                  <Point X="3.191756859995" Y="-1.186628456608" Z="0.6" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.6" />
                  <Point X="3.356319865131" Y="-1.12526230415" Z="0.6" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.6" />
                  <Point X="3.75620495042" Y="-1.16290786747" Z="0.6" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.6" />
                  <Point X="4.971451688861" Y="-1.032007228156" Z="0.6" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.6" />
                  <Point X="5.034622247469" Y="-0.657993373377" Z="0.6" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.6" />
                  <Point X="4.740263723245" Y="-0.486699562078" Z="0.6" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.6" />
                  <Point X="3.677341722367" Y="-0.179996226331" Z="0.6" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.6" />
                  <Point X="3.613076128315" Y="-0.113353220375" Z="0.6" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.6" />
                  <Point X="3.58581452825" Y="-0.022869690964" Z="0.6" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.6" />
                  <Point X="3.595556922174" Y="0.073740840268" Z="0.6" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.6" />
                  <Point X="3.642303310086" Y="0.1505955182" Z="0.6" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.6" />
                  <Point X="3.726053691985" Y="0.208152617911" Z="0.6" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.6" />
                  <Point X="4.055704094061" Y="0.30327237069" Z="0.6" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.6" />
                  <Point X="4.997713292515" Y="0.892241707398" Z="0.6" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.6" />
                  <Point X="4.904771740163" Y="1.31013474821" Z="0.6" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.6" />
                  <Point X="4.545195356283" Y="1.364481864039" Z="0.6" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.6" />
                  <Point X="3.391250407235" Y="1.231522712239" Z="0.6" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.6" />
                  <Point X="3.315804255256" Y="1.26439100797" Z="0.6" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.6" />
                  <Point X="3.262637203432" Y="1.329425068505" Z="0.6" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.6" />
                  <Point X="3.237774638594" Y="1.412078167591" Z="0.6" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.6" />
                  <Point X="3.250020825593" Y="1.491094630085" Z="0.6" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.6" />
                  <Point X="3.299219935494" Y="1.566850780321" Z="0.6" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.6" />
                  <Point X="3.581437114599" Y="1.790752281626" Z="0.6" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.6" />
                  <Point X="4.287688831957" Y="2.718940097594" Z="0.6" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.6" />
                  <Point X="4.058108922492" Y="3.051010866978" Z="0.6" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.6" />
                  <Point X="3.648983490483" Y="2.924661638005" Z="0.6" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.6" />
                  <Point X="2.44859705846" Y="2.25061146709" Z="0.6" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.6" />
                  <Point X="2.376601075812" Y="2.251918857252" Z="0.6" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.6" />
                  <Point X="2.311844541624" Y="2.286689141941" Z="0.6" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.6" />
                  <Point X="2.264069458294" Y="2.345180318758" Z="0.6" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.6" />
                  <Point X="2.247510845507" Y="2.41315736802" Z="0.6" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.6" />
                  <Point X="2.261916475567" Y="2.490872517422" Z="0.6" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.6" />
                  <Point X="2.470963714854" Y="2.863155591472" Z="0.6" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.6" />
                  <Point X="2.842298494292" Y="4.205881881344" Z="0.6" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.6" />
                  <Point X="2.449914766763" Y="4.445900202318" Z="0.6" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.6" />
                  <Point X="2.041496510848" Y="4.647725197997" Z="0.6" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.6" />
                  <Point X="1.617886738469" Y="4.811601465918" Z="0.6" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.6" />
                  <Point X="1.017416207602" Y="4.968840584994" Z="0.6" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.6" />
                  <Point X="0.351684914208" Y="5.059730095877" Z="0.6" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.6" />
                  <Point X="0.14749961696" Y="4.905600566256" Z="0.6" />
                  <Point X="0" Y="4.355124473572" Z="0.6" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>