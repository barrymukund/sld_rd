<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#199" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3017" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.11319140625" Y="-4.719858398438" />
                  <Point X="-24.4366953125" Y="-3.512524902344" />
                  <Point X="-24.44227734375" Y="-3.497140625" />
                  <Point X="-24.457634765625" Y="-3.467376708984" />
                  <Point X="-24.589859375" Y="-3.276866699219" />
                  <Point X="-24.62136328125" Y="-3.231476318359" />
                  <Point X="-24.643248046875" Y="-3.209020263672" />
                  <Point X="-24.66950390625" Y="-3.189776611328" />
                  <Point X="-24.69750390625" Y="-3.175669189453" />
                  <Point X="-24.90211328125" Y="-3.112165771484" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.24143359375" Y="-3.1605390625" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.189776611328" />
                  <Point X="-25.344439453125" Y="-3.209020263672" />
                  <Point X="-25.36632421875" Y="-3.231477050781" />
                  <Point X="-25.498546875" Y="-3.421987060547" />
                  <Point X="-25.53005078125" Y="-3.467377441406" />
                  <Point X="-25.5381875" Y="-3.481572265625" />
                  <Point X="-25.550990234375" Y="-3.512524169922" />
                  <Point X="-25.628068359375" Y="-3.800180175781" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.025943359375" Y="-4.85571484375" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.24641796875" Y="-4.802362792969" />
                  <Point X="-26.2149609375" Y="-4.563435058594" />
                  <Point X="-26.21419921875" Y="-4.547929199219" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.265390625" Y="-4.270481445312" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.18296484375" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.512021484375" Y="-3.966000244141" />
                  <Point X="-26.556904296875" Y="-3.926639404297" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.893046875" Y="-3.874579101562" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.983419921875" Y="-3.873708984375" />
                  <Point X="-27.01446875" Y="-3.882029052734" />
                  <Point X="-27.042658203125" Y="-3.894802001953" />
                  <Point X="-27.25098828125" Y="-4.034003662109" />
                  <Point X="-27.300623046875" Y="-4.067169677734" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.335099609375" Y="-4.099461425781" />
                  <Point X="-27.378373046875" Y="-4.15585546875" />
                  <Point X="-27.480146484375" Y="-4.288489746094" />
                  <Point X="-27.723447265625" Y="-4.137844726562" />
                  <Point X="-27.80171484375" Y="-4.0893828125" />
                  <Point X="-28.10472265625" Y="-3.856078125" />
                  <Point X="-28.02495703125" Y="-3.717921142578" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647655273438" />
                  <Point X="-27.406587890625" Y="-2.616129882812" />
                  <Point X="-27.40557421875" Y="-2.585197753906" />
                  <Point X="-27.414556640625" Y="-2.555581298828" />
                  <Point X="-27.428771484375" Y="-2.526752685547" />
                  <Point X="-27.446798828125" Y="-2.501592285156" />
                  <Point X="-27.464146484375" Y="-2.484243652344" />
                  <Point X="-27.4893046875" Y="-2.46621484375" />
                  <Point X="-27.518134765625" Y="-2.451996826172" />
                  <Point X="-27.547751953125" Y="-2.443011962891" />
                  <Point X="-27.5786875" Y="-2.444024169922" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-27.887279296875" Y="-2.604436523438" />
                  <Point X="-28.8180234375" Y="-3.141801513672" />
                  <Point X="-29.021029296875" Y="-2.87509375" />
                  <Point X="-29.082857421875" Y="-2.79386328125" />
                  <Point X="-29.306142578125" Y="-2.419450195312" />
                  <Point X="-29.1578515625" Y="-2.305662841797" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.084578125" Y="-1.475592773438" />
                  <Point X="-28.06661328125" Y="-1.448461303711" />
                  <Point X="-28.053857421875" Y="-1.419834350586" />
                  <Point X="-28.047634765625" Y="-1.395810791016" />
                  <Point X="-28.04615234375" Y="-1.390087158203" />
                  <Point X="-28.04334765625" Y="-1.359656005859" />
                  <Point X="-28.045556640625" Y="-1.327985473633" />
                  <Point X="-28.052556640625" Y="-1.298240966797" />
                  <Point X="-28.068638671875" Y="-1.2722578125" />
                  <Point X="-28.089470703125" Y="-1.248301513672" />
                  <Point X="-28.11297265625" Y="-1.228767089844" />
                  <Point X="-28.134359375" Y="-1.2161796875" />
                  <Point X="-28.139455078125" Y="-1.213180419922" />
                  <Point X="-28.168716796875" Y="-1.201956665039" />
                  <Point X="-28.200603515625" Y="-1.195474975586" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-28.545126953125" Y="-1.23561730957" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.809892578125" Y="-1.087331298828" />
                  <Point X="-29.834076171875" Y="-0.992650390625" />
                  <Point X="-29.892423828125" Y="-0.584698425293" />
                  <Point X="-29.73114453125" Y="-0.541483825684" />
                  <Point X="-28.532875" Y="-0.220408309937" />
                  <Point X="-28.517490234375" Y="-0.214826828003" />
                  <Point X="-28.487728515625" Y="-0.199470199585" />
                  <Point X="-28.465314453125" Y="-0.183914260864" />
                  <Point X="-28.459974609375" Y="-0.180207992554" />
                  <Point X="-28.43751953125" Y="-0.158323776245" />
                  <Point X="-28.418275390625" Y="-0.132068588257" />
                  <Point X="-28.404166015625" Y="-0.104065689087" />
                  <Point X="-28.3966953125" Y="-0.079994010925" />
                  <Point X="-28.39490625" Y="-0.07422278595" />
                  <Point X="-28.390646484375" Y="-0.046095420837" />
                  <Point X="-28.390646484375" Y="-0.016459867477" />
                  <Point X="-28.394916015625" Y="0.011698616028" />
                  <Point X="-28.40238671875" Y="0.035770446777" />
                  <Point X="-28.404169921875" Y="0.041513889313" />
                  <Point X="-28.41826953125" Y="0.06949659729" />
                  <Point X="-28.437513671875" Y="0.095756637573" />
                  <Point X="-28.459974609375" Y="0.117647850037" />
                  <Point X="-28.482388671875" Y="0.133203781128" />
                  <Point X="-28.492552734375" Y="0.139353271484" />
                  <Point X="-28.5134609375" Y="0.150280349731" />
                  <Point X="-28.532875" Y="0.157848327637" />
                  <Point X="-28.81837109375" Y="0.234346725464" />
                  <Point X="-29.89181640625" Y="0.521975463867" />
                  <Point X="-29.840072265625" Y="0.871656860352" />
                  <Point X="-29.82448828125" Y="0.976968200684" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-29.62461328125" Y="1.412875854492" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.744984375" Y="1.299341674805" />
                  <Point X="-28.72342578125" Y="1.301227661133" />
                  <Point X="-28.70313671875" Y="1.305263427734" />
                  <Point X="-28.65353125" Y="1.320904541016" />
                  <Point X="-28.6417109375" Y="1.324630981445" />
                  <Point X="-28.62278125" Y="1.332959838867" />
                  <Point X="-28.60403515625" Y="1.343782104492" />
                  <Point X="-28.587353515625" Y="1.356012695312" />
                  <Point X="-28.57371484375" Y="1.371563842773" />
                  <Point X="-28.561298828125" Y="1.389294555664" />
                  <Point X="-28.55134765625" Y="1.407432983398" />
                  <Point X="-28.531443359375" Y="1.45548815918" />
                  <Point X="-28.526701171875" Y="1.46693737793" />
                  <Point X="-28.5209140625" Y="1.486797241211" />
                  <Point X="-28.51715625" Y="1.508112426758" />
                  <Point X="-28.515802734375" Y="1.528755004883" />
                  <Point X="-28.518951171875" Y="1.549200927734" />
                  <Point X="-28.5245546875" Y="1.570107299805" />
                  <Point X="-28.53205078125" Y="1.58937890625" />
                  <Point X="-28.556068359375" Y="1.635516113281" />
                  <Point X="-28.561791015625" Y="1.646508666992" />
                  <Point X="-28.573283203125" Y="1.663708496094" />
                  <Point X="-28.5871953125" Y="1.680287719727" />
                  <Point X="-28.60213671875" Y="1.69458996582" />
                  <Point X="-28.765896484375" Y="1.820248657227" />
                  <Point X="-29.351859375" Y="2.269873291016" />
                  <Point X="-29.14170703125" Y="2.629915771484" />
                  <Point X="-29.0811484375" Y="2.733664794922" />
                  <Point X="-28.752630859375" Y="3.155929931641" />
                  <Point X="-28.73419921875" Y="3.149248291016" />
                  <Point X="-28.20665625" Y="2.844671386719" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.077705078125" Y="2.819751953125" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.99901171875" Y="2.826504882812" />
                  <Point X="-27.9804609375" Y="2.835654541016" />
                  <Point X="-27.96220703125" Y="2.847284179688" />
                  <Point X="-27.946078125" Y="2.860228759766" />
                  <Point X="-27.897037109375" Y="2.909268310547" />
                  <Point X="-27.885353515625" Y="2.920952148438" />
                  <Point X="-27.87240625" Y="2.937083740234" />
                  <Point X="-27.86077734375" Y="2.955336914062" />
                  <Point X="-27.85162890625" Y="2.973886474609" />
                  <Point X="-27.8467109375" Y="2.993976318359" />
                  <Point X="-27.843884765625" Y="3.015433837891" />
                  <Point X="-27.84343359375" Y="3.036119628906" />
                  <Point X="-27.849478515625" Y="3.105208251953" />
                  <Point X="-27.85091796875" Y="3.121669189453" />
                  <Point X="-27.854955078125" Y="3.141958251953" />
                  <Point X="-27.86146484375" Y="3.162602539062" />
                  <Point X="-27.869794921875" Y="3.181533447266" />
                  <Point X="-27.94236328125" Y="3.307224121094" />
                  <Point X="-28.18333203125" Y="3.724595947266" />
                  <Point X="-27.806083984375" Y="4.013827880859" />
                  <Point X="-27.70062109375" Y="4.094685791016" />
                  <Point X="-27.183212890625" Y="4.382146972656" />
                  <Point X="-27.16703515625" Y="4.391134277344" />
                  <Point X="-27.0431953125" Y="4.229741210938" />
                  <Point X="-27.028888671875" Y="4.214796875" />
                  <Point X="-27.012306640625" Y="4.200883789062" />
                  <Point X="-26.995111328125" Y="4.18939453125" />
                  <Point X="-26.918216796875" Y="4.149365234375" />
                  <Point X="-26.89989453125" Y="4.139827636719" />
                  <Point X="-26.880615234375" Y="4.132330078125" />
                  <Point X="-26.8597109375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.797314453125" Y="4.128692871094" />
                  <Point X="-26.777453125" Y="4.134481933594" />
                  <Point X="-26.69736328125" Y="4.167657226563" />
                  <Point X="-26.678279296875" Y="4.175561523437" />
                  <Point X="-26.660146484375" Y="4.185509277344" />
                  <Point X="-26.642416015625" Y="4.197923828125" />
                  <Point X="-26.62686328125" Y="4.211562011719" />
                  <Point X="-26.6146328125" Y="4.228243164063" />
                  <Point X="-26.603810546875" Y="4.246987304688" />
                  <Point X="-26.595478515625" Y="4.265921386719" />
                  <Point X="-26.56941015625" Y="4.348599609375" />
                  <Point X="-26.56319921875" Y="4.368297851562" />
                  <Point X="-26.5591640625" Y="4.388583496094" />
                  <Point X="-26.55727734375" Y="4.410145507812" />
                  <Point X="-26.557728515625" Y="4.430826660156" />
                  <Point X="-26.565978515625" Y="4.493492675781" />
                  <Point X="-26.584201171875" Y="4.631897460938" />
                  <Point X="-26.086158203125" Y="4.77153125" />
                  <Point X="-25.949630859375" Y="4.80980859375" />
                  <Point X="-25.322376953125" Y="4.883219726562" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.28996484375" Y="4.868745605469" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286314941406" />
                  <Point X="-24.816599609375" Y="4.425083496094" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.275171875" Y="4.844223632812" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.6370078125" Y="4.706448242188" />
                  <Point X="-23.51897265625" Y="4.677950683594" />
                  <Point X="-23.181470703125" Y="4.555536621094" />
                  <Point X="-23.10535546875" Y="4.527929199219" />
                  <Point X="-22.778728515625" Y="4.375176757813" />
                  <Point X="-22.705423828125" Y="4.34089453125" />
                  <Point X="-22.3898671875" Y="4.15705078125" />
                  <Point X="-22.319021484375" Y="4.115775878906" />
                  <Point X="-22.05673828125" Y="3.929254150391" />
                  <Point X="-22.156578125" Y="3.756326904297" />
                  <Point X="-22.852416015625" Y="2.551098144531" />
                  <Point X="-22.857921875" Y="2.539932373047" />
                  <Point X="-22.866921875" Y="2.516057861328" />
                  <Point X="-22.88426171875" Y="2.451219726562" />
                  <Point X="-22.8863046875" Y="2.441612060547" />
                  <Point X="-22.8917734375" Y="2.407260742188" />
                  <Point X="-22.892271484375" Y="2.380949951172" />
                  <Point X="-22.885509765625" Y="2.324883544922" />
                  <Point X="-22.883900390625" Y="2.311531494141" />
                  <Point X="-22.878560546875" Y="2.289618408203" />
                  <Point X="-22.870294921875" Y="2.267524658203" />
                  <Point X="-22.859927734375" Y="2.247469970703" />
                  <Point X="-22.825234375" Y="2.196342773437" />
                  <Point X="-22.819140625" Y="2.188313232422" />
                  <Point X="-22.79757421875" Y="2.162831054688" />
                  <Point X="-22.778400390625" Y="2.145592041016" />
                  <Point X="-22.7272734375" Y="2.110899902344" />
                  <Point X="-22.715091796875" Y="2.102634521484" />
                  <Point X="-22.695044921875" Y="2.092270996094" />
                  <Point X="-22.67295703125" Y="2.084005615234" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.59496875" Y="2.071902832031" />
                  <Point X="-22.584400390625" Y="2.071222900391" />
                  <Point X="-22.552140625" Y="2.070949462891" />
                  <Point X="-22.52679296875" Y="2.074170898438" />
                  <Point X="-22.461955078125" Y="2.091509521484" />
                  <Point X="-22.456587890625" Y="2.093115722656" />
                  <Point X="-22.4290546875" Y="2.102248535156" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-22.124310546875" Y="2.27593359375" />
                  <Point X="-21.032673828125" Y="2.906190429688" />
                  <Point X="-20.91875390625" Y="2.747866455078" />
                  <Point X="-20.876720703125" Y="2.689449951172" />
                  <Point X="-20.737798828125" Y="2.459883544922" />
                  <Point X="-20.853076171875" Y="2.371427734375" />
                  <Point X="-21.76921484375" Y="1.668451171875" />
                  <Point X="-21.778576171875" Y="1.660239379883" />
                  <Point X="-21.79602734375" Y="1.641626098633" />
                  <Point X="-21.84269140625" Y="1.580749023438" />
                  <Point X="-21.847935546875" Y="1.573173339844" />
                  <Point X="-21.867521484375" Y="1.541722045898" />
                  <Point X="-21.878369140625" Y="1.51708984375" />
                  <Point X="-21.895751953125" Y="1.454934326172" />
                  <Point X="-21.89989453125" Y="1.440125366211" />
                  <Point X="-21.90334765625" Y="1.417826660156" />
                  <Point X="-21.9041640625" Y="1.394251342773" />
                  <Point X="-21.902259765625" Y="1.371765625" />
                  <Point X="-21.887990234375" Y="1.302609741211" />
                  <Point X="-21.885611328125" Y="1.293424438477" />
                  <Point X="-21.875013671875" Y="1.259573608398" />
                  <Point X="-21.863716796875" Y="1.235742431641" />
                  <Point X="-21.82490625" Y="1.176751708984" />
                  <Point X="-21.81566015625" Y="1.162696777344" />
                  <Point X="-21.801107421875" Y="1.145451660156" />
                  <Point X="-21.783861328125" Y="1.129359985352" />
                  <Point X="-21.76565234375" Y="1.116034545898" />
                  <Point X="-21.70941015625" Y="1.084375" />
                  <Point X="-21.700451171875" Y="1.079936035156" />
                  <Point X="-21.669076171875" Y="1.066395751953" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.567837890625" Y="1.049388549805" />
                  <Point X="-21.562671875" Y="1.048848999023" />
                  <Point X="-21.5314765625" Y="1.046451049805" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-21.239017578125" Y="1.082896484375" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.17211328125" Y="1.006950073242" />
                  <Point X="-20.15405859375" Y="0.93278692627" />
                  <Point X="-20.1091328125" Y="0.644238952637" />
                  <Point X="-20.233505859375" Y="0.610913024902" />
                  <Point X="-21.283419921875" Y="0.32958972168" />
                  <Point X="-21.29520703125" Y="0.325586883545" />
                  <Point X="-21.318453125" Y="0.315068237305" />
                  <Point X="-21.393162109375" Y="0.271884490967" />
                  <Point X="-21.410962890625" Y="0.261595672607" />
                  <Point X="-21.425689453125" Y="0.251095123291" />
                  <Point X="-21.45246875" Y="0.225576278687" />
                  <Point X="-21.497294921875" Y="0.16845741272" />
                  <Point X="-21.507974609375" Y="0.154848480225" />
                  <Point X="-21.519697265625" Y="0.135569900513" />
                  <Point X="-21.529470703125" Y="0.114108100891" />
                  <Point X="-21.536318359375" Y="0.09260471344" />
                  <Point X="-21.551259765625" Y="0.014583392143" />
                  <Point X="-21.55252734375" Y="0.005718378067" />
                  <Point X="-21.556087890625" Y="-0.03168277359" />
                  <Point X="-21.5548203125" Y="-0.058554225922" />
                  <Point X="-21.53987890625" Y="-0.136575546265" />
                  <Point X="-21.536318359375" Y="-0.155164703369" />
                  <Point X="-21.529470703125" Y="-0.176668243408" />
                  <Point X="-21.519697265625" Y="-0.1981300354" />
                  <Point X="-21.507974609375" Y="-0.217408630371" />
                  <Point X="-21.4631484375" Y="-0.27452734375" />
                  <Point X="-21.45694140625" Y="-0.281671813965" />
                  <Point X="-21.43194921875" Y="-0.307702209473" />
                  <Point X="-21.410962890625" Y="-0.324155670166" />
                  <Point X="-21.33625390625" Y="-0.367339447021" />
                  <Point X="-21.318453125" Y="-0.377628082275" />
                  <Point X="-21.3072890625" Y="-0.383139312744" />
                  <Point X="-21.283419921875" Y="-0.392149871826" />
                  <Point X="-21.03326953125" Y="-0.459177185059" />
                  <Point X="-20.108525390625" Y="-0.706961853027" />
                  <Point X="-20.1348984375" Y="-0.881881530762" />
                  <Point X="-20.144974609375" Y="-0.948725036621" />
                  <Point X="-20.19882421875" Y="-1.18469921875" />
                  <Point X="-20.358013671875" Y="-1.163741577148" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710144043" />
                  <Point X="-21.62533984375" Y="-1.005508728027" />
                  <Point X="-21.77196875" Y="-1.037379394531" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.836025390625" Y="-1.056596923828" />
                  <Point X="-21.863853515625" Y="-1.073490356445" />
                  <Point X="-21.8876015625" Y="-1.093960571289" />
                  <Point X="-21.97623046875" Y="-1.200552368164" />
                  <Point X="-21.997345703125" Y="-1.225948608398" />
                  <Point X="-22.0120703125" Y="-1.250336181641" />
                  <Point X="-22.023412109375" Y="-1.277722045898" />
                  <Point X="-22.030240234375" Y="-1.305366333008" />
                  <Point X="-22.042943359375" Y="-1.443407592773" />
                  <Point X="-22.04596875" Y="-1.47629699707" />
                  <Point X="-22.043650390625" Y="-1.507563842773" />
                  <Point X="-22.035919921875" Y="-1.53918359375" />
                  <Point X="-22.023548828125" Y="-1.567996826172" />
                  <Point X="-21.94240234375" Y="-1.69421496582" />
                  <Point X="-21.923068359375" Y="-1.724287475586" />
                  <Point X="-21.9130625" Y="-1.737242919922" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.65723046875" Y="-1.939036743164" />
                  <Point X="-20.786876953125" Y="-2.606882080078" />
                  <Point X="-20.84678125" Y="-2.703815917969" />
                  <Point X="-20.875205078125" Y="-2.749806884766" />
                  <Point X="-20.971017578125" Y="-2.8859453125" />
                  <Point X="-21.114603515625" Y="-2.803045410156" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.245775390625" Y="-2.159825195312" />
                  <Point X="-22.420287109375" Y="-2.12830859375" />
                  <Point X="-22.461865234375" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395507812" />
                  <Point X="-22.525390625" Y="-2.125353515625" />
                  <Point X="-22.555166015625" Y="-2.135177001953" />
                  <Point X="-22.700142578125" Y="-2.211477050781" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.757615234375" Y="-2.246549072266" />
                  <Point X="-22.77857421875" Y="-2.267508056641" />
                  <Point X="-22.795466796875" Y="-2.290438232422" />
                  <Point X="-22.871767578125" Y="-2.435414550781" />
                  <Point X="-22.889947265625" Y="-2.469956298828" />
                  <Point X="-22.899771484375" Y="-2.499735351563" />
                  <Point X="-22.904728515625" Y="-2.531909423828" />
                  <Point X="-22.90432421875" Y="-2.563259521484" />
                  <Point X="-22.872806640625" Y="-2.737771240234" />
                  <Point X="-22.865296875" Y="-2.779349853516" />
                  <Point X="-22.86101171875" Y="-2.795141601562" />
                  <Point X="-22.8481796875" Y="-2.826078125" />
                  <Point X="-22.6990078125" Y="-3.084454101562" />
                  <Point X="-22.13871484375" Y="-4.054905517578" />
                  <Point X="-22.1844375" Y="-4.087563232422" />
                  <Point X="-22.218177734375" Y="-4.111662597656" />
                  <Point X="-22.298232421875" Y="-4.163480957031" />
                  <Point X="-22.413923828125" Y="-4.012710693359" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922177490234" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.45019140625" Y="-2.789902587891" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.520009765625" Y="-2.751167236328" />
                  <Point X="-23.551630859375" Y="-2.743435546875" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.77113671875" Y="-2.758438476562" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397705078" />
                  <Point X="-23.871021484375" Y="-2.780740966797" />
                  <Point X="-23.89540234375" Y="-2.7954609375" />
                  <Point X="-24.040755859375" Y="-2.916316650391" />
                  <Point X="-24.07538671875" Y="-2.945111328125" />
                  <Point X="-24.095857421875" Y="-2.968860107422" />
                  <Point X="-24.11275" Y="-2.996685791016" />
                  <Point X="-24.124375" Y="-3.025808105469" />
                  <Point X="-24.167833984375" Y="-3.225756591797" />
                  <Point X="-24.178189453125" Y="-3.273395996094" />
                  <Point X="-24.180275390625" Y="-3.289626953125" />
                  <Point X="-24.1802578125" Y="-3.323120361328" />
                  <Point X="-24.137982421875" Y="-3.644227783203" />
                  <Point X="-23.977935546875" Y="-4.859915527344" />
                  <Point X="-23.9924296875" Y="-4.863092773438" />
                  <Point X="-24.024318359375" Y="-4.870083007812" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.007841796875" Y="-4.762455566406" />
                  <Point X="-26.058419921875" Y="-4.752637695312" />
                  <Point X="-26.14124609375" Y="-4.731327636719" />
                  <Point X="-26.1207734375" Y="-4.575835449219" />
                  <Point X="-26.120076171875" Y="-4.568096191406" />
                  <Point X="-26.11944921875" Y="-4.541030273438" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497688964844" />
                  <Point X="-26.172216796875" Y="-4.251947265625" />
                  <Point X="-26.18386328125" Y="-4.193397460938" />
                  <Point X="-26.188126953125" Y="-4.178467773438" />
                  <Point X="-26.199029296875" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135463867188" />
                  <Point X="-26.221740234375" Y="-4.107626464844" />
                  <Point X="-26.23057421875" Y="-4.094861083984" />
                  <Point X="-26.25020703125" Y="-4.070937744141" />
                  <Point X="-26.261005859375" Y="-4.059779541016" />
                  <Point X="-26.4493828125" Y="-3.894575683594" />
                  <Point X="-26.494265625" Y="-3.85521484375" />
                  <Point X="-26.50673828125" Y="-3.845965332031" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.886833984375" Y="-3.779782470703" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.992732421875" Y="-3.779166503906" />
                  <Point X="-27.008009765625" Y="-3.781946533203" />
                  <Point X="-27.03905859375" Y="-3.790266601562" />
                  <Point X="-27.053677734375" Y="-3.795497558594" />
                  <Point X="-27.0818671875" Y="-3.808270507812" />
                  <Point X="-27.0954375" Y="-3.8158125" />
                  <Point X="-27.303767578125" Y="-3.955014160156" />
                  <Point X="-27.35340234375" Y="-3.988180175781" />
                  <Point X="-27.35968359375" Y="-3.992759765625" />
                  <Point X="-27.380443359375" Y="-4.010133789063" />
                  <Point X="-27.4027578125" Y="-4.032772216797" />
                  <Point X="-27.410466796875" Y="-4.041628417969" />
                  <Point X="-27.453740234375" Y="-4.098022460938" />
                  <Point X="-27.50319921875" Y="-4.162479492188" />
                  <Point X="-27.673435546875" Y="-4.057073974609" />
                  <Point X="-27.74759375" Y="-4.011156738281" />
                  <Point X="-27.98086328125" Y="-3.831547363281" />
                  <Point X="-27.942685546875" Y="-3.765421386719" />
                  <Point X="-27.341490234375" Y="-2.724119873047" />
                  <Point X="-27.3348515625" Y="-2.7100859375" />
                  <Point X="-27.32394921875" Y="-2.681121582031" />
                  <Point X="-27.319685546875" Y="-2.666190917969" />
                  <Point X="-27.3134140625" Y="-2.634665527344" />
                  <Point X="-27.311638671875" Y="-2.619241455078" />
                  <Point X="-27.310625" Y="-2.588309326172" />
                  <Point X="-27.3146640625" Y="-2.557625244141" />
                  <Point X="-27.323646484375" Y="-2.528008789062" />
                  <Point X="-27.3293515625" Y="-2.513568359375" />
                  <Point X="-27.34356640625" Y="-2.484739746094" />
                  <Point X="-27.351546875" Y="-2.471422119141" />
                  <Point X="-27.36957421875" Y="-2.44626171875" />
                  <Point X="-27.37962109375" Y="-2.434418945313" />
                  <Point X="-27.39696875" Y="-2.4170703125" />
                  <Point X="-27.408810546875" Y="-2.407024169922" />
                  <Point X="-27.43396875" Y="-2.388995361328" />
                  <Point X="-27.44728515625" Y="-2.381012695312" />
                  <Point X="-27.476115234375" Y="-2.366794677734" />
                  <Point X="-27.490556640625" Y="-2.361087890625" />
                  <Point X="-27.520173828125" Y="-2.352103027344" />
                  <Point X="-27.550859375" Y="-2.348062744141" />
                  <Point X="-27.581794921875" Y="-2.349074951172" />
                  <Point X="-27.597220703125" Y="-2.350849365234" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-27.934779296875" Y="-2.5221640625" />
                  <Point X="-28.793087890625" Y="-3.017708496094" />
                  <Point X="-28.945435546875" Y="-2.817555419922" />
                  <Point X="-29.00401953125" Y="-2.740587646484" />
                  <Point X="-29.181265625" Y="-2.443373779297" />
                  <Point X="-29.10001953125" Y="-2.381031494141" />
                  <Point X="-28.048123046875" Y="-1.573882080078" />
                  <Point X="-28.03648046875" Y="-1.563308349609" />
                  <Point X="-28.015103515625" Y="-1.540387695312" />
                  <Point X="-28.005369140625" Y="-1.528040771484" />
                  <Point X="-27.987404296875" Y="-1.500909301758" />
                  <Point X="-27.979837890625" Y="-1.487127319336" />
                  <Point X="-27.96708203125" Y="-1.458500488281" />
                  <Point X="-27.961892578125" Y="-1.443655517578" />
                  <Point X="-27.955669921875" Y="-1.419631713867" />
                  <Point X="-27.951552734375" Y="-1.398806030273" />
                  <Point X="-27.948748046875" Y="-1.368374755859" />
                  <Point X="-27.948578125" Y="-1.353045898438" />
                  <Point X="-27.950787109375" Y="-1.321375366211" />
                  <Point X="-27.95308203125" Y="-1.30622277832" />
                  <Point X="-27.96008203125" Y="-1.276478393555" />
                  <Point X="-27.97177734375" Y="-1.248243530273" />
                  <Point X="-27.987859375" Y="-1.222260375977" />
                  <Point X="-27.996951171875" Y="-1.209920043945" />
                  <Point X="-28.017783203125" Y="-1.185963745117" />
                  <Point X="-28.02874609375" Y="-1.175243286133" />
                  <Point X="-28.052248046875" Y="-1.155708862305" />
                  <Point X="-28.06478515625" Y="-1.146895019531" />
                  <Point X="-28.086171875" Y="-1.134307617188" />
                  <Point X="-28.10543359375" Y="-1.124481445312" />
                  <Point X="-28.1346953125" Y="-1.11325769043" />
                  <Point X="-28.14979296875" Y="-1.108860473633" />
                  <Point X="-28.1816796875" Y="-1.10237890625" />
                  <Point X="-28.197296875" Y="-1.100532592773" />
                  <Point X="-28.22862109375" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-28.55752734375" Y="-1.141430053711" />
                  <Point X="-29.66091796875" Y="-1.286694213867" />
                  <Point X="-29.71784765625" Y="-1.063820068359" />
                  <Point X="-29.740759765625" Y="-0.974113830566" />
                  <Point X="-29.786451171875" Y="-0.654654541016" />
                  <Point X="-29.706556640625" Y="-0.633246765137" />
                  <Point X="-28.508287109375" Y="-0.312171325684" />
                  <Point X="-28.5004765625" Y="-0.30971282959" />
                  <Point X="-28.473927734375" Y="-0.299250671387" />
                  <Point X="-28.444166015625" Y="-0.283894042969" />
                  <Point X="-28.4335625" Y="-0.277515594482" />
                  <Point X="-28.4111484375" Y="-0.261959686279" />
                  <Point X="-28.393669921875" Y="-0.248242385864" />
                  <Point X="-28.37121484375" Y="-0.226358108521" />
                  <Point X="-28.3608984375" Y="-0.214484924316" />
                  <Point X="-28.341654296875" Y="-0.188229797363" />
                  <Point X="-28.333435546875" Y="-0.174815246582" />
                  <Point X="-28.319326171875" Y="-0.146812301636" />
                  <Point X="-28.313435546875" Y="-0.132224227905" />
                  <Point X="-28.30596484375" Y="-0.108152519226" />
                  <Point X="-28.305955078125" Y="-0.108123092651" />
                  <Point X="-28.3009765625" Y="-0.088447944641" />
                  <Point X="-28.296716796875" Y="-0.060320617676" />
                  <Point X="-28.295646484375" Y="-0.046095470428" />
                  <Point X="-28.295646484375" Y="-0.016459917068" />
                  <Point X="-28.296720703125" Y="-0.002218276262" />
                  <Point X="-28.300990234375" Y="0.025940263748" />
                  <Point X="-28.304185546875" Y="0.039857017517" />
                  <Point X="-28.31165625" Y="0.063928871155" />
                  <Point X="-28.311658203125" Y="0.063939277649" />
                  <Point X="-28.319330078125" Y="0.084261528015" />
                  <Point X="-28.3334296875" Y="0.112244247437" />
                  <Point X="-28.341642578125" Y="0.125651069641" />
                  <Point X="-28.36088671875" Y="0.151911102295" />
                  <Point X="-28.37120703125" Y="0.163789047241" />
                  <Point X="-28.39366796875" Y="0.18568031311" />
                  <Point X="-28.40580859375" Y="0.195693328857" />
                  <Point X="-28.42822265625" Y="0.211249237061" />
                  <Point X="-28.44855078125" Y="0.22354838562" />
                  <Point X="-28.469458984375" Y="0.23447543335" />
                  <Point X="-28.47895703125" Y="0.238792922974" />
                  <Point X="-28.49837109375" Y="0.246360961914" />
                  <Point X="-28.508287109375" Y="0.249611343384" />
                  <Point X="-28.793783203125" Y="0.326109619141" />
                  <Point X="-29.7854453125" Y="0.591824768066" />
                  <Point X="-29.746095703125" Y="0.857750671387" />
                  <Point X="-29.73133203125" Y="0.957522094727" />
                  <Point X="-29.6335859375" Y="1.318237304688" />
                  <Point X="-28.778064453125" Y="1.20560559082" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.747056640625" Y="1.204364257812" />
                  <Point X="-28.736705078125" Y="1.204703125" />
                  <Point X="-28.715146484375" Y="1.206589111328" />
                  <Point X="-28.704892578125" Y="1.208053100586" />
                  <Point X="-28.684603515625" Y="1.212088989258" />
                  <Point X="-28.674568359375" Y="1.214660766602" />
                  <Point X="-28.624962890625" Y="1.230301757812" />
                  <Point X="-28.603451171875" Y="1.23767565918" />
                  <Point X="-28.584521484375" Y="1.246004516602" />
                  <Point X="-28.575283203125" Y="1.250685913086" />
                  <Point X="-28.556537109375" Y="1.261508178711" />
                  <Point X="-28.54786328125" Y="1.26716796875" />
                  <Point X="-28.531181640625" Y="1.27939855957" />
                  <Point X="-28.5159296875" Y="1.293373046875" />
                  <Point X="-28.502291015625" Y="1.308924194336" />
                  <Point X="-28.495896484375" Y="1.317071655273" />
                  <Point X="-28.48348046875" Y="1.334802368164" />
                  <Point X="-28.478009765625" Y="1.343600341797" />
                  <Point X="-28.46805859375" Y="1.361738769531" />
                  <Point X="-28.463578125" Y="1.371079345703" />
                  <Point X="-28.443673828125" Y="1.419134521484" />
                  <Point X="-28.435494140625" Y="1.440359985352" />
                  <Point X="-28.42970703125" Y="1.460219726562" />
                  <Point X="-28.427357421875" Y="1.470303344727" />
                  <Point X="-28.423599609375" Y="1.491618652344" />
                  <Point X="-28.422359375" Y="1.501896728516" />
                  <Point X="-28.421005859375" Y="1.522539306641" />
                  <Point X="-28.42191015625" Y="1.543213500977" />
                  <Point X="-28.42505859375" Y="1.563659423828" />
                  <Point X="-28.427189453125" Y="1.573795410156" />
                  <Point X="-28.43279296875" Y="1.594701904297" />
                  <Point X="-28.436017578125" Y="1.604546020508" />
                  <Point X="-28.443513671875" Y="1.623817626953" />
                  <Point X="-28.44778515625" Y="1.633245117188" />
                  <Point X="-28.471802734375" Y="1.679382324219" />
                  <Point X="-28.48280078125" Y="1.699286499023" />
                  <Point X="-28.49429296875" Y="1.716486328125" />
                  <Point X="-28.500509765625" Y="1.724774536133" />
                  <Point X="-28.514421875" Y="1.741353759766" />
                  <Point X="-28.52150390625" Y="1.748914672852" />
                  <Point X="-28.5364453125" Y="1.763217041016" />
                  <Point X="-28.5443046875" Y="1.769958251953" />
                  <Point X="-28.708064453125" Y="1.895616943359" />
                  <Point X="-29.22761328125" Y="2.294280761719" />
                  <Point X="-29.05966015625" Y="2.582026367188" />
                  <Point X="-29.00228515625" Y="2.680322021484" />
                  <Point X="-28.726337890625" Y="3.035013427734" />
                  <Point X="-28.25415625" Y="2.762398925781" />
                  <Point X="-28.24491796875" Y="2.757717285156" />
                  <Point X="-28.225984375" Y="2.749385986328" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.085984375" Y="2.725113525391" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310546875" />
                  <Point X="-27.976431640625" Y="2.734227294922" />
                  <Point X="-27.95698828125" Y="2.7413046875" />
                  <Point X="-27.9384375" Y="2.750454345703" />
                  <Point X="-27.929416015625" Y="2.755533691406" />
                  <Point X="-27.911162109375" Y="2.767163330078" />
                  <Point X="-27.902744140625" Y="2.773194580078" />
                  <Point X="-27.886615234375" Y="2.786139160156" />
                  <Point X="-27.878904296875" Y="2.793052490234" />
                  <Point X="-27.82986328125" Y="2.842092041016" />
                  <Point X="-27.811265625" Y="2.861488525391" />
                  <Point X="-27.798318359375" Y="2.877620117188" />
                  <Point X="-27.79228515625" Y="2.8860390625" />
                  <Point X="-27.78065625" Y="2.904292236328" />
                  <Point X="-27.775576171875" Y="2.91331640625" />
                  <Point X="-27.766427734375" Y="2.931865966797" />
                  <Point X="-27.759353515625" Y="2.951297607422" />
                  <Point X="-27.754435546875" Y="2.971387451172" />
                  <Point X="-27.7525234375" Y="2.981571044922" />
                  <Point X="-27.749697265625" Y="3.003028564453" />
                  <Point X="-27.748908203125" Y="3.013362304688" />
                  <Point X="-27.74845703125" Y="3.034048095703" />
                  <Point X="-27.748794921875" Y="3.044400146484" />
                  <Point X="-27.75483984375" Y="3.113488769531" />
                  <Point X="-27.757744140625" Y="3.140208740234" />
                  <Point X="-27.76178125" Y="3.160497802734" />
                  <Point X="-27.764353515625" Y="3.170527832031" />
                  <Point X="-27.77086328125" Y="3.191172119141" />
                  <Point X="-27.774509765625" Y="3.200864501953" />
                  <Point X="-27.78283984375" Y="3.219795410156" />
                  <Point X="-27.7875234375" Y="3.229033935547" />
                  <Point X="-27.860091796875" Y="3.354724609375" />
                  <Point X="-28.05938671875" Y="3.699914794922" />
                  <Point X="-27.74828125" Y="3.938436035156" />
                  <Point X="-27.648365234375" Y="4.015041503906" />
                  <Point X="-27.192525390625" Y="4.268296875" />
                  <Point X="-27.118564453125" Y="4.171909179688" />
                  <Point X="-27.111818359375" Y="4.164046386719" />
                  <Point X="-27.09751171875" Y="4.149102050781" />
                  <Point X="-27.089951171875" Y="4.142020507812" />
                  <Point X="-27.073369140625" Y="4.128107421875" />
                  <Point X="-27.065083984375" Y="4.121893554688" />
                  <Point X="-27.047888671875" Y="4.110404296875" />
                  <Point X="-27.038978515625" Y="4.10512890625" />
                  <Point X="-26.962083984375" Y="4.065099365234" />
                  <Point X="-26.94376171875" Y="4.055561767578" />
                  <Point X="-26.934326171875" Y="4.051287353516" />
                  <Point X="-26.915046875" Y="4.043789794922" />
                  <Point X="-26.905203125" Y="4.040566894531" />
                  <Point X="-26.884298828125" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834228516" />
                  <Point X="-26.853716796875" Y="4.029688232422" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031377929688" />
                  <Point X="-26.7808203125" Y="4.035135742188" />
                  <Point X="-26.77073046875" Y="4.037488037109" />
                  <Point X="-26.750869140625" Y="4.043277099609" />
                  <Point X="-26.74109765625" Y="4.046713867188" />
                  <Point X="-26.6610078125" Y="4.079889160156" />
                  <Point X="-26.641923828125" Y="4.087793457031" />
                  <Point X="-26.6325859375" Y="4.092271972656" />
                  <Point X="-26.614453125" Y="4.102219726562" />
                  <Point X="-26.605658203125" Y="4.107688964844" />
                  <Point X="-26.587927734375" Y="4.120103515625" />
                  <Point X="-26.57978125" Y="4.12649609375" />
                  <Point X="-26.564228515625" Y="4.140134277344" />
                  <Point X="-26.55025" Y="4.155389648437" />
                  <Point X="-26.53801953125" Y="4.172070800781" />
                  <Point X="-26.532361328125" Y="4.1807421875" />
                  <Point X="-26.5215390625" Y="4.199486328125" />
                  <Point X="-26.516857421875" Y="4.208723144531" />
                  <Point X="-26.508525390625" Y="4.227657226562" />
                  <Point X="-26.504875" Y="4.237354492188" />
                  <Point X="-26.478806640625" Y="4.320032714844" />
                  <Point X="-26.472595703125" Y="4.339730957031" />
                  <Point X="-26.470025390625" Y="4.349763671875" />
                  <Point X="-26.465990234375" Y="4.370049316406" />
                  <Point X="-26.464525390625" Y="4.380302246094" />
                  <Point X="-26.462638671875" Y="4.401864257812" />
                  <Point X="-26.46230078125" Y="4.412217285156" />
                  <Point X="-26.462751953125" Y="4.4328984375" />
                  <Point X="-26.463541015625" Y="4.4432265625" />
                  <Point X="-26.471791015625" Y="4.505892578125" />
                  <Point X="-26.479263671875" Y="4.562654785156" />
                  <Point X="-26.06051171875" Y="4.680058105469" />
                  <Point X="-25.931169921875" Y="4.716320800781" />
                  <Point X="-25.365220703125" Y="4.782557128906" />
                  <Point X="-25.225666015625" Y="4.2617265625" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218911621094" />
                  <Point X="-24.767251953125" Y="4.247104980469" />
                  <Point X="-24.76201953125" Y="4.2617265625" />
                  <Point X="-24.7248359375" Y="4.400495117188" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.28506640625" Y="4.749740234375" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.659302734375" Y="4.6141015625" />
                  <Point X="-23.546404296875" Y="4.586844238281" />
                  <Point X="-23.21386328125" Y="4.466229492188" />
                  <Point X="-23.141740234375" Y="4.440069824219" />
                  <Point X="-22.81897265625" Y="4.289122558594" />
                  <Point X="-22.749546875" Y="4.256654785156" />
                  <Point X="-22.437689453125" Y="4.074965576172" />
                  <Point X="-22.3705703125" Y="4.035861572266" />
                  <Point X="-22.18221875" Y="3.901915771484" />
                  <Point X="-22.238849609375" Y="3.803826904297" />
                  <Point X="-22.9346875" Y="2.598598144531" />
                  <Point X="-22.93762109375" Y="2.593112548828" />
                  <Point X="-22.946814453125" Y="2.573442626953" />
                  <Point X="-22.955814453125" Y="2.549568115234" />
                  <Point X="-22.958697265625" Y="2.5406015625" />
                  <Point X="-22.976037109375" Y="2.475763427734" />
                  <Point X="-22.980123046875" Y="2.456548095703" />
                  <Point X="-22.985591796875" Y="2.422196777344" />
                  <Point X="-22.986755859375" Y="2.40905859375" />
                  <Point X="-22.98725390625" Y="2.382747802734" />
                  <Point X="-22.986587890625" Y="2.369575195312" />
                  <Point X="-22.979826171875" Y="2.313508789062" />
                  <Point X="-22.97619921875" Y="2.289039794922" />
                  <Point X="-22.970859375" Y="2.267126708984" />
                  <Point X="-22.967537109375" Y="2.256330566406" />
                  <Point X="-22.959271484375" Y="2.234236816406" />
                  <Point X="-22.954685546875" Y="2.223899169922" />
                  <Point X="-22.944318359375" Y="2.203844482422" />
                  <Point X="-22.938537109375" Y="2.194127441406" />
                  <Point X="-22.90384375" Y="2.143000244141" />
                  <Point X="-22.89165625" Y="2.126941162109" />
                  <Point X="-22.87008984375" Y="2.101458984375" />
                  <Point X="-22.86108984375" Y="2.092186279297" />
                  <Point X="-22.841916015625" Y="2.074947265625" />
                  <Point X="-22.8317421875" Y="2.066980957031" />
                  <Point X="-22.780615234375" Y="2.03228894043" />
                  <Point X="-22.75871875" Y="2.018244262695" />
                  <Point X="-22.738671875" Y="2.007880737305" />
                  <Point X="-22.72833984375" Y="2.003296508789" />
                  <Point X="-22.706251953125" Y="1.99503112793" />
                  <Point X="-22.69544921875" Y="1.991706542969" />
                  <Point X="-22.67352734375" Y="1.986364501953" />
                  <Point X="-22.662408203125" Y="1.984346801758" />
                  <Point X="-22.606341796875" Y="1.97758605957" />
                  <Point X="-22.585205078125" Y="1.976226318359" />
                  <Point X="-22.5529453125" Y="1.975952880859" />
                  <Point X="-22.5401640625" Y="1.976707519531" />
                  <Point X="-22.51481640625" Y="1.979928955078" />
                  <Point X="-22.50225" Y="1.982395751953" />
                  <Point X="-22.437412109375" Y="1.999734375" />
                  <Point X="-22.426677734375" Y="2.002946777344" />
                  <Point X="-22.39914453125" Y="2.012079589844" />
                  <Point X="-22.3901484375" Y="2.015581054688" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-22.076810546875" Y="2.193661132813" />
                  <Point X="-21.059595703125" Y="2.780950195312" />
                  <Point X="-20.9958671875" Y="2.692381103516" />
                  <Point X="-20.956041015625" Y="2.637032958984" />
                  <Point X="-20.86311328125" Y="2.483470458984" />
                  <Point X="-20.910908203125" Y="2.446796142578" />
                  <Point X="-21.827046875" Y="1.743819580078" />
                  <Point X="-21.831861328125" Y="1.739867919922" />
                  <Point X="-21.847880859375" Y="1.725216186523" />
                  <Point X="-21.86533203125" Y="1.706602905273" />
                  <Point X="-21.87142578125" Y="1.699420410156" />
                  <Point X="-21.91808984375" Y="1.638543457031" />
                  <Point X="-21.928578125" Y="1.623392089844" />
                  <Point X="-21.9481640625" Y="1.591940795898" />
                  <Point X="-21.95446484375" Y="1.580010253906" />
                  <Point X="-21.9653125" Y="1.555378173828" />
                  <Point X="-21.969859375" Y="1.542676391602" />
                  <Point X="-21.9872421875" Y="1.480520874023" />
                  <Point X="-21.993775390625" Y="1.454663452148" />
                  <Point X="-21.997228515625" Y="1.432364868164" />
                  <Point X="-21.998291015625" Y="1.421114624023" />
                  <Point X="-21.999107421875" Y="1.397539306641" />
                  <Point X="-21.998826171875" Y="1.38623449707" />
                  <Point X="-21.996921875" Y="1.363748901367" />
                  <Point X="-21.995298828125" Y="1.352567871094" />
                  <Point X="-21.981029296875" Y="1.283411987305" />
                  <Point X="-21.976271484375" Y="1.265041137695" />
                  <Point X="-21.965673828125" Y="1.231190429688" />
                  <Point X="-21.960857421875" Y="1.218880615234" />
                  <Point X="-21.949560546875" Y="1.195049438477" />
                  <Point X="-21.943080078125" Y="1.183528076172" />
                  <Point X="-21.90426953125" Y="1.124537475586" />
                  <Point X="-21.888263671875" Y="1.101428833008" />
                  <Point X="-21.8737109375" Y="1.08418359375" />
                  <Point X="-21.86591796875" Y="1.07599206543" />
                  <Point X="-21.848671875" Y="1.059900390625" />
                  <Point X="-21.83996484375" Y="1.052695678711" />
                  <Point X="-21.821755859375" Y="1.039370239258" />
                  <Point X="-21.81225390625" Y="1.033249511719" />
                  <Point X="-21.75601171875" Y="1.001590026855" />
                  <Point X="-21.73809375" Y="0.992712036133" />
                  <Point X="-21.70671875" Y="0.97917175293" />
                  <Point X="-21.694361328125" Y="0.974822570801" />
                  <Point X="-21.669166015625" Y="0.967865539551" />
                  <Point X="-21.656328125" Y="0.965257629395" />
                  <Point X="-21.58028515625" Y="0.955207458496" />
                  <Point X="-21.569953125" Y="0.954128479004" />
                  <Point X="-21.5387578125" Y="0.95173046875" />
                  <Point X="-21.52890234375" Y="0.951485961914" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-21.2266171875" Y="0.98870916748" />
                  <Point X="-20.295296875" Y="1.111319824219" />
                  <Point X="-20.26441796875" Y="0.984479187012" />
                  <Point X="-20.247310546875" Y="0.914205505371" />
                  <Point X="-20.216126953125" Y="0.713921020508" />
                  <Point X="-20.25809375" Y="0.70267590332" />
                  <Point X="-21.3080078125" Y="0.42135269165" />
                  <Point X="-21.31396875" Y="0.419544250488" />
                  <Point X="-21.33437109375" Y="0.412138519287" />
                  <Point X="-21.3576171875" Y="0.401619873047" />
                  <Point X="-21.365994140625" Y="0.397316497803" />
                  <Point X="-21.440703125" Y="0.354132781982" />
                  <Point X="-21.45850390625" Y="0.343844055176" />
                  <Point X="-21.4661171875" Y="0.338946044922" />
                  <Point X="-21.4912265625" Y="0.319869262695" />
                  <Point X="-21.518005859375" Y="0.294350402832" />
                  <Point X="-21.527203125" Y="0.284226501465" />
                  <Point X="-21.572029296875" Y="0.22710760498" />
                  <Point X="-21.589146484375" Y="0.20420614624" />
                  <Point X="-21.600869140625" Y="0.184927536011" />
                  <Point X="-21.606154296875" Y="0.174941558838" />
                  <Point X="-21.615927734375" Y="0.153479675293" />
                  <Point X="-21.6199921875" Y="0.142934127808" />
                  <Point X="-21.62683984375" Y="0.121430778503" />
                  <Point X="-21.629623046875" Y="0.110472961426" />
                  <Point X="-21.644564453125" Y="0.032451583862" />
                  <Point X="-21.647099609375" Y="0.01472161293" />
                  <Point X="-21.65066015625" Y="-0.022679637909" />
                  <Point X="-21.650982421875" Y="-0.036159137726" />
                  <Point X="-21.64971484375" Y="-0.063030605316" />
                  <Point X="-21.648125" Y="-0.076422424316" />
                  <Point X="-21.63318359375" Y="-0.15444380188" />
                  <Point X="-21.62683984375" Y="-0.183990478516" />
                  <Point X="-21.6199921875" Y="-0.205494125366" />
                  <Point X="-21.615927734375" Y="-0.216039810181" />
                  <Point X="-21.606154296875" Y="-0.237501708984" />
                  <Point X="-21.600869140625" Y="-0.247487670898" />
                  <Point X="-21.589146484375" Y="-0.266766296387" />
                  <Point X="-21.582708984375" Y="-0.276058929443" />
                  <Point X="-21.5378828125" Y="-0.333177703857" />
                  <Point X="-21.52546875" Y="-0.347466461182" />
                  <Point X="-21.5004765625" Y="-0.373496856689" />
                  <Point X="-21.4905625" Y="-0.382464324951" />
                  <Point X="-21.469576171875" Y="-0.398917755127" />
                  <Point X="-21.45850390625" Y="-0.406404052734" />
                  <Point X="-21.383794921875" Y="-0.449587768555" />
                  <Point X="-21.365994140625" Y="-0.459876342773" />
                  <Point X="-21.360505859375" Y="-0.462813720703" />
                  <Point X="-21.34083984375" Y="-0.472017333984" />
                  <Point X="-21.316970703125" Y="-0.481027893066" />
                  <Point X="-21.3080078125" Y="-0.483912841797" />
                  <Point X="-21.057857421875" Y="-0.550940124512" />
                  <Point X="-20.21512109375" Y="-0.776750976562" />
                  <Point X="-20.2288359375" Y="-0.867718261719" />
                  <Point X="-20.238380859375" Y="-0.93103894043" />
                  <Point X="-20.2721953125" Y="-1.079219848633" />
                  <Point X="-20.34561328125" Y="-1.069554443359" />
                  <Point X="-21.563216796875" Y="-0.909253601074" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042297363" />
                  <Point X="-21.63327734375" Y="-0.910840881348" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.792146484375" Y="-0.944546936035" />
                  <Point X="-21.82708203125" Y="-0.952140075684" />
                  <Point X="-21.842123046875" Y="-0.9567421875" />
                  <Point X="-21.871244140625" Y="-0.968366577148" />
                  <Point X="-21.88532421875" Y="-0.975389282227" />
                  <Point X="-21.91315234375" Y="-0.992282653809" />
                  <Point X="-21.92587890625" Y="-1.001533081055" />
                  <Point X="-21.949626953125" Y="-1.022003234863" />
                  <Point X="-21.9606484375" Y="-1.033223022461" />
                  <Point X="-22.04927734375" Y="-1.139814819336" />
                  <Point X="-22.070392578125" Y="-1.16521105957" />
                  <Point X="-22.078671875" Y="-1.176845947266" />
                  <Point X="-22.093396484375" Y="-1.201233520508" />
                  <Point X="-22.099841796875" Y="-1.213986328125" />
                  <Point X="-22.11118359375" Y="-1.241372070312" />
                  <Point X="-22.115640625" Y="-1.254941650391" />
                  <Point X="-22.12246875" Y="-1.2825859375" />
                  <Point X="-22.12483984375" Y="-1.296660766602" />
                  <Point X="-22.13754296875" Y="-1.434702026367" />
                  <Point X="-22.140568359375" Y="-1.467591430664" />
                  <Point X="-22.140708984375" Y="-1.483321777344" />
                  <Point X="-22.138390625" Y="-1.514588623047" />
                  <Point X="-22.135931640625" Y="-1.53012512207" />
                  <Point X="-22.128201171875" Y="-1.561744873047" />
                  <Point X="-22.12321484375" Y="-1.576663696289" />
                  <Point X="-22.11084375" Y="-1.605476928711" />
                  <Point X="-22.103458984375" Y="-1.619371582031" />
                  <Point X="-22.0223125" Y="-1.74558972168" />
                  <Point X="-22.002978515625" Y="-1.775662231445" />
                  <Point X="-21.998255859375" Y="-1.782356201172" />
                  <Point X="-21.98019921875" Y="-1.804456542969" />
                  <Point X="-21.956505859375" Y="-1.828122802734" />
                  <Point X="-21.947201171875" Y="-1.836277587891" />
                  <Point X="-21.7150625" Y="-2.014405151367" />
                  <Point X="-20.912828125" Y="-2.629980957031" />
                  <Point X="-20.92759375" Y="-2.653874023438" />
                  <Point X="-20.95451171875" Y="-2.697427978516" />
                  <Point X="-20.9987265625" Y="-2.760251464844" />
                  <Point X="-21.067103515625" Y="-2.720773193359" />
                  <Point X="-22.151544921875" Y="-2.094670898438" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.18497265625" Y="-2.079513427734" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228892578125" Y="-2.066337646484" />
                  <Point X="-22.403404296875" Y="-2.034820922852" />
                  <Point X="-22.444982421875" Y="-2.027311889648" />
                  <Point X="-22.460640625" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403442383" />
                  <Point X="-22.507685546875" Y="-2.02650378418" />
                  <Point X="-22.539859375" Y="-2.031461914062" />
                  <Point X="-22.555154296875" Y="-2.03513659668" />
                  <Point X="-22.5849296875" Y="-2.044960083008" />
                  <Point X="-22.59941015625" Y="-2.051108886719" />
                  <Point X="-22.74438671875" Y="-2.127408935547" />
                  <Point X="-22.778927734375" Y="-2.145587890625" />
                  <Point X="-22.791029296875" Y="-2.153169433594" />
                  <Point X="-22.8139609375" Y="-2.1700625" />
                  <Point X="-22.824791015625" Y="-2.179374023438" />
                  <Point X="-22.84575" Y="-2.200333007812" />
                  <Point X="-22.855060546875" Y="-2.211161376953" />
                  <Point X="-22.871953125" Y="-2.234091552734" />
                  <Point X="-22.87953515625" Y="-2.246193359375" />
                  <Point X="-22.9558359375" Y="-2.391169677734" />
                  <Point X="-22.974015625" Y="-2.425711425781" />
                  <Point X="-22.9801640625" Y="-2.440193359375" />
                  <Point X="-22.98998828125" Y="-2.469972412109" />
                  <Point X="-22.9936640625" Y="-2.48526953125" />
                  <Point X="-22.99862109375" Y="-2.517443603516" />
                  <Point X="-22.999720703125" Y="-2.533134521484" />
                  <Point X="-22.99931640625" Y="-2.564484619141" />
                  <Point X="-22.9978125" Y="-2.580143798828" />
                  <Point X="-22.966294921875" Y="-2.754655517578" />
                  <Point X="-22.95878515625" Y="-2.796234130859" />
                  <Point X="-22.95698046875" Y="-2.804228759766" />
                  <Point X="-22.94876171875" Y="-2.831539306641" />
                  <Point X="-22.9359296875" Y="-2.862475830078" />
                  <Point X="-22.930453125" Y="-2.873577636719" />
                  <Point X="-22.78128125" Y="-3.131953613281" />
                  <Point X="-22.26410546875" Y="-4.027722900391" />
                  <Point X="-22.276244140625" Y="-4.036083007813" />
                  <Point X="-22.3385546875" Y="-3.954877929688" />
                  <Point X="-23.16608203125" Y="-2.876422363281" />
                  <Point X="-23.171345703125" Y="-2.870144042969" />
                  <Point X="-23.191169921875" Y="-2.849624023438" />
                  <Point X="-23.216748046875" Y="-2.828003662109" />
                  <Point X="-23.22669921875" Y="-2.820646728516" />
                  <Point X="-23.39881640625" Y="-2.7099921875" />
                  <Point X="-23.43982421875" Y="-2.683627929688" />
                  <Point X="-23.453716796875" Y="-2.676245605469" />
                  <Point X="-23.48252734375" Y="-2.663874511719" />
                  <Point X="-23.4974453125" Y="-2.658885742188" />
                  <Point X="-23.52906640625" Y="-2.651154052734" />
                  <Point X="-23.54460546875" Y="-2.648695800781" />
                  <Point X="-23.575875" Y="-2.646376953125" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.779841796875" Y="-2.663838134766" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670339355469" />
                  <Point X="-23.866423828125" Y="-2.677171386719" />
                  <Point X="-23.8799921875" Y="-2.681629394531" />
                  <Point X="-23.907376953125" Y="-2.69297265625" />
                  <Point X="-23.920123046875" Y="-2.6994140625" />
                  <Point X="-23.94450390625" Y="-2.714134033203" />
                  <Point X="-23.956138671875" Y="-2.722412597656" />
                  <Point X="-24.1014921875" Y="-2.843268310547" />
                  <Point X="-24.136123046875" Y="-2.872062988281" />
                  <Point X="-24.14734375" Y="-2.883086181641" />
                  <Point X="-24.167814453125" Y="-2.906834960938" />
                  <Point X="-24.177064453125" Y="-2.919560546875" />
                  <Point X="-24.19395703125" Y="-2.947386230469" />
                  <Point X="-24.20098046875" Y="-2.961466064453" />
                  <Point X="-24.21260546875" Y="-2.990588378906" />
                  <Point X="-24.21720703125" Y="-3.005630859375" />
                  <Point X="-24.260666015625" Y="-3.205579345703" />
                  <Point X="-24.271021484375" Y="-3.25321875" />
                  <Point X="-24.2724140625" Y="-3.261286621094" />
                  <Point X="-24.275275390625" Y="-3.289676757812" />
                  <Point X="-24.2752578125" Y="-3.323170166016" />
                  <Point X="-24.2744453125" Y="-3.335520507812" />
                  <Point X="-24.232169921875" Y="-3.656627929688" />
                  <Point X="-24.166912109375" Y="-4.152318359375" />
                  <Point X="-24.344931640625" Y="-3.487937988281" />
                  <Point X="-24.347392578125" Y="-3.480122070312" />
                  <Point X="-24.357853515625" Y="-3.453579833984" />
                  <Point X="-24.3732109375" Y="-3.423815917969" />
                  <Point X="-24.37958984375" Y="-3.413209472656" />
                  <Point X="-24.511814453125" Y="-3.222699462891" />
                  <Point X="-24.543318359375" Y="-3.177309082031" />
                  <Point X="-24.553328125" Y="-3.165172119141" />
                  <Point X="-24.575212890625" Y="-3.142716064453" />
                  <Point X="-24.587087890625" Y="-3.132396972656" />
                  <Point X="-24.61334375" Y="-3.113153320312" />
                  <Point X="-24.6267578125" Y="-3.104936523438" />
                  <Point X="-24.6547578125" Y="-3.090829101562" />
                  <Point X="-24.66934375" Y="-3.084938476562" />
                  <Point X="-24.873953125" Y="-3.021435058594" />
                  <Point X="-24.922703125" Y="-3.006305175781" />
                  <Point X="-24.936623046875" Y="-3.003109130859" />
                  <Point X="-24.964783203125" Y="-2.99883984375" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.26959375" Y="-3.069808349609" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.3329296875" Y="-3.090829101562" />
                  <Point X="-25.3609296875" Y="-3.104936523438" />
                  <Point X="-25.37434375" Y="-3.113153320312" />
                  <Point X="-25.400599609375" Y="-3.132396972656" />
                  <Point X="-25.412474609375" Y="-3.142717285156" />
                  <Point X="-25.434359375" Y="-3.165174072266" />
                  <Point X="-25.444369140625" Y="-3.177310546875" />
                  <Point X="-25.576591796875" Y="-3.367820556641" />
                  <Point X="-25.608095703125" Y="-3.4132109375" />
                  <Point X="-25.612470703125" Y="-3.420133056641" />
                  <Point X="-25.625974609375" Y="-3.445260742188" />
                  <Point X="-25.63877734375" Y="-3.476212646484" />
                  <Point X="-25.64275390625" Y="-3.487936035156" />
                  <Point X="-25.71983203125" Y="-3.775592041016" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.944314929352" Y="2.617655674738" />
                  <Point X="-21.096548314169" Y="2.759615602694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.230967761741" Y="3.817478851562" />
                  <Point X="-22.622637941656" Y="4.182717203124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.905136402687" Y="2.451224998766" />
                  <Point X="-21.182579909885" Y="2.709945254779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.279717088754" Y="3.733042225637" />
                  <Point X="-22.933453521198" Y="4.342661311249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.981553040508" Y="2.392588557562" />
                  <Point X="-21.268611505601" Y="2.660274906864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.328466476573" Y="3.648605656414" />
                  <Point X="-23.199784737686" Y="4.461123079729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.057969682179" Y="2.333952119948" />
                  <Point X="-21.354643101316" Y="2.610604558949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.377215864392" Y="3.56416908719" />
                  <Point X="-23.427749212569" Y="4.543807282857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.13438632385" Y="2.275315682333" />
                  <Point X="-21.440674697032" Y="2.560934211034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.42596525221" Y="3.479732517967" />
                  <Point X="-23.636531565495" Y="4.608603867875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.210802965521" Y="2.216679244719" />
                  <Point X="-21.526706292748" Y="2.511263863119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.474714640029" Y="3.395295948744" />
                  <Point X="-23.82449166443" Y="4.65398338692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.287219607191" Y="2.158042807105" />
                  <Point X="-21.612737888464" Y="2.461593515204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.523464027848" Y="3.31085937952" />
                  <Point X="-24.012451741171" Y="4.699362885267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.29272817682" Y="1.100768436445" />
                  <Point X="-20.302961116986" Y="1.110310807524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.363636248862" Y="2.09940636949" />
                  <Point X="-21.698769484179" Y="2.411923167289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.572213415666" Y="3.226422810297" />
                  <Point X="-24.195739687317" Y="4.740385551352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.251818566329" Y="0.932723498689" />
                  <Point X="-20.425024699149" Y="1.094240830555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.440052890533" Y="2.040769931876" />
                  <Point X="-21.784801079895" Y="2.362252819375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.620962803485" Y="3.141986241073" />
                  <Point X="-24.352658998783" Y="4.756819067796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.226258283316" Y="0.778992040369" />
                  <Point X="-20.547088281312" Y="1.078170853586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.516469532204" Y="1.982133494261" />
                  <Point X="-21.870832675611" Y="2.31258247146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.669712191304" Y="3.05754967185" />
                  <Point X="-24.509578697821" Y="4.773252945656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.277996821723" Y="0.697342899164" />
                  <Point X="-20.669151863475" Y="1.062100876617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.592886173875" Y="1.923497056647" />
                  <Point X="-21.956864271327" Y="2.262912123545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.718461579123" Y="2.973113102626" />
                  <Point X="-24.629737755996" Y="4.755406971336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.386201750215" Y="0.668349518573" />
                  <Point X="-20.791215445638" Y="1.046030899647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.669302815545" Y="1.864860619033" />
                  <Point X="-22.042895867042" Y="2.21324177563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.767210966941" Y="2.888676533403" />
                  <Point X="-24.657585134168" Y="4.651478962787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.494406678707" Y="0.639356137982" />
                  <Point X="-20.913279027801" Y="1.029960922678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.745719457216" Y="1.806224181418" />
                  <Point X="-22.128927460298" Y="2.163571425421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.81596035476" Y="2.804239964179" />
                  <Point X="-24.685432512341" Y="4.547550954238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.602611607199" Y="0.610362757391" />
                  <Point X="-21.035342609965" Y="1.013890945709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.822136098887" Y="1.747587743804" />
                  <Point X="-22.214959051954" Y="2.11390107372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.864709742579" Y="2.719803394956" />
                  <Point X="-24.713279890513" Y="4.443622945689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.710816535691" Y="0.5813693768" />
                  <Point X="-21.157406192128" Y="0.99782096874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.887413219582" Y="1.678563534827" />
                  <Point X="-22.300990643609" Y="2.064230722019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.913459130397" Y="2.635366825732" />
                  <Point X="-24.741127526478" Y="4.339695177535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.819021464184" Y="0.552375996209" />
                  <Point X="-21.279469755301" Y="0.981750974062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.942893773123" Y="1.600403879188" />
                  <Point X="-22.388797072745" Y="2.016215433044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.956952364487" Y="2.546028813862" />
                  <Point X="-24.771375237008" Y="4.23800551512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.927226392676" Y="0.523382615618" />
                  <Point X="-21.401533293606" Y="0.965680956195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.980289869735" Y="1.505380194636" />
                  <Point X="-22.494149412462" Y="1.984561970385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.982730288774" Y="2.440171008344" />
                  <Point X="-24.823874068213" Y="4.15706535842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.322114911257" Y="4.621682461088" />
                  <Point X="-25.480201183308" Y="4.769100294687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.035431321168" Y="0.494389235027" />
                  <Point X="-21.525757417857" Y="0.951625717317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.998992615502" Y="1.392924678411" />
                  <Point X="-22.628879570845" Y="1.98030376683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.978804137676" Y="2.306613704411" />
                  <Point X="-24.905420328793" Y="4.103212367824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.275716389405" Y="4.448519030682" />
                  <Point X="-25.603964698398" Y="4.754615530818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.14363624966" Y="0.465395854436" />
                  <Point X="-21.68806519172" Y="0.973084056237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.966013344238" Y="1.232274901623" />
                  <Point X="-25.02874776567" Y="4.088320954442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.229317867552" Y="4.275355600276" />
                  <Point X="-25.727728213489" Y="4.740130766949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.251841178152" Y="0.436402473845" />
                  <Point X="-25.85149172858" Y="4.72564600308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.355072637904" Y="0.402771258623" />
                  <Point X="-25.969318667098" Y="4.705625291997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.216891800501" Y="-0.788495651812" />
                  <Point X="-20.235278453783" Y="-0.771349820243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.441633878787" Y="0.353594812817" />
                  <Point X="-26.076416035077" Y="4.675599094519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.234061010443" Y="-0.902381213329" />
                  <Point X="-20.430738659286" Y="-0.718976338676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.51770549289" Y="0.29463663179" />
                  <Point X="-26.183513289335" Y="4.645572790994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.256671676893" Y="-1.011192534561" />
                  <Point X="-20.626198864789" Y="-0.66660285711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.577186592893" Y="0.220207546079" />
                  <Point X="-26.290610543593" Y="4.615546487469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.331372213184" Y="-1.071429266332" />
                  <Point X="-20.821659070292" Y="-0.614229375543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.623111893399" Y="0.133137472832" />
                  <Point X="-26.397707797851" Y="4.585520183944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.493567448311" Y="-1.05007587148" />
                  <Point X="-21.017119275795" Y="-0.561855893976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.645725846486" Y="0.024329216439" />
                  <Point X="-26.474614650616" Y="4.527340875571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.655762749238" Y="-1.028722415269" />
                  <Point X="-21.212579098376" Y="-0.50948276949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.641851908334" Y="-0.109179398135" />
                  <Point X="-26.463899693568" Y="4.387452907672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.817958050166" Y="-1.007368959058" />
                  <Point X="-21.475013421666" Y="-0.394654912707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.52420211265" Y="-0.348785716297" />
                  <Point X="-26.490642333913" Y="4.282494714432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.980153351093" Y="-0.986015502847" />
                  <Point X="-26.528323062997" Y="4.187736453956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.142348652021" Y="-0.964662046636" />
                  <Point X="-26.592019685207" Y="4.117238406298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.304543952948" Y="-0.943308590425" />
                  <Point X="-26.681957937346" Y="4.071211074434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.466739253875" Y="-0.921955134214" />
                  <Point X="-26.782290454062" Y="4.034876551097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.619218535809" Y="-0.909662012292" />
                  <Point X="-26.943778552538" Y="4.055570530353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.734534187672" Y="-0.932024536066" />
                  <Point X="-27.266890460727" Y="4.226981150446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.84576520505" Y="-0.958196043119" />
                  <Point X="-27.354180671395" Y="4.178484479962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.932483487209" Y="-1.007226045566" />
                  <Point X="-27.441470882064" Y="4.129987809479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.997008141617" Y="-1.076951940707" />
                  <Point X="-27.528761092733" Y="4.081491138995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.057843706092" Y="-1.150117967864" />
                  <Point X="-27.616051303401" Y="4.032994468511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.107867815766" Y="-1.233365839728" />
                  <Point X="-27.696510983791" Y="3.978128225496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.129143048483" Y="-1.343422473062" />
                  <Point X="-27.772955953093" Y="3.919518203825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.140150935352" Y="-1.463053561295" />
                  <Point X="-27.849401204969" Y="3.860908445659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.95499958979" Y="-2.698121179192" />
                  <Point X="-21.563392679224" Y="-2.130785444993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.090947140043" Y="-1.63883295152" />
                  <Point X="-27.925846456846" Y="3.802298687493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.074721080049" Y="-2.716375192195" />
                  <Point X="-28.002291708722" Y="3.743688929327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.440455580449" Y="-2.505218361856" />
                  <Point X="-28.018263156255" Y="3.628686436295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.806190080848" Y="-2.294061531517" />
                  <Point X="-27.855799875829" Y="3.347290867549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.167346219338" Y="-2.087174092728" />
                  <Point X="-27.756020258071" Y="3.124348759897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.353028378444" Y="-2.04391878694" />
                  <Point X="-27.751394894714" Y="2.990139429983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.510530120102" Y="-2.026942145554" />
                  <Point X="-27.78735538428" Y="2.893777020204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.615071848986" Y="-2.059351515044" />
                  <Point X="-27.849823006729" Y="2.822132911729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.704114521687" Y="-2.106213988244" />
                  <Point X="-27.922455860512" Y="2.759968034826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.792162378924" Y="-2.154004141873" />
                  <Point X="-28.024338796883" Y="2.725079301207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.861211567075" Y="-2.219510841041" />
                  <Point X="-28.173457896162" Y="2.734239002112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.909940869258" Y="-2.303966140423" />
                  <Point X="-28.487298009981" Y="2.897003534079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.955798772767" Y="-2.391099062387" />
                  <Point X="-28.746626553603" Y="3.008935204468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.993720061418" Y="-2.485632997438" />
                  <Point X="-28.805194389966" Y="2.933654486635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.990956295877" Y="-2.618106359304" />
                  <Point X="-28.863762226328" Y="2.858373768801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.962745023708" Y="-2.774309905005" />
                  <Point X="-28.922330062691" Y="2.783093050967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.854484437115" Y="-3.005160644041" />
                  <Point X="-28.980897899054" Y="2.707812333133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.692020503646" Y="-3.286556821759" />
                  <Point X="-29.033453070241" Y="2.626924714316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.529553943333" Y="-3.567955449048" />
                  <Point X="-22.901830550032" Y="-3.220801897085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.506887696321" Y="-2.656576980195" />
                  <Point X="-29.082549289771" Y="2.542811570895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.367087383019" Y="-3.849354076338" />
                  <Point X="-22.5514337107" Y="-3.677448344702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.651101767191" Y="-2.65199129228" />
                  <Point X="-29.131645157959" Y="2.458698099843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.777887007068" Y="-2.663658252199" />
                  <Point X="-29.180741026147" Y="2.374584628791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.892400248184" Y="-2.6867690361" />
                  <Point X="-28.421629886755" Y="1.536805930449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.975875625739" Y="-2.738823096014" />
                  <Point X="-28.441311869034" Y="1.425263567044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.04951389303" Y="-2.800050409652" />
                  <Point X="-28.48352793808" Y="1.334734579503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.123151988489" Y="-2.861277883527" />
                  <Point X="-28.549336958115" Y="1.266206374685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.185336353722" Y="-2.933186133629" />
                  <Point X="-28.643770983686" Y="1.224371419371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.222218713993" Y="-3.028688885069" />
                  <Point X="-28.761961158855" Y="1.204689431945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.245693758381" Y="-3.136694160834" />
                  <Point X="-28.922653217603" Y="1.224641092145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.269169486313" Y="-3.244698799183" />
                  <Point X="-29.084848510022" Y="1.245994540422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.269319375079" Y="-3.374455134452" />
                  <Point X="-29.247043802441" Y="1.267347988699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.249824517047" Y="-3.522530492473" />
                  <Point X="-24.402582788943" Y="-3.380081099398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.743898011248" Y="-3.06179950547" />
                  <Point X="-29.40923909486" Y="1.288701436976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.230329732957" Y="-3.670605781543" />
                  <Point X="-24.317855931456" Y="-3.58898628101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.947978396535" Y="-3.00138757621" />
                  <Point X="-29.571434387279" Y="1.310054885252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.210835658219" Y="-3.81868040913" />
                  <Point X="-24.271456997175" Y="-3.762150096012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.07775205485" Y="-3.010267790852" />
                  <Point X="-28.296208524022" Y="-0.009008579272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.59985573752" Y="0.274147028179" />
                  <Point X="-29.650917535678" Y="1.254278011424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.191341583481" Y="-3.966755036718" />
                  <Point X="-24.225058062894" Y="-3.935313911015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.182264406566" Y="-3.042704554994" />
                  <Point X="-28.311176880149" Y="-0.124946470173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.795315651025" Y="0.326520237453" />
                  <Point X="-29.679016281014" Y="1.150584406547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.171847508744" Y="-4.114829664306" />
                  <Point X="-24.178659128612" Y="-4.108477726017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.286776777553" Y="-3.075141301165" />
                  <Point X="-28.358282466937" Y="-0.210915908657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.990775766105" Y="0.378893634698" />
                  <Point X="-29.70711502635" Y="1.04689080167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.380484198384" Y="-3.117653826361" />
                  <Point X="-28.429318309661" Y="-0.274570022464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.186235881184" Y="0.431267031944" />
                  <Point X="-29.733665413575" Y="0.941753329496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.448863104934" Y="-3.183785573234" />
                  <Point X="-28.523827103136" Y="-0.316335255581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.381695996263" Y="0.483640429189" />
                  <Point X="-29.750555986587" Y="0.827607934838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.503594403217" Y="-3.262643920706" />
                  <Point X="-28.632031989642" Y="-0.345328675324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.577156111342" Y="0.536013826434" />
                  <Point X="-29.767446347471" Y="0.713462342369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.558325701499" Y="-3.341502268178" />
                  <Point X="-28.740236876147" Y="-0.374322095068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.772616226422" Y="0.588387223679" />
                  <Point X="-29.784336708355" Y="0.599316749899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.612747899692" Y="-3.420648856146" />
                  <Point X="-28.848441762653" Y="-0.403315514812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.65017757895" Y="-3.515641224374" />
                  <Point X="-27.949071813294" Y="-1.371887669413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.240616251828" Y="-1.1000180822" />
                  <Point X="-28.956646649159" Y="-0.432308934556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.678025264341" Y="-3.619568946436" />
                  <Point X="-27.975420331458" Y="-1.477213387532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.36297133388" Y="-1.115816231126" />
                  <Point X="-29.064851535665" Y="-0.4613023543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.705872949733" Y="-3.723496668499" />
                  <Point X="-28.029926786819" Y="-1.55628140442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.485034895082" Y="-1.131886227642" />
                  <Point X="-29.173056422171" Y="-0.490295774044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.733720529526" Y="-3.827424489034" />
                  <Point X="-28.10420302166" Y="-1.616913803694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.607098464168" Y="-1.147956216805" />
                  <Point X="-29.281261308676" Y="-0.519289193788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.761568003185" Y="-3.931352408541" />
                  <Point X="-28.180619559885" Y="-1.675550337773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.729162044784" Y="-1.164026195217" />
                  <Point X="-29.389466195182" Y="-0.548282613532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.789415476844" Y="-4.035280328047" />
                  <Point X="-27.311528735421" Y="-2.615886751613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.595908411498" Y="-2.350698413482" />
                  <Point X="-28.257036098111" Y="-1.734186871852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.8512256254" Y="-1.180096173629" />
                  <Point X="-29.497671081688" Y="-0.577276033275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.817262950503" Y="-4.139208247554" />
                  <Point X="-27.339429968954" Y="-2.719764539227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.697955699959" Y="-2.385433886296" />
                  <Point X="-28.333452636336" Y="-1.79282340593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.973289206016" Y="-1.196166152042" />
                  <Point X="-29.605875968194" Y="-0.606269453019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.845110424162" Y="-4.24313616706" />
                  <Point X="-27.387884093" Y="-2.804476446372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.783987282284" Y="-2.435104246698" />
                  <Point X="-28.409869174562" Y="-1.851459940009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.095352786632" Y="-1.212236130454" />
                  <Point X="-29.714080847247" Y="-0.635262879713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.872957897821" Y="-4.347064086567" />
                  <Point X="-27.436633517209" Y="-2.888912981661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.87001886461" Y="-2.4847746071" />
                  <Point X="-28.486285712787" Y="-1.910096474088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.217416367247" Y="-1.228306108866" />
                  <Point X="-29.779351498731" Y="-0.704293121327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.90080537148" Y="-4.450992006073" />
                  <Point X="-26.186339344916" Y="-4.18472726824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.593611566793" Y="-3.804939777174" />
                  <Point X="-27.485382941418" Y="-2.97334951695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.956050451791" Y="-2.534444962973" />
                  <Point X="-28.562702251012" Y="-1.968733008166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.339479947863" Y="-1.244376087278" />
                  <Point X="-29.757913511958" Y="-0.854180476213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.928652845139" Y="-4.55491992558" />
                  <Point X="-26.153693674633" Y="-4.34506595708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.750288631629" Y="-3.788732159367" />
                  <Point X="-27.534132365627" Y="-3.057786052239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.042082053757" Y="-2.584115305059" />
                  <Point X="-28.639118789238" Y="-2.027369542245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.461543528479" Y="-1.26044606569" />
                  <Point X="-29.732056668037" Y="-1.008188482054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.956500318798" Y="-4.658847845086" />
                  <Point X="-26.122468179425" Y="-4.504080311238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.900116037503" Y="-3.778911951877" />
                  <Point X="-27.582881789836" Y="-3.142222587528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.128113655723" Y="-2.633785647146" />
                  <Point X="-28.715535327463" Y="-2.086006076324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.583607109095" Y="-1.276516044102" />
                  <Point X="-29.68850383069" Y="-1.178698268728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.984347792457" Y="-4.762775764593" />
                  <Point X="-26.127776712687" Y="-4.62902613269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.029875142589" Y="-3.787805737625" />
                  <Point X="-27.631631214045" Y="-3.226659122817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.214145257689" Y="-2.683455989232" />
                  <Point X="-28.791951865689" Y="-2.144642610403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.120896127266" Y="-3.832823405063" />
                  <Point X="-27.680380638254" Y="-3.311095658106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.300176859655" Y="-2.733126331319" />
                  <Point X="-28.868368403914" Y="-2.203279144481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.202046022106" Y="-3.887046012691" />
                  <Point X="-27.729130062463" Y="-3.395532193395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.386208461621" Y="-2.782796673406" />
                  <Point X="-28.94478494214" Y="-2.26191567856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.283195916946" Y="-3.941268620318" />
                  <Point X="-27.777879486672" Y="-3.479968728684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.472240063587" Y="-2.832467015492" />
                  <Point X="-29.021201480365" Y="-2.320552212639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.363684360552" Y="-3.9961080412" />
                  <Point X="-27.826628910881" Y="-3.564405263973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.558271665553" Y="-2.882137357579" />
                  <Point X="-29.097618018591" Y="-2.379188746718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.428693789599" Y="-4.065381876678" />
                  <Point X="-27.87537833509" Y="-3.648841799262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.644303267519" Y="-2.931807699665" />
                  <Point X="-29.174034611016" Y="-2.437825230254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.486793616469" Y="-4.141099020423" />
                  <Point X="-27.924127759299" Y="-3.733278334551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.730334869485" Y="-2.981478041752" />
                  <Point X="-29.023264652767" Y="-2.708316599662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.849951937068" Y="-3.932344516612" />
                  <Point X="-27.972877136832" Y="-3.817714913366" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.0001640625" Y="0.001626220703" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.204955078125" Y="-4.744445800781" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521543945312" />
                  <Point X="-24.667904296875" Y="-3.331033935547" />
                  <Point X="-24.699408203125" Y="-3.285643554688" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.9302734375" Y="-3.202896484375" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.2132734375" Y="-3.251269775391" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643554688" />
                  <Point X="-25.420501953125" Y="-3.476153564453" />
                  <Point X="-25.452005859375" Y="-3.521543945312" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.5363046875" Y="-3.824768310547" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-26.044044921875" Y="-4.948974121094" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.337587890625" Y="-4.876999511719" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.348091796875" Y="-4.846833984375" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.358564453125" Y="-4.289015625" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.202628417969" />
                  <Point X="-26.57466015625" Y="-4.037424804688" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.899259765625" Y="-3.969375732422" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791503906" />
                  <Point X="-27.198208984375" Y="-4.112993164062" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.303005859375" Y="-4.213688476562" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.773458984375" Y="-4.218615234375" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.184482421875" Y="-3.914562744141" />
                  <Point X="-28.228580078125" Y="-3.880608398438" />
                  <Point X="-28.107228515625" Y="-3.670421142578" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597594238281" />
                  <Point X="-27.5139765625" Y="-2.568765625" />
                  <Point X="-27.53132421875" Y="-2.551416992188" />
                  <Point X="-27.560154296875" Y="-2.537198974609" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-27.839779296875" Y="-2.686708984375" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-29.096623046875" Y="-2.932631835938" />
                  <Point X="-29.161697265625" Y="-2.847136962891" />
                  <Point X="-29.397318359375" Y="-2.452037841797" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-29.21568359375" Y="-2.230294189453" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.396013305664" />
                  <Point X="-28.139599609375" Y="-1.371989746094" />
                  <Point X="-28.1381171875" Y="-1.366266113281" />
                  <Point X="-28.140326171875" Y="-1.334595581055" />
                  <Point X="-28.161158203125" Y="-1.310639282227" />
                  <Point X="-28.182544921875" Y="-1.298052001953" />
                  <Point X="-28.187640625" Y="-1.295052734375" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.5327265625" Y="-1.32980456543" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.9019375" Y="-1.110842407227" />
                  <Point X="-29.927392578125" Y="-1.011187866211" />
                  <Point X="-29.98973046875" Y="-0.575330078125" />
                  <Point X="-29.998396484375" Y="-0.51474230957" />
                  <Point X="-29.755732421875" Y="-0.449720855713" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.54189453125" Y="-0.121424591064" />
                  <Point X="-28.51948046875" Y="-0.105868728638" />
                  <Point X="-28.514140625" Y="-0.102162414551" />
                  <Point X="-28.494896484375" Y="-0.075907302856" />
                  <Point X="-28.48742578125" Y="-0.051835510254" />
                  <Point X="-28.485646484375" Y="-0.046095401764" />
                  <Point X="-28.485646484375" Y="-0.01645980835" />
                  <Point X="-28.4931171875" Y="0.007611982346" />
                  <Point X="-28.494896484375" Y="0.013342202187" />
                  <Point X="-28.514140625" Y="0.039602336884" />
                  <Point X="-28.5365546875" Y="0.055158203125" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-28.842958984375" Y="0.142583831787" />
                  <Point X="-29.998185546875" Y="0.452126037598" />
                  <Point X="-29.934048828125" Y="0.885562866211" />
                  <Point X="-29.91764453125" Y="0.996415466309" />
                  <Point X="-29.79216015625" Y="1.459493286133" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.612212890625" Y="1.507063110352" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731705078125" Y="1.395866210938" />
                  <Point X="-28.682099609375" Y="1.411507324219" />
                  <Point X="-28.670279296875" Y="1.415233764648" />
                  <Point X="-28.651533203125" Y="1.426056030273" />
                  <Point X="-28.6391171875" Y="1.443786743164" />
                  <Point X="-28.619212890625" Y="1.491841796875" />
                  <Point X="-28.614470703125" Y="1.503291137695" />
                  <Point X="-28.610712890625" Y="1.524606323242" />
                  <Point X="-28.61631640625" Y="1.545512695312" />
                  <Point X="-28.640333984375" Y="1.591649902344" />
                  <Point X="-28.646056640625" Y="1.602642456055" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-28.823728515625" Y="1.744880371094" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.22375390625" Y="2.677805175781" />
                  <Point X="-29.16001171875" Y="2.7870078125" />
                  <Point X="-28.827611328125" Y="3.214264160156" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.68669921875" Y="3.231520263672" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.06942578125" Y="2.914390380859" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927405029297" />
                  <Point X="-27.9642109375" Y="2.976444580078" />
                  <Point X="-27.95252734375" Y="2.988128417969" />
                  <Point X="-27.9408984375" Y="3.006381591797" />
                  <Point X="-27.938072265625" Y="3.027839111328" />
                  <Point X="-27.9441171875" Y="3.096927734375" />
                  <Point X="-27.945556640625" Y="3.113388671875" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.024634765625" Y="3.259723632812" />
                  <Point X="-28.30727734375" Y="3.749276855469" />
                  <Point X="-27.86388671875" Y="4.089219726562" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.229349609375" Y="4.465190917969" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.129322265625" Y="4.498040039062" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.951244140625" Y="4.27366015625" />
                  <Point X="-26.874349609375" Y="4.233630859375" />
                  <Point X="-26.85602734375" Y="4.224093261719" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.81380859375" Y="4.22225" />
                  <Point X="-26.73371875" Y="4.255425292969" />
                  <Point X="-26.714634765625" Y="4.263329589844" />
                  <Point X="-26.696904296875" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.29448828125" />
                  <Point X="-26.660013671875" Y="4.377166503906" />
                  <Point X="-26.653802734375" Y="4.396864746094" />
                  <Point X="-26.651916015625" Y="4.418426757812" />
                  <Point X="-26.660166015625" Y="4.481092773438" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.1118046875" Y="4.863004394531" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.333419921875" Y="4.977575683594" />
                  <Point X="-25.22419921875" Y="4.990358398438" />
                  <Point X="-25.198201171875" Y="4.893333496094" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.90836328125" Y="4.449671875" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.26527734375" Y="4.93870703125" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.614712890625" Y="4.798794921875" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.149078125" Y="4.64484375" />
                  <Point X="-23.068970703125" Y="4.615788085938" />
                  <Point X="-22.738484375" Y="4.461230957031" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.342044921875" Y="4.239135742188" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-21.966373046875" Y="3.981564208984" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.074306640625" Y="3.708826660156" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491514160156" />
                  <Point X="-22.792486328125" Y="2.426676025391" />
                  <Point X="-22.797955078125" Y="2.392324707031" />
                  <Point X="-22.791193359375" Y="2.336258300781" />
                  <Point X="-22.789583984375" Y="2.32290625" />
                  <Point X="-22.781318359375" Y="2.3008125" />
                  <Point X="-22.746625" Y="2.249685302734" />
                  <Point X="-22.72505859375" Y="2.224203125" />
                  <Point X="-22.673931640625" Y="2.189510986328" />
                  <Point X="-22.66175" Y="2.181245605469" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.583595703125" Y="2.166219482422" />
                  <Point X="-22.5513359375" Y="2.165946044922" />
                  <Point X="-22.486498046875" Y="2.183284667969" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-22.171810546875" Y="2.358206054688" />
                  <Point X="-21.005751953125" Y="3.031430419922" />
                  <Point X="-20.841640625" Y="2.803352050781" />
                  <Point X="-20.797400390625" Y="2.741869384766" />
                  <Point X="-20.629546875" Y="2.464490722656" />
                  <Point X="-20.612486328125" Y="2.436296142578" />
                  <Point X="-20.795244140625" Y="2.296059570312" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583831665039" />
                  <Point X="-21.76729296875" Y="1.522954589844" />
                  <Point X="-21.78687890625" Y="1.491503295898" />
                  <Point X="-21.80426171875" Y="1.42934777832" />
                  <Point X="-21.808404296875" Y="1.414538818359" />
                  <Point X="-21.809220703125" Y="1.390963500977" />
                  <Point X="-21.794951171875" Y="1.321807495117" />
                  <Point X="-21.784353515625" Y="1.287956787109" />
                  <Point X="-21.74554296875" Y="1.228966186523" />
                  <Point X="-21.736296875" Y="1.214911254883" />
                  <Point X="-21.71905078125" Y="1.198819580078" />
                  <Point X="-21.66280859375" Y="1.16716003418" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.555390625" Y="1.143569580078" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-21.25141796875" Y="1.177083618164" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.07980859375" Y="1.029420898438" />
                  <Point X="-20.060806640625" Y="0.951367492676" />
                  <Point X="-20.007912109375" Y="0.611634338379" />
                  <Point X="-20.002140625" Y="0.57455645752" />
                  <Point X="-20.20891796875" Y="0.51915020752" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.23281980896" />
                  <Point X="-21.34562109375" Y="0.189636123657" />
                  <Point X="-21.363421875" Y="0.179347290039" />
                  <Point X="-21.377734375" Y="0.166925933838" />
                  <Point X="-21.422560546875" Y="0.109807189941" />
                  <Point X="-21.433240234375" Y="0.096198257446" />
                  <Point X="-21.443013671875" Y="0.074736480713" />
                  <Point X="-21.457955078125" Y="-0.003284866333" />
                  <Point X="-21.461515625" Y="-0.040685997009" />
                  <Point X="-21.44657421875" Y="-0.118707336426" />
                  <Point X="-21.443013671875" Y="-0.137296554565" />
                  <Point X="-21.433240234375" Y="-0.158758331299" />
                  <Point X="-21.3884140625" Y="-0.215877075195" />
                  <Point X="-21.363421875" Y="-0.241907363892" />
                  <Point X="-21.288712890625" Y="-0.285091033936" />
                  <Point X="-21.270912109375" Y="-0.295379730225" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-21.008681640625" Y="-0.367414245605" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.0409609375" Y="-0.896044250488" />
                  <Point X="-20.051568359375" Y="-0.966411010742" />
                  <Point X="-20.1193359375" Y="-1.263378417969" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-20.3704140625" Y="-1.257928833008" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.751791015625" Y="-1.130211791992" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.8145546875" Y="-1.154698242188" />
                  <Point X="-21.90318359375" Y="-1.261289916992" />
                  <Point X="-21.924298828125" Y="-1.286686157227" />
                  <Point X="-21.935640625" Y="-1.314072021484" />
                  <Point X="-21.94834375" Y="-1.45211315918" />
                  <Point X="-21.951369140625" Y="-1.485002441406" />
                  <Point X="-21.943638671875" Y="-1.516622070312" />
                  <Point X="-21.8624921875" Y="-1.642840209961" />
                  <Point X="-21.843158203125" Y="-1.672912719727" />
                  <Point X="-21.831537109375" Y="-1.685540771484" />
                  <Point X="-21.5993984375" Y="-1.863668334961" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.765966796875" Y="-2.753757568359" />
                  <Point X="-20.7958671875" Y="-2.802139160156" />
                  <Point X="-20.93601953125" Y="-3.001278564453" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.162103515625" Y="-2.885317871094" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.437169921875" Y="-2.221796142578" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219245117188" />
                  <Point X="-22.6558984375" Y="-2.295545166016" />
                  <Point X="-22.690439453125" Y="-2.313724121094" />
                  <Point X="-22.7113984375" Y="-2.334683105469" />
                  <Point X="-22.78769921875" Y="-2.479659423828" />
                  <Point X="-22.80587890625" Y="-2.514201171875" />
                  <Point X="-22.8108359375" Y="-2.546375244141" />
                  <Point X="-22.779318359375" Y="-2.720886962891" />
                  <Point X="-22.77180859375" Y="-2.762465576172" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.616734375" Y="-3.036954589844" />
                  <Point X="-22.01332421875" Y="-4.082087890625" />
                  <Point X="-22.129220703125" Y="-4.164868652344" />
                  <Point X="-22.164720703125" Y="-4.190224609375" />
                  <Point X="-22.32022265625" Y="-4.290879394531" />
                  <Point X="-22.48929296875" Y="-4.07054296875" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467529297" />
                  <Point X="-23.50156640625" Y="-2.869812988281" />
                  <Point X="-23.54257421875" Y="-2.843448730469" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.762431640625" Y="-2.853038818359" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509277344" />
                  <Point X="-23.98001953125" Y="-2.989364990234" />
                  <Point X="-24.014650390625" Y="-3.018159667969" />
                  <Point X="-24.03154296875" Y="-3.045985351562" />
                  <Point X="-24.075001953125" Y="-3.245933837891" />
                  <Point X="-24.085357421875" Y="-3.293573242188" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-24.043794921875" Y="-3.631827636719" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.972087890625" Y="-4.955889648438" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#198" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.156061723514" Y="4.937554784567" Z="2.05" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.05" />
                  <Point X="-0.345389572826" Y="5.058516680955" Z="2.05" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.05" />
                  <Point X="-1.131460266896" Y="4.942428104167" Z="2.05" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.05" />
                  <Point X="-1.71589601634" Y="4.50584653185" Z="2.05" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.05" />
                  <Point X="-1.713856853068" Y="4.423481960811" Z="2.05" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.05" />
                  <Point X="-1.759007531452" Y="4.332899850776" Z="2.05" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.05" />
                  <Point X="-1.857419911193" Y="4.309261883632" Z="2.05" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.05" />
                  <Point X="-2.095811966217" Y="4.559758099315" Z="2.05" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.05" />
                  <Point X="-2.259789677052" Y="4.540178315111" Z="2.05" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.05" />
                  <Point X="-2.900466297684" Y="4.160179403398" Z="2.05" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.05" />
                  <Point X="-3.074092346887" Y="3.266003777046" Z="2.05" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.05" />
                  <Point X="-3.000084535351" Y="3.123852070152" Z="2.05" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.05" />
                  <Point X="-3.005724034164" Y="3.043079533723" Z="2.05" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.05" />
                  <Point X="-3.071224395455" Y="2.995480163822" Z="2.05" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.05" />
                  <Point X="-3.667855571251" Y="3.306101601859" Z="2.05" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.05" />
                  <Point X="-3.873230399871" Y="3.276246752366" Z="2.05" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.05" />
                  <Point X="-4.273091904677" Y="2.734290693657" Z="2.05" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.05" />
                  <Point X="-3.860323746639" Y="1.736492692725" Z="2.05" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.05" />
                  <Point X="-3.690839944027" Y="1.599841595554" Z="2.05" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.05" />
                  <Point X="-3.671564937153" Y="1.542254963542" Z="2.05" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.05" />
                  <Point X="-3.703289122963" Y="1.490473529865" Z="2.05" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.05" />
                  <Point X="-4.611845064803" Y="1.587915349362" Z="2.05" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.05" />
                  <Point X="-4.8465768165" Y="1.503850332546" Z="2.05" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.05" />
                  <Point X="-4.98966622695" Y="0.924162146069" Z="2.05" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.05" />
                  <Point X="-3.862057218618" Y="0.125567585289" Z="2.05" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.05" />
                  <Point X="-3.571220711147" Y="0.045362750081" Z="2.05" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.05" />
                  <Point X="-3.547027820652" Y="0.024071695047" Z="2.05" />
                  <Point X="-3.539556741714" Y="0" Z="2.05" />
                  <Point X="-3.541336780483" Y="-0.005735256013" Z="2.05" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.05" />
                  <Point X="-3.554147883976" Y="-0.033513233053" Z="2.05" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.05" />
                  <Point X="-4.774830570155" Y="-0.370144471467" Z="2.05" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.05" />
                  <Point X="-5.045383205101" Y="-0.551128833465" Z="2.05" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.05" />
                  <Point X="-4.956539992345" Y="-1.091936418944" Z="2.05" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.05" />
                  <Point X="-3.532358686375" Y="-1.348096835761" Z="2.05" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.05" />
                  <Point X="-3.214063284898" Y="-1.309862358117" Z="2.05" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.05" />
                  <Point X="-3.194158753913" Y="-1.328173594577" Z="2.05" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.05" />
                  <Point X="-4.252278371728" Y="-2.159346529909" Z="2.05" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.05" />
                  <Point X="-4.446418477542" Y="-2.446367620021" Z="2.05" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.05" />
                  <Point X="-4.142565118095" Y="-2.931635083954" Z="2.05" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.05" />
                  <Point X="-2.820937184329" Y="-2.698730234309" Z="2.05" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.05" />
                  <Point X="-2.569501409736" Y="-2.558829059395" Z="2.05" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.05" />
                  <Point X="-3.156686778859" Y="-3.614141198163" Z="2.05" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.05" />
                  <Point X="-3.22114228452" Y="-3.922899753615" Z="2.05" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.05" />
                  <Point X="-2.805937203153" Y="-4.229846176627" Z="2.05" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.05" />
                  <Point X="-2.269495174205" Y="-4.212846497244" Z="2.05" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.05" />
                  <Point X="-2.176586114454" Y="-4.123286306883" Z="2.05" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.05" />
                  <Point X="-1.908686904358" Y="-3.987988736587" Z="2.05" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.05" />
                  <Point X="-1.61378433819" Y="-4.043736893921" Z="2.05" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.05" />
                  <Point X="-1.413759689477" Y="-4.26749030341" Z="2.05" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.05" />
                  <Point X="-1.403820778501" Y="-4.809027807669" Z="2.05" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.05" />
                  <Point X="-1.356202952323" Y="-4.894142121602" Z="2.05" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.05" />
                  <Point X="-1.059729054718" Y="-4.966778062887" Z="2.05" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.05" />
                  <Point X="-0.494163979497" Y="-3.806429123842" Z="2.05" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.05" />
                  <Point X="-0.385583380809" Y="-3.473382686698" Z="2.05" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.05" />
                  <Point X="-0.204609404142" Y="-3.267742576692" Z="2.05" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.05" />
                  <Point X="0.048749675218" Y="-3.219369468596" Z="2.05" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.05" />
                  <Point X="0.284862478624" Y="-3.32826306907" Z="2.05" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.05" />
                  <Point X="0.740590880904" Y="-4.726106905974" Z="2.05" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.05" />
                  <Point X="0.852368308019" Y="-5.007459185956" Z="2.05" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.05" />
                  <Point X="1.032460390834" Y="-4.973449906005" Z="2.05" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.05" />
                  <Point X="0.999620385556" Y="-3.594021136898" Z="2.05" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.05" />
                  <Point X="0.96770036171" Y="-3.225274513428" Z="2.05" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.05" />
                  <Point X="1.04579129374" Y="-2.996531241797" Z="2.05" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.05" />
                  <Point X="1.235992750459" Y="-2.871548394122" Z="2.05" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.05" />
                  <Point X="1.465238256601" Y="-2.880590815793" Z="2.05" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.05" />
                  <Point X="2.464882077291" Y="-4.069700770901" Z="2.05" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.05" />
                  <Point X="2.699610938868" Y="-4.302335992734" Z="2.05" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.05" />
                  <Point X="2.893685697903" Y="-4.174275675276" Z="2.05" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.05" />
                  <Point X="2.420410310898" Y="-2.980674446849" Z="2.05" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.05" />
                  <Point X="2.263727813843" Y="-2.680719931401" Z="2.05" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.05" />
                  <Point X="2.250390152276" Y="-2.471666520364" Z="2.05" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.05" />
                  <Point X="2.361231858008" Y="-2.308511157691" Z="2.05" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.05" />
                  <Point X="2.547786872111" Y="-2.239720195648" Z="2.05" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.05" />
                  <Point X="3.806739150258" Y="-2.897339189377" Z="2.05" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.05" />
                  <Point X="4.098711750201" Y="-2.998776213844" Z="2.05" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.05" />
                  <Point X="4.270409547741" Y="-2.748763045649" Z="2.05" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.05" />
                  <Point X="3.424882964217" Y="-1.792720447032" Z="2.05" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.05" />
                  <Point X="3.173409067564" Y="-1.584520640644" Z="2.05" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.05" />
                  <Point X="3.095288966916" Y="-1.425413228641" Z="2.05" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.05" />
                  <Point X="3.129107684116" Y="-1.261975965772" Z="2.05" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.05" />
                  <Point X="3.252670934429" Y="-1.147790828024" Z="2.05" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.05" />
                  <Point X="4.616903773121" Y="-1.276221008595" Z="2.05" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.05" />
                  <Point X="4.923252518858" Y="-1.243222568524" Z="2.05" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.05" />
                  <Point X="5.00232450809" Y="-0.872217453006" Z="2.05" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.05" />
                  <Point X="3.998102095533" Y="-0.287837962411" Z="2.05" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.05" />
                  <Point X="3.730152541352" Y="-0.210521826315" Z="2.05" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.05" />
                  <Point X="3.644762619705" Y="-0.153729256581" Z="2.05" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.05" />
                  <Point X="3.596376692047" Y="-0.078021372973" Z="2.05" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.05" />
                  <Point X="3.584994758377" Y="0.018589158214" Z="2.05" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.05" />
                  <Point X="3.610616818695" Y="0.110219481994" Z="2.05" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.05" />
                  <Point X="3.673242873001" Y="0.177627017927" Z="2.05" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.05" />
                  <Point X="4.797865721773" Y="0.502133970358" Z="2.05" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.05" />
                  <Point X="5.035334646635" Y="0.65060589122" Z="2.05" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.05" />
                  <Point X="4.962616133929" Y="1.072527370675" Z="2.05" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.05" />
                  <Point X="3.735898937029" Y="1.25793593636" Z="2.05" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.05" />
                  <Point X="3.445003617907" Y="1.224418570627" Z="2.05" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.05" />
                  <Point X="3.355560553918" Y="1.242011655283" Z="2.05" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.05" />
                  <Point X="3.290071635506" Y="1.287725937644" Z="2.05" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.05" />
                  <Point X="3.247861251184" Y="1.363193300112" Z="2.05" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.05" />
                  <Point X="3.237733529699" Y="1.447158203649" Z="2.05" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.05" />
                  <Point X="3.266234048747" Y="1.523818091726" Z="2.05" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.05" />
                  <Point X="4.22903550732" Y="2.287671988631" Z="2.05" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.05" />
                  <Point X="4.407072872855" Y="2.521656713079" Z="2.05" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.05" />
                  <Point X="4.192789270756" Y="2.863835735763" Z="2.05" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.05" />
                  <Point X="2.797032414539" Y="2.43278748303" Z="2.05" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.05" />
                  <Point X="2.494429769152" Y="2.262867731022" Z="2.05" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.05" />
                  <Point X="2.416233382731" Y="2.247139935107" Z="2.05" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.05" />
                  <Point X="2.347985344505" Y="2.262166133984" Z="2.05" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.05" />
                  <Point X="2.288592466251" Y="2.309039515877" Z="2.05" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.05" />
                  <Point X="2.252289767652" Y="2.373525061101" Z="2.05" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.05" />
                  <Point X="2.249660211635" Y="2.44503980673" Z="2.05" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.05" />
                  <Point X="2.96283786983" Y="3.715106667416" Z="2.05" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.05" />
                  <Point X="3.056446797409" Y="4.053591439075" Z="2.05" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.05" />
                  <Point X="2.676968282912" Y="4.313618298063" Z="2.05" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.05" />
                  <Point X="2.276540076682" Y="4.537803183401" Z="2.05" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.05" />
                  <Point X="1.861814224208" Y="4.723127164625" Z="2.05" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.05" />
                  <Point X="1.390864807794" Y="4.878678580506" Z="2.05" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.05" />
                  <Point X="0.733773678584" Y="5.019715197532" Z="2.05" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.05" />
                  <Point X="0.037182838039" Y="4.493892721383" Z="2.05" />
                  <Point X="0" Y="4.355124473572" Z="2.05" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>