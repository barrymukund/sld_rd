<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#183" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2481" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.176275390625" Y="-4.484426757812" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140136719" />
                  <Point X="-24.457634765625" Y="-3.467376708984" />
                  <Point X="-24.564076171875" Y="-3.314016357422" />
                  <Point X="-24.62136328125" Y="-3.231476318359" />
                  <Point X="-24.643248046875" Y="-3.209021240234" />
                  <Point X="-24.66950390625" Y="-3.189777099609" />
                  <Point X="-24.69750390625" Y="-3.175669189453" />
                  <Point X="-24.862212890625" Y="-3.124549072266" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9790234375" Y="-3.092766357422" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097036132812" />
                  <Point X="-25.201533203125" Y="-3.148156005859" />
                  <Point X="-25.29018359375" Y="-3.175669433594" />
                  <Point X="-25.31818359375" Y="-3.18977734375" />
                  <Point X="-25.344439453125" Y="-3.209021484375" />
                  <Point X="-25.36632421875" Y="-3.231477294922" />
                  <Point X="-25.472763671875" Y="-3.384837402344" />
                  <Point X="-25.53005078125" Y="-3.467377685547" />
                  <Point X="-25.5381875" Y="-3.481571777344" />
                  <Point X="-25.550990234375" Y="-3.512524658203" />
                  <Point X="-25.691150390625" Y="-4.035612060547" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-25.978740234375" Y="-4.864876953125" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.24641796875" Y="-4.802362792969" />
                  <Point X="-26.246310546875" Y="-4.801552734375" />
                  <Point X="-26.2149609375" Y="-4.5634375" />
                  <Point X="-26.21419921875" Y="-4.547929199219" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.255857421875" Y="-4.318401367187" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.182963867188" />
                  <Point X="-26.30401171875" Y="-4.155126953125" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.475287109375" Y="-3.998215576172" />
                  <Point X="-26.556904296875" Y="-3.926639648438" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.84429296875" Y="-3.877774658203" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708740234" />
                  <Point X="-27.014466796875" Y="-3.882028564453" />
                  <Point X="-27.042658203125" Y="-3.894801757812" />
                  <Point X="-27.21036328125" Y="-4.006859130859" />
                  <Point X="-27.300623046875" Y="-4.067169677734" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.3351015625" Y="-4.099461914062" />
                  <Point X="-27.413791015625" Y="-4.20201171875" />
                  <Point X="-27.4801484375" Y="-4.288489257813" />
                  <Point X="-27.654255859375" Y="-4.180686035156" />
                  <Point X="-27.801712890625" Y="-4.089383789063" />
                  <Point X="-28.062044921875" Y="-3.888937988281" />
                  <Point X="-28.104720703125" Y="-3.856078613281" />
                  <Point X="-27.90772265625" Y="-3.514865966797" />
                  <Point X="-27.423759765625" Y="-2.676619384766" />
                  <Point X="-27.412859375" Y="-2.647654296875" />
                  <Point X="-27.406587890625" Y="-2.616127197266" />
                  <Point X="-27.405576171875" Y="-2.585190917969" />
                  <Point X="-27.414560546875" Y="-2.555571044922" />
                  <Point X="-27.428779296875" Y="-2.526741699219" />
                  <Point X="-27.4468046875" Y="-2.501589355469" />
                  <Point X="-27.464146484375" Y="-2.484246826172" />
                  <Point X="-27.489294921875" Y="-2.466222412109" />
                  <Point X="-27.518125" Y="-2.452000976562" />
                  <Point X="-27.54774609375" Y="-2.443012939453" />
                  <Point X="-27.57868359375" Y="-2.444023925781" />
                  <Point X="-27.610212890625" Y="-2.450294433594" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.0903359375" Y="-2.721670410156" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-28.966365234375" Y="-2.946910888672" />
                  <Point X="-29.082857421875" Y="-2.79386328125" />
                  <Point X="-29.269498046875" Y="-2.480895996094" />
                  <Point X="-29.306142578125" Y="-2.419449951172" />
                  <Point X="-28.95273046875" Y="-2.148267822266" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.084578125" Y="-1.475594238281" />
                  <Point X="-28.066611328125" Y="-1.44846105957" />
                  <Point X="-28.05385546875" Y="-1.41983215332" />
                  <Point X="-28.046150390625" Y="-1.390084960938" />
                  <Point X="-28.043345703125" Y="-1.359647949219" />
                  <Point X="-28.045556640625" Y="-1.327978881836" />
                  <Point X="-28.05255859375" Y="-1.298236816406" />
                  <Point X="-28.068640625" Y="-1.272255859375" />
                  <Point X="-28.08947265625" Y="-1.248300537109" />
                  <Point X="-28.112970703125" Y="-1.228767700195" />
                  <Point X="-28.139453125" Y="-1.213181152344" />
                  <Point X="-28.16871484375" Y="-1.201957397461" />
                  <Point X="-28.2006015625" Y="-1.195475097656" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-28.80146484375" Y="-1.269364868164" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.788513671875" Y="-1.171030273438" />
                  <Point X="-29.834076171875" Y="-0.992650268555" />
                  <Point X="-29.88345703125" Y="-0.647395751953" />
                  <Point X="-29.892423828125" Y="-0.584698425293" />
                  <Point X="-29.49748046875" Y="-0.478873657227" />
                  <Point X="-28.532875" Y="-0.220408309937" />
                  <Point X="-28.517490234375" Y="-0.214826828003" />
                  <Point X="-28.48773046875" Y="-0.199470809937" />
                  <Point X="-28.4696875" Y="-0.186948394775" />
                  <Point X="-28.4599765625" Y="-0.180208755493" />
                  <Point X="-28.43751953125" Y="-0.158324523926" />
                  <Point X="-28.4182734375" Y="-0.132066467285" />
                  <Point X="-28.404166015625" Y="-0.104063110352" />
                  <Point X="-28.39815234375" Y="-0.084685394287" />
                  <Point X="-28.394916015625" Y="-0.07425617981" />
                  <Point X="-28.390646484375" Y="-0.046098609924" />
                  <Point X="-28.390646484375" Y="-0.016461536407" />
                  <Point X="-28.394916015625" Y="0.011696187019" />
                  <Point X="-28.4009296875" Y="0.031073747635" />
                  <Point X="-28.404166015625" Y="0.041502960205" />
                  <Point X="-28.4182734375" Y="0.069506469727" />
                  <Point X="-28.43751953125" Y="0.095764381409" />
                  <Point X="-28.4599765625" Y="0.117648757935" />
                  <Point X="-28.47801953125" Y="0.130171020508" />
                  <Point X="-28.486259765625" Y="0.135286956787" />
                  <Point X="-28.511537109375" Y="0.149246505737" />
                  <Point X="-28.532875" Y="0.157848327637" />
                  <Point X="-29.05203515625" Y="0.296956939697" />
                  <Point X="-29.89181640625" Y="0.521975646973" />
                  <Point X="-29.8538515625" Y="0.778540344238" />
                  <Point X="-29.82448828125" Y="0.976968078613" />
                  <Point X="-29.7250859375" Y="1.343791870117" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-29.4571171875" Y="1.390824462891" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.744982421875" Y="1.299341796875" />
                  <Point X="-28.723421875" Y="1.301228271484" />
                  <Point X="-28.70313671875" Y="1.305263549805" />
                  <Point X="-28.663203125" Y="1.317854492188" />
                  <Point X="-28.6417109375" Y="1.324631103516" />
                  <Point X="-28.622775390625" Y="1.332963256836" />
                  <Point X="-28.60403125" Y="1.343785766602" />
                  <Point X="-28.5873515625" Y="1.35601550293" />
                  <Point X="-28.57371484375" Y="1.371564819336" />
                  <Point X="-28.561298828125" Y="1.389295776367" />
                  <Point X="-28.55134765625" Y="1.407432983398" />
                  <Point X="-28.53532421875" Y="1.44611706543" />
                  <Point X="-28.526701171875" Y="1.466937255859" />
                  <Point X="-28.520912109375" Y="1.486806152344" />
                  <Point X="-28.51715625" Y="1.508120605469" />
                  <Point X="-28.515806640625" Y="1.528755615234" />
                  <Point X="-28.518951171875" Y="1.549193969727" />
                  <Point X="-28.524552734375" Y="1.570099731445" />
                  <Point X="-28.53205078125" Y="1.589378417969" />
                  <Point X="-28.551384765625" Y="1.626518798828" />
                  <Point X="-28.561791015625" Y="1.646508056641" />
                  <Point X="-28.573283203125" Y="1.663707763672" />
                  <Point X="-28.5871953125" Y="1.680287231445" />
                  <Point X="-28.60213671875" Y="1.694590087891" />
                  <Point X="-28.899927734375" Y="1.92309387207" />
                  <Point X="-29.351859375" Y="2.269873291016" />
                  <Point X="-29.19524609375" Y="2.538189208984" />
                  <Point X="-29.081146484375" Y="2.733665527344" />
                  <Point X="-28.81784765625" Y="3.072102539062" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.631328125" Y="3.089855224609" />
                  <Point X="-28.20665625" Y="2.844671142578" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.09117578125" Y="2.820930664063" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.99901171875" Y="2.826504882812" />
                  <Point X="-27.9804609375" Y="2.835654541016" />
                  <Point X="-27.96220703125" Y="2.847284179688" />
                  <Point X="-27.946078125" Y="2.860228515625" />
                  <Point X="-27.906599609375" Y="2.899705322266" />
                  <Point X="-27.885353515625" Y="2.920951904297" />
                  <Point X="-27.872404296875" Y="2.937086181641" />
                  <Point X="-27.860775390625" Y="2.955340576172" />
                  <Point X="-27.85162890625" Y="2.973889404297" />
                  <Point X="-27.8467109375" Y="2.993978271484" />
                  <Point X="-27.843884765625" Y="3.015437255859" />
                  <Point X="-27.84343359375" Y="3.036120605469" />
                  <Point X="-27.848298828125" Y="3.091736816406" />
                  <Point X="-27.85091796875" Y="3.121670166016" />
                  <Point X="-27.854955078125" Y="3.141964355469" />
                  <Point X="-27.86146484375" Y="3.162605957031" />
                  <Point X="-27.869794921875" Y="3.181533203125" />
                  <Point X="-28.001755859375" Y="3.410095458984" />
                  <Point X="-28.18333203125" Y="3.724595703125" />
                  <Point X="-27.899333984375" Y="3.942333984375" />
                  <Point X="-27.70062109375" Y="4.094685791016" />
                  <Point X="-27.285927734375" Y="4.325080566406" />
                  <Point X="-27.16703515625" Y="4.391134277344" />
                  <Point X="-27.0431953125" Y="4.229740722656" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.99511328125" Y="4.189395019531" />
                  <Point X="-26.933212890625" Y="4.157171386719" />
                  <Point X="-26.899896484375" Y="4.139828125" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.777451171875" Y="4.134482421875" />
                  <Point X="-26.712978515625" Y="4.161188476562" />
                  <Point X="-26.67827734375" Y="4.175562011719" />
                  <Point X="-26.660142578125" Y="4.18551171875" />
                  <Point X="-26.6424140625" Y="4.19792578125" />
                  <Point X="-26.62686328125" Y="4.2115625" />
                  <Point X="-26.6146328125" Y="4.2282421875" />
                  <Point X="-26.603810546875" Y="4.246985839844" />
                  <Point X="-26.595478515625" Y="4.265921386719" />
                  <Point X="-26.574494140625" Y="4.332477050781" />
                  <Point X="-26.56319921875" Y="4.368297851562" />
                  <Point X="-26.5591640625" Y="4.388583007812" />
                  <Point X="-26.55727734375" Y="4.410145019531" />
                  <Point X="-26.557728515625" Y="4.430827636719" />
                  <Point X="-26.572732421875" Y="4.544782714844" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.206876953125" Y="4.737687011719" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.446900390625" Y="4.868645996094" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.259533203125" Y="4.755171386719" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.78616796875" Y="4.538658691406" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.38057421875" Y="4.855262207031" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.74002734375" Y="4.731320800781" />
                  <Point X="-23.51897265625" Y="4.677950683594" />
                  <Point X="-23.248759765625" Y="4.579943359375" />
                  <Point X="-23.105359375" Y="4.527930664062" />
                  <Point X="-22.843568359375" Y="4.4055" />
                  <Point X="-22.705419921875" Y="4.340891601563" />
                  <Point X="-22.452501953125" Y="4.193541992188" />
                  <Point X="-22.31902734375" Y="4.115778320312" />
                  <Point X="-22.08050390625" Y="3.946155761719" />
                  <Point X="-22.05673828125" Y="3.929254394531" />
                  <Point X="-22.292267578125" Y="3.521305664062" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857921875" Y="2.539932373047" />
                  <Point X="-22.866921875" Y="2.51605859375" />
                  <Point X="-22.880880859375" Y="2.463864013672" />
                  <Point X="-22.888392578125" Y="2.435772216797" />
                  <Point X="-22.891380859375" Y="2.417936035156" />
                  <Point X="-22.892271484375" Y="2.380953613281" />
                  <Point X="-22.886828125" Y="2.335820068359" />
                  <Point X="-22.883900390625" Y="2.311528808594" />
                  <Point X="-22.87855859375" Y="2.289607177734" />
                  <Point X="-22.870291015625" Y="2.267514892578" />
                  <Point X="-22.859927734375" Y="2.247469726562" />
                  <Point X="-22.832" Y="2.206312255859" />
                  <Point X="-22.81696875" Y="2.184161132813" />
                  <Point X="-22.805529296875" Y="2.170325439453" />
                  <Point X="-22.7783984375" Y="2.145592529297" />
                  <Point X="-22.7372421875" Y="2.117665527344" />
                  <Point X="-22.71508984375" Y="2.102635009766" />
                  <Point X="-22.695044921875" Y="2.092271972656" />
                  <Point X="-22.672958984375" Y="2.084006591797" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.60590234375" Y="2.073221191406" />
                  <Point X="-22.581611328125" Y="2.070291992188" />
                  <Point X="-22.563533203125" Y="2.069845703125" />
                  <Point X="-22.526794921875" Y="2.074170898438" />
                  <Point X="-22.474599609375" Y="2.088128417969" />
                  <Point X="-22.4465078125" Y="2.095640625" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.8892890625" Y="2.411623046875" />
                  <Point X="-21.032673828125" Y="2.906190673828" />
                  <Point X="-20.95590625" Y="2.799501220703" />
                  <Point X="-20.87671875" Y="2.689451416016" />
                  <Point X="-20.7437578125" Y="2.469729492188" />
                  <Point X="-20.73780078125" Y="2.459883789063" />
                  <Point X="-21.031724609375" Y="2.234346679688" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.77857421875" Y="1.660241699219" />
                  <Point X="-21.79602734375" Y="1.641626831055" />
                  <Point X="-21.833591796875" Y="1.59262109375" />
                  <Point X="-21.85380859375" Y="1.566245727539" />
                  <Point X="-21.86339453125" Y="1.550909790039" />
                  <Point X="-21.8783671875" Y="1.517088623047" />
                  <Point X="-21.892361328125" Y="1.467053466797" />
                  <Point X="-21.899892578125" Y="1.440124145508" />
                  <Point X="-21.90334765625" Y="1.417824707031" />
                  <Point X="-21.9041640625" Y="1.394255004883" />
                  <Point X="-21.902259765625" Y="1.371766479492" />
                  <Point X="-21.890771484375" Y="1.316095947266" />
                  <Point X="-21.88458984375" Y="1.286133544922" />
                  <Point X="-21.8793203125" Y="1.268982299805" />
                  <Point X="-21.863716796875" Y="1.235741455078" />
                  <Point X="-21.832474609375" Y="1.188254150391" />
                  <Point X="-21.81566015625" Y="1.162695922852" />
                  <Point X="-21.801107421875" Y="1.145451538086" />
                  <Point X="-21.78386328125" Y="1.129361083984" />
                  <Point X="-21.76565234375" Y="1.116034545898" />
                  <Point X="-21.720376953125" Y="1.090548828125" />
                  <Point X="-21.696009765625" Y="1.07683215332" />
                  <Point X="-21.6794765625" Y="1.069501098633" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.582666015625" Y="1.051348266602" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-21.015763671875" Y="1.112288330078" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.1880703125" Y="1.072496826172" />
                  <Point X="-20.154060546875" Y="0.932788513184" />
                  <Point X="-20.11216015625" Y="0.663676757812" />
                  <Point X="-20.109134765625" Y="0.644238830566" />
                  <Point X="-20.438240234375" Y="0.556054626465" />
                  <Point X="-21.283419921875" Y="0.32958972168" />
                  <Point X="-21.295208984375" Y="0.325586120605" />
                  <Point X="-21.318453125" Y="0.315067810059" />
                  <Point X="-21.37859375" Y="0.280305053711" />
                  <Point X="-21.410962890625" Y="0.26159552002" />
                  <Point X="-21.425685546875" Y="0.251098770142" />
                  <Point X="-21.45246875" Y="0.225576583862" />
                  <Point X="-21.488552734375" Y="0.179596084595" />
                  <Point X="-21.507974609375" Y="0.154848937988" />
                  <Point X="-21.519701171875" Y="0.135564727783" />
                  <Point X="-21.529474609375" Y="0.114098838806" />
                  <Point X="-21.536318359375" Y="0.09260395813" />
                  <Point X="-21.54834765625" Y="0.029796735764" />
                  <Point X="-21.5548203125" Y="-0.004006680489" />
                  <Point X="-21.556515625" Y="-0.021876962662" />
                  <Point X="-21.5548203125" Y="-0.058553314209" />
                  <Point X="-21.54279296875" Y="-0.121360534668" />
                  <Point X="-21.536318359375" Y="-0.155163955688" />
                  <Point X="-21.52947265625" Y="-0.176665664673" />
                  <Point X="-21.51969921875" Y="-0.198128982544" />
                  <Point X="-21.507974609375" Y="-0.217408935547" />
                  <Point X="-21.471890625" Y="-0.263389434814" />
                  <Point X="-21.45246875" Y="-0.28813671875" />
                  <Point X="-21.439998046875" Y="-0.301238983154" />
                  <Point X="-21.410962890625" Y="-0.324155670166" />
                  <Point X="-21.350822265625" Y="-0.358918395996" />
                  <Point X="-21.318453125" Y="-0.377628082275" />
                  <Point X="-21.3072890625" Y="-0.38313885498" />
                  <Point X="-21.283419921875" Y="-0.392149871826" />
                  <Point X="-20.82853515625" Y="-0.514035583496" />
                  <Point X="-20.108525390625" Y="-0.706961730957" />
                  <Point X="-20.12598828125" Y="-0.822784667969" />
                  <Point X="-20.144974609375" Y="-0.948724487305" />
                  <Point X="-20.19866015625" Y="-1.18397644043" />
                  <Point X="-20.19882421875" Y="-1.18469921875" />
                  <Point X="-20.595447265625" Y="-1.132482788086" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710144043" />
                  <Point X="-21.62533984375" Y="-1.005508728027" />
                  <Point X="-21.743376953125" Y="-1.031164550781" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.836021484375" Y="-1.056595947266" />
                  <Point X="-21.863849609375" Y="-1.07348840332" />
                  <Point X="-21.8876015625" Y="-1.093960083008" />
                  <Point X="-21.958947265625" Y="-1.179766357422" />
                  <Point X="-21.997345703125" Y="-1.225948120117" />
                  <Point X="-22.012068359375" Y="-1.250334228516" />
                  <Point X="-22.02341015625" Y="-1.277718994141" />
                  <Point X="-22.030240234375" Y="-1.305365722656" />
                  <Point X="-22.04046484375" Y="-1.416488769531" />
                  <Point X="-22.04596875" Y="-1.476296386719" />
                  <Point X="-22.04365234375" Y="-1.507560546875" />
                  <Point X="-22.035921875" Y="-1.539182373047" />
                  <Point X="-22.023548828125" Y="-1.567996826172" />
                  <Point X="-21.9582265625" Y="-1.669602294922" />
                  <Point X="-21.923068359375" Y="-1.724287353516" />
                  <Point X="-21.9130625" Y="-1.737243774414" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.467234375" Y="-2.084824707031" />
                  <Point X="-20.786876953125" Y="-2.606882080078" />
                  <Point X="-20.821666015625" Y="-2.663175048828" />
                  <Point X="-20.875203125" Y="-2.749804199219" />
                  <Point X="-20.971017578125" Y="-2.885944824219" />
                  <Point X="-21.3260703125" Y="-2.680954833984" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.2457734375" Y="-2.159825195312" />
                  <Point X="-22.38625390625" Y="-2.134454345703" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135177001953" />
                  <Point X="-22.67187109375" Y="-2.196598388672" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.757615234375" Y="-2.246549072266" />
                  <Point X="-22.77857421875" Y="-2.267508300781" />
                  <Point X="-22.795466796875" Y="-2.290438476562" />
                  <Point X="-22.856888671875" Y="-2.407144287109" />
                  <Point X="-22.889947265625" Y="-2.469956542969" />
                  <Point X="-22.899771484375" Y="-2.499735351563" />
                  <Point X="-22.904728515625" Y="-2.531909423828" />
                  <Point X="-22.90432421875" Y="-2.563259277344" />
                  <Point X="-22.878953125" Y="-2.703740966797" />
                  <Point X="-22.865296875" Y="-2.779349609375" />
                  <Point X="-22.86101171875" Y="-2.795141601562" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.5769140625" Y="-3.295921630859" />
                  <Point X="-22.138712890625" Y="-4.05490625" />
                  <Point X="-22.1546328125" Y="-4.066276855469" />
                  <Point X="-22.21813671875" Y="-4.111635253906" />
                  <Point X="-22.298232421875" Y="-4.16348046875" />
                  <Point X="-22.57529296875" Y="-3.80241015625" />
                  <Point X="-23.241451171875" Y="-2.934254882812" />
                  <Point X="-23.25249609375" Y="-2.922177490234" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.41662890625" Y="-2.811480224609" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.520009765625" Y="-2.751167236328" />
                  <Point X="-23.551630859375" Y="-2.743435546875" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.734431640625" Y="-2.755060791016" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.843634765625" Y="-2.769396972656" />
                  <Point X="-23.87101953125" Y="-2.780739746094" />
                  <Point X="-23.89540234375" Y="-2.7954609375" />
                  <Point X="-24.01241015625" Y="-2.892749755859" />
                  <Point X="-24.07538671875" Y="-2.945111328125" />
                  <Point X="-24.095857421875" Y="-2.968860839844" />
                  <Point X="-24.11275" Y="-2.9966875" />
                  <Point X="-24.124375" Y="-3.02580859375" />
                  <Point X="-24.159359375" Y="-3.186767089844" />
                  <Point X="-24.178189453125" Y="-3.273396484375" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323120361328" />
                  <Point X="-24.1033828125" Y="-3.907037353516" />
                  <Point X="-23.977935546875" Y="-4.859915039062" />
                  <Point X="-24.0243203125" Y="-4.870083007812" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.058419921875" Y="-4.752637695312" />
                  <Point X="-26.141244140625" Y="-4.731328125" />
                  <Point X="-26.1207734375" Y="-4.575837890625" />
                  <Point X="-26.120076171875" Y="-4.568098144531" />
                  <Point X="-26.11944921875" Y="-4.541030273438" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497689453125" />
                  <Point X="-26.16268359375" Y="-4.299867675781" />
                  <Point X="-26.18386328125" Y="-4.193397949219" />
                  <Point X="-26.188125" Y="-4.17846875" />
                  <Point X="-26.19902734375" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135462402344" />
                  <Point X="-26.221740234375" Y="-4.107625488281" />
                  <Point X="-26.230576171875" Y="-4.094859863281" />
                  <Point X="-26.250208984375" Y="-4.070937011719" />
                  <Point X="-26.261005859375" Y="-4.059779785156" />
                  <Point X="-26.4126484375" Y="-3.926791259766" />
                  <Point X="-26.494265625" Y="-3.855215332031" />
                  <Point X="-26.506736328125" Y="-3.845965820312" />
                  <Point X="-26.53301953125" Y="-3.829621582031" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.838080078125" Y="-3.782978027344" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166259766" />
                  <Point X="-27.008005859375" Y="-3.781946044922" />
                  <Point X="-27.0390546875" Y="-3.790265869141" />
                  <Point X="-27.053673828125" Y="-3.795496337891" />
                  <Point X="-27.081865234375" Y="-3.80826953125" />
                  <Point X="-27.0954375" Y="-3.815812255859" />
                  <Point X="-27.263142578125" Y="-3.927869628906" />
                  <Point X="-27.35340234375" Y="-3.988180175781" />
                  <Point X="-27.35968359375" Y="-3.992759765625" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032770507812" />
                  <Point X="-27.410470703125" Y="-4.041629394531" />
                  <Point X="-27.48916015625" Y="-4.144179199219" />
                  <Point X="-27.503201171875" Y="-4.162478515625" />
                  <Point X="-27.604244140625" Y="-4.099915527344" />
                  <Point X="-27.74758984375" Y="-4.011158447266" />
                  <Point X="-27.980861328125" Y="-3.831548828125" />
                  <Point X="-27.82544921875" Y="-3.562365722656" />
                  <Point X="-27.341486328125" Y="-2.724119140625" />
                  <Point X="-27.33484765625" Y="-2.710079589844" />
                  <Point X="-27.323947265625" Y="-2.681114501953" />
                  <Point X="-27.319685546875" Y="-2.666188964844" />
                  <Point X="-27.3134140625" Y="-2.634661865234" />
                  <Point X="-27.311638671875" Y="-2.619232421875" />
                  <Point X="-27.310626953125" Y="-2.588296142578" />
                  <Point X="-27.314666015625" Y="-2.557615966797" />
                  <Point X="-27.323650390625" Y="-2.52799609375" />
                  <Point X="-27.329359375" Y="-2.513549560547" />
                  <Point X="-27.343578125" Y="-2.484720214844" />
                  <Point X="-27.351560546875" Y="-2.471403320312" />
                  <Point X="-27.3695859375" Y="-2.446250976562" />
                  <Point X="-27.37962890625" Y="-2.434415527344" />
                  <Point X="-27.396970703125" Y="-2.417072998047" />
                  <Point X="-27.4088046875" Y="-2.40703125" />
                  <Point X="-27.433953125" Y="-2.389006835938" />
                  <Point X="-27.447267578125" Y="-2.381024169922" />
                  <Point X="-27.47609765625" Y="-2.366802734375" />
                  <Point X="-27.490541015625" Y="-2.36109375" />
                  <Point X="-27.520162109375" Y="-2.352105712891" />
                  <Point X="-27.550849609375" Y="-2.348063720703" />
                  <Point X="-27.581787109375" Y="-2.349074707031" />
                  <Point X="-27.59721484375" Y="-2.350848632812" />
                  <Point X="-27.628744140625" Y="-2.357119140625" />
                  <Point X="-27.64367578125" Y="-2.361383056641" />
                  <Point X="-27.67264453125" Y="-2.372285888672" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.1378359375" Y="-2.639397949219" />
                  <Point X="-28.793087890625" Y="-3.017708251953" />
                  <Point X="-28.890771484375" Y="-2.889372558594" />
                  <Point X="-29.00401953125" Y="-2.740587646484" />
                  <Point X="-29.181265625" Y="-2.443373291016" />
                  <Point X="-28.8948984375" Y="-2.223636474609" />
                  <Point X="-28.048123046875" Y="-1.573882080078" />
                  <Point X="-28.036482421875" Y="-1.563310546875" />
                  <Point X="-28.01510546875" Y="-1.540391235352" />
                  <Point X="-28.005369140625" Y="-1.528043945312" />
                  <Point X="-27.98740234375" Y="-1.500910766602" />
                  <Point X="-27.9798359375" Y="-1.487125" />
                  <Point X="-27.967080078125" Y="-1.45849609375" />
                  <Point X="-27.961890625" Y="-1.443652832031" />
                  <Point X="-27.954185546875" Y="-1.413905639648" />
                  <Point X="-27.95155078125" Y="-1.398802001953" />
                  <Point X="-27.94874609375" Y="-1.368364990234" />
                  <Point X="-27.948576171875" Y="-1.353031738281" />
                  <Point X="-27.950787109375" Y="-1.321362548828" />
                  <Point X="-27.953083984375" Y="-1.306208862305" />
                  <Point X="-27.9600859375" Y="-1.276466796875" />
                  <Point X="-27.97178125" Y="-1.248236328125" />
                  <Point X="-27.98786328125" Y="-1.222255371094" />
                  <Point X="-27.996955078125" Y="-1.209916625977" />
                  <Point X="-28.017787109375" Y="-1.185961303711" />
                  <Point X="-28.028744140625" Y="-1.175244873047" />
                  <Point X="-28.0522421875" Y="-1.155711914062" />
                  <Point X="-28.064783203125" Y="-1.146895629883" />
                  <Point X="-28.091265625" Y="-1.131309082031" />
                  <Point X="-28.105431640625" Y="-1.124482177734" />
                  <Point X="-28.134693359375" Y="-1.113258422852" />
                  <Point X="-28.1497890625" Y="-1.108861572266" />
                  <Point X="-28.18167578125" Y="-1.102379272461" />
                  <Point X="-28.197294921875" Y="-1.100532714844" />
                  <Point X="-28.22862109375" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-28.813865234375" Y="-1.175177612305" />
                  <Point X="-29.66091796875" Y="-1.286694213867" />
                  <Point X="-29.69646875" Y="-1.147519042969" />
                  <Point X="-29.740759765625" Y="-0.97411340332" />
                  <Point X="-29.786451171875" Y="-0.654654602051" />
                  <Point X="-29.472892578125" Y="-0.570636657715" />
                  <Point X="-28.508287109375" Y="-0.312171325684" />
                  <Point X="-28.5004765625" Y="-0.30971282959" />
                  <Point X="-28.473927734375" Y="-0.299250213623" />
                  <Point X="-28.44416796875" Y="-0.283894195557" />
                  <Point X="-28.433564453125" Y="-0.277516052246" />
                  <Point X="-28.415521484375" Y="-0.264993530273" />
                  <Point X="-28.393673828125" Y="-0.248245956421" />
                  <Point X="-28.371216796875" Y="-0.226361816406" />
                  <Point X="-28.360896484375" Y="-0.214485519409" />
                  <Point X="-28.341650390625" Y="-0.188227416992" />
                  <Point X="-28.333431640625" Y="-0.174807815552" />
                  <Point X="-28.31932421875" Y="-0.146804428101" />
                  <Point X="-28.313435546875" Y="-0.132220657349" />
                  <Point X="-28.307421875" Y="-0.112842910767" />
                  <Point X="-28.300990234375" Y="-0.088498184204" />
                  <Point X="-28.296720703125" Y="-0.060340679169" />
                  <Point X="-28.295646484375" Y="-0.046098594666" />
                  <Point X="-28.295646484375" Y="-0.016461551666" />
                  <Point X="-28.296720703125" Y="-0.002219613791" />
                  <Point X="-28.300990234375" Y="0.025938182831" />
                  <Point X="-28.304185546875" Y="0.03985389328" />
                  <Point X="-28.31019921875" Y="0.059231498718" />
                  <Point X="-28.31932421875" Y="0.084244140625" />
                  <Point X="-28.333431640625" Y="0.112247665405" />
                  <Point X="-28.34165234375" Y="0.125667572021" />
                  <Point X="-28.3608984375" Y="0.151925521851" />
                  <Point X="-28.371216796875" Y="0.163801376343" />
                  <Point X="-28.393673828125" Y="0.185685806274" />
                  <Point X="-28.405810546875" Y="0.195694213867" />
                  <Point X="-28.423853515625" Y="0.208216598511" />
                  <Point X="-28.440333984375" Y="0.2184480896" />
                  <Point X="-28.465611328125" Y="0.23240763855" />
                  <Point X="-28.476017578125" Y="0.237356491089" />
                  <Point X="-28.49735546875" Y="0.245958343506" />
                  <Point X="-28.508287109375" Y="0.249611343384" />
                  <Point X="-29.027447265625" Y="0.388719848633" />
                  <Point X="-29.7854453125" Y="0.591825134277" />
                  <Point X="-29.759875" Y="0.764634277344" />
                  <Point X="-29.73133203125" Y="0.95752130127" />
                  <Point X="-29.633583984375" Y="1.318236938477" />
                  <Point X="-29.469517578125" Y="1.296637207031" />
                  <Point X="-28.778064453125" Y="1.20560559082" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.7470546875" Y="1.204364379883" />
                  <Point X="-28.736701171875" Y="1.204703369141" />
                  <Point X="-28.715140625" Y="1.20658984375" />
                  <Point X="-28.70488671875" Y="1.208053955078" />
                  <Point X="-28.6846015625" Y="1.212089233398" />
                  <Point X="-28.6745703125" Y="1.214660400391" />
                  <Point X="-28.63463671875" Y="1.227251342773" />
                  <Point X="-28.61314453125" Y="1.234027954102" />
                  <Point X="-28.60344921875" Y="1.237677001953" />
                  <Point X="-28.584513671875" Y="1.246009155273" />
                  <Point X="-28.5752734375" Y="1.250691894531" />
                  <Point X="-28.556529296875" Y="1.261514404297" />
                  <Point X="-28.547857421875" Y="1.267172851562" />
                  <Point X="-28.531177734375" Y="1.279402587891" />
                  <Point X="-28.515927734375" Y="1.293376953125" />
                  <Point X="-28.502291015625" Y="1.308926147461" />
                  <Point X="-28.495896484375" Y="1.317073242188" />
                  <Point X="-28.48348046875" Y="1.334804077148" />
                  <Point X="-28.47801171875" Y="1.343599121094" />
                  <Point X="-28.468060546875" Y="1.361736328125" />
                  <Point X="-28.463578125" Y="1.371078125" />
                  <Point X="-28.4475546875" Y="1.409762207031" />
                  <Point X="-28.438931640625" Y="1.430582397461" />
                  <Point X="-28.435494140625" Y="1.440362915039" />
                  <Point X="-28.429705078125" Y="1.460231689453" />
                  <Point X="-28.427353515625" Y="1.470320068359" />
                  <Point X="-28.42359765625" Y="1.491634521484" />
                  <Point X="-28.422359375" Y="1.501920532227" />
                  <Point X="-28.421009765625" Y="1.522555541992" />
                  <Point X="-28.421912109375" Y="1.543201782227" />
                  <Point X="-28.425056640625" Y="1.563640258789" />
                  <Point X="-28.4271875" Y="1.57378125" />
                  <Point X="-28.4327890625" Y="1.594686889648" />
                  <Point X="-28.436013671875" Y="1.60453527832" />
                  <Point X="-28.44351171875" Y="1.623813964844" />
                  <Point X="-28.44778515625" Y="1.633244384766" />
                  <Point X="-28.467119140625" Y="1.670384765625" />
                  <Point X="-28.477525390625" Y="1.690374023438" />
                  <Point X="-28.48280078125" Y="1.699286254883" />
                  <Point X="-28.49429296875" Y="1.716485961914" />
                  <Point X="-28.500509765625" Y="1.72477331543" />
                  <Point X="-28.514421875" Y="1.741352783203" />
                  <Point X="-28.521501953125" Y="1.748912719727" />
                  <Point X="-28.536443359375" Y="1.763215698242" />
                  <Point X="-28.5443046875" Y="1.769958618164" />
                  <Point X="-28.842095703125" Y="1.998462402344" />
                  <Point X="-29.22761328125" Y="2.294280517578" />
                  <Point X="-29.11319921875" Y="2.490299560547" />
                  <Point X="-29.00228125" Y="2.680324462891" />
                  <Point X="-28.7428671875" Y="3.013768554688" />
                  <Point X="-28.726337890625" Y="3.035013427734" />
                  <Point X="-28.678828125" Y="3.007583007812" />
                  <Point X="-28.25415625" Y="2.762398925781" />
                  <Point X="-28.24491796875" Y="2.757716796875" />
                  <Point X="-28.225984375" Y="2.749385742188" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.099455078125" Y="2.726292236328" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310546875" />
                  <Point X="-27.976431640625" Y="2.734227294922" />
                  <Point X="-27.95698828125" Y="2.7413046875" />
                  <Point X="-27.9384375" Y="2.750454345703" />
                  <Point X="-27.929416015625" Y="2.755533691406" />
                  <Point X="-27.911162109375" Y="2.767163330078" />
                  <Point X="-27.90274609375" Y="2.773194091797" />
                  <Point X="-27.8866171875" Y="2.786138427734" />
                  <Point X="-27.878904296875" Y="2.793052001953" />
                  <Point X="-27.83942578125" Y="2.832528808594" />
                  <Point X="-27.8181796875" Y="2.853775390625" />
                  <Point X="-27.811265625" Y="2.861489013672" />
                  <Point X="-27.79831640625" Y="2.877623291016" />
                  <Point X="-27.79228125" Y="2.886043945312" />
                  <Point X="-27.78065234375" Y="2.904298339844" />
                  <Point X="-27.7755703125" Y="2.913326171875" />
                  <Point X="-27.766423828125" Y="2.931875" />
                  <Point X="-27.759353515625" Y="2.951299560547" />
                  <Point X="-27.754435546875" Y="2.971388427734" />
                  <Point X="-27.7525234375" Y="2.981573730469" />
                  <Point X="-27.749697265625" Y="3.003032714844" />
                  <Point X="-27.748908203125" Y="3.013365478516" />
                  <Point X="-27.74845703125" Y="3.034048828125" />
                  <Point X="-27.748794921875" Y="3.044399414062" />
                  <Point X="-27.75366015625" Y="3.100015625" />
                  <Point X="-27.756279296875" Y="3.129948974609" />
                  <Point X="-27.757744140625" Y="3.140205322266" />
                  <Point X="-27.76178125" Y="3.160499511719" />
                  <Point X="-27.764353515625" Y="3.170537353516" />
                  <Point X="-27.77086328125" Y="3.191178955078" />
                  <Point X="-27.774513671875" Y="3.200874267578" />
                  <Point X="-27.78284375" Y="3.219801513672" />
                  <Point X="-27.7875234375" Y="3.229033447266" />
                  <Point X="-27.919484375" Y="3.457595703125" />
                  <Point X="-28.05938671875" Y="3.699914550781" />
                  <Point X="-27.84153125" Y="3.866942138672" />
                  <Point X="-27.648365234375" Y="4.015041503906" />
                  <Point X="-27.239791015625" Y="4.242036621094" />
                  <Point X="-27.192525390625" Y="4.268296875" />
                  <Point X="-27.118564453125" Y="4.171908691406" />
                  <Point X="-27.1118203125" Y="4.164047363281" />
                  <Point X="-27.097517578125" Y="4.149106445313" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.065091796875" Y="4.121897460938" />
                  <Point X="-27.047892578125" Y="4.110405273438" />
                  <Point X="-27.03898046875" Y="4.105129394531" />
                  <Point X="-26.977080078125" Y="4.072905517578" />
                  <Point X="-26.943763671875" Y="4.055562255859" />
                  <Point X="-26.934326171875" Y="4.051286621094" />
                  <Point X="-26.915044921875" Y="4.0437890625" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031378173828" />
                  <Point X="-26.780818359375" Y="4.035136474609" />
                  <Point X="-26.770728515625" Y="4.037488525391" />
                  <Point X="-26.7508671875" Y="4.043277587891" />
                  <Point X="-26.741095703125" Y="4.046714111328" />
                  <Point X="-26.676623046875" Y="4.073420166016" />
                  <Point X="-26.641921875" Y="4.087793701172" />
                  <Point X="-26.63258203125" Y="4.092274169922" />
                  <Point X="-26.614447265625" Y="4.102224121094" />
                  <Point X="-26.60565234375" Y="4.107693359375" />
                  <Point X="-26.587923828125" Y="4.120107421875" />
                  <Point X="-26.579779296875" Y="4.126499023438" />
                  <Point X="-26.564228515625" Y="4.140135742188" />
                  <Point X="-26.550251953125" Y="4.15538671875" />
                  <Point X="-26.538021484375" Y="4.17206640625" />
                  <Point X="-26.532361328125" Y="4.180740234375" />
                  <Point X="-26.5215390625" Y="4.199483886719" />
                  <Point X="-26.51685546875" Y="4.208724121094" />
                  <Point X="-26.5085234375" Y="4.227659667969" />
                  <Point X="-26.504875" Y="4.237354980469" />
                  <Point X="-26.483890625" Y="4.303910644531" />
                  <Point X="-26.472595703125" Y="4.339731445312" />
                  <Point X="-26.470025390625" Y="4.349763671875" />
                  <Point X="-26.465990234375" Y="4.370048828125" />
                  <Point X="-26.464525390625" Y="4.380301757812" />
                  <Point X="-26.462638671875" Y="4.401863769531" />
                  <Point X="-26.46230078125" Y="4.412216796875" />
                  <Point X="-26.462751953125" Y="4.432899414062" />
                  <Point X="-26.463541015625" Y="4.443229003906" />
                  <Point X="-26.478544921875" Y="4.557184082031" />
                  <Point X="-26.479265625" Y="4.562655273438" />
                  <Point X="-26.18123046875" Y="4.646214355469" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.435857421875" Y="4.774290039063" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.351296875" Y="4.730583984375" />
                  <Point X="-25.22566796875" Y="4.261728027344" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.694404296875" Y="4.514070800781" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.39046875" Y="4.760778808594" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.762322265625" Y="4.638974121094" />
                  <Point X="-23.546404296875" Y="4.586844238281" />
                  <Point X="-23.28115234375" Y="4.490636230469" />
                  <Point X="-23.141744140625" Y="4.440071777344" />
                  <Point X="-22.8838125" Y="4.319445800781" />
                  <Point X="-22.749546875" Y="4.256653808594" />
                  <Point X="-22.50032421875" Y="4.111456542969" />
                  <Point X="-22.370580078125" Y="4.035865966797" />
                  <Point X="-22.18221875" Y="3.901915527344" />
                  <Point X="-22.3745390625" Y="3.568805664062" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.937619140625" Y="2.593113037109" />
                  <Point X="-22.946814453125" Y="2.573443603516" />
                  <Point X="-22.955814453125" Y="2.549569824219" />
                  <Point X="-22.958697265625" Y="2.540603027344" />
                  <Point X="-22.97265625" Y="2.488408447266" />
                  <Point X="-22.98016796875" Y="2.460316650391" />
                  <Point X="-22.9820859375" Y="2.451469726562" />
                  <Point X="-22.986353515625" Y="2.420223144531" />
                  <Point X="-22.987244140625" Y="2.383240722656" />
                  <Point X="-22.986587890625" Y="2.369578613281" />
                  <Point X="-22.98114453125" Y="2.324445068359" />
                  <Point X="-22.978216796875" Y="2.300153808594" />
                  <Point X="-22.97619921875" Y="2.289037597656" />
                  <Point X="-22.970857421875" Y="2.267115966797" />
                  <Point X="-22.967533203125" Y="2.256310546875" />
                  <Point X="-22.959265625" Y="2.234218261719" />
                  <Point X="-22.9546796875" Y="2.223885986328" />
                  <Point X="-22.94431640625" Y="2.203840820312" />
                  <Point X="-22.9385390625" Y="2.194127929688" />
                  <Point X="-22.910611328125" Y="2.152970458984" />
                  <Point X="-22.895580078125" Y="2.130819335937" />
                  <Point X="-22.89018359375" Y="2.123625976562" />
                  <Point X="-22.869529296875" Y="2.100119384766" />
                  <Point X="-22.8423984375" Y="2.075386474609" />
                  <Point X="-22.831740234375" Y="2.066981933594" />
                  <Point X="-22.790583984375" Y="2.039055053711" />
                  <Point X="-22.768431640625" Y="2.024024414062" />
                  <Point X="-22.75871875" Y="2.018245727539" />
                  <Point X="-22.738673828125" Y="2.00788269043" />
                  <Point X="-22.728341796875" Y="2.003298461914" />
                  <Point X="-22.706255859375" Y="1.995033081055" />
                  <Point X="-22.695453125" Y="1.991708007812" />
                  <Point X="-22.673529296875" Y="1.986365112305" />
                  <Point X="-22.662408203125" Y="1.984346801758" />
                  <Point X="-22.617275390625" Y="1.978904541016" />
                  <Point X="-22.592984375" Y="1.975975219727" />
                  <Point X="-22.583955078125" Y="1.975320922852" />
                  <Point X="-22.55242578125" Y="1.975497314453" />
                  <Point X="-22.5156875" Y="1.979822509766" />
                  <Point X="-22.50225390625" Y="1.982395629883" />
                  <Point X="-22.45005859375" Y="1.996353149414" />
                  <Point X="-22.421966796875" Y="2.003865356445" />
                  <Point X="-22.416005859375" Y="2.005670776367" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572753906" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.8417890625" Y="2.329350585938" />
                  <Point X="-21.059595703125" Y="2.780950683594" />
                  <Point X="-21.03301953125" Y="2.744015625" />
                  <Point X="-20.95603515625" Y="2.637028808594" />
                  <Point X="-20.86311328125" Y="2.483471923828" />
                  <Point X="-21.089556640625" Y="2.309715087891" />
                  <Point X="-21.827046875" Y="1.743819824219" />
                  <Point X="-21.831859375" Y="1.739869506836" />
                  <Point X="-21.847876953125" Y="1.725219482422" />
                  <Point X="-21.865330078125" Y="1.706604614258" />
                  <Point X="-21.87142578125" Y="1.699421386719" />
                  <Point X="-21.908990234375" Y="1.650415649414" />
                  <Point X="-21.92920703125" Y="1.624040283203" />
                  <Point X="-21.934365234375" Y="1.616599365234" />
                  <Point X="-21.950263671875" Y="1.589366333008" />
                  <Point X="-21.965236328125" Y="1.555545288086" />
                  <Point X="-21.96985546875" Y="1.542677001953" />
                  <Point X="-21.983849609375" Y="1.492641723633" />
                  <Point X="-21.991380859375" Y="1.465712402344" />
                  <Point X="-21.993771484375" Y="1.454669921875" />
                  <Point X="-21.9972265625" Y="1.432370483398" />
                  <Point X="-21.998291015625" Y="1.42111328125" />
                  <Point X="-21.999107421875" Y="1.397543579102" />
                  <Point X="-21.998826171875" Y="1.386239257812" />
                  <Point X="-21.996921875" Y="1.363750732422" />
                  <Point X="-21.995298828125" Y="1.352566650391" />
                  <Point X="-21.983810546875" Y="1.296896118164" />
                  <Point X="-21.97762890625" Y="1.26693371582" />
                  <Point X="-21.975400390625" Y="1.258233032227" />
                  <Point X="-21.96531640625" Y="1.228614746094" />
                  <Point X="-21.949712890625" Y="1.195373901367" />
                  <Point X="-21.943080078125" Y="1.18352722168" />
                  <Point X="-21.911837890625" Y="1.136039916992" />
                  <Point X="-21.8950234375" Y="1.110481689453" />
                  <Point X="-21.88826171875" Y="1.101426391602" />
                  <Point X="-21.873708984375" Y="1.084182006836" />
                  <Point X="-21.86591796875" Y="1.075993041992" />
                  <Point X="-21.848673828125" Y="1.059902709961" />
                  <Point X="-21.83996484375" Y="1.052696166992" />
                  <Point X="-21.82175390625" Y="1.039369628906" />
                  <Point X="-21.812251953125" Y="1.033249267578" />
                  <Point X="-21.7669765625" Y="1.00776348877" />
                  <Point X="-21.742609375" Y="0.994046813965" />
                  <Point X="-21.734517578125" Y="0.989986877441" />
                  <Point X="-21.705318359375" Y="0.978083496094" />
                  <Point X="-21.66972265625" Y="0.968021179199" />
                  <Point X="-21.656328125" Y="0.965257629395" />
                  <Point X="-21.59511328125" Y="0.957167236328" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.555966796875" Y="0.952199829102" />
                  <Point X="-21.53428125" Y="0.951222900391" />
                  <Point X="-21.50878125" Y="0.952032287598" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-21.00336328125" Y="1.018101135254" />
                  <Point X="-20.295296875" Y="1.111319824219" />
                  <Point X="-20.280375" Y="1.050026123047" />
                  <Point X="-20.2473125" Y="0.914208007812" />
                  <Point X="-20.216126953125" Y="0.713921203613" />
                  <Point X="-20.462828125" Y="0.647817565918" />
                  <Point X="-21.3080078125" Y="0.42135269165" />
                  <Point X="-21.31396875" Y="0.419543945312" />
                  <Point X="-21.334375" Y="0.412137023926" />
                  <Point X="-21.357619140625" Y="0.401618682861" />
                  <Point X="-21.365994140625" Y="0.397316192627" />
                  <Point X="-21.426134765625" Y="0.362553436279" />
                  <Point X="-21.45850390625" Y="0.343843902588" />
                  <Point X="-21.46611328125" Y="0.338948425293" />
                  <Point X="-21.49122265625" Y="0.319873413086" />
                  <Point X="-21.518005859375" Y="0.294351287842" />
                  <Point X="-21.527203125" Y="0.284225921631" />
                  <Point X="-21.563287109375" Y="0.238245407104" />
                  <Point X="-21.582708984375" Y="0.213498199463" />
                  <Point X="-21.58914453125" Y="0.204208084106" />
                  <Point X="-21.60087109375" Y="0.184923812866" />
                  <Point X="-21.606162109375" Y="0.174930114746" />
                  <Point X="-21.615935546875" Y="0.15346421814" />
                  <Point X="-21.619998046875" Y="0.142920303345" />
                  <Point X="-21.626841796875" Y="0.121425422668" />
                  <Point X="-21.629623046875" Y="0.11047429657" />
                  <Point X="-21.64165234375" Y="0.047666999817" />
                  <Point X="-21.648125" Y="0.013863613129" />
                  <Point X="-21.649396484375" Y="0.004965411663" />
                  <Point X="-21.6514140625" Y="-0.026263528824" />
                  <Point X="-21.64971875" Y="-0.062939945221" />
                  <Point X="-21.648125" Y="-0.076420783997" />
                  <Point X="-21.63609765625" Y="-0.139228088379" />
                  <Point X="-21.629623046875" Y="-0.173031463623" />
                  <Point X="-21.626841796875" Y="-0.183984527588" />
                  <Point X="-21.61999609375" Y="-0.205486251831" />
                  <Point X="-21.615931640625" Y="-0.216034912109" />
                  <Point X="-21.606158203125" Y="-0.237498291016" />
                  <Point X="-21.600869140625" Y="-0.24749005127" />
                  <Point X="-21.58914453125" Y="-0.266770019531" />
                  <Point X="-21.582708984375" Y="-0.276058197021" />
                  <Point X="-21.546625" Y="-0.322038696289" />
                  <Point X="-21.527203125" Y="-0.346785919189" />
                  <Point X="-21.52128125" Y="-0.353632965088" />
                  <Point X="-21.49885546875" Y="-0.375810028076" />
                  <Point X="-21.4698203125" Y="-0.398726776123" />
                  <Point X="-21.45850390625" Y="-0.406404052734" />
                  <Point X="-21.39836328125" Y="-0.441166809082" />
                  <Point X="-21.365994140625" Y="-0.459876495361" />
                  <Point X="-21.360501953125" Y="-0.462815063477" />
                  <Point X="-21.340841796875" Y="-0.472016296387" />
                  <Point X="-21.31697265625" Y="-0.481027313232" />
                  <Point X="-21.3080078125" Y="-0.483912841797" />
                  <Point X="-20.853123046875" Y="-0.605798461914" />
                  <Point X="-20.21512109375" Y="-0.776750854492" />
                  <Point X="-20.21992578125" Y="-0.808621459961" />
                  <Point X="-20.238380859375" Y="-0.931037109375" />
                  <Point X="-20.272197265625" Y="-1.079219726563" />
                  <Point X="-20.583046875" Y="-1.038295532227" />
                  <Point X="-21.563216796875" Y="-0.909253601074" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042297363" />
                  <Point X="-21.63327734375" Y="-0.910840881348" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.7635546875" Y="-0.938332092285" />
                  <Point X="-21.82708203125" Y="-0.952140075684" />
                  <Point X="-21.842125" Y="-0.956742736816" />
                  <Point X="-21.8712421875" Y="-0.968366088867" />
                  <Point X="-21.885318359375" Y="-0.975387084961" />
                  <Point X="-21.913146484375" Y="-0.992279541016" />
                  <Point X="-21.92587109375" Y="-1.001528198242" />
                  <Point X="-21.949623046875" Y="-1.021999938965" />
                  <Point X="-21.9606484375" Y="-1.03322277832" />
                  <Point X="-22.031994140625" Y="-1.119029052734" />
                  <Point X="-22.070392578125" Y="-1.16521081543" />
                  <Point X="-22.078673828125" Y="-1.176848022461" />
                  <Point X="-22.093396484375" Y="-1.201234130859" />
                  <Point X="-22.099837890625" Y="-1.213983032227" />
                  <Point X="-22.1111796875" Y="-1.241367797852" />
                  <Point X="-22.11563671875" Y="-1.254934448242" />
                  <Point X="-22.122466796875" Y="-1.282581176758" />
                  <Point X="-22.12483984375" Y="-1.296661376953" />
                  <Point X="-22.135064453125" Y="-1.407784423828" />
                  <Point X="-22.140568359375" Y="-1.467592041016" />
                  <Point X="-22.140708984375" Y="-1.483315795898" />
                  <Point X="-22.138392578125" Y="-1.514579956055" />
                  <Point X="-22.135935546875" Y="-1.530120605469" />
                  <Point X="-22.128205078125" Y="-1.56174230957" />
                  <Point X="-22.12321484375" Y="-1.576666137695" />
                  <Point X="-22.110841796875" Y="-1.60548059082" />
                  <Point X="-22.103458984375" Y="-1.619371337891" />
                  <Point X="-22.03813671875" Y="-1.72097668457" />
                  <Point X="-22.002978515625" Y="-1.775661987305" />
                  <Point X="-21.9982578125" Y="-1.782353393555" />
                  <Point X="-21.980197265625" Y="-1.804458618164" />
                  <Point X="-21.95650390625" Y="-1.828124023438" />
                  <Point X="-21.947201171875" Y="-1.836277587891" />
                  <Point X="-21.52506640625" Y="-2.160193359375" />
                  <Point X="-20.912828125" Y="-2.62998046875" />
                  <Point X="-20.95452734375" Y="-2.697453613281" />
                  <Point X="-20.998724609375" Y="-2.760251953125" />
                  <Point X="-21.2785703125" Y="-2.598682373047" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.184970703125" Y="-2.079513916016" />
                  <Point X="-22.216876953125" Y="-2.069326416016" />
                  <Point X="-22.228888671875" Y="-2.066337646484" />
                  <Point X="-22.369369140625" Y="-2.040966796875" />
                  <Point X="-22.444978515625" Y="-2.027311889648" />
                  <Point X="-22.460638671875" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.55515625" Y="-2.03513659668" />
                  <Point X="-22.584931640625" Y="-2.044960571289" />
                  <Point X="-22.59941015625" Y="-2.051109130859" />
                  <Point X="-22.716115234375" Y="-2.112530517578" />
                  <Point X="-22.778927734375" Y="-2.145588134766" />
                  <Point X="-22.791029296875" Y="-2.153169433594" />
                  <Point X="-22.8139609375" Y="-2.1700625" />
                  <Point X="-22.824791015625" Y="-2.179374267578" />
                  <Point X="-22.84575" Y="-2.200333496094" />
                  <Point X="-22.855060546875" Y="-2.211161621094" />
                  <Point X="-22.871953125" Y="-2.234091796875" />
                  <Point X="-22.87953515625" Y="-2.246193847656" />
                  <Point X="-22.94095703125" Y="-2.362899658203" />
                  <Point X="-22.974015625" Y="-2.425711914062" />
                  <Point X="-22.9801640625" Y="-2.440193359375" />
                  <Point X="-22.98998828125" Y="-2.469972167969" />
                  <Point X="-22.9936640625" Y="-2.48526953125" />
                  <Point X="-22.99862109375" Y="-2.517443603516" />
                  <Point X="-22.999720703125" Y="-2.533134521484" />
                  <Point X="-22.99931640625" Y="-2.564484375" />
                  <Point X="-22.9978125" Y="-2.580143310547" />
                  <Point X="-22.97244140625" Y="-2.720625" />
                  <Point X="-22.95878515625" Y="-2.796233642578" />
                  <Point X="-22.95698046875" Y="-2.804228271484" />
                  <Point X="-22.94876171875" Y="-2.831543701172" />
                  <Point X="-22.935927734375" Y="-2.862480712891" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.659185546875" Y="-3.343421630859" />
                  <Point X="-22.264103515625" Y="-4.027722900391" />
                  <Point X="-22.2762421875" Y="-4.036082275391" />
                  <Point X="-22.499923828125" Y="-3.744577636719" />
                  <Point X="-23.16608203125" Y="-2.876422363281" />
                  <Point X="-23.171345703125" Y="-2.870143066406" />
                  <Point X="-23.191169921875" Y="-2.849624023438" />
                  <Point X="-23.216748046875" Y="-2.828003662109" />
                  <Point X="-23.22669921875" Y="-2.820646728516" />
                  <Point X="-23.36525390625" Y="-2.731569824219" />
                  <Point X="-23.43982421875" Y="-2.683627929688" />
                  <Point X="-23.453716796875" Y="-2.676245605469" />
                  <Point X="-23.48252734375" Y="-2.663874511719" />
                  <Point X="-23.4974453125" Y="-2.658885742188" />
                  <Point X="-23.52906640625" Y="-2.651154052734" />
                  <Point X="-23.54460546875" Y="-2.648695800781" />
                  <Point X="-23.575875" Y="-2.646376953125" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.74313671875" Y="-2.660460449219" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670339111328" />
                  <Point X="-23.866421875" Y="-2.677170410156" />
                  <Point X="-23.87998828125" Y="-2.681627929688" />
                  <Point X="-23.907373046875" Y="-2.692970703125" />
                  <Point X="-23.92012109375" Y="-2.699412841797" />
                  <Point X="-23.94450390625" Y="-2.714134033203" />
                  <Point X="-23.956138671875" Y="-2.722413085938" />
                  <Point X="-24.073146484375" Y="-2.819701904297" />
                  <Point X="-24.136123046875" Y="-2.872063476562" />
                  <Point X="-24.147345703125" Y="-2.883087402344" />
                  <Point X="-24.16781640625" Y="-2.906836914062" />
                  <Point X="-24.177064453125" Y="-2.9195625" />
                  <Point X="-24.19395703125" Y="-2.947389160156" />
                  <Point X="-24.20098046875" Y="-2.961466552734" />
                  <Point X="-24.21260546875" Y="-2.990587646484" />
                  <Point X="-24.21720703125" Y="-3.005631347656" />
                  <Point X="-24.25219140625" Y="-3.16658984375" />
                  <Point X="-24.271021484375" Y="-3.253219238281" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323170166016" />
                  <Point X="-24.2744453125" Y="-3.335520507812" />
                  <Point X="-24.1975703125" Y="-3.9194375" />
                  <Point X="-24.16691015625" Y="-4.1523203125" />
                  <Point X="-24.344931640625" Y="-3.487937011719" />
                  <Point X="-24.347392578125" Y="-3.480122314453" />
                  <Point X="-24.357853515625" Y="-3.453578857422" />
                  <Point X="-24.3732109375" Y="-3.423815429688" />
                  <Point X="-24.37958984375" Y="-3.413209228516" />
                  <Point X="-24.48603125" Y="-3.259848876953" />
                  <Point X="-24.543318359375" Y="-3.177308837891" />
                  <Point X="-24.553330078125" Y="-3.165170654297" />
                  <Point X="-24.57521484375" Y="-3.142715576172" />
                  <Point X="-24.587087890625" Y="-3.132398681641" />
                  <Point X="-24.61334375" Y="-3.113154541016" />
                  <Point X="-24.6267578125" Y="-3.104937744141" />
                  <Point X="-24.6547578125" Y="-3.090829833984" />
                  <Point X="-24.66934375" Y="-3.084938720703" />
                  <Point X="-24.834052734375" Y="-3.033818603516" />
                  <Point X="-24.922703125" Y="-3.006305419922" />
                  <Point X="-24.936623046875" Y="-3.003109375" />
                  <Point X="-24.964783203125" Y="-2.99883984375" />
                  <Point X="-24.9790234375" Y="-2.997766357422" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.998840087891" />
                  <Point X="-25.051064453125" Y="-3.003109619141" />
                  <Point X="-25.064984375" Y="-3.006305664062" />
                  <Point X="-25.229693359375" Y="-3.057425537109" />
                  <Point X="-25.31834375" Y="-3.084938964844" />
                  <Point X="-25.3329296875" Y="-3.090830078125" />
                  <Point X="-25.3609296875" Y="-3.104937988281" />
                  <Point X="-25.37434375" Y="-3.113154785156" />
                  <Point X="-25.400599609375" Y="-3.132398925781" />
                  <Point X="-25.412474609375" Y="-3.142717041016" />
                  <Point X="-25.434359375" Y="-3.165172851562" />
                  <Point X="-25.444369140625" Y="-3.177310546875" />
                  <Point X="-25.55080859375" Y="-3.330670654297" />
                  <Point X="-25.608095703125" Y="-3.4132109375" />
                  <Point X="-25.61246875" Y="-3.420131591797" />
                  <Point X="-25.625974609375" Y="-3.445261230469" />
                  <Point X="-25.63877734375" Y="-3.476214111328" />
                  <Point X="-25.64275390625" Y="-3.487937011719" />
                  <Point X="-25.7829140625" Y="-4.011024414062" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.932769409462" Y="-3.748250737224" />
                  <Point X="-27.471791168183" Y="-4.121543556662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.968425808314" Y="-2.787350562798" />
                  <Point X="-28.729379709763" Y="-2.980926276602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.884677490798" Y="-3.664952646324" />
                  <Point X="-27.413938908009" Y="-4.046149234476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.124313705636" Y="-2.538872873818" />
                  <Point X="-28.641254065948" Y="-2.930046857119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.836585572135" Y="-3.581654555423" />
                  <Point X="-27.343083102033" Y="-3.981284976054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.135066338235" Y="-2.407923404865" />
                  <Point X="-28.553128422133" Y="-2.879167437635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.788493467063" Y="-3.498356615474" />
                  <Point X="-27.260373736359" Y="-3.926019541013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.057556080376" Y="-2.348447815328" />
                  <Point X="-28.465002778318" Y="-2.828288018152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.740401305817" Y="-3.415058721012" />
                  <Point X="-27.17766396781" Y="-3.870754432213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.13871039578" Y="-4.712082446074" />
                  <Point X="-26.102696506188" Y="-4.741245918839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.980045822517" Y="-2.288972225791" />
                  <Point X="-28.376877134503" Y="-2.777408598669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.692309144572" Y="-3.331760826551" />
                  <Point X="-27.094914404976" Y="-3.81552154819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.124167329791" Y="-4.601617029945" />
                  <Point X="-25.973793545738" Y="-4.723387319283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.902535564659" Y="-2.229496636254" />
                  <Point X="-28.288751490687" Y="-2.726529179185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.644216983326" Y="-3.24846293209" />
                  <Point X="-26.989273462308" Y="-3.778825738055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.127516495333" Y="-4.476662770406" />
                  <Point X="-25.946878557517" Y="-4.622940488238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.825025418248" Y="-2.170020956468" />
                  <Point X="-28.200625846872" Y="-2.675649759702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.596124822081" Y="-3.165165037628" />
                  <Point X="-26.83275860025" Y="-3.783326815547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.156500982465" Y="-4.330949436756" />
                  <Point X="-25.919963569297" Y="-4.522493657193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.747515284019" Y="-2.110545266818" />
                  <Point X="-28.112500154745" Y="-2.624770379341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.548032660835" Y="-3.081867143167" />
                  <Point X="-26.668507728689" Y="-3.794092390016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.186405961174" Y="-4.184490703724" />
                  <Point X="-25.893048581076" Y="-4.422046826148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.673172231203" Y="-1.238720892012" />
                  <Point X="-29.620500994426" Y="-1.281373218563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.67000514979" Y="-2.051069577167" />
                  <Point X="-28.024374342886" Y="-2.573891095937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.49994049959" Y="-2.998569248705" />
                  <Point X="-25.866133592856" Y="-4.321599995102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.712539505912" Y="-1.084599742763" />
                  <Point X="-29.490654578061" Y="-1.264278614543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.59249501556" Y="-1.991593887517" />
                  <Point X="-27.936248531026" Y="-2.523011812534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.451848338344" Y="-2.915271354244" />
                  <Point X="-25.839218604635" Y="-4.221153164057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.746358087608" Y="-0.93497183652" />
                  <Point X="-29.360808161696" Y="-1.247184010523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.514984881331" Y="-1.932118197867" />
                  <Point X="-27.848122719166" Y="-2.47213252913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.403756177099" Y="-2.831973459782" />
                  <Point X="-25.812303616414" Y="-4.120706333012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.766132367956" Y="-0.796716781267" />
                  <Point X="-29.230961745331" Y="-1.230089406503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.437474747102" Y="-1.872642508217" />
                  <Point X="-27.759996907307" Y="-2.421253245726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.355664015853" Y="-2.748675565321" />
                  <Point X="-25.785388628194" Y="-4.020259501967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.785906648303" Y="-0.658461726014" />
                  <Point X="-29.101115328966" Y="-1.212994802483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.359964612872" Y="-1.813166818567" />
                  <Point X="-27.670504630092" Y="-2.371480504149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.317865937372" Y="-2.657041687001" />
                  <Point X="-25.75847399336" Y="-3.919812384755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.676149366351" Y="-0.625099261705" />
                  <Point X="-28.971268912601" Y="-1.195900198464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.282454478643" Y="-1.753691128917" />
                  <Point X="-27.548002334468" Y="-2.348438748416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.322798261392" Y="-2.530805411003" />
                  <Point X="-25.731559394306" Y="-3.819365238569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.562724175864" Y="-0.594707011164" />
                  <Point X="-28.841422496236" Y="-1.178805594444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.204944344414" Y="-1.694215439267" />
                  <Point X="-25.704644795252" Y="-3.718918092383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.449298975061" Y="-0.564314768977" />
                  <Point X="-28.711576099055" Y="-1.161710974889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.127434210185" Y="-1.634739749616" />
                  <Point X="-25.677730196198" Y="-3.618470946197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.335873734977" Y="-0.533922558598" />
                  <Point X="-28.581729707042" Y="-1.144616351149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.049924075955" Y="-1.575264059966" />
                  <Point X="-25.650815597144" Y="-3.518023800011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.222448494894" Y="-0.503530348219" />
                  <Point X="-28.451883315028" Y="-1.12752172741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.988583495562" Y="-1.502694523796" />
                  <Point X="-25.614978724438" Y="-3.424801768567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.10902325481" Y="-0.47313813784" />
                  <Point X="-28.322036923015" Y="-1.11042710367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.953329818859" Y="-1.409000229542" />
                  <Point X="-25.561407049275" Y="-3.345941096986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.995598014727" Y="-0.442745927461" />
                  <Point X="-28.180798414977" Y="-1.102557633592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.958683837171" Y="-1.282422472239" />
                  <Point X="-25.507091774365" Y="-3.267682580607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.882172774643" Y="-0.412353717082" />
                  <Point X="-25.452776510764" Y="-3.18942405507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.76874753456" Y="-0.381961506703" />
                  <Point X="-25.385713660657" Y="-3.121488321547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.181920605013" Y="-4.096300717279" />
                  <Point X="-24.173374266391" Y="-4.103221405837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.655322294476" Y="-0.351569296324" />
                  <Point X="-25.29054575539" Y="-3.076311612945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.223752039389" Y="-3.940184130876" />
                  <Point X="-24.191388584513" Y="-3.966391539893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.773024121772" Y="0.675769956053" />
                  <Point X="-29.611954480744" Y="0.545338332516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.541897054393" Y="-0.321177085945" />
                  <Point X="-25.181415146655" Y="-3.042441678672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.265583473765" Y="-3.784067544473" />
                  <Point X="-24.209402813854" Y="-3.829561745842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.75687140507" Y="0.784931902734" />
                  <Point X="-29.386346518428" Y="0.48488676563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.440044231029" Y="-0.281413717281" />
                  <Point X="-25.072284865234" Y="-3.008571479345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.30741490814" Y="-3.62795095807" />
                  <Point X="-24.227416996814" Y="-3.692731989351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.740717963463" Y="0.8940932624" />
                  <Point X="-29.160738556111" Y="0.424435198744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.365374108751" Y="-0.219638231299" />
                  <Point X="-24.924690097928" Y="-3.005849206533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.351321021336" Y="-3.470154329884" />
                  <Point X="-24.245431179773" Y="-3.55590223286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.719951353717" Y="0.999518952164" />
                  <Point X="-28.935130678891" Y="0.363983700769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.315636895182" Y="-0.137672473943" />
                  <Point X="-24.680243888575" Y="-3.081555685082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.519346491344" Y="-3.211847828342" />
                  <Point X="-24.263445362732" Y="-3.419072476369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.69278669102" Y="1.099763600804" />
                  <Point X="-28.709522924538" Y="0.303532302288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.295646484375" Y="-0.031618230671" />
                  <Point X="-24.275049268566" Y="-3.287433659941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.665622028323" Y="1.200008249445" />
                  <Point X="-28.477667442893" Y="0.238021594003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.345639716722" Y="0.131107649411" />
                  <Point X="-24.255354145423" Y="-3.181140297435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.638457365626" Y="1.300252898086" />
                  <Point X="-24.232761110944" Y="-3.077193617258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.485660302748" Y="1.298762435008" />
                  <Point X="-24.206778773672" Y="-2.975991540366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.30539707864" Y="1.275030313113" />
                  <Point X="-24.15688395784" Y="-2.894153406806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.125133848588" Y="1.251298186405" />
                  <Point X="-24.085343811889" Y="-2.82984331597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.944870618537" Y="1.227566059697" />
                  <Point X="-24.010862787299" Y="-2.767914701698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.765766381148" Y="1.204772466742" />
                  <Point X="-23.934086683616" Y="-2.707844605832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.640346017867" Y="1.225451217879" />
                  <Point X="-23.831052781292" Y="-2.669037656052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.495086261109" Y="-3.75088201298" />
                  <Point X="-22.361466846894" Y="-3.859084881135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.544212085784" Y="1.26984565339" />
                  <Point X="-23.696040155869" Y="-2.656126565639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.74281957375" Y="-3.428029373153" />
                  <Point X="-22.494012502852" Y="-3.629509366511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.480128867411" Y="1.340194245115" />
                  <Point X="-23.555233130699" Y="-2.647907687624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.990552742331" Y="-3.105176849983" />
                  <Point X="-22.62655815881" Y="-3.399933851887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.439398987329" Y="1.429453997311" />
                  <Point X="-22.759103351056" Y="-3.17035871277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.421655381227" Y="1.537327667158" />
                  <Point X="-22.891648391882" Y="-2.94078369627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.220502318903" Y="2.306463321015" />
                  <Point X="-28.805037768474" Y="1.970026761719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.497932784842" Y="1.721338049459" />
                  <Point X="-22.965567450897" Y="-2.758683063771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.172051716599" Y="2.38947095563" />
                  <Point X="-22.991427012294" Y="-2.615500245087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.123601114295" Y="2.472478590246" />
                  <Point X="-22.994510186957" Y="-2.490761380713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.075149902334" Y="2.555485731171" />
                  <Point X="-22.959034319425" Y="-2.397247013044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.026698523703" Y="2.638492737128" />
                  <Point X="-22.913923793905" Y="-2.31153463758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.973339416693" Y="2.717525543006" />
                  <Point X="-22.866964123529" Y="-2.227319670095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.914994338812" Y="2.792520789282" />
                  <Point X="-22.799693558524" Y="-2.15955214078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.856649260931" Y="2.867516035559" />
                  <Point X="-22.710471751679" Y="-2.109560376615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.79830418305" Y="2.942511281835" />
                  <Point X="-22.618978593695" Y="-2.061407916338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.739958982948" Y="3.017506429139" />
                  <Point X="-22.510575616517" Y="-2.026948757647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.323189160158" Y="2.802255039886" />
                  <Point X="-22.334540749648" Y="-2.047256823364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.075879063603" Y="2.724229431208" />
                  <Point X="-22.059179480677" Y="-2.147997823577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.950164795217" Y="2.744670182684" />
                  <Point X="-21.533257981683" Y="-2.451638497417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.869977755442" Y="2.801978156965" />
                  <Point X="-21.007333486554" Y="-2.755281597478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.803877975177" Y="2.870693769068" />
                  <Point X="-20.945609009823" Y="-2.683022934432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.758213238378" Y="2.955957353088" />
                  <Point X="-22.09836148885" Y="-1.627300223931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.75125908662" Y="3.07256815079" />
                  <Point X="-22.140597550303" Y="-1.470855976981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.783347241399" Y="3.220794784944" />
                  <Point X="-22.130381280425" Y="-1.356886790447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.914803397068" Y="3.449488039631" />
                  <Point X="-22.113469971841" Y="-1.248339139359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.047347890513" Y="3.67906261287" />
                  <Point X="-22.068290473849" Y="-1.162682616701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.988888203499" Y="3.7539650505" />
                  <Point X="-22.007548323924" Y="-1.089628481092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.911346700233" Y="3.813415338005" />
                  <Point X="-21.945012926097" Y="-1.018026489002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.833805220607" Y="3.872865644653" />
                  <Point X="-21.860618824691" Y="-0.964125326057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.756263954591" Y="3.93231612428" />
                  <Point X="-21.74617831825" Y="-0.934555262164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.678722688575" Y="3.991766603906" />
                  <Point X="-21.625329627224" Y="-0.910174443829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.593885734238" Y="4.045309151619" />
                  <Point X="-21.458482912571" Y="-0.923042090586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.504354828407" Y="4.095050612359" />
                  <Point X="-21.278219684716" Y="-0.946774215516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.414823922575" Y="4.144792073099" />
                  <Point X="-21.097956456861" Y="-0.970506340445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.325293016743" Y="4.194533533839" />
                  <Point X="-20.917693229005" Y="-0.994238465375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.235762140957" Y="4.244275018909" />
                  <Point X="-20.73743000115" Y="-1.017970590305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.819773614763" Y="4.029656311165" />
                  <Point X="-21.629554869781" Y="-0.173299957311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.215305887039" Y="-0.508752169303" />
                  <Point X="-20.557166769477" Y="-1.041702718325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.707213346141" Y="4.060748961623" />
                  <Point X="-21.651073447549" Y="-0.033632397858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.989698453966" Y="-0.569203307616" />
                  <Point X="-20.376903515035" Y="-1.065434864784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.610503201526" Y="4.104676789425" />
                  <Point X="-21.636128579678" Y="0.076507645522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.764090808383" Y="-0.629654618016" />
                  <Point X="-20.262328244375" Y="-1.035973930804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.539789084746" Y="4.169655785495" />
                  <Point X="-21.606310698737" Y="0.174603760392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.538482836809" Y="-0.690106192398" />
                  <Point X="-20.238782818136" Y="-0.932798482267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.498276895991" Y="4.258282036618" />
                  <Point X="-21.55188588522" Y="0.252773574156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.312874865236" Y="-0.75055776678" />
                  <Point X="-20.22223736258" Y="-0.823954569239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.468674858724" Y="4.356552938249" />
                  <Point X="-21.487388087658" Y="0.322786446274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.468170000545" Y="4.478386270917" />
                  <Point X="-21.402447632166" Y="0.376245180404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.436190643043" Y="4.57473205658" />
                  <Point X="-21.307396539059" Y="0.421516481629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.324057362475" Y="4.606170475145" />
                  <Point X="-21.193971248326" Y="0.451908650992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.211924081906" Y="4.637608893711" />
                  <Point X="-21.939193732841" Y="1.17762007889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.686065791033" Y="0.972641113259" />
                  <Point X="-21.080545957592" Y="0.482300820355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.099790518041" Y="4.669047082868" />
                  <Point X="-21.993497223425" Y="1.343836337272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.509626390057" Y="0.952005462282" />
                  <Point X="-20.967120666858" Y="0.512692989718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.987656847406" Y="4.700485185563" />
                  <Point X="-22.966742134423" Y="2.254196685347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.6284483971" Y="1.980251818332" />
                  <Point X="-21.991629107427" Y="1.464565725525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.37919019878" Y="0.968622475994" />
                  <Point X="-20.853695376124" Y="0.543085159081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.865715566625" Y="4.72398124216" />
                  <Point X="-25.158100090506" Y="4.150965527957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.124455863594" Y="4.123720970195" />
                  <Point X="-22.987012640169" Y="2.392853576004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.485629164447" Y="1.986841242856" />
                  <Point X="-21.962013352746" Y="1.562825519012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.249343822707" Y="0.985717112643" />
                  <Point X="-20.740270085391" Y="0.573477328445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.733821370112" Y="4.739417586512" />
                  <Point X="-25.2483197673" Y="4.346266140465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.935777752202" Y="4.093174606935" />
                  <Point X="-22.969345199776" Y="2.500788923626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.377279348324" Y="2.02134345052" />
                  <Point X="-21.912908700864" Y="1.645303514723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.119497446635" Y="1.002811749291" />
                  <Point X="-20.626844794657" Y="0.603869497808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.601927173599" Y="4.754853930864" />
                  <Point X="-25.290150762585" Y="4.502382371298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.841978981558" Y="4.139460018895" />
                  <Point X="-22.936042394347" Y="2.596063002289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.288318800805" Y="2.071546778315" />
                  <Point X="-21.85338525" Y="1.719344533372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.989651065969" Y="1.019906382219" />
                  <Point X="-20.513419503923" Y="0.634261667171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.470032977085" Y="4.770290275217" />
                  <Point X="-25.331981757869" Y="4.658498602132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.782854888992" Y="4.213824431517" />
                  <Point X="-22.888024395525" Y="2.679420952296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.200193080557" Y="2.122426135904" />
                  <Point X="-21.778579436582" Y="1.781010138836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.859804646403" Y="1.037000983647" />
                  <Point X="-20.399994297628" Y="0.664653904911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.749366725516" Y="4.308948410193" />
                  <Point X="-22.839932376702" Y="2.76271896209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.112067360309" Y="2.173305493493" />
                  <Point X="-21.701069197071" Y="1.840485743231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.729958226836" Y="1.054095585074" />
                  <Point X="-20.286569159319" Y="0.695046197705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.722451997452" Y="4.409395451909" />
                  <Point X="-22.791840357879" Y="2.846016971883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.023941640061" Y="2.224184851082" />
                  <Point X="-21.623558957561" Y="1.899961347626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.600111807269" Y="1.071190186501" />
                  <Point X="-20.22438049859" Y="0.766928971961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.695537269388" Y="4.509842493625" />
                  <Point X="-22.743748339056" Y="2.929314981676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.935815919813" Y="2.275064208672" />
                  <Point X="-21.54604871805" Y="1.959436952021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.470265387702" Y="1.088284787928" />
                  <Point X="-20.24616029559" Y="0.906808062578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.668622357604" Y="4.610289386568" />
                  <Point X="-22.695656320234" Y="3.012612991469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.847690199565" Y="2.325943566261" />
                  <Point X="-21.46853847854" Y="2.018912556416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.340418968135" Y="1.105379389355" />
                  <Point X="-20.282415907258" Y="1.05840943678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.641707437747" Y="4.710736272972" />
                  <Point X="-22.647564301411" Y="3.095911001262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.759564555193" Y="2.376822985293" />
                  <Point X="-21.39102823903" Y="2.078388160812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.576623376059" Y="4.780274397761" />
                  <Point X="-22.599472282588" Y="3.179209011055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.671438916266" Y="2.427702408735" />
                  <Point X="-21.313517999519" Y="2.137863765207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.403244039732" Y="4.762116738278" />
                  <Point X="-22.551380263766" Y="3.262507020848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.58331327734" Y="2.478581832177" />
                  <Point X="-21.236007760009" Y="2.197339369602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.229865120129" Y="4.74395941625" />
                  <Point X="-22.503288244943" Y="3.345805030641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.495187638413" Y="2.529461255619" />
                  <Point X="-21.158497520498" Y="2.256814973997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.028672890106" Y="4.703279319535" />
                  <Point X="-22.45519622612" Y="3.429103040434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.407061999487" Y="2.580340679062" />
                  <Point X="-21.080987305231" Y="2.316290598024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.813591643886" Y="4.651352119266" />
                  <Point X="-22.407104207297" Y="3.512401050227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.31893636056" Y="2.631220102504" />
                  <Point X="-21.003477284998" Y="2.375766379987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.598509500499" Y="4.599424192486" />
                  <Point X="-22.359012206004" Y="3.595699074215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.230810721634" Y="2.682099525946" />
                  <Point X="-20.925967264765" Y="2.435242161949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.339219467154" Y="4.511697422276" />
                  <Point X="-22.310920241474" Y="3.678997127974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.142685082707" Y="2.732978949388" />
                  <Point X="-20.890540334356" Y="2.528796158118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.042493357565" Y="4.393655515259" />
                  <Point X="-22.262828276944" Y="3.762295181732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.652618680828" Y="4.20018338585" />
                  <Point X="-22.214736312414" Y="3.845593235491" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.0001640625" Y="0.001626220703" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.2680390625" Y="-4.509014648438" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.64212109375" Y="-3.368183837891" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399658203" />
                  <Point X="-24.890373046875" Y="-3.215279541016" />
                  <Point X="-24.9790234375" Y="-3.187766357422" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.173373046875" Y="-3.238886474609" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285644042969" />
                  <Point X="-25.39471875" Y="-3.439004150391" />
                  <Point X="-25.452005859375" Y="-3.521544433594" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.59938671875" Y="-4.060199707031" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.996841796875" Y="-4.958136230469" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.291021484375" Y="-4.88898046875" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.340498046875" Y="-4.789153808594" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.534756835938" />
                  <Point X="-26.34903125" Y="-4.336935058594" />
                  <Point X="-26.3702109375" Y="-4.230465332031" />
                  <Point X="-26.386283203125" Y="-4.202628417969" />
                  <Point X="-26.53792578125" Y="-4.069639892578" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.850505859375" Y="-3.972571289062" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791259766" />
                  <Point X="-27.157583984375" Y="-4.085848632812" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.338421875" Y="-4.259844238281" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.704267578125" Y="-4.261456542969" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.120001953125" Y="-3.964210693359" />
                  <Point X="-28.228580078125" Y="-3.880608154297" />
                  <Point X="-27.98999609375" Y="-3.467366210938" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597592529297" />
                  <Point X="-27.51398046875" Y="-2.568763183594" />
                  <Point X="-27.531322265625" Y="-2.551420654297" />
                  <Point X="-27.56015234375" Y="-2.53719921875" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.0428359375" Y="-2.803942871094" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-29.041958984375" Y="-3.004448974609" />
                  <Point X="-29.161697265625" Y="-2.847136962891" />
                  <Point X="-29.35108984375" Y="-2.529554443359" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-29.0105625" Y="-2.072899169922" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.1458203125" Y="-1.396011474609" />
                  <Point X="-28.138115234375" Y="-1.366264282227" />
                  <Point X="-28.140326171875" Y="-1.334595092773" />
                  <Point X="-28.161158203125" Y="-1.310639770508" />
                  <Point X="-28.187640625" Y="-1.295053222656" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.789064453125" Y="-1.363552124023" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.88055859375" Y="-1.194541503906" />
                  <Point X="-29.927392578125" Y="-1.011187805176" />
                  <Point X="-29.9775" Y="-0.660846191406" />
                  <Point X="-29.998396484375" Y="-0.51474230957" />
                  <Point X="-29.522068359375" Y="-0.387110717773" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.541896484375" Y="-0.121425506592" />
                  <Point X="-28.523853515625" Y="-0.108903144836" />
                  <Point X="-28.514142578125" Y="-0.102163482666" />
                  <Point X="-28.494896484375" Y="-0.075905471802" />
                  <Point X="-28.4888828125" Y="-0.056527839661" />
                  <Point X="-28.485646484375" Y="-0.04609859848" />
                  <Point X="-28.485646484375" Y="-0.016461481094" />
                  <Point X="-28.49166015625" Y="0.002916153193" />
                  <Point X="-28.494896484375" Y="0.013345396042" />
                  <Point X="-28.514142578125" Y="0.039603401184" />
                  <Point X="-28.532185546875" Y="0.052125762939" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.076623046875" Y="0.205193969727" />
                  <Point X="-29.998185546875" Y="0.452126068115" />
                  <Point X="-29.947828125" Y="0.792446105957" />
                  <Point X="-29.91764453125" Y="0.996415283203" />
                  <Point X="-29.816779296875" Y="1.368639038086" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.444716796875" Y="1.48501171875" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866699219" />
                  <Point X="-28.69176953125" Y="1.408457641602" />
                  <Point X="-28.67027734375" Y="1.41523425293" />
                  <Point X="-28.651533203125" Y="1.426056884766" />
                  <Point X="-28.6391171875" Y="1.443787719727" />
                  <Point X="-28.62309375" Y="1.482471801758" />
                  <Point X="-28.614470703125" Y="1.503291992188" />
                  <Point X="-28.61071484375" Y="1.524606689453" />
                  <Point X="-28.61631640625" Y="1.545512451172" />
                  <Point X="-28.635650390625" Y="1.582652832031" />
                  <Point X="-28.646056640625" Y="1.602642211914" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-28.957759765625" Y="1.847725341797" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.27729296875" Y="2.586078857422" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-28.892828125" Y="3.130436767578" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.583828125" Y="3.172127441406" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.082896484375" Y="2.915569091797" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927405029297" />
                  <Point X="-27.9737734375" Y="2.966881835938" />
                  <Point X="-27.95252734375" Y="2.988128417969" />
                  <Point X="-27.9408984375" Y="3.0063828125" />
                  <Point X="-27.938072265625" Y="3.027841796875" />
                  <Point X="-27.9429375" Y="3.083458007812" />
                  <Point X="-27.945556640625" Y="3.113391357422" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.08402734375" Y="3.362595214844" />
                  <Point X="-28.30727734375" Y="3.749276855469" />
                  <Point X="-27.95713671875" Y="4.017725830078" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.332064453125" Y="4.408124511719" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.097830078125" Y="4.456999023438" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.889345703125" Y="4.241437011719" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.749333984375" Y="4.248956542969" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.696904296875" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.294487792969" />
                  <Point X="-26.66509765625" Y="4.361043457031" />
                  <Point X="-26.653802734375" Y="4.396864257812" />
                  <Point X="-26.651916015625" Y="4.418426269531" />
                  <Point X="-26.666919921875" Y="4.532381347656" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.2325234375" Y="4.829159667969" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.457943359375" Y="4.963001953125" />
                  <Point X="-25.22419921875" Y="4.990358398438" />
                  <Point X="-25.16776953125" Y="4.779759277344" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.877931640625" Y="4.563246582031" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.3706796875" Y="4.949745605469" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.717732421875" Y="4.823667480469" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.2163671875" Y="4.669250488281" />
                  <Point X="-23.068970703125" Y="4.615788085938" />
                  <Point X="-22.80332421875" Y="4.4915546875" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.4046796875" Y="4.275626953125" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-22.025447265625" Y="4.023575439453" />
                  <Point X="-21.9312578125" Y="3.956593017578" />
                  <Point X="-22.20999609375" Y="3.473805664062" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491514160156" />
                  <Point X="-22.78910546875" Y="2.439319580078" />
                  <Point X="-22.7966171875" Y="2.411227783203" />
                  <Point X="-22.797955078125" Y="2.392328613281" />
                  <Point X="-22.79251171875" Y="2.347195068359" />
                  <Point X="-22.789583984375" Y="2.322903808594" />
                  <Point X="-22.78131640625" Y="2.300811523438" />
                  <Point X="-22.753388671875" Y="2.259654052734" />
                  <Point X="-22.738357421875" Y="2.237502929688" />
                  <Point X="-22.725056640625" Y="2.224203125" />
                  <Point X="-22.683900390625" Y="2.196276123047" />
                  <Point X="-22.661748046875" Y="2.181245605469" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.594529296875" Y="2.167537841797" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5513359375" Y="2.165946289062" />
                  <Point X="-22.499140625" Y="2.179903808594" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.9367890625" Y="2.493895507813" />
                  <Point X="-21.005751953125" Y="3.031430664062" />
                  <Point X="-20.87879296875" Y="2.854987060547" />
                  <Point X="-20.79740234375" Y="2.741874267578" />
                  <Point X="-20.66248046875" Y="2.518913330078" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-20.973892578125" Y="2.158978271484" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583832275391" />
                  <Point X="-21.758193359375" Y="1.534826538086" />
                  <Point X="-21.77841015625" Y="1.508451171875" />
                  <Point X="-21.78687890625" Y="1.491500366211" />
                  <Point X="-21.800873046875" Y="1.441465209961" />
                  <Point X="-21.808404296875" Y="1.414535888672" />
                  <Point X="-21.809220703125" Y="1.390966308594" />
                  <Point X="-21.797732421875" Y="1.335295776367" />
                  <Point X="-21.79155078125" Y="1.305333496094" />
                  <Point X="-21.784353515625" Y="1.287955688477" />
                  <Point X="-21.753111328125" Y="1.240468383789" />
                  <Point X="-21.736296875" Y="1.21491027832" />
                  <Point X="-21.719052734375" Y="1.198819946289" />
                  <Point X="-21.67377734375" Y="1.173334228516" />
                  <Point X="-21.64941015625" Y="1.159617553711" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.57021875" Y="1.145529296875" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-21.0281640625" Y="1.206475585938" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.095765625" Y="1.094967529297" />
                  <Point X="-20.06080859375" Y="0.951367797852" />
                  <Point X="-20.018291015625" Y="0.678291992188" />
                  <Point X="-20.002140625" Y="0.574556396484" />
                  <Point X="-20.41365234375" Y="0.464291778564" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819503784" />
                  <Point X="-21.331052734375" Y="0.198056671143" />
                  <Point X="-21.363421875" Y="0.179347137451" />
                  <Point X="-21.377734375" Y="0.166927307129" />
                  <Point X="-21.413818359375" Y="0.120946815491" />
                  <Point X="-21.433240234375" Y="0.096199623108" />
                  <Point X="-21.443013671875" Y="0.074733589172" />
                  <Point X="-21.45504296875" Y="0.011926455498" />
                  <Point X="-21.461515625" Y="-0.021876972198" />
                  <Point X="-21.461515625" Y="-0.040685844421" />
                  <Point X="-21.44948828125" Y="-0.103492973328" />
                  <Point X="-21.443013671875" Y="-0.137296401978" />
                  <Point X="-21.433240234375" Y="-0.15875970459" />
                  <Point X="-21.39715625" Y="-0.204740188599" />
                  <Point X="-21.377734375" Y="-0.229487380981" />
                  <Point X="-21.363421875" Y="-0.241907211304" />
                  <Point X="-21.30328125" Y="-0.276669891357" />
                  <Point X="-21.270912109375" Y="-0.295379577637" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.803947265625" Y="-0.422272674561" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.03205078125" Y="-0.836947387695" />
                  <Point X="-20.051568359375" Y="-0.966413330078" />
                  <Point X="-20.106041015625" Y="-1.205112670898" />
                  <Point X="-20.125453125" Y="-1.290178588867" />
                  <Point X="-20.60784765625" Y="-1.226670043945" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.72319921875" Y="-1.123997070312" />
                  <Point X="-21.7867265625" Y="-1.137805053711" />
                  <Point X="-21.8145546875" Y="-1.154697387695" />
                  <Point X="-21.885900390625" Y="-1.240503662109" />
                  <Point X="-21.924298828125" Y="-1.286685424805" />
                  <Point X="-21.935640625" Y="-1.31407019043" />
                  <Point X="-21.945865234375" Y="-1.425193115234" />
                  <Point X="-21.951369140625" Y="-1.485000610352" />
                  <Point X="-21.943638671875" Y="-1.516622314453" />
                  <Point X="-21.87831640625" Y="-1.618227783203" />
                  <Point X="-21.843158203125" Y="-1.672912841797" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-21.40940234375" Y="-2.009456054688" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.7408515625" Y="-2.713116699219" />
                  <Point X="-20.7958671875" Y="-2.802139648438" />
                  <Point X="-20.9085234375" Y="-2.962209472656" />
                  <Point X="-20.943310546875" Y="-3.011638183594" />
                  <Point X="-21.3735703125" Y="-2.763227294922" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.403138671875" Y="-2.227941894531" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.627626953125" Y="-2.280666259766" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.334683105469" />
                  <Point X="-22.7728203125" Y="-2.451388916016" />
                  <Point X="-22.80587890625" Y="-2.514201171875" />
                  <Point X="-22.8108359375" Y="-2.546375244141" />
                  <Point X="-22.78546484375" Y="-2.686856933594" />
                  <Point X="-22.77180859375" Y="-2.762465576172" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.494642578125" Y="-3.248421630859" />
                  <Point X="-22.01332421875" Y="-4.082088867188" />
                  <Point X="-22.099416015625" Y="-4.14358203125" />
                  <Point X="-22.164685546875" Y="-4.190202148438" />
                  <Point X="-22.290658203125" Y="-4.271741699219" />
                  <Point X="-22.32022265625" Y="-4.29087890625" />
                  <Point X="-22.650662109375" Y="-3.860242675781" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467529297" />
                  <Point X="-23.46800390625" Y="-2.891390625" />
                  <Point X="-23.54257421875" Y="-2.843448730469" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.7257265625" Y="-2.849661132812" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868508789062" />
                  <Point X="-23.951673828125" Y="-2.965797607422" />
                  <Point X="-24.014650390625" Y="-3.018159179688" />
                  <Point X="-24.03154296875" Y="-3.045985839844" />
                  <Point X="-24.06652734375" Y="-3.206944335938" />
                  <Point X="-24.085357421875" Y="-3.293573730469" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-24.0091953125" Y="-3.894637207031" />
                  <Point X="-23.872357421875" Y="-4.934028320312" />
                  <Point X="-23.943892578125" Y="-4.949708984375" />
                  <Point X="-24.005650390625" Y="-4.96324609375" />
                  <Point X="-24.12201171875" Y="-4.984385253906" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#182" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.125629508639" Y="4.823980206672" Z="1.65" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.65" />
                  <Point X="-0.469912334385" Y="5.043943110425" Z="1.65" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.65" />
                  <Point X="-1.252178203445" Y="4.908583028187" Z="1.65" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.65" />
                  <Point X="-1.722648312199" Y="4.557135362634" Z="1.65" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.65" />
                  <Point X="-1.718940200075" Y="4.407359683241" Z="1.65" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.65" />
                  <Point X="-1.77462546924" Y="4.326430634745" Z="1.65" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.65" />
                  <Point X="-1.872414581505" Y="4.317067670483" Z="1.65" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.65" />
                  <Point X="-2.064319912689" Y="4.518716837131" Z="1.65" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.65" />
                  <Point X="-2.362504832927" Y="4.483112020102" Z="1.65" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.65" />
                  <Point X="-2.993716423558" Y="4.088685555161" Z="1.65" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.65" />
                  <Point X="-3.133485194254" Y="3.368875200345" Z="1.65" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.65" />
                  <Point X="-2.998905846443" Y="3.11037973065" Z="1.65" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.65" />
                  <Point X="-3.015286797011" Y="3.033516770875" Z="1.65" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.65" />
                  <Point X="-3.084696734957" Y="2.99665885273" Z="1.65" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.65" />
                  <Point X="-3.564984147952" Y="3.246708754492" Z="1.65" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.65" />
                  <Point X="-3.938447569163" Y="3.192419266796" Z="1.65" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.65" />
                  <Point X="-4.326631857437" Y="2.642563931115" Z="1.65" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.65" />
                  <Point X="-3.994353963097" Y="1.839337683475" Z="1.65" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.65" />
                  <Point X="-3.686156501953" Y="1.590844785858" Z="1.65" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.65" />
                  <Point X="-3.675446429225" Y="1.532884212133" Z="1.65" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.65" />
                  <Point X="-3.712962534561" Y="1.487423517907" Z="1.65" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.65" />
                  <Point X="-4.444349026869" Y="1.565864070969" Z="1.65" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.65" />
                  <Point X="-4.871196482541" Y="1.412996219563" Z="1.65" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.65" />
                  <Point X="-5.003445168995" Y="0.831045370337" Z="1.65" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.65" />
                  <Point X="-4.095721209114" Y="0.188177663805" Z="1.65" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.65" />
                  <Point X="-3.566850179384" Y="0.042329351451" Z="1.65" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.65" />
                  <Point X="-3.5455709517" Y="0.019377686936" Z="1.65" />
                  <Point X="-3.539556741714" Y="0" Z="1.65" />
                  <Point X="-3.542793649435" Y="-0.010429264125" Z="1.65" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.65" />
                  <Point X="-3.55851841574" Y="-0.036546631683" Z="1.65" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.65" />
                  <Point X="-4.541166579658" Y="-0.307534392951" Z="1.65" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.65" />
                  <Point X="-5.033152444448" Y="-0.636645021899" Z="1.65" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.65" />
                  <Point X="-4.935160716439" Y="-1.175635487009" Z="1.65" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.65" />
                  <Point X="-3.788696396723" Y="-1.381844329081" Z="1.65" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.65" />
                  <Point X="-3.209892799467" Y="-1.312316924358" Z="1.65" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.65" />
                  <Point X="-3.195372060448" Y="-1.332858219357" Z="1.65" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.65" />
                  <Point X="-4.047157140572" Y="-2.00195148008" Z="1.65" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.65" />
                  <Point X="-4.40019080394" Y="-2.523884372212" Z="1.65" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.65" />
                  <Point X="-4.08790118129" Y="-3.003452242808" Z="1.65" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.65" />
                  <Point X="-3.023992208709" Y="-2.815964105668" Z="1.65" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.65" />
                  <Point X="-2.566769358056" Y="-2.561561111075" Z="1.65" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.65" />
                  <Point X="-3.039452907501" Y="-3.411086173783" Z="1.65" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.65" />
                  <Point X="-3.156661884968" Y="-3.972547513625" Z="1.65" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.65" />
                  <Point X="-2.736747004072" Y="-4.272687029783" Z="1.65" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.65" />
                  <Point X="-2.304911785335" Y="-4.259002307355" Z="1.65" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.65" />
                  <Point X="-2.135961502823" Y="-4.096141769278" Z="1.65" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.65" />
                  <Point X="-1.859932707158" Y="-3.991184258974" Z="1.65" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.65" />
                  <Point X="-1.577050342366" Y="-4.075951787535" Z="1.65" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.65" />
                  <Point X="-1.404227808407" Y="-4.315410260848" Z="1.65" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.65" />
                  <Point X="-1.396226996457" Y="-4.751347331373" Z="1.65" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.65" />
                  <Point X="-1.309636461511" Y="-4.906123274894" Z="1.65" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.65" />
                  <Point X="-1.012526887399" Y="-4.975940169876" Z="1.65" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.65" />
                  <Point X="-0.557247649777" Y="-4.04186060328" Z="1.65" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.65" />
                  <Point X="-0.359799490546" Y="-3.436233016523" Z="1.65" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.65" />
                  <Point X="-0.164710335927" Y="-3.255359378332" Z="1.65" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.65" />
                  <Point X="0.088648743434" Y="-3.231752666957" Z="1.65" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.65" />
                  <Point X="0.310646368887" Y="-3.365412739245" Z="1.65" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.65" />
                  <Point X="0.677507210624" Y="-4.490675426536" Z="1.65" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.65" />
                  <Point X="0.880768635723" Y="-5.002299882919" Z="1.65" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.65" />
                  <Point X="1.060656110338" Y="-4.967269420509" Z="1.65" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.65" />
                  <Point X="1.034219943712" Y="-3.856830841699" Z="1.65" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.65" />
                  <Point X="0.976175044097" Y="-3.186284326887" Z="1.65" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.65" />
                  <Point X="1.074135189454" Y="-2.972964221958" Z="1.65" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.65" />
                  <Point X="1.27269931876" Y="-2.868170622319" Z="1.65" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.65" />
                  <Point X="1.498801003679" Y="-2.902168561089" Z="1.65" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.65" />
                  <Point X="2.303513134109" Y="-3.859400712666" Z="1.65" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.65" />
                  <Point X="2.730355334248" Y="-4.282435738058" Z="1.65" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.65" />
                  <Point X="2.923487119629" Y="-4.152989213659" Z="1.65" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.65" />
                  <Point X="2.54250097529" Y="-3.192141592205" Z="1.65" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.65" />
                  <Point X="2.257582021067" Y="-2.646689940221" Z="1.65" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.65" />
                  <Point X="2.265268748407" Y="-2.443395971216" Z="1.65" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.65" />
                  <Point X="2.389502407156" Y="-2.29363256156" Z="1.65" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.65" />
                  <Point X="2.581816863292" Y="-2.245865988424" Z="1.65" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.65" />
                  <Point X="3.595272004902" Y="-2.775248524985" Z="1.65" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.65" />
                  <Point X="4.126208959577" Y="-2.959706466834" Z="1.65" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.65" />
                  <Point X="4.295525499896" Y="-2.708121650852" Z="1.65" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.65" />
                  <Point X="3.614877568823" Y="-1.938508454239" Z="1.65" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.65" />
                  <Point X="3.157585395189" Y="-1.559907958202" Z="1.65" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.65" />
                  <Point X="3.097765967032" Y="-1.398495076051" Z="1.65" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.65" />
                  <Point X="3.146390215393" Y="-1.241190446003" Z="1.65" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.65" />
                  <Point X="3.281263742898" Y="-1.141576062817" Z="1.65" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.65" />
                  <Point X="4.379469615134" Y="-1.244962211043" Z="1.65" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.65" />
                  <Point X="4.936548841618" Y="-1.184956267733" Z="1.65" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.65" />
                  <Point X="5.011234229298" Y="-0.813121155177" Z="1.65" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.65" />
                  <Point X="4.202836337661" Y="-0.342696334733" Z="1.65" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.65" />
                  <Point X="3.715584039563" Y="-0.202100971147" Z="1.65" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.65" />
                  <Point X="3.636021518632" Y="-0.142591039697" Z="1.65" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.65" />
                  <Point X="3.593462991689" Y="-0.062807115867" Z="1.65" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.65" />
                  <Point X="3.587908458735" Y="0.033803415333" Z="1.65" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.65" />
                  <Point X="3.619357919768" Y="0.121357698878" Z="1.65" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.65" />
                  <Point X="3.68781137479" Y="0.186047873095" Z="1.65" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.65" />
                  <Point X="4.593131479646" Y="0.447275598036" Z="1.65" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.65" />
                  <Point X="5.02495634205" Y="0.717264047407" Z="1.65" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.65" />
                  <Point X="4.946659059787" Y="1.138074233444" Z="1.65" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.65" />
                  <Point X="3.959153121651" Y="1.287327916409" Z="1.65" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.65" />
                  <Point X="3.430175145997" Y="1.22637833383" Z="1.65" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.65" />
                  <Point X="3.344593299115" Y="1.248185269817" Z="1.65" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.65" />
                  <Point X="3.282503516313" Y="1.299229146157" Z="1.65" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.65" />
                  <Point X="3.245078737367" Y="1.376678780796" Z="1.65" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.65" />
                  <Point X="3.241123128566" Y="1.459278597149" Z="1.65" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.65" />
                  <Point X="3.275333603712" Y="1.535689178235" Z="1.65" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.65" />
                  <Point X="4.050387674845" Y="2.150590690147" Z="1.65" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.65" />
                  <Point X="4.374139344331" Y="2.576079715703" Z="1.65" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.65" />
                  <Point X="4.155636071235" Y="2.915470254719" Z="1.65" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.65" />
                  <Point X="3.032053401006" Y="2.568476905092" Z="1.65" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.65" />
                  <Point X="2.481786262754" Y="2.259486692696" Z="1.65" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.65" />
                  <Point X="2.405300332547" Y="2.248458258457" Z="1.65" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.65" />
                  <Point X="2.338015467848" Y="2.268931101696" Z="1.65" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.65" />
                  <Point X="2.281827498539" Y="2.319009392534" Z="1.65" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.65" />
                  <Point X="2.250971444302" Y="2.384458111286" Z="1.65" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.65" />
                  <Point X="2.253041249961" Y="2.457683313128" Z="1.65" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.65" />
                  <Point X="2.827148447767" Y="3.480085680949" Z="1.65" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.65" />
                  <Point X="2.997371403445" Y="4.095602595563" Z="1.65" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.65" />
                  <Point X="2.614332830181" Y="4.350109857857" Z="1.65" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.65" />
                  <Point X="2.211700472314" Y="4.568126497772" Z="1.65" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.65" />
                  <Point X="1.794523883314" Y="4.74753386843" Z="1.65" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.65" />
                  <Point X="1.287844504293" Y="4.903550857606" Z="1.65" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.65" />
                  <Point X="0.628369881515" Y="5.030753790179" Z="1.65" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.65" />
                  <Point X="0.067615052914" Y="4.607467299279" Z="1.65" />
                  <Point X="0" Y="4.355124473572" Z="1.65" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>