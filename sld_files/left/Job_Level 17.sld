<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#149" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1342" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004715820312" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442382812" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.310328125" Y="-3.984135009766" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140136719" />
                  <Point X="-24.457634765625" Y="-3.467376708984" />
                  <Point X="-24.50928515625" Y="-3.392959228516" />
                  <Point X="-24.62136328125" Y="-3.231476318359" />
                  <Point X="-24.643248046875" Y="-3.209020751953" />
                  <Point X="-24.66950390625" Y="-3.189776855469" />
                  <Point X="-24.69750390625" Y="-3.175669433594" />
                  <Point X="-24.777427734375" Y="-3.150863525391" />
                  <Point X="-24.95086328125" Y="-3.097036132812" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097036132812" />
                  <Point X="-25.116748046875" Y="-3.121841796875" />
                  <Point X="-25.29018359375" Y="-3.175669433594" />
                  <Point X="-25.31818359375" Y="-3.189776855469" />
                  <Point X="-25.344439453125" Y="-3.209020751953" />
                  <Point X="-25.36632421875" Y="-3.231477539062" />
                  <Point X="-25.41797265625" Y="-3.305894775391" />
                  <Point X="-25.53005078125" Y="-3.467377929688" />
                  <Point X="-25.5381875" Y="-3.481571533203" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.825203125" Y="-4.535903808594" />
                  <Point X="-25.9165859375" Y="-4.876940917969" />
                  <Point X="-26.079341796875" Y="-4.845349609375" />
                  <Point X="-26.16839453125" Y="-4.822437011719" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.230173828125" Y="-4.678982421875" />
                  <Point X="-26.2149609375" Y="-4.563438476562" />
                  <Point X="-26.21419921875" Y="-4.5479296875" />
                  <Point X="-26.2165078125" Y="-4.51622265625" />
                  <Point X="-26.235603515625" Y="-4.42023046875" />
                  <Point X="-26.277037109375" Y="-4.211931152344" />
                  <Point X="-26.287939453125" Y="-4.182965332031" />
                  <Point X="-26.30401171875" Y="-4.155127929687" />
                  <Point X="-26.32364453125" Y="-4.131204589844" />
                  <Point X="-26.397228515625" Y="-4.066672363281" />
                  <Point X="-26.556904296875" Y="-3.926639648438" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.74069140625" Y="-3.884565185547" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708496094" />
                  <Point X="-27.014466796875" Y="-3.882028076172" />
                  <Point X="-27.042658203125" Y="-3.894801757812" />
                  <Point X="-27.12403515625" Y="-3.949177001953" />
                  <Point X="-27.300623046875" Y="-4.067169921875" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.3351015625" Y="-4.099461914062" />
                  <Point X="-27.4801484375" Y="-4.288488769531" />
                  <Point X="-27.5072265625" Y="-4.27172265625" />
                  <Point X="-27.80171484375" Y="-4.089382568359" />
                  <Point X="-27.9250234375" Y="-3.994439453125" />
                  <Point X="-28.10472265625" Y="-3.856077392578" />
                  <Point X="-27.6586015625" Y="-3.083374267578" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647655273438" />
                  <Point X="-27.406587890625" Y="-2.616129882812" />
                  <Point X="-27.40557421875" Y="-2.5851953125" />
                  <Point X="-27.41455859375" Y="-2.555576904297" />
                  <Point X="-27.428775390625" Y="-2.526748046875" />
                  <Point X="-27.44680078125" Y="-2.501592041016" />
                  <Point X="-27.4641484375" Y="-2.484243408203" />
                  <Point X="-27.4893046875" Y="-2.466215576172" />
                  <Point X="-27.5181328125" Y="-2.451997558594" />
                  <Point X="-27.547751953125" Y="-2.44301171875" />
                  <Point X="-27.5786875" Y="-2.444023925781" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.521828125" Y="-2.970792480469" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-28.850205078125" Y="-3.099521972656" />
                  <Point X="-29.082861328125" Y="-2.793858398438" />
                  <Point X="-29.171265625" Y="-2.645619384766" />
                  <Point X="-29.306142578125" Y="-2.419450439453" />
                  <Point X="-28.51684765625" Y="-1.813803466797" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.08306640625" Y="-1.475883056641" />
                  <Point X="-28.063556640625" Y="-1.444362548828" />
                  <Point X="-28.05441015625" Y="-1.419906738281" />
                  <Point X="-28.051423828125" Y="-1.410444580078" />
                  <Point X="-28.046150390625" Y="-1.390081298828" />
                  <Point X="-28.042037109375" Y="-1.358597290039" />
                  <Point X="-28.04273828125" Y="-1.335732177734" />
                  <Point X="-28.0488828125" Y="-1.313697265625" />
                  <Point X="-28.06088671875" Y="-1.284715209961" />
                  <Point X="-28.073873046875" Y="-1.262480834961" />
                  <Point X="-28.092353515625" Y="-1.244550537109" />
                  <Point X="-28.113208984375" Y="-1.229204467773" />
                  <Point X="-28.121326171875" Y="-1.223848999023" />
                  <Point X="-28.139455078125" Y="-1.213179321289" />
                  <Point X="-28.16871875" Y="-1.201955810547" />
                  <Point X="-28.20060546875" Y="-1.195474609375" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-29.34618359375" Y="-1.341078369141" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.74308203125" Y="-1.348890869141" />
                  <Point X="-29.834076171875" Y="-0.99265045166" />
                  <Point X="-29.857466796875" Y="-0.8291171875" />
                  <Point X="-29.892423828125" Y="-0.584698425293" />
                  <Point X="-29.0009453125" Y="-0.345827331543" />
                  <Point X="-28.532875" Y="-0.220408279419" />
                  <Point X="-28.51189453125" Y="-0.212003646851" />
                  <Point X="-28.4875703125" Y="-0.198706802368" />
                  <Point X="-28.47897265625" Y="-0.193393234253" />
                  <Point X="-28.459974609375" Y="-0.18020765686" />
                  <Point X="-28.436015625" Y="-0.15867817688" />
                  <Point X="-28.415" Y="-0.12805947876" />
                  <Point X="-28.40465625" Y="-0.103918128967" />
                  <Point X="-28.401248046875" Y="-0.094660438538" />
                  <Point X="-28.394916015625" Y="-0.074256469727" />
                  <Point X="-28.38947265625" Y="-0.045516029358" />
                  <Point X="-28.39048828125" Y="-0.011471729279" />
                  <Point X="-28.39565234375" Y="0.01275067234" />
                  <Point X="-28.397833984375" Y="0.021099294662" />
                  <Point X="-28.404166015625" Y="0.041503257751" />
                  <Point X="-28.417482421875" Y="0.070830368042" />
                  <Point X="-28.440578125" Y="0.100283920288" />
                  <Point X="-28.46115234375" Y="0.117878700256" />
                  <Point X="-28.46873046875" Y="0.123723999023" />
                  <Point X="-28.487728515625" Y="0.136909576416" />
                  <Point X="-28.501923828125" Y="0.145047348022" />
                  <Point X="-28.532875" Y="0.157848297119" />
                  <Point X="-29.5485703125" Y="0.430003234863" />
                  <Point X="-29.89181640625" Y="0.521975524902" />
                  <Point X="-29.883130859375" Y="0.580667175293" />
                  <Point X="-29.82448828125" Y="0.976967407227" />
                  <Point X="-29.77740234375" Y="1.150726928711" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-29.101189453125" Y="1.343965576172" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228149414" />
                  <Point X="-28.703134765625" Y="1.305263793945" />
                  <Point X="-28.6837578125" Y="1.311373535156" />
                  <Point X="-28.641708984375" Y="1.324631347656" />
                  <Point X="-28.622775390625" Y="1.332962524414" />
                  <Point X="-28.60403125" Y="1.343784790039" />
                  <Point X="-28.587353515625" Y="1.356014282227" />
                  <Point X="-28.573716796875" Y="1.371563232422" />
                  <Point X="-28.56130078125" Y="1.389293457031" />
                  <Point X="-28.551349609375" Y="1.407431640625" />
                  <Point X="-28.54357421875" Y="1.42620300293" />
                  <Point X="-28.526703125" Y="1.466936035156" />
                  <Point X="-28.520912109375" Y="1.486805664062" />
                  <Point X="-28.51715625" Y="1.50812097168" />
                  <Point X="-28.515806640625" Y="1.528755249023" />
                  <Point X="-28.518951171875" Y="1.549193115234" />
                  <Point X="-28.524552734375" Y="1.570099609375" />
                  <Point X="-28.532048828125" Y="1.589375366211" />
                  <Point X="-28.5414296875" Y="1.607397583008" />
                  <Point X="-28.5617890625" Y="1.646505249023" />
                  <Point X="-28.573283203125" Y="1.663709228516" />
                  <Point X="-28.5871953125" Y="1.680288330078" />
                  <Point X="-28.60213671875" Y="1.694590332031" />
                  <Point X="-29.1847421875" Y="2.141639648438" />
                  <Point X="-29.351859375" Y="2.269873779297" />
                  <Point X="-29.30901953125" Y="2.343270019531" />
                  <Point X="-29.0811484375" Y="2.733665283203" />
                  <Point X="-28.95643359375" Y="2.893969482422" />
                  <Point X="-28.75050390625" Y="3.158662353516" />
                  <Point X="-28.412724609375" Y="2.963645263672" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.1198046875" Y="2.823435302734" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.999013671875" Y="2.826504638672" />
                  <Point X="-27.980462890625" Y="2.835653076172" />
                  <Point X="-27.962208984375" Y="2.847281982422" />
                  <Point X="-27.946078125" Y="2.860228271484" />
                  <Point X="-27.926921875" Y="2.879384033203" />
                  <Point X="-27.885353515625" Y="2.920951660156" />
                  <Point X="-27.872404296875" Y="2.937085449219" />
                  <Point X="-27.860775390625" Y="2.95533984375" />
                  <Point X="-27.85162890625" Y="2.973888671875" />
                  <Point X="-27.8467109375" Y="2.993977539062" />
                  <Point X="-27.843884765625" Y="3.015436279297" />
                  <Point X="-27.84343359375" Y="3.036121337891" />
                  <Point X="-27.845794921875" Y="3.063108886719" />
                  <Point X="-27.85091796875" Y="3.121670898438" />
                  <Point X="-27.854955078125" Y="3.141962158203" />
                  <Point X="-27.86146484375" Y="3.162604736328" />
                  <Point X="-27.869794921875" Y="3.181532958984" />
                  <Point X="-28.12796484375" Y="3.628697021484" />
                  <Point X="-28.18333203125" Y="3.724596435547" />
                  <Point X="-28.0974921875" Y="3.790409667969" />
                  <Point X="-27.70062109375" Y="4.094685791016" />
                  <Point X="-27.504197265625" Y="4.203813964844" />
                  <Point X="-27.16703515625" Y="4.391133789062" />
                  <Point X="-27.106279296875" Y="4.311954101562" />
                  <Point X="-27.0431953125" Y="4.229741210938" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.995111328125" Y="4.18939453125" />
                  <Point X="-26.96507421875" Y="4.173758300781" />
                  <Point X="-26.89989453125" Y="4.139827636719" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.777451171875" Y="4.134482421875" />
                  <Point X="-26.746166015625" Y="4.14744140625" />
                  <Point X="-26.67827734375" Y="4.175562011719" />
                  <Point X="-26.660142578125" Y="4.18551171875" />
                  <Point X="-26.6424140625" Y="4.19792578125" />
                  <Point X="-26.62686328125" Y="4.2115625" />
                  <Point X="-26.6146328125" Y="4.2282421875" />
                  <Point X="-26.603810546875" Y="4.246985839844" />
                  <Point X="-26.595478515625" Y="4.265923828125" />
                  <Point X="-26.585296875" Y="4.298219726563" />
                  <Point X="-26.56319921875" Y="4.368300292969" />
                  <Point X="-26.5591640625" Y="4.388583007812" />
                  <Point X="-26.55727734375" Y="4.410145019531" />
                  <Point X="-26.557728515625" Y="4.430826660156" />
                  <Point X="-26.584201171875" Y="4.6318984375" />
                  <Point X="-26.46340234375" Y="4.665766113281" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.71151171875" Y="4.837677246094" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.194865234375" Y="4.513825195312" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.721498046875" Y="4.780004394531" />
                  <Point X="-24.692578125" Y="4.887937011719" />
                  <Point X="-24.60455859375" Y="4.878719238281" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.9589453125" Y="4.784174316406" />
                  <Point X="-23.518970703125" Y="4.677950195312" />
                  <Point X="-23.391751953125" Y="4.631807617188" />
                  <Point X="-23.105353515625" Y="4.527928222656" />
                  <Point X="-22.981353515625" Y="4.4699375" />
                  <Point X="-22.705423828125" Y="4.340893554688" />
                  <Point X="-22.585603515625" Y="4.271086914062" />
                  <Point X="-22.319021484375" Y="4.115775878906" />
                  <Point X="-22.206041015625" Y="4.0354296875" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.580607421875" Y="3.021885986328" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857919921875" Y="2.539934814453" />
                  <Point X="-22.866921875" Y="2.516059082031" />
                  <Point X="-22.8736953125" Y="2.490731933594" />
                  <Point X="-22.888392578125" Y="2.435772705078" />
                  <Point X="-22.891380859375" Y="2.417935791016" />
                  <Point X="-22.892271484375" Y="2.380953857422" />
                  <Point X="-22.889630859375" Y="2.359053222656" />
                  <Point X="-22.883900390625" Y="2.311529052734" />
                  <Point X="-22.87855859375" Y="2.289606933594" />
                  <Point X="-22.87029296875" Y="2.267518066406" />
                  <Point X="-22.859927734375" Y="2.247467773438" />
                  <Point X="-22.846375" Y="2.227496337891" />
                  <Point X="-22.81696875" Y="2.184159179688" />
                  <Point X="-22.805533203125" Y="2.170326660156" />
                  <Point X="-22.778400390625" Y="2.145593261719" />
                  <Point X="-22.7584296875" Y="2.132041748047" />
                  <Point X="-22.715091796875" Y="2.102635742188" />
                  <Point X="-22.695046875" Y="2.092272216797" />
                  <Point X="-22.672958984375" Y="2.084006347656" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.629134765625" Y="2.076022705078" />
                  <Point X="-22.581611328125" Y="2.070291992188" />
                  <Point X="-22.563533203125" Y="2.069845703125" />
                  <Point X="-22.526794921875" Y="2.074170898438" />
                  <Point X="-22.501466796875" Y="2.080943847656" />
                  <Point X="-22.4465078125" Y="2.095640625" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.389869140625" Y="2.699963134766" />
                  <Point X="-21.032673828125" Y="2.906190185547" />
                  <Point X="-20.87671875" Y="2.689449707031" />
                  <Point X="-20.8137421875" Y="2.585379150391" />
                  <Point X="-20.73780078125" Y="2.459883789063" />
                  <Point X="-21.4113515625" Y="1.943049194336" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.77857421875" Y="1.660242553711" />
                  <Point X="-21.796025390625" Y="1.641628173828" />
                  <Point X="-21.81425390625" Y="1.617848388672" />
                  <Point X="-21.853806640625" Y="1.566246826172" />
                  <Point X="-21.86339453125" Y="1.550909790039" />
                  <Point X="-21.8783671875" Y="1.517090087891" />
                  <Point X="-21.885158203125" Y="1.492810791016" />
                  <Point X="-21.899892578125" Y="1.440125610352" />
                  <Point X="-21.90334765625" Y="1.41782434082" />
                  <Point X="-21.9041640625" Y="1.394253295898" />
                  <Point X="-21.902259765625" Y="1.371766357422" />
                  <Point X="-21.896685546875" Y="1.344752563477" />
                  <Point X="-21.88458984375" Y="1.286133666992" />
                  <Point X="-21.8793203125" Y="1.268979003906" />
                  <Point X="-21.863716796875" Y="1.235741088867" />
                  <Point X="-21.848556640625" Y="1.212698120117" />
                  <Point X="-21.81566015625" Y="1.162695556641" />
                  <Point X="-21.801107421875" Y="1.145450195312" />
                  <Point X="-21.78386328125" Y="1.129360473633" />
                  <Point X="-21.76565234375" Y="1.116034057617" />
                  <Point X="-21.743681640625" Y="1.103667114258" />
                  <Point X="-21.696009765625" Y="1.076831542969" />
                  <Point X="-21.679478515625" Y="1.069501342773" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.61417578125" Y="1.055512817383" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.541349609375" Y="1.174746337891" />
                  <Point X="-20.223162109375" Y="1.216636230469" />
                  <Point X="-20.22198046875" Y="1.211785522461" />
                  <Point X="-20.15405859375" Y="0.932788879395" />
                  <Point X="-20.13421484375" Y="0.805326477051" />
                  <Point X="-20.109134765625" Y="0.644238647461" />
                  <Point X="-20.87330078125" Y="0.439480743408" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.2952109375" Y="0.325585296631" />
                  <Point X="-21.318453125" Y="0.31506817627" />
                  <Point X="-21.34763671875" Y="0.298199890137" />
                  <Point X="-21.410962890625" Y="0.261595794678" />
                  <Point X="-21.425685546875" Y="0.25109765625" />
                  <Point X="-21.45246875" Y="0.225576385498" />
                  <Point X="-21.469978515625" Y="0.203264572144" />
                  <Point X="-21.507974609375" Y="0.154848602295" />
                  <Point X="-21.51969921875" Y="0.13556803894" />
                  <Point X="-21.52947265625" Y="0.11410458374" />
                  <Point X="-21.536318359375" Y="0.092602264404" />
                  <Point X="-21.542154296875" Y="0.062125495911" />
                  <Point X="-21.5548203125" Y="-0.004008045197" />
                  <Point X="-21.556515625" Y="-0.021874832153" />
                  <Point X="-21.5548203125" Y="-0.058551937103" />
                  <Point X="-21.548984375" Y="-0.089028709412" />
                  <Point X="-21.536318359375" Y="-0.155162399292" />
                  <Point X="-21.52947265625" Y="-0.176664718628" />
                  <Point X="-21.51969921875" Y="-0.198128173828" />
                  <Point X="-21.507974609375" Y="-0.217408737183" />
                  <Point X="-21.49046484375" Y="-0.239720565796" />
                  <Point X="-21.45246875" Y="-0.288136383057" />
                  <Point X="-21.439998046875" Y="-0.301237548828" />
                  <Point X="-21.410962890625" Y="-0.324155761719" />
                  <Point X="-21.381779296875" Y="-0.341024230957" />
                  <Point X="-21.318453125" Y="-0.37762802124" />
                  <Point X="-21.307291015625" Y="-0.383137695312" />
                  <Point X="-21.283419921875" Y="-0.392149810791" />
                  <Point X="-20.3934765625" Y="-0.630609558105" />
                  <Point X="-20.108525390625" Y="-0.706961975098" />
                  <Point X="-20.144974609375" Y="-0.948722900391" />
                  <Point X="-20.170404296875" Y="-1.060160400391" />
                  <Point X="-20.19882421875" Y="-1.18469909668" />
                  <Point X="-21.09999609375" Y="-1.066057739258" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710266113" />
                  <Point X="-21.62533984375" Y="-1.005508666992" />
                  <Point X="-21.6826171875" Y="-1.017958251953" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.8360234375" Y="-1.056597045898" />
                  <Point X="-21.863849609375" Y="-1.073489013672" />
                  <Point X="-21.8876015625" Y="-1.093960693359" />
                  <Point X="-21.922220703125" Y="-1.13559777832" />
                  <Point X="-21.997345703125" Y="-1.225948608398" />
                  <Point X="-22.012064453125" Y="-1.250329956055" />
                  <Point X="-22.023408203125" Y="-1.277716064453" />
                  <Point X="-22.030240234375" Y="-1.305364257812" />
                  <Point X="-22.035203125" Y="-1.359286132812" />
                  <Point X="-22.04596875" Y="-1.476294555664" />
                  <Point X="-22.04365234375" Y="-1.507562255859" />
                  <Point X="-22.035921875" Y="-1.539182739258" />
                  <Point X="-22.023548828125" Y="-1.567996337891" />
                  <Point X="-21.9918515625" Y="-1.617300048828" />
                  <Point X="-21.923068359375" Y="-1.724286987305" />
                  <Point X="-21.9130625" Y="-1.737243286133" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.06349609375" Y="-2.394624267578" />
                  <Point X="-20.786875" Y="-2.6068828125" />
                  <Point X="-20.875185546875" Y="-2.749781738281" />
                  <Point X="-20.927779296875" Y="-2.824509277344" />
                  <Point X="-20.971017578125" Y="-2.8859453125" />
                  <Point X="-21.775439453125" Y="-2.421512207031" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.245775390625" Y="-2.159825195312" />
                  <Point X="-22.313943359375" Y="-2.147514160156" />
                  <Point X="-22.461865234375" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135176757812" />
                  <Point X="-22.611796875" Y="-2.164981201172" />
                  <Point X="-22.73468359375" Y="-2.229655761719" />
                  <Point X="-22.757615234375" Y="-2.246549560547" />
                  <Point X="-22.77857421875" Y="-2.267509033203" />
                  <Point X="-22.795466796875" Y="-2.290438720703" />
                  <Point X="-22.825271484375" Y="-2.347069580078" />
                  <Point X="-22.889947265625" Y="-2.469956787109" />
                  <Point X="-22.899771484375" Y="-2.499735107422" />
                  <Point X="-22.904728515625" Y="-2.531908203125" />
                  <Point X="-22.904322265625" Y="-2.563260253906" />
                  <Point X="-22.892009765625" Y="-2.631428222656" />
                  <Point X="-22.865294921875" Y="-2.779350585938" />
                  <Point X="-22.86101171875" Y="-2.795139648438" />
                  <Point X="-22.8481796875" Y="-2.826078613281" />
                  <Point X="-22.317474609375" Y="-3.745289306641" />
                  <Point X="-22.13871484375" Y="-4.054907470703" />
                  <Point X="-22.218134765625" Y="-4.111634277344" />
                  <Point X="-22.276947265625" Y="-4.149702636719" />
                  <Point X="-22.298232421875" Y="-4.163480957031" />
                  <Point X="-22.918201171875" Y="-3.355522949219" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.345306640625" Y="-2.857333007812" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.6564296875" Y="-2.747883056641" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-23.9521796875" Y="-2.842669921875" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968861572266" />
                  <Point X="-24.11275" Y="-2.996688476562" />
                  <Point X="-24.124375" Y="-3.02580859375" />
                  <Point X="-24.1413515625" Y="-3.103912841797" />
                  <Point X="-24.178189453125" Y="-3.273396484375" />
                  <Point X="-24.180275390625" Y="-3.289627685547" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.029859375" Y="-4.4655078125" />
                  <Point X="-23.977935546875" Y="-4.859916015625" />
                  <Point X="-24.02430859375" Y="-4.870081054688" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.05843359375" Y="-4.752635253906" />
                  <Point X="-26.141244140625" Y="-4.731328613281" />
                  <Point X="-26.135986328125" Y="-4.691381347656" />
                  <Point X="-26.1207734375" Y="-4.575837402344" />
                  <Point X="-26.120076171875" Y="-4.568098632812" />
                  <Point X="-26.11944921875" Y="-4.541030761719" />
                  <Point X="-26.1217578125" Y="-4.509323730469" />
                  <Point X="-26.123333984375" Y="-4.4976875" />
                  <Point X="-26.1424296875" Y="-4.4016953125" />
                  <Point X="-26.18386328125" Y="-4.193395996094" />
                  <Point X="-26.188126953125" Y="-4.178466308594" />
                  <Point X="-26.199029296875" Y="-4.149500488281" />
                  <Point X="-26.20566796875" Y="-4.135464355469" />
                  <Point X="-26.221740234375" Y="-4.107626953125" />
                  <Point X="-26.23057421875" Y="-4.094861572266" />
                  <Point X="-26.25020703125" Y="-4.070938232422" />
                  <Point X="-26.261005859375" Y="-4.059780273438" />
                  <Point X="-26.33458984375" Y="-3.995248046875" />
                  <Point X="-26.494265625" Y="-3.855215332031" />
                  <Point X="-26.506736328125" Y="-3.845965820312" />
                  <Point X="-26.53301953125" Y="-3.829621582031" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.734478515625" Y="-3.789768554688" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166015625" />
                  <Point X="-27.008005859375" Y="-3.781945556641" />
                  <Point X="-27.0390546875" Y="-3.790265136719" />
                  <Point X="-27.053673828125" Y="-3.795496337891" />
                  <Point X="-27.081865234375" Y="-3.808270019531" />
                  <Point X="-27.0954375" Y="-3.8158125" />
                  <Point X="-27.176814453125" Y="-3.870187744141" />
                  <Point X="-27.35340234375" Y="-3.988180664062" />
                  <Point X="-27.35968359375" Y="-3.992759277344" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032770507812" />
                  <Point X="-27.410470703125" Y="-4.041629150391" />
                  <Point X="-27.503201171875" Y="-4.162478027344" />
                  <Point X="-27.747591796875" Y="-4.011156982422" />
                  <Point X="-27.86706640625" Y="-3.919166748047" />
                  <Point X="-27.98086328125" Y="-3.831546630859" />
                  <Point X="-27.576330078125" Y="-3.130874267578" />
                  <Point X="-27.341490234375" Y="-2.724119628906" />
                  <Point X="-27.3348515625" Y="-2.7100859375" />
                  <Point X="-27.32394921875" Y="-2.681121582031" />
                  <Point X="-27.319685546875" Y="-2.666190917969" />
                  <Point X="-27.3134140625" Y="-2.634665527344" />
                  <Point X="-27.311638671875" Y="-2.619241210938" />
                  <Point X="-27.310625" Y="-2.588306640625" />
                  <Point X="-27.3146640625" Y="-2.557619140625" />
                  <Point X="-27.3236484375" Y="-2.528000732422" />
                  <Point X="-27.32935546875" Y="-2.513559570312" />
                  <Point X="-27.343572265625" Y="-2.484730712891" />
                  <Point X="-27.351552734375" Y="-2.471415039063" />
                  <Point X="-27.369578125" Y="-2.446259033203" />
                  <Point X="-27.379623046875" Y="-2.434418701172" />
                  <Point X="-27.396970703125" Y="-2.417070068359" />
                  <Point X="-27.408810546875" Y="-2.407024658203" />
                  <Point X="-27.433966796875" Y="-2.388996826172" />
                  <Point X="-27.447283203125" Y="-2.381014404297" />
                  <Point X="-27.476111328125" Y="-2.366796386719" />
                  <Point X="-27.490552734375" Y="-2.361089111328" />
                  <Point X="-27.520171875" Y="-2.352103271484" />
                  <Point X="-27.550859375" Y="-2.3480625" />
                  <Point X="-27.581794921875" Y="-2.349074707031" />
                  <Point X="-27.597220703125" Y="-2.350849121094" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.569328125" Y="-2.888520019531" />
                  <Point X="-28.793087890625" Y="-3.017708251953" />
                  <Point X="-29.0040234375" Y="-2.740581787109" />
                  <Point X="-29.089673828125" Y="-2.596960693359" />
                  <Point X="-29.181265625" Y="-2.443374023438" />
                  <Point X="-28.459015625" Y="-1.88917199707" />
                  <Point X="-28.048123046875" Y="-1.573881958008" />
                  <Point X="-28.039162109375" Y="-1.566068725586" />
                  <Point X="-28.0162734375" Y="-1.543438354492" />
                  <Point X="-28.002287109375" Y="-1.525881347656" />
                  <Point X="-27.98277734375" Y="-1.494360839844" />
                  <Point X="-27.974576171875" Y="-1.477641113281" />
                  <Point X="-27.9654296875" Y="-1.453185424805" />
                  <Point X="-27.95945703125" Y="-1.434260864258" />
                  <Point X="-27.95418359375" Y="-1.413897583008" />
                  <Point X="-27.951951171875" Y="-1.402388183594" />
                  <Point X="-27.947837890625" Y="-1.370904174805" />
                  <Point X="-27.94708203125" Y="-1.355685424805" />
                  <Point X="-27.947783203125" Y="-1.3328203125" />
                  <Point X="-27.95123046875" Y="-1.310214599609" />
                  <Point X="-27.957375" Y="-1.2881796875" />
                  <Point X="-27.96111328125" Y="-1.277344604492" />
                  <Point X="-27.9731171875" Y="-1.248362548828" />
                  <Point X="-27.978853515625" Y="-1.236802612305" />
                  <Point X="-27.99183984375" Y="-1.214568237305" />
                  <Point X="-28.007720703125" Y="-1.194298461914" />
                  <Point X="-28.026201171875" Y="-1.176368164062" />
                  <Point X="-28.03605078125" Y="-1.168033203125" />
                  <Point X="-28.05690625" Y="-1.152687133789" />
                  <Point X="-28.073140625" Y="-1.141976318359" />
                  <Point X="-28.09126953125" Y="-1.131306640625" />
                  <Point X="-28.105435546875" Y="-1.124479248047" />
                  <Point X="-28.13469921875" Y="-1.113255737305" />
                  <Point X="-28.149796875" Y="-1.10885949707" />
                  <Point X="-28.18168359375" Y="-1.102378173828" />
                  <Point X="-28.197298828125" Y="-1.100532104492" />
                  <Point X="-28.22862109375" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196655273" />
                  <Point X="-29.358583984375" Y="-1.246891235352" />
                  <Point X="-29.66091796875" Y="-1.286694335938" />
                  <Point X="-29.740759765625" Y="-0.974113220215" />
                  <Point X="-29.763423828125" Y="-0.815666015625" />
                  <Point X="-29.786451171875" Y="-0.654654541016" />
                  <Point X="-28.976357421875" Y="-0.437590270996" />
                  <Point X="-28.508287109375" Y="-0.312171295166" />
                  <Point X="-28.497546875" Y="-0.308595550537" />
                  <Point X="-28.47656640625" Y="-0.300190948486" />
                  <Point X="-28.466326171875" Y="-0.295361724854" />
                  <Point X="-28.442001953125" Y="-0.282064880371" />
                  <Point X="-28.424806640625" Y="-0.271437896729" />
                  <Point X="-28.40580859375" Y="-0.258252227783" />
                  <Point X="-28.396478515625" Y="-0.250869842529" />
                  <Point X="-28.37251953125" Y="-0.229340316772" />
                  <Point X="-28.357689453125" Y="-0.212438034058" />
                  <Point X="-28.336673828125" Y="-0.181819335938" />
                  <Point X="-28.327677734375" Y="-0.165474090576" />
                  <Point X="-28.317333984375" Y="-0.141332672119" />
                  <Point X="-28.310517578125" Y="-0.122817382812" />
                  <Point X="-28.304185546875" Y="-0.102413391113" />
                  <Point X="-28.301576171875" Y="-0.091934883118" />
                  <Point X="-28.2961328125" Y="-0.063194484711" />
                  <Point X="-28.294515625" Y="-0.042683185577" />
                  <Point X="-28.29553125" Y="-0.00863888073" />
                  <Point X="-28.297576171875" Y="0.008336529732" />
                  <Point X="-28.302740234375" Y="0.032558940887" />
                  <Point X="-28.307103515625" Y="0.049256278992" />
                  <Point X="-28.313435546875" Y="0.069660270691" />
                  <Point X="-28.317666015625" Y="0.080780090332" />
                  <Point X="-28.330982421875" Y="0.110107246399" />
                  <Point X="-28.342724609375" Y="0.129450668335" />
                  <Point X="-28.3658203125" Y="0.158904159546" />
                  <Point X="-28.378833984375" Y="0.172483093262" />
                  <Point X="-28.399408203125" Y="0.190077819824" />
                  <Point X="-28.414564453125" Y="0.20176864624" />
                  <Point X="-28.4335625" Y="0.214954162598" />
                  <Point X="-28.44048046875" Y="0.219327102661" />
                  <Point X="-28.465615234375" Y="0.23283543396" />
                  <Point X="-28.49656640625" Y="0.245636322021" />
                  <Point X="-28.508287109375" Y="0.249611251831" />
                  <Point X="-29.523982421875" Y="0.521766235352" />
                  <Point X="-29.7854453125" Y="0.591824707031" />
                  <Point X="-29.73133203125" Y="0.957521850586" />
                  <Point X="-29.685708984375" Y="1.125879638672" />
                  <Point X="-29.6335859375" Y="1.318236938477" />
                  <Point X="-29.11358984375" Y="1.249778320313" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815551758" />
                  <Point X="-28.747056640625" Y="1.204364257812" />
                  <Point X="-28.736705078125" Y="1.204703369141" />
                  <Point X="-28.71514453125" Y="1.20658984375" />
                  <Point X="-28.704890625" Y="1.208053466797" />
                  <Point X="-28.6846015625" Y="1.212089111328" />
                  <Point X="-28.67456640625" Y="1.214661010742" />
                  <Point X="-28.655189453125" Y="1.220770751953" />
                  <Point X="-28.613140625" Y="1.234028564453" />
                  <Point X="-28.603447265625" Y="1.237677124023" />
                  <Point X="-28.584513671875" Y="1.246008300781" />
                  <Point X="-28.5752734375" Y="1.250690795898" />
                  <Point X="-28.556529296875" Y="1.261513061523" />
                  <Point X="-28.547853515625" Y="1.267174438477" />
                  <Point X="-28.53117578125" Y="1.279403930664" />
                  <Point X="-28.5159296875" Y="1.293374755859" />
                  <Point X="-28.50229296875" Y="1.308923706055" />
                  <Point X="-28.495900390625" Y="1.317069946289" />
                  <Point X="-28.483484375" Y="1.334800292969" />
                  <Point X="-28.47801171875" Y="1.343598754883" />
                  <Point X="-28.468060546875" Y="1.361736938477" />
                  <Point X="-28.46358203125" Y="1.371076538086" />
                  <Point X="-28.455806640625" Y="1.389847900391" />
                  <Point X="-28.438935546875" Y="1.430581054688" />
                  <Point X="-28.435498046875" Y="1.440354248047" />
                  <Point X="-28.42970703125" Y="1.460223876953" />
                  <Point X="-28.427353515625" Y="1.47032019043" />
                  <Point X="-28.42359765625" Y="1.491635498047" />
                  <Point X="-28.422359375" Y="1.501920532227" />
                  <Point X="-28.421009765625" Y="1.522554931641" />
                  <Point X="-28.421912109375" Y="1.543201782227" />
                  <Point X="-28.425056640625" Y="1.563639648438" />
                  <Point X="-28.4271875" Y="1.573779663086" />
                  <Point X="-28.4327890625" Y="1.594686157227" />
                  <Point X="-28.43601171875" Y="1.604531982422" />
                  <Point X="-28.4435078125" Y="1.623807617188" />
                  <Point X="-28.44778125" Y="1.63323815918" />
                  <Point X="-28.457162109375" Y="1.651260498047" />
                  <Point X="-28.477521484375" Y="1.690368041992" />
                  <Point X="-28.482796875" Y="1.699280639648" />
                  <Point X="-28.494291015625" Y="1.716484619141" />
                  <Point X="-28.500509765625" Y="1.724775634766" />
                  <Point X="-28.514421875" Y="1.741354736328" />
                  <Point X="-28.52150390625" Y="1.748915893555" />
                  <Point X="-28.5364453125" Y="1.763217895508" />
                  <Point X="-28.5443046875" Y="1.769958862305" />
                  <Point X="-29.12691015625" Y="2.217008300781" />
                  <Point X="-29.227615234375" Y="2.294281738281" />
                  <Point X="-29.22697265625" Y="2.295381103516" />
                  <Point X="-29.00228515625" Y="2.680321533203" />
                  <Point X="-28.881453125" Y="2.835635253906" />
                  <Point X="-28.726337890625" Y="3.035013427734" />
                  <Point X="-28.460224609375" Y="2.881372802734" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.128083984375" Y="2.728796875" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310791016" />
                  <Point X="-27.97643359375" Y="2.734227294922" />
                  <Point X="-27.95699609375" Y="2.741302001953" />
                  <Point X="-27.9384453125" Y="2.750450439453" />
                  <Point X="-27.929419921875" Y="2.755530761719" />
                  <Point X="-27.911166015625" Y="2.767159667969" />
                  <Point X="-27.90274609375" Y="2.773192626953" />
                  <Point X="-27.886615234375" Y="2.786138916016" />
                  <Point X="-27.878904296875" Y="2.793052246094" />
                  <Point X="-27.859748046875" Y="2.812208007812" />
                  <Point X="-27.8181796875" Y="2.853775634766" />
                  <Point X="-27.811265625" Y="2.861487548828" />
                  <Point X="-27.79831640625" Y="2.877621337891" />
                  <Point X="-27.79228125" Y="2.886043212891" />
                  <Point X="-27.78065234375" Y="2.904297607422" />
                  <Point X="-27.7755703125" Y="2.913325439453" />
                  <Point X="-27.766423828125" Y="2.931874267578" />
                  <Point X="-27.759353515625" Y="2.951298828125" />
                  <Point X="-27.754435546875" Y="2.971387695312" />
                  <Point X="-27.7525234375" Y="2.981572998047" />
                  <Point X="-27.749697265625" Y="3.003031738281" />
                  <Point X="-27.748908203125" Y="3.013364746094" />
                  <Point X="-27.74845703125" Y="3.034049804688" />
                  <Point X="-27.748794921875" Y="3.044401855469" />
                  <Point X="-27.75115625" Y="3.071389404297" />
                  <Point X="-27.756279296875" Y="3.129951416016" />
                  <Point X="-27.757744140625" Y="3.140208496094" />
                  <Point X="-27.76178125" Y="3.160499755859" />
                  <Point X="-27.764353515625" Y="3.170533935547" />
                  <Point X="-27.77086328125" Y="3.191176513672" />
                  <Point X="-27.774513671875" Y="3.200871337891" />
                  <Point X="-27.78284375" Y="3.219799560547" />
                  <Point X="-27.7875234375" Y="3.229032958984" />
                  <Point X="-28.045693359375" Y="3.676197021484" />
                  <Point X="-28.05938671875" Y="3.699916015625" />
                  <Point X="-28.039689453125" Y="3.715018310547" />
                  <Point X="-27.6483671875" Y="4.015040283203" />
                  <Point X="-27.458060546875" Y="4.12076953125" />
                  <Point X="-27.1925234375" Y="4.268295898437" />
                  <Point X="-27.1816484375" Y="4.254122070313" />
                  <Point X="-27.118564453125" Y="4.171909179688" />
                  <Point X="-27.1118203125" Y="4.164048828125" />
                  <Point X="-27.097517578125" Y="4.149107421875" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.06508984375" Y="4.121895996094" />
                  <Point X="-27.047888671875" Y="4.110403320312" />
                  <Point X="-27.0389765625" Y="4.105128417969" />
                  <Point X="-27.008939453125" Y="4.0894921875" />
                  <Point X="-26.943759765625" Y="4.055561523438" />
                  <Point X="-26.93432421875" Y="4.051286621094" />
                  <Point X="-26.915044921875" Y="4.043789550781" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031378173828" />
                  <Point X="-26.780818359375" Y="4.035136474609" />
                  <Point X="-26.770728515625" Y="4.037488525391" />
                  <Point X="-26.7508671875" Y="4.043277587891" />
                  <Point X="-26.741095703125" Y="4.046714111328" />
                  <Point X="-26.709810546875" Y="4.059673095703" />
                  <Point X="-26.641921875" Y="4.087793701172" />
                  <Point X="-26.63258203125" Y="4.092274169922" />
                  <Point X="-26.614447265625" Y="4.102224121094" />
                  <Point X="-26.60565234375" Y="4.107693359375" />
                  <Point X="-26.587923828125" Y="4.120107421875" />
                  <Point X="-26.579779296875" Y="4.126499023438" />
                  <Point X="-26.564228515625" Y="4.140135742188" />
                  <Point X="-26.550251953125" Y="4.15538671875" />
                  <Point X="-26.538021484375" Y="4.17206640625" />
                  <Point X="-26.532361328125" Y="4.180740234375" />
                  <Point X="-26.5215390625" Y="4.199483886719" />
                  <Point X="-26.516853515625" Y="4.208728515625" />
                  <Point X="-26.508521484375" Y="4.227666503906" />
                  <Point X="-26.504875" Y="4.237359863281" />
                  <Point X="-26.494693359375" Y="4.269655761719" />
                  <Point X="-26.472595703125" Y="4.339736328125" />
                  <Point X="-26.470025390625" Y="4.349763671875" />
                  <Point X="-26.465990234375" Y="4.370046386719" />
                  <Point X="-26.464525390625" Y="4.380301757812" />
                  <Point X="-26.462638671875" Y="4.401863769531" />
                  <Point X="-26.46230078125" Y="4.412216796875" />
                  <Point X="-26.462751953125" Y="4.4328984375" />
                  <Point X="-26.463541015625" Y="4.443227050781" />
                  <Point X="-26.479263671875" Y="4.562655273438" />
                  <Point X="-26.437755859375" Y="4.57429296875" />
                  <Point X="-25.931173828125" Y="4.7163203125" />
                  <Point X="-25.70046875" Y="4.743321289062" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.28662890625" Y="4.489237792969" />
                  <Point X="-25.22566796875" Y="4.261728027344" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.629734375" Y="4.755416503906" />
                  <Point X="-24.621806640625" Y="4.785005859375" />
                  <Point X="-24.614453125" Y="4.784235839844" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.981240234375" Y="4.691827636719" />
                  <Point X="-23.54640234375" Y="4.58684375" />
                  <Point X="-23.42414453125" Y="4.542500488281" />
                  <Point X="-23.14173828125" Y="4.440069335938" />
                  <Point X="-23.02159765625" Y="4.383883300781" />
                  <Point X="-22.74955078125" Y="4.256655761719" />
                  <Point X="-22.63342578125" Y="4.189001464844" />
                  <Point X="-22.37056640625" Y="4.035859375" />
                  <Point X="-22.26109765625" Y="3.958010498047" />
                  <Point X="-22.182216796875" Y="3.901915527344" />
                  <Point X="-22.66287890625" Y="3.069385986328" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.937623046875" Y="2.593108398438" />
                  <Point X="-22.9468125" Y="2.573449951172" />
                  <Point X="-22.955814453125" Y="2.54957421875" />
                  <Point X="-22.958697265625" Y="2.540603027344" />
                  <Point X="-22.965470703125" Y="2.515275878906" />
                  <Point X="-22.98016796875" Y="2.460316650391" />
                  <Point X="-22.9820859375" Y="2.451469726562" />
                  <Point X="-22.986353515625" Y="2.420222900391" />
                  <Point X="-22.987244140625" Y="2.383240966797" />
                  <Point X="-22.986587890625" Y="2.369581787109" />
                  <Point X="-22.983947265625" Y="2.347681152344" />
                  <Point X="-22.978216796875" Y="2.300156982422" />
                  <Point X="-22.97619921875" Y="2.289038330078" />
                  <Point X="-22.970857421875" Y="2.267116210938" />
                  <Point X="-22.967533203125" Y="2.256312744141" />
                  <Point X="-22.959267578125" Y="2.234223876953" />
                  <Point X="-22.95468359375" Y="2.223891601562" />
                  <Point X="-22.944318359375" Y="2.203841308594" />
                  <Point X="-22.938537109375" Y="2.194123291016" />
                  <Point X="-22.924984375" Y="2.174151855469" />
                  <Point X="-22.895578125" Y="2.130814697266" />
                  <Point X="-22.8901875" Y="2.123628173828" />
                  <Point X="-22.869533203125" Y="2.100118896484" />
                  <Point X="-22.842400390625" Y="2.075385498047" />
                  <Point X="-22.8317421875" Y="2.066982910156" />
                  <Point X="-22.811771484375" Y="2.053431396484" />
                  <Point X="-22.76843359375" Y="2.024025512695" />
                  <Point X="-22.75872265625" Y="2.018247192383" />
                  <Point X="-22.738677734375" Y="2.007883789062" />
                  <Point X="-22.72834375" Y="2.003298339844" />
                  <Point X="-22.706255859375" Y="1.995032470703" />
                  <Point X="-22.695451171875" Y="1.991707519531" />
                  <Point X="-22.67352734375" Y="1.986364868164" />
                  <Point X="-22.662408203125" Y="1.984346801758" />
                  <Point X="-22.6405078125" Y="1.981705932617" />
                  <Point X="-22.592984375" Y="1.975975219727" />
                  <Point X="-22.583955078125" Y="1.975320922852" />
                  <Point X="-22.55242578125" Y="1.975497314453" />
                  <Point X="-22.5156875" Y="1.979822631836" />
                  <Point X="-22.50225390625" Y="1.982395507812" />
                  <Point X="-22.47692578125" Y="1.989168457031" />
                  <Point X="-22.421966796875" Y="2.003865112305" />
                  <Point X="-22.416005859375" Y="2.005670654297" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572875977" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.342369140625" Y="2.617690673828" />
                  <Point X="-21.05959375" Y="2.780950927734" />
                  <Point X="-20.956044921875" Y="2.637041503906" />
                  <Point X="-20.89501953125" Y="2.536195556641" />
                  <Point X="-20.863115234375" Y="2.483471679688" />
                  <Point X="-21.46918359375" Y="2.018417724609" />
                  <Point X="-21.827046875" Y="1.743819946289" />
                  <Point X="-21.831857421875" Y="1.739872802734" />
                  <Point X="-21.847880859375" Y="1.725217285156" />
                  <Point X="-21.86533203125" Y="1.706602905273" />
                  <Point X="-21.871421875" Y="1.699423828125" />
                  <Point X="-21.889650390625" Y="1.675644165039" />
                  <Point X="-21.929203125" Y="1.624042602539" />
                  <Point X="-21.934361328125" Y="1.616605102539" />
                  <Point X="-21.95026171875" Y="1.589367797852" />
                  <Point X="-21.965234375" Y="1.555548095703" />
                  <Point X="-21.96985546875" Y="1.54267980957" />
                  <Point X="-21.976646484375" Y="1.518400512695" />
                  <Point X="-21.991380859375" Y="1.465715332031" />
                  <Point X="-21.9937734375" Y="1.454670043945" />
                  <Point X="-21.997228515625" Y="1.432368896484" />
                  <Point X="-21.998291015625" Y="1.421112792969" />
                  <Point X="-21.999107421875" Y="1.397541748047" />
                  <Point X="-21.998826171875" Y="1.386236938477" />
                  <Point X="-21.996921875" Y="1.36375" />
                  <Point X="-21.995298828125" Y="1.352567749023" />
                  <Point X="-21.989724609375" Y="1.325554077148" />
                  <Point X="-21.97762890625" Y="1.266935180664" />
                  <Point X="-21.97540234375" Y="1.25823815918" />
                  <Point X="-21.96531640625" Y="1.228608520508" />
                  <Point X="-21.949712890625" Y="1.195370605469" />
                  <Point X="-21.943080078125" Y="1.183526733398" />
                  <Point X="-21.927919921875" Y="1.160483642578" />
                  <Point X="-21.8950234375" Y="1.110481201172" />
                  <Point X="-21.888263671875" Y="1.101427978516" />
                  <Point X="-21.8737109375" Y="1.084182617188" />
                  <Point X="-21.86591796875" Y="1.075990234375" />
                  <Point X="-21.848673828125" Y="1.059900512695" />
                  <Point X="-21.83996484375" Y="1.05269519043" />
                  <Point X="-21.82175390625" Y="1.039369018555" />
                  <Point X="-21.812251953125" Y="1.033247924805" />
                  <Point X="-21.79028125" Y="1.02088104248" />
                  <Point X="-21.742609375" Y="0.994045349121" />
                  <Point X="-21.734517578125" Y="0.989986328125" />
                  <Point X="-21.7053203125" Y="0.978083557129" />
                  <Point X="-21.66972265625" Y="0.968020935059" />
                  <Point X="-21.656328125" Y="0.96525769043" />
                  <Point X="-21.626623046875" Y="0.961331848145" />
                  <Point X="-21.562166015625" Y="0.952812927246" />
                  <Point X="-21.555966796875" Y="0.952199707031" />
                  <Point X="-21.53428125" Y="0.951222961426" />
                  <Point X="-21.50878125" Y="0.952032348633" />
                  <Point X="-21.49939453125" Y="0.952797180176" />
                  <Point X="-20.52894921875" Y="1.080559082031" />
                  <Point X="-20.295296875" Y="1.111319702148" />
                  <Point X="-20.24730859375" Y="0.914204101562" />
                  <Point X="-20.228083984375" Y="0.790712585449" />
                  <Point X="-20.21612890625" Y="0.713921020508" />
                  <Point X="-20.897888671875" Y="0.531243774414" />
                  <Point X="-21.3080078125" Y="0.421352722168" />
                  <Point X="-21.313970703125" Y="0.419543548584" />
                  <Point X="-21.334375" Y="0.412136627197" />
                  <Point X="-21.3576171875" Y="0.401619476318" />
                  <Point X="-21.365994140625" Y="0.39731729126" />
                  <Point X="-21.395177734375" Y="0.380448883057" />
                  <Point X="-21.45850390625" Y="0.343844848633" />
                  <Point X="-21.4661171875" Y="0.338945343018" />
                  <Point X="-21.491220703125" Y="0.319873474121" />
                  <Point X="-21.51800390625" Y="0.294352233887" />
                  <Point X="-21.527203125" Y="0.284226104736" />
                  <Point X="-21.544712890625" Y="0.261914245605" />
                  <Point X="-21.582708984375" Y="0.213498260498" />
                  <Point X="-21.58914453125" Y="0.204208587646" />
                  <Point X="-21.600869140625" Y="0.184928024292" />
                  <Point X="-21.606158203125" Y="0.174937149048" />
                  <Point X="-21.615931640625" Y="0.153473632812" />
                  <Point X="-21.61999609375" Y="0.142924377441" />
                  <Point X="-21.626841796875" Y="0.121422058105" />
                  <Point X="-21.629623046875" Y="0.11046900177" />
                  <Point X="-21.635458984375" Y="0.079992240906" />
                  <Point X="-21.648125" Y="0.013858761787" />
                  <Point X="-21.649396484375" Y="0.004965910435" />
                  <Point X="-21.6514140625" Y="-0.026261247635" />
                  <Point X="-21.64971875" Y="-0.062938407898" />
                  <Point X="-21.648125" Y="-0.076418655396" />
                  <Point X="-21.6422890625" Y="-0.10689541626" />
                  <Point X="-21.629623046875" Y="-0.173029190063" />
                  <Point X="-21.626841796875" Y="-0.183982254028" />
                  <Point X="-21.61999609375" Y="-0.205484558105" />
                  <Point X="-21.615931640625" Y="-0.216033813477" />
                  <Point X="-21.606158203125" Y="-0.237497344971" />
                  <Point X="-21.600869140625" Y="-0.247488220215" />
                  <Point X="-21.58914453125" Y="-0.266768768311" />
                  <Point X="-21.582708984375" Y="-0.276058441162" />
                  <Point X="-21.56519921875" Y="-0.298370147705" />
                  <Point X="-21.527203125" Y="-0.346786010742" />
                  <Point X="-21.521279296875" Y="-0.353635437012" />
                  <Point X="-21.498857421875" Y="-0.37580670166" />
                  <Point X="-21.469822265625" Y="-0.398724945068" />
                  <Point X="-21.45850390625" Y="-0.406404602051" />
                  <Point X="-21.4293203125" Y="-0.423273132324" />
                  <Point X="-21.365994140625" Y="-0.45987689209" />
                  <Point X="-21.360501953125" Y="-0.462815460205" />
                  <Point X="-21.340845703125" Y="-0.472014770508" />
                  <Point X="-21.316974609375" Y="-0.481026824951" />
                  <Point X="-21.3080078125" Y="-0.483912780762" />
                  <Point X="-20.418064453125" Y="-0.722372558594" />
                  <Point X="-20.215119140625" Y="-0.776751281738" />
                  <Point X="-20.238380859375" Y="-0.931036071777" />
                  <Point X="-20.2630234375" Y="-1.039024902344" />
                  <Point X="-20.2721953125" Y="-1.079219726563" />
                  <Point X="-21.087595703125" Y="-0.971870422363" />
                  <Point X="-21.563216796875" Y="-0.909253540039" />
                  <Point X="-21.571375" Y="-0.908535522461" />
                  <Point X="-21.59990234375" Y="-0.908042541504" />
                  <Point X="-21.63327734375" Y="-0.910840820312" />
                  <Point X="-21.645517578125" Y="-0.912676147461" />
                  <Point X="-21.702794921875" Y="-0.925125854492" />
                  <Point X="-21.82708203125" Y="-0.952140014648" />
                  <Point X="-21.842125" Y="-0.956743164062" />
                  <Point X="-21.871244140625" Y="-0.968367553711" />
                  <Point X="-21.8853203125" Y="-0.975388916016" />
                  <Point X="-21.913146484375" Y="-0.992280944824" />
                  <Point X="-21.92587109375" Y="-1.001528869629" />
                  <Point X="-21.949623046875" Y="-1.022000488281" />
                  <Point X="-21.960650390625" Y="-1.033224365234" />
                  <Point X="-21.99526953125" Y="-1.074861450195" />
                  <Point X="-22.07039453125" Y="-1.165212280273" />
                  <Point X="-22.07867578125" Y="-1.176851196289" />
                  <Point X="-22.09339453125" Y="-1.201232421875" />
                  <Point X="-22.09983203125" Y="-1.213974975586" />
                  <Point X="-22.11117578125" Y="-1.241360961914" />
                  <Point X="-22.115634765625" Y="-1.254926513672" />
                  <Point X="-22.122466796875" Y="-1.282574584961" />
                  <Point X="-22.12483984375" Y="-1.296657348633" />
                  <Point X="-22.129802734375" Y="-1.350579223633" />
                  <Point X="-22.140568359375" Y="-1.467587768555" />
                  <Point X="-22.140708984375" Y="-1.483313232422" />
                  <Point X="-22.138392578125" Y="-1.514580932617" />
                  <Point X="-22.13593359375" Y="-1.530123046875" />
                  <Point X="-22.128203125" Y="-1.561743530273" />
                  <Point X="-22.12321484375" Y="-1.576667358398" />
                  <Point X="-22.110841796875" Y="-1.605480957031" />
                  <Point X="-22.103458984375" Y="-1.619370605469" />
                  <Point X="-22.07176171875" Y="-1.668674316406" />
                  <Point X="-22.002978515625" Y="-1.775661254883" />
                  <Point X="-21.998255859375" Y="-1.782353271484" />
                  <Point X="-21.98019921875" Y="-1.804457397461" />
                  <Point X="-21.956505859375" Y="-1.828123291016" />
                  <Point X="-21.947201171875" Y="-1.836277709961" />
                  <Point X="-21.121328125" Y="-2.469992919922" />
                  <Point X="-20.912826171875" Y="-2.629981689453" />
                  <Point X="-20.95450390625" Y="-2.697421386719" />
                  <Point X="-20.998724609375" Y="-2.760252197266" />
                  <Point X="-21.727939453125" Y="-2.339239746094" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.18497265625" Y="-2.079513427734" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228892578125" Y="-2.066337646484" />
                  <Point X="-22.297060546875" Y="-2.054026611328" />
                  <Point X="-22.444982421875" Y="-2.027312011719" />
                  <Point X="-22.460640625" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.55515625" Y="-2.035136352539" />
                  <Point X="-22.584931640625" Y="-2.044960083008" />
                  <Point X="-22.59941015625" Y="-2.051108642578" />
                  <Point X="-22.656041015625" Y="-2.080913085938" />
                  <Point X="-22.778927734375" Y="-2.145587646484" />
                  <Point X="-22.79103125" Y="-2.153170410156" />
                  <Point X="-22.813962890625" Y="-2.170064208984" />
                  <Point X="-22.824791015625" Y="-2.179375244141" />
                  <Point X="-22.84575" Y="-2.200334716797" />
                  <Point X="-22.85505859375" Y="-2.211161621094" />
                  <Point X="-22.871951171875" Y="-2.234091308594" />
                  <Point X="-22.87953515625" Y="-2.246194091797" />
                  <Point X="-22.90933984375" Y="-2.302824951172" />
                  <Point X="-22.974015625" Y="-2.425712158203" />
                  <Point X="-22.9801640625" Y="-2.440193115234" />
                  <Point X="-22.98998828125" Y="-2.469971435547" />
                  <Point X="-22.9936640625" Y="-2.485268798828" />
                  <Point X="-22.99862109375" Y="-2.517441894531" />
                  <Point X="-22.999720703125" Y="-2.533139160156" />
                  <Point X="-22.999314453125" Y="-2.564491210938" />
                  <Point X="-22.99780859375" Y="-2.580145996094" />
                  <Point X="-22.98549609375" Y="-2.648313964844" />
                  <Point X="-22.95878125" Y="-2.796236328125" />
                  <Point X="-22.95698046875" Y="-2.804222900391" />
                  <Point X="-22.948763671875" Y="-2.831534912109" />
                  <Point X="-22.935931640625" Y="-2.862473876953" />
                  <Point X="-22.930453125" Y="-2.873578613281" />
                  <Point X="-22.399748046875" Y="-3.792789306641" />
                  <Point X="-22.26410546875" Y="-4.027725341797" />
                  <Point X="-22.276244140625" Y="-4.036082519531" />
                  <Point X="-22.84283203125" Y="-3.297690673828" />
                  <Point X="-23.16608203125" Y="-2.876422851562" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849625732422" />
                  <Point X="-23.21674609375" Y="-2.828004638672" />
                  <Point X="-23.22669921875" Y="-2.820646972656" />
                  <Point X="-23.293931640625" Y="-2.777422851562" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.453716796875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663873535156" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.665134765625" Y="-2.653282714844" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920125" Y="-2.699414794922" />
                  <Point X="-23.944505859375" Y="-2.714135742188" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-24.012916015625" Y="-2.769622070312" />
                  <Point X="-24.136123046875" Y="-2.872063720703" />
                  <Point X="-24.147345703125" Y="-2.883088378906" />
                  <Point X="-24.16781640625" Y="-2.906838378906" />
                  <Point X="-24.177064453125" Y="-2.919563720703" />
                  <Point X="-24.19395703125" Y="-2.947390625" />
                  <Point X="-24.200978515625" Y="-2.961466552734" />
                  <Point X="-24.212603515625" Y="-2.990586669922" />
                  <Point X="-24.21720703125" Y="-3.005630859375" />
                  <Point X="-24.23418359375" Y="-3.083735107422" />
                  <Point X="-24.271021484375" Y="-3.25321875" />
                  <Point X="-24.2724140625" Y="-3.261287353516" />
                  <Point X="-24.275275390625" Y="-3.289677490234" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520019531" />
                  <Point X="-24.166912109375" Y="-4.1523203125" />
                  <Point X="-24.218564453125" Y="-3.959547363281" />
                  <Point X="-24.344931640625" Y="-3.487937011719" />
                  <Point X="-24.347392578125" Y="-3.480122314453" />
                  <Point X="-24.357853515625" Y="-3.453578857422" />
                  <Point X="-24.3732109375" Y="-3.423815429688" />
                  <Point X="-24.37958984375" Y="-3.413209228516" />
                  <Point X="-24.431240234375" Y="-3.338791748047" />
                  <Point X="-24.543318359375" Y="-3.177308837891" />
                  <Point X="-24.553328125" Y="-3.165171386719" />
                  <Point X="-24.575212890625" Y="-3.142715820312" />
                  <Point X="-24.587087890625" Y="-3.132397705078" />
                  <Point X="-24.61334375" Y="-3.113153808594" />
                  <Point X="-24.6267578125" Y="-3.104936767578" />
                  <Point X="-24.6547578125" Y="-3.090829345703" />
                  <Point X="-24.66934375" Y="-3.084938964844" />
                  <Point X="-24.749267578125" Y="-3.060133056641" />
                  <Point X="-24.922703125" Y="-3.006305664062" />
                  <Point X="-24.936623046875" Y="-3.003109619141" />
                  <Point X="-24.964783203125" Y="-2.998840087891" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.998840087891" />
                  <Point X="-25.051064453125" Y="-3.003109619141" />
                  <Point X="-25.064984375" Y="-3.006305664062" />
                  <Point X="-25.144908203125" Y="-3.031111328125" />
                  <Point X="-25.31834375" Y="-3.084938964844" />
                  <Point X="-25.3329296875" Y="-3.090829345703" />
                  <Point X="-25.3609296875" Y="-3.104936767578" />
                  <Point X="-25.37434375" Y="-3.113153808594" />
                  <Point X="-25.400599609375" Y="-3.132397705078" />
                  <Point X="-25.412474609375" Y="-3.142717773438" />
                  <Point X="-25.434359375" Y="-3.165174560547" />
                  <Point X="-25.444369140625" Y="-3.177311279297" />
                  <Point X="-25.496017578125" Y="-3.251728515625" />
                  <Point X="-25.608095703125" Y="-3.413211669922" />
                  <Point X="-25.61246875" Y="-3.420130615234" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213867188" />
                  <Point X="-25.64275390625" Y="-3.487936767578" />
                  <Point X="-25.916966796875" Y="-4.511315917969" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.588259759685" Y="1.312269648458" />
                  <Point X="-29.692993220932" Y="0.567052349381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.494068907199" Y="1.299869217604" />
                  <Point X="-29.600541129365" Y="0.542279991731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.768066999154" Y="-0.649728509885" />
                  <Point X="-29.777527999375" Y="-0.717047024402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.399878054713" Y="1.28746878675" />
                  <Point X="-29.508089040775" Y="0.517507612891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.668379356899" Y="-0.623017249248" />
                  <Point X="-29.725899220387" Y="-1.032292344342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.141778766561" Y="2.441337476271" />
                  <Point X="-29.168789853833" Y="2.249143603727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.305687202226" Y="1.275068355896" />
                  <Point X="-29.415636966531" Y="0.492735131978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.568691714644" Y="-0.596305988612" />
                  <Point X="-29.664015274956" Y="-1.274568363484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.01542093809" Y="2.657816972398" />
                  <Point X="-29.082194710859" Y="2.182696891388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.21149634974" Y="1.262667925042" />
                  <Point X="-29.323184892287" Y="0.467962651065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.469004072389" Y="-0.569594727976" />
                  <Point X="-29.568067883028" Y="-1.274470366776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.897372345202" Y="2.815173185242" />
                  <Point X="-28.995599587929" Y="2.116250036428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.117305497254" Y="1.250267494188" />
                  <Point X="-29.230732818043" Y="0.443190170152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.369316430134" Y="-0.54288346734" />
                  <Point X="-29.470325781866" Y="-1.261602350327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.780288007795" Y="2.965668363838" />
                  <Point X="-28.909004465" Y="2.049803181469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.023114643244" Y="1.237867074174" />
                  <Point X="-29.138280743799" Y="0.418417689239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.269628787879" Y="-0.516172206703" />
                  <Point X="-29.372583680704" Y="-1.248734333878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.678490939772" Y="3.007388968711" />
                  <Point X="-28.82240934207" Y="1.983356326509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.928923789171" Y="1.225466654606" />
                  <Point X="-29.045828669555" Y="0.393645208326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.169941145624" Y="-0.489460946067" />
                  <Point X="-29.274841582431" Y="-1.235866337987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.589757285786" Y="2.956158552879" />
                  <Point X="-28.73581421914" Y="1.91690947155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.834732935099" Y="1.213066235037" />
                  <Point X="-28.953376595311" Y="0.368872727413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.070253503369" Y="-0.462749685431" />
                  <Point X="-29.177099484641" Y="-1.222998345532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.5010236318" Y="2.904928137047" />
                  <Point X="-28.64921909621" Y="1.85046261659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.739989762676" Y="1.204595764734" />
                  <Point X="-28.860924521067" Y="0.3441002465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.970565861587" Y="-0.436038428163" />
                  <Point X="-29.079357386851" Y="-1.210130353077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.412289999084" Y="2.853697569868" />
                  <Point X="-28.56262397328" Y="1.784015761631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.641161281975" Y="1.225193773262" />
                  <Point X="-28.768472446823" Y="0.319327765587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.870878227482" Y="-0.409327225511" />
                  <Point X="-28.981615289061" Y="-1.197262360622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.153780886153" Y="-2.422284237403" />
                  <Point X="-29.161421479955" Y="-2.476649887204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.323556384473" Y="2.802466873873" />
                  <Point X="-28.479404565517" Y="1.693549445183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.538348070286" Y="1.274144616014" />
                  <Point X="-28.676020372578" Y="0.294555284674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.771190593376" Y="-0.38261602286" />
                  <Point X="-28.883873191271" Y="-1.184394368167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.046251177297" Y="-2.339773773516" />
                  <Point X="-29.083784369258" Y="-2.60683631118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.234552916435" Y="2.753156284774" />
                  <Point X="-28.583568298334" Y="0.269782803761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.67150295927" Y="-0.355904820208" />
                  <Point X="-28.786131093482" Y="-1.171526375712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.938721468442" Y="-2.257263309629" />
                  <Point X="-29.006147004511" Y="-2.737020927496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.999060609328" Y="3.746167945856" />
                  <Point X="-28.016098489431" Y="3.624937129638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.141873245875" Y="2.730003235797" />
                  <Point X="-28.491332469986" Y="0.243471653346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.571815325164" Y="-0.329193617556" />
                  <Point X="-28.688388995692" Y="-1.158658383257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.831191759586" Y="-2.174752845742" />
                  <Point X="-28.925257365183" Y="-2.844064407722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.891541730655" Y="3.82860134899" />
                  <Point X="-27.938945721916" Y="3.49130442485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.046839045092" Y="2.723603539875" />
                  <Point X="-28.402561072479" Y="0.192509796621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.471467714962" Y="-0.297786440976" />
                  <Point X="-28.590646897902" Y="-1.145790390802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.72366205073" Y="-2.092242381855" />
                  <Point X="-28.844276266766" Y="-2.950457122728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.784022851982" Y="3.911034752123" />
                  <Point X="-27.861792954401" Y="3.357671720062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.947779263271" Y="2.745847341389" />
                  <Point X="-28.32122765039" Y="0.08862399481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.364654869809" Y="-0.220376727369" />
                  <Point X="-28.492904800112" Y="-1.132922398348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.616132341875" Y="-2.009731917968" />
                  <Point X="-28.754677496998" Y="-2.995531919915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.676503973309" Y="3.993468155256" />
                  <Point X="-27.784716651059" Y="3.223494944418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.839702025494" Y="2.832253675989" />
                  <Point X="-28.395162702322" Y="-1.120054405893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.508602633019" Y="-1.92722145408" />
                  <Point X="-28.650272272197" Y="-2.93525331527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.57153985687" Y="4.057723480544" />
                  <Point X="-28.297420604533" Y="-1.107186413438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.401072901671" Y="-1.844710830151" />
                  <Point X="-28.545867058903" Y="-2.874974792501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.467481269564" Y="4.115535631252" />
                  <Point X="-28.200535940944" Y="-1.100419382338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.293543151074" Y="-1.762200069259" />
                  <Point X="-28.441461885311" Y="-2.814696552221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.36342262275" Y="4.173348205384" />
                  <Point X="-28.107853389352" Y="-1.12355193171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.186013400477" Y="-1.679689308367" />
                  <Point X="-28.337056711718" Y="-2.754418311941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.259363970012" Y="4.231160821666" />
                  <Point X="-28.020165599193" Y="-1.182224055348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.07848364988" Y="-1.597178547474" />
                  <Point X="-28.232651538125" Y="-2.69414007166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.163523132591" Y="4.230500643658" />
                  <Point X="-27.947355781887" Y="-1.34675845656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.959811123553" Y="-1.435382817531" />
                  <Point X="-28.128246364533" Y="-2.63386183138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.081071824365" Y="4.134569015021" />
                  <Point X="-28.02384119094" Y="-2.5735835911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.992664071322" Y="4.081019693482" />
                  <Point X="-27.919436017347" Y="-2.513305350819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.902516738776" Y="4.039848123272" />
                  <Point X="-27.815030843755" Y="-2.453027110539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.80787069993" Y="4.030686511664" />
                  <Point X="-27.710625670162" Y="-2.392748870259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.890947965068" Y="-3.675808667701" />
                  <Point X="-27.919478127785" Y="-3.878811323674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.707742916089" Y="4.060529542412" />
                  <Point X="-27.609136525879" Y="-2.353219256635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.764148257255" Y="-3.456185036686" />
                  <Point X="-27.832911956917" Y="-3.945464183256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.605129390048" Y="4.108059547954" />
                  <Point X="-27.513337465153" Y="-2.354176691274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.637348549443" Y="-3.236561405672" />
                  <Point X="-27.746321407216" Y="-4.011943578433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.443913574791" Y="4.572566507838" />
                  <Point X="-26.463030399865" Y="4.436543229517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.479884362471" Y="4.316621054291" />
                  <Point X="-27.423365246424" Y="-2.396594261036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.510548845675" Y="-3.016937803436" />
                  <Point X="-27.658067602753" Y="-4.066588301034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.344044861609" Y="4.600566155069" />
                  <Point X="-27.340650942655" Y="-2.490654579154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.383749145659" Y="-2.797314227895" />
                  <Point X="-27.569813798291" Y="-4.121233023636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.244176157123" Y="4.628565740421" />
                  <Point X="-27.474402032193" Y="-4.124946202748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.144307452637" Y="4.656565325774" />
                  <Point X="-27.359918365683" Y="-3.992955759119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.044438748151" Y="4.684564911126" />
                  <Point X="-27.25397691554" Y="-3.921746343184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.944570043665" Y="4.712564496478" />
                  <Point X="-27.148100764166" Y="-3.851001552137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.846719435323" Y="4.726204581625" />
                  <Point X="-27.043873538539" Y="-3.79198947742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.749181468046" Y="4.737620110012" />
                  <Point X="-26.945682274046" Y="-3.775925497811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.651643516741" Y="4.749035524747" />
                  <Point X="-26.850624286417" Y="-3.782155941526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.554105581373" Y="4.760450826092" />
                  <Point X="-26.755566298789" Y="-3.788386385242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.456567646004" Y="4.771866127437" />
                  <Point X="-26.660508297213" Y="-3.794616729725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.361227221762" Y="4.767645324649" />
                  <Point X="-26.567305356222" Y="-3.814046516117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.298299746673" Y="4.532794404839" />
                  <Point X="-26.47903485516" Y="-3.868572436241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.2353721273" Y="4.297944511667" />
                  <Point X="-26.393627801951" Y="-3.943472846522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.159834771589" Y="4.152817554642" />
                  <Point X="-26.308220768002" Y="-4.018373393849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.071391569988" Y="4.099520462707" />
                  <Point X="-26.22430919813" Y="-4.103914720982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.977461737163" Y="4.085262780453" />
                  <Point X="-26.158717730684" Y="-4.319810350232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.877170204792" Y="4.116270942533" />
                  <Point X="-26.121339006811" Y="-4.736450080881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.759442169977" Y="4.271346266174" />
                  <Point X="-26.02849673848" Y="-4.758446186607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.591760616946" Y="4.781859340851" />
                  <Point X="-25.876723157129" Y="-4.361124211961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.497218496888" Y="4.771958308644" />
                  <Point X="-25.674967076124" Y="-3.608158272636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.402676376829" Y="4.762057276437" />
                  <Point X="-25.537299875973" Y="-3.311208415678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.308134256771" Y="4.75215624423" />
                  <Point X="-25.418564722304" Y="-3.148967069037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.213592136712" Y="4.742255212023" />
                  <Point X="-25.313417672071" Y="-3.083410102174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.120036185911" Y="4.72533722094" />
                  <Point X="-25.213108737486" Y="-3.052278116905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.027250851987" Y="4.702936005867" />
                  <Point X="-25.11279977801" Y="-3.021145954523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.934465528758" Y="4.680534714686" />
                  <Point X="-25.013633048942" Y="-2.998141183811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.841680216052" Y="4.65813334864" />
                  <Point X="-24.919008045161" Y="-3.007452467683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.748894903345" Y="4.635731982595" />
                  <Point X="-24.827083987853" Y="-3.03598198432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.656109590638" Y="4.613330616549" />
                  <Point X="-24.735159949046" Y="-3.064511632594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.563324277931" Y="4.590929250504" />
                  <Point X="-24.643707510507" Y="-3.096396891134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.471769228883" Y="4.55977410367" />
                  <Point X="-24.556921349968" Y="-3.161484442876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.380488632861" Y="4.526666122085" />
                  <Point X="-24.476701164713" Y="-3.273291336352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.289208072691" Y="4.493557885405" />
                  <Point X="-24.396922109718" Y="-3.388237034722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.19792751252" Y="4.460449648725" />
                  <Point X="-24.325298259868" Y="-3.561210032859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.107132667882" Y="4.423885366449" />
                  <Point X="-24.132033570889" Y="-2.868663487251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.237299673201" Y="-3.617670724434" />
                  <Point X="-24.262370637731" Y="-3.796059906361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.017115564024" Y="4.381787170974" />
                  <Point X="-24.023406435693" Y="-2.778344429208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.190899328352" Y="-3.970118286349" />
                  <Point X="-24.199443122383" Y="-4.030910539712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.92709844663" Y="4.339689071815" />
                  <Point X="-23.916093610635" Y="-2.697377173731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.837081329236" Y="4.297590972656" />
                  <Point X="-23.815912896124" Y="-2.667157521694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.74710133649" Y="4.255228717904" />
                  <Point X="-23.718722334814" Y="-2.658213915208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.658428180682" Y="4.203567835167" />
                  <Point X="-22.867680936055" Y="2.714657115258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.940397654046" Y="2.197250781752" />
                  <Point X="-23.621531765703" Y="-2.649270253217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.569755040858" Y="4.151906838694" />
                  <Point X="-22.740881090576" Y="2.934281725822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.859411719471" Y="2.090892477808" />
                  <Point X="-23.525969384473" Y="-2.651911749975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.481081907311" Y="4.10024579756" />
                  <Point X="-22.614081336032" Y="3.153905689345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.772488920739" Y="2.026777157329" />
                  <Point X="-23.434934980173" Y="-2.686771476679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.392408773765" Y="4.048584756426" />
                  <Point X="-22.487281726848" Y="3.373528618584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.681946532667" Y="1.988416553249" />
                  <Point X="-23.34695106229" Y="-2.743336542083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.304833336918" Y="3.989113197427" />
                  <Point X="-22.360482117664" Y="3.593151547823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.587814083536" Y="1.975600560925" />
                  <Point X="-23.258967160544" Y="-2.799901722304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.217616571337" Y="3.927089559765" />
                  <Point X="-22.233682508479" Y="3.812774477062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.490483129954" Y="1.985543110334" />
                  <Point X="-23.172707374273" Y="-2.868734621573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.390348014767" Y="2.015438306319" />
                  <Point X="-23.091512210792" Y="-2.973604184497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.286370799087" Y="2.072671467826" />
                  <Point X="-22.89838004979" Y="-2.282000624444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.965456542162" Y="-2.759274667354" />
                  <Point X="-23.010429428246" Y="-3.07927337932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.181965614798" Y="2.132949784219" />
                  <Point X="-22.783694543692" Y="-2.148574017523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.894370653641" Y="-2.936075459239" />
                  <Point X="-22.929346645699" Y="-3.184942574143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.077560430508" Y="2.193228100611" />
                  <Point X="-22.680025859077" Y="-2.093536168609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.817217906121" Y="-3.069708306304" />
                  <Point X="-22.848263863153" Y="-3.290611768966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.973155246218" Y="2.253506417004" />
                  <Point X="-22.576892557157" Y="-2.042307765522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.740065158602" Y="-3.203341153368" />
                  <Point X="-22.767181076549" Y="-3.396280934924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.868750061929" Y="2.313784733397" />
                  <Point X="-21.984461056785" Y="1.490458224052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.998868618928" Y="1.387943092607" />
                  <Point X="-22.478607413759" Y="-2.025575802788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.662912411083" Y="-3.336974000433" />
                  <Point X="-22.686098289655" Y="-3.501950098809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.764344877639" Y="2.374063049789" />
                  <Point X="-21.856883824804" Y="1.715614226991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.933673308619" Y="1.169228658861" />
                  <Point X="-22.384454107379" Y="-2.038243388074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.585759663564" Y="-3.470606847498" />
                  <Point X="-22.60501550276" Y="-3.607619262694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.65993969335" Y="2.434341366182" />
                  <Point X="-21.748517492119" Y="1.804077578748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.852591080094" Y="1.06355552197" />
                  <Point X="-22.290895161886" Y="-2.055140070813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.508606916045" Y="-3.604239694563" />
                  <Point X="-22.523932715866" Y="-3.71328842658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.55553450906" Y="2.494619682575" />
                  <Point X="-21.640987803595" Y="1.886587897965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.764680355759" Y="1.006469657412" />
                  <Point X="-22.069787794486" Y="-1.16448257418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.126384044168" Y="-1.567185815567" />
                  <Point X="-22.19781092571" Y="-2.075414485854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.431454168526" Y="-3.737872541628" />
                  <Point X="-22.442849928971" Y="-3.818957590465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.451129324771" Y="2.554897998967" />
                  <Point X="-21.533458115071" Y="1.969098217182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.673981171336" Y="0.969224717341" />
                  <Point X="-21.954531799259" Y="-1.026996726178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.049565750308" Y="-1.70319842407" />
                  <Point X="-22.108108108515" Y="-2.119748947137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.354301252505" Y="-3.871504189737" />
                  <Point X="-22.361767142077" Y="-3.92462675435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.346724140481" Y="2.61517631536" />
                  <Point X="-21.425928404263" Y="2.051608694964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.580022405356" Y="0.955172905183" />
                  <Point X="-21.849116962978" Y="-0.959534362577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.969381693255" Y="-1.815262383062" />
                  <Point X="-22.019374462156" Y="-2.170979417239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.277148218926" Y="-4.005135001386" />
                  <Point X="-22.280684355182" Y="-4.030295918235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.242318960114" Y="2.675454603841" />
                  <Point X="-21.318398660341" Y="2.134119408361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.484140431797" Y="0.954805426011" />
                  <Point X="-21.589775841916" Y="0.203170427235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.637134928504" Y="-0.133806983553" />
                  <Point X="-21.749782882206" Y="-0.935338822612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.883293449567" Y="-1.88531587123" />
                  <Point X="-21.930640815797" Y="-2.222209887341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.137913779918" Y="2.735732891108" />
                  <Point X="-21.210868916419" Y="2.216630121758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.386398329087" Y="0.967673453475" />
                  <Point X="-21.475793467472" Y="0.331593992483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.564461301123" Y="-0.299310426434" />
                  <Point X="-21.650826404059" Y="-0.91383006493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.796698323557" Y="-1.951762704274" />
                  <Point X="-21.841907169438" Y="-2.273440357443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.039541737691" Y="2.753083171139" />
                  <Point X="-21.103339172497" Y="2.299140835155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.288656226377" Y="0.980541480939" />
                  <Point X="-21.371032311615" Y="0.394405178188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.481233591651" Y="-0.389717673152" />
                  <Point X="-21.554412498491" Y="-0.910412651195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.710103197547" Y="-2.018209537318" />
                  <Point X="-21.753173523079" Y="-2.324670827545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.959284045629" Y="2.641543152467" />
                  <Point X="-20.995809428575" Y="2.381651548552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.190914123666" Y="0.993409508403" />
                  <Point X="-21.269875482656" Y="0.431570245409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.392968903486" Y="-0.444284954187" />
                  <Point X="-21.46022165501" Y="-0.922813146127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.623508071537" Y="-2.084656370361" />
                  <Point X="-21.664439870432" Y="-2.375901252901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.881334459538" Y="2.513580106445" />
                  <Point X="-20.888279684653" Y="2.464162261949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.093172020956" Y="1.006277535867" />
                  <Point X="-21.170187847293" Y="0.458281457008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.302800700464" Y="-0.485308023241" />
                  <Point X="-21.366030811529" Y="-0.935213641058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.536912945527" Y="-2.151103203405" />
                  <Point X="-21.575706215285" Y="-2.427131660475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.995429918246" Y="1.019145563331" />
                  <Point X="-21.07050021193" Y="0.484992668607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.210348625641" Y="-0.51008050004" />
                  <Point X="-21.271839968048" Y="-0.947614135989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.450317819517" Y="-2.217550036449" />
                  <Point X="-21.486972560139" Y="-2.47836206805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.897687815536" Y="1.032013590795" />
                  <Point X="-20.970812576566" Y="0.511703880206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.117896550819" Y="-0.534852976839" />
                  <Point X="-21.177649124567" Y="-0.96001463092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.363722693507" Y="-2.283996869492" />
                  <Point X="-21.398238904992" Y="-2.529592475624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.799945712826" Y="1.044881618259" />
                  <Point X="-20.871124939286" Y="0.538415105448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.025444475997" Y="-0.559625453637" />
                  <Point X="-21.083458280737" Y="-0.972415123371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.277127567497" Y="-2.350443702536" />
                  <Point X="-21.309505249846" Y="-2.580822883199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.702203610116" Y="1.057749645723" />
                  <Point X="-20.771437296781" Y="0.565126367861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.932992401174" Y="-0.584397930436" />
                  <Point X="-20.98926742932" Y="-0.984815561828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.190532441487" Y="-2.41689053558" />
                  <Point X="-21.220771594699" Y="-2.632053290774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.604461507406" Y="1.070617673187" />
                  <Point X="-20.671749654276" Y="0.591837630274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.840540326352" Y="-0.609170407235" />
                  <Point X="-20.895076577902" Y="-0.997216000285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.103937309757" Y="-2.483337327924" />
                  <Point X="-21.132037939552" Y="-2.683283698348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.506719409865" Y="1.083485663873" />
                  <Point X="-20.572062011771" Y="0.618548892687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.74808825153" Y="-0.633942884033" />
                  <Point X="-20.800885726484" Y="-1.009616438742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.017342155266" Y="-2.549783958312" />
                  <Point X="-21.043304284406" Y="-2.734514105923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.408977329882" Y="1.096353529624" />
                  <Point X="-20.472374369267" Y="0.6452601551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.655636176707" Y="-0.658715360832" />
                  <Point X="-20.706694875066" Y="-1.022016877198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.930747000774" Y="-2.6162305887" />
                  <Point X="-20.938523476216" Y="-2.671563086601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.3112352499" Y="1.109221395375" />
                  <Point X="-20.372686726762" Y="0.671971417513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.563184101885" Y="-0.683487837631" />
                  <Point X="-20.612504023648" Y="-1.034417315655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.244891567613" Y="0.898678052825" />
                  <Point X="-20.272999084257" Y="0.698682679927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.470732027063" Y="-0.708260314429" />
                  <Point X="-20.51831317223" Y="-1.046817754112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.378279942974" Y="-0.733032725297" />
                  <Point X="-20.424122320812" Y="-1.059218192569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.285827846619" Y="-0.757805048885" />
                  <Point X="-20.329931469395" Y="-1.071618631026" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.402091796875" Y="-4.00872265625" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.587330078125" Y="-3.447126708984" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.805587890625" Y="-3.241593994141" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.088587890625" Y="-3.212572265625" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643798828" />
                  <Point X="-25.339927734375" Y="-3.360061035156" />
                  <Point X="-25.452005859375" Y="-3.521544189453" />
                  <Point X="-25.4592265625" Y="-3.537112060547" />
                  <Point X="-25.733439453125" Y="-4.560491699219" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.896537109375" Y="-4.97760546875" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.19206640625" Y="-4.914440429688" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.324361328125" Y="-4.666583007812" />
                  <Point X="-26.3091484375" Y="-4.5510390625" />
                  <Point X="-26.309681640625" Y="-4.5347578125" />
                  <Point X="-26.32877734375" Y="-4.438765625" />
                  <Point X="-26.3702109375" Y="-4.230466308594" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.4598671875" Y="-4.138096679688" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.746904296875" Y="-3.979361816406" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791015625" />
                  <Point X="-27.071255859375" Y="-4.028166259766" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.41368359375" Y="-4.357925292969" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.55723828125" Y="-4.352493164063" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-27.98298046875" Y="-4.069711914062" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.740873046875" Y="-3.035874267578" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597594238281" />
                  <Point X="-27.513978515625" Y="-2.568765380859" />
                  <Point X="-27.531326171875" Y="-2.551416748047" />
                  <Point X="-27.560154296875" Y="-2.537198730469" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.474328125" Y="-3.053064941406" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-28.925798828125" Y="-3.157060546875" />
                  <Point X="-29.16169921875" Y="-2.847135742188" />
                  <Point X="-29.252857421875" Y="-2.694277832031" />
                  <Point X="-29.43101953125" Y="-2.395526855469" />
                  <Point X="-28.5746796875" Y="-1.738434814453" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.152537109375" Y="-1.411083862305" />
                  <Point X="-28.143390625" Y="-1.386628051758" />
                  <Point X="-28.1381171875" Y="-1.366264892578" />
                  <Point X="-28.13665234375" Y="-1.350049926758" />
                  <Point X="-28.14865625" Y="-1.321067871094" />
                  <Point X="-28.16951171875" Y="-1.305721801758" />
                  <Point X="-28.187640625" Y="-1.295052246094" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.333783203125" Y="-1.43526550293" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.835126953125" Y="-1.372401855469" />
                  <Point X="-29.927392578125" Y="-1.011187438965" />
                  <Point X="-29.951509765625" Y="-0.842567932129" />
                  <Point X="-29.99839453125" Y="-0.514742248535" />
                  <Point X="-29.025533203125" Y="-0.254064346313" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.533138671875" Y="-0.115348609924" />
                  <Point X="-28.514140625" Y="-0.102163024902" />
                  <Point X="-28.502322265625" Y="-0.090644767761" />
                  <Point X="-28.491978515625" Y="-0.066503471375" />
                  <Point X="-28.485646484375" Y="-0.046099510193" />
                  <Point X="-28.483400390625" Y="-0.031280040741" />
                  <Point X="-28.488564453125" Y="-0.007057653904" />
                  <Point X="-28.494896484375" Y="0.013346308708" />
                  <Point X="-28.502322265625" Y="0.028084686279" />
                  <Point X="-28.522896484375" Y="0.045679382324" />
                  <Point X="-28.54189453125" Y="0.058864971161" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.573158203125" Y="0.338240325928" />
                  <Point X="-29.998185546875" Y="0.452126037598" />
                  <Point X="-29.977107421875" Y="0.594573120117" />
                  <Point X="-29.91764453125" Y="0.996414794922" />
                  <Point X="-29.869095703125" Y="1.17557409668" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.0887890625" Y="1.438152832031" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.712326171875" Y="1.401976318359" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.651533203125" Y="1.426056518555" />
                  <Point X="-28.6391171875" Y="1.443786743164" />
                  <Point X="-28.631341796875" Y="1.462558105469" />
                  <Point X="-28.614470703125" Y="1.503291137695" />
                  <Point X="-28.61071484375" Y="1.524606445312" />
                  <Point X="-28.61631640625" Y="1.545512817383" />
                  <Point X="-28.625697265625" Y="1.56353515625" />
                  <Point X="-28.646056640625" Y="1.602642700195" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-29.24257421875" Y="2.066270996094" />
                  <Point X="-29.47610546875" Y="2.245466308594" />
                  <Point X="-29.39106640625" Y="2.391159423828" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-29.0314140625" Y="2.952303466797" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.365224609375" Y="3.045917724609" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.111525390625" Y="2.918073730469" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927404296875" />
                  <Point X="-27.994095703125" Y="2.946560058594" />
                  <Point X="-27.95252734375" Y="2.988127685547" />
                  <Point X="-27.9408984375" Y="3.006382080078" />
                  <Point X="-27.938072265625" Y="3.027840820312" />
                  <Point X="-27.94043359375" Y="3.054828369141" />
                  <Point X="-27.945556640625" Y="3.113390380859" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.210236328125" Y="3.581197021484" />
                  <Point X="-28.30727734375" Y="3.749276855469" />
                  <Point X="-28.155294921875" Y="3.865801269531" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.550333984375" Y="4.286858398438" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.03091015625" Y="4.369786132812" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.921208984375" Y="4.258024414062" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.782521484375" Y="4.235209472656" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.696904296875" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.294487792969" />
                  <Point X="-26.675900390625" Y="4.326783691406" />
                  <Point X="-26.653802734375" Y="4.396864257812" />
                  <Point X="-26.651916015625" Y="4.418426269531" />
                  <Point X="-26.681267578125" Y="4.641370117188" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.489048828125" Y="4.757238769531" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.7225546875" Y="4.932033203125" />
                  <Point X="-25.22419921875" Y="4.990358398438" />
                  <Point X="-25.1031015625" Y="4.538413085938" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.81326171875" Y="4.804592285156" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.5946640625" Y="4.973202636719" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.936650390625" Y="4.876520996094" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.359359375" Y="4.721114746094" />
                  <Point X="-23.068970703125" Y="4.615788085938" />
                  <Point X="-22.941109375" Y="4.555991699219" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.53778125" Y="4.353171875" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-22.150984375" Y="4.112849121094" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.4983359375" Y="2.974385986328" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491515136719" />
                  <Point X="-22.781919921875" Y="2.466187988281" />
                  <Point X="-22.7966171875" Y="2.411228759766" />
                  <Point X="-22.797955078125" Y="2.392325927734" />
                  <Point X="-22.795314453125" Y="2.370425292969" />
                  <Point X="-22.789583984375" Y="2.322901123047" />
                  <Point X="-22.781318359375" Y="2.300812255859" />
                  <Point X="-22.767765625" Y="2.280840820312" />
                  <Point X="-22.738359375" Y="2.237503662109" />
                  <Point X="-22.72505859375" Y="2.224203613281" />
                  <Point X="-22.705087890625" Y="2.210652099609" />
                  <Point X="-22.66175" Y="2.18124609375" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.61776171875" Y="2.170339355469" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5513359375" Y="2.165946289062" />
                  <Point X="-22.5260078125" Y="2.172719238281" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.437369140625" Y="2.782235595703" />
                  <Point X="-21.005751953125" Y="3.031430419922" />
                  <Point X="-20.957744140625" Y="2.964710449219" />
                  <Point X="-20.79740234375" Y="2.741874267578" />
                  <Point X="-20.73246484375" Y="2.634562255859" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.35351953125" Y="1.867680664062" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583832519531" />
                  <Point X="-21.738857421875" Y="1.560052734375" />
                  <Point X="-21.77841015625" Y="1.508451171875" />
                  <Point X="-21.78687890625" Y="1.491500366211" />
                  <Point X="-21.793669921875" Y="1.467221069336" />
                  <Point X="-21.808404296875" Y="1.414535888672" />
                  <Point X="-21.809220703125" Y="1.39096484375" />
                  <Point X="-21.803646484375" Y="1.363951049805" />
                  <Point X="-21.79155078125" Y="1.30533215332" />
                  <Point X="-21.784353515625" Y="1.287955444336" />
                  <Point X="-21.769193359375" Y="1.264912475586" />
                  <Point X="-21.736296875" Y="1.21491003418" />
                  <Point X="-21.719052734375" Y="1.19882019043" />
                  <Point X="-21.69708203125" Y="1.18645324707" />
                  <Point X="-21.64941015625" Y="1.159617675781" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.601728515625" Y="1.149693847656" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.55375" Y="1.26893359375" />
                  <Point X="-20.1510234375" Y="1.321953125" />
                  <Point X="-20.12967578125" Y="1.234254394531" />
                  <Point X="-20.060806640625" Y="0.951366821289" />
                  <Point X="-20.040345703125" Y="0.819940368652" />
                  <Point X="-20.002140625" Y="0.574556335449" />
                  <Point X="-20.848712890625" Y="0.347717773438" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819198608" />
                  <Point X="-21.300095703125" Y="0.215950866699" />
                  <Point X="-21.363421875" Y="0.179346832275" />
                  <Point X="-21.377734375" Y="0.166926696777" />
                  <Point X="-21.395244140625" Y="0.144614883423" />
                  <Point X="-21.433240234375" Y="0.096199020386" />
                  <Point X="-21.443013671875" Y="0.074735565186" />
                  <Point X="-21.448849609375" Y="0.044258766174" />
                  <Point X="-21.461515625" Y="-0.02187484169" />
                  <Point X="-21.461515625" Y="-0.04068523407" />
                  <Point X="-21.4556796875" Y="-0.071162033081" />
                  <Point X="-21.443013671875" Y="-0.137295639038" />
                  <Point X="-21.433240234375" Y="-0.158759094238" />
                  <Point X="-21.41573046875" Y="-0.181070907593" />
                  <Point X="-21.377734375" Y="-0.22948677063" />
                  <Point X="-21.363421875" Y="-0.241906906128" />
                  <Point X="-21.33423828125" Y="-0.258775390625" />
                  <Point X="-21.270912109375" Y="-0.295379272461" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.368888671875" Y="-0.538846679688" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.0131171875" Y="-0.711367492676" />
                  <Point X="-20.051568359375" Y="-0.966412597656" />
                  <Point X="-20.07778515625" Y="-1.081296264648" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-21.112396484375" Y="-1.160244995117" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.662439453125" Y="-1.110790649414" />
                  <Point X="-21.7867265625" Y="-1.137805053711" />
                  <Point X="-21.814552734375" Y="-1.154697021484" />
                  <Point X="-21.849171875" Y="-1.196334106445" />
                  <Point X="-21.924296875" Y="-1.286685058594" />
                  <Point X="-21.935640625" Y="-1.314071166992" />
                  <Point X="-21.940603515625" Y="-1.367993041992" />
                  <Point X="-21.951369140625" Y="-1.485001464844" />
                  <Point X="-21.943638671875" Y="-1.516622070312" />
                  <Point X="-21.91194140625" Y="-1.56592565918" />
                  <Point X="-21.843158203125" Y="-1.672912719727" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-21.0056640625" Y="-2.319255615234" />
                  <Point X="-20.660923828125" Y="-2.583784179688" />
                  <Point X="-20.687478515625" Y="-2.626753417969" />
                  <Point X="-20.7958671875" Y="-2.802140136719" />
                  <Point X="-20.850091796875" Y="-2.879186035156" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.822939453125" Y="-2.503784667969" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.330826171875" Y="-2.241001708984" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.567552734375" Y="-2.249049316406" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.334683349609" />
                  <Point X="-22.741203125" Y="-2.391314208984" />
                  <Point X="-22.80587890625" Y="-2.514201416016" />
                  <Point X="-22.8108359375" Y="-2.546374511719" />
                  <Point X="-22.7985234375" Y="-2.614542480469" />
                  <Point X="-22.77180859375" Y="-2.76246484375" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.235201171875" Y="-3.697789306641" />
                  <Point X="-22.01332421875" Y="-4.082089111328" />
                  <Point X="-22.036087890625" Y="-4.098348632812" />
                  <Point X="-22.164685546875" Y="-4.190202148438" />
                  <Point X="-22.225326171875" Y="-4.229453613281" />
                  <Point X="-22.32022265625" Y="-4.29087890625" />
                  <Point X="-22.9935703125" Y="-3.413355224609" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.396681640625" Y="-2.937243164062" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.647724609375" Y="-2.842483398438" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.891443359375" Y="-2.915717773438" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045986328125" />
                  <Point X="-24.04851953125" Y="-3.124090576172" />
                  <Point X="-24.085357421875" Y="-3.29357421875" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-23.935671875" Y="-4.453107910156" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.8839765625" Y="-4.936575683594" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.061662109375" Y="-4.973421386719" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#148" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.06096105203" Y="4.582634228643" Z="0.8" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.8" />
                  <Point X="-0.734523202698" Y="5.012974273047" Z="0.8" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.8" />
                  <Point X="-1.508703818612" Y="4.836662241728" Z="0.8" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.8" />
                  <Point X="-1.736996940898" Y="4.66612412805" Z="0.8" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.8" />
                  <Point X="-1.729742312464" Y="4.373099843406" Z="0.8" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.8" />
                  <Point X="-1.80781358704" Y="4.31268355068" Z="0.8" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.8" />
                  <Point X="-1.904278255919" Y="4.333654967541" Z="0.8" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.8" />
                  <Point X="-1.997399298944" Y="4.431504154989" Z="0.8" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.8" />
                  <Point X="-2.580774539162" Y="4.361846143208" Z="0.8" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.8" />
                  <Point X="-3.19187294104" Y="3.936761127657" Z="0.8" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.8" />
                  <Point X="-3.25969499491" Y="3.587476974855" Z="0.8" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.8" />
                  <Point X="-2.996401132513" Y="3.081751009208" Z="0.8" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.8" />
                  <Point X="-3.035607668061" Y="3.013195899825" Z="0.8" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.8" />
                  <Point X="-3.113325456399" Y="2.99916356666" Z="0.8" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.8" />
                  <Point X="-3.346382373442" Y="3.120498953836" Z="0.8" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.8" />
                  <Point X="-4.077034053908" Y="3.01428585996" Z="0.8" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.8" />
                  <Point X="-4.44040425705" Y="2.447644560713" Z="0.8" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.8" />
                  <Point X="-4.27916817307" Y="2.05788328882" Z="0.8" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.8" />
                  <Point X="-3.676204187547" Y="1.571726565253" Z="0.8" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.8" />
                  <Point X="-3.683694599879" Y="1.512971365389" Z="0.8" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.8" />
                  <Point X="-3.733518534205" Y="1.480942242495" Z="0.8" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.8" />
                  <Point X="-4.08841994626" Y="1.519005104384" Z="0.8" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.8" />
                  <Point X="-4.923513272879" Y="1.219931229473" Z="0.8" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.8" />
                  <Point X="-5.03272542084" Y="0.633172221906" Z="0.8" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.8" />
                  <Point X="-4.592257188919" Y="0.321224080653" Z="0.8" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.8" />
                  <Point X="-3.557562799386" Y="0.035883379362" Z="0.8" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.8" />
                  <Point X="-3.542475105177" Y="0.009402919699" Z="0.8" />
                  <Point X="-3.539556741714" Y="0" Z="0.8" />
                  <Point X="-3.545889495958" Y="-0.020404031362" Z="0.8" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.8" />
                  <Point X="-3.567805795737" Y="-0.042992603772" Z="0.8" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.8" />
                  <Point X="-4.044630599854" Y="-0.174487976103" Z="0.8" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.8" />
                  <Point X="-5.007162078061" Y="-0.818366922321" Z="0.8" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.8" />
                  <Point X="-4.889729755139" Y="-1.353496006647" Z="0.8" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.8" />
                  <Point X="-4.333414031213" Y="-1.453557752385" Z="0.8" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.8" />
                  <Point X="-3.201030517925" Y="-1.317532877622" Z="0.8" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.8" />
                  <Point X="-3.197950336836" Y="-1.342813047014" Z="0.8" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.8" />
                  <Point X="-3.611274524366" Y="-1.667486999192" Z="0.8" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.8" />
                  <Point X="-4.301956997537" Y="-2.688607470617" Z="0.8" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.8" />
                  <Point X="-3.971740315579" Y="-3.156063705373" Z="0.8" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.8" />
                  <Point X="-3.455484135518" Y="-3.065086082304" Z="0.8" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.8" />
                  <Point X="-2.560963748236" Y="-2.567366720896" Z="0.8" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.8" />
                  <Point X="-2.790330930864" Y="-2.979594246974" Z="0.8" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.8" />
                  <Point X="-3.01964103592" Y="-4.078049003647" Z="0.8" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.8" />
                  <Point X="-2.589717831024" Y="-4.363723842738" Z="0.8" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.8" />
                  <Point X="-2.380172083985" Y="-4.35708340384" Z="0.8" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.8" />
                  <Point X="-2.049634203106" Y="-4.038459626868" Z="0.8" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.8" />
                  <Point X="-1.756330038109" Y="-3.997974744045" Z="0.8" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.8" />
                  <Point X="-1.49899060124" Y="-4.144408436467" Z="0.8" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.8" />
                  <Point X="-1.383972561133" Y="-4.417240170402" Z="0.8" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.8" />
                  <Point X="-1.380090209615" Y="-4.628776319243" Z="0.8" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.8" />
                  <Point X="-1.210682668534" Y="-4.931583225641" Z="0.8" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.8" />
                  <Point X="-0.912222281846" Y="-4.995409647229" Z="0.8" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.8" />
                  <Point X="-0.691300449121" Y="-4.542152497085" Z="0.8" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.8" />
                  <Point X="-0.305008723737" Y="-3.357289967401" Z="0.8" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.8" />
                  <Point X="-0.07992481597" Y="-3.229045081815" Z="0.8" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.8" />
                  <Point X="0.173434263391" Y="-3.258066963473" Z="0.8" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.8" />
                  <Point X="0.365437135695" Y="-3.444355788367" Z="0.8" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.8" />
                  <Point X="0.54345441128" Y="-3.99038353273" Z="0.8" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.8" />
                  <Point X="0.941119332093" Y="-4.991336363966" Z="0.8" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.8" />
                  <Point X="1.120572014285" Y="-4.954135888832" Z="0.8" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.8" />
                  <Point X="1.107744004794" Y="-4.415301464399" Z="0.8" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.8" />
                  <Point X="0.99418374417" Y="-3.103430180487" Z="0.8" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.8" />
                  <Point X="1.134365967848" Y="-2.9228843048" Z="0.8" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.8" />
                  <Point X="1.350700776399" Y="-2.860992857236" Z="0.8" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.8" />
                  <Point X="1.570121841219" Y="-2.948021269843" Z="0.8" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.8" />
                  <Point X="1.960604129846" Y="-3.412513088919" Z="0.8" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.8" />
                  <Point X="2.79568717443" Y="-4.240147696871" Z="0.8" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.8" />
                  <Point X="2.986815140796" Y="-4.107755482724" Z="0.8" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.8" />
                  <Point X="2.801943637122" Y="-3.641509276087" Z="0.8" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.8" />
                  <Point X="2.244522211419" Y="-2.574376208962" Z="0.8" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.8" />
                  <Point X="2.296885765186" Y="-2.383321054278" Z="0.8" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.8" />
                  <Point X="2.449577324094" Y="-2.262015544781" Z="0.8" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.8" />
                  <Point X="2.65413059455" Y="-2.258925798072" Z="0.8" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.8" />
                  <Point X="3.14590432102" Y="-2.515805863152" Z="0.8" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.8" />
                  <Point X="4.184640529501" Y="-2.876683254438" Z="0.8" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.8" />
                  <Point X="4.348896898227" Y="-2.621758686907" Z="0.8" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.8" />
                  <Point X="4.018616103612" Y="-2.248307969555" Z="0.8" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.8" />
                  <Point X="3.123960091393" Y="-1.507606008013" Z="0.8" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.8" />
                  <Point X="3.103029592278" Y="-1.341294001797" Z="0.8" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.8" />
                  <Point X="3.183115594356" Y="-1.197021216493" Z="0.8" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.8" />
                  <Point X="3.342023460896" Y="-1.128369686754" Z="0.8" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.8" />
                  <Point X="3.874922029413" Y="-1.178537266246" Z="0.8" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.8" />
                  <Point X="4.964803527481" Y="-1.061140378552" Z="0.8" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.8" />
                  <Point X="5.030167386865" Y="-0.687541522291" Z="0.8" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.8" />
                  <Point X="4.637896602181" Y="-0.459270375917" Z="0.8" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.8" />
                  <Point X="3.684625973261" Y="-0.184206653915" Z="0.8" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.8" />
                  <Point X="3.617446678851" Y="-0.118922328818" Z="0.8" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.8" />
                  <Point X="3.587271378429" Y="-0.030476819516" Z="0.8" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.8" />
                  <Point X="3.594100071995" Y="0.066133711709" Z="0.8" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.8" />
                  <Point X="3.637932759549" Y="0.145026409757" Z="0.8" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.8" />
                  <Point X="3.718769441091" Y="0.203942190327" Z="0.8" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.8" />
                  <Point X="4.158071215125" Y="0.330701556851" Z="0.8" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.8" />
                  <Point X="5.002902444808" Y="0.858912629305" Z="0.8" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.8" />
                  <Point X="4.912750277234" Y="1.277361316826" Z="0.8" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.8" />
                  <Point X="4.433568263972" Y="1.349785874014" Z="0.8" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.8" />
                  <Point X="3.39866464319" Y="1.230542830637" Z="0.8" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.8" />
                  <Point X="3.321287882657" Y="1.261304200703" Z="0.8" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.8" />
                  <Point X="3.266421263028" Y="1.323673464248" Z="0.8" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.8" />
                  <Point X="3.239165895503" Y="1.405335427249" Z="0.8" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.8" />
                  <Point X="3.24832602616" Y="1.485034433335" Z="0.8" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.8" />
                  <Point X="3.294670158012" Y="1.560915237067" Z="0.8" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.8" />
                  <Point X="3.670761030836" Y="1.859292930868" Z="0.8" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.8" />
                  <Point X="4.304155596219" Y="2.691728596281" Z="0.8" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.8" />
                  <Point X="4.076685522253" Y="3.0251936075" Z="0.8" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.8" />
                  <Point X="3.531472997249" Y="2.856816926974" Z="0.8" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.8" />
                  <Point X="2.454918811659" Y="2.252301986253" Z="0.8" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.8" />
                  <Point X="2.382067600904" Y="2.251259695577" Z="0.8" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.8" />
                  <Point X="2.316829479953" Y="2.283306658084" Z="0.8" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.8" />
                  <Point X="2.26745194215" Y="2.34019538043" Z="0.8" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.8" />
                  <Point X="2.248170007182" Y="2.407690842928" Z="0.8" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.8" />
                  <Point X="2.260225956404" Y="2.484550764223" Z="0.8" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.8" />
                  <Point X="2.538808425886" Y="2.980666084706" Z="0.8" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.8" />
                  <Point X="2.871836191273" Y="4.1848763031" Z="0.8" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.8" />
                  <Point X="2.481232493129" Y="4.42765442242" Z="0.8" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.8" />
                  <Point X="2.073916313032" Y="4.632563540811" Z="0.8" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.8" />
                  <Point X="1.651531908916" Y="4.799398114016" Z="0.8" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.8" />
                  <Point X="1.068926359353" Y="4.956404446444" Z="0.8" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.8" />
                  <Point X="0.404386812742" Y="5.054210799554" Z="0.8" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.8" />
                  <Point X="0.132283509523" Y="4.848813277308" Z="0.8" />
                  <Point X="0" Y="4.355124473572" Z="0.8" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>