<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#191" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2749" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.144734375" Y="-4.602142578125" />
                  <Point X="-24.4366953125" Y="-3.512524414063" />
                  <Point X="-24.44227734375" Y="-3.497141357422" />
                  <Point X="-24.457634765625" Y="-3.467376708984" />
                  <Point X="-24.576966796875" Y="-3.29544140625" />
                  <Point X="-24.62136328125" Y="-3.231476318359" />
                  <Point X="-24.64325" Y="-3.209019775391" />
                  <Point X="-24.669505859375" Y="-3.189776367188" />
                  <Point X="-24.69750390625" Y="-3.175669189453" />
                  <Point X="-24.8821640625" Y="-3.118357421875" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.221484375" Y="-3.154347412109" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.189777587891" />
                  <Point X="-25.344439453125" Y="-3.209021972656" />
                  <Point X="-25.36632421875" Y="-3.231477294922" />
                  <Point X="-25.48565625" Y="-3.403412353516" />
                  <Point X="-25.53005078125" Y="-3.467377685547" />
                  <Point X="-25.5381875" Y="-3.481571533203" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.659609375" Y="-3.917895996094" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.002341796875" Y="-4.860295898438" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.24641796875" Y="-4.802362792969" />
                  <Point X="-26.2149609375" Y="-4.563437011719" />
                  <Point X="-26.21419921875" Y="-4.547929199219" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.260623046875" Y="-4.29444140625" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.182963867188" />
                  <Point X="-26.30401171875" Y="-4.155126953125" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.493654296875" Y="-3.982107666016" />
                  <Point X="-26.556904296875" Y="-3.926639404297" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.868669921875" Y="-3.876176757812" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708740234" />
                  <Point X="-27.014466796875" Y="-3.882028564453" />
                  <Point X="-27.042658203125" Y="-3.894801757812" />
                  <Point X="-27.23067578125" Y="-4.020431396484" />
                  <Point X="-27.300623046875" Y="-4.067169677734" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.335099609375" Y="-4.099461425781" />
                  <Point X="-27.39608203125" Y="-4.17893359375" />
                  <Point X="-27.480146484375" Y="-4.288489746094" />
                  <Point X="-27.6888515625" Y="-4.159265625" />
                  <Point X="-27.801712890625" Y="-4.089384033203" />
                  <Point X="-28.09428515625" Y="-3.864114013672" />
                  <Point X="-28.10472265625" Y="-3.856077880859" />
                  <Point X="-27.96633984375" Y="-3.616393798828" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647654541016" />
                  <Point X="-27.406587890625" Y="-2.616128173828" />
                  <Point X="-27.40557421875" Y="-2.585194091797" />
                  <Point X="-27.41455859375" Y="-2.555575927734" />
                  <Point X="-27.428775390625" Y="-2.526747070312" />
                  <Point X="-27.44680078125" Y="-2.501591552734" />
                  <Point X="-27.46414453125" Y="-2.484246582031" />
                  <Point X="-27.489298828125" Y="-2.466219726562" />
                  <Point X="-27.51812890625" Y="-2.451999511719" />
                  <Point X="-27.54774609375" Y="-2.443012695312" />
                  <Point X="-27.57868359375" Y="-2.444023925781" />
                  <Point X="-27.610212890625" Y="-2.450294433594" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-27.98880859375" Y="-2.663053466797" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-28.993697265625" Y="-2.911002441406" />
                  <Point X="-29.082857421875" Y="-2.793862548828" />
                  <Point X="-29.29261328125" Y="-2.442137695313" />
                  <Point X="-29.306142578125" Y="-2.419450439453" />
                  <Point X="-29.055291015625" Y="-2.226965332031" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.08458203125" Y="-1.475598754883" />
                  <Point X="-28.0666171875" Y="-1.448471679688" />
                  <Point X="-28.053857421875" Y="-1.4198359375" />
                  <Point X="-28.0482421875" Y="-1.398154663086" />
                  <Point X="-28.04615234375" Y="-1.390088500977" />
                  <Point X="-28.04334765625" Y="-1.359666625977" />
                  <Point X="-28.0455546875" Y="-1.327992919922" />
                  <Point X="-28.0525546875" Y="-1.298242797852" />
                  <Point X="-28.068640625" Y="-1.272255249023" />
                  <Point X="-28.0894765625" Y="-1.24829675293" />
                  <Point X="-28.11297265625" Y="-1.228766845703" />
                  <Point X="-28.1322734375" Y="-1.217406616211" />
                  <Point X="-28.139455078125" Y="-1.213180419922" />
                  <Point X="-28.168720703125" Y="-1.201954956055" />
                  <Point X="-28.20060546875" Y="-1.195474609375" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-28.673296875" Y="-1.252491088867" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.799203125" Y="-1.129180908203" />
                  <Point X="-29.834076171875" Y="-0.992650634766" />
                  <Point X="-29.889572265625" Y="-0.604637756348" />
                  <Point X="-29.892423828125" Y="-0.584698364258" />
                  <Point X="-29.6143125" Y="-0.510178741455" />
                  <Point X="-28.532875" Y="-0.220408309937" />
                  <Point X="-28.517494140625" Y="-0.214828048706" />
                  <Point X="-28.487728515625" Y="-0.199470657349" />
                  <Point X="-28.4675" Y="-0.185431564331" />
                  <Point X="-28.459974609375" Y="-0.180208602905" />
                  <Point X="-28.437515625" Y="-0.158319366455" />
                  <Point X="-28.418271484375" Y="-0.132060699463" />
                  <Point X="-28.404166015625" Y="-0.1040625" />
                  <Point X="-28.397423828125" Y="-0.08233795929" />
                  <Point X="-28.394916015625" Y="-0.074255577087" />
                  <Point X="-28.390646484375" Y="-0.046097396851" />
                  <Point X="-28.390646484375" Y="-0.016462751389" />
                  <Point X="-28.394916015625" Y="0.01169527626" />
                  <Point X="-28.401658203125" Y="0.033420124054" />
                  <Point X="-28.404166015625" Y="0.041502353668" />
                  <Point X="-28.418271484375" Y="0.069500549316" />
                  <Point X="-28.437515625" Y="0.095759376526" />
                  <Point X="-28.459974609375" Y="0.117648612976" />
                  <Point X="-28.480203125" Y="0.131687698364" />
                  <Point X="-28.489306640625" Y="0.137274810791" />
                  <Point X="-28.512400390625" Y="0.149717971802" />
                  <Point X="-28.532875" Y="0.157848327637" />
                  <Point X="-28.935203125" Y="0.265651824951" />
                  <Point X="-29.891814453125" Y="0.521975402832" />
                  <Point X="-29.8469609375" Y="0.825098693848" />
                  <Point X="-29.82448828125" Y="0.976968505859" />
                  <Point X="-29.71277734375" Y="1.389219360352" />
                  <Point X="-29.70355078125" Y="1.423268066406" />
                  <Point X="-29.540865234375" Y="1.401850097656" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228027344" />
                  <Point X="-28.703134765625" Y="1.305263793945" />
                  <Point X="-28.658365234375" Y="1.319379760742" />
                  <Point X="-28.641708984375" Y="1.324631225586" />
                  <Point X="-28.62277734375" Y="1.332961181641" />
                  <Point X="-28.604033203125" Y="1.343782836914" />
                  <Point X="-28.5873515625" Y="1.356014160156" />
                  <Point X="-28.573712890625" Y="1.37156628418" />
                  <Point X="-28.561298828125" Y="1.389295532227" />
                  <Point X="-28.551349609375" Y="1.407432495117" />
                  <Point X="-28.53338671875" Y="1.450802001953" />
                  <Point X="-28.526703125" Y="1.466936767578" />
                  <Point X="-28.520916015625" Y="1.486787597656" />
                  <Point X="-28.51715625" Y="1.508103515625" />
                  <Point X="-28.515802734375" Y="1.528749511719" />
                  <Point X="-28.518951171875" Y="1.549198730469" />
                  <Point X="-28.5245546875" Y="1.570106201172" />
                  <Point X="-28.53205078125" Y="1.589378662109" />
                  <Point X="-28.5537265625" Y="1.631017456055" />
                  <Point X="-28.561791015625" Y="1.646508300781" />
                  <Point X="-28.573283203125" Y="1.663707763672" />
                  <Point X="-28.5871953125" Y="1.680287231445" />
                  <Point X="-28.60213671875" Y="1.694590087891" />
                  <Point X="-28.832912109375" Y="1.871671264648" />
                  <Point X="-29.351859375" Y="2.269873291016" />
                  <Point X="-29.1684765625" Y="2.584052246094" />
                  <Point X="-29.081146484375" Y="2.733665771484" />
                  <Point X="-28.78523828125" Y="3.114016357422" />
                  <Point X="-28.75050390625" Y="3.158661865234" />
                  <Point X="-28.682763671875" Y="3.119551757813" />
                  <Point X="-28.20665625" Y="2.844671142578" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.084439453125" Y="2.820341308594" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.99901171875" Y="2.826505126953" />
                  <Point X="-27.980458984375" Y="2.835655029297" />
                  <Point X="-27.962205078125" Y="2.847284912109" />
                  <Point X="-27.946078125" Y="2.860228759766" />
                  <Point X="-27.901818359375" Y="2.904486816406" />
                  <Point X="-27.885353515625" Y="2.920952148438" />
                  <Point X="-27.872404296875" Y="2.937085449219" />
                  <Point X="-27.860775390625" Y="2.955339355469" />
                  <Point X="-27.85162890625" Y="2.973888427734" />
                  <Point X="-27.8467109375" Y="2.993977783203" />
                  <Point X="-27.843884765625" Y="3.015436035156" />
                  <Point X="-27.84343359375" Y="3.036120605469" />
                  <Point X="-27.848888671875" Y="3.098473144531" />
                  <Point X="-27.85091796875" Y="3.121670166016" />
                  <Point X="-27.854955078125" Y="3.141962158203" />
                  <Point X="-27.86146484375" Y="3.162604736328" />
                  <Point X="-27.869794921875" Y="3.181533447266" />
                  <Point X="-27.972060546875" Y="3.358659912109" />
                  <Point X="-28.18333203125" Y="3.724595703125" />
                  <Point X="-27.852708984375" Y="3.978080810547" />
                  <Point X="-27.70062109375" Y="4.094685791016" />
                  <Point X="-27.2345703125" Y="4.353613769531" />
                  <Point X="-27.16703515625" Y="4.391134277344" />
                  <Point X="-27.0431953125" Y="4.229740722656" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.995111328125" Y="4.18939453125" />
                  <Point X="-26.925712890625" Y="4.153268066406" />
                  <Point X="-26.89989453125" Y="4.139827636719" />
                  <Point X="-26.8806171875" Y="4.132331054688" />
                  <Point X="-26.8597109375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.12358203125" />
                  <Point X="-26.818626953125" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.777453125" Y="4.134481933594" />
                  <Point X="-26.705171875" Y="4.164422851562" />
                  <Point X="-26.678279296875" Y="4.175561523437" />
                  <Point X="-26.6601484375" Y="4.185507324219" />
                  <Point X="-26.64241796875" Y="4.197920898438" />
                  <Point X="-26.626865234375" Y="4.211559082031" />
                  <Point X="-26.6146328125" Y="4.228241210938" />
                  <Point X="-26.603810546875" Y="4.246984863281" />
                  <Point X="-26.595478515625" Y="4.265920898438" />
                  <Point X="-26.571953125" Y="4.340537597656" />
                  <Point X="-26.56319921875" Y="4.368297363281" />
                  <Point X="-26.5591640625" Y="4.388583007812" />
                  <Point X="-26.55727734375" Y="4.410146484375" />
                  <Point X="-26.557728515625" Y="4.430827636719" />
                  <Point X="-26.56935546875" Y="4.519138183594" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.146517578125" Y="4.754609375" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.384638671875" Y="4.875932617188" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.27475" Y="4.811958984375" />
                  <Point X="-25.133904296875" Y="4.286315917969" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286314941406" />
                  <Point X="-24.8013828125" Y="4.481870605469" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.327873046875" Y="4.849743164062" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.688517578125" Y="4.718884277344" />
                  <Point X="-23.51897265625" Y="4.677950683594" />
                  <Point X="-23.215115234375" Y="4.567739746094" />
                  <Point X="-23.10535546875" Y="4.527929199219" />
                  <Point X="-22.8111484375" Y="4.390338378906" />
                  <Point X="-22.705423828125" Y="4.34089453125" />
                  <Point X="-22.421185546875" Y="4.175296386719" />
                  <Point X="-22.3190234375" Y="4.115775878906" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.224421875" Y="3.638816162109" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857919921875" Y="2.5399375" />
                  <Point X="-22.866921875" Y="2.51605859375" />
                  <Point X="-22.8825703125" Y="2.457542236328" />
                  <Point X="-22.888392578125" Y="2.435772216797" />
                  <Point X="-22.891380859375" Y="2.417936767578" />
                  <Point X="-22.892271484375" Y="2.380953857422" />
                  <Point X="-22.886169921875" Y="2.330353759766" />
                  <Point X="-22.883900390625" Y="2.311529052734" />
                  <Point X="-22.878556640625" Y="2.289603027344" />
                  <Point X="-22.8702890625" Y="2.267512695312" />
                  <Point X="-22.859927734375" Y="2.247469482422" />
                  <Point X="-22.8286171875" Y="2.201327148438" />
                  <Point X="-22.81696875" Y="2.184160888672" />
                  <Point X="-22.805533203125" Y="2.170329345703" />
                  <Point X="-22.778400390625" Y="2.145592529297" />
                  <Point X="-22.7322578125" Y="2.114282958984" />
                  <Point X="-22.715091796875" Y="2.102635009766" />
                  <Point X="-22.695046875" Y="2.092272705078" />
                  <Point X="-22.672958984375" Y="2.084006591797" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.600435546875" Y="2.072562011719" />
                  <Point X="-22.59141015625" Y="2.071907714844" />
                  <Point X="-22.55368359375" Y="2.070975341797" />
                  <Point X="-22.526794921875" Y="2.074170898438" />
                  <Point X="-22.46827734375" Y="2.089819091797" />
                  <Point X="-22.4465078125" Y="2.095640625" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-22.00680078125" Y="2.343778320312" />
                  <Point X="-21.032673828125" Y="2.906190429688" />
                  <Point X="-20.937330078125" Y="2.773684082031" />
                  <Point X="-20.87671875" Y="2.689450439453" />
                  <Point X="-20.737798828125" Y="2.459884033203" />
                  <Point X="-20.942400390625" Y="2.302887451172" />
                  <Point X="-21.76921484375" Y="1.668451171875" />
                  <Point X="-21.7785703125" Y="1.660245605469" />
                  <Point X="-21.79602734375" Y="1.641626953125" />
                  <Point X="-21.838140625" Y="1.586685424805" />
                  <Point X="-21.85380859375" Y="1.566245605469" />
                  <Point X="-21.863392578125" Y="1.550915283203" />
                  <Point X="-21.878369140625" Y="1.517089355469" />
                  <Point X="-21.894056640625" Y="1.460994018555" />
                  <Point X="-21.89989453125" Y="1.440125" />
                  <Point X="-21.90334765625" Y="1.417827026367" />
                  <Point X="-21.9041640625" Y="1.394253417969" />
                  <Point X="-21.902259765625" Y="1.371766601562" />
                  <Point X="-21.889380859375" Y="1.309353515625" />
                  <Point X="-21.88458984375" Y="1.286133789062" />
                  <Point X="-21.879318359375" Y="1.268976318359" />
                  <Point X="-21.863716796875" Y="1.23574206543" />
                  <Point X="-21.828689453125" Y="1.182503051758" />
                  <Point X="-21.81566015625" Y="1.162696533203" />
                  <Point X="-21.801111328125" Y="1.145456176758" />
                  <Point X="-21.7838671875" Y="1.129364501953" />
                  <Point X="-21.76565234375" Y="1.116034667969" />
                  <Point X="-21.714892578125" Y="1.087462280273" />
                  <Point X="-21.696009765625" Y="1.076832397461" />
                  <Point X="-21.67948046875" Y="1.069502563477" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.575251953125" Y="1.050368408203" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-21.127390625" Y="1.097592285156" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.180091796875" Y="1.039723510742" />
                  <Point X="-20.15405859375" Y="0.932786804199" />
                  <Point X="-20.1091328125" Y="0.644239013672" />
                  <Point X="-20.335873046875" Y="0.583484069824" />
                  <Point X="-21.283419921875" Y="0.32958972168" />
                  <Point X="-21.2952109375" Y="0.325585357666" />
                  <Point X="-21.318453125" Y="0.315067810059" />
                  <Point X="-21.38587890625" Y="0.276094543457" />
                  <Point X="-21.410962890625" Y="0.261595367432" />
                  <Point X="-21.425685546875" Y="0.251098312378" />
                  <Point X="-21.45246875" Y="0.22557673645" />
                  <Point X="-21.492923828125" Y="0.174027053833" />
                  <Point X="-21.507974609375" Y="0.154848937988" />
                  <Point X="-21.51969921875" Y="0.135569137573" />
                  <Point X="-21.52947265625" Y="0.114106430054" />
                  <Point X="-21.536318359375" Y="0.092604255676" />
                  <Point X="-21.549802734375" Y="0.022190063477" />
                  <Point X="-21.5548203125" Y="-0.004006225109" />
                  <Point X="-21.556515625" Y="-0.021874078751" />
                  <Point X="-21.5548203125" Y="-0.058553920746" />
                  <Point X="-21.5413359375" Y="-0.128968109131" />
                  <Point X="-21.536318359375" Y="-0.155164260864" />
                  <Point X="-21.52947265625" Y="-0.176666427612" />
                  <Point X="-21.51969921875" Y="-0.19812928772" />
                  <Point X="-21.507974609375" Y="-0.217409088135" />
                  <Point X="-21.46751953125" Y="-0.268958770752" />
                  <Point X="-21.45246875" Y="-0.28813671875" />
                  <Point X="-21.439998046875" Y="-0.301238525391" />
                  <Point X="-21.410962890625" Y="-0.32415536499" />
                  <Point X="-21.343537109375" Y="-0.363128631592" />
                  <Point X="-21.318453125" Y="-0.377627929688" />
                  <Point X="-21.307291015625" Y="-0.383137786865" />
                  <Point X="-21.283419921875" Y="-0.392149871826" />
                  <Point X="-20.93090234375" Y="-0.486606445312" />
                  <Point X="-20.108525390625" Y="-0.706961730957" />
                  <Point X="-20.130443359375" Y="-0.852333129883" />
                  <Point X="-20.144974609375" Y="-0.948725341797" />
                  <Point X="-20.198826171875" Y="-1.18469921875" />
                  <Point X="-20.47673046875" Y="-1.148112304688" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710144043" />
                  <Point X="-21.62533984375" Y="-1.005508728027" />
                  <Point X="-21.757671875" Y="-1.034271850586" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.836025390625" Y="-1.056596923828" />
                  <Point X="-21.8638515625" Y="-1.073489135742" />
                  <Point X="-21.8876015625" Y="-1.093960083008" />
                  <Point X="-21.967587890625" Y="-1.190159057617" />
                  <Point X="-21.997345703125" Y="-1.225947998047" />
                  <Point X="-22.01206640625" Y="-1.250330810547" />
                  <Point X="-22.02341015625" Y="-1.277717651367" />
                  <Point X="-22.030240234375" Y="-1.305366210938" />
                  <Point X="-22.041705078125" Y="-1.429948242188" />
                  <Point X="-22.04596875" Y="-1.476296508789" />
                  <Point X="-22.043650390625" Y="-1.507563232422" />
                  <Point X="-22.035919921875" Y="-1.539183349609" />
                  <Point X="-22.023548828125" Y="-1.567996948242" />
                  <Point X="-21.950314453125" Y="-1.681908813477" />
                  <Point X="-21.923068359375" Y="-1.724287475586" />
                  <Point X="-21.9130625" Y="-1.737243774414" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.562232421875" Y="-2.011930664062" />
                  <Point X="-20.786876953125" Y="-2.606882324219" />
                  <Point X="-20.83422265625" Y="-2.683495361328" />
                  <Point X="-20.875201171875" Y="-2.749803466797" />
                  <Point X="-20.971017578125" Y="-2.8859453125" />
                  <Point X="-21.220337890625" Y="-2.742000244141" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.245775390625" Y="-2.159825195312" />
                  <Point X="-22.403271484375" Y="-2.131381591797" />
                  <Point X="-22.461865234375" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395507812" />
                  <Point X="-22.525390625" Y="-2.125353515625" />
                  <Point X="-22.555166015625" Y="-2.135177001953" />
                  <Point X="-22.6860078125" Y="-2.204037597656" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.757615234375" Y="-2.246549072266" />
                  <Point X="-22.77857421875" Y="-2.267508056641" />
                  <Point X="-22.795466796875" Y="-2.290438476562" />
                  <Point X="-22.864328125" Y="-2.421279541016" />
                  <Point X="-22.889947265625" Y="-2.469956542969" />
                  <Point X="-22.899771484375" Y="-2.499734863281" />
                  <Point X="-22.904728515625" Y="-2.531908203125" />
                  <Point X="-22.90432421875" Y="-2.563259277344" />
                  <Point X="-22.87587890625" Y="-2.720755859375" />
                  <Point X="-22.865296875" Y="-2.779349609375" />
                  <Point X="-22.86101171875" Y="-2.795139648438" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.637958984375" Y="-3.190187988281" />
                  <Point X="-22.138712890625" Y="-4.054906494141" />
                  <Point X="-22.169533203125" Y="-4.076920166016" />
                  <Point X="-22.218154296875" Y="-4.111646972656" />
                  <Point X="-22.298232421875" Y="-4.163481933594" />
                  <Point X="-22.494607421875" Y="-3.907560546875" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922177490234" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.43341015625" Y="-2.80069140625" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.520009765625" Y="-2.751167236328" />
                  <Point X="-23.551630859375" Y="-2.743435546875" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.752783203125" Y="-2.756749755859" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.843634765625" Y="-2.769396972656" />
                  <Point X="-23.87101953125" Y="-2.780739746094" />
                  <Point X="-23.89540234375" Y="-2.7954609375" />
                  <Point X="-24.02658203125" Y="-2.904533203125" />
                  <Point X="-24.07538671875" Y="-2.945111328125" />
                  <Point X="-24.095859375" Y="-2.968861572266" />
                  <Point X="-24.112751953125" Y="-2.996688720703" />
                  <Point X="-24.124375" Y="-3.02580859375" />
                  <Point X="-24.16359765625" Y="-3.206262207031" />
                  <Point X="-24.178189453125" Y="-3.273396484375" />
                  <Point X="-24.180275390625" Y="-3.289627685547" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.12068359375" Y="-3.775632324219" />
                  <Point X="-23.977935546875" Y="-4.859915527344" />
                  <Point X="-23.97833203125" Y="-4.860002441406" />
                  <Point X="-24.0243203125" Y="-4.870083007812" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.058419921875" Y="-4.752637695312" />
                  <Point X="-26.14124609375" Y="-4.731327636719" />
                  <Point X="-26.1207734375" Y="-4.575837890625" />
                  <Point X="-26.120076171875" Y="-4.56809765625" />
                  <Point X="-26.11944921875" Y="-4.541030273438" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497689453125" />
                  <Point X="-26.16744921875" Y="-4.275907714844" />
                  <Point X="-26.18386328125" Y="-4.193397949219" />
                  <Point X="-26.188125" Y="-4.17846875" />
                  <Point X="-26.19902734375" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135462402344" />
                  <Point X="-26.221740234375" Y="-4.107625488281" />
                  <Point X="-26.230576171875" Y="-4.094859863281" />
                  <Point X="-26.250208984375" Y="-4.070937011719" />
                  <Point X="-26.261005859375" Y="-4.059779785156" />
                  <Point X="-26.431015625" Y="-3.910683349609" />
                  <Point X="-26.494265625" Y="-3.855215087891" />
                  <Point X="-26.50673828125" Y="-3.845965332031" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.86245703125" Y="-3.781380126953" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166259766" />
                  <Point X="-27.008005859375" Y="-3.781946044922" />
                  <Point X="-27.0390546875" Y="-3.790265869141" />
                  <Point X="-27.053673828125" Y="-3.795496337891" />
                  <Point X="-27.081865234375" Y="-3.80826953125" />
                  <Point X="-27.0954375" Y="-3.815812255859" />
                  <Point X="-27.283455078125" Y="-3.941441894531" />
                  <Point X="-27.35340234375" Y="-3.988180175781" />
                  <Point X="-27.35968359375" Y="-3.992759765625" />
                  <Point X="-27.380443359375" Y="-4.010133789063" />
                  <Point X="-27.4027578125" Y="-4.032772216797" />
                  <Point X="-27.410466796875" Y="-4.041628417969" />
                  <Point X="-27.47144921875" Y="-4.121100585938" />
                  <Point X="-27.50319921875" Y="-4.162479492188" />
                  <Point X="-27.63883984375" Y="-4.078494873047" />
                  <Point X="-27.747591796875" Y="-4.011157714844" />
                  <Point X="-27.98086328125" Y="-3.831547119141" />
                  <Point X="-27.884068359375" Y="-3.663894042969" />
                  <Point X="-27.341490234375" Y="-2.724119873047" />
                  <Point X="-27.3348515625" Y="-2.710085205078" />
                  <Point X="-27.32394921875" Y="-2.681120117188" />
                  <Point X="-27.319685546875" Y="-2.666189453125" />
                  <Point X="-27.3134140625" Y="-2.634663085938" />
                  <Point X="-27.311638671875" Y="-2.619239501953" />
                  <Point X="-27.310625" Y="-2.588305419922" />
                  <Point X="-27.3146640625" Y="-2.557617675781" />
                  <Point X="-27.3236484375" Y="-2.527999511719" />
                  <Point X="-27.32935546875" Y="-2.51355859375" />
                  <Point X="-27.343572265625" Y="-2.484729736328" />
                  <Point X="-27.3515546875" Y="-2.471413330078" />
                  <Point X="-27.369580078125" Y="-2.4462578125" />
                  <Point X="-27.379623046875" Y="-2.434418701172" />
                  <Point X="-27.396966796875" Y="-2.417073730469" />
                  <Point X="-27.408806640625" Y="-2.407028320312" />
                  <Point X="-27.4339609375" Y="-2.389001464844" />
                  <Point X="-27.447275390625" Y="-2.381020019531" />
                  <Point X="-27.47610546875" Y="-2.366799804688" />
                  <Point X="-27.490544921875" Y="-2.361092285156" />
                  <Point X="-27.520162109375" Y="-2.35210546875" />
                  <Point X="-27.550849609375" Y="-2.348063476562" />
                  <Point X="-27.581787109375" Y="-2.349074707031" />
                  <Point X="-27.59721484375" Y="-2.350848632812" />
                  <Point X="-27.628744140625" Y="-2.357119140625" />
                  <Point X="-27.64367578125" Y="-2.361383056641" />
                  <Point X="-27.67264453125" Y="-2.372285888672" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.03630859375" Y="-2.580781005859" />
                  <Point X="-28.793087890625" Y="-3.017708251953" />
                  <Point X="-28.918103515625" Y="-2.853464111328" />
                  <Point X="-29.00401953125" Y="-2.740587402344" />
                  <Point X="-29.181265625" Y="-2.443374023438" />
                  <Point X="-28.997458984375" Y="-2.302333984375" />
                  <Point X="-28.048123046875" Y="-1.573881958008" />
                  <Point X="-28.036484375" Y="-1.563311035156" />
                  <Point X="-28.015111328125" Y="-1.540396362305" />
                  <Point X="-28.005376953125" Y="-1.528052734375" />
                  <Point X="-27.987412109375" Y="-1.50092565918" />
                  <Point X="-27.979841796875" Y="-1.487137573242" />
                  <Point X="-27.96708203125" Y="-1.458501953125" />
                  <Point X="-27.961892578125" Y="-1.443654174805" />
                  <Point X="-27.95627734375" Y="-1.421972900391" />
                  <Point X="-27.951552734375" Y="-1.398809814453" />
                  <Point X="-27.948748046875" Y="-1.368387939453" />
                  <Point X="-27.948578125" Y="-1.353062988281" />
                  <Point X="-27.95078515625" Y="-1.321389404297" />
                  <Point X="-27.953080078125" Y="-1.30623425293" />
                  <Point X="-27.960080078125" Y="-1.276484130859" />
                  <Point X="-27.97177734375" Y="-1.248242675781" />
                  <Point X="-27.98786328125" Y="-1.222255249023" />
                  <Point X="-27.99695703125" Y="-1.20991394043" />
                  <Point X="-28.01779296875" Y="-1.185955566406" />
                  <Point X="-28.028751953125" Y="-1.175238891602" />
                  <Point X="-28.052248046875" Y="-1.155709106445" />
                  <Point X="-28.06478515625" Y="-1.146895751953" />
                  <Point X="-28.0840859375" Y="-1.135535522461" />
                  <Point X="-28.10543359375" Y="-1.124481567383" />
                  <Point X="-28.13469921875" Y="-1.113256103516" />
                  <Point X="-28.149798828125" Y="-1.108858276367" />
                  <Point X="-28.18168359375" Y="-1.102377929688" />
                  <Point X="-28.197298828125" Y="-1.100532104492" />
                  <Point X="-28.22862109375" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-28.685697265625" Y="-1.158303833008" />
                  <Point X="-29.66091796875" Y="-1.286694213867" />
                  <Point X="-29.707158203125" Y="-1.105669555664" />
                  <Point X="-29.740759765625" Y="-0.974114257812" />
                  <Point X="-29.786451171875" Y="-0.654654541016" />
                  <Point X="-29.589724609375" Y="-0.601941711426" />
                  <Point X="-28.508287109375" Y="-0.312171325684" />
                  <Point X="-28.500474609375" Y="-0.309712524414" />
                  <Point X="-28.473935546875" Y="-0.299253326416" />
                  <Point X="-28.444169921875" Y="-0.283895965576" />
                  <Point X="-28.4335625" Y="-0.277516204834" />
                  <Point X="-28.413333984375" Y="-0.263477111816" />
                  <Point X="-28.39366796875" Y="-0.248241195679" />
                  <Point X="-28.371208984375" Y="-0.226351867676" />
                  <Point X="-28.360890625" Y="-0.214475708008" />
                  <Point X="-28.341646484375" Y="-0.188217010498" />
                  <Point X="-28.3334296875" Y="-0.174803649902" />
                  <Point X="-28.31932421875" Y="-0.146805465698" />
                  <Point X="-28.313435546875" Y="-0.132220809937" />
                  <Point X="-28.306693359375" Y="-0.110496154785" />
                  <Point X="-28.300990234375" Y="-0.088497291565" />
                  <Point X="-28.296720703125" Y="-0.060339195251" />
                  <Point X="-28.295646484375" Y="-0.04609740448" />
                  <Point X="-28.295646484375" Y="-0.016462741852" />
                  <Point X="-28.296720703125" Y="-0.002220951557" />
                  <Point X="-28.300990234375" Y="0.025937143326" />
                  <Point X="-28.304185546875" Y="0.039853153229" />
                  <Point X="-28.310927734375" Y="0.061577957153" />
                  <Point X="-28.31932421875" Y="0.084245323181" />
                  <Point X="-28.3334296875" Y="0.112243499756" />
                  <Point X="-28.341646484375" Y="0.125656723022" />
                  <Point X="-28.360890625" Y="0.151915557861" />
                  <Point X="-28.371208984375" Y="0.163791870117" />
                  <Point X="-28.39366796875" Y="0.18568119812" />
                  <Point X="-28.40580859375" Y="0.195694076538" />
                  <Point X="-28.426037109375" Y="0.209733139038" />
                  <Point X="-28.444244140625" Y="0.220907348633" />
                  <Point X="-28.467337890625" Y="0.233350509644" />
                  <Point X="-28.47733984375" Y="0.238011474609" />
                  <Point X="-28.497814453125" Y="0.246141738892" />
                  <Point X="-28.508287109375" Y="0.249611343384" />
                  <Point X="-28.910615234375" Y="0.357414825439" />
                  <Point X="-29.7854453125" Y="0.591824890137" />
                  <Point X="-29.752984375" Y="0.811192932129" />
                  <Point X="-29.73133203125" Y="0.957522338867" />
                  <Point X="-29.6335859375" Y="1.318237182617" />
                  <Point X="-29.553265625" Y="1.307662841797" />
                  <Point X="-28.778064453125" Y="1.20560559082" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.20470324707" />
                  <Point X="-28.71514453125" Y="1.206589477539" />
                  <Point X="-28.704890625" Y="1.208053588867" />
                  <Point X="-28.6846015625" Y="1.212089233398" />
                  <Point X="-28.67456640625" Y="1.214660766602" />
                  <Point X="-28.629796875" Y="1.228776733398" />
                  <Point X="-28.60344921875" Y="1.237676391602" />
                  <Point X="-28.584517578125" Y="1.246006347656" />
                  <Point X="-28.57527734375" Y="1.250688232422" />
                  <Point X="-28.556533203125" Y="1.261509887695" />
                  <Point X="-28.547859375" Y="1.267170166016" />
                  <Point X="-28.531177734375" Y="1.279401611328" />
                  <Point X="-28.51592578125" Y="1.293376708984" />
                  <Point X="-28.502287109375" Y="1.308928833008" />
                  <Point X="-28.495892578125" Y="1.317076782227" />
                  <Point X="-28.483478515625" Y="1.334806030273" />
                  <Point X="-28.4780078125" Y="1.34360534668" />
                  <Point X="-28.46805859375" Y="1.3617421875" />
                  <Point X="-28.463580078125" Y="1.371079956055" />
                  <Point X="-28.4456171875" Y="1.414449462891" />
                  <Point X="-28.4355" Y="1.440348266602" />
                  <Point X="-28.429712890625" Y="1.46019909668" />
                  <Point X="-28.427359375" Y="1.470285888672" />
                  <Point X="-28.423599609375" Y="1.491601806641" />
                  <Point X="-28.422359375" Y="1.501888671875" />
                  <Point X="-28.421005859375" Y="1.522534790039" />
                  <Point X="-28.421908203125" Y="1.543205810547" />
                  <Point X="-28.425056640625" Y="1.563654907227" />
                  <Point X="-28.427189453125" Y="1.573791992188" />
                  <Point X="-28.43279296875" Y="1.594699584961" />
                  <Point X="-28.436015625" Y="1.604543579102" />
                  <Point X="-28.44351171875" Y="1.623816040039" />
                  <Point X="-28.44778515625" Y="1.633244750977" />
                  <Point X="-28.4694609375" Y="1.674883544922" />
                  <Point X="-28.48280078125" Y="1.699287109375" />
                  <Point X="-28.49429296875" Y="1.716486572266" />
                  <Point X="-28.500509765625" Y="1.72477331543" />
                  <Point X="-28.514421875" Y="1.741352783203" />
                  <Point X="-28.521501953125" Y="1.748912719727" />
                  <Point X="-28.536443359375" Y="1.763215698242" />
                  <Point X="-28.5443046875" Y="1.769958496094" />
                  <Point X="-28.775080078125" Y="1.947039794922" />
                  <Point X="-29.22761328125" Y="2.294280761719" />
                  <Point X="-29.0864296875" Y="2.536162841797" />
                  <Point X="-29.002283203125" Y="2.680322509766" />
                  <Point X="-28.726337890625" Y="3.035013183594" />
                  <Point X="-28.25415625" Y="2.762398925781" />
                  <Point X="-28.24491796875" Y="2.757716796875" />
                  <Point X="-28.225984375" Y="2.749385742188" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.09271875" Y="2.725702880859" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310791016" />
                  <Point X="-27.976431640625" Y="2.734227783203" />
                  <Point X="-27.9569921875" Y="2.741303466797" />
                  <Point X="-27.938439453125" Y="2.750453369141" />
                  <Point X="-27.929412109375" Y="2.755534667969" />
                  <Point X="-27.911158203125" Y="2.767164550781" />
                  <Point X="-27.902740234375" Y="2.773197265625" />
                  <Point X="-27.88661328125" Y="2.786141113281" />
                  <Point X="-27.878904296875" Y="2.793052246094" />
                  <Point X="-27.83464453125" Y="2.837310302734" />
                  <Point X="-27.8181796875" Y="2.853775634766" />
                  <Point X="-27.811265625" Y="2.861487060547" />
                  <Point X="-27.79831640625" Y="2.877620361328" />
                  <Point X="-27.79228125" Y="2.886042236328" />
                  <Point X="-27.78065234375" Y="2.904296142578" />
                  <Point X="-27.7755703125" Y="2.913325195312" />
                  <Point X="-27.766423828125" Y="2.931874267578" />
                  <Point X="-27.759353515625" Y="2.951299072266" />
                  <Point X="-27.754435546875" Y="2.971388427734" />
                  <Point X="-27.7525234375" Y="2.981572998047" />
                  <Point X="-27.749697265625" Y="3.00303125" />
                  <Point X="-27.748908203125" Y="3.013364501953" />
                  <Point X="-27.74845703125" Y="3.034049072266" />
                  <Point X="-27.748794921875" Y="3.044400390625" />
                  <Point X="-27.75425" Y="3.106752929688" />
                  <Point X="-27.756279296875" Y="3.129949951172" />
                  <Point X="-27.757744140625" Y="3.140207275391" />
                  <Point X="-27.76178125" Y="3.160499267578" />
                  <Point X="-27.764353515625" Y="3.170533935547" />
                  <Point X="-27.77086328125" Y="3.191176513672" />
                  <Point X="-27.77451171875" Y="3.200870361328" />
                  <Point X="-27.782841796875" Y="3.219799072266" />
                  <Point X="-27.7875234375" Y="3.229033935547" />
                  <Point X="-27.8897890625" Y="3.406160400391" />
                  <Point X="-28.05938671875" Y="3.699914550781" />
                  <Point X="-27.79490625" Y="3.902688964844" />
                  <Point X="-27.648365234375" Y="4.015041503906" />
                  <Point X="-27.192525390625" Y="4.268296875" />
                  <Point X="-27.118564453125" Y="4.171908691406" />
                  <Point X="-27.1118203125" Y="4.164047363281" />
                  <Point X="-27.097517578125" Y="4.149106445313" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.06508984375" Y="4.121895996094" />
                  <Point X="-27.047888671875" Y="4.110403320312" />
                  <Point X="-27.0389765625" Y="4.105128417969" />
                  <Point X="-26.969578125" Y="4.069001953125" />
                  <Point X="-26.943759765625" Y="4.055561523438" />
                  <Point X="-26.934326171875" Y="4.051286865234" />
                  <Point X="-26.915048828125" Y="4.043790283203" />
                  <Point X="-26.905205078125" Y="4.040568359375" />
                  <Point X="-26.884298828125" Y="4.034966308594" />
                  <Point X="-26.8741640625" Y="4.032834960938" />
                  <Point X="-26.853720703125" Y="4.029687988281" />
                  <Point X="-26.833052734375" Y="4.028785400391" />
                  <Point X="-26.812412109375" Y="4.030138427734" />
                  <Point X="-26.802130859375" Y="4.031378417969" />
                  <Point X="-26.78081640625" Y="4.03513671875" />
                  <Point X="-26.770728515625" Y="4.037488769531" />
                  <Point X="-26.750869140625" Y="4.04327734375" />
                  <Point X="-26.74109765625" Y="4.046713867188" />
                  <Point X="-26.66881640625" Y="4.076654785156" />
                  <Point X="-26.641923828125" Y="4.087793457031" />
                  <Point X="-26.63258984375" Y="4.092270263672" />
                  <Point X="-26.614458984375" Y="4.102216308594" />
                  <Point X="-26.605662109375" Y="4.107685058594" />
                  <Point X="-26.587931640625" Y="4.120098632812" />
                  <Point X="-26.579783203125" Y="4.126493164063" />
                  <Point X="-26.56423046875" Y="4.140131347656" />
                  <Point X="-26.55025390625" Y="4.1553828125" />
                  <Point X="-26.538021484375" Y="4.172064941406" />
                  <Point X="-26.532361328125" Y="4.180739257812" />
                  <Point X="-26.5215390625" Y="4.199482910156" />
                  <Point X="-26.51685546875" Y="4.208724121094" />
                  <Point X="-26.5085234375" Y="4.22766015625" />
                  <Point X="-26.504875" Y="4.237354980469" />
                  <Point X="-26.481349609375" Y="4.311971679688" />
                  <Point X="-26.472595703125" Y="4.339731445312" />
                  <Point X="-26.470025390625" Y="4.349763183594" />
                  <Point X="-26.465990234375" Y="4.370048828125" />
                  <Point X="-26.464525390625" Y="4.380302734375" />
                  <Point X="-26.462638671875" Y="4.401866210938" />
                  <Point X="-26.46230078125" Y="4.412218261719" />
                  <Point X="-26.462751953125" Y="4.432899414062" />
                  <Point X="-26.463541015625" Y="4.443228515625" />
                  <Point X="-26.47516796875" Y="4.5315390625" />
                  <Point X="-26.479265625" Y="4.562655273438" />
                  <Point X="-26.12087109375" Y="4.66313671875" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.373595703125" Y="4.781576660156" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.22566796875" Y="4.261729003906" />
                  <Point X="-25.220435546875" Y="4.247106933594" />
                  <Point X="-25.20766015625" Y="4.218912597656" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218911621094" />
                  <Point X="-24.767251953125" Y="4.247104980469" />
                  <Point X="-24.76201953125" Y="4.2617265625" />
                  <Point X="-24.709619140625" Y="4.457282226563" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.337767578125" Y="4.755259765625" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.7108125" Y="4.626537597656" />
                  <Point X="-23.546404296875" Y="4.586844238281" />
                  <Point X="-23.2475078125" Y="4.478432617188" />
                  <Point X="-23.141740234375" Y="4.440069824219" />
                  <Point X="-22.851392578125" Y="4.304284179688" />
                  <Point X="-22.749546875" Y="4.256654785156" />
                  <Point X="-22.4690078125" Y="4.093211425781" />
                  <Point X="-22.37057421875" Y="4.035863037109" />
                  <Point X="-22.18221875" Y="3.901916015625" />
                  <Point X="-22.3066953125" Y="3.686315917969" />
                  <Point X="-22.934689453125" Y="2.59859765625" />
                  <Point X="-22.937619140625" Y="2.593116699219" />
                  <Point X="-22.9468125" Y="2.573448730469" />
                  <Point X="-22.955814453125" Y="2.549569824219" />
                  <Point X="-22.958697265625" Y="2.540601074219" />
                  <Point X="-22.974345703125" Y="2.482084716797" />
                  <Point X="-22.98016796875" Y="2.460314697266" />
                  <Point X="-22.9820859375" Y="2.451470458984" />
                  <Point X="-22.986353515625" Y="2.420223876953" />
                  <Point X="-22.987244140625" Y="2.383240966797" />
                  <Point X="-22.986587890625" Y="2.369580810547" />
                  <Point X="-22.980486328125" Y="2.318980712891" />
                  <Point X="-22.97619921875" Y="2.289034423828" />
                  <Point X="-22.97085546875" Y="2.267108398438" />
                  <Point X="-22.967529296875" Y="2.256303955078" />
                  <Point X="-22.95926171875" Y="2.234213623047" />
                  <Point X="-22.9546796875" Y="2.223886962891" />
                  <Point X="-22.944318359375" Y="2.20384375" />
                  <Point X="-22.9385390625" Y="2.194127197266" />
                  <Point X="-22.907228515625" Y="2.147984863281" />
                  <Point X="-22.895580078125" Y="2.130818603516" />
                  <Point X="-22.890185546875" Y="2.123627197266" />
                  <Point X="-22.869537109375" Y="2.100125976562" />
                  <Point X="-22.842404296875" Y="2.075389160156" />
                  <Point X="-22.8317421875" Y="2.066981201172" />
                  <Point X="-22.785599609375" Y="2.035671630859" />
                  <Point X="-22.76843359375" Y="2.024023681641" />
                  <Point X="-22.75871875" Y="2.018244384766" />
                  <Point X="-22.738673828125" Y="2.007882080078" />
                  <Point X="-22.72834375" Y="2.003299194336" />
                  <Point X="-22.706255859375" Y="1.995033081055" />
                  <Point X="-22.695453125" Y="1.991708007812" />
                  <Point X="-22.673529296875" Y="1.986365112305" />
                  <Point X="-22.662408203125" Y="1.984346801758" />
                  <Point X="-22.61180859375" Y="1.978245239258" />
                  <Point X="-22.5937578125" Y="1.976936767578" />
                  <Point X="-22.55603125" Y="1.976004394531" />
                  <Point X="-22.54247265625" Y="1.976639160156" />
                  <Point X="-22.515583984375" Y="1.979834716797" />
                  <Point X="-22.50225390625" Y="1.982395629883" />
                  <Point X="-22.443736328125" Y="1.998043823242" />
                  <Point X="-22.421966796875" Y="2.003865356445" />
                  <Point X="-22.416005859375" Y="2.005670776367" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572753906" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.95930078125" Y="2.261505859375" />
                  <Point X="-21.059595703125" Y="2.780950439453" />
                  <Point X="-21.014443359375" Y="2.718198486328" />
                  <Point X="-20.956037109375" Y="2.637028320312" />
                  <Point X="-20.86311328125" Y="2.483471435547" />
                  <Point X="-21.000232421875" Y="2.378255859375" />
                  <Point X="-21.827046875" Y="1.743819824219" />
                  <Point X="-21.831857421875" Y="1.739872070313" />
                  <Point X="-21.847873046875" Y="1.725224121094" />
                  <Point X="-21.865330078125" Y="1.70660546875" />
                  <Point X="-21.87142578125" Y="1.699420410156" />
                  <Point X="-21.9135390625" Y="1.644479003906" />
                  <Point X="-21.92920703125" Y="1.62403918457" />
                  <Point X="-21.93436328125" Y="1.616605102539" />
                  <Point X="-21.950259765625" Y="1.589375732422" />
                  <Point X="-21.965236328125" Y="1.555549804688" />
                  <Point X="-21.969859375" Y="1.542675170898" />
                  <Point X="-21.985546875" Y="1.486579833984" />
                  <Point X="-21.991384765625" Y="1.46571081543" />
                  <Point X="-21.993775390625" Y="1.454663696289" />
                  <Point X="-21.997228515625" Y="1.432365722656" />
                  <Point X="-21.998291015625" Y="1.421115112305" />
                  <Point X="-21.999107421875" Y="1.397541503906" />
                  <Point X="-21.998826171875" Y="1.386237060547" />
                  <Point X="-21.996921875" Y="1.363750244141" />
                  <Point X="-21.995298828125" Y="1.352567871094" />
                  <Point X="-21.982419921875" Y="1.290154785156" />
                  <Point X="-21.97762890625" Y="1.266935058594" />
                  <Point X="-21.975400390625" Y="1.258233032227" />
                  <Point X="-21.965314453125" Y="1.228606323242" />
                  <Point X="-21.949712890625" Y="1.195372070312" />
                  <Point X="-21.943080078125" Y="1.183526733398" />
                  <Point X="-21.908052734375" Y="1.130287841797" />
                  <Point X="-21.8950234375" Y="1.110481201172" />
                  <Point X="-21.888263671875" Y="1.101428222656" />
                  <Point X="-21.87371484375" Y="1.084187988281" />
                  <Point X="-21.86592578125" Y="1.076000244141" />
                  <Point X="-21.848681640625" Y="1.059908569336" />
                  <Point X="-21.839970703125" Y="1.052700439453" />
                  <Point X="-21.821755859375" Y="1.039370605469" />
                  <Point X="-21.812251953125" Y="1.033248901367" />
                  <Point X="-21.7614921875" Y="1.004676635742" />
                  <Point X="-21.742609375" Y="0.994046630859" />
                  <Point X="-21.73451953125" Y="0.989988037109" />
                  <Point X="-21.70532421875" Y="0.978085327148" />
                  <Point X="-21.669724609375" Y="0.96802142334" />
                  <Point X="-21.656328125" Y="0.965257629395" />
                  <Point X="-21.58769921875" Y="0.95618737793" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.555966796875" Y="0.952199829102" />
                  <Point X="-21.53428125" Y="0.951222900391" />
                  <Point X="-21.50878125" Y="0.952032287598" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-21.114990234375" Y="1.003404968262" />
                  <Point X="-20.295296875" Y="1.111319824219" />
                  <Point X="-20.272396484375" Y="1.017252624512" />
                  <Point X="-20.247310546875" Y="0.914205383301" />
                  <Point X="-20.216126953125" Y="0.713921264648" />
                  <Point X="-20.3604609375" Y="0.675246948242" />
                  <Point X="-21.3080078125" Y="0.42135269165" />
                  <Point X="-21.31396875" Y="0.419543792725" />
                  <Point X="-21.334376953125" Y="0.412136138916" />
                  <Point X="-21.357619140625" Y="0.401618530273" />
                  <Point X="-21.365994140625" Y="0.397316497803" />
                  <Point X="-21.433419921875" Y="0.358343261719" />
                  <Point X="-21.45850390625" Y="0.343844055176" />
                  <Point X="-21.46611328125" Y="0.338947540283" />
                  <Point X="-21.491220703125" Y="0.319873718262" />
                  <Point X="-21.51800390625" Y="0.294352172852" />
                  <Point X="-21.527203125" Y="0.284226501465" />
                  <Point X="-21.567658203125" Y="0.232676803589" />
                  <Point X="-21.582708984375" Y="0.213498794556" />
                  <Point X="-21.58914453125" Y="0.20421031189" />
                  <Point X="-21.600869140625" Y="0.184930511475" />
                  <Point X="-21.60615625" Y="0.174939331055" />
                  <Point X="-21.6159296875" Y="0.153476699829" />
                  <Point X="-21.61999609375" Y="0.14292640686" />
                  <Point X="-21.626841796875" Y="0.121424232483" />
                  <Point X="-21.629623046875" Y="0.110472213745" />
                  <Point X="-21.643107421875" Y="0.040057952881" />
                  <Point X="-21.648125" Y="0.013861680984" />
                  <Point X="-21.649396484375" Y="0.004967195034" />
                  <Point X="-21.6514140625" Y="-0.026260259628" />
                  <Point X="-21.64971875" Y="-0.062940093994" />
                  <Point X="-21.648125" Y="-0.076421829224" />
                  <Point X="-21.634640625" Y="-0.146836090088" />
                  <Point X="-21.629623046875" Y="-0.173032211304" />
                  <Point X="-21.626841796875" Y="-0.183984237671" />
                  <Point X="-21.61999609375" Y="-0.205486404419" />
                  <Point X="-21.615931640625" Y="-0.216036392212" />
                  <Point X="-21.606158203125" Y="-0.237499328613" />
                  <Point X="-21.600869140625" Y="-0.247490646362" />
                  <Point X="-21.58914453125" Y="-0.266770446777" />
                  <Point X="-21.582708984375" Y="-0.276058807373" />
                  <Point X="-21.54225390625" Y="-0.32760848999" />
                  <Point X="-21.527203125" Y="-0.346786499023" />
                  <Point X="-21.52128125" Y="-0.353634155273" />
                  <Point X="-21.49885546875" Y="-0.375809448242" />
                  <Point X="-21.4698203125" Y="-0.398726348877" />
                  <Point X="-21.45850390625" Y="-0.406404052734" />
                  <Point X="-21.391078125" Y="-0.445377288818" />
                  <Point X="-21.365994140625" Y="-0.459876647949" />
                  <Point X="-21.36050390625" Y="-0.462814758301" />
                  <Point X="-21.340845703125" Y="-0.472014831543" />
                  <Point X="-21.316974609375" Y="-0.481027008057" />
                  <Point X="-21.3080078125" Y="-0.483912841797" />
                  <Point X="-20.955490234375" Y="-0.578369384766" />
                  <Point X="-20.21512109375" Y="-0.776750854492" />
                  <Point X="-20.224380859375" Y="-0.838169921875" />
                  <Point X="-20.238380859375" Y="-0.931036315918" />
                  <Point X="-20.272197265625" Y="-1.079219848633" />
                  <Point X="-20.464330078125" Y="-1.053925048828" />
                  <Point X="-21.563216796875" Y="-0.909253601074" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042297363" />
                  <Point X="-21.63327734375" Y="-0.910840881348" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.777849609375" Y="-0.94143939209" />
                  <Point X="-21.82708203125" Y="-0.952140075684" />
                  <Point X="-21.842123046875" Y="-0.9567421875" />
                  <Point X="-21.871244140625" Y="-0.968366577148" />
                  <Point X="-21.88532421875" Y="-0.975389282227" />
                  <Point X="-21.913150390625" Y="-0.992281494141" />
                  <Point X="-21.925875" Y="-1.001530395508" />
                  <Point X="-21.949625" Y="-1.022001281738" />
                  <Point X="-21.960650390625" Y="-1.033223022461" />
                  <Point X="-22.04063671875" Y="-1.129422119141" />
                  <Point X="-22.07039453125" Y="-1.1652109375" />
                  <Point X="-22.078673828125" Y="-1.176847900391" />
                  <Point X="-22.09339453125" Y="-1.201230712891" />
                  <Point X="-22.0998359375" Y="-1.2139765625" />
                  <Point X="-22.1111796875" Y="-1.24136340332" />
                  <Point X="-22.115638671875" Y="-1.254934448242" />
                  <Point X="-22.12246875" Y="-1.282583007812" />
                  <Point X="-22.12483984375" Y="-1.296660522461" />
                  <Point X="-22.1363046875" Y="-1.421242553711" />
                  <Point X="-22.140568359375" Y="-1.467590942383" />
                  <Point X="-22.140708984375" Y="-1.483321289062" />
                  <Point X="-22.138390625" Y="-1.514588012695" />
                  <Point X="-22.135931640625" Y="-1.530124267578" />
                  <Point X="-22.128201171875" Y="-1.561744384766" />
                  <Point X="-22.12321484375" Y="-1.576663208008" />
                  <Point X="-22.11084375" Y="-1.60547668457" />
                  <Point X="-22.103458984375" Y="-1.619371582031" />
                  <Point X="-22.030224609375" Y="-1.733283447266" />
                  <Point X="-22.002978515625" Y="-1.775662109375" />
                  <Point X="-21.998255859375" Y="-1.782353881836" />
                  <Point X="-21.980197265625" Y="-1.804458618164" />
                  <Point X="-21.95650390625" Y="-1.828124023438" />
                  <Point X="-21.947201171875" Y="-1.836277587891" />
                  <Point X="-21.620064453125" Y="-2.087299072266" />
                  <Point X="-20.912828125" Y="-2.62998046875" />
                  <Point X="-20.915037109375" Y="-2.633553710938" />
                  <Point X="-20.95453515625" Y="-2.697466064453" />
                  <Point X="-20.998724609375" Y="-2.760252441406" />
                  <Point X="-21.172837890625" Y="-2.659727783203" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.18497265625" Y="-2.079513427734" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228892578125" Y="-2.066337646484" />
                  <Point X="-22.386388671875" Y="-2.037893920898" />
                  <Point X="-22.444982421875" Y="-2.027311889648" />
                  <Point X="-22.460640625" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403442383" />
                  <Point X="-22.507685546875" Y="-2.02650378418" />
                  <Point X="-22.539859375" Y="-2.031461914062" />
                  <Point X="-22.555154296875" Y="-2.03513659668" />
                  <Point X="-22.5849296875" Y="-2.044960083008" />
                  <Point X="-22.59941015625" Y="-2.051108886719" />
                  <Point X="-22.730251953125" Y="-2.119969482422" />
                  <Point X="-22.778927734375" Y="-2.145587890625" />
                  <Point X="-22.791029296875" Y="-2.153169433594" />
                  <Point X="-22.8139609375" Y="-2.1700625" />
                  <Point X="-22.824791015625" Y="-2.179374023438" />
                  <Point X="-22.84575" Y="-2.200333007812" />
                  <Point X="-22.855060546875" Y="-2.211161865234" />
                  <Point X="-22.871953125" Y="-2.234092285156" />
                  <Point X="-22.87953515625" Y="-2.246193847656" />
                  <Point X="-22.948396484375" Y="-2.377034912109" />
                  <Point X="-22.974015625" Y="-2.425711914062" />
                  <Point X="-22.9801640625" Y="-2.440192871094" />
                  <Point X="-22.98998828125" Y="-2.469971191406" />
                  <Point X="-22.9936640625" Y="-2.485268554688" />
                  <Point X="-22.99862109375" Y="-2.517441894531" />
                  <Point X="-22.999720703125" Y="-2.533133300781" />
                  <Point X="-22.99931640625" Y="-2.564484375" />
                  <Point X="-22.9978125" Y="-2.580144042969" />
                  <Point X="-22.9693671875" Y="-2.737640625" />
                  <Point X="-22.95878515625" Y="-2.796234375" />
                  <Point X="-22.95698046875" Y="-2.804230957031" />
                  <Point X="-22.94876171875" Y="-2.831539794922" />
                  <Point X="-22.935927734375" Y="-2.862478759766" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.72023046875" Y="-3.237687988281" />
                  <Point X="-22.264103515625" Y="-4.027722167969" />
                  <Point X="-22.276244140625" Y="-4.036082519531" />
                  <Point X="-22.41923828125" Y="-3.849728271484" />
                  <Point X="-23.16608203125" Y="-2.876422851562" />
                  <Point X="-23.171345703125" Y="-2.870144042969" />
                  <Point X="-23.191169921875" Y="-2.849624023438" />
                  <Point X="-23.216748046875" Y="-2.828003662109" />
                  <Point X="-23.22669921875" Y="-2.820646728516" />
                  <Point X="-23.38203515625" Y="-2.720781005859" />
                  <Point X="-23.43982421875" Y="-2.683627929688" />
                  <Point X="-23.453716796875" Y="-2.676245605469" />
                  <Point X="-23.48252734375" Y="-2.663874511719" />
                  <Point X="-23.4974453125" Y="-2.658885742188" />
                  <Point X="-23.52906640625" Y="-2.651154052734" />
                  <Point X="-23.54460546875" Y="-2.648695800781" />
                  <Point X="-23.575875" Y="-2.646376953125" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.76148828125" Y="-2.662149414062" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670339111328" />
                  <Point X="-23.866421875" Y="-2.677170410156" />
                  <Point X="-23.87998828125" Y="-2.681627929688" />
                  <Point X="-23.907373046875" Y="-2.692970703125" />
                  <Point X="-23.92012109375" Y="-2.699412841797" />
                  <Point X="-23.94450390625" Y="-2.714134033203" />
                  <Point X="-23.956138671875" Y="-2.722413085938" />
                  <Point X="-24.087318359375" Y="-2.831485351563" />
                  <Point X="-24.136123046875" Y="-2.872063476562" />
                  <Point X="-24.14734375" Y="-2.883084960938" />
                  <Point X="-24.16781640625" Y="-2.906835205078" />
                  <Point X="-24.177068359375" Y="-2.919563964844" />
                  <Point X="-24.1939609375" Y="-2.947391113281" />
                  <Point X="-24.200982421875" Y="-2.961471679688" />
                  <Point X="-24.21260546875" Y="-2.990591552734" />
                  <Point X="-24.21720703125" Y="-3.005630859375" />
                  <Point X="-24.2564296875" Y="-3.186084472656" />
                  <Point X="-24.271021484375" Y="-3.25321875" />
                  <Point X="-24.2724140625" Y="-3.261287353516" />
                  <Point X="-24.275275390625" Y="-3.289677490234" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520019531" />
                  <Point X="-24.21487109375" Y="-3.788032226562" />
                  <Point X="-24.166912109375" Y="-4.152317871094" />
                  <Point X="-24.344931640625" Y="-3.487936767578" />
                  <Point X="-24.347392578125" Y="-3.480119384766" />
                  <Point X="-24.357853515625" Y="-3.453581542969" />
                  <Point X="-24.3732109375" Y="-3.423816894531" />
                  <Point X="-24.37958984375" Y="-3.413209716797" />
                  <Point X="-24.498921875" Y="-3.241274414062" />
                  <Point X="-24.543318359375" Y="-3.177309326172" />
                  <Point X="-24.553330078125" Y="-3.165169921875" />
                  <Point X="-24.575216796875" Y="-3.142713378906" />
                  <Point X="-24.587091796875" Y="-3.132396240234" />
                  <Point X="-24.61334765625" Y="-3.113152832031" />
                  <Point X="-24.6267578125" Y="-3.104937255859" />
                  <Point X="-24.654755859375" Y="-3.090830078125" />
                  <Point X="-24.66934375" Y="-3.084938476562" />
                  <Point X="-24.85400390625" Y="-3.027626708984" />
                  <Point X="-24.922703125" Y="-3.006305175781" />
                  <Point X="-24.936623046875" Y="-3.003109130859" />
                  <Point X="-24.964783203125" Y="-2.99883984375" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.24964453125" Y="-3.063616699219" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.332931640625" Y="-3.090830322266" />
                  <Point X="-25.360931640625" Y="-3.104938720703" />
                  <Point X="-25.37434375" Y="-3.113155273438" />
                  <Point X="-25.400599609375" Y="-3.132399658203" />
                  <Point X="-25.41247265625" Y="-3.142716796875" />
                  <Point X="-25.434357421875" Y="-3.165172119141" />
                  <Point X="-25.444369140625" Y="-3.177310302734" />
                  <Point X="-25.563701171875" Y="-3.349245361328" />
                  <Point X="-25.608095703125" Y="-3.413210693359" />
                  <Point X="-25.61246875" Y="-3.420130859375" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213867188" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.751373046875" Y="-3.893308105469" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.948705993491" Y="3.784772307267" />
                  <Point X="-28.044654078524" Y="3.674396661512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.642525615816" Y="2.986624133434" />
                  <Point X="-29.193365974385" Y="2.352954787514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.598648890331" Y="4.042662896228" />
                  <Point X="-27.994417342076" Y="3.587383372767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.558713341007" Y="2.938235083275" />
                  <Point X="-29.162156048736" Y="2.244053656735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.355194289629" Y="4.177921334232" />
                  <Point X="-27.944180605627" Y="3.500370084021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.474901066198" Y="2.889846033115" />
                  <Point X="-29.086646643499" Y="2.186113247731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.172941744496" Y="4.242774861051" />
                  <Point X="-27.893943869179" Y="3.413356795276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.391088791389" Y="2.841456982956" />
                  <Point X="-29.011137238261" Y="2.128172838727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.113649146547" Y="4.166179149276" />
                  <Point X="-27.843706470441" Y="3.326344268409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.30727651658" Y="2.793067932796" />
                  <Point X="-28.935627833024" Y="2.070232429724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.593872294798" Y="1.31300879667" />
                  <Point X="-29.653631046922" Y="1.244264216172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.040209262277" Y="4.105858028734" />
                  <Point X="-27.79346901199" Y="3.239331810233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.221002518236" Y="2.747510771619" />
                  <Point X="-28.860118427787" Y="2.01229202072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.480922459269" Y="1.298138675826" />
                  <Point X="-29.710641489261" Y="1.033877161187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.953601452439" Y="4.060684873752" />
                  <Point X="-27.757182026964" Y="3.136271168163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.112573617939" Y="2.727439909713" />
                  <Point X="-28.784609022549" Y="1.954351611716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.36797263844" Y="1.283268538073" />
                  <Point X="-29.747782610515" Y="0.84634714545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.36304361426" Y="4.595239910194" />
                  <Point X="-26.467703331426" Y="4.474842678057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.854558314788" Y="4.029816927021" />
                  <Point X="-27.750155779103" Y="2.999549898487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.981976194304" Y="2.73287101669" />
                  <Point X="-28.709099781254" Y="1.896411014118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.25502281761" Y="1.26839840032" />
                  <Point X="-29.773605498652" Y="0.671837267517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.196602750318" Y="4.641904178506" />
                  <Point X="-26.480177422942" Y="4.315688834031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.69874238509" Y="4.064258606651" />
                  <Point X="-28.633590563636" Y="1.838470389283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.14207299678" Y="1.253528262567" />
                  <Point X="-29.730160226562" Y="0.577011292736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.030162083846" Y="4.688568219655" />
                  <Point X="-28.558081346018" Y="1.780529764448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.02912317595" Y="1.238658124814" />
                  <Point X="-29.628064590695" Y="0.549654843515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.874383671922" Y="4.722966740023" />
                  <Point X="-28.491504625984" Y="1.712313476593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.91617335512" Y="1.223787987061" />
                  <Point X="-29.525968954828" Y="0.522298394295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.734250682659" Y="4.739367260444" />
                  <Point X="-28.443216912605" Y="1.623058093285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.80322353429" Y="1.208917849308" />
                  <Point X="-29.423873318961" Y="0.494941945074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.594117693395" Y="4.755767780865" />
                  <Point X="-28.422333903675" Y="1.502277203769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.671520099938" Y="1.215621276072" />
                  <Point X="-29.321777683094" Y="0.467585495853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.453984704132" Y="4.772168301285" />
                  <Point X="-29.219682047227" Y="0.440229046633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.354350354318" Y="4.741980466349" />
                  <Point X="-29.11758641136" Y="0.412872597412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.324692204197" Y="4.631294222027" />
                  <Point X="-29.015490775493" Y="0.385516148192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.295034054076" Y="4.520607977706" />
                  <Point X="-28.913395139626" Y="0.358159698971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.265375903956" Y="4.409921733384" />
                  <Point X="-28.811299497158" Y="0.330803257345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.631960016797" Y="-0.613258677502" />
                  <Point X="-29.769707735297" Y="-0.771719301031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.235717753835" Y="4.299235489063" />
                  <Point X="-28.709203854505" Y="0.303446815931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.467861057144" Y="-0.569288461897" />
                  <Point X="-29.75192301749" Y="-0.896064366771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.196356279152" Y="4.199711642762" />
                  <Point X="-28.607108211852" Y="0.276090374517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.303762119707" Y="-0.525318271848" />
                  <Point X="-29.730116776869" Y="-1.015783199718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.566707250807" Y="4.779235949371" />
                  <Point X="-24.648593032137" Y="4.685037133528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.131999629427" Y="4.128941456163" />
                  <Point X="-28.505152579549" Y="0.248572869619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.139663182271" Y="-0.481348081799" />
                  <Point X="-29.701530264496" Y="-1.127702222248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.45133432138" Y="4.767153279195" />
                  <Point X="-24.704681211475" Y="4.475711020762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.039841932479" Y="4.090152715978" />
                  <Point X="-28.417930168396" Y="0.204106732573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.975564244834" Y="-0.43737789175" />
                  <Point X="-29.672942478944" Y="-1.239619780154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.335961391809" Y="4.755070609185" />
                  <Point X="-24.760772147547" Y="4.266381736737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.901320658401" Y="4.104699170168" />
                  <Point X="-28.350155961733" Y="0.137267995506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.811465307397" Y="-0.393407701701" />
                  <Point X="-29.578562324866" Y="-1.275851875871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.220588453165" Y="4.742987949612" />
                  <Point X="-28.305441945338" Y="0.04390154409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.64736636996" Y="-0.349437511652" />
                  <Point X="-29.436418610201" Y="-1.257138280472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.111787081463" Y="4.723345567044" />
                  <Point X="-28.303681106204" Y="-0.098876885436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.479913775992" Y="-0.301609381081" />
                  <Point X="-29.294274895536" Y="-1.238424685074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.007746246931" Y="4.698226812913" />
                  <Point X="-29.152131180871" Y="-1.219711089675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.903705412399" Y="4.673108058783" />
                  <Point X="-29.009987466206" Y="-1.200997494277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.799664577867" Y="4.647989304652" />
                  <Point X="-28.867843751541" Y="-1.182283898878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.695623743975" Y="4.622870549785" />
                  <Point X="-28.725700036876" Y="-1.163570303479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.591582913829" Y="4.59775179061" />
                  <Point X="-28.583556347902" Y="-1.144856737635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.492259950966" Y="4.567205745961" />
                  <Point X="-28.44141266899" Y="-1.126143183366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.396558100994" Y="4.532494087445" />
                  <Point X="-28.299268990079" Y="-1.107429629097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.300856251021" Y="4.497782428929" />
                  <Point X="-28.170905596139" Y="-1.104568479302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.205154462449" Y="4.46307069978" />
                  <Point X="-28.076056754737" Y="-1.140261411928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.111547148561" Y="4.425949553125" />
                  <Point X="-28.003844312699" Y="-1.201994543237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.886025613454" Y="-2.216828041067" />
                  <Point X="-29.141265464292" Y="-2.510447901735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.022053335566" Y="4.384096364999" />
                  <Point X="-27.956382293677" Y="-1.292199779248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.507987076032" Y="-1.926748494142" />
                  <Point X="-29.090047438914" Y="-2.596332346697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.932559522571" Y="4.342243176872" />
                  <Point X="-27.961777668489" Y="-1.443210491214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.12994853861" Y="-1.636668947216" />
                  <Point X="-29.038829413536" Y="-2.682216791659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.843065691935" Y="4.300390009041" />
                  <Point X="-28.985194036487" Y="-2.765320391629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.753571689331" Y="4.258537039035" />
                  <Point X="-28.926430246876" Y="-2.842524427809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.669746574373" Y="4.210162759777" />
                  <Point X="-28.867666171095" Y="-2.919728134788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.586188411994" Y="4.161481386706" />
                  <Point X="-28.808902048069" Y="-2.996931787418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.502630249615" Y="4.112800013636" />
                  <Point X="-28.608389378769" Y="-2.911072390644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.419072239537" Y="4.064118465363" />
                  <Point X="-28.355684960349" Y="-2.765173254566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.337935075772" Y="4.012652051972" />
                  <Point X="-28.102980541929" Y="-2.619274118488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.260146504727" Y="3.957333523308" />
                  <Point X="-27.850277286455" Y="-2.473376320225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.182357933682" Y="3.902014994644" />
                  <Point X="-27.622210710847" Y="-2.355819780141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.430712576139" Y="3.471511616938" />
                  <Point X="-27.498752803641" Y="-2.358601747305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.679652278698" Y="3.040335204574" />
                  <Point X="-27.412606147745" Y="-2.404305399213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.928591981257" Y="2.60915879221" />
                  <Point X="-27.348906393382" Y="-2.475831257482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.986906247705" Y="2.397271859161" />
                  <Point X="-27.311963326532" Y="-2.57813716375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.971583314559" Y="2.270094834121" />
                  <Point X="-27.397857801648" Y="-2.821751497514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.926792665266" Y="2.176816538769" />
                  <Point X="-27.646796829422" Y="-3.252927133628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.86844669687" Y="2.099131854264" />
                  <Point X="-27.895736038856" Y="-3.684102978716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.793233045046" Y="2.040851219876" />
                  <Point X="-27.931242911763" Y="-3.869753006784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.706957615968" Y="1.995295704571" />
                  <Point X="-27.855837185811" Y="-3.927812685163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.596845967584" Y="1.977160622901" />
                  <Point X="-27.780431459859" Y="-3.985872363541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.455566160637" Y="1.994880406155" />
                  <Point X="-27.70139869575" Y="-4.039759611812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.237579563173" Y="2.100841257838" />
                  <Point X="-27.619567556112" Y="-4.090427697282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.984875014182" Y="2.246740544121" />
                  <Point X="-27.537736026157" Y="-4.141095333744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.732171023823" Y="2.392639187772" />
                  <Point X="-27.17573630462" Y="-3.869466333903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.479467096364" Y="2.538537759064" />
                  <Point X="-26.969362498254" Y="-3.776864470218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.226763168905" Y="2.684436330357" />
                  <Point X="-26.848222689634" Y="-3.782313104762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.040300009302" Y="2.754133615037" />
                  <Point X="-26.729131853303" Y="-3.790118812294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.983293552184" Y="2.674907999076" />
                  <Point X="-21.723823402888" Y="1.823025854223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.970964880447" Y="1.538722106325" />
                  <Point X="-26.612342774156" Y="-3.800572388572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.929075679972" Y="2.592474483138" />
                  <Point X="-21.345787047733" Y="2.113102890736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.99694562039" Y="1.364030640661" />
                  <Point X="-26.519193334565" Y="-3.838220259354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.877412926564" Y="2.507101639252" />
                  <Point X="-20.967750367095" Y="2.403180301677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.971921380447" Y="1.248013692469" />
                  <Point X="-26.44536740703" Y="-3.898097287921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.925426968125" Y="1.15669535228" />
                  <Point X="-26.373942199573" Y="-3.96073602902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.867896821719" Y="1.078072171931" />
                  <Point X="-26.302517126003" Y="-4.023374924138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.79125767005" Y="1.02143138753" />
                  <Point X="-25.406847020898" Y="-3.13782837517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.457047436206" Y="-3.19557734697" />
                  <Point X="-26.2345609614" Y="-4.09000434254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.70350793368" Y="0.977571868755" />
                  <Point X="-25.204198510684" Y="-3.049511974487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.668348013836" Y="-3.58345489914" />
                  <Point X="-26.187497258141" Y="-4.180667788421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.595342796892" Y="0.957197581642" />
                  <Point X="-25.035999483272" Y="-3.000825170464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.724437150623" Y="-3.792782113325" />
                  <Point X="-26.163159353182" Y="-4.297474274696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.469918359829" Y="0.956677848295" />
                  <Point X="-24.916547646163" Y="-3.008215594106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.780526514729" Y="-4.002109589011" />
                  <Point X="-26.139719539778" Y="-4.415313897121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.32777468645" Y="0.9753913962" />
                  <Point X="-24.817416366411" Y="-3.038982144949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.836616088864" Y="-4.211437306307" />
                  <Point X="-26.119733455947" Y="-4.537126580935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.18563101307" Y="0.994104944105" />
                  <Point X="-24.71828520818" Y="-3.069748835586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.892705662999" Y="-4.420765023603" />
                  <Point X="-26.137421271818" Y="-4.702278128744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.043487313733" Y="1.012818521871" />
                  <Point X="-24.624304391077" Y="-3.106440315943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.948795237134" Y="-4.6300927409" />
                  <Point X="-26.055769212427" Y="-4.753152222472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.901343588751" Y="1.031532129138" />
                  <Point X="-24.551456172075" Y="-3.167442069518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.759199863769" Y="1.050245736405" />
                  <Point X="-21.305244635339" Y="0.422093082263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.644736581762" Y="0.031552272592" />
                  <Point X="-24.494855043734" Y="-3.247133962899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.617056138787" Y="1.068959343672" />
                  <Point X="-21.141145743882" Y="0.466063219417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.641733590871" Y="-0.109797224797" />
                  <Point X="-23.87635383798" Y="-2.680433759208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.236598989553" Y="-3.094848400432" />
                  <Point X="-24.438971666548" Y="-3.327651534532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.474912413805" Y="1.087672950938" />
                  <Point X="-20.977046852426" Y="0.510033356571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.613277121063" Y="-0.221865844186" />
                  <Point X="-23.732243888369" Y="-2.659458269246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.274644615381" Y="-3.283418929655" />
                  <Point X="-24.383088289362" Y="-3.408169106165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.332768688823" Y="1.106386558205" />
                  <Point X="-20.81294796097" Y="0.554003493726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.559840818971" Y="-0.305198453698" />
                  <Point X="-23.595422784083" Y="-2.646867636671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.263872228488" Y="-3.41583075934" />
                  <Point X="-24.340601389453" Y="-3.504097562026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.275017266411" Y="1.028017926783" />
                  <Point X="-20.648849069514" Y="0.59797363088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.496773944001" Y="-0.377452356427" />
                  <Point X="-23.48392430295" Y="-2.66340734976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.247315892809" Y="-3.541588917072" />
                  <Point X="-24.310943240685" Y="-3.614783807903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.247478179435" Y="0.914893979167" />
                  <Point X="-20.484750178057" Y="0.641943768034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.416946212301" Y="-0.430425099096" />
                  <Point X="-21.896656538671" Y="-0.982268703171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.101436779654" Y="-1.217841422821" />
                  <Point X="-23.398641442261" Y="-2.710104684383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.230759557131" Y="-3.667347074805" />
                  <Point X="-24.281285091916" Y="-3.725470053781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.228306117233" Y="0.792144870589" />
                  <Point X="-20.320651234485" Y="0.685913965141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.330589368961" Y="-0.475886958008" />
                  <Point X="-21.725372681005" Y="-0.930033207881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.13440205136" Y="-1.400567673163" />
                  <Point X="-22.785758048104" Y="-2.149867033672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.894943248147" Y="-2.275470238337" />
                  <Point X="-23.317892905194" Y="-2.762018161649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.214203221634" Y="-3.793105232747" />
                  <Point X="-24.251626943148" Y="-3.836156299658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.22988630616" Y="-0.504845379288" />
                  <Point X="-21.580669142507" Y="-0.908374872017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.132686450748" Y="-1.543398143657" />
                  <Point X="-22.56215630607" Y="-2.037446697073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.999623191854" Y="-2.540694781684" />
                  <Point X="-23.237144304757" Y="-2.813931566016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.197646890466" Y="-3.918863395669" />
                  <Point X="-24.22196879438" Y="-3.946842545535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.127790613473" Y="-0.532201763144" />
                  <Point X="-21.466612550231" Y="-0.921971814865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.090446896286" Y="-1.639611137906" />
                  <Point X="-22.429846287897" Y="-2.030045475445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.982369265979" Y="-2.665650453694" />
                  <Point X="-23.165851771701" Y="-2.876722931555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.181090559298" Y="-4.044621558591" />
                  <Point X="-24.192310645611" Y="-4.057528791413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.025694920785" Y="-0.559558147" />
                  <Point X="-21.353662741625" Y="-0.93684196668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.036930947134" Y="-1.722852123956" />
                  <Point X="-22.321050263097" Y="-2.049694008921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.960715676861" Y="-2.785544892106" />
                  <Point X="-23.10683463998" Y="-2.953635530975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.923599249266" Y="-0.586914555208" />
                  <Point X="-21.240712933018" Y="-0.951712118494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.9810678284" Y="-1.803393000272" />
                  <Point X="-22.213248051311" Y="-2.070485793491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.922833133176" Y="-2.886770053904" />
                  <Point X="-23.047817508258" Y="-3.030548130394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.821503624347" Y="-0.614271017022" />
                  <Point X="-21.127763124412" Y="-0.966582270308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.909155375692" Y="-1.865471229829" />
                  <Point X="-22.122814642008" Y="-2.111258099708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.872596135838" Y="-2.973783042529" />
                  <Point X="-22.988800376536" Y="-3.107460729814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.719407999428" Y="-0.641627478837" />
                  <Point X="-21.014813315805" Y="-0.981452422123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.833646046493" Y="-1.923411726304" />
                  <Point X="-22.039002378875" Y="-2.1596471633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.8223591385" Y="-3.060796031155" />
                  <Point X="-22.929783244815" Y="-3.184373329234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.617312374508" Y="-0.668983940651" />
                  <Point X="-20.901863507199" Y="-0.996322573937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.758136717294" Y="-1.98135222278" />
                  <Point X="-21.955190115743" Y="-2.208036226892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.772122141162" Y="-3.147809019781" />
                  <Point X="-22.870766113093" Y="-3.261285928653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.515216749589" Y="-0.696340402466" />
                  <Point X="-20.788913698592" Y="-1.011192725752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.682627388094" Y="-2.039292719256" />
                  <Point X="-21.87137785261" Y="-2.256425290483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.721885143824" Y="-3.234822008406" />
                  <Point X="-22.811748981372" Y="-3.338198528073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.41312112467" Y="-0.72369686428" />
                  <Point X="-20.675963889985" Y="-1.026062877566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.607118044261" Y="-2.097233198897" />
                  <Point X="-21.787565589478" Y="-2.304814354075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.671648128536" Y="-3.321834976384" />
                  <Point X="-22.75273184965" Y="-3.415111127493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.311025499751" Y="-0.751053326095" />
                  <Point X="-20.563014081379" Y="-1.04093302938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.53160862971" Y="-2.155173597186" />
                  <Point X="-21.703753326345" Y="-2.353203417667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.621411112638" Y="-3.408847943658" />
                  <Point X="-22.693714717928" Y="-3.492023726913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.216722748288" Y="-0.787374463335" />
                  <Point X="-20.450064272109" Y="-1.055803180432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.456099215158" Y="-2.213113995476" />
                  <Point X="-21.619941063213" Y="-2.401592481258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.57117409674" Y="-3.495860910933" />
                  <Point X="-22.634697586207" Y="-3.568936326332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.24644396527" Y="-0.966368855612" />
                  <Point X="-20.33711445825" Y="-1.070673326204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.380589800607" Y="-2.271054393766" />
                  <Point X="-21.53612880008" Y="-2.44998154485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.520937080841" Y="-3.582873878207" />
                  <Point X="-22.575680454485" Y="-3.645848925752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.305080386056" Y="-2.328994792055" />
                  <Point X="-21.452316536948" Y="-2.498370608442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.470700064943" Y="-3.669886845482" />
                  <Point X="-22.516663322764" Y="-3.722761525172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.229570971505" Y="-2.386935190345" />
                  <Point X="-21.368504273815" Y="-2.546759672033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.420463049044" Y="-3.756899812756" />
                  <Point X="-22.457646191042" Y="-3.799674124592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.154061556953" Y="-2.444875588634" />
                  <Point X="-21.284692010682" Y="-2.595148735625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.370226033146" Y="-3.843912780031" />
                  <Point X="-22.39862910127" Y="-3.876586772268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.078552142402" Y="-2.502815986924" />
                  <Point X="-21.20087974755" Y="-2.643537799217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.319989017247" Y="-3.930925747305" />
                  <Point X="-22.339612089675" Y="-3.953499509878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.003042727851" Y="-2.560756385213" />
                  <Point X="-21.117067529689" Y="-2.691926914887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.269752001349" Y="-4.017938714579" />
                  <Point X="-22.280595078081" Y="-4.030412247489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.9275333133" Y="-2.618696783503" />
                  <Point X="-21.033255334591" Y="-2.740316056744" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.0001640625" Y="0.001626220703" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.236498046875" Y="-4.62673046875" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521543701172" />
                  <Point X="-24.65501171875" Y="-3.349608398438" />
                  <Point X="-24.699408203125" Y="-3.285643310547" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.91032421875" Y="-3.209088134766" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.19332421875" Y="-3.245078125" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285644287109" />
                  <Point X="-25.407611328125" Y="-3.457579345703" />
                  <Point X="-25.452005859375" Y="-3.521544677734" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.567845703125" Y="-3.942483886719" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-26.020443359375" Y="-4.953555175781" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.3143046875" Y="-4.882989746094" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.344294921875" Y="-4.817994140625" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.534756835938" />
                  <Point X="-26.353796875" Y="-4.312975097656" />
                  <Point X="-26.3702109375" Y="-4.230465332031" />
                  <Point X="-26.386283203125" Y="-4.202628417969" />
                  <Point X="-26.55629296875" Y="-4.053532226562" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.8748828125" Y="-3.970973388672" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791259766" />
                  <Point X="-27.177896484375" Y="-4.099420898438" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.32071484375" Y="-4.236766601563" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.73886328125" Y="-4.240036132813" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.1522421875" Y="-3.93938671875" />
                  <Point X="-28.228580078125" Y="-3.880608154297" />
                  <Point X="-28.048611328125" Y="-3.568893798828" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597593261719" />
                  <Point X="-27.513978515625" Y="-2.568764404297" />
                  <Point X="-27.531322265625" Y="-2.551419433594" />
                  <Point X="-27.56015234375" Y="-2.53719921875" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-27.94130859375" Y="-2.745325927734" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-29.069291015625" Y="-2.968540527344" />
                  <Point X="-29.161697265625" Y="-2.847135742188" />
                  <Point X="-29.374205078125" Y="-2.490796386719" />
                  <Point X="-29.43101953125" Y="-2.395526855469" />
                  <Point X="-29.113123046875" Y="-2.151596679688" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.396017700195" />
                  <Point X="-28.14020703125" Y="-1.374336425781" />
                  <Point X="-28.1381171875" Y="-1.366270263672" />
                  <Point X="-28.14032421875" Y="-1.334596435547" />
                  <Point X="-28.16116015625" Y="-1.310637939453" />
                  <Point X="-28.1804609375" Y="-1.299277709961" />
                  <Point X="-28.187642578125" Y="-1.295051513672" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.660896484375" Y="-1.346678344727" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.891248046875" Y="-1.152692138672" />
                  <Point X="-29.927392578125" Y="-1.011187805176" />
                  <Point X="-29.983615234375" Y="-0.618088134766" />
                  <Point X="-29.998396484375" Y="-0.51474230957" />
                  <Point X="-29.638900390625" Y="-0.418415771484" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.54189453125" Y="-0.121425201416" />
                  <Point X="-28.521666015625" Y="-0.10738608551" />
                  <Point X="-28.514140625" Y="-0.10216317749" />
                  <Point X="-28.494896484375" Y="-0.075904411316" />
                  <Point X="-28.488154296875" Y="-0.054179775238" />
                  <Point X="-28.485646484375" Y="-0.046097381592" />
                  <Point X="-28.485646484375" Y="-0.016462697983" />
                  <Point X="-28.492388671875" Y="0.005262090206" />
                  <Point X="-28.494896484375" Y="0.013344331741" />
                  <Point X="-28.514140625" Y="0.039603096008" />
                  <Point X="-28.534369140625" Y="0.053642211914" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-28.959791015625" Y="0.173888900757" />
                  <Point X="-29.998185546875" Y="0.452126037598" />
                  <Point X="-29.9409375" Y="0.839004760742" />
                  <Point X="-29.91764453125" Y="0.996414611816" />
                  <Point X="-29.804470703125" Y="1.414065917969" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.52846484375" Y="1.496037353516" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.68693359375" Y="1.409982666016" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.651533203125" Y="1.426055786133" />
                  <Point X="-28.639119140625" Y="1.44378503418" />
                  <Point X="-28.62115625" Y="1.487154541016" />
                  <Point X="-28.61447265625" Y="1.503289306641" />
                  <Point X="-28.610712890625" Y="1.524605224609" />
                  <Point X="-28.61631640625" Y="1.545512573242" />
                  <Point X="-28.6379921875" Y="1.587151367188" />
                  <Point X="-28.646056640625" Y="1.602642211914" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-28.890744140625" Y="1.796302856445" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.2505234375" Y="2.631941894531" />
                  <Point X="-29.16001171875" Y="2.787007568359" />
                  <Point X="-28.86021875" Y="3.172350585938" />
                  <Point X="-28.774669921875" Y="3.282310791016" />
                  <Point X="-28.635263671875" Y="3.201823974609" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.07616015625" Y="2.914979736328" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927405273438" />
                  <Point X="-27.9689921875" Y="2.971663330078" />
                  <Point X="-27.95252734375" Y="2.988128662109" />
                  <Point X="-27.9408984375" Y="3.006382568359" />
                  <Point X="-27.938072265625" Y="3.027840820312" />
                  <Point X="-27.94352734375" Y="3.090193359375" />
                  <Point X="-27.945556640625" Y="3.113390380859" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.05433203125" Y="3.311159423828" />
                  <Point X="-28.30727734375" Y="3.749276855469" />
                  <Point X="-27.91051171875" Y="4.05347265625" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.28070703125" Y="4.436657714844" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.113576171875" Y="4.47751953125" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.88184765625" Y="4.237534179687" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.218491699219" />
                  <Point X="-26.81380859375" Y="4.22225" />
                  <Point X="-26.74152734375" Y="4.252190917969" />
                  <Point X="-26.714634765625" Y="4.263329589844" />
                  <Point X="-26.696904296875" Y="4.275743164062" />
                  <Point X="-26.68608203125" Y="4.294486816406" />
                  <Point X="-26.662556640625" Y="4.369103515625" />
                  <Point X="-26.653802734375" Y="4.39686328125" />
                  <Point X="-26.651916015625" Y="4.418426757812" />
                  <Point X="-26.66354296875" Y="4.506737304688" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.1721640625" Y="4.84608203125" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.395681640625" Y="4.970288574219" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.182986328125" Y="4.836546386719" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.893146484375" Y="4.506458984375" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.317978515625" Y="4.9442265625" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.66622265625" Y="4.811230957031" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.18272265625" Y="4.657046875" />
                  <Point X="-23.068970703125" Y="4.615788085938" />
                  <Point X="-22.770904296875" Y="4.476392578125" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.37336328125" Y="4.257381347656" />
                  <Point X="-22.26747265625" Y="4.195688964844" />
                  <Point X="-21.99591015625" Y="4.002569824219" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.142150390625" Y="3.591316162109" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491516113281" />
                  <Point X="-22.790794921875" Y="2.432999755859" />
                  <Point X="-22.7966171875" Y="2.411229736328" />
                  <Point X="-22.797955078125" Y="2.392326904297" />
                  <Point X="-22.791853515625" Y="2.341726806641" />
                  <Point X="-22.789583984375" Y="2.322902099609" />
                  <Point X="-22.78131640625" Y="2.300811767578" />
                  <Point X="-22.750005859375" Y="2.254669433594" />
                  <Point X="-22.738357421875" Y="2.237503173828" />
                  <Point X="-22.72505859375" Y="2.224203857422" />
                  <Point X="-22.678916015625" Y="2.192894287109" />
                  <Point X="-22.66175" Y="2.181246337891" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.5890625" Y="2.166878662109" />
                  <Point X="-22.5513359375" Y="2.165946289062" />
                  <Point X="-22.492818359375" Y="2.181594482422" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-22.05430078125" Y="2.42605078125" />
                  <Point X="-21.005751953125" Y="3.031430419922" />
                  <Point X="-20.860216796875" Y="2.829169677734" />
                  <Point X="-20.797400390625" Y="2.741871582031" />
                  <Point X="-20.646013671875" Y="2.491702148438" />
                  <Point X="-20.612486328125" Y="2.436296142578" />
                  <Point X="-20.884568359375" Y="2.227519042969" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583833374023" />
                  <Point X="-21.7627421875" Y="1.528891845703" />
                  <Point X="-21.77841015625" Y="1.508452026367" />
                  <Point X="-21.78687890625" Y="1.491503540039" />
                  <Point X="-21.80256640625" Y="1.435408203125" />
                  <Point X="-21.808404296875" Y="1.4145390625" />
                  <Point X="-21.809220703125" Y="1.390965332031" />
                  <Point X="-21.796341796875" Y="1.328552246094" />
                  <Point X="-21.79155078125" Y="1.305332519531" />
                  <Point X="-21.784353515625" Y="1.287957519531" />
                  <Point X="-21.749326171875" Y="1.23471862793" />
                  <Point X="-21.736296875" Y="1.214912109375" />
                  <Point X="-21.719052734375" Y="1.19882043457" />
                  <Point X="-21.66829296875" Y="1.170248046875" />
                  <Point X="-21.64941015625" Y="1.159618164062" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.5628046875" Y="1.144549438477" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-21.139791015625" Y="1.191779541016" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.087787109375" Y="1.062194213867" />
                  <Point X="-20.060806640625" Y="0.951367553711" />
                  <Point X="-20.0131015625" Y="0.644963012695" />
                  <Point X="-20.002140625" Y="0.57455645752" />
                  <Point X="-20.31128515625" Y="0.491721069336" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819198608" />
                  <Point X="-21.338337890625" Y="0.193845947266" />
                  <Point X="-21.363421875" Y="0.179346679688" />
                  <Point X="-21.377734375" Y="0.166926849365" />
                  <Point X="-21.418189453125" Y="0.115377227783" />
                  <Point X="-21.433240234375" Y="0.096199172974" />
                  <Point X="-21.443013671875" Y="0.074736328125" />
                  <Point X="-21.456498046875" Y="0.004322164059" />
                  <Point X="-21.461515625" Y="-0.021874082565" />
                  <Point X="-21.461515625" Y="-0.040685997009" />
                  <Point X="-21.44803125" Y="-0.111100158691" />
                  <Point X="-21.443013671875" Y="-0.137296401978" />
                  <Point X="-21.433240234375" Y="-0.158759246826" />
                  <Point X="-21.39278515625" Y="-0.210308868408" />
                  <Point X="-21.377734375" Y="-0.229486923218" />
                  <Point X="-21.363421875" Y="-0.24190675354" />
                  <Point X="-21.29599609375" Y="-0.280880004883" />
                  <Point X="-21.270912109375" Y="-0.295379272461" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.906314453125" Y="-0.394843536377" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.036505859375" Y="-0.866495849609" />
                  <Point X="-20.051568359375" Y="-0.966414001465" />
                  <Point X="-20.112689453125" Y="-1.23424597168" />
                  <Point X="-20.125453125" Y="-1.290178588867" />
                  <Point X="-20.489130859375" Y="-1.242299560547" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.737494140625" Y="-1.127104248047" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.814552734375" Y="-1.154697021484" />
                  <Point X="-21.8945390625" Y="-1.250896118164" />
                  <Point X="-21.924296875" Y="-1.286685058594" />
                  <Point X="-21.935640625" Y="-1.314071899414" />
                  <Point X="-21.94710546875" Y="-1.438653930664" />
                  <Point X="-21.951369140625" Y="-1.485002197266" />
                  <Point X="-21.943638671875" Y="-1.516622314453" />
                  <Point X="-21.870404296875" Y="-1.630534179688" />
                  <Point X="-21.843158203125" Y="-1.672912841797" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-21.504400390625" Y="-1.936562133789" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.753408203125" Y="-2.733436767578" />
                  <Point X="-20.7958671875" Y="-2.802141357422" />
                  <Point X="-20.922271484375" Y="-2.981744140625" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.267837890625" Y="-2.824272705078" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.420154296875" Y="-2.224869140625" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219245117188" />
                  <Point X="-22.641763671875" Y="-2.288105712891" />
                  <Point X="-22.690439453125" Y="-2.313724121094" />
                  <Point X="-22.7113984375" Y="-2.334683105469" />
                  <Point X="-22.780259765625" Y="-2.465524169922" />
                  <Point X="-22.80587890625" Y="-2.514201171875" />
                  <Point X="-22.8108359375" Y="-2.546374511719" />
                  <Point X="-22.782390625" Y="-2.70387109375" />
                  <Point X="-22.77180859375" Y="-2.76246484375" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.5556875" Y="-3.142687988281" />
                  <Point X="-22.01332421875" Y="-4.082088623047" />
                  <Point X="-22.11431640625" Y="-4.154225097656" />
                  <Point X="-22.164705078125" Y="-4.190215332031" />
                  <Point X="-22.30602734375" Y="-4.28169140625" />
                  <Point X="-22.32022265625" Y="-4.290879882812" />
                  <Point X="-22.5699765625" Y="-3.965392822266" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467529297" />
                  <Point X="-23.48478515625" Y="-2.880601806641" />
                  <Point X="-23.54257421875" Y="-2.843448730469" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.744078125" Y="-2.851350097656" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868508789062" />
                  <Point X="-23.965845703125" Y="-2.977581054688" />
                  <Point X="-24.014650390625" Y="-3.018159179688" />
                  <Point X="-24.03154296875" Y="-3.045986328125" />
                  <Point X="-24.070765625" Y="-3.226439941406" />
                  <Point X="-24.085357421875" Y="-3.29357421875" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-24.02649609375" Y="-3.763232421875" />
                  <Point X="-23.872357421875" Y="-4.934028320312" />
                  <Point X="-23.957990234375" Y="-4.952799316406" />
                  <Point X="-24.005650390625" Y="-4.96324609375" />
                  <Point X="-24.136212890625" Y="-4.98696484375" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#190" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.140845616077" Y="4.88076749562" Z="1.85" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.85" />
                  <Point X="-0.407650953605" Y="5.05122989569" Z="1.85" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.85" />
                  <Point X="-1.191819235171" Y="4.925505566177" Z="1.85" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.85" />
                  <Point X="-1.71927216427" Y="4.531490947242" Z="1.85" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.85" />
                  <Point X="-1.716398526572" Y="4.415420822026" Z="1.85" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.85" />
                  <Point X="-1.766816500346" Y="4.329665242761" Z="1.85" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.85" />
                  <Point X="-1.864917246349" Y="4.313164777058" Z="1.85" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.85" />
                  <Point X="-2.080065939453" Y="4.539237468223" Z="1.85" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.85" />
                  <Point X="-2.31114725499" Y="4.511645167607" Z="1.85" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.85" />
                  <Point X="-2.947091360621" Y="4.12443247928" Z="1.85" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.85" />
                  <Point X="-3.10378877057" Y="3.317439488696" Z="1.85" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.85" />
                  <Point X="-2.999495190897" Y="3.117115900401" Z="1.85" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.85" />
                  <Point X="-3.010505415587" Y="3.038298152299" Z="1.85" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.85" />
                  <Point X="-3.077960565206" Y="2.996069508276" Z="1.85" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.85" />
                  <Point X="-3.616419859601" Y="3.276405178175" Z="1.85" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.85" />
                  <Point X="-3.905838984517" Y="3.234333009581" Z="1.85" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.85" />
                  <Point X="-4.299861881057" Y="2.688427312386" Z="1.85" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.85" />
                  <Point X="-3.927338854868" Y="1.7879151881" Z="1.85" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.85" />
                  <Point X="-3.68849822299" Y="1.595343190706" Z="1.85" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.85" />
                  <Point X="-3.673505683189" Y="1.537569587837" Z="1.85" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.85" />
                  <Point X="-3.708125828762" Y="1.488948523886" Z="1.85" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.85" />
                  <Point X="-4.528097045836" Y="1.576889710165" Z="1.85" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.85" />
                  <Point X="-4.858886649521" Y="1.458423276055" Z="1.85" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.85" />
                  <Point X="-4.996555697973" Y="0.877603758203" Z="1.85" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.85" />
                  <Point X="-3.978889213866" Y="0.156872624547" Z="1.85" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.85" />
                  <Point X="-3.569035445265" Y="0.043846050766" Z="1.85" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.85" />
                  <Point X="-3.546299386176" Y="0.021724690992" Z="1.85" />
                  <Point X="-3.539556741714" Y="0" Z="1.85" />
                  <Point X="-3.542065214959" Y="-0.008082260069" Z="1.85" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.85" />
                  <Point X="-3.556333149858" Y="-0.035029932368" Z="1.85" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.85" />
                  <Point X="-4.657998574907" Y="-0.338839432209" Z="1.85" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.85" />
                  <Point X="-5.039267824775" Y="-0.593886927682" Z="1.85" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.85" />
                  <Point X="-4.945850354392" Y="-1.133785952976" Z="1.85" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.85" />
                  <Point X="-3.660527541549" Y="-1.364970582421" Z="1.85" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.85" />
                  <Point X="-3.211978042183" Y="-1.311089641238" Z="1.85" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.85" />
                  <Point X="-3.19476540718" Y="-1.330515906967" Z="1.85" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.85" />
                  <Point X="-4.14971775615" Y="-2.080649004995" Z="1.85" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.85" />
                  <Point X="-4.423304640741" Y="-2.485125996116" Z="1.85" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.85" />
                  <Point X="-4.115233149693" Y="-2.967543663381" Z="1.85" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.85" />
                  <Point X="-2.922464696519" Y="-2.757347169988" Z="1.85" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.85" />
                  <Point X="-2.568135383896" Y="-2.560195085235" Z="1.85" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.85" />
                  <Point X="-3.09806984318" Y="-3.512613685973" Z="1.85" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.85" />
                  <Point X="-3.188902084744" Y="-3.94772363362" Z="1.85" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.85" />
                  <Point X="-2.771342103613" Y="-4.251266603205" Z="1.85" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.85" />
                  <Point X="-2.28720347977" Y="-4.235924402299" Z="1.85" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.85" />
                  <Point X="-2.156273808638" Y="-4.109714038081" Z="1.85" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.85" />
                  <Point X="-1.884309805758" Y="-3.98958649778" Z="1.85" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.85" />
                  <Point X="-1.595417340278" Y="-4.059844340728" Z="1.85" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.85" />
                  <Point X="-1.408993748942" Y="-4.291450282129" Z="1.85" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.85" />
                  <Point X="-1.400023887479" Y="-4.780187569521" Z="1.85" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.85" />
                  <Point X="-1.332919706917" Y="-4.900132698248" Z="1.85" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.85" />
                  <Point X="-1.036127971058" Y="-4.971359116382" Z="1.85" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.85" />
                  <Point X="-0.525705814637" Y="-3.924144863561" Z="1.85" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.85" />
                  <Point X="-0.372691435677" Y="-3.45480785161" Z="1.85" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.85" />
                  <Point X="-0.184659870035" Y="-3.261550977512" Z="1.85" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.85" />
                  <Point X="0.068699209326" Y="-3.225561067776" Z="1.85" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.85" />
                  <Point X="0.297754423755" Y="-3.346837904158" Z="1.85" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.85" />
                  <Point X="0.709049045764" Y="-4.608391166255" Z="1.85" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.85" />
                  <Point X="0.866568471871" Y="-5.004879534438" Z="1.85" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.85" />
                  <Point X="1.046558250586" Y="-4.970359663257" Z="1.85" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.85" />
                  <Point X="1.016920164634" Y="-3.725425989298" Z="1.85" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.85" />
                  <Point X="0.971937702904" Y="-3.205779420157" Z="1.85" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.85" />
                  <Point X="1.059963241597" Y="-2.984747731877" Z="1.85" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.85" />
                  <Point X="1.25434603461" Y="-2.869859508221" Z="1.85" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.85" />
                  <Point X="1.48201963014" Y="-2.891379688441" Z="1.85" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.85" />
                  <Point X="2.3841976057" Y="-3.964550741783" Z="1.85" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.85" />
                  <Point X="2.714983136558" Y="-4.292385865396" Z="1.85" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.85" />
                  <Point X="2.908586408766" Y="-4.163632444467" Z="1.85" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.85" />
                  <Point X="2.481455643094" Y="-3.086408019527" Z="1.85" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.85" />
                  <Point X="2.260654917455" Y="-2.663704935811" Z="1.85" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.85" />
                  <Point X="2.257829450342" Y="-2.45753124579" Z="1.85" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.85" />
                  <Point X="2.375367132582" Y="-2.301071859625" Z="1.85" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.85" />
                  <Point X="2.564801867702" Y="-2.242793092036" Z="1.85" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.85" />
                  <Point X="3.70100557758" Y="-2.836293857181" Z="1.85" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.85" />
                  <Point X="4.112460354889" Y="-2.979241340339" Z="1.85" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.85" />
                  <Point X="4.282967523818" Y="-2.728442348251" Z="1.85" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.85" />
                  <Point X="3.51988026652" Y="-1.865614450636" Z="1.85" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.85" />
                  <Point X="3.165497231376" Y="-1.572214299423" Z="1.85" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.85" />
                  <Point X="3.096527466974" Y="-1.411954152346" Z="1.85" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.85" />
                  <Point X="3.137748949754" Y="-1.251583205888" Z="1.85" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.85" />
                  <Point X="3.266967338664" Y="-1.144683445421" Z="1.85" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.85" />
                  <Point X="4.498186694127" Y="-1.260591609819" Z="1.85" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.85" />
                  <Point X="4.929900680238" Y="-1.214089418129" Z="1.85" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.85" />
                  <Point X="5.006779368694" Y="-0.842669304092" Z="1.85" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.85" />
                  <Point X="4.100469216597" Y="-0.315267148572" Z="1.85" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.85" />
                  <Point X="3.722868290457" Y="-0.206311398731" Z="1.85" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.85" />
                  <Point X="3.640392069169" Y="-0.148160148139" Z="1.85" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.85" />
                  <Point X="3.594919841868" Y="-0.07041424442" Z="1.85" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.85" />
                  <Point X="3.586451608556" Y="0.026196286773" Z="1.85" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.85" />
                  <Point X="3.614987369232" Y="0.115788590436" Z="1.85" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.85" />
                  <Point X="3.680527123895" Y="0.181837445511" Z="1.85" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.85" />
                  <Point X="4.695498600709" Y="0.474704784197" Z="1.85" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.85" />
                  <Point X="5.030145494343" Y="0.683934969314" Z="1.85" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.85" />
                  <Point X="4.954637596858" Y="1.10530080206" Z="1.85" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.85" />
                  <Point X="3.84752602934" Y="1.272631926384" Z="1.85" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.85" />
                  <Point X="3.437589381952" Y="1.225398452228" Z="1.85" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.85" />
                  <Point X="3.350076926516" Y="1.24509846255" Z="1.85" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.85" />
                  <Point X="3.28628757591" Y="1.2934775419" Z="1.85" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.85" />
                  <Point X="3.246469994276" Y="1.369936040454" Z="1.85" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.85" />
                  <Point X="3.239428329133" Y="1.453218400399" Z="1.85" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.85" />
                  <Point X="3.27078382623" Y="1.52975363498" Z="1.85" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.85" />
                  <Point X="4.139711591082" Y="2.219131339389" Z="1.85" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.85" />
                  <Point X="4.390606108593" Y="2.548868214391" Z="1.85" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.85" />
                  <Point X="4.174212670996" Y="2.889652995241" Z="1.85" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.85" />
                  <Point X="2.914542907773" Y="2.500632194061" Z="1.85" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.85" />
                  <Point X="2.488108015953" Y="2.261177211859" Z="1.85" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.85" />
                  <Point X="2.410766857639" Y="2.247799096782" Z="1.85" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.85" />
                  <Point X="2.343000406177" Y="2.26554861784" Z="1.85" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.85" />
                  <Point X="2.285209982395" Y="2.314024454205" Z="1.85" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.85" />
                  <Point X="2.251630605977" Y="2.378991586193" Z="1.85" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.85" />
                  <Point X="2.251350730798" Y="2.451361559929" Z="1.85" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.85" />
                  <Point X="2.894993158799" Y="3.597596174182" Z="1.85" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.85" />
                  <Point X="3.026909100427" Y="4.074597017319" Z="1.85" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.85" />
                  <Point X="2.645650556547" Y="4.33186407796" Z="1.85" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.85" />
                  <Point X="2.244120274498" Y="4.552964840587" Z="1.85" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.85" />
                  <Point X="1.828169053761" Y="4.735330516528" Z="1.85" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.85" />
                  <Point X="1.339354656043" Y="4.891114719056" Z="1.85" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.85" />
                  <Point X="0.681071780049" Y="5.025234493856" Z="1.85" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.85" />
                  <Point X="0.052398945476" Y="4.550680010331" Z="1.85" />
                  <Point X="0" Y="4.355124473572" Z="1.85" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>