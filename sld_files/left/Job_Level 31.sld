<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#177" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2280" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000473632812" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.199931640625" Y="-4.396139648438" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140625" />
                  <Point X="-24.457634765625" Y="-3.467376953125" />
                  <Point X="-24.55440625" Y="-3.327947753906" />
                  <Point X="-24.62136328125" Y="-3.2314765625" />
                  <Point X="-24.643248046875" Y="-3.209020263672" />
                  <Point X="-24.66950390625" Y="-3.189776611328" />
                  <Point X="-24.69750390625" Y="-3.175669189453" />
                  <Point X="-24.847251953125" Y="-3.129192871094" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.186572265625" Y="-3.143511962891" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.189776611328" />
                  <Point X="-25.344439453125" Y="-3.209020263672" />
                  <Point X="-25.36632421875" Y="-3.231477294922" />
                  <Point X="-25.46309375" Y="-3.370906494141" />
                  <Point X="-25.53005078125" Y="-3.467377685547" />
                  <Point X="-25.5381875" Y="-3.481572265625" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.71480859375" Y="-4.1238984375" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-25.961041015625" Y="-4.8683125" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.24641796875" Y="-4.802362792969" />
                  <Point X="-26.243462890625" Y="-4.779922363281" />
                  <Point X="-26.2149609375" Y="-4.5634375" />
                  <Point X="-26.21419921875" Y="-4.547929199219" />
                  <Point X="-26.2165078125" Y="-4.516223632812" />
                  <Point X="-26.252283203125" Y="-4.336371582031" />
                  <Point X="-26.277037109375" Y="-4.211932128906" />
                  <Point X="-26.287939453125" Y="-4.182964355469" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.461513671875" Y="-4.010295898437" />
                  <Point X="-26.556904296875" Y="-3.926639404297" />
                  <Point X="-26.5831875" Y="-3.910295898438" />
                  <Point X="-26.612884765625" Y="-3.897994628906" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.826009765625" Y="-3.878972900391" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708740234" />
                  <Point X="-27.014466796875" Y="-3.882028564453" />
                  <Point X="-27.042658203125" Y="-3.894801757812" />
                  <Point X="-27.19512890625" Y="-3.996679931641" />
                  <Point X="-27.300623046875" Y="-4.067169677734" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.3351015625" Y="-4.099461914062" />
                  <Point X="-27.427072265625" Y="-4.2193203125" />
                  <Point X="-27.4801484375" Y="-4.288489257813" />
                  <Point X="-27.628310546875" Y="-4.196750976562" />
                  <Point X="-27.80171484375" Y="-4.0893828125" />
                  <Point X="-28.03786328125" Y="-3.907555908203" />
                  <Point X="-28.104720703125" Y="-3.856078125" />
                  <Point X="-27.863759765625" Y="-3.438720214844" />
                  <Point X="-27.423759765625" Y="-2.676619384766" />
                  <Point X="-27.412859375" Y="-2.647655761719" />
                  <Point X="-27.406587890625" Y="-2.616131347656" />
                  <Point X="-27.40557421875" Y="-2.585198974609" />
                  <Point X="-27.414556640625" Y="-2.555582275391" />
                  <Point X="-27.428771484375" Y="-2.526753662109" />
                  <Point X="-27.446798828125" Y="-2.501593261719" />
                  <Point X="-27.464146484375" Y="-2.484244628906" />
                  <Point X="-27.489302734375" Y="-2.466216796875" />
                  <Point X="-27.5181328125" Y="-2.451997802734" />
                  <Point X="-27.547751953125" Y="-2.443011962891" />
                  <Point X="-27.5786875" Y="-2.444024169922" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.16648046875" Y="-2.765633056641" />
                  <Point X="-28.8180234375" Y="-3.141801513672" />
                  <Point X="-28.9458671875" Y="-2.973842529297" />
                  <Point X="-29.082859375" Y="-2.793861816406" />
                  <Point X="-29.2521640625" Y="-2.50996484375" />
                  <Point X="-29.306142578125" Y="-2.419450439453" />
                  <Point X="-28.875810546875" Y="-2.089244628906" />
                  <Point X="-28.105955078125" Y="-1.498513305664" />
                  <Point X="-28.084576171875" Y="-1.475591186523" />
                  <Point X="-28.066611328125" Y="-1.448458496094" />
                  <Point X="-28.053857421875" Y="-1.419832763672" />
                  <Point X="-28.04615234375" Y="-1.390085449219" />
                  <Point X="-28.04334765625" Y="-1.359654663086" />
                  <Point X="-28.045556640625" Y="-1.327984741211" />
                  <Point X="-28.05255859375" Y="-1.298239746094" />
                  <Point X="-28.068640625" Y="-1.272256469727" />
                  <Point X="-28.08947265625" Y="-1.24830065918" />
                  <Point X="-28.112970703125" Y="-1.228767211914" />
                  <Point X="-28.139453125" Y="-1.213180664062" />
                  <Point X="-28.168716796875" Y="-1.201956665039" />
                  <Point X="-28.200603515625" Y="-1.195474975586" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-28.897591796875" Y="-1.282020385742" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.78049609375" Y="-1.202417724609" />
                  <Point X="-29.834078125" Y="-0.99265057373" />
                  <Point X="-29.878869140625" Y="-0.679464355469" />
                  <Point X="-29.892421875" Y="-0.584698364258" />
                  <Point X="-29.40985546875" Y="-0.455394927979" />
                  <Point X="-28.532875" Y="-0.220408432007" />
                  <Point X="-28.509681640625" Y="-0.210755172729" />
                  <Point X="-28.477708984375" Y="-0.19215007019" />
                  <Point X="-28.464509765625" Y="-0.182884780884" />
                  <Point X="-28.441341796875" Y="-0.163489929199" />
                  <Point X="-28.425365234375" Y="-0.146345352173" />
                  <Point X="-28.4140703125" Y="-0.125811309814" />
                  <Point X="-28.401177734375" Y="-0.093456283569" />
                  <Point X="-28.3967109375" Y="-0.078987014771" />
                  <Point X="-28.390681640625" Y="-0.051976680756" />
                  <Point X="-28.38840234375" Y="-0.030623823166" />
                  <Point X="-28.3909765625" Y="-0.009304466248" />
                  <Point X="-28.398689453125" Y="0.023133892059" />
                  <Point X="-28.403408203125" Y="0.037667362213" />
                  <Point X="-28.4146171875" Y="0.064594367981" />
                  <Point X="-28.426224609375" Y="0.084953781128" />
                  <Point X="-28.4424609375" Y="0.101852523804" />
                  <Point X="-28.47068359375" Y="0.124754959106" />
                  <Point X="-28.484072265625" Y="0.133845077515" />
                  <Point X="-28.510990234375" Y="0.148942596436" />
                  <Point X="-28.532875" Y="0.157848297119" />
                  <Point X="-29.139658203125" Y="0.320435638428" />
                  <Point X="-29.891814453125" Y="0.521975463867" />
                  <Point X="-29.859017578125" Y="0.743621398926" />
                  <Point X="-29.82448828125" Y="0.976968078613" />
                  <Point X="-29.734318359375" Y="1.309721923828" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-29.394306640625" Y="1.382555175781" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228149414" />
                  <Point X="-28.703134765625" Y="1.305263671875" />
                  <Point X="-28.666830078125" Y="1.31671081543" />
                  <Point X="-28.641708984375" Y="1.324631225586" />
                  <Point X="-28.6227734375" Y="1.332963378906" />
                  <Point X="-28.604029296875" Y="1.343786254883" />
                  <Point X="-28.5873515625" Y="1.356016235352" />
                  <Point X="-28.57371484375" Y="1.371565917969" />
                  <Point X="-28.561298828125" Y="1.389297241211" />
                  <Point X="-28.551349609375" Y="1.407432373047" />
                  <Point X="-28.53678125" Y="1.442602539063" />
                  <Point X="-28.526703125" Y="1.466936767578" />
                  <Point X="-28.520912109375" Y="1.486806152344" />
                  <Point X="-28.51715625" Y="1.508120117188" />
                  <Point X="-28.515806640625" Y="1.528755615234" />
                  <Point X="-28.518951171875" Y="1.549194580078" />
                  <Point X="-28.524552734375" Y="1.570099731445" />
                  <Point X="-28.53205078125" Y="1.589378662109" />
                  <Point X="-28.54962890625" Y="1.623145141602" />
                  <Point X="-28.561791015625" Y="1.646508300781" />
                  <Point X="-28.57328125" Y="1.663705444336" />
                  <Point X="-28.587193359375" Y="1.680285644531" />
                  <Point X="-28.60213671875" Y="1.694590332031" />
                  <Point X="-28.950189453125" Y="1.961660644531" />
                  <Point X="-29.351859375" Y="2.269873535156" />
                  <Point X="-29.21532421875" Y="2.503791748047" />
                  <Point X="-29.0811484375" Y="2.733665039062" />
                  <Point X="-28.8423046875" Y="3.040667236328" />
                  <Point X="-28.75050390625" Y="3.158662353516" />
                  <Point X="-28.59275" Y="3.067582763672" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.096228515625" Y="2.821372558594" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.019107421875" Y="2.821587890625" />
                  <Point X="-27.999015625" Y="2.826504150391" />
                  <Point X="-27.98046484375" Y="2.83565234375" />
                  <Point X="-27.9622109375" Y="2.847281005859" />
                  <Point X="-27.946078125" Y="2.860228759766" />
                  <Point X="-27.9101875" Y="2.896119384766" />
                  <Point X="-27.885353515625" Y="2.920952148438" />
                  <Point X="-27.87241015625" Y="2.937078857422" />
                  <Point X="-27.860779296875" Y="2.955333251953" />
                  <Point X="-27.85162890625" Y="2.973885009766" />
                  <Point X="-27.8467109375" Y="2.993977294922" />
                  <Point X="-27.843884765625" Y="3.015436523438" />
                  <Point X="-27.84343359375" Y="3.036120605469" />
                  <Point X="-27.847857421875" Y="3.086684814453" />
                  <Point X="-27.85091796875" Y="3.121670166016" />
                  <Point X="-27.854955078125" Y="3.141962402344" />
                  <Point X="-27.86146484375" Y="3.162604736328" />
                  <Point X="-27.869794921875" Y="3.181533203125" />
                  <Point X="-28.024029296875" Y="3.448672119141" />
                  <Point X="-28.18333203125" Y="3.724595458984" />
                  <Point X="-27.934302734375" Y="3.915523681641" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.3244453125" Y="4.303680175781" />
                  <Point X="-27.16703515625" Y="4.391133789062" />
                  <Point X="-27.161390625" Y="4.383776855469" />
                  <Point X="-27.0431953125" Y="4.229741699219" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.99511328125" Y="4.18939453125" />
                  <Point X="-26.9388359375" Y="4.160098144531" />
                  <Point X="-26.899896484375" Y="4.139827636719" />
                  <Point X="-26.8806171875" Y="4.132331054688" />
                  <Point X="-26.8597109375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.12358203125" />
                  <Point X="-26.818626953125" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.777453125" Y="4.134481933594" />
                  <Point X="-26.718837890625" Y="4.158762207031" />
                  <Point X="-26.678279296875" Y="4.175561523437" />
                  <Point X="-26.660146484375" Y="4.185509277344" />
                  <Point X="-26.642416015625" Y="4.197923828125" />
                  <Point X="-26.626865234375" Y="4.211561523438" />
                  <Point X="-26.6146328125" Y="4.2282421875" />
                  <Point X="-26.603810546875" Y="4.246985839844" />
                  <Point X="-26.595478515625" Y="4.265921386719" />
                  <Point X="-26.576400390625" Y="4.326431152344" />
                  <Point X="-26.56319921875" Y="4.368297851562" />
                  <Point X="-26.5591640625" Y="4.388583496094" />
                  <Point X="-26.55727734375" Y="4.410145996094" />
                  <Point X="-26.557728515625" Y="4.430827148438" />
                  <Point X="-26.575263671875" Y="4.564015625" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.252146484375" Y="4.724994628906" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.493595703125" Y="4.863180664062" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.24812109375" Y="4.712581054688" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.774755859375" Y="4.581249023438" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.4201015625" Y="4.859401855469" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.77866015625" Y="4.740647949219" />
                  <Point X="-23.51897265625" Y="4.677950195312" />
                  <Point X="-23.273994140625" Y="4.589095703125" />
                  <Point X="-23.105357421875" Y="4.527930175781" />
                  <Point X="-22.8678828125" Y="4.41687109375" />
                  <Point X="-22.705423828125" Y="4.340894042969" />
                  <Point X="-22.475990234375" Y="4.207226074219" />
                  <Point X="-22.319017578125" Y="4.1157734375" />
                  <Point X="-22.102658203125" Y="3.961909912109" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.343150390625" Y="3.433172851562" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857919921875" Y="2.539937988281" />
                  <Point X="-22.866921875" Y="2.516057861328" />
                  <Point X="-22.879611328125" Y="2.468604492188" />
                  <Point X="-22.888392578125" Y="2.435771484375" />
                  <Point X="-22.891380859375" Y="2.4179375" />
                  <Point X="-22.892271484375" Y="2.380952636719" />
                  <Point X="-22.887322265625" Y="2.339919189453" />
                  <Point X="-22.883900390625" Y="2.311527832031" />
                  <Point X="-22.87855859375" Y="2.289607177734" />
                  <Point X="-22.870291015625" Y="2.267514892578" />
                  <Point X="-22.859927734375" Y="2.247469726562" />
                  <Point X="-22.834537109375" Y="2.210051025391" />
                  <Point X="-22.81696875" Y="2.184161132813" />
                  <Point X="-22.80553515625" Y="2.170329589844" />
                  <Point X="-22.778400390625" Y="2.145592529297" />
                  <Point X="-22.740982421875" Y="2.120202392578" />
                  <Point X="-22.715091796875" Y="2.102635009766" />
                  <Point X="-22.695044921875" Y="2.092271728516" />
                  <Point X="-22.67295703125" Y="2.084006103516" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.610001953125" Y="2.073715576172" />
                  <Point X="-22.581611328125" Y="2.070291992188" />
                  <Point X="-22.563533203125" Y="2.069845703125" />
                  <Point X="-22.52679296875" Y="2.074171142578" />
                  <Point X="-22.47933984375" Y="2.086860839844" />
                  <Point X="-22.446505859375" Y="2.095640869141" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.80115625" Y="2.462506591797" />
                  <Point X="-21.032671875" Y="2.90619140625" />
                  <Point X="-20.9698359375" Y="2.818864257813" />
                  <Point X="-20.876728515625" Y="2.689467773438" />
                  <Point X="-20.756109375" Y="2.490138427734" />
                  <Point X="-20.73780078125" Y="2.459883300781" />
                  <Point X="-21.09871875" Y="2.182941650391" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.77857421875" Y="1.6602421875" />
                  <Point X="-21.796025390625" Y="1.641627075195" />
                  <Point X="-21.830177734375" Y="1.597072998047" />
                  <Point X="-21.853806640625" Y="1.566245849609" />
                  <Point X="-21.86339453125" Y="1.550910888672" />
                  <Point X="-21.8783671875" Y="1.517088623047" />
                  <Point X="-21.89108984375" Y="1.471598754883" />
                  <Point X="-21.899892578125" Y="1.440124267578" />
                  <Point X="-21.90334765625" Y="1.417824707031" />
                  <Point X="-21.9041640625" Y="1.394252563477" />
                  <Point X="-21.90226171875" Y="1.371766845703" />
                  <Point X="-21.891818359375" Y="1.321153320312" />
                  <Point X="-21.884591796875" Y="1.286134155273" />
                  <Point X="-21.8793203125" Y="1.268976806641" />
                  <Point X="-21.863716796875" Y="1.235741333008" />
                  <Point X="-21.8353125" Y="1.192567749023" />
                  <Point X="-21.81566015625" Y="1.162695922852" />
                  <Point X="-21.801109375" Y="1.145452026367" />
                  <Point X="-21.783865234375" Y="1.129361328125" />
                  <Point X="-21.765654296875" Y="1.116034667969" />
                  <Point X="-21.7244921875" Y="1.092864013672" />
                  <Point X="-21.69601171875" Y="1.07683215332" />
                  <Point X="-21.6794765625" Y="1.069500366211" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.5882265625" Y="1.052083251953" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.93204296875" Y="1.123310302734" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.1940546875" Y="1.097077148437" />
                  <Point X="-20.154060546875" Y="0.932788513184" />
                  <Point X="-20.116052734375" Y="0.688673583984" />
                  <Point X="-20.109134765625" Y="0.644238647461" />
                  <Point X="-20.515015625" Y="0.535482971191" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.2952109375" Y="0.325585754395" />
                  <Point X="-21.318453125" Y="0.315067901611" />
                  <Point X="-21.373130859375" Y="0.283463012695" />
                  <Point X="-21.410962890625" Y="0.261595489502" />
                  <Point X="-21.425685546875" Y="0.251097351074" />
                  <Point X="-21.45246875" Y="0.225576538086" />
                  <Point X="-21.485275390625" Y="0.183772872925" />
                  <Point X="-21.507974609375" Y="0.154848754883" />
                  <Point X="-21.51969921875" Y="0.135567443848" />
                  <Point X="-21.52947265625" Y="0.114103523254" />
                  <Point X="-21.536318359375" Y="0.092603782654" />
                  <Point X="-21.54725390625" Y="0.035502075195" />
                  <Point X="-21.5548203125" Y="-0.004006527424" />
                  <Point X="-21.556515625" Y="-0.02187528801" />
                  <Point X="-21.5548203125" Y="-0.058553455353" />
                  <Point X="-21.543884765625" Y="-0.115655166626" />
                  <Point X="-21.536318359375" Y="-0.155163925171" />
                  <Point X="-21.52947265625" Y="-0.176663650513" />
                  <Point X="-21.51969921875" Y="-0.198127578735" />
                  <Point X="-21.507974609375" Y="-0.217408889771" />
                  <Point X="-21.47516796875" Y="-0.259212554932" />
                  <Point X="-21.45246875" Y="-0.288136535645" />
                  <Point X="-21.439998046875" Y="-0.301237243652" />
                  <Point X="-21.410962890625" Y="-0.324155609131" />
                  <Point X="-21.35628515625" Y="-0.355760498047" />
                  <Point X="-21.318453125" Y="-0.377627868652" />
                  <Point X="-21.307291015625" Y="-0.383138305664" />
                  <Point X="-21.283419921875" Y="-0.392149963379" />
                  <Point X="-20.751759765625" Y="-0.53460748291" />
                  <Point X="-20.108525390625" Y="-0.706961853027" />
                  <Point X="-20.122646484375" Y="-0.800623901367" />
                  <Point X="-20.144974609375" Y="-0.948723754883" />
                  <Point X="-20.193673828125" Y="-1.162126464844" />
                  <Point X="-20.19882421875" Y="-1.184698974609" />
                  <Point X="-20.684486328125" Y="-1.120760620117" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710266113" />
                  <Point X="-21.62533984375" Y="-1.005508666992" />
                  <Point X="-21.732654296875" Y="-1.028834106445" />
                  <Point X="-21.806904296875" Y="-1.04497265625" />
                  <Point X="-21.8360234375" Y="-1.056596313477" />
                  <Point X="-21.8638515625" Y="-1.073488891602" />
                  <Point X="-21.8876015625" Y="-1.093959960937" />
                  <Point X="-21.952466796875" Y="-1.171971679688" />
                  <Point X="-21.997345703125" Y="-1.225947998047" />
                  <Point X="-22.012068359375" Y="-1.250335205078" />
                  <Point X="-22.02341015625" Y="-1.277720458984" />
                  <Point X="-22.030240234375" Y="-1.305365722656" />
                  <Point X="-22.039537109375" Y="-1.406394287109" />
                  <Point X="-22.04596875" Y="-1.476296020508" />
                  <Point X="-22.04365234375" Y="-1.507562011719" />
                  <Point X="-22.035921875" Y="-1.539182739258" />
                  <Point X="-22.023548828125" Y="-1.567996704102" />
                  <Point X="-21.96416015625" Y="-1.660372314453" />
                  <Point X="-21.923068359375" Y="-1.724287353516" />
                  <Point X="-21.9130625" Y="-1.737243286133" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.395986328125" Y="-2.139495361328" />
                  <Point X="-20.786876953125" Y="-2.6068828125" />
                  <Point X="-20.81224609375" Y="-2.647934814453" />
                  <Point X="-20.875193359375" Y="-2.749792724609" />
                  <Point X="-20.971017578125" Y="-2.8859453125" />
                  <Point X="-21.40537109375" Y="-2.635170898438" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.245775390625" Y="-2.159825195312" />
                  <Point X="-22.37349609375" Y="-2.136759033203" />
                  <Point X="-22.461865234375" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135177001953" />
                  <Point X="-22.66126953125" Y="-2.191018798828" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.757615234375" Y="-2.246548828125" />
                  <Point X="-22.77857421875" Y="-2.2675078125" />
                  <Point X="-22.795466796875" Y="-2.290438476562" />
                  <Point X="-22.85130859375" Y="-2.396542724609" />
                  <Point X="-22.889947265625" Y="-2.469956542969" />
                  <Point X="-22.899771484375" Y="-2.499735107422" />
                  <Point X="-22.904728515625" Y="-2.531909423828" />
                  <Point X="-22.90432421875" Y="-2.563259277344" />
                  <Point X="-22.8812578125" Y="-2.690979736328" />
                  <Point X="-22.865296875" Y="-2.779349609375" />
                  <Point X="-22.86101171875" Y="-2.795141601562" />
                  <Point X="-22.8481796875" Y="-2.826078369141" />
                  <Point X="-22.5311328125" Y="-3.375221679688" />
                  <Point X="-22.13871484375" Y="-4.054907470703" />
                  <Point X="-22.14345703125" Y="-4.058294677734" />
                  <Point X="-22.218134765625" Y="-4.111634277344" />
                  <Point X="-22.298232421875" Y="-4.163480957031" />
                  <Point X="-22.635806640625" Y="-3.723547607422" />
                  <Point X="-23.241451171875" Y="-2.934254882812" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.404041015625" Y="-2.819572021484" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.720666015625" Y="-2.753794189453" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.843634765625" Y="-2.769396972656" />
                  <Point X="-23.87101953125" Y="-2.780739746094" />
                  <Point X="-23.89540234375" Y="-2.7954609375" />
                  <Point X="-24.00178125" Y="-2.883912109375" />
                  <Point X="-24.07538671875" Y="-2.945111328125" />
                  <Point X="-24.095857421875" Y="-2.968860839844" />
                  <Point X="-24.11275" Y="-2.9966875" />
                  <Point X="-24.124375" Y="-3.02580859375" />
                  <Point X="-24.156181640625" Y="-3.172145751953" />
                  <Point X="-24.178189453125" Y="-3.273396484375" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323120361328" />
                  <Point X="-24.090408203125" Y="-4.005591064453" />
                  <Point X="-23.977935546875" Y="-4.859915527344" />
                  <Point X="-24.024318359375" Y="-4.870083007812" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.05841796875" Y="-4.752638183594" />
                  <Point X="-26.141244140625" Y="-4.731328125" />
                  <Point X="-26.1207734375" Y="-4.575840332031" />
                  <Point X="-26.120076171875" Y="-4.568098144531" />
                  <Point X="-26.11944921875" Y="-4.541030273438" />
                  <Point X="-26.1217578125" Y="-4.509324707031" />
                  <Point X="-26.123333984375" Y="-4.497689941406" />
                  <Point X="-26.159109375" Y="-4.317837890625" />
                  <Point X="-26.18386328125" Y="-4.1933984375" />
                  <Point X="-26.188125" Y="-4.178469238281" />
                  <Point X="-26.19902734375" Y="-4.149501464844" />
                  <Point X="-26.20566796875" Y="-4.135462890625" />
                  <Point X="-26.221740234375" Y="-4.107625976562" />
                  <Point X="-26.23057421875" Y="-4.094861083984" />
                  <Point X="-26.25020703125" Y="-4.070937744141" />
                  <Point X="-26.261005859375" Y="-4.059779296875" />
                  <Point X="-26.398875" Y="-3.93887109375" />
                  <Point X="-26.494265625" Y="-3.855214599609" />
                  <Point X="-26.50673828125" Y="-3.845964599609" />
                  <Point X="-26.533021484375" Y="-3.82962109375" />
                  <Point X="-26.54683203125" Y="-3.822527587891" />
                  <Point X="-26.576529296875" Y="-3.810226318359" />
                  <Point X="-26.5913125" Y="-3.805476318359" />
                  <Point X="-26.621455078125" Y="-3.798447998047" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.819796875" Y="-3.784176269531" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166259766" />
                  <Point X="-27.008005859375" Y="-3.781946044922" />
                  <Point X="-27.0390546875" Y="-3.790265869141" />
                  <Point X="-27.053673828125" Y="-3.795496337891" />
                  <Point X="-27.081865234375" Y="-3.80826953125" />
                  <Point X="-27.0954375" Y="-3.815812255859" />
                  <Point X="-27.247908203125" Y="-3.917690429688" />
                  <Point X="-27.35340234375" Y="-3.988180175781" />
                  <Point X="-27.35968359375" Y="-3.992759765625" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032770507812" />
                  <Point X="-27.410470703125" Y="-4.041629638672" />
                  <Point X="-27.50244140625" Y="-4.161487792969" />
                  <Point X="-27.503201171875" Y="-4.162478515625" />
                  <Point X="-27.578298828125" Y="-4.11598046875" />
                  <Point X="-27.74759375" Y="-4.011156494141" />
                  <Point X="-27.97990625" Y="-3.832283447266" />
                  <Point X="-27.980861328125" Y="-3.831548339844" />
                  <Point X="-27.781486328125" Y="-3.486220214844" />
                  <Point X="-27.341486328125" Y="-2.724119384766" />
                  <Point X="-27.33484765625" Y="-2.710081054688" />
                  <Point X="-27.323947265625" Y="-2.681117431641" />
                  <Point X="-27.319685546875" Y="-2.666191894531" />
                  <Point X="-27.3134140625" Y="-2.634667480469" />
                  <Point X="-27.311638671875" Y="-2.619242919922" />
                  <Point X="-27.310625" Y="-2.588310546875" />
                  <Point X="-27.3146640625" Y="-2.557626708984" />
                  <Point X="-27.323646484375" Y="-2.528010009766" />
                  <Point X="-27.3293515625" Y="-2.513569335938" />
                  <Point X="-27.34356640625" Y="-2.484740722656" />
                  <Point X="-27.351546875" Y="-2.471423095703" />
                  <Point X="-27.36957421875" Y="-2.446262695312" />
                  <Point X="-27.37962109375" Y="-2.434419921875" />
                  <Point X="-27.39696875" Y="-2.417071289062" />
                  <Point X="-27.40880859375" Y="-2.407025878906" />
                  <Point X="-27.43396484375" Y="-2.388998046875" />
                  <Point X="-27.44728125" Y="-2.381015625" />
                  <Point X="-27.476111328125" Y="-2.366796630859" />
                  <Point X="-27.490552734375" Y="-2.361089355469" />
                  <Point X="-27.520171875" Y="-2.352103515625" />
                  <Point X="-27.550859375" Y="-2.348062744141" />
                  <Point X="-27.581794921875" Y="-2.349074951172" />
                  <Point X="-27.597220703125" Y="-2.350849365234" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.21398046875" Y="-2.683360595703" />
                  <Point X="-28.793087890625" Y="-3.017708496094" />
                  <Point X="-28.8702734375" Y="-2.916303955078" />
                  <Point X="-29.004021484375" Y="-2.740584716797" />
                  <Point X="-29.170572265625" Y="-2.461306396484" />
                  <Point X="-29.181265625" Y="-2.443374023438" />
                  <Point X="-28.817978515625" Y="-2.16461328125" />
                  <Point X="-28.048123046875" Y="-1.573881835938" />
                  <Point X="-28.036482421875" Y="-1.563309082031" />
                  <Point X="-28.015103515625" Y="-1.540386962891" />
                  <Point X="-28.005365234375" Y="-1.528037597656" />
                  <Point X="-27.987400390625" Y="-1.500904907227" />
                  <Point X="-27.979833984375" Y="-1.48712097168" />
                  <Point X="-27.967080078125" Y="-1.458495117188" />
                  <Point X="-27.961892578125" Y="-1.443653198242" />
                  <Point X="-27.9541875" Y="-1.413905883789" />
                  <Point X="-27.951552734375" Y="-1.398804321289" />
                  <Point X="-27.948748046875" Y="-1.368373535156" />
                  <Point X="-27.948578125" Y="-1.353044433594" />
                  <Point X="-27.950787109375" Y="-1.321374511719" />
                  <Point X="-27.953083984375" Y="-1.306216796875" />
                  <Point X="-27.9600859375" Y="-1.276471801758" />
                  <Point X="-27.971779296875" Y="-1.248242431641" />
                  <Point X="-27.987861328125" Y="-1.222259155273" />
                  <Point X="-27.996955078125" Y="-1.20991796875" />
                  <Point X="-28.017787109375" Y="-1.185962158203" />
                  <Point X="-28.028744140625" Y="-1.175245849609" />
                  <Point X="-28.0522421875" Y="-1.155712280273" />
                  <Point X="-28.064783203125" Y="-1.146895141602" />
                  <Point X="-28.091265625" Y="-1.13130859375" />
                  <Point X="-28.10543359375" Y="-1.124481079102" />
                  <Point X="-28.134697265625" Y="-1.113257080078" />
                  <Point X="-28.14979296875" Y="-1.108860595703" />
                  <Point X="-28.1816796875" Y="-1.10237890625" />
                  <Point X="-28.197296875" Y="-1.100532470703" />
                  <Point X="-28.22862109375" Y="-1.09944152832" />
                  <Point X="-28.244328125" Y="-1.100196655273" />
                  <Point X="-28.9099921875" Y="-1.187833129883" />
                  <Point X="-29.66091796875" Y="-1.286694335938" />
                  <Point X="-29.688451171875" Y="-1.17890637207" />
                  <Point X="-29.740763671875" Y="-0.974112609863" />
                  <Point X="-29.784826171875" Y="-0.666014526367" />
                  <Point X="-29.786451171875" Y="-0.654654663086" />
                  <Point X="-29.385267578125" Y="-0.547157836914" />
                  <Point X="-28.508287109375" Y="-0.312171417236" />
                  <Point X="-28.49637109375" Y="-0.30811505127" />
                  <Point X="-28.473177734375" Y="-0.298461700439" />
                  <Point X="-28.461900390625" Y="-0.29286517334" />
                  <Point X="-28.429927734375" Y="-0.27425994873" />
                  <Point X="-28.42312890625" Y="-0.269905609131" />
                  <Point X="-28.403529296875" Y="-0.255729217529" />
                  <Point X="-28.380361328125" Y="-0.236334365845" />
                  <Point X="-28.371841796875" Y="-0.228255828857" />
                  <Point X="-28.355865234375" Y="-0.211111282349" />
                  <Point X="-28.342126953125" Y="-0.192131408691" />
                  <Point X="-28.33083203125" Y="-0.171597366333" />
                  <Point X="-28.325818359375" Y="-0.160977233887" />
                  <Point X="-28.31292578125" Y="-0.128622177124" />
                  <Point X="-28.310404296875" Y="-0.121478767395" />
                  <Point X="-28.3039921875" Y="-0.099683670044" />
                  <Point X="-28.297962890625" Y="-0.07267339325" />
                  <Point X="-28.29621875" Y="-0.062060085297" />
                  <Point X="-28.293939453125" Y="-0.040707290649" />
                  <Point X="-28.294087890625" Y="-0.019235742569" />
                  <Point X="-28.296662109375" Y="0.002083614111" />
                  <Point X="-28.298552734375" Y="0.012671061516" />
                  <Point X="-28.306265625" Y="0.045109489441" />
                  <Point X="-28.30833203125" Y="0.052471076965" />
                  <Point X="-28.315703125" Y="0.07417640686" />
                  <Point X="-28.326912109375" Y="0.10110345459" />
                  <Point X="-28.332087890625" Y="0.111646461487" />
                  <Point X="-28.3436953125" Y="0.132005859375" />
                  <Point X="-28.357720703125" Y="0.150772918701" />
                  <Point X="-28.37395703125" Y="0.167671630859" />
                  <Point X="-28.382599609375" Y="0.175619689941" />
                  <Point X="-28.410822265625" Y="0.198522186279" />
                  <Point X="-28.4173203125" Y="0.20335168457" />
                  <Point X="-28.437599609375" Y="0.216702316284" />
                  <Point X="-28.464517578125" Y="0.231799880981" />
                  <Point X="-28.475181640625" Y="0.236935836792" />
                  <Point X="-28.49706640625" Y="0.245841629028" />
                  <Point X="-28.508287109375" Y="0.249611297607" />
                  <Point X="-29.1150703125" Y="0.412198608398" />
                  <Point X="-29.7854453125" Y="0.591824951172" />
                  <Point X="-29.765041015625" Y="0.729715637207" />
                  <Point X="-29.73133203125" Y="0.957520019531" />
                  <Point X="-29.642625" Y="1.28487487793" />
                  <Point X="-29.633583984375" Y="1.318237060547" />
                  <Point X="-29.40670703125" Y="1.288367919922" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815551758" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.204703369141" />
                  <Point X="-28.71514453125" Y="1.206589599609" />
                  <Point X="-28.704890625" Y="1.208053344727" />
                  <Point X="-28.6846015625" Y="1.212088867188" />
                  <Point X="-28.67456640625" Y="1.214660766602" />
                  <Point X="-28.63826171875" Y="1.226107910156" />
                  <Point X="-28.613140625" Y="1.234028320312" />
                  <Point X="-28.603447265625" Y="1.237677124023" />
                  <Point X="-28.58451171875" Y="1.246009399414" />
                  <Point X="-28.57526953125" Y="1.250692749023" />
                  <Point X="-28.556525390625" Y="1.261515625" />
                  <Point X="-28.5478515625" Y="1.267177001953" />
                  <Point X="-28.531173828125" Y="1.279406982422" />
                  <Point X="-28.515927734375" Y="1.293378295898" />
                  <Point X="-28.502291015625" Y="1.308927978516" />
                  <Point X="-28.495896484375" Y="1.317074951172" />
                  <Point X="-28.48348046875" Y="1.334806274414" />
                  <Point X="-28.478009765625" Y="1.343603393555" />
                  <Point X="-28.468060546875" Y="1.361738647461" />
                  <Point X="-28.46358203125" Y="1.371076538086" />
                  <Point X="-28.449013671875" Y="1.406246704102" />
                  <Point X="-28.438935546875" Y="1.430580932617" />
                  <Point X="-28.435498046875" Y="1.440354614258" />
                  <Point X="-28.42970703125" Y="1.460223999023" />
                  <Point X="-28.427353515625" Y="1.470319702148" />
                  <Point X="-28.42359765625" Y="1.491633666992" />
                  <Point X="-28.422359375" Y="1.501920043945" />
                  <Point X="-28.421009765625" Y="1.522555664062" />
                  <Point X="-28.421912109375" Y="1.543201416016" />
                  <Point X="-28.425056640625" Y="1.563640380859" />
                  <Point X="-28.4271875" Y="1.573782470703" />
                  <Point X="-28.4327890625" Y="1.594687866211" />
                  <Point X="-28.436013671875" Y="1.60453503418" />
                  <Point X="-28.44351171875" Y="1.623813842773" />
                  <Point X="-28.44778515625" Y="1.633245605469" />
                  <Point X="-28.46536328125" Y="1.667011962891" />
                  <Point X="-28.477525390625" Y="1.690375244141" />
                  <Point X="-28.48280078125" Y="1.699285766602" />
                  <Point X="-28.494291015625" Y="1.716482910156" />
                  <Point X="-28.500505859375" Y="1.72476940918" />
                  <Point X="-28.51441796875" Y="1.741349609375" />
                  <Point X="-28.5215" Y="1.748911254883" />
                  <Point X="-28.536443359375" Y="1.763215942383" />
                  <Point X="-28.5443046875" Y="1.769958862305" />
                  <Point X="-28.892357421875" Y="2.037029174805" />
                  <Point X="-29.22761328125" Y="2.294281005859" />
                  <Point X="-29.13327734375" Y="2.45590234375" />
                  <Point X="-29.00228515625" Y="2.680322509766" />
                  <Point X="-28.76732421875" Y="2.982333007812" />
                  <Point X="-28.726337890625" Y="3.035013671875" />
                  <Point X="-28.64025" Y="2.985310302734" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.1045078125" Y="2.726734130859" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.00670703125" Y="2.727400634766" />
                  <Point X="-27.99652734375" Y="2.729310302734" />
                  <Point X="-27.976435546875" Y="2.7342265625" />
                  <Point X="-27.956998046875" Y="2.741301269531" />
                  <Point X="-27.938447265625" Y="2.750449462891" />
                  <Point X="-27.929421875" Y="2.755529541016" />
                  <Point X="-27.91116796875" Y="2.767158203125" />
                  <Point X="-27.902748046875" Y="2.773191650391" />
                  <Point X="-27.886615234375" Y="2.786139404297" />
                  <Point X="-27.87890234375" Y="2.793053710938" />
                  <Point X="-27.84301171875" Y="2.828944335938" />
                  <Point X="-27.818177734375" Y="2.853777099609" />
                  <Point X="-27.811265625" Y="2.861488525391" />
                  <Point X="-27.798322265625" Y="2.877615234375" />
                  <Point X="-27.792291015625" Y="2.886030517578" />
                  <Point X="-27.78066015625" Y="2.904284912109" />
                  <Point X="-27.775580078125" Y="2.913309570313" />
                  <Point X="-27.7664296875" Y="2.931861328125" />
                  <Point X="-27.759353515625" Y="2.951298828125" />
                  <Point X="-27.754435546875" Y="2.971391113281" />
                  <Point X="-27.7525234375" Y="2.981572998047" />
                  <Point X="-27.749697265625" Y="3.003032226562" />
                  <Point X="-27.748908203125" Y="3.013364746094" />
                  <Point X="-27.74845703125" Y="3.034048828125" />
                  <Point X="-27.748794921875" Y="3.044400390625" />
                  <Point X="-27.75321875" Y="3.094964599609" />
                  <Point X="-27.756279296875" Y="3.129949951172" />
                  <Point X="-27.757744140625" Y="3.14020703125" />
                  <Point X="-27.76178125" Y="3.160499267578" />
                  <Point X="-27.764353515625" Y="3.170534423828" />
                  <Point X="-27.77086328125" Y="3.191176757812" />
                  <Point X="-27.77451171875" Y="3.200870849609" />
                  <Point X="-27.782841796875" Y="3.219799316406" />
                  <Point X="-27.7875234375" Y="3.229033691406" />
                  <Point X="-27.9417578125" Y="3.496172607422" />
                  <Point X="-28.05938671875" Y="3.699914306641" />
                  <Point X="-27.8765" Y="3.840131835938" />
                  <Point X="-27.648365234375" Y="4.015041748047" />
                  <Point X="-27.27830859375" Y="4.220635742188" />
                  <Point X="-27.1925234375" Y="4.268296386719" />
                  <Point X="-27.11856640625" Y="4.171913574219" />
                  <Point X="-27.111822265625" Y="4.164050292969" />
                  <Point X="-27.09751953125" Y="4.149108398438" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.06509375" Y="4.121898925781" />
                  <Point X="-27.04789453125" Y="4.11040625" />
                  <Point X="-27.03898046875" Y="4.105128417969" />
                  <Point X="-26.982703125" Y="4.075832275391" />
                  <Point X="-26.943763671875" Y="4.055561767578" />
                  <Point X="-26.934326171875" Y="4.051285644531" />
                  <Point X="-26.915046875" Y="4.0437890625" />
                  <Point X="-26.905205078125" Y="4.040568359375" />
                  <Point X="-26.884298828125" Y="4.034966308594" />
                  <Point X="-26.8741640625" Y="4.032834960938" />
                  <Point X="-26.853720703125" Y="4.029687988281" />
                  <Point X="-26.833052734375" Y="4.028785400391" />
                  <Point X="-26.812412109375" Y="4.030138427734" />
                  <Point X="-26.802130859375" Y="4.031378417969" />
                  <Point X="-26.78081640625" Y="4.03513671875" />
                  <Point X="-26.770728515625" Y="4.037488769531" />
                  <Point X="-26.750869140625" Y="4.04327734375" />
                  <Point X="-26.74109765625" Y="4.046713867188" />
                  <Point X="-26.682482421875" Y="4.070994140625" />
                  <Point X="-26.641923828125" Y="4.087793457031" />
                  <Point X="-26.6325859375" Y="4.092271972656" />
                  <Point X="-26.614453125" Y="4.102219726562" />
                  <Point X="-26.605658203125" Y="4.107688964844" />
                  <Point X="-26.587927734375" Y="4.120103515625" />
                  <Point X="-26.57977734375" Y="4.126499023438" />
                  <Point X="-26.5642265625" Y="4.14013671875" />
                  <Point X="-26.550255859375" Y="4.155382324219" />
                  <Point X="-26.5380234375" Y="4.172062988281" />
                  <Point X="-26.532361328125" Y="4.180740234375" />
                  <Point X="-26.5215390625" Y="4.199483886719" />
                  <Point X="-26.51685546875" Y="4.208724121094" />
                  <Point X="-26.5085234375" Y="4.227659667969" />
                  <Point X="-26.504875" Y="4.237354980469" />
                  <Point X="-26.485796875" Y="4.297864746094" />
                  <Point X="-26.472595703125" Y="4.339731445312" />
                  <Point X="-26.470025390625" Y="4.349763671875" />
                  <Point X="-26.465990234375" Y="4.370049316406" />
                  <Point X="-26.464525390625" Y="4.380302734375" />
                  <Point X="-26.462638671875" Y="4.401865234375" />
                  <Point X="-26.46230078125" Y="4.412217773438" />
                  <Point X="-26.462751953125" Y="4.432898925781" />
                  <Point X="-26.463541015625" Y="4.443227539062" />
                  <Point X="-26.479263671875" Y="4.562655273438" />
                  <Point X="-26.2265" Y="4.633521972656" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.482552734375" Y="4.768824707031" />
                  <Point X="-25.36522265625" Y="4.782556152344" />
                  <Point X="-25.339884765625" Y="4.687993164062" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.6829921875" Y="4.556661132812" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.42999609375" Y="4.764918457031" />
                  <Point X="-24.172123046875" Y="4.737912109375" />
                  <Point X="-23.800955078125" Y="4.648301269531" />
                  <Point X="-23.546404296875" Y="4.58684375" />
                  <Point X="-23.30638671875" Y="4.499788574219" />
                  <Point X="-23.1417421875" Y="4.440070800781" />
                  <Point X="-22.908126953125" Y="4.330816894531" />
                  <Point X="-22.749546875" Y="4.256653808594" />
                  <Point X="-22.5238125" Y="4.125141113281" />
                  <Point X="-22.37056640625" Y="4.035859130859" />
                  <Point X="-22.182216796875" Y="3.901915527344" />
                  <Point X="-22.425421875" Y="3.480672851563" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.9376171875" Y="2.593118164062" />
                  <Point X="-22.946814453125" Y="2.573447753906" />
                  <Point X="-22.95581640625" Y="2.549567626953" />
                  <Point X="-22.958697265625" Y="2.540599365234" />
                  <Point X="-22.97138671875" Y="2.493145996094" />
                  <Point X="-22.98016796875" Y="2.460312988281" />
                  <Point X="-22.9820859375" Y="2.451470947266" />
                  <Point X="-22.986353515625" Y="2.420224609375" />
                  <Point X="-22.987244140625" Y="2.383239746094" />
                  <Point X="-22.986587890625" Y="2.369576660156" />
                  <Point X="-22.981638671875" Y="2.328543212891" />
                  <Point X="-22.978216796875" Y="2.300151855469" />
                  <Point X="-22.97619921875" Y="2.289035644531" />
                  <Point X="-22.970857421875" Y="2.267114990234" />
                  <Point X="-22.967533203125" Y="2.256310546875" />
                  <Point X="-22.959265625" Y="2.234218261719" />
                  <Point X="-22.9546796875" Y="2.223885986328" />
                  <Point X="-22.94431640625" Y="2.203840820312" />
                  <Point X="-22.9385390625" Y="2.194127929688" />
                  <Point X="-22.9131484375" Y="2.156709228516" />
                  <Point X="-22.895580078125" Y="2.130819335937" />
                  <Point X="-22.89019140625" Y="2.123633789062" />
                  <Point X="-22.869537109375" Y="2.100124267578" />
                  <Point X="-22.84240234375" Y="2.075387207031" />
                  <Point X="-22.8317421875" Y="2.066981689453" />
                  <Point X="-22.79432421875" Y="2.041591552734" />
                  <Point X="-22.76843359375" Y="2.024024169922" />
                  <Point X="-22.758716796875" Y="2.018244384766" />
                  <Point X="-22.738669921875" Y="2.007881103516" />
                  <Point X="-22.72833984375" Y="2.003297363281" />
                  <Point X="-22.706251953125" Y="1.995031860352" />
                  <Point X="-22.695451171875" Y="1.991707519531" />
                  <Point X="-22.673529296875" Y="1.986364990234" />
                  <Point X="-22.662408203125" Y="1.984346801758" />
                  <Point X="-22.621375" Y="1.979398925781" />
                  <Point X="-22.592984375" Y="1.975975219727" />
                  <Point X="-22.583955078125" Y="1.975320922852" />
                  <Point X="-22.55242578125" Y="1.975497314453" />
                  <Point X="-22.515685546875" Y="1.979822753906" />
                  <Point X="-22.50225" Y="1.982395996094" />
                  <Point X="-22.454796875" Y="1.995085693359" />
                  <Point X="-22.421962890625" Y="2.003865722656" />
                  <Point X="-22.416" Y="2.005671875" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572875977" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.75365625" Y="2.380234130859" />
                  <Point X="-21.05959375" Y="2.780951416016" />
                  <Point X="-21.04694921875" Y="2.763378417969" />
                  <Point X="-20.95604296875" Y="2.637041503906" />
                  <Point X="-20.863115234375" Y="2.483471679688" />
                  <Point X="-21.15655078125" Y="2.258310302734" />
                  <Point X="-21.827046875" Y="1.743820068359" />
                  <Point X="-21.831857421875" Y="1.739871459961" />
                  <Point X="-21.847880859375" Y="1.725215576172" />
                  <Point X="-21.86533203125" Y="1.706600341797" />
                  <Point X="-21.871421875" Y="1.699421875" />
                  <Point X="-21.90557421875" Y="1.654867797852" />
                  <Point X="-21.929203125" Y="1.624040649414" />
                  <Point X="-21.934357421875" Y="1.616609130859" />
                  <Point X="-21.950263671875" Y="1.589366577148" />
                  <Point X="-21.965236328125" Y="1.555544067383" />
                  <Point X="-21.96985546875" Y="1.542676391602" />
                  <Point X="-21.982578125" Y="1.497186401367" />
                  <Point X="-21.991380859375" Y="1.465712036133" />
                  <Point X="-21.993771484375" Y="1.454669921875" />
                  <Point X="-21.9972265625" Y="1.432370361328" />
                  <Point X="-21.998291015625" Y="1.421113037109" />
                  <Point X="-21.999107421875" Y="1.397540893555" />
                  <Point X="-21.998826171875" Y="1.386243896484" />
                  <Point X="-21.996923828125" Y="1.363758178711" />
                  <Point X="-21.995302734375" Y="1.352569213867" />
                  <Point X="-21.984859375" Y="1.301955810547" />
                  <Point X="-21.9776328125" Y="1.266936645508" />
                  <Point X="-21.97540234375" Y="1.258233276367" />
                  <Point X="-21.965314453125" Y="1.228603881836" />
                  <Point X="-21.9497109375" Y="1.195368530273" />
                  <Point X="-21.943080078125" Y="1.183526977539" />
                  <Point X="-21.91467578125" Y="1.140353393555" />
                  <Point X="-21.8950234375" Y="1.110481689453" />
                  <Point X="-21.888265625" Y="1.101430175781" />
                  <Point X="-21.87371484375" Y="1.084186279297" />
                  <Point X="-21.865921875" Y="1.075994018555" />
                  <Point X="-21.848677734375" Y="1.059903320313" />
                  <Point X="-21.83996875" Y="1.052696655273" />
                  <Point X="-21.8217578125" Y="1.039369995117" />
                  <Point X="-21.812255859375" Y="1.033249633789" />
                  <Point X="-21.77109375" Y="1.010078857422" />
                  <Point X="-21.74261328125" Y="0.994047058105" />
                  <Point X="-21.73451953125" Y="0.989986694336" />
                  <Point X="-21.70531640625" Y="0.978082275391" />
                  <Point X="-21.669720703125" Y="0.968020690918" />
                  <Point X="-21.656328125" Y="0.965257629395" />
                  <Point X="-21.600673828125" Y="0.957902282715" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.555966796875" Y="0.952199768066" />
                  <Point X="-21.53428125" Y="0.951222839355" />
                  <Point X="-21.50878125" Y="0.952032409668" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-20.919642578125" Y="1.029123046875" />
                  <Point X="-20.295296875" Y="1.111319702148" />
                  <Point X="-20.286359375" Y="1.074606689453" />
                  <Point X="-20.2473125" Y="0.914209899902" />
                  <Point X="-20.21612890625" Y="0.713921081543" />
                  <Point X="-20.539603515625" Y="0.62724597168" />
                  <Point X="-21.3080078125" Y="0.421352752686" />
                  <Point X="-21.313966796875" Y="0.419544616699" />
                  <Point X="-21.334376953125" Y="0.412136047363" />
                  <Point X="-21.357619140625" Y="0.401618164062" />
                  <Point X="-21.365994140625" Y="0.39731640625" />
                  <Point X="-21.420671875" Y="0.365711456299" />
                  <Point X="-21.45850390625" Y="0.343843994141" />
                  <Point X="-21.4661171875" Y="0.338944946289" />
                  <Point X="-21.491220703125" Y="0.319873809814" />
                  <Point X="-21.51800390625" Y="0.294353027344" />
                  <Point X="-21.527203125" Y="0.284226287842" />
                  <Point X="-21.560009765625" Y="0.242422698975" />
                  <Point X="-21.582708984375" Y="0.21349861145" />
                  <Point X="-21.589146484375" Y="0.204207305908" />
                  <Point X="-21.60087109375" Y="0.184926010132" />
                  <Point X="-21.606158203125" Y="0.174935882568" />
                  <Point X="-21.615931640625" Y="0.153471908569" />
                  <Point X="-21.619994140625" Y="0.142926528931" />
                  <Point X="-21.62683984375" Y="0.121426742554" />
                  <Point X="-21.629623046875" Y="0.110472488403" />
                  <Point X="-21.64055859375" Y="0.05337084198" />
                  <Point X="-21.648125" Y="0.013862125397" />
                  <Point X="-21.649396484375" Y="0.004966303349" />
                  <Point X="-21.6514140625" Y="-0.026261590958" />
                  <Point X="-21.64971875" Y="-0.062939785004" />
                  <Point X="-21.648125" Y="-0.076422111511" />
                  <Point X="-21.637189453125" Y="-0.133523910522" />
                  <Point X="-21.629623046875" Y="-0.173032623291" />
                  <Point X="-21.62683984375" Y="-0.183986877441" />
                  <Point X="-21.619994140625" Y="-0.20548664856" />
                  <Point X="-21.615931640625" Y="-0.216032043457" />
                  <Point X="-21.606158203125" Y="-0.237496017456" />
                  <Point X="-21.60087109375" Y="-0.24748614502" />
                  <Point X="-21.589146484375" Y="-0.266767425537" />
                  <Point X="-21.582708984375" Y="-0.276058746338" />
                  <Point X="-21.54990234375" Y="-0.317862335205" />
                  <Point X="-21.527203125" Y="-0.3467862854" />
                  <Point X="-21.52127734375" Y="-0.353636749268" />
                  <Point X="-21.498857421875" Y="-0.375806243896" />
                  <Point X="-21.469822265625" Y="-0.398724639893" />
                  <Point X="-21.45850390625" Y="-0.40640411377" />
                  <Point X="-21.403826171875" Y="-0.438009063721" />
                  <Point X="-21.365994140625" Y="-0.459876403809" />
                  <Point X="-21.360505859375" Y="-0.462812896729" />
                  <Point X="-21.34084375" Y="-0.472015930176" />
                  <Point X="-21.31697265625" Y="-0.481027526855" />
                  <Point X="-21.3080078125" Y="-0.483912872314" />
                  <Point X="-20.77634765625" Y="-0.626370422363" />
                  <Point X="-20.215119140625" Y="-0.776751098633" />
                  <Point X="-20.216583984375" Y="-0.786461181641" />
                  <Point X="-20.238380859375" Y="-0.931035583496" />
                  <Point X="-20.272197265625" Y="-1.079219604492" />
                  <Point X="-20.6720859375" Y="-1.026573364258" />
                  <Point X="-21.563216796875" Y="-0.909253540039" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042419434" />
                  <Point X="-21.63327734375" Y="-0.910840820312" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.75283203125" Y="-0.936001647949" />
                  <Point X="-21.82708203125" Y="-0.952140197754" />
                  <Point X="-21.842123046875" Y="-0.95674230957" />
                  <Point X="-21.8712421875" Y="-0.968365905762" />
                  <Point X="-21.8853203125" Y="-0.975387451172" />
                  <Point X="-21.9131484375" Y="-0.992280090332" />
                  <Point X="-21.925875" Y="-1.001530395508" />
                  <Point X="-21.949625" Y="-1.022001403809" />
                  <Point X="-21.9606484375" Y="-1.033222290039" />
                  <Point X="-22.025513671875" Y="-1.111234008789" />
                  <Point X="-22.070392578125" Y="-1.165210327148" />
                  <Point X="-22.078673828125" Y="-1.176849609375" />
                  <Point X="-22.093396484375" Y="-1.201236816406" />
                  <Point X="-22.099837890625" Y="-1.21398449707" />
                  <Point X="-22.1111796875" Y="-1.241369750977" />
                  <Point X="-22.11563671875" Y="-1.254934692383" />
                  <Point X="-22.122466796875" Y="-1.282579956055" />
                  <Point X="-22.12483984375" Y="-1.296660522461" />
                  <Point X="-22.13413671875" Y="-1.397688964844" />
                  <Point X="-22.140568359375" Y="-1.467590698242" />
                  <Point X="-22.140708984375" Y="-1.483315063477" />
                  <Point X="-22.138392578125" Y="-1.514580932617" />
                  <Point X="-22.135935546875" Y="-1.530122680664" />
                  <Point X="-22.128205078125" Y="-1.561743286133" />
                  <Point X="-22.12321484375" Y="-1.576666992188" />
                  <Point X="-22.110841796875" Y="-1.605480957031" />
                  <Point X="-22.103458984375" Y="-1.619371459961" />
                  <Point X="-22.0440703125" Y="-1.711746948242" />
                  <Point X="-22.002978515625" Y="-1.775661987305" />
                  <Point X="-21.998255859375" Y="-1.782354614258" />
                  <Point X="-21.98019921875" Y="-1.804457397461" />
                  <Point X="-21.956505859375" Y="-1.828123291016" />
                  <Point X="-21.947201171875" Y="-1.836277709961" />
                  <Point X="-21.453818359375" Y="-2.214864013672" />
                  <Point X="-20.912828125" Y="-2.629981201172" />
                  <Point X="-20.954525390625" Y="-2.697451171875" />
                  <Point X="-20.998724609375" Y="-2.760252441406" />
                  <Point X="-21.35787109375" Y="-2.5528984375" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.18497265625" Y="-2.079513427734" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228892578125" Y="-2.066337646484" />
                  <Point X="-22.35661328125" Y="-2.043271362305" />
                  <Point X="-22.444982421875" Y="-2.027311889648" />
                  <Point X="-22.460640625" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.55515625" Y="-2.03513659668" />
                  <Point X="-22.584931640625" Y="-2.044960571289" />
                  <Point X="-22.59941015625" Y="-2.051109130859" />
                  <Point X="-22.705513671875" Y="-2.106950927734" />
                  <Point X="-22.778927734375" Y="-2.145588134766" />
                  <Point X="-22.791029296875" Y="-2.153168945313" />
                  <Point X="-22.8139609375" Y="-2.170061767578" />
                  <Point X="-22.824791015625" Y="-2.179373779297" />
                  <Point X="-22.84575" Y="-2.200332763672" />
                  <Point X="-22.855060546875" Y="-2.211161865234" />
                  <Point X="-22.871953125" Y="-2.234092529297" />
                  <Point X="-22.87953515625" Y="-2.246194091797" />
                  <Point X="-22.935376953125" Y="-2.352298339844" />
                  <Point X="-22.974015625" Y="-2.425712158203" />
                  <Point X="-22.9801640625" Y="-2.440193115234" />
                  <Point X="-22.98998828125" Y="-2.469971679688" />
                  <Point X="-22.9936640625" Y="-2.485269287109" />
                  <Point X="-22.99862109375" Y="-2.517443603516" />
                  <Point X="-22.999720703125" Y="-2.533134521484" />
                  <Point X="-22.99931640625" Y="-2.564484375" />
                  <Point X="-22.9978125" Y="-2.580143310547" />
                  <Point X="-22.97474609375" Y="-2.707863769531" />
                  <Point X="-22.95878515625" Y="-2.796233642578" />
                  <Point X="-22.95698046875" Y="-2.804228271484" />
                  <Point X="-22.94876171875" Y="-2.8315390625" />
                  <Point X="-22.9359296875" Y="-2.862475830078" />
                  <Point X="-22.930453125" Y="-2.873578125" />
                  <Point X="-22.61340625" Y="-3.422721435547" />
                  <Point X="-22.26410546875" Y="-4.027725341797" />
                  <Point X="-22.276244140625" Y="-4.036082763672" />
                  <Point X="-22.5604375" Y="-3.665715087891" />
                  <Point X="-23.16608203125" Y="-2.876422363281" />
                  <Point X="-23.17134765625" Y="-2.870141113281" />
                  <Point X="-23.19116796875" Y="-2.849625732422" />
                  <Point X="-23.21674609375" Y="-2.828004638672" />
                  <Point X="-23.22669921875" Y="-2.820646972656" />
                  <Point X="-23.352666015625" Y="-2.739661865234" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.453716796875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663873535156" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.72937109375" Y="-2.659193847656" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670339111328" />
                  <Point X="-23.866421875" Y="-2.677170410156" />
                  <Point X="-23.87998828125" Y="-2.681627929688" />
                  <Point X="-23.907373046875" Y="-2.692970703125" />
                  <Point X="-23.92012109375" Y="-2.699412841797" />
                  <Point X="-23.94450390625" Y="-2.714134033203" />
                  <Point X="-23.956138671875" Y="-2.722413085938" />
                  <Point X="-24.062517578125" Y="-2.810864257813" />
                  <Point X="-24.136123046875" Y="-2.872063476562" />
                  <Point X="-24.147345703125" Y="-2.883087402344" />
                  <Point X="-24.16781640625" Y="-2.906836914062" />
                  <Point X="-24.177064453125" Y="-2.9195625" />
                  <Point X="-24.19395703125" Y="-2.947389160156" />
                  <Point X="-24.20098046875" Y="-2.961466552734" />
                  <Point X="-24.21260546875" Y="-2.990587646484" />
                  <Point X="-24.21720703125" Y="-3.005631347656" />
                  <Point X="-24.249013671875" Y="-3.151968505859" />
                  <Point X="-24.271021484375" Y="-3.253219238281" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323170166016" />
                  <Point X="-24.2744453125" Y="-3.335520507812" />
                  <Point X="-24.184595703125" Y="-4.017991210938" />
                  <Point X="-24.166912109375" Y="-4.152317871094" />
                  <Point X="-24.344931640625" Y="-3.487937011719" />
                  <Point X="-24.347392578125" Y="-3.480121337891" />
                  <Point X="-24.357853515625" Y="-3.453579589844" />
                  <Point X="-24.3732109375" Y="-3.423815917969" />
                  <Point X="-24.37958984375" Y="-3.413209960938" />
                  <Point X="-24.476361328125" Y="-3.273780761719" />
                  <Point X="-24.543318359375" Y="-3.177309570312" />
                  <Point X="-24.553328125" Y="-3.165172851562" />
                  <Point X="-24.575212890625" Y="-3.142716552734" />
                  <Point X="-24.587087890625" Y="-3.132396972656" />
                  <Point X="-24.61334375" Y="-3.113153320312" />
                  <Point X="-24.6267578125" Y="-3.104936523438" />
                  <Point X="-24.6547578125" Y="-3.090829101562" />
                  <Point X="-24.66934375" Y="-3.084938476562" />
                  <Point X="-24.819091796875" Y="-3.038462158203" />
                  <Point X="-24.922703125" Y="-3.006305175781" />
                  <Point X="-24.936623046875" Y="-3.003109130859" />
                  <Point X="-24.964783203125" Y="-2.99883984375" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.214732421875" Y="-3.05278125" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.3329296875" Y="-3.090829101562" />
                  <Point X="-25.3609296875" Y="-3.104936523438" />
                  <Point X="-25.37434375" Y="-3.113153320312" />
                  <Point X="-25.400599609375" Y="-3.132396972656" />
                  <Point X="-25.4124765625" Y="-3.142717529297" />
                  <Point X="-25.434361328125" Y="-3.165174560547" />
                  <Point X="-25.444369140625" Y="-3.177311035156" />
                  <Point X="-25.541138671875" Y="-3.316740234375" />
                  <Point X="-25.608095703125" Y="-3.413211425781" />
                  <Point X="-25.612470703125" Y="-3.4201328125" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213134766" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.806572265625" Y="-4.099310546875" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.939134116515" Y="2.425140245888" />
                  <Point X="-21.123319776496" Y="2.744159167002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.015152998654" Y="2.366808812088" />
                  <Point X="-21.205592184007" Y="2.696659156871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.232956895617" Y="0.822005420146" />
                  <Point X="-20.39259684348" Y="1.098509920761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.091171880794" Y="2.308477378289" />
                  <Point X="-21.287864591518" Y="2.649159146741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.271660173235" Y="0.699041463399" />
                  <Point X="-20.494544402445" Y="1.085088272597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.167190765106" Y="2.250145948253" />
                  <Point X="-21.370136999029" Y="2.601659136611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.366660121912" Y="0.673586201224" />
                  <Point X="-20.59649196141" Y="1.071666624432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.243209662771" Y="2.191814541342" />
                  <Point X="-21.452409406539" Y="2.554159126481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.206376887254" Y="3.860069110333" />
                  <Point X="-22.264195759885" Y="3.960214335366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.461660070589" Y="0.648130939049" />
                  <Point X="-20.698439520376" Y="1.058244976268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.319228560435" Y="2.133483134432" />
                  <Point X="-21.53468181405" Y="2.50665911635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.26122521987" Y="3.765069209133" />
                  <Point X="-22.441387884232" Y="4.077120097437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.5566600295" Y="0.622675694599" />
                  <Point X="-20.800387079341" Y="1.044823328104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.395247458099" Y="2.075151727522" />
                  <Point X="-21.616954221561" Y="2.45915910622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.316073552485" Y="3.670069307934" />
                  <Point X="-22.606684543998" Y="4.173422310473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.651660035175" Y="0.597220531149" />
                  <Point X="-20.902334638306" Y="1.031401679939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.471266355763" Y="2.016820320612" />
                  <Point X="-21.699226629072" Y="2.41165909609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.370921885101" Y="3.575069406734" />
                  <Point X="-22.769941580907" Y="4.266191793092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.746660040851" Y="0.571765367698" />
                  <Point X="-21.004282195093" Y="1.017980028001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.547285253428" Y="1.958488913702" />
                  <Point X="-21.781499043671" Y="2.364159098237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.425770217154" Y="3.480069504561" />
                  <Point X="-22.920212685264" Y="4.336468980747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.841660046526" Y="0.546310204248" />
                  <Point X="-21.106229751433" Y="1.00455837529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.623304151092" Y="1.900157506792" />
                  <Point X="-21.863771472127" Y="2.316659124385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.480618461197" Y="3.38506944995" />
                  <Point X="-23.070483395152" Y="4.406745485164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.936660052202" Y="0.520855040797" />
                  <Point X="-21.208177307774" Y="0.991136722579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.699323048756" Y="1.841826099882" />
                  <Point X="-21.946043900583" Y="2.269159150533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.53546670524" Y="3.290069395339" />
                  <Point X="-23.214697925965" Y="4.466532379721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.232112300622" Y="-0.889457461299" />
                  <Point X="-20.312202135974" Y="-0.750737797298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.031660057878" Y="0.495399877347" />
                  <Point X="-21.310124864114" Y="0.977715069869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.77534194642" Y="1.783494692971" />
                  <Point X="-22.028316329039" Y="2.221659176681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.590314949284" Y="3.195069340728" />
                  <Point X="-23.353450410142" Y="4.516858731993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.260880519263" Y="-1.029629444968" />
                  <Point X="-20.441974541238" Y="-0.71596539796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.126660063553" Y="0.469944713896" />
                  <Point X="-21.412072420455" Y="0.964293417158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.850053182752" Y="1.722898350195" />
                  <Point X="-22.110588757495" Y="2.174159202828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.645163193327" Y="3.100069286117" />
                  <Point X="-23.492202628738" Y="4.567184624264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.347683874652" Y="-1.069281623167" />
                  <Point X="-20.571746946502" Y="-0.681192998623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.221660069229" Y="0.444489550446" />
                  <Point X="-21.514583718509" Y="0.951848193737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.914071697895" Y="1.643781671047" />
                  <Point X="-22.192861185951" Y="2.126659228976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.700011437371" Y="3.005069231506" />
                  <Point X="-23.62407628844" Y="4.605596503047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.466404300987" Y="-1.053651812859" />
                  <Point X="-20.701519351765" Y="-0.646420599286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.316436981599" Y="0.418647978056" />
                  <Point X="-21.63001438331" Y="0.961779969925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.968145424332" Y="1.54744011259" />
                  <Point X="-22.275133614407" Y="2.079159255124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.754859681414" Y="2.910069176895" />
                  <Point X="-23.751540404212" Y="4.636370827707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.585124727321" Y="-1.038022002552" />
                  <Point X="-20.831291735084" Y="-0.61164823796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.401851408735" Y="0.376590105554" />
                  <Point X="-21.765912408493" Y="1.007162254189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.998665437736" Y="1.410302326454" />
                  <Point X="-22.357406042863" Y="2.031659281272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.809707925457" Y="2.815069122284" />
                  <Point X="-23.879004213298" Y="4.667144621169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.703845163645" Y="-1.022392174941" />
                  <Point X="-20.961064088514" Y="-0.5768759284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.482588838458" Y="0.326431435908" />
                  <Point X="-22.44716507644" Y="1.997126487866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.864556169501" Y="2.720069067673" />
                  <Point X="-24.006467828214" Y="4.69791807832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.822565627323" Y="-1.006762299954" />
                  <Point X="-21.090836441945" Y="-0.54210361884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.550660802917" Y="0.254335536922" />
                  <Point X="-22.544886477277" Y="1.976384919103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.919404413544" Y="2.625069013063" />
                  <Point X="-24.13393144313" Y="4.728691535471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.941286091001" Y="-0.991132424966" />
                  <Point X="-21.220608795376" Y="-0.50733130928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.609843354347" Y="0.166842722919" />
                  <Point X="-22.658938251768" Y="1.983928387214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.965545874309" Y="2.514988367442" />
                  <Point X="-24.253895832118" Y="4.746475952296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.060006554678" Y="-0.975502549978" />
                  <Point X="-21.354342518743" Y="-0.465697705724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.643913346228" Y="0.035853679871" />
                  <Point X="-22.80682335507" Y="2.050072899815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.985346444334" Y="2.359283960745" />
                  <Point X="-24.370651965109" Y="4.758703506734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.178727018356" Y="-0.95987267499" />
                  <Point X="-21.547971073063" Y="-0.320323211846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.626961626285" Y="-0.183507560347" />
                  <Point X="-24.487408114683" Y="4.770931089891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.297447482034" Y="-0.944242800002" />
                  <Point X="-24.604164281397" Y="4.783158702736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.416167945711" Y="-0.928612925014" />
                  <Point X="-24.651325000599" Y="4.674843464517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.534888409389" Y="-0.912983050027" />
                  <Point X="-24.686097640372" Y="4.545071443316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.644822306635" Y="-0.912571954522" />
                  <Point X="-24.720870091443" Y="4.415299095275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.742311895498" Y="-0.933715033403" />
                  <Point X="-24.755642542514" Y="4.285526747234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.83922550082" Y="-0.955855745041" />
                  <Point X="-24.804454844475" Y="4.180072134264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.948779399095" Y="-2.688153634629" />
                  <Point X="-21.037674266915" Y="-2.534183207032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.923534122773" Y="-0.999828928302" />
                  <Point X="-24.877281435832" Y="4.116211490637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.025912267296" Y="-2.744555587972" />
                  <Point X="-21.234622118993" Y="-2.383059520792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.99210718454" Y="-1.07105690129" />
                  <Point X="-24.969836616806" Y="4.086521766587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.359619894463" Y="4.761646207431" />
                  <Point X="-25.371282781352" Y="4.781846920084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.190457365597" Y="-2.649555117578" />
                  <Point X="-21.431569971071" Y="-2.231935834552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.056848815985" Y="-1.148921106262" />
                  <Point X="-25.091250072356" Y="4.106816040323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.264621065833" Y="4.407103409584" />
                  <Point X="-25.47403639331" Y="4.769821396657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.355002463898" Y="-2.554554647184" />
                  <Point X="-21.628517692676" Y="-2.080812374296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.111901600201" Y="-1.243566886903" />
                  <Point X="-25.576789877125" Y="4.757795651279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.519547292685" Y="-2.459554643602" />
                  <Point X="-21.825465397666" Y="-1.92968894282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.133927619769" Y="-1.395416701922" />
                  <Point X="-25.67954334936" Y="4.745769885844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.684092116691" Y="-2.364554648302" />
                  <Point X="-25.782296821595" Y="4.733744120409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.848636940696" Y="-2.269554653002" />
                  <Point X="-25.88505029383" Y="4.721718354974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.013181764702" Y="-2.174554657702" />
                  <Point X="-25.983207950552" Y="4.701732403569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.175331103536" Y="-2.083703764427" />
                  <Point X="-26.077621866092" Y="4.675262102227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.302755622191" Y="-2.052998023987" />
                  <Point X="-26.172035781632" Y="4.648791800884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.425221621163" Y="-2.030880691567" />
                  <Point X="-26.266449637287" Y="4.622321395815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.535014090218" Y="-2.030714556875" />
                  <Point X="-26.360863411297" Y="4.595850849335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.625122851015" Y="-2.064641604969" />
                  <Point X="-26.455277185307" Y="4.569380302855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.709255217391" Y="-2.108920071844" />
                  <Point X="-26.46337833772" Y="4.393411910434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.792695861512" Y="-2.15439663681" />
                  <Point X="-26.497139821357" Y="4.261888515433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.863222398824" Y="-2.222241090904" />
                  <Point X="-26.547508214879" Y="4.159129132109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.917472177974" Y="-2.318277717117" />
                  <Point X="-26.621967896762" Y="4.098097084245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.96978371098" Y="-2.417671484129" />
                  <Point X="-26.70954448425" Y="4.059784183329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.999421472335" Y="-2.556337375639" />
                  <Point X="-26.802794732035" Y="4.03129835031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.945014828524" Y="-2.840572446989" />
                  <Point X="-26.921050626289" Y="4.046123567452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.632646367513" Y="-3.571610492143" />
                  <Point X="-27.082505973012" Y="4.13577243113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.075719497399" Y="-2.994185319712" />
                  <Point X="-27.250211081525" Y="4.236246199764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.320381913294" Y="-2.760417584679" />
                  <Point X="-27.333266557824" Y="4.19010250456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.486604959091" Y="-2.662510823969" />
                  <Point X="-27.416322121212" Y="4.1439589602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.604833146283" Y="-2.647733596866" />
                  <Point X="-27.4993776846" Y="4.097815415839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.708995646081" Y="-2.657318854973" />
                  <Point X="-27.582433247988" Y="4.051671871479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.813158192413" Y="-2.666904032479" />
                  <Point X="-27.664042026064" Y="4.003022421449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.907707549694" Y="-2.693139741846" />
                  <Point X="-27.74008015558" Y="3.944724325084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.986113621212" Y="-2.747336442355" />
                  <Point X="-27.816118285096" Y="3.886426228718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.060230354942" Y="-2.808962493844" />
                  <Point X="-27.892156445657" Y="3.828128186123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.134347747168" Y="-2.870587404784" />
                  <Point X="-27.968194725944" Y="3.769830350901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.196620543422" Y="-2.952727757742" />
                  <Point X="-28.044233006232" Y="3.71153251568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.233224932681" Y="-3.079327095767" />
                  <Point X="-27.749134132534" Y="3.010406273179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.263227413816" Y="-3.217361274087" />
                  <Point X="-27.78952694286" Y="2.890368672923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.26550705723" Y="-3.40341281587" />
                  <Point X="-27.856165688523" Y="2.815790366164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.233104028898" Y="-3.649536507261" />
                  <Point X="-27.930666250794" Y="2.75482912521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.200701000566" Y="-3.895660198652" />
                  <Point X="-24.265978093869" Y="-3.78259695648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.668597375399" Y="-3.085239904762" />
                  <Point X="-28.023268118509" Y="2.725220264969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.168299132454" Y="-4.141781880481" />
                  <Point X="-24.17097887406" Y="-4.137140431868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.802290467575" Y="-3.043676676494" />
                  <Point X="-28.135399071695" Y="2.729436772988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.935223127544" Y="-3.003430555441" />
                  <Point X="-28.269110966018" Y="2.771032567531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.04558488306" Y="-3.002278387676" />
                  <Point X="-28.433655990099" Y="2.866032909373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.139588451193" Y="-3.029459431576" />
                  <Point X="-28.598201014181" Y="2.961033251215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.232615667865" Y="-3.058331565814" />
                  <Point X="-28.74027081545" Y="3.017105365235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.325323311156" Y="-3.087757217383" />
                  <Point X="-28.803238985145" Y="2.936169434405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.406358098045" Y="-3.137400849312" />
                  <Point X="-28.866206779501" Y="2.855232853471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.47092310056" Y="-3.215570984564" />
                  <Point X="-28.929174573857" Y="2.774296272536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.530805442194" Y="-3.301851726379" />
                  <Point X="-28.992142368213" Y="2.693359691602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.590688490684" Y="-3.388131243881" />
                  <Point X="-28.421504807266" Y="1.514986443334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.588127378986" Y="1.803585203241" />
                  <Point X="-29.048549845341" Y="2.601060307914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.642756610343" Y="-3.487946615177" />
                  <Point X="-28.457120739704" Y="1.386675047876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.785074969507" Y="1.954708436451" />
                  <Point X="-29.103697693522" Y="2.506579182892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.677529195288" Y="-3.617718731343" />
                  <Point X="-28.514133562923" Y="1.295424154374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.982022811803" Y="2.105832105749" />
                  <Point X="-29.158845394063" Y="2.412097802148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.712301780232" Y="-3.747490847508" />
                  <Point X="-28.593114839332" Y="1.24222373796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.178970955342" Y="2.256956296815" />
                  <Point X="-29.213992923798" Y="2.317616125562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.747074365177" Y="-3.877262963674" />
                  <Point X="-28.685329429556" Y="1.211944093428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.781846950121" Y="-4.00703507984" />
                  <Point X="-28.792460771359" Y="1.207501020513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.816619506986" Y="-4.136807244641" />
                  <Point X="-28.911181197948" Y="1.223130831261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.851391994749" Y="-4.26657952913" />
                  <Point X="-28.293972647312" Y="-0.035905737306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.440877385198" Y="0.218540732586" />
                  <Point X="-29.029901624536" Y="1.238760642009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.886164482512" Y="-4.396351813619" />
                  <Point X="-28.32835586283" Y="-0.166352261101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.579534572738" Y="0.268702026239" />
                  <Point X="-29.148622051125" Y="1.254390452757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.920936970275" Y="-4.526124098108" />
                  <Point X="-28.392015494794" Y="-0.246090544148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.709307021379" Y="0.303474500708" />
                  <Point X="-29.267342477714" Y="1.270020263505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.955709458038" Y="-4.655896382597" />
                  <Point X="-26.163431209465" Y="-4.296111755288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.339745595448" Y="-3.99072628066" />
                  <Point X="-28.471854635694" Y="-0.297805095676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.839079470021" Y="0.338246975178" />
                  <Point X="-29.386062904302" Y="1.285650074253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.003384618565" Y="-4.763320582305" />
                  <Point X="-26.119922666702" Y="-4.561470761917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.546433535557" Y="-3.82273226708" />
                  <Point X="-27.950773058807" Y="-1.390344861734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.103820893549" Y="-1.125258235971" />
                  <Point X="-28.564552494087" Y="-0.327247695188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.968851918662" Y="0.373019449648" />
                  <Point X="-29.504783406602" Y="1.301280016135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.129861142339" Y="-4.734256817163" />
                  <Point X="-26.139444417006" Y="-4.717658098537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.672828857298" Y="-3.793809147985" />
                  <Point X="-27.321066557251" Y="-2.671028516285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.502018276296" Y="-2.357610945182" />
                  <Point X="-27.992334911757" Y="-1.508357620768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.228418657289" Y="-1.099448578664" />
                  <Point X="-28.659552482642" Y="-0.352702888292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.098624367304" Y="0.407791924118" />
                  <Point X="-29.623503924838" Y="1.316909985621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.786839795829" Y="-3.786336409831" />
                  <Point X="-27.370798733989" Y="-2.774889859403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.613723536222" Y="-2.354131759517" />
                  <Point X="-28.059264976752" Y="-1.582431347641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.331088661686" Y="-1.111618914636" />
                  <Point X="-28.754552471197" Y="-0.378158081395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.228396773923" Y="0.442564325803" />
                  <Point X="-29.66564940482" Y="1.199908098259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.900850631817" Y="-3.778863849286" />
                  <Point X="-27.425647064448" Y="-2.869889764338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.703499776754" Y="-2.388634749604" />
                  <Point X="-28.135283874064" Y="-1.640762755162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.433036207899" Y="-1.125040584888" />
                  <Point X="-28.849552459753" Y="-0.403613274499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.358169174444" Y="0.477336716925" />
                  <Point X="-29.700689608389" Y="1.070599511149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.008665608988" Y="-3.782122831008" />
                  <Point X="-27.480495394907" Y="-2.964889669273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.785772212364" Y="-2.436134711064" />
                  <Point X="-28.211302771376" Y="-1.699094162683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.534983754112" Y="-1.138462255139" />
                  <Point X="-28.944552448308" Y="-0.429068467602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.487941574964" Y="0.512109108047" />
                  <Point X="-29.734140772151" Y="0.938538626356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.097944444876" Y="-3.81748735121" />
                  <Point X="-27.535343725366" Y="-3.059889574207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.868044647975" Y="-2.483634672524" />
                  <Point X="-28.287321668687" Y="-1.757425570204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.636931300326" Y="-1.151883925391" />
                  <Point X="-29.039552436864" Y="-0.454523660706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.617713975485" Y="0.546881499169" />
                  <Point X="-29.756519980996" Y="0.78730055311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.177103434067" Y="-3.870379960056" />
                  <Point X="-27.590192055825" Y="-3.154889479142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.950317083585" Y="-2.531134633984" />
                  <Point X="-28.363340565999" Y="-1.815756977725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.738878846539" Y="-1.165305595643" />
                  <Point X="-29.134552425419" Y="-0.479978853809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.747486376006" Y="0.581653890291" />
                  <Point X="-29.778899268787" Y="0.6360626166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.256262407947" Y="-3.92327259542" />
                  <Point X="-27.645040386284" Y="-3.249889384076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.032589519196" Y="-2.578634595444" />
                  <Point X="-28.43935946331" Y="-1.874088385246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.840826392752" Y="-1.178727265894" />
                  <Point X="-29.229552413975" Y="-0.505434046913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.335421252067" Y="-3.976165455535" />
                  <Point X="-27.699888716743" Y="-3.344889289011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.114861954806" Y="-2.626134556905" />
                  <Point X="-28.515378360622" Y="-1.932419792767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.942773943755" Y="-1.19214892785" />
                  <Point X="-29.32455240253" Y="-0.530889240016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.408577512969" Y="-4.039455094762" />
                  <Point X="-27.754737047202" Y="-3.439889193946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.197134390417" Y="-2.673634518365" />
                  <Point X="-28.591397257933" Y="-1.990751200288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.044721504864" Y="-1.205570572301" />
                  <Point X="-29.41955239495" Y="-0.556344426426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.471271305026" Y="-4.1208662616" />
                  <Point X="-27.809585329394" Y="-3.534889182481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.279406804689" Y="-2.721134516784" />
                  <Point X="-28.667416155245" Y="-2.049082607809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.146669065973" Y="-1.218992216753" />
                  <Point X="-29.514552394215" Y="-0.58179960098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.586843110344" Y="-4.110690022868" />
                  <Point X="-27.864433565638" Y="-3.629889250602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.361679213466" Y="-2.76863452472" />
                  <Point X="-28.743435052557" Y="-2.10741401533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.248616627082" Y="-1.232413861205" />
                  <Point X="-29.60955239348" Y="-0.607254775535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.759136671508" Y="-4.002268821114" />
                  <Point X="-27.919281801881" Y="-3.724889318722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.443951622244" Y="-2.816134532656" />
                  <Point X="-28.819453949103" Y="-2.165745424177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.350564188191" Y="-1.245835505656" />
                  <Point X="-29.704552392745" Y="-0.632709950089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.956624742166" Y="-3.850209448846" />
                  <Point X="-27.974130038125" Y="-3.819889386843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.526224031022" Y="-2.863634540592" />
                  <Point X="-28.895472806975" Y="-2.224076900009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.4525117493" Y="-1.259257150108" />
                  <Point X="-29.781469454463" Y="-0.689485691224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.608496439799" Y="-2.911134548528" />
                  <Point X="-28.971491664847" Y="-2.282408375841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.554459310408" Y="-1.27267879456" />
                  <Point X="-29.745349471564" Y="-0.942047336773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.690768848577" Y="-2.958634556464" />
                  <Point X="-29.047510522719" Y="-2.340739851674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.656406871517" Y="-1.286100439012" />
                  <Point X="-29.664769629503" Y="-1.271615717288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.773041257355" Y="-3.0061345644" />
                  <Point X="-29.123529380591" Y="-2.399071327506" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.2916953125" Y="-4.420727539062" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521543945312" />
                  <Point X="-24.632451171875" Y="-3.382114746094" />
                  <Point X="-24.699408203125" Y="-3.285643554688" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.875412109375" Y="-3.219923583984" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.158412109375" Y="-3.234242675781" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643554688" />
                  <Point X="-25.385048828125" Y="-3.425072753906" />
                  <Point X="-25.452005859375" Y="-3.521543945312" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.623044921875" Y="-4.148486328125" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.979142578125" Y="-4.961571777344" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.27355859375" Y="-4.893473632812" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.337650390625" Y="-4.7675234375" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.34545703125" Y="-4.354905273438" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.52415234375" Y="-4.081720703125" />
                  <Point X="-26.61954296875" Y="-3.998064208984" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.83222265625" Y="-3.97376953125" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791259766" />
                  <Point X="-27.142349609375" Y="-4.075669433594" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.351703125" Y="-4.277152832031" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.678322265625" Y="-4.277521484375" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.0958203125" Y="-3.982828369141" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.946033203125" Y="-3.391220458984" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597595214844" />
                  <Point X="-27.5139765625" Y="-2.568766601562" />
                  <Point X="-27.53132421875" Y="-2.55141796875" />
                  <Point X="-27.560154296875" Y="-2.537198974609" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.11898046875" Y="-2.847905517578" />
                  <Point X="-28.842958984375" Y="-3.26589453125" />
                  <Point X="-29.0214609375" Y="-3.031380859375" />
                  <Point X="-29.161697265625" Y="-2.847137207031" />
                  <Point X="-29.333755859375" Y="-2.558623535156" />
                  <Point X="-29.43101953125" Y="-2.395526855469" />
                  <Point X="-28.933642578125" Y="-2.013875976562" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.396012084961" />
                  <Point X="-28.1381171875" Y="-1.366264892578" />
                  <Point X="-28.140326171875" Y="-1.334594970703" />
                  <Point X="-28.161158203125" Y="-1.310639282227" />
                  <Point X="-28.187640625" Y="-1.295052734375" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.88519140625" Y="-1.376207519531" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.872541015625" Y="-1.225928710938" />
                  <Point X="-29.927392578125" Y="-1.011188232422" />
                  <Point X="-29.972912109375" Y="-0.692914428711" />
                  <Point X="-29.99839453125" Y="-0.5147421875" />
                  <Point X="-29.434443359375" Y="-0.363632019043" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.525490234375" Y="-0.110040214539" />
                  <Point X="-28.502322265625" Y="-0.09064540863" />
                  <Point X="-28.4894296875" Y="-0.058290412903" />
                  <Point X="-28.483400390625" Y="-0.031280040741" />
                  <Point X="-28.49111328125" Y="0.001158297658" />
                  <Point X="-28.502322265625" Y="0.028085329056" />
                  <Point X="-28.530544921875" Y="0.050987781525" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.16424609375" Y="0.228672653198" />
                  <Point X="-29.998185546875" Y="0.452126037598" />
                  <Point X="-29.952994140625" Y="0.757527526855" />
                  <Point X="-29.91764453125" Y="0.996414855957" />
                  <Point X="-29.82601171875" Y="1.334568725586" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.38190625" Y="1.476742431641" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.6953984375" Y="1.407313720703" />
                  <Point X="-28.67027734375" Y="1.415234008789" />
                  <Point X="-28.651533203125" Y="1.426056884766" />
                  <Point X="-28.6391171875" Y="1.443788208008" />
                  <Point X="-28.624548828125" Y="1.478958251953" />
                  <Point X="-28.614470703125" Y="1.503292602539" />
                  <Point X="-28.61071484375" Y="1.524606567383" />
                  <Point X="-28.61631640625" Y="1.54551171875" />
                  <Point X="-28.63389453125" Y="1.579278320312" />
                  <Point X="-28.646056640625" Y="1.602641479492" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-29.008021484375" Y="1.886292236328" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.29737109375" Y="2.551681152344" />
                  <Point X="-29.16001171875" Y="2.787007080078" />
                  <Point X="-28.91728515625" Y="3.099001220703" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.54525" Y="3.149855224609" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.08794921875" Y="2.916010986328" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.0315078125" Y="2.915775146484" />
                  <Point X="-28.01325390625" Y="2.927403808594" />
                  <Point X="-27.97736328125" Y="2.963294433594" />
                  <Point X="-27.952529296875" Y="2.988127197266" />
                  <Point X="-27.9408984375" Y="3.006381591797" />
                  <Point X="-27.938072265625" Y="3.027840820312" />
                  <Point X="-27.94249609375" Y="3.078405029297" />
                  <Point X="-27.945556640625" Y="3.113390380859" />
                  <Point X="-27.95206640625" Y="3.134032714844" />
                  <Point X="-28.10630078125" Y="3.401171630859" />
                  <Point X="-28.30727734375" Y="3.749276611328" />
                  <Point X="-27.99210546875" Y="3.990915527344" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.37058203125" Y="4.386724609375" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.086021484375" Y="4.441608398438" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.89496875" Y="4.244364257813" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.218491699219" />
                  <Point X="-26.81380859375" Y="4.22225" />
                  <Point X="-26.755193359375" Y="4.246530273438" />
                  <Point X="-26.714634765625" Y="4.263329589844" />
                  <Point X="-26.696904296875" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.294487792969" />
                  <Point X="-26.66700390625" Y="4.354997558594" />
                  <Point X="-26.653802734375" Y="4.396864257812" />
                  <Point X="-26.651916015625" Y="4.418426757812" />
                  <Point X="-26.669451171875" Y="4.551615234375" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.27779296875" Y="4.816467285156" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.504638671875" Y="4.957536621094" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.156357421875" Y="4.737168945312" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.86651953125" Y="4.605836914062" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.41020703125" Y="4.953885253906" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.756365234375" Y="4.832994628906" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.2416015625" Y="4.678402832031" />
                  <Point X="-23.068970703125" Y="4.615788085938" />
                  <Point X="-22.827638671875" Y="4.50292578125" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.42816796875" Y="4.289311523438" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-22.0476015625" Y="4.039329589844" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.26087890625" Y="3.385672851562" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491516357422" />
                  <Point X="-22.7878359375" Y="2.444062988281" />
                  <Point X="-22.7966171875" Y="2.411229980469" />
                  <Point X="-22.797955078125" Y="2.392328613281" />
                  <Point X="-22.793005859375" Y="2.351295166016" />
                  <Point X="-22.789583984375" Y="2.322903808594" />
                  <Point X="-22.78131640625" Y="2.300811523438" />
                  <Point X="-22.75592578125" Y="2.263392822266" />
                  <Point X="-22.738357421875" Y="2.237502929688" />
                  <Point X="-22.72505859375" Y="2.224203369141" />
                  <Point X="-22.687640625" Y="2.198813232422" />
                  <Point X="-22.66175" Y="2.181245849609" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.59862890625" Y="2.168032226562" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5513359375" Y="2.165946289062" />
                  <Point X="-22.5038828125" Y="2.178635986328" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.84865625" Y="2.544779052734" />
                  <Point X="-21.005751953125" Y="3.031430664062" />
                  <Point X="-20.892724609375" Y="2.874350341797" />
                  <Point X="-20.797404296875" Y="2.741877197266" />
                  <Point X="-20.67483203125" Y="2.539321777344" />
                  <Point X="-20.612486328125" Y="2.436295654297" />
                  <Point X="-21.04088671875" Y="2.107572998047" />
                  <Point X="-21.7113828125" Y="1.593082641602" />
                  <Point X="-21.72062890625" Y="1.583832275391" />
                  <Point X="-21.75478125" Y="1.539278198242" />
                  <Point X="-21.77841015625" Y="1.508451171875" />
                  <Point X="-21.78687890625" Y="1.491500854492" />
                  <Point X="-21.7996015625" Y="1.446010864258" />
                  <Point X="-21.808404296875" Y="1.414536376953" />
                  <Point X="-21.809220703125" Y="1.390964233398" />
                  <Point X="-21.79877734375" Y="1.340350830078" />
                  <Point X="-21.79155078125" Y="1.305331542969" />
                  <Point X="-21.784353515625" Y="1.287955688477" />
                  <Point X="-21.75594921875" Y="1.244782104492" />
                  <Point X="-21.736296875" Y="1.21491027832" />
                  <Point X="-21.719052734375" Y="1.198819702148" />
                  <Point X="-21.677890625" Y="1.175649047852" />
                  <Point X="-21.64941015625" Y="1.15961730957" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.575779296875" Y="1.146264282227" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.944443359375" Y="1.217497558594" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.10175" Y="1.119547485352" />
                  <Point X="-20.06080859375" Y="0.951367675781" />
                  <Point X="-20.02218359375" Y="0.703288635254" />
                  <Point X="-20.002140625" Y="0.574556335449" />
                  <Point X="-20.490427734375" Y="0.443720031738" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819351196" />
                  <Point X="-21.32558984375" Y="0.20121446228" />
                  <Point X="-21.363421875" Y="0.179346984863" />
                  <Point X="-21.377734375" Y="0.166926696777" />
                  <Point X="-21.410541015625" Y="0.125123008728" />
                  <Point X="-21.433240234375" Y="0.096199020386" />
                  <Point X="-21.443013671875" Y="0.074735107422" />
                  <Point X="-21.45394921875" Y="0.017633401871" />
                  <Point X="-21.461515625" Y="-0.021875299454" />
                  <Point X="-21.461515625" Y="-0.040684780121" />
                  <Point X="-21.450580078125" Y="-0.097786483765" />
                  <Point X="-21.443013671875" Y="-0.137295181274" />
                  <Point X="-21.433240234375" Y="-0.158759094238" />
                  <Point X="-21.40043359375" Y="-0.200562774658" />
                  <Point X="-21.377734375" Y="-0.22948677063" />
                  <Point X="-21.363421875" Y="-0.241907058716" />
                  <Point X="-21.308744140625" Y="-0.273511962891" />
                  <Point X="-21.270912109375" Y="-0.295379425049" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.727171875" Y="-0.442844573975" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.028708984375" Y="-0.814786193848" />
                  <Point X="-20.051568359375" Y="-0.966413330078" />
                  <Point X="-20.1010546875" Y="-1.183262695312" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-20.69688671875" Y="-1.214947998047" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.7124765625" Y="-1.121666503906" />
                  <Point X="-21.7867265625" Y="-1.137805053711" />
                  <Point X="-21.8145546875" Y="-1.154697631836" />
                  <Point X="-21.879419921875" Y="-1.232709350586" />
                  <Point X="-21.924298828125" Y="-1.286685668945" />
                  <Point X="-21.935640625" Y="-1.314071044922" />
                  <Point X="-21.9449375" Y="-1.415099609375" />
                  <Point X="-21.951369140625" Y="-1.485001342773" />
                  <Point X="-21.943638671875" Y="-1.516622070312" />
                  <Point X="-21.88425" Y="-1.608997802734" />
                  <Point X="-21.843158203125" Y="-1.672912719727" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-21.338154296875" Y="-2.064126708984" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.731431640625" Y="-2.697875732422" />
                  <Point X="-20.795865234375" Y="-2.802139160156" />
                  <Point X="-20.8982109375" Y="-2.947557861328" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.45287109375" Y="-2.717443359375" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.39037890625" Y="-2.230246582031" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.617025390625" Y="-2.275086669922" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.334682861328" />
                  <Point X="-22.767240234375" Y="-2.440787109375" />
                  <Point X="-22.80587890625" Y="-2.514200927734" />
                  <Point X="-22.8108359375" Y="-2.546375244141" />
                  <Point X="-22.78776953125" Y="-2.674095703125" />
                  <Point X="-22.77180859375" Y="-2.762465576172" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.448859375" Y="-3.327721923828" />
                  <Point X="-22.01332421875" Y="-4.082088867188" />
                  <Point X="-22.088240234375" Y="-4.135599609375" />
                  <Point X="-22.164685546875" Y="-4.190202148438" />
                  <Point X="-22.27912890625" Y="-4.264279296875" />
                  <Point X="-22.32022265625" Y="-4.29087890625" />
                  <Point X="-22.71117578125" Y="-3.781380126953" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.455416015625" Y="-2.899482177734" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.7119609375" Y="-2.84839453125" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868508789062" />
                  <Point X="-23.941044921875" Y="-2.956959960938" />
                  <Point X="-24.014650390625" Y="-3.018159179688" />
                  <Point X="-24.03154296875" Y="-3.045985839844" />
                  <Point X="-24.063349609375" Y="-3.192322998047" />
                  <Point X="-24.085357421875" Y="-3.293573730469" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-23.996220703125" Y="-3.993190917969" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.933318359375" Y="-4.947391601562" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.111361328125" Y="-4.982450195312" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#176" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.114217428061" Y="4.781389739961" Z="1.5" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.5" />
                  <Point X="-0.51660836997" Y="5.038478021476" Z="1.5" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.5" />
                  <Point X="-1.297447429651" Y="4.895891124694" Z="1.5" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.5" />
                  <Point X="-1.725180423146" Y="4.576368674178" Z="1.5" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.5" />
                  <Point X="-1.720846455202" Y="4.401313829153" Z="1.5" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.5" />
                  <Point X="-1.780482195911" Y="4.324004678734" Z="1.5" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.5" />
                  <Point X="-1.878037582872" Y="4.319994840552" Z="1.5" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.5" />
                  <Point X="-2.052510392617" Y="4.503326363812" Z="1.5" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.5" />
                  <Point X="-2.401023016381" Y="4.461712159474" Z="1.5" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.5" />
                  <Point X="-3.028685220761" Y="4.061875362072" Z="1.5" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.5" />
                  <Point X="-3.155757512017" Y="3.407451984082" Z="1.5" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.5" />
                  <Point X="-2.998463838102" Y="3.105327603337" Z="1.5" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.5" />
                  <Point X="-3.018872833079" Y="3.029930734808" Z="1.5" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.5" />
                  <Point X="-3.08974886227" Y="2.997100861071" Z="1.5" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.5" />
                  <Point X="-3.526407364215" Y="3.224436436729" Z="1.5" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.5" />
                  <Point X="-3.962904007647" Y="3.160983959707" Z="1.5" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.5" />
                  <Point X="-4.346709339722" Y="2.608166395162" Z="1.5" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.5" />
                  <Point X="-4.044615294269" Y="1.877904555007" Z="1.5" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.5" />
                  <Point X="-3.684400211176" Y="1.587470982221" Z="1.5" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.5" />
                  <Point X="-3.676901988752" Y="1.529370180355" Z="1.5" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.5" />
                  <Point X="-3.716590063909" Y="1.486279763422" Z="1.5" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.5" />
                  <Point X="-4.381538012644" Y="1.557594841571" Z="1.5" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.5" />
                  <Point X="-4.880428857307" Y="1.378925927194" Z="1.5" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.5" />
                  <Point X="-5.008612272262" Y="0.796126579437" Z="1.5" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.5" />
                  <Point X="-4.18334520555" Y="0.211656443249" Z="1.5" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.5" />
                  <Point X="-3.565211229972" Y="0.041191826964" Z="1.5" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.5" />
                  <Point X="-3.545024625843" Y="0.017617433894" Z="1.5" />
                  <Point X="-3.539556741714" Y="0" Z="1.5" />
                  <Point X="-3.543339975292" Y="-0.012189517167" Z="1.5" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.5" />
                  <Point X="-3.560157365151" Y="-0.037684156169" Z="1.5" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.5" />
                  <Point X="-4.453542583222" Y="-0.284055613507" Z="1.5" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.5" />
                  <Point X="-5.028565909203" Y="-0.668713592562" Z="1.5" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.5" />
                  <Point X="-4.927143487974" Y="-1.207022637533" Z="1.5" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.5" />
                  <Point X="-3.884823038104" Y="-1.394499639076" Z="1.5" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.5" />
                  <Point X="-3.20832886743" Y="-1.313237386699" Z="1.5" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.5" />
                  <Point X="-3.195827050399" Y="-1.33461495365" Z="1.5" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.5" />
                  <Point X="-3.970236678889" Y="-1.942928336394" Z="1.5" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.5" />
                  <Point X="-4.382855426339" Y="-2.552953154283" Z="1.5" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.5" />
                  <Point X="-4.067402204988" Y="-3.030383677378" Z="1.5" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.5" />
                  <Point X="-3.100137842852" Y="-2.859926807427" Z="1.5" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.5" />
                  <Point X="-2.565744838676" Y="-2.562585630455" Z="1.5" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.5" />
                  <Point X="-2.995490205741" Y="-3.33494053964" Z="1.5" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.5" />
                  <Point X="-3.132481735136" Y="-3.991165423629" Z="1.5" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.5" />
                  <Point X="-2.710800679417" Y="-4.288752349716" Z="1.5" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.5" />
                  <Point X="-2.318193014508" Y="-4.276310736146" Z="1.5" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.5" />
                  <Point X="-2.120727273461" Y="-4.085962567676" Z="1.5" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.5" />
                  <Point X="-1.841649883209" Y="-3.992382579868" Z="1.5" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.5" />
                  <Point X="-1.563275093932" Y="-4.088032372641" Z="1.5" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.5" />
                  <Point X="-1.400653353005" Y="-4.333380244887" Z="1.5" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.5" />
                  <Point X="-1.393379328191" Y="-4.729717152762" Z="1.5" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.5" />
                  <Point X="-1.292174027456" Y="-4.910616207379" Z="1.5" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.5" />
                  <Point X="-0.994826074654" Y="-4.979375959997" Z="1.5" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.5" />
                  <Point X="-0.580904026132" Y="-4.130147408069" Z="1.5" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.5" />
                  <Point X="-0.350130531697" Y="-3.422301890207" Z="1.5" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.5" />
                  <Point X="-0.149748185347" Y="-3.250715678946" Z="1.5" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.5" />
                  <Point X="0.103610894014" Y="-3.236396366342" Z="1.5" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.5" />
                  <Point X="0.320315327735" Y="-3.379343865561" Z="1.5" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.5" />
                  <Point X="0.653850834269" Y="-4.402388621747" Z="1.5" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.5" />
                  <Point X="0.891418758611" Y="-5.000365144281" Z="1.5" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.5" />
                  <Point X="1.071229505152" Y="-4.964951738448" Z="1.5" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.5" />
                  <Point X="1.047194778021" Y="-3.955384480999" Z="1.5" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.5" />
                  <Point X="0.979353049992" Y="-3.171663006934" Z="1.5" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.5" />
                  <Point X="1.084764150347" Y="-2.964126589518" Z="1.5" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.5" />
                  <Point X="1.286464281873" Y="-2.866903957892" Z="1.5" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.5" />
                  <Point X="1.511387033833" Y="-2.910260215575" Z="1.5" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.5" />
                  <Point X="2.242999780416" Y="-3.780538190829" Z="1.5" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.5" />
                  <Point X="2.741884482515" Y="-4.274973142554" Z="1.5" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.5" />
                  <Point X="2.934662652776" Y="-4.145006790553" Z="1.5" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.5" />
                  <Point X="2.588284974437" Y="-3.271441771714" Z="1.5" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.5" />
                  <Point X="2.255277348776" Y="-2.633928693528" Z="1.5" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.5" />
                  <Point X="2.270848221957" Y="-2.432794515286" Z="1.5" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.5" />
                  <Point X="2.400103863086" Y="-2.28805308801" Z="1.5" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.5" />
                  <Point X="2.594578109985" Y="-2.248170660715" Z="1.5" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.5" />
                  <Point X="3.515971825393" Y="-2.729464525838" Z="1.5" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.5" />
                  <Point X="4.136520413093" Y="-2.945055311705" Z="1.5" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.5" />
                  <Point X="4.304943981954" Y="-2.692881127803" Z="1.5" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.5" />
                  <Point X="3.686125545551" Y="-1.993178956942" Z="1.5" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.5" />
                  <Point X="3.151651518049" Y="-1.550678202286" Z="1.5" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.5" />
                  <Point X="3.098694842075" Y="-1.38840076883" Z="1.5" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.5" />
                  <Point X="3.152871164621" Y="-1.23339587609" Z="1.5" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.5" />
                  <Point X="3.291986046074" Y="-1.139245525865" Z="1.5" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.5" />
                  <Point X="4.290431805889" Y="-1.233240161961" Z="1.5" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.5" />
                  <Point X="4.941534962652" Y="-1.163106404936" Z="1.5" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.5" />
                  <Point X="5.014575374751" Y="-0.790960043491" Z="1.5" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.5" />
                  <Point X="4.279611678459" Y="-0.363268224354" Z="1.5" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.5" />
                  <Point X="3.710120851392" Y="-0.198943150459" Z="1.5" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.5" />
                  <Point X="3.63274360573" Y="-0.138414208365" Z="1.5" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.5" />
                  <Point X="3.592370354055" Y="-0.057101769452" Z="1.5" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.5" />
                  <Point X="3.589001096369" Y="0.039508761752" Z="1.5" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.5" />
                  <Point X="3.622635832671" Y="0.12553453021" Z="1.5" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.5" />
                  <Point X="3.69327456296" Y="0.189205693783" Z="1.5" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.5" />
                  <Point X="4.516356138848" Y="0.426703708415" Z="1.5" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.5" />
                  <Point X="5.021064477831" Y="0.742260855977" Z="1.5" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.5" />
                  <Point X="4.940675156983" Y="1.162654306982" Z="1.5" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.5" />
                  <Point X="4.042873440884" Y="1.298349908928" Z="1.5" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.5" />
                  <Point X="3.424614469031" Y="1.227113245031" Z="1.5" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.5" />
                  <Point X="3.340480578563" Y="1.250500375268" Z="1.5" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.5" />
                  <Point X="3.279665471616" Y="1.30354284935" Z="1.5" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.5" />
                  <Point X="3.244035294685" Y="1.381735836052" Z="1.5" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.5" />
                  <Point X="3.242394228142" Y="1.463823744711" Z="1.5" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.5" />
                  <Point X="3.278745936824" Y="1.540140835676" Z="1.5" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.5" />
                  <Point X="3.983394737667" Y="2.099185203215" Z="1.5" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.5" />
                  <Point X="4.361789271135" Y="2.596488341688" Z="1.5" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.5" />
                  <Point X="4.141703621415" Y="2.934833199327" Z="1.5" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.5" />
                  <Point X="3.120186270931" Y="2.619360438365" Z="1.5" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.5" />
                  <Point X="2.477044947855" Y="2.258218803324" Z="1.5" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.5" />
                  <Point X="2.401200438728" Y="2.248952629713" Z="1.5" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.5" />
                  <Point X="2.334276764102" Y="2.271467964588" Z="1.5" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.5" />
                  <Point X="2.279290635647" Y="2.32274809628" Z="1.5" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.5" />
                  <Point X="2.250477073045" Y="2.388558005105" Z="1.5" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.5" />
                  <Point X="2.254309139334" Y="2.462424628027" Z="1.5" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.5" />
                  <Point X="2.776264914494" Y="3.391952811024" Z="1.5" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.5" />
                  <Point X="2.975218130709" Y="4.111356779246" Z="1.5" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.5" />
                  <Point X="2.590844535407" Y="4.36379419278" Z="1.5" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.5" />
                  <Point X="2.187385620676" Y="4.579497740662" Z="1.5" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.5" />
                  <Point X="1.769290005479" Y="4.756686382357" Z="1.5" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.5" />
                  <Point X="1.24921189048" Y="4.912877961519" Z="1.5" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.5" />
                  <Point X="0.588843457614" Y="5.034893262422" Z="1.5" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.5" />
                  <Point X="0.079027133492" Y="4.65005776599" Z="1.5" />
                  <Point X="0" Y="4.355124473572" Z="1.5" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>