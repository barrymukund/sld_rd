<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#215" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3553" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004715820312" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.436697265625" Y="-3.512517578125" />
                  <Point X="-24.44228125" Y="-3.497130615234" />
                  <Point X="-24.457638671875" Y="-3.467373291016" />
                  <Point X="-24.615646484375" Y="-3.239713623047" />
                  <Point X="-24.6213671875" Y="-3.231472900391" />
                  <Point X="-24.643255859375" Y="-3.209014160156" />
                  <Point X="-24.669509765625" Y="-3.189773681641" />
                  <Point X="-24.697505859375" Y="-3.175668701172" />
                  <Point X="-24.942013671875" Y="-3.099782226562" />
                  <Point X="-24.950865234375" Y="-3.097035400391" />
                  <Point X="-24.979025390625" Y="-3.092766113281" />
                  <Point X="-25.0086640625" Y="-3.092766357422" />
                  <Point X="-25.03682421875" Y="-3.097035644531" />
                  <Point X="-25.28133203125" Y="-3.172921875" />
                  <Point X="-25.29018359375" Y="-3.175668945312" />
                  <Point X="-25.318185546875" Y="-3.189778808594" />
                  <Point X="-25.344443359375" Y="-3.209025634766" />
                  <Point X="-25.366326171875" Y="-3.231479492188" />
                  <Point X="-25.524333984375" Y="-3.459139160156" />
                  <Point X="-25.529759765625" Y="-3.467944091797" />
                  <Point X="-25.542697265625" Y="-3.491750244141" />
                  <Point X="-25.55098828125" Y="-3.5125234375" />
                  <Point X="-25.564982421875" Y="-3.564747802734" />
                  <Point X="-25.916583984375" Y="-4.876941894531" />
                  <Point X="-26.07314453125" Y="-4.846552734375" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.2149609375" Y="-4.563438476562" />
                  <Point X="-26.21419921875" Y="-4.5479296875" />
                  <Point X="-26.2165078125" Y="-4.516220214844" />
                  <Point X="-26.274921875" Y="-4.22255859375" />
                  <Point X="-26.277037109375" Y="-4.211928710938" />
                  <Point X="-26.28794140625" Y="-4.182956542969" />
                  <Point X="-26.304013671875" Y="-4.155123046875" />
                  <Point X="-26.32364453125" Y="-4.131204589844" />
                  <Point X="-26.548755859375" Y="-3.933785400391" />
                  <Point X="-26.556904296875" Y="-3.926639404297" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.941802734375" Y="-3.871383544922" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708496094" />
                  <Point X="-27.014466796875" Y="-3.882028076172" />
                  <Point X="-27.042658203125" Y="-3.894801513672" />
                  <Point X="-27.29161328125" Y="-4.061148193359" />
                  <Point X="-27.300626953125" Y="-4.067172119141" />
                  <Point X="-27.31762890625" Y="-4.081698486328" />
                  <Point X="-27.33737109375" Y="-4.103069824219" />
                  <Point X="-27.342958984375" Y="-4.109700683594" />
                  <Point X="-27.4801484375" Y="-4.288489257813" />
                  <Point X="-27.79263671875" Y="-4.09500390625" />
                  <Point X="-27.801712890625" Y="-4.089383789063" />
                  <Point X="-28.104720703125" Y="-3.856077148438" />
                  <Point X="-27.42376171875" Y="-2.676620117188" />
                  <Point X="-27.412859375" Y="-2.647655029297" />
                  <Point X="-27.406587890625" Y="-2.616129150391" />
                  <Point X="-27.40557421875" Y="-2.585194824219" />
                  <Point X="-27.41455859375" Y="-2.555576416016" />
                  <Point X="-27.428775390625" Y="-2.526747558594" />
                  <Point X="-27.446802734375" Y="-2.501590820312" />
                  <Point X="-27.46350390625" Y="-2.484888427734" />
                  <Point X="-27.464109375" Y="-2.484281982422" />
                  <Point X="-27.489267578125" Y="-2.466240234375" />
                  <Point X="-27.518099609375" Y="-2.452010498047" />
                  <Point X="-27.547724609375" Y="-2.443015625" />
                  <Point X="-27.57866796875" Y="-2.444023681641" />
                  <Point X="-27.610205078125" Y="-2.450293212891" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-27.684224609375" Y="-2.487202636719" />
                  <Point X="-28.8180234375" Y="-3.141801025391" />
                  <Point X="-29.07569140625" Y="-2.803277587891" />
                  <Point X="-29.082861328125" Y="-2.793855957031" />
                  <Point X="-29.306142578125" Y="-2.419450927734" />
                  <Point X="-28.105958984375" Y="-1.498516723633" />
                  <Point X="-28.084578125" Y="-1.475593505859" />
                  <Point X="-28.06661328125" Y="-1.448462524414" />
                  <Point X="-28.053857421875" Y="-1.419834350586" />
                  <Point X="-28.04642578125" Y="-1.391142700195" />
                  <Point X="-28.0461640625" Y="-1.390133789062" />
                  <Point X="-28.043349609375" Y="-1.359697875977" />
                  <Point X="-28.04555078125" Y="-1.328018798828" />
                  <Point X="-28.052546875" Y="-1.298264038086" />
                  <Point X="-28.068630859375" Y="-1.272271362305" />
                  <Point X="-28.089466796875" Y="-1.248307006836" />
                  <Point X="-28.112970703125" Y="-1.228767944336" />
                  <Point X="-28.13852734375" Y="-1.213725952148" />
                  <Point X="-28.139490234375" Y="-1.213159545898" />
                  <Point X="-28.168740234375" Y="-1.201947631836" />
                  <Point X="-28.200615234375" Y="-1.19547253418" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-28.2887890625" Y="-1.201869873047" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.831271484375" Y="-1.003632568359" />
                  <Point X="-29.834078125" Y="-0.992649963379" />
                  <Point X="-29.892423828125" Y="-0.584698669434" />
                  <Point X="-28.532876953125" Y="-0.220408920288" />
                  <Point X="-28.5174921875" Y="-0.21482774353" />
                  <Point X="-28.4877265625" Y="-0.19946913147" />
                  <Point X="-28.460943359375" Y="-0.180879821777" />
                  <Point X="-28.459998046875" Y="-0.180223632812" />
                  <Point X="-28.4375625" Y="-0.158365661621" />
                  <Point X="-28.41829296875" Y="-0.132093933105" />
                  <Point X="-28.404166015625" Y="-0.104066749573" />
                  <Point X="-28.39523828125" Y="-0.075300956726" />
                  <Point X="-28.394841796875" Y="-0.074011802673" />
                  <Point X="-28.39063671875" Y="-0.046031822205" />
                  <Point X="-28.390646484375" Y="-0.016428142548" />
                  <Point X="-28.394916015625" Y="0.011699830055" />
                  <Point X="-28.40384375" Y="0.040465473175" />
                  <Point X="-28.4041796875" Y="0.041548042297" />
                  <Point X="-28.418240234375" Y="0.069437705994" />
                  <Point X="-28.437494140625" Y="0.095730384827" />
                  <Point X="-28.45997265625" Y="0.117646789551" />
                  <Point X="-28.486755859375" Y="0.136236099243" />
                  <Point X="-28.500005859375" Y="0.143928390503" />
                  <Point X="-28.532875" Y="0.157848175049" />
                  <Point X="-28.58470703125" Y="0.171736679077" />
                  <Point X="-29.891814453125" Y="0.521975402832" />
                  <Point X="-29.82629296875" Y="0.9647734375" />
                  <Point X="-29.82448828125" Y="0.97697052002" />
                  <Point X="-29.70355078125" Y="1.423268066406" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.744982421875" Y="1.299341796875" />
                  <Point X="-28.723421875" Y="1.301228271484" />
                  <Point X="-28.703134765625" Y="1.305263793945" />
                  <Point X="-28.64385546875" Y="1.323954956055" />
                  <Point X="-28.64173046875" Y="1.324624633789" />
                  <Point X="-28.6227890625" Y="1.332956176758" />
                  <Point X="-28.604041015625" Y="1.343778442383" />
                  <Point X="-28.587359375" Y="1.356008544922" />
                  <Point X="-28.57371875" Y="1.371559570312" />
                  <Point X="-28.56130078125" Y="1.389291748047" />
                  <Point X="-28.55134765625" Y="1.40743359375" />
                  <Point X="-28.5275625" Y="1.464859130859" />
                  <Point X="-28.526697265625" Y="1.466948120117" />
                  <Point X="-28.52091796875" Y="1.486784790039" />
                  <Point X="-28.517158203125" Y="1.508104370117" />
                  <Point X="-28.5158046875" Y="1.528742675781" />
                  <Point X="-28.518951171875" Y="1.549184814453" />
                  <Point X="-28.524552734375" Y="1.570095703125" />
                  <Point X="-28.53205078125" Y="1.589380615234" />
                  <Point X="-28.560751953125" Y="1.644514770508" />
                  <Point X="-28.561791015625" Y="1.646510498047" />
                  <Point X="-28.573287109375" Y="1.663713500977" />
                  <Point X="-28.58719921875" Y="1.680291381836" />
                  <Point X="-28.602134765625" Y="1.694589477539" />
                  <Point X="-28.631865234375" Y="1.717403198242" />
                  <Point X="-29.351859375" Y="2.269873779297" />
                  <Point X="-29.088166015625" Y="2.721642089844" />
                  <Point X="-29.0811484375" Y="2.733665039062" />
                  <Point X="-28.75050390625" Y="3.158661621094" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.83633984375" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796630859" />
                  <Point X="-28.064232421875" Y="2.818573486328" />
                  <Point X="-28.061244140625" Y="2.818312011719" />
                  <Point X="-28.04056640625" Y="2.818763183594" />
                  <Point X="-28.019107421875" Y="2.821588134766" />
                  <Point X="-27.999013671875" Y="2.826504638672" />
                  <Point X="-27.9804609375" Y="2.835654052734" />
                  <Point X="-27.96220703125" Y="2.847283447266" />
                  <Point X="-27.946078125" Y="2.860229003906" />
                  <Point X="-27.887474609375" Y="2.918831298828" />
                  <Point X="-27.885353515625" Y="2.920952392578" />
                  <Point X="-27.87240234375" Y="2.937089355469" />
                  <Point X="-27.8607734375" Y="2.955345458984" />
                  <Point X="-27.851625" Y="2.973897460938" />
                  <Point X="-27.8467109375" Y="2.993989501953" />
                  <Point X="-27.84388671875" Y="3.015450683594" />
                  <Point X="-27.843435546875" Y="3.036124755859" />
                  <Point X="-27.850658203125" Y="3.118685791016" />
                  <Point X="-27.850919921875" Y="3.121674316406" />
                  <Point X="-27.85495703125" Y="3.14196484375" />
                  <Point X="-27.86146484375" Y="3.162602783203" />
                  <Point X="-27.869794921875" Y="3.181535644531" />
                  <Point X="-27.882970703125" Y="3.204354980469" />
                  <Point X="-28.183330078125" Y="3.724596191406" />
                  <Point X="-27.71283203125" Y="4.085322509766" />
                  <Point X="-27.700615234375" Y="4.094690185547" />
                  <Point X="-27.16703515625" Y="4.391134765625" />
                  <Point X="-27.0431953125" Y="4.229741210938" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.995111328125" Y="4.18939453125" />
                  <Point X="-26.903220703125" Y="4.141559570313" />
                  <Point X="-26.8998984375" Y="4.139830078125" />
                  <Point X="-26.880626953125" Y="4.132334960938" />
                  <Point X="-26.85971875" Y="4.126730957031" />
                  <Point X="-26.8392734375" Y="4.123583007812" />
                  <Point X="-26.8186328125" Y="4.124935058594" />
                  <Point X="-26.79731640625" Y="4.128692871094" />
                  <Point X="-26.777453125" Y="4.134481445312" />
                  <Point X="-26.681744140625" Y="4.174125976563" />
                  <Point X="-26.678279296875" Y="4.175561035156" />
                  <Point X="-26.660146484375" Y="4.185509277344" />
                  <Point X="-26.642416015625" Y="4.197923828125" />
                  <Point X="-26.62686328125" Y="4.211562988281" />
                  <Point X="-26.614630859375" Y="4.228245117188" />
                  <Point X="-26.60380859375" Y="4.246990234375" />
                  <Point X="-26.595478515625" Y="4.265923828125" />
                  <Point X="-26.564359375" Y="4.364626464844" />
                  <Point X="-26.563228515625" Y="4.368202148438" />
                  <Point X="-26.559169921875" Y="4.388556640625" />
                  <Point X="-26.557279296875" Y="4.410134765625" />
                  <Point X="-26.557728515625" Y="4.430828125" />
                  <Point X="-26.5592265625" Y="4.442205566406" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-25.96544140625" Y="4.805376464844" />
                  <Point X="-25.9496328125" Y="4.80980859375" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.85378515625" Y="4.286311035156" />
                  <Point X="-24.847033203125" Y="4.311504882812" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.16976953125" Y="4.833185058594" />
                  <Point X="-24.155958984375" Y="4.831738769531" />
                  <Point X="-23.53398828125" Y="4.681576171875" />
                  <Point X="-23.51896875" Y="4.677949707031" />
                  <Point X="-23.1141796875" Y="4.531129882812" />
                  <Point X="-23.105357421875" Y="4.5279296875" />
                  <Point X="-22.713888671875" Y="4.344853515625" />
                  <Point X="-22.705427734375" Y="4.340896484375" />
                  <Point X="-22.327232421875" Y="4.12055859375" />
                  <Point X="-22.31902734375" Y="4.115777832031" />
                  <Point X="-22.05673828125" Y="3.929254638672" />
                  <Point X="-22.8524140625" Y="2.551102539062" />
                  <Point X="-22.857921875" Y="2.53993359375" />
                  <Point X="-22.866921875" Y="2.516057861328" />
                  <Point X="-22.887642578125" Y="2.438576171875" />
                  <Point X="-22.890431640625" Y="2.423128173828" />
                  <Point X="-22.89251953125" Y="2.401420166016" />
                  <Point X="-22.892271484375" Y="2.380950927734" />
                  <Point X="-22.884201171875" Y="2.314037353516" />
                  <Point X="-22.883912109375" Y="2.311611816406" />
                  <Point X="-22.878572265625" Y="2.289659667969" />
                  <Point X="-22.870302734375" Y="2.267542236328" />
                  <Point X="-22.8599296875" Y="2.247471435547" />
                  <Point X="-22.81847265625" Y="2.186374267578" />
                  <Point X="-22.80856640625" Y="2.174101806641" />
                  <Point X="-22.79377734375" Y="2.158608642578" />
                  <Point X="-22.778400390625" Y="2.145593505859" />
                  <Point X="-22.7173046875" Y="2.104136474609" />
                  <Point X="-22.71505859375" Y="2.102612548828" />
                  <Point X="-22.695021484375" Y="2.092259277344" />
                  <Point X="-22.672951171875" Y="2.084002929688" />
                  <Point X="-22.6510390625" Y="2.078663818359" />
                  <Point X="-22.5840390625" Y="2.070584716797" />
                  <Point X="-22.56801953125" Y="2.070015380859" />
                  <Point X="-22.546689453125" Y="2.071060058594" />
                  <Point X="-22.526794921875" Y="2.074171142578" />
                  <Point X="-22.4493125" Y="2.094890869141" />
                  <Point X="-22.43962109375" Y="2.098048339844" />
                  <Point X="-22.41146484375" Y="2.110145507813" />
                  <Point X="-22.35933203125" Y="2.140244873047" />
                  <Point X="-21.032671875" Y="2.90619140625" />
                  <Point X="-20.88159765625" Y="2.696230957031" />
                  <Point X="-20.876728515625" Y="2.689464599609" />
                  <Point X="-20.73780078125" Y="2.459883789063" />
                  <Point X="-21.76921875" Y="1.668448852539" />
                  <Point X="-21.7785703125" Y="1.660244995117" />
                  <Point X="-21.79602734375" Y="1.641627685547" />
                  <Point X="-21.851791015625" Y="1.568879882813" />
                  <Point X="-21.860142578125" Y="1.555928833008" />
                  <Point X="-21.87062890625" Y="1.536344604492" />
                  <Point X="-21.8783671875" Y="1.517088623047" />
                  <Point X="-21.899140625" Y="1.442812744141" />
                  <Point X="-21.899892578125" Y="1.440124389648" />
                  <Point X="-21.90334765625" Y="1.417825073242" />
                  <Point X="-21.9041640625" Y="1.394254394531" />
                  <Point X="-21.902259765625" Y="1.371767333984" />
                  <Point X="-21.88520703125" Y="1.289125976562" />
                  <Point X="-21.880861328125" Y="1.274289794922" />
                  <Point X="-21.873044921875" Y="1.253920288086" />
                  <Point X="-21.86371484375" Y="1.235739501953" />
                  <Point X="-21.81739453125" Y="1.165335449219" />
                  <Point X="-21.81571875" Y="1.162782592773" />
                  <Point X="-21.801123046875" Y="1.145469726562" />
                  <Point X="-21.783869140625" Y="1.1293671875" />
                  <Point X="-21.765650390625" Y="1.116034301758" />
                  <Point X="-21.69844921875" Y="1.078206542969" />
                  <Point X="-21.684111328125" Y="1.071637817383" />
                  <Point X="-21.66369140625" Y="1.064265014648" />
                  <Point X="-21.643876953125" Y="1.059438110352" />
                  <Point X="-21.553005859375" Y="1.047428222656" />
                  <Point X="-21.54309765625" Y="1.046643066406" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-21.4622734375" Y="1.053504150391" />
                  <Point X="-20.22316015625" Y="1.21663659668" />
                  <Point X="-20.15615625" Y="0.941404541016" />
                  <Point X="-20.154064453125" Y="0.932807067871" />
                  <Point X="-20.109134765625" Y="0.64423828125" />
                  <Point X="-21.28341796875" Y="0.329590332031" />
                  <Point X="-21.2952109375" Y="0.325585357666" />
                  <Point X="-21.318453125" Y="0.315067657471" />
                  <Point X="-21.4077265625" Y="0.26346572876" />
                  <Point X="-21.420173828125" Y="0.254881393433" />
                  <Point X="-21.43772265625" Y="0.240590621948" />
                  <Point X="-21.45246875" Y="0.225576126099" />
                  <Point X="-21.50603515625" Y="0.15731918335" />
                  <Point X="-21.507970703125" Y="0.15485319519" />
                  <Point X="-21.51969140625" Y="0.135581436157" />
                  <Point X="-21.52946875" Y="0.114115386963" />
                  <Point X="-21.536318359375" Y="0.092605926514" />
                  <Point X="-21.554173828125" Y="-0.000629799545" />
                  <Point X="-21.555828125" Y="-0.01573157692" />
                  <Point X="-21.556474609375" Y="-0.037920143127" />
                  <Point X="-21.5548203125" Y="-0.058555438995" />
                  <Point X="-21.53696484375" Y="-0.151791015625" />
                  <Point X="-21.536318359375" Y="-0.15516607666" />
                  <Point X="-21.529470703125" Y="-0.176669464111" />
                  <Point X="-21.519697265625" Y="-0.198130950928" />
                  <Point X="-21.507974609375" Y="-0.217408630371" />
                  <Point X="-21.454408203125" Y="-0.285665435791" />
                  <Point X="-21.443853515625" Y="-0.297058654785" />
                  <Point X="-21.4276015625" Y="-0.311950073242" />
                  <Point X="-21.410962890625" Y="-0.324155975342" />
                  <Point X="-21.32169140625" Y="-0.37575592041" />
                  <Point X="-21.313072265625" Y="-0.380168151855" />
                  <Point X="-21.283419921875" Y="-0.392149871826" />
                  <Point X="-21.23800390625" Y="-0.404318908691" />
                  <Point X="-20.108525390625" Y="-0.706961975098" />
                  <Point X="-20.143806640625" Y="-0.94097668457" />
                  <Point X="-20.1449765625" Y="-0.94872644043" />
                  <Point X="-20.19882421875" Y="-1.18469921875" />
                  <Point X="-21.5756171875" Y="-1.003441040039" />
                  <Point X="-21.59196484375" Y="-1.002710327148" />
                  <Point X="-21.625341796875" Y="-1.005508850098" />
                  <Point X="-21.800564453125" Y="-1.043593994141" />
                  <Point X="-21.80690625" Y="-1.04497265625" />
                  <Point X="-21.836025390625" Y="-1.056596801758" />
                  <Point X="-21.863849609375" Y="-1.073487304688" />
                  <Point X="-21.8876015625" Y="-1.093958618164" />
                  <Point X="-21.99351171875" Y="-1.2213359375" />
                  <Point X="-21.997345703125" Y="-1.225946655273" />
                  <Point X="-22.01206640625" Y="-1.250329467773" />
                  <Point X="-22.02341015625" Y="-1.277716308594" />
                  <Point X="-22.030240234375" Y="-1.305365356445" />
                  <Point X="-22.04541796875" Y="-1.470311645508" />
                  <Point X="-22.045970703125" Y="-1.476287841797" />
                  <Point X="-22.043650390625" Y="-1.507576660156" />
                  <Point X="-22.03591796875" Y="-1.539190673828" />
                  <Point X="-22.023548828125" Y="-1.567995239258" />
                  <Point X="-21.926578125" Y="-1.718825805664" />
                  <Point X="-21.9195390625" Y="-1.728401245117" />
                  <Point X="-21.904408203125" Y="-1.746491088867" />
                  <Point X="-21.88937109375" Y="-1.760908569336" />
                  <Point X="-21.8472265625" Y="-1.793248046875" />
                  <Point X="-20.786876953125" Y="-2.606883056641" />
                  <Point X="-20.87189453125" Y="-2.744454589844" />
                  <Point X="-20.87519921875" Y="-2.749799560547" />
                  <Point X="-20.971017578125" Y="-2.8859453125" />
                  <Point X="-22.199046875" Y="-2.176941894531" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.2457734375" Y="-2.159825195312" />
                  <Point X="-22.454314453125" Y="-2.122162841797" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525388671875" Y="-2.125352783203" />
                  <Point X="-22.5551640625" Y="-2.135176757812" />
                  <Point X="-22.72841015625" Y="-2.226355224609" />
                  <Point X="-22.734681640625" Y="-2.229655761719" />
                  <Point X="-22.757609375" Y="-2.246544677734" />
                  <Point X="-22.7785703125" Y="-2.267503173828" />
                  <Point X="-22.795466796875" Y="-2.290437255859" />
                  <Point X="-22.886646484375" Y="-2.463684082031" />
                  <Point X="-22.889947265625" Y="-2.469955322266" />
                  <Point X="-22.899771484375" Y="-2.499734375" />
                  <Point X="-22.904728515625" Y="-2.531908691406" />
                  <Point X="-22.90432421875" Y="-2.563259033203" />
                  <Point X="-22.86666015625" Y="-2.77180078125" />
                  <Point X="-22.863986328125" Y="-2.782801513672" />
                  <Point X="-22.856720703125" Y="-2.806463867188" />
                  <Point X="-22.8481796875" Y="-2.826077636719" />
                  <Point X="-22.82109765625" Y="-2.872986328125" />
                  <Point X="-22.13871484375" Y="-4.054906738281" />
                  <Point X="-22.214234375" Y="-4.108848632812" />
                  <Point X="-22.21814453125" Y="-4.111641113281" />
                  <Point X="-22.298234375" Y="-4.163481445312" />
                  <Point X="-23.241451171875" Y="-2.934256103516" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.48375390625" Y="-2.768324951172" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.80784375" Y="-2.76181640625" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397705078" />
                  <Point X="-23.871021484375" Y="-2.780740966797" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-24.069099609375" Y="-2.939884033203" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968861328125" />
                  <Point X="-24.11275" Y="-2.996687988281" />
                  <Point X="-24.124375" Y="-3.025809082031" />
                  <Point X="-24.17630859375" Y="-3.264747802734" />
                  <Point X="-24.178" Y="-3.275420654297" />
                  <Point X="-24.18059375" Y="-3.301215820312" />
                  <Point X="-24.1802578125" Y="-3.323121337891" />
                  <Point X="-24.17258203125" Y="-3.381418945312" />
                  <Point X="-23.977935546875" Y="-4.859915527344" />
                  <Point X="-24.020623046875" Y="-4.869272949219" />
                  <Point X="-24.024326171875" Y="-4.870084472656" />
                  <Point X="-24.070681640625" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.05504296875" Y="-4.753293457031" />
                  <Point X="-26.058423828125" Y="-4.75263671875" />
                  <Point X="-26.141244140625" Y="-4.731328613281" />
                  <Point X="-26.1207734375" Y="-4.575838378906" />
                  <Point X="-26.120076171875" Y="-4.568098632812" />
                  <Point X="-26.11944921875" Y="-4.541031738281" />
                  <Point X="-26.1217578125" Y="-4.509322265625" />
                  <Point X="-26.123333984375" Y="-4.497686523438" />
                  <Point X="-26.181748046875" Y="-4.204024902344" />
                  <Point X="-26.188126953125" Y="-4.178465332031" />
                  <Point X="-26.19903125" Y="-4.149493164062" />
                  <Point X="-26.205671875" Y="-4.135450683594" />
                  <Point X="-26.221744140625" Y="-4.1076171875" />
                  <Point X="-26.230580078125" Y="-4.094853027344" />
                  <Point X="-26.2502109375" Y="-4.070934570312" />
                  <Point X="-26.261005859375" Y="-4.059780029297" />
                  <Point X="-26.4861171875" Y="-3.862360839844" />
                  <Point X="-26.50673828125" Y="-3.845965332031" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.93558984375" Y="-3.776586914062" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166015625" />
                  <Point X="-27.008005859375" Y="-3.781945556641" />
                  <Point X="-27.0390546875" Y="-3.790265136719" />
                  <Point X="-27.053673828125" Y="-3.79549609375" />
                  <Point X="-27.081865234375" Y="-3.80826953125" />
                  <Point X="-27.0954375" Y="-3.815812011719" />
                  <Point X="-27.344392578125" Y="-3.982158691406" />
                  <Point X="-27.362337890625" Y="-3.994944824219" />
                  <Point X="-27.37933984375" Y="-4.009471191406" />
                  <Point X="-27.387412109375" Y="-4.017235839844" />
                  <Point X="-27.407154296875" Y="-4.038607177734" />
                  <Point X="-27.418328125" Y="-4.051868408203" />
                  <Point X="-27.503203125" Y="-4.162478027344" />
                  <Point X="-27.742625" Y="-4.014233398438" />
                  <Point X="-27.747591796875" Y="-4.011157958984" />
                  <Point X="-27.98086328125" Y="-3.831546630859" />
                  <Point X="-27.341490234375" Y="-2.724120117188" />
                  <Point X="-27.3348515625" Y="-2.710085693359" />
                  <Point X="-27.32394921875" Y="-2.681120605469" />
                  <Point X="-27.319685546875" Y="-2.666190429688" />
                  <Point X="-27.3134140625" Y="-2.634664550781" />
                  <Point X="-27.311638671875" Y="-2.619240478516" />
                  <Point X="-27.310625" Y="-2.588306152344" />
                  <Point X="-27.3146640625" Y="-2.557618652344" />
                  <Point X="-27.3236484375" Y="-2.528000244141" />
                  <Point X="-27.32935546875" Y="-2.513559082031" />
                  <Point X="-27.343572265625" Y="-2.484730224609" />
                  <Point X="-27.3515546875" Y="-2.471411621094" />
                  <Point X="-27.36958203125" Y="-2.446254882812" />
                  <Point X="-27.379625" Y="-2.434418212891" />
                  <Point X="-27.396326171875" Y="-2.417715820312" />
                  <Point X="-27.40874609375" Y="-2.407081298828" />
                  <Point X="-27.433904296875" Y="-2.389039550781" />
                  <Point X="-27.44722265625" Y="-2.381050537109" />
                  <Point X="-27.4760546875" Y="-2.366820800781" />
                  <Point X="-27.4905" Y="-2.361108154297" />
                  <Point X="-27.520125" Y="-2.35211328125" />
                  <Point X="-27.550818359375" Y="-2.348065917969" />
                  <Point X="-27.58176171875" Y="-2.349073974609" />
                  <Point X="-27.59719140625" Y="-2.350847167969" />
                  <Point X="-27.628728515625" Y="-2.357116699219" />
                  <Point X="-27.6436640625" Y="-2.361380126953" />
                  <Point X="-27.672640625" Y="-2.372284179688" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-27.731724609375" Y="-2.404930175781" />
                  <Point X="-28.793087890625" Y="-3.017708007813" />
                  <Point X="-29.00009765625" Y="-2.745739501953" />
                  <Point X="-29.00401953125" Y="-2.740585693359" />
                  <Point X="-29.181265625" Y="-2.443374267578" />
                  <Point X="-28.048126953125" Y="-1.573885131836" />
                  <Point X="-28.03648828125" Y="-1.563314086914" />
                  <Point X="-28.015107421875" Y="-1.540390991211" />
                  <Point X="-28.005369140625" Y="-1.528042358398" />
                  <Point X="-27.987404296875" Y="-1.500911254883" />
                  <Point X="-27.979837890625" Y="-1.487127197266" />
                  <Point X="-27.96708203125" Y="-1.458499023438" />
                  <Point X="-27.961892578125" Y="-1.443654907227" />
                  <Point X="-27.95446875" Y="-1.414996582031" />
                  <Point X="-27.951568359375" Y="-1.398881225586" />
                  <Point X="-27.94875390625" Y="-1.36844519043" />
                  <Point X="-27.948578125" Y="-1.353112915039" />
                  <Point X="-27.950779296875" Y="-1.32143371582" />
                  <Point X="-27.953072265625" Y="-1.306274902344" />
                  <Point X="-27.960068359375" Y="-1.276520141602" />
                  <Point X="-27.97176171875" Y="-1.248275390625" />
                  <Point X="-27.987845703125" Y="-1.222282592773" />
                  <Point X="-27.996939453125" Y="-1.209938964844" />
                  <Point X="-28.017775390625" Y="-1.185974487305" />
                  <Point X="-28.028736328125" Y="-1.175253417969" />
                  <Point X="-28.052240234375" Y="-1.155714355469" />
                  <Point X="-28.064783203125" Y="-1.146896362305" />
                  <Point X="-28.09033984375" Y="-1.131854370117" />
                  <Point X="-28.10548828125" Y="-1.124453125" />
                  <Point X="-28.13473828125" Y="-1.113241088867" />
                  <Point X="-28.149828125" Y="-1.108849243164" />
                  <Point X="-28.181703125" Y="-1.102374023438" />
                  <Point X="-28.197314453125" Y="-1.100529907227" />
                  <Point X="-28.228626953125" Y="-1.099441162109" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-28.301189453125" Y="-1.107682617188" />
                  <Point X="-29.66091796875" Y="-1.286694335938" />
                  <Point X="-29.7392265625" Y="-0.980121398926" />
                  <Point X="-29.740763671875" Y="-0.974101135254" />
                  <Point X="-29.786451171875" Y="-0.654654846191" />
                  <Point X="-28.5082890625" Y="-0.312171875" />
                  <Point X="-28.500478515625" Y="-0.30971395874" />
                  <Point X="-28.473931640625" Y="-0.299251647949" />
                  <Point X="-28.444166015625" Y="-0.283893096924" />
                  <Point X="-28.43355859375" Y="-0.277513153076" />
                  <Point X="-28.406775390625" Y="-0.258923858643" />
                  <Point X="-28.393705078125" Y="-0.248268936157" />
                  <Point X="-28.37126953125" Y="-0.226410964966" />
                  <Point X="-28.360958984375" Y="-0.214552047729" />
                  <Point X="-28.341689453125" Y="-0.188280273438" />
                  <Point X="-28.3334609375" Y="-0.174853530884" />
                  <Point X="-28.319333984375" Y="-0.146826370239" />
                  <Point X="-28.313435546875" Y="-0.132225814819" />
                  <Point X="-28.3045078125" Y="-0.103460144043" />
                  <Point X="-28.300896484375" Y="-0.088130729675" />
                  <Point X="-28.29669140625" Y="-0.060150688171" />
                  <Point X="-28.29563671875" Y="-0.046000450134" />
                  <Point X="-28.295646484375" Y="-0.016396848679" />
                  <Point X="-28.29672265625" Y="-0.002171406984" />
                  <Point X="-28.3009921875" Y="0.025956518173" />
                  <Point X="-28.304185546875" Y="0.039859149933" />
                  <Point X="-28.31311328125" Y="0.068624816895" />
                  <Point X="-28.319349609375" Y="0.08431463623" />
                  <Point X="-28.33341015625" Y="0.112204322815" />
                  <Point X="-28.34159375" Y="0.125565368652" />
                  <Point X="-28.36084765625" Y="0.151857955933" />
                  <Point X="-28.37117578125" Y="0.163750610352" />
                  <Point X="-28.393654296875" Y="0.185666976929" />
                  <Point X="-28.4058046875" Y="0.195690704346" />
                  <Point X="-28.432587890625" Y="0.214280014038" />
                  <Point X="-28.43905859375" Y="0.218394485474" />
                  <Point X="-28.462958984375" Y="0.231407318115" />
                  <Point X="-28.495828125" Y="0.245327041626" />
                  <Point X="-28.508287109375" Y="0.249611099243" />
                  <Point X="-28.560119140625" Y="0.263499603271" />
                  <Point X="-29.7854453125" Y="0.591824768066" />
                  <Point X="-29.73231640625" Y="0.950867614746" />
                  <Point X="-29.73133203125" Y="0.957524658203" />
                  <Point X="-29.6335859375" Y="1.318237182617" />
                  <Point X="-28.778064453125" Y="1.20560559082" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.7470546875" Y="1.204364257812" />
                  <Point X="-28.736701171875" Y="1.204703369141" />
                  <Point X="-28.715140625" Y="1.20658984375" />
                  <Point X="-28.70488671875" Y="1.208053833008" />
                  <Point X="-28.684599609375" Y="1.212089233398" />
                  <Point X="-28.67456640625" Y="1.214660888672" />
                  <Point X="-28.61529296875" Y="1.233350097656" />
                  <Point X="-28.60348046875" Y="1.237665161133" />
                  <Point X="-28.5845390625" Y="1.245996704102" />
                  <Point X="-28.575294921875" Y="1.250680175781" />
                  <Point X="-28.556546875" Y="1.261502441406" />
                  <Point X="-28.54787109375" Y="1.267163085938" />
                  <Point X="-28.531189453125" Y="1.279393310547" />
                  <Point X="-28.51594140625" Y="1.293363525391" />
                  <Point X="-28.50230078125" Y="1.308914550781" />
                  <Point X="-28.49590234375" Y="1.317064575195" />
                  <Point X="-28.483484375" Y="1.334796875" />
                  <Point X="-28.47801171875" Y="1.343597167969" />
                  <Point X="-28.46805859375" Y="1.361739013672" />
                  <Point X="-28.463578125" Y="1.371080322266" />
                  <Point X="-28.43979296875" Y="1.428505981445" />
                  <Point X="-28.435490234375" Y="1.440375244141" />
                  <Point X="-28.4297109375" Y="1.460211914062" />
                  <Point X="-28.427361328125" Y="1.470285888672" />
                  <Point X="-28.4236015625" Y="1.49160546875" />
                  <Point X="-28.422361328125" Y="1.501887329102" />
                  <Point X="-28.4210078125" Y="1.522525634766" />
                  <Point X="-28.42191015625" Y="1.543195068359" />
                  <Point X="-28.425056640625" Y="1.563637084961" />
                  <Point X="-28.4271875" Y="1.573766479492" />
                  <Point X="-28.4327890625" Y="1.594677368164" />
                  <Point X="-28.436009765625" Y="1.604521728516" />
                  <Point X="-28.4435078125" Y="1.623806518555" />
                  <Point X="-28.44778515625" Y="1.633246948242" />
                  <Point X="-28.476486328125" Y="1.688381103516" />
                  <Point X="-28.4828046875" Y="1.699294189453" />
                  <Point X="-28.49430078125" Y="1.716497192383" />
                  <Point X="-28.500515625" Y="1.724782470703" />
                  <Point X="-28.514427734375" Y="1.741360351562" />
                  <Point X="-28.52150390625" Y="1.748915039062" />
                  <Point X="-28.536439453125" Y="1.763213134766" />
                  <Point X="-28.54430078125" Y="1.76995703125" />
                  <Point X="-28.57403125" Y="1.792770874023" />
                  <Point X="-29.22761328125" Y="2.294281494141" />
                  <Point X="-29.006119140625" Y="2.673752441406" />
                  <Point X="-29.00228515625" Y="2.680321777344" />
                  <Point X="-28.726337890625" Y="3.035012695312" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.24491796875" Y="2.757716552734" />
                  <Point X="-28.225984375" Y="2.749385498047" />
                  <Point X="-28.2162890625" Y="2.745736328125" />
                  <Point X="-28.195646484375" Y="2.739228027344" />
                  <Point X="-28.185611328125" Y="2.736656494141" />
                  <Point X="-28.16532421875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731158203125" />
                  <Point X="-28.07251171875" Y="2.723935058594" />
                  <Point X="-28.059171875" Y="2.723334716797" />
                  <Point X="-28.038494140625" Y="2.723785888672" />
                  <Point X="-28.02816796875" Y="2.724575927734" />
                  <Point X="-28.006708984375" Y="2.727400878906" />
                  <Point X="-27.996529296875" Y="2.729310302734" />
                  <Point X="-27.976435546875" Y="2.734226806641" />
                  <Point X="-27.95699609375" Y="2.741302246094" />
                  <Point X="-27.938443359375" Y="2.750451660156" />
                  <Point X="-27.929416015625" Y="2.755532714844" />
                  <Point X="-27.911162109375" Y="2.767162109375" />
                  <Point X="-27.9027421875" Y="2.773196044922" />
                  <Point X="-27.88661328125" Y="2.786141601562" />
                  <Point X="-27.878904296875" Y="2.793053222656" />
                  <Point X="-27.82030078125" Y="2.851655517578" />
                  <Point X="-27.811263671875" Y="2.861489990234" />
                  <Point X="-27.7983125" Y="2.877626953125" />
                  <Point X="-27.79227734375" Y="2.886050537109" />
                  <Point X="-27.7806484375" Y="2.904306640625" />
                  <Point X="-27.7755703125" Y="2.913329589844" />
                  <Point X="-27.766421875" Y="2.931881591797" />
                  <Point X="-27.759345703125" Y="2.951327880859" />
                  <Point X="-27.754431640625" Y="2.971419921875" />
                  <Point X="-27.7525234375" Y="2.981594726562" />
                  <Point X="-27.74969921875" Y="3.003055908203" />
                  <Point X="-27.74891015625" Y="3.013377929688" />
                  <Point X="-27.748458984375" Y="3.034052001953" />
                  <Point X="-27.748796875" Y="3.044404052734" />
                  <Point X="-27.75601953125" Y="3.126965087891" />
                  <Point X="-27.75774609375" Y="3.140212646484" />
                  <Point X="-27.761783203125" Y="3.160503173828" />
                  <Point X="-27.76435546875" Y="3.170534667969" />
                  <Point X="-27.77086328125" Y="3.191172607422" />
                  <Point X="-27.774509765625" Y="3.200861572266" />
                  <Point X="-27.78283984375" Y="3.219794433594" />
                  <Point X="-27.7875234375" Y="3.229038330078" />
                  <Point X="-27.80069921875" Y="3.251857666016" />
                  <Point X="-28.059384765625" Y="3.699915771484" />
                  <Point X="-27.655029296875" Y="4.009930664062" />
                  <Point X="-27.648357421875" Y="4.015046875" />
                  <Point X="-27.1925234375" Y="4.268296875" />
                  <Point X="-27.118564453125" Y="4.171909179688" />
                  <Point X="-27.1118203125" Y="4.164048828125" />
                  <Point X="-27.097517578125" Y="4.149107421875" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.06508984375" Y="4.121895996094" />
                  <Point X="-27.047888671875" Y="4.110403320312" />
                  <Point X="-27.0389765625" Y="4.105128417969" />
                  <Point X="-26.9470859375" Y="4.057293457031" />
                  <Point X="-26.934333984375" Y="4.051290527344" />
                  <Point X="-26.9150625" Y="4.043795410156" />
                  <Point X="-26.905220703125" Y="4.040573730469" />
                  <Point X="-26.8843125" Y="4.034969726562" />
                  <Point X="-26.87417578125" Y="4.032837402344" />
                  <Point X="-26.85373046875" Y="4.029689453125" />
                  <Point X="-26.833064453125" Y="4.028786132812" />
                  <Point X="-26.812423828125" Y="4.030138183594" />
                  <Point X="-26.802140625" Y="4.031377685547" />
                  <Point X="-26.78082421875" Y="4.035135498047" />
                  <Point X="-26.770736328125" Y="4.037486816406" />
                  <Point X="-26.750873046875" Y="4.043275390625" />
                  <Point X="-26.74109765625" Y="4.046713134766" />
                  <Point X="-26.645388671875" Y="4.086357666016" />
                  <Point X="-26.632583984375" Y="4.092272460938" />
                  <Point X="-26.614451171875" Y="4.102220703125" />
                  <Point X="-26.605658203125" Y="4.107688964844" />
                  <Point X="-26.587927734375" Y="4.120103515625" />
                  <Point X="-26.579779296875" Y="4.126498535156" />
                  <Point X="-26.5642265625" Y="4.140137695312" />
                  <Point X="-26.550251953125" Y="4.15538671875" />
                  <Point X="-26.53801953125" Y="4.172068847656" />
                  <Point X="-26.532357421875" Y="4.18074609375" />
                  <Point X="-26.52153515625" Y="4.199491210938" />
                  <Point X="-26.5168515625" Y="4.208732910156" />
                  <Point X="-26.508521484375" Y="4.227666503906" />
                  <Point X="-26.504875" Y="4.237358398438" />
                  <Point X="-26.47378125" Y="4.335979980469" />
                  <Point X="-26.4700625" Y="4.349625488281" />
                  <Point X="-26.46600390625" Y="4.369979980469" />
                  <Point X="-26.464533203125" Y="4.380264648438" />
                  <Point X="-26.462642578125" Y="4.401842773438" />
                  <Point X="-26.46230078125" Y="4.412196777344" />
                  <Point X="-26.46275" Y="4.432890136719" />
                  <Point X="-26.4650390625" Y="4.454606933594" />
                  <Point X="-26.479263671875" Y="4.562654785156" />
                  <Point X="-25.939794921875" Y="4.713903320312" />
                  <Point X="-25.931171875" Y="4.716320800781" />
                  <Point X="-25.36522265625" Y="4.782556152344" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.76725390625" Y="4.2471015625" />
                  <Point X="-24.7620234375" Y="4.26171875" />
                  <Point X="-24.755271484375" Y="4.286912597656" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.1796640625" Y="4.738701660156" />
                  <Point X="-24.172123046875" Y="4.737912109375" />
                  <Point X="-23.556283203125" Y="4.589229492188" />
                  <Point X="-23.54640234375" Y="4.58684375" />
                  <Point X="-23.146572265625" Y="4.441822753906" />
                  <Point X="-23.141748046875" Y="4.440072753906" />
                  <Point X="-22.7541328125" Y="4.258798828125" />
                  <Point X="-22.749546875" Y="4.256654296875" />
                  <Point X="-22.3750546875" Y="4.038473632812" />
                  <Point X="-22.37058203125" Y="4.035867431641" />
                  <Point X="-22.18221875" Y="3.901915283203" />
                  <Point X="-22.934685546875" Y="2.598602539062" />
                  <Point X="-22.9376171875" Y="2.593119384766" />
                  <Point X="-22.94681640625" Y="2.573442382812" />
                  <Point X="-22.95581640625" Y="2.549566650391" />
                  <Point X="-22.958697265625" Y="2.540601074219" />
                  <Point X="-22.97941796875" Y="2.463119384766" />
                  <Point X="-22.981130859375" Y="2.455455078125" />
                  <Point X="-22.98499609375" Y="2.432223388672" />
                  <Point X="-22.987083984375" Y="2.410515380859" />
                  <Point X="-22.98751171875" Y="2.400269042969" />
                  <Point X="-22.987263671875" Y="2.379799804688" />
                  <Point X="-22.986587890625" Y="2.369575683594" />
                  <Point X="-22.978517578125" Y="2.302662109375" />
                  <Point X="-22.976220703125" Y="2.289157958984" />
                  <Point X="-22.970880859375" Y="2.267205810547" />
                  <Point X="-22.967556640625" Y="2.256389404297" />
                  <Point X="-22.959287109375" Y="2.234271972656" />
                  <Point X="-22.954697265625" Y="2.223925048828" />
                  <Point X="-22.94432421875" Y="2.203854248047" />
                  <Point X="-22.938541015625" Y="2.194130371094" />
                  <Point X="-22.897083984375" Y="2.133033203125" />
                  <Point X="-22.89239453125" Y="2.126704589844" />
                  <Point X="-22.87728515625" Y="2.108506347656" />
                  <Point X="-22.86249609375" Y="2.093013183594" />
                  <Point X="-22.85515234375" Y="2.086095947266" />
                  <Point X="-22.839775390625" Y="2.073080810547" />
                  <Point X="-22.8317421875" Y="2.066982910156" />
                  <Point X="-22.770646484375" Y="2.025525756836" />
                  <Point X="-22.75866796875" Y="2.018213378906" />
                  <Point X="-22.738630859375" Y="2.007860107422" />
                  <Point X="-22.728306640625" Y="2.003281494141" />
                  <Point X="-22.706236328125" Y="1.995025268555" />
                  <Point X="-22.69544140625" Y="1.991703369141" />
                  <Point X="-22.673529296875" Y="1.986364257812" />
                  <Point X="-22.662412109375" Y="1.984347167969" />
                  <Point X="-22.595412109375" Y="1.976267944336" />
                  <Point X="-22.5874140625" Y="1.97564465332" />
                  <Point X="-22.563373046875" Y="1.975129272461" />
                  <Point X="-22.54204296875" Y="1.976173950195" />
                  <Point X="-22.53201171875" Y="1.977200927734" />
                  <Point X="-22.5121171875" Y="1.980311889648" />
                  <Point X="-22.50225390625" Y="1.982395874023" />
                  <Point X="-22.424771484375" Y="2.003115600586" />
                  <Point X="-22.402119140625" Y="2.010763549805" />
                  <Point X="-22.373962890625" Y="2.022860595703" />
                  <Point X="-22.36396484375" Y="2.027873413086" />
                  <Point X="-22.31183203125" Y="2.057972900391" />
                  <Point X="-21.05959375" Y="2.780951660156" />
                  <Point X="-20.9587109375" Y="2.640745605469" />
                  <Point X="-20.956046875" Y="2.637044189453" />
                  <Point X="-20.863115234375" Y="2.483471679688" />
                  <Point X="-21.82705078125" Y="1.743817382812" />
                  <Point X="-21.831869140625" Y="1.73986315918" />
                  <Point X="-21.84787109375" Y="1.725225952148" />
                  <Point X="-21.865328125" Y="1.706608642578" />
                  <Point X="-21.871423828125" Y="1.699422241211" />
                  <Point X="-21.9271875" Y="1.626674560547" />
                  <Point X="-21.931630859375" Y="1.620364746094" />
                  <Point X="-21.943892578125" Y="1.600772460938" />
                  <Point X="-21.95437890625" Y="1.581188110352" />
                  <Point X="-21.95877734375" Y="1.571768310547" />
                  <Point X="-21.966515625" Y="1.552512329102" />
                  <Point X="-21.96985546875" Y="1.542676269531" />
                  <Point X="-21.99062890625" Y="1.468400390625" />
                  <Point X="-21.993771484375" Y="1.454670043945" />
                  <Point X="-21.9972265625" Y="1.432370849609" />
                  <Point X="-21.998291015625" Y="1.421113525391" />
                  <Point X="-21.999107421875" Y="1.39754284668" />
                  <Point X="-21.998826171875" Y="1.386238037109" />
                  <Point X="-21.996921875" Y="1.363750976562" />
                  <Point X="-21.995298828125" Y="1.352568847656" />
                  <Point X="-21.97824609375" Y="1.269927490234" />
                  <Point X="-21.976376953125" Y="1.262421264648" />
                  <Point X="-21.9695546875" Y="1.240255126953" />
                  <Point X="-21.96173828125" Y="1.219885620117" />
                  <Point X="-21.957564453125" Y="1.210545898438" />
                  <Point X="-21.948234375" Y="1.192365112305" />
                  <Point X="-21.943078125" Y="1.183524414062" />
                  <Point X="-21.8968125" Y="1.113203125" />
                  <Point X="-21.8883515625" Y="1.101549316406" />
                  <Point X="-21.873755859375" Y="1.084236450195" />
                  <Point X="-21.86594140625" Y="1.076017089844" />
                  <Point X="-21.8486875" Y="1.059914794922" />
                  <Point X="-21.83997265625" Y="1.052703613281" />
                  <Point X="-21.82175390625" Y="1.039370605469" />
                  <Point X="-21.81225" Y="1.033249023438" />
                  <Point X="-21.745048828125" Y="0.995421203613" />
                  <Point X="-21.738017578125" Y="0.991838928223" />
                  <Point X="-21.716373046875" Y="0.982283630371" />
                  <Point X="-21.695953125" Y="0.974910888672" />
                  <Point X="-21.68617578125" Y="0.971964294434" />
                  <Point X="-21.666361328125" Y="0.967137329102" />
                  <Point X="-21.65632421875" Y="0.965257141113" />
                  <Point X="-21.565453125" Y="0.953247192383" />
                  <Point X="-21.542060546875" Y="0.951648742676" />
                  <Point X="-21.5107578125" Y="0.95199017334" />
                  <Point X="-21.49939453125" Y="0.952797180176" />
                  <Point X="-21.449873046875" Y="0.959316955566" />
                  <Point X="-20.295296875" Y="1.111319824219" />
                  <Point X="-20.2484609375" Y="0.91893359375" />
                  <Point X="-20.247318359375" Y="0.914245239258" />
                  <Point X="-20.21612890625" Y="0.713920532227" />
                  <Point X="-21.308005859375" Y="0.42135333252" />
                  <Point X="-21.313966796875" Y="0.419544586182" />
                  <Point X="-21.334376953125" Y="0.412135894775" />
                  <Point X="-21.357619140625" Y="0.401618133545" />
                  <Point X="-21.365994140625" Y="0.397316101074" />
                  <Point X="-21.455267578125" Y="0.345714233398" />
                  <Point X="-21.461662109375" Y="0.341670959473" />
                  <Point X="-21.480162109375" Y="0.32854574585" />
                  <Point X="-21.4977109375" Y="0.31425491333" />
                  <Point X="-21.5055" Y="0.307157287598" />
                  <Point X="-21.52024609375" Y="0.292142669678" />
                  <Point X="-21.527203125" Y="0.284225830078" />
                  <Point X="-21.58076953125" Y="0.215968811035" />
                  <Point X="-21.589138671875" Y="0.204217651367" />
                  <Point X="-21.600859375" Y="0.184945861816" />
                  <Point X="-21.606146484375" Y="0.174959594727" />
                  <Point X="-21.615923828125" Y="0.153493545532" />
                  <Point X="-21.619990234375" Y="0.142941467285" />
                  <Point X="-21.62683984375" Y="0.121432014465" />
                  <Point X="-21.629623046875" Y="0.110474494934" />
                  <Point X="-21.647478515625" Y="0.017238891602" />
                  <Point X="-21.648609375" Y="0.009714858055" />
                  <Point X="-21.650787109375" Y="-0.012964847565" />
                  <Point X="-21.65143359375" Y="-0.035153354645" />
                  <Point X="-21.651169921875" Y="-0.04551177597" />
                  <Point X="-21.649515625" Y="-0.066147026062" />
                  <Point X="-21.648125" Y="-0.07642414856" />
                  <Point X="-21.63026953125" Y="-0.169659606934" />
                  <Point X="-21.62683984375" Y="-0.183992050171" />
                  <Point X="-21.6199921875" Y="-0.205495407104" />
                  <Point X="-21.615927734375" Y="-0.216041549683" />
                  <Point X="-21.606154296875" Y="-0.237502990723" />
                  <Point X="-21.6008671875" Y="-0.24749029541" />
                  <Point X="-21.58914453125" Y="-0.266768035889" />
                  <Point X="-21.582708984375" Y="-0.276058441162" />
                  <Point X="-21.529142578125" Y="-0.344315155029" />
                  <Point X="-21.524099609375" Y="-0.350227081299" />
                  <Point X="-21.508033203125" Y="-0.3671015625" />
                  <Point X="-21.49178125" Y="-0.381992980957" />
                  <Point X="-21.48379296875" Y="-0.388549346924" />
                  <Point X="-21.467154296875" Y="-0.400755279541" />
                  <Point X="-21.45850390625" Y="-0.406404754639" />
                  <Point X="-21.369232421875" Y="-0.458004821777" />
                  <Point X="-21.3486640625" Y="-0.46824911499" />
                  <Point X="-21.31901171875" Y="-0.480230926514" />
                  <Point X="-21.3080078125" Y="-0.48391293335" />
                  <Point X="-21.262591796875" Y="-0.496081878662" />
                  <Point X="-20.215119140625" Y="-0.776751281738" />
                  <Point X="-20.2377421875" Y="-0.926796020508" />
                  <Point X="-20.2383828125" Y="-0.931041503906" />
                  <Point X="-20.272197265625" Y="-1.079219726563" />
                  <Point X="-21.563216796875" Y="-0.909253845215" />
                  <Point X="-21.571375" Y="-0.908535827637" />
                  <Point X="-21.59990234375" Y="-0.908042541504" />
                  <Point X="-21.633279296875" Y="-0.910841003418" />
                  <Point X="-21.64551953125" Y="-0.912676330566" />
                  <Point X="-21.8207421875" Y="-0.950761535645" />
                  <Point X="-21.842126953125" Y="-0.956742858887" />
                  <Point X="-21.87124609375" Y="-0.968366943359" />
                  <Point X="-21.885322265625" Y="-0.97538848877" />
                  <Point X="-21.913146484375" Y="-0.992278869629" />
                  <Point X="-21.92587109375" Y="-1.001526672363" />
                  <Point X="-21.949623046875" Y="-1.02199798584" />
                  <Point X="-21.960650390625" Y="-1.033221557617" />
                  <Point X="-22.066560546875" Y="-1.160598876953" />
                  <Point X="-22.078673828125" Y="-1.176846557617" />
                  <Point X="-22.09339453125" Y="-1.201229370117" />
                  <Point X="-22.0998359375" Y="-1.213975219727" />
                  <Point X="-22.1111796875" Y="-1.241362060547" />
                  <Point X="-22.115638671875" Y="-1.25493347168" />
                  <Point X="-22.12246875" Y="-1.282582519531" />
                  <Point X="-22.12483984375" Y="-1.296660644531" />
                  <Point X="-22.140013671875" Y="-1.4615625" />
                  <Point X="-22.1407109375" Y="-1.483313598633" />
                  <Point X="-22.138390625" Y="-1.514602539062" />
                  <Point X="-22.1359296875" Y="-1.530147216797" />
                  <Point X="-22.128197265625" Y="-1.561761230469" />
                  <Point X="-22.1232109375" Y="-1.576675292969" />
                  <Point X="-22.110841796875" Y="-1.605479858398" />
                  <Point X="-22.103458984375" Y="-1.619370239258" />
                  <Point X="-22.00648828125" Y="-1.770200683594" />
                  <Point X="-21.992408203125" Y="-1.789351806641" />
                  <Point X="-21.97727734375" Y="-1.807441650391" />
                  <Point X="-21.97015625" Y="-1.815064208984" />
                  <Point X="-21.955119140625" Y="-1.829481689453" />
                  <Point X="-21.947205078125" Y="-1.836276367188" />
                  <Point X="-21.905060546875" Y="-1.868615844727" />
                  <Point X="-20.912830078125" Y="-2.629981689453" />
                  <Point X="-20.952708984375" Y="-2.694512695312" />
                  <Point X="-20.95451953125" Y="-2.697442626953" />
                  <Point X="-20.998724609375" Y="-2.760252197266" />
                  <Point X="-22.151546875" Y="-2.094669433594" />
                  <Point X="-22.158810546875" Y="-2.090883544922" />
                  <Point X="-22.184970703125" Y="-2.079513916016" />
                  <Point X="-22.216876953125" Y="-2.069326416016" />
                  <Point X="-22.228888671875" Y="-2.066337646484" />
                  <Point X="-22.4374296875" Y="-2.028675170898" />
                  <Point X="-22.460638671875" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539857421875" Y="-2.0314609375" />
                  <Point X="-22.555154296875" Y="-2.035136352539" />
                  <Point X="-22.5849296875" Y="-2.044960327148" />
                  <Point X="-22.599408203125" Y="-2.051108886719" />
                  <Point X="-22.772654296875" Y="-2.142287353516" />
                  <Point X="-22.7910234375" Y="-2.153167236328" />
                  <Point X="-22.813951171875" Y="-2.170056152344" />
                  <Point X="-22.82478125" Y="-2.179365722656" />
                  <Point X="-22.8457421875" Y="-2.20032421875" />
                  <Point X="-22.8550546875" Y="-2.211154296875" />
                  <Point X="-22.871951171875" Y="-2.234088378906" />
                  <Point X="-22.87953515625" Y="-2.246192382812" />
                  <Point X="-22.97071484375" Y="-2.419439208984" />
                  <Point X="-22.9801640625" Y="-2.440192382812" />
                  <Point X="-22.98998828125" Y="-2.469971435547" />
                  <Point X="-22.9936640625" Y="-2.485268554688" />
                  <Point X="-22.99862109375" Y="-2.517442871094" />
                  <Point X="-22.999720703125" Y="-2.533133789062" />
                  <Point X="-22.99931640625" Y="-2.564484130859" />
                  <Point X="-22.9978125" Y="-2.580143554688" />
                  <Point X="-22.9601484375" Y="-2.788685302734" />
                  <Point X="-22.95480078125" Y="-2.810686767578" />
                  <Point X="-22.94753515625" Y="-2.834349121094" />
                  <Point X="-22.9438203125" Y="-2.844392578125" />
                  <Point X="-22.935279296875" Y="-2.864006347656" />
                  <Point X="-22.930453125" Y="-2.873576660156" />
                  <Point X="-22.90337109375" Y="-2.920485351562" />
                  <Point X="-22.26410546875" Y="-4.027725097656" />
                  <Point X="-22.276244140625" Y="-4.036083740234" />
                  <Point X="-23.16608203125" Y="-2.876423828125" />
                  <Point X="-23.171345703125" Y="-2.870145751953" />
                  <Point X="-23.19116796875" Y="-2.849625732422" />
                  <Point X="-23.21674609375" Y="-2.828004638672" />
                  <Point X="-23.22669921875" Y="-2.820646972656" />
                  <Point X="-23.43237890625" Y="-2.688414794922" />
                  <Point X="-23.453716796875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663873535156" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.816548828125" Y="-2.667216064453" />
                  <Point X="-23.8387734375" Y="-2.670339355469" />
                  <Point X="-23.866423828125" Y="-2.677171386719" />
                  <Point X="-23.8799921875" Y="-2.681629394531" />
                  <Point X="-23.907376953125" Y="-2.69297265625" />
                  <Point X="-23.920123046875" Y="-2.699414306641" />
                  <Point X="-23.94450390625" Y="-2.714134521484" />
                  <Point X="-23.956138671875" Y="-2.722413085938" />
                  <Point X="-24.1298359375" Y="-2.8668359375" />
                  <Point X="-24.147345703125" Y="-2.883087890625" />
                  <Point X="-24.16781640625" Y="-2.906837646484" />
                  <Point X="-24.177064453125" Y="-2.919562988281" />
                  <Point X="-24.19395703125" Y="-2.947389648438" />
                  <Point X="-24.20098046875" Y="-2.961467041016" />
                  <Point X="-24.21260546875" Y="-2.990588134766" />
                  <Point X="-24.21720703125" Y="-3.005631835938" />
                  <Point X="-24.269140625" Y="-3.244570556641" />
                  <Point X="-24.2725234375" Y="-3.265916259766" />
                  <Point X="-24.2751171875" Y="-3.291711425781" />
                  <Point X="-24.27558203125" Y="-3.302672607422" />
                  <Point X="-24.27524609375" Y="-3.324578125" />
                  <Point X="-24.2744453125" Y="-3.335522460938" />
                  <Point X="-24.26676953125" Y="-3.393820068359" />
                  <Point X="-24.166912109375" Y="-4.152311523437" />
                  <Point X="-24.34493359375" Y="-3.4879296875" />
                  <Point X="-24.347396484375" Y="-3.480109863281" />
                  <Point X="-24.357861328125" Y="-3.453562255859" />
                  <Point X="-24.37321875" Y="-3.423804931641" />
                  <Point X="-24.37959375" Y="-3.413206298828" />
                  <Point X="-24.5376015625" Y="-3.185546630859" />
                  <Point X="-24.553333984375" Y="-3.165166748047" />
                  <Point X="-24.57522265625" Y="-3.142708007813" />
                  <Point X="-24.587099609375" Y="-3.132388427734" />
                  <Point X="-24.613353515625" Y="-3.113147949219" />
                  <Point X="-24.626765625" Y="-3.104933105469" />
                  <Point X="-24.65476171875" Y="-3.090828125" />
                  <Point X="-24.669345703125" Y="-3.084937988281" />
                  <Point X="-24.913853515625" Y="-3.009051513672" />
                  <Point X="-24.936625" Y="-3.003108642578" />
                  <Point X="-24.96478515625" Y="-2.998839355469" />
                  <Point X="-24.979025390625" Y="-2.997766113281" />
                  <Point X="-25.0086640625" Y="-2.997766357422" />
                  <Point X="-25.022904296875" Y="-2.998839599609" />
                  <Point X="-25.051064453125" Y="-3.003108886719" />
                  <Point X="-25.064984375" Y="-3.006304931641" />
                  <Point X="-25.3094921875" Y="-3.082191162109" />
                  <Point X="-25.332931640625" Y="-3.090830810547" />
                  <Point X="-25.36093359375" Y="-3.104940673828" />
                  <Point X="-25.37434765625" Y="-3.113157958984" />
                  <Point X="-25.40060546875" Y="-3.132404785156" />
                  <Point X="-25.412478515625" Y="-3.142721191406" />
                  <Point X="-25.434361328125" Y="-3.165175048828" />
                  <Point X="-25.44437109375" Y="-3.1773125" />
                  <Point X="-25.60237890625" Y="-3.404972167969" />
                  <Point X="-25.61323046875" Y="-3.42258203125" />
                  <Point X="-25.62616796875" Y="-3.446388183594" />
                  <Point X="-25.6309296875" Y="-3.456534912109" />
                  <Point X="-25.639220703125" Y="-3.477308105469" />
                  <Point X="-25.64275" Y="-3.487934570312" />
                  <Point X="-25.656744140625" Y="-3.540158935547" />
                  <Point X="-25.985423828125" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.150217175013" Y="-2.495437215166" />
                  <Point X="-28.708651348597" Y="-2.96895859083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.951577044788" Y="-3.780821385399" />
                  <Point X="-27.722019177728" Y="-4.026992059174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.115749958974" Y="-2.393102256531" />
                  <Point X="-28.62421480657" Y="-2.920209173847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.901906683903" Y="-3.694789803592" />
                  <Point X="-27.486309949437" Y="-4.140462737622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.040032850353" Y="-2.335002391995" />
                  <Point X="-28.539778264542" Y="-2.871459756864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.852236323018" Y="-3.608758221784" />
                  <Point X="-27.427672974049" Y="-4.064046672642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.964315741731" Y="-2.276902527459" />
                  <Point X="-28.455341722515" Y="-2.822710339881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.802565962133" Y="-3.522726639977" />
                  <Point X="-27.36226428595" Y="-3.994892380487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.888598633109" Y="-2.218802662923" />
                  <Point X="-28.370905180487" Y="-2.773960922899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.752895601249" Y="-3.43669505817" />
                  <Point X="-27.282689160401" Y="-3.940929732587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.673371259336" Y="-1.237940531437" />
                  <Point X="-29.631517087985" Y="-1.282823635178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.812881524487" Y="-2.160702798387" />
                  <Point X="-28.28646863846" Y="-2.725211505916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.703225240364" Y="-3.350663476363" />
                  <Point X="-27.202658902225" Y="-3.887455154674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.722375075582" Y="-1.046093849588" />
                  <Point X="-29.515824344086" Y="-1.267592391077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.737164415865" Y="-2.102602933851" />
                  <Point X="-28.202032096432" Y="-2.676462088933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.653554879479" Y="-3.264631894555" />
                  <Point X="-27.12262864405" Y="-3.833980576761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.755464484828" Y="-0.871313279843" />
                  <Point X="-29.400131600186" Y="-1.252361146976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.661447307243" Y="-2.044503069315" />
                  <Point X="-28.117595554405" Y="-2.62771267195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.603884518594" Y="-3.178600312748" />
                  <Point X="-27.034608744978" Y="-3.78907383975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.778995830275" Y="-0.706782478645" />
                  <Point X="-29.284438856286" Y="-1.237129902875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.585730198621" Y="-1.986403204779" />
                  <Point X="-28.033159012377" Y="-2.578963254968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.554214157709" Y="-3.092568730941" />
                  <Point X="-26.915104834122" Y="-3.777929571841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.126992475673" Y="-4.623076605026" />
                  <Point X="-25.994628034754" Y="-4.765020089787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.715450300242" Y="-0.635630194079" />
                  <Point X="-29.168746112386" Y="-1.221898658774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.510013089999" Y="-1.928303340242" />
                  <Point X="-27.94872247035" Y="-2.530213837985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.504543796824" Y="-3.006537149134" />
                  <Point X="-26.776752604303" Y="-3.786997651426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.12585825988" Y="-4.484996379917" />
                  <Point X="-25.958112952844" Y="-4.664881198435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.611522355629" Y="-0.607782747343" />
                  <Point X="-29.053053368487" Y="-1.206667414673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.434295981377" Y="-1.870203475706" />
                  <Point X="-27.864285928322" Y="-2.481464421002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.454873435939" Y="-2.920505567327" />
                  <Point X="-26.638400374483" Y="-3.79606573101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.161079749121" Y="-4.307929434298" />
                  <Point X="-25.929119509991" Y="-4.556676336711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.507594411016" Y="-0.579935300607" />
                  <Point X="-28.937360624587" Y="-1.191436170573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.358578872755" Y="-1.81210361117" />
                  <Point X="-27.779849386295" Y="-2.432715004019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.405203075055" Y="-2.834473985519" />
                  <Point X="-26.269682354303" Y="-4.052170876038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.235716215821" Y="-4.088595100147" />
                  <Point X="-25.900126067138" Y="-4.448471474986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.403666466404" Y="-0.552087853871" />
                  <Point X="-28.821667880687" Y="-1.176204926472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.282861764134" Y="-1.754003746634" />
                  <Point X="-27.695412758287" Y="-2.383965679239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.35553271417" Y="-2.748442403712" />
                  <Point X="-25.871132624286" Y="-4.340266613262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.299738521791" Y="-0.524240407135" />
                  <Point X="-28.705975136788" Y="-1.160973682371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.207144655512" Y="-1.695903882098" />
                  <Point X="-27.596476747596" Y="-2.350765038763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.316635461933" Y="-2.650858077281" />
                  <Point X="-25.842139181433" Y="-4.232061751537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.195810577178" Y="-0.496392960399" />
                  <Point X="-28.590282392888" Y="-1.14574243827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.13142754689" Y="-1.637804017562" />
                  <Point X="-27.424802130128" Y="-2.395567004205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.347019299062" Y="-2.478978878418" />
                  <Point X="-25.81314573858" Y="-4.123856889812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.091882632566" Y="-0.468545513663" />
                  <Point X="-28.474589648988" Y="-1.130511194169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.055710438268" Y="-1.579704153026" />
                  <Point X="-25.784152295728" Y="-4.015652028088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.987954687953" Y="-0.440698066927" />
                  <Point X="-28.358896905089" Y="-1.115279950068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.99233953067" Y="-1.508364608824" />
                  <Point X="-25.755158852875" Y="-3.907447166363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.88402674334" Y="-0.412850620191" />
                  <Point X="-28.243120487773" Y="-1.100138434721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.953666616255" Y="-1.410539709532" />
                  <Point X="-25.726165410022" Y="-3.799242304639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.772511724483" Y="0.679229395549" />
                  <Point X="-29.659548064807" Y="0.558090701542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.780098798728" Y="-0.385003173455" />
                  <Point X="-28.075508581285" Y="-1.14058367603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.969108738129" Y="-1.254683538583" />
                  <Point X="-25.697171967169" Y="-3.691037442914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.754722358682" Y="0.799449158928" />
                  <Point X="-29.486384037184" Y="0.511691539253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.676170854115" Y="-0.357155726719" />
                  <Point X="-25.668178524317" Y="-3.582832581189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.73693299288" Y="0.919668922307" />
                  <Point X="-29.31322000956" Y="0.465292376964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.572242909502" Y="-0.329308279983" />
                  <Point X="-25.638460870706" Y="-3.475404340421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.711294007812" Y="1.031470999599" />
                  <Point X="-29.140055981937" Y="0.418893214675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.471530432863" Y="-0.298012666005" />
                  <Point X="-25.590373732249" Y="-3.387674960421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.682046437424" Y="1.139403342907" />
                  <Point X="-28.966891954314" Y="0.372494052386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.390726849228" Y="-0.245367378117" />
                  <Point X="-25.534947362979" Y="-3.307815941901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.652798867036" Y="1.247335686214" />
                  <Point X="-28.793727926691" Y="0.326094890097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.331048720195" Y="-0.170067813729" />
                  <Point X="-25.479520993709" Y="-3.227956923381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.582782428211" Y="1.311548770667" />
                  <Point X="-28.620563899068" Y="0.279695727807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.297657505283" Y="-0.066578985155" />
                  <Point X="-25.420976856564" Y="-3.151441301574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.434707451155" Y="1.292054321169" />
                  <Point X="-28.42430709767" Y="0.208532597494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.332151601998" Y="0.109707927479" />
                  <Point X="-25.342911549063" Y="-3.095859572041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.286632474098" Y="1.272559871672" />
                  <Point X="-25.244555352303" Y="-3.062037157248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.138557497041" Y="1.253065422174" />
                  <Point X="-25.143815273736" Y="-3.030771142713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.194712621242" Y="-4.048559129849" />
                  <Point X="-24.178246730868" Y="-4.066216635468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.990482519984" Y="1.233570972677" />
                  <Point X="-25.04112047707" Y="-3.001601306704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.247086369098" Y="-3.853098638785" />
                  <Point X="-24.199600172524" Y="-3.90402135015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.842407542928" Y="1.214076523179" />
                  <Point X="-24.900376400516" Y="-3.013234327886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.299460116955" Y="-3.657638147721" />
                  <Point X="-24.220953614179" Y="-3.741826064831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.706659198919" Y="1.207800769262" />
                  <Point X="-24.717573644052" Y="-3.069969761388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.356392032334" Y="-3.457289620431" />
                  <Point X="-24.242307055835" Y="-3.579630779513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.604324553382" Y="1.237356820073" />
                  <Point X="-24.26366049749" Y="-3.417435494194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.521719190754" Y="1.288069936546" />
                  <Point X="-24.272770169879" Y="-3.26837004393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.465498307346" Y="1.367076942965" />
                  <Point X="-24.249468404819" Y="-3.154061605034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.217073285995" Y="2.312338955942" />
                  <Point X="-29.131363668509" Y="2.220426644001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.42825861073" Y="1.466438780179" />
                  <Point X="-24.224915058992" Y="-3.041095322188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.167067375217" Y="2.398010704542" />
                  <Point X="-28.674717395822" Y="1.870029992258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.442361879212" Y="1.620859206644" />
                  <Point X="-24.189400388354" Y="-2.939883621093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.117061464438" Y="2.483682453142" />
                  <Point X="-24.128589091105" Y="-2.865799230842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.067055553659" Y="2.569354201743" />
                  <Point X="-24.055422686185" Y="-2.804964071468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.01704964288" Y="2.655025950343" />
                  <Point X="-23.982256281265" Y="-2.744128912093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.960648550127" Y="2.733839705899" />
                  <Point X="-23.902101769086" Y="-2.690787580285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.901567679959" Y="2.809779752005" />
                  <Point X="-23.795953754256" Y="-2.665320867384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.84248680979" Y="2.885719798112" />
                  <Point X="-23.676323343425" Y="-2.654312254091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.783405939622" Y="2.961659844218" />
                  <Point X="-23.55218940673" Y="-2.64813308102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.361434764558" Y="-3.925061100601" />
                  <Point X="-22.265075899443" Y="-4.028393332484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.716751010034" Y="3.029477705994" />
                  <Point X="-23.313400241994" Y="-2.76490658694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.964822673097" Y="-3.138710264842" />
                  <Point X="-22.472672426218" Y="-3.666476790226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.435354303983" Y="2.867013205957" />
                  <Point X="-22.683829870178" Y="-3.300741631797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.183508606562" Y="2.736238282924" />
                  <Point X="-22.894987314139" Y="-2.935006473369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.041930532864" Y="2.7237109093" />
                  <Point X="-22.974226567339" Y="-2.710736254996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.937477517004" Y="2.75099528606" />
                  <Point X="-22.999577290581" Y="-2.54425440998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.862291981029" Y="2.80966519247" />
                  <Point X="-22.97575357425" Y="-2.430505695294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.797208353044" Y="2.879168068919" />
                  <Point X="-22.92939284937" Y="-2.340924963393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.754248603648" Y="2.972395900513" />
                  <Point X="-22.882529976493" Y="-2.251882719293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.754728455015" Y="3.112207000741" />
                  <Point X="-22.822270034116" Y="-2.17720707333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.874891028212" Y="3.380362106989" />
                  <Point X="-22.740646030053" Y="-2.125441578638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.049821080277" Y="3.707248143902" />
                  <Point X="-22.653512904385" Y="-2.079583893575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.974077659399" Y="3.765319791998" />
                  <Point X="-22.562733042808" Y="-2.037636853995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.898334238521" Y="3.823391440094" />
                  <Point X="-22.441684025688" Y="-2.028149509698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.822590817643" Y="3.88146308819" />
                  <Point X="-22.285753653113" Y="-2.056067839554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.746847396765" Y="3.939534736286" />
                  <Point X="-22.082904991767" Y="-2.134299874216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.671103975887" Y="3.997606384382" />
                  <Point X="-21.801508102974" Y="-2.29676457022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.588488438598" Y="4.048308589867" />
                  <Point X="-21.520111214182" Y="-2.459229266224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.502922509401" Y="4.095846887387" />
                  <Point X="-21.238714325389" Y="-2.621693962228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.417356580204" Y="4.143385184908" />
                  <Point X="-22.137816621645" Y="-1.518228269975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.574655081475" Y="-2.122145084343" />
                  <Point X="-20.990503449552" Y="-2.748571016327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.331790651007" Y="4.190923482428" />
                  <Point X="-22.132895782048" Y="-1.38420870175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.118006690521" Y="-2.472544007649" />
                  <Point X="-20.936092039029" Y="-2.667623587804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.246224721809" Y="4.238461779949" />
                  <Point X="-22.117295676787" Y="-1.26164124387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.947669888217" Y="4.057597440813" />
                  <Point X="-22.073252656114" Y="-1.169575078499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.794568283797" Y="4.032712593415" />
                  <Point X="-22.012432803862" Y="-1.095499862366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.695385096967" Y="4.065648169933" />
                  <Point X="-21.950369673659" Y="-1.022757898607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.605074064992" Y="4.108097967709" />
                  <Point X="-21.871208095912" Y="-0.968351774983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.536707202489" Y="4.174080006194" />
                  <Point X="-21.768336919621" Y="-0.939371082965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.495006690103" Y="4.268658204155" />
                  <Point X="-21.660331836095" Y="-0.915895832426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.465151738805" Y="4.375939211179" />
                  <Point X="-21.532908608067" Y="-0.913243992457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.474314488182" Y="4.525061579546" />
                  <Point X="-21.384833648361" Y="-0.932738423349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.400156741679" Y="4.584833655226" />
                  <Point X="-21.236758688654" Y="-0.952232854241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.297182705291" Y="4.613704043293" />
                  <Point X="-21.088683728947" Y="-0.971727285133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.194208668902" Y="4.642574431361" />
                  <Point X="-20.94060876924" Y="-0.991221716025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.091234632514" Y="4.671444819429" />
                  <Point X="-21.644080281403" Y="-0.097544355352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.275717686181" Y="-0.492564876412" />
                  <Point X="-20.792533809534" Y="-1.010716146917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.988260596125" Y="4.700315207497" />
                  <Point X="-21.643004857702" Y="0.040598916556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.102553755081" Y="-0.538963935193" />
                  <Point X="-20.644458849827" Y="-1.030210577809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.878985373386" Y="4.722428400486" />
                  <Point X="-21.616643417244" Y="0.151626255294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.929389738736" Y="-0.585363085387" />
                  <Point X="-20.49638389012" Y="-1.049705008701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.76187069812" Y="4.736134809882" />
                  <Point X="-21.565222085736" Y="0.235780150993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.756225722392" Y="-0.631762235581" />
                  <Point X="-20.348308930413" Y="-1.069199439593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.644756022854" Y="4.749841219277" />
                  <Point X="-25.240217228898" Y="4.316026474648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.027720181098" Y="4.088151289615" />
                  <Point X="-21.503546333467" Y="0.308937526728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.583061706047" Y="-0.678161385776" />
                  <Point X="-20.25978596045" Y="-1.024832180064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.527641347588" Y="4.763547628673" />
                  <Point X="-25.292590118077" Y="4.511486044892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.910318272335" Y="4.101549678796" />
                  <Point X="-21.424518956967" Y="0.36348756357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.409897689703" Y="-0.72456053597" />
                  <Point X="-20.23545511731" Y="-0.911627292301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.410526672322" Y="4.777254038069" />
                  <Point X="-25.344963007255" Y="4.706945615137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.828041896279" Y="4.152615590175" />
                  <Point X="-21.338322970853" Y="0.410350207767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.236733673359" Y="-0.770959686164" />
                  <Point X="-20.217375819483" Y="-0.791718442953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.773467702574" Y="4.233388455107" />
                  <Point X="-22.979331822864" Y="2.309413276173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.679422855993" Y="1.987800284245" />
                  <Point X="-21.960272744111" Y="1.216606206451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.763920387466" Y="1.006044083046" />
                  <Point X="-21.236543485587" Y="0.440501595081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.741480074121" Y="4.338382445881" />
                  <Point X="-22.98181082425" Y="2.451368202327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.538977657113" Y="1.976487770128" />
                  <Point X="-21.999107255706" Y="1.397547644191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.587508623806" Y="0.956162150253" />
                  <Point X="-21.132615487972" Y="0.468348984979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.712486487343" Y="4.446587153265" />
                  <Point X="-22.952266965909" Y="2.558982815705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.432087931128" Y="2.001159095195" />
                  <Point X="-21.977859272533" Y="1.514058494521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.459386450963" Y="0.958064463671" />
                  <Point X="-21.028687490356" Y="0.496196374876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.683492900566" Y="4.554791860649" />
                  <Point X="-22.905865693883" Y="2.648520066114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.340003875301" Y="2.041707557669" />
                  <Point X="-21.937841468415" Y="1.610441176177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.343693728668" Y="0.973295730941" />
                  <Point X="-20.92475949274" Y="0.524043764773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.654499313788" Y="4.662996568033" />
                  <Point X="-22.856195383019" Y="2.734551701562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.255567517334" Y="2.090457172033" />
                  <Point X="-21.880220323779" Y="1.687946586268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.228000982933" Y="0.988526973073" />
                  <Point X="-20.820831495125" Y="0.55189115467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.62550572701" Y="4.771201275418" />
                  <Point X="-22.806525072154" Y="2.820583337009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.171130946408" Y="2.139206558026" />
                  <Point X="-21.812696035306" Y="1.754832174779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.112308237198" Y="1.003758215205" />
                  <Point X="-20.716903497509" Y="0.579738544568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.496218021996" Y="4.771853708605" />
                  <Point X="-22.75685476129" Y="2.906614972457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.086694375482" Y="2.187955944018" />
                  <Point X="-21.736978904153" Y="1.812932015154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.996615491462" Y="1.018989457337" />
                  <Point X="-20.612975499894" Y="0.607585934465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.352263231191" Y="4.756777617923" />
                  <Point X="-22.707184450425" Y="2.992646607904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.002257804555" Y="2.236705330011" />
                  <Point X="-21.661261772999" Y="1.871031855528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.880922745727" Y="1.03422069947" />
                  <Point X="-20.509047502278" Y="0.635433324362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.208308440385" Y="4.74170152724" />
                  <Point X="-22.657514139561" Y="3.078678243352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.917821233629" Y="2.285454716003" />
                  <Point X="-21.585544641846" Y="1.929131695902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.765229999992" Y="1.049451941602" />
                  <Point X="-20.405119504663" Y="0.66328071426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.046624092796" Y="4.707612814622" />
                  <Point X="-22.607843828696" Y="3.164709878799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.833384662702" Y="2.334204101996" />
                  <Point X="-21.509827510693" Y="1.987231536276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.649537254256" Y="1.064683183734" />
                  <Point X="-20.301191507047" Y="0.691128104157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.878986418863" Y="4.66713994111" />
                  <Point X="-22.558173517832" Y="3.250741514247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.748948091776" Y="2.382953487988" />
                  <Point X="-21.43411037954" Y="2.045331376651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.533844508521" Y="1.079914425867" />
                  <Point X="-20.220854779162" Y="0.744274033543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.711348744929" Y="4.626667067598" />
                  <Point X="-22.508503206967" Y="3.336773149694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.66451152085" Y="2.431702873981" />
                  <Point X="-21.358393248387" Y="2.103431217025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.418151762786" Y="1.095145667999" />
                  <Point X="-20.246889243923" Y="0.911489101571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.543250871168" Y="4.58570069019" />
                  <Point X="-22.458832896103" Y="3.422804785142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.580074949923" Y="2.480452259973" />
                  <Point X="-21.282676117233" Y="2.161531057399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.30245901705" Y="1.110376910131" />
                  <Point X="-20.2924558417" Y="1.099649817885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.346965181517" Y="4.514506581019" />
                  <Point X="-22.409162585238" Y="3.508836420589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.495638378997" Y="2.529201645966" />
                  <Point X="-21.20695898608" Y="2.219630897773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.150679491866" Y="4.443312471847" />
                  <Point X="-22.359492274374" Y="3.594868056037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.41120180807" Y="2.577951031958" />
                  <Point X="-21.131241854927" Y="2.277730738148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.921874496322" Y="4.337245676564" />
                  <Point X="-22.309821963509" Y="3.680899691484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.326765237144" Y="2.626700417951" />
                  <Point X="-21.055524723774" Y="2.335830578522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.677901948353" Y="4.214913672653" />
                  <Point X="-22.260151652645" Y="3.766931326932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.242328666217" Y="2.675449803943" />
                  <Point X="-20.979807592621" Y="2.393930418896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.393486731834" Y="4.049212216439" />
                  <Point X="-22.21048134178" Y="3.852962962379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.157892095291" Y="2.724199189936" />
                  <Point X="-20.904090461468" Y="2.45203025927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.073455524365" Y="2.772948575928" />
                  <Point X="-20.987550932754" Y="2.680827179837" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.0001640625" Y="0.001626220703" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.14187109375" Y="-4.979877441406" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.53568359375" Y="-3.521540283203" />
                  <Point X="-24.69369140625" Y="-3.293880615234" />
                  <Point X="-24.699412109375" Y="-3.285639892578" />
                  <Point X="-24.725666015625" Y="-3.266399414062" />
                  <Point X="-24.970173828125" Y="-3.190512939453" />
                  <Point X="-24.979025390625" Y="-3.187766113281" />
                  <Point X="-25.0086640625" Y="-3.187766357422" />
                  <Point X="-25.253171875" Y="-3.263652587891" />
                  <Point X="-25.2620234375" Y="-3.266399658203" />
                  <Point X="-25.28828125" Y="-3.285646484375" />
                  <Point X="-25.4462890625" Y="-3.513306152344" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.473220703125" Y="-3.589336669922" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-26.09124609375" Y="-4.939812011719" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.53475390625" />
                  <Point X="-26.368095703125" Y="-4.241092285156" />
                  <Point X="-26.3702109375" Y="-4.230462402344" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.61139453125" Y="-4.005209960938" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.948015625" Y="-3.966180175781" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791015625" />
                  <Point X="-27.238833984375" Y="-4.140137695312" />
                  <Point X="-27.24784765625" Y="-4.146161621094" />
                  <Point X="-27.26758984375" Y="-4.167532714844" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.8426484375" Y="-4.175774414063" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.228580078125" Y="-3.880607421875" />
                  <Point X="-28.224462890625" Y="-3.873476318359" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.59759375" />
                  <Point X="-27.513978515625" Y="-2.568764892578" />
                  <Point X="-27.53070703125" Y="-2.552035888672" />
                  <Point X="-27.5313125" Y="-2.551429931641" />
                  <Point X="-27.56014453125" Y="-2.537200195312" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-27.636724609375" Y="-2.569475097656" />
                  <Point X="-28.842958984375" Y="-3.265894042969" />
                  <Point X="-29.15128515625" Y="-2.860815673828" />
                  <Point X="-29.161703125" Y="-2.847126464844" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-29.4208046875" Y="-2.387689453125" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.396013793945" />
                  <Point X="-28.138390625" Y="-1.367322143555" />
                  <Point X="-28.13812109375" Y="-1.366282958984" />
                  <Point X="-28.140322265625" Y="-1.334603881836" />
                  <Point X="-28.161158203125" Y="-1.310639526367" />
                  <Point X="-28.18671484375" Y="-1.29559753418" />
                  <Point X="-28.18765234375" Y="-1.295046142578" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.276388671875" Y="-1.296057128906" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.92331640625" Y="-1.027143676758" />
                  <Point X="-29.927392578125" Y="-1.011188598633" />
                  <Point X="-29.998396484375" Y="-0.5147421875" />
                  <Point X="-29.989396484375" Y="-0.512330871582" />
                  <Point X="-28.557462890625" Y="-0.128645339966" />
                  <Point X="-28.54189453125" Y="-0.121424980164" />
                  <Point X="-28.515111328125" Y="-0.102835762024" />
                  <Point X="-28.514166015625" Y="-0.102179534912" />
                  <Point X="-28.494896484375" Y="-0.075907676697" />
                  <Point X="-28.48596875" Y="-0.047141880035" />
                  <Point X="-28.48563671875" Y="-0.046063220978" />
                  <Point X="-28.485646484375" Y="-0.016459415436" />
                  <Point X="-28.49457421875" Y="0.012306232452" />
                  <Point X="-28.49488671875" Y="0.013310048103" />
                  <Point X="-28.514140625" Y="0.039602741241" />
                  <Point X="-28.540923828125" Y="0.058191959381" />
                  <Point X="-28.557462890625" Y="0.066085273743" />
                  <Point X="-28.609294921875" Y="0.07997379303" />
                  <Point X="-29.998185546875" Y="0.452126037598" />
                  <Point X="-29.92026953125" Y="0.97867956543" />
                  <Point X="-29.91764453125" Y="0.996417663574" />
                  <Point X="-29.773515625" Y="1.528299072266" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866699219" />
                  <Point X="-28.672423828125" Y="1.414557739258" />
                  <Point X="-28.670283203125" Y="1.415232299805" />
                  <Point X="-28.65153515625" Y="1.42605456543" />
                  <Point X="-28.6391171875" Y="1.443786743164" />
                  <Point X="-28.61533203125" Y="1.501212402344" />
                  <Point X="-28.614474609375" Y="1.503283691406" />
                  <Point X="-28.61071484375" Y="1.524603271484" />
                  <Point X="-28.61631640625" Y="1.545514282227" />
                  <Point X="-28.645017578125" Y="1.6006484375" />
                  <Point X="-28.646056640625" Y="1.602644165039" />
                  <Point X="-28.65996875" Y="1.619221801758" />
                  <Point X="-28.68969921875" Y="1.642035522461" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.170212890625" Y="2.769531738281" />
                  <Point X="-29.16001171875" Y="2.787008300781" />
                  <Point X="-28.774669921875" Y="3.282310546875" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920435058594" />
                  <Point X="-28.055953125" Y="2.913211914062" />
                  <Point X="-28.05296484375" Y="2.912950439453" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927404785156" />
                  <Point X="-27.9546484375" Y="2.986007080078" />
                  <Point X="-27.95252734375" Y="2.988128173828" />
                  <Point X="-27.9408984375" Y="3.006384277344" />
                  <Point X="-27.93807421875" Y="3.027845458984" />
                  <Point X="-27.945296875" Y="3.110406494141" />
                  <Point X="-27.94555859375" Y="3.113395019531" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-27.9652421875" Y="3.156852294922" />
                  <Point X="-28.30727734375" Y="3.749276611328" />
                  <Point X="-27.770634765625" Y="4.160714355469" />
                  <Point X="-27.75287109375" Y="4.174334472656" />
                  <Point X="-27.141546875" Y="4.513972167969" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.85935546875" Y="4.225825683594" />
                  <Point X="-26.856033203125" Y="4.224096191406" />
                  <Point X="-26.835125" Y="4.2184921875" />
                  <Point X="-26.81380859375" Y="4.22225" />
                  <Point X="-26.718099609375" Y="4.26189453125" />
                  <Point X="-26.714634765625" Y="4.263329589844" />
                  <Point X="-26.696904296875" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.294489257812" />
                  <Point X="-26.6549375" Y="4.393272949219" />
                  <Point X="-26.653806640625" Y="4.396848632813" />
                  <Point X="-26.651916015625" Y="4.418426757812" />
                  <Point X="-26.6534140625" Y="4.429804199219" />
                  <Point X="-26.68913671875" Y="4.701141113281" />
                  <Point X="-25.991087890625" Y="4.896849609375" />
                  <Point X="-25.96809375" Y="4.903296386719" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.938794921875" Y="4.336097167969" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.159875" Y="4.927668457031" />
                  <Point X="-24.139794921875" Y="4.925565429688" />
                  <Point X="-23.511693359375" Y="4.773922851562" />
                  <Point X="-23.491537109375" Y="4.769056152344" />
                  <Point X="-23.081787109375" Y="4.620437011719" />
                  <Point X="-23.068966796875" Y="4.615786621094" />
                  <Point X="-22.67364453125" Y="4.430908203125" />
                  <Point X="-22.66130859375" Y="4.425138671875" />
                  <Point X="-22.27941015625" Y="4.202643554688" />
                  <Point X="-22.26747265625" Y="4.195688476562" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-21.9386171875" Y="3.94384765625" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491514648438" />
                  <Point X="-22.7958671875" Y="2.414032958984" />
                  <Point X="-22.797955078125" Y="2.392324951172" />
                  <Point X="-22.78987890625" Y="2.325355224609" />
                  <Point X="-22.789587890625" Y="2.322929931641" />
                  <Point X="-22.781318359375" Y="2.3008125" />
                  <Point X="-22.739861328125" Y="2.239715332031" />
                  <Point X="-22.73984765625" Y="2.239697265625" />
                  <Point X="-22.72505859375" Y="2.224204101562" />
                  <Point X="-22.663962890625" Y="2.182747070313" />
                  <Point X="-22.661736328125" Y="2.181237060547" />
                  <Point X="-22.639666015625" Y="2.172980712891" />
                  <Point X="-22.572666015625" Y="2.164901611328" />
                  <Point X="-22.5513359375" Y="2.165946289062" />
                  <Point X="-22.473853515625" Y="2.186666015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-22.40683203125" Y="2.222516845703" />
                  <Point X="-21.00575" Y="3.031431152344" />
                  <Point X="-20.804484375" Y="2.751716552734" />
                  <Point X="-20.797412109375" Y="2.741889160156" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-20.61659765625" Y="2.433140869141" />
                  <Point X="-21.7113828125" Y="1.593082763672" />
                  <Point X="-21.72062890625" Y="1.583833129883" />
                  <Point X="-21.776392578125" Y="1.511085205078" />
                  <Point X="-21.7763984375" Y="1.511076416016" />
                  <Point X="-21.78687890625" Y="1.491500976562" />
                  <Point X="-21.80765234375" Y="1.417225097656" />
                  <Point X="-21.808404296875" Y="1.414536621094" />
                  <Point X="-21.809220703125" Y="1.390965820312" />
                  <Point X="-21.79216796875" Y="1.308324462891" />
                  <Point X="-21.7843515625" Y="1.287954956055" />
                  <Point X="-21.73798046875" Y="1.217474609375" />
                  <Point X="-21.7363046875" Y="1.214922241211" />
                  <Point X="-21.71905078125" Y="1.198819702148" />
                  <Point X="-21.651849609375" Y="1.160991943359" />
                  <Point X="-21.651849609375" Y="1.160991210938" />
                  <Point X="-21.6314296875" Y="1.153619140625" />
                  <Point X="-21.54055859375" Y="1.14160925293" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-21.474673828125" Y="1.14769152832" />
                  <Point X="-20.1510234375" Y="1.321953369141" />
                  <Point X="-20.0638515625" Y="0.963875488281" />
                  <Point X="-20.060810546875" Y="0.951378173828" />
                  <Point X="-20.002140625" Y="0.574555908203" />
                  <Point X="-20.00418359375" Y="0.574008544922" />
                  <Point X="-21.25883203125" Y="0.237826843262" />
                  <Point X="-21.270912109375" Y="0.23281918335" />
                  <Point X="-21.360185546875" Y="0.181217193604" />
                  <Point X="-21.360185546875" Y="0.181217529297" />
                  <Point X="-21.377734375" Y="0.166926361084" />
                  <Point X="-21.43130078125" Y="0.098669494629" />
                  <Point X="-21.433236328125" Y="0.096203536987" />
                  <Point X="-21.443013671875" Y="0.074737342834" />
                  <Point X="-21.460869140625" Y="-0.018498380661" />
                  <Point X="-21.461515625" Y="-0.040686824799" />
                  <Point X="-21.44366015625" Y="-0.133922393799" />
                  <Point X="-21.443013671875" Y="-0.137297409058" />
                  <Point X="-21.433240234375" Y="-0.158758880615" />
                  <Point X="-21.379673828125" Y="-0.22701574707" />
                  <Point X="-21.379669921875" Y="-0.227020309448" />
                  <Point X="-21.363421875" Y="-0.241907165527" />
                  <Point X="-21.274150390625" Y="-0.293507110596" />
                  <Point X="-21.25883203125" Y="-0.300386901855" />
                  <Point X="-21.213416015625" Y="-0.312555908203" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.049869140625" Y="-0.955139099121" />
                  <Point X="-20.0515703125" Y="-0.966416381836" />
                  <Point X="-20.125451171875" Y="-1.290178833008" />
                  <Point X="-20.13298046875" Y="-1.28918762207" />
                  <Point X="-21.588017578125" Y="-1.097628295898" />
                  <Point X="-21.6051640625" Y="-1.098341308594" />
                  <Point X="-21.78038671875" Y="-1.136426513672" />
                  <Point X="-21.786728515625" Y="-1.137805175781" />
                  <Point X="-21.814552734375" Y="-1.154695678711" />
                  <Point X="-21.920462890625" Y="-1.282072998047" />
                  <Point X="-21.924296875" Y="-1.28668371582" />
                  <Point X="-21.935640625" Y="-1.314070556641" />
                  <Point X="-21.9508203125" Y="-1.479029785156" />
                  <Point X="-21.95137109375" Y="-1.485006103516" />
                  <Point X="-21.943638671875" Y="-1.516620117188" />
                  <Point X="-21.84666796875" Y="-1.667450805664" />
                  <Point X="-21.831537109375" Y="-1.685540771484" />
                  <Point X="-21.789392578125" Y="-1.717880249023" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.791080078125" Y="-2.794395996094" />
                  <Point X="-20.7958671875" Y="-2.802140869141" />
                  <Point X="-20.943310546875" Y="-3.011638183594" />
                  <Point X="-20.95063671875" Y="-3.007408691406" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.47119921875" Y="-2.215650390625" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510919921875" Y="-2.219244628906" />
                  <Point X="-22.684166015625" Y="-2.310423095703" />
                  <Point X="-22.6904375" Y="-2.313723632812" />
                  <Point X="-22.7113984375" Y="-2.334682128906" />
                  <Point X="-22.802578125" Y="-2.507928955078" />
                  <Point X="-22.80587890625" Y="-2.514200195312" />
                  <Point X="-22.8108359375" Y="-2.546374511719" />
                  <Point X="-22.773171875" Y="-2.754916259766" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.73882421875" Y="-2.825487304688" />
                  <Point X="-22.01332421875" Y="-4.082088623047" />
                  <Point X="-22.159017578125" Y="-4.186153808594" />
                  <Point X="-22.164697265625" Y="-4.190209960937" />
                  <Point X="-22.32022265625" Y="-4.290879394531" />
                  <Point X="-22.327923828125" Y="-4.280842773438" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.53512890625" Y="-2.848235107422" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.799138671875" Y="-2.856416748047" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509277344" />
                  <Point X="-24.00836328125" Y="-3.012932128906" />
                  <Point X="-24.014650390625" Y="-3.018159667969" />
                  <Point X="-24.03154296875" Y="-3.045986328125" />
                  <Point X="-24.0834765625" Y="-3.284925048828" />
                  <Point X="-24.0834765625" Y="-3.284925292969" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-24.07839453125" Y="-3.369017822266" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-24.00028125" Y="-4.962069824219" />
                  <Point X="-24.005654296875" Y="-4.963247558594" />
                  <Point X="-24.139796875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#214" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.186493938389" Y="5.051129362463" Z="2.45" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.45" />
                  <Point X="-0.220866811266" Y="5.073090251485" Z="2.45" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.45" />
                  <Point X="-1.010742330347" Y="4.976273180148" Z="2.45" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.45" />
                  <Point X="-1.709143720482" Y="4.454557701065" Z="2.45" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.45" />
                  <Point X="-1.708773506062" Y="4.439604238381" Z="2.45" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.45" />
                  <Point X="-1.743389593664" Y="4.339369066807" Z="2.45" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.45" />
                  <Point X="-1.842425240881" Y="4.301456096782" Z="2.45" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.45" />
                  <Point X="-2.127304019744" Y="4.600799361499" Z="2.45" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.45" />
                  <Point X="-2.157074521177" Y="4.59724461012" Z="2.45" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.45" />
                  <Point X="-2.80721617181" Y="4.231673251636" Z="2.45" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.45" />
                  <Point X="-3.014699499519" Y="3.163132353748" Z="2.45" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.45" />
                  <Point X="-3.001263224259" Y="3.137324409654" Z="2.45" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.45" />
                  <Point X="-2.996161271317" Y="3.05264229657" Z="2.45" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.45" />
                  <Point X="-3.057752055953" Y="2.994301474914" Z="2.45" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.45" />
                  <Point X="-3.77072699455" Y="3.365494449226" Z="2.45" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.45" />
                  <Point X="-3.808013230579" Y="3.360074237936" Z="2.45" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.45" />
                  <Point X="-4.219551951918" Y="2.826017456199" Z="2.45" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.45" />
                  <Point X="-3.726293530182" Y="1.633647701974" Z="2.45" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.45" />
                  <Point X="-3.6955233861" Y="1.608838405251" Z="2.45" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.45" />
                  <Point X="-3.667683445081" Y="1.551625714951" Z="2.45" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.45" />
                  <Point X="-3.693615711366" Y="1.493523541824" Z="2.45" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.45" />
                  <Point X="-4.779341102737" Y="1.609966627755" Z="2.45" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.45" />
                  <Point X="-4.821957150459" Y="1.59470444553" Z="2.45" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.45" />
                  <Point X="-4.975887284906" Y="1.017278921801" Z="2.45" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.45" />
                  <Point X="-3.628393228122" Y="0.062957506772" Z="2.45" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.45" />
                  <Point X="-3.575591242911" Y="0.048396148711" Z="2.45" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.45" />
                  <Point X="-3.548484689604" Y="0.028765703159" Z="2.45" />
                  <Point X="-3.539556741714" Y="0" Z="2.45" />
                  <Point X="-3.539879911531" Y="-0.001041247902" Z="2.45" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.45" />
                  <Point X="-3.549777352213" Y="-0.030479834423" Z="2.45" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.45" />
                  <Point X="-5.008494560651" Y="-0.432754549984" Z="2.45" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.45" />
                  <Point X="-5.057613965754" Y="-0.465612645031" Z="2.45" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.45" />
                  <Point X="-4.977919268251" Y="-1.008237350879" Z="2.45" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.45" />
                  <Point X="-3.276020976027" Y="-1.314349342442" Z="2.45" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.45" />
                  <Point X="-3.21823377033" Y="-1.307407791875" Z="2.45" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.45" />
                  <Point X="-3.192945447377" Y="-1.323488969798" Z="2.45" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.45" />
                  <Point X="-4.457399602884" Y="-2.316741579739" Z="2.45" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.45" />
                  <Point X="-4.492646151143" Y="-2.36885086783" Z="2.45" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.45" />
                  <Point X="-4.197229054901" Y="-2.8598179251" Z="2.45" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.45" />
                  <Point X="-2.617882159948" Y="-2.581496362951" Z="2.45" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.45" />
                  <Point X="-2.572233461416" Y="-2.556097007715" Z="2.45" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.45" />
                  <Point X="-3.273920650217" Y="-3.817196222544" Z="2.45" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.45" />
                  <Point X="-3.285622684072" Y="-3.873251993604" Z="2.45" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.45" />
                  <Point X="-2.875127402235" Y="-4.187005323472" Z="2.45" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.45" />
                  <Point X="-2.234078563076" Y="-4.166690687133" Z="2.45" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.45" />
                  <Point X="-2.217210726085" Y="-4.150430844488" Z="2.45" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.45" />
                  <Point X="-1.957441101558" Y="-3.984793214201" Z="2.45" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.45" />
                  <Point X="-1.650518334014" Y="-4.011522000306" Z="2.45" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.45" />
                  <Point X="-1.423291570547" Y="-4.219570345972" Z="2.45" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.45" />
                  <Point X="-1.411414560544" Y="-4.866708283966" Z="2.45" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.45" />
                  <Point X="-1.402769443136" Y="-4.882160968309" Z="2.45" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.45" />
                  <Point X="-1.106931222036" Y="-4.957615955897" Z="2.45" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.45" />
                  <Point X="-0.431080309218" Y="-3.570997644404" Z="2.45" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.45" />
                  <Point X="-0.411367271071" Y="-3.510532356873" Z="2.45" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.45" />
                  <Point X="-0.244508472357" Y="-3.280125775053" Z="2.45" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.45" />
                  <Point X="0.008850607003" Y="-3.206986270235" Z="2.45" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.45" />
                  <Point X="0.259078588361" Y="-3.291113398895" Z="2.45" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.45" />
                  <Point X="0.803674551183" Y="-4.961538385412" Z="2.45" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.45" />
                  <Point X="0.823967980316" Y="-5.012618488993" Z="2.45" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.45" />
                  <Point X="1.00426467133" Y="-4.9796303915" Z="2.45" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.45" />
                  <Point X="0.9650208274" Y="-3.331211432098" Z="2.45" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.45" />
                  <Point X="0.959225679323" Y="-3.264264699969" Z="2.45" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.45" />
                  <Point X="1.017447398025" Y="-3.020098261636" Z="2.45" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.45" />
                  <Point X="1.199286182159" Y="-2.874926165926" Z="2.45" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.45" />
                  <Point X="1.431675509524" Y="-2.859013070496" Z="2.45" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.45" />
                  <Point X="2.626251020474" Y="-4.280000829135" Z="2.45" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.45" />
                  <Point X="2.668866543488" Y="-4.32223624741" Z="2.45" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.45" />
                  <Point X="2.863884276177" Y="-4.195562136892" Z="2.45" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.45" />
                  <Point X="2.298319646506" Y="-2.769207301493" Z="2.45" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.45" />
                  <Point X="2.269873606618" Y="-2.714749922581" Z="2.45" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.45" />
                  <Point X="2.235511556145" Y="-2.499937069511" Z="2.45" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.45" />
                  <Point X="2.332961308861" Y="-2.323389753822" Z="2.45" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.45" />
                  <Point X="2.513756880931" Y="-2.233574402873" Z="2.45" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.45" />
                  <Point X="4.018206295614" Y="-3.019429853769" Z="2.45" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.45" />
                  <Point X="4.071214540824" Y="-3.037845960853" Z="2.45" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.45" />
                  <Point X="4.245293595585" Y="-2.789404440447" Z="2.45" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.45" />
                  <Point X="3.234888359611" Y="-1.646932439825" Z="2.45" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.45" />
                  <Point X="3.189232739938" Y="-1.609133323086" Z="2.45" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.45" />
                  <Point X="3.0928119668" Y="-1.452331381231" Z="2.45" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.45" />
                  <Point X="3.111825152839" Y="-1.282761485542" Z="2.45" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.45" />
                  <Point X="3.224078125959" Y="-1.15400559323" Z="2.45" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.45" />
                  <Point X="4.854337931107" Y="-1.307479806147" Z="2.45" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.45" />
                  <Point X="4.909956196099" Y="-1.301488869315" Z="2.45" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.45" />
                  <Point X="4.993414786882" Y="-0.931313750835" Z="2.45" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.45" />
                  <Point X="3.793367853406" Y="-0.232979590089" Z="2.45" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.45" />
                  <Point X="3.744721043141" Y="-0.218942681483" Z="2.45" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.45" />
                  <Point X="3.653503720779" Y="-0.164867473465" Z="2.45" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.45" />
                  <Point X="3.599290392405" Y="-0.093235630079" Z="2.45" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.45" />
                  <Point X="3.582081058019" Y="0.003374901096" Z="2.45" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.45" />
                  <Point X="3.601875717621" Y="0.09908126511" Z="2.45" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.45" />
                  <Point X="3.658674371212" Y="0.169206162759" Z="2.45" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.45" />
                  <Point X="5.002599963901" Y="0.55699234268" Z="2.45" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.45" />
                  <Point X="5.04571295122" Y="0.583947735033" Z="2.45" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.45" />
                  <Point X="4.978573208071" Y="1.006980507907" Z="2.45" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.45" />
                  <Point X="3.512644752408" Y="1.22854395631" Z="2.45" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.45" />
                  <Point X="3.459832089817" Y="1.222458807423" Z="2.45" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.45" />
                  <Point X="3.366527808721" Y="1.235838040748" Z="2.45" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.45" />
                  <Point X="3.297639754699" Y="1.27622272913" Z="2.45" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.45" />
                  <Point X="3.250643765002" Y="1.349707819428" Z="2.45" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.45" />
                  <Point X="3.234343930832" Y="1.43503781015" Z="2.45" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.45" />
                  <Point X="3.257134493783" Y="1.511947005217" Z="2.45" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.45" />
                  <Point X="4.407683339794" Y="2.424753287114" Z="2.45" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.45" />
                  <Point X="4.440006401379" Y="2.467233710454" Z="2.45" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.45" />
                  <Point X="4.229942470277" Y="2.812201216807" Z="2.45" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.45" />
                  <Point X="2.562011428072" Y="2.297098060968" Z="2.45" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.45" />
                  <Point X="2.507073275549" Y="2.266248769348" Z="2.45" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.45" />
                  <Point X="2.427166432916" Y="2.245821611756" Z="2.45" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.45" />
                  <Point X="2.357955221162" Y="2.255401166272" Z="2.45" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.45" />
                  <Point X="2.295357433963" Y="2.29906963922" Z="2.45" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.45" />
                  <Point X="2.253608091003" Y="2.362592010916" Z="2.45" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.45" />
                  <Point X="2.246279173309" Y="2.432396300333" Z="2.45" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.45" />
                  <Point X="3.098527291892" Y="3.950127653883" Z="2.45" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.45" />
                  <Point X="3.115522191372" Y="4.011580282588" Z="2.45" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.45" />
                  <Point X="2.739603735642" Y="4.277126738268" Z="2.45" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.45" />
                  <Point X="2.34137968105" Y="4.50747986903" Z="2.45" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.45" />
                  <Point X="1.929104565101" Y="4.698720460821" Z="2.45" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.45" />
                  <Point X="1.493885111295" Y="4.853806303406" Z="2.45" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.45" />
                  <Point X="0.839177475654" Y="5.008676604885" Z="2.45" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.45" />
                  <Point X="0.006750623164" Y="4.380318143488" Z="2.45" />
                  <Point X="0" Y="4.355124473572" Z="2.45" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>