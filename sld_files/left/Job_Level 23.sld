<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#161" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1744" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004716064453" />
                  <Width Value="9.78389453125" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.263015625" Y="-4.160708496094" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140136719" />
                  <Point X="-24.457634765625" Y="-3.467376708984" />
                  <Point X="-24.528623046875" Y="-3.365097167969" />
                  <Point X="-24.62136328125" Y="-3.231476318359" />
                  <Point X="-24.643248046875" Y="-3.209020751953" />
                  <Point X="-24.66950390625" Y="-3.189776855469" />
                  <Point X="-24.69750390625" Y="-3.175669189453" />
                  <Point X="-24.807353515625" Y="-3.141575927734" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097036132812" />
                  <Point X="-25.146671875" Y="-3.131129150391" />
                  <Point X="-25.29018359375" Y="-3.175669433594" />
                  <Point X="-25.31818359375" Y="-3.189776855469" />
                  <Point X="-25.344439453125" Y="-3.209020751953" />
                  <Point X="-25.36632421875" Y="-3.231477294922" />
                  <Point X="-25.437310546875" Y="-3.333756835938" />
                  <Point X="-25.53005078125" Y="-3.467377685547" />
                  <Point X="-25.5381875" Y="-3.481572021484" />
                  <Point X="-25.550990234375" Y="-3.512524658203" />
                  <Point X="-25.777890625" Y="-4.359330078125" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.2033203125" Y="-4.813451171875" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.235869140625" Y="-4.722243164062" />
                  <Point X="-26.2149609375" Y="-4.563438476562" />
                  <Point X="-26.21419921875" Y="-4.5479296875" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.242751953125" Y="-4.384291015625" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.182964355469" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.424779296875" Y="-4.042510986328" />
                  <Point X="-26.556904296875" Y="-3.926639648438" />
                  <Point X="-26.5831875" Y="-3.910295898438" />
                  <Point X="-26.612884765625" Y="-3.897994628906" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.777255859375" Y="-3.882168457031" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708496094" />
                  <Point X="-27.014466796875" Y="-3.882028076172" />
                  <Point X="-27.042658203125" Y="-3.894801757812" />
                  <Point X="-27.15450390625" Y="-3.969535400391" />
                  <Point X="-27.300623046875" Y="-4.067169921875" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.3351015625" Y="-4.099461425781" />
                  <Point X="-27.462490234375" Y="-4.265475585938" />
                  <Point X="-27.4801484375" Y="-4.28848828125" />
                  <Point X="-27.559119140625" Y="-4.239591796875" />
                  <Point X="-27.80171484375" Y="-4.089383544922" />
                  <Point X="-27.9733828125" Y="-3.957203613281" />
                  <Point X="-28.10472265625" Y="-3.856077148438" />
                  <Point X="-27.74652734375" Y="-3.235665527344" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647655029297" />
                  <Point X="-27.406587890625" Y="-2.616129394531" />
                  <Point X="-27.40557421875" Y="-2.585194824219" />
                  <Point X="-27.41455859375" Y="-2.555576660156" />
                  <Point X="-27.428775390625" Y="-2.526747802734" />
                  <Point X="-27.44680078125" Y="-2.501591796875" />
                  <Point X="-27.4641484375" Y="-2.484243164062" />
                  <Point X="-27.4893046875" Y="-2.466215087891" />
                  <Point X="-27.5181328125" Y="-2.451997314453" />
                  <Point X="-27.547751953125" Y="-2.44301171875" />
                  <Point X="-27.5786875" Y="-2.444023925781" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.36953515625" Y="-2.882866943359" />
                  <Point X="-28.8180234375" Y="-3.141801513672" />
                  <Point X="-28.891203125" Y="-3.045659423828" />
                  <Point X="-29.082857421875" Y="-2.793862060547" />
                  <Point X="-29.205935546875" Y="-2.587481689453" />
                  <Point X="-29.306142578125" Y="-2.419449951172" />
                  <Point X="-28.670689453125" Y="-1.931849487305" />
                  <Point X="-28.105955078125" Y="-1.498513305664" />
                  <Point X="-28.08458203125" Y="-1.475599365234" />
                  <Point X="-28.0666171875" Y="-1.448472900391" />
                  <Point X="-28.053857421875" Y="-1.419839355469" />
                  <Point X="-28.046150390625" Y="-1.390085205078" />
                  <Point X="-28.043345703125" Y="-1.359647827148" />
                  <Point X="-28.045556640625" Y="-1.327978759766" />
                  <Point X="-28.05255859375" Y="-1.298236572266" />
                  <Point X="-28.068640625" Y="-1.272255615234" />
                  <Point X="-28.08947265625" Y="-1.248300537109" />
                  <Point X="-28.112970703125" Y="-1.228767822266" />
                  <Point X="-28.139453125" Y="-1.213181152344" />
                  <Point X="-28.16871484375" Y="-1.20195715332" />
                  <Point X="-28.2006015625" Y="-1.195475097656" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-29.1539296875" Y="-1.315767700195" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.7591171875" Y="-1.286116333008" />
                  <Point X="-29.834078125" Y="-0.992649841309" />
                  <Point X="-29.866638671875" Y="-0.76498059082" />
                  <Point X="-29.892421875" Y="-0.584698425293" />
                  <Point X="-29.17619140625" Y="-0.392784820557" />
                  <Point X="-28.532875" Y="-0.220408309937" />
                  <Point X="-28.510783203125" Y="-0.211385437012" />
                  <Point X="-28.48318359375" Y="-0.195814331055" />
                  <Point X="-28.46987109375" Y="-0.186735900879" />
                  <Point X="-28.44233203125" Y="-0.164307647705" />
                  <Point X="-28.426107421875" Y="-0.147354537964" />
                  <Point X="-28.41453125" Y="-0.126941108704" />
                  <Point X="-28.403095703125" Y="-0.099280044556" />
                  <Point X="-28.398431640625" Y="-0.084821853638" />
                  <Point X="-28.390943359375" Y="-0.053117092133" />
                  <Point X="-28.38840234375" Y="-0.031759788513" />
                  <Point X="-28.390728515625" Y="-0.010377929688" />
                  <Point X="-28.396986328125" Y="0.017366771698" />
                  <Point X="-28.401470703125" Y="0.031789136887" />
                  <Point X="-28.41413671875" Y="0.063410263062" />
                  <Point X="-28.425484375" Y="0.0839479599" />
                  <Point X="-28.44151953125" Y="0.101077804565" />
                  <Point X="-28.46537109375" Y="0.120946861267" />
                  <Point X="-28.478544921875" Y="0.130151123047" />
                  <Point X="-28.50983203125" Y="0.148281585693" />
                  <Point X="-28.532875" Y="0.157848327637" />
                  <Point X="-29.373322265625" Y="0.38304574585" />
                  <Point X="-29.89181640625" Y="0.521975585938" />
                  <Point X="-29.872796875" Y="0.650504821777" />
                  <Point X="-29.82448828125" Y="0.976968994141" />
                  <Point X="-29.758939453125" Y="1.218868041992" />
                  <Point X="-29.70355078125" Y="1.423268066406" />
                  <Point X="-29.226810546875" Y="1.36050402832" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228027344" />
                  <Point X="-28.703134765625" Y="1.305263916016" />
                  <Point X="-28.67650390625" Y="1.313661132812" />
                  <Point X="-28.641708984375" Y="1.324631225586" />
                  <Point X="-28.62277734375" Y="1.332961791992" />
                  <Point X="-28.604033203125" Y="1.343783935547" />
                  <Point X="-28.5873515625" Y="1.35601550293" />
                  <Point X="-28.573712890625" Y="1.371567993164" />
                  <Point X="-28.561298828125" Y="1.389297973633" />
                  <Point X="-28.551349609375" Y="1.40743359375" />
                  <Point X="-28.5406640625" Y="1.433232543945" />
                  <Point X="-28.526703125" Y="1.466937744141" />
                  <Point X="-28.520916015625" Y="1.486788085938" />
                  <Point X="-28.51715625" Y="1.508103149414" />
                  <Point X="-28.515802734375" Y="1.52874987793" />
                  <Point X="-28.518951171875" Y="1.549199951172" />
                  <Point X="-28.5245546875" Y="1.570106445312" />
                  <Point X="-28.53205078125" Y="1.58937878418" />
                  <Point X="-28.5449453125" Y="1.61414831543" />
                  <Point X="-28.561791015625" Y="1.646508422852" />
                  <Point X="-28.57328125" Y="1.663705810547" />
                  <Point X="-28.587193359375" Y="1.680285888672" />
                  <Point X="-28.60213671875" Y="1.694590087891" />
                  <Point X="-29.08421875" Y="2.064505615234" />
                  <Point X="-29.351859375" Y="2.269873291016" />
                  <Point X="-29.26886328125" Y="2.412064941406" />
                  <Point X="-29.081146484375" Y="2.733666992188" />
                  <Point X="-28.907521484375" Y="2.956840087891" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.48987890625" Y="3.008189697266" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.109701171875" Y="2.822551269531" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.04056640625" Y="2.818762939453" />
                  <Point X="-28.019107421875" Y="2.821587890625" />
                  <Point X="-27.999015625" Y="2.826504150391" />
                  <Point X="-27.980462890625" Y="2.835652832031" />
                  <Point X="-27.962208984375" Y="2.847281738281" />
                  <Point X="-27.946076171875" Y="2.860228515625" />
                  <Point X="-27.919748046875" Y="2.886556396484" />
                  <Point X="-27.8853515625" Y="2.920951904297" />
                  <Point X="-27.872404296875" Y="2.937084228516" />
                  <Point X="-27.860775390625" Y="2.955338134766" />
                  <Point X="-27.85162890625" Y="2.973887451172" />
                  <Point X="-27.8467109375" Y="2.9939765625" />
                  <Point X="-27.843884765625" Y="3.015435058594" />
                  <Point X="-27.84343359375" Y="3.036122070312" />
                  <Point X="-27.8466796875" Y="3.073213867188" />
                  <Point X="-27.85091796875" Y="3.121671630859" />
                  <Point X="-27.854955078125" Y="3.141960205078" />
                  <Point X="-27.86146484375" Y="3.162603515625" />
                  <Point X="-27.869794921875" Y="3.181532958984" />
                  <Point X="-28.083421875" Y="3.551543457031" />
                  <Point X="-28.18333203125" Y="3.724595947266" />
                  <Point X="-28.0275546875" Y="3.844030029297" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.427162109375" Y="4.246614257813" />
                  <Point X="-27.16703515625" Y="4.391134277344" />
                  <Point X="-27.1298984375" Y="4.342734863281" />
                  <Point X="-27.0431953125" Y="4.229741210938" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.995111328125" Y="4.18939453125" />
                  <Point X="-26.953828125" Y="4.167903808594" />
                  <Point X="-26.89989453125" Y="4.139827636719" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.777451171875" Y="4.134482421875" />
                  <Point X="-26.734453125" Y="4.152293457031" />
                  <Point X="-26.67827734375" Y="4.175562011719" />
                  <Point X="-26.660142578125" Y="4.18551171875" />
                  <Point X="-26.6424140625" Y="4.19792578125" />
                  <Point X="-26.62686328125" Y="4.2115625" />
                  <Point X="-26.6146328125" Y="4.2282421875" />
                  <Point X="-26.603810546875" Y="4.246985839844" />
                  <Point X="-26.595478515625" Y="4.265922851563" />
                  <Point X="-26.581484375" Y="4.310310546875" />
                  <Point X="-26.56319921875" Y="4.368299316406" />
                  <Point X="-26.5591640625" Y="4.388583496094" />
                  <Point X="-26.55727734375" Y="4.410145996094" />
                  <Point X="-26.557728515625" Y="4.430826660156" />
                  <Point X="-26.582015625" Y="4.615303710938" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.37286328125" Y="4.691149902344" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.618119140625" Y="4.848607421875" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.217689453125" Y="4.599006835938" />
                  <Point X="-25.133904296875" Y="4.286315917969" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.744322265625" Y="4.694823730469" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.525505859375" Y="4.870440429688" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.881681640625" Y="4.765520019531" />
                  <Point X="-23.5189765625" Y="4.677951660156" />
                  <Point X="-23.34128515625" Y="4.613501953125" />
                  <Point X="-23.105349609375" Y="4.527926757812" />
                  <Point X="-22.93272265625" Y="4.447194824219" />
                  <Point X="-22.705423828125" Y="4.34089453125" />
                  <Point X="-22.538626953125" Y="4.243718261719" />
                  <Point X="-22.319021484375" Y="4.115775878906" />
                  <Point X="-22.161734375" Y="4.003921386719" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.47883984375" Y="3.198151855469" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857919921875" Y="2.539936767578" />
                  <Point X="-22.866921875" Y="2.516057861328" />
                  <Point X="-22.87623046875" Y="2.481248046875" />
                  <Point X="-22.888392578125" Y="2.435771484375" />
                  <Point X="-22.891380859375" Y="2.417937011719" />
                  <Point X="-22.892271484375" Y="2.380950683594" />
                  <Point X="-22.888640625" Y="2.350850097656" />
                  <Point X="-22.883900390625" Y="2.311525878906" />
                  <Point X="-22.878556640625" Y="2.289605224609" />
                  <Point X="-22.8702890625" Y="2.267513671875" />
                  <Point X="-22.859927734375" Y="2.247470458984" />
                  <Point X="-22.841302734375" Y="2.220021728516" />
                  <Point X="-22.81696875" Y="2.184161865234" />
                  <Point X="-22.80553515625" Y="2.170329345703" />
                  <Point X="-22.77840234375" Y="2.145593261719" />
                  <Point X="-22.750955078125" Y="2.126968017578" />
                  <Point X="-22.71509375" Y="2.102635742188" />
                  <Point X="-22.695044921875" Y="2.092270996094" />
                  <Point X="-22.67295703125" Y="2.084005615234" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.620935546875" Y="2.075033935547" />
                  <Point X="-22.581611328125" Y="2.070291992188" />
                  <Point X="-22.563533203125" Y="2.069845703125" />
                  <Point X="-22.526794921875" Y="2.074170898438" />
                  <Point X="-22.491984375" Y="2.083479492188" />
                  <Point X="-22.4465078125" Y="2.095640625" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.56613671875" Y="2.598196044922" />
                  <Point X="-21.032673828125" Y="2.906190673828" />
                  <Point X="-21.0069921875" Y="2.870498779297" />
                  <Point X="-20.876720703125" Y="2.689452148438" />
                  <Point X="-20.78904296875" Y="2.544561279297" />
                  <Point X="-20.73780078125" Y="2.459883300781" />
                  <Point X="-21.277365234375" Y="2.045860229492" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.778572265625" Y="1.660242919922" />
                  <Point X="-21.796025390625" Y="1.641627319336" />
                  <Point X="-21.821078125" Y="1.608944213867" />
                  <Point X="-21.853806640625" Y="1.566245849609" />
                  <Point X="-21.863392578125" Y="1.550912353516" />
                  <Point X="-21.878369140625" Y="1.517087402344" />
                  <Point X="-21.887701171875" Y="1.483717773438" />
                  <Point X="-21.89989453125" Y="1.440122924805" />
                  <Point X="-21.90334765625" Y="1.417825561523" />
                  <Point X="-21.9041640625" Y="1.39425402832" />
                  <Point X="-21.902259765625" Y="1.371765136719" />
                  <Point X="-21.89459765625" Y="1.334637207031" />
                  <Point X="-21.88458984375" Y="1.286132324219" />
                  <Point X="-21.8793203125" Y="1.268981445313" />
                  <Point X="-21.86371875" Y="1.235741210938" />
                  <Point X="-21.8428828125" Y="1.204070800781" />
                  <Point X="-21.815662109375" Y="1.162695922852" />
                  <Point X="-21.801107421875" Y="1.145450561523" />
                  <Point X="-21.78386328125" Y="1.129360473633" />
                  <Point X="-21.76565234375" Y="1.116034301758" />
                  <Point X="-21.73545703125" Y="1.099037353516" />
                  <Point X="-21.696009765625" Y="1.07683215332" />
                  <Point X="-21.6794765625" Y="1.069501098633" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.6030546875" Y="1.05404284668" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.7087890625" Y="1.152702270508" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.21001171875" Y="1.162624145508" />
                  <Point X="-20.15405859375" Y="0.932788085938" />
                  <Point X="-20.1264296875" Y="0.75533203125" />
                  <Point X="-20.109134765625" Y="0.644238891602" />
                  <Point X="-20.71975" Y="0.480624511719" />
                  <Point X="-21.283419921875" Y="0.329589874268" />
                  <Point X="-21.2952109375" Y="0.325585662842" />
                  <Point X="-21.318453125" Y="0.315067932129" />
                  <Point X="-21.3585625" Y="0.291883789062" />
                  <Point X="-21.410962890625" Y="0.26159552002" />
                  <Point X="-21.4256875" Y="0.251096343994" />
                  <Point X="-21.452466796875" Y="0.225577194214" />
                  <Point X="-21.476533203125" Y="0.194911590576" />
                  <Point X="-21.50797265625" Y="0.154849395752" />
                  <Point X="-21.51969921875" Y="0.135568069458" />
                  <Point X="-21.52947265625" Y="0.114104911804" />
                  <Point X="-21.536318359375" Y="0.092603347778" />
                  <Point X="-21.54433984375" Y="0.050715881348" />
                  <Point X="-21.5548203125" Y="-0.004007135868" />
                  <Point X="-21.556515625" Y="-0.021874988556" />
                  <Point X="-21.5548203125" Y="-0.058553012848" />
                  <Point X="-21.546798828125" Y="-0.100440483093" />
                  <Point X="-21.536318359375" Y="-0.155163345337" />
                  <Point X="-21.52947265625" Y="-0.176665054321" />
                  <Point X="-21.51969921875" Y="-0.198128219604" />
                  <Point X="-21.50797265625" Y="-0.217409698486" />
                  <Point X="-21.48390625" Y="-0.248075149536" />
                  <Point X="-21.452466796875" Y="-0.288137329102" />
                  <Point X="-21.44" Y="-0.301235626221" />
                  <Point X="-21.410962890625" Y="-0.324155670166" />
                  <Point X="-21.370853515625" Y="-0.347339691162" />
                  <Point X="-21.318453125" Y="-0.377627929688" />
                  <Point X="-21.307291015625" Y="-0.383138244629" />
                  <Point X="-21.283419921875" Y="-0.392149871826" />
                  <Point X="-20.54702734375" Y="-0.589465820312" />
                  <Point X="-20.10852734375" Y="-0.706961669922" />
                  <Point X="-20.11373828125" Y="-0.741527770996" />
                  <Point X="-20.14498046875" Y="-0.94874810791" />
                  <Point X="-20.180376953125" Y="-1.103860107422" />
                  <Point X="-20.19882421875" Y="-1.18469921875" />
                  <Point X="-20.921919921875" Y="-1.089501831055" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710144043" />
                  <Point X="-21.62533984375" Y="-1.005508728027" />
                  <Point X="-21.704060546875" Y="-1.022619140625" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.836021484375" Y="-1.056595703125" />
                  <Point X="-21.863849609375" Y="-1.07348815918" />
                  <Point X="-21.8876015625" Y="-1.093960083008" />
                  <Point X="-21.93518359375" Y="-1.151186157227" />
                  <Point X="-21.997345703125" Y="-1.225948120117" />
                  <Point X="-22.0120703125" Y="-1.250335449219" />
                  <Point X="-22.023412109375" Y="-1.277721069336" />
                  <Point X="-22.030240234375" Y="-1.305365234375" />
                  <Point X="-22.037060546875" Y="-1.379475708008" />
                  <Point X="-22.04596875" Y="-1.476295532227" />
                  <Point X="-22.04365234375" Y="-1.507562133789" />
                  <Point X="-22.035921875" Y="-1.539182983398" />
                  <Point X="-22.023548828125" Y="-1.56799621582" />
                  <Point X="-21.979984375" Y="-1.635759277344" />
                  <Point X="-21.923068359375" Y="-1.724286743164" />
                  <Point X="-21.9130625" Y="-1.737243286133" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.2059921875" Y="-2.285283447266" />
                  <Point X="-20.786876953125" Y="-2.6068828125" />
                  <Point X="-20.787130859375" Y="-2.607293701172" />
                  <Point X="-20.875197265625" Y="-2.749798339844" />
                  <Point X="-20.94840234375" Y="-2.853811767578" />
                  <Point X="-20.971017578125" Y="-2.885945068359" />
                  <Point X="-21.616837890625" Y="-2.513080078125" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.2457734375" Y="-2.159825195312" />
                  <Point X="-22.339462890625" Y="-2.142904785156" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135176757812" />
                  <Point X="-22.633" Y="-2.176140136719" />
                  <Point X="-22.73468359375" Y="-2.229655761719" />
                  <Point X="-22.757615234375" Y="-2.246550292969" />
                  <Point X="-22.77857421875" Y="-2.267510253906" />
                  <Point X="-22.79546484375" Y="-2.290438232422" />
                  <Point X="-22.8364296875" Y="-2.368271972656" />
                  <Point X="-22.8899453125" Y="-2.469956298828" />
                  <Point X="-22.899771484375" Y="-2.499735351563" />
                  <Point X="-22.904728515625" Y="-2.531907958984" />
                  <Point X="-22.90432421875" Y="-2.563259765625" />
                  <Point X="-22.88740234375" Y="-2.656950195312" />
                  <Point X="-22.865296875" Y="-2.779350097656" />
                  <Point X="-22.86101171875" Y="-2.795139648438" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.4090390625" Y="-3.586688964844" />
                  <Point X="-22.138712890625" Y="-4.05490625" />
                  <Point X="-22.218134765625" Y="-4.111634277344" />
                  <Point X="-22.298234375" Y="-4.163481445312" />
                  <Point X="-22.79717578125" Y="-3.513247802734" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.370478515625" Y="-2.841149658203" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.683958984375" Y="-2.750416259766" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-23.9734375" Y="-2.860345214844" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968861083984" />
                  <Point X="-24.11275" Y="-2.996687744141" />
                  <Point X="-24.124375" Y="-3.025808837891" />
                  <Point X="-24.14770703125" Y="-3.133155761719" />
                  <Point X="-24.178189453125" Y="-3.273396728516" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323120361328" />
                  <Point X="-24.05580859375" Y="-4.268400390625" />
                  <Point X="-23.97793359375" Y="-4.859916015625" />
                  <Point X="-24.02430859375" Y="-4.870080566406" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.05842578125" Y="-4.75263671875" />
                  <Point X="-26.14124609375" Y="-4.731328125" />
                  <Point X="-26.1207734375" Y="-4.575837402344" />
                  <Point X="-26.120076171875" Y="-4.568098632812" />
                  <Point X="-26.11944921875" Y="-4.541030761719" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497688964844" />
                  <Point X="-26.149578125" Y="-4.365756835938" />
                  <Point X="-26.18386328125" Y="-4.193397460938" />
                  <Point X="-26.188125" Y="-4.178468261719" />
                  <Point X="-26.19902734375" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135462890625" />
                  <Point X="-26.221740234375" Y="-4.107625976562" />
                  <Point X="-26.23057421875" Y="-4.094861083984" />
                  <Point X="-26.25020703125" Y="-4.070937744141" />
                  <Point X="-26.261005859375" Y="-4.059779541016" />
                  <Point X="-26.362140625" Y="-3.971086425781" />
                  <Point X="-26.494265625" Y="-3.855215087891" />
                  <Point X="-26.50673828125" Y="-3.845965087891" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822527587891" />
                  <Point X="-26.576529296875" Y="-3.810226318359" />
                  <Point X="-26.5913125" Y="-3.805476318359" />
                  <Point X="-26.621455078125" Y="-3.798447998047" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.77104296875" Y="-3.787371826172" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166015625" />
                  <Point X="-27.008005859375" Y="-3.781945556641" />
                  <Point X="-27.0390546875" Y="-3.790265136719" />
                  <Point X="-27.053673828125" Y="-3.795496337891" />
                  <Point X="-27.081865234375" Y="-3.808270019531" />
                  <Point X="-27.0954375" Y="-3.8158125" />
                  <Point X="-27.207283203125" Y="-3.890546142578" />
                  <Point X="-27.35340234375" Y="-3.988180664062" />
                  <Point X="-27.35968359375" Y="-3.992759277344" />
                  <Point X="-27.380439453125" Y="-4.010130859375" />
                  <Point X="-27.402755859375" Y="-4.032769287109" />
                  <Point X="-27.410470703125" Y="-4.041628662109" />
                  <Point X="-27.503203125" Y="-4.162477539063" />
                  <Point X="-27.509107421875" Y="-4.158821289062" />
                  <Point X="-27.74759765625" Y="-4.011154541016" />
                  <Point X="-27.91542578125" Y="-3.881931396484" />
                  <Point X="-27.98086328125" Y="-3.831546386719" />
                  <Point X="-27.664255859375" Y="-3.283165527344" />
                  <Point X="-27.341490234375" Y="-2.724119628906" />
                  <Point X="-27.3348515625" Y="-2.710085693359" />
                  <Point X="-27.32394921875" Y="-2.68112109375" />
                  <Point X="-27.319685546875" Y="-2.666190429688" />
                  <Point X="-27.3134140625" Y="-2.634664794922" />
                  <Point X="-27.311638671875" Y="-2.619240722656" />
                  <Point X="-27.310625" Y="-2.588306152344" />
                  <Point X="-27.3146640625" Y="-2.557618408203" />
                  <Point X="-27.3236484375" Y="-2.528000244141" />
                  <Point X="-27.32935546875" Y="-2.513559326172" />
                  <Point X="-27.343572265625" Y="-2.48473046875" />
                  <Point X="-27.351552734375" Y="-2.471414794922" />
                  <Point X="-27.369578125" Y="-2.446258789062" />
                  <Point X="-27.379623046875" Y="-2.434418457031" />
                  <Point X="-27.396970703125" Y="-2.417069824219" />
                  <Point X="-27.408810546875" Y="-2.407024658203" />
                  <Point X="-27.433966796875" Y="-2.388996582031" />
                  <Point X="-27.447283203125" Y="-2.381013671875" />
                  <Point X="-27.476111328125" Y="-2.366795898438" />
                  <Point X="-27.4905546875" Y="-2.361088623047" />
                  <Point X="-27.520173828125" Y="-2.352103027344" />
                  <Point X="-27.550859375" Y="-2.3480625" />
                  <Point X="-27.581794921875" Y="-2.349074707031" />
                  <Point X="-27.597220703125" Y="-2.350849121094" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.41703515625" Y="-2.800594482422" />
                  <Point X="-28.793087890625" Y="-3.017708740234" />
                  <Point X="-28.815609375" Y="-2.988120849609" />
                  <Point X="-29.00401953125" Y="-2.7405859375" />
                  <Point X="-29.12434375" Y="-2.538822753906" />
                  <Point X="-29.181265625" Y="-2.443373291016" />
                  <Point X="-28.612857421875" Y="-2.007217895508" />
                  <Point X="-28.048123046875" Y="-1.573881835938" />
                  <Point X="-28.036484375" Y="-1.563312011719" />
                  <Point X="-28.015111328125" Y="-1.540398071289" />
                  <Point X="-28.005376953125" Y="-1.528054199219" />
                  <Point X="-27.987412109375" Y="-1.500927734375" />
                  <Point X="-27.97984375" Y="-1.487141479492" />
                  <Point X="-27.967083984375" Y="-1.45850793457" />
                  <Point X="-27.961892578125" Y="-1.443660522461" />
                  <Point X="-27.954185546875" Y="-1.41390637207" />
                  <Point X="-27.95155078125" Y="-1.398802124023" />
                  <Point X="-27.94874609375" Y="-1.368364746094" />
                  <Point X="-27.948576171875" Y="-1.353031616211" />
                  <Point X="-27.950787109375" Y="-1.321362426758" />
                  <Point X="-27.953083984375" Y="-1.306208862305" />
                  <Point X="-27.9600859375" Y="-1.276466674805" />
                  <Point X="-27.97178125" Y="-1.248235961914" />
                  <Point X="-27.98786328125" Y="-1.222255126953" />
                  <Point X="-27.996955078125" Y="-1.209916015625" />
                  <Point X="-28.017787109375" Y="-1.1859609375" />
                  <Point X="-28.028744140625" Y="-1.175244628906" />
                  <Point X="-28.0522421875" Y="-1.155711914062" />
                  <Point X="-28.064783203125" Y="-1.146895874023" />
                  <Point X="-28.091265625" Y="-1.131309204102" />
                  <Point X="-28.105431640625" Y="-1.124482421875" />
                  <Point X="-28.134693359375" Y="-1.113258422852" />
                  <Point X="-28.1497890625" Y="-1.108861206055" />
                  <Point X="-28.18167578125" Y="-1.102379150391" />
                  <Point X="-28.197294921875" Y="-1.100532714844" />
                  <Point X="-28.22862109375" Y="-1.09944128418" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-29.166330078125" Y="-1.221580566406" />
                  <Point X="-29.66091796875" Y="-1.286694335938" />
                  <Point X="-29.667072265625" Y="-1.262604492188" />
                  <Point X="-29.740763671875" Y="-0.974110961914" />
                  <Point X="-29.772595703125" Y="-0.751530822754" />
                  <Point X="-29.786451171875" Y="-0.654654724121" />
                  <Point X="-29.151603515625" Y="-0.484547790527" />
                  <Point X="-28.508287109375" Y="-0.312171264648" />
                  <Point X="-28.496955078125" Y="-0.308355682373" />
                  <Point X="-28.47486328125" Y="-0.299332794189" />
                  <Point X="-28.464103515625" Y="-0.294125640869" />
                  <Point X="-28.43650390625" Y="-0.27855456543" />
                  <Point X="-28.42966015625" Y="-0.274301116943" />
                  <Point X="-28.40987890625" Y="-0.260397613525" />
                  <Point X="-28.38233984375" Y="-0.23796937561" />
                  <Point X="-28.37369921875" Y="-0.229991897583" />
                  <Point X="-28.357474609375" Y="-0.213038772583" />
                  <Point X="-28.343470703125" Y="-0.194217025757" />
                  <Point X="-28.33189453125" Y="-0.173803527832" />
                  <Point X="-28.32673828125" Y="-0.163236297607" />
                  <Point X="-28.315302734375" Y="-0.135575195312" />
                  <Point X="-28.31268359375" Y="-0.128446060181" />
                  <Point X="-28.305974609375" Y="-0.106658836365" />
                  <Point X="-28.298486328125" Y="-0.074954162598" />
                  <Point X="-28.296609375" Y="-0.064340705872" />
                  <Point X="-28.294068359375" Y="-0.042983448029" />
                  <Point X="-28.293958984375" Y="-0.021485149384" />
                  <Point X="-28.29628515625" Y="-0.000103371479" />
                  <Point X="-28.298056640625" Y="0.010524204254" />
                  <Point X="-28.304314453125" Y="0.038268974304" />
                  <Point X="-28.30626953125" Y="0.04557334137" />
                  <Point X="-28.313283203125" Y="0.067113548279" />
                  <Point X="-28.32594921875" Y="0.098734550476" />
                  <Point X="-28.330984375" Y="0.109353805542" />
                  <Point X="-28.34233203125" Y="0.129891555786" />
                  <Point X="-28.35612890625" Y="0.148870391846" />
                  <Point X="-28.3721640625" Y="0.16600038147" />
                  <Point X="-28.38071484375" Y="0.174069702148" />
                  <Point X="-28.40456640625" Y="0.193938796997" />
                  <Point X="-28.4109609375" Y="0.198822250366" />
                  <Point X="-28.4309140625" Y="0.212347381592" />
                  <Point X="-28.462201171875" Y="0.230477874756" />
                  <Point X="-28.47340625" Y="0.236020477295" />
                  <Point X="-28.49644921875" Y="0.245587173462" />
                  <Point X="-28.508287109375" Y="0.249611297607" />
                  <Point X="-29.348734375" Y="0.474808685303" />
                  <Point X="-29.7854453125" Y="0.591824951172" />
                  <Point X="-29.7788203125" Y="0.636598327637" />
                  <Point X="-29.73133203125" Y="0.957522949219" />
                  <Point X="-29.66724609375" Y="1.194021362305" />
                  <Point X="-29.6335859375" Y="1.318237182617" />
                  <Point X="-29.2392109375" Y="1.266316772461" />
                  <Point X="-28.778064453125" Y="1.20560559082" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.204703125" />
                  <Point X="-28.71514453125" Y="1.206589477539" />
                  <Point X="-28.704888671875" Y="1.208053588867" />
                  <Point X="-28.684599609375" Y="1.212089477539" />
                  <Point X="-28.67456640625" Y="1.214661376953" />
                  <Point X="-28.647935546875" Y="1.22305859375" />
                  <Point X="-28.613140625" Y="1.234028686523" />
                  <Point X="-28.603447265625" Y="1.237677368164" />
                  <Point X="-28.584515625" Y="1.246008056641" />
                  <Point X="-28.57527734375" Y="1.250689819336" />
                  <Point X="-28.556533203125" Y="1.26151184082" />
                  <Point X="-28.547859375" Y="1.267171875" />
                  <Point X="-28.531177734375" Y="1.279403442383" />
                  <Point X="-28.51592578125" Y="1.29337890625" />
                  <Point X="-28.502287109375" Y="1.308931396484" />
                  <Point X="-28.495892578125" Y="1.317079956055" />
                  <Point X="-28.483478515625" Y="1.334809936523" />
                  <Point X="-28.478009765625" Y="1.343605224609" />
                  <Point X="-28.468060546875" Y="1.361740844727" />
                  <Point X="-28.463580078125" Y="1.371080810547" />
                  <Point X="-28.45289453125" Y="1.396879638672" />
                  <Point X="-28.43893359375" Y="1.430584960938" />
                  <Point X="-28.4355" Y="1.440348632813" />
                  <Point X="-28.429712890625" Y="1.460198974609" />
                  <Point X="-28.427359375" Y="1.470285766602" />
                  <Point X="-28.423599609375" Y="1.491600708008" />
                  <Point X="-28.422359375" Y="1.501888793945" />
                  <Point X="-28.421005859375" Y="1.522535400391" />
                  <Point X="-28.421908203125" Y="1.543205444336" />
                  <Point X="-28.425056640625" Y="1.563655517578" />
                  <Point X="-28.427189453125" Y="1.573794433594" />
                  <Point X="-28.43279296875" Y="1.594700927734" />
                  <Point X="-28.436015625" Y="1.604544067383" />
                  <Point X="-28.44351171875" Y="1.62381640625" />
                  <Point X="-28.44778515625" Y="1.633245849609" />
                  <Point X="-28.4606796875" Y="1.658015258789" />
                  <Point X="-28.477525390625" Y="1.690375366211" />
                  <Point X="-28.48280078125" Y="1.699285522461" />
                  <Point X="-28.494291015625" Y="1.716482788086" />
                  <Point X="-28.500505859375" Y="1.724770019531" />
                  <Point X="-28.51441796875" Y="1.741350097656" />
                  <Point X="-28.521501953125" Y="1.748912597656" />
                  <Point X="-28.5364453125" Y="1.763216796875" />
                  <Point X="-28.5443046875" Y="1.769958618164" />
                  <Point X="-29.02638671875" Y="2.139874023438" />
                  <Point X="-29.22761328125" Y="2.294280517578" />
                  <Point X="-29.18681640625" Y="2.364175292969" />
                  <Point X="-29.00228125" Y="2.680326416016" />
                  <Point X="-28.832541015625" Y="2.898506103516" />
                  <Point X="-28.726337890625" Y="3.035013427734" />
                  <Point X="-28.53737890625" Y="2.925917236328" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.11798046875" Y="2.727912841797" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.038494140625" Y="2.723785644531" />
                  <Point X="-28.02816796875" Y="2.724575683594" />
                  <Point X="-28.006708984375" Y="2.727400634766" />
                  <Point X="-27.99652734375" Y="2.729310302734" />
                  <Point X="-27.976435546875" Y="2.7342265625" />
                  <Point X="-27.957" Y="2.741300292969" />
                  <Point X="-27.938447265625" Y="2.750448974609" />
                  <Point X="-27.929419921875" Y="2.755530517578" />
                  <Point X="-27.911166015625" Y="2.767159423828" />
                  <Point X="-27.90275" Y="2.773190185547" />
                  <Point X="-27.8866171875" Y="2.786136962891" />
                  <Point X="-27.878900390625" Y="2.793052978516" />
                  <Point X="-27.852572265625" Y="2.819380859375" />
                  <Point X="-27.81817578125" Y="2.853776367188" />
                  <Point X="-27.81126171875" Y="2.861489990234" />
                  <Point X="-27.798314453125" Y="2.877622314453" />
                  <Point X="-27.79228125" Y="2.886041015625" />
                  <Point X="-27.78065234375" Y="2.904294921875" />
                  <Point X="-27.7755703125" Y="2.913324462891" />
                  <Point X="-27.766423828125" Y="2.931873779297" />
                  <Point X="-27.759353515625" Y="2.951297851562" />
                  <Point X="-27.754435546875" Y="2.971386962891" />
                  <Point X="-27.7525234375" Y="2.981571777344" />
                  <Point X="-27.749697265625" Y="3.003030273438" />
                  <Point X="-27.748908203125" Y="3.013363769531" />
                  <Point X="-27.74845703125" Y="3.03405078125" />
                  <Point X="-27.748794921875" Y="3.044404296875" />
                  <Point X="-27.752041015625" Y="3.08149609375" />
                  <Point X="-27.756279296875" Y="3.129953857422" />
                  <Point X="-27.757744140625" Y="3.140211669922" />
                  <Point X="-27.76178125" Y="3.160500244141" />
                  <Point X="-27.764353515625" Y="3.170531005859" />
                  <Point X="-27.77086328125" Y="3.191174316406" />
                  <Point X="-27.77451171875" Y="3.200867919922" />
                  <Point X="-27.782841796875" Y="3.219797363281" />
                  <Point X="-27.7875234375" Y="3.229033203125" />
                  <Point X="-28.001150390625" Y="3.599043701172" />
                  <Point X="-28.05938671875" Y="3.699915039062" />
                  <Point X="-27.969751953125" Y="3.768638427734" />
                  <Point X="-27.6483671875" Y="4.015041015625" />
                  <Point X="-27.381025390625" Y="4.1635703125" />
                  <Point X="-27.192525390625" Y="4.268296875" />
                  <Point X="-27.118564453125" Y="4.17191015625" />
                  <Point X="-27.1118203125" Y="4.164048828125" />
                  <Point X="-27.097517578125" Y="4.149107421875" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.06508984375" Y="4.121895996094" />
                  <Point X="-27.047888671875" Y="4.110403320312" />
                  <Point X="-27.0389765625" Y="4.105128417969" />
                  <Point X="-26.997693359375" Y="4.083637939453" />
                  <Point X="-26.943759765625" Y="4.055561767578" />
                  <Point X="-26.93432421875" Y="4.051286621094" />
                  <Point X="-26.915044921875" Y="4.043789550781" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031378173828" />
                  <Point X="-26.780818359375" Y="4.035136474609" />
                  <Point X="-26.770728515625" Y="4.037488525391" />
                  <Point X="-26.7508671875" Y="4.043277587891" />
                  <Point X="-26.741095703125" Y="4.046714355469" />
                  <Point X="-26.69809765625" Y="4.064525390625" />
                  <Point X="-26.641921875" Y="4.087793945312" />
                  <Point X="-26.63258203125" Y="4.092274169922" />
                  <Point X="-26.614447265625" Y="4.102224121094" />
                  <Point X="-26.60565234375" Y="4.107693359375" />
                  <Point X="-26.587923828125" Y="4.120107421875" />
                  <Point X="-26.579779296875" Y="4.126499023438" />
                  <Point X="-26.564228515625" Y="4.140135742188" />
                  <Point X="-26.550251953125" Y="4.15538671875" />
                  <Point X="-26.538021484375" Y="4.17206640625" />
                  <Point X="-26.532361328125" Y="4.180740234375" />
                  <Point X="-26.5215390625" Y="4.199483886719" />
                  <Point X="-26.51685546875" Y="4.2087265625" />
                  <Point X="-26.5085234375" Y="4.227663574219" />
                  <Point X="-26.504875" Y="4.237357910156" />
                  <Point X="-26.490880859375" Y="4.281745605469" />
                  <Point X="-26.472595703125" Y="4.339734375" />
                  <Point X="-26.470025390625" Y="4.349764160156" />
                  <Point X="-26.465990234375" Y="4.370048339844" />
                  <Point X="-26.464525390625" Y="4.380302734375" />
                  <Point X="-26.462638671875" Y="4.401865234375" />
                  <Point X="-26.46230078125" Y="4.412218261719" />
                  <Point X="-26.462751953125" Y="4.432898925781" />
                  <Point X="-26.463541015625" Y="4.4432265625" />
                  <Point X="-26.479263671875" Y="4.562655761719" />
                  <Point X="-26.347216796875" Y="4.599677246094" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.607076171875" Y="4.754251464844" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.309453125" Y="4.574419433594" />
                  <Point X="-25.22566796875" Y="4.261728515625" />
                  <Point X="-25.220435546875" Y="4.247106933594" />
                  <Point X="-25.20766015625" Y="4.218912597656" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.65255859375" Y="4.670235839844" />
                  <Point X="-24.621806640625" Y="4.785005859375" />
                  <Point X="-24.535400390625" Y="4.77595703125" />
                  <Point X="-24.172123046875" Y="4.737912109375" />
                  <Point X="-23.9039765625" Y="4.673173339844" />
                  <Point X="-23.54640625" Y="4.586844726562" />
                  <Point X="-23.373677734375" Y="4.524194824219" />
                  <Point X="-23.141734375" Y="4.440067382812" />
                  <Point X="-22.972966796875" Y="4.361140625" />
                  <Point X="-22.749546875" Y="4.256654296875" />
                  <Point X="-22.58644921875" Y="4.161633300781" />
                  <Point X="-22.3705703125" Y="4.035861572266" />
                  <Point X="-22.216791015625" Y="3.926501953125" />
                  <Point X="-22.18221875" Y="3.901915283203" />
                  <Point X="-22.561111328125" Y="3.245651855469" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.937619140625" Y="2.593114257812" />
                  <Point X="-22.9468125" Y="2.573447998047" />
                  <Point X="-22.955814453125" Y="2.549569091797" />
                  <Point X="-22.958697265625" Y="2.540599853516" />
                  <Point X="-22.968005859375" Y="2.505790039062" />
                  <Point X="-22.98016796875" Y="2.460313476562" />
                  <Point X="-22.9820859375" Y="2.451470458984" />
                  <Point X="-22.986353515625" Y="2.420223876953" />
                  <Point X="-22.987244140625" Y="2.383237548828" />
                  <Point X="-22.986587890625" Y="2.369573730469" />
                  <Point X="-22.98295703125" Y="2.339473144531" />
                  <Point X="-22.978216796875" Y="2.300148925781" />
                  <Point X="-22.976197265625" Y="2.289025878906" />
                  <Point X="-22.970853515625" Y="2.267105224609" />
                  <Point X="-22.967529296875" Y="2.256307617188" />
                  <Point X="-22.95926171875" Y="2.234216064453" />
                  <Point X="-22.9546796875" Y="2.223887939453" />
                  <Point X="-22.944318359375" Y="2.203844726562" />
                  <Point X="-22.9385390625" Y="2.194129638672" />
                  <Point X="-22.9199140625" Y="2.166680908203" />
                  <Point X="-22.895580078125" Y="2.130821044922" />
                  <Point X="-22.890193359375" Y="2.123636962891" />
                  <Point X="-22.869537109375" Y="2.100125" />
                  <Point X="-22.842404296875" Y="2.075388916016" />
                  <Point X="-22.83174609375" Y="2.066983398438" />
                  <Point X="-22.804298828125" Y="2.048358154297" />
                  <Point X="-22.7684375" Y="2.024026000977" />
                  <Point X="-22.758720703125" Y="2.01824597168" />
                  <Point X="-22.738671875" Y="2.007881103516" />
                  <Point X="-22.72833984375" Y="2.003296508789" />
                  <Point X="-22.706251953125" Y="1.99503112793" />
                  <Point X="-22.69544921875" Y="1.991706420898" />
                  <Point X="-22.67352734375" Y="1.986364501953" />
                  <Point X="-22.662408203125" Y="1.984346801758" />
                  <Point X="-22.63230859375" Y="1.980717163086" />
                  <Point X="-22.592984375" Y="1.975975219727" />
                  <Point X="-22.583955078125" Y="1.975320922852" />
                  <Point X="-22.55242578125" Y="1.975497314453" />
                  <Point X="-22.5156875" Y="1.979822387695" />
                  <Point X="-22.50225390625" Y="1.982395507812" />
                  <Point X="-22.467443359375" Y="1.991704101562" />
                  <Point X="-22.421966796875" Y="2.003865112305" />
                  <Point X="-22.416005859375" Y="2.005670654297" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572875977" />
                  <Point X="-22.36396484375" Y="2.027872680664" />
                  <Point X="-21.51863671875" Y="2.515923583984" />
                  <Point X="-21.059595703125" Y="2.780950683594" />
                  <Point X="-20.9560390625" Y="2.637031494141" />
                  <Point X="-20.8703203125" Y="2.495377929688" />
                  <Point X="-20.863115234375" Y="2.483470947266" />
                  <Point X="-21.335197265625" Y="2.121228759766" />
                  <Point X="-21.827046875" Y="1.743820068359" />
                  <Point X="-21.83186328125" Y="1.739867675781" />
                  <Point X="-21.847876953125" Y="1.725219482422" />
                  <Point X="-21.865330078125" Y="1.706603759766" />
                  <Point X="-21.871421875" Y="1.699422119141" />
                  <Point X="-21.896474609375" Y="1.666738891602" />
                  <Point X="-21.929203125" Y="1.624040527344" />
                  <Point X="-21.934361328125" Y="1.616605224609" />
                  <Point X="-21.9502578125" Y="1.589373779297" />
                  <Point X="-21.965234375" Y="1.555548828125" />
                  <Point X="-21.969859375" Y="1.542673095703" />
                  <Point X="-21.97919140625" Y="1.509303588867" />
                  <Point X="-21.991384765625" Y="1.465708618164" />
                  <Point X="-21.993775390625" Y="1.454661987305" />
                  <Point X="-21.997228515625" Y="1.432364624023" />
                  <Point X="-21.998291015625" Y="1.421113891602" />
                  <Point X="-21.999107421875" Y="1.397542358398" />
                  <Point X="-21.998826171875" Y="1.38623840332" />
                  <Point X="-21.996921875" Y="1.363749633789" />
                  <Point X="-21.995298828125" Y="1.352564453125" />
                  <Point X="-21.98763671875" Y="1.315436645508" />
                  <Point X="-21.97762890625" Y="1.266931762695" />
                  <Point X="-21.975400390625" Y="1.258231201172" />
                  <Point X="-21.965318359375" Y="1.228617553711" />
                  <Point X="-21.949716796875" Y="1.195377197266" />
                  <Point X="-21.943083984375" Y="1.18352734375" />
                  <Point X="-21.922248046875" Y="1.151856933594" />
                  <Point X="-21.89502734375" Y="1.110482055664" />
                  <Point X="-21.88826171875" Y="1.101423461914" />
                  <Point X="-21.87370703125" Y="1.084178222656" />
                  <Point X="-21.86591796875" Y="1.075991333008" />
                  <Point X="-21.848673828125" Y="1.059901245117" />
                  <Point X="-21.83996484375" Y="1.052694824219" />
                  <Point X="-21.82175390625" Y="1.039368652344" />
                  <Point X="-21.812251953125" Y="1.033248779297" />
                  <Point X="-21.782056640625" Y="1.01625177002" />
                  <Point X="-21.742609375" Y="0.994046630859" />
                  <Point X="-21.734517578125" Y="0.989986816406" />
                  <Point X="-21.705318359375" Y="0.978083618164" />
                  <Point X="-21.66972265625" Y="0.968021118164" />
                  <Point X="-21.656328125" Y="0.965257751465" />
                  <Point X="-21.615501953125" Y="0.959861877441" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.555966796875" Y="0.952199768066" />
                  <Point X="-21.53428125" Y="0.951222839355" />
                  <Point X="-21.50878125" Y="0.952032409668" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-20.696388671875" Y="1.058515014648" />
                  <Point X="-20.295296875" Y="1.111319702148" />
                  <Point X="-20.247310546875" Y="0.914210083008" />
                  <Point X="-20.220298828125" Y="0.740717163086" />
                  <Point X="-20.216126953125" Y="0.713921508789" />
                  <Point X="-20.744337890625" Y="0.572387451172" />
                  <Point X="-21.3080078125" Y="0.421352752686" />
                  <Point X="-21.31396875" Y="0.419544311523" />
                  <Point X="-21.334376953125" Y="0.412136199951" />
                  <Point X="-21.357619140625" Y="0.401618469238" />
                  <Point X="-21.365994140625" Y="0.39731640625" />
                  <Point X="-21.406103515625" Y="0.374132293701" />
                  <Point X="-21.45850390625" Y="0.343843994141" />
                  <Point X="-21.4661171875" Y="0.338945831299" />
                  <Point X="-21.491224609375" Y="0.31987008667" />
                  <Point X="-21.51800390625" Y="0.294350952148" />
                  <Point X="-21.52719921875" Y="0.284228088379" />
                  <Point X="-21.551265625" Y="0.253562423706" />
                  <Point X="-21.582705078125" Y="0.213500244141" />
                  <Point X="-21.589140625" Y="0.204213989258" />
                  <Point X="-21.6008671875" Y="0.184932693481" />
                  <Point X="-21.606158203125" Y="0.174937667847" />
                  <Point X="-21.615931640625" Y="0.153474441528" />
                  <Point X="-21.61999609375" Y="0.142925628662" />
                  <Point X="-21.626841796875" Y="0.121424064636" />
                  <Point X="-21.629623046875" Y="0.110471305847" />
                  <Point X="-21.63764453125" Y="0.068583732605" />
                  <Point X="-21.648125" Y="0.013860787392" />
                  <Point X="-21.649396484375" Y="0.004966154575" />
                  <Point X="-21.6514140625" Y="-0.026261293411" />
                  <Point X="-21.64971875" Y="-0.062939338684" />
                  <Point X="-21.648125" Y="-0.076420921326" />
                  <Point X="-21.640103515625" Y="-0.11830834198" />
                  <Point X="-21.629623046875" Y="-0.173031280518" />
                  <Point X="-21.626841796875" Y="-0.183983901978" />
                  <Point X="-21.61999609375" Y="-0.205485610962" />
                  <Point X="-21.615931640625" Y="-0.216034576416" />
                  <Point X="-21.606158203125" Y="-0.237497787476" />
                  <Point X="-21.6008671875" Y="-0.247492538452" />
                  <Point X="-21.589140625" Y="-0.266773986816" />
                  <Point X="-21.582705078125" Y="-0.276060668945" />
                  <Point X="-21.558638671875" Y="-0.306726165771" />
                  <Point X="-21.52719921875" Y="-0.346788360596" />
                  <Point X="-21.52128125" Y="-0.353633178711" />
                  <Point X="-21.498859375" Y="-0.375804443359" />
                  <Point X="-21.469822265625" Y="-0.398724487305" />
                  <Point X="-21.45850390625" Y="-0.406404266357" />
                  <Point X="-21.41839453125" Y="-0.429588256836" />
                  <Point X="-21.365994140625" Y="-0.459876556396" />
                  <Point X="-21.360505859375" Y="-0.462813323975" />
                  <Point X="-21.34084375" Y="-0.472015930176" />
                  <Point X="-21.31697265625" Y="-0.481027526855" />
                  <Point X="-21.3080078125" Y="-0.483912872314" />
                  <Point X="-20.571615234375" Y="-0.681228759766" />
                  <Point X="-20.215123046875" Y="-0.776750610352" />
                  <Point X="-20.238392578125" Y="-0.931085144043" />
                  <Point X="-20.2721953125" Y="-1.079219726563" />
                  <Point X="-20.90951953125" Y="-0.99531451416" />
                  <Point X="-21.563216796875" Y="-0.909253540039" />
                  <Point X="-21.571375" Y="-0.908535705566" />
                  <Point X="-21.59990234375" Y="-0.908042419434" />
                  <Point X="-21.63327734375" Y="-0.910841003418" />
                  <Point X="-21.645517578125" Y="-0.912676330566" />
                  <Point X="-21.72423828125" Y="-0.929786682129" />
                  <Point X="-21.82708203125" Y="-0.952140014648" />
                  <Point X="-21.842125" Y="-0.956742431641" />
                  <Point X="-21.8712421875" Y="-0.968365600586" />
                  <Point X="-21.885318359375" Y="-0.97538671875" />
                  <Point X="-21.913146484375" Y="-0.992279174805" />
                  <Point X="-21.925873046875" Y="-1.001528442383" />
                  <Point X="-21.949625" Y="-1.022000366211" />
                  <Point X="-21.9606484375" Y="-1.03322277832" />
                  <Point X="-22.00823046875" Y="-1.090448974609" />
                  <Point X="-22.070392578125" Y="-1.16521081543" />
                  <Point X="-22.078671875" Y="-1.176844970703" />
                  <Point X="-22.093396484375" Y="-1.201232421875" />
                  <Point X="-22.09983984375" Y="-1.213985229492" />
                  <Point X="-22.111181640625" Y="-1.241370849609" />
                  <Point X="-22.115640625" Y="-1.254940673828" />
                  <Point X="-22.12246875" Y="-1.282584838867" />
                  <Point X="-22.12483984375" Y="-1.296659301758" />
                  <Point X="-22.13166015625" Y="-1.370769775391" />
                  <Point X="-22.140568359375" Y="-1.467589599609" />
                  <Point X="-22.140708984375" Y="-1.483314453125" />
                  <Point X="-22.138392578125" Y="-1.514580932617" />
                  <Point X="-22.135935546875" Y="-1.530122680664" />
                  <Point X="-22.128205078125" Y="-1.561743530273" />
                  <Point X="-22.123212890625" Y="-1.57666809082" />
                  <Point X="-22.11083984375" Y="-1.605481323242" />
                  <Point X="-22.103458984375" Y="-1.619370239258" />
                  <Point X="-22.05989453125" Y="-1.687133300781" />
                  <Point X="-22.002978515625" Y="-1.775660644531" />
                  <Point X="-21.9982578125" Y="-1.782352416992" />
                  <Point X="-21.98019921875" Y="-1.804457397461" />
                  <Point X="-21.956505859375" Y="-1.828123291016" />
                  <Point X="-21.947201171875" Y="-1.836277709961" />
                  <Point X="-21.26382421875" Y="-2.360652099609" />
                  <Point X="-20.912828125" Y="-2.629981689453" />
                  <Point X="-20.954517578125" Y="-2.697441650391" />
                  <Point X="-20.998724609375" Y="-2.760252197266" />
                  <Point X="-21.569337890625" Y="-2.430807617188" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.184970703125" Y="-2.079513916016" />
                  <Point X="-22.216876953125" Y="-2.069326416016" />
                  <Point X="-22.228888671875" Y="-2.066337646484" />
                  <Point X="-22.322578125" Y="-2.049417236328" />
                  <Point X="-22.444978515625" Y="-2.027312011719" />
                  <Point X="-22.460638671875" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.55515625" Y="-2.035136352539" />
                  <Point X="-22.584931640625" Y="-2.044960083008" />
                  <Point X="-22.59941015625" Y="-2.051108642578" />
                  <Point X="-22.677244140625" Y="-2.092072021484" />
                  <Point X="-22.778927734375" Y="-2.145587646484" />
                  <Point X="-22.79103125" Y="-2.153171630859" />
                  <Point X="-22.813962890625" Y="-2.170066162109" />
                  <Point X="-22.824791015625" Y="-2.179376708984" />
                  <Point X="-22.84575" Y="-2.200336669922" />
                  <Point X="-22.855060546875" Y="-2.211164306641" />
                  <Point X="-22.871951171875" Y="-2.234092285156" />
                  <Point X="-22.87953125" Y="-2.246192626953" />
                  <Point X="-22.92049609375" Y="-2.324026367188" />
                  <Point X="-22.97401171875" Y="-2.425710693359" />
                  <Point X="-22.98016015625" Y="-2.440187988281" />
                  <Point X="-22.989986328125" Y="-2.469967041016" />
                  <Point X="-22.9936640625" Y="-2.485268798828" />
                  <Point X="-22.99862109375" Y="-2.51744140625" />
                  <Point X="-22.999720703125" Y="-2.5331328125" />
                  <Point X="-22.99931640625" Y="-2.564484619141" />
                  <Point X="-22.9978125" Y="-2.580145019531" />
                  <Point X="-22.980890625" Y="-2.673835449219" />
                  <Point X="-22.95878515625" Y="-2.796235351563" />
                  <Point X="-22.95698046875" Y="-2.804232177734" />
                  <Point X="-22.94876171875" Y="-2.831539794922" />
                  <Point X="-22.935927734375" Y="-2.862478759766" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.491310546875" Y="-3.634188964844" />
                  <Point X="-22.264103515625" Y="-4.02772265625" />
                  <Point X="-22.276244140625" Y="-4.036083496094" />
                  <Point X="-22.721806640625" Y="-3.455415527344" />
                  <Point X="-23.16608203125" Y="-2.876422851562" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849625732422" />
                  <Point X="-23.21674609375" Y="-2.828004638672" />
                  <Point X="-23.22669921875" Y="-2.820646972656" />
                  <Point X="-23.319103515625" Y="-2.761239501953" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.453716796875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663873535156" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.6926640625" Y="-2.655815917969" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920125" Y="-2.699414794922" />
                  <Point X="-23.944505859375" Y="-2.714135742188" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-24.034173828125" Y="-2.787297363281" />
                  <Point X="-24.136123046875" Y="-2.872063720703" />
                  <Point X="-24.147345703125" Y="-2.883087646484" />
                  <Point X="-24.16781640625" Y="-2.906837158203" />
                  <Point X="-24.177064453125" Y="-2.919562744141" />
                  <Point X="-24.19395703125" Y="-2.947389404297" />
                  <Point X="-24.20098046875" Y="-2.961466796875" />
                  <Point X="-24.21260546875" Y="-2.990587890625" />
                  <Point X="-24.21720703125" Y="-3.005631591797" />
                  <Point X="-24.2405390625" Y="-3.112978515625" />
                  <Point X="-24.271021484375" Y="-3.253219482422" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323170166016" />
                  <Point X="-24.2744453125" Y="-3.335520507812" />
                  <Point X="-24.16691015625" Y="-4.152323242188" />
                  <Point X="-24.171251953125" Y="-4.136120605469" />
                  <Point X="-24.344931640625" Y="-3.487937011719" />
                  <Point X="-24.347392578125" Y="-3.480122314453" />
                  <Point X="-24.357853515625" Y="-3.453578857422" />
                  <Point X="-24.3732109375" Y="-3.423815429688" />
                  <Point X="-24.37958984375" Y="-3.413209228516" />
                  <Point X="-24.450578125" Y="-3.3109296875" />
                  <Point X="-24.543318359375" Y="-3.177308837891" />
                  <Point X="-24.553328125" Y="-3.165171386719" />
                  <Point X="-24.575212890625" Y="-3.142715820312" />
                  <Point X="-24.587087890625" Y="-3.132397705078" />
                  <Point X="-24.61334375" Y="-3.113153808594" />
                  <Point X="-24.6267578125" Y="-3.104937255859" />
                  <Point X="-24.6547578125" Y="-3.090829589844" />
                  <Point X="-24.66934375" Y="-3.084938476562" />
                  <Point X="-24.779193359375" Y="-3.050845214844" />
                  <Point X="-24.922703125" Y="-3.006305175781" />
                  <Point X="-24.936623046875" Y="-3.003109130859" />
                  <Point X="-24.964783203125" Y="-2.99883984375" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.998840087891" />
                  <Point X="-25.051064453125" Y="-3.003109619141" />
                  <Point X="-25.064984375" Y="-3.006305664062" />
                  <Point X="-25.17483203125" Y="-3.040398681641" />
                  <Point X="-25.31834375" Y="-3.084938964844" />
                  <Point X="-25.3329296875" Y="-3.090829345703" />
                  <Point X="-25.3609296875" Y="-3.104936767578" />
                  <Point X="-25.37434375" Y="-3.113153808594" />
                  <Point X="-25.400599609375" Y="-3.132397705078" />
                  <Point X="-25.412474609375" Y="-3.142717285156" />
                  <Point X="-25.434359375" Y="-3.165173828125" />
                  <Point X="-25.444369140625" Y="-3.177310791016" />
                  <Point X="-25.51535546875" Y="-3.279590332031" />
                  <Point X="-25.608095703125" Y="-3.413211181641" />
                  <Point X="-25.61246875" Y="-3.420132080078" />
                  <Point X="-25.625974609375" Y="-3.445261230469" />
                  <Point X="-25.63877734375" Y="-3.476213867188" />
                  <Point X="-25.64275390625" Y="-3.487937011719" />
                  <Point X="-25.869654296875" Y="-4.3347421875" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.121061225234" Y="-2.397176783564" />
                  <Point X="-28.727895046161" Y="-2.980069614796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.045551843682" Y="-2.339236338661" />
                  <Point X="-28.645421743706" Y="-2.932453607213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.67311495013" Y="-1.238948065016" />
                  <Point X="-29.642541526236" Y="-1.284275029955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.970042462131" Y="-2.281295893759" />
                  <Point X="-28.562948441252" Y="-2.884837599629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.954815971269" Y="-3.78643106331" />
                  <Point X="-27.863359253829" Y="-3.922021222896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.741733577482" Y="-0.967329059643" />
                  <Point X="-29.537296648695" Y="-1.270419270786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.894533080579" Y="-2.223355448857" />
                  <Point X="-28.480475138797" Y="-2.837221592046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.901967416413" Y="-3.694894561236" />
                  <Point X="-27.646371537367" Y="-4.073831035221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.772567471961" Y="-0.751728224435" />
                  <Point X="-29.432051771154" Y="-1.256563511616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.819023699027" Y="-2.165415003954" />
                  <Point X="-28.398001832097" Y="-2.789605590755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.849118861556" Y="-3.603358059162" />
                  <Point X="-27.486590675568" Y="-4.140828197692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.733096647749" Y="-0.640358421058" />
                  <Point X="-29.326806893613" Y="-1.242707752447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.743514317476" Y="-2.107474559052" />
                  <Point X="-28.31552851125" Y="-2.74198961044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.796270306699" Y="-3.511821557087" />
                  <Point X="-27.425606363315" Y="-4.061353451983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.63604625183" Y="-0.614353843281" />
                  <Point X="-29.221562016073" Y="-1.228851993278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.668004935924" Y="-2.04953411415" />
                  <Point X="-28.233055190403" Y="-2.694373630124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.743421751842" Y="-3.420285055013" />
                  <Point X="-27.358074271578" Y="-3.991586188568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.53899585591" Y="-0.588349265503" />
                  <Point X="-29.116317141946" Y="-1.214996229046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.592495549475" Y="-1.991593676509" />
                  <Point X="-28.150581869556" Y="-2.646757649809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.690573196985" Y="-3.328748552939" />
                  <Point X="-27.279216077346" Y="-3.938610562636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.44194545999" Y="-0.562344687725" />
                  <Point X="-29.01107227159" Y="-1.201140459225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.516986149761" Y="-1.933653258533" />
                  <Point X="-28.068108548708" Y="-2.599141669493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.637724608998" Y="-3.237212099982" />
                  <Point X="-27.200225924448" Y="-3.885830573473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.34489506407" Y="-0.536340109947" />
                  <Point X="-28.905827401235" Y="-1.187284689403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.441476750047" Y="-1.875712840558" />
                  <Point X="-27.985635227861" Y="-2.551525689178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.584875988149" Y="-3.145675695745" />
                  <Point X="-27.121235800046" Y="-3.833050542061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.24784466815" Y="-0.51033553217" />
                  <Point X="-28.800582530879" Y="-1.173428919581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.365967350333" Y="-1.817772422582" />
                  <Point X="-27.903161907014" Y="-2.503909708862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.5320273673" Y="-3.054139291508" />
                  <Point X="-27.036047687156" Y="-3.789459406465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.150794272516" Y="-0.484330953969" />
                  <Point X="-28.695337660523" Y="-1.159573149759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.290457950619" Y="-1.759832004607" />
                  <Point X="-27.820688586167" Y="-2.456293728547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.479178746451" Y="-2.962602887271" />
                  <Point X="-26.929887443937" Y="-3.776960732722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.781241760065" Y="0.620233590516" />
                  <Point X="-29.756925370773" Y="0.584183060857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.053743910839" Y="-0.458326325424" />
                  <Point X="-28.590092790168" Y="-1.145717379938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.214948550905" Y="-1.701891586631" />
                  <Point X="-27.738215265319" Y="-2.408677748231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.426330125602" Y="-2.871066483035" />
                  <Point X="-26.809996466693" Y="-3.784818709315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.760625740572" Y="0.759556791436" />
                  <Point X="-29.617055426006" Y="0.546705046824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.956693549161" Y="-0.432321696879" />
                  <Point X="-28.484847919812" Y="-1.131861610116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.139439151191" Y="-1.643951168656" />
                  <Point X="-27.653126105586" Y="-2.364939908549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.373481504753" Y="-2.779530078798" />
                  <Point X="-26.69010542383" Y="-3.79267678319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.12748322085" Y="-4.626798501346" />
                  <Point X="-26.040220743378" Y="-4.756170444462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.740009641617" Y="0.89887987455" />
                  <Point X="-29.477185481238" Y="0.509227032791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.859643187484" Y="-0.406317068335" />
                  <Point X="-28.379603049456" Y="-1.118005840294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.063929751477" Y="-1.586010750681" />
                  <Point X="-27.54982772958" Y="-2.348198342178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.324421742072" Y="-2.682376461326" />
                  <Point X="-26.558694400867" Y="-3.81761392992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.133102982792" Y="-4.448579154892" />
                  <Point X="-25.966386850484" Y="-4.695745985473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.712313749182" Y="1.027706732186" />
                  <Point X="-29.337315532331" Y="0.47174901262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.762592825807" Y="-0.38031243979" />
                  <Point X="-28.274358179101" Y="-1.104150070472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.99698171171" Y="-1.515377594609" />
                  <Point X="-27.361332811991" Y="-2.457765843011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.34697887258" Y="-2.479046433327" />
                  <Point X="-26.309369449762" Y="-4.017365664157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.181031211701" Y="-4.207634926674" />
                  <Point X="-25.933807231147" Y="-4.574159550725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.679471837731" Y="1.148904302884" />
                  <Point X="-29.197445536855" Y="0.43427092341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.66554246413" Y="-0.354307811245" />
                  <Point X="-28.157670441644" Y="-1.107259048783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.953496925953" Y="-1.40995873395" />
                  <Point X="-25.901227611809" Y="-4.452573115976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.646629766945" Y="1.27010163736" />
                  <Point X="-29.05757554138" Y="0.396792834199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.568492102453" Y="-0.328303182701" />
                  <Point X="-25.868648007631" Y="-4.330986658752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.557774820092" Y="1.308256468043" />
                  <Point X="-28.917705545904" Y="0.359314744988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.473791898513" Y="-0.298814302024" />
                  <Point X="-25.836068879117" Y="-4.209399496329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.432016712487" Y="1.291700112982" />
                  <Point X="-28.777835550429" Y="0.321836655777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.393894366627" Y="-0.247379557532" />
                  <Point X="-25.803489750602" Y="-4.087812333906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.306258604883" Y="1.27514375792" />
                  <Point X="-28.637965554953" Y="0.284358566567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.330650707115" Y="-0.171254431882" />
                  <Point X="-25.770910622087" Y="-3.966225171483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.180500483502" Y="1.258587382435" />
                  <Point X="-28.497453482225" Y="0.245928558682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.29534459193" Y="-0.053710193459" />
                  <Point X="-25.738331493572" Y="-3.84463800906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.054742346389" Y="1.242030983625" />
                  <Point X="-25.705752365057" Y="-3.723050846637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.928984209275" Y="1.225474584815" />
                  <Point X="-25.673173236543" Y="-3.601463684214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.803226072162" Y="1.208918186005" />
                  <Point X="-25.640211932393" Y="-3.48044312047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.690043947814" Y="1.21100649286" />
                  <Point X="-25.589267595894" Y="-3.386083498583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.595732733662" Y="1.241072074613" />
                  <Point X="-25.531153918707" Y="-3.302352861369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.210878595123" Y="2.322951025505" />
                  <Point X="-29.15283960874" Y="2.236904689641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.51623334203" Y="1.293097086307" />
                  <Point X="-25.473040626581" Y="-3.21862165328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.15771882606" Y="2.414026133545" />
                  <Point X="-28.915311948215" Y="2.054643157953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.460027969927" Y="1.379656902154" />
                  <Point X="-25.410692603815" Y="-3.14116869155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.10455899154" Y="2.50510114454" />
                  <Point X="-28.677783906626" Y="1.872381061312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.423197001358" Y="1.494940452467" />
                  <Point X="-25.33067136751" Y="-3.0899173464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.05139915702" Y="2.596176155536" />
                  <Point X="-25.236559393025" Y="-3.059556379694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.997614538348" Y="2.686324885934" />
                  <Point X="-25.141804796744" Y="-3.03014813898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.936237480209" Y="2.765217361921" />
                  <Point X="-25.045972535194" Y="-3.002337602731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.874860422069" Y="2.844109837909" />
                  <Point X="-24.929805622474" Y="-3.004674426616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.174294455439" Y="-4.124765794138" />
                  <Point X="-24.169627226163" Y="-4.131685246092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.813483142627" Y="2.923001985801" />
                  <Point X="-24.78536412944" Y="-3.048930039674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.249816806195" Y="-3.842911597909" />
                  <Point X="-24.197417897399" Y="-3.920596174882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.752105371758" Y="3.001893405124" />
                  <Point X="-24.636207555071" Y="-3.100176048284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.325339156951" Y="-3.56105740168" />
                  <Point X="-24.225208568634" Y="-3.709507103673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.617450303417" Y="2.972146763335" />
                  <Point X="-24.25299923987" Y="-3.498418032463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.429772409722" Y="2.863790550238" />
                  <Point X="-24.275272329881" Y="-3.295509111817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.243092313469" Y="2.756913632682" />
                  <Point X="-24.251082241126" Y="-3.161484686483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.108373388163" Y="2.727072319052" />
                  <Point X="-24.223155588133" Y="-3.032999945444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.995467194834" Y="2.729569710466" />
                  <Point X="-24.180860954363" Y="-2.925816611901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.907839102817" Y="2.769543428243" />
                  <Point X="-24.114598517687" Y="-2.854167007449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.837219468126" Y="2.834733220987" />
                  <Point X="-24.041181744934" Y="-2.79312414242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.775601658625" Y="2.913268768404" />
                  <Point X="-23.967765407034" Y="-2.732080632693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.748760954957" Y="3.043363495524" />
                  <Point X="-23.885630012471" Y="-2.683963656058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.954614385729" Y="3.518441463969" />
                  <Point X="-23.784335206027" Y="-2.664251675658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.995478968558" Y="3.748913406213" />
                  <Point X="-23.676441418915" Y="-2.654323086428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.919948350912" Y="3.806822367311" />
                  <Point X="-23.566754427235" Y="-2.647053032299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.844417569701" Y="3.864731085914" />
                  <Point X="-23.418052248886" Y="-2.697625371105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.76888678849" Y="3.922639804517" />
                  <Point X="-23.213892764509" Y="-2.830416547247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.693356007279" Y="3.980548523119" />
                  <Point X="-22.357469096859" Y="-3.930229142669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.614661666193" Y="4.03376707133" />
                  <Point X="-22.781004632667" Y="-3.132424181754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.531307348524" Y="4.080076920144" />
                  <Point X="-22.982775672981" Y="-2.663398606061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.447953030855" Y="4.126386768957" />
                  <Point X="-22.99226404329" Y="-2.479443811839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.364598715575" Y="4.172696621313" />
                  <Point X="-22.94767305933" Y="-2.375664957458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.281244410029" Y="4.219006488101" />
                  <Point X="-22.897449264258" Y="-2.280237088975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.197890104483" Y="4.265316354888" />
                  <Point X="-22.840376627017" Y="-2.194963046572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.940963600192" Y="4.054294854597" />
                  <Point X="-22.764287277828" Y="-2.137882439051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.810254944275" Y="4.030399009836" />
                  <Point X="-22.679717920368" Y="-2.093373960806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.714187309379" Y="4.057860590748" />
                  <Point X="-22.594921323001" Y="-2.049202379578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.625448747268" Y="4.096187968908" />
                  <Point X="-22.496185094225" Y="-2.025697151792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.550560793203" Y="4.155049717947" />
                  <Point X="-22.371561936545" Y="-2.040570874393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.500610781617" Y="4.250883487139" />
                  <Point X="-22.241075939653" Y="-2.064136613576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.466083056407" Y="4.369581736159" />
                  <Point X="-22.076770617151" Y="-2.137841564889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.478667402498" Y="4.558126503235" />
                  <Point X="-21.889093239483" Y="-2.246197012945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.384964568533" Y="4.589094045707" />
                  <Point X="-21.701415861815" Y="-2.354552461001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.288597679908" Y="4.616111964722" />
                  <Point X="-22.131970548876" Y="-1.546341180408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.924824313375" Y="-1.853448103935" />
                  <Point X="-21.513738348148" Y="-2.462908110684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.192230753976" Y="4.643129828427" />
                  <Point X="-22.132157588608" Y="-1.376176175853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.687296465003" Y="-2.035709914119" />
                  <Point X="-21.326060511413" Y="-2.571264239335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.095863828044" Y="4.670147692132" />
                  <Point X="-22.110159285056" Y="-1.238902295327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.449768616631" Y="-2.217971724303" />
                  <Point X="-21.138382674678" Y="-2.679620367986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.999496902113" Y="4.697165555837" />
                  <Point X="-22.056524362868" Y="-1.148531630764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.212240686455" Y="-2.400233655766" />
                  <Point X="-20.983753155739" Y="-2.738980350598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.900266136484" Y="4.719937602588" />
                  <Point X="-21.993257386842" Y="-1.072441073269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.974712461399" Y="-2.582496024408" />
                  <Point X="-20.927102779456" Y="-2.653080280579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.794059585367" Y="4.732367622049" />
                  <Point X="-21.926268154559" Y="-1.001868987616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.687853034249" Y="4.744797641509" />
                  <Point X="-21.842117287413" Y="-0.956740071964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.581646452925" Y="4.757227616185" />
                  <Point X="-21.742961880838" Y="-0.933856300823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.475439775647" Y="4.769657448605" />
                  <Point X="-25.286849248286" Y="4.490060493708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.014258907676" Y="4.085928694326" />
                  <Point X="-21.642919961852" Y="-0.912286838379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.36923309837" Y="4.782087281026" />
                  <Point X="-25.362370917389" Y="4.771913679345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.910225851525" Y="4.101581052589" />
                  <Point X="-21.527174671661" Y="-0.913998581159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.829214175329" Y="4.151364010214" />
                  <Point X="-21.401416540036" Y="-0.930554971832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.772054029429" Y="4.236508315696" />
                  <Point X="-21.275658408411" Y="-0.947111362504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.737129284464" Y="4.354617958722" />
                  <Point X="-21.149900276786" Y="-0.963667753177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.704549804934" Y="4.476204600744" />
                  <Point X="-21.650223436508" Y="-0.052020458183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.380953233362" Y="-0.451229951351" />
                  <Point X="-21.024142145162" Y="-0.980224143849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.671970325404" Y="4.597791242765" />
                  <Point X="-21.633075272463" Y="0.092444049869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.230267249336" Y="-0.504743403022" />
                  <Point X="-20.898384016212" Y="-0.996780530557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.6393911503" Y="4.719378336118" />
                  <Point X="-22.949817470568" Y="2.21448234512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.883080610409" Y="2.115540881088" />
                  <Point X="-21.591385099076" Y="0.200523532782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.090397270019" Y="-0.542221468278" />
                  <Point X="-20.772625914792" Y="-1.013336876448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.565058207597" Y="4.779062923339" />
                  <Point X="-22.98427352618" Y="2.435453255047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.682899839467" Y="1.98864839019" />
                  <Point X="-21.530455321129" Y="0.280079128926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.950527290701" Y="-0.579699533534" />
                  <Point X="-20.646867813372" Y="-1.02989322234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.441757706808" Y="4.766150120218" />
                  <Point X="-22.952530556685" Y="2.558280074196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.559412257893" Y="1.975458228399" />
                  <Point X="-21.458762389521" Y="0.343677693552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.810657311383" Y="-0.61717759879" />
                  <Point X="-20.521109711952" Y="-1.046449568232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.318457147878" Y="4.7532372309" />
                  <Point X="-22.902610105087" Y="2.654157667874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.45756187684" Y="1.994346535569" />
                  <Point X="-21.982350730927" Y="1.289817038837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.807482302515" Y="1.030563932248" />
                  <Point X="-21.376324027008" Y="0.391345501729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.670787332066" Y="-0.654655664045" />
                  <Point X="-20.395351610533" Y="-1.063005914124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.195156588948" Y="4.740324341582" />
                  <Point X="-22.849761558898" Y="2.745694182798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.365168357724" Y="2.027255217132" />
                  <Point X="-21.989923747152" Y="1.470932203853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.648109335334" Y="0.964171498416" />
                  <Point X="-21.285958520012" Y="0.427260834905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.530917351539" Y="-0.692133731094" />
                  <Point X="-20.271596017227" Y="-1.076593419647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.06081216993" Y="4.711038256355" />
                  <Point X="-22.796913012708" Y="2.837230697722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.282657498623" Y="2.074815544698" />
                  <Point X="-21.952225432034" Y="1.584929860028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.524983747965" Y="0.951518015106" />
                  <Point X="-21.18890810277" Y="0.453265381071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.391047368065" Y="-0.729611802511" />
                  <Point X="-20.242629095253" Y="-0.949650940796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.923930733017" Y="4.677990887421" />
                  <Point X="-22.744064466519" Y="2.928767212646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.200184207282" Y="2.122431568758" />
                  <Point X="-21.894528282679" Y="1.669278025147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.418444331797" Y="0.963454541834" />
                  <Point X="-21.091857685528" Y="0.479269927238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.251177384591" Y="-0.767089873929" />
                  <Point X="-20.22051945292" Y="-0.8125421268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.787049244912" Y="4.644943442592" />
                  <Point X="-22.691215920329" Y="3.02030372757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.117710915941" Y="2.170047592818" />
                  <Point X="-21.829087382215" Y="1.742145607121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.313199452961" Y="0.977310299082" />
                  <Point X="-20.994807268286" Y="0.505274473404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.65016774807" Y="4.611895984811" />
                  <Point X="-22.638367374139" Y="3.111840242494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.0352376246" Y="2.217663616878" />
                  <Point X="-21.753626246967" Y="1.800157580111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.207954574124" Y="0.99116605633" />
                  <Point X="-20.897756851044" Y="0.531279019571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.509699472186" Y="4.573530908418" />
                  <Point X="-22.58551882795" Y="3.203376757418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.952764333259" Y="2.265279640939" />
                  <Point X="-21.678116785611" Y="1.858097906698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.102709695287" Y="1.005021813578" />
                  <Point X="-20.800706433802" Y="0.557283565737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.357994199308" Y="4.518506298878" />
                  <Point X="-22.532670309128" Y="3.294913312917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.870291041918" Y="2.312895664999" />
                  <Point X="-21.602607324255" Y="1.916038233286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.99746481645" Y="1.018877570827" />
                  <Point X="-20.703656040876" Y="0.583288147953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.206288989299" Y="4.463481782546" />
                  <Point X="-22.479821813794" Y="3.386449903237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.787817750577" Y="2.360511689059" />
                  <Point X="-21.527097862899" Y="1.973978559873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.892219937614" Y="1.032733328075" />
                  <Point X="-20.606605681641" Y="0.609292780119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.045570830184" Y="4.395095019658" />
                  <Point X="-22.426973318459" Y="3.477986493556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.705344459236" Y="2.408127713119" />
                  <Point X="-21.451588401543" Y="2.031918886461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.786975058777" Y="1.046589085323" />
                  <Point X="-20.509555322407" Y="0.635297412285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.878176321418" Y="4.316810161366" />
                  <Point X="-22.374124823125" Y="3.569523083876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.622871167895" Y="2.45574373718" />
                  <Point X="-21.376078940187" Y="2.089859213048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.681730178298" Y="1.060444840136" />
                  <Point X="-20.412504963172" Y="0.661302044451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.705830917549" Y="4.231185299235" />
                  <Point X="-22.32127632779" Y="3.661059674195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.540397876554" Y="2.50335976124" />
                  <Point X="-21.300569532971" Y="2.147799619901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.576485287671" Y="1.074300579904" />
                  <Point X="-20.315454603937" Y="0.687306676617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.51705812487" Y="4.121205831639" />
                  <Point X="-22.268427832455" Y="3.752596264515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.457924545629" Y="2.550975726615" />
                  <Point X="-21.225060189672" Y="2.205740121515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.471240397044" Y="1.088156319672" />
                  <Point X="-20.218404244703" Y="0.713311308783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.321239041146" Y="4.000779807967" />
                  <Point X="-22.215579337121" Y="3.844132854834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.375451200517" Y="2.598591670956" />
                  <Point X="-21.149550846372" Y="2.263680623128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.365995506416" Y="1.10201205944" />
                  <Point X="-20.251822554427" Y="0.93274369716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.292977855404" Y="2.646207615297" />
                  <Point X="-21.074041503073" Y="2.321621124742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.210504510292" Y="2.693823559638" />
                  <Point X="-20.998532159774" Y="2.379561626356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.12803116518" Y="2.741439503979" />
                  <Point X="-20.923022816474" Y="2.437502127969" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.354779296875" Y="-4.185296386719" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.60666796875" Y="-3.419264648438" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.835513671875" Y="-3.232306640625" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.11851171875" Y="-3.221859619141" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643798828" />
                  <Point X="-25.359265625" Y="-3.387923339844" />
                  <Point X="-25.452005859375" Y="-3.521544189453" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.686126953125" Y="-4.38391796875" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.931939453125" Y="-4.970733886719" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.2269921875" Y="-4.905454589844" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.330056640625" Y="-4.70984375" />
                  <Point X="-26.3091484375" Y="-4.5510390625" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.33592578125" Y="-4.402825195312" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.48741796875" Y="-4.113935546875" />
                  <Point X="-26.61954296875" Y="-3.998064208984" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.78346875" Y="-3.976965087891" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791015625" />
                  <Point X="-27.101724609375" Y="-4.048524658203" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.38712109375" Y="-4.32330859375" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.609130859375" Y="-4.320362304688" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.03133984375" Y="-4.032476074219" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.828798828125" Y="-3.188165527344" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597593994141" />
                  <Point X="-27.513978515625" Y="-2.568765136719" />
                  <Point X="-27.531326171875" Y="-2.551416503906" />
                  <Point X="-27.560154296875" Y="-2.537198730469" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.32203515625" Y="-2.965139404297" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-28.966796875" Y="-3.103197753906" />
                  <Point X="-29.161697265625" Y="-2.847135986328" />
                  <Point X="-29.28752734375" Y="-2.636140380859" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-28.728521484375" Y="-1.856480957031" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.396018188477" />
                  <Point X="-28.138115234375" Y="-1.366264038086" />
                  <Point X="-28.140326171875" Y="-1.334594970703" />
                  <Point X="-28.161158203125" Y="-1.310639770508" />
                  <Point X="-28.187640625" Y="-1.295053100586" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.141529296875" Y="-1.409954956055" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.851162109375" Y="-1.309627685547" />
                  <Point X="-29.927392578125" Y="-1.011188171387" />
                  <Point X="-29.960681640625" Y="-0.778430725098" />
                  <Point X="-29.99839453125" Y="-0.514742248535" />
                  <Point X="-29.200779296875" Y="-0.301021881104" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.52986328125" Y="-0.113074172974" />
                  <Point X="-28.50232421875" Y="-0.09064591217" />
                  <Point X="-28.490888671875" Y="-0.062984870911" />
                  <Point X="-28.483400390625" Y="-0.031280040741" />
                  <Point X="-28.489658203125" Y="-0.00353524971" />
                  <Point X="-28.50232421875" Y="0.028085834503" />
                  <Point X="-28.52617578125" Y="0.04795488739" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.39791015625" Y="0.291282806396" />
                  <Point X="-29.998185546875" Y="0.452126068115" />
                  <Point X="-29.9667734375" Y="0.664410766602" />
                  <Point X="-29.91764453125" Y="0.996414611816" />
                  <Point X="-29.8506328125" Y="1.243714355469" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.21441015625" Y="1.45469128418" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.705072265625" Y="1.404263671875" />
                  <Point X="-28.67027734375" Y="1.415234008789" />
                  <Point X="-28.651533203125" Y="1.426056030273" />
                  <Point X="-28.639119140625" Y="1.443786010742" />
                  <Point X="-28.62843359375" Y="1.469585205078" />
                  <Point X="-28.61447265625" Y="1.503290283203" />
                  <Point X="-28.610712890625" Y="1.52460546875" />
                  <Point X="-28.61631640625" Y="1.545511962891" />
                  <Point X="-28.6292109375" Y="1.570281616211" />
                  <Point X="-28.646056640625" Y="1.602641601562" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-29.14205078125" Y="1.989137084961" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.35091015625" Y="2.459954589844" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-28.982501953125" Y="3.015174072266" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.44237890625" Y="3.090462158203" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.101421875" Y="2.917189697266" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775146484" />
                  <Point X="-28.013251953125" Y="2.927404052734" />
                  <Point X="-27.986923828125" Y="2.953731933594" />
                  <Point X="-27.95252734375" Y="2.988127441406" />
                  <Point X="-27.9408984375" Y="3.006381347656" />
                  <Point X="-27.938072265625" Y="3.02783984375" />
                  <Point X="-27.941318359375" Y="3.064931640625" />
                  <Point X="-27.945556640625" Y="3.113389404297" />
                  <Point X="-27.95206640625" Y="3.134032714844" />
                  <Point X="-28.165693359375" Y="3.504043212891" />
                  <Point X="-28.30727734375" Y="3.749276611328" />
                  <Point X="-28.085357421875" Y="3.919421630859" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.473298828125" Y="4.329658203125" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.054529296875" Y="4.400566894531" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.909962890625" Y="4.252169921875" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.77080859375" Y="4.240061523438" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.696904296875" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.294487792969" />
                  <Point X="-26.672087890625" Y="4.338875488281" />
                  <Point X="-26.653802734375" Y="4.396864257812" />
                  <Point X="-26.651916015625" Y="4.418426757812" />
                  <Point X="-26.676203125" Y="4.602903808594" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.398509765625" Y="4.782622558594" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.629162109375" Y="4.942963378906" />
                  <Point X="-25.22419921875" Y="4.990358398438" />
                  <Point X="-25.12592578125" Y="4.623594238281" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.8360859375" Y="4.719411621094" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.515611328125" Y="4.964923828125" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.85938671875" Y="4.857866699219" />
                  <Point X="-23.491544921875" Y="4.76905859375" />
                  <Point X="-23.308892578125" Y="4.702809082031" />
                  <Point X="-23.06896484375" Y="4.615785644531" />
                  <Point X="-22.892478515625" Y="4.533249023438" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.4908046875" Y="4.325803222656" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-22.106677734375" Y="4.081340820312" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.396568359375" Y="3.150651855469" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491515869141" />
                  <Point X="-22.784455078125" Y="2.456706054688" />
                  <Point X="-22.7966171875" Y="2.411229492188" />
                  <Point X="-22.797955078125" Y="2.392327636719" />
                  <Point X="-22.79432421875" Y="2.362227050781" />
                  <Point X="-22.789583984375" Y="2.322902832031" />
                  <Point X="-22.78131640625" Y="2.300811279297" />
                  <Point X="-22.76269140625" Y="2.273362548828" />
                  <Point X="-22.738357421875" Y="2.237502685547" />
                  <Point X="-22.72505859375" Y="2.224203125" />
                  <Point X="-22.697611328125" Y="2.205577880859" />
                  <Point X="-22.66175" Y="2.181245605469" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.6095625" Y="2.169350585938" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5513359375" Y="2.165946289062" />
                  <Point X="-22.516525390625" Y="2.175254882813" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.61363671875" Y="2.680468505859" />
                  <Point X="-21.005751953125" Y="3.031430664062" />
                  <Point X="-20.92987890625" Y="2.925984619141" />
                  <Point X="-20.797404296875" Y="2.741875244141" />
                  <Point X="-20.707765625" Y="2.593744873047" />
                  <Point X="-20.612486328125" Y="2.436295654297" />
                  <Point X="-21.219533203125" Y="1.970491699219" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583832641602" />
                  <Point X="-21.745681640625" Y="1.551149536133" />
                  <Point X="-21.77841015625" Y="1.508451293945" />
                  <Point X="-21.78687890625" Y="1.491501708984" />
                  <Point X="-21.7962109375" Y="1.458132080078" />
                  <Point X="-21.808404296875" Y="1.414537231445" />
                  <Point X="-21.809220703125" Y="1.390965698242" />
                  <Point X="-21.80155859375" Y="1.353837768555" />
                  <Point X="-21.79155078125" Y="1.305332885742" />
                  <Point X="-21.784353515625" Y="1.287955200195" />
                  <Point X="-21.763517578125" Y="1.256284912109" />
                  <Point X="-21.736296875" Y="1.214909912109" />
                  <Point X="-21.719052734375" Y="1.198819824219" />
                  <Point X="-21.688857421875" Y="1.181822875977" />
                  <Point X="-21.64941015625" Y="1.159617553711" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.590607421875" Y="1.148223999023" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.721189453125" Y="1.246889526367" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.11770703125" Y="1.185094360352" />
                  <Point X="-20.060806640625" Y="0.951366943359" />
                  <Point X="-20.032560546875" Y="0.769947021484" />
                  <Point X="-20.002140625" Y="0.57455645752" />
                  <Point X="-20.695162109375" Y="0.388861572266" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819351196" />
                  <Point X="-21.311021484375" Y="0.209635314941" />
                  <Point X="-21.363421875" Y="0.179346984863" />
                  <Point X="-21.377734375" Y="0.166926239014" />
                  <Point X="-21.40180078125" Y="0.136260665894" />
                  <Point X="-21.433240234375" Y="0.096198562622" />
                  <Point X="-21.443013671875" Y="0.074735412598" />
                  <Point X="-21.45103515625" Y="0.032847919464" />
                  <Point X="-21.461515625" Y="-0.021874994278" />
                  <Point X="-21.461515625" Y="-0.040685081482" />
                  <Point X="-21.453494140625" Y="-0.08257257843" />
                  <Point X="-21.443013671875" Y="-0.13729548645" />
                  <Point X="-21.433240234375" Y="-0.158758636475" />
                  <Point X="-21.409173828125" Y="-0.189424057007" />
                  <Point X="-21.377734375" Y="-0.229486312866" />
                  <Point X="-21.363421875" Y="-0.241907058716" />
                  <Point X="-21.3233125" Y="-0.265091094971" />
                  <Point X="-21.270912109375" Y="-0.295379425049" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.522439453125" Y="-0.497702880859" />
                  <Point X="-20.001931640625" Y="-0.637172546387" />
                  <Point X="-20.019798828125" Y="-0.75568963623" />
                  <Point X="-20.051568359375" Y="-0.966413208008" />
                  <Point X="-20.0877578125" Y="-1.12499621582" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-20.9343203125" Y="-1.183689086914" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.6838828125" Y="-1.115451538086" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.8145546875" Y="-1.154697265625" />
                  <Point X="-21.86213671875" Y="-1.211923461914" />
                  <Point X="-21.924298828125" Y="-1.286685424805" />
                  <Point X="-21.935640625" Y="-1.314071166992" />
                  <Point X="-21.9424609375" Y="-1.388181640625" />
                  <Point X="-21.951369140625" Y="-1.485001464844" />
                  <Point X="-21.943638671875" Y="-1.516622192383" />
                  <Point X="-21.90007421875" Y="-1.584385253906" />
                  <Point X="-21.843158203125" Y="-1.672912719727" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-21.14816015625" Y="-2.209914794922" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.70631640625" Y="-2.657234619141" />
                  <Point X="-20.7958671875" Y="-2.802140625" />
                  <Point X="-20.87071484375" Y="-2.908488525391" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.664337890625" Y="-2.595352539062" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.35634765625" Y="-2.236392333984" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.588755859375" Y="-2.260208251953" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.334683837891" />
                  <Point X="-22.75236328125" Y="-2.412517578125" />
                  <Point X="-22.80587890625" Y="-2.514201904297" />
                  <Point X="-22.8108359375" Y="-2.546374511719" />
                  <Point X="-22.7939140625" Y="-2.640064941406" />
                  <Point X="-22.77180859375" Y="-2.76246484375" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.326767578125" Y="-3.539188964844" />
                  <Point X="-22.01332421875" Y="-4.082088867188" />
                  <Point X="-22.058439453125" Y="-4.114313476562" />
                  <Point X="-22.164685546875" Y="-4.190202148438" />
                  <Point X="-22.248384765625" Y="-4.24437890625" />
                  <Point X="-22.320224609375" Y="-4.29087890625" />
                  <Point X="-22.872544921875" Y="-3.571080078125" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.421853515625" Y="-2.921059814453" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.67525390625" Y="-2.845016601562" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.912701171875" Y="-2.933393066406" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045986083984" />
                  <Point X="-24.054875" Y="-3.153333007812" />
                  <Point X="-24.085357421875" Y="-3.293573974609" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-23.96162109375" Y="-4.256000488281" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.905123046875" Y="-4.9412109375" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.0829609375" Y="-4.977291015625" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#160" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.083785213187" Y="4.667815162065" Z="1.1" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.1" />
                  <Point X="-0.641131131529" Y="5.023904450945" Z="1.1" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.1" />
                  <Point X="-1.4181653662" Y="4.862046048714" Z="1.1" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.1" />
                  <Point X="-1.731932719004" Y="4.627657504962" Z="1.1" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.1" />
                  <Point X="-1.725929802209" Y="4.385191551583" Z="1.1" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.1" />
                  <Point X="-1.796100133699" Y="4.317535462703" Z="1.1" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.1" />
                  <Point X="-1.893032253184" Y="4.327800627403" Z="1.1" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.1" />
                  <Point X="-2.021018339089" Y="4.462285101628" Z="1.1" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.1" />
                  <Point X="-2.503738172256" Y="4.404645864465" Z="1.1" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.1" />
                  <Point X="-3.121935346634" Y="3.990381513835" Z="1.1" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.1" />
                  <Point X="-3.215150359384" Y="3.510323407381" Z="1.1" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.1" />
                  <Point X="-2.997285149194" Y="3.091855263835" Z="1.1" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.1" />
                  <Point X="-3.028435595926" Y="3.020367971961" Z="1.1" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.1" />
                  <Point X="-3.103221201772" Y="2.998279549979" Z="1.1" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.1" />
                  <Point X="-3.423535940916" Y="3.165043589361" Z="1.1" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.1" />
                  <Point X="-4.028121176939" Y="3.077156474137" Z="1.1" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.1" />
                  <Point X="-4.400249292481" Y="2.516439632619" Z="1.1" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.1" />
                  <Point X="-4.178645510727" Y="1.980749545757" Z="1.1" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.1" />
                  <Point X="-3.679716769102" Y="1.578474172525" Z="1.1" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.1" />
                  <Point X="-3.680783480825" Y="1.519999428946" Z="1.1" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.1" />
                  <Point X="-3.726263475507" Y="1.483229751464" Z="1.1" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.1" />
                  <Point X="-4.214041974711" Y="1.535543563178" Z="1.1" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.1" />
                  <Point X="-4.905048523348" Y="1.288071814211" Z="1.1" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.1" />
                  <Point X="-5.022391214307" Y="0.703009803705" Z="1.1" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.1" />
                  <Point X="-4.417009196047" Y="0.274266521766" Z="1.1" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.1" />
                  <Point X="-3.560840698209" Y="0.038158428334" Z="1.1" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.1" />
                  <Point X="-3.543567756891" Y="0.012923425782" Z="1.1" />
                  <Point X="-3.539556741714" Y="0" Z="1.1" />
                  <Point X="-3.544796844244" Y="-0.016883525278" Z="1.1" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.1" />
                  <Point X="-3.564527896915" Y="-0.0407175548" Z="1.1" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.1" />
                  <Point X="-4.219878592726" Y="-0.22144553499" Z="1.1" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.1" />
                  <Point X="-5.016335148551" Y="-0.754229780996" Z="1.1" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.1" />
                  <Point X="-4.905764212068" Y="-1.290721705598" Z="1.1" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.1" />
                  <Point X="-4.141160748452" Y="-1.428247132395" Z="1.1" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.1" />
                  <Point X="-3.204158381999" Y="-1.315691952941" Z="1.1" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.1" />
                  <Point X="-3.197040356934" Y="-1.33929957843" Z="1.1" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.1" />
                  <Point X="-3.765115447733" Y="-1.785533286564" Z="1.1" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.1" />
                  <Point X="-4.336627752738" Y="-2.630469906474" Z="1.1" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.1" />
                  <Point X="-4.012738268183" Y="-3.102200836232" Z="1.1" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.1" />
                  <Point X="-3.303192867233" Y="-2.977160678785" Z="1.1" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.1" />
                  <Point X="-2.563012786996" Y="-2.565317682136" Z="1.1" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.1" />
                  <Point X="-2.878256334383" Y="-3.131885515259" Z="1.1" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.1" />
                  <Point X="-3.068001335584" Y="-4.040813183639" Z="1.1" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.1" />
                  <Point X="-2.641610480335" Y="-4.331593202872" Z="1.1" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.1" />
                  <Point X="-2.353609625638" Y="-4.322466546257" Z="1.1" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.1" />
                  <Point X="-2.08010266183" Y="-4.058818030071" Z="1.1" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.1" />
                  <Point X="-1.792895686009" Y="-3.995578102255" Z="1.1" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.1" />
                  <Point X="-1.526541098108" Y="-4.120247266256" Z="1.1" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.1" />
                  <Point X="-1.391121471935" Y="-4.381300202324" Z="1.1" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.1" />
                  <Point X="-1.385785546148" Y="-4.672036676465" Z="1.1" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.1" />
                  <Point X="-1.245607536643" Y="-4.922597360672" Z="1.1" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.1" />
                  <Point X="-0.947623907336" Y="-4.988538066987" Z="1.1" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.1" />
                  <Point X="-0.643987696411" Y="-4.365578887507" Z="1.1" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.1" />
                  <Point X="-0.324346641434" Y="-3.385152220032" Z="1.1" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.1" />
                  <Point X="-0.109849117132" Y="-3.238332480585" Z="1.1" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.1" />
                  <Point X="0.143509962229" Y="-3.248779564703" Z="1.1" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.1" />
                  <Point X="0.346099217998" Y="-3.416493535736" Z="1.1" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.1" />
                  <Point X="0.59076716399" Y="-4.166957142309" Z="1.1" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.1" />
                  <Point X="0.919819086315" Y="-4.995205841244" Z="1.1" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.1" />
                  <Point X="1.099425224657" Y="-4.958771252953" Z="1.1" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.1" />
                  <Point X="1.081794336177" Y="-4.218194185799" Z="1.1" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.1" />
                  <Point X="0.987827732379" Y="-3.132672820393" Z="1.1" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.1" />
                  <Point X="1.113108046062" Y="-2.940559569679" Z="1.1" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.1" />
                  <Point X="1.323170850173" Y="-2.863526186089" Z="1.1" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.1" />
                  <Point X="1.544949780911" Y="-2.931837960871" Z="1.1" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.1" />
                  <Point X="2.081630837233" Y="-3.570238132594" Z="1.1" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.1" />
                  <Point X="2.772628877895" Y="-4.255072887878" Z="1.1" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.1" />
                  <Point X="2.964464074502" Y="-4.123720328936" Z="1.1" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.1" />
                  <Point X="2.710375638829" Y="-3.48290891707" Z="1.1" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.1" />
                  <Point X="2.249131556001" Y="-2.599898702348" Z="1.1" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.1" />
                  <Point X="2.285726818088" Y="-2.404523966139" Z="1.1" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.1" />
                  <Point X="2.428374412234" Y="-2.273174491879" Z="1.1" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.1" />
                  <Point X="2.628608101165" Y="-2.25431645349" Z="1.1" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.1" />
                  <Point X="3.304504680037" Y="-2.607373861446" Z="1.1" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.1" />
                  <Point X="4.164017622469" Y="-2.905985564696" Z="1.1" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.1" />
                  <Point X="4.33005993411" Y="-2.652239733005" Z="1.1" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.1" />
                  <Point X="3.876120150157" Y="-2.138966964149" Z="1.1" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.1" />
                  <Point X="3.135827845674" Y="-1.526065519844" Z="1.1" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.1" />
                  <Point X="3.101171842191" Y="-1.36148261624" Z="1.1" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.1" />
                  <Point X="3.170153695898" Y="-1.21261035632" Z="1.1" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.1" />
                  <Point X="3.320578854544" Y="-1.133030760658" Z="1.1" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.1" />
                  <Point X="4.052997647903" Y="-1.20198136441" Z="1.1" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.1" />
                  <Point X="4.954831285412" Y="-1.104840104145" Z="1.1" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.1" />
                  <Point X="5.023485095959" Y="-0.731863745663" Z="1.1" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.1" />
                  <Point X="4.484345920586" Y="-0.418126596676" Z="1.1" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.1" />
                  <Point X="3.695552349603" Y="-0.190522295291" Z="1.1" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.1" />
                  <Point X="3.624002504656" Y="-0.127275991481" Z="1.1" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.1" />
                  <Point X="3.589456653697" Y="-0.041887512346" Z="1.1" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.1" />
                  <Point X="3.591914796727" Y="0.05472301887" Z="1.1" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.1" />
                  <Point X="3.631376933744" Y="0.136672747094" Z="1.1" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.1" />
                  <Point X="3.707843064749" Y="0.197626548951" Z="1.1" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.1" />
                  <Point X="4.311621896721" Y="0.371845336093" Z="1.1" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.1" />
                  <Point X="5.010686173246" Y="0.808919012165" Z="1.1" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.1" />
                  <Point X="4.924718082841" Y="1.22820116975" Z="1.1" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.1" />
                  <Point X="4.266127625506" Y="1.327741888977" Z="1.1" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.1" />
                  <Point X="3.409785997122" Y="1.229073008235" Z="1.1" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.1" />
                  <Point X="3.32951332376" Y="1.256673989802" Z="1.1" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.1" />
                  <Point X="3.272097352423" Y="1.315046057863" Z="1.1" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.1" />
                  <Point X="3.241252780867" Y="1.395221316736" Z="1.1" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.1" />
                  <Point X="3.245783827009" Y="1.475944138211" Z="1.1" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.1" />
                  <Point X="3.287845491788" Y="1.552011922185" Z="1.1" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.1" />
                  <Point X="3.804746905192" Y="1.962103904731" Z="1.1" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.1" />
                  <Point X="4.328855742611" Y="2.650911344313" Z="1.1" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.1" />
                  <Point X="4.104550421894" Y="2.986467718283" Z="1.1" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.1" />
                  <Point X="3.355207257399" Y="2.755049860427" Z="1.1" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.1" />
                  <Point X="2.464401441457" Y="2.254837764998" Z="1.1" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.1" />
                  <Point X="2.390267388543" Y="2.250270953064" Z="1.1" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.1" />
                  <Point X="2.324306887445" Y="2.2782329323" Z="1.1" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.1" />
                  <Point X="2.272525667935" Y="2.332717972937" Z="1.1" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.1" />
                  <Point X="2.249158749695" Y="2.39949105529" Z="1.1" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.1" />
                  <Point X="2.25769017766" Y="2.475068134425" Z="1.1" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.1" />
                  <Point X="2.640575492432" Y="3.156931824556" Z="1.1" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.1" />
                  <Point X="2.916142736746" Y="4.153367935734" Z="1.1" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.1" />
                  <Point X="2.528209082677" Y="4.400285752575" Z="1.1" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.1" />
                  <Point X="2.122546016308" Y="4.609821055033" Z="1.1" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.1" />
                  <Point X="1.701999664586" Y="4.781093086162" Z="1.1" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.1" />
                  <Point X="1.146191586978" Y="4.937750238619" Z="1.1" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.1" />
                  <Point X="0.483439660544" Y="5.045931855069" Z="1.1" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.1" />
                  <Point X="0.109459348366" Y="4.763632343886" Z="1.1" />
                  <Point X="0" Y="4.355124473572" Z="1.1" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>