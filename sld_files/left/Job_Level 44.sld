<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#203" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3151" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000473632812" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.097419921875" Y="-4.778716796875" />
                  <Point X="-24.4366953125" Y="-3.512525146484" />
                  <Point X="-24.44227734375" Y="-3.497142333984" />
                  <Point X="-24.457634765625" Y="-3.467376220703" />
                  <Point X="-24.5963046875" Y="-3.267578613281" />
                  <Point X="-24.62136328125" Y="-3.231475830078" />
                  <Point X="-24.64325" Y="-3.209018798828" />
                  <Point X="-24.669505859375" Y="-3.189775634766" />
                  <Point X="-24.69750390625" Y="-3.175668945312" />
                  <Point X="-24.912087890625" Y="-3.109069824219" />
                  <Point X="-24.95086328125" Y="-3.097035644531" />
                  <Point X="-24.9790234375" Y="-3.092766357422" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.251408203125" Y="-3.163634765625" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.318185546875" Y="-3.189778076172" />
                  <Point X="-25.34444140625" Y="-3.209022705078" />
                  <Point X="-25.36632421875" Y="-3.231477539062" />
                  <Point X="-25.504994140625" Y="-3.431274902344" />
                  <Point X="-25.53005078125" Y="-3.467377929688" />
                  <Point X="-25.5381875" Y="-3.481571289062" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.612296875" Y="-3.741322509766" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.037744140625" Y="-4.853424316406" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.246416015625" Y="-4.802362304688" />
                  <Point X="-26.2149609375" Y="-4.563438964844" />
                  <Point X="-26.21419921875" Y="-4.547929199219" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.2677734375" Y="-4.258501464844" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.18296484375" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.521205078125" Y="-3.957946533203" />
                  <Point X="-26.556904296875" Y="-3.926639404297" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.905236328125" Y="-3.873780273438" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.983419921875" Y="-3.873708984375" />
                  <Point X="-27.01446875" Y="-3.882029052734" />
                  <Point X="-27.042658203125" Y="-3.894802001953" />
                  <Point X="-27.26114453125" Y="-4.040790283203" />
                  <Point X="-27.300623046875" Y="-4.067170166016" />
                  <Point X="-27.31278515625" Y="-4.076822021484" />
                  <Point X="-27.335099609375" Y="-4.099460449219" />
                  <Point X="-27.36951953125" Y="-4.144315429688" />
                  <Point X="-27.480146484375" Y="-4.288489257813" />
                  <Point X="-27.740744140625" Y="-4.127134765625" />
                  <Point X="-27.80171484375" Y="-4.0893828125" />
                  <Point X="-28.10472265625" Y="-3.856077392578" />
                  <Point X="-28.054265625" Y="-3.768685058594" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647654785156" />
                  <Point X="-27.406587890625" Y="-2.616128662109" />
                  <Point X="-27.40557421875" Y="-2.585194335938" />
                  <Point X="-27.41455859375" Y="-2.555576171875" />
                  <Point X="-27.428775390625" Y="-2.526747314453" />
                  <Point X="-27.446802734375" Y="-2.501590087891" />
                  <Point X="-27.46149609375" Y="-2.486896484375" />
                  <Point X="-27.464146484375" Y="-2.48424609375" />
                  <Point X="-27.489294921875" Y="-2.466222412109" />
                  <Point X="-27.518125" Y="-2.452000976562" />
                  <Point X="-27.54774609375" Y="-2.443012939453" />
                  <Point X="-27.57868359375" Y="-2.444023925781" />
                  <Point X="-27.610212890625" Y="-2.450294433594" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-27.836515625" Y="-2.575128173828" />
                  <Point X="-28.818021484375" Y="-3.141800537109" />
                  <Point X="-29.034693359375" Y="-2.857139648438" />
                  <Point X="-29.082865234375" Y="-2.793850830078" />
                  <Point X="-29.306142578125" Y="-2.419449707031" />
                  <Point X="-29.2091328125" Y="-2.34501171875" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.084578125" Y="-1.475592895508" />
                  <Point X="-28.06661328125" Y="-1.448461547852" />
                  <Point X="-28.05385546875" Y="-1.419830688477" />
                  <Point X="-28.04733203125" Y="-1.394641357422" />
                  <Point X="-28.0461640625" Y="-1.390139648438" />
                  <Point X="-28.04334765625" Y="-1.35966796875" />
                  <Point X="-28.0455546875" Y="-1.327995361328" />
                  <Point X="-28.052552734375" Y="-1.29824987793" />
                  <Point X="-28.068634765625" Y="-1.272265258789" />
                  <Point X="-28.089466796875" Y="-1.248306518555" />
                  <Point X="-28.11296875" Y="-1.228768188477" />
                  <Point X="-28.1353984375" Y="-1.215567016602" />
                  <Point X="-28.139451171875" Y="-1.213181518555" />
                  <Point X="-28.168712890625" Y="-1.201957885742" />
                  <Point X="-28.2006015625" Y="-1.195475219727" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-28.48104296875" Y="-1.227180419922" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.81523828125" Y="-1.066406860352" />
                  <Point X="-29.83407421875" Y="-0.992653991699" />
                  <Point X="-29.892421875" Y="-0.584698242188" />
                  <Point X="-29.789560546875" Y="-0.557136230469" />
                  <Point X="-28.532875" Y="-0.220408279419" />
                  <Point X="-28.517494140625" Y="-0.214828155518" />
                  <Point X="-28.487732421875" Y="-0.199471679688" />
                  <Point X="-28.4642265625" Y="-0.183157546997" />
                  <Point X="-28.459978515625" Y="-0.180209625244" />
                  <Point X="-28.437525390625" Y="-0.158329956055" />
                  <Point X="-28.418279296875" Y="-0.132074935913" />
                  <Point X="-28.404166015625" Y="-0.104065368652" />
                  <Point X="-28.39633203125" Y="-0.078820198059" />
                  <Point X="-28.394916015625" Y="-0.074258293152" />
                  <Point X="-28.3906484375" Y="-0.046102851868" />
                  <Point X="-28.3906484375" Y="-0.01645728302" />
                  <Point X="-28.394916015625" Y="0.011698309898" />
                  <Point X="-28.40275" Y="0.036943473816" />
                  <Point X="-28.404166015625" Y="0.04150522995" />
                  <Point X="-28.418279296875" Y="0.06951449585" />
                  <Point X="-28.437525390625" Y="0.095769676208" />
                  <Point X="-28.459978515625" Y="0.117649650574" />
                  <Point X="-28.483484375" Y="0.133963775635" />
                  <Point X="-28.494279296875" Y="0.140441421509" />
                  <Point X="-28.514091796875" Y="0.150607589722" />
                  <Point X="-28.532875" Y="0.157848297119" />
                  <Point X="-28.759955078125" Y="0.218694366455" />
                  <Point X="-29.891814453125" Y="0.521975463867" />
                  <Point X="-29.836626953125" Y="0.894936218262" />
                  <Point X="-29.82448828125" Y="0.976968444824" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-29.66648828125" Y="1.418388549805" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744982421875" Y="1.299342041016" />
                  <Point X="-28.723421875" Y="1.301228637695" />
                  <Point X="-28.70313671875" Y="1.305263793945" />
                  <Point X="-28.651111328125" Y="1.321667114258" />
                  <Point X="-28.6417109375" Y="1.324631225586" />
                  <Point X="-28.62277734375" Y="1.332961791992" />
                  <Point X="-28.60403125" Y="1.343784790039" />
                  <Point X="-28.58734765625" Y="1.356017700195" />
                  <Point X="-28.573708984375" Y="1.371571899414" />
                  <Point X="-28.561294921875" Y="1.389303100586" />
                  <Point X="-28.55134765625" Y="1.407434204102" />
                  <Point X="-28.53047265625" Y="1.457831787109" />
                  <Point X="-28.526701171875" Y="1.466938476562" />
                  <Point X="-28.520912109375" Y="1.486805908203" />
                  <Point X="-28.51715625" Y="1.508119628906" />
                  <Point X="-28.5158046875" Y="1.528751464844" />
                  <Point X="-28.51894921875" Y="1.549186889648" />
                  <Point X="-28.524548828125" Y="1.570091674805" />
                  <Point X="-28.532046875" Y="1.589375976562" />
                  <Point X="-28.557234375" Y="1.637762451172" />
                  <Point X="-28.56180078125" Y="1.646528930664" />
                  <Point X="-28.5732890625" Y="1.663717041016" />
                  <Point X="-28.58719921875" Y="1.680291870117" />
                  <Point X="-28.60213671875" Y="1.694590332031" />
                  <Point X="-28.732390625" Y="1.794537841797" />
                  <Point X="-29.351859375" Y="2.269873535156" />
                  <Point X="-29.128322265625" Y="2.652847412109" />
                  <Point X="-29.0811484375" Y="2.7336640625" />
                  <Point X="-28.75050390625" Y="3.158662353516" />
                  <Point X="-28.20665625" Y="2.844671142578" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.0743359375" Y="2.819457275391" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.99901171875" Y="2.826504882812" />
                  <Point X="-27.9804609375" Y="2.835654541016" />
                  <Point X="-27.96220703125" Y="2.847284179688" />
                  <Point X="-27.946078125" Y="2.860229003906" />
                  <Point X="-27.894646484375" Y="2.911659179688" />
                  <Point X="-27.885353515625" Y="2.920952392578" />
                  <Point X="-27.87240625" Y="2.937082763672" />
                  <Point X="-27.86077734375" Y="2.955335449219" />
                  <Point X="-27.85162890625" Y="2.973885253906" />
                  <Point X="-27.846712890625" Y="2.993975341797" />
                  <Point X="-27.84388671875" Y="3.015431884766" />
                  <Point X="-27.84343359375" Y="3.036118408203" />
                  <Point X="-27.8497734375" Y="3.108575195312" />
                  <Point X="-27.85091796875" Y="3.12166796875" />
                  <Point X="-27.854953125" Y="3.141955078125" />
                  <Point X="-27.861462890625" Y="3.162600830078" />
                  <Point X="-27.869794921875" Y="3.181533691406" />
                  <Point X="-27.927515625" Y="3.281506591797" />
                  <Point X="-28.18333203125" Y="3.724595947266" />
                  <Point X="-27.782771484375" Y="4.031701416016" />
                  <Point X="-27.70062109375" Y="4.094686279297" />
                  <Point X="-27.167037109375" Y="4.391133789062" />
                  <Point X="-27.043197265625" Y="4.229743164063" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.995111328125" Y="4.18939453125" />
                  <Point X="-26.914466796875" Y="4.147413574219" />
                  <Point X="-26.89989453125" Y="4.139827636719" />
                  <Point X="-26.8806171875" Y="4.132331054688" />
                  <Point X="-26.8597109375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.12358203125" />
                  <Point X="-26.818626953125" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.777453125" Y="4.134481445312" />
                  <Point X="-26.69345703125" Y="4.169274414063" />
                  <Point X="-26.678279296875" Y="4.175561035156" />
                  <Point X="-26.660150390625" Y="4.185506347656" />
                  <Point X="-26.642419921875" Y="4.197919433594" />
                  <Point X="-26.6268671875" Y="4.211557128906" />
                  <Point X="-26.614634765625" Y="4.228237792969" />
                  <Point X="-26.6038125" Y="4.246979980469" />
                  <Point X="-26.595478515625" Y="4.265918945313" />
                  <Point X="-26.568140625" Y="4.352627441406" />
                  <Point X="-26.56319921875" Y="4.368295410156" />
                  <Point X="-26.5591640625" Y="4.388581542969" />
                  <Point X="-26.55727734375" Y="4.410146972656" />
                  <Point X="-26.557728515625" Y="4.430827636719" />
                  <Point X="-26.564291015625" Y="4.480671386719" />
                  <Point X="-26.584201171875" Y="4.631897460938" />
                  <Point X="-26.055978515625" Y="4.779992675781" />
                  <Point X="-25.949630859375" Y="4.80980859375" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.133904296875" Y="4.28631640625" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.85378515625" Y="4.286314453125" />
                  <Point X="-24.824208984375" Y="4.396689453125" />
                  <Point X="-24.692580078125" Y="4.8879375" />
                  <Point X="-24.2488203125" Y="4.841463867188" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.611251953125" Y="4.70023046875" />
                  <Point X="-23.51897265625" Y="4.677950683594" />
                  <Point X="-23.1646484375" Y="4.549435058594" />
                  <Point X="-23.105353515625" Y="4.527928222656" />
                  <Point X="-22.76251953125" Y="4.367596191406" />
                  <Point X="-22.705423828125" Y="4.340893554688" />
                  <Point X="-22.374208984375" Y="4.147927734375" />
                  <Point X="-22.319021484375" Y="4.115775390625" />
                  <Point X="-22.05673828125" Y="3.929254882812" />
                  <Point X="-22.12265625" Y="3.815081542969" />
                  <Point X="-22.85241796875" Y="2.55109765625" />
                  <Point X="-22.857921875" Y="2.53993359375" />
                  <Point X="-22.866921875" Y="2.516056152344" />
                  <Point X="-22.88510546875" Y="2.448057128906" />
                  <Point X="-22.887302734375" Y="2.437450195312" />
                  <Point X="-22.891927734375" Y="2.406260009766" />
                  <Point X="-22.892271484375" Y="2.380953125" />
                  <Point X="-22.885181640625" Y="2.322153320312" />
                  <Point X="-22.883900390625" Y="2.311528320312" />
                  <Point X="-22.87855859375" Y="2.289604736328" />
                  <Point X="-22.87029296875" Y="2.267517089844" />
                  <Point X="-22.859927734375" Y="2.247470214844" />
                  <Point X="-22.82354296875" Y="2.193850585938" />
                  <Point X="-22.816798828125" Y="2.185061035156" />
                  <Point X="-22.796921875" Y="2.162070556641" />
                  <Point X="-22.7783984375" Y="2.145592041016" />
                  <Point X="-22.724779296875" Y="2.109208740234" />
                  <Point X="-22.71508984375" Y="2.102634521484" />
                  <Point X="-22.695044921875" Y="2.092271484375" />
                  <Point X="-22.672958984375" Y="2.084006347656" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.592236328125" Y="2.071573242188" />
                  <Point X="-22.580681640625" Y="2.070890136719" />
                  <Point X="-22.55115234375" Y="2.070946777344" />
                  <Point X="-22.52679296875" Y="2.074171142578" />
                  <Point X="-22.45879296875" Y="2.092354980469" />
                  <Point X="-22.452748046875" Y="2.094188720703" />
                  <Point X="-22.42837890625" Y="2.102475830078" />
                  <Point X="-22.41146484375" Y="2.110145263672" />
                  <Point X="-22.18306640625" Y="2.24201171875" />
                  <Point X="-21.032673828125" Y="2.906190673828" />
                  <Point X="-20.90946484375" Y="2.734957763672" />
                  <Point X="-20.876720703125" Y="2.689451416016" />
                  <Point X="-20.73780078125" Y="2.459883789063" />
                  <Point X="-20.808416015625" Y="2.405698730469" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.77857421875" Y="1.660241821289" />
                  <Point X="-21.79602734375" Y="1.64162512207" />
                  <Point X="-21.844966796875" Y="1.577780517578" />
                  <Point X="-21.850755859375" Y="1.569321533203" />
                  <Point X="-21.868064453125" Y="1.540839233398" />
                  <Point X="-21.878369140625" Y="1.517090087891" />
                  <Point X="-21.896599609375" Y="1.451904296875" />
                  <Point X="-21.89989453125" Y="1.440125610352" />
                  <Point X="-21.90334765625" Y="1.417818603516" />
                  <Point X="-21.904162109375" Y="1.394241577148" />
                  <Point X="-21.902259765625" Y="1.371764404297" />
                  <Point X="-21.887294921875" Y="1.299237304688" />
                  <Point X="-21.884607421875" Y="1.289088500977" />
                  <Point X="-21.874708984375" Y="1.258612182617" />
                  <Point X="-21.86371875" Y="1.235743530273" />
                  <Point X="-21.823015625" Y="1.173877197266" />
                  <Point X="-21.815662109375" Y="1.162698242188" />
                  <Point X="-21.801111328125" Y="1.145456420898" />
                  <Point X="-21.783865234375" Y="1.12936340332" />
                  <Point X="-21.76565234375" Y="1.116034790039" />
                  <Point X="-21.70666796875" Y="1.08283190918" />
                  <Point X="-21.696779296875" Y="1.077997070312" />
                  <Point X="-21.66814453125" Y="1.065999511719" />
                  <Point X="-21.643880859375" Y="1.059438598633" />
                  <Point X="-21.564130859375" Y="1.04889831543" />
                  <Point X="-21.55826171875" Y="1.048307250977" />
                  <Point X="-21.5307734375" Y="1.046399658203" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-21.29483203125" Y="1.075548461914" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.168125" Y="0.990563049316" />
                  <Point X="-20.15405859375" Y="0.932783813477" />
                  <Point X="-20.109134765625" Y="0.644238708496" />
                  <Point X="-20.182322265625" Y="0.624627807617" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.295208984375" Y="0.32558605957" />
                  <Point X="-21.318453125" Y="0.315068023682" />
                  <Point X="-21.3968046875" Y="0.269779266357" />
                  <Point X="-21.404962890625" Y="0.264489532471" />
                  <Point X="-21.43343359375" Y="0.243883972168" />
                  <Point X="-21.45246875" Y="0.225575637817" />
                  <Point X="-21.49948046875" Y="0.165672180176" />
                  <Point X="-21.507974609375" Y="0.154848007202" />
                  <Point X="-21.519697265625" Y="0.135570327759" />
                  <Point X="-21.529470703125" Y="0.114109291077" />
                  <Point X="-21.536318359375" Y="0.092604850769" />
                  <Point X="-21.55198828125" Y="0.010779978752" />
                  <Point X="-21.55334765625" Y="0.000891140282" />
                  <Point X="-21.5561796875" Y="-0.032706439972" />
                  <Point X="-21.5548203125" Y="-0.058554367065" />
                  <Point X="-21.539150390625" Y="-0.140379241943" />
                  <Point X="-21.536318359375" Y="-0.155164825439" />
                  <Point X="-21.529470703125" Y="-0.176669570923" />
                  <Point X="-21.519697265625" Y="-0.198130462646" />
                  <Point X="-21.507974609375" Y="-0.217407836914" />
                  <Point X="-21.460962890625" Y="-0.277311279297" />
                  <Point X="-21.454041015625" Y="-0.285191650391" />
                  <Point X="-21.431234375" Y="-0.308437866211" />
                  <Point X="-21.410962890625" Y="-0.324155609131" />
                  <Point X="-21.332611328125" Y="-0.369444396973" />
                  <Point X="-21.3277421875" Y="-0.372073242188" />
                  <Point X="-21.30150390625" Y="-0.385264282227" />
                  <Point X="-21.283419921875" Y="-0.392149963379" />
                  <Point X="-21.084453125" Y="-0.445462677002" />
                  <Point X="-20.108525390625" Y="-0.706961853027" />
                  <Point X="-20.137125" Y="-0.896655395508" />
                  <Point X="-20.144974609375" Y="-0.948725219727" />
                  <Point X="-20.19882421875" Y="-1.18469909668" />
                  <Point X="-20.298654296875" Y="-1.171556274414" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710266113" />
                  <Point X="-21.62533984375" Y="-1.005508666992" />
                  <Point X="-21.7791171875" Y="-1.038933105469" />
                  <Point X="-21.806904296875" Y="-1.04497265625" />
                  <Point X="-21.8360234375" Y="-1.056596801758" />
                  <Point X="-21.8638515625" Y="-1.073489868164" />
                  <Point X="-21.8876015625" Y="-1.093960571289" />
                  <Point X="-21.98055078125" Y="-1.205748657227" />
                  <Point X="-21.997345703125" Y="-1.225948608398" />
                  <Point X="-22.0120703125" Y="-1.250336547852" />
                  <Point X="-22.023412109375" Y="-1.27772277832" />
                  <Point X="-22.030240234375" Y="-1.305366943359" />
                  <Point X="-22.0435625" Y="-1.450137451172" />
                  <Point X="-22.04596875" Y="-1.476297241211" />
                  <Point X="-22.043650390625" Y="-1.507568847656" />
                  <Point X="-22.03591796875" Y="-1.539189453125" />
                  <Point X="-22.023546875" Y="-1.567997558594" />
                  <Point X="-21.9384453125" Y="-1.700369018555" />
                  <Point X="-21.92306640625" Y="-1.724288452148" />
                  <Point X="-21.9130625" Y="-1.73724206543" />
                  <Point X="-21.889369140625" Y="-1.760909057617" />
                  <Point X="-21.704728515625" Y="-1.902589599609" />
                  <Point X="-20.786876953125" Y="-2.606882080078" />
                  <Point X="-20.853060546875" Y="-2.7139765625" />
                  <Point X="-20.87519921875" Y="-2.749799804688" />
                  <Point X="-20.971017578125" Y="-2.8859453125" />
                  <Point X="-21.061736328125" Y="-2.833568359375" />
                  <Point X="-22.199044921875" Y="-2.176943359375" />
                  <Point X="-22.2138671875" Y="-2.170012207031" />
                  <Point X="-22.245775390625" Y="-2.159825439453" />
                  <Point X="-22.428794921875" Y="-2.126772460938" />
                  <Point X="-22.461865234375" Y="-2.120799804688" />
                  <Point X="-22.493216796875" Y="-2.120395751953" />
                  <Point X="-22.525390625" Y="-2.125353759766" />
                  <Point X="-22.555166015625" Y="-2.135177246094" />
                  <Point X="-22.7072109375" Y="-2.215196777344" />
                  <Point X="-22.73468359375" Y="-2.22965625" />
                  <Point X="-22.757611328125" Y="-2.246546875" />
                  <Point X="-22.7785703125" Y="-2.267504394531" />
                  <Point X="-22.795466796875" Y="-2.2904375" />
                  <Point X="-22.875486328125" Y="-2.442481445312" />
                  <Point X="-22.889947265625" Y="-2.469955566406" />
                  <Point X="-22.899771484375" Y="-2.499734375" />
                  <Point X="-22.904728515625" Y="-2.531908935547" />
                  <Point X="-22.90432421875" Y="-2.563259033203" />
                  <Point X="-22.87126953125" Y="-2.746278320312" />
                  <Point X="-22.865296875" Y="-2.779349365234" />
                  <Point X="-22.86101171875" Y="-2.795139648438" />
                  <Point X="-22.8481796875" Y="-2.826078613281" />
                  <Point X="-22.729529296875" Y="-3.031587646484" />
                  <Point X="-22.13871484375" Y="-4.054906982422" />
                  <Point X="-22.191884765625" Y="-4.092885009766" />
                  <Point X="-22.218154296875" Y="-4.111646972656" />
                  <Point X="-22.298232421875" Y="-4.163481933594" />
                  <Point X="-22.37358203125" Y="-4.065285400391" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.45858203125" Y="-2.784508056641" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.780314453125" Y="-2.759282958984" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397705078" />
                  <Point X="-23.871021484375" Y="-2.780740966797" />
                  <Point X="-23.89540234375" Y="-2.7954609375" />
                  <Point X="-24.047841796875" Y="-2.922208496094" />
                  <Point X="-24.07538671875" Y="-2.945111328125" />
                  <Point X="-24.095857421875" Y="-2.968860839844" />
                  <Point X="-24.11275" Y="-2.996687011719" />
                  <Point X="-24.124375" Y="-3.025808349609" />
                  <Point X="-24.169953125" Y="-3.235504394531" />
                  <Point X="-24.178189453125" Y="-3.273396240234" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323120361328" />
                  <Point X="-24.1466328125" Y="-3.578525146484" />
                  <Point X="-23.977935546875" Y="-4.859915039062" />
                  <Point X="-23.99948046875" Y="-4.864637695312" />
                  <Point X="-24.02433203125" Y="-4.870085449219" />
                  <Point X="-24.070681640625" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.019642578125" Y="-4.760165039062" />
                  <Point X="-26.05842578125" Y="-4.75263671875" />
                  <Point X="-26.141244140625" Y="-4.731328125" />
                  <Point X="-26.1207734375" Y="-4.575838867188" />
                  <Point X="-26.120076171875" Y="-4.568099121094" />
                  <Point X="-26.11944921875" Y="-4.541030273438" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497688964844" />
                  <Point X="-26.174599609375" Y="-4.239967285156" />
                  <Point X="-26.18386328125" Y="-4.193397460938" />
                  <Point X="-26.188126953125" Y="-4.178467773438" />
                  <Point X="-26.199029296875" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135463867188" />
                  <Point X="-26.221740234375" Y="-4.107626464844" />
                  <Point X="-26.23057421875" Y="-4.094861083984" />
                  <Point X="-26.25020703125" Y="-4.070937744141" />
                  <Point X="-26.261005859375" Y="-4.059779541016" />
                  <Point X="-26.45856640625" Y="-3.886521972656" />
                  <Point X="-26.494265625" Y="-3.85521484375" />
                  <Point X="-26.50673828125" Y="-3.845965332031" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.8990234375" Y="-3.778983642578" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.992732421875" Y="-3.779166503906" />
                  <Point X="-27.008009765625" Y="-3.781946533203" />
                  <Point X="-27.03905859375" Y="-3.790266601562" />
                  <Point X="-27.053677734375" Y="-3.795497558594" />
                  <Point X="-27.0818671875" Y="-3.808270507812" />
                  <Point X="-27.0954375" Y="-3.8158125" />
                  <Point X="-27.313923828125" Y="-3.96180078125" />
                  <Point X="-27.35340234375" Y="-3.988180664062" />
                  <Point X="-27.359677734375" Y="-3.992755859375" />
                  <Point X="-27.380443359375" Y="-4.0101328125" />
                  <Point X="-27.4027578125" Y="-4.032771240234" />
                  <Point X="-27.410466796875" Y="-4.041626464844" />
                  <Point X="-27.44488671875" Y="-4.086481445312" />
                  <Point X="-27.503201171875" Y="-4.162478515625" />
                  <Point X="-27.690732421875" Y="-4.046364013672" />
                  <Point X="-27.747595703125" Y="-4.011154785156" />
                  <Point X="-27.98086328125" Y="-3.831547363281" />
                  <Point X="-27.971994140625" Y="-3.816185791016" />
                  <Point X="-27.341490234375" Y="-2.724120361328" />
                  <Point X="-27.3348515625" Y="-2.710085449219" />
                  <Point X="-27.32394921875" Y="-2.681120605469" />
                  <Point X="-27.319685546875" Y="-2.666189941406" />
                  <Point X="-27.3134140625" Y="-2.634663818359" />
                  <Point X="-27.311638671875" Y="-2.619239990234" />
                  <Point X="-27.310625" Y="-2.588305664062" />
                  <Point X="-27.3146640625" Y="-2.557617919922" />
                  <Point X="-27.3236484375" Y="-2.527999755859" />
                  <Point X="-27.32935546875" Y="-2.513558837891" />
                  <Point X="-27.343572265625" Y="-2.484729980469" />
                  <Point X="-27.3515546875" Y="-2.471412109375" />
                  <Point X="-27.36958203125" Y="-2.446254882812" />
                  <Point X="-27.379626953125" Y="-2.434415527344" />
                  <Point X="-27.3943203125" Y="-2.419721923828" />
                  <Point X="-27.408806640625" Y="-2.407029296875" />
                  <Point X="-27.433955078125" Y="-2.389005615234" />
                  <Point X="-27.447267578125" Y="-2.381024169922" />
                  <Point X="-27.47609765625" Y="-2.366802734375" />
                  <Point X="-27.490541015625" Y="-2.36109375" />
                  <Point X="-27.520162109375" Y="-2.352105712891" />
                  <Point X="-27.550849609375" Y="-2.348063720703" />
                  <Point X="-27.581787109375" Y="-2.349074707031" />
                  <Point X="-27.59721484375" Y="-2.350848632812" />
                  <Point X="-27.628744140625" Y="-2.357119140625" />
                  <Point X="-27.64367578125" Y="-2.361383056641" />
                  <Point X="-27.67264453125" Y="-2.372285888672" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-27.884015625" Y="-2.492855712891" />
                  <Point X="-28.7930859375" Y="-3.017707519531" />
                  <Point X="-28.959099609375" Y="-2.799601318359" />
                  <Point X="-29.00402734375" Y="-2.740575683594" />
                  <Point X="-29.181265625" Y="-2.443373291016" />
                  <Point X="-29.15130078125" Y="-2.420380371094" />
                  <Point X="-28.048123046875" Y="-1.573882080078" />
                  <Point X="-28.03648046875" Y="-1.56330847168" />
                  <Point X="-28.015103515625" Y="-1.540387939453" />
                  <Point X="-28.005369140625" Y="-1.528041137695" />
                  <Point X="-27.987404296875" Y="-1.500909912109" />
                  <Point X="-27.979837890625" Y="-1.487128173828" />
                  <Point X="-27.967080078125" Y="-1.458497314453" />
                  <Point X="-27.961888671875" Y="-1.443647705078" />
                  <Point X="-27.955365234375" Y="-1.418458374023" />
                  <Point X="-27.95156640625" Y="-1.39888293457" />
                  <Point X="-27.94875" Y="-1.368411254883" />
                  <Point X="-27.948578125" Y="-1.353064086914" />
                  <Point X="-27.95078515625" Y="-1.321391357422" />
                  <Point X="-27.953080078125" Y="-1.306239257812" />
                  <Point X="-27.960078125" Y="-1.276493774414" />
                  <Point X="-27.971771484375" Y="-1.248254516602" />
                  <Point X="-27.987853515625" Y="-1.222269897461" />
                  <Point X="-27.9969453125" Y="-1.209931152344" />
                  <Point X="-28.01777734375" Y="-1.185972290039" />
                  <Point X="-28.028734375" Y="-1.175254272461" />
                  <Point X="-28.052236328125" Y="-1.155715942383" />
                  <Point X="-28.06478125" Y="-1.146895996094" />
                  <Point X="-28.0872109375" Y="-1.133694824219" />
                  <Point X="-28.1054296875" Y="-1.124482421875" />
                  <Point X="-28.13469140625" Y="-1.113258789062" />
                  <Point X="-28.149787109375" Y="-1.108862060547" />
                  <Point X="-28.18167578125" Y="-1.102379394531" />
                  <Point X="-28.197294921875" Y="-1.100532714844" />
                  <Point X="-28.22862109375" Y="-1.09944152832" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-28.493443359375" Y="-1.132993164062" />
                  <Point X="-29.66091796875" Y="-1.286694213867" />
                  <Point X="-29.723193359375" Y="-1.042895385742" />
                  <Point X="-29.740759765625" Y="-0.974117004395" />
                  <Point X="-29.78644921875" Y="-0.654653991699" />
                  <Point X="-29.76497265625" Y="-0.648899047852" />
                  <Point X="-28.508287109375" Y="-0.312171081543" />
                  <Point X="-28.5004765625" Y="-0.30971270752" />
                  <Point X="-28.47393359375" Y="-0.299252166748" />
                  <Point X="-28.444171875" Y="-0.283895721436" />
                  <Point X="-28.43356640625" Y="-0.277516387939" />
                  <Point X="-28.410060546875" Y="-0.261202209473" />
                  <Point X="-28.393677734375" Y="-0.248247924805" />
                  <Point X="-28.371224609375" Y="-0.226368270874" />
                  <Point X="-28.36090625" Y="-0.21449508667" />
                  <Point X="-28.34166015625" Y="-0.188240112305" />
                  <Point X="-28.33344140625" Y="-0.174822875977" />
                  <Point X="-28.319328125" Y="-0.146813415527" />
                  <Point X="-28.31343359375" Y="-0.132220870972" />
                  <Point X="-28.305599609375" Y="-0.106975799561" />
                  <Point X="-28.30098828125" Y="-0.088494987488" />
                  <Point X="-28.296720703125" Y="-0.060339572906" />
                  <Point X="-28.2956484375" Y="-0.046102840424" />
                  <Point X="-28.2956484375" Y="-0.016457332611" />
                  <Point X="-28.296720703125" Y="-0.00222059989" />
                  <Point X="-28.30098828125" Y="0.025934963226" />
                  <Point X="-28.30418359375" Y="0.039853939056" />
                  <Point X="-28.312017578125" Y="0.065099021912" />
                  <Point X="-28.319328125" Y="0.084253540039" />
                  <Point X="-28.33344140625" Y="0.11226285553" />
                  <Point X="-28.34166015625" Y="0.125679489136" />
                  <Point X="-28.36090625" Y="0.151934616089" />
                  <Point X="-28.371224609375" Y="0.163807647705" />
                  <Point X="-28.393677734375" Y="0.185687606812" />
                  <Point X="-28.4058125" Y="0.195694381714" />
                  <Point X="-28.429318359375" Y="0.212008575439" />
                  <Point X="-28.45091015625" Y="0.224963882446" />
                  <Point X="-28.47072265625" Y="0.235129974365" />
                  <Point X="-28.479921875" Y="0.239249511719" />
                  <Point X="-28.498705078125" Y="0.246490112305" />
                  <Point X="-28.508287109375" Y="0.249611190796" />
                  <Point X="-28.7353671875" Y="0.310457244873" />
                  <Point X="-29.7854453125" Y="0.591824951172" />
                  <Point X="-29.742650390625" Y="0.881030273438" />
                  <Point X="-29.73133203125" Y="0.95752130127" />
                  <Point X="-29.6335859375" Y="1.318236938477" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767734375" Y="1.204815551758" />
                  <Point X="-28.747052734375" Y="1.204364624023" />
                  <Point X="-28.736701171875" Y="1.204703735352" />
                  <Point X="-28.715140625" Y="1.206590209961" />
                  <Point X="-28.70488671875" Y="1.208054199219" />
                  <Point X="-28.6846015625" Y="1.212089355469" />
                  <Point X="-28.6745703125" Y="1.214660522461" />
                  <Point X="-28.622544921875" Y="1.231063842773" />
                  <Point X="-28.603451171875" Y="1.237676025391" />
                  <Point X="-28.584517578125" Y="1.246006469727" />
                  <Point X="-28.57527734375" Y="1.250689208984" />
                  <Point X="-28.55653125" Y="1.261512207031" />
                  <Point X="-28.547857421875" Y="1.267172607422" />
                  <Point X="-28.531173828125" Y="1.279405395508" />
                  <Point X="-28.51591796875" Y="1.293385009766" />
                  <Point X="-28.502279296875" Y="1.308939086914" />
                  <Point X="-28.49588671875" Y="1.317086303711" />
                  <Point X="-28.48347265625" Y="1.334817626953" />
                  <Point X="-28.478005859375" Y="1.343608398438" />
                  <Point X="-28.46805859375" Y="1.361739501953" />
                  <Point X="-28.463578125" Y="1.371079833984" />
                  <Point X="-28.442703125" Y="1.421477416992" />
                  <Point X="-28.435494140625" Y="1.440362182617" />
                  <Point X="-28.429705078125" Y="1.460229614258" />
                  <Point X="-28.427353515625" Y="1.470319213867" />
                  <Point X="-28.42359765625" Y="1.49163293457" />
                  <Point X="-28.422359375" Y="1.501909545898" />
                  <Point X="-28.4210078125" Y="1.522541625977" />
                  <Point X="-28.42191015625" Y="1.543199707031" />
                  <Point X="-28.4250546875" Y="1.563635253906" />
                  <Point X="-28.42718359375" Y="1.573767211914" />
                  <Point X="-28.432783203125" Y="1.594672119141" />
                  <Point X="-28.436005859375" Y="1.604518432617" />
                  <Point X="-28.44350390625" Y="1.623802734375" />
                  <Point X="-28.447779296875" Y="1.633240844727" />
                  <Point X="-28.472966796875" Y="1.681627319336" />
                  <Point X="-28.47298046875" Y="1.681650268555" />
                  <Point X="-28.482818359375" Y="1.699319213867" />
                  <Point X="-28.494306640625" Y="1.716507446289" />
                  <Point X="-28.50051953125" Y="1.724787597656" />
                  <Point X="-28.5144296875" Y="1.741362426758" />
                  <Point X="-28.5215078125" Y="1.748918823242" />
                  <Point X="-28.5364453125" Y="1.763217407227" />
                  <Point X="-28.5443046875" Y="1.769958862305" />
                  <Point X="-28.67455859375" Y="1.86990637207" />
                  <Point X="-29.22761328125" Y="2.294281005859" />
                  <Point X="-29.046275390625" Y="2.604958007812" />
                  <Point X="-29.00228515625" Y="2.680321044922" />
                  <Point X="-28.726337890625" Y="3.035013183594" />
                  <Point X="-28.25415625" Y="2.762398681641" />
                  <Point X="-28.24491796875" Y="2.757716796875" />
                  <Point X="-28.225984375" Y="2.749385742188" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.082615234375" Y="2.724818847656" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310546875" />
                  <Point X="-27.976431640625" Y="2.734227294922" />
                  <Point X="-27.95698828125" Y="2.7413046875" />
                  <Point X="-27.9384375" Y="2.750454345703" />
                  <Point X="-27.929416015625" Y="2.755533691406" />
                  <Point X="-27.911162109375" Y="2.767163330078" />
                  <Point X="-27.902744140625" Y="2.773195068359" />
                  <Point X="-27.886615234375" Y="2.786139892578" />
                  <Point X="-27.878904296875" Y="2.793052978516" />
                  <Point X="-27.82747265625" Y="2.844483154297" />
                  <Point X="-27.811267578125" Y="2.861486083984" />
                  <Point X="-27.7983203125" Y="2.877616455078" />
                  <Point X="-27.79228515625" Y="2.886037109375" />
                  <Point X="-27.78065625" Y="2.904289794922" />
                  <Point X="-27.775576171875" Y="2.913315429688" />
                  <Point X="-27.766427734375" Y="2.931865234375" />
                  <Point X="-27.7593515625" Y="2.951305175781" />
                  <Point X="-27.754435546875" Y="2.971395263672" />
                  <Point X="-27.75252734375" Y="2.981569580078" />
                  <Point X="-27.749701171875" Y="3.003026123047" />
                  <Point X="-27.74891015625" Y="3.0133515625" />
                  <Point X="-27.74845703125" Y="3.034038085938" />
                  <Point X="-27.748794921875" Y="3.044399169922" />
                  <Point X="-27.755134765625" Y="3.116855957031" />
                  <Point X="-27.757744140625" Y="3.140200683594" />
                  <Point X="-27.761779296875" Y="3.160487792969" />
                  <Point X="-27.764349609375" Y="3.170522949219" />
                  <Point X="-27.770859375" Y="3.191168701172" />
                  <Point X="-27.774509765625" Y="3.200866943359" />
                  <Point X="-27.782841796875" Y="3.219799804688" />
                  <Point X="-27.7875234375" Y="3.229034423828" />
                  <Point X="-27.845244140625" Y="3.329007324219" />
                  <Point X="-28.05938671875" Y="3.699915039062" />
                  <Point X="-27.72496875" Y="3.956309570312" />
                  <Point X="-27.648365234375" Y="4.015041748047" />
                  <Point X="-27.192525390625" Y="4.268296386719" />
                  <Point X="-27.11856640625" Y="4.171910644531" />
                  <Point X="-27.111822265625" Y="4.16405078125" />
                  <Point X="-27.097517578125" Y="4.149107421875" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.06508984375" Y="4.121895996094" />
                  <Point X="-27.047888671875" Y="4.110403320312" />
                  <Point X="-27.0389765625" Y="4.105128417969" />
                  <Point X="-26.95833203125" Y="4.063147460938" />
                  <Point X="-26.934326171875" Y="4.051286865234" />
                  <Point X="-26.915048828125" Y="4.043790283203" />
                  <Point X="-26.905205078125" Y="4.040568359375" />
                  <Point X="-26.884298828125" Y="4.034966308594" />
                  <Point X="-26.8741640625" Y="4.032834960938" />
                  <Point X="-26.853720703125" Y="4.029687988281" />
                  <Point X="-26.833052734375" Y="4.028785400391" />
                  <Point X="-26.812412109375" Y="4.030138427734" />
                  <Point X="-26.802130859375" Y="4.031378417969" />
                  <Point X="-26.78081640625" Y="4.03513671875" />
                  <Point X="-26.77073046875" Y="4.037488037109" />
                  <Point X="-26.75087109375" Y="4.043276123047" />
                  <Point X="-26.74109765625" Y="4.046713134766" />
                  <Point X="-26.6571015625" Y="4.081506103516" />
                  <Point X="-26.632587890625" Y="4.092270996094" />
                  <Point X="-26.614458984375" Y="4.102216308594" />
                  <Point X="-26.605666015625" Y="4.107683105469" />
                  <Point X="-26.587935546875" Y="4.120096191406" />
                  <Point X="-26.579787109375" Y="4.126490722656" />
                  <Point X="-26.564234375" Y="4.140128417969" />
                  <Point X="-26.5502578125" Y="4.155377929688" />
                  <Point X="-26.538025390625" Y="4.17205859375" />
                  <Point X="-26.532365234375" Y="4.180732910156" />
                  <Point X="-26.52154296875" Y="4.199475097656" />
                  <Point X="-26.516859375" Y="4.208716796875" />
                  <Point X="-26.508525390625" Y="4.227655761719" />
                  <Point X="-26.504875" Y="4.237353027344" />
                  <Point X="-26.477537109375" Y="4.324061523438" />
                  <Point X="-26.470025390625" Y="4.34976171875" />
                  <Point X="-26.465990234375" Y="4.370047851562" />
                  <Point X="-26.464525390625" Y="4.380301757812" />
                  <Point X="-26.462638671875" Y="4.4018671875" />
                  <Point X="-26.46230078125" Y="4.412219238281" />
                  <Point X="-26.462751953125" Y="4.432899902344" />
                  <Point X="-26.463541015625" Y="4.443228515625" />
                  <Point X="-26.470103515625" Y="4.493072265625" />
                  <Point X="-26.479263671875" Y="4.562654296875" />
                  <Point X="-26.03033203125" Y="4.68851953125" />
                  <Point X="-25.931169921875" Y="4.716320800781" />
                  <Point X="-25.36522265625" Y="4.782556152344" />
                  <Point X="-25.22566796875" Y="4.261728515625" />
                  <Point X="-25.220435546875" Y="4.247107910156" />
                  <Point X="-25.20766015625" Y="4.218913085938" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.780025390625" Y="4.218916015625" />
                  <Point X="-24.767251953125" Y="4.247108886719" />
                  <Point X="-24.7620234375" Y="4.261725585938" />
                  <Point X="-24.732447265625" Y="4.372100585938" />
                  <Point X="-24.621810546875" Y="4.785006347656" />
                  <Point X="-24.25871484375" Y="4.74698046875" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.633546875" Y="4.607883789062" />
                  <Point X="-23.546404296875" Y="4.586844238281" />
                  <Point X="-23.197041015625" Y="4.460127929688" />
                  <Point X="-23.14173828125" Y="4.440069335938" />
                  <Point X="-22.802763671875" Y="4.281541992188" />
                  <Point X="-22.749546875" Y="4.256653320312" />
                  <Point X="-22.42203125" Y="4.065842529297" />
                  <Point X="-22.3705703125" Y="4.035861572266" />
                  <Point X="-22.18221875" Y="3.901916015625" />
                  <Point X="-22.204927734375" Y="3.862581542969" />
                  <Point X="-22.934689453125" Y="2.59859765625" />
                  <Point X="-22.937625" Y="2.593105224609" />
                  <Point X="-22.94681640625" Y="2.573440185547" />
                  <Point X="-22.95581640625" Y="2.549562744141" />
                  <Point X="-22.958697265625" Y="2.54059765625" />
                  <Point X="-22.976880859375" Y="2.472598632813" />
                  <Point X="-22.981275390625" Y="2.451384765625" />
                  <Point X="-22.985900390625" Y="2.420194580078" />
                  <Point X="-22.986919921875" Y="2.407550292969" />
                  <Point X="-22.987263671875" Y="2.382243408203" />
                  <Point X="-22.986587890625" Y="2.369580810547" />
                  <Point X="-22.979498046875" Y="2.310781005859" />
                  <Point X="-22.97619921875" Y="2.2890390625" />
                  <Point X="-22.970857421875" Y="2.267115478516" />
                  <Point X="-22.967533203125" Y="2.256308837891" />
                  <Point X="-22.959267578125" Y="2.234221191406" />
                  <Point X="-22.9546796875" Y="2.223884765625" />
                  <Point X="-22.944314453125" Y="2.203837890625" />
                  <Point X="-22.938537109375" Y="2.194127441406" />
                  <Point X="-22.90215234375" Y="2.1405078125" />
                  <Point X="-22.8886640625" Y="2.122928710938" />
                  <Point X="-22.868787109375" Y="2.099938232422" />
                  <Point X="-22.860064453125" Y="2.091091796875" />
                  <Point X="-22.841541015625" Y="2.07461328125" />
                  <Point X="-22.831740234375" Y="2.066981201172" />
                  <Point X="-22.77812109375" Y="2.03059765625" />
                  <Point X="-22.75871875" Y="2.018245239258" />
                  <Point X="-22.738673828125" Y="2.007882202148" />
                  <Point X="-22.728341796875" Y="2.003297607422" />
                  <Point X="-22.706255859375" Y="1.995032470703" />
                  <Point X="-22.695451171875" Y="1.991707519531" />
                  <Point X="-22.67352734375" Y="1.986364746094" />
                  <Point X="-22.662408203125" Y="1.984346923828" />
                  <Point X="-22.603609375" Y="1.977256469727" />
                  <Point X="-22.5805" Y="1.975890258789" />
                  <Point X="-22.550970703125" Y="1.975946899414" />
                  <Point X="-22.538685546875" Y="1.976768188477" />
                  <Point X="-22.514326171875" Y="1.979992553711" />
                  <Point X="-22.502251953125" Y="1.982395874023" />
                  <Point X="-22.434251953125" Y="2.000579711914" />
                  <Point X="-22.422162109375" Y="2.004247070313" />
                  <Point X="-22.39779296875" Y="2.012534179688" />
                  <Point X="-22.389146484375" Y="2.015954833984" />
                  <Point X="-22.36396484375" Y="2.027872802734" />
                  <Point X="-22.13556640625" Y="2.159739501953" />
                  <Point X="-21.059595703125" Y="2.780950927734" />
                  <Point X="-20.986578125" Y="2.679472167969" />
                  <Point X="-20.9560390625" Y="2.637030761719" />
                  <Point X="-20.863115234375" Y="2.483471435547" />
                  <Point X="-20.866248046875" Y="2.481067138672" />
                  <Point X="-21.827046875" Y="1.743819946289" />
                  <Point X="-21.831859375" Y="1.739869995117" />
                  <Point X="-21.847880859375" Y="1.725216186523" />
                  <Point X="-21.865333984375" Y="1.706599609375" />
                  <Point X="-21.871423828125" Y="1.699420166016" />
                  <Point X="-21.92036328125" Y="1.635575561523" />
                  <Point X="-21.93194140625" Y="1.618657348633" />
                  <Point X="-21.94925" Y="1.590175048828" />
                  <Point X="-21.95521484375" Y="1.578653198242" />
                  <Point X="-21.96551953125" Y="1.554904174805" />
                  <Point X="-21.969859375" Y="1.542676879883" />
                  <Point X="-21.98808984375" Y="1.477491210938" />
                  <Point X="-21.993775390625" Y="1.454658569336" />
                  <Point X="-21.997228515625" Y="1.4323515625" />
                  <Point X="-21.998291015625" Y="1.421098388672" />
                  <Point X="-21.99910546875" Y="1.397521362305" />
                  <Point X="-21.99882421875" Y="1.386229980469" />
                  <Point X="-21.996921875" Y="1.363752807617" />
                  <Point X="-21.99530078125" Y="1.352566894531" />
                  <Point X="-21.9803359375" Y="1.280039916992" />
                  <Point X="-21.9749609375" Y="1.259742431641" />
                  <Point X="-21.9650625" Y="1.229266113281" />
                  <Point X="-21.960333984375" Y="1.217462158203" />
                  <Point X="-21.94934375" Y="1.19459362793" />
                  <Point X="-21.94308203125" Y="1.183528442383" />
                  <Point X="-21.90237890625" Y="1.121662231445" />
                  <Point X="-21.888263671875" Y="1.101427978516" />
                  <Point X="-21.873712890625" Y="1.084186279297" />
                  <Point X="-21.865923828125" Y="1.075999511719" />
                  <Point X="-21.848677734375" Y="1.059906494141" />
                  <Point X="-21.83996875" Y="1.052699584961" />
                  <Point X="-21.821755859375" Y="1.03937109375" />
                  <Point X="-21.812251953125" Y="1.033249633789" />
                  <Point X="-21.753267578125" Y="1.000046691895" />
                  <Point X="-21.733490234375" Y="0.99037701416" />
                  <Point X="-21.70485546875" Y="0.978379577637" />
                  <Point X="-21.69294140625" Y="0.974293029785" />
                  <Point X="-21.668677734375" Y="0.967732116699" />
                  <Point X="-21.656328125" Y="0.96525769043" />
                  <Point X="-21.576578125" Y="0.954717346191" />
                  <Point X="-21.564837890625" Y="0.953535217285" />
                  <Point X="-21.537349609375" Y="0.951627624512" />
                  <Point X="-21.52784765625" Y="0.951444824219" />
                  <Point X="-21.49939453125" Y="0.952797302246" />
                  <Point X="-21.282431640625" Y="0.98136126709" />
                  <Point X="-20.295296875" Y="1.111319702148" />
                  <Point X="-20.2604296875" Y="0.968092651367" />
                  <Point X="-20.247310546875" Y="0.914204101562" />
                  <Point X="-20.21612890625" Y="0.713920654297" />
                  <Point X="-21.3080078125" Y="0.421352661133" />
                  <Point X="-21.31396875" Y="0.419543762207" />
                  <Point X="-21.334373046875" Y="0.412137298584" />
                  <Point X="-21.3576171875" Y="0.401619262695" />
                  <Point X="-21.365994140625" Y="0.397316619873" />
                  <Point X="-21.444345703125" Y="0.352027801514" />
                  <Point X="-21.460662109375" Y="0.341448394775" />
                  <Point X="-21.4891328125" Y="0.32084286499" />
                  <Point X="-21.4992890625" Y="0.312353515625" />
                  <Point X="-21.51832421875" Y="0.294045288086" />
                  <Point X="-21.527203125" Y="0.284226043701" />
                  <Point X="-21.57421484375" Y="0.224322570801" />
                  <Point X="-21.58914453125" Y="0.204207351685" />
                  <Point X="-21.6008671875" Y="0.184929626465" />
                  <Point X="-21.606154296875" Y="0.174943069458" />
                  <Point X="-21.615927734375" Y="0.153482070923" />
                  <Point X="-21.6199921875" Y="0.142934005737" />
                  <Point X="-21.62683984375" Y="0.121429618835" />
                  <Point X="-21.629623046875" Y="0.110473136902" />
                  <Point X="-21.64529296875" Y="0.028648370743" />
                  <Point X="-21.64801171875" Y="0.008870678902" />
                  <Point X="-21.65084375" Y="-0.024726858139" />
                  <Point X="-21.651048828125" Y="-0.037695690155" />
                  <Point X="-21.649689453125" Y="-0.063543586731" />
                  <Point X="-21.648125" Y="-0.076422653198" />
                  <Point X="-21.632455078125" Y="-0.158247558594" />
                  <Point X="-21.62683984375" Y="-0.183989196777" />
                  <Point X="-21.6199921875" Y="-0.205493881226" />
                  <Point X="-21.615927734375" Y="-0.216042541504" />
                  <Point X="-21.606154296875" Y="-0.237503387451" />
                  <Point X="-21.6008671875" Y="-0.247490402222" />
                  <Point X="-21.58914453125" Y="-0.266767822266" />
                  <Point X="-21.582708984375" Y="-0.276058227539" />
                  <Point X="-21.535697265625" Y="-0.335961730957" />
                  <Point X="-21.521853515625" Y="-0.351722595215" />
                  <Point X="-21.499046875" Y="-0.374968841553" />
                  <Point X="-21.4894453125" Y="-0.383514068604" />
                  <Point X="-21.469173828125" Y="-0.399231811523" />
                  <Point X="-21.45850390625" Y="-0.406404205322" />
                  <Point X="-21.38015234375" Y="-0.451692871094" />
                  <Point X="-21.3704140625" Y="-0.456950561523" />
                  <Point X="-21.34417578125" Y="-0.470141571045" />
                  <Point X="-21.33530859375" Y="-0.474046203613" />
                  <Point X="-21.3080078125" Y="-0.483912963867" />
                  <Point X="-21.109041015625" Y="-0.537225646973" />
                  <Point X="-20.21512109375" Y="-0.776751220703" />
                  <Point X="-20.2310625" Y="-0.882492614746" />
                  <Point X="-20.238380859375" Y="-0.931038208008" />
                  <Point X="-20.2721953125" Y="-1.079219726563" />
                  <Point X="-20.28625390625" Y="-1.077369018555" />
                  <Point X="-21.563216796875" Y="-0.909253662109" />
                  <Point X="-21.571375" Y="-0.908535644531" />
                  <Point X="-21.59990234375" Y="-0.908042541504" />
                  <Point X="-21.63327734375" Y="-0.910840942383" />
                  <Point X="-21.645517578125" Y="-0.912676147461" />
                  <Point X="-21.799294921875" Y="-0.946100708008" />
                  <Point X="-21.82708203125" Y="-0.952140258789" />
                  <Point X="-21.842125" Y="-0.956742797852" />
                  <Point X="-21.871244140625" Y="-0.968366943359" />
                  <Point X="-21.8853203125" Y="-0.97538861084" />
                  <Point X="-21.9131484375" Y="-0.992281677246" />
                  <Point X="-21.925875" Y="-1.001530761719" />
                  <Point X="-21.949625" Y="-1.022001464844" />
                  <Point X="-21.9606484375" Y="-1.033223144531" />
                  <Point X="-22.05359765625" Y="-1.145011230469" />
                  <Point X="-22.070392578125" Y="-1.165211181641" />
                  <Point X="-22.078671875" Y="-1.176846435547" />
                  <Point X="-22.093396484375" Y="-1.201234375" />
                  <Point X="-22.099841796875" Y="-1.213986816406" />
                  <Point X="-22.11118359375" Y="-1.241373168945" />
                  <Point X="-22.115640625" Y="-1.254942382812" />
                  <Point X="-22.12246875" Y="-1.282586547852" />
                  <Point X="-22.12483984375" Y="-1.296661499023" />
                  <Point X="-22.138162109375" Y="-1.441432006836" />
                  <Point X="-22.140568359375" Y="-1.467591674805" />
                  <Point X="-22.140708984375" Y="-1.483320922852" />
                  <Point X="-22.138390625" Y="-1.514592529297" />
                  <Point X="-22.135931640625" Y="-1.530135009766" />
                  <Point X="-22.12819921875" Y="-1.561755615234" />
                  <Point X="-22.123208984375" Y="-1.576675170898" />
                  <Point X="-22.110837890625" Y="-1.605483276367" />
                  <Point X="-22.10345703125" Y="-1.619371948242" />
                  <Point X="-22.01835546875" Y="-1.751743408203" />
                  <Point X="-22.0029765625" Y="-1.775662841797" />
                  <Point X="-21.99825390625" Y="-1.782355224609" />
                  <Point X="-21.980201171875" Y="-1.804454589844" />
                  <Point X="-21.9565078125" Y="-1.828121582031" />
                  <Point X="-21.947201171875" Y="-1.83627746582" />
                  <Point X="-21.762560546875" Y="-1.977958129883" />
                  <Point X="-20.912828125" Y="-2.629980712891" />
                  <Point X="-20.933873046875" Y="-2.664034423828" />
                  <Point X="-20.954521484375" Y="-2.697445800781" />
                  <Point X="-20.998724609375" Y="-2.760252197266" />
                  <Point X="-21.014236328125" Y="-2.751296142578" />
                  <Point X="-22.151544921875" Y="-2.094671142578" />
                  <Point X="-22.158802734375" Y="-2.090887451172" />
                  <Point X="-22.184974609375" Y="-2.079512207031" />
                  <Point X="-22.2168828125" Y="-2.069325439453" />
                  <Point X="-22.228892578125" Y="-2.066337890625" />
                  <Point X="-22.411912109375" Y="-2.033284667969" />
                  <Point X="-22.444982421875" Y="-2.027312133789" />
                  <Point X="-22.460640625" Y="-2.025807739258" />
                  <Point X="-22.4919921875" Y="-2.025403686523" />
                  <Point X="-22.507685546875" Y="-2.026504150391" />
                  <Point X="-22.539859375" Y="-2.031462036133" />
                  <Point X="-22.555154296875" Y="-2.03513684082" />
                  <Point X="-22.5849296875" Y="-2.044960327148" />
                  <Point X="-22.59941015625" Y="-2.051109130859" />
                  <Point X="-22.751455078125" Y="-2.131128662109" />
                  <Point X="-22.778927734375" Y="-2.145588134766" />
                  <Point X="-22.791029296875" Y="-2.153170410156" />
                  <Point X="-22.81395703125" Y="-2.170061035156" />
                  <Point X="-22.824783203125" Y="-2.179369384766" />
                  <Point X="-22.8457421875" Y="-2.200326904297" />
                  <Point X="-22.855052734375" Y="-2.211153808594" />
                  <Point X="-22.87194921875" Y="-2.234086914062" />
                  <Point X="-22.87953515625" Y="-2.246193115234" />
                  <Point X="-22.9595546875" Y="-2.398237060547" />
                  <Point X="-22.974015625" Y="-2.425711181641" />
                  <Point X="-22.9801640625" Y="-2.440192382812" />
                  <Point X="-22.98998828125" Y="-2.469971191406" />
                  <Point X="-22.9936640625" Y="-2.485268798828" />
                  <Point X="-22.99862109375" Y="-2.517443359375" />
                  <Point X="-22.999720703125" Y="-2.533134033203" />
                  <Point X="-22.99931640625" Y="-2.564484130859" />
                  <Point X="-22.9978125" Y="-2.580143554688" />
                  <Point X="-22.9647578125" Y="-2.763162841797" />
                  <Point X="-22.95878515625" Y="-2.796233886719" />
                  <Point X="-22.95698046875" Y="-2.80423046875" />
                  <Point X="-22.948763671875" Y="-2.831534912109" />
                  <Point X="-22.935931640625" Y="-2.862473876953" />
                  <Point X="-22.930453125" Y="-2.873578613281" />
                  <Point X="-22.811802734375" Y="-3.079087646484" />
                  <Point X="-22.26410546875" Y="-4.027724853516" />
                  <Point X="-22.276244140625" Y="-4.036082763672" />
                  <Point X="-22.298212890625" Y="-4.007452636719" />
                  <Point X="-23.16608203125" Y="-2.876422363281" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849625732422" />
                  <Point X="-23.21674609375" Y="-2.828004638672" />
                  <Point X="-23.22669921875" Y="-2.820646972656" />
                  <Point X="-23.40720703125" Y="-2.704597900391" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.453716796875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663873535156" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.78901953125" Y="-2.664682617188" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670339355469" />
                  <Point X="-23.866423828125" Y="-2.677171386719" />
                  <Point X="-23.8799921875" Y="-2.681629394531" />
                  <Point X="-23.907376953125" Y="-2.69297265625" />
                  <Point X="-23.920123046875" Y="-2.6994140625" />
                  <Point X="-23.94450390625" Y="-2.714134033203" />
                  <Point X="-23.956138671875" Y="-2.722412597656" />
                  <Point X="-24.108578125" Y="-2.84916015625" />
                  <Point X="-24.136123046875" Y="-2.872062988281" />
                  <Point X="-24.147345703125" Y="-2.883087402344" />
                  <Point X="-24.16781640625" Y="-2.906836914062" />
                  <Point X="-24.177064453125" Y="-2.919562011719" />
                  <Point X="-24.19395703125" Y="-2.947388183594" />
                  <Point X="-24.20098046875" Y="-2.961466308594" />
                  <Point X="-24.21260546875" Y="-2.990587646484" />
                  <Point X="-24.21720703125" Y="-3.005630859375" />
                  <Point X="-24.26278515625" Y="-3.215326904297" />
                  <Point X="-24.271021484375" Y="-3.25321875" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323170166016" />
                  <Point X="-24.2744453125" Y="-3.335520507812" />
                  <Point X="-24.2408203125" Y="-3.590925292969" />
                  <Point X="-24.166912109375" Y="-4.152312011719" />
                  <Point X="-24.344931640625" Y="-3.4879375" />
                  <Point X="-24.347392578125" Y="-3.480119628906" />
                  <Point X="-24.3578515625" Y="-3.453583984375" />
                  <Point X="-24.373208984375" Y="-3.423817871094" />
                  <Point X="-24.37958984375" Y="-3.413209228516" />
                  <Point X="-24.518259765625" Y="-3.213411621094" />
                  <Point X="-24.543318359375" Y="-3.177308837891" />
                  <Point X="-24.553330078125" Y="-3.165170166016" />
                  <Point X="-24.575216796875" Y="-3.142713134766" />
                  <Point X="-24.587091796875" Y="-3.132394775391" />
                  <Point X="-24.61334765625" Y="-3.113151611328" />
                  <Point X="-24.626759765625" Y="-3.104936035156" />
                  <Point X="-24.6547578125" Y="-3.090829345703" />
                  <Point X="-24.66934375" Y="-3.084938232422" />
                  <Point X="-24.883927734375" Y="-3.018339111328" />
                  <Point X="-24.922703125" Y="-3.006304931641" />
                  <Point X="-24.936623046875" Y="-3.003108886719" />
                  <Point X="-24.964783203125" Y="-2.998839599609" />
                  <Point X="-24.9790234375" Y="-2.997766357422" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.279568359375" Y="-3.072904052734" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.3329296875" Y="-3.090829833984" />
                  <Point X="-25.360931640625" Y="-3.104938720703" />
                  <Point X="-25.37434765625" Y="-3.11315625" />
                  <Point X="-25.400603515625" Y="-3.132400878906" />
                  <Point X="-25.412478515625" Y="-3.142719726562" />
                  <Point X="-25.434361328125" Y="-3.165174560547" />
                  <Point X="-25.444369140625" Y="-3.177310546875" />
                  <Point X="-25.5830390625" Y="-3.377107910156" />
                  <Point X="-25.608095703125" Y="-3.4132109375" />
                  <Point X="-25.61246875" Y="-3.420129882812" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476214111328" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.704060546875" Y="-3.716734619141" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.720286847536" Y="0.998281707425" />
                  <Point X="-29.712852597068" Y="0.57237378335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.691877073791" Y="-0.629313140391" />
                  <Point X="-29.68183155258" Y="-1.204820665128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.630850930668" Y="1.317876869002" />
                  <Point X="-29.617391648119" Y="0.546795088197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.597304923363" Y="-0.603972602369" />
                  <Point X="-29.585561143623" Y="-1.27677329307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.535617613677" Y="1.305339199983" />
                  <Point X="-29.52193069917" Y="0.521216393043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.502732772935" Y="-0.578632064346" />
                  <Point X="-29.490764515529" Y="-1.264293071968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.440384296686" Y="1.292801530965" />
                  <Point X="-29.426469750221" Y="0.49563769789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.408160622508" Y="-0.553291526324" />
                  <Point X="-29.395967887434" Y="-1.251812850866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.345150979695" Y="1.280263861946" />
                  <Point X="-29.331008801273" Y="0.470059002736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.31358847208" Y="-0.527950988301" />
                  <Point X="-29.30117125934" Y="-1.239332629764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.249917662704" Y="1.267726192928" />
                  <Point X="-29.235547852324" Y="0.444480307582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.219016321652" Y="-0.502610450279" />
                  <Point X="-29.206374631245" Y="-1.226852408662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.174412728419" Y="2.38542681217" />
                  <Point X="-29.172077944981" Y="2.251667158578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.154684345713" Y="1.255188523909" />
                  <Point X="-29.140086903375" Y="0.418901612429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.124444171225" Y="-0.477269912256" />
                  <Point X="-29.111578003151" Y="-1.21437218756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.091330281656" Y="-2.374363375069" />
                  <Point X="-29.087377649055" Y="-2.600809545127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.082157148974" Y="2.543483612858" />
                  <Point X="-29.075773595598" Y="2.177770084879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.059451028722" Y="1.242650854891" />
                  <Point X="-29.044625954426" Y="0.393322917275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.029872020797" Y="-0.451929374234" />
                  <Point X="-29.016781375056" Y="-1.201891966458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.997571590608" Y="-2.302419780427" />
                  <Point X="-28.989592527327" Y="-2.759540009615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.989811053999" Y="2.696354782366" />
                  <Point X="-28.979469246214" Y="2.103873011179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.964217711732" Y="1.230113185872" />
                  <Point X="-28.949165005477" Y="0.367744222121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.935299870369" Y="-0.426588836211" />
                  <Point X="-28.921984746962" Y="-1.189411745356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.903812899559" Y="-2.230476185784" />
                  <Point X="-28.892348023021" Y="-2.887298522798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.896881556272" Y="2.8158028306" />
                  <Point X="-28.88316489683" Y="2.02997593748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.868984394741" Y="1.217575516854" />
                  <Point X="-28.853704056528" Y="0.342165526968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.840727719942" Y="-0.401248298189" />
                  <Point X="-28.827188118867" Y="-1.176931524254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.810054208511" Y="-2.158532591142" />
                  <Point X="-28.795103521992" Y="-3.015056848149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.803952058545" Y="2.935250878834" />
                  <Point X="-28.786860547447" Y="1.95607886378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.773755236374" Y="1.205276095288" />
                  <Point X="-28.758243107579" Y="0.316586831814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.746155569514" Y="-0.375907760166" />
                  <Point X="-28.732391490772" Y="-1.164451303152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.716295517462" Y="-2.086588996499" />
                  <Point X="-28.700971087438" Y="-2.964525004594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.710519532254" Y="3.025880439925" />
                  <Point X="-28.690556198063" Y="1.882181790081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.678885265863" Y="1.213554532115" />
                  <Point X="-28.662782158045" Y="0.291008103126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.651583419086" Y="-0.350567222144" />
                  <Point X="-28.637594862678" Y="-1.151971082049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.622536826414" Y="-2.014645401857" />
                  <Point X="-28.606904588623" Y="-2.910215705117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.614537786068" Y="2.970465291004" />
                  <Point X="-28.594251848975" Y="1.808284733313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.584437949755" Y="1.246046823562" />
                  <Point X="-28.567321208327" Y="0.265429363869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.557011268659" Y="-0.325226684121" />
                  <Point X="-28.542798234583" Y="-1.139490860947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.528778135365" Y="-1.942701807214" />
                  <Point X="-28.512838089807" Y="-2.855906405639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.518556039881" Y="2.915050142082" />
                  <Point X="-28.497714680599" Y="1.721049468441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.490790532967" Y="1.324365316316" />
                  <Point X="-28.471786172453" Y="0.235606231626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.462552686079" Y="-0.293379848428" />
                  <Point X="-28.448001606382" Y="-1.127010645963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.435019444317" Y="-1.870758212572" />
                  <Point X="-28.418771590991" Y="-2.801597106162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.422574293695" Y="2.85963499316" />
                  <Point X="-28.375592752207" Y="0.168064283988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.368757459758" Y="-0.223529358137" />
                  <Point X="-28.353204978065" Y="-1.114530437623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.341260753268" Y="-1.79881461793" />
                  <Point X="-28.324705092176" Y="-2.747287806684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.326592547508" Y="2.804219844239" />
                  <Point X="-28.258408349748" Y="-1.102050229283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.24750206222" Y="-1.726871023287" />
                  <Point X="-28.23063859336" Y="-2.692978507206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.230656830234" Y="2.75144168997" />
                  <Point X="-28.163323009089" Y="-1.106110339892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.153743371171" Y="-1.654927428645" />
                  <Point X="-28.136572094545" Y="-2.638669207729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.135258046807" Y="2.729424455185" />
                  <Point X="-28.067625845078" Y="-1.145221786922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.059984680123" Y="-1.582983834002" />
                  <Point X="-28.042505595729" Y="-2.584359908251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.057212646544" Y="3.701581876004" />
                  <Point X="-28.0571148664" Y="3.69598005532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.040144520596" Y="2.72374959533" />
                  <Point X="-27.970770746132" Y="-1.250671281908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.967140752483" Y="-1.458633478784" />
                  <Point X="-27.948439096914" Y="-2.530050608774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.927339568895" Y="-3.738841759392" />
                  <Point X="-27.924970197177" Y="-3.874582974195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.963452924594" Y="3.773466410347" />
                  <Point X="-27.959138247208" Y="3.526278708437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.945535077927" Y="2.746953662296" />
                  <Point X="-27.854372598407" Y="-2.47574129161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.835113379201" Y="-3.579101220946" />
                  <Point X="-27.828661358775" Y="-3.948737223612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.869693202644" Y="3.845350944689" />
                  <Point X="-27.861161628016" Y="3.356577361555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.85179839512" Y="2.820158108257" />
                  <Point X="-27.760306100571" Y="-2.421431936007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.742887189507" Y="-3.4193606825" />
                  <Point X="-27.732393074739" Y="-4.020568114903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.775933480694" Y="3.917235479032" />
                  <Point X="-27.762793518951" Y="3.164447574903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.759091645197" Y="2.952367369609" />
                  <Point X="-27.666191867503" Y="-2.369857330001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.650660999813" Y="-3.259620144054" />
                  <Point X="-27.636340481354" Y="-4.080042097105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.68217376948" Y="3.989120628415" />
                  <Point X="-27.571545999928" Y="-2.348740044514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.558434810119" Y="-3.099879605609" />
                  <Point X="-27.540287900313" Y="-4.13951537214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.588195260607" Y="4.048470868328" />
                  <Point X="-27.476217067205" Y="-2.366755535156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.466208620425" Y="-2.940139067163" />
                  <Point X="-27.446169948939" Y="-4.088153787753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.494093356955" Y="4.100751826069" />
                  <Point X="-27.380028597326" Y="-2.434013876469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.373982430731" Y="-2.780398528717" />
                  <Point X="-27.352906300798" Y="-3.987849203907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.399991453303" Y="4.15303278381" />
                  <Point X="-27.258987239241" Y="-3.925093229534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.30588954965" Y="4.205313741551" />
                  <Point X="-27.165068158729" Y="-3.862338341124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.211787645998" Y="4.257594699292" />
                  <Point X="-27.071082740169" Y="-3.803383956892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.115209260489" Y="4.167998106498" />
                  <Point X="-26.976518863592" Y="-3.77756941033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.018915105456" Y="4.09468506672" />
                  <Point X="-26.881459612806" Y="-3.780134833142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.923066687235" Y="4.046908271828" />
                  <Point X="-26.786336314682" Y="-3.78636952548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.82774195681" Y="4.029133530705" />
                  <Point X="-26.691213016558" Y="-3.792604217818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.733092220154" Y="4.050029156684" />
                  <Point X="-26.595992919132" Y="-3.804384538483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.638767713536" Y="4.089557199069" />
                  <Point X="-26.500167599096" Y="-3.850838039205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.545026661898" Y="4.162511354838" />
                  <Point X="-26.403690227667" Y="-3.934647539287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.457105151341" Y="4.568866795902" />
                  <Point X="-26.307198680875" Y="-4.019269145315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.362553398425" Y="4.595375906564" />
                  <Point X="-26.21029593806" Y="-4.12744815573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.268001645509" Y="4.621885017227" />
                  <Point X="-26.104576023816" Y="-4.740762579034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.173449892592" Y="4.64839412789" />
                  <Point X="-26.009187459151" Y="-4.762194381356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.078898139676" Y="4.674903238553" />
                  <Point X="-25.918455268052" Y="-4.516862720705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.984346383304" Y="4.701412151235" />
                  <Point X="-25.829251769041" Y="-4.18395234901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.889676907369" Y="4.721176914716" />
                  <Point X="-25.74004827003" Y="-3.851041977315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.794856140305" Y="4.73227421516" />
                  <Point X="-25.650844766903" Y="-3.51813184145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.700035373241" Y="4.743371515604" />
                  <Point X="-25.558898987857" Y="-3.342326587733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.605214606176" Y="4.754468816048" />
                  <Point X="-25.46621545893" Y="-3.208786996387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.510393839112" Y="4.765566116491" />
                  <Point X="-25.372885857494" Y="-3.112260874321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.415573072047" Y="4.776663416935" />
                  <Point X="-25.278563804285" Y="-3.072592276247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.317556306288" Y="4.604662074787" />
                  <Point X="-25.184061291231" Y="-3.043262215772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.21613503672" Y="4.237616840066" />
                  <Point X="-25.089558778176" Y="-3.013932155297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.119082373451" Y="4.120848892583" />
                  <Point X="-24.994826479792" Y="-2.997766487587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.023484957541" Y="4.087452010458" />
                  <Point X="-24.899537474574" Y="-3.013494532965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.928608186772" Y="4.095340860787" />
                  <Point X="-24.804005468343" Y="-3.043144097073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.834473589693" Y="4.145748813365" />
                  <Point X="-24.708473459982" Y="-3.07279378324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.74273965346" Y="4.333690533682" />
                  <Point X="-24.612746854261" Y="-3.113591944706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.653536199396" Y="4.666603480431" />
                  <Point X="-24.515931468642" Y="-3.216766264698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.560476338149" Y="4.778583007546" />
                  <Point X="-24.41846578352" Y="-3.357196218287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.465287861075" Y="4.768614215651" />
                  <Point X="-24.319513397936" Y="-3.582799184315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.370099384001" Y="4.758645423755" />
                  <Point X="-24.233282462223" Y="-3.079590775352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.221840691921" Y="-3.735089356891" />
                  <Point X="-24.217878114388" Y="-3.962105271721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.274910906927" Y="4.74867663186" />
                  <Point X="-24.141793177848" Y="-2.87763295944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.179722435174" Y="4.738708144837" />
                  <Point X="-24.048152669067" Y="-2.798918707207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.08432407606" Y="4.716715218879" />
                  <Point X="-23.954494043173" Y="-2.721242383734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.988907501554" Y="4.693678733877" />
                  <Point X="-23.860275351926" Y="-2.675652182809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.893490927048" Y="4.670642248874" />
                  <Point X="-23.765490148943" Y="-2.662517417476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.798074352543" Y="4.647605763872" />
                  <Point X="-23.670628048541" Y="-2.653788102371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.702657778037" Y="4.624569278869" />
                  <Point X="-23.57574277258" Y="-2.646386514124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.607241200028" Y="4.601532593168" />
                  <Point X="-23.480407159121" Y="-2.664784743878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.51175095011" Y="4.574275246581" />
                  <Point X="-23.384442282554" Y="-2.719233432947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.41613110355" Y="4.539593313376" />
                  <Point X="-23.28834946556" Y="-2.781011824193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.32051125699" Y="4.504911380172" />
                  <Point X="-23.192151851062" Y="-2.84879406037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.224891410429" Y="4.470229446967" />
                  <Point X="-23.095039046845" Y="-2.969007480456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.129248536746" Y="4.434228290722" />
                  <Point X="-22.997812883928" Y="-3.095715216083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.033452063329" Y="4.389427411684" />
                  <Point X="-22.916404077334" Y="-2.316247214945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.905934548094" Y="-2.916046143383" />
                  <Point X="-22.900586721012" Y="-3.222422951709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.937655589912" Y="4.344626532647" />
                  <Point X="-22.907985873115" Y="2.64484959573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.899113422241" Y="2.136547225614" />
                  <Point X="-22.823793668397" Y="-2.178518582164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.807957937206" Y="-3.085747014491" />
                  <Point X="-22.803360558096" Y="-3.349130687336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.841859116496" Y="4.299825653609" />
                  <Point X="-22.815759680816" Y="2.804589984961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.802538803298" Y="2.047166419231" />
                  <Point X="-22.729805275206" Y="-2.119734614483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.709981340929" Y="-3.25544704858" />
                  <Point X="-22.706134395179" Y="-3.475838422962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.746055565166" Y="4.2546192812" />
                  <Point X="-22.723533488518" Y="2.964330374191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.70661668766" Y="1.995167502105" />
                  <Point X="-22.635655700096" Y="-2.070184752674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.612004744651" Y="-3.425147082669" />
                  <Point X="-22.608908232263" Y="-3.602546158589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.650064933511" Y="4.19869508413" />
                  <Point X="-22.631307296219" Y="3.124070763422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.611305778249" Y="1.978184566352" />
                  <Point X="-22.541311048425" Y="-2.031810819628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.514028148374" Y="-3.594847116758" />
                  <Point X="-22.511682069346" Y="-3.729253894215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.554074301856" Y="4.142770887059" />
                  <Point X="-22.539081103921" Y="3.283811152652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.516318262963" Y="1.979728867584" />
                  <Point X="-22.446377441607" Y="-2.027178104358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.416051552096" Y="-3.764547150848" />
                  <Point X="-22.41445590643" Y="-3.855961629842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.458083670201" Y="4.086846689989" />
                  <Point X="-22.446854911622" Y="3.443551541883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.421734298052" Y="2.004392554279" />
                  <Point X="-22.351064565719" Y="-2.044273699548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.318074955819" Y="-3.934247184937" />
                  <Point X="-22.317229743513" Y="-3.982669365468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.362073782143" Y="4.029819294341" />
                  <Point X="-22.354628719324" Y="3.603291931114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.327497187978" Y="2.04892754132" />
                  <Point X="-22.255749626146" Y="-2.061487523115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.26586506169" Y="3.961400798398" />
                  <Point X="-22.262402527026" Y="3.763032320344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.233430695182" Y="2.103237185676" />
                  <Point X="-22.160232826873" Y="-2.090265881211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.139364202387" Y="2.157546830032" />
                  <Point X="-22.081092179674" Y="-1.180855115325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.072600056706" Y="-1.66736851435" />
                  <Point X="-22.06426185073" Y="-2.145064014755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.045297703332" Y="2.211856115798" />
                  <Point X="-21.988078793016" Y="-1.066213060756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.975103864374" Y="-1.809546224799" />
                  <Point X="-21.968280106264" Y="-2.200479065082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.951231204013" Y="2.266165386478" />
                  <Point X="-21.939705887442" Y="1.605880442298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.932040774588" Y="1.16674642101" />
                  <Point X="-21.894551850233" Y="-0.980992616857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.878705376607" Y="-1.888836482879" />
                  <Point X="-21.872298361799" Y="-2.255894115409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.857164704695" Y="2.320474657158" />
                  <Point X="-21.846791816031" Y="1.726212263563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.834971777661" Y="1.049042718883" />
                  <Point X="-21.800143201339" Y="-0.946285082238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.782401021254" Y="-1.962733898567" />
                  <Point X="-21.776316617333" Y="-2.311309165735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.763098205377" Y="2.374783927838" />
                  <Point X="-21.753075442851" Y="1.800580247272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.738980145098" Y="0.993061179848" />
                  <Point X="-21.705487850414" Y="-0.925711097527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.686096672769" Y="-2.036630920773" />
                  <Point X="-21.680334872868" Y="-2.366724216062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.669031706059" Y="2.429093198518" />
                  <Point X="-21.659316751987" Y="1.872523852507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.643450654034" Y="0.963555709528" />
                  <Point X="-21.628628389606" Y="0.114388749204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.62322185724" Y="-0.195351282636" />
                  <Point X="-21.610765885612" Y="-0.90895341926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.589792326067" Y="-2.11052784088" />
                  <Point X="-21.584353128402" Y="-2.422139266389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.57496520674" Y="2.483402469198" />
                  <Point X="-21.565558061123" Y="1.944467457742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.548241170557" Y="0.952383461634" />
                  <Point X="-21.536374453801" Y="0.272539713976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.525551400065" Y="-0.347512619254" />
                  <Point X="-21.515636834797" Y="-0.915517683048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.493487979364" Y="-2.184424760988" />
                  <Point X="-21.488371383936" Y="-2.477554316716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.480898707422" Y="2.537711739878" />
                  <Point X="-21.47179937026" Y="2.016411062977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.453339757967" Y="0.958860583027" />
                  <Point X="-21.442763416667" Y="0.352942395706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.429213450686" Y="-0.423334635425" />
                  <Point X="-21.42040351719" Y="-0.928055387356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.397183632661" Y="-2.258321681095" />
                  <Point X="-21.392389639471" Y="-2.532969367043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.386832208104" Y="2.592021010558" />
                  <Point X="-21.378040679396" Y="2.088354668211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.358543131695" Y="0.971340908544" />
                  <Point X="-21.348669278113" Y="0.405668215659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.333301141946" Y="-0.474771715659" />
                  <Point X="-21.325170199583" Y="-0.940593091664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.300879285959" Y="-2.332218601202" />
                  <Point X="-21.296407895005" Y="-2.588384417369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.292765708786" Y="2.646330281238" />
                  <Point X="-21.284281988533" Y="2.160298273446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.26374650493" Y="0.983821205771" />
                  <Point X="-21.254180334807" Y="0.435775686466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.237798738816" Y="-0.502725319271" />
                  <Point X="-21.229936881976" Y="-0.953130795972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.204574939256" Y="-2.40611552131" />
                  <Point X="-21.200426150539" Y="-2.643799467696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.198699209467" Y="2.700639551918" />
                  <Point X="-21.190523297669" Y="2.232241878681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.168949876153" Y="0.996301387762" />
                  <Point X="-21.159608184145" Y="0.461116211107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.142337792663" Y="-0.528303854239" />
                  <Point X="-21.13470356437" Y="-0.96566850028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.108270592553" Y="-2.480012441417" />
                  <Point X="-21.104444406074" Y="-2.699214518023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.104632710149" Y="2.754948822598" />
                  <Point X="-21.096764606805" Y="2.304185483916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.074153247375" Y="1.008781569753" />
                  <Point X="-21.065036033484" Y="0.486456735747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.046876844263" Y="-0.553882517959" />
                  <Point X="-21.039470246763" Y="-0.978206204588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.011966245851" Y="-2.553909361525" />
                  <Point X="-21.00846265921" Y="-2.754629705763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.008840856071" Y="2.710412585278" />
                  <Point X="-21.003005915942" Y="2.376129089151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.979356618598" Y="1.021261751745" />
                  <Point X="-20.970463882823" Y="0.511797260387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.951415894659" Y="-0.579461250643" />
                  <Point X="-20.944236929156" Y="-0.990743908896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.915661899148" Y="-2.627806281632" />
                  <Point X="-20.915547146086" Y="-2.634380480143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.911253665547" Y="2.563021591873" />
                  <Point X="-20.909247225078" Y="2.448072694386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.884559989821" Y="1.033741933736" />
                  <Point X="-20.875891732162" Y="0.537137785028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.855954945055" Y="-0.605039983328" />
                  <Point X="-20.849003611549" Y="-1.003281613204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.789763361044" Y="1.046222115727" />
                  <Point X="-20.7813195815" Y="0.562478309668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.760493995451" Y="-0.630618716012" />
                  <Point X="-20.753770293942" Y="-1.015819317512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.694966732266" Y="1.058702297718" />
                  <Point X="-20.686747430839" Y="0.587818834309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.665033045847" Y="-0.656197448696" />
                  <Point X="-20.658536976335" Y="-1.02835702182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.600170103489" Y="1.071182479709" />
                  <Point X="-20.592175280178" Y="0.613159358949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.569572096244" Y="-0.68177618138" />
                  <Point X="-20.563303658728" Y="-1.040894726128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.505373474712" Y="1.0836626617" />
                  <Point X="-20.497603129517" Y="0.638499883589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.47411114664" Y="-0.707354914064" />
                  <Point X="-20.468070341121" Y="-1.053432430436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.410576845935" Y="1.096142843691" />
                  <Point X="-20.403030978855" Y="0.66384040823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.378650197036" Y="-0.732933646749" />
                  <Point X="-20.372837023514" Y="-1.065970134744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.315780217158" Y="1.108623025682" />
                  <Point X="-20.308458828194" Y="0.68918093287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.283189247432" Y="-0.758512379433" />
                  <Point X="-20.277603707425" Y="-1.078507752095" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.18918359375" Y="-4.803303710938" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521543212891" />
                  <Point X="-24.674349609375" Y="-3.321745605469" />
                  <Point X="-24.699408203125" Y="-3.285642822266" />
                  <Point X="-24.7256640625" Y="-3.266399658203" />
                  <Point X="-24.940248046875" Y="-3.199800537109" />
                  <Point X="-24.9790234375" Y="-3.187766357422" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.223248046875" Y="-3.254365478516" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.28564453125" />
                  <Point X="-25.42694921875" Y="-3.485441894531" />
                  <Point X="-25.452005859375" Y="-3.521544921875" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.520533203125" Y="-3.765910400391" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-26.055845703125" Y="-4.94668359375" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.349228515625" Y="-4.87400390625" />
                  <Point X="-26.349990234375" Y="-4.861254394531" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.360947265625" Y="-4.277035644531" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.202628417969" />
                  <Point X="-26.58384375" Y="-4.02937109375" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.91144921875" Y="-3.968576904297" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791503906" />
                  <Point X="-27.208365234375" Y="-4.119779785156" />
                  <Point X="-27.24784375" Y="-4.146159667969" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.29415234375" Y="-4.202149414062" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.790755859375" Y="-4.207905273438" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.2006015625" Y="-3.902150634766" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-28.136537109375" Y="-3.721185058594" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597593505859" />
                  <Point X="-27.513978515625" Y="-2.568764648438" />
                  <Point X="-27.528671875" Y="-2.554071044922" />
                  <Point X="-27.531322265625" Y="-2.551420654297" />
                  <Point X="-27.56015234375" Y="-2.53719921875" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-27.789015625" Y="-2.657400634766" />
                  <Point X="-28.84295703125" Y="-3.265893554688" />
                  <Point X="-29.110287109375" Y="-2.914677978516" />
                  <Point X="-29.161703125" Y="-2.847126464844" />
                  <Point X="-29.408875" Y="-2.432658691406" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-29.26696484375" Y="-2.269643066406" />
                  <Point X="-28.163787109375" Y="-1.423144897461" />
                  <Point X="-28.145822265625" Y="-1.396013427734" />
                  <Point X="-28.139298828125" Y="-1.37082421875" />
                  <Point X="-28.1381171875" Y="-1.366271850586" />
                  <Point X="-28.14032421875" Y="-1.334599243164" />
                  <Point X="-28.16115625" Y="-1.310640380859" />
                  <Point X="-28.1835859375" Y="-1.297439208984" />
                  <Point X="-28.187638671875" Y="-1.295053710938" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.468642578125" Y="-1.321367675781" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.907283203125" Y="-1.089918212891" />
                  <Point X="-29.927390625" Y="-1.011190368652" />
                  <Point X="-29.992787109375" Y="-0.553950439453" />
                  <Point X="-29.99839453125" Y="-0.514742126465" />
                  <Point X="-29.8141484375" Y="-0.465373291016" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.5418984375" Y="-0.121427024841" />
                  <Point X="-28.518392578125" Y="-0.105112861633" />
                  <Point X="-28.51414453125" Y="-0.102165000916" />
                  <Point X="-28.4948984375" Y="-0.075909889221" />
                  <Point X="-28.487064453125" Y="-0.050664669037" />
                  <Point X="-28.4856484375" Y="-0.046102855682" />
                  <Point X="-28.4856484375" Y="-0.016457221985" />
                  <Point X="-28.493482421875" Y="0.008787993431" />
                  <Point X="-28.4948984375" Y="0.013349655151" />
                  <Point X="-28.51414453125" Y="0.039604923248" />
                  <Point X="-28.537650390625" Y="0.055919086456" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-28.78454296875" Y="0.126931373596" />
                  <Point X="-29.998185546875" Y="0.452126037598" />
                  <Point X="-29.930603515625" Y="0.908842285156" />
                  <Point X="-29.91764453125" Y="0.996414916992" />
                  <Point X="-29.786005859375" Y="1.482206542969" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.654087890625" Y="1.512575805664" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866943359" />
                  <Point X="-28.679677734375" Y="1.412270385742" />
                  <Point X="-28.67027734375" Y="1.415234375" />
                  <Point X="-28.65153125" Y="1.426057373047" />
                  <Point X="-28.6391171875" Y="1.443788574219" />
                  <Point X="-28.6182421875" Y="1.494186157227" />
                  <Point X="-28.614470703125" Y="1.50329284668" />
                  <Point X="-28.61071484375" Y="1.524606445312" />
                  <Point X="-28.616314453125" Y="1.545511108398" />
                  <Point X="-28.641501953125" Y="1.593897583008" />
                  <Point X="-28.64605859375" Y="1.602646728516" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-28.79022265625" Y="1.719169311523" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.210369140625" Y="2.700736816406" />
                  <Point X="-29.16001171875" Y="2.7870078125" />
                  <Point X="-28.811306640625" Y="3.235220947266" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.712416015625" Y="3.246368408203" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.066056640625" Y="2.914095703125" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927405029297" />
                  <Point X="-27.9618203125" Y="2.978835205078" />
                  <Point X="-27.95252734375" Y="2.988128417969" />
                  <Point X="-27.9408984375" Y="3.006381103516" />
                  <Point X="-27.938072265625" Y="3.027837646484" />
                  <Point X="-27.944412109375" Y="3.100294433594" />
                  <Point X="-27.945556640625" Y="3.113387207031" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.009787109375" Y="3.234005859375" />
                  <Point X="-28.30727734375" Y="3.749276855469" />
                  <Point X="-27.84057421875" Y="4.107093261719" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.203669921875" Y="4.479457519531" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.1371953125" Y="4.508300292969" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.8706015625" Y="4.2316796875" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.218491699219" />
                  <Point X="-26.81380859375" Y="4.22225" />
                  <Point X="-26.7298125" Y="4.25704296875" />
                  <Point X="-26.714634765625" Y="4.263329589844" />
                  <Point X="-26.696904296875" Y="4.275742675781" />
                  <Point X="-26.68608203125" Y="4.294484863281" />
                  <Point X="-26.658744140625" Y="4.381193359375" />
                  <Point X="-26.653802734375" Y="4.396861328125" />
                  <Point X="-26.651916015625" Y="4.418426757812" />
                  <Point X="-26.658478515625" Y="4.468270507812" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.081625" Y="4.871465820312" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.3022890625" Y="4.98121875" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.205810546875" Y="4.921727539062" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.915970703125" Y="4.421278320312" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.23892578125" Y="4.935947265625" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.58895703125" Y="4.792577148438" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.132255859375" Y="4.6387421875" />
                  <Point X="-23.068970703125" Y="4.615788085938" />
                  <Point X="-22.722275390625" Y="4.453650390625" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.32638671875" Y="4.230012695313" />
                  <Point X="-22.26747265625" Y="4.195688964844" />
                  <Point X="-21.951603515625" Y="3.971061523438" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.0403828125" Y="3.767581787109" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491514648438" />
                  <Point X="-22.793330078125" Y="2.423515625" />
                  <Point X="-22.793330078125" Y="2.423515380859" />
                  <Point X="-22.797955078125" Y="2.392325439453" />
                  <Point X="-22.790865234375" Y="2.333525634766" />
                  <Point X="-22.789583984375" Y="2.322900634766" />
                  <Point X="-22.781318359375" Y="2.300812988281" />
                  <Point X="-22.74493359375" Y="2.247193359375" />
                  <Point X="-22.725056640625" Y="2.224202880859" />
                  <Point X="-22.6714375" Y="2.187819580078" />
                  <Point X="-22.661748046875" Y="2.181245361328" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.58086328125" Y="2.165889892578" />
                  <Point X="-22.551333984375" Y="2.165946533203" />
                  <Point X="-22.483333984375" Y="2.184130371094" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-22.23056640625" Y="2.324283935547" />
                  <Point X="-21.005751953125" Y="3.031430419922" />
                  <Point X="-20.8323515625" Y="2.790443359375" />
                  <Point X="-20.79740234375" Y="2.741872070312" />
                  <Point X="-20.621314453125" Y="2.450884521484" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-20.750583984375" Y="2.330330078125" />
                  <Point X="-21.7113828125" Y="1.593082641602" />
                  <Point X="-21.720630859375" Y="1.583830200195" />
                  <Point X="-21.7695703125" Y="1.519985473633" />
                  <Point X="-21.78687890625" Y="1.491503295898" />
                  <Point X="-21.805109375" Y="1.426317626953" />
                  <Point X="-21.808404296875" Y="1.414538818359" />
                  <Point X="-21.80921875" Y="1.390961791992" />
                  <Point X="-21.79425390625" Y="1.318434692383" />
                  <Point X="-21.78435546875" Y="1.287958496094" />
                  <Point X="-21.74365234375" Y="1.226092163086" />
                  <Point X="-21.736298828125" Y="1.214913085938" />
                  <Point X="-21.719052734375" Y="1.198819946289" />
                  <Point X="-21.660068359375" Y="1.16561706543" />
                  <Point X="-21.63143359375" Y="1.153619506836" />
                  <Point X="-21.55168359375" Y="1.143079467773" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-21.307232421875" Y="1.169735717773" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.0758203125" Y="1.013033569336" />
                  <Point X="-20.060806640625" Y="0.951366333008" />
                  <Point X="-20.005318359375" Y="0.594969238281" />
                  <Point X="-20.002140625" Y="0.574556335449" />
                  <Point X="-20.157734375" Y="0.532864868164" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819503784" />
                  <Point X="-21.349263671875" Y="0.187530685425" />
                  <Point X="-21.377734375" Y="0.166925170898" />
                  <Point X="-21.42474609375" Y="0.107021781921" />
                  <Point X="-21.433240234375" Y="0.096197494507" />
                  <Point X="-21.443013671875" Y="0.074736480713" />
                  <Point X="-21.45868359375" Y="-0.00708838129" />
                  <Point X="-21.461515625" Y="-0.040685997009" />
                  <Point X="-21.445845703125" Y="-0.122511009216" />
                  <Point X="-21.443013671875" Y="-0.137296554565" />
                  <Point X="-21.433240234375" Y="-0.158757415771" />
                  <Point X="-21.386228515625" Y="-0.218660812378" />
                  <Point X="-21.363421875" Y="-0.241907058716" />
                  <Point X="-21.2850703125" Y="-0.287195861816" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-21.059865234375" Y="-0.353699737549" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.0431875" Y="-0.910817932129" />
                  <Point X="-20.051568359375" Y="-0.966412109375" />
                  <Point X="-20.12266015625" Y="-1.277945068359" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-20.3110546875" Y="-1.265743530273" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.758939453125" Y="-1.13176550293" />
                  <Point X="-21.7867265625" Y="-1.137805053711" />
                  <Point X="-21.8145546875" Y="-1.154697998047" />
                  <Point X="-21.90750390625" Y="-1.266486206055" />
                  <Point X="-21.924298828125" Y="-1.286686035156" />
                  <Point X="-21.935640625" Y="-1.314072387695" />
                  <Point X="-21.948962890625" Y="-1.458842895508" />
                  <Point X="-21.951369140625" Y="-1.485002685547" />
                  <Point X="-21.94363671875" Y="-1.516623291016" />
                  <Point X="-21.85853515625" Y="-1.648994628906" />
                  <Point X="-21.84315625" Y="-1.67291394043" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-21.646896484375" Y="-1.827221069336" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.77224609375" Y="-2.763918212891" />
                  <Point X="-20.795865234375" Y="-2.802138183594" />
                  <Point X="-20.94289453125" Y="-3.011046386719" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.109236328125" Y="-2.915840576172" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312988281" />
                  <Point X="-22.445677734375" Y="-2.220260009766" />
                  <Point X="-22.478748046875" Y="-2.214287353516" />
                  <Point X="-22.510921875" Y="-2.219245361328" />
                  <Point X="-22.662966796875" Y="-2.299264892578" />
                  <Point X="-22.690439453125" Y="-2.313724365234" />
                  <Point X="-22.7113984375" Y="-2.334681884766" />
                  <Point X="-22.79141796875" Y="-2.486725830078" />
                  <Point X="-22.80587890625" Y="-2.514199951172" />
                  <Point X="-22.8108359375" Y="-2.546374511719" />
                  <Point X="-22.77778125" Y="-2.729393798828" />
                  <Point X="-22.77180859375" Y="-2.76246484375" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.647255859375" Y="-2.984087646484" />
                  <Point X="-22.01332421875" Y="-4.082088378906" />
                  <Point X="-22.13666796875" Y="-4.170189941406" />
                  <Point X="-22.164705078125" Y="-4.190215332031" />
                  <Point X="-22.32022265625" Y="-4.290879882812" />
                  <Point X="-22.448951171875" Y="-4.123117675781" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.50995703125" Y="-2.864418212891" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.771609375" Y="-2.853883300781" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509277344" />
                  <Point X="-23.98710546875" Y="-2.995256835938" />
                  <Point X="-24.014650390625" Y="-3.018159667969" />
                  <Point X="-24.03154296875" Y="-3.045985839844" />
                  <Point X="-24.07712109375" Y="-3.255681884766" />
                  <Point X="-24.085357421875" Y="-3.293573730469" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-24.0524453125" Y="-3.566125" />
                  <Point X="-23.872357421875" Y="-4.934028320312" />
                  <Point X="-23.979138671875" Y="-4.957434570312" />
                  <Point X="-24.00566015625" Y="-4.963248535156" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#202" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.163669777233" Y="4.965948429041" Z="2.15" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.15" />
                  <Point X="-0.314258882436" Y="5.062160073588" Z="2.15" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.15" />
                  <Point X="-1.101280782759" Y="4.950889373162" Z="2.15" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.15" />
                  <Point X="-1.714207942376" Y="4.493024324154" Z="2.15" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.15" />
                  <Point X="-1.712586016317" Y="4.427512530203" Z="2.15" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.15" />
                  <Point X="-1.755103047005" Y="4.334517154784" Z="2.15" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.15" />
                  <Point X="-1.853671243615" Y="4.30731043692" Z="2.15" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.15" />
                  <Point X="-2.103684979599" Y="4.570018414861" Z="2.15" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.15" />
                  <Point X="-2.234110888084" Y="4.554444888864" Z="2.15" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.15" />
                  <Point X="-2.877153766216" Y="4.178052865458" Z="2.15" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.15" />
                  <Point X="-3.059244135045" Y="3.240285921222" Z="2.15" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.15" />
                  <Point X="-3.000379207578" Y="3.127220155028" Z="2.15" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.15" />
                  <Point X="-3.003333343452" Y="3.045470224434" Z="2.15" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.15" />
                  <Point X="-3.067856310579" Y="2.995185491595" Z="2.15" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.15" />
                  <Point X="-3.693573427075" Y="3.320949813701" Z="2.15" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.15" />
                  <Point X="-3.856926107548" Y="3.297203623759" Z="2.15" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.15" />
                  <Point X="-4.259706916488" Y="2.757222384293" Z="2.15" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.15" />
                  <Point X="-3.826816192525" Y="1.710781445037" Z="2.15" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.15" />
                  <Point X="-3.692010804545" Y="1.602090797978" Z="2.15" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.15" />
                  <Point X="-3.670594564135" Y="1.544597651394" Z="2.15" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.15" />
                  <Point X="-3.700870770064" Y="1.491236032855" Z="2.15" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.15" />
                  <Point X="-4.653719074286" Y="1.59342816896" Z="2.15" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.15" />
                  <Point X="-4.84042189999" Y="1.526563860792" Z="2.15" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.15" />
                  <Point X="-4.986221491439" Y="0.947441340002" Z="2.15" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.15" />
                  <Point X="-3.803641220994" Y="0.10991506566" Z="2.15" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.15" />
                  <Point X="-3.572313344088" Y="0.046121099738" Z="2.15" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.15" />
                  <Point X="-3.54739203789" Y="0.025245197075" Z="2.15" />
                  <Point X="-3.539556741714" Y="0" Z="2.15" />
                  <Point X="-3.540972563245" Y="-0.004561753986" Z="2.15" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.15" />
                  <Point X="-3.553055251035" Y="-0.032754883395" Z="2.15" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.15" />
                  <Point X="-4.833246567779" Y="-0.385796991096" Z="2.15" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.15" />
                  <Point X="-5.048440895264" Y="-0.529749786357" Z="2.15" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.15" />
                  <Point X="-4.961884811321" Y="-1.071011651928" Z="2.15" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.15" />
                  <Point X="-3.468274258788" Y="-1.339659962431" Z="2.15" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.15" />
                  <Point X="-3.215105906256" Y="-1.309248716557" Z="2.15" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.15" />
                  <Point X="-3.193855427279" Y="-1.327002438382" Z="2.15" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.15" />
                  <Point X="-4.303558679517" Y="-2.198695292367" Z="2.15" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.15" />
                  <Point X="-4.457975395942" Y="-2.426988431973" Z="2.15" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.15" />
                  <Point X="-4.156231102297" Y="-2.913680794241" Z="2.15" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.15" />
                  <Point X="-2.770173428233" Y="-2.66942176647" Z="2.15" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.15" />
                  <Point X="-2.570184422656" Y="-2.558146046475" Z="2.15" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.15" />
                  <Point X="-3.185995246699" Y="-3.664904954259" Z="2.15" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.15" />
                  <Point X="-3.237262384408" Y="-3.910487813612" Z="2.15" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.15" />
                  <Point X="-2.823234752924" Y="-4.219135963338" Z="2.15" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.15" />
                  <Point X="-2.260641021423" Y="-4.201307544716" Z="2.15" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.15" />
                  <Point X="-2.186742267362" Y="-4.130072441285" Z="2.15" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.15" />
                  <Point X="-1.920875453658" Y="-3.987189855991" Z="2.15" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.15" />
                  <Point X="-1.622967837146" Y="-4.035683170517" Z="2.15" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.15" />
                  <Point X="-1.416142659744" Y="-4.255510314051" Z="2.15" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.15" />
                  <Point X="-1.405719224011" Y="-4.823447926743" Z="2.15" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.15" />
                  <Point X="-1.367844575027" Y="-4.891146833279" Z="2.15" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.15" />
                  <Point X="-1.071529596547" Y="-4.964487536139" Z="2.15" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.15" />
                  <Point X="-0.478393061928" Y="-3.747571253982" Z="2.15" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.15" />
                  <Point X="-0.392029353374" Y="-3.482670104242" Z="2.15" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.15" />
                  <Point X="-0.214584171196" Y="-3.270838376283" Z="2.15" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.15" />
                  <Point X="0.038774908164" Y="-3.216273669005" Z="2.15" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.15" />
                  <Point X="0.278416506058" Y="-3.318975651526" Z="2.15" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.15" />
                  <Point X="0.756361798474" Y="-4.784964775834" Z="2.15" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.15" />
                  <Point X="0.845268226093" Y="-5.008749011715" Z="2.15" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.15" />
                  <Point X="1.025411460958" Y="-4.974995027378" Z="2.15" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.15" />
                  <Point X="0.990970496017" Y="-3.528318710698" Z="2.15" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.15" />
                  <Point X="0.965581691113" Y="-3.235022060063" Z="2.15" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.15" />
                  <Point X="1.038705319811" Y="-3.002422996756" Z="2.15" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.15" />
                  <Point X="1.226816108384" Y="-2.872392837073" Z="2.15" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.15" />
                  <Point X="1.456847569832" Y="-2.875196379468" Z="2.15" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.15" />
                  <Point X="2.505224313087" Y="-4.122275785459" Z="2.15" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.15" />
                  <Point X="2.691924840023" Y="-4.307311056403" Z="2.15" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.15" />
                  <Point X="2.886235342472" Y="-4.17959729068" Z="2.15" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.15" />
                  <Point X="2.3898876448" Y="-2.92780766051" Z="2.15" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.15" />
                  <Point X="2.265264262037" Y="-2.689227429196" Z="2.15" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.15" />
                  <Point X="2.246670503243" Y="-2.478734157651" Z="2.15" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.15" />
                  <Point X="2.354164220721" Y="-2.312230806724" Z="2.15" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.15" />
                  <Point X="2.539279374316" Y="-2.238183747454" Z="2.15" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.15" />
                  <Point X="3.859605936597" Y="-2.927861855475" Z="2.15" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.15" />
                  <Point X="4.091837447857" Y="-3.008543650596" Z="2.15" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.15" />
                  <Point X="4.264130559702" Y="-2.758923394349" Z="2.15" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.15" />
                  <Point X="3.377384313066" Y="-1.75627344523" Z="2.15" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.15" />
                  <Point X="3.177364985657" Y="-1.590673811254" Z="2.15" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.15" />
                  <Point X="3.094669716887" Y="-1.432142766789" Z="2.15" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.15" />
                  <Point X="3.124787051296" Y="-1.267172345715" Z="2.15" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.15" />
                  <Point X="3.245522732311" Y="-1.149344519326" Z="2.15" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.15" />
                  <Point X="4.676262312617" Y="-1.284035707983" Z="2.15" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.15" />
                  <Point X="4.919928438168" Y="-1.257789143722" Z="2.15" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.15" />
                  <Point X="5.000097077788" Y="-0.886991527463" Z="2.15" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.15" />
                  <Point X="3.946918535001" Y="-0.274123369331" Z="2.15" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.15" />
                  <Point X="3.733794666799" Y="-0.212627040107" Z="2.15" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.15" />
                  <Point X="3.646947894974" Y="-0.156513810802" Z="2.15" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.15" />
                  <Point X="3.597105117137" Y="-0.081824937249" Z="2.15" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.15" />
                  <Point X="3.584266333288" Y="0.014785593935" Z="2.15" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.15" />
                  <Point X="3.608431543426" Y="0.107434927773" Z="2.15" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.15" />
                  <Point X="3.669600747553" Y="0.175521804135" Z="2.15" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.15" />
                  <Point X="4.849049282305" Y="0.515848563438" Z="2.15" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.15" />
                  <Point X="5.037929222781" Y="0.633941352173" Z="2.15" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.15" />
                  <Point X="4.966605402464" Y="1.056140654983" Z="2.15" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.15" />
                  <Point X="3.680085390874" Y="1.250587941347" Z="2.15" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.15" />
                  <Point X="3.448710735884" Y="1.223928629826" Z="2.15" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.15" />
                  <Point X="3.358302367619" Y="1.240468251649" Z="2.15" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.15" />
                  <Point X="3.291963665304" Y="1.284850135515" Z="2.15" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.15" />
                  <Point X="3.248556879639" Y="1.359821929941" Z="2.15" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.15" />
                  <Point X="3.236886129982" Y="1.444128105274" Z="2.15" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.15" />
                  <Point X="3.263959160006" Y="1.520850320099" Z="2.15" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.15" />
                  <Point X="4.273697465438" Y="2.321942313252" Z="2.15" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.15" />
                  <Point X="4.415306254986" Y="2.508050962422" Z="2.15" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.15" />
                  <Point X="4.202077570637" Y="2.850927106024" Z="2.15" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.15" />
                  <Point X="2.738277167922" Y="2.398865127514" Z="2.15" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.15" />
                  <Point X="2.497590645751" Y="2.263712990604" Z="2.15" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.15" />
                  <Point X="2.418966645278" Y="2.246810354269" Z="2.15" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.15" />
                  <Point X="2.350477813669" Y="2.260474892056" Z="2.15" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.15" />
                  <Point X="2.290283708179" Y="2.306547046713" Z="2.15" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.15" />
                  <Point X="2.25261934849" Y="2.370791798555" Z="2.15" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.15" />
                  <Point X="2.248814952054" Y="2.441878930131" Z="2.15" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.15" />
                  <Point X="2.996760225345" Y="3.773861914033" Z="2.15" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.15" />
                  <Point X="3.071215645899" Y="4.043088649953" Z="2.15" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.15" />
                  <Point X="2.692627146094" Y="4.304495408114" Z="2.15" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.15" />
                  <Point X="2.292749977774" Y="4.530222354808" Z="2.15" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.15" />
                  <Point X="1.878636809431" Y="4.717025488674" Z="2.15" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.15" />
                  <Point X="1.416619883669" Y="4.872460511231" Z="2.15" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.15" />
                  <Point X="0.760124627852" Y="5.01695554937" Z="2.15" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.15" />
                  <Point X="0.02957478432" Y="4.46549907691" Z="2.15" />
                  <Point X="0" Y="4.355124473572" Z="2.15" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>