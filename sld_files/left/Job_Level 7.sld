<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#129" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="672" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000473632812" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.38918359375" Y="-3.689845703125" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140869141" />
                  <Point X="-24.457634765625" Y="-3.467376953125" />
                  <Point X="-24.4770546875" Y="-3.439396728516" />
                  <Point X="-24.62136328125" Y="-3.2314765625" />
                  <Point X="-24.643248046875" Y="-3.209020751953" />
                  <Point X="-24.66950390625" Y="-3.189776855469" />
                  <Point X="-24.69750390625" Y="-3.175669433594" />
                  <Point X="-24.7275546875" Y="-3.166342529297" />
                  <Point X="-24.95086328125" Y="-3.097036132812" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.066875" Y="-3.106362548828" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.189776855469" />
                  <Point X="-25.344439453125" Y="-3.209020751953" />
                  <Point X="-25.366326171875" Y="-3.231479980469" />
                  <Point X="-25.385744140625" Y="-3.259459960938" />
                  <Point X="-25.530052734375" Y="-3.467380371094" />
                  <Point X="-25.5381875" Y="-3.481571533203" />
                  <Point X="-25.550990234375" Y="-3.512524169922" />
                  <Point X="-25.90405859375" Y="-4.830192871094" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.0793359375" Y="-4.845351074219" />
                  <Point X="-26.110189453125" Y="-4.837413085938" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.220681640625" Y="-4.606883300781" />
                  <Point X="-26.2149609375" Y="-4.563439941406" />
                  <Point X="-26.21419921875" Y="-4.547930664062" />
                  <Point X="-26.2165078125" Y="-4.51622265625" />
                  <Point X="-26.2236875" Y="-4.480130371094" />
                  <Point X="-26.277037109375" Y="-4.211931152344" />
                  <Point X="-26.287939453125" Y="-4.182964355469" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131205566406" />
                  <Point X="-26.351310546875" Y="-4.106941894531" />
                  <Point X="-26.556904296875" Y="-3.926640869141" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.679748046875" Y="-3.888559570312" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708740234" />
                  <Point X="-27.014466796875" Y="-3.882028564453" />
                  <Point X="-27.042658203125" Y="-3.894801513672" />
                  <Point X="-27.073255859375" Y="-3.91524609375" />
                  <Point X="-27.300623046875" Y="-4.067169433594" />
                  <Point X="-27.31278515625" Y="-4.076821533203" />
                  <Point X="-27.3351015625" Y="-4.099461425781" />
                  <Point X="-27.4801484375" Y="-4.288489257813" />
                  <Point X="-27.80170703125" Y="-4.089387207031" />
                  <Point X="-27.844419921875" Y="-4.056499511719" />
                  <Point X="-28.104720703125" Y="-3.856076660156" />
                  <Point X="-27.51205859375" Y="-2.829555175781" />
                  <Point X="-27.42376171875" Y="-2.676619384766" />
                  <Point X="-27.412857421875" Y="-2.647646484375" />
                  <Point X="-27.406587890625" Y="-2.616115966797" />
                  <Point X="-27.405751953125" Y="-2.583920410156" />
                  <Point X="-27.415720703125" Y="-2.553295654297" />
                  <Point X="-27.432" Y="-2.522407226562" />
                  <Point X="-27.448865234375" Y="-2.499528564453" />
                  <Point X="-27.464146484375" Y="-2.484245849609" />
                  <Point X="-27.489302734375" Y="-2.466216308594" />
                  <Point X="-27.5181328125" Y="-2.451997558594" />
                  <Point X="-27.547751953125" Y="-2.443011962891" />
                  <Point X="-27.5786875" Y="-2.444024169922" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.775646484375" Y="-3.117334716797" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-29.082853515625" Y="-2.793868652344" />
                  <Point X="-29.113478515625" Y="-2.742515625" />
                  <Point X="-29.306142578125" Y="-2.419449462891" />
                  <Point X="-28.260447265625" Y="-1.617059448242" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.083064453125" Y="-1.475880615234" />
                  <Point X="-28.064640625" Y="-1.447130737305" />
                  <Point X="-28.057013671875" Y="-1.428534667969" />
                  <Point X="-28.052943359375" Y="-1.416305786133" />
                  <Point X="-28.04615234375" Y="-1.390086547852" />
                  <Point X="-28.04334765625" Y="-1.359662475586" />
                  <Point X="-28.0455546875" Y="-1.327991455078" />
                  <Point X="-28.0531640625" Y="-1.296805786133" />
                  <Point X="-28.070724609375" Y="-1.269934814453" />
                  <Point X="-28.09469921875" Y="-1.244129394531" />
                  <Point X="-28.11611328125" Y="-1.226917724609" />
                  <Point X="-28.139455078125" Y="-1.213179931641" />
                  <Point X="-28.168716796875" Y="-1.201956176758" />
                  <Point X="-28.200603515625" Y="-1.195474731445" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-29.66660546875" Y="-1.383262695312" />
                  <Point X="-29.7321015625" Y="-1.391885375977" />
                  <Point X="-29.834076171875" Y="-0.992655029297" />
                  <Point X="-29.842177734375" Y="-0.936012573242" />
                  <Point X="-29.892421875" Y="-0.584698425293" />
                  <Point X="-28.708865234375" Y="-0.267564727783" />
                  <Point X="-28.532875" Y="-0.220408309937" />
                  <Point X="-28.514708984375" Y="-0.213481506348" />
                  <Point X="-28.49584765625" Y="-0.203976394653" />
                  <Point X="-28.48443359375" Y="-0.197184387207" />
                  <Point X="-28.45997265625" Y="-0.180207092285" />
                  <Point X="-28.436021484375" Y="-0.158682907104" />
                  <Point X="-28.41623046875" Y="-0.130805847168" />
                  <Point X="-28.407705078125" Y="-0.112528610229" />
                  <Point X="-28.40306640625" Y="-0.100525177002" />
                  <Point X="-28.3949140625" Y="-0.07425390625" />
                  <Point X="-28.39069921875" Y="-0.042951843262" />
                  <Point X="-28.391798828125" Y="-0.009772438049" />
                  <Point X="-28.396013671875" Y="0.015236849785" />
                  <Point X="-28.4041640625" Y="0.041501293182" />
                  <Point X="-28.417484375" Y="0.0708334198" />
                  <Point X="-28.4382578125" Y="0.098234008789" />
                  <Point X="-28.4533671875" Y="0.112033729553" />
                  <Point X="-28.463265625" Y="0.119931533813" />
                  <Point X="-28.4877265625" Y="0.136908981323" />
                  <Point X="-28.501923828125" Y="0.145046920776" />
                  <Point X="-28.532875" Y="0.157848327637" />
                  <Point X="-29.840650390625" Y="0.508265838623" />
                  <Point X="-29.89181640625" Y="0.521975585938" />
                  <Point X="-29.824486328125" Y="0.976972167969" />
                  <Point X="-29.808177734375" Y="1.037159667969" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-28.891818359375" Y="1.316401489258" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228027344" />
                  <Point X="-28.7031328125" Y="1.305264282227" />
                  <Point X="-28.69584765625" Y="1.307561523438" />
                  <Point X="-28.64170703125" Y="1.324631835938" />
                  <Point X="-28.622775390625" Y="1.332962646484" />
                  <Point X="-28.60403125" Y="1.343784912109" />
                  <Point X="-28.587353515625" Y="1.356014282227" />
                  <Point X="-28.573716796875" Y="1.371563232422" />
                  <Point X="-28.56130078125" Y="1.389293579102" />
                  <Point X="-28.551349609375" Y="1.407428588867" />
                  <Point X="-28.54842578125" Y="1.414486450195" />
                  <Point X="-28.526703125" Y="1.466932739258" />
                  <Point X="-28.5209140625" Y="1.486797119141" />
                  <Point X="-28.51715625" Y="1.508112060547" />
                  <Point X="-28.515802734375" Y="1.528754882812" />
                  <Point X="-28.518951171875" Y="1.549201171875" />
                  <Point X="-28.5245546875" Y="1.570107177734" />
                  <Point X="-28.532048828125" Y="1.589376831055" />
                  <Point X="-28.535576171875" Y="1.596152954102" />
                  <Point X="-28.5617890625" Y="1.646506591797" />
                  <Point X="-28.573283203125" Y="1.663706665039" />
                  <Point X="-28.5871953125" Y="1.680286499023" />
                  <Point X="-28.60213671875" Y="1.694590087891" />
                  <Point X="-29.351859375" Y="2.269874023438" />
                  <Point X="-29.081150390625" Y="2.733661621094" />
                  <Point X="-29.037955078125" Y="2.789185546875" />
                  <Point X="-28.75050390625" Y="3.158662353516" />
                  <Point X="-28.28413671875" Y="2.889404052734" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.136646484375" Y="2.824908691406" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.999013671875" Y="2.826504638672" />
                  <Point X="-27.980462890625" Y="2.835653564453" />
                  <Point X="-27.962208984375" Y="2.847282714844" />
                  <Point X="-27.946080078125" Y="2.860226074219" />
                  <Point X="-27.938876953125" Y="2.867428466797" />
                  <Point X="-27.88535546875" Y="2.920949462891" />
                  <Point X="-27.87240234375" Y="2.937089599609" />
                  <Point X="-27.8607734375" Y="2.955345947266" />
                  <Point X="-27.851625" Y="2.973897705078" />
                  <Point X="-27.8467109375" Y="2.993989501953" />
                  <Point X="-27.84388671875" Y="3.015450927734" />
                  <Point X="-27.843435546875" Y="3.036120117188" />
                  <Point X="-27.844322265625" Y="3.046261962891" />
                  <Point X="-27.85091796875" Y="3.121664306641" />
                  <Point X="-27.854955078125" Y="3.141961425781" />
                  <Point X="-27.86146484375" Y="3.162604248047" />
                  <Point X="-27.869794921875" Y="3.181532714844" />
                  <Point X="-28.183333984375" Y="3.724596679688" />
                  <Point X="-27.700626953125" Y="4.094683349609" />
                  <Point X="-27.632591796875" Y="4.132481933594" />
                  <Point X="-27.16703515625" Y="4.391134277344" />
                  <Point X="-27.0669140625" Y="4.26065234375" />
                  <Point X="-27.0431953125" Y="4.229741210938" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.99511328125" Y="4.189395019531" />
                  <Point X="-26.9838203125" Y="4.183516113281" />
                  <Point X="-26.899896484375" Y="4.139828125" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.777447265625" Y="4.134483886719" />
                  <Point X="-26.765685546875" Y="4.139356445312" />
                  <Point X="-26.6782734375" Y="4.175563476562" />
                  <Point X="-26.660146484375" Y="4.185508789062" />
                  <Point X="-26.642416015625" Y="4.197922851562" />
                  <Point X="-26.626859375" Y="4.211563964844" />
                  <Point X="-26.614626953125" Y="4.228249511719" />
                  <Point X="-26.603806640625" Y="4.246993652344" />
                  <Point X="-26.595478515625" Y="4.265925292969" />
                  <Point X="-26.591650390625" Y="4.278068359375" />
                  <Point X="-26.56319921875" Y="4.368301757813" />
                  <Point X="-26.5591640625" Y="4.388583007812" />
                  <Point X="-26.55727734375" Y="4.410143066406" />
                  <Point X="-26.557728515625" Y="4.430825683594" />
                  <Point X="-26.584201171875" Y="4.631896972656" />
                  <Point X="-25.949630859375" Y="4.80980859375" />
                  <Point X="-25.8671640625" Y="4.819459960938" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.15682421875" Y="4.371856933594" />
                  <Point X="-25.133904296875" Y="4.286315917969" />
                  <Point X="-25.12112890625" Y="4.258122558594" />
                  <Point X="-25.10326953125" Y="4.231395507813" />
                  <Point X="-25.08211328125" Y="4.208808105469" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183885742188" />
                  <Point X="-24.932869140625" Y="4.194217773438" />
                  <Point X="-24.905576171875" Y="4.208806640625" />
                  <Point X="-24.88441796875" Y="4.23139453125" />
                  <Point X="-24.86655859375" Y="4.25812109375" />
                  <Point X="-24.853783203125" Y="4.286314941406" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-24.087720703125" Y="4.815264648438" />
                  <Point X="-23.5189765625" Y="4.677951660156" />
                  <Point X="-23.475865234375" Y="4.662315429688" />
                  <Point X="-23.105359375" Y="4.5279296875" />
                  <Point X="-23.062404296875" Y="4.507841308594" />
                  <Point X="-22.705423828125" Y="4.340893066406" />
                  <Point X="-22.663896484375" Y="4.316700195313" />
                  <Point X="-22.31901953125" Y="4.1157734375" />
                  <Point X="-22.279884765625" Y="4.087943603516" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.75021875" Y="2.728109863281" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.859828125" Y="2.534932861328" />
                  <Point X="-22.867376953125" Y="2.513327392578" />
                  <Point X="-22.86946875" Y="2.50653515625" />
                  <Point X="-22.888392578125" Y="2.435771484375" />
                  <Point X="-22.891609375" Y="2.412432128906" />
                  <Point X="-22.891953125" Y="2.385295898438" />
                  <Point X="-22.89127734375" Y="2.372721923828" />
                  <Point X="-22.883900390625" Y="2.311531494141" />
                  <Point X="-22.87855859375" Y="2.289609619141" />
                  <Point X="-22.87029296875" Y="2.26751953125" />
                  <Point X="-22.859927734375" Y="2.247467285156" />
                  <Point X="-22.85483203125" Y="2.239958251953" />
                  <Point X="-22.81696875" Y="2.184158691406" />
                  <Point X="-22.801279296875" Y="2.166327880859" />
                  <Point X="-22.780470703125" Y="2.147932617188" />
                  <Point X="-22.770890625" Y="2.140496337891" />
                  <Point X="-22.71508984375" Y="2.102634033203" />
                  <Point X="-22.695044921875" Y="2.092271728516" />
                  <Point X="-22.67295703125" Y="2.084006103516" />
                  <Point X="-22.651037109375" Y="2.078663818359" />
                  <Point X="-22.642802734375" Y="2.077670654297" />
                  <Point X="-22.58161328125" Y="2.070292236328" />
                  <Point X="-22.557376953125" Y="2.070483398438" />
                  <Point X="-22.528951171875" Y="2.074367675781" />
                  <Point X="-22.517271484375" Y="2.076717773438" />
                  <Point X="-22.4465078125" Y="2.095640869141" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.09609375" Y="2.869574951172" />
                  <Point X="-21.032671875" Y="2.906191162109" />
                  <Point X="-20.8767265625" Y="2.689463867188" />
                  <Point X="-20.854908203125" Y="2.653407714844" />
                  <Point X="-20.73780078125" Y="2.459884033203" />
                  <Point X="-21.634662109375" Y="1.771697387695" />
                  <Point X="-21.76921484375" Y="1.668451171875" />
                  <Point X="-21.782521484375" Y="1.656044189453" />
                  <Point X="-21.79862109375" Y="1.637853271484" />
                  <Point X="-21.802880859375" Y="1.632685791016" />
                  <Point X="-21.85380859375" Y="1.566245483398" />
                  <Point X="-21.865916015625" Y="1.545433105469" />
                  <Point X="-21.8769375" Y="1.519353881836" />
                  <Point X="-21.880919921875" Y="1.507960571289" />
                  <Point X="-21.899892578125" Y="1.440124755859" />
                  <Point X="-21.90334765625" Y="1.417824951172" />
                  <Point X="-21.9041640625" Y="1.394253173828" />
                  <Point X="-21.90226171875" Y="1.371767456055" />
                  <Point X="-21.900166015625" Y="1.361610595703" />
                  <Point X="-21.884591796875" Y="1.286134765625" />
                  <Point X="-21.8766796875" Y="1.263165893555" />
                  <Point X="-21.86378125" Y="1.23712512207" />
                  <Point X="-21.858015625" Y="1.227076538086" />
                  <Point X="-21.815658203125" Y="1.162695068359" />
                  <Point X="-21.801107421875" Y="1.145452880859" />
                  <Point X="-21.783865234375" Y="1.12936340332" />
                  <Point X="-21.7656484375" Y="1.116031982422" />
                  <Point X="-21.75738671875" Y="1.111382080078" />
                  <Point X="-21.696005859375" Y="1.076829467773" />
                  <Point X="-21.673009765625" Y="1.067595458984" />
                  <Point X="-21.643865234375" Y="1.060121337891" />
                  <Point X="-21.632712890625" Y="1.057962646484" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.26228125" Y="1.211486328125" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.154060546875" Y="0.9327890625" />
                  <Point X="-20.1471875" Y="0.888646911621" />
                  <Point X="-20.109134765625" Y="0.644238769531" />
                  <Point X="-21.12921875" Y="0.370907836914" />
                  <Point X="-21.283419921875" Y="0.329589874268" />
                  <Point X="-21.300794921875" Y="0.323056671143" />
                  <Point X="-21.32384765625" Y="0.311706695557" />
                  <Point X="-21.32942578125" Y="0.308725372314" />
                  <Point X="-21.410962890625" Y="0.261595367432" />
                  <Point X="-21.43045703125" Y="0.246661941528" />
                  <Point X="-21.451353515625" Y="0.225852386475" />
                  <Point X="-21.459052734375" Y="0.217186981201" />
                  <Point X="-21.507974609375" Y="0.154848480225" />
                  <Point X="-21.51969921875" Y="0.135566864014" />
                  <Point X="-21.52947265625" Y="0.114102783203" />
                  <Point X="-21.53631640625" Y="0.092609725952" />
                  <Point X="-21.53851171875" Y="0.081150917053" />
                  <Point X="-21.554818359375" Y="-0.004000760555" />
                  <Point X="-21.556267578125" Y="-0.028748209" />
                  <Point X="-21.554072265625" Y="-0.059015666962" />
                  <Point X="-21.552625" Y="-0.07001121521" />
                  <Point X="-21.536318359375" Y="-0.155162734985" />
                  <Point X="-21.52947265625" Y="-0.17666293335" />
                  <Point X="-21.51969921875" Y="-0.198126998901" />
                  <Point X="-21.50797265625" Y="-0.217410903931" />
                  <Point X="-21.501388671875" Y="-0.225800064087" />
                  <Point X="-21.452466796875" Y="-0.288138549805" />
                  <Point X="-21.434345703125" Y="-0.30577645874" />
                  <Point X="-21.409060546875" Y="-0.324539398193" />
                  <Point X="-21.399990234375" Y="-0.330497924805" />
                  <Point X="-21.318453125" Y="-0.377627929688" />
                  <Point X="-21.307291015625" Y="-0.383138244629" />
                  <Point X="-21.283419921875" Y="-0.392149871826" />
                  <Point X="-20.13755859375" Y="-0.699182495117" />
                  <Point X="-20.108525390625" Y="-0.706961853027" />
                  <Point X="-20.14497265625" Y="-0.948713195801" />
                  <Point X="-20.15378515625" Y="-0.987324523926" />
                  <Point X="-20.19882421875" Y="-1.18469909668" />
                  <Point X="-21.396787109375" Y="-1.026984130859" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710144043" />
                  <Point X="-21.62533984375" Y="-1.005508728027" />
                  <Point X="-21.646875" Y="-1.01018951416" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.8360234375" Y="-1.056595947266" />
                  <Point X="-21.8638515625" Y="-1.07348840332" />
                  <Point X="-21.887599609375" Y="-1.093957763672" />
                  <Point X="-21.9006171875" Y="-1.109612792969" />
                  <Point X="-21.99734375" Y="-1.225945922852" />
                  <Point X="-22.012068359375" Y="-1.250334594727" />
                  <Point X="-22.02341015625" Y="-1.277719726562" />
                  <Point X="-22.030240234375" Y="-1.30536730957" />
                  <Point X="-22.03210546875" Y="-1.325641479492" />
                  <Point X="-22.04596875" Y="-1.476297485352" />
                  <Point X="-22.04365234375" Y="-1.507561401367" />
                  <Point X="-22.035921875" Y="-1.539182373047" />
                  <Point X="-22.023548828125" Y="-1.56799621582" />
                  <Point X="-22.011630859375" Y="-1.586534057617" />
                  <Point X="-21.923068359375" Y="-1.724286865234" />
                  <Point X="-21.9130625" Y="-1.73724206543" />
                  <Point X="-21.889369140625" Y="-1.760909423828" />
                  <Point X="-20.82600390625" Y="-2.576859130859" />
                  <Point X="-20.786875" Y="-2.606883300781" />
                  <Point X="-20.875185546875" Y="-2.749780761719" />
                  <Point X="-20.893408203125" Y="-2.775674316406" />
                  <Point X="-20.971017578125" Y="-2.885945556641" />
                  <Point X="-22.0397734375" Y="-2.268898925781" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.245775390625" Y="-2.159824951172" />
                  <Point X="-22.27140625" Y="-2.155196289062" />
                  <Point X="-22.461865234375" Y="-2.120799316406" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135176269531" />
                  <Point X="-22.576458984375" Y="-2.146382324219" />
                  <Point X="-22.73468359375" Y="-2.229655273438" />
                  <Point X="-22.757615234375" Y="-2.246549804688" />
                  <Point X="-22.77857421875" Y="-2.267509521484" />
                  <Point X="-22.79546484375" Y="-2.290436767578" />
                  <Point X="-22.806671875" Y="-2.311729492188" />
                  <Point X="-22.8899453125" Y="-2.469954833984" />
                  <Point X="-22.899771484375" Y="-2.499735351563" />
                  <Point X="-22.904728515625" Y="-2.531908447266" />
                  <Point X="-22.90432421875" Y="-2.563258789062" />
                  <Point X="-22.8996953125" Y="-2.588889160156" />
                  <Point X="-22.865296875" Y="-2.779349121094" />
                  <Point X="-22.86101171875" Y="-2.795140136719" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.164857421875" Y="-4.009623291016" />
                  <Point X="-22.138712890625" Y="-4.054905517578" />
                  <Point X="-22.218130859375" Y="-4.111630859375" />
                  <Point X="-22.23851953125" Y="-4.124829101562" />
                  <Point X="-22.298232421875" Y="-4.16348046875" />
                  <Point X="-23.119912109375" Y="-3.092647705078" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900556640625" />
                  <Point X="-23.303353515625" Y="-2.8843046875" />
                  <Point X="-23.49119921875" Y="-2.763537841797" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.610546875" Y="-2.743660888672" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-23.91675" Y="-2.813211181641" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968861083984" />
                  <Point X="-24.11275" Y="-2.996687744141" />
                  <Point X="-24.124375" Y="-3.025808837891" />
                  <Point X="-24.1307578125" Y="-3.055175292969" />
                  <Point X="-24.178189453125" Y="-3.273396728516" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-23.986609375" Y="-4.79401953125" />
                  <Point X="-23.97793359375" Y="-4.859915527344" />
                  <Point X="-24.024322265625" Y="-4.870083496094" />
                  <Point X="-24.043140625" Y="-4.873502441406" />
                  <Point X="-24.07068359375" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.0584296875" Y="-4.752636230469" />
                  <Point X="-26.08651953125" Y="-4.745409179688" />
                  <Point X="-26.141244140625" Y="-4.731328613281" />
                  <Point X="-26.126494140625" Y="-4.619282714844" />
                  <Point X="-26.1207734375" Y="-4.575839355469" />
                  <Point X="-26.120076171875" Y="-4.568100097656" />
                  <Point X="-26.11944921875" Y="-4.541032226562" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497687988281" />
                  <Point X="-26.130513671875" Y="-4.461595703125" />
                  <Point X="-26.18386328125" Y="-4.193396484375" />
                  <Point X="-26.188126953125" Y="-4.178467285156" />
                  <Point X="-26.199029296875" Y="-4.149500488281" />
                  <Point X="-26.20566796875" Y="-4.135462890625" />
                  <Point X="-26.221740234375" Y="-4.107625976562" />
                  <Point X="-26.230576171875" Y="-4.094858886719" />
                  <Point X="-26.250208984375" Y="-4.070937011719" />
                  <Point X="-26.261005859375" Y="-4.059782470703" />
                  <Point X="-26.288671875" Y="-4.035518798828" />
                  <Point X="-26.494265625" Y="-3.855217773438" />
                  <Point X="-26.506734375" Y="-3.84596875" />
                  <Point X="-26.533017578125" Y="-3.829623291016" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.67353515625" Y="-3.793762939453" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166259766" />
                  <Point X="-27.008005859375" Y="-3.781946044922" />
                  <Point X="-27.0390546875" Y="-3.790265869141" />
                  <Point X="-27.053673828125" Y="-3.79549609375" />
                  <Point X="-27.081865234375" Y="-3.808269042969" />
                  <Point X="-27.0954375" Y="-3.815811767578" />
                  <Point X="-27.12603515625" Y="-3.836256347656" />
                  <Point X="-27.35340234375" Y="-3.9881796875" />
                  <Point X="-27.3596796875" Y="-3.992755859375" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032771484375" />
                  <Point X="-27.410470703125" Y="-4.04162890625" />
                  <Point X="-27.503203125" Y="-4.162478515625" />
                  <Point X="-27.747587890625" Y="-4.01116015625" />
                  <Point X="-27.786462890625" Y="-3.981227294922" />
                  <Point X="-27.98086328125" Y="-3.831546142578" />
                  <Point X="-27.429787109375" Y="-2.877055175781" />
                  <Point X="-27.341490234375" Y="-2.724119384766" />
                  <Point X="-27.334849609375" Y="-2.710082275391" />
                  <Point X="-27.3239453125" Y="-2.681109375" />
                  <Point X="-27.319681640625" Y="-2.666173583984" />
                  <Point X="-27.313412109375" Y="-2.634643066406" />
                  <Point X="-27.311619140625" Y="-2.618581787109" />
                  <Point X="-27.310783203125" Y="-2.586386230469" />
                  <Point X="-27.31541796875" Y="-2.554515380859" />
                  <Point X="-27.32538671875" Y="-2.523890625" />
                  <Point X="-27.331677734375" Y="-2.509002441406" />
                  <Point X="-27.34795703125" Y="-2.478114013672" />
                  <Point X="-27.35553125" Y="-2.466037597656" />
                  <Point X="-27.372396484375" Y="-2.443158935547" />
                  <Point X="-27.3816875" Y="-2.432356689453" />
                  <Point X="-27.39696875" Y="-2.417073974609" />
                  <Point X="-27.4088046875" Y="-2.407029541016" />
                  <Point X="-27.4339609375" Y="-2.389" />
                  <Point X="-27.44728125" Y="-2.381014892578" />
                  <Point X="-27.476111328125" Y="-2.366796142578" />
                  <Point X="-27.4905546875" Y="-2.361088867188" />
                  <Point X="-27.520173828125" Y="-2.352103271484" />
                  <Point X="-27.550859375" Y="-2.348062744141" />
                  <Point X="-27.581794921875" Y="-2.349074951172" />
                  <Point X="-27.597220703125" Y="-2.350849365234" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.793087890625" Y="-3.017708251953" />
                  <Point X="-29.004013671875" Y="-2.740593994141" />
                  <Point X="-29.03188671875" Y="-2.693856933594" />
                  <Point X="-29.181265625" Y="-2.443372802734" />
                  <Point X="-28.202615234375" Y="-1.692427978516" />
                  <Point X="-28.048123046875" Y="-1.573881958008" />
                  <Point X="-28.039162109375" Y="-1.566067871094" />
                  <Point X="-28.016271484375" Y="-1.543435058594" />
                  <Point X="-28.003078125" Y="-1.527137939453" />
                  <Point X="-27.984654296875" Y="-1.498388061523" />
                  <Point X="-27.97674609375" Y="-1.4831796875" />
                  <Point X="-27.969119140625" Y="-1.464583618164" />
                  <Point X="-27.960978515625" Y="-1.440125610352" />
                  <Point X="-27.9541875" Y="-1.41390637207" />
                  <Point X="-27.951552734375" Y="-1.398807250977" />
                  <Point X="-27.948748046875" Y="-1.368383178711" />
                  <Point X="-27.948578125" Y="-1.353058349609" />
                  <Point X="-27.95078515625" Y="-1.321387451172" />
                  <Point X="-27.95326171875" Y="-1.305471923828" />
                  <Point X="-27.96087109375" Y="-1.274286254883" />
                  <Point X="-27.973640625" Y="-1.244835571289" />
                  <Point X="-27.991201171875" Y="-1.217964599609" />
                  <Point X="-28.001125" Y="-1.205273803711" />
                  <Point X="-28.025099609375" Y="-1.179468505859" />
                  <Point X="-28.03518359375" Y="-1.170082641602" />
                  <Point X="-28.05659765625" Y="-1.15287097168" />
                  <Point X="-28.067927734375" Y="-1.145045166016" />
                  <Point X="-28.09126953125" Y="-1.131307373047" />
                  <Point X="-28.10543359375" Y="-1.124480957031" />
                  <Point X="-28.1346953125" Y="-1.113257324219" />
                  <Point X="-28.14979296875" Y="-1.108859863281" />
                  <Point X="-28.1816796875" Y="-1.102378417969" />
                  <Point X="-28.197296875" Y="-1.100532348633" />
                  <Point X="-28.22862109375" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-29.660919921875" Y="-1.286694458008" />
                  <Point X="-29.740759765625" Y="-0.974118286133" />
                  <Point X="-29.748134765625" Y="-0.922561645508" />
                  <Point X="-29.786451171875" Y="-0.654654541016" />
                  <Point X="-28.68427734375" Y="-0.359327636719" />
                  <Point X="-28.508287109375" Y="-0.312171234131" />
                  <Point X="-28.49902734375" Y="-0.309174255371" />
                  <Point X="-28.471955078125" Y="-0.298317779541" />
                  <Point X="-28.45309375" Y="-0.28881262207" />
                  <Point X="-28.430265625" Y="-0.275228637695" />
                  <Point X="-28.4058046875" Y="-0.258251434326" />
                  <Point X="-28.39647265625" Y="-0.25086680603" />
                  <Point X="-28.372521484375" Y="-0.229342498779" />
                  <Point X="-28.35855859375" Y="-0.213677352905" />
                  <Point X="-28.338767578125" Y="-0.185800308228" />
                  <Point X="-28.33013671875" Y="-0.170964477539" />
                  <Point X="-28.321611328125" Y="-0.152687286377" />
                  <Point X="-28.312333984375" Y="-0.128680526733" />
                  <Point X="-28.304181640625" Y="-0.102409210205" />
                  <Point X="-28.300763671875" Y="-0.08693132782" />
                  <Point X="-28.296548828125" Y="-0.055629268646" />
                  <Point X="-28.295751953125" Y="-0.039805095673" />
                  <Point X="-28.2968515625" Y="-0.006625781536" />
                  <Point X="-28.298119140625" Y="0.006015336514" />
                  <Point X="-28.302333984375" Y="0.031024702072" />
                  <Point X="-28.30528125" Y="0.043392799377" />
                  <Point X="-28.313431640625" Y="0.06965713501" />
                  <Point X="-28.317666015625" Y="0.080782150269" />
                  <Point X="-28.330986328125" Y="0.110114212036" />
                  <Point X="-28.34178125" Y="0.128227035522" />
                  <Point X="-28.3625546875" Y="0.155627593994" />
                  <Point X="-28.37419140625" Y="0.168380477905" />
                  <Point X="-28.38930078125" Y="0.182180114746" />
                  <Point X="-28.40909765625" Y="0.197975601196" />
                  <Point X="-28.43355859375" Y="0.214953079224" />
                  <Point X="-28.440482421875" Y="0.219328994751" />
                  <Point X="-28.465615234375" Y="0.232834503174" />
                  <Point X="-28.49656640625" Y="0.245635986328" />
                  <Point X="-28.508287109375" Y="0.249611343384" />
                  <Point X="-29.7854453125" Y="0.591825073242" />
                  <Point X="-29.731328125" Y="0.957528991699" />
                  <Point X="-29.716484375" Y="1.012314025879" />
                  <Point X="-29.6335859375" Y="1.318237060547" />
                  <Point X="-28.90421875" Y="1.222214233398" />
                  <Point X="-28.778064453125" Y="1.20560546875" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.20470324707" />
                  <Point X="-28.71514453125" Y="1.206589355469" />
                  <Point X="-28.704890625" Y="1.208053588867" />
                  <Point X="-28.684599609375" Y="1.21208984375" />
                  <Point X="-28.66727734375" Y="1.216959228516" />
                  <Point X="-28.61313671875" Y="1.234029541016" />
                  <Point X="-28.603443359375" Y="1.237678466797" />
                  <Point X="-28.58451171875" Y="1.246009277344" />
                  <Point X="-28.5752734375" Y="1.250690917969" />
                  <Point X="-28.556529296875" Y="1.261513183594" />
                  <Point X="-28.54785546875" Y="1.267174316406" />
                  <Point X="-28.531177734375" Y="1.279403564453" />
                  <Point X="-28.5159296875" Y="1.293374755859" />
                  <Point X="-28.50229296875" Y="1.308923706055" />
                  <Point X="-28.495900390625" Y="1.31707019043" />
                  <Point X="-28.483484375" Y="1.334800537109" />
                  <Point X="-28.478015625" Y="1.343592651367" />
                  <Point X="-28.468064453125" Y="1.361727661133" />
                  <Point X="-28.460658203125" Y="1.378127685547" />
                  <Point X="-28.438935546875" Y="1.43057409668" />
                  <Point X="-28.435498046875" Y="1.440352661133" />
                  <Point X="-28.429708984375" Y="1.460217041016" />
                  <Point X="-28.427357421875" Y="1.470303100586" />
                  <Point X="-28.423599609375" Y="1.491618041992" />
                  <Point X="-28.422359375" Y="1.501896362305" />
                  <Point X="-28.421005859375" Y="1.52253918457" />
                  <Point X="-28.42191015625" Y="1.543213134766" />
                  <Point X="-28.42505859375" Y="1.563659423828" />
                  <Point X="-28.427189453125" Y="1.573796264648" />
                  <Point X="-28.43279296875" Y="1.594702270508" />
                  <Point X="-28.436015625" Y="1.604541137695" />
                  <Point X="-28.443509765625" Y="1.623810791016" />
                  <Point X="-28.451310546875" Y="1.640018310547" />
                  <Point X="-28.4775234375" Y="1.690372070312" />
                  <Point X="-28.482802734375" Y="1.699290283203" />
                  <Point X="-28.494296875" Y="1.716490356445" />
                  <Point X="-28.500509765625" Y="1.724771484375" />
                  <Point X="-28.514421875" Y="1.741351318359" />
                  <Point X="-28.5215" Y="1.748910400391" />
                  <Point X="-28.53644140625" Y="1.763213989258" />
                  <Point X="-28.5443046875" Y="1.769958618164" />
                  <Point X="-29.22761328125" Y="2.29428125" />
                  <Point X="-29.002287109375" Y="2.68031640625" />
                  <Point X="-28.96297265625" Y="2.730852783203" />
                  <Point X="-28.726337890625" Y="3.035013183594" />
                  <Point X="-28.33163671875" Y="2.807131835938" />
                  <Point X="-28.25415625" Y="2.762398681641" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.14492578125" Y="2.730270263672" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310791016" />
                  <Point X="-27.97643359375" Y="2.734227294922" />
                  <Point X="-27.956994140625" Y="2.741302978516" />
                  <Point X="-27.938443359375" Y="2.750451904297" />
                  <Point X="-27.929419921875" Y="2.755531738281" />
                  <Point X="-27.911166015625" Y="2.767160888672" />
                  <Point X="-27.90275" Y="2.773190429688" />
                  <Point X="-27.88662109375" Y="2.786133789062" />
                  <Point X="-27.871705078125" Y="2.80025" />
                  <Point X="-27.81818359375" Y="2.853770996094" />
                  <Point X="-27.811265625" Y="2.861488769531" />
                  <Point X="-27.7983125" Y="2.87762890625" />
                  <Point X="-27.79227734375" Y="2.886051269531" />
                  <Point X="-27.7806484375" Y="2.904307617188" />
                  <Point X="-27.7755703125" Y="2.913329589844" />
                  <Point X="-27.766421875" Y="2.931881347656" />
                  <Point X="-27.759345703125" Y="2.951327880859" />
                  <Point X="-27.754431640625" Y="2.971419677734" />
                  <Point X="-27.7525234375" Y="2.981594726562" />
                  <Point X="-27.74969921875" Y="3.003056152344" />
                  <Point X="-27.74891015625" Y="3.013377685547" />
                  <Point X="-27.748458984375" Y="3.034046875" />
                  <Point X="-27.74968359375" Y="3.054536376953" />
                  <Point X="-27.756279296875" Y="3.129938720703" />
                  <Point X="-27.7577421875" Y="3.140196777344" />
                  <Point X="-27.761779296875" Y="3.160493896484" />
                  <Point X="-27.764353515625" Y="3.170532958984" />
                  <Point X="-27.77086328125" Y="3.19117578125" />
                  <Point X="-27.77451171875" Y="3.200870361328" />
                  <Point X="-27.782841796875" Y="3.219798828125" />
                  <Point X="-27.7875234375" Y="3.229032714844" />
                  <Point X="-28.059388671875" Y="3.699915527344" />
                  <Point X="-27.64837109375" Y="4.015038574219" />
                  <Point X="-27.586455078125" Y="4.049437744141" />
                  <Point X="-27.1925234375" Y="4.268296386719" />
                  <Point X="-27.142283203125" Y="4.2028203125" />
                  <Point X="-27.118564453125" Y="4.171909179688" />
                  <Point X="-27.1118203125" Y="4.164048828125" />
                  <Point X="-27.097517578125" Y="4.149107421875" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.065091796875" Y="4.121897460938" />
                  <Point X="-27.047892578125" Y="4.110405273438" />
                  <Point X="-27.0276875" Y="4.099250488281" />
                  <Point X="-26.943763671875" Y="4.0555625" />
                  <Point X="-26.934326171875" Y="4.051286621094" />
                  <Point X="-26.915044921875" Y="4.0437890625" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031378173828" />
                  <Point X="-26.780818359375" Y="4.035136474609" />
                  <Point X="-26.7707265625" Y="4.037489013672" />
                  <Point X="-26.750861328125" Y="4.043279541016" />
                  <Point X="-26.729326171875" Y="4.05158984375" />
                  <Point X="-26.6419140625" Y="4.087796875" />
                  <Point X="-26.632578125" Y="4.092275390625" />
                  <Point X="-26.614451171875" Y="4.102220703125" />
                  <Point X="-26.60566015625" Y="4.1076875" />
                  <Point X="-26.5879296875" Y="4.1201015625" />
                  <Point X="-26.579783203125" Y="4.126494140625" />
                  <Point X="-26.5642265625" Y="4.140135253906" />
                  <Point X="-26.5502421875" Y="4.155395507812" />
                  <Point X="-26.538009765625" Y="4.172081054688" />
                  <Point X="-26.5323515625" Y="4.180754882812" />
                  <Point X="-26.52153125" Y="4.199499023438" />
                  <Point X="-26.516849609375" Y="4.208740234375" />
                  <Point X="-26.508521484375" Y="4.227671875" />
                  <Point X="-26.501046875" Y="4.249505371094" />
                  <Point X="-26.472595703125" Y="4.339738769531" />
                  <Point X="-26.470025390625" Y="4.349763671875" />
                  <Point X="-26.465990234375" Y="4.370044921875" />
                  <Point X="-26.464525390625" Y="4.380301269531" />
                  <Point X="-26.462638671875" Y="4.401861328125" />
                  <Point X="-26.46230078125" Y="4.41221484375" />
                  <Point X="-26.462751953125" Y="4.432897460938" />
                  <Point X="-26.463541015625" Y="4.443226074219" />
                  <Point X="-26.479265625" Y="4.562654296875" />
                  <Point X="-25.931173828125" Y="4.7163203125" />
                  <Point X="-25.85612109375" Y="4.725104003906" />
                  <Point X="-25.36522265625" Y="4.782556152344" />
                  <Point X="-25.248587890625" Y="4.347269042969" />
                  <Point X="-25.22566796875" Y="4.261728027344" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912597656" />
                  <Point X="-25.2001171875" Y="4.205341308594" />
                  <Point X="-25.1822578125" Y="4.178614257812" />
                  <Point X="-25.17260546875" Y="4.166452636719" />
                  <Point X="-25.15144921875" Y="4.143865234375" />
                  <Point X="-25.126896484375" Y="4.125025390625" />
                  <Point X="-25.0996015625" Y="4.110436035156" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.978369140625" Y="4.085113037109" />
                  <Point X="-24.94783203125" Y="4.090154541016" />
                  <Point X="-24.93276953125" Y="4.093927246094" />
                  <Point X="-24.90233203125" Y="4.104259277344" />
                  <Point X="-24.8880859375" Y="4.110436035156" />
                  <Point X="-24.86079296875" Y="4.125024902344" />
                  <Point X="-24.8362421875" Y="4.143861328125" />
                  <Point X="-24.815083984375" Y="4.16644921875" />
                  <Point X="-24.8054296875" Y="4.178612792969" />
                  <Point X="-24.7875703125" Y="4.205339355469" />
                  <Point X="-24.78002734375" Y="4.218911621094" />
                  <Point X="-24.767251953125" Y="4.24710546875" />
                  <Point X="-24.76201953125" Y="4.261727050781" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-24.110015625" Y="4.72291796875" />
                  <Point X="-23.54640234375" Y="4.58684375" />
                  <Point X="-23.508255859375" Y="4.573008300781" />
                  <Point X="-23.14175390625" Y="4.440075195312" />
                  <Point X="-23.1026484375" Y="4.421786621094" />
                  <Point X="-22.74955078125" Y="4.256654296875" />
                  <Point X="-22.71171875" Y="4.234614257812" />
                  <Point X="-22.3705546875" Y="4.035850830078" />
                  <Point X="-22.334939453125" Y="4.0105234375" />
                  <Point X="-22.182216796875" Y="3.901915771484" />
                  <Point X="-22.832490234375" Y="2.775609863281" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.938771484375" Y="2.590693847656" />
                  <Point X="-22.94951171875" Y="2.566267822266" />
                  <Point X="-22.957060546875" Y="2.544662353516" />
                  <Point X="-22.961244140625" Y="2.531077880859" />
                  <Point X="-22.98016796875" Y="2.460314208984" />
                  <Point X="-22.982501953125" Y="2.448742431641" />
                  <Point X="-22.98571875" Y="2.425403076172" />
                  <Point X="-22.9866015625" Y="2.413635498047" />
                  <Point X="-22.9869453125" Y="2.386499267578" />
                  <Point X="-22.98559375" Y="2.361351318359" />
                  <Point X="-22.978216796875" Y="2.300160888672" />
                  <Point X="-22.97619921875" Y="2.289040527344" />
                  <Point X="-22.970857421875" Y="2.267118652344" />
                  <Point X="-22.967533203125" Y="2.256317138672" />
                  <Point X="-22.959267578125" Y="2.234227050781" />
                  <Point X="-22.954685546875" Y="2.223896240234" />
                  <Point X="-22.9443203125" Y="2.203843994141" />
                  <Point X="-22.93344140625" Y="2.186613525391" />
                  <Point X="-22.895578125" Y="2.130813964844" />
                  <Point X="-22.8882890625" Y="2.121402832031" />
                  <Point X="-22.872599609375" Y="2.103572021484" />
                  <Point X="-22.86419921875" Y="2.09515234375" />
                  <Point X="-22.843390625" Y="2.076757080078" />
                  <Point X="-22.82423046875" Y="2.061884521484" />
                  <Point X="-22.7684296875" Y="2.024022216797" />
                  <Point X="-22.758716796875" Y="2.018243408203" />
                  <Point X="-22.738671875" Y="2.007881103516" />
                  <Point X="-22.72833984375" Y="2.003297485352" />
                  <Point X="-22.706251953125" Y="1.995031860352" />
                  <Point X="-22.695451171875" Y="1.991707763672" />
                  <Point X="-22.67353125" Y="1.986365478516" />
                  <Point X="-22.654177734375" Y="1.983354248047" />
                  <Point X="-22.59298828125" Y="1.975975830078" />
                  <Point X="-22.58086328125" Y="1.975295166016" />
                  <Point X="-22.556626953125" Y="1.975486328125" />
                  <Point X="-22.544515625" Y="1.976358154297" />
                  <Point X="-22.51608984375" Y="1.980242553711" />
                  <Point X="-22.49273046875" Y="1.984942504883" />
                  <Point X="-22.421966796875" Y="2.003865600586" />
                  <Point X="-22.4160078125" Y="2.005670288086" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572875977" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.05959375" Y="2.780951416016" />
                  <Point X="-20.956041015625" Y="2.637037353516" />
                  <Point X="-20.936185546875" Y="2.604224853516" />
                  <Point X="-20.86311328125" Y="2.483472167969" />
                  <Point X="-21.692494140625" Y="1.847065795898" />
                  <Point X="-21.827046875" Y="1.743819824219" />
                  <Point X="-21.834" Y="1.737933959961" />
                  <Point X="-21.853662109375" Y="1.719005493164" />
                  <Point X="-21.86976171875" Y="1.700814575195" />
                  <Point X="-21.878279296875" Y="1.690479736328" />
                  <Point X="-21.92920703125" Y="1.624039428711" />
                  <Point X="-21.935923828125" Y="1.614015625" />
                  <Point X="-21.94803125" Y="1.59320324707" />
                  <Point X="-21.953421875" Y="1.582414794922" />
                  <Point X="-21.964443359375" Y="1.556335449219" />
                  <Point X="-21.972408203125" Y="1.533548706055" />
                  <Point X="-21.991380859375" Y="1.465712890625" />
                  <Point X="-21.993771484375" Y="1.454670288086" />
                  <Point X="-21.9972265625" Y="1.432370483398" />
                  <Point X="-21.998291015625" Y="1.42111328125" />
                  <Point X="-21.999107421875" Y="1.397541503906" />
                  <Point X="-21.998826171875" Y="1.386244384766" />
                  <Point X="-21.996923828125" Y="1.363758789062" />
                  <Point X="-21.99320703125" Y="1.342413330078" />
                  <Point X="-21.9776328125" Y="1.26693737793" />
                  <Point X="-21.974412109375" Y="1.255194335938" />
                  <Point X="-21.9665" Y="1.232225463867" />
                  <Point X="-21.96180859375" Y="1.22099987793" />
                  <Point X="-21.94891015625" Y="1.194959106445" />
                  <Point X="-21.93737890625" Y="1.174861938477" />
                  <Point X="-21.895021484375" Y="1.11048046875" />
                  <Point X="-21.888259765625" Y="1.10142578125" />
                  <Point X="-21.873708984375" Y="1.08418359375" />
                  <Point X="-21.865919921875" Y="1.07599609375" />
                  <Point X="-21.848677734375" Y="1.059906616211" />
                  <Point X="-21.83996875" Y="1.052699584961" />
                  <Point X="-21.821751953125" Y="1.039368286133" />
                  <Point X="-21.803982421875" Y="1.02859387207" />
                  <Point X="-21.7426015625" Y="0.994041320801" />
                  <Point X="-21.73140625" Y="0.988671264648" />
                  <Point X="-21.70841015625" Y="0.979437194824" />
                  <Point X="-21.696609375" Y="0.975573303223" />
                  <Point X="-21.66746484375" Y="0.968099182129" />
                  <Point X="-21.64516015625" Y="0.963781555176" />
                  <Point X="-21.562166015625" Y="0.952813049316" />
                  <Point X="-21.555966796875" Y="0.952199829102" />
                  <Point X="-21.53428125" Y="0.951222900391" />
                  <Point X="-21.50878125" Y="0.952032287598" />
                  <Point X="-21.49939453125" Y="0.952797302246" />
                  <Point X="-20.295296875" Y="1.111319946289" />
                  <Point X="-20.2473125" Y="0.914211486816" />
                  <Point X="-20.241056640625" Y="0.874031311035" />
                  <Point X="-20.21612890625" Y="0.713921081543" />
                  <Point X="-21.153806640625" Y="0.462670715332" />
                  <Point X="-21.3080078125" Y="0.421352813721" />
                  <Point X="-21.31685546875" Y="0.418511566162" />
                  <Point X="-21.3427578125" Y="0.408286468506" />
                  <Point X="-21.365810546875" Y="0.396936584473" />
                  <Point X="-21.376966796875" Y="0.390973999023" />
                  <Point X="-21.45850390625" Y="0.343843902588" />
                  <Point X="-21.468734375" Y="0.337010498047" />
                  <Point X="-21.488228515625" Y="0.322077178955" />
                  <Point X="-21.4974921875" Y="0.313976928711" />
                  <Point X="-21.518388671875" Y="0.293167358398" />
                  <Point X="-21.533787109375" Y="0.275836730957" />
                  <Point X="-21.582708984375" Y="0.213498214722" />
                  <Point X="-21.589146484375" Y="0.204206466675" />
                  <Point X="-21.60087109375" Y="0.184924880981" />
                  <Point X="-21.606158203125" Y="0.174935043335" />
                  <Point X="-21.615931640625" Y="0.153470932007" />
                  <Point X="-21.619994140625" Y="0.142926422119" />
                  <Point X="-21.626837890625" Y="0.121433334351" />
                  <Point X="-21.631814453125" Y="0.09902620697" />
                  <Point X="-21.64812109375" Y="0.01387451458" />
                  <Point X="-21.64965625" Y="0.001552934885" />
                  <Point X="-21.65110546875" Y="-0.02319455719" />
                  <Point X="-21.65101953125" Y="-0.035620616913" />
                  <Point X="-21.64882421875" Y="-0.065887962341" />
                  <Point X="-21.6459296875" Y="-0.087879241943" />
                  <Point X="-21.629623046875" Y="-0.173030639648" />
                  <Point X="-21.62683984375" Y="-0.183985183716" />
                  <Point X="-21.619994140625" Y="-0.205485412598" />
                  <Point X="-21.615931640625" Y="-0.216031097412" />
                  <Point X="-21.606158203125" Y="-0.23749520874" />
                  <Point X="-21.600869140625" Y="-0.247486679077" />
                  <Point X="-21.589142578125" Y="-0.2667706604" />
                  <Point X="-21.57612109375" Y="-0.284451873779" />
                  <Point X="-21.52719921875" Y="-0.346790252686" />
                  <Point X="-21.518728515625" Y="-0.356215148926" />
                  <Point X="-21.500607421875" Y="-0.373853118896" />
                  <Point X="-21.49095703125" Y="-0.382066467285" />
                  <Point X="-21.465671875" Y="-0.400829376221" />
                  <Point X="-21.44753125" Y="-0.412746551514" />
                  <Point X="-21.365994140625" Y="-0.459876495361" />
                  <Point X="-21.360505859375" Y="-0.462813293457" />
                  <Point X="-21.34084375" Y="-0.472015869141" />
                  <Point X="-21.31697265625" Y="-0.48102746582" />
                  <Point X="-21.3080078125" Y="-0.483912841797" />
                  <Point X="-20.215119140625" Y="-0.776751281738" />
                  <Point X="-20.238376953125" Y="-0.931014465332" />
                  <Point X="-20.246404296875" Y="-0.96618572998" />
                  <Point X="-20.272197265625" Y="-1.079219482422" />
                  <Point X="-21.38438671875" Y="-0.932796875" />
                  <Point X="-21.563216796875" Y="-0.909253662109" />
                  <Point X="-21.571375" Y="-0.908535644531" />
                  <Point X="-21.59990234375" Y="-0.908042358398" />
                  <Point X="-21.63327734375" Y="-0.910840942383" />
                  <Point X="-21.645517578125" Y="-0.912676269531" />
                  <Point X="-21.667052734375" Y="-0.917356994629" />
                  <Point X="-21.82708203125" Y="-0.952140136719" />
                  <Point X="-21.842123046875" Y="-0.956741943359" />
                  <Point X="-21.8712421875" Y="-0.96836541748" />
                  <Point X="-21.8853203125" Y="-0.975386962891" />
                  <Point X="-21.9131484375" Y="-0.992279418945" />
                  <Point X="-21.925875" Y="-1.001529907227" />
                  <Point X="-21.949623046875" Y="-1.021999267578" />
                  <Point X="-21.96064453125" Y="-1.033218261719" />
                  <Point X="-21.973662109375" Y="-1.048873046875" />
                  <Point X="-22.070388671875" Y="-1.165206298828" />
                  <Point X="-22.078669921875" Y="-1.176844848633" />
                  <Point X="-22.09339453125" Y="-1.201233520508" />
                  <Point X="-22.099837890625" Y="-1.213983764648" />
                  <Point X="-22.1111796875" Y="-1.241368896484" />
                  <Point X="-22.11563671875" Y="-1.254935791016" />
                  <Point X="-22.122466796875" Y="-1.282583251953" />
                  <Point X="-22.12483984375" Y="-1.29666394043" />
                  <Point X="-22.126705078125" Y="-1.316938232422" />
                  <Point X="-22.140568359375" Y="-1.467594238281" />
                  <Point X="-22.140708984375" Y="-1.483317016602" />
                  <Point X="-22.138392578125" Y="-1.514580932617" />
                  <Point X="-22.135935546875" Y="-1.530121948242" />
                  <Point X="-22.128205078125" Y="-1.561743041992" />
                  <Point X="-22.12321484375" Y="-1.576666748047" />
                  <Point X="-22.110841796875" Y="-1.60548059082" />
                  <Point X="-22.103458984375" Y="-1.619370605469" />
                  <Point X="-22.091541015625" Y="-1.637908447266" />
                  <Point X="-22.002978515625" Y="-1.775661254883" />
                  <Point X="-21.99825390625" Y="-1.782356201172" />
                  <Point X="-21.980201171875" Y="-1.804454101562" />
                  <Point X="-21.9565078125" Y="-1.828121337891" />
                  <Point X="-21.947201171875" Y="-1.836277954102" />
                  <Point X="-20.912826171875" Y="-2.629982177734" />
                  <Point X="-20.954505859375" Y="-2.697422851562" />
                  <Point X="-20.97109765625" Y="-2.721" />
                  <Point X="-20.998724609375" Y="-2.760252685547" />
                  <Point X="-21.9922734375" Y="-2.186626464844" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.18497265625" Y="-2.079513671875" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228892578125" Y="-2.066337158203" />
                  <Point X="-22.2545234375" Y="-2.061708496094" />
                  <Point X="-22.444982421875" Y="-2.027311523438" />
                  <Point X="-22.460640625" Y="-2.025807250977" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503295898" />
                  <Point X="-22.539859375" Y="-2.03146105957" />
                  <Point X="-22.555154296875" Y="-2.035135986328" />
                  <Point X="-22.5849296875" Y="-2.044959228516" />
                  <Point X="-22.59941015625" Y="-2.051107666016" />
                  <Point X="-22.620703125" Y="-2.062313720703" />
                  <Point X="-22.778927734375" Y="-2.145586669922" />
                  <Point X="-22.79103125" Y="-2.153171142578" />
                  <Point X="-22.813962890625" Y="-2.170065673828" />
                  <Point X="-22.824791015625" Y="-2.179375732422" />
                  <Point X="-22.84575" Y="-2.200335449219" />
                  <Point X="-22.85505859375" Y="-2.211162353516" />
                  <Point X="-22.87194921875" Y="-2.234089599609" />
                  <Point X="-22.87953125" Y="-2.246189941406" />
                  <Point X="-22.89073828125" Y="-2.267482666016" />
                  <Point X="-22.97401171875" Y="-2.425708007812" />
                  <Point X="-22.980162109375" Y="-2.440187744141" />
                  <Point X="-22.98998828125" Y="-2.469968261719" />
                  <Point X="-22.9936640625" Y="-2.485269042969" />
                  <Point X="-22.99862109375" Y="-2.517442138672" />
                  <Point X="-22.999720703125" Y="-2.533133544922" />
                  <Point X="-22.99931640625" Y="-2.564483886719" />
                  <Point X="-22.9978125" Y="-2.580142822266" />
                  <Point X="-22.99318359375" Y="-2.605773193359" />
                  <Point X="-22.95878515625" Y="-2.796233154297" />
                  <Point X="-22.95698046875" Y="-2.804229248047" />
                  <Point X="-22.94876171875" Y="-2.831540771484" />
                  <Point X="-22.935927734375" Y="-2.862479248047" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.264103515625" Y="-4.02772265625" />
                  <Point X="-22.276244140625" Y="-4.036081298828" />
                  <Point X="-23.04454296875" Y="-3.034815429688" />
                  <Point X="-23.16608203125" Y="-2.876422851562" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849626464844" />
                  <Point X="-23.21674609375" Y="-2.828004882812" />
                  <Point X="-23.22669921875" Y="-2.820645996094" />
                  <Point X="-23.251978515625" Y="-2.804394042969" />
                  <Point X="-23.43982421875" Y="-2.683627197266" />
                  <Point X="-23.45371875" Y="-2.676244140625" />
                  <Point X="-23.48253125" Y="-2.663873046875" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.619251953125" Y="-2.649060546875" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920125" Y="-2.699414794922" />
                  <Point X="-23.944505859375" Y="-2.714135742188" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-23.977486328125" Y="-2.740163330078" />
                  <Point X="-24.136123046875" Y="-2.872063720703" />
                  <Point X="-24.147345703125" Y="-2.883087646484" />
                  <Point X="-24.16781640625" Y="-2.906837158203" />
                  <Point X="-24.177064453125" Y="-2.919562744141" />
                  <Point X="-24.19395703125" Y="-2.947389404297" />
                  <Point X="-24.20098046875" Y="-2.961466796875" />
                  <Point X="-24.21260546875" Y="-2.990587890625" />
                  <Point X="-24.21720703125" Y="-3.005631591797" />
                  <Point X="-24.22358984375" Y="-3.034998046875" />
                  <Point X="-24.271021484375" Y="-3.253219482422" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520263672" />
                  <Point X="-24.16691015625" Y="-4.152328613281" />
                  <Point X="-24.297419921875" Y="-3.6652578125" />
                  <Point X="-24.344931640625" Y="-3.487936767578" />
                  <Point X="-24.347392578125" Y="-3.48012109375" />
                  <Point X="-24.357853515625" Y="-3.453580078125" />
                  <Point X="-24.3732109375" Y="-3.423816162109" />
                  <Point X="-24.37958984375" Y="-3.413209716797" />
                  <Point X="-24.399009765625" Y="-3.385229492188" />
                  <Point X="-24.543318359375" Y="-3.177309326172" />
                  <Point X="-24.553328125" Y="-3.165172119141" />
                  <Point X="-24.575212890625" Y="-3.142716308594" />
                  <Point X="-24.587087890625" Y="-3.132397705078" />
                  <Point X="-24.61334375" Y="-3.113153808594" />
                  <Point X="-24.6267578125" Y="-3.104936767578" />
                  <Point X="-24.6547578125" Y="-3.090829345703" />
                  <Point X="-24.66934375" Y="-3.084938964844" />
                  <Point X="-24.69939453125" Y="-3.075612060547" />
                  <Point X="-24.922703125" Y="-3.006305664062" />
                  <Point X="-24.936623046875" Y="-3.003109619141" />
                  <Point X="-24.964783203125" Y="-2.998840087891" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.09503515625" Y="-3.015631835938" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.3329296875" Y="-3.090829589844" />
                  <Point X="-25.3609296875" Y="-3.104937255859" />
                  <Point X="-25.37434375" Y="-3.113153808594" />
                  <Point X="-25.400599609375" Y="-3.132397705078" />
                  <Point X="-25.4124765625" Y="-3.142718261719" />
                  <Point X="-25.43436328125" Y="-3.165177490234" />
                  <Point X="-25.444373046875" Y="-3.177316162109" />
                  <Point X="-25.463791015625" Y="-3.205296142578" />
                  <Point X="-25.608099609375" Y="-3.413216552734" />
                  <Point X="-25.61247265625" Y="-3.420135498047" />
                  <Point X="-25.625974609375" Y="-3.445260742188" />
                  <Point X="-25.63877734375" Y="-3.476213378906" />
                  <Point X="-25.64275390625" Y="-3.487936279297" />
                  <Point X="-25.98542578125" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.17906662717" Y="-2.447060164451" />
                  <Point X="-28.708977440136" Y="-2.969147098657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.961956230453" Y="-3.798798202973" />
                  <Point X="-27.82308709043" Y="-3.953028007807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.106327030176" Y="-2.385870398959" />
                  <Point X="-28.624866989647" Y="-2.92058594536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.91201174414" Y="-3.712291902283" />
                  <Point X="-27.511019786559" Y="-4.157638588717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.03072540695" Y="-2.327859235618" />
                  <Point X="-28.540756539159" Y="-2.872024792064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.862067257826" Y="-3.625785601592" />
                  <Point X="-27.445976860094" Y="-4.087900804614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.955123783724" Y="-2.269848072277" />
                  <Point X="-28.45664608867" Y="-2.823463638768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.812122771513" Y="-3.539279300902" />
                  <Point X="-27.384478112116" Y="-4.014226811527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.879522160498" Y="-2.211836908936" />
                  <Point X="-28.372535638181" Y="-2.774902485471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.762178285199" Y="-3.452773000211" />
                  <Point X="-27.307632653187" Y="-3.957597067684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.691108956315" Y="-1.168503184395" />
                  <Point X="-29.59276783708" Y="-1.277722062139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.803920537272" Y="-2.153825745595" />
                  <Point X="-28.288425187692" Y="-2.726341332175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.712233798885" Y="-3.366266699521" />
                  <Point X="-27.227817343926" Y="-3.904265676788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.741224583503" Y="-0.970868869414" />
                  <Point X="-29.478480414806" Y="-1.262675831368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.728318914046" Y="-2.095814582254" />
                  <Point X="-28.204314737203" Y="-2.677780178879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.662289312572" Y="-3.27976039883" />
                  <Point X="-27.148002034665" Y="-3.850934285891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.765365925208" Y="-0.802081920955" />
                  <Point X="-29.364192992532" Y="-1.247629600596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.65271729082" Y="-2.037803418913" />
                  <Point X="-28.120204286715" Y="-2.629219025582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.612344826258" Y="-3.193254098139" />
                  <Point X="-27.065328940522" Y="-3.800776786649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.773419163746" Y="-0.651162621212" />
                  <Point X="-29.249905570258" Y="-1.232583369824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.577115667594" Y="-1.979792255573" />
                  <Point X="-28.036093836226" Y="-2.580657872286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.562400339945" Y="-3.106747797449" />
                  <Point X="-26.959716427418" Y="-3.776096093187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.135934935456" Y="-4.690998127645" />
                  <Point X="-26.087070693368" Y="-4.745267366436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.670431214889" Y="-0.623567053852" />
                  <Point X="-29.135618147983" Y="-1.217537139052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.501514044368" Y="-1.921781092232" />
                  <Point X="-27.951983385737" Y="-2.53209671899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.512455853631" Y="-3.020241496758" />
                  <Point X="-26.824907326196" Y="-3.78384149588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.12004310595" Y="-4.56667252014" />
                  <Point X="-25.974971536258" Y="-4.727790820986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.567443266031" Y="-0.595971486493" />
                  <Point X="-29.021330725709" Y="-1.20249090828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.425912421142" Y="-1.863769928891" />
                  <Point X="-27.867872935248" Y="-2.483535565693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.462511367318" Y="-2.933735196068" />
                  <Point X="-26.689054809429" Y="-3.792745728936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.142903841236" Y="-4.399307829196" />
                  <Point X="-25.945653961755" Y="-4.618376013897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.464455317174" Y="-0.568375919133" />
                  <Point X="-28.907043303435" Y="-1.187444677508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.350310797916" Y="-1.80575876555" />
                  <Point X="-27.783762484759" Y="-2.434974412397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.412566968235" Y="-2.847228798498" />
                  <Point X="-26.521649127039" Y="-3.836693302614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.179153568916" Y="-4.217073155739" />
                  <Point X="-25.916336387252" Y="-4.508961206807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.361467368317" Y="-0.540780351774" />
                  <Point X="-28.792755881161" Y="-1.172398446736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.27470917469" Y="-1.747747602209" />
                  <Point X="-27.699652034271" Y="-2.3864132591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.362622734919" Y="-2.760722216824" />
                  <Point X="-25.887018812749" Y="-4.399546399718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.258479419459" Y="-0.513184784414" />
                  <Point X="-28.678468458887" Y="-1.157352215964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.199107551472" Y="-1.689736438859" />
                  <Point X="-27.602833590641" Y="-2.351965762024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.319738922274" Y="-2.666374243595" />
                  <Point X="-25.857701238245" Y="-4.290131592628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.155491470602" Y="-0.485589217055" />
                  <Point X="-28.564181036613" Y="-1.142305985192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.12350592843" Y="-1.631725275313" />
                  <Point X="-27.450089663458" Y="-2.379629806881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.329976147309" Y="-2.513029381116" />
                  <Point X="-25.828383663742" Y="-4.180716785539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.052503521744" Y="-0.457993649695" />
                  <Point X="-28.449893614339" Y="-1.12725975442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.047915855699" Y="-1.573701283848" />
                  <Point X="-25.799066089239" Y="-4.071301978449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.949515572887" Y="-0.430398082336" />
                  <Point X="-28.335606192065" Y="-1.112213523648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.986000951492" Y="-1.500489479078" />
                  <Point X="-25.769748514736" Y="-3.96188717136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.77932626518" Y="0.633175329701" />
                  <Point X="-29.728309614783" Y="0.576515599306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.84652762403" Y="-0.402802514976" />
                  <Point X="-28.218968454014" Y="-1.099777582992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.9513826064" Y="-1.396961774143" />
                  <Point X="-25.740430940233" Y="-3.852472364271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.76128217519" Y="0.755110609776" />
                  <Point X="-29.559825597998" Y="0.531370413952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.743539675172" Y="-0.375206947617" />
                  <Point X="-25.71111336573" Y="-3.743057557181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.743238085199" Y="0.877045889851" />
                  <Point X="-29.391341581212" Y="0.486225228599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.640551710234" Y="-0.347611398117" />
                  <Point X="-25.681795791227" Y="-3.633642750092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.721275873111" Y="0.99462965449" />
                  <Point X="-29.222857564427" Y="0.441080043245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.5375637235" Y="-0.320015872823" />
                  <Point X="-25.652478216723" Y="-3.524227943002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.691703976141" Y="1.103762007865" />
                  <Point X="-29.054373547642" Y="0.395934857892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.443151241243" Y="-0.282896284938" />
                  <Point X="-25.614676226657" Y="-3.424236034018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.662131634152" Y="1.212893866996" />
                  <Point X="-28.885889530856" Y="0.350789672539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.368051571283" Y="-0.224327646017" />
                  <Point X="-25.559635662753" Y="-3.343389500876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.628551135896" Y="1.317574217616" />
                  <Point X="-28.717405514071" Y="0.305644487185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.316539307014" Y="-0.139562539144" />
                  <Point X="-25.50398999802" Y="-3.263215000287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.483524543388" Y="1.29848114123" />
                  <Point X="-28.548921497286" Y="0.260499301832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.296410214743" Y="-0.019942888696" />
                  <Point X="-25.448345110188" Y="-3.183039636863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.338497950879" Y="1.279388064844" />
                  <Point X="-25.379820896941" Y="-3.117168213426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.19347135837" Y="1.260294988458" />
                  <Point X="-25.28916079983" Y="-3.075881179636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.048444765862" Y="1.241201912072" />
                  <Point X="-25.189246826265" Y="-3.044871616847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.903418171613" Y="1.222108833754" />
                  <Point X="-25.089332855491" Y="-3.013862050958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.210402232408" Y="-3.990013400621" />
                  <Point X="-24.184490765038" Y="-4.018791000559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.759857275118" Y="1.204643577704" />
                  <Point X="-24.975769274015" Y="-2.998011913537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.264561262952" Y="-3.787888431269" />
                  <Point X="-24.206383229424" Y="-3.852501683394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.648454821672" Y="1.22289389096" />
                  <Point X="-24.808572564571" Y="-3.041727399246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.318719563482" Y="-3.585764272682" />
                  <Point X="-24.22827569381" Y="-3.686212366229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.555813392007" Y="1.26198043202" />
                  <Point X="-24.620214403145" Y="-3.108945058359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.491719439206" Y="-3.251653173402" />
                  <Point X="-24.250168158196" Y="-3.519923049065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.48788608904" Y="1.328514791483" />
                  <Point X="-24.272060622582" Y="-3.3536337319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.443034614029" Y="1.420677454265" />
                  <Point X="-24.263946658398" Y="-3.220669929831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.178129483709" Y="2.379058288356" />
                  <Point X="-28.820560697234" Y="1.981937919185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.421725270903" Y="1.538986303344" />
                  <Point X="-24.239088298101" Y="-3.106302663638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.127852118378" Y="2.465194889443" />
                  <Point X="-24.213325047447" Y="-2.992940379999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.077574753047" Y="2.551331490531" />
                  <Point X="-24.16546938177" Y="-2.904114208969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.027297387716" Y="2.637468091619" />
                  <Point X="-24.096303575583" Y="-2.838955346681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.972508870273" Y="2.718594550715" />
                  <Point X="-24.023198616395" Y="-2.778171357014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.913253347103" Y="2.794759897347" />
                  <Point X="-23.949696059048" Y="-2.717828944838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.853997497115" Y="2.87092488101" />
                  <Point X="-23.85991747274" Y="-2.675562894118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.794741647126" Y="2.947089864674" />
                  <Point X="-23.745485910325" Y="-2.660676747191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.735485797138" Y="3.023254848337" />
                  <Point X="-23.627432283191" Y="-2.64981331067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.501200155838" Y="2.905029555302" />
                  <Point X="-23.488835796318" Y="-2.661765031265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.419520774852" Y="-3.8493596764" />
                  <Point X="-22.273363702394" Y="-4.011683550203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.237972512106" Y="2.754660912161" />
                  <Point X="-23.194021028949" Y="-2.847214729224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.156626304024" Y="-2.888745778715" />
                  <Point X="-22.501826398387" Y="-3.615974748625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.083323154178" Y="2.724880672073" />
                  <Point X="-22.730289094379" Y="-3.220265947047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.966996605816" Y="2.737662223892" />
                  <Point X="-22.945599182748" Y="-2.839164596098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.884560970271" Y="2.788083447625" />
                  <Point X="-22.984470088888" Y="-2.654018809039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.817029991345" Y="2.855057969528" />
                  <Point X="-22.995843188032" Y="-2.499412430561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.764103557382" Y="2.938252481841" />
                  <Point X="-22.959388938189" Y="-2.397923704417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.750621098258" Y="3.065253966244" />
                  <Point X="-22.912231763008" Y="-2.3083217811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.818388776408" Y="3.282492869936" />
                  <Point X="-22.862628017361" Y="-2.221437049562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.046853302839" Y="3.678203704421" />
                  <Point X="-22.794174622896" Y="-2.1554869739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.987910905516" Y="3.754716812538" />
                  <Point X="-22.708586847722" Y="-2.108566555887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.912283629244" Y="3.812699485285" />
                  <Point X="-22.621853052104" Y="-2.062918922522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.836656352972" Y="3.870682158032" />
                  <Point X="-22.524476976343" Y="-2.02909073867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.7610290767" Y="3.928664830779" />
                  <Point X="-22.389167653158" Y="-2.037391694135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.685401800428" Y="3.986647503526" />
                  <Point X="-22.236508268429" Y="-2.064961845084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.604884206153" Y="4.039198927897" />
                  <Point X="-22.006544842533" Y="-2.1783868316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.519674585122" Y="4.086539328634" />
                  <Point X="-21.740305401481" Y="-2.332100414536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.434464934022" Y="4.133879695975" />
                  <Point X="-21.474066009164" Y="-2.485813943347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.349255282922" Y="4.181220063316" />
                  <Point X="-22.046230893651" Y="-1.708385189852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.895031379713" Y="-1.876309262268" />
                  <Point X="-21.207826616846" Y="-2.639527472159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.264045631821" Y="4.228560430657" />
                  <Point X="-22.140071051839" Y="-1.462189863538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.481453150374" Y="-2.193659147396" />
                  <Point X="-20.986688228984" Y="-2.74315126099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.012802347096" Y="4.091501766611" />
                  <Point X="-22.128217931346" Y="-1.33337881526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.067874921035" Y="-2.511009032524" />
                  <Point X="-20.932332261531" Y="-2.661544406462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.828751755289" Y="4.029068148226" />
                  <Point X="-22.102373675089" Y="-1.220106497459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.723403990618" Y="4.05404287461" />
                  <Point X="-22.04810371088" Y="-1.138404126652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.63084818955" Y="4.093224515861" />
                  <Point X="-21.986730621529" Y="-1.064590575522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.554655640173" Y="4.150579389223" />
                  <Point X="-21.919710249032" Y="-0.997048967728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.505136848026" Y="4.237558471182" />
                  <Point X="-21.831181825259" Y="-0.953394470852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.471866415609" Y="4.342583184803" />
                  <Point X="-21.72454362535" Y="-0.929852917992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.4684892313" Y="4.480807713882" />
                  <Point X="-21.615192411877" Y="-0.90932447195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.427433685453" Y="4.577186183099" />
                  <Point X="-21.477228002901" Y="-0.920574198923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.325365016124" Y="4.605802713806" />
                  <Point X="-21.332201481887" Y="-0.939667195905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.223296346794" Y="4.634419244514" />
                  <Point X="-21.187174840116" Y="-0.958760327003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.121227677465" Y="4.663035775222" />
                  <Point X="-21.042148198345" Y="-0.9778534581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.019159008135" Y="4.69165230593" />
                  <Point X="-21.625594992797" Y="-0.187894874208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.396663521956" Y="-0.442149030762" />
                  <Point X="-20.897121556574" Y="-0.996946589198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.915216604416" Y="4.718187843776" />
                  <Point X="-21.650797160951" Y="-0.017929758618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.206808528847" Y="-0.511029089875" />
                  <Point X="-20.752094914803" Y="-1.016039720295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.799568284532" Y="4.73172264463" />
                  <Point X="-21.631113086111" Y="0.102184133759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.038324582676" Y="-0.556174196802" />
                  <Point X="-20.607068273032" Y="-1.035132851393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.683920017496" Y="4.745257504179" />
                  <Point X="-25.203891185867" Y="4.212131476293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.124128224948" Y="4.123545733677" />
                  <Point X="-21.591562210705" Y="0.200233708798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.869840636506" Y="-0.60131930373" />
                  <Point X="-20.462041631261" Y="-1.05422598249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.56827175046" Y="4.758792363728" />
                  <Point X="-25.270132697308" Y="4.427675400138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.963846912779" Y="4.087510574725" />
                  <Point X="-21.532800343459" Y="0.276947315876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.701356690335" Y="-0.646464410658" />
                  <Point X="-20.31701498949" Y="-1.073319113588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.452623483424" Y="4.772327223277" />
                  <Point X="-25.324291958677" Y="4.629800625846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.866866646191" Y="4.121778349199" />
                  <Point X="-21.462684744724" Y="0.341051326674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.532872744165" Y="-0.691609517585" />
                  <Point X="-20.254337948322" Y="-1.000953747664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.798944286376" Y="4.188318198588" />
                  <Point X="-21.37883033162" Y="0.389896838294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.364388797994" Y="-0.736754624513" />
                  <Point X="-20.231363264875" Y="-0.884494446387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.756328980861" Y="4.282964379197" />
                  <Point X="-22.97933172244" Y="2.309408985178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.692659678783" Y="1.991027425841" />
                  <Point X="-21.28489463067" Y="0.427545945467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.727011278807" Y="4.392379044627" />
                  <Point X="-22.981486436873" Y="2.45377731023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.551184081964" Y="1.975878129708" />
                  <Point X="-21.996179275801" Y="1.359482846192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.639176152093" Y="0.96299070917" />
                  <Point X="-21.181906574145" Y="0.45514139325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.697693576753" Y="4.501793710057" />
                  <Point X="-22.951028759905" Y="2.561925905253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.443390595915" Y="1.998136607321" />
                  <Point X="-21.984890114629" Y="1.488920234751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.50197359913" Y="0.952587109019" />
                  <Point X="-21.078918601978" Y="0.482736934722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.6683758747" Y="4.611208375488" />
                  <Point X="-22.904028175442" Y="2.651701740182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.349729880505" Y="2.036091116876" />
                  <Point X="-21.949073573276" Y="1.591117207922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.387582063041" Y="0.967517709686" />
                  <Point X="-20.975930661465" Y="0.510332511348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.639058172646" Y="4.720623040918" />
                  <Point X="-22.854083910006" Y="2.738208286183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.265619446963" Y="2.084652288994" />
                  <Point X="-21.893189897737" Y="1.671027370732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.27329464956" Y="0.982563950223" />
                  <Point X="-20.872942720951" Y="0.537928087974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.56371633128" Y="4.778922721244" />
                  <Point X="-22.804139525486" Y="2.824714699926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.181509013421" Y="2.133213461112" />
                  <Point X="-21.829231949301" Y="1.741970145013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.159007236078" Y="0.99761019076" />
                  <Point X="-20.769954780437" Y="0.5655236646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.422571761222" Y="4.764141067574" />
                  <Point X="-22.754195050264" Y="2.911221012935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.097398579879" Y="2.18177463323" />
                  <Point X="-21.753722464341" Y="1.800083638265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.044719822597" Y="1.012656431297" />
                  <Point X="-20.666966839923" Y="0.593119241226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.281427191163" Y="4.749359413904" />
                  <Point X="-22.704250575041" Y="2.997727325943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.013288146337" Y="2.230335805347" />
                  <Point X="-21.678120849567" Y="1.858094810993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.930432409115" Y="1.027702671834" />
                  <Point X="-20.563978899409" Y="0.620714817852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.135275130155" Y="4.729016378118" />
                  <Point X="-22.654306099818" Y="3.084233638951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.929177712795" Y="2.278896977465" />
                  <Point X="-21.602519230357" Y="1.916105978794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.816144995633" Y="1.042748912371" />
                  <Point X="-20.460990958895" Y="0.648310394478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.97193137025" Y="4.689580026384" />
                  <Point X="-22.604361624596" Y="3.170739951959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.845067279253" Y="2.327458149583" />
                  <Point X="-21.526917611147" Y="1.974117146595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.701857582152" Y="1.057795152908" />
                  <Point X="-20.358003018381" Y="0.675905971104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.808587563566" Y="4.650143622698" />
                  <Point X="-22.554417149373" Y="3.257246264968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.760956845711" Y="2.376019321701" />
                  <Point X="-21.451315991936" Y="2.032128314396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.58757016867" Y="1.072841393445" />
                  <Point X="-20.255015077868" Y="0.703501547729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.645243756882" Y="4.610707219012" />
                  <Point X="-22.504472674151" Y="3.343752577976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.676846412169" Y="2.424580493818" />
                  <Point X="-21.375714372726" Y="2.090139482197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.473282755188" Y="1.087887633982" />
                  <Point X="-20.23276336219" Y="0.820763786059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.471441389027" Y="4.559655406403" />
                  <Point X="-22.454528198928" Y="3.430258890984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.592735978627" Y="2.473141665936" />
                  <Point X="-21.300112753516" Y="2.148150649998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.358995341707" Y="1.102933874519" />
                  <Point X="-20.268895005671" Y="1.002867313727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.28161070152" Y="4.490802341396" />
                  <Point X="-22.404583723705" Y="3.516765203993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.508625545085" Y="2.521702838054" />
                  <Point X="-21.224511134306" Y="2.206161817799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.083621516753" Y="4.412888347231" />
                  <Point X="-22.354639248483" Y="3.603271517001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.424515111544" Y="2.570264010172" />
                  <Point X="-21.148909515096" Y="2.2641729856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.862801251781" Y="4.309617869662" />
                  <Point X="-22.30469477326" Y="3.689777830009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.340404678002" Y="2.618825182289" />
                  <Point X="-21.073307895886" Y="2.322184153401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.618567346636" Y="4.180343910299" />
                  <Point X="-22.254750298037" Y="3.776284143018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.25629424446" Y="2.667386354407" />
                  <Point X="-20.997706276676" Y="2.380195321202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.342962376497" Y="4.016228853551" />
                  <Point X="-22.204805822815" Y="3.862790456026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.172183810918" Y="2.715947526525" />
                  <Point X="-20.922104657466" Y="2.438206489003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.088073377376" Y="2.764508698643" />
                  <Point X="-20.920675837212" Y="2.578594895585" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.480947265625" Y="-3.71443359375" />
                  <Point X="-24.528458984375" Y="-3.537112548828" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.555099609375" Y="-3.493563964844" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.75571484375" Y="-3.257072998047" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.03871484375" Y="-3.197093261719" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643798828" />
                  <Point X="-25.307697265625" Y="-3.313623779297" />
                  <Point X="-25.452005859375" Y="-3.521544189453" />
                  <Point X="-25.4592265625" Y="-3.537112060547" />
                  <Point X="-25.812294921875" Y="-4.854780761719" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.133859375" Y="-4.929416992188" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.314869140625" Y="-4.594483398438" />
                  <Point X="-26.3091484375" Y="-4.551040039062" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.316861328125" Y="-4.498665039062" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.41394921875" Y="-4.178365234375" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.6859609375" Y="-3.983356201172" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791259766" />
                  <Point X="-27.0204765625" Y="-3.994235839844" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157293945313" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.47075" Y="-4.406044433594" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-27.90237890625" Y="-4.131771484375" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.594330078125" Y="-2.782055175781" />
                  <Point X="-27.506033203125" Y="-2.629119384766" />
                  <Point X="-27.499763671875" Y="-2.597588867188" />
                  <Point X="-27.51604296875" Y="-2.566700439453" />
                  <Point X="-27.53132421875" Y="-2.551417724609" />
                  <Point X="-27.560154296875" Y="-2.537198974609" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.728146484375" Y="-3.199607177734" />
                  <Point X="-28.842958984375" Y="-3.26589453125" />
                  <Point X="-28.85746875" Y="-3.24683203125" />
                  <Point X="-29.16169921875" Y="-2.847135742188" />
                  <Point X="-29.195072265625" Y="-2.791173828125" />
                  <Point X="-29.43101953125" Y="-2.395526855469" />
                  <Point X="-28.318279296875" Y="-1.541690917969" />
                  <Point X="-28.163787109375" Y="-1.423144897461" />
                  <Point X="-28.15253515625" Y="-1.411081787109" />
                  <Point X="-28.144908203125" Y="-1.392485717773" />
                  <Point X="-28.1381171875" Y="-1.366266601562" />
                  <Point X="-28.14032421875" Y="-1.334595703125" />
                  <Point X="-28.164298828125" Y="-1.308790283203" />
                  <Point X="-28.187640625" Y="-1.295052490234" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.654205078125" Y="-1.477449951172" />
                  <Point X="-29.803283203125" Y="-1.497076416016" />
                  <Point X="-29.808404296875" Y="-1.477025878906" />
                  <Point X="-29.927392578125" Y="-1.011187683105" />
                  <Point X="-29.936220703125" Y="-0.94946307373" />
                  <Point X="-29.99839453125" Y="-0.514742248535" />
                  <Point X="-28.733453125" Y="-0.175801742554" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.5386015625" Y="-0.119140266418" />
                  <Point X="-28.514140625" Y="-0.102162872314" />
                  <Point X="-28.50232421875" Y="-0.090647277832" />
                  <Point X="-28.493798828125" Y="-0.072369987488" />
                  <Point X="-28.485646484375" Y="-0.04609859848" />
                  <Point X="-28.48674609375" Y="-0.012919065475" />
                  <Point X="-28.494896484375" Y="0.013345396042" />
                  <Point X="-28.50232421875" Y="0.028087657928" />
                  <Point X="-28.51743359375" Y="0.041887428284" />
                  <Point X="-28.54189453125" Y="0.058864818573" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.86523828125" Y="0.416502929688" />
                  <Point X="-29.9981875" Y="0.452126220703" />
                  <Point X="-29.99433203125" Y="0.478177093506" />
                  <Point X="-29.91764453125" Y="0.996414978027" />
                  <Point X="-29.89987109375" Y="1.062006103516" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-28.87941796875" Y="1.410588745117" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.72441796875" Y="1.398163818359" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.651533203125" Y="1.426056518555" />
                  <Point X="-28.6391171875" Y="1.443786743164" />
                  <Point X="-28.636193359375" Y="1.450844726562" />
                  <Point X="-28.614470703125" Y="1.503291137695" />
                  <Point X="-28.610712890625" Y="1.524606079102" />
                  <Point X="-28.61631640625" Y="1.545512084961" />
                  <Point X="-28.61984375" Y="1.552288330078" />
                  <Point X="-28.646056640625" Y="1.602641845703" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-29.410111328125" Y="2.194827148438" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.457990234375" Y="2.276500976562" />
                  <Point X="-29.16001171875" Y="2.787007568359" />
                  <Point X="-29.112935546875" Y="2.847519042969" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.23663671875" Y="2.971676513672" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.1283671875" Y="2.919547119141" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927404541016" />
                  <Point X="-28.006048828125" Y="2.934606933594" />
                  <Point X="-27.95252734375" Y="2.988127929688" />
                  <Point X="-27.9408984375" Y="3.006384277344" />
                  <Point X="-27.93807421875" Y="3.027845703125" />
                  <Point X="-27.9389609375" Y="3.037987548828" />
                  <Point X="-27.945556640625" Y="3.113389892578" />
                  <Point X="-27.95206640625" Y="3.134032714844" />
                  <Point X="-28.284478515625" Y="3.709785888672" />
                  <Point X="-28.30727734375" Y="3.749276611328" />
                  <Point X="-28.271857421875" Y="3.776433837891" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.678728515625" Y="4.215525878906" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-26.991544921875" Y="4.318484375" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.939953125" Y="4.267781738281" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.802044921875" Y="4.227123046875" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.69690234375" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.29448828125" />
                  <Point X="-26.68225390625" Y="4.306631347656" />
                  <Point X="-26.653802734375" Y="4.396864746094" />
                  <Point X="-26.651916015625" Y="4.418424804688" />
                  <Point X="-26.689138671875" Y="4.701140136719" />
                  <Point X="-26.6399453125" Y="4.714932128906" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.87820703125" Y="4.913815917969" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.065060546875" Y="4.396444824219" />
                  <Point X="-25.042140625" Y="4.310903808594" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176269531" />
                  <Point X="-24.945546875" Y="4.310902832031" />
                  <Point X="-24.77522265625" Y="4.946560058594" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.72641796875" Y="4.987000976562" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-24.06542578125" Y="4.907611328125" />
                  <Point X="-23.491544921875" Y="4.76905859375" />
                  <Point X="-23.44347265625" Y="4.751622558594" />
                  <Point X="-23.06896484375" Y="4.615785644531" />
                  <Point X="-23.022158203125" Y="4.593895507812" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.61607421875" Y="4.398786132812" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-22.224828125" Y="4.16536328125" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.667947265625" Y="2.680609863281" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.777693359375" Y="2.481992431641" />
                  <Point X="-22.7966171875" Y="2.411228759766" />
                  <Point X="-22.7969609375" Y="2.384092529297" />
                  <Point X="-22.789583984375" Y="2.322902099609" />
                  <Point X="-22.781318359375" Y="2.300812011719" />
                  <Point X="-22.77622265625" Y="2.293302978516" />
                  <Point X="-22.738359375" Y="2.237503417969" />
                  <Point X="-22.71755078125" Y="2.219108154297" />
                  <Point X="-22.66175" Y="2.181245849609" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.631427734375" Y="2.171987060547" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5418125" Y="2.168492919922" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.14359375" Y="2.951847412109" />
                  <Point X="-21.005751953125" Y="3.031430419922" />
                  <Point X="-21.004185546875" Y="3.029253662109" />
                  <Point X="-20.79740234375" Y="2.741874267578" />
                  <Point X="-20.773630859375" Y="2.702591064453" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.576830078125" Y="1.696328857422" />
                  <Point X="-21.7113828125" Y="1.593082641602" />
                  <Point X="-21.727482421875" Y="1.574891845703" />
                  <Point X="-21.77841015625" Y="1.508451538086" />
                  <Point X="-21.789431640625" Y="1.482372314453" />
                  <Point X="-21.808404296875" Y="1.414536621094" />
                  <Point X="-21.809220703125" Y="1.39096472168" />
                  <Point X="-21.807125" Y="1.380807739258" />
                  <Point X="-21.79155078125" Y="1.30533190918" />
                  <Point X="-21.77865234375" Y="1.279291137695" />
                  <Point X="-21.736294921875" Y="1.214909667969" />
                  <Point X="-21.719052734375" Y="1.19882019043" />
                  <Point X="-21.710791015625" Y="1.194170288086" />
                  <Point X="-21.64941015625" Y="1.159617675781" />
                  <Point X="-21.620265625" Y="1.152143554688" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.274681640625" Y="1.305673583984" />
                  <Point X="-20.151025390625" Y="1.32195324707" />
                  <Point X="-20.14962109375" Y="1.316188232422" />
                  <Point X="-20.06080859375" Y="0.951367492676" />
                  <Point X="-20.053318359375" Y="0.903263122559" />
                  <Point X="-20.002140625" Y="0.574556396484" />
                  <Point X="-21.104630859375" Y="0.279144836426" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.281884765625" Y="0.226476867676" />
                  <Point X="-21.363421875" Y="0.179346832275" />
                  <Point X="-21.384318359375" Y="0.158537322998" />
                  <Point X="-21.433240234375" Y="0.09619871521" />
                  <Point X="-21.443013671875" Y="0.074734649658" />
                  <Point X="-21.445208984375" Y="0.063275733948" />
                  <Point X="-21.461515625" Y="-0.021875907898" />
                  <Point X="-21.4593203125" Y="-0.052143241882" />
                  <Point X="-21.443013671875" Y="-0.137294723511" />
                  <Point X="-21.433240234375" Y="-0.158758789063" />
                  <Point X="-21.42665625" Y="-0.167147857666" />
                  <Point X="-21.377734375" Y="-0.229486465454" />
                  <Point X="-21.35244921875" Y="-0.248249404907" />
                  <Point X="-21.270912109375" Y="-0.295379425049" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.112970703125" Y="-0.607419616699" />
                  <Point X="-20.001931640625" Y="-0.637172363281" />
                  <Point X="-20.00198046875" Y="-0.637497253418" />
                  <Point X="-20.051568359375" Y="-0.966412841797" />
                  <Point X="-20.061166015625" Y="-1.008463684082" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-21.4091875" Y="-1.121171508789" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.626697265625" Y="-1.103021972656" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.8145546875" Y="-1.154697387695" />
                  <Point X="-21.827572265625" Y="-1.170352416992" />
                  <Point X="-21.924298828125" Y="-1.286685424805" />
                  <Point X="-21.935640625" Y="-1.314070556641" />
                  <Point X="-21.937505859375" Y="-1.334344726563" />
                  <Point X="-21.951369140625" Y="-1.485000854492" />
                  <Point X="-21.943638671875" Y="-1.516621826172" />
                  <Point X="-21.931720703125" Y="-1.535159667969" />
                  <Point X="-21.843158203125" Y="-1.672912475586" />
                  <Point X="-21.831537109375" Y="-1.685540771484" />
                  <Point X="-20.768171875" Y="-2.501490722656" />
                  <Point X="-20.660923828125" Y="-2.583784423828" />
                  <Point X="-20.7958671875" Y="-2.802140869141" />
                  <Point X="-20.81571875" Y="-2.830348876953" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-22.0872734375" Y="-2.351171386719" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.2882890625" Y="-2.248684082031" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.53221484375" Y="-2.230450927734" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.33468359375" />
                  <Point X="-22.72260546875" Y="-2.355976318359" />
                  <Point X="-22.80587890625" Y="-2.514201660156" />
                  <Point X="-22.8108359375" Y="-2.546374755859" />
                  <Point X="-22.80620703125" Y="-2.572005126953" />
                  <Point X="-22.77180859375" Y="-2.762465087891" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.0825859375" Y="-3.962123291016" />
                  <Point X="-22.01332421875" Y="-4.082088623047" />
                  <Point X="-22.1646875" Y="-4.190202636719" />
                  <Point X="-22.18689453125" Y="-4.204578125" />
                  <Point X="-22.32022265625" Y="-4.29087890625" />
                  <Point X="-23.19528125" Y="-3.150479980469" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.354728515625" Y="-2.964215332031" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.601841796875" Y="-2.838261230469" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.856013671875" Y="-2.886259033203" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045986083984" />
                  <Point X="-24.03792578125" Y="-3.075352539062" />
                  <Point X="-24.085357421875" Y="-3.293573974609" />
                  <Point X="-24.0860703125" Y="-3.310719970703" />
                  <Point X="-23.892421875" Y="-4.781619628906" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-24.005650390625" Y="-4.963246582031" />
                  <Point X="-24.02616015625" Y="-4.96697265625" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#128" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.022920783437" Y="4.440666006273" Z="0.3" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.3" />
                  <Point X="-0.890176654647" Y="4.994757309884" Z="0.3" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.3" />
                  <Point X="-1.659601239299" Y="4.794355896753" Z="0.3" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.3" />
                  <Point X="-1.74543731072" Y="4.73023516653" Z="0.3" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.3" />
                  <Point X="-1.736096496222" Y="4.352946996444" Z="0.3" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.3" />
                  <Point X="-1.827336009275" Y="4.304597030641" Z="0.3" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.3" />
                  <Point X="-1.923021593809" Y="4.343412201105" Z="0.3" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.3" />
                  <Point X="-1.958034232034" Y="4.380202577259" Z="0.3" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.3" />
                  <Point X="-2.709168484006" Y="4.290513274447" Z="0.3" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.3" />
                  <Point X="-3.308435598382" Y="3.84739381736" Z="0.3" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.3" />
                  <Point X="-3.333936054119" Y="3.716066253979" Z="0.3" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.3" />
                  <Point X="-2.994927771378" Y="3.064910584831" Z="0.3" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.3" />
                  <Point X="-3.04756112162" Y="3.001242446267" Z="0.3" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.3" />
                  <Point X="-3.130165880776" Y="3.000636927795" Z="0.3" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.3" />
                  <Point X="-3.217793094318" Y="3.046257894626" Z="0.3" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.3" />
                  <Point X="-4.158555515522" Y="2.909501502997" Z="0.3" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.3" />
                  <Point X="-4.507329198" Y="2.332986107535" Z="0.3" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.3" />
                  <Point X="-4.446705943642" Y="2.186439527258" Z="0.3" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.3" />
                  <Point X="-3.670349884955" Y="1.560480553132" Z="0.3" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.3" />
                  <Point X="-3.688546464969" Y="1.501257926128" Z="0.3" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.3" />
                  <Point X="-3.745610298701" Y="1.477129727547" Z="0.3" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.3" />
                  <Point X="-3.879049898843" Y="1.491441006393" Z="0.3" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.3" />
                  <Point X="-4.95428785543" Y="1.106363588244" Z="0.3" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.3" />
                  <Point X="-5.049949098396" Y="0.516776252241" Z="0.3" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.3" />
                  <Point X="-4.884337177039" Y="0.399486678799" Z="0.3" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.3" />
                  <Point X="-3.552099634682" Y="0.032091631074" Z="0.3" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.3" />
                  <Point X="-3.540654018987" Y="0.003535409559" Z="0.3" />
                  <Point X="-3.539556741714" Y="0" Z="0.3" />
                  <Point X="-3.547710582148" Y="-0.026271541502" Z="0.3" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.3" />
                  <Point X="-3.573268960442" Y="-0.04678435206" Z="0.3" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.3" />
                  <Point X="-3.752550611733" Y="-0.096225377957" Z="0.3" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.3" />
                  <Point X="-4.991873627245" Y="-0.925262157864" Z="0.3" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.3" />
                  <Point X="-4.863005660257" Y="-1.458119841728" Z="0.3" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.3" />
                  <Point X="-4.653836169148" Y="-1.495742119034" Z="0.3" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.3" />
                  <Point X="-3.195817411136" Y="-1.320601085424" Z="0.3" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.3" />
                  <Point X="-3.199466970005" Y="-1.348668827989" Z="0.3" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.3" />
                  <Point X="-3.354872985421" Y="-1.470743186905" Z="0.3" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.3" />
                  <Point X="-4.244172405535" Y="-2.785503410855" Z="0.3" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.3" />
                  <Point X="-3.903410394573" Y="-3.24583515394" Z="0.3" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.3" />
                  <Point X="-3.709302915994" Y="-3.211628421502" Z="0.3" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.3" />
                  <Point X="-2.557548683636" Y="-2.570781785496" Z="0.3" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.3" />
                  <Point X="-2.643788591666" Y="-2.725775466498" Z="0.3" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.3" />
                  <Point X="-2.93904053648" Y="-4.14010870366" Z="0.3" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.3" />
                  <Point X="-2.503230082172" Y="-4.417274909183" Z="0.3" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.3" />
                  <Point X="-2.424442847897" Y="-4.414778166478" Z="0.3" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.3" />
                  <Point X="-1.998853438567" Y="-4.004528954861" Z="0.3" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.3" />
                  <Point X="-1.69538729161" Y="-4.001969147028" Z="0.3" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.3" />
                  <Point X="-1.45307310646" Y="-4.184677053485" Z="0.3" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.3" />
                  <Point X="-1.372057709795" Y="-4.477140117199" Z="0.3" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.3" />
                  <Point X="-1.370597982061" Y="-4.556675723872" Z="0.3" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.3" />
                  <Point X="-1.152474555018" Y="-4.946559667257" Z="0.3" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.3" />
                  <Point X="-0.853219572698" Y="-5.006862280966" Z="0.3" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.3" />
                  <Point X="-0.77015503697" Y="-4.836441846383" Z="0.3" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.3" />
                  <Point X="-0.272778860908" Y="-3.310852879682" Z="0.3" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.3" />
                  <Point X="-0.030050980702" Y="-3.213566083864" Z="0.3" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.3" />
                  <Point X="0.22330809866" Y="-3.273545961424" Z="0.3" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.3" />
                  <Point X="0.397666998524" Y="-3.490792876086" Z="0.3" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.3" />
                  <Point X="0.464599823431" Y="-3.696094183433" Z="0.3" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.3" />
                  <Point X="0.976619741722" Y="-4.98488723517" Z="0.3" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.3" />
                  <Point X="1.155816663665" Y="-4.946410281962" Z="0.3" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.3" />
                  <Point X="1.150993452488" Y="-4.743813595399" Z="0.3" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.3" />
                  <Point X="1.004777097153" Y="-3.054692447311" Z="0.3" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.3" />
                  <Point X="1.169795837491" Y="-2.893425530001" Z="0.3" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.3" />
                  <Point X="1.396583986774" Y="-2.856770642481" Z="0.3" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.3" />
                  <Point X="1.612075275066" Y="-2.974993451463" Z="0.3" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.3" />
                  <Point X="1.758892950868" Y="-3.149638016126" Z="0.3" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.3" />
                  <Point X="2.834117668655" Y="-4.215272378526" Z="0.3" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.3" />
                  <Point X="3.024066917953" Y="-4.081147405703" Z="0.3" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.3" />
                  <Point X="2.954556967612" Y="-3.905843207782" Z="0.3" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.3" />
                  <Point X="2.23683997045" Y="-2.531838719987" Z="0.3" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.3" />
                  <Point X="2.31548401035" Y="-2.347982867844" Z="0.3" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.3" />
                  <Point X="2.484915510528" Y="-2.243417299617" Z="0.3" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.3" />
                  <Point X="2.696668083526" Y="-2.266608039041" Z="0.3" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.3" />
                  <Point X="2.881570389325" Y="-2.363192532662" Z="0.3" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.3" />
                  <Point X="4.219012041221" Y="-2.827846070676" Z="0.3" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.3" />
                  <Point X="4.380291838421" Y="-2.57095694341" Z="0.3" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.3" />
                  <Point X="4.25610935937" Y="-2.430542978564" Z="0.3" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.3" />
                  <Point X="3.104180500925" Y="-1.476840154961" Z="0.3" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.3" />
                  <Point X="3.106125842423" Y="-1.307646311059" Z="0.3" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.3" />
                  <Point X="3.204718758452" Y="-1.171039316781" Z="0.3" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.3" />
                  <Point X="3.377764471483" Y="-1.120601230245" Z="0.3" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.3" />
                  <Point X="3.578129331931" Y="-1.139463769306" Z="0.3" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.3" />
                  <Point X="4.981423930931" Y="-0.988307502563" Z="0.3" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.3" />
                  <Point X="5.041304538375" Y="-0.613671150005" Z="0.3" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.3" />
                  <Point X="4.893814404841" Y="-0.52784334132" Z="0.3" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.3" />
                  <Point X="3.666415346025" Y="-0.173680584955" Z="0.3" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.3" />
                  <Point X="3.60652030251" Y="-0.104999557712" Z="0.3" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.3" />
                  <Point X="3.583629252982" Y="-0.011458998134" Z="0.3" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.3" />
                  <Point X="3.597742197442" Y="0.085151533107" Z="0.3" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.3" />
                  <Point X="3.648859135891" Y="0.158949180863" Z="0.3" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.3" />
                  <Point X="3.736980068327" Y="0.214468259287" Z="0.3" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.3" />
                  <Point X="3.902153412466" Y="0.262128591449" Z="0.3" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.3" />
                  <Point X="4.989929564077" Y="0.942235324539" Z="0.3" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.3" />
                  <Point X="4.892803934557" Y="1.359294895286" Z="0.3" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.3" />
                  <Point X="4.712635994749" Y="1.386525849076" Z="0.3" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.3" />
                  <Point X="3.380129053303" Y="1.232992534642" Z="0.3" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.3" />
                  <Point X="3.307578814153" Y="1.269021218871" Z="0.3" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.3" />
                  <Point X="3.256961114038" Y="1.33805247489" Z="0.3" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.3" />
                  <Point X="3.235687753231" Y="1.422192278104" Z="0.3" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.3" />
                  <Point X="3.252563024744" Y="1.50018492521" Z="0.3" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.3" />
                  <Point X="3.306044601717" Y="1.575754095203" Z="0.3" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.3" />
                  <Point X="3.447451240243" Y="1.687941307763" Z="0.3" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.3" />
                  <Point X="4.262988685564" Y="2.759757349562" Z="0.3" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.3" />
                  <Point X="4.030244022852" Y="3.089736756195" Z="0.3" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.3" />
                  <Point X="3.825249230333" Y="3.026428704552" Z="0.3" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.3" />
                  <Point X="2.439114428662" Y="2.248075688346" Z="0.3" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.3" />
                  <Point X="2.368401288174" Y="2.252907599765" Z="0.3" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.3" />
                  <Point X="2.304367134131" Y="2.291762867725" Z="0.3" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.3" />
                  <Point X="2.25899573251" Y="2.352657726251" Z="0.3" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.3" />
                  <Point X="2.246522102994" Y="2.421357155659" Z="0.3" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.3" />
                  <Point X="2.264452254312" Y="2.50035514722" Z="0.3" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.3" />
                  <Point X="2.369196648308" Y="2.686889851622" Z="0.3" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.3" />
                  <Point X="2.797991948819" Y="4.23739024871" Z="0.3" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.3" />
                  <Point X="2.402938177215" Y="4.473268872164" Z="0.3" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.3" />
                  <Point X="1.992866807573" Y="4.670467683775" Z="0.3" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.3" />
                  <Point X="1.567418982799" Y="4.829906493772" Z="0.3" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.3" />
                  <Point X="0.940150979976" Y="4.98749479282" Z="0.3" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.3" />
                  <Point X="0.272632066406" Y="5.068009040363" Z="0.3" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.3" />
                  <Point X="0.170323778116" Y="4.990781499678" Z="0.3" />
                  <Point X="0" Y="4.355124473572" Z="0.3" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>