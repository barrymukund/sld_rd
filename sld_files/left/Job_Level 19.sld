<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#153" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1476" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004716064453" />
                  <Width Value="9.78389453125" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.294556640625" Y="-4.042992919922" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140136719" />
                  <Point X="-24.457634765625" Y="-3.467377197266" />
                  <Point X="-24.51573046875" Y="-3.383672363281" />
                  <Point X="-24.62136328125" Y="-3.231476806641" />
                  <Point X="-24.643248046875" Y="-3.209020751953" />
                  <Point X="-24.66950390625" Y="-3.189776855469" />
                  <Point X="-24.69750390625" Y="-3.175669433594" />
                  <Point X="-24.78740234375" Y="-3.147767822266" />
                  <Point X="-24.95086328125" Y="-3.097036132812" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097036132812" />
                  <Point X="-25.12672265625" Y="-3.1249375" />
                  <Point X="-25.29018359375" Y="-3.175669433594" />
                  <Point X="-25.31818359375" Y="-3.189776855469" />
                  <Point X="-25.344439453125" Y="-3.209020751953" />
                  <Point X="-25.36632421875" Y="-3.231477783203" />
                  <Point X="-25.42441796875" Y="-3.315182373047" />
                  <Point X="-25.53005078125" Y="-3.467378173828" />
                  <Point X="-25.5381875" Y="-3.481572021484" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.80943359375" Y="-4.477045898438" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.0793359375" Y="-4.845351074219" />
                  <Point X="-26.180037109375" Y="-4.819441894531" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.232072265625" Y="-4.693402832031" />
                  <Point X="-26.2149609375" Y="-4.563438476562" />
                  <Point X="-26.21419921875" Y="-4.5479296875" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.237986328125" Y="-4.408250976562" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.182965332031" />
                  <Point X="-26.30401171875" Y="-4.155127929687" />
                  <Point X="-26.32364453125" Y="-4.131204589844" />
                  <Point X="-26.406412109375" Y="-4.058618408203" />
                  <Point X="-26.556904296875" Y="-3.926639648438" />
                  <Point X="-26.5831875" Y="-3.910295898438" />
                  <Point X="-26.612884765625" Y="-3.897994628906" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.75287890625" Y="-3.883766113281" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708496094" />
                  <Point X="-27.014466796875" Y="-3.882028076172" />
                  <Point X="-27.042658203125" Y="-3.894801757812" />
                  <Point X="-27.13419140625" Y="-3.955963134766" />
                  <Point X="-27.300623046875" Y="-4.067169921875" />
                  <Point X="-27.31278515625" Y="-4.076821533203" />
                  <Point X="-27.3351015625" Y="-4.099461425781" />
                  <Point X="-27.4801484375" Y="-4.288489257813" />
                  <Point X="-27.524525390625" Y="-4.26101171875" />
                  <Point X="-27.801716796875" Y="-4.089382080078" />
                  <Point X="-27.941142578125" Y="-3.982027587891" />
                  <Point X="-28.10472265625" Y="-3.856077392578" />
                  <Point X="-27.68791015625" Y="-3.134137939453" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647654052734" />
                  <Point X="-27.406587890625" Y="-2.616126708984" />
                  <Point X="-27.405576171875" Y="-2.585190429688" />
                  <Point X="-27.414560546875" Y="-2.555570800781" />
                  <Point X="-27.428779296875" Y="-2.526741455078" />
                  <Point X="-27.446802734375" Y="-2.501590087891" />
                  <Point X="-27.464146484375" Y="-2.484245117188" />
                  <Point X="-27.489302734375" Y="-2.466216796875" />
                  <Point X="-27.5181328125" Y="-2.451997802734" />
                  <Point X="-27.547751953125" Y="-2.443011962891" />
                  <Point X="-27.5786875" Y="-2.444024169922" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.4710625" Y="-2.941483886719" />
                  <Point X="-28.8180234375" Y="-3.141801513672" />
                  <Point X="-28.86387109375" Y="-3.081567871094" />
                  <Point X="-29.082857421875" Y="-2.79386328125" />
                  <Point X="-29.182822265625" Y="-2.626239501953" />
                  <Point X="-29.306142578125" Y="-2.419450195312" />
                  <Point X="-28.56812890625" Y="-1.853151977539" />
                  <Point X="-28.105955078125" Y="-1.498513305664" />
                  <Point X="-28.084578125" Y="-1.475593139648" />
                  <Point X="-28.06661328125" Y="-1.448461669922" />
                  <Point X="-28.053857421875" Y="-1.419833984375" />
                  <Point X="-28.04615234375" Y="-1.390086669922" />
                  <Point X="-28.04334765625" Y="-1.359656005859" />
                  <Point X="-28.045556640625" Y="-1.327985229492" />
                  <Point X="-28.052556640625" Y="-1.298241088867" />
                  <Point X="-28.068638671875" Y="-1.272258056641" />
                  <Point X="-28.089470703125" Y="-1.248301513672" />
                  <Point X="-28.11297265625" Y="-1.228766723633" />
                  <Point X="-28.139455078125" Y="-1.213180297852" />
                  <Point X="-28.168716796875" Y="-1.201956176758" />
                  <Point X="-28.200603515625" Y="-1.195474731445" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-29.28209765625" Y="-1.332641357422" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.748427734375" Y="-1.327965698242" />
                  <Point X="-29.834078125" Y="-0.992649475098" />
                  <Point X="-29.8605234375" Y="-0.807738769531" />
                  <Point X="-29.892421875" Y="-0.584698242188" />
                  <Point X="-29.059361328125" Y="-0.361479736328" />
                  <Point X="-28.532875" Y="-0.220408309937" />
                  <Point X="-28.51148828125" Y="-0.211779632568" />
                  <Point X="-28.48607421875" Y="-0.197725219727" />
                  <Point X="-28.4778828125" Y="-0.19263659668" />
                  <Point X="-28.4599765625" Y="-0.180209365845" />
                  <Point X="-28.436017578125" Y="-0.158677139282" />
                  <Point X="-28.41482421875" Y="-0.127646934509" />
                  <Point X="-28.4041171875" Y="-0.10232951355" />
                  <Point X="-28.400884765625" Y="-0.093486351013" />
                  <Point X="-28.39491796875" Y="-0.074261795044" />
                  <Point X="-28.389474609375" Y="-0.045532279968" />
                  <Point X="-28.39057421875" Y="-0.011076621056" />
                  <Point X="-28.396103515625" Y="0.014324285507" />
                  <Point X="-28.39819921875" Y="0.022278406143" />
                  <Point X="-28.40416796875" Y="0.041509033203" />
                  <Point X="-28.417484375" Y="0.070833267212" />
                  <Point X="-28.440921875" Y="0.100575378418" />
                  <Point X="-28.462587890625" Y="0.118927749634" />
                  <Point X="-28.46982421875" Y="0.124484031677" />
                  <Point X="-28.48773046875" Y="0.136911270142" />
                  <Point X="-28.50192578125" Y="0.14504737854" />
                  <Point X="-28.532875" Y="0.157848327637" />
                  <Point X="-29.490154296875" Y="0.414350860596" />
                  <Point X="-29.891814453125" Y="0.521975402832" />
                  <Point X="-29.879685546875" Y="0.603946166992" />
                  <Point X="-29.82448828125" Y="0.976968688965" />
                  <Point X="-29.771248046875" Y="1.173440673828" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-29.1430625" Y="1.349478393555" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228027344" />
                  <Point X="-28.703134765625" Y="1.305263916016" />
                  <Point X="-28.68133984375" Y="1.312136230469" />
                  <Point X="-28.641708984375" Y="1.324631469727" />
                  <Point X="-28.622775390625" Y="1.332962768555" />
                  <Point X="-28.60403125" Y="1.34378527832" />
                  <Point X="-28.5873515625" Y="1.356014892578" />
                  <Point X="-28.57371484375" Y="1.371564208984" />
                  <Point X="-28.561298828125" Y="1.389294799805" />
                  <Point X="-28.551349609375" Y="1.407430908203" />
                  <Point X="-28.542603515625" Y="1.428544799805" />
                  <Point X="-28.526703125" Y="1.466935180664" />
                  <Point X="-28.520912109375" Y="1.486805541992" />
                  <Point X="-28.51715625" Y="1.508120239258" />
                  <Point X="-28.515806640625" Y="1.528755126953" />
                  <Point X="-28.518951171875" Y="1.549193481445" />
                  <Point X="-28.524552734375" Y="1.570099243164" />
                  <Point X="-28.53205078125" Y="1.589378662109" />
                  <Point X="-28.542603515625" Y="1.609649902344" />
                  <Point X="-28.561791015625" Y="1.646508056641" />
                  <Point X="-28.57328125" Y="1.663705810547" />
                  <Point X="-28.587193359375" Y="1.680285888672" />
                  <Point X="-28.60213671875" Y="1.694590087891" />
                  <Point X="-29.151234375" Y="2.115928222656" />
                  <Point X="-29.351859375" Y="2.269873779297" />
                  <Point X="-29.295634765625" Y="2.366201660156" />
                  <Point X="-29.0811484375" Y="2.733664794922" />
                  <Point X="-28.94012890625" Y="2.914926025391" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.438443359375" Y="2.978493408203" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.1164375" Y="2.823140625" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.04056640625" Y="2.818762939453" />
                  <Point X="-28.019107421875" Y="2.821587890625" />
                  <Point X="-27.999015625" Y="2.826504394531" />
                  <Point X="-27.980462890625" Y="2.835653320313" />
                  <Point X="-27.962208984375" Y="2.847282470703" />
                  <Point X="-27.946078125" Y="2.860228759766" />
                  <Point X="-27.92453125" Y="2.881775390625" />
                  <Point X="-27.885353515625" Y="2.920952148438" />
                  <Point X="-27.872404296875" Y="2.937084960938" />
                  <Point X="-27.860775390625" Y="2.955339111328" />
                  <Point X="-27.85162890625" Y="2.973888183594" />
                  <Point X="-27.8467109375" Y="2.993977294922" />
                  <Point X="-27.843884765625" Y="3.015436035156" />
                  <Point X="-27.84343359375" Y="3.036121826172" />
                  <Point X="-27.84608984375" Y="3.066477539062" />
                  <Point X="-27.85091796875" Y="3.121671386719" />
                  <Point X="-27.854955078125" Y="3.141961669922" />
                  <Point X="-27.86146484375" Y="3.162604492188" />
                  <Point X="-27.869794921875" Y="3.181532958984" />
                  <Point X="-28.1131171875" Y="3.602979003906" />
                  <Point X="-28.18333203125" Y="3.724595947266" />
                  <Point X="-28.0741796875" Y="3.808282958984" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.47851953125" Y="4.218081054688" />
                  <Point X="-27.16703515625" Y="4.391134277344" />
                  <Point X="-27.11415234375" Y="4.322214355469" />
                  <Point X="-27.0431953125" Y="4.229741210938" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.99511328125" Y="4.189395019531" />
                  <Point X="-26.961328125" Y="4.171807128906" />
                  <Point X="-26.899896484375" Y="4.139828125" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.777451171875" Y="4.134482421875" />
                  <Point X="-26.74226171875" Y="4.149059082031" />
                  <Point X="-26.67827734375" Y="4.175562011719" />
                  <Point X="-26.660146484375" Y="4.185508789062" />
                  <Point X="-26.642416015625" Y="4.197922851562" />
                  <Point X="-26.626859375" Y="4.211563964844" />
                  <Point X="-26.614626953125" Y="4.228249511719" />
                  <Point X="-26.603806640625" Y="4.246993652344" />
                  <Point X="-26.595478515625" Y="4.265922851563" />
                  <Point X="-26.584025390625" Y="4.302249511719" />
                  <Point X="-26.56319921875" Y="4.368299316406" />
                  <Point X="-26.5591640625" Y="4.388583496094" />
                  <Point X="-26.55727734375" Y="4.410145019531" />
                  <Point X="-26.557728515625" Y="4.430826660156" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.43322265625" Y="4.674227050781" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.680380859375" Y="4.8413203125" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.20247265625" Y="4.542219238281" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183885742188" />
                  <Point X="-24.932869140625" Y="4.194217773438" />
                  <Point X="-24.905576171875" Y="4.208806640625" />
                  <Point X="-24.88441796875" Y="4.23139453125" />
                  <Point X="-24.86655859375" Y="4.25812109375" />
                  <Point X="-24.853783203125" Y="4.286314941406" />
                  <Point X="-24.729107421875" Y="4.751610351562" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.57820703125" Y="4.875959472656" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.93319140625" Y="4.777956054688" />
                  <Point X="-23.51897265625" Y="4.677950195312" />
                  <Point X="-23.3749296875" Y="4.625706054688" />
                  <Point X="-23.105359375" Y="4.527930664062" />
                  <Point X="-22.965142578125" Y="4.462355957031" />
                  <Point X="-22.705421875" Y="4.340893066406" />
                  <Point X="-22.569943359375" Y="4.261963378906" />
                  <Point X="-22.319017578125" Y="4.1157734375" />
                  <Point X="-22.191271484375" Y="4.024926757812" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.54668359375" Y="3.080641357422" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857921875" Y="2.53993359375" />
                  <Point X="-22.866919921875" Y="2.516059814453" />
                  <Point X="-22.8745390625" Y="2.487571777344" />
                  <Point X="-22.888390625" Y="2.4357734375" />
                  <Point X="-22.891380859375" Y="2.417935791016" />
                  <Point X="-22.892271484375" Y="2.380952392578" />
                  <Point X="-22.88930078125" Y="2.356318359375" />
                  <Point X="-22.883900390625" Y="2.311527587891" />
                  <Point X="-22.87855859375" Y="2.289607666016" />
                  <Point X="-22.87029296875" Y="2.267518554688" />
                  <Point X="-22.859927734375" Y="2.247468017578" />
                  <Point X="-22.84468359375" Y="2.225004150391" />
                  <Point X="-22.81696875" Y="2.184159423828" />
                  <Point X="-22.805533203125" Y="2.170328125" />
                  <Point X="-22.778400390625" Y="2.145593261719" />
                  <Point X="-22.7559375" Y="2.130350585938" />
                  <Point X="-22.715091796875" Y="2.102635742188" />
                  <Point X="-22.695044921875" Y="2.092271728516" />
                  <Point X="-22.67295703125" Y="2.084006103516" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.62640234375" Y="2.075693115234" />
                  <Point X="-22.581611328125" Y="2.070291992188" />
                  <Point X="-22.563533203125" Y="2.069845703125" />
                  <Point X="-22.526794921875" Y="2.074171142578" />
                  <Point X="-22.498306640625" Y="2.081789306641" />
                  <Point X="-22.4465078125" Y="2.095640869141" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.448625" Y="2.666040771484" />
                  <Point X="-21.032671875" Y="2.90619140625" />
                  <Point X="-21.02556640625" Y="2.896315673828" />
                  <Point X="-20.87673046875" Y="2.689468017578" />
                  <Point X="-20.8055078125" Y="2.571772705078" />
                  <Point X="-20.73780078125" Y="2.459883789063" />
                  <Point X="-21.366689453125" Y="1.977319458008" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.7785703125" Y="1.660245117188" />
                  <Point X="-21.79602734375" Y="1.641626220703" />
                  <Point X="-21.816529296875" Y="1.614878662109" />
                  <Point X="-21.85380859375" Y="1.566244873047" />
                  <Point X="-21.86339453125" Y="1.550908813477" />
                  <Point X="-21.8783671875" Y="1.517090087891" />
                  <Point X="-21.886005859375" Y="1.489780395508" />
                  <Point X="-21.899892578125" Y="1.440125488281" />
                  <Point X="-21.90334765625" Y="1.41782409668" />
                  <Point X="-21.9041640625" Y="1.394253173828" />
                  <Point X="-21.90226171875" Y="1.371767211914" />
                  <Point X="-21.8959921875" Y="1.341381958008" />
                  <Point X="-21.884591796875" Y="1.286134399414" />
                  <Point X="-21.8793203125" Y="1.268977294922" />
                  <Point X="-21.863716796875" Y="1.235740600586" />
                  <Point X="-21.8466640625" Y="1.209821533203" />
                  <Point X="-21.81566015625" Y="1.16269519043" />
                  <Point X="-21.801107421875" Y="1.145451416016" />
                  <Point X="-21.78386328125" Y="1.129361083984" />
                  <Point X="-21.765650390625" Y="1.116033813477" />
                  <Point X="-21.7409375" Y="1.102123535156" />
                  <Point X="-21.6960078125" Y="1.076831298828" />
                  <Point X="-21.679478515625" Y="1.069501708984" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.61046875" Y="1.055023071289" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.597162109375" Y="1.16739831543" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.217990234375" Y="1.195397827148" />
                  <Point X="-20.15405859375" Y="0.932788757324" />
                  <Point X="-20.131619140625" Y="0.788661376953" />
                  <Point X="-20.109134765625" Y="0.644238891602" />
                  <Point X="-20.8221171875" Y="0.453195373535" />
                  <Point X="-21.283419921875" Y="0.329589874268" />
                  <Point X="-21.295208984375" Y="0.325586120605" />
                  <Point X="-21.318455078125" Y="0.315067504883" />
                  <Point X="-21.351279296875" Y="0.296093841553" />
                  <Point X="-21.41096484375" Y="0.261595062256" />
                  <Point X="-21.4256875" Y="0.251096801758" />
                  <Point X="-21.45246875" Y="0.225576583862" />
                  <Point X="-21.4721640625" Y="0.200480163574" />
                  <Point X="-21.507974609375" Y="0.154848937988" />
                  <Point X="-21.51969921875" Y="0.135568222046" />
                  <Point X="-21.52947265625" Y="0.114104911804" />
                  <Point X="-21.536318359375" Y="0.092602737427" />
                  <Point X="-21.5428828125" Y="0.058322551727" />
                  <Point X="-21.5548203125" Y="-0.004007742882" />
                  <Point X="-21.556515625" Y="-0.021874988556" />
                  <Point X="-21.5548203125" Y="-0.058552253723" />
                  <Point X="-21.548255859375" Y="-0.092832748413" />
                  <Point X="-21.536318359375" Y="-0.155162734985" />
                  <Point X="-21.52947265625" Y="-0.176664901733" />
                  <Point X="-21.51969921875" Y="-0.198128219604" />
                  <Point X="-21.507974609375" Y="-0.217409240723" />
                  <Point X="-21.488279296875" Y="-0.242505508423" />
                  <Point X="-21.45246875" Y="-0.288136871338" />
                  <Point X="-21.439998046875" Y="-0.301237457275" />
                  <Point X="-21.410962890625" Y="-0.324155822754" />
                  <Point X="-21.37813671875" Y="-0.343129486084" />
                  <Point X="-21.318453125" Y="-0.377628234863" />
                  <Point X="-21.307291015625" Y="-0.383137786865" />
                  <Point X="-21.283419921875" Y="-0.392149871826" />
                  <Point X="-20.44466015625" Y="-0.616895141602" />
                  <Point X="-20.10852734375" Y="-0.706961791992" />
                  <Point X="-20.109283203125" Y="-0.711980102539" />
                  <Point X="-20.144978515625" Y="-0.948747192383" />
                  <Point X="-20.173728515625" Y="-1.074726806641" />
                  <Point X="-20.19882421875" Y="-1.18469921875" />
                  <Point X="-21.04063671875" Y="-1.073872436523" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710144043" />
                  <Point X="-21.62533984375" Y="-1.005508728027" />
                  <Point X="-21.689765625" Y="-1.01951184082" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.8360234375" Y="-1.056596313477" />
                  <Point X="-21.8638515625" Y="-1.073489013672" />
                  <Point X="-21.8876015625" Y="-1.093959716797" />
                  <Point X="-21.92654296875" Y="-1.14079309082" />
                  <Point X="-21.997345703125" Y="-1.225947631836" />
                  <Point X="-22.012068359375" Y="-1.250335449219" />
                  <Point X="-22.02341015625" Y="-1.277720947266" />
                  <Point X="-22.030240234375" Y="-1.305364624023" />
                  <Point X="-22.035822265625" Y="-1.366016113281" />
                  <Point X="-22.04596875" Y="-1.476294921875" />
                  <Point X="-22.04365234375" Y="-1.507562011719" />
                  <Point X="-22.035921875" Y="-1.539182983398" />
                  <Point X="-22.023548828125" Y="-1.567995727539" />
                  <Point X="-21.987896484375" Y="-1.623452758789" />
                  <Point X="-21.923068359375" Y="-1.724286254883" />
                  <Point X="-21.9130625" Y="-1.737243408203" />
                  <Point X="-21.889369140625" Y="-1.760909423828" />
                  <Point X="-21.11099609375" Y="-2.358177246094" />
                  <Point X="-20.786875" Y="-2.6068828125" />
                  <Point X="-20.875177734375" Y="-2.749768310547" />
                  <Point X="-20.934654296875" Y="-2.83427734375" />
                  <Point X="-20.971017578125" Y="-2.885945068359" />
                  <Point X="-21.722572265625" Y="-2.452034912109" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.245775390625" Y="-2.159825195312" />
                  <Point X="-22.322451171875" Y="-2.145977783203" />
                  <Point X="-22.461865234375" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135176513672" />
                  <Point X="-22.618865234375" Y="-2.168700439453" />
                  <Point X="-22.73468359375" Y="-2.229655517578" />
                  <Point X="-22.757615234375" Y="-2.246549804688" />
                  <Point X="-22.77857421875" Y="-2.267509521484" />
                  <Point X="-22.79546484375" Y="-2.290437744141" />
                  <Point X="-22.828990234375" Y="-2.354136230469" />
                  <Point X="-22.8899453125" Y="-2.469955810547" />
                  <Point X="-22.899771484375" Y="-2.499735351563" />
                  <Point X="-22.904728515625" Y="-2.531908691406" />
                  <Point X="-22.90432421875" Y="-2.563259033203" />
                  <Point X="-22.8904765625" Y="-2.639934570312" />
                  <Point X="-22.865296875" Y="-2.779349365234" />
                  <Point X="-22.86101171875" Y="-2.795140869141" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.347994140625" Y="-3.692422607422" />
                  <Point X="-22.13871484375" Y="-4.054904785156" />
                  <Point X="-22.21815234375" Y="-4.111645996094" />
                  <Point X="-22.2846328125" Y="-4.154677734375" />
                  <Point X="-22.298232421875" Y="-4.163480957031" />
                  <Point X="-22.877859375" Y="-3.408097900391" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.353697265625" Y="-2.851938720703" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.66560546875" Y="-2.748727539062" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-23.959265625" Y="-2.848561767578" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968860839844" />
                  <Point X="-24.11275" Y="-2.996687255859" />
                  <Point X="-24.124375" Y="-3.025809570312" />
                  <Point X="-24.14346875" Y="-3.113661376953" />
                  <Point X="-24.178189453125" Y="-3.273397460938" />
                  <Point X="-24.180275390625" Y="-3.289627197266" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.0385078125" Y="-4.399805175781" />
                  <Point X="-23.97793359375" Y="-4.859915527344" />
                  <Point X="-24.02430859375" Y="-4.870080566406" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.058427734375" Y="-4.752636230469" />
                  <Point X="-26.14124609375" Y="-4.731328613281" />
                  <Point X="-26.137884765625" Y="-4.705801757812" />
                  <Point X="-26.1207734375" Y="-4.575837402344" />
                  <Point X="-26.120076171875" Y="-4.568098632812" />
                  <Point X="-26.11944921875" Y="-4.541030761719" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497688476562" />
                  <Point X="-26.1448125" Y="-4.389716308594" />
                  <Point X="-26.18386328125" Y="-4.193396972656" />
                  <Point X="-26.188126953125" Y="-4.178467285156" />
                  <Point X="-26.199029296875" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135464355469" />
                  <Point X="-26.221740234375" Y="-4.107626953125" />
                  <Point X="-26.23057421875" Y="-4.094861572266" />
                  <Point X="-26.25020703125" Y="-4.070938232422" />
                  <Point X="-26.261005859375" Y="-4.059780273438" />
                  <Point X="-26.3437734375" Y="-3.987194091797" />
                  <Point X="-26.494265625" Y="-3.855215332031" />
                  <Point X="-26.50673828125" Y="-3.845965087891" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822527587891" />
                  <Point X="-26.576529296875" Y="-3.810226318359" />
                  <Point X="-26.5913125" Y="-3.805476318359" />
                  <Point X="-26.621455078125" Y="-3.798447998047" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.746666015625" Y="-3.788969482422" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166015625" />
                  <Point X="-27.008005859375" Y="-3.781945556641" />
                  <Point X="-27.0390546875" Y="-3.790265136719" />
                  <Point X="-27.053673828125" Y="-3.795496337891" />
                  <Point X="-27.081865234375" Y="-3.808270019531" />
                  <Point X="-27.0954375" Y="-3.8158125" />
                  <Point X="-27.186970703125" Y="-3.876973876953" />
                  <Point X="-27.35340234375" Y="-3.988180664062" />
                  <Point X="-27.359677734375" Y="-3.992754882812" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032771484375" />
                  <Point X="-27.410470703125" Y="-4.04162890625" />
                  <Point X="-27.503203125" Y="-4.162478515625" />
                  <Point X="-27.747595703125" Y="-4.011156005859" />
                  <Point X="-27.883185546875" Y="-3.906755371094" />
                  <Point X="-27.98086328125" Y="-3.831546630859" />
                  <Point X="-27.605638671875" Y="-3.181637939453" />
                  <Point X="-27.341490234375" Y="-2.724119628906" />
                  <Point X="-27.3348515625" Y="-2.710084716797" />
                  <Point X="-27.32394921875" Y="-2.681119140625" />
                  <Point X="-27.319685546875" Y="-2.666188476562" />
                  <Point X="-27.3134140625" Y="-2.634661132812" />
                  <Point X="-27.311638671875" Y="-2.619231933594" />
                  <Point X="-27.310626953125" Y="-2.588295654297" />
                  <Point X="-27.314666015625" Y="-2.557615234375" />
                  <Point X="-27.323650390625" Y="-2.527995605469" />
                  <Point X="-27.329359375" Y="-2.513549316406" />
                  <Point X="-27.343578125" Y="-2.484719970703" />
                  <Point X="-27.35155859375" Y="-2.471405517578" />
                  <Point X="-27.36958203125" Y="-2.446254150391" />
                  <Point X="-27.379625" Y="-2.434417236328" />
                  <Point X="-27.39696875" Y="-2.417072265625" />
                  <Point X="-27.40880859375" Y="-2.407027099609" />
                  <Point X="-27.43396484375" Y="-2.388998779297" />
                  <Point X="-27.44728125" Y="-2.381015625" />
                  <Point X="-27.476111328125" Y="-2.366796630859" />
                  <Point X="-27.490552734375" Y="-2.361089355469" />
                  <Point X="-27.520171875" Y="-2.352103515625" />
                  <Point X="-27.550859375" Y="-2.348062744141" />
                  <Point X="-27.581794921875" Y="-2.349074951172" />
                  <Point X="-27.597220703125" Y="-2.350849365234" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.5185625" Y="-2.859211425781" />
                  <Point X="-28.793087890625" Y="-3.017708740234" />
                  <Point X="-29.00401953125" Y="-2.740587402344" />
                  <Point X="-29.10123046875" Y="-2.577580810547" />
                  <Point X="-29.181265625" Y="-2.443373535156" />
                  <Point X="-28.510296875" Y="-1.928520629883" />
                  <Point X="-28.048123046875" Y="-1.573881835938" />
                  <Point X="-28.036482421875" Y="-1.563308959961" />
                  <Point X="-28.01510546875" Y="-1.540388793945" />
                  <Point X="-28.005369140625" Y="-1.528041137695" />
                  <Point X="-27.987404296875" Y="-1.500909667969" />
                  <Point X="-27.979837890625" Y="-1.487126953125" />
                  <Point X="-27.96708203125" Y="-1.458499145508" />
                  <Point X="-27.961892578125" Y="-1.443654663086" />
                  <Point X="-27.9541875" Y="-1.413907348633" />
                  <Point X="-27.951552734375" Y="-1.398805541992" />
                  <Point X="-27.948748046875" Y="-1.36837487793" />
                  <Point X="-27.948578125" Y="-1.353046142578" />
                  <Point X="-27.950787109375" Y="-1.321375366211" />
                  <Point X="-27.95308203125" Y="-1.306222412109" />
                  <Point X="-27.96008203125" Y="-1.276478271484" />
                  <Point X="-27.97177734375" Y="-1.248243530273" />
                  <Point X="-27.987859375" Y="-1.222260498047" />
                  <Point X="-27.996951171875" Y="-1.209920654297" />
                  <Point X="-28.017783203125" Y="-1.185964111328" />
                  <Point X="-28.02874609375" Y="-1.175243774414" />
                  <Point X="-28.052248046875" Y="-1.155709106445" />
                  <Point X="-28.064787109375" Y="-1.14689440918" />
                  <Point X="-28.09126953125" Y="-1.131308105469" />
                  <Point X="-28.105431640625" Y="-1.124481689453" />
                  <Point X="-28.134693359375" Y="-1.113257568359" />
                  <Point X="-28.14979296875" Y="-1.108859741211" />
                  <Point X="-28.1816796875" Y="-1.102378417969" />
                  <Point X="-28.197296875" Y="-1.100532348633" />
                  <Point X="-28.22862109375" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-29.294498046875" Y="-1.238454223633" />
                  <Point X="-29.660919921875" Y="-1.286694458008" />
                  <Point X="-29.740763671875" Y="-0.974107727051" />
                  <Point X="-29.76648046875" Y="-0.794289001465" />
                  <Point X="-29.786451171875" Y="-0.654654418945" />
                  <Point X="-29.0347734375" Y="-0.453242675781" />
                  <Point X="-28.508287109375" Y="-0.312171203613" />
                  <Point X="-28.497330078125" Y="-0.308508117676" />
                  <Point X="-28.475943359375" Y="-0.299879364014" />
                  <Point X="-28.465513671875" Y="-0.294914031982" />
                  <Point X="-28.440099609375" Y="-0.280859649658" />
                  <Point X="-28.423716796875" Y="-0.270682403564" />
                  <Point X="-28.405810546875" Y="-0.258255157471" />
                  <Point X="-28.396474609375" Y="-0.250867553711" />
                  <Point X="-28.372515625" Y="-0.229335372925" />
                  <Point X="-28.357568359375" Y="-0.212256973267" />
                  <Point X="-28.336375" Y="-0.181226760864" />
                  <Point X="-28.327326171875" Y="-0.164650405884" />
                  <Point X="-28.316619140625" Y="-0.139333099365" />
                  <Point X="-28.310154296875" Y="-0.121646682739" />
                  <Point X="-28.3041875" Y="-0.102422172546" />
                  <Point X="-28.301578125" Y="-0.091946784973" />
                  <Point X="-28.296134765625" Y="-0.063217250824" />
                  <Point X="-28.2945234375" Y="-0.042502048492" />
                  <Point X="-28.295623046875" Y="-0.008046369553" />
                  <Point X="-28.297748046875" Y="0.009129823685" />
                  <Point X="-28.30327734375" Y="0.03453080368" />
                  <Point X="-28.30746875" Y="0.050438949585" />
                  <Point X="-28.3134375" Y="0.069669700623" />
                  <Point X="-28.317669921875" Y="0.080789070129" />
                  <Point X="-28.330986328125" Y="0.110113250732" />
                  <Point X="-28.3428671875" Y="0.129632781982" />
                  <Point X="-28.3663046875" Y="0.15937487793" />
                  <Point X="-28.37951953125" Y="0.173064682007" />
                  <Point X="-28.401185546875" Y="0.19141708374" />
                  <Point X="-28.415658203125" Y="0.202529754639" />
                  <Point X="-28.433564453125" Y="0.214957000732" />
                  <Point X="-28.440490234375" Y="0.219333053589" />
                  <Point X="-28.465615234375" Y="0.23283454895" />
                  <Point X="-28.496564453125" Y="0.2456355896" />
                  <Point X="-28.508287109375" Y="0.249611236572" />
                  <Point X="-29.46556640625" Y="0.506113769531" />
                  <Point X="-29.7854453125" Y="0.591825073242" />
                  <Point X="-29.73133203125" Y="0.957520019531" />
                  <Point X="-29.6795546875" Y="1.148593505859" />
                  <Point X="-29.6335859375" Y="1.318237060547" />
                  <Point X="-29.155462890625" Y="1.255291137695" />
                  <Point X="-28.778064453125" Y="1.20560559082" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.20470324707" />
                  <Point X="-28.71514453125" Y="1.206589477539" />
                  <Point X="-28.704888671875" Y="1.208053710938" />
                  <Point X="-28.684599609375" Y="1.212089477539" />
                  <Point X="-28.67456640625" Y="1.214661254883" />
                  <Point X="-28.652771484375" Y="1.221533569336" />
                  <Point X="-28.613140625" Y="1.234028808594" />
                  <Point X="-28.603447265625" Y="1.237677246094" />
                  <Point X="-28.584513671875" Y="1.246008666992" />
                  <Point X="-28.5752734375" Y="1.25069140625" />
                  <Point X="-28.556529296875" Y="1.261513916016" />
                  <Point X="-28.547857421875" Y="1.267172119141" />
                  <Point X="-28.531177734375" Y="1.279401733398" />
                  <Point X="-28.515927734375" Y="1.293376220703" />
                  <Point X="-28.502291015625" Y="1.30892565918" />
                  <Point X="-28.495896484375" Y="1.317071777344" />
                  <Point X="-28.48348046875" Y="1.334802368164" />
                  <Point X="-28.4780078125" Y="1.343602905273" />
                  <Point X="-28.46805859375" Y="1.361739013672" />
                  <Point X="-28.46358203125" Y="1.371074462891" />
                  <Point X="-28.4548359375" Y="1.392188232422" />
                  <Point X="-28.438935546875" Y="1.430578735352" />
                  <Point X="-28.435498046875" Y="1.440354248047" />
                  <Point X="-28.42970703125" Y="1.460224609375" />
                  <Point X="-28.427353515625" Y="1.470319580078" />
                  <Point X="-28.42359765625" Y="1.491634155273" />
                  <Point X="-28.422359375" Y="1.501920043945" />
                  <Point X="-28.421009765625" Y="1.522554931641" />
                  <Point X="-28.421912109375" Y="1.543201293945" />
                  <Point X="-28.425056640625" Y="1.563639648438" />
                  <Point X="-28.4271875" Y="1.573780761719" />
                  <Point X="-28.4327890625" Y="1.594686523438" />
                  <Point X="-28.436013671875" Y="1.604533569336" />
                  <Point X="-28.44351171875" Y="1.623812988281" />
                  <Point X="-28.44778515625" Y="1.633245361328" />
                  <Point X="-28.458337890625" Y="1.653516601562" />
                  <Point X="-28.477525390625" Y="1.690374755859" />
                  <Point X="-28.482798828125" Y="1.699284301758" />
                  <Point X="-28.4942890625" Y="1.716481933594" />
                  <Point X="-28.500505859375" Y="1.724769897461" />
                  <Point X="-28.51441796875" Y="1.741350219727" />
                  <Point X="-28.521501953125" Y="1.748912597656" />
                  <Point X="-28.5364453125" Y="1.763216796875" />
                  <Point X="-28.5443046875" Y="1.769958618164" />
                  <Point X="-29.09340234375" Y="2.191296630859" />
                  <Point X="-29.227615234375" Y="2.294281494141" />
                  <Point X="-29.213587890625" Y="2.318312744141" />
                  <Point X="-29.00228515625" Y="2.680320556641" />
                  <Point X="-28.8651484375" Y="2.856591796875" />
                  <Point X="-28.726337890625" Y="3.035013183594" />
                  <Point X="-28.485943359375" Y="2.896220947266" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.124716796875" Y="2.728502197266" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.038494140625" Y="2.723785644531" />
                  <Point X="-28.02816796875" Y="2.724575683594" />
                  <Point X="-28.006708984375" Y="2.727400634766" />
                  <Point X="-27.99652734375" Y="2.729310546875" />
                  <Point X="-27.976435546875" Y="2.734227050781" />
                  <Point X="-27.957" Y="2.741301025391" />
                  <Point X="-27.938447265625" Y="2.750449951172" />
                  <Point X="-27.929419921875" Y="2.755531494141" />
                  <Point X="-27.911166015625" Y="2.767160644531" />
                  <Point X="-27.90274609375" Y="2.773193115234" />
                  <Point X="-27.886615234375" Y="2.786139404297" />
                  <Point X="-27.878904296875" Y="2.793053222656" />
                  <Point X="-27.857357421875" Y="2.814599853516" />
                  <Point X="-27.8181796875" Y="2.853776611328" />
                  <Point X="-27.811267578125" Y="2.861485839844" />
                  <Point X="-27.798318359375" Y="2.877618652344" />
                  <Point X="-27.79228125" Y="2.886042236328" />
                  <Point X="-27.78065234375" Y="2.904296386719" />
                  <Point X="-27.7755703125" Y="2.913324951172" />
                  <Point X="-27.766423828125" Y="2.931874023438" />
                  <Point X="-27.759353515625" Y="2.951298583984" />
                  <Point X="-27.754435546875" Y="2.971387695312" />
                  <Point X="-27.7525234375" Y="2.981572753906" />
                  <Point X="-27.749697265625" Y="3.003031494141" />
                  <Point X="-27.748908203125" Y="3.013364501953" />
                  <Point X="-27.74845703125" Y="3.034050292969" />
                  <Point X="-27.748794921875" Y="3.044403076172" />
                  <Point X="-27.751451171875" Y="3.074758789062" />
                  <Point X="-27.756279296875" Y="3.129952636719" />
                  <Point X="-27.757744140625" Y="3.140209960938" />
                  <Point X="-27.76178125" Y="3.160500244141" />
                  <Point X="-27.764353515625" Y="3.170533203125" />
                  <Point X="-27.77086328125" Y="3.191176025391" />
                  <Point X="-27.77451171875" Y="3.200870605469" />
                  <Point X="-27.782841796875" Y="3.219799072266" />
                  <Point X="-27.7875234375" Y="3.229032958984" />
                  <Point X="-28.030845703125" Y="3.650479003906" />
                  <Point X="-28.05938671875" Y="3.699915527344" />
                  <Point X="-28.016376953125" Y="3.732891601562" />
                  <Point X="-27.6483671875" Y="4.015040527344" />
                  <Point X="-27.4323828125" Y="4.135037109375" />
                  <Point X="-27.192525390625" Y="4.268296386719" />
                  <Point X="-27.189521484375" Y="4.2643828125" />
                  <Point X="-27.118564453125" Y="4.171909667969" />
                  <Point X="-27.1118203125" Y="4.164048828125" />
                  <Point X="-27.097517578125" Y="4.149107421875" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.065091796875" Y="4.121897460938" />
                  <Point X="-27.047892578125" Y="4.110405273438" />
                  <Point X="-27.03898046875" Y="4.105129394531" />
                  <Point X="-27.0051953125" Y="4.087541503906" />
                  <Point X="-26.943763671875" Y="4.0555625" />
                  <Point X="-26.934326171875" Y="4.051286621094" />
                  <Point X="-26.915044921875" Y="4.0437890625" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031378173828" />
                  <Point X="-26.780818359375" Y="4.035136474609" />
                  <Point X="-26.770728515625" Y="4.037488525391" />
                  <Point X="-26.7508671875" Y="4.043277587891" />
                  <Point X="-26.741095703125" Y="4.046714599609" />
                  <Point X="-26.70590625" Y="4.061291259766" />
                  <Point X="-26.641921875" Y="4.087794189453" />
                  <Point X="-26.632583984375" Y="4.092272705078" />
                  <Point X="-26.614453125" Y="4.102219238281" />
                  <Point X="-26.60566015625" Y="4.1076875" />
                  <Point X="-26.5879296875" Y="4.1201015625" />
                  <Point X="-26.579783203125" Y="4.126494140625" />
                  <Point X="-26.5642265625" Y="4.140135253906" />
                  <Point X="-26.5502421875" Y="4.155395507812" />
                  <Point X="-26.538009765625" Y="4.172081054688" />
                  <Point X="-26.5323515625" Y="4.180754882812" />
                  <Point X="-26.52153125" Y="4.199499023438" />
                  <Point X="-26.516849609375" Y="4.208736328125" />
                  <Point X="-26.508521484375" Y="4.227665527344" />
                  <Point X="-26.504875" Y="4.237357421875" />
                  <Point X="-26.493421875" Y="4.273684082031" />
                  <Point X="-26.472595703125" Y="4.339733886719" />
                  <Point X="-26.470025390625" Y="4.349764160156" />
                  <Point X="-26.465990234375" Y="4.370048339844" />
                  <Point X="-26.464525390625" Y="4.380302246094" />
                  <Point X="-26.462638671875" Y="4.401863769531" />
                  <Point X="-26.46230078125" Y="4.412216796875" />
                  <Point X="-26.462751953125" Y="4.4328984375" />
                  <Point X="-26.463541015625" Y="4.443227050781" />
                  <Point X="-26.479263671875" Y="4.562654785156" />
                  <Point X="-26.407576171875" Y="4.58275390625" />
                  <Point X="-25.931173828125" Y="4.7163203125" />
                  <Point X="-25.669337890625" Y="4.746964355469" />
                  <Point X="-25.36522265625" Y="4.782556152344" />
                  <Point X="-25.294236328125" Y="4.517631347656" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.978369140625" Y="4.085113037109" />
                  <Point X="-24.94783203125" Y="4.090154541016" />
                  <Point X="-24.93276953125" Y="4.093927246094" />
                  <Point X="-24.90233203125" Y="4.104259277344" />
                  <Point X="-24.8880859375" Y="4.110436035156" />
                  <Point X="-24.86079296875" Y="4.125024902344" />
                  <Point X="-24.8362421875" Y="4.143861328125" />
                  <Point X="-24.815083984375" Y="4.16644921875" />
                  <Point X="-24.8054296875" Y="4.178612792969" />
                  <Point X="-24.7875703125" Y="4.205339355469" />
                  <Point X="-24.78002734375" Y="4.218911621094" />
                  <Point X="-24.767251953125" Y="4.24710546875" />
                  <Point X="-24.76201953125" Y="4.261727050781" />
                  <Point X="-24.63734375" Y="4.727022460938" />
                  <Point X="-24.621806640625" Y="4.785005859375" />
                  <Point X="-24.5881015625" Y="4.781476074219" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.955486328125" Y="4.685609375" />
                  <Point X="-23.546396484375" Y="4.586841796875" />
                  <Point X="-23.4073203125" Y="4.536398925781" />
                  <Point X="-23.14175" Y="4.440074707031" />
                  <Point X="-23.00538671875" Y="4.376301757813" />
                  <Point X="-22.749548828125" Y="4.256655273438" />
                  <Point X="-22.617765625" Y="4.179877929687" />
                  <Point X="-22.37056640625" Y="4.035859130859" />
                  <Point X="-22.246328125" Y="3.947507324219" />
                  <Point X="-22.18221875" Y="3.901916503906" />
                  <Point X="-22.62895703125" Y="3.128141357422" />
                  <Point X="-22.934689453125" Y="2.598597900391" />
                  <Point X="-22.937619140625" Y="2.593116699219" />
                  <Point X="-22.946818359375" Y="2.573438476562" />
                  <Point X="-22.95581640625" Y="2.549564697266" />
                  <Point X="-22.958693359375" Y="2.540604980469" />
                  <Point X="-22.9663125" Y="2.512116943359" />
                  <Point X="-22.9801640625" Y="2.460318603516" />
                  <Point X="-22.982083984375" Y="2.451479736328" />
                  <Point X="-22.986353515625" Y="2.420222900391" />
                  <Point X="-22.987244140625" Y="2.383239501953" />
                  <Point X="-22.986587890625" Y="2.369578369141" />
                  <Point X="-22.9836171875" Y="2.344944335938" />
                  <Point X="-22.978216796875" Y="2.300153564453" />
                  <Point X="-22.97619921875" Y="2.289034667969" />
                  <Point X="-22.970857421875" Y="2.267114746094" />
                  <Point X="-22.967533203125" Y="2.256313720703" />
                  <Point X="-22.959267578125" Y="2.234224609375" />
                  <Point X="-22.95468359375" Y="2.223892333984" />
                  <Point X="-22.944318359375" Y="2.203841796875" />
                  <Point X="-22.938537109375" Y="2.194123535156" />
                  <Point X="-22.92329296875" Y="2.171659667969" />
                  <Point X="-22.895578125" Y="2.130814941406" />
                  <Point X="-22.890185546875" Y="2.123625244141" />
                  <Point X="-22.86953515625" Y="2.100122314453" />
                  <Point X="-22.84240234375" Y="2.075387451172" />
                  <Point X="-22.8317421875" Y="2.066983154297" />
                  <Point X="-22.809279296875" Y="2.051740478516" />
                  <Point X="-22.76843359375" Y="2.024025512695" />
                  <Point X="-22.758720703125" Y="2.018246459961" />
                  <Point X="-22.738673828125" Y="2.007882324219" />
                  <Point X="-22.72833984375" Y="2.003297485352" />
                  <Point X="-22.706251953125" Y="1.995031860352" />
                  <Point X="-22.695451171875" Y="1.991707519531" />
                  <Point X="-22.673529296875" Y="1.986364990234" />
                  <Point X="-22.662408203125" Y="1.984346923828" />
                  <Point X="-22.637775390625" Y="1.981376464844" />
                  <Point X="-22.592984375" Y="1.975975341797" />
                  <Point X="-22.583955078125" Y="1.975320922852" />
                  <Point X="-22.55242578125" Y="1.975497436523" />
                  <Point X="-22.5156875" Y="1.979822998047" />
                  <Point X="-22.50225390625" Y="1.982395874023" />
                  <Point X="-22.473765625" Y="1.990014038086" />
                  <Point X="-22.421966796875" Y="2.003865722656" />
                  <Point X="-22.4160078125" Y="2.005670288086" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572875977" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.401125" Y="2.583768310547" />
                  <Point X="-21.05959375" Y="2.780951171875" />
                  <Point X="-20.956048828125" Y="2.637046875" />
                  <Point X="-20.88678515625" Y="2.522588623047" />
                  <Point X="-20.863115234375" Y="2.483471679688" />
                  <Point X="-21.424521484375" Y="2.052687988281" />
                  <Point X="-21.827046875" Y="1.743819946289" />
                  <Point X="-21.831859375" Y="1.739869384766" />
                  <Point X="-21.847873046875" Y="1.725223144531" />
                  <Point X="-21.865330078125" Y="1.706604248047" />
                  <Point X="-21.87142578125" Y="1.699419189453" />
                  <Point X="-21.891927734375" Y="1.67267175293" />
                  <Point X="-21.92920703125" Y="1.624037841797" />
                  <Point X="-21.9343671875" Y="1.616598022461" />
                  <Point X="-21.95026171875" Y="1.589367797852" />
                  <Point X="-21.965234375" Y="1.555549072266" />
                  <Point X="-21.96985546875" Y="1.542679931641" />
                  <Point X="-21.977494140625" Y="1.515370117188" />
                  <Point X="-21.991380859375" Y="1.465715332031" />
                  <Point X="-21.9937734375" Y="1.454670043945" />
                  <Point X="-21.997228515625" Y="1.432368652344" />
                  <Point X="-21.998291015625" Y="1.421112548828" />
                  <Point X="-21.999107421875" Y="1.397541625977" />
                  <Point X="-21.998826171875" Y="1.386244506836" />
                  <Point X="-21.996923828125" Y="1.363758666992" />
                  <Point X="-21.995302734375" Y="1.352569946289" />
                  <Point X="-21.989033203125" Y="1.322184570312" />
                  <Point X="-21.9776328125" Y="1.266937011719" />
                  <Point X="-21.97540234375" Y="1.258233154297" />
                  <Point X="-21.965314453125" Y="1.228605712891" />
                  <Point X="-21.9497109375" Y="1.195368896484" />
                  <Point X="-21.943080078125" Y="1.183525512695" />
                  <Point X="-21.92602734375" Y="1.157606445312" />
                  <Point X="-21.8950234375" Y="1.110480102539" />
                  <Point X="-21.88826171875" Y="1.101424316406" />
                  <Point X="-21.873708984375" Y="1.084180541992" />
                  <Point X="-21.86591796875" Y="1.075992797852" />
                  <Point X="-21.848673828125" Y="1.05990234375" />
                  <Point X="-21.839962890625" Y="1.052694824219" />
                  <Point X="-21.82175" Y="1.039367431641" />
                  <Point X="-21.812248046875" Y="1.033247436523" />
                  <Point X="-21.78753515625" Y="1.019337097168" />
                  <Point X="-21.74260546875" Y="0.994044921875" />
                  <Point X="-21.734517578125" Y="0.989986633301" />
                  <Point X="-21.7053203125" Y="0.97808416748" />
                  <Point X="-21.66972265625" Y="0.968021240234" />
                  <Point X="-21.656328125" Y="0.965257568359" />
                  <Point X="-21.622916015625" Y="0.96084197998" />
                  <Point X="-21.562166015625" Y="0.952812927246" />
                  <Point X="-21.555966796875" Y="0.952199707031" />
                  <Point X="-21.53428125" Y="0.951222839355" />
                  <Point X="-21.50878125" Y="0.952032348633" />
                  <Point X="-21.49939453125" Y="0.952797180176" />
                  <Point X="-20.58476171875" Y="1.07321105957" />
                  <Point X="-20.295296875" Y="1.111319824219" />
                  <Point X="-20.247310546875" Y="0.914213684082" />
                  <Point X="-20.22548828125" Y="0.774046691895" />
                  <Point X="-20.216126953125" Y="0.713921386719" />
                  <Point X="-20.846705078125" Y="0.544958251953" />
                  <Point X="-21.3080078125" Y="0.421352813721" />
                  <Point X="-21.31396875" Y="0.419543792725" />
                  <Point X="-21.334373046875" Y="0.412137908936" />
                  <Point X="-21.357619140625" Y="0.401619262695" />
                  <Point X="-21.365998046875" Y="0.397315460205" />
                  <Point X="-21.398822265625" Y="0.378341674805" />
                  <Point X="-21.4585078125" Y="0.343842895508" />
                  <Point X="-21.466119140625" Y="0.338944274902" />
                  <Point X="-21.491224609375" Y="0.319871643066" />
                  <Point X="-21.518005859375" Y="0.294351318359" />
                  <Point X="-21.527203125" Y="0.28422668457" />
                  <Point X="-21.5468984375" Y="0.259130249023" />
                  <Point X="-21.582708984375" Y="0.213499008179" />
                  <Point X="-21.58914453125" Y="0.204208602905" />
                  <Point X="-21.600869140625" Y="0.184927902222" />
                  <Point X="-21.606158203125" Y="0.174937484741" />
                  <Point X="-21.615931640625" Y="0.153474258423" />
                  <Point X="-21.61999609375" Y="0.142925018311" />
                  <Point X="-21.626841796875" Y="0.121422706604" />
                  <Point X="-21.629623046875" Y="0.110470092773" />
                  <Point X="-21.6361875" Y="0.076189788818" />
                  <Point X="-21.648125" Y="0.013859597206" />
                  <Point X="-21.649396484375" Y="0.004965857506" />
                  <Point X="-21.6514140625" Y="-0.026261436462" />
                  <Point X="-21.64971875" Y="-0.062938732147" />
                  <Point X="-21.648125" Y="-0.076419418335" />
                  <Point X="-21.641560546875" Y="-0.110699867249" />
                  <Point X="-21.629623046875" Y="-0.173029907227" />
                  <Point X="-21.626841796875" Y="-0.183982818604" />
                  <Point X="-21.61999609375" Y="-0.205484985352" />
                  <Point X="-21.615931640625" Y="-0.216034225464" />
                  <Point X="-21.606158203125" Y="-0.23749760437" />
                  <Point X="-21.600869140625" Y="-0.24748727417" />
                  <Point X="-21.58914453125" Y="-0.266768432617" />
                  <Point X="-21.582708984375" Y="-0.276059570312" />
                  <Point X="-21.563013671875" Y="-0.301155883789" />
                  <Point X="-21.527203125" Y="-0.346787261963" />
                  <Point X="-21.52127734375" Y="-0.353637420654" />
                  <Point X="-21.498857421875" Y="-0.37580645752" />
                  <Point X="-21.469822265625" Y="-0.398724853516" />
                  <Point X="-21.45850390625" Y="-0.406404937744" />
                  <Point X="-21.425677734375" Y="-0.425378570557" />
                  <Point X="-21.365994140625" Y="-0.459877349854" />
                  <Point X="-21.360501953125" Y="-0.462815917969" />
                  <Point X="-21.340845703125" Y="-0.472014923096" />
                  <Point X="-21.316974609375" Y="-0.481026977539" />
                  <Point X="-21.3080078125" Y="-0.483912780762" />
                  <Point X="-20.469248046875" Y="-0.70865802002" />
                  <Point X="-20.21512109375" Y="-0.776751403809" />
                  <Point X="-20.238384765625" Y="-0.931057373047" />
                  <Point X="-20.26634765625" Y="-1.053590087891" />
                  <Point X="-20.272197265625" Y="-1.079219848633" />
                  <Point X="-21.028236328125" Y="-0.979685119629" />
                  <Point X="-21.563216796875" Y="-0.909253662109" />
                  <Point X="-21.571375" Y="-0.908535705566" />
                  <Point X="-21.59990234375" Y="-0.908042419434" />
                  <Point X="-21.63327734375" Y="-0.910840942383" />
                  <Point X="-21.645517578125" Y="-0.912676147461" />
                  <Point X="-21.709943359375" Y="-0.926679260254" />
                  <Point X="-21.82708203125" Y="-0.952140014648" />
                  <Point X="-21.842125" Y="-0.956742248535" />
                  <Point X="-21.871244140625" Y="-0.968366027832" />
                  <Point X="-21.8853203125" Y="-0.975387695312" />
                  <Point X="-21.9131484375" Y="-0.992280334473" />
                  <Point X="-21.925875" Y="-1.001529907227" />
                  <Point X="-21.949625" Y="-1.022000610352" />
                  <Point X="-21.9606484375" Y="-1.033221801758" />
                  <Point X="-21.99958984375" Y="-1.080055175781" />
                  <Point X="-22.070392578125" Y="-1.165209716797" />
                  <Point X="-22.07867578125" Y="-1.176850097656" />
                  <Point X="-22.0933984375" Y="-1.201237915039" />
                  <Point X="-22.099837890625" Y="-1.213985107422" />
                  <Point X="-22.1111796875" Y="-1.241370483398" />
                  <Point X="-22.11563671875" Y="-1.254933959961" />
                  <Point X="-22.122466796875" Y="-1.282577636719" />
                  <Point X="-22.12483984375" Y="-1.296658081055" />
                  <Point X="-22.130421875" Y="-1.357309692383" />
                  <Point X="-22.140568359375" Y="-1.467588378906" />
                  <Point X="-22.140708984375" Y="-1.483313720703" />
                  <Point X="-22.138392578125" Y="-1.514580810547" />
                  <Point X="-22.135935546875" Y="-1.530122680664" />
                  <Point X="-22.128205078125" Y="-1.561743530273" />
                  <Point X="-22.123212890625" Y="-1.576668579102" />
                  <Point X="-22.11083984375" Y="-1.605481445313" />
                  <Point X="-22.103458984375" Y="-1.619369140625" />
                  <Point X="-22.067806640625" Y="-1.674826171875" />
                  <Point X="-22.002978515625" Y="-1.775659545898" />
                  <Point X="-21.9982578125" Y="-1.782350219727" />
                  <Point X="-21.98019921875" Y="-1.80445715332" />
                  <Point X="-21.956505859375" Y="-1.828123291016" />
                  <Point X="-21.947201171875" Y="-1.836277954102" />
                  <Point X="-21.168828125" Y="-2.433545654297" />
                  <Point X="-20.912826171875" Y="-2.629981445312" />
                  <Point X="-20.954501953125" Y="-2.697417236328" />
                  <Point X="-20.998724609375" Y="-2.760252197266" />
                  <Point X="-21.675072265625" Y="-2.369762451172" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.18497265625" Y="-2.079513427734" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228892578125" Y="-2.066337646484" />
                  <Point X="-22.305568359375" Y="-2.052490234375" />
                  <Point X="-22.444982421875" Y="-2.027312011719" />
                  <Point X="-22.460640625" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.555154296875" Y="-2.035136108398" />
                  <Point X="-22.5849296875" Y="-2.044959594727" />
                  <Point X="-22.59941015625" Y="-2.051108154297" />
                  <Point X="-22.663109375" Y="-2.084632080078" />
                  <Point X="-22.778927734375" Y="-2.145587158203" />
                  <Point X="-22.79103125" Y="-2.153170898438" />
                  <Point X="-22.813962890625" Y="-2.170065185547" />
                  <Point X="-22.824791015625" Y="-2.179375732422" />
                  <Point X="-22.84575" Y="-2.200335449219" />
                  <Point X="-22.855060546875" Y="-2.2111640625" />
                  <Point X="-22.871951171875" Y="-2.234092285156" />
                  <Point X="-22.87953125" Y="-2.246191894531" />
                  <Point X="-22.913056640625" Y="-2.309890380859" />
                  <Point X="-22.97401171875" Y="-2.425709960938" />
                  <Point X="-22.98016015625" Y="-2.440187988281" />
                  <Point X="-22.989986328125" Y="-2.469967529297" />
                  <Point X="-22.9936640625" Y="-2.485269042969" />
                  <Point X="-22.99862109375" Y="-2.517442382812" />
                  <Point X="-22.999720703125" Y="-2.533133789062" />
                  <Point X="-22.99931640625" Y="-2.564484130859" />
                  <Point X="-22.9978125" Y="-2.580143066406" />
                  <Point X="-22.98396484375" Y="-2.656818603516" />
                  <Point X="-22.95878515625" Y="-2.796233398438" />
                  <Point X="-22.95698046875" Y="-2.804228759766" />
                  <Point X="-22.94876171875" Y="-2.831542236328" />
                  <Point X="-22.935927734375" Y="-2.862479980469" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.430265625" Y="-3.739922607422" />
                  <Point X="-22.264103515625" Y="-4.027723144531" />
                  <Point X="-22.2762421875" Y="-4.036082763672" />
                  <Point X="-22.802490234375" Y="-3.350265380859" />
                  <Point X="-23.16608203125" Y="-2.876422607422" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849625732422" />
                  <Point X="-23.21674609375" Y="-2.828004638672" />
                  <Point X="-23.22669921875" Y="-2.820646972656" />
                  <Point X="-23.302322265625" Y="-2.772028564453" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.453716796875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663873535156" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.674310546875" Y="-2.654127197266" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920125" Y="-2.699414794922" />
                  <Point X="-23.944505859375" Y="-2.714135742188" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-24.020001953125" Y="-2.775513916016" />
                  <Point X="-24.136123046875" Y="-2.872063720703" />
                  <Point X="-24.147345703125" Y="-2.883087158203" />
                  <Point X="-24.16781640625" Y="-2.906836425781" />
                  <Point X="-24.177064453125" Y="-2.919562255859" />
                  <Point X="-24.19395703125" Y="-2.947388671875" />
                  <Point X="-24.20098046875" Y="-2.961467529297" />
                  <Point X="-24.21260546875" Y="-2.99058984375" />
                  <Point X="-24.21720703125" Y="-3.005633300781" />
                  <Point X="-24.23630078125" Y="-3.093485107422" />
                  <Point X="-24.271021484375" Y="-3.253221191406" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677001953" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520263672" />
                  <Point X="-24.16691015625" Y="-4.152324707031" />
                  <Point X="-24.20279296875" Y="-4.018405273438" />
                  <Point X="-24.344931640625" Y="-3.487937011719" />
                  <Point X="-24.347392578125" Y="-3.480122314453" />
                  <Point X="-24.357853515625" Y="-3.453578125" />
                  <Point X="-24.3732109375" Y="-3.423815185547" />
                  <Point X="-24.37958984375" Y="-3.413210205078" />
                  <Point X="-24.437685546875" Y="-3.329505371094" />
                  <Point X="-24.543318359375" Y="-3.177309814453" />
                  <Point X="-24.553328125" Y="-3.165172607422" />
                  <Point X="-24.575212890625" Y="-3.142716552734" />
                  <Point X="-24.587087890625" Y="-3.132397705078" />
                  <Point X="-24.61334375" Y="-3.113153808594" />
                  <Point X="-24.6267578125" Y="-3.104936767578" />
                  <Point X="-24.6547578125" Y="-3.090829345703" />
                  <Point X="-24.66934375" Y="-3.084938964844" />
                  <Point X="-24.7592421875" Y="-3.057037353516" />
                  <Point X="-24.922703125" Y="-3.006305664062" />
                  <Point X="-24.936623046875" Y="-3.003109619141" />
                  <Point X="-24.964783203125" Y="-2.998840087891" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.998840087891" />
                  <Point X="-25.051064453125" Y="-3.003109619141" />
                  <Point X="-25.064984375" Y="-3.006305664062" />
                  <Point X="-25.1548828125" Y="-3.03420703125" />
                  <Point X="-25.31834375" Y="-3.084938964844" />
                  <Point X="-25.3329296875" Y="-3.090829345703" />
                  <Point X="-25.3609296875" Y="-3.104936767578" />
                  <Point X="-25.37434375" Y="-3.113153808594" />
                  <Point X="-25.400599609375" Y="-3.132397705078" />
                  <Point X="-25.4124765625" Y="-3.142718017578" />
                  <Point X="-25.434361328125" Y="-3.165175048828" />
                  <Point X="-25.444369140625" Y="-3.177311767578" />
                  <Point X="-25.502462890625" Y="-3.261016357422" />
                  <Point X="-25.608095703125" Y="-3.413212158203" />
                  <Point X="-25.61246875" Y="-3.420131347656" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213378906" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.901197265625" Y="-4.452458007812" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.333868027908" Y="-3.960983571788" />
                  <Point X="-22.311865238244" Y="-3.944997609362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.391720319455" Y="-3.885589264077" />
                  <Point X="-22.359626960862" Y="-3.862272074193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.449572611002" Y="-3.810194956366" />
                  <Point X="-22.407388683481" Y="-3.779546539023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.00191506691" Y="-2.758410184595" />
                  <Point X="-20.992733239025" Y="-2.751739196152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.507424902549" Y="-3.734800648655" />
                  <Point X="-22.455150352688" Y="-3.696820965049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.091973410305" Y="-2.706414943211" />
                  <Point X="-20.948789680084" Y="-2.602385873887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.565277194096" Y="-3.659406340944" />
                  <Point X="-22.502911972795" Y="-3.6140953554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.182031753701" Y="-2.654419701827" />
                  <Point X="-21.027395536601" Y="-2.542069913735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.623129485643" Y="-3.584012033233" />
                  <Point X="-22.550673592901" Y="-3.531369745751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.272090097096" Y="-2.602424460443" />
                  <Point X="-21.106001393119" Y="-2.481753953583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.68098177719" Y="-3.508617725521" />
                  <Point X="-22.598435213007" Y="-3.448644136102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.362148440492" Y="-2.550429219059" />
                  <Point X="-21.184607176877" Y="-2.421437940568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.738834068737" Y="-3.43322341781" />
                  <Point X="-22.646196833113" Y="-3.365918526453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.452206783887" Y="-2.498433977675" />
                  <Point X="-21.263212670934" Y="-2.361121717072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.796686360283" Y="-3.357829110099" />
                  <Point X="-22.693958453219" Y="-3.283192916804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.542265127283" Y="-2.446438736292" />
                  <Point X="-21.341818164991" Y="-2.300805493577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.854538483037" Y="-3.282434679753" />
                  <Point X="-22.741720073325" Y="-3.200467307155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.632323470678" Y="-2.394443494908" />
                  <Point X="-21.420423659048" Y="-2.240489270082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.17449596301" Y="-4.124013466249" />
                  <Point X="-24.170974284981" Y="-4.121454817391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.912390586969" Y="-3.207040235731" />
                  <Point X="-22.789481693431" Y="-3.117741697506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.722381813454" Y="-2.342448253073" />
                  <Point X="-21.499029153105" Y="-2.180173046586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.200832585513" Y="-4.025721684679" />
                  <Point X="-24.185084244222" Y="-4.014279844986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.9702426909" Y="-3.13164579171" />
                  <Point X="-22.837243313537" Y="-3.035016087857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.812440155668" Y="-2.290453010831" />
                  <Point X="-21.577634647162" Y="-2.119856823091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.227169676729" Y="-3.927430243649" />
                  <Point X="-24.199194203463" Y="-3.907104872581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.028094794832" Y="-3.056251347688" />
                  <Point X="-22.885004933644" Y="-2.952290478208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.902498497883" Y="-2.238457768589" />
                  <Point X="-21.656240141218" Y="-2.059540599596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.301613944101" Y="-1.07534705784" />
                  <Point X="-20.265290192604" Y="-1.0489563076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.25350680564" Y="-3.829138830006" />
                  <Point X="-24.213304162704" Y="-3.799929900175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.085946898764" Y="-2.980856903667" />
                  <Point X="-22.932519197475" Y="-2.869385153707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.992556840097" Y="-2.186462526347" />
                  <Point X="-21.734845635275" Y="-1.9992243761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.438443453013" Y="-1.057333057288" />
                  <Point X="-20.235155125301" Y="-0.909635441758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.27984393455" Y="-3.730847416363" />
                  <Point X="-24.227414121945" Y="-3.69275492777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.143799002696" Y="-2.905462459645" />
                  <Point X="-22.962807191158" Y="-2.773964211343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.082615182312" Y="-2.134467284105" />
                  <Point X="-21.813451129332" Y="-1.938908152605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.575272961924" Y="-1.039319056736" />
                  <Point X="-20.215273810852" Y="-0.777764363436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.306181063461" Y="-3.632556002719" />
                  <Point X="-24.241524081186" Y="-3.585579955364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.208456104317" Y="-2.835012135848" />
                  <Point X="-22.981555412403" Y="-2.670159133539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.174729011864" Y="-2.08396544083" />
                  <Point X="-21.892056623389" Y="-1.87859192911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.712102470836" Y="-1.021305056184" />
                  <Point X="-20.332290825719" Y="-0.745355743375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.019786557042" Y="-4.760136812167" />
                  <Point X="-25.974902720477" Y="-4.727526796082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.332518192372" Y="-3.534264589076" />
                  <Point X="-24.255634040427" Y="-3.478404982959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.292278160294" Y="-2.778485966438" />
                  <Point X="-22.999212923766" Y="-2.565561608621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.29552679561" Y="-2.054303710148" />
                  <Point X="-21.968174332462" Y="-1.816468224024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.848931979748" Y="-1.003291055632" />
                  <Point X="-20.450367556881" Y="-0.713717052269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.141191819644" Y="-4.730916440709" />
                  <Point X="-25.935831649717" Y="-4.581713543698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.364733754601" Y="-3.440244107237" />
                  <Point X="-24.269743999668" Y="-3.371230010553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.378025606316" Y="-2.723358674778" />
                  <Point X="-22.976539311271" Y="-2.431661807016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.42497332477" Y="-2.030925660823" />
                  <Point X="-22.025473018036" Y="-1.740671698029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.985761488659" Y="-0.98527705508" />
                  <Point X="-20.56844436654" Y="-0.682078418195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.124095437573" Y="-4.601068734197" />
                  <Point X="-25.896760640412" Y="-4.435900335964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.416316217005" Y="-3.36029450201" />
                  <Point X="-24.271319677712" Y="-3.2549483498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.466970279185" Y="-2.670554304395" />
                  <Point X="-22.876729088444" Y="-2.241718977541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.653706041942" Y="-2.079683249532" />
                  <Point X="-22.076931712909" Y="-1.660632170427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.122591052716" Y="-0.967263094593" />
                  <Point X="-20.68652119114" Y="-0.650439794976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.125871458859" Y="-4.484932631329" />
                  <Point X="-25.857690110852" Y="-4.290087476784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.470496079166" Y="-3.282232018169" />
                  <Point X="-24.241100416958" Y="-3.115566313835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.596074596753" Y="-2.646927623794" />
                  <Point X="-22.123159743163" Y="-1.576792342531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.259420641596" Y="-0.949249152141" />
                  <Point X="-20.804598015741" Y="-0.618801171758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.146280855775" Y="-4.382334468297" />
                  <Point X="-25.818619581292" Y="-4.144274617605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.524676052441" Y="-3.204169615057" />
                  <Point X="-24.205210462133" Y="-2.972064277464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.781137879135" Y="-2.663957510955" />
                  <Point X="-22.140608194488" Y="-1.472042926604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.396250230477" Y="-0.93123520969" />
                  <Point X="-20.922674840341" Y="-0.587162548539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.166689269884" Y="-4.279735591214" />
                  <Point X="-25.779549051732" Y="-3.998461758425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.587300084863" Y="-3.132242180024" />
                  <Point X="-22.129426308254" Y="-1.346492352849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.533079819357" Y="-0.913221267239" />
                  <Point X="-21.040751664941" Y="-0.555523925321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.188299193455" Y="-4.178009661853" />
                  <Point X="-25.740478522173" Y="-3.852648899246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.679484479646" Y="-3.08179160539" />
                  <Point X="-22.09471702915" Y="-1.203848127601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.714628412336" Y="-0.927697583075" />
                  <Point X="-21.158828489541" Y="-0.523885302102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.232406412007" Y="-4.09262897406" />
                  <Point X="-25.701407992613" Y="-3.706836040066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.792731206969" Y="-3.046643711085" />
                  <Point X="-21.276905314141" Y="-0.492246678883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.300792465921" Y="-4.024887892689" />
                  <Point X="-25.662337463053" Y="-3.561023180887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.905978583003" Y="-3.011496288097" />
                  <Point X="-21.381578050842" Y="-0.450869415757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.374022547207" Y="-3.96066620321" />
                  <Point X="-25.595461078235" Y="-3.395008185335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.058366605857" Y="-3.004786209596" />
                  <Point X="-21.470593253085" Y="-0.398116287963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.447252824206" Y="-3.896444655925" />
                  <Point X="-25.418877215243" Y="-3.149286041249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.350835976304" Y="-3.099851187502" />
                  <Point X="-21.539682192996" Y="-0.33088588316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.524335889433" Y="-3.835022323138" />
                  <Point X="-21.596471473253" Y="-0.25471925254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.29078653304" Y="0.693916384701" />
                  <Point X="-20.220916020324" Y="0.744680283643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.633217777708" Y="-3.796703187637" />
                  <Point X="-21.63158190134" Y="-0.162802013858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.546844666347" Y="0.625305719075" />
                  <Point X="-20.237340327165" Y="0.850173784092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.781091347007" Y="-3.786713166638" />
                  <Point X="-21.649914893701" Y="-0.058695254612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.802902799653" Y="0.556695053449" />
                  <Point X="-20.25685583159" Y="0.953421398035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.929341196071" Y="-3.776996528892" />
                  <Point X="-21.637852755924" Y="0.067494859324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.058960502406" Y="0.488084700638" />
                  <Point X="-20.281147155927" Y="1.053199175705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.567089604924" Y="-4.122921412229" />
                  <Point X="-21.561736460014" Y="0.240223043239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.315874846323" Y="0.41885196159" />
                  <Point X="-20.377707795933" Y="1.100470222072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.654348775528" Y="-4.068892452768" />
                  <Point X="-20.575099664097" Y="1.07448309303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.741607946132" Y="-4.014863493308" />
                  <Point X="-20.772491601623" Y="1.048495913595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.820677836747" Y="-3.954884673662" />
                  <Point X="-20.96988354272" Y="1.022508731565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.899144434837" Y="-3.89446753634" />
                  <Point X="-21.167275483816" Y="0.996521549536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.977611273037" Y="-3.834050573468" />
                  <Point X="-21.364667424913" Y="0.970534367506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.868920281712" Y="-3.637655487997" />
                  <Point X="-21.551785802862" Y="0.952011366017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.752137285141" Y="-3.435381216577" />
                  <Point X="-21.685309272691" Y="0.972427344562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.63535428857" Y="-3.233106945158" />
                  <Point X="-21.784623304619" Y="1.017697934601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.518571085825" Y="-3.030832523945" />
                  <Point X="-21.865955980112" Y="1.076032744801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.401787812713" Y="-2.828558051607" />
                  <Point X="-21.922559081915" Y="1.152334641987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.316265597259" Y="-2.648996067128" />
                  <Point X="-21.968069158641" Y="1.236696093655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.321428510518" Y="-2.535320685316" />
                  <Point X="-21.992030967252" Y="1.336713278514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.36645970145" Y="-2.450611302753" />
                  <Point X="-21.994087027952" Y="1.452645920837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.439069197119" Y="-2.385938731431" />
                  <Point X="-21.937546302102" Y="1.611151620594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.54891365828" Y="-2.348318946068" />
                  <Point X="-20.8739284785" Y="2.501341660848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.00799865885" Y="-2.564437265088" />
                  <Point X="-20.923286628365" Y="2.58290732373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.793233561408" Y="-3.017517358409" />
                  <Point X="-20.97470564453" Y="2.6629756796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.850785974314" Y="-2.941905176112" />
                  <Point X="-21.030191824784" Y="2.740089067792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.908338387219" Y="-2.866292993814" />
                  <Point X="-21.429601581872" Y="2.56732735103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.965890800125" Y="-2.790680811517" />
                  <Point X="-22.216682522851" Y="2.112906032288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.020509209748" Y="-2.712936951058" />
                  <Point X="-22.567549419148" Y="1.975412768322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.069368164313" Y="-2.631008601561" />
                  <Point X="-22.703384468865" Y="1.994149285771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.118226976158" Y="-2.549080148372" />
                  <Point X="-22.797107788868" Y="2.043481765785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.167085520457" Y="-2.467151500798" />
                  <Point X="-22.873913579349" Y="2.105105550466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.979849027788" Y="-1.487147240212" />
                  <Point X="-22.930108921968" Y="2.181703702041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.948979418249" Y="-1.347292698196" />
                  <Point X="-22.971356363424" Y="2.269162139514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.97255167069" Y="-1.246992484213" />
                  <Point X="-22.986863791666" Y="2.375321791258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.031859657374" Y="-1.172655800927" />
                  <Point X="-22.967785226391" Y="2.506609638167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.119663596619" Y="-1.119022639052" />
                  <Point X="-22.88547674646" Y="2.683836707115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.257820152997" Y="-1.101972794922" />
                  <Point X="-22.768693323244" Y="2.886111288509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.455212089233" Y="-1.12795997342" />
                  <Point X="-22.651909900028" Y="3.088385869904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.652604025469" Y="-1.153947151918" />
                  <Point X="-22.535127034104" Y="3.290660046403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.849995961706" Y="-1.179934330417" />
                  <Point X="-22.418344304504" Y="3.492934123856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.047387897942" Y="-1.205921508915" />
                  <Point X="-22.301561574904" Y="3.695208201309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.244779834178" Y="-1.231908687414" />
                  <Point X="-22.184778845305" Y="3.897482278763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.442171642043" Y="-1.257895772645" />
                  <Point X="-22.262105681101" Y="3.958727501863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.639563406688" Y="-1.283882826475" />
                  <Point X="-22.343782825556" Y="4.016812040713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.68348184182" Y="-1.198364979499" />
                  <Point X="-22.430850008211" Y="4.070980487583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.708780963475" Y="-1.099319409441" />
                  <Point X="-28.693770819943" Y="-0.361871373808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.29827799608" Y="-0.07452901775" />
                  <Point X="-22.520547089768" Y="4.123238201056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.734080085131" Y="-1.000273839382" />
                  <Point X="-28.949828623609" Y="-0.430481799936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.304324322359" Y="0.038504526932" />
                  <Point X="-22.610244171325" Y="4.175495914529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.751957681584" Y="-0.895836215641" />
                  <Point X="-29.205886657812" Y="-0.49909239356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.342147693552" Y="0.128450697071" />
                  <Point X="-22.699941174319" Y="4.227753685082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.767170733913" Y="-0.789462687277" />
                  <Point X="-29.46194480646" Y="-0.567703070333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.408925577946" Y="0.19736018199" />
                  <Point X="-22.793496624665" Y="4.277208129542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.782384343801" Y="-0.683089564003" />
                  <Point X="-29.718002955109" Y="-0.636313747106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.501704510391" Y="0.247378799728" />
                  <Point X="-22.891826632727" Y="4.323193654768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.619310101032" Y="0.279359794459" />
                  <Point X="-22.990156640789" Y="4.369179179994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.737386919745" Y="0.310998421955" />
                  <Point X="-23.088486350443" Y="4.415164922027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.855463738458" Y="0.342637049451" />
                  <Point X="-23.191158664861" Y="4.457995577016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.973540557171" Y="0.374275676947" />
                  <Point X="-23.298963581763" Y="4.497097178021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.091617375883" Y="0.405914304443" />
                  <Point X="-23.406768498666" Y="4.536198779025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.209694194596" Y="0.437552931939" />
                  <Point X="-23.514574147541" Y="4.575299848221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.327771013309" Y="0.469191559436" />
                  <Point X="-23.63189892703" Y="4.607484864196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.445847832022" Y="0.500830186932" />
                  <Point X="-23.75321038251" Y="4.636773390518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.563924680818" Y="0.532468792571" />
                  <Point X="-28.575612708149" Y="1.250519471651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.51470245457" Y="1.294773361268" />
                  <Point X="-23.874521837991" Y="4.666061916839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.682001535646" Y="0.564107393828" />
                  <Point X="-28.795834925962" Y="1.207945122661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.426273797966" Y="1.476446998848" />
                  <Point X="-23.995833281104" Y="4.695350452146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.783032530569" Y="0.608130537232" />
                  <Point X="-28.932664455507" Y="1.225959108223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.431545041336" Y="1.590043674227" />
                  <Point X="-24.997376473584" Y="4.085113187038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.764735860989" Y="4.25413648583" />
                  <Point X="-24.1171446994" Y="4.724639005484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.763563372928" Y="0.739702166105" />
                  <Point X="-29.069493985051" Y="1.243973093784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.471364343327" Y="1.678539715758" />
                  <Point X="-25.11374249614" Y="4.117994780699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.724817936028" Y="4.400565013806" />
                  <Point X="-24.249365286619" Y="4.746001583654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.744094215287" Y="0.871273794978" />
                  <Point X="-29.206323551422" Y="1.261987052589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.527813069274" Y="1.754953773567" />
                  <Point X="-25.185419544534" Y="4.183344814621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.685747422326" Y="4.546377861464" />
                  <Point X="-24.390627052043" Y="4.760795361355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.717682931911" Y="1.007889173432" />
                  <Point X="-29.343153180041" Y="1.280000966169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.604845242305" Y="1.816413081698" />
                  <Point X="-25.227866294089" Y="4.269931903756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.646676908624" Y="4.692190709122" />
                  <Point X="-24.531888817466" Y="4.775589139056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.678062071548" Y="1.154101871345" />
                  <Point X="-29.47998280866" Y="1.298014879749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.683450753798" Y="1.876729292526" />
                  <Point X="-25.254203126581" Y="4.36822353276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.638442742501" Y="1.300313456691" />
                  <Point X="-29.616812437279" Y="1.316028793328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.76205626529" Y="1.937045503353" />
                  <Point X="-25.280539959074" Y="4.466515161764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.840661776783" Y="1.997361714181" />
                  <Point X="-25.306876915345" Y="4.564806700837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.919267288276" Y="2.057677925008" />
                  <Point X="-27.993989553883" Y="2.729931549261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.808954900928" Y="2.864367093787" />
                  <Point X="-25.333214005734" Y="4.663098142468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.997872799768" Y="2.117994135835" />
                  <Point X="-28.154048481281" Y="2.731068389382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.748641029681" Y="3.02561414414" />
                  <Point X="-25.359551096124" Y="4.761389584098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.076478311261" Y="2.178310346663" />
                  <Point X="-28.264405444834" Y="2.768315819961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.757254657259" Y="3.136782435246" />
                  <Point X="-25.516392398909" Y="4.764864165339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.155083960333" Y="2.238626457534" />
                  <Point X="-28.354463765608" Y="2.820311077781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.788818641764" Y="3.231276316012" />
                  <Point X="-25.709050074068" Y="4.742316628853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.218417893346" Y="2.310038119596" />
                  <Point X="-28.444522086382" Y="2.872306335601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.836580323506" Y="3.31400188088" />
                  <Point X="-26.851646775865" Y="4.0295979905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.486804311857" Y="4.294671556625" />
                  <Point X="-25.901708092112" Y="4.719768843245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.099405517595" Y="2.5139321298" />
                  <Point X="-28.534580372508" Y="2.924301618593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.884342005248" Y="3.396727445748" />
                  <Point X="-26.963437321455" Y="4.065803862763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.4626805419" Y="4.429624959297" />
                  <Point X="-26.154104854432" Y="4.65381831935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.963632518713" Y="2.730003445505" />
                  <Point X="-28.624638629126" Y="2.976296923024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.932103686989" Y="3.479453010616" />
                  <Point X="-27.056112639688" Y="4.115897760632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.475941123835" Y="4.537417040437" />
                  <Point X="-26.417288092808" Y="4.580030961875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.753500012391" Y="3.000100105727" />
                  <Point X="-28.714696885745" Y="3.028292227455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.979865368731" Y="3.562178575485" />
                  <Point X="-27.12646714801" Y="4.182208676163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.027627050472" Y="3.644904140353" />
                  <Point X="-27.184319251609" Y="4.257603120425" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.3863203125" Y="-4.067580566406" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.593775390625" Y="-3.437839355469" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.8155625" Y="-3.238498291016" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.0985625" Y="-3.21566796875" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643798828" />
                  <Point X="-25.346373046875" Y="-3.369348388672" />
                  <Point X="-25.452005859375" Y="-3.521544189453" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.717669921875" Y="-4.501633789062" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.908337890625" Y="-4.975314941406" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.203708984375" Y="-4.9114453125" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.326259765625" Y="-4.681003417969" />
                  <Point X="-26.3091484375" Y="-4.5510390625" />
                  <Point X="-26.309681640625" Y="-4.5347578125" />
                  <Point X="-26.33116015625" Y="-4.426785644531" />
                  <Point X="-26.3702109375" Y="-4.230466308594" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.46905078125" Y="-4.13004296875" />
                  <Point X="-26.61954296875" Y="-3.998064208984" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.759091796875" Y="-3.978562744141" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791015625" />
                  <Point X="-27.081412109375" Y="-4.034952392578" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157293945313" />
                  <Point X="-27.404828125" Y="-4.346386230469" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.574537109375" Y="-4.341782714844" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-27.999099609375" Y="-4.057299804688" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.770181640625" Y="-3.086637939453" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597592285156" />
                  <Point X="-27.51398046875" Y="-2.568762939453" />
                  <Point X="-27.53132421875" Y="-2.55141796875" />
                  <Point X="-27.560154296875" Y="-2.537198974609" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.4235625" Y="-3.023756347656" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-28.93946484375" Y="-3.139106201172" />
                  <Point X="-29.161697265625" Y="-2.847135742188" />
                  <Point X="-29.2644140625" Y="-2.674898681641" />
                  <Point X="-29.43101953125" Y="-2.395526855469" />
                  <Point X="-28.6259609375" Y="-1.777783447266" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.396013427734" />
                  <Point X="-28.1381171875" Y="-1.366266113281" />
                  <Point X="-28.140326171875" Y="-1.334595458984" />
                  <Point X="-28.161158203125" Y="-1.310638916016" />
                  <Point X="-28.187640625" Y="-1.295052490234" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.269697265625" Y="-1.426828613281" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.84047265625" Y="-1.351477294922" />
                  <Point X="-29.927392578125" Y="-1.011188171387" />
                  <Point X="-29.95456640625" Y="-0.821188903809" />
                  <Point X="-29.99839453125" Y="-0.5147421875" />
                  <Point X="-29.08394921875" Y="-0.269716796875" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.532048828125" Y="-0.114590919495" />
                  <Point X="-28.514142578125" Y="-0.102163635254" />
                  <Point X="-28.502322265625" Y="-0.090643539429" />
                  <Point X="-28.491615234375" Y="-0.065326057434" />
                  <Point X="-28.4856484375" Y="-0.046101486206" />
                  <Point X="-28.483400390625" Y="-0.031283159256" />
                  <Point X="-28.4889296875" Y="-0.005882251263" />
                  <Point X="-28.4948984375" Y="0.013348286629" />
                  <Point X="-28.50232421875" Y="0.028085977554" />
                  <Point X="-28.523990234375" Y="0.046438289642" />
                  <Point X="-28.541896484375" Y="0.058865581512" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.5147421875" Y="0.322587860107" />
                  <Point X="-29.998185546875" Y="0.452126037598" />
                  <Point X="-29.973662109375" Y="0.617852355957" />
                  <Point X="-29.91764453125" Y="0.996414916992" />
                  <Point X="-29.86294140625" Y="1.198287475586" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.130662109375" Y="1.443665771484" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.709908203125" Y="1.402738891602" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.651533203125" Y="1.426056640625" />
                  <Point X="-28.6391171875" Y="1.443787231445" />
                  <Point X="-28.63037109375" Y="1.464901245117" />
                  <Point X="-28.614470703125" Y="1.503291503906" />
                  <Point X="-28.61071484375" Y="1.524606201172" />
                  <Point X="-28.61631640625" Y="1.545511962891" />
                  <Point X="-28.626869140625" Y="1.565783325195" />
                  <Point X="-28.646056640625" Y="1.602641601562" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-29.20906640625" Y="2.040559692383" />
                  <Point X="-29.47610546875" Y="2.245466308594" />
                  <Point X="-29.377681640625" Y="2.414091064453" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-29.015109375" Y="2.973260253906" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.390943359375" Y="3.060765869141" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.108158203125" Y="2.917779052734" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775146484" />
                  <Point X="-28.013251953125" Y="2.927404296875" />
                  <Point X="-27.991705078125" Y="2.948950927734" />
                  <Point X="-27.95252734375" Y="2.988127685547" />
                  <Point X="-27.9408984375" Y="3.006381835938" />
                  <Point X="-27.938072265625" Y="3.027840576172" />
                  <Point X="-27.940728515625" Y="3.058196289062" />
                  <Point X="-27.945556640625" Y="3.113390136719" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.195388671875" Y="3.555479003906" />
                  <Point X="-28.30727734375" Y="3.749276611328" />
                  <Point X="-28.131982421875" Y="3.883674560547" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.52465625" Y="4.301125" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.038783203125" Y="4.380046386719" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.9174609375" Y="4.256072753906" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.7786171875" Y="4.236827148438" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.69690234375" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.29448828125" />
                  <Point X="-26.67462890625" Y="4.330814941406" />
                  <Point X="-26.653802734375" Y="4.396864746094" />
                  <Point X="-26.651916015625" Y="4.418426269531" />
                  <Point X="-26.679580078125" Y="4.628547851562" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.458869140625" Y="4.765699707031" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.691423828125" Y="4.935676269531" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.110708984375" Y="4.566807128906" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176269531" />
                  <Point X="-24.945546875" Y="4.310902832031" />
                  <Point X="-24.82087109375" Y="4.776198242188" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.5683125" Y="4.970442871094" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.910896484375" Y="4.870302734375" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.342537109375" Y="4.715013183594" />
                  <Point X="-23.068970703125" Y="4.615788085938" />
                  <Point X="-22.9248984375" Y="4.548410644531" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.52212109375" Y="4.344048828125" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-22.13621484375" Y="4.102346191406" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.464412109375" Y="3.033141357422" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491514648438" />
                  <Point X="-22.782765625" Y="2.463026611328" />
                  <Point X="-22.7966171875" Y="2.411228271484" />
                  <Point X="-22.797955078125" Y="2.392326416016" />
                  <Point X="-22.794984375" Y="2.367692382812" />
                  <Point X="-22.789583984375" Y="2.322901611328" />
                  <Point X="-22.781318359375" Y="2.3008125" />
                  <Point X="-22.76607421875" Y="2.278348632812" />
                  <Point X="-22.738359375" Y="2.23750390625" />
                  <Point X="-22.72505859375" Y="2.224203369141" />
                  <Point X="-22.702595703125" Y="2.208960693359" />
                  <Point X="-22.66175" Y="2.181245849609" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.615029296875" Y="2.170009765625" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5513359375" Y="2.165946289062" />
                  <Point X="-22.52284765625" Y="2.173564453125" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.496125" Y="2.748313232422" />
                  <Point X="-21.005751953125" Y="3.031430664062" />
                  <Point X="-20.948455078125" Y="2.951801757812" />
                  <Point X="-20.79740234375" Y="2.741874267578" />
                  <Point X="-20.72423046875" Y="2.620956542969" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.308857421875" Y="1.901950927734" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583833251953" />
                  <Point X="-21.741130859375" Y="1.557085693359" />
                  <Point X="-21.77841015625" Y="1.508451904297" />
                  <Point X="-21.78687890625" Y="1.491500244141" />
                  <Point X="-21.794517578125" Y="1.464190551758" />
                  <Point X="-21.808404296875" Y="1.414535644531" />
                  <Point X="-21.809220703125" Y="1.390964599609" />
                  <Point X="-21.802951171875" Y="1.360579345703" />
                  <Point X="-21.79155078125" Y="1.305331787109" />
                  <Point X="-21.784353515625" Y="1.287955810547" />
                  <Point X="-21.76730078125" Y="1.262036865234" />
                  <Point X="-21.736296875" Y="1.214910400391" />
                  <Point X="-21.719052734375" Y="1.19882019043" />
                  <Point X="-21.69433984375" Y="1.184909912109" />
                  <Point X="-21.64941015625" Y="1.159617797852" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.598021484375" Y="1.149203979492" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.6095625" Y="1.261585571289" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.125685546875" Y="1.217867797852" />
                  <Point X="-20.060806640625" Y="0.951367004395" />
                  <Point X="-20.03775" Y="0.803276184082" />
                  <Point X="-20.002140625" Y="0.574556396484" />
                  <Point X="-20.797529296875" Y="0.361432434082" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819503784" />
                  <Point X="-21.303736328125" Y="0.21384588623" />
                  <Point X="-21.363421875" Y="0.179347137451" />
                  <Point X="-21.377734375" Y="0.166926544189" />
                  <Point X="-21.3974296875" Y="0.141830093384" />
                  <Point X="-21.433240234375" Y="0.096198867798" />
                  <Point X="-21.443013671875" Y="0.074735565186" />
                  <Point X="-21.449578125" Y="0.040455253601" />
                  <Point X="-21.461515625" Y="-0.021874994278" />
                  <Point X="-21.461515625" Y="-0.040685081482" />
                  <Point X="-21.454951171875" Y="-0.074965545654" />
                  <Point X="-21.443013671875" Y="-0.137295639038" />
                  <Point X="-21.433240234375" Y="-0.15875894165" />
                  <Point X="-21.413544921875" Y="-0.183855239868" />
                  <Point X="-21.377734375" Y="-0.229486618042" />
                  <Point X="-21.363421875" Y="-0.24190675354" />
                  <Point X="-21.330595703125" Y="-0.260880523682" />
                  <Point X="-21.270912109375" Y="-0.295379272461" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.420072265625" Y="-0.525132202148" />
                  <Point X="-20.001931640625" Y="-0.637172546387" />
                  <Point X="-20.01534375" Y="-0.726141479492" />
                  <Point X="-20.051568359375" Y="-0.966413208008" />
                  <Point X="-20.081109375" Y="-1.095862915039" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-21.053037109375" Y="-1.168059570312" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.669587890625" Y="-1.112344360352" />
                  <Point X="-21.7867265625" Y="-1.137805053711" />
                  <Point X="-21.8145546875" Y="-1.154697631836" />
                  <Point X="-21.85349609375" Y="-1.201531005859" />
                  <Point X="-21.924298828125" Y="-1.286685546875" />
                  <Point X="-21.935640625" Y="-1.314071166992" />
                  <Point X="-21.94122265625" Y="-1.37472253418" />
                  <Point X="-21.951369140625" Y="-1.485001464844" />
                  <Point X="-21.943638671875" Y="-1.516622314453" />
                  <Point X="-21.907986328125" Y="-1.572079101562" />
                  <Point X="-21.843158203125" Y="-1.672912841797" />
                  <Point X="-21.831537109375" Y="-1.685540771484" />
                  <Point X="-21.0531640625" Y="-2.282808837891" />
                  <Point X="-20.660923828125" Y="-2.583784179688" />
                  <Point X="-20.6937578125" Y="-2.636913574219" />
                  <Point X="-20.7958671875" Y="-2.802140380859" />
                  <Point X="-20.85696484375" Y="-2.888953369141" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.770072265625" Y="-2.534307373047" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.339333984375" Y="-2.239465332031" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.57462109375" Y="-2.252768798828" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.33468359375" />
                  <Point X="-22.744923828125" Y="-2.398382080078" />
                  <Point X="-22.80587890625" Y="-2.514201660156" />
                  <Point X="-22.8108359375" Y="-2.546375" />
                  <Point X="-22.79698828125" Y="-2.623050537109" />
                  <Point X="-22.77180859375" Y="-2.762465332031" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.26572265625" Y="-3.644922607422" />
                  <Point X="-22.01332421875" Y="-4.082087646484" />
                  <Point X="-22.043541015625" Y="-4.103669921875" />
                  <Point X="-22.16470703125" Y="-4.190215820312" />
                  <Point X="-22.23301171875" Y="-4.234428710938" />
                  <Point X="-22.32022265625" Y="-4.29087890625" />
                  <Point X="-22.953228515625" Y="-3.465930175781" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.405072265625" Y="-2.931848876953" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.656900390625" Y="-2.843327880859" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.898529296875" Y="-2.921609619141" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045985839844" />
                  <Point X="-24.05063671875" Y="-3.133837646484" />
                  <Point X="-24.085357421875" Y="-3.293573730469" />
                  <Point X="-24.0860703125" Y="-3.310719970703" />
                  <Point X="-23.9443203125" Y="-4.387405273438" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.891025390625" Y="-4.938120605469" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.06876171875" Y="-4.974711425781" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#152" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.068569105749" Y="4.611027873117" Z="0.9" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.9" />
                  <Point X="-0.703392512309" Y="5.01661766568" Z="0.9" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.9" />
                  <Point X="-1.478524334475" Y="4.845123510723" Z="0.9" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.9" />
                  <Point X="-1.735308866933" Y="4.653301920354" Z="0.9" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.9" />
                  <Point X="-1.728471475712" Y="4.377130412799" Z="0.9" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.9" />
                  <Point X="-1.803909102593" Y="4.314300854688" Z="0.9" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.9" />
                  <Point X="-1.90052958834" Y="4.331703520828" Z="0.9" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.9" />
                  <Point X="-2.005272312325" Y="4.441764470535" Z="0.9" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.9" />
                  <Point X="-2.555095750193" Y="4.376112716961" Z="0.9" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.9" />
                  <Point X="-3.168560409571" Y="3.954634589716" Z="0.9" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.9" />
                  <Point X="-3.244846783068" Y="3.561759119031" Z="0.9" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.9" />
                  <Point X="-2.99669580474" Y="3.085119094084" Z="0.9" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.9" />
                  <Point X="-3.033216977349" Y="3.015586590537" Z="0.9" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.9" />
                  <Point X="-3.109957371523" Y="2.998868894433" Z="0.9" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.9" />
                  <Point X="-3.372100229266" Y="3.135347165678" Z="0.9" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.9" />
                  <Point X="-4.060729761585" Y="3.035242731352" Z="0.9" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.9" />
                  <Point X="-4.427019268861" Y="2.470576251348" Z="0.9" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.9" />
                  <Point X="-4.245660618956" Y="2.032172041132" Z="0.9" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.9" />
                  <Point X="-3.677375048065" Y="1.573975767677" Z="0.9" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.9" />
                  <Point X="-3.682724226861" Y="1.515314053241" Z="0.9" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.9" />
                  <Point X="-3.731100181305" Y="1.481704745484" Z="0.9" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.9" />
                  <Point X="-4.130293955744" Y="1.524517923982" Z="0.9" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.9" />
                  <Point X="-4.917358356369" Y="1.242644757719" Z="0.9" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.9" />
                  <Point X="-5.029280685329" Y="0.656451415839" Z="0.9" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.9" />
                  <Point X="-4.533841191295" Y="0.305571561024" Z="0.9" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.9" />
                  <Point X="-3.558655432327" Y="0.036641729019" Z="0.9" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.9" />
                  <Point X="-3.542839322415" Y="0.010576421727" Z="0.9" />
                  <Point X="-3.539556741714" Y="0" Z="0.9" />
                  <Point X="-3.54552527872" Y="-0.019230529334" Z="0.9" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.9" />
                  <Point X="-3.566713162796" Y="-0.042234254115" Z="0.9" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.9" />
                  <Point X="-4.103046597478" Y="-0.190140495732" Z="0.9" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.9" />
                  <Point X="-5.010219768224" Y="-0.796987875213" Z="0.9" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.9" />
                  <Point X="-4.895074574115" Y="-1.332571239631" Z="0.9" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.9" />
                  <Point X="-4.269329603626" Y="-1.445120879055" Z="0.9" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.9" />
                  <Point X="-3.202073139283" Y="-1.316919236061" Z="0.9" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.9" />
                  <Point X="-3.197647010202" Y="-1.341641890819" Z="0.9" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.9" />
                  <Point X="-3.662554832155" Y="-1.70683576165" Z="0.9" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.9" />
                  <Point X="-4.313513915937" Y="-2.669228282569" Z="0.9" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.9" />
                  <Point X="-3.985406299781" Y="-3.138109415659" Z="0.9" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.9" />
                  <Point X="-3.404720379423" Y="-3.035777614465" Z="0.9" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.9" />
                  <Point X="-2.561646761156" Y="-2.566683707976" Z="0.9" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.9" />
                  <Point X="-2.819639398704" Y="-3.030358003069" Z="0.9" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.9" />
                  <Point X="-3.035761135808" Y="-4.065637063645" Z="0.9" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.9" />
                  <Point X="-2.607015380794" Y="-4.353013629449" Z="0.9" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.9" />
                  <Point X="-2.371317931202" Y="-4.345544451312" Z="0.9" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.9" />
                  <Point X="-2.059790356014" Y="-4.045245761269" Z="0.9" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.9" />
                  <Point X="-1.768518587409" Y="-3.997175863448" Z="0.9" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.9" />
                  <Point X="-1.508174100196" Y="-4.136354713063" Z="0.9" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.9" />
                  <Point X="-1.3863555314" Y="-4.405260181043" Z="0.9" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.9" />
                  <Point X="-1.381988655126" Y="-4.643196438317" Z="0.9" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.9" />
                  <Point X="-1.222324291237" Y="-4.928587937318" Z="0.9" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.9" />
                  <Point X="-0.924022823676" Y="-4.993119120482" Z="0.9" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.9" />
                  <Point X="-0.675529531551" Y="-4.483294627226" Z="0.9" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.9" />
                  <Point X="-0.311454696303" Y="-3.366577384944" Z="0.9" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.9" />
                  <Point X="-0.089899583024" Y="-3.232140881405" Z="0.9" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.9" />
                  <Point X="0.163459496337" Y="-3.254971163883" Z="0.9" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.9" />
                  <Point X="0.358991163129" Y="-3.435068370823" Z="0.9" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.9" />
                  <Point X="0.55922532885" Y="-4.04924140259" Z="0.9" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.9" />
                  <Point X="0.934019250167" Y="-4.992626189725" Z="0.9" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.9" />
                  <Point X="1.113523084409" Y="-4.955681010205" Z="0.9" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.9" />
                  <Point X="1.099094115255" Y="-4.349599038199" Z="0.9" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.9" />
                  <Point X="0.992065073573" Y="-3.113177727123" Z="0.9" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.9" />
                  <Point X="1.127279993919" Y="-2.928776059759" Z="0.9" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.9" />
                  <Point X="1.341524134323" Y="-2.861837300187" Z="0.9" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.9" />
                  <Point X="1.56173115445" Y="-2.942626833519" Z="0.9" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.9" />
                  <Point X="2.000946365642" Y="-3.465088103477" Z="0.9" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.9" />
                  <Point X="2.788001075585" Y="-4.24512276054" Z="0.9" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.9" />
                  <Point X="2.979364785364" Y="-4.113077098128" Z="0.9" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.9" />
                  <Point X="2.771420971024" Y="-3.588642489748" Z="0.9" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.9" />
                  <Point X="2.246058659613" Y="-2.582883706757" Z="0.9" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.9" />
                  <Point X="2.293166116154" Y="-2.390388691565" Z="0.9" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.9" />
                  <Point X="2.442509686807" Y="-2.265735193814" Z="0.9" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.9" />
                  <Point X="2.645623096755" Y="-2.257389349878" Z="0.9" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.9" />
                  <Point X="3.198771107359" Y="-2.54632852925" Z="0.9" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.9" />
                  <Point X="4.177766227157" Y="-2.886450691191" Z="0.9" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.9" />
                  <Point X="4.342617910188" Y="-2.631919035607" Z="0.9" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.9" />
                  <Point X="3.97111745246" Y="-2.211860967753" Z="0.9" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.9" />
                  <Point X="3.127916009487" Y="-1.513759178623" Z="0.9" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.9" />
                  <Point X="3.102410342249" Y="-1.348023539944" Z="0.9" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.9" />
                  <Point X="3.178794961537" Y="-1.202217596435" Z="0.9" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.9" />
                  <Point X="3.334875258779" Y="-1.129923378055" Z="0.9" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.9" />
                  <Point X="3.93428056891" Y="-1.186351965634" Z="0.9" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.9" />
                  <Point X="4.961479446792" Y="-1.07570695375" Z="0.9" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.9" />
                  <Point X="5.027939956563" Y="-0.702315596748" Z="0.9" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.9" />
                  <Point X="4.58671304165" Y="-0.445555782837" Z="0.9" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.9" />
                  <Point X="3.688268098709" Y="-0.186311867707" Z="0.9" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.9" />
                  <Point X="3.61963195412" Y="-0.121706883039" Z="0.9" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.9" />
                  <Point X="3.587999803519" Y="-0.034280383793" Z="0.9" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.9" />
                  <Point X="3.593371646906" Y="0.062330147429" Z="0.9" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.9" />
                  <Point X="3.635747484281" Y="0.142241855536" Z="0.9" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.9" />
                  <Point X="3.715127315644" Y="0.201836976535" Z="0.9" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.9" />
                  <Point X="4.209254775657" Y="0.344416149932" Z="0.9" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.9" />
                  <Point X="5.005497020954" Y="0.842248090258" Z="0.9" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.9" />
                  <Point X="4.91673954577" Y="1.260974601134" Z="0.9" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.9" />
                  <Point X="4.377754717816" Y="1.342437879002" Z="0.9" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.9" />
                  <Point X="3.402371761167" Y="1.230052889837" Z="0.9" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.9" />
                  <Point X="3.324029696358" Y="1.25976079707" Z="0.9" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.9" />
                  <Point X="3.268313292827" Y="1.32079766212" Z="0.9" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.9" />
                  <Point X="3.239861523958" Y="1.401964057078" Z="0.9" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.9" />
                  <Point X="3.247478626443" Y="1.482004334961" Z="0.9" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.9" />
                  <Point X="3.29239526927" Y="1.55794746544" Z="0.9" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.9" />
                  <Point X="3.715422988955" Y="1.893563255489" Z="0.9" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.9" />
                  <Point X="4.31238897835" Y="2.678122845625" Z="0.9" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.9" />
                  <Point X="4.085973822133" Y="3.012284977761" Z="0.9" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.9" />
                  <Point X="3.472717750632" Y="2.822894571458" Z="0.9" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.9" />
                  <Point X="2.458079688258" Y="2.253147245835" Z="0.9" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.9" />
                  <Point X="2.384800863451" Y="2.250930114739" Z="0.9" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.9" />
                  <Point X="2.319321949117" Y="2.281615416156" Z="0.9" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.9" />
                  <Point X="2.269143184079" Y="2.337702911265" Z="0.9" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.9" />
                  <Point X="2.24849958802" Y="2.404957580382" Z="0.9" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.9" />
                  <Point X="2.259380696823" Y="2.481389887624" Z="0.9" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.9" />
                  <Point X="2.572730781401" Y="3.039421331323" Z="0.9" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.9" />
                  <Point X="2.886605039764" Y="4.174373513978" Z="0.9" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.9" />
                  <Point X="2.496891356311" Y="4.418531532472" Z="0.9" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.9" />
                  <Point X="2.090126214124" Y="4.624982712218" Z="0.9" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.9" />
                  <Point X="1.668354494139" Y="4.793296438064" Z="0.9" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.9" />
                  <Point X="1.094681435228" Y="4.950186377169" Z="0.9" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.9" />
                  <Point X="0.43073776201" Y="5.051451151392" Z="0.9" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.9" />
                  <Point X="0.124675455804" Y="4.820419632834" Z="0.9" />
                  <Point X="0" Y="4.355124473572" Z="0.9" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>