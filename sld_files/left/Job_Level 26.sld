<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#167" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1945" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.239359375" Y="-4.248995117188" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140625" />
                  <Point X="-24.457634765625" Y="-3.467376953125" />
                  <Point X="-24.538291015625" Y="-3.351166259766" />
                  <Point X="-24.62136328125" Y="-3.2314765625" />
                  <Point X="-24.643248046875" Y="-3.209020263672" />
                  <Point X="-24.66950390625" Y="-3.189776611328" />
                  <Point X="-24.69750390625" Y="-3.175669433594" />
                  <Point X="-24.822314453125" Y="-3.136932617188" />
                  <Point X="-24.95086328125" Y="-3.097036132812" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.161634765625" Y="-3.135772460938" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.18977734375" />
                  <Point X="-25.344439453125" Y="-3.209021484375" />
                  <Point X="-25.36632421875" Y="-3.231477050781" />
                  <Point X="-25.44698046875" Y="-3.3476875" />
                  <Point X="-25.53005078125" Y="-3.467377441406" />
                  <Point X="-25.5381875" Y="-3.481571777344" />
                  <Point X="-25.550990234375" Y="-3.512524658203" />
                  <Point X="-25.754234375" Y="-4.271043457031" />
                  <Point X="-25.9165859375" Y="-4.876941894531" />
                  <Point X="-25.9315390625" Y="-4.8740390625" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.22078125" Y="-4.808958007813" />
                  <Point X="-26.246416015625" Y="-4.802362304688" />
                  <Point X="-26.238716796875" Y="-4.743873046875" />
                  <Point X="-26.2149609375" Y="-4.563438476562" />
                  <Point X="-26.21419921875" Y="-4.5479296875" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.246326171875" Y="-4.366321289063" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.182964355469" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.4385546875" Y="-4.030430419922" />
                  <Point X="-26.556904296875" Y="-3.926639648438" />
                  <Point X="-26.5831875" Y="-3.910295898438" />
                  <Point X="-26.612884765625" Y="-3.897994628906" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.7955390625" Y="-3.880970214844" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708496094" />
                  <Point X="-27.014466796875" Y="-3.882028076172" />
                  <Point X="-27.042658203125" Y="-3.894801513672" />
                  <Point X="-27.16973828125" Y="-3.979714355469" />
                  <Point X="-27.300623046875" Y="-4.067169677734" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.3351015625" Y="-4.099461425781" />
                  <Point X="-27.449208984375" Y="-4.248166992188" />
                  <Point X="-27.4801484375" Y="-4.288488769531" />
                  <Point X="-27.58506640625" Y="-4.223526367188" />
                  <Point X="-27.801712890625" Y="-4.089383789063" />
                  <Point X="-27.997564453125" Y="-3.938585449219" />
                  <Point X="-28.10472265625" Y="-3.856077392578" />
                  <Point X="-27.790490234375" Y="-3.311811279297" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647654296875" />
                  <Point X="-27.406587890625" Y="-2.616127197266" />
                  <Point X="-27.405576171875" Y="-2.585190917969" />
                  <Point X="-27.414560546875" Y="-2.555571044922" />
                  <Point X="-27.428779296875" Y="-2.526741699219" />
                  <Point X="-27.4468046875" Y="-2.501589355469" />
                  <Point X="-27.4641484375" Y="-2.484244873047" />
                  <Point X="-27.489298828125" Y="-2.466219726562" />
                  <Point X="-27.518126953125" Y="-2.452" />
                  <Point X="-27.54774609375" Y="-2.443012451172" />
                  <Point X="-27.57868359375" Y="-2.444023925781" />
                  <Point X="-27.610212890625" Y="-2.450294677734" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.293390625" Y="-2.838904296875" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-28.911701171875" Y="-3.018728271484" />
                  <Point X="-29.082857421875" Y="-2.793863525391" />
                  <Point X="-29.223271484375" Y="-2.558412597656" />
                  <Point X="-29.306142578125" Y="-2.419450195312" />
                  <Point X="-28.747609375" Y="-1.990872558594" />
                  <Point X="-28.105955078125" Y="-1.498513305664" />
                  <Point X="-28.08458203125" Y="-1.475599365234" />
                  <Point X="-28.0666171875" Y="-1.44847253418" />
                  <Point X="-28.053857421875" Y="-1.419838989258" />
                  <Point X="-28.046150390625" Y="-1.390084594727" />
                  <Point X="-28.043345703125" Y="-1.359647705078" />
                  <Point X="-28.045556640625" Y="-1.327977661133" />
                  <Point X="-28.05255859375" Y="-1.298233398438" />
                  <Point X="-28.068642578125" Y="-1.272251098633" />
                  <Point X="-28.0894765625" Y="-1.248295776367" />
                  <Point X="-28.112974609375" Y="-1.228766113281" />
                  <Point X="-28.13945703125" Y="-1.2131796875" />
                  <Point X="-28.168720703125" Y="-1.201955444336" />
                  <Point X="-28.20060546875" Y="-1.195474609375" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-29.057802734375" Y="-1.303112304688" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.767134765625" Y="-1.254729125977" />
                  <Point X="-29.834078125" Y="-0.992649963379" />
                  <Point X="-29.8712265625" Y="-0.732911682129" />
                  <Point X="-29.892423828125" Y="-0.584698425293" />
                  <Point X="-29.26381640625" Y="-0.41626361084" />
                  <Point X="-28.532875" Y="-0.220408309937" />
                  <Point X="-28.510328125" Y="-0.21112739563" />
                  <Point X="-28.48108984375" Y="-0.194419067383" />
                  <Point X="-28.467900390625" Y="-0.185325759888" />
                  <Point X="-28.441998046875" Y="-0.16403427124" />
                  <Point X="-28.425828125" Y="-0.146979553223" />
                  <Point X="-28.414337890625" Y="-0.126477233887" />
                  <Point X="-28.40235546875" Y="-0.097054794312" />
                  <Point X="-28.39779296875" Y="-0.082671287537" />
                  <Point X="-28.390853515625" Y="-0.052728359222" />
                  <Point X="-28.388400390625" Y="-0.031333280563" />
                  <Point X="-28.390830078125" Y="-0.009935461044" />
                  <Point X="-28.3976328125" Y="0.019567884445" />
                  <Point X="-28.40217578125" Y="0.033948051453" />
                  <Point X="-28.414294921875" Y="0.063810077667" />
                  <Point X="-28.4257578125" Y="0.084325500488" />
                  <Point X="-28.44190625" Y="0.101399299622" />
                  <Point X="-28.4673984375" Y="0.12240663147" />
                  <Point X="-28.48057421875" Y="0.13151512146" />
                  <Point X="-28.51022265625" Y="0.148507751465" />
                  <Point X="-28.532875" Y="0.157848327637" />
                  <Point X="-29.28569921875" Y="0.359566955566" />
                  <Point X="-29.891814453125" Y="0.521975341797" />
                  <Point X="-29.86762890625" Y="0.685423461914" />
                  <Point X="-29.82448828125" Y="0.976969116211" />
                  <Point X="-29.74970703125" Y="1.252937988281" />
                  <Point X="-29.70355078125" Y="1.423268066406" />
                  <Point X="-29.28962109375" Y="1.368773193359" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228027344" />
                  <Point X="-28.703134765625" Y="1.305263549805" />
                  <Point X="-28.672875" Y="1.31480456543" />
                  <Point X="-28.641708984375" Y="1.324631103516" />
                  <Point X="-28.62277734375" Y="1.332961669922" />
                  <Point X="-28.604033203125" Y="1.343783569336" />
                  <Point X="-28.5873515625" Y="1.356014892578" />
                  <Point X="-28.573712890625" Y="1.371566894531" />
                  <Point X="-28.561298828125" Y="1.389296142578" />
                  <Point X="-28.551349609375" Y="1.407433837891" />
                  <Point X="-28.539208984375" Y="1.436746948242" />
                  <Point X="-28.526703125" Y="1.466938110352" />
                  <Point X="-28.520916015625" Y="1.486788085938" />
                  <Point X="-28.51715625" Y="1.508103759766" />
                  <Point X="-28.515802734375" Y="1.52874987793" />
                  <Point X="-28.518951171875" Y="1.54919934082" />
                  <Point X="-28.5245546875" Y="1.570106445312" />
                  <Point X="-28.53205078125" Y="1.589378051758" />
                  <Point X="-28.546701171875" Y="1.617521728516" />
                  <Point X="-28.561791015625" Y="1.64650769043" />
                  <Point X="-28.573283203125" Y="1.663707763672" />
                  <Point X="-28.5871953125" Y="1.680287231445" />
                  <Point X="-28.60213671875" Y="1.694590087891" />
                  <Point X="-29.03395703125" Y="2.025938720703" />
                  <Point X="-29.351859375" Y="2.269873535156" />
                  <Point X="-29.248787109375" Y="2.446462402344" />
                  <Point X="-29.0811484375" Y="2.733665283203" />
                  <Point X="-28.883064453125" Y="2.988275146484" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.528455078125" Y="3.030462158203" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.1046484375" Y="2.822109375" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.04056640625" Y="2.818762939453" />
                  <Point X="-28.019107421875" Y="2.821587890625" />
                  <Point X="-27.999015625" Y="2.826504150391" />
                  <Point X="-27.980462890625" Y="2.835652832031" />
                  <Point X="-27.962208984375" Y="2.847281738281" />
                  <Point X="-27.946076171875" Y="2.860229003906" />
                  <Point X="-27.916162109375" Y="2.890143066406" />
                  <Point X="-27.8853515625" Y="2.920952392578" />
                  <Point X="-27.872404296875" Y="2.937084716797" />
                  <Point X="-27.860775390625" Y="2.955338867188" />
                  <Point X="-27.85162890625" Y="2.973887939453" />
                  <Point X="-27.8467109375" Y="2.993977050781" />
                  <Point X="-27.843884765625" Y="3.015436035156" />
                  <Point X="-27.84343359375" Y="3.03612109375" />
                  <Point X="-27.84712109375" Y="3.078265136719" />
                  <Point X="-27.85091796875" Y="3.121670654297" />
                  <Point X="-27.854955078125" Y="3.141961669922" />
                  <Point X="-27.86146484375" Y="3.162604492188" />
                  <Point X="-27.869794921875" Y="3.181532958984" />
                  <Point X="-28.0611484375" Y="3.512966552734" />
                  <Point X="-28.18333203125" Y="3.724595947266" />
                  <Point X="-27.9925859375" Y="3.870840087891" />
                  <Point X="-27.70062109375" Y="4.094685791016" />
                  <Point X="-27.388642578125" Y="4.268013671875" />
                  <Point X="-27.167037109375" Y="4.391133789062" />
                  <Point X="-27.141708984375" Y="4.358125" />
                  <Point X="-27.0431953125" Y="4.229740722656" />
                  <Point X="-27.028890625" Y="4.214797851563" />
                  <Point X="-27.012310546875" Y="4.200885742188" />
                  <Point X="-26.99511328125" Y="4.18939453125" />
                  <Point X="-26.94820703125" Y="4.1649765625" />
                  <Point X="-26.899896484375" Y="4.139827636719" />
                  <Point X="-26.8806171875" Y="4.132331054688" />
                  <Point X="-26.8597109375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.12358203125" />
                  <Point X="-26.818625" Y="4.124935058594" />
                  <Point X="-26.79730859375" Y="4.128693847656" />
                  <Point X="-26.777451171875" Y="4.134481933594" />
                  <Point X="-26.728595703125" Y="4.15471875" />
                  <Point X="-26.67827734375" Y="4.175561523437" />
                  <Point X="-26.660142578125" Y="4.18551171875" />
                  <Point X="-26.6424140625" Y="4.19792578125" />
                  <Point X="-26.62686328125" Y="4.2115625" />
                  <Point X="-26.6146328125" Y="4.228241210938" />
                  <Point X="-26.603810546875" Y="4.246984375" />
                  <Point X="-26.595478515625" Y="4.265922363281" />
                  <Point X="-26.579578125" Y="4.316355957031" />
                  <Point X="-26.56319921875" Y="4.368298828125" />
                  <Point X="-26.5591640625" Y="4.388583007812" />
                  <Point X="-26.55727734375" Y="4.410145507812" />
                  <Point X="-26.557728515625" Y="4.430826660156" />
                  <Point X="-26.579484375" Y="4.5960703125" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.32759375" Y="4.703841796875" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.571421875" Y="4.854072265625" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.2291015625" Y="4.641597167969" />
                  <Point X="-25.133904296875" Y="4.286315917969" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286314941406" />
                  <Point X="-24.755734375" Y="4.652232421875" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.485978515625" Y="4.86630078125" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.843048828125" Y="4.756192871094" />
                  <Point X="-23.51897265625" Y="4.677950195312" />
                  <Point X="-23.31605078125" Y="4.604350097656" />
                  <Point X="-23.105359375" Y="4.527930664062" />
                  <Point X="-22.908408203125" Y="4.435823242188" />
                  <Point X="-22.705419921875" Y="4.340891601563" />
                  <Point X="-22.515138671875" Y="4.230033691406" />
                  <Point X="-22.31902734375" Y="4.115777832031" />
                  <Point X="-22.139580078125" Y="3.988166748047" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.42795703125" Y="3.286284667969" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857919921875" Y="2.5399375" />
                  <Point X="-22.866921875" Y="2.516057373047" />
                  <Point X="-22.877498046875" Y="2.476506347656" />
                  <Point X="-22.888392578125" Y="2.435770996094" />
                  <Point X="-22.891380859375" Y="2.417937011719" />
                  <Point X="-22.892271484375" Y="2.380951904297" />
                  <Point X="-22.888146484375" Y="2.346751464844" />
                  <Point X="-22.883900390625" Y="2.311527099609" />
                  <Point X="-22.878556640625" Y="2.289605224609" />
                  <Point X="-22.8702890625" Y="2.267513671875" />
                  <Point X="-22.859927734375" Y="2.247469970703" />
                  <Point X="-22.838765625" Y="2.216282470703" />
                  <Point X="-22.81696875" Y="2.184161376953" />
                  <Point X="-22.805533203125" Y="2.170328125" />
                  <Point X="-22.778400390625" Y="2.145592529297" />
                  <Point X="-22.747212890625" Y="2.124430419922" />
                  <Point X="-22.715091796875" Y="2.102635009766" />
                  <Point X="-22.695046875" Y="2.092272216797" />
                  <Point X="-22.672958984375" Y="2.084006347656" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.6168359375" Y="2.074539550781" />
                  <Point X="-22.581611328125" Y="2.070291992188" />
                  <Point X="-22.563533203125" Y="2.069845458984" />
                  <Point X="-22.52679296875" Y="2.074170898438" />
                  <Point X="-22.4872421875" Y="2.084747558594" />
                  <Point X="-22.446505859375" Y="2.095640625" />
                  <Point X="-22.434716796875" Y="2.099637939453" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.65426953125" Y="2.5473125" />
                  <Point X="-21.032671875" Y="2.906191650391" />
                  <Point X="-20.993056640625" Y="2.851135498047" />
                  <Point X="-20.87673046875" Y="2.689468017578" />
                  <Point X="-20.77669140625" Y="2.524152587891" />
                  <Point X="-20.73780078125" Y="2.459883789063" />
                  <Point X="-21.210373046875" Y="2.097265625" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.778572265625" Y="1.660243408203" />
                  <Point X="-21.79602734375" Y="1.641627075195" />
                  <Point X="-21.8244921875" Y="1.604492431641" />
                  <Point X="-21.85380859375" Y="1.566245849609" />
                  <Point X="-21.863392578125" Y="1.550911743164" />
                  <Point X="-21.878369140625" Y="1.517088134766" />
                  <Point X="-21.88897265625" Y="1.479173461914" />
                  <Point X="-21.89989453125" Y="1.440123779297" />
                  <Point X="-21.90334765625" Y="1.417825317383" />
                  <Point X="-21.9041640625" Y="1.39425378418" />
                  <Point X="-21.902259765625" Y="1.371766113281" />
                  <Point X="-21.8935546875" Y="1.329581298828" />
                  <Point X="-21.88458984375" Y="1.286133300781" />
                  <Point X="-21.8793203125" Y="1.26898046875" />
                  <Point X="-21.86371875" Y="1.235741455078" />
                  <Point X="-21.840044921875" Y="1.199757446289" />
                  <Point X="-21.815662109375" Y="1.162696166992" />
                  <Point X="-21.801107421875" Y="1.145449951172" />
                  <Point X="-21.78386328125" Y="1.129360229492" />
                  <Point X="-21.76565234375" Y="1.116034667969" />
                  <Point X="-21.73134375" Y="1.096722412109" />
                  <Point X="-21.696009765625" Y="1.07683215332" />
                  <Point X="-21.679478515625" Y="1.069501342773" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.597494140625" Y="1.053308105469" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.792509765625" Y="1.141680297852" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.20402734375" Y="1.138043823242" />
                  <Point X="-20.154060546875" Y="0.932788818359" />
                  <Point X="-20.1225390625" Y="0.730334899902" />
                  <Point X="-20.109134765625" Y="0.644238647461" />
                  <Point X="-20.642974609375" Y="0.501196350098" />
                  <Point X="-21.283419921875" Y="0.329589874268" />
                  <Point X="-21.295208984375" Y="0.325586120605" />
                  <Point X="-21.318453125" Y="0.315067810059" />
                  <Point X="-21.364025390625" Y="0.288725799561" />
                  <Point X="-21.410962890625" Y="0.261595367432" />
                  <Point X="-21.4256875" Y="0.251097106934" />
                  <Point X="-21.45246875" Y="0.225576583862" />
                  <Point X="-21.4798125" Y="0.190734161377" />
                  <Point X="-21.507974609375" Y="0.154848937988" />
                  <Point X="-21.51969921875" Y="0.135566558838" />
                  <Point X="-21.52947265625" Y="0.114102180481" />
                  <Point X="-21.536318359375" Y="0.092604560852" />
                  <Point X="-21.54543359375" Y="0.04501159668" />
                  <Point X="-21.5548203125" Y="-0.004005921364" />
                  <Point X="-21.556515625" Y="-0.021876050949" />
                  <Point X="-21.5548203125" Y="-0.058554073334" />
                  <Point X="-21.545705078125" Y="-0.106147041321" />
                  <Point X="-21.536318359375" Y="-0.155164703369" />
                  <Point X="-21.52947265625" Y="-0.176662322998" />
                  <Point X="-21.51969921875" Y="-0.198126693726" />
                  <Point X="-21.507974609375" Y="-0.217409088135" />
                  <Point X="-21.480630859375" Y="-0.252251358032" />
                  <Point X="-21.45246875" Y="-0.288136871338" />
                  <Point X="-21.44" Y="-0.301236694336" />
                  <Point X="-21.410962890625" Y="-0.32415536499" />
                  <Point X="-21.365390625" Y="-0.350497375488" />
                  <Point X="-21.318453125" Y="-0.377627929688" />
                  <Point X="-21.3072890625" Y="-0.38313885498" />
                  <Point X="-21.283419921875" Y="-0.392149871826" />
                  <Point X="-20.623802734375" Y="-0.568893859863" />
                  <Point X="-20.108525390625" Y="-0.706961853027" />
                  <Point X="-20.117078125" Y="-0.763688720703" />
                  <Point X="-20.144974609375" Y="-0.948723754883" />
                  <Point X="-20.18536328125" Y="-1.125709838867" />
                  <Point X="-20.19882421875" Y="-1.18469921875" />
                  <Point X="-20.8328828125" Y="-1.101223999023" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710144043" />
                  <Point X="-21.62533984375" Y="-1.005508728027" />
                  <Point X="-21.714783203125" Y="-1.024949584961" />
                  <Point X="-21.806904296875" Y="-1.044972412109" />
                  <Point X="-21.836025390625" Y="-1.056597167969" />
                  <Point X="-21.8638515625" Y="-1.073489379883" />
                  <Point X="-21.8876015625" Y="-1.093960205078" />
                  <Point X="-21.9416640625" Y="-1.158980957031" />
                  <Point X="-21.997345703125" Y="-1.225948242188" />
                  <Point X="-22.012064453125" Y="-1.250329223633" />
                  <Point X="-22.023408203125" Y="-1.277714599609" />
                  <Point X="-22.030240234375" Y="-1.305365966797" />
                  <Point X="-22.03798828125" Y="-1.389570678711" />
                  <Point X="-22.04596875" Y="-1.476296386719" />
                  <Point X="-22.04365234375" Y="-1.507560913086" />
                  <Point X="-22.035921875" Y="-1.539182495117" />
                  <Point X="-22.023548828125" Y="-1.567996337891" />
                  <Point X="-21.97405078125" Y="-1.644989135742" />
                  <Point X="-21.923068359375" Y="-1.724286865234" />
                  <Point X="-21.9130625" Y="-1.737244140625" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.277240234375" Y="-2.230612792969" />
                  <Point X="-20.786876953125" Y="-2.6068828125" />
                  <Point X="-20.796548828125" Y="-2.622533935547" />
                  <Point X="-20.87519921875" Y="-2.749800537109" />
                  <Point X="-20.958712890625" Y="-2.868462890625" />
                  <Point X="-20.971017578125" Y="-2.885945556641" />
                  <Point X="-21.5375390625" Y="-2.558864257812" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.2457734375" Y="-2.159825195312" />
                  <Point X="-22.352224609375" Y="-2.140600097656" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135176757812" />
                  <Point X="-22.6436015625" Y="-2.181719482422" />
                  <Point X="-22.73468359375" Y="-2.229655761719" />
                  <Point X="-22.757615234375" Y="-2.246549072266" />
                  <Point X="-22.77857421875" Y="-2.267508300781" />
                  <Point X="-22.795466796875" Y="-2.290438720703" />
                  <Point X="-22.842009765625" Y="-2.378874023438" />
                  <Point X="-22.889947265625" Y="-2.469956787109" />
                  <Point X="-22.899771484375" Y="-2.499734863281" />
                  <Point X="-22.904728515625" Y="-2.531908203125" />
                  <Point X="-22.90432421875" Y="-2.563259521484" />
                  <Point X="-22.88509765625" Y="-2.669711181641" />
                  <Point X="-22.865296875" Y="-2.779349853516" />
                  <Point X="-22.86101171875" Y="-2.795139648438" />
                  <Point X="-22.8481796875" Y="-2.826078613281" />
                  <Point X="-22.454826171875" Y="-3.507388671875" />
                  <Point X="-22.13871484375" Y="-4.054907226563" />
                  <Point X="-22.218126953125" Y="-4.11162890625" />
                  <Point X="-22.298234375" Y="-4.163480957031" />
                  <Point X="-22.736662109375" Y="-3.592110351562" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.383064453125" Y="-2.833058105469" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.697724609375" Y="-2.751683105469" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-23.98406640625" Y="-2.869182861328" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968860839844" />
                  <Point X="-24.11275" Y="-2.996687255859" />
                  <Point X="-24.124375" Y="-3.02580859375" />
                  <Point X="-24.150884765625" Y="-3.147776855469" />
                  <Point X="-24.178189453125" Y="-3.273396484375" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323120361328" />
                  <Point X="-24.068783203125" Y="-4.169846679688" />
                  <Point X="-23.977935546875" Y="-4.859915527344" />
                  <Point X="-24.024318359375" Y="-4.870083007812" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.058421875" Y="-4.752637207031" />
                  <Point X="-26.14124609375" Y="-4.731328125" />
                  <Point X="-26.1207734375" Y="-4.575836914062" />
                  <Point X="-26.120076171875" Y="-4.568098632812" />
                  <Point X="-26.11944921875" Y="-4.541030761719" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497688964844" />
                  <Point X="-26.15315234375" Y="-4.347787109375" />
                  <Point X="-26.18386328125" Y="-4.193397460938" />
                  <Point X="-26.188125" Y="-4.178468261719" />
                  <Point X="-26.19902734375" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135462890625" />
                  <Point X="-26.221740234375" Y="-4.107625976562" />
                  <Point X="-26.23057421875" Y="-4.094861083984" />
                  <Point X="-26.25020703125" Y="-4.070937744141" />
                  <Point X="-26.261005859375" Y="-4.059779296875" />
                  <Point X="-26.375916015625" Y="-3.959005615234" />
                  <Point X="-26.494265625" Y="-3.85521484375" />
                  <Point X="-26.50673828125" Y="-3.845965087891" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822527587891" />
                  <Point X="-26.576529296875" Y="-3.810226318359" />
                  <Point X="-26.5913125" Y="-3.805476318359" />
                  <Point X="-26.621455078125" Y="-3.798447998047" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.789326171875" Y="-3.786173583984" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166015625" />
                  <Point X="-27.008005859375" Y="-3.781945556641" />
                  <Point X="-27.0390546875" Y="-3.790265136719" />
                  <Point X="-27.053673828125" Y="-3.79549609375" />
                  <Point X="-27.081865234375" Y="-3.80826953125" />
                  <Point X="-27.0954375" Y="-3.815812011719" />
                  <Point X="-27.222517578125" Y="-3.900724853516" />
                  <Point X="-27.35340234375" Y="-3.988180175781" />
                  <Point X="-27.35968359375" Y="-3.992759765625" />
                  <Point X="-27.380439453125" Y="-4.010130859375" />
                  <Point X="-27.402755859375" Y="-4.032769287109" />
                  <Point X="-27.410470703125" Y="-4.041628662109" />
                  <Point X="-27.503203125" Y="-4.162477539063" />
                  <Point X="-27.5350546875" Y="-4.142755859375" />
                  <Point X="-27.747591796875" Y="-4.011156738281" />
                  <Point X="-27.939607421875" Y="-3.863312744141" />
                  <Point X="-27.98086328125" Y="-3.831546630859" />
                  <Point X="-27.70821875" Y="-3.359311279297" />
                  <Point X="-27.341490234375" Y="-2.724119628906" />
                  <Point X="-27.3348515625" Y="-2.710084960938" />
                  <Point X="-27.32394921875" Y="-2.681119628906" />
                  <Point X="-27.319685546875" Y="-2.666188964844" />
                  <Point X="-27.3134140625" Y="-2.634661865234" />
                  <Point X="-27.311638671875" Y="-2.619232421875" />
                  <Point X="-27.310626953125" Y="-2.588296142578" />
                  <Point X="-27.314666015625" Y="-2.557615966797" />
                  <Point X="-27.323650390625" Y="-2.52799609375" />
                  <Point X="-27.329359375" Y="-2.513549560547" />
                  <Point X="-27.343578125" Y="-2.484720214844" />
                  <Point X="-27.351560546875" Y="-2.471403320312" />
                  <Point X="-27.3695859375" Y="-2.446250976562" />
                  <Point X="-27.37962890625" Y="-2.434415527344" />
                  <Point X="-27.39697265625" Y="-2.417071044922" />
                  <Point X="-27.40880859375" Y="-2.407028320312" />
                  <Point X="-27.433958984375" Y="-2.389003173828" />
                  <Point X="-27.4472734375" Y="-2.381020751953" />
                  <Point X="-27.4761015625" Y="-2.366801025391" />
                  <Point X="-27.49054296875" Y="-2.361093017578" />
                  <Point X="-27.520162109375" Y="-2.35210546875" />
                  <Point X="-27.550849609375" Y="-2.348063232422" />
                  <Point X="-27.581787109375" Y="-2.349074707031" />
                  <Point X="-27.59721484375" Y="-2.350848876953" />
                  <Point X="-27.628744140625" Y="-2.357119628906" />
                  <Point X="-27.64367578125" Y="-2.361383056641" />
                  <Point X="-27.67264453125" Y="-2.372285644531" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.340890625" Y="-2.756631835938" />
                  <Point X="-28.793087890625" Y="-3.017708251953" />
                  <Point X="-28.836107421875" Y="-2.961189941406" />
                  <Point X="-29.00401953125" Y="-2.740588134766" />
                  <Point X="-29.1416796875" Y="-2.50975390625" />
                  <Point X="-29.181265625" Y="-2.443373779297" />
                  <Point X="-28.68977734375" Y="-2.066241210938" />
                  <Point X="-28.048123046875" Y="-1.573881835938" />
                  <Point X="-28.036484375" Y="-1.563312011719" />
                  <Point X="-28.015111328125" Y="-1.540398071289" />
                  <Point X="-28.005376953125" Y="-1.528053710938" />
                  <Point X="-27.987412109375" Y="-1.500926879883" />
                  <Point X="-27.97984375" Y="-1.487140991211" />
                  <Point X="-27.967083984375" Y="-1.458507446289" />
                  <Point X="-27.961892578125" Y="-1.443659912109" />
                  <Point X="-27.954185546875" Y="-1.413905517578" />
                  <Point X="-27.95155078125" Y="-1.398801635742" />
                  <Point X="-27.94874609375" Y="-1.368364746094" />
                  <Point X="-27.948576171875" Y="-1.353031738281" />
                  <Point X="-27.950787109375" Y="-1.321361694336" />
                  <Point X="-27.953083984375" Y="-1.306209106445" />
                  <Point X="-27.9600859375" Y="-1.27646496582" />
                  <Point X="-27.971783203125" Y="-1.24823034668" />
                  <Point X="-27.9878671875" Y="-1.222248046875" />
                  <Point X="-27.996958984375" Y="-1.209908447266" />
                  <Point X="-28.01779296875" Y="-1.185953369141" />
                  <Point X="-28.02875390625" Y="-1.175235229492" />
                  <Point X="-28.052251953125" Y="-1.155705566406" />
                  <Point X="-28.0647890625" Y="-1.146893920898" />
                  <Point X="-28.091271484375" Y="-1.131307495117" />
                  <Point X="-28.105435546875" Y="-1.12448034668" />
                  <Point X="-28.13469921875" Y="-1.113256103516" />
                  <Point X="-28.149798828125" Y="-1.108859008789" />
                  <Point X="-28.18168359375" Y="-1.102378173828" />
                  <Point X="-28.197298828125" Y="-1.100532104492" />
                  <Point X="-28.22862109375" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-29.070203125" Y="-1.208925048828" />
                  <Point X="-29.66091796875" Y="-1.286694335938" />
                  <Point X="-29.67508984375" Y="-1.231217285156" />
                  <Point X="-29.740763671875" Y="-0.97411138916" />
                  <Point X="-29.77718359375" Y="-0.719461425781" />
                  <Point X="-29.786453125" Y="-0.654654663086" />
                  <Point X="-29.239228515625" Y="-0.508026580811" />
                  <Point X="-28.508287109375" Y="-0.312171325684" />
                  <Point X="-28.49671484375" Y="-0.308256896973" />
                  <Point X="-28.47416796875" Y="-0.298976013184" />
                  <Point X="-28.463193359375" Y="-0.293609527588" />
                  <Point X="-28.433955078125" Y="-0.27690133667" />
                  <Point X="-28.427166015625" Y="-0.272632293701" />
                  <Point X="-28.407576171875" Y="-0.258714508057" />
                  <Point X="-28.381673828125" Y="-0.237423080444" />
                  <Point X="-28.37305859375" Y="-0.229397293091" />
                  <Point X="-28.356888671875" Y="-0.212342666626" />
                  <Point X="-28.342955078125" Y="-0.193424316406" />
                  <Point X="-28.33146484375" Y="-0.172922088623" />
                  <Point X="-28.326353515625" Y="-0.162308914185" />
                  <Point X="-28.31437109375" Y="-0.132886489868" />
                  <Point X="-28.311802734375" Y="-0.125778747559" />
                  <Point X="-28.30524609375" Y="-0.104119636536" />
                  <Point X="-28.298306640625" Y="-0.074176727295" />
                  <Point X="-28.29647265625" Y="-0.063550041199" />
                  <Point X="-28.29401953125" Y="-0.042154884338" />
                  <Point X="-28.2940078125" Y="-0.020615116119" />
                  <Point X="-28.2964375" Y="0.000782717347" />
                  <Point X="-28.298259765625" Y="0.011409106255" />
                  <Point X="-28.3050625" Y="0.040912536621" />
                  <Point X="-28.307044921875" Y="0.048185993195" />
                  <Point X="-28.3141484375" Y="0.069672698975" />
                  <Point X="-28.326267578125" Y="0.099534759521" />
                  <Point X="-28.33136328125" Y="0.110148216248" />
                  <Point X="-28.342826171875" Y="0.130663528442" />
                  <Point X="-28.35673828125" Y="0.149604171753" />
                  <Point X="-28.37288671875" Y="0.166678131104" />
                  <Point X="-28.381490234375" Y="0.174713272095" />
                  <Point X="-28.406982421875" Y="0.19572052002" />
                  <Point X="-28.413376953125" Y="0.200551513672" />
                  <Point X="-28.433333984375" Y="0.213937530518" />
                  <Point X="-28.462982421875" Y="0.23093019104" />
                  <Point X="-28.4740078125" Y="0.236334259033" />
                  <Point X="-28.49666015625" Y="0.245674758911" />
                  <Point X="-28.508287109375" Y="0.249611343384" />
                  <Point X="-29.261111328125" Y="0.451329925537" />
                  <Point X="-29.7854453125" Y="0.591824829102" />
                  <Point X="-29.77365234375" Y="0.671517578125" />
                  <Point X="-29.73133203125" Y="0.957523376465" />
                  <Point X="-29.658013671875" Y="1.228091186523" />
                  <Point X="-29.6335859375" Y="1.318237304688" />
                  <Point X="-29.302021484375" Y="1.2745859375" />
                  <Point X="-28.778064453125" Y="1.20560559082" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.20470324707" />
                  <Point X="-28.71514453125" Y="1.206589477539" />
                  <Point X="-28.704890625" Y="1.208053222656" />
                  <Point X="-28.6846015625" Y="1.212088745117" />
                  <Point X="-28.67456640625" Y="1.214660522461" />
                  <Point X="-28.644306640625" Y="1.224201538086" />
                  <Point X="-28.613140625" Y="1.234028076172" />
                  <Point X="-28.603447265625" Y="1.237677246094" />
                  <Point X="-28.584515625" Y="1.2460078125" />
                  <Point X="-28.57527734375" Y="1.250689331055" />
                  <Point X="-28.556533203125" Y="1.261511108398" />
                  <Point X="-28.547859375" Y="1.267171020508" />
                  <Point X="-28.531177734375" Y="1.27940234375" />
                  <Point X="-28.51592578125" Y="1.293377197266" />
                  <Point X="-28.502287109375" Y="1.308929199219" />
                  <Point X="-28.495892578125" Y="1.317077392578" />
                  <Point X="-28.483478515625" Y="1.334806518555" />
                  <Point X="-28.4780078125" Y="1.343607421875" />
                  <Point X="-28.46805859375" Y="1.361745117188" />
                  <Point X="-28.463580078125" Y="1.37108215332" />
                  <Point X="-28.451439453125" Y="1.400395141602" />
                  <Point X="-28.43893359375" Y="1.430586425781" />
                  <Point X="-28.4355" Y="1.440348388672" />
                  <Point X="-28.429712890625" Y="1.460198486328" />
                  <Point X="-28.427359375" Y="1.470286254883" />
                  <Point X="-28.423599609375" Y="1.491601928711" />
                  <Point X="-28.422359375" Y="1.501889160156" />
                  <Point X="-28.421005859375" Y="1.52253527832" />
                  <Point X="-28.421908203125" Y="1.543205932617" />
                  <Point X="-28.425056640625" Y="1.563655395508" />
                  <Point X="-28.427189453125" Y="1.573793212891" />
                  <Point X="-28.43279296875" Y="1.594700317383" />
                  <Point X="-28.436017578125" Y="1.604545043945" />
                  <Point X="-28.443513671875" Y="1.623816772461" />
                  <Point X="-28.44778515625" Y="1.633243530273" />
                  <Point X="-28.462435546875" Y="1.661387207031" />
                  <Point X="-28.477525390625" Y="1.690373168945" />
                  <Point X="-28.48280078125" Y="1.69928515625" />
                  <Point X="-28.49429296875" Y="1.716485229492" />
                  <Point X="-28.500509765625" Y="1.72477331543" />
                  <Point X="-28.514421875" Y="1.741352783203" />
                  <Point X="-28.521501953125" Y="1.748912719727" />
                  <Point X="-28.536443359375" Y="1.763215698242" />
                  <Point X="-28.5443046875" Y="1.769958496094" />
                  <Point X="-28.976125" Y="2.101307128906" />
                  <Point X="-29.22761328125" Y="2.29428125" />
                  <Point X="-29.166740234375" Y="2.398573242188" />
                  <Point X="-29.00228515625" Y="2.680321777344" />
                  <Point X="-28.808083984375" Y="2.929940917969" />
                  <Point X="-28.726337890625" Y="3.035013427734" />
                  <Point X="-28.575955078125" Y="2.948189697266" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.112927734375" Y="2.727470947266" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.038494140625" Y="2.723785644531" />
                  <Point X="-28.02816796875" Y="2.724575683594" />
                  <Point X="-28.006708984375" Y="2.727400634766" />
                  <Point X="-27.99652734375" Y="2.729310302734" />
                  <Point X="-27.976435546875" Y="2.7342265625" />
                  <Point X="-27.957" Y="2.741300292969" />
                  <Point X="-27.938447265625" Y="2.750448974609" />
                  <Point X="-27.929419921875" Y="2.755530517578" />
                  <Point X="-27.911166015625" Y="2.767159423828" />
                  <Point X="-27.902748046875" Y="2.773191162109" />
                  <Point X="-27.886615234375" Y="2.786138427734" />
                  <Point X="-27.878900390625" Y="2.793053955078" />
                  <Point X="-27.848986328125" Y="2.822968017578" />
                  <Point X="-27.81817578125" Y="2.85377734375" />
                  <Point X="-27.81126171875" Y="2.861490478516" />
                  <Point X="-27.798314453125" Y="2.877622802734" />
                  <Point X="-27.79228125" Y="2.886041992188" />
                  <Point X="-27.78065234375" Y="2.904296142578" />
                  <Point X="-27.7755703125" Y="2.913324707031" />
                  <Point X="-27.766423828125" Y="2.931873779297" />
                  <Point X="-27.759353515625" Y="2.951298339844" />
                  <Point X="-27.754435546875" Y="2.971387451172" />
                  <Point X="-27.7525234375" Y="2.981572509766" />
                  <Point X="-27.749697265625" Y="3.003031494141" />
                  <Point X="-27.748908203125" Y="3.013364501953" />
                  <Point X="-27.74845703125" Y="3.034049560547" />
                  <Point X="-27.748794921875" Y="3.044401611328" />
                  <Point X="-27.752482421875" Y="3.086545654297" />
                  <Point X="-27.756279296875" Y="3.129951171875" />
                  <Point X="-27.757744140625" Y="3.140208496094" />
                  <Point X="-27.76178125" Y="3.160499511719" />
                  <Point X="-27.764353515625" Y="3.170533203125" />
                  <Point X="-27.77086328125" Y="3.191176025391" />
                  <Point X="-27.77451171875" Y="3.200870605469" />
                  <Point X="-27.782841796875" Y="3.219799072266" />
                  <Point X="-27.7875234375" Y="3.229032958984" />
                  <Point X="-27.978876953125" Y="3.560466552734" />
                  <Point X="-28.05938671875" Y="3.699915039062" />
                  <Point X="-27.934783203125" Y="3.795448486328" />
                  <Point X="-27.6483671875" Y="4.015040527344" />
                  <Point X="-27.342505859375" Y="4.184969238281" />
                  <Point X="-27.192525390625" Y="4.268295898437" />
                  <Point X="-27.118564453125" Y="4.171909179688" />
                  <Point X="-27.1118203125" Y="4.164046875" />
                  <Point X="-27.097515625" Y="4.149104003906" />
                  <Point X="-27.089955078125" Y="4.142022949219" />
                  <Point X="-27.073375" Y="4.128110839844" />
                  <Point X="-27.065091796875" Y="4.121896972656" />
                  <Point X="-27.04789453125" Y="4.110405761719" />
                  <Point X="-27.03898046875" Y="4.105128417969" />
                  <Point X="-26.99207421875" Y="4.080710693359" />
                  <Point X="-26.943763671875" Y="4.055561767578" />
                  <Point X="-26.934326171875" Y="4.051285644531" />
                  <Point X="-26.915046875" Y="4.0437890625" />
                  <Point X="-26.905205078125" Y="4.040568359375" />
                  <Point X="-26.884298828125" Y="4.034966308594" />
                  <Point X="-26.8741640625" Y="4.032834960938" />
                  <Point X="-26.853720703125" Y="4.029687988281" />
                  <Point X="-26.8330546875" Y="4.028785400391" />
                  <Point X="-26.812412109375" Y="4.030138427734" />
                  <Point X="-26.802126953125" Y="4.031378417969" />
                  <Point X="-26.780810546875" Y="4.035137207031" />
                  <Point X="-26.770724609375" Y="4.037489257812" />
                  <Point X="-26.7508671875" Y="4.04327734375" />
                  <Point X="-26.741095703125" Y="4.046713623047" />
                  <Point X="-26.692240234375" Y="4.066950439453" />
                  <Point X="-26.641921875" Y="4.087793212891" />
                  <Point X="-26.632580078125" Y="4.092274658203" />
                  <Point X="-26.6144453125" Y="4.102224609375" />
                  <Point X="-26.60565234375" Y="4.107693359375" />
                  <Point X="-26.587923828125" Y="4.120107421875" />
                  <Point X="-26.579779296875" Y="4.126499023438" />
                  <Point X="-26.564228515625" Y="4.140135742188" />
                  <Point X="-26.55025390625" Y="4.155384765625" />
                  <Point X="-26.5380234375" Y="4.172063476562" />
                  <Point X="-26.532361328125" Y="4.18073828125" />
                  <Point X="-26.5215390625" Y="4.199481445312" />
                  <Point X="-26.516853515625" Y="4.208727050781" />
                  <Point X="-26.508521484375" Y="4.227665039062" />
                  <Point X="-26.504875" Y="4.237357421875" />
                  <Point X="-26.488974609375" Y="4.287791015625" />
                  <Point X="-26.472595703125" Y="4.339733886719" />
                  <Point X="-26.470025390625" Y="4.349763671875" />
                  <Point X="-26.465990234375" Y="4.370047851562" />
                  <Point X="-26.464525390625" Y="4.380302246094" />
                  <Point X="-26.462638671875" Y="4.401864746094" />
                  <Point X="-26.46230078125" Y="4.412217285156" />
                  <Point X="-26.462751953125" Y="4.4328984375" />
                  <Point X="-26.463541015625" Y="4.443227050781" />
                  <Point X="-26.479265625" Y="4.562655273438" />
                  <Point X="-26.301947265625" Y="4.612369140625" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.56037890625" Y="4.759716308594" />
                  <Point X="-25.36522265625" Y="4.782556152344" />
                  <Point X="-25.320865234375" Y="4.617009765625" />
                  <Point X="-25.22566796875" Y="4.261728515625" />
                  <Point X="-25.220435546875" Y="4.247106933594" />
                  <Point X="-25.20766015625" Y="4.218912597656" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218911621094" />
                  <Point X="-24.767251953125" Y="4.247104980469" />
                  <Point X="-24.76201953125" Y="4.2617265625" />
                  <Point X="-24.663970703125" Y="4.627644042969" />
                  <Point X="-24.621806640625" Y="4.785005859375" />
                  <Point X="-24.495873046875" Y="4.771817382812" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.86534375" Y="4.663846191406" />
                  <Point X="-23.54640234375" Y="4.586843261719" />
                  <Point X="-23.348443359375" Y="4.51504296875" />
                  <Point X="-23.141744140625" Y="4.440071777344" />
                  <Point X="-22.94865234375" Y="4.349769042969" />
                  <Point X="-22.74954296875" Y="4.256651367187" />
                  <Point X="-22.5629609375" Y="4.147948730469" />
                  <Point X="-22.370578125" Y="4.035865478516" />
                  <Point X="-22.19463671875" Y="3.910746826172" />
                  <Point X="-22.18221875" Y="3.901915283203" />
                  <Point X="-22.510228515625" Y="3.333784667969" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.937619140625" Y="2.593116699219" />
                  <Point X="-22.946814453125" Y="2.573447265625" />
                  <Point X="-22.95581640625" Y="2.549567138672" />
                  <Point X="-22.958697265625" Y="2.540598632812" />
                  <Point X="-22.9692734375" Y="2.501047607422" />
                  <Point X="-22.98016796875" Y="2.460312255859" />
                  <Point X="-22.9820859375" Y="2.451470458984" />
                  <Point X="-22.986353515625" Y="2.420224121094" />
                  <Point X="-22.987244140625" Y="2.383239013672" />
                  <Point X="-22.986587890625" Y="2.369576171875" />
                  <Point X="-22.982462890625" Y="2.335375732422" />
                  <Point X="-22.978216796875" Y="2.300151367188" />
                  <Point X="-22.976197265625" Y="2.289028320312" />
                  <Point X="-22.970853515625" Y="2.267106445312" />
                  <Point X="-22.967529296875" Y="2.256307617188" />
                  <Point X="-22.95926171875" Y="2.234216064453" />
                  <Point X="-22.9546796875" Y="2.223888671875" />
                  <Point X="-22.944318359375" Y="2.203844970703" />
                  <Point X="-22.9385390625" Y="2.194128662109" />
                  <Point X="-22.917376953125" Y="2.162941162109" />
                  <Point X="-22.895580078125" Y="2.130820068359" />
                  <Point X="-22.890189453125" Y="2.123632324219" />
                  <Point X="-22.86953515625" Y="2.100123291016" />
                  <Point X="-22.84240234375" Y="2.075387695312" />
                  <Point X="-22.8317421875" Y="2.066981445312" />
                  <Point X="-22.8005546875" Y="2.045819213867" />
                  <Point X="-22.76843359375" Y="2.024023925781" />
                  <Point X="-22.75871875" Y="2.018245239258" />
                  <Point X="-22.738673828125" Y="2.007882324219" />
                  <Point X="-22.72834375" Y="2.003298461914" />
                  <Point X="-22.706255859375" Y="1.995032470703" />
                  <Point X="-22.695451171875" Y="1.991707397461" />
                  <Point X="-22.67352734375" Y="1.986364746094" />
                  <Point X="-22.662408203125" Y="1.984346801758" />
                  <Point X="-22.628208984375" Y="1.98022277832" />
                  <Point X="-22.592984375" Y="1.975975219727" />
                  <Point X="-22.58395703125" Y="1.975320922852" />
                  <Point X="-22.55242578125" Y="1.975496948242" />
                  <Point X="-22.515685546875" Y="1.979822509766" />
                  <Point X="-22.50225" Y="1.982395751953" />
                  <Point X="-22.46269921875" Y="1.992972412109" />
                  <Point X="-22.421962890625" Y="2.003865356445" />
                  <Point X="-22.416" Y="2.00567175293" />
                  <Point X="-22.39559765625" Y="2.01306640625" />
                  <Point X="-22.372345703125" Y="2.023573486328" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.60676953125" Y="2.465040039062" />
                  <Point X="-21.05959375" Y="2.780951660156" />
                  <Point X="-20.956048828125" Y="2.637047607422" />
                  <Point X="-20.86311328125" Y="2.483471679688" />
                  <Point X="-21.268205078125" Y="2.172634033203" />
                  <Point X="-21.827046875" Y="1.743819946289" />
                  <Point X="-21.831859375" Y="1.739869506836" />
                  <Point X="-21.847875" Y="1.725222290039" />
                  <Point X="-21.865330078125" Y="1.706605957031" />
                  <Point X="-21.87142578125" Y="1.699421630859" />
                  <Point X="-21.899890625" Y="1.662286987305" />
                  <Point X="-21.92920703125" Y="1.624040405273" />
                  <Point X="-21.9343671875" Y="1.616596435547" />
                  <Point X="-21.9502578125" Y="1.589374389648" />
                  <Point X="-21.965234375" Y="1.55555078125" />
                  <Point X="-21.969859375" Y="1.542674926758" />
                  <Point X="-21.980462890625" Y="1.504760131836" />
                  <Point X="-21.991384765625" Y="1.465710449219" />
                  <Point X="-21.993775390625" Y="1.454662231445" />
                  <Point X="-21.997228515625" Y="1.432363647461" />
                  <Point X="-21.998291015625" Y="1.421113647461" />
                  <Point X="-21.999107421875" Y="1.397542114258" />
                  <Point X="-21.998826171875" Y="1.386237670898" />
                  <Point X="-21.996921875" Y="1.36375" />
                  <Point X="-21.995298828125" Y="1.352566894531" />
                  <Point X="-21.98659375" Y="1.310382080078" />
                  <Point X="-21.97762890625" Y="1.266934082031" />
                  <Point X="-21.975400390625" Y="1.258235107422" />
                  <Point X="-21.965318359375" Y="1.228615234375" />
                  <Point X="-21.949716796875" Y="1.195376342773" />
                  <Point X="-21.943083984375" Y="1.183527832031" />
                  <Point X="-21.91941015625" Y="1.147543701172" />
                  <Point X="-21.89502734375" Y="1.110482421875" />
                  <Point X="-21.888263671875" Y="1.101425537109" />
                  <Point X="-21.873708984375" Y="1.084179321289" />
                  <Point X="-21.86591796875" Y="1.075989990234" />
                  <Point X="-21.848673828125" Y="1.059900268555" />
                  <Point X="-21.839962890625" Y="1.052693359375" />
                  <Point X="-21.821751953125" Y="1.039367675781" />
                  <Point X="-21.812251953125" Y="1.033249267578" />
                  <Point X="-21.777943359375" Y="1.013936889648" />
                  <Point X="-21.742609375" Y="0.994046508789" />
                  <Point X="-21.734521484375" Y="0.989988037109" />
                  <Point X="-21.7053203125" Y="0.978083496094" />
                  <Point X="-21.66972265625" Y="0.968020996094" />
                  <Point X="-21.656328125" Y="0.965257629395" />
                  <Point X="-21.60994140625" Y="0.95912713623" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.555966796875" Y="0.952199829102" />
                  <Point X="-21.53428125" Y="0.951222900391" />
                  <Point X="-21.50878125" Y="0.952032287598" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-20.780109375" Y="1.047493041992" />
                  <Point X="-20.295296875" Y="1.111319824219" />
                  <Point X="-20.2473125" Y="0.914207702637" />
                  <Point X="-20.216408203125" Y="0.715719787598" />
                  <Point X="-20.21612890625" Y="0.713921081543" />
                  <Point X="-20.6675625" Y="0.592959228516" />
                  <Point X="-21.3080078125" Y="0.421352844238" />
                  <Point X="-21.31396875" Y="0.419543792725" />
                  <Point X="-21.334375" Y="0.412137023926" />
                  <Point X="-21.357619140625" Y="0.401618682861" />
                  <Point X="-21.365994140625" Y="0.397316040039" />
                  <Point X="-21.41156640625" Y="0.370974121094" />
                  <Point X="-21.45850390625" Y="0.343843597412" />
                  <Point X="-21.46611328125" Y="0.338947967529" />
                  <Point X="-21.491224609375" Y="0.319871490479" />
                  <Point X="-21.518005859375" Y="0.294350982666" />
                  <Point X="-21.527203125" Y="0.284226654053" />
                  <Point X="-21.554546875" Y="0.249384246826" />
                  <Point X="-21.582708984375" Y="0.213498947144" />
                  <Point X="-21.589146484375" Y="0.204205551147" />
                  <Point X="-21.60087109375" Y="0.184923080444" />
                  <Point X="-21.606158203125" Y="0.174934280396" />
                  <Point X="-21.615931640625" Y="0.153469863892" />
                  <Point X="-21.619994140625" Y="0.142927734375" />
                  <Point X="-21.62683984375" Y="0.121430183411" />
                  <Point X="-21.629623046875" Y="0.110474594116" />
                  <Point X="-21.63873828125" Y="0.06288167572" />
                  <Point X="-21.648125" Y="0.013864208221" />
                  <Point X="-21.649396484375" Y="0.004966303349" />
                  <Point X="-21.6514140625" Y="-0.026262340546" />
                  <Point X="-21.64971875" Y="-0.062940391541" />
                  <Point X="-21.648125" Y="-0.076424201965" />
                  <Point X="-21.639009765625" Y="-0.124017120361" />
                  <Point X="-21.629623046875" Y="-0.173034744263" />
                  <Point X="-21.62683984375" Y="-0.183990325928" />
                  <Point X="-21.619994140625" Y="-0.205487884521" />
                  <Point X="-21.615931640625" Y="-0.216029998779" />
                  <Point X="-21.606158203125" Y="-0.237494415283" />
                  <Point X="-21.60087109375" Y="-0.247483215332" />
                  <Point X="-21.589146484375" Y="-0.266765716553" />
                  <Point X="-21.582708984375" Y="-0.276059234619" />
                  <Point X="-21.555365234375" Y="-0.310901489258" />
                  <Point X="-21.527203125" Y="-0.346787109375" />
                  <Point X="-21.52128125" Y="-0.353634155273" />
                  <Point X="-21.498857421875" Y="-0.375807189941" />
                  <Point X="-21.4698203125" Y="-0.398725891113" />
                  <Point X="-21.45850390625" Y="-0.406403594971" />
                  <Point X="-21.412931640625" Y="-0.432745666504" />
                  <Point X="-21.365994140625" Y="-0.459876190186" />
                  <Point X="-21.36050390625" Y="-0.462814483643" />
                  <Point X="-21.340841796875" Y="-0.472016296387" />
                  <Point X="-21.31697265625" Y="-0.481027313232" />
                  <Point X="-21.3080078125" Y="-0.483912841797" />
                  <Point X="-20.648390625" Y="-0.660656799316" />
                  <Point X="-20.21512109375" Y="-0.776751159668" />
                  <Point X="-20.238380859375" Y="-0.931036315918" />
                  <Point X="-20.272197265625" Y="-1.079219848633" />
                  <Point X="-20.820482421875" Y="-1.007036743164" />
                  <Point X="-21.563216796875" Y="-0.909253601074" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042297363" />
                  <Point X="-21.63327734375" Y="-0.910840881348" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.7349609375" Y="-0.932117126465" />
                  <Point X="-21.82708203125" Y="-0.952139892578" />
                  <Point X="-21.842125" Y="-0.956742492676" />
                  <Point X="-21.87124609375" Y="-0.9683671875" />
                  <Point X="-21.88532421875" Y="-0.975389465332" />
                  <Point X="-21.913150390625" Y="-0.992281616211" />
                  <Point X="-21.925875" Y="-1.001530395508" />
                  <Point X="-21.949625" Y="-1.022001281738" />
                  <Point X="-21.960650390625" Y="-1.033223266602" />
                  <Point X="-22.014712890625" Y="-1.098243896484" />
                  <Point X="-22.07039453125" Y="-1.165211303711" />
                  <Point X="-22.078673828125" Y="-1.176850219727" />
                  <Point X="-22.093392578125" Y="-1.201231201172" />
                  <Point X="-22.09983203125" Y="-1.213973388672" />
                  <Point X="-22.11117578125" Y="-1.241358642578" />
                  <Point X="-22.115634765625" Y="-1.254927368164" />
                  <Point X="-22.122466796875" Y="-1.282578857422" />
                  <Point X="-22.12483984375" Y="-1.296661376953" />
                  <Point X="-22.132587890625" Y="-1.380866210938" />
                  <Point X="-22.140568359375" Y="-1.467591796875" />
                  <Point X="-22.140708984375" Y="-1.483315795898" />
                  <Point X="-22.138392578125" Y="-1.514580444336" />
                  <Point X="-22.135935546875" Y="-1.53012097168" />
                  <Point X="-22.128205078125" Y="-1.561742553711" />
                  <Point X="-22.12321484375" Y="-1.576666870117" />
                  <Point X="-22.110841796875" Y="-1.605480712891" />
                  <Point X="-22.103458984375" Y="-1.619370239258" />
                  <Point X="-22.0539609375" Y="-1.696363037109" />
                  <Point X="-22.002978515625" Y="-1.775660766602" />
                  <Point X="-21.998259765625" Y="-1.782350463867" />
                  <Point X="-21.980197265625" Y="-1.804459350586" />
                  <Point X="-21.95650390625" Y="-1.828124511719" />
                  <Point X="-21.947201171875" Y="-1.836277587891" />
                  <Point X="-21.335072265625" Y="-2.305981445313" />
                  <Point X="-20.912828125" Y="-2.629981201172" />
                  <Point X="-20.954529296875" Y="-2.697457275391" />
                  <Point X="-20.998724609375" Y="-2.760252929688" />
                  <Point X="-21.4900390625" Y="-2.476591796875" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.184970703125" Y="-2.079513916016" />
                  <Point X="-22.216876953125" Y="-2.069326416016" />
                  <Point X="-22.228888671875" Y="-2.066337646484" />
                  <Point X="-22.33533984375" Y="-2.047112426758" />
                  <Point X="-22.444978515625" Y="-2.027311889648" />
                  <Point X="-22.460638671875" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.55515625" Y="-2.035136474609" />
                  <Point X="-22.584931640625" Y="-2.044960083008" />
                  <Point X="-22.59941015625" Y="-2.051108642578" />
                  <Point X="-22.687845703125" Y="-2.097651367188" />
                  <Point X="-22.778927734375" Y="-2.145587646484" />
                  <Point X="-22.791029296875" Y="-2.153169677734" />
                  <Point X="-22.8139609375" Y="-2.170062988281" />
                  <Point X="-22.824791015625" Y="-2.179374267578" />
                  <Point X="-22.84575" Y="-2.200333496094" />
                  <Point X="-22.855060546875" Y="-2.211162109375" />
                  <Point X="-22.871953125" Y="-2.234092529297" />
                  <Point X="-22.87953515625" Y="-2.246194335938" />
                  <Point X="-22.926078125" Y="-2.334629638672" />
                  <Point X="-22.974015625" Y="-2.425712402344" />
                  <Point X="-22.9801640625" Y="-2.440192871094" />
                  <Point X="-22.98998828125" Y="-2.469970947266" />
                  <Point X="-22.9936640625" Y="-2.485268554688" />
                  <Point X="-22.99862109375" Y="-2.517441894531" />
                  <Point X="-22.999720703125" Y="-2.533133300781" />
                  <Point X="-22.99931640625" Y="-2.564484619141" />
                  <Point X="-22.9978125" Y="-2.58014453125" />
                  <Point X="-22.9785859375" Y="-2.686596191406" />
                  <Point X="-22.95878515625" Y="-2.796234863281" />
                  <Point X="-22.95698046875" Y="-2.804231689453" />
                  <Point X="-22.948763671875" Y="-2.831534912109" />
                  <Point X="-22.935931640625" Y="-2.862473876953" />
                  <Point X="-22.930453125" Y="-2.873578613281" />
                  <Point X="-22.537099609375" Y="-3.554888671875" />
                  <Point X="-22.26410546875" Y="-4.027725341797" />
                  <Point X="-22.276244140625" Y="-4.036083007813" />
                  <Point X="-22.66129296875" Y="-3.534278076172" />
                  <Point X="-23.16608203125" Y="-2.876422851562" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849625732422" />
                  <Point X="-23.21674609375" Y="-2.828004638672" />
                  <Point X="-23.22669921875" Y="-2.820646972656" />
                  <Point X="-23.331689453125" Y="-2.753147949219" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.453716796875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663873535156" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.7064296875" Y="-2.657082763672" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920125" Y="-2.699414794922" />
                  <Point X="-23.944505859375" Y="-2.714135742188" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-24.044802734375" Y="-2.796135009766" />
                  <Point X="-24.136123046875" Y="-2.872063720703" />
                  <Point X="-24.147345703125" Y="-2.883087158203" />
                  <Point X="-24.16781640625" Y="-2.906836425781" />
                  <Point X="-24.177064453125" Y="-2.919562255859" />
                  <Point X="-24.19395703125" Y="-2.947388671875" />
                  <Point X="-24.20098046875" Y="-2.961466552734" />
                  <Point X="-24.21260546875" Y="-2.990587890625" />
                  <Point X="-24.21720703125" Y="-3.005631347656" />
                  <Point X="-24.243716796875" Y="-3.127599609375" />
                  <Point X="-24.271021484375" Y="-3.253219238281" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323170166016" />
                  <Point X="-24.2744453125" Y="-3.335520507812" />
                  <Point X="-24.16691015625" Y="-4.152324707031" />
                  <Point X="-24.344931640625" Y="-3.487937011719" />
                  <Point X="-24.347392578125" Y="-3.480121337891" />
                  <Point X="-24.357853515625" Y="-3.453579589844" />
                  <Point X="-24.3732109375" Y="-3.423815917969" />
                  <Point X="-24.37958984375" Y="-3.413209960938" />
                  <Point X="-24.46024609375" Y="-3.296999267578" />
                  <Point X="-24.543318359375" Y="-3.177309570312" />
                  <Point X="-24.553328125" Y="-3.165172851562" />
                  <Point X="-24.575212890625" Y="-3.142716552734" />
                  <Point X="-24.587087890625" Y="-3.132396972656" />
                  <Point X="-24.61334375" Y="-3.113153320312" />
                  <Point X="-24.626759765625" Y="-3.104936279297" />
                  <Point X="-24.654759765625" Y="-3.090829101562" />
                  <Point X="-24.66934375" Y="-3.084938964844" />
                  <Point X="-24.794154296875" Y="-3.046202148438" />
                  <Point X="-24.922703125" Y="-3.006305664062" />
                  <Point X="-24.936623046875" Y="-3.003109619141" />
                  <Point X="-24.964783203125" Y="-2.998840087891" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.189794921875" Y="-3.045041748047" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.332931640625" Y="-3.090830078125" />
                  <Point X="-25.360931640625" Y="-3.104938232422" />
                  <Point X="-25.37434375" Y="-3.113154785156" />
                  <Point X="-25.400599609375" Y="-3.132398925781" />
                  <Point X="-25.412474609375" Y="-3.142716552734" />
                  <Point X="-25.434359375" Y="-3.165172119141" />
                  <Point X="-25.444369140625" Y="-3.177310058594" />
                  <Point X="-25.525025390625" Y="-3.293520507812" />
                  <Point X="-25.608095703125" Y="-3.413210449219" />
                  <Point X="-25.61246875" Y="-3.420131835938" />
                  <Point X="-25.625974609375" Y="-3.445261230469" />
                  <Point X="-25.63877734375" Y="-3.476214111328" />
                  <Point X="-25.64275390625" Y="-3.487936767578" />
                  <Point X="-25.845998046875" Y="-4.246455566406" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.229053370669" Y="3.820795192757" />
                  <Point X="-23.05661315688" Y="4.400258793382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.276734751047" Y="3.738208468798" />
                  <Point X="-23.426712066734" Y="4.543431254012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.043687988948" Y="2.758846245079" />
                  <Point X="-21.06817894943" Y="2.775995000227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.324416131425" Y="3.655621744839" />
                  <Point X="-23.711140198325" Y="4.626616389899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.897725790413" Y="2.540668827439" />
                  <Point X="-21.158956573861" Y="2.723584591221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.372097511804" Y="3.57303502088" />
                  <Point X="-23.963929900611" Y="4.687648059089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.91967917971" Y="2.440067170181" />
                  <Point X="-21.249734198292" Y="2.671174182216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.419778892182" Y="3.49044829692" />
                  <Point X="-24.206480842763" Y="4.741510471251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.998705347166" Y="2.379428302417" />
                  <Point X="-21.340511822723" Y="2.618763773211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.46746027256" Y="3.407861572961" />
                  <Point X="-24.401236841679" Y="4.761906503872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.077731514622" Y="2.318789434654" />
                  <Point X="-21.431289447154" Y="2.566353364206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.515141648653" Y="3.325274846001" />
                  <Point X="-24.595992766841" Y="4.782302484848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.156757682079" Y="2.25815056689" />
                  <Point X="-21.522067071584" Y="2.513942955201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.562822987439" Y="3.242688092919" />
                  <Point X="-24.644503888206" Y="4.700296751783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.235783849535" Y="2.197511699127" />
                  <Point X="-21.612844688452" Y="2.4615325409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.610504326226" Y="3.160101339837" />
                  <Point X="-24.670669423184" Y="4.602644470683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.314810016978" Y="2.136872831354" />
                  <Point X="-21.703622199863" Y="2.409122052758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.658185665012" Y="3.077514586755" />
                  <Point X="-24.696835568167" Y="4.504992616714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.393836184412" Y="2.076233963574" />
                  <Point X="-21.794399711275" Y="2.356711564616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.705867003798" Y="2.994927833672" />
                  <Point X="-24.72300171315" Y="4.407340762745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.472862351845" Y="2.015595095795" />
                  <Point X="-21.885177222686" Y="2.304301076474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.753548342585" Y="2.91234108059" />
                  <Point X="-24.749167858134" Y="4.309688908776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.351534501315" Y="4.731470573098" />
                  <Point X="-25.416004520586" Y="4.77661296658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.28466235342" Y="1.067634914064" />
                  <Point X="-20.338860137933" Y="1.105584611335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.551888519279" Y="1.954956228015" />
                  <Point X="-21.975954734098" Y="2.251890588332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.801229681371" Y="2.829754327508" />
                  <Point X="-24.781462949859" Y="4.216328589517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.313283175884" Y="4.588713120752" />
                  <Point X="-25.557913165039" Y="4.760004883231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.250628872183" Y="0.927830828019" />
                  <Point X="-20.478274918414" Y="1.087230305633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.630914686712" Y="1.894317360236" />
                  <Point X="-22.066732245509" Y="2.19948010019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.848911020157" Y="2.747167574426" />
                  <Point X="-24.839751524664" Y="4.141169103055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.275031364014" Y="4.445955327798" />
                  <Point X="-25.699821555178" Y="4.743396621808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.229020948108" Y="0.796727210764" />
                  <Point X="-20.617689698896" Y="1.068875999932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.709940854146" Y="1.833678492456" />
                  <Point X="-22.157509756921" Y="2.147069612048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.896592358944" Y="2.664580821343" />
                  <Point X="-24.936556947479" Y="4.092979403917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.236779552144" Y="4.303197534844" />
                  <Point X="-25.841729940819" Y="4.726788357237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.259711214445" Y="0.70224318067" />
                  <Point X="-20.757104479377" Y="1.05052169423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.78896702158" Y="1.773039624677" />
                  <Point X="-22.248287268332" Y="2.094659123905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.94317766452" Y="2.581226617546" />
                  <Point X="-25.974900198142" Y="4.704061589348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.379499082182" Y="0.670145962713" />
                  <Point X="-20.896519239436" Y="1.032167374229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.863105597424" Y="1.708978428423" />
                  <Point X="-22.339064779744" Y="2.042248635763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.973220508092" Y="2.486289257152" />
                  <Point X="-26.093171532926" Y="4.670902483585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.499286949919" Y="0.638048744756" />
                  <Point X="-21.03593399546" Y="1.013813051402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.921600786859" Y="1.633963615082" />
                  <Point X="-22.442161575331" Y="1.998464203266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.987089866876" Y="2.38002710079" />
                  <Point X="-26.211442867709" Y="4.637743377823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.619074817656" Y="0.6059515268" />
                  <Point X="-21.175348751484" Y="0.995458728575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.967245807433" Y="1.549951016637" />
                  <Point X="-22.574809918629" Y="1.975371987242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.964511561564" Y="2.248244015278" />
                  <Point X="-26.329714192775" Y="4.604584265255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.738862829311" Y="0.573854409615" />
                  <Point X="-21.314763507507" Y="0.977104405748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.994069979932" Y="1.452759918495" />
                  <Point X="-26.447985486164" Y="4.571425130508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.858650938838" Y="0.541757360962" />
                  <Point X="-21.454178263531" Y="0.958750082922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.991699971956" Y="1.335126835112" />
                  <Point X="-26.466893782583" Y="4.468691276262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.978439048365" Y="0.509660312308" />
                  <Point X="-21.622764486702" Y="0.960821841292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.944542484776" Y="1.186133221173" />
                  <Point X="-26.469126712405" Y="4.354281204624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.098227157892" Y="0.477563263654" />
                  <Point X="-26.49816501637" Y="4.258640458025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.218015267419" Y="0.445466215001" />
                  <Point X="-26.538744126914" Y="4.171080671189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.335388628261" Y="0.411678341115" />
                  <Point X="-26.609991202298" Y="4.104994824516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.428722363922" Y="0.361057740462" />
                  <Point X="-26.710489823253" Y="4.059391130556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.509990680583" Y="0.301988842473" />
                  <Point X="-26.832463131087" Y="4.02882417423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.57084024248" Y="0.228622578477" />
                  <Point X="-27.274805806798" Y="4.222582264302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.223287167862" Y="-0.83091782844" />
                  <Point X="-20.353660142364" Y="-0.739629688915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.618738011515" Y="0.146187371487" />
                  <Point X="-27.367157013257" Y="4.171273689295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.239421033471" Y="-0.935594360052" />
                  <Point X="-20.621957269197" Y="-0.667739604159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.641889731611" Y="0.046424794488" />
                  <Point X="-27.459508707691" Y="4.119965455972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.262240557804" Y="-1.035589543028" />
                  <Point X="-20.890254271053" Y="-0.595849606912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.649573591507" Y="-0.064168494822" />
                  <Point X="-27.551860402125" Y="4.068657222648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.38717561036" Y="-1.064082663374" />
                  <Point X="-21.158551259251" Y="-0.523959619229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.621893269518" Y="-0.199524050871" />
                  <Point X="-27.644212096559" Y="4.017348989325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.591155130164" Y="-1.0372282519" />
                  <Point X="-27.723870596584" Y="3.957152885593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.795134649968" Y="-1.010373840425" />
                  <Point X="-27.802931098783" Y="3.896538059275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.999114302175" Y="-0.98351933624" />
                  <Point X="-27.881991600982" Y="3.835923232957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.20309397317" Y="-0.9566648189" />
                  <Point X="-27.96105191802" Y="3.775308276988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.407073644165" Y="-0.929810301561" />
                  <Point X="-28.040111862946" Y="3.714693060463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.60337335395" Y="-0.908333350953" />
                  <Point X="-27.974395426155" Y="3.552704330106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.735016729461" Y="-0.932129252997" />
                  <Point X="-27.862000613286" Y="3.358031048947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.85700806664" Y="-0.962683585041" />
                  <Point X="-27.765628797315" Y="3.174577191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.944378871087" Y="-1.01747947508" />
                  <Point X="-27.74901999952" Y="3.046973999651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.007335079508" Y="-1.089370649298" />
                  <Point X="-27.763142387486" Y="2.94088901623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.068281025417" Y="-1.162669424482" />
                  <Point X="-27.812848656822" Y="2.859720134783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.11311238794" Y="-1.247251752428" />
                  <Point X="-27.880815387753" Y="2.791337366197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.129884092815" Y="-1.351481664177" />
                  <Point X="-27.968832245806" Y="2.736993847762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.139909808074" Y="-1.46043516871" />
                  <Point X="-28.1219920942" Y="2.728263942226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.116195215872" Y="-1.593013890868" />
                  <Point X="-28.722720562392" Y="3.032924958139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.980665149076" Y="-1.803886651224" />
                  <Point X="-28.784522583394" Y="2.960225613189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.92904115945" Y="-2.656215282055" />
                  <Point X="-28.842930892973" Y="2.885149965918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.981348067141" Y="-2.735563176922" />
                  <Point X="-28.901339064162" Y="2.810074221745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.642702596464" Y="-2.388451335993" />
                  <Point X="-28.959747235351" Y="2.734998477573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.282073881101" Y="-2.056732328708" />
                  <Point X="-29.015341876754" Y="2.657952678634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.492402845345" Y="-2.025431988373" />
                  <Point X="-29.063395256655" Y="2.575626431546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.611941456356" Y="-2.057703737768" />
                  <Point X="-29.111448636557" Y="2.493300184457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.706498071502" Y="-2.107468068987" />
                  <Point X="-29.159502016459" Y="2.410973937369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.798742194188" Y="-2.158851624859" />
                  <Point X="-29.207554626151" Y="2.328647150973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.866835515179" Y="-2.227145754132" />
                  <Point X="-28.490221424601" Y="1.710391449908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.913390129222" Y="-2.310521448373" />
                  <Point X="-28.422491214826" Y="1.546992660526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.957991022892" Y="-2.395265152347" />
                  <Point X="-28.435562220459" Y="1.44017149127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.993801971029" Y="-2.486163642443" />
                  <Point X="-28.473921309067" Y="1.35105722834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.993845884803" Y="-2.60210647962" />
                  <Point X="-28.534126832761" Y="1.27724000394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.96986771446" Y="-2.734869761179" />
                  <Point X="-28.630310033703" Y="1.228614620356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.925740098078" Y="-2.881741836745" />
                  <Point X="-28.761762633839" Y="1.204685135956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.813346016949" Y="-3.076414605534" />
                  <Point X="-28.963586364122" Y="1.230030047358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.70095193582" Y="-3.271087374323" />
                  <Point X="-23.051696315294" Y="-3.02549351583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.592777728348" Y="-2.646624231625" />
                  <Point X="-29.167565952" Y="1.256884506499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.588557854691" Y="-3.465760143112" />
                  <Point X="-22.859375159918" Y="-3.276131824514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.739166571163" Y="-2.660095246309" />
                  <Point X="-29.371545546804" Y="1.283738970489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.476162845504" Y="-3.660433561734" />
                  <Point X="-22.667054004542" Y="-3.526770133198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.875941955169" Y="-2.680297677319" />
                  <Point X="-29.575525155" Y="1.310593443857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.363767052604" Y="-3.855107529118" />
                  <Point X="-22.474731667622" Y="-3.777409269208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.967696956683" Y="-2.732023719522" />
                  <Point X="-29.652481816932" Y="1.248505492725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.273567084493" Y="-4.034239812668" />
                  <Point X="-22.282409294216" Y="-4.028048430766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.043413572265" Y="-2.794979960457" />
                  <Point X="-29.678896343885" Y="1.151027557683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.119131020056" Y="-2.857935618671" />
                  <Point X="-28.319690060703" Y="0.083327486284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.587468404616" Y="0.270827901262" />
                  <Point X="-29.705310875688" Y="1.053549626037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.18293526502" Y="-2.92923299131" />
                  <Point X="-28.294918076476" Y="-0.04999162974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.85576528293" Y="0.342717812003" />
                  <Point X="-29.731563601506" Y="0.955958396622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.220145650762" Y="-3.019151584646" />
                  <Point X="-28.320536997449" Y="-0.148026654086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.124062161243" Y="0.414607722744" />
                  <Point X="-29.747113142297" Y="0.850872716367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.242022968542" Y="-3.119806507754" />
                  <Point X="-28.371820320734" Y="-0.22809127047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.392359084257" Y="0.486497664784" />
                  <Point X="-29.762662683088" Y="0.745787036112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.263901119286" Y="-3.220460847613" />
                  <Point X="-28.452557814597" Y="-0.287531854583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.660656053946" Y="0.558387639507" />
                  <Point X="-29.778212512264" Y="0.640701557786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.274891457517" Y="-3.328738915868" />
                  <Point X="-28.562319719805" Y="-0.326649327081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.258564724507" Y="-3.456144603328" />
                  <Point X="-24.436040918857" Y="-3.331874434191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.883724306529" Y="-3.018403151412" />
                  <Point X="-28.682107732007" Y="-0.358746443882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.241745957987" Y="-3.583894816362" />
                  <Point X="-24.337112534822" Y="-3.517118420368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.066124221485" Y="-3.006658941924" />
                  <Point X="-28.80189574421" Y="-0.390843560683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.224927191466" Y="-3.711645029395" />
                  <Point X="-24.298860869175" Y="-3.659876110935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.180884730287" Y="-3.042276354504" />
                  <Point X="-28.921683756412" Y="-0.422940677484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.208108424946" Y="-3.839395242428" />
                  <Point X="-24.260609203529" Y="-3.802633801502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.295645294357" Y="-3.077893728386" />
                  <Point X="-29.041471768615" Y="-0.455037794285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.191289658425" Y="-3.967145455462" />
                  <Point X="-24.222357537883" Y="-3.94539149207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.392211527172" Y="-3.126250910164" />
                  <Point X="-27.949806835263" Y="-1.33540339575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.279166720046" Y="-1.104783121641" />
                  <Point X="-29.161259780818" Y="-0.487134911086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.174470891905" Y="-4.094895668495" />
                  <Point X="-24.184105872236" Y="-4.088149182637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.457630715337" Y="-3.1964174874" />
                  <Point X="-27.961729132217" Y="-1.443028899482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.418581500648" Y="-1.123137427257" />
                  <Point X="-29.28104782313" Y="-0.519232006803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.51179818704" Y="-3.27446260132" />
                  <Point X="-28.005573181216" Y="-1.528302551799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.557996281251" Y="-1.141491732874" />
                  <Point X="-29.400835921582" Y="-0.551329063212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.565965363349" Y="-3.352507922077" />
                  <Point X="-28.075788858828" Y="-1.595110590967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.697411061853" Y="-1.15984603849" />
                  <Point X="-29.520624020034" Y="-0.583426119621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.618633786127" Y="-3.431602681355" />
                  <Point X="-28.154814936698" Y="-1.65574952146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.836825842456" Y="-1.178200344107" />
                  <Point X="-29.640412118485" Y="-0.615523176029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.652399180254" Y="-3.523933483789" />
                  <Point X="-28.233841014567" Y="-1.716388451953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.976240623058" Y="-1.196554649724" />
                  <Point X="-29.760200216937" Y="-0.647620232438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.678564941456" Y="-3.621585606485" />
                  <Point X="-27.392728564481" Y="-2.421315315918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.457429734181" Y="-2.376011069163" />
                  <Point X="-28.312867092437" Y="-1.777027382446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.115655385531" Y="-1.214908968035" />
                  <Point X="-29.772059419476" Y="-0.755289915355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.704730702658" Y="-3.71923772918" />
                  <Point X="-27.310834156112" Y="-2.594631983927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.643852930276" Y="-2.361449727892" />
                  <Point X="-28.391893170307" Y="-1.837666312938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.255070110525" Y="-1.233263312589" />
                  <Point X="-29.753627050576" Y="-0.884169984939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.73089646386" Y="-3.816889851876" />
                  <Point X="-27.329997070366" Y="-2.697187552845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.740307065982" Y="-2.409885400911" />
                  <Point X="-28.470919248177" Y="-1.898305243431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.39448483552" Y="-1.251617657143" />
                  <Point X="-29.729863931984" Y="-1.016782685641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.757062225062" Y="-3.914541974572" />
                  <Point X="-27.374790142654" Y="-2.781796691901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.831084706468" Y="-2.462295798675" />
                  <Point X="-28.549945326046" Y="-1.958944173924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.533899560514" Y="-1.269972001697" />
                  <Point X="-29.693787727969" Y="-1.158017101574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.783227986264" Y="-4.012194097267" />
                  <Point X="-27.422471626166" Y="-2.864383343646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.921862346953" Y="-2.514706196439" />
                  <Point X="-28.628971403916" Y="-2.019583104417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.809393747467" Y="-4.109846219963" />
                  <Point X="-27.470153109677" Y="-2.94696999539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.012639987439" Y="-2.567116594202" />
                  <Point X="-28.707997486903" Y="-2.080222031326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.835559508669" Y="-4.207498342658" />
                  <Point X="-27.517834593189" Y="-3.029556647135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.103417627924" Y="-2.619526991966" />
                  <Point X="-28.78702358697" Y="-2.140860946276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.861725499194" Y="-4.30515030478" />
                  <Point X="-26.45452007694" Y="-3.890071072832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.556993250191" Y="-3.818318584458" />
                  <Point X="-27.5655160767" Y="-3.11214329888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.194195268409" Y="-2.671937389729" />
                  <Point X="-28.866049687037" Y="-2.201499861226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.887891641926" Y="-4.402802160326" />
                  <Point X="-26.183363365295" Y="-4.195910632295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.766380696607" Y="-3.787677502003" />
                  <Point X="-27.613197560212" Y="-3.194729950624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.284972908895" Y="-2.724347787493" />
                  <Point X="-28.945075787104" Y="-2.262138776177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.914057784658" Y="-4.500454015871" />
                  <Point X="-26.156560934833" Y="-4.330651482079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.948803244779" Y="-3.775917444566" />
                  <Point X="-27.660879043723" Y="-3.277316602369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.375750507853" Y="-2.776758214334" />
                  <Point X="-29.024101887171" Y="-2.322777691127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.940223927391" Y="-4.598105871416" />
                  <Point X="-26.129758389205" Y="-4.465392412504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.073585105577" Y="-3.804517830936" />
                  <Point X="-27.708560526336" Y="-3.359903254743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.466528040198" Y="-2.829168687818" />
                  <Point X="-29.103127987238" Y="-2.383416606077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.966390070123" Y="-4.695757726962" />
                  <Point X="-26.122198395069" Y="-4.586659563319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.160753755163" Y="-3.859455271333" />
                  <Point X="-27.756241884419" Y="-3.442489994313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.557305572544" Y="-2.881579161302" />
                  <Point X="-29.179930607644" Y="-2.445612418247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.047856193707" Y="-4.754688119052" />
                  <Point X="-26.136179074686" Y="-4.692843771994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.245505495327" Y="-3.916085049926" />
                  <Point X="-27.803923242503" Y="-3.525076733883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.64808310489" Y="-2.933989634786" />
                  <Point X="-29.061183100511" Y="-2.644733903817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.330257149227" Y="-3.972714888922" />
                  <Point X="-27.851604600586" Y="-3.607663473453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.738860637235" Y="-2.98640010827" />
                  <Point X="-28.905996207597" Y="-2.869370521999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.405544537535" Y="-4.035971678029" />
                  <Point X="-27.89928595867" Y="-3.690250213024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.463813455559" Y="-4.111144928317" />
                  <Point X="-27.946967316753" Y="-3.772836952594" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.0001640625" Y="0.001626220703" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.331123046875" Y="-4.273583007813" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521543945312" />
                  <Point X="-24.6163359375" Y="-3.405333251953" />
                  <Point X="-24.699408203125" Y="-3.285643554688" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.850474609375" Y="-3.227663085938" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.133474609375" Y="-3.226503173828" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285644042969" />
                  <Point X="-25.368935546875" Y="-3.401854492188" />
                  <Point X="-25.452005859375" Y="-3.521544433594" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.662470703125" Y="-4.295631347656" />
                  <Point X="-25.847744140625" Y="-4.987077148438" />
                  <Point X="-25.949640625" Y="-4.967298339844" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.244453125" Y="-4.900961425781" />
                  <Point X="-26.351587890625" Y="-4.873396484375" />
                  <Point X="-26.332904296875" Y="-4.731473632812" />
                  <Point X="-26.3091484375" Y="-4.5510390625" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.3395" Y="-4.38485546875" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.501193359375" Y="-4.101854980469" />
                  <Point X="-26.61954296875" Y="-3.998064208984" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.801751953125" Y="-3.975766845703" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791015625" />
                  <Point X="-27.116958984375" Y="-4.058703857422" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.37383984375" Y="-4.306" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.635078125" Y="-4.304297363281" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.055521484375" Y="-4.013858154297" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.87276171875" Y="-3.264311279297" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597592529297" />
                  <Point X="-27.51398046875" Y="-2.568763183594" />
                  <Point X="-27.53132421875" Y="-2.551418701172" />
                  <Point X="-27.56015234375" Y="-2.537198974609" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.245890625" Y="-2.921176757812" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-28.987294921875" Y="-3.076266357422" />
                  <Point X="-29.161697265625" Y="-2.847135742188" />
                  <Point X="-29.30486328125" Y="-2.607071533203" />
                  <Point X="-29.43101953125" Y="-2.395526855469" />
                  <Point X="-28.80544140625" Y="-1.91550402832" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.396018066406" />
                  <Point X="-28.138115234375" Y="-1.366263671875" />
                  <Point X="-28.140326171875" Y="-1.33459362793" />
                  <Point X="-28.16116015625" Y="-1.310638305664" />
                  <Point X="-28.187642578125" Y="-1.295051879883" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.04540234375" Y="-1.397299560547" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.8591796875" Y="-1.278240600586" />
                  <Point X="-29.927392578125" Y="-1.011187744141" />
                  <Point X="-29.96526953125" Y="-0.746362304688" />
                  <Point X="-29.998396484375" Y="-0.51474230957" />
                  <Point X="-29.288404296875" Y="-0.324500701904" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.528224609375" Y="-0.111936950684" />
                  <Point X="-28.502322265625" Y="-0.090645477295" />
                  <Point X="-28.49033984375" Y="-0.061223060608" />
                  <Point X="-28.483400390625" Y="-0.031280040741" />
                  <Point X="-28.490203125" Y="-0.001776633501" />
                  <Point X="-28.502322265625" Y="0.028085395813" />
                  <Point X="-28.527814453125" Y="0.049092720032" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.310287109375" Y="0.267803955078" />
                  <Point X="-29.998185546875" Y="0.45212600708" />
                  <Point X="-29.96160546875" Y="0.699329650879" />
                  <Point X="-29.91764453125" Y="0.996414489746" />
                  <Point X="-29.841400390625" Y="1.277784545898" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.277220703125" Y="1.462960449219" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.701443359375" Y="1.405407592773" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.651533203125" Y="1.426056030273" />
                  <Point X="-28.639119140625" Y="1.443785400391" />
                  <Point X="-28.626978515625" Y="1.473098632812" />
                  <Point X="-28.61447265625" Y="1.503289672852" />
                  <Point X="-28.610712890625" Y="1.52460546875" />
                  <Point X="-28.61631640625" Y="1.545512573242" />
                  <Point X="-28.630966796875" Y="1.57365625" />
                  <Point X="-28.646056640625" Y="1.602642211914" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-29.0917890625" Y="1.9505703125" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.330833984375" Y="2.494351806641" />
                  <Point X="-29.16001171875" Y="2.7870078125" />
                  <Point X="-28.958044921875" Y="3.046609375" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.480955078125" Y="3.112734619141" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.096369140625" Y="2.916747802734" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775146484" />
                  <Point X="-28.013251953125" Y="2.927404052734" />
                  <Point X="-27.983337890625" Y="2.957318115234" />
                  <Point X="-27.95252734375" Y="2.988127441406" />
                  <Point X="-27.9408984375" Y="3.006381591797" />
                  <Point X="-27.938072265625" Y="3.027840576172" />
                  <Point X="-27.941759765625" Y="3.069984619141" />
                  <Point X="-27.945556640625" Y="3.113390136719" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.143419921875" Y="3.465466552734" />
                  <Point X="-28.30727734375" Y="3.749276611328" />
                  <Point X="-28.050388671875" Y="3.946231689453" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.434779296875" Y="4.351058105469" />
                  <Point X="-27.141548828125" Y="4.513971679688" />
                  <Point X="-27.06633984375" Y="4.41595703125" />
                  <Point X="-26.967826171875" Y="4.287572753906" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.90433984375" Y="4.249242675781" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.218491699219" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.764951171875" Y="4.242487304688" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.696904296875" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.294487304688" />
                  <Point X="-26.670181640625" Y="4.344920898438" />
                  <Point X="-26.653802734375" Y="4.396863769531" />
                  <Point X="-26.651916015625" Y="4.418426269531" />
                  <Point X="-26.673671875" Y="4.583669921875" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.353240234375" Y="4.795314453125" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.58246484375" Y="4.948428222656" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.137337890625" Y="4.666184570312" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.847498046875" Y="4.676820800781" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.476083984375" Y="4.960784179688" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.82075390625" Y="4.848539550781" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.283658203125" Y="4.693657226562" />
                  <Point X="-23.068970703125" Y="4.615788085938" />
                  <Point X="-22.8681640625" Y="4.521877929688" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.46731640625" Y="4.312118652344" />
                  <Point X="-22.26747265625" Y="4.195688964844" />
                  <Point X="-22.0845234375" Y="4.065586425781" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.345685546875" Y="3.238784667969" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491516113281" />
                  <Point X="-22.78572265625" Y="2.451965087891" />
                  <Point X="-22.7966171875" Y="2.411229736328" />
                  <Point X="-22.797955078125" Y="2.392327636719" />
                  <Point X="-22.793830078125" Y="2.358127197266" />
                  <Point X="-22.789583984375" Y="2.322902832031" />
                  <Point X="-22.78131640625" Y="2.300811279297" />
                  <Point X="-22.760154296875" Y="2.269623779297" />
                  <Point X="-22.738357421875" Y="2.237502685547" />
                  <Point X="-22.72505859375" Y="2.224203613281" />
                  <Point X="-22.69387109375" Y="2.203041503906" />
                  <Point X="-22.66175" Y="2.18124609375" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.605462890625" Y="2.168856201172" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5513359375" Y="2.165946044922" />
                  <Point X="-22.51178515625" Y="2.176522705078" />
                  <Point X="-22.471048828125" Y="2.187415771484" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.70176953125" Y="2.629584960938" />
                  <Point X="-21.005751953125" Y="3.031430664062" />
                  <Point X="-20.9159453125" Y="2.906621826172" />
                  <Point X="-20.797404296875" Y="2.741876220703" />
                  <Point X="-20.6954140625" Y="2.573336425781" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.152541015625" Y="2.021897094727" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583832763672" />
                  <Point X="-21.74909375" Y="1.546697998047" />
                  <Point X="-21.77841015625" Y="1.508451416016" />
                  <Point X="-21.78687890625" Y="1.491501464844" />
                  <Point X="-21.797482421875" Y="1.453586791992" />
                  <Point X="-21.808404296875" Y="1.414536987305" />
                  <Point X="-21.809220703125" Y="1.390965332031" />
                  <Point X="-21.800515625" Y="1.348780517578" />
                  <Point X="-21.79155078125" Y="1.305332519531" />
                  <Point X="-21.784353515625" Y="1.287955200195" />
                  <Point X="-21.7606796875" Y="1.251971191406" />
                  <Point X="-21.736296875" Y="1.214909912109" />
                  <Point X="-21.719052734375" Y="1.19882019043" />
                  <Point X="-21.684744140625" Y="1.17950793457" />
                  <Point X="-21.64941015625" Y="1.159617675781" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.585046875" Y="1.147489135742" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.80491015625" Y="1.235867553711" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.11172265625" Y="1.160514404297" />
                  <Point X="-20.06080859375" Y="0.951367736816" />
                  <Point X="-20.028669921875" Y="0.744950073242" />
                  <Point X="-20.002140625" Y="0.574556335449" />
                  <Point X="-20.61838671875" Y="0.40943347168" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819503784" />
                  <Point X="-21.316484375" Y="0.206477523804" />
                  <Point X="-21.363421875" Y="0.179346984863" />
                  <Point X="-21.377734375" Y="0.166926544189" />
                  <Point X="-21.405078125" Y="0.132084152222" />
                  <Point X="-21.433240234375" Y="0.096198867798" />
                  <Point X="-21.443013671875" Y="0.07473449707" />
                  <Point X="-21.45212890625" Y="0.027141580582" />
                  <Point X="-21.461515625" Y="-0.021876060486" />
                  <Point X="-21.461515625" Y="-0.040684017181" />
                  <Point X="-21.452400390625" Y="-0.088276939392" />
                  <Point X="-21.443013671875" Y="-0.137294570923" />
                  <Point X="-21.433240234375" Y="-0.15875894165" />
                  <Point X="-21.405896484375" Y="-0.19360118103" />
                  <Point X="-21.377734375" Y="-0.229486618042" />
                  <Point X="-21.363421875" Y="-0.241907058716" />
                  <Point X="-21.317849609375" Y="-0.268249023438" />
                  <Point X="-21.270912109375" Y="-0.295379577637" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.59921484375" Y="-0.477130981445" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.023140625" Y="-0.777851013184" />
                  <Point X="-20.051568359375" Y="-0.966413391113" />
                  <Point X="-20.092744140625" Y="-1.146846069336" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-20.845283203125" Y="-1.195411254883" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.69460546875" Y="-1.117782104492" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.814552734375" Y="-1.154697143555" />
                  <Point X="-21.868615234375" Y="-1.219717895508" />
                  <Point X="-21.924296875" Y="-1.286685180664" />
                  <Point X="-21.935640625" Y="-1.314070556641" />
                  <Point X="-21.943388671875" Y="-1.398275268555" />
                  <Point X="-21.951369140625" Y="-1.485000854492" />
                  <Point X="-21.943638671875" Y="-1.516622436523" />
                  <Point X="-21.894140625" Y="-1.593615234375" />
                  <Point X="-21.843158203125" Y="-1.672912963867" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-21.219408203125" Y="-2.155244140625" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.715734375" Y="-2.672474853516" />
                  <Point X="-20.7958671875" Y="-2.802140380859" />
                  <Point X="-20.881025390625" Y="-2.923139404297" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.5850390625" Y="-2.64113671875" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.369109375" Y="-2.234087646484" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.599357421875" Y="-2.265787597656" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.334683105469" />
                  <Point X="-22.75794140625" Y="-2.423118408203" />
                  <Point X="-22.80587890625" Y="-2.514201171875" />
                  <Point X="-22.8108359375" Y="-2.546374511719" />
                  <Point X="-22.791609375" Y="-2.652826171875" />
                  <Point X="-22.77180859375" Y="-2.76246484375" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.372552734375" Y="-3.459888671875" />
                  <Point X="-22.01332421875" Y="-4.082088867188" />
                  <Point X="-22.069615234375" Y="-4.122295898438" />
                  <Point X="-22.164685546875" Y="-4.190202148438" />
                  <Point X="-22.2599140625" Y="-4.251841308594" />
                  <Point X="-22.320224609375" Y="-4.29087890625" />
                  <Point X="-22.81203125" Y="-3.649942626953" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.434439453125" Y="-2.912968261719" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.68901953125" Y="-2.846283447266" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.923330078125" Y="-2.942230712891" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045985839844" />
                  <Point X="-24.058052734375" Y="-3.167954101562" />
                  <Point X="-24.085357421875" Y="-3.293573730469" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-23.974595703125" Y="-4.157446777344" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.915697265625" Y="-4.943528808594" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.093611328125" Y="-4.979225585938" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#166" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.095197293765" Y="4.710405628776" Z="1.25" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.25" />
                  <Point X="-0.594435095944" Y="5.029369539894" Z="1.25" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.25" />
                  <Point X="-1.372896139994" Y="4.874737952206" Z="1.25" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.25" />
                  <Point X="-1.729400608057" Y="4.608424193418" Z="1.25" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.25" />
                  <Point X="-1.724023547081" Y="4.391237405672" Z="1.25" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.25" />
                  <Point X="-1.790243407028" Y="4.319961418715" Z="1.25" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.25" />
                  <Point X="-1.887409251817" Y="4.324873457334" Z="1.25" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.25" />
                  <Point X="-2.032827859162" Y="4.477675574947" Z="1.25" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.25" />
                  <Point X="-2.465219988803" Y="4.426045725093" Z="1.25" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.25" />
                  <Point X="-3.086966549432" Y="4.017191706924" Z="1.25" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.25" />
                  <Point X="-3.192878041622" Y="3.471746623644" Z="1.25" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.25" />
                  <Point X="-2.997727157535" Y="3.096907391148" Z="1.25" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.25" />
                  <Point X="-3.024849559858" Y="3.023954008028" Z="1.25" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.25" />
                  <Point X="-3.098169074459" Y="2.997837541638" Z="1.25" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.25" />
                  <Point X="-3.462112724653" Y="3.187315907124" Z="1.25" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.25" />
                  <Point X="-4.003664738454" Y="3.108591781226" Z="1.25" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.25" />
                  <Point X="-4.380171810196" Y="2.550837168573" Z="1.25" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.25" />
                  <Point X="-4.128384179555" Y="1.942182674226" Z="1.25" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.25" />
                  <Point X="-3.68147305988" Y="1.581847976161" Z="1.25" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.25" />
                  <Point X="-3.679327921298" Y="1.523513460724" Z="1.25" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.25" />
                  <Point X="-3.722635946158" Y="1.484373505948" Z="1.25" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.25" />
                  <Point X="-4.276852988936" Y="1.543812792576" Z="1.25" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.25" />
                  <Point X="-4.895816148582" Y="1.32214210658" Z="1.25" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.25" />
                  <Point X="-5.01722411104" Y="0.737928594605" Z="1.25" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.25" />
                  <Point X="-4.32938519961" Y="0.250787742322" Z="1.25" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.25" />
                  <Point X="-3.56247964762" Y="0.039295952821" Z="1.25" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.25" />
                  <Point X="-3.544114082748" Y="0.014683678824" Z="1.25" />
                  <Point X="-3.539556741714" Y="0" Z="1.25" />
                  <Point X="-3.544250518387" Y="-0.015123272237" Z="1.25" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.25" />
                  <Point X="-3.562888947503" Y="-0.039580030313" Z="1.25" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.25" />
                  <Point X="-4.307502589162" Y="-0.244924314434" Z="1.25" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.25" />
                  <Point X="-5.020921683795" Y="-0.722161210333" Z="1.25" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.25" />
                  <Point X="-4.913781440533" Y="-1.259334555074" Z="1.25" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.25" />
                  <Point X="-4.045034107071" Y="-1.4155918224" Z="1.25" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.25" />
                  <Point X="-3.205722314035" Y="-1.3147714906" Z="1.25" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.25" />
                  <Point X="-3.196585366983" Y="-1.337542844137" Z="1.25" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.25" />
                  <Point X="-3.842035909416" Y="-1.84455643025" Z="1.25" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.25" />
                  <Point X="-4.353963130338" Y="-2.601401124402" Z="1.25" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.25" />
                  <Point X="-4.033237244485" Y="-3.075269401662" Z="1.25" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.25" />
                  <Point X="-3.22704723309" Y="-2.933197977026" Z="1.25" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.25" />
                  <Point X="-2.564037306376" Y="-2.564293162756" Z="1.25" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.25" />
                  <Point X="-2.922219036142" Y="-3.208031149402" Z="1.25" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.25" />
                  <Point X="-3.092181485416" Y="-4.022195273636" Z="1.25" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.25" />
                  <Point X="-2.667556804991" Y="-4.315527882938" Z="1.25" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.25" />
                  <Point X="-2.340328396464" Y="-4.305158117465" Z="1.25" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.25" />
                  <Point X="-2.095336891191" Y="-4.068997231673" Z="1.25" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.25" />
                  <Point X="-1.811178509959" Y="-3.99437978136" Z="1.25" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.25" />
                  <Point X="-1.540316346542" Y="-4.10816668115" Z="1.25" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.25" />
                  <Point X="-1.394695927337" Y="-4.363330218285" Z="1.25" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.25" />
                  <Point X="-1.388633214414" Y="-4.693666855076" Z="1.25" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.25" />
                  <Point X="-1.263069970698" Y="-4.918104428187" Z="1.25" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.25" />
                  <Point X="-0.96532472008" Y="-4.985102276866" Z="1.25" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.25" />
                  <Point X="-0.620331320056" Y="-4.277292082717" Z="1.25" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.25" />
                  <Point X="-0.334015600283" Y="-3.399083346348" Z="1.25" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.25" />
                  <Point X="-0.124811267712" Y="-3.242976179971" Z="1.25" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.25" />
                  <Point X="0.128547811649" Y="-3.244135865317" Z="1.25" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.25" />
                  <Point X="0.336430259149" Y="-3.40256240942" Z="1.25" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.25" />
                  <Point X="0.614423540345" Y="-4.255243947098" Z="1.25" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.25" />
                  <Point X="0.909168963426" Y="-4.997140579883" Z="1.25" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.25" />
                  <Point X="1.088851829843" Y="-4.961088935014" Z="1.25" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.25" />
                  <Point X="1.068819501868" Y="-4.119640546499" Z="1.25" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.25" />
                  <Point X="0.984649726484" Y="-3.147294140346" Z="1.25" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.25" />
                  <Point X="1.102479085169" Y="-2.949397202119" Z="1.25" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.25" />
                  <Point X="1.30940588706" Y="-2.864792850515" Z="1.25" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.25" />
                  <Point X="1.532363750757" Y="-2.923746306385" Z="1.25" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.25" />
                  <Point X="2.142144190927" Y="-3.649100654432" Z="1.25" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.25" />
                  <Point X="2.761099729628" Y="-4.262535483382" Z="1.25" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.25" />
                  <Point X="2.953288541354" Y="-4.131702752042" Z="1.25" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.25" />
                  <Point X="2.664591639682" Y="-3.403608737561" Z="1.25" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.25" />
                  <Point X="2.251436228292" Y="-2.61265994904" Z="1.25" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.25" />
                  <Point X="2.280147344539" Y="-2.415125422069" Z="1.25" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.25" />
                  <Point X="2.417772956303" Y="-2.278753965428" Z="1.25" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.25" />
                  <Point X="2.615846854472" Y="-2.252011781199" Z="1.25" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.25" />
                  <Point X="3.383804859546" Y="-2.653157860593" Z="1.25" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.25" />
                  <Point X="4.153706168953" Y="-2.920636719824" Z="1.25" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.25" />
                  <Point X="4.320641452052" Y="-2.667480256054" Z="1.25" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.25" />
                  <Point X="3.80487217343" Y="-2.084296461447" Z="1.25" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.25" />
                  <Point X="3.141761722815" Y="-1.53529527576" Z="1.25" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.25" />
                  <Point X="3.100242967148" Y="-1.371576923461" Z="1.25" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.25" />
                  <Point X="3.163672746669" Y="-1.220404926234" Z="1.25" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.25" />
                  <Point X="3.309856551368" Y="-1.135361297611" Z="1.25" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.25" />
                  <Point X="4.142035457148" Y="-1.213703413492" Z="1.25" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.25" />
                  <Point X="4.949845164377" Y="-1.126689966942" Z="1.25" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.25" />
                  <Point X="5.020143950506" Y="-0.754024857348" Z="1.25" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.25" />
                  <Point X="4.407570579788" Y="-0.397554707055" Z="1.25" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.25" />
                  <Point X="3.701015537774" Y="-0.193680115979" Z="1.25" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.25" />
                  <Point X="3.627280417559" Y="-0.131452822812" Z="1.25" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.25" />
                  <Point X="3.590549291332" Y="-0.047592858761" Z="1.25" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.25" />
                  <Point X="3.590822159093" Y="0.049017672451" Z="1.25" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.25" />
                  <Point X="3.628099020842" Y="0.132495915763" Z="1.25" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.25" />
                  <Point X="3.702379876578" Y="0.194468728263" Z="1.25" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.25" />
                  <Point X="4.388397237518" Y="0.392417225714" Z="1.25" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.25" />
                  <Point X="5.014578037466" Y="0.783922203594" Z="1.25" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.25" />
                  <Point X="4.930701985644" Y="1.203621096212" Z="1.25" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.25" />
                  <Point X="4.182407306272" Y="1.316719896459" Z="1.25" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.25" />
                  <Point X="3.415346674088" Y="1.228338097034" Z="1.25" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.25" />
                  <Point X="3.333626044311" Y="1.254358884352" Z="1.25" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.25" />
                  <Point X="3.27493539712" Y="1.31073235467" Z="1.25" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.25" />
                  <Point X="3.242296223549" Y="1.39016426148" Z="1.25" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.25" />
                  <Point X="3.244512727434" Y="1.471398990648" Z="1.25" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.25" />
                  <Point X="3.284433158676" Y="1.547560264744" Z="1.25" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.25" />
                  <Point X="3.87173984237" Y="2.013509391663" Z="1.25" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.25" />
                  <Point X="4.341205815808" Y="2.630502718328" Z="1.25" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.25" />
                  <Point X="4.118482871714" Y="2.967104773675" Z="1.25" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.25" />
                  <Point X="3.267074387473" Y="2.704166327154" Z="1.25" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.25" />
                  <Point X="2.469142756356" Y="2.25610565437" Z="1.25" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.25" />
                  <Point X="2.394367282362" Y="2.249776581807" Z="1.25" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.25" />
                  <Point X="2.328045591192" Y="2.275696069408" Z="1.25" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.25" />
                  <Point X="2.275062530827" Y="2.328979269191" Z="1.25" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.25" />
                  <Point X="2.249653120951" Y="2.39539116147" Z="1.25" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.25" />
                  <Point X="2.256422288287" Y="2.470326819526" Z="1.25" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.25" />
                  <Point X="2.691459025705" Y="3.245064694482" Z="1.25" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.25" />
                  <Point X="2.938296009482" Y="4.137613752051" Z="1.25" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.25" />
                  <Point X="2.551697377451" Y="4.386601417652" Z="1.25" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.25" />
                  <Point X="2.146860867946" Y="4.598449812144" Z="1.25" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.25" />
                  <Point X="1.727233542421" Y="4.771940572235" Z="1.25" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.25" />
                  <Point X="1.184824200791" Y="4.928423134707" Z="1.25" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.25" />
                  <Point X="0.522966084445" Y="5.041792382826" Z="1.25" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.25" />
                  <Point X="0.098047267788" Y="4.721041877175" Z="1.25" />
                  <Point X="0" Y="4.355124473572" Z="1.25" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>