<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#193" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2816" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004715576172" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766443847656" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.13684765625" Y="-4.631571777344" />
                  <Point X="-24.4366953125" Y="-3.512524902344" />
                  <Point X="-24.44227734375" Y="-3.497139648438" />
                  <Point X="-24.457634765625" Y="-3.467376953125" />
                  <Point X="-24.58019140625" Y="-3.290797851562" />
                  <Point X="-24.62136328125" Y="-3.2314765625" />
                  <Point X="-24.643248046875" Y="-3.209021728516" />
                  <Point X="-24.66950390625" Y="-3.18977734375" />
                  <Point X="-24.69750390625" Y="-3.175669189453" />
                  <Point X="-24.887150390625" Y="-3.116809570313" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9790234375" Y="-3.092766357422" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.226470703125" Y="-3.155895263672" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.18977734375" />
                  <Point X="-25.344439453125" Y="-3.209021484375" />
                  <Point X="-25.36632421875" Y="-3.231477050781" />
                  <Point X="-25.48887890625" Y="-3.408055908203" />
                  <Point X="-25.53005078125" Y="-3.467377441406" />
                  <Point X="-25.5381875" Y="-3.481571777344" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.651724609375" Y="-3.888467041016" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.0082421875" Y="-4.859150390625" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.24641796875" Y="-4.802362304688" />
                  <Point X="-26.2149609375" Y="-4.563436523438" />
                  <Point X="-26.21419921875" Y="-4.547929199219" />
                  <Point X="-26.2165078125" Y="-4.51622265625" />
                  <Point X="-26.261814453125" Y="-4.288450683594" />
                  <Point X="-26.277037109375" Y="-4.211931152344" />
                  <Point X="-26.287939453125" Y="-4.182962890625" />
                  <Point X="-26.30401171875" Y="-4.155126464844" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.49824609375" Y="-3.978080810547" />
                  <Point X="-26.556904296875" Y="-3.926639404297" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.874765625" Y="-3.87577734375" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708740234" />
                  <Point X="-27.014466796875" Y="-3.882028564453" />
                  <Point X="-27.042658203125" Y="-3.894801757812" />
                  <Point X="-27.23575390625" Y="-4.023824462891" />
                  <Point X="-27.300623046875" Y="-4.067169677734" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.3351015625" Y="-4.099461425781" />
                  <Point X="-27.39165625" Y="-4.1731640625" />
                  <Point X="-27.4801484375" Y="-4.288488769531" />
                  <Point X="-27.6975" Y="-4.15391015625" />
                  <Point X="-27.80171484375" Y="-4.089383544922" />
                  <Point X="-28.10234375" Y="-3.857907958984" />
                  <Point X="-28.104720703125" Y="-3.856077148438" />
                  <Point X="-27.980994140625" Y="-3.641775634766" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647654541016" />
                  <Point X="-27.406587890625" Y="-2.616127929688" />
                  <Point X="-27.40557421875" Y="-2.585193847656" />
                  <Point X="-27.41455859375" Y="-2.555575927734" />
                  <Point X="-27.428775390625" Y="-2.526746826172" />
                  <Point X="-27.4468046875" Y="-2.501587890625" />
                  <Point X="-27.4641484375" Y="-2.484244628906" />
                  <Point X="-27.489294921875" Y="-2.466222412109" />
                  <Point X="-27.518125" Y="-2.452000976562" />
                  <Point X="-27.54774609375" Y="-2.443012939453" />
                  <Point X="-27.57868359375" Y="-2.444023925781" />
                  <Point X="-27.610212890625" Y="-2.450294433594" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-27.96342578125" Y="-2.648399169922" />
                  <Point X="-28.818021484375" Y="-3.141800537109" />
                  <Point X="-29.000529296875" Y="-2.902025146484" />
                  <Point X="-29.082865234375" Y="-2.793849365234" />
                  <Point X="-29.298390625" Y="-2.432448242188" />
                  <Point X="-29.306142578125" Y="-2.419449951172" />
                  <Point X="-29.080931640625" Y="-2.246639892578" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.084578125" Y="-1.47559375" />
                  <Point X="-28.06661328125" Y="-1.448462768555" />
                  <Point X="-28.053857421875" Y="-1.419834960938" />
                  <Point X="-28.04808984375" Y="-1.397568237305" />
                  <Point X="-28.04615234375" Y="-1.390087646484" />
                  <Point X="-28.04334765625" Y="-1.359656860352" />
                  <Point X="-28.045556640625" Y="-1.327985107422" />
                  <Point X="-28.05255859375" Y="-1.298238769531" />
                  <Point X="-28.068642578125" Y="-1.272254394531" />
                  <Point X="-28.0894765625" Y="-1.248297729492" />
                  <Point X="-28.11297265625" Y="-1.228766845703" />
                  <Point X="-28.132794921875" Y="-1.217099853516" />
                  <Point X="-28.139455078125" Y="-1.213180297852" />
                  <Point X="-28.168720703125" Y="-1.201955444336" />
                  <Point X="-28.20060546875" Y="-1.195474609375" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-28.64125390625" Y="-1.248272705078" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.801875" Y="-1.118718505859" />
                  <Point X="-29.834076171875" Y="-0.992649841309" />
                  <Point X="-29.8911015625" Y="-0.593948059082" />
                  <Point X="-29.892423828125" Y="-0.584698608398" />
                  <Point X="-29.64351953125" Y="-0.518005065918" />
                  <Point X="-28.532875" Y="-0.220408309937" />
                  <Point X="-28.5174921875" Y="-0.214827133179" />
                  <Point X="-28.4877265625" Y="-0.199468673706" />
                  <Point X="-28.466966796875" Y="-0.185060073853" />
                  <Point X="-28.459986328125" Y="-0.180216949463" />
                  <Point X="-28.4375234375" Y="-0.158329238892" />
                  <Point X="-28.418275390625" Y="-0.132070251465" />
                  <Point X="-28.4041640625" Y="-0.104063110352" />
                  <Point X="-28.397240234375" Y="-0.081751747131" />
                  <Point X="-28.3949140625" Y="-0.07425617981" />
                  <Point X="-28.390646484375" Y="-0.046099822998" />
                  <Point X="-28.390646484375" Y="-0.016460323334" />
                  <Point X="-28.3949140625" Y="0.011696035385" />
                  <Point X="-28.401837890625" Y="0.034007553101" />
                  <Point X="-28.4041640625" Y="0.041503112793" />
                  <Point X="-28.418275390625" Y="0.069507835388" />
                  <Point X="-28.43751953125" Y="0.095763168335" />
                  <Point X="-28.4599765625" Y="0.1176483078" />
                  <Point X="-28.480751953125" Y="0.132066574097" />
                  <Point X="-28.49009375" Y="0.137781784058" />
                  <Point X="-28.512640625" Y="0.149846847534" />
                  <Point X="-28.532875" Y="0.157848327637" />
                  <Point X="-28.905994140625" Y="0.257825653076" />
                  <Point X="-29.891814453125" Y="0.521975402832" />
                  <Point X="-29.84523828125" Y="0.83673828125" />
                  <Point X="-29.82448828125" Y="0.976968383789" />
                  <Point X="-29.70969921875" Y="1.400575927734" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-29.561802734375" Y="1.404606567383" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228027344" />
                  <Point X="-28.703134765625" Y="1.305263549805" />
                  <Point X="-28.65715625" Y="1.319760742188" />
                  <Point X="-28.641708984375" Y="1.324631103516" />
                  <Point X="-28.622775390625" Y="1.332963012695" />
                  <Point X="-28.60403125" Y="1.343785766602" />
                  <Point X="-28.5873515625" Y="1.356015625" />
                  <Point X="-28.57371484375" Y="1.371565307617" />
                  <Point X="-28.561298828125" Y="1.389296386719" />
                  <Point X="-28.55134765625" Y="1.407433349609" />
                  <Point X="-28.5328984375" Y="1.451974365234" />
                  <Point X="-28.526701171875" Y="1.46693762207" />
                  <Point X="-28.520912109375" Y="1.486805297852" />
                  <Point X="-28.51715625" Y="1.508118896484" />
                  <Point X="-28.5158046875" Y="1.528750732422" />
                  <Point X="-28.51894921875" Y="1.549186157227" />
                  <Point X="-28.524548828125" Y="1.570090942383" />
                  <Point X="-28.532046875" Y="1.589374389648" />
                  <Point X="-28.554306640625" Y="1.632137817383" />
                  <Point X="-28.561798828125" Y="1.646524902344" />
                  <Point X="-28.573287109375" Y="1.663714355469" />
                  <Point X="-28.587197265625" Y="1.680290161133" />
                  <Point X="-28.60213671875" Y="1.694590087891" />
                  <Point X="-28.816158203125" Y="1.858815551758" />
                  <Point X="-29.351859375" Y="2.269873291016" />
                  <Point X="-29.161783203125" Y="2.595518310547" />
                  <Point X="-29.081146484375" Y="2.733665527344" />
                  <Point X="-28.7770859375" Y="3.124494873047" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.69562109375" Y="3.126975585938" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.082755859375" Y="2.820193847656" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.99901171875" Y="2.826505126953" />
                  <Point X="-27.980458984375" Y="2.835655029297" />
                  <Point X="-27.962205078125" Y="2.847284912109" />
                  <Point X="-27.946078125" Y="2.860228759766" />
                  <Point X="-27.900623046875" Y="2.905682128906" />
                  <Point X="-27.885353515625" Y="2.920952148438" />
                  <Point X="-27.87240625" Y="2.937083007812" />
                  <Point X="-27.86077734375" Y="2.955335693359" />
                  <Point X="-27.85162890625" Y="2.973885498047" />
                  <Point X="-27.846712890625" Y="2.993975585938" />
                  <Point X="-27.84388671875" Y="3.015432373047" />
                  <Point X="-27.84343359375" Y="3.036119384766" />
                  <Point X="-27.849037109375" Y="3.100156005859" />
                  <Point X="-27.85091796875" Y="3.121668945312" />
                  <Point X="-27.854953125" Y="3.141956054688" />
                  <Point X="-27.861462890625" Y="3.162601318359" />
                  <Point X="-27.869794921875" Y="3.181532958984" />
                  <Point X="-27.964634765625" Y="3.345800537109" />
                  <Point X="-28.18333203125" Y="3.724595703125" />
                  <Point X="-27.841052734375" Y="3.987017822266" />
                  <Point X="-27.70062109375" Y="4.094685791016" />
                  <Point X="-27.22173046875" Y="4.360747070312" />
                  <Point X="-27.16703515625" Y="4.391134277344" />
                  <Point X="-27.0431953125" Y="4.229742675781" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.99511328125" Y="4.189395019531" />
                  <Point X="-26.923841796875" Y="4.15229296875" />
                  <Point X="-26.899896484375" Y="4.139828125" />
                  <Point X="-26.88061328125" Y="4.132330078125" />
                  <Point X="-26.859705078125" Y="4.126728515625" />
                  <Point X="-26.839265625" Y="4.123582519531" />
                  <Point X="-26.818626953125" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128692871094" />
                  <Point X="-26.777451171875" Y="4.134481933594" />
                  <Point X="-26.703216796875" Y="4.165231445312" />
                  <Point X="-26.67827734375" Y="4.175561523437" />
                  <Point X="-26.66014453125" Y="4.185509277344" />
                  <Point X="-26.6424140625" Y="4.197923828125" />
                  <Point X="-26.626859375" Y="4.211564453125" />
                  <Point X="-26.614626953125" Y="4.228249511719" />
                  <Point X="-26.603806640625" Y="4.246993652344" />
                  <Point X="-26.595478515625" Y="4.265921386719" />
                  <Point X="-26.57131640625" Y="4.342553710938" />
                  <Point X="-26.56319921875" Y="4.368297851562" />
                  <Point X="-26.5591640625" Y="4.388583496094" />
                  <Point X="-26.55727734375" Y="4.410145019531" />
                  <Point X="-26.557728515625" Y="4.430827148438" />
                  <Point X="-26.56851171875" Y="4.5127265625" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.131427734375" Y="4.75883984375" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.369072265625" Y="4.877754394531" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.278552734375" Y="4.826155273438" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286314941406" />
                  <Point X="-24.8051875" Y="4.467673828125" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.314697265625" Y="4.84836328125" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.675640625" Y="4.715775390625" />
                  <Point X="-23.5189765625" Y="4.677951660156" />
                  <Point X="-23.206705078125" Y="4.564688476562" />
                  <Point X="-23.105349609375" Y="4.527926757812" />
                  <Point X="-22.80304296875" Y="4.386548339844" />
                  <Point X="-22.705423828125" Y="4.340894042969" />
                  <Point X="-22.41335546875" Y="4.170734863281" />
                  <Point X="-22.319021484375" Y="4.115775878906" />
                  <Point X="-22.05673828125" Y="3.929254394531" />
                  <Point X="-22.2074609375" Y="3.668193847656" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.85791796875" Y="2.539942382812" />
                  <Point X="-22.866921875" Y="2.516059326172" />
                  <Point X="-22.8829921875" Y="2.455962402344" />
                  <Point X="-22.888392578125" Y="2.435772949219" />
                  <Point X="-22.891380859375" Y="2.417937255859" />
                  <Point X="-22.892271484375" Y="2.38094921875" />
                  <Point X="-22.88600390625" Y="2.328982666016" />
                  <Point X="-22.883900390625" Y="2.311530273438" />
                  <Point X="-22.878560546875" Y="2.289617431641" />
                  <Point X="-22.870294921875" Y="2.267523925781" />
                  <Point X="-22.859927734375" Y="2.247469482422" />
                  <Point X="-22.827771484375" Y="2.200081054688" />
                  <Point X="-22.8224453125" Y="2.192971679688" />
                  <Point X="-22.798341796875" Y="2.163751708984" />
                  <Point X="-22.778400390625" Y="2.145592285156" />
                  <Point X="-22.73101171875" Y="2.113437011719" />
                  <Point X="-22.715091796875" Y="2.102634765625" />
                  <Point X="-22.695046875" Y="2.092272216797" />
                  <Point X="-22.672958984375" Y="2.084006347656" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.599068359375" Y="2.072397216797" />
                  <Point X="-22.589701171875" Y="2.071735107422" />
                  <Point X="-22.553341796875" Y="2.070967529297" />
                  <Point X="-22.526794921875" Y="2.074170898438" />
                  <Point X="-22.466697265625" Y="2.090241699219" />
                  <Point X="-22.4465078125" Y="2.095640625" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-22.036177734375" Y="2.326817138672" />
                  <Point X="-21.032673828125" Y="2.906190429688" />
                  <Point X="-20.932685546875" Y="2.767229492188" />
                  <Point X="-20.87671875" Y="2.689448974609" />
                  <Point X="-20.73780078125" Y="2.459883544922" />
                  <Point X="-20.9200703125" Y="2.320022949219" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.778576171875" Y="1.660239379883" />
                  <Point X="-21.796025390625" Y="1.641626586914" />
                  <Point X="-21.83927734375" Y="1.585201293945" />
                  <Point X="-21.853806640625" Y="1.566245239258" />
                  <Point X="-21.863390625" Y="1.550916381836" />
                  <Point X="-21.878369140625" Y="1.517089477539" />
                  <Point X="-21.89448046875" Y="1.459479125977" />
                  <Point X="-21.89989453125" Y="1.440125" />
                  <Point X="-21.90334765625" Y="1.417827026367" />
                  <Point X="-21.9041640625" Y="1.394253173828" />
                  <Point X="-21.902259765625" Y="1.371766723633" />
                  <Point X="-21.889033203125" Y="1.30766784668" />
                  <Point X="-21.88458984375" Y="1.286133789062" />
                  <Point X="-21.879318359375" Y="1.268976928711" />
                  <Point X="-21.863716796875" Y="1.235741943359" />
                  <Point X="-21.827744140625" Y="1.181065185547" />
                  <Point X="-21.81566015625" Y="1.162696533203" />
                  <Point X="-21.801109375" Y="1.14545324707" />
                  <Point X="-21.783865234375" Y="1.129362548828" />
                  <Point X="-21.76565234375" Y="1.116034912109" />
                  <Point X="-21.713521484375" Y="1.086690795898" />
                  <Point X="-21.696009765625" Y="1.076832519531" />
                  <Point X="-21.67948046875" Y="1.069502563477" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.5733984375" Y="1.050123413086" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-21.155298828125" Y="1.093918212891" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.17809765625" Y="1.031529907227" />
                  <Point X="-20.15405859375" Y="0.932786865234" />
                  <Point X="-20.1091328125" Y="0.644238830566" />
                  <Point X="-20.31028125" Y="0.590341186523" />
                  <Point X="-21.283419921875" Y="0.32958972168" />
                  <Point X="-21.295208984375" Y="0.325586120605" />
                  <Point X="-21.318453125" Y="0.315067932129" />
                  <Point X="-21.38769921875" Y="0.275042327881" />
                  <Point X="-21.410962890625" Y="0.261595672607" />
                  <Point X="-21.42569140625" Y="0.251093765259" />
                  <Point X="-21.45246875" Y="0.225576126099" />
                  <Point X="-21.494017578125" Y="0.172634216309" />
                  <Point X="-21.507974609375" Y="0.154848480225" />
                  <Point X="-21.51969921875" Y="0.135567153931" />
                  <Point X="-21.52947265625" Y="0.114104003906" />
                  <Point X="-21.536318359375" Y="0.092603652954" />
                  <Point X="-21.55016796875" Y="0.02028767395" />
                  <Point X="-21.5548203125" Y="-0.004006832123" />
                  <Point X="-21.556515625" Y="-0.021876050949" />
                  <Point X="-21.5548203125" Y="-0.058553161621" />
                  <Point X="-21.540970703125" Y="-0.130869293213" />
                  <Point X="-21.536318359375" Y="-0.155163803101" />
                  <Point X="-21.52947265625" Y="-0.176664154053" />
                  <Point X="-21.51969921875" Y="-0.198127304077" />
                  <Point X="-21.50797265625" Y="-0.217408630371" />
                  <Point X="-21.466423828125" Y="-0.270350402832" />
                  <Point X="-21.452466796875" Y="-0.288136260986" />
                  <Point X="-21.44000390625" Y="-0.301231994629" />
                  <Point X="-21.410962890625" Y="-0.324155670166" />
                  <Point X="-21.341716796875" Y="-0.364181427002" />
                  <Point X="-21.318453125" Y="-0.377628082275" />
                  <Point X="-21.3072890625" Y="-0.38313885498" />
                  <Point X="-21.283419921875" Y="-0.392149871826" />
                  <Point X="-20.956494140625" Y="-0.479749176025" />
                  <Point X="-20.108525390625" Y="-0.706961914062" />
                  <Point X="-20.131556640625" Y="-0.859719970703" />
                  <Point X="-20.144974609375" Y="-0.948725036621" />
                  <Point X="-20.19882421875" Y="-1.18469921875" />
                  <Point X="-20.44705078125" Y="-1.15201953125" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710144043" />
                  <Point X="-21.62533984375" Y="-1.005508728027" />
                  <Point X="-21.76124609375" Y="-1.035048706055" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.836025390625" Y="-1.056596923828" />
                  <Point X="-21.8638515625" Y="-1.073489135742" />
                  <Point X="-21.8876015625" Y="-1.093960083008" />
                  <Point X="-21.969748046875" Y="-1.192757324219" />
                  <Point X="-21.997345703125" Y="-1.225947998047" />
                  <Point X="-22.012064453125" Y="-1.250329589844" />
                  <Point X="-22.023408203125" Y="-1.277715454102" />
                  <Point X="-22.030240234375" Y="-1.305365722656" />
                  <Point X="-22.042013671875" Y="-1.433312744141" />
                  <Point X="-22.04596875" Y="-1.476296386719" />
                  <Point X="-22.04365234375" Y="-1.507562011719" />
                  <Point X="-22.035921875" Y="-1.539182617188" />
                  <Point X="-22.023548828125" Y="-1.567996704102" />
                  <Point X="-21.9483359375" Y="-1.684984985352" />
                  <Point X="-21.923068359375" Y="-1.724287231445" />
                  <Point X="-21.913064453125" Y="-1.737241577148" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.585982421875" Y="-1.99370715332" />
                  <Point X="-20.786876953125" Y="-2.606882080078" />
                  <Point X="-20.83736328125" Y="-2.688575927734" />
                  <Point X="-20.875203125" Y="-2.749803955078" />
                  <Point X="-20.971017578125" Y="-2.8859453125" />
                  <Point X="-21.193904296875" Y="-2.757261474609" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.245775390625" Y="-2.159825195312" />
                  <Point X="-22.407525390625" Y="-2.13061328125" />
                  <Point X="-22.461865234375" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395507812" />
                  <Point X="-22.525390625" Y="-2.125353515625" />
                  <Point X="-22.555166015625" Y="-2.135177001953" />
                  <Point X="-22.689541015625" Y="-2.205897460938" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.75761328125" Y="-2.246548095703" />
                  <Point X="-22.778572265625" Y="-2.267506591797" />
                  <Point X="-22.795466796875" Y="-2.290438232422" />
                  <Point X="-22.8661875" Y="-2.424813232422" />
                  <Point X="-22.889947265625" Y="-2.469956298828" />
                  <Point X="-22.899771484375" Y="-2.499734863281" />
                  <Point X="-22.904728515625" Y="-2.531909179688" />
                  <Point X="-22.90432421875" Y="-2.563259277344" />
                  <Point X="-22.875111328125" Y="-2.725009765625" />
                  <Point X="-22.865296875" Y="-2.779349609375" />
                  <Point X="-22.86101171875" Y="-2.795140869141" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.653220703125" Y="-3.163754638672" />
                  <Point X="-22.13871484375" Y="-4.054904541016" />
                  <Point X="-22.17326171875" Y="-4.079580810547" />
                  <Point X="-22.218158203125" Y="-4.111649414062" />
                  <Point X="-22.298234375" Y="-4.16348046875" />
                  <Point X="-22.4744375" Y="-3.933847900391" />
                  <Point X="-23.241451171875" Y="-2.934254882812" />
                  <Point X="-23.25249609375" Y="-2.922177490234" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.43760546875" Y="-2.797994140625" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.520009765625" Y="-2.751167236328" />
                  <Point X="-23.551630859375" Y="-2.743435546875" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.757373046875" Y="-2.757171875" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.843634765625" Y="-2.769396972656" />
                  <Point X="-23.87101953125" Y="-2.780739746094" />
                  <Point X="-23.89540234375" Y="-2.7954609375" />
                  <Point X="-24.030125" Y="-2.907479003906" />
                  <Point X="-24.07538671875" Y="-2.945111328125" />
                  <Point X="-24.095857421875" Y="-2.968860839844" />
                  <Point X="-24.11275" Y="-2.9966875" />
                  <Point X="-24.124375" Y="-3.02580859375" />
                  <Point X="-24.16465625" Y="-3.211135986328" />
                  <Point X="-24.178189453125" Y="-3.273396484375" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323120361328" />
                  <Point X="-24.1250078125" Y="-3.74278125" />
                  <Point X="-23.977935546875" Y="-4.859915527344" />
                  <Point X="-23.98185546875" Y="-4.860774902344" />
                  <Point X="-24.024330078125" Y="-4.870085449219" />
                  <Point X="-24.07068359375" Y="-4.878506347656" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-25.990140625" Y="-4.765891113281" />
                  <Point X="-26.05842578125" Y="-4.75263671875" />
                  <Point X="-26.14124609375" Y="-4.731328125" />
                  <Point X="-26.1207734375" Y="-4.575837402344" />
                  <Point X="-26.120076171875" Y="-4.568097167969" />
                  <Point X="-26.11944921875" Y="-4.541030273438" />
                  <Point X="-26.1217578125" Y="-4.509323730469" />
                  <Point X="-26.123333984375" Y="-4.497688964844" />
                  <Point X="-26.168640625" Y="-4.269916992188" />
                  <Point X="-26.18386328125" Y="-4.193397460938" />
                  <Point X="-26.188125" Y="-4.17846875" />
                  <Point X="-26.19902734375" Y="-4.149500488281" />
                  <Point X="-26.20566796875" Y="-4.1354609375" />
                  <Point X="-26.221740234375" Y="-4.107624511719" />
                  <Point X="-26.230576171875" Y="-4.094858642578" />
                  <Point X="-26.250208984375" Y="-4.070936279297" />
                  <Point X="-26.261005859375" Y="-4.059779785156" />
                  <Point X="-26.435607421875" Y="-3.906656494141" />
                  <Point X="-26.494265625" Y="-3.855215087891" />
                  <Point X="-26.50673828125" Y="-3.845965332031" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.868552734375" Y="-3.780980712891" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166259766" />
                  <Point X="-27.008005859375" Y="-3.781946044922" />
                  <Point X="-27.0390546875" Y="-3.790265869141" />
                  <Point X="-27.053673828125" Y="-3.795496337891" />
                  <Point X="-27.081865234375" Y="-3.80826953125" />
                  <Point X="-27.0954375" Y="-3.815812255859" />
                  <Point X="-27.288533203125" Y="-3.944834960938" />
                  <Point X="-27.35340234375" Y="-3.988180175781" />
                  <Point X="-27.35968359375" Y="-3.992759765625" />
                  <Point X="-27.380439453125" Y="-4.010130859375" />
                  <Point X="-27.402755859375" Y="-4.032769287109" />
                  <Point X="-27.410470703125" Y="-4.041628662109" />
                  <Point X="-27.467025390625" Y="-4.115331054688" />
                  <Point X="-27.503203125" Y="-4.162477539063" />
                  <Point X="-27.64748828125" Y="-4.073139648438" />
                  <Point X="-27.747595703125" Y="-4.01115625" />
                  <Point X="-27.980861328125" Y="-3.831548095703" />
                  <Point X="-27.898720703125" Y="-3.689275390625" />
                  <Point X="-27.34148828125" Y="-2.724119384766" />
                  <Point X="-27.3348515625" Y="-2.710085205078" />
                  <Point X="-27.32394921875" Y="-2.681120117188" />
                  <Point X="-27.319685546875" Y="-2.666189453125" />
                  <Point X="-27.3134140625" Y="-2.634662841797" />
                  <Point X="-27.311638671875" Y="-2.619239257812" />
                  <Point X="-27.310625" Y="-2.588305175781" />
                  <Point X="-27.3146640625" Y="-2.5576171875" />
                  <Point X="-27.3236484375" Y="-2.527999267578" />
                  <Point X="-27.32935546875" Y="-2.513558837891" />
                  <Point X="-27.343572265625" Y="-2.484729736328" />
                  <Point X="-27.351556640625" Y="-2.47141015625" />
                  <Point X="-27.3695859375" Y="-2.446251220703" />
                  <Point X="-27.379630859375" Y="-2.434411865234" />
                  <Point X="-27.396974609375" Y="-2.417068603516" />
                  <Point X="-27.40880859375" Y="-2.407027832031" />
                  <Point X="-27.433955078125" Y="-2.389005615234" />
                  <Point X="-27.447267578125" Y="-2.381024169922" />
                  <Point X="-27.47609765625" Y="-2.366802734375" />
                  <Point X="-27.490541015625" Y="-2.36109375" />
                  <Point X="-27.520162109375" Y="-2.352105712891" />
                  <Point X="-27.550849609375" Y="-2.348063720703" />
                  <Point X="-27.581787109375" Y="-2.349074707031" />
                  <Point X="-27.59721484375" Y="-2.350848632812" />
                  <Point X="-27.628744140625" Y="-2.357119140625" />
                  <Point X="-27.64367578125" Y="-2.361383056641" />
                  <Point X="-27.67264453125" Y="-2.372285888672" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.01092578125" Y="-2.566126708984" />
                  <Point X="-28.7930859375" Y="-3.017707519531" />
                  <Point X="-28.924935546875" Y="-2.844486572266" />
                  <Point X="-29.00402734375" Y="-2.740573242188" />
                  <Point X="-29.181265625" Y="-2.443373291016" />
                  <Point X="-29.023099609375" Y="-2.322008544922" />
                  <Point X="-28.048123046875" Y="-1.573881958008" />
                  <Point X="-28.036482421875" Y="-1.563309814453" />
                  <Point X="-28.01510546875" Y="-1.540390136719" />
                  <Point X="-28.005369140625" Y="-1.528042602539" />
                  <Point X="-27.987404296875" Y="-1.500911499023" />
                  <Point X="-27.979837890625" Y="-1.487127807617" />
                  <Point X="-27.96708203125" Y="-1.45850012207" />
                  <Point X="-27.961892578125" Y="-1.443656005859" />
                  <Point X="-27.956125" Y="-1.421389282227" />
                  <Point X="-27.951552734375" Y="-1.398806518555" />
                  <Point X="-27.948748046875" Y="-1.368375732422" />
                  <Point X="-27.948578125" Y="-1.35304699707" />
                  <Point X="-27.950787109375" Y="-1.321375244141" />
                  <Point X="-27.953083984375" Y="-1.306218139648" />
                  <Point X="-27.9600859375" Y="-1.276471801758" />
                  <Point X="-27.97178125" Y="-1.248238647461" />
                  <Point X="-27.987865234375" Y="-1.222254150391" />
                  <Point X="-27.996958984375" Y="-1.209913696289" />
                  <Point X="-28.01779296875" Y="-1.185957275391" />
                  <Point X="-28.02875" Y="-1.175241455078" />
                  <Point X="-28.05224609375" Y="-1.155710693359" />
                  <Point X="-28.06478515625" Y="-1.146895385742" />
                  <Point X="-28.084607421875" Y="-1.135228393555" />
                  <Point X="-28.10543359375" Y="-1.124480834961" />
                  <Point X="-28.13469921875" Y="-1.113255981445" />
                  <Point X="-28.149798828125" Y="-1.108858886719" />
                  <Point X="-28.18168359375" Y="-1.102378295898" />
                  <Point X="-28.197298828125" Y="-1.100532226562" />
                  <Point X="-28.22862109375" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196655273" />
                  <Point X="-28.653654296875" Y="-1.154085449219" />
                  <Point X="-29.66091796875" Y="-1.286694335938" />
                  <Point X="-29.709830078125" Y="-1.095207275391" />
                  <Point X="-29.740759765625" Y="-0.974113525391" />
                  <Point X="-29.786451171875" Y="-0.654654846191" />
                  <Point X="-29.618931640625" Y="-0.609767944336" />
                  <Point X="-28.508287109375" Y="-0.312171234131" />
                  <Point X="-28.500474609375" Y="-0.309712127686" />
                  <Point X="-28.473931640625" Y="-0.299251281738" />
                  <Point X="-28.444166015625" Y="-0.28389276123" />
                  <Point X="-28.43355859375" Y="-0.277512664795" />
                  <Point X="-28.412798828125" Y="-0.263104125977" />
                  <Point X="-28.412798828125" Y="-0.263103942871" />
                  <Point X="-28.3936875" Y="-0.248257583618" />
                  <Point X="-28.371224609375" Y="-0.226369888306" />
                  <Point X="-28.36090234375" Y="-0.214492538452" />
                  <Point X="-28.341654296875" Y="-0.188233551025" />
                  <Point X="-28.333435546875" Y="-0.174816467285" />
                  <Point X="-28.31932421875" Y="-0.146809371948" />
                  <Point X="-28.313431640625" Y="-0.132219665527" />
                  <Point X="-28.3065078125" Y="-0.109908241272" />
                  <Point X="-28.300986328125" Y="-0.088492424011" />
                  <Point X="-28.29671875" Y="-0.060336116791" />
                  <Point X="-28.295646484375" Y="-0.046099822998" />
                  <Point X="-28.295646484375" Y="-0.016460256577" />
                  <Point X="-28.29671875" Y="-0.002224114895" />
                  <Point X="-28.300986328125" Y="0.025932344437" />
                  <Point X="-28.304181640625" Y="0.039852367401" />
                  <Point X="-28.31110546875" Y="0.0621639328" />
                  <Point X="-28.319326171875" Y="0.08425226593" />
                  <Point X="-28.3334375" Y="0.112256980896" />
                  <Point X="-28.341654296875" Y="0.125668861389" />
                  <Point X="-28.3608984375" Y="0.151924133301" />
                  <Point X="-28.371216796875" Y="0.163798965454" />
                  <Point X="-28.393673828125" Y="0.185684127808" />
                  <Point X="-28.4058125" Y="0.195694473267" />
                  <Point X="-28.426587890625" Y="0.210112686157" />
                  <Point X="-28.445271484375" Y="0.221543411255" />
                  <Point X="-28.467818359375" Y="0.233608474731" />
                  <Point X="-28.47770703125" Y="0.238190368652" />
                  <Point X="-28.49794140625" Y="0.24619178772" />
                  <Point X="-28.508287109375" Y="0.249611297607" />
                  <Point X="-28.88140625" Y="0.349588592529" />
                  <Point X="-29.7854453125" Y="0.591824829102" />
                  <Point X="-29.75126171875" Y="0.822832275391" />
                  <Point X="-29.73133203125" Y="0.957522155762" />
                  <Point X="-29.633583984375" Y="1.318237060547" />
                  <Point X="-29.574203125" Y="1.310419311523" />
                  <Point X="-28.778064453125" Y="1.20560546875" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.20470324707" />
                  <Point X="-28.71514453125" Y="1.206589599609" />
                  <Point X="-28.704890625" Y="1.208053222656" />
                  <Point X="-28.6846015625" Y="1.212088745117" />
                  <Point X="-28.67456640625" Y="1.214660522461" />
                  <Point X="-28.628587890625" Y="1.229157592773" />
                  <Point X="-28.6034453125" Y="1.237678100586" />
                  <Point X="-28.58451171875" Y="1.246010009766" />
                  <Point X="-28.5752734375" Y="1.250692260742" />
                  <Point X="-28.556529296875" Y="1.261514892578" />
                  <Point X="-28.547857421875" Y="1.267173095703" />
                  <Point X="-28.531177734375" Y="1.279402832031" />
                  <Point X="-28.515927734375" Y="1.293377685547" />
                  <Point X="-28.502291015625" Y="1.308927368164" />
                  <Point X="-28.495896484375" Y="1.317073852539" />
                  <Point X="-28.48348046875" Y="1.334804931641" />
                  <Point X="-28.47801171875" Y="1.343599243164" />
                  <Point X="-28.468060546875" Y="1.361736206055" />
                  <Point X="-28.463578125" Y="1.371078857422" />
                  <Point X="-28.44512890625" Y="1.415619873047" />
                  <Point X="-28.435494140625" Y="1.440361572266" />
                  <Point X="-28.429705078125" Y="1.460229370117" />
                  <Point X="-28.427353515625" Y="1.470318481445" />
                  <Point X="-28.42359765625" Y="1.491632080078" />
                  <Point X="-28.422359375" Y="1.501908935547" />
                  <Point X="-28.4210078125" Y="1.522540771484" />
                  <Point X="-28.42191015625" Y="1.543198974609" />
                  <Point X="-28.4250546875" Y="1.563634399414" />
                  <Point X="-28.42718359375" Y="1.573766601562" />
                  <Point X="-28.432783203125" Y="1.594671386719" />
                  <Point X="-28.436005859375" Y="1.604519042969" />
                  <Point X="-28.44350390625" Y="1.623802368164" />
                  <Point X="-28.447779296875" Y="1.63323828125" />
                  <Point X="-28.4700390625" Y="1.676001708984" />
                  <Point X="-28.470046875" Y="1.676016601562" />
                  <Point X="-28.482814453125" Y="1.6993125" />
                  <Point X="-28.494302734375" Y="1.716501953125" />
                  <Point X="-28.500515625" Y="1.724782714844" />
                  <Point X="-28.51442578125" Y="1.741358642578" />
                  <Point X="-28.5215078125" Y="1.748918212891" />
                  <Point X="-28.536447265625" Y="1.763218139648" />
                  <Point X="-28.5443046875" Y="1.769958496094" />
                  <Point X="-28.758326171875" Y="1.934183959961" />
                  <Point X="-29.22761328125" Y="2.294280517578" />
                  <Point X="-29.079736328125" Y="2.547628662109" />
                  <Point X="-29.002283203125" Y="2.680323242188" />
                  <Point X="-28.726337890625" Y="3.035013183594" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.09103515625" Y="2.725555419922" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310791016" />
                  <Point X="-27.976431640625" Y="2.734227783203" />
                  <Point X="-27.9569921875" Y="2.741303466797" />
                  <Point X="-27.938439453125" Y="2.750453369141" />
                  <Point X="-27.929412109375" Y="2.755534667969" />
                  <Point X="-27.911158203125" Y="2.767164550781" />
                  <Point X="-27.902740234375" Y="2.773197265625" />
                  <Point X="-27.88661328125" Y="2.786141113281" />
                  <Point X="-27.878904296875" Y="2.793052246094" />
                  <Point X="-27.83344921875" Y="2.838505615234" />
                  <Point X="-27.8181796875" Y="2.853775634766" />
                  <Point X="-27.811265625" Y="2.861487060547" />
                  <Point X="-27.798318359375" Y="2.877617919922" />
                  <Point X="-27.79228515625" Y="2.886037353516" />
                  <Point X="-27.78065625" Y="2.904290039062" />
                  <Point X="-27.775576171875" Y="2.913315673828" />
                  <Point X="-27.766427734375" Y="2.931865478516" />
                  <Point X="-27.7593515625" Y="2.951305419922" />
                  <Point X="-27.754435546875" Y="2.971395507813" />
                  <Point X="-27.75252734375" Y="2.981569824219" />
                  <Point X="-27.749701171875" Y="3.003026611328" />
                  <Point X="-27.74891015625" Y="3.013352050781" />
                  <Point X="-27.74845703125" Y="3.0340390625" />
                  <Point X="-27.748794921875" Y="3.044400634766" />
                  <Point X="-27.7543984375" Y="3.108437255859" />
                  <Point X="-27.756279296875" Y="3.129950195312" />
                  <Point X="-27.757744140625" Y="3.140201660156" />
                  <Point X="-27.761779296875" Y="3.160488769531" />
                  <Point X="-27.764349609375" Y="3.170524414062" />
                  <Point X="-27.770859375" Y="3.191169677734" />
                  <Point X="-27.77451171875" Y="3.200869628906" />
                  <Point X="-27.78284375" Y="3.219801269531" />
                  <Point X="-27.7875234375" Y="3.229032958984" />
                  <Point X="-27.88236328125" Y="3.393300537109" />
                  <Point X="-28.05938671875" Y="3.699914550781" />
                  <Point X="-27.78325" Y="3.911625976562" />
                  <Point X="-27.648365234375" Y="4.015041259766" />
                  <Point X="-27.1925234375" Y="4.268296875" />
                  <Point X="-27.118564453125" Y="4.171910644531" />
                  <Point X="-27.11182421875" Y="4.164053710938" />
                  <Point X="-27.097521484375" Y="4.149110839844" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.065091796875" Y="4.121897460938" />
                  <Point X="-27.047892578125" Y="4.110405273438" />
                  <Point X="-27.03898046875" Y="4.105129394531" />
                  <Point X="-26.967708984375" Y="4.068027099609" />
                  <Point X="-26.943763671875" Y="4.055562255859" />
                  <Point X="-26.93432421875" Y="4.051286132812" />
                  <Point X="-26.915041015625" Y="4.043788085938" />
                  <Point X="-26.905197265625" Y="4.040566162109" />
                  <Point X="-26.8842890625" Y="4.034964599609" />
                  <Point X="-26.87415625" Y="4.032834228516" />
                  <Point X="-26.853716796875" Y="4.029688232422" />
                  <Point X="-26.833052734375" Y="4.028785888672" />
                  <Point X="-26.8124140625" Y="4.030138427734" />
                  <Point X="-26.8021328125" Y="4.031377929688" />
                  <Point X="-26.780818359375" Y="4.035135742188" />
                  <Point X="-26.770728515625" Y="4.037488037109" />
                  <Point X="-26.7508671875" Y="4.043277099609" />
                  <Point X="-26.741095703125" Y="4.046713623047" />
                  <Point X="-26.666861328125" Y="4.077463134766" />
                  <Point X="-26.641921875" Y="4.087793212891" />
                  <Point X="-26.632583984375" Y="4.092271972656" />
                  <Point X="-26.614451171875" Y="4.102219726562" />
                  <Point X="-26.60565625" Y="4.107688964844" />
                  <Point X="-26.58792578125" Y="4.120103515625" />
                  <Point X="-26.57977734375" Y="4.126498046875" />
                  <Point X="-26.56422265625" Y="4.140138671875" />
                  <Point X="-26.550244140625" Y="4.15539453125" />
                  <Point X="-26.53801171875" Y="4.172079589844" />
                  <Point X="-26.5323515625" Y="4.180754882812" />
                  <Point X="-26.52153125" Y="4.199499023438" />
                  <Point X="-26.5168515625" Y="4.208733886719" />
                  <Point X="-26.5085234375" Y="4.227661621094" />
                  <Point X="-26.504875" Y="4.237354492188" />
                  <Point X="-26.480712890625" Y="4.313986816406" />
                  <Point X="-26.472595703125" Y="4.339730957031" />
                  <Point X="-26.470025390625" Y="4.349763671875" />
                  <Point X="-26.465990234375" Y="4.370049316406" />
                  <Point X="-26.464525390625" Y="4.380302246094" />
                  <Point X="-26.462638671875" Y="4.401863769531" />
                  <Point X="-26.46230078125" Y="4.412216796875" />
                  <Point X="-26.462751953125" Y="4.432898925781" />
                  <Point X="-26.463541015625" Y="4.443228027344" />
                  <Point X="-26.47432421875" Y="4.525127441406" />
                  <Point X="-26.479265625" Y="4.562654785156" />
                  <Point X="-26.10578125" Y="4.667366699219" />
                  <Point X="-25.931173828125" Y="4.7163203125" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.22566796875" Y="4.261727050781" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218911621094" />
                  <Point X="-24.767251953125" Y="4.247104980469" />
                  <Point X="-24.76201953125" Y="4.2617265625" />
                  <Point X="-24.713423828125" Y="4.443085449219" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.324591796875" Y="4.753879882812" />
                  <Point X="-24.172123046875" Y="4.737912109375" />
                  <Point X="-23.697935546875" Y="4.623428710938" />
                  <Point X="-23.54640625" Y="4.586844726562" />
                  <Point X="-23.23909765625" Y="4.475381347656" />
                  <Point X="-23.141734375" Y="4.440067382812" />
                  <Point X="-22.843287109375" Y="4.300494140625" />
                  <Point X="-22.749548828125" Y="4.256654296875" />
                  <Point X="-22.461177734375" Y="4.088649658203" />
                  <Point X="-22.3705703125" Y="4.035861572266" />
                  <Point X="-22.18221875" Y="3.901916748047" />
                  <Point X="-22.289734375" Y="3.715693603516" />
                  <Point X="-22.934689453125" Y="2.59859765625" />
                  <Point X="-22.9376171875" Y="2.593119384766" />
                  <Point X="-22.946810546875" Y="2.573454833984" />
                  <Point X="-22.955814453125" Y="2.549571777344" />
                  <Point X="-22.958697265625" Y="2.540600585938" />
                  <Point X="-22.974767578125" Y="2.480503662109" />
                  <Point X="-22.98016796875" Y="2.460314208984" />
                  <Point X="-22.9820859375" Y="2.451470947266" />
                  <Point X="-22.986353515625" Y="2.420224121094" />
                  <Point X="-22.987244140625" Y="2.383236083984" />
                  <Point X="-22.986587890625" Y="2.369573974609" />
                  <Point X="-22.9803203125" Y="2.317607421875" />
                  <Point X="-22.97619921875" Y="2.289038330078" />
                  <Point X="-22.970859375" Y="2.267125488281" />
                  <Point X="-22.967537109375" Y="2.256329345703" />
                  <Point X="-22.959271484375" Y="2.234235839844" />
                  <Point X="-22.954685546875" Y="2.223897949219" />
                  <Point X="-22.944318359375" Y="2.203843505859" />
                  <Point X="-22.938537109375" Y="2.194126953125" />
                  <Point X="-22.906380859375" Y="2.146738525391" />
                  <Point X="-22.895728515625" Y="2.132519775391" />
                  <Point X="-22.871625" Y="2.103299804688" />
                  <Point X="-22.8623046875" Y="2.093511474609" />
                  <Point X="-22.84236328125" Y="2.075352050781" />
                  <Point X="-22.8317421875" Y="2.066980957031" />
                  <Point X="-22.784353515625" Y="2.034825683594" />
                  <Point X="-22.75871875" Y="2.018244628906" />
                  <Point X="-22.738673828125" Y="2.007882080078" />
                  <Point X="-22.72834375" Y="2.003298339844" />
                  <Point X="-22.706255859375" Y="1.995032348633" />
                  <Point X="-22.695451171875" Y="1.991707519531" />
                  <Point X="-22.67352734375" Y="1.986364746094" />
                  <Point X="-22.662408203125" Y="1.984346923828" />
                  <Point X="-22.61044140625" Y="1.978080444336" />
                  <Point X="-22.59170703125" Y="1.976756225586" />
                  <Point X="-22.55534765625" Y="1.975988647461" />
                  <Point X="-22.5419609375" Y="1.976651733398" />
                  <Point X="-22.5154140625" Y="1.979854980469" />
                  <Point X="-22.50225390625" Y="1.982395629883" />
                  <Point X="-22.44215625" Y="1.998466430664" />
                  <Point X="-22.421966796875" Y="2.003865356445" />
                  <Point X="-22.416005859375" Y="2.005670654297" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572875977" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.988677734375" Y="2.244544677734" />
                  <Point X="-21.059595703125" Y="2.780950439453" />
                  <Point X="-21.009798828125" Y="2.711743896484" />
                  <Point X="-20.956037109375" Y="2.637026611328" />
                  <Point X="-20.863115234375" Y="2.483471191406" />
                  <Point X="-20.97790234375" Y="2.395391357422" />
                  <Point X="-21.827046875" Y="1.743819824219" />
                  <Point X="-21.83186328125" Y="1.7398671875" />
                  <Point X="-21.8478828125" Y="1.725213378906" />
                  <Point X="-21.86533203125" Y="1.706600585938" />
                  <Point X="-21.871421875" Y="1.699421264648" />
                  <Point X="-21.914673828125" Y="1.64299597168" />
                  <Point X="-21.929203125" Y="1.624039916992" />
                  <Point X="-21.934357421875" Y="1.616608154297" />
                  <Point X="-21.950255859375" Y="1.589380126953" />
                  <Point X="-21.965234375" Y="1.555553100586" />
                  <Point X="-21.969859375" Y="1.542675537109" />
                  <Point X="-21.985970703125" Y="1.485065185547" />
                  <Point X="-21.991384765625" Y="1.46571105957" />
                  <Point X="-21.993775390625" Y="1.454663818359" />
                  <Point X="-21.997228515625" Y="1.432365722656" />
                  <Point X="-21.998291015625" Y="1.421115112305" />
                  <Point X="-21.999107421875" Y="1.397541259766" />
                  <Point X="-21.998826171875" Y="1.386236694336" />
                  <Point X="-21.996921875" Y="1.363750244141" />
                  <Point X="-21.995298828125" Y="1.352568359375" />
                  <Point X="-21.982072265625" Y="1.288469360352" />
                  <Point X="-21.97762890625" Y="1.266935302734" />
                  <Point X="-21.975400390625" Y="1.258232177734" />
                  <Point X="-21.965314453125" Y="1.228607788086" />
                  <Point X="-21.949712890625" Y="1.195372680664" />
                  <Point X="-21.943080078125" Y="1.18352734375" />
                  <Point X="-21.907107421875" Y="1.128850463867" />
                  <Point X="-21.8950234375" Y="1.110481811523" />
                  <Point X="-21.888263671875" Y="1.101429443359" />
                  <Point X="-21.873712890625" Y="1.084186157227" />
                  <Point X="-21.865921875" Y="1.075995239258" />
                  <Point X="-21.848677734375" Y="1.059904541016" />
                  <Point X="-21.839966796875" Y="1.052696899414" />
                  <Point X="-21.82175390625" Y="1.039369262695" />
                  <Point X="-21.812251953125" Y="1.033249145508" />
                  <Point X="-21.76012109375" Y="1.003905090332" />
                  <Point X="-21.742609375" Y="0.99404675293" />
                  <Point X="-21.734521484375" Y="0.989988464355" />
                  <Point X="-21.70532421875" Y="0.978085266113" />
                  <Point X="-21.669724609375" Y="0.96802142334" />
                  <Point X="-21.656328125" Y="0.965257751465" />
                  <Point X="-21.585845703125" Y="0.955942382812" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.555966796875" Y="0.952199768066" />
                  <Point X="-21.53428125" Y="0.951222839355" />
                  <Point X="-21.50878125" Y="0.952032409668" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-21.1428984375" Y="0.999731018066" />
                  <Point X="-20.295296875" Y="1.111319702148" />
                  <Point X="-20.27040234375" Y="1.009059326172" />
                  <Point X="-20.247310546875" Y="0.914205688477" />
                  <Point X="-20.216126953125" Y="0.713921081543" />
                  <Point X="-20.334869140625" Y="0.682104064941" />
                  <Point X="-21.3080078125" Y="0.421352630615" />
                  <Point X="-21.31396875" Y="0.419544036865" />
                  <Point X="-21.334375" Y="0.412137268066" />
                  <Point X="-21.357619140625" Y="0.40161907959" />
                  <Point X="-21.365994140625" Y="0.397316467285" />
                  <Point X="-21.435240234375" Y="0.35729095459" />
                  <Point X="-21.45850390625" Y="0.343844299316" />
                  <Point X="-21.4661171875" Y="0.338946136475" />
                  <Point X="-21.491228515625" Y="0.319867126465" />
                  <Point X="-21.518005859375" Y="0.294349456787" />
                  <Point X="-21.527203125" Y="0.284226898193" />
                  <Point X="-21.568751953125" Y="0.231285049438" />
                  <Point X="-21.582708984375" Y="0.213499343872" />
                  <Point X="-21.589146484375" Y="0.204207000732" />
                  <Point X="-21.60087109375" Y="0.184925704956" />
                  <Point X="-21.606158203125" Y="0.174936767578" />
                  <Point X="-21.615931640625" Y="0.153473526001" />
                  <Point X="-21.619994140625" Y="0.142926208496" />
                  <Point X="-21.62683984375" Y="0.121425979614" />
                  <Point X="-21.629623046875" Y="0.110472915649" />
                  <Point X="-21.64347265625" Y="0.03815687561" />
                  <Point X="-21.648125" Y="0.013862381935" />
                  <Point X="-21.649396484375" Y="0.004965815067" />
                  <Point X="-21.6514140625" Y="-0.026262531281" />
                  <Point X="-21.64971875" Y="-0.062939689636" />
                  <Point X="-21.648125" Y="-0.076422317505" />
                  <Point X="-21.634275390625" Y="-0.148738510132" />
                  <Point X="-21.629623046875" Y="-0.173032989502" />
                  <Point X="-21.62683984375" Y="-0.183986053467" />
                  <Point X="-21.619994140625" Y="-0.205486434937" />
                  <Point X="-21.615931640625" Y="-0.2160337677" />
                  <Point X="-21.606158203125" Y="-0.237496841431" />
                  <Point X="-21.6008671875" Y="-0.247491882324" />
                  <Point X="-21.589140625" Y="-0.266773162842" />
                  <Point X="-21.582705078125" Y="-0.276059570312" />
                  <Point X="-21.54115625" Y="-0.329001281738" />
                  <Point X="-21.52719921875" Y="-0.346787139893" />
                  <Point X="-21.521283203125" Y="-0.353628112793" />
                  <Point X="-21.498865234375" Y="-0.375800109863" />
                  <Point X="-21.46982421875" Y="-0.398723846436" />
                  <Point X="-21.45850390625" Y="-0.406404083252" />
                  <Point X="-21.3892578125" Y="-0.446429870605" />
                  <Point X="-21.365994140625" Y="-0.459876525879" />
                  <Point X="-21.360501953125" Y="-0.462815093994" />
                  <Point X="-21.340841796875" Y="-0.472016357422" />
                  <Point X="-21.31697265625" Y="-0.48102734375" />
                  <Point X="-21.3080078125" Y="-0.483912872314" />
                  <Point X="-20.98108203125" Y="-0.571512084961" />
                  <Point X="-20.21512109375" Y="-0.776751098633" />
                  <Point X="-20.225494140625" Y="-0.845556945801" />
                  <Point X="-20.238380859375" Y="-0.931038146973" />
                  <Point X="-20.2721953125" Y="-1.079219726563" />
                  <Point X="-20.434650390625" Y="-1.057832275391" />
                  <Point X="-21.563216796875" Y="-0.909253601074" />
                  <Point X="-21.571375" Y="-0.908535644531" />
                  <Point X="-21.59990234375" Y="-0.908042358398" />
                  <Point X="-21.63327734375" Y="-0.910840942383" />
                  <Point X="-21.645517578125" Y="-0.912676269531" />
                  <Point X="-21.781423828125" Y="-0.942216247559" />
                  <Point X="-21.82708203125" Y="-0.952140075684" />
                  <Point X="-21.842123046875" Y="-0.95674206543" />
                  <Point X="-21.871244140625" Y="-0.968366455078" />
                  <Point X="-21.88532421875" Y="-0.975389160156" />
                  <Point X="-21.913150390625" Y="-0.99228137207" />
                  <Point X="-21.925875" Y="-1.001530456543" />
                  <Point X="-21.949625" Y="-1.022001342773" />
                  <Point X="-21.960650390625" Y="-1.033223144531" />
                  <Point X="-22.042796875" Y="-1.132020385742" />
                  <Point X="-22.07039453125" Y="-1.16521105957" />
                  <Point X="-22.07867578125" Y="-1.176850830078" />
                  <Point X="-22.09339453125" Y="-1.201232421875" />
                  <Point X="-22.09983203125" Y="-1.213974243164" />
                  <Point X="-22.11117578125" Y="-1.241360107422" />
                  <Point X="-22.115634765625" Y="-1.254927490234" />
                  <Point X="-22.122466796875" Y="-1.282577758789" />
                  <Point X="-22.12483984375" Y="-1.296660766602" />
                  <Point X="-22.13661328125" Y="-1.424607910156" />
                  <Point X="-22.140568359375" Y="-1.467591430664" />
                  <Point X="-22.140708984375" Y="-1.483315551758" />
                  <Point X="-22.138392578125" Y="-1.514581176758" />
                  <Point X="-22.135935546875" Y="-1.530122802734" />
                  <Point X="-22.128205078125" Y="-1.561743408203" />
                  <Point X="-22.12321484375" Y="-1.576666748047" />
                  <Point X="-22.110841796875" Y="-1.605480834961" />
                  <Point X="-22.103458984375" Y="-1.619371582031" />
                  <Point X="-22.02824609375" Y="-1.736359863281" />
                  <Point X="-22.002978515625" Y="-1.775662109375" />
                  <Point X="-21.9982578125" Y="-1.782351928711" />
                  <Point X="-21.980201171875" Y="-1.804456054688" />
                  <Point X="-21.956505859375" Y="-1.828123657227" />
                  <Point X="-21.947201171875" Y="-1.836277587891" />
                  <Point X="-21.643814453125" Y="-2.069075683594" />
                  <Point X="-20.912828125" Y="-2.629980712891" />
                  <Point X="-20.91817578125" Y="-2.638633789062" />
                  <Point X="-20.95451171875" Y="-2.697427734375" />
                  <Point X="-20.9987265625" Y="-2.760251464844" />
                  <Point X="-21.146404296875" Y="-2.674989013672" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.18497265625" Y="-2.079513427734" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228892578125" Y="-2.066337646484" />
                  <Point X="-22.390642578125" Y="-2.037125610352" />
                  <Point X="-22.444982421875" Y="-2.027311889648" />
                  <Point X="-22.460640625" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403442383" />
                  <Point X="-22.507685546875" Y="-2.02650390625" />
                  <Point X="-22.539859375" Y="-2.031461791992" />
                  <Point X="-22.555154296875" Y="-2.03513659668" />
                  <Point X="-22.5849296875" Y="-2.044960083008" />
                  <Point X="-22.59941015625" Y="-2.051108886719" />
                  <Point X="-22.73378515625" Y="-2.121829345703" />
                  <Point X="-22.778927734375" Y="-2.145587890625" />
                  <Point X="-22.791029296875" Y="-2.153170166016" />
                  <Point X="-22.813958984375" Y="-2.170062255859" />
                  <Point X="-22.824787109375" Y="-2.179372070313" />
                  <Point X="-22.84574609375" Y="-2.200330566406" />
                  <Point X="-22.855056640625" Y="-2.211157958984" />
                  <Point X="-22.871951171875" Y="-2.234089599609" />
                  <Point X="-22.87953515625" Y="-2.246193847656" />
                  <Point X="-22.950255859375" Y="-2.380568847656" />
                  <Point X="-22.974015625" Y="-2.425711914062" />
                  <Point X="-22.9801640625" Y="-2.440192871094" />
                  <Point X="-22.98998828125" Y="-2.469971435547" />
                  <Point X="-22.9936640625" Y="-2.485269042969" />
                  <Point X="-22.99862109375" Y="-2.517443359375" />
                  <Point X="-22.999720703125" Y="-2.533134277344" />
                  <Point X="-22.99931640625" Y="-2.564484375" />
                  <Point X="-22.9978125" Y="-2.580143554688" />
                  <Point X="-22.968599609375" Y="-2.741894042969" />
                  <Point X="-22.95878515625" Y="-2.796233886719" />
                  <Point X="-22.95698046875" Y="-2.804229248047" />
                  <Point X="-22.94876171875" Y="-2.831542236328" />
                  <Point X="-22.935927734375" Y="-2.862479980469" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.7354921875" Y="-3.211254638672" />
                  <Point X="-22.264103515625" Y="-4.02772265625" />
                  <Point X="-22.276244140625" Y="-4.036082763672" />
                  <Point X="-22.399068359375" Y="-3.876015625" />
                  <Point X="-23.16608203125" Y="-2.876422607422" />
                  <Point X="-23.171345703125" Y="-2.870143066406" />
                  <Point X="-23.191169921875" Y="-2.849624023438" />
                  <Point X="-23.216748046875" Y="-2.828003662109" />
                  <Point X="-23.22669921875" Y="-2.820646728516" />
                  <Point X="-23.38623046875" Y="-2.718083740234" />
                  <Point X="-23.43982421875" Y="-2.683627929688" />
                  <Point X="-23.453716796875" Y="-2.676245605469" />
                  <Point X="-23.48252734375" Y="-2.663874511719" />
                  <Point X="-23.4974453125" Y="-2.658885742188" />
                  <Point X="-23.52906640625" Y="-2.651154052734" />
                  <Point X="-23.54460546875" Y="-2.648695800781" />
                  <Point X="-23.575875" Y="-2.646376953125" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.766078125" Y="-2.662571533203" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670339111328" />
                  <Point X="-23.866421875" Y="-2.677170410156" />
                  <Point X="-23.87998828125" Y="-2.681627929688" />
                  <Point X="-23.907373046875" Y="-2.692970703125" />
                  <Point X="-23.92012109375" Y="-2.699412841797" />
                  <Point X="-23.94450390625" Y="-2.714134033203" />
                  <Point X="-23.956138671875" Y="-2.722413085938" />
                  <Point X="-24.090861328125" Y="-2.834431152344" />
                  <Point X="-24.136123046875" Y="-2.872063476562" />
                  <Point X="-24.147345703125" Y="-2.883087402344" />
                  <Point X="-24.16781640625" Y="-2.906836914062" />
                  <Point X="-24.177064453125" Y="-2.9195625" />
                  <Point X="-24.19395703125" Y="-2.947389160156" />
                  <Point X="-24.20098046875" Y="-2.961466552734" />
                  <Point X="-24.21260546875" Y="-2.990587646484" />
                  <Point X="-24.21720703125" Y="-3.005631347656" />
                  <Point X="-24.25748828125" Y="-3.190958740234" />
                  <Point X="-24.271021484375" Y="-3.253219238281" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323170166016" />
                  <Point X="-24.2744453125" Y="-3.335520507812" />
                  <Point X="-24.2191953125" Y="-3.755181396484" />
                  <Point X="-24.166912109375" Y="-4.152315917969" />
                  <Point X="-24.344931640625" Y="-3.487937744141" />
                  <Point X="-24.347390625" Y="-3.480124023438" />
                  <Point X="-24.357853515625" Y="-3.453577392578" />
                  <Point X="-24.3732109375" Y="-3.423814697266" />
                  <Point X="-24.37958984375" Y="-3.413209472656" />
                  <Point X="-24.502146484375" Y="-3.236630371094" />
                  <Point X="-24.543318359375" Y="-3.177309082031" />
                  <Point X="-24.553330078125" Y="-3.165170654297" />
                  <Point X="-24.57521484375" Y="-3.142715820312" />
                  <Point X="-24.587087890625" Y="-3.132399414062" />
                  <Point X="-24.61334375" Y="-3.113155029297" />
                  <Point X="-24.626755859375" Y="-3.104938232422" />
                  <Point X="-24.654755859375" Y="-3.090830078125" />
                  <Point X="-24.66934375" Y="-3.084938720703" />
                  <Point X="-24.858990234375" Y="-3.026079101562" />
                  <Point X="-24.922703125" Y="-3.006305419922" />
                  <Point X="-24.936623046875" Y="-3.003109375" />
                  <Point X="-24.964783203125" Y="-2.99883984375" />
                  <Point X="-24.9790234375" Y="-2.997766357422" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.254630859375" Y="-3.065164550781" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.332931640625" Y="-3.090830078125" />
                  <Point X="-25.360931640625" Y="-3.104938232422" />
                  <Point X="-25.37434375" Y="-3.113154785156" />
                  <Point X="-25.400599609375" Y="-3.132398925781" />
                  <Point X="-25.412474609375" Y="-3.142716552734" />
                  <Point X="-25.434359375" Y="-3.165172119141" />
                  <Point X="-25.444369140625" Y="-3.177310058594" />
                  <Point X="-25.566923828125" Y="-3.353888916016" />
                  <Point X="-25.608095703125" Y="-3.413210449219" />
                  <Point X="-25.61246875" Y="-3.420131835938" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213623047" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.74348828125" Y="-3.863879150391" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.684228610846" Y="-1.195435005857" />
                  <Point X="-29.640997575242" Y="-1.284071764232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.104352961436" Y="-2.384356277481" />
                  <Point X="-28.799658313094" Y="-3.009072885484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.763283127897" Y="-0.816637882745" />
                  <Point X="-29.541677837028" Y="-1.270996061929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.027440297871" Y="-2.325339263946" />
                  <Point X="-28.712454866371" Y="-2.971155104192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.742353401886" Y="-0.642838837283" />
                  <Point X="-29.442358098814" Y="-1.257920359626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.950527704203" Y="-2.266322107103" />
                  <Point X="-28.62998156957" Y="-2.923539078343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.648872992204" Y="-0.617790737259" />
                  <Point X="-29.3430383606" Y="-1.244844657324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.873615114715" Y="-2.207304941689" />
                  <Point X="-28.54750827277" Y="-2.875923052495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.555392563272" Y="-0.592742676703" />
                  <Point X="-29.243718622387" Y="-1.231768955021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.796702525227" Y="-2.148287776274" />
                  <Point X="-28.465034975969" Y="-2.828307026646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.978075247232" Y="-3.826722429169" />
                  <Point X="-27.972630661668" Y="-3.837885483867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.461912125269" Y="-0.567694634746" />
                  <Point X="-29.144398884173" Y="-1.218693252718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.71978993574" Y="-2.08927061086" />
                  <Point X="-28.382561679168" Y="-2.780691000797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.920780073416" Y="-3.727483601041" />
                  <Point X="-27.803368718658" Y="-3.968212552747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.368431687266" Y="-0.542646592789" />
                  <Point X="-29.045079145959" Y="-1.205617550416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.642877346252" Y="-2.030253445446" />
                  <Point X="-28.300088382367" Y="-2.733074974949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.86348480873" Y="-3.628244959225" />
                  <Point X="-27.646065501997" Y="-4.074020599054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.274951249264" Y="-0.517598550832" />
                  <Point X="-28.945759407745" Y="-1.192541848113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.565964756764" Y="-1.971236280031" />
                  <Point X="-28.217615085567" Y="-2.6854589491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.806189487154" Y="-3.52900643405" />
                  <Point X="-27.499548130828" Y="-4.157714384912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.767966510132" Y="0.70994387559" />
                  <Point X="-29.699067512478" Y="0.56867999602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.181470811261" Y="-0.492550508874" />
                  <Point X="-28.846439669532" Y="-1.17946614581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.489052167276" Y="-1.912219114617" />
                  <Point X="-28.135141788766" Y="-2.637842923252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.748894165578" Y="-3.429767908874" />
                  <Point X="-27.434925731585" Y="-4.073498595226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.743363371423" Y="0.876211308888" />
                  <Point X="-29.577480429435" Y="0.536100875677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.087990373258" Y="-0.467502466917" />
                  <Point X="-28.747119931318" Y="-1.166390443508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.412139577788" Y="-1.853201949203" />
                  <Point X="-28.052668491965" Y="-2.590226897403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.691598844002" Y="-3.330529383699" />
                  <Point X="-27.366020719391" Y="-3.998063463324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.712042505915" Y="1.028705361123" />
                  <Point X="-29.455893346391" Y="0.503521755333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.994509935255" Y="-0.44245442496" />
                  <Point X="-28.647800192691" Y="-1.153314742052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.335226988301" Y="-1.794184783788" />
                  <Point X="-27.970195160868" Y="-2.542610941872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.634303522426" Y="-3.231290858524" />
                  <Point X="-27.286837446021" Y="-3.943701889796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.674291570801" Y="1.168015816941" />
                  <Point X="-29.334306263348" Y="0.47094263499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.901029497252" Y="-0.417406383003" />
                  <Point X="-28.548480447467" Y="-1.140239054122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.258314398813" Y="-1.735167618374" />
                  <Point X="-27.887721794623" Y="-2.494995058405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.57700820085" Y="-3.132052333349" />
                  <Point X="-27.207119725322" Y="-3.890436095681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.636540635686" Y="1.307326272759" />
                  <Point X="-29.212719180304" Y="0.438363514647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.807549059249" Y="-0.392358341046" />
                  <Point X="-28.449160702243" Y="-1.127163366192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.181401809325" Y="-1.676150452959" />
                  <Point X="-27.805248428378" Y="-2.447379174938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.519712879274" Y="-3.032813808173" />
                  <Point X="-27.127402004623" Y="-3.837170301565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.529480315871" Y="1.304531430869" />
                  <Point X="-29.091132097261" Y="0.405784394304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.714068621246" Y="-0.367310299089" />
                  <Point X="-28.34984095702" Y="-1.114087678262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.104489219837" Y="-1.617133287545" />
                  <Point X="-27.722775062133" Y="-2.399763291471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.462417557698" Y="-2.933575282998" />
                  <Point X="-27.04376047154" Y="-3.791949515054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.416530471229" Y="1.289661273601" />
                  <Point X="-28.969545014218" Y="0.37320527396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.620588183243" Y="-0.342262257132" />
                  <Point X="-28.250521211796" Y="-1.101011990332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.028971297782" Y="-1.555256630137" />
                  <Point X="-27.636760338868" Y="-2.359408265908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.405122236122" Y="-2.834336757823" />
                  <Point X="-26.945885210459" Y="-3.775912195736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.303580626588" Y="1.274791116334" />
                  <Point X="-28.847957908723" Y="0.340626107584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.52710774524" Y="-0.317214215175" />
                  <Point X="-28.139539735394" Y="-1.111846394635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.968697314712" Y="-1.462125266065" />
                  <Point X="-27.53561774069" Y="-2.350069980393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.347826914546" Y="-2.735098232648" />
                  <Point X="-26.836697608177" Y="-3.783068613039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.190630781946" Y="1.259920959066" />
                  <Point X="-28.726370744066" Y="0.308046819909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.439137638693" Y="-0.280868319465" />
                  <Point X="-27.397432724412" Y="-2.416679907007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.310820194836" Y="-2.594261909128" />
                  <Point X="-26.727509938917" Y="-3.79022516767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.077680937304" Y="1.245050801799" />
                  <Point X="-28.604783579409" Y="0.275467532234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.364048453795" Y="-0.218112620616" />
                  <Point X="-26.61733364033" Y="-3.799408712808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.964731092663" Y="1.230180644531" />
                  <Point X="-28.481669300433" Y="0.239757196201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.30830362989" Y="-0.11569510411" />
                  <Point X="-26.477057504051" Y="-3.870306070794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.123361136721" Y="-4.595491091485" />
                  <Point X="-26.045491606408" Y="-4.755147288626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.851781248021" Y="1.215310487264" />
                  <Point X="-26.292358394841" Y="-4.032284020837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.163257406135" Y="-4.296980273934" />
                  <Point X="-25.967230775514" Y="-4.698894427748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.740844475925" Y="1.204567740369" />
                  <Point X="-25.929752642121" Y="-4.559024645511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.194393887516" Y="2.351193184408" />
                  <Point X="-29.130166086511" Y="2.21950667727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.644667760469" Y="1.224087594306" />
                  <Point X="-25.892274508728" Y="-4.419154863275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.136812104794" Y="2.449844377194" />
                  <Point X="-28.961253214732" Y="2.089895310476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.557072201462" Y="1.261201426275" />
                  <Point X="-25.854796375335" Y="-4.279285081038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.079230320483" Y="2.548495566724" />
                  <Point X="-28.792340342952" Y="1.960283943682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.485716441577" Y="1.331611780773" />
                  <Point X="-25.817318241943" Y="-4.139415298801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.021648357012" Y="2.64714638892" />
                  <Point X="-28.623426954599" Y="1.830671517754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.434583826859" Y="1.443485727494" />
                  <Point X="-25.77984010855" Y="-3.999545516565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.959164378624" Y="2.735746591101" />
                  <Point X="-25.742361971692" Y="-3.859675741434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.894196459942" Y="2.819253960955" />
                  <Point X="-25.704883722974" Y="-3.719806195647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.82922854126" Y="2.90276133081" />
                  <Point X="-25.667405474257" Y="-3.579936649861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.764260622579" Y="2.986268700664" />
                  <Point X="-25.626625953719" Y="-3.446835714372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.665091012215" Y="2.999652210675" />
                  <Point X="-25.566535114179" Y="-3.353328850417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.517963826294" Y="2.914708119286" />
                  <Point X="-25.504460106481" Y="-3.263890134058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.370836640372" Y="2.829764027897" />
                  <Point X="-25.442246335563" Y="-3.174735924465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.225925694836" Y="2.749363902686" />
                  <Point X="-25.368369392113" Y="-3.109494762317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.109399987099" Y="2.727162139576" />
                  <Point X="-25.280393706578" Y="-3.073160305228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.004061182247" Y="2.727896926428" />
                  <Point X="-25.188592789778" Y="-3.044668734497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.916008806978" Y="2.764074146261" />
                  <Point X="-25.096791913997" Y="-3.016177079665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.842298402594" Y="2.829656764095" />
                  <Point X="-25.000074154301" Y="-2.99776653081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.776555642956" Y="2.911575474559" />
                  <Point X="-24.884416853262" Y="-3.018187796332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.056453838234" Y="3.702163162695" />
                  <Point X="-28.03342633609" Y="3.654949786588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.751670729728" Y="3.077265184478" />
                  <Point X="-24.759865860821" Y="-3.056843831498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.9795238521" Y="3.7611446597" />
                  <Point X="-24.632000289845" Y="-3.10229575977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.224261996271" Y="-3.938283149443" />
                  <Point X="-24.184304148698" Y="-4.020208877823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.902593865966" Y="3.820126156704" />
                  <Point X="-24.223382833955" Y="-3.723374356209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.825663879832" Y="3.879107653708" />
                  <Point X="-24.262462536036" Y="-3.426537749799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.748733930189" Y="3.938089225529" />
                  <Point X="-24.261858591796" Y="-3.211064675887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.671804025386" Y="3.997070889286" />
                  <Point X="-24.229275828916" Y="-3.061157896682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.59054053396" Y="4.047167383741" />
                  <Point X="-24.185700717754" Y="-2.933788771388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.507378147109" Y="4.093370565613" />
                  <Point X="-24.117614534478" Y="-2.85667479141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.424215760259" Y="4.139573747486" />
                  <Point X="-24.042413590359" Y="-2.79414823292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.341053373408" Y="4.185776929359" />
                  <Point X="-23.967212956116" Y="-2.731621039091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.257890986558" Y="4.231980111231" />
                  <Point X="-23.88490564085" Y="-2.683664700665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.130452198404" Y="4.18740321742" />
                  <Point X="-23.788490356703" Y="-2.664633985032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.966130670743" Y="4.06720550111" />
                  <Point X="-23.687333300292" Y="-2.655325343286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.841883057268" Y="4.029171485002" />
                  <Point X="-23.585956960051" Y="-2.646466300021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.744207908221" Y="4.045619094791" />
                  <Point X="-23.468919292886" Y="-2.669717735512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.656196607137" Y="4.081880529182" />
                  <Point X="-23.318346412191" Y="-2.761726548132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.574512636056" Y="4.131114912587" />
                  <Point X="-23.140356216713" Y="-2.909949186577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.512024036424" Y="4.219705639814" />
                  <Point X="-22.850277117122" Y="-3.287988135723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.469948622855" Y="4.350149600844" />
                  <Point X="-22.560198017531" Y="-3.66602708487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.46926469971" Y="4.565458693699" />
                  <Point X="-22.999521656599" Y="-2.548568796883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.354209703387" Y="-3.871654373571" />
                  <Point X="-22.274573659486" Y="-4.034932460309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.376282227301" Y="4.591527716426" />
                  <Point X="-22.963495026183" Y="-2.405722992517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.283299754892" Y="4.617596739154" />
                  <Point X="-22.908636389849" Y="-2.30148852223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.190317282483" Y="4.643665761881" />
                  <Point X="-22.849913509986" Y="-2.205176925296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.097334806312" Y="4.669734776895" />
                  <Point X="-22.774433485312" Y="-2.14322256674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.00435229249" Y="4.695803714714" />
                  <Point X="-22.690325990379" Y="-2.098957143601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.909877365814" Y="4.718812752724" />
                  <Point X="-22.606218363774" Y="-2.054691990428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.809887785121" Y="4.730515074417" />
                  <Point X="-22.513809185875" Y="-2.027447539764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.709898204427" Y="4.74221739611" />
                  <Point X="-22.404623156534" Y="-2.034600732061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.609908623734" Y="4.753919717803" />
                  <Point X="-22.288716393453" Y="-2.055533470565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.50991904304" Y="4.765622039496" />
                  <Point X="-22.167651827764" Y="-2.08704127157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.409929462347" Y="4.77732436119" />
                  <Point X="-25.307608164401" Y="4.567534610936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.080920030017" Y="4.102755058168" />
                  <Point X="-22.021658483056" Y="-2.169660643962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.967493991254" Y="4.086908558264" />
                  <Point X="-21.874531398419" Y="-2.254604527685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.876335417777" Y="4.116717127979" />
                  <Point X="-22.139937378084" Y="-1.493730284893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.001202492951" Y="-1.778178952843" />
                  <Point X="-21.727404313783" Y="-2.339548411409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.802769011039" Y="4.182594984738" />
                  <Point X="-22.125715593156" Y="-1.306177922059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.819302215112" Y="-1.934418448175" />
                  <Point X="-21.580277229146" Y="-2.424492295133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.752675331221" Y="4.296599063678" />
                  <Point X="-22.081120382917" Y="-1.180900309821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.650388758584" Y="-2.064031013881" />
                  <Point X="-21.43315014451" Y="-2.509436178856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.715196920723" Y="4.436468277763" />
                  <Point X="-22.015358636391" Y="-1.099020528245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.48147592328" Y="-2.193642305889" />
                  <Point X="-21.286023059873" Y="-2.59438006258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.677718919843" Y="4.576338331691" />
                  <Point X="-21.947936184052" Y="-1.020545698178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.312563113135" Y="-2.323253546316" />
                  <Point X="-21.138895952815" Y="-2.679323992276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.640240939304" Y="4.716208427326" />
                  <Point X="-21.868267951988" Y="-0.967178437324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.143650302989" Y="-2.452864786743" />
                  <Point X="-20.995773969422" Y="-2.756056201534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.565207735989" Y="4.779078905429" />
                  <Point X="-21.775385746478" Y="-0.940903836988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.974737492843" Y="-2.58247602717" />
                  <Point X="-20.934480280288" Y="-2.665015544722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.453821048155" Y="4.76741369457" />
                  <Point X="-21.679819655868" Y="-0.920132016584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.342434360322" Y="4.75574848371" />
                  <Point X="-21.579849874831" Y="-0.908389099579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.231047623575" Y="4.744083172564" />
                  <Point X="-21.467590778618" Y="-0.921843012689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.115696139202" Y="4.724288924129" />
                  <Point X="-22.986615803938" Y="2.409331175287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.813654299944" Y="2.054707539203" />
                  <Point X="-21.354640944029" Y="-0.936713149346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.995891531204" Y="4.695364419219" />
                  <Point X="-22.952982408207" Y="2.557083837921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.674772053145" Y="1.986668078169" />
                  <Point X="-21.634928854589" Y="-0.14532642647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.552221171376" Y="-0.314902307089" />
                  <Point X="-21.24169110944" Y="-0.951583286003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.876086923206" Y="4.666439914309" />
                  <Point X="-22.898285845857" Y="2.661650609121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.56395478538" Y="1.976170351465" />
                  <Point X="-21.636494294625" Y="0.074594544357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.379671061644" Y="-0.45197111683" />
                  <Point X="-21.128741274851" Y="-0.96645342266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.756282315208" Y="4.637515409399" />
                  <Point X="-22.840990516399" Y="2.760889118136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.466019725176" Y="1.992085064411" />
                  <Point X="-21.591847160914" Y="0.199765697702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.250936334824" Y="-0.499205078667" />
                  <Point X="-21.015791440261" Y="-0.981323559317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.63647766975" Y="4.608590827684" />
                  <Point X="-22.783695186941" Y="2.86012762715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.375078005768" Y="2.022338250856" />
                  <Point X="-21.527290287438" Y="0.284115835119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.129349288898" Y="-0.531784122909" />
                  <Point X="-20.902841605672" Y="-0.996193695974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.51453625206" Y="4.575285213655" />
                  <Point X="-22.726399857483" Y="2.959366136165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.292269077262" Y="2.06926612973" />
                  <Point X="-21.993385731804" Y="1.456464458353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.7774021309" Y="1.013632451701" />
                  <Point X="-21.452435417866" Y="0.347351951581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.007762242972" Y="-0.564363167151" />
                  <Point X="-20.789891771083" Y="-1.011063832631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.386121985545" Y="4.528708292811" />
                  <Point X="-22.669104528026" Y="3.05860464518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.209795763516" Y="2.116882120835" />
                  <Point X="-21.951331287471" Y="1.586951412688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.647544844581" Y="0.96409690181" />
                  <Point X="-21.369983018182" Y="0.39501082287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.886175122654" Y="-0.596942363918" />
                  <Point X="-20.676941936494" Y="-1.025933969288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.257707719029" Y="4.482131371968" />
                  <Point X="-22.611809198568" Y="3.157843154195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.12732244977" Y="2.16449811194" />
                  <Point X="-21.889186193139" Y="1.676246430151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.535597480967" Y="0.951282135246" />
                  <Point X="-21.280702104197" Y="0.428669165053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.764587981423" Y="-0.629521603563" />
                  <Point X="-20.563992101904" Y="-1.040804105945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.128468723635" Y="4.433863506336" />
                  <Point X="-22.55451386911" Y="3.25708166321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.044849136024" Y="2.212114103046" />
                  <Point X="-21.819333491842" Y="1.749738511445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.434787758015" Y="0.961302916116" />
                  <Point X="-21.187221639601" Y="0.453717152487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.643000840193" Y="-0.662100843208" />
                  <Point X="-20.451042267315" Y="-1.055674242603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.99153836889" Y="4.369826017081" />
                  <Point X="-22.497218539652" Y="3.356320172225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.96237582214" Y="2.259730093869" />
                  <Point X="-21.742420881705" Y="1.808755634521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.335468037333" Y="0.974378654363" />
                  <Point X="-21.093741175006" Y="0.478765139921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.521413698962" Y="-0.694680082852" />
                  <Point X="-20.338092487964" Y="-1.070544266005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.854608014146" Y="4.305788527826" />
                  <Point X="-22.439923210194" Y="3.455558681239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.879902507961" Y="2.307346084088" />
                  <Point X="-21.665508271567" Y="1.867772757597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.23614831665" Y="0.987454392609" />
                  <Point X="-21.00026071041" Y="0.503813127355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.399826557732" Y="-0.727259322497" />
                  <Point X="-20.258160712226" Y="-1.017717349758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.715180669531" Y="4.236631450648" />
                  <Point X="-22.382627880736" Y="3.554797190254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.797429193783" Y="2.354962074308" />
                  <Point X="-21.58859566143" Y="1.926789880673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.13682859401" Y="1.000530126843" />
                  <Point X="-20.906780245815" Y="0.528861114789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.278239416501" Y="-0.759838562142" />
                  <Point X="-20.228076652756" Y="-0.862687469351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.567527476592" Y="4.150608885051" />
                  <Point X="-22.325332551278" Y="3.654035699269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.714955879605" Y="2.402578064527" />
                  <Point X="-21.511683051292" Y="1.985807003748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.037508841299" Y="1.013605799422" />
                  <Point X="-20.813299781219" Y="0.553909102223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.419874185743" Y="4.06458611871" />
                  <Point X="-22.268037246123" Y="3.753274258112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.632482565427" Y="2.450194054747" />
                  <Point X="-21.434770441154" Y="2.044824126824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.938189088588" Y="1.026681472" />
                  <Point X="-20.719819316624" Y="0.578957089657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.262780475965" Y="3.95920762517" />
                  <Point X="-22.21074198084" Y="3.852512898706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.550009251249" Y="2.497810044967" />
                  <Point X="-21.357857831017" Y="2.1038412499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.838869335876" Y="1.039757144579" />
                  <Point X="-20.626338852028" Y="0.604005077092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.46753593707" Y="2.545426035186" />
                  <Point X="-21.280945220879" Y="2.162858372976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.739549583165" Y="1.052832817158" />
                  <Point X="-20.532858387432" Y="0.629053064526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.385062622892" Y="2.593042025406" />
                  <Point X="-21.204032610741" Y="2.221875496052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.640229830454" Y="1.065908489736" />
                  <Point X="-20.439377922837" Y="0.65410105196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.302589308714" Y="2.640658015626" />
                  <Point X="-21.127120000604" Y="2.280892619128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.540910077743" Y="1.078984162315" />
                  <Point X="-20.345897458241" Y="0.679149039394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.220115994536" Y="2.688274005845" />
                  <Point X="-21.050207390466" Y="2.339909742204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.441590325032" Y="1.092059834893" />
                  <Point X="-20.252417047071" Y="0.704197136365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.137642680358" Y="2.735889996065" />
                  <Point X="-20.973294788658" Y="2.398926882358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.342270572321" Y="1.105135507472" />
                  <Point X="-20.246448996873" Y="0.908672163226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.041987214401" Y="2.756478569853" />
                  <Point X="-20.896382317564" Y="2.457944290516" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.0001640625" Y="0.001625976562" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.228611328125" Y="-4.656159179688" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544433594" />
                  <Point X="-24.658236328125" Y="-3.344965332031" />
                  <Point X="-24.699408203125" Y="-3.285644042969" />
                  <Point X="-24.7256640625" Y="-3.266399658203" />
                  <Point X="-24.915310546875" Y="-3.207540039062" />
                  <Point X="-24.9790234375" Y="-3.187766357422" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.198310546875" Y="-3.246625976562" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285644042969" />
                  <Point X="-25.410833984375" Y="-3.462222900391" />
                  <Point X="-25.452005859375" Y="-3.521544433594" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.5599609375" Y="-3.913054931641" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-26.02634375" Y="-4.952409667969" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.320125" Y="-4.8814921875" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.345244140625" Y="-4.825204101562" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.534756347656" />
                  <Point X="-26.35498828125" Y="-4.306984375" />
                  <Point X="-26.3702109375" Y="-4.23046484375" />
                  <Point X="-26.386283203125" Y="-4.202628417969" />
                  <Point X="-26.560884765625" Y="-4.049505371094" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.880978515625" Y="-3.970573974609" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791259766" />
                  <Point X="-27.182974609375" Y="-4.102813964844" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.316287109375" Y="-4.230997070312" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.74751171875" Y="-4.234680664062" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.16030078125" Y="-3.933180419922" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-28.063265625" Y="-3.594275634766" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597593017578" />
                  <Point X="-27.513978515625" Y="-2.568763916016" />
                  <Point X="-27.531322265625" Y="-2.551420654297" />
                  <Point X="-27.56015234375" Y="-2.53719921875" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-27.91592578125" Y="-2.730671630859" />
                  <Point X="-28.84295703125" Y="-3.265893554688" />
                  <Point X="-29.076123046875" Y="-2.959563720703" />
                  <Point X="-29.161703125" Y="-2.847126464844" />
                  <Point X="-29.379982421875" Y="-2.481106689453" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-29.138763671875" Y="-2.171271240234" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.396013916016" />
                  <Point X="-28.1400546875" Y="-1.373747192383" />
                  <Point X="-28.1381171875" Y="-1.366266601562" />
                  <Point X="-28.140326171875" Y="-1.334594970703" />
                  <Point X="-28.16116015625" Y="-1.310638305664" />
                  <Point X="-28.180982421875" Y="-1.298971313477" />
                  <Point X="-28.187642578125" Y="-1.295051757812" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.628853515625" Y="-1.342459960938" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.893919921875" Y="-1.142229492188" />
                  <Point X="-29.927392578125" Y="-1.011187194824" />
                  <Point X="-29.98514453125" Y="-0.607398620605" />
                  <Point X="-29.998396484375" Y="-0.514742370605" />
                  <Point X="-29.668107421875" Y="-0.426242004395" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.54189453125" Y="-0.121424743652" />
                  <Point X="-28.521125" Y="-0.107009292603" />
                  <Point X="-28.51414453125" Y="-0.102165763855" />
                  <Point X="-28.494896484375" Y="-0.075906845093" />
                  <Point X="-28.48797265625" Y="-0.053595344543" />
                  <Point X="-28.485646484375" Y="-0.046099815369" />
                  <Point X="-28.485646484375" Y="-0.016460264206" />
                  <Point X="-28.4925703125" Y="0.005851236343" />
                  <Point X="-28.494896484375" Y="0.013346765518" />
                  <Point X="-28.514140625" Y="0.039602031708" />
                  <Point X="-28.534916015625" Y="0.05402022171" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-28.93058203125" Y="0.166062667847" />
                  <Point X="-29.998185546875" Y="0.452126037598" />
                  <Point X="-29.93921484375" Y="0.85064440918" />
                  <Point X="-29.91764453125" Y="0.996414794922" />
                  <Point X="-29.801392578125" Y="1.425422851562" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.54940234375" Y="1.498793823242" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.685724609375" Y="1.410363769531" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.651533203125" Y="1.426056762695" />
                  <Point X="-28.6391171875" Y="1.443787841797" />
                  <Point X="-28.62066796875" Y="1.488328735352" />
                  <Point X="-28.614470703125" Y="1.503292114258" />
                  <Point X="-28.61071484375" Y="1.524605712891" />
                  <Point X="-28.616314453125" Y="1.545510498047" />
                  <Point X="-28.63857421875" Y="1.588273925781" />
                  <Point X="-28.64605859375" Y="1.602645874023" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-28.873990234375" Y="1.783447143555" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.243830078125" Y="2.643407958984" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-28.85206640625" Y="3.182829101562" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.64812109375" Y="3.209248046875" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.0744765625" Y="2.914832275391" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927405273438" />
                  <Point X="-27.967796875" Y="2.972858642578" />
                  <Point X="-27.95252734375" Y="2.988128662109" />
                  <Point X="-27.9408984375" Y="3.006381347656" />
                  <Point X="-27.938072265625" Y="3.027838134766" />
                  <Point X="-27.94367578125" Y="3.091874755859" />
                  <Point X="-27.945556640625" Y="3.113387695312" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.04690625" Y="3.298300537109" />
                  <Point X="-28.30727734375" Y="3.749276855469" />
                  <Point X="-27.89885546875" Y="4.062409667969" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.2678671875" Y="4.443791015625" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.117513671875" Y="4.482649414062" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.879974609375" Y="4.23655859375" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.83512109375" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.22225" />
                  <Point X="-26.739572265625" Y="4.252999511719" />
                  <Point X="-26.7146328125" Y="4.263329589844" />
                  <Point X="-26.69690234375" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.29448828125" />
                  <Point X="-26.661919921875" Y="4.371120605469" />
                  <Point X="-26.653802734375" Y="4.396864746094" />
                  <Point X="-26.651916015625" Y="4.418426269531" />
                  <Point X="-26.66269921875" Y="4.500325683594" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.15707421875" Y="4.8503125" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.380115234375" Y="4.972110351562" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.1867890625" Y="4.850743164062" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.896951171875" Y="4.492262207031" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.304802734375" Y="4.942846679688" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.653345703125" Y="4.808122070312" />
                  <Point X="-23.491544921875" Y="4.76905859375" />
                  <Point X="-23.1743125" Y="4.653995605469" />
                  <Point X="-23.0689609375" Y="4.615784179688" />
                  <Point X="-22.762798828125" Y="4.472603027344" />
                  <Point X="-22.6613046875" Y="4.42513671875" />
                  <Point X="-22.365533203125" Y="4.252819824219" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-21.988525390625" Y="3.997318603516" />
                  <Point X="-21.9312578125" Y="3.956593017578" />
                  <Point X="-22.125189453125" Y="3.620693847656" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491518066406" />
                  <Point X="-22.791216796875" Y="2.431421142578" />
                  <Point X="-22.7966171875" Y="2.411231689453" />
                  <Point X="-22.797955078125" Y="2.392324462891" />
                  <Point X="-22.7916875" Y="2.340357910156" />
                  <Point X="-22.789583984375" Y="2.322905517578" />
                  <Point X="-22.781318359375" Y="2.300812011719" />
                  <Point X="-22.749162109375" Y="2.253423583984" />
                  <Point X="-22.72505859375" Y="2.224203613281" />
                  <Point X="-22.677669921875" Y="2.192048339844" />
                  <Point X="-22.66175" Y="2.18124609375" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.5876953125" Y="2.166713867188" />
                  <Point X="-22.5513359375" Y="2.165946289062" />
                  <Point X="-22.49123828125" Y="2.182017089844" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-22.083677734375" Y="2.409089599609" />
                  <Point X="-21.005751953125" Y="3.031430419922" />
                  <Point X="-20.855572265625" Y="2.822715332031" />
                  <Point X="-20.79740234375" Y="2.741874023438" />
                  <Point X="-20.6418984375" Y="2.484898925781" />
                  <Point X="-20.612486328125" Y="2.436295654297" />
                  <Point X="-20.86223828125" Y="2.244654296875" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583831665039" />
                  <Point X="-21.763880859375" Y="1.527406494141" />
                  <Point X="-21.77841015625" Y="1.508450561523" />
                  <Point X="-21.78687890625" Y="1.491503417969" />
                  <Point X="-21.802990234375" Y="1.433893066406" />
                  <Point X="-21.808404296875" Y="1.41453894043" />
                  <Point X="-21.809220703125" Y="1.390965087891" />
                  <Point X="-21.795994140625" Y="1.326866088867" />
                  <Point X="-21.79155078125" Y="1.30533215332" />
                  <Point X="-21.784353515625" Y="1.287956665039" />
                  <Point X="-21.748380859375" Y="1.233279907227" />
                  <Point X="-21.736296875" Y="1.214911254883" />
                  <Point X="-21.719052734375" Y="1.198820556641" />
                  <Point X="-21.666921875" Y="1.169476318359" />
                  <Point X="-21.64941015625" Y="1.159618164062" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.560951171875" Y="1.144304443359" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-21.16769921875" Y="1.18810546875" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.08579296875" Y="1.054000610352" />
                  <Point X="-20.060806640625" Y="0.951367370605" />
                  <Point X="-20.0118046875" Y="0.63663067627" />
                  <Point X="-20.002140625" Y="0.574556396484" />
                  <Point X="-20.285693359375" Y="0.498578338623" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819503784" />
                  <Point X="-21.340158203125" Y="0.192793762207" />
                  <Point X="-21.363421875" Y="0.179347137451" />
                  <Point X="-21.377734375" Y="0.166925323486" />
                  <Point X="-21.419283203125" Y="0.113983383179" />
                  <Point X="-21.433240234375" Y="0.096197647095" />
                  <Point X="-21.443013671875" Y="0.07473449707" />
                  <Point X="-21.45686328125" Y="0.002418428898" />
                  <Point X="-21.461515625" Y="-0.021876060486" />
                  <Point X="-21.461515625" Y="-0.040684017181" />
                  <Point X="-21.447666015625" Y="-0.113000091553" />
                  <Point X="-21.443013671875" Y="-0.137294570923" />
                  <Point X="-21.433240234375" Y="-0.158757720947" />
                  <Point X="-21.39169140625" Y="-0.211699508667" />
                  <Point X="-21.377734375" Y="-0.229485397339" />
                  <Point X="-21.363421875" Y="-0.241907211304" />
                  <Point X="-21.29417578125" Y="-0.281932952881" />
                  <Point X="-21.270912109375" Y="-0.295379577637" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.93190625" Y="-0.38798614502" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.037619140625" Y="-0.873882568359" />
                  <Point X="-20.051568359375" Y="-0.966411682129" />
                  <Point X="-20.114349609375" Y="-1.241528442383" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-20.459451171875" Y="-1.246206787109" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.741068359375" Y="-1.127881103516" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.814552734375" Y="-1.154697021484" />
                  <Point X="-21.89669921875" Y="-1.253494262695" />
                  <Point X="-21.924296875" Y="-1.286684936523" />
                  <Point X="-21.935640625" Y="-1.314070800781" />
                  <Point X="-21.9474140625" Y="-1.442017700195" />
                  <Point X="-21.951369140625" Y="-1.485001220703" />
                  <Point X="-21.943638671875" Y="-1.516621826172" />
                  <Point X="-21.86842578125" Y="-1.633610229492" />
                  <Point X="-21.843158203125" Y="-1.672912353516" />
                  <Point X="-21.831537109375" Y="-1.685540771484" />
                  <Point X="-21.528150390625" Y="-1.918338745117" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.756548828125" Y="-2.738517578125" />
                  <Point X="-20.7958671875" Y="-2.802139648438" />
                  <Point X="-20.925708984375" Y="-2.986627929688" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.241404296875" Y="-2.839533935547" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.424408203125" Y="-2.224100830078" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219245117188" />
                  <Point X="-22.645296875" Y="-2.289965576172" />
                  <Point X="-22.690439453125" Y="-2.313724121094" />
                  <Point X="-22.7113984375" Y="-2.334682617188" />
                  <Point X="-22.782119140625" Y="-2.469057617188" />
                  <Point X="-22.80587890625" Y="-2.514200683594" />
                  <Point X="-22.8108359375" Y="-2.546375" />
                  <Point X="-22.781623046875" Y="-2.708125488281" />
                  <Point X="-22.77180859375" Y="-2.762465332031" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.57094921875" Y="-3.116254638672" />
                  <Point X="-22.01332421875" Y="-4.082087646484" />
                  <Point X="-22.118044921875" Y="-4.156886230469" />
                  <Point X="-22.16470703125" Y="-4.190215820312" />
                  <Point X="-22.309873046875" Y="-4.284179199219" />
                  <Point X="-22.32022265625" Y="-4.29087890625" />
                  <Point X="-22.549806640625" Y="-3.991680419922" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467529297" />
                  <Point X="-23.48898046875" Y="-2.877904541016" />
                  <Point X="-23.54257421875" Y="-2.843448730469" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.74866796875" Y="-2.851772216797" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868508789062" />
                  <Point X="-23.969388671875" Y="-2.980526855469" />
                  <Point X="-24.014650390625" Y="-3.018159179688" />
                  <Point X="-24.03154296875" Y="-3.045985839844" />
                  <Point X="-24.07182421875" Y="-3.231313232422" />
                  <Point X="-24.085357421875" Y="-3.293573730469" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-24.0308203125" Y="-3.730381103516" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.961513671875" Y="-4.953571777344" />
                  <Point X="-24.005658203125" Y="-4.963248535156" />
                  <Point X="-24.139759765625" Y="-4.987609863281" />
                  <Point X="-24.139798828125" Y="-4.987616699219" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#192" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.144649642936" Y="4.894964317856" Z="1.9" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.9" />
                  <Point X="-0.39208560841" Y="5.053051592006" Z="1.9" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.9" />
                  <Point X="-1.176729493102" Y="4.929736200675" Z="1.9" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.9" />
                  <Point X="-1.718428127287" Y="4.525079843394" Z="1.9" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.9" />
                  <Point X="-1.715763108196" Y="4.417436106722" Z="1.9" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.9" />
                  <Point X="-1.764864258123" Y="4.330473894765" Z="1.9" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.9" />
                  <Point X="-1.86304291256" Y="4.312189053701" Z="1.9" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.9" />
                  <Point X="-2.084002446144" Y="4.544367625996" Z="1.9" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.9" />
                  <Point X="-2.298307860506" Y="4.518778454483" Z="1.9" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.9" />
                  <Point X="-2.935435094887" Y="4.133369210309" Z="1.9" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.9" />
                  <Point X="-3.09636466465" Y="3.304580560783" Z="1.9" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.9" />
                  <Point X="-2.99964252701" Y="3.118799942839" Z="1.9" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.9" />
                  <Point X="-3.009310070231" Y="3.039493497655" Z="1.9" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.9" />
                  <Point X="-3.076276522768" Y="2.995922172163" Z="1.9" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.9" />
                  <Point X="-3.629278787514" Y="3.283829284096" Z="1.9" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.9" />
                  <Point X="-3.897686838355" Y="3.244811445277" Z="1.9" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.9" />
                  <Point X="-4.293169386962" Y="2.699893157704" Z="1.9" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.9" />
                  <Point X="-3.910585077811" Y="1.775059564256" Z="1.9" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.9" />
                  <Point X="-3.689083653249" Y="1.596467791918" Z="1.9" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.9" />
                  <Point X="-3.67302049668" Y="1.538740931764" Z="1.9" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.9" />
                  <Point X="-3.706916652312" Y="1.489329775381" Z="1.9" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.9" />
                  <Point X="-4.549034050578" Y="1.579646119964" Z="1.9" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.9" />
                  <Point X="-4.855809191265" Y="1.469780040178" Z="1.9" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.9" />
                  <Point X="-4.994833330217" Y="0.88924335517" Z="1.9" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.9" />
                  <Point X="-3.949681215054" Y="0.149046364732" Z="1.9" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.9" />
                  <Point X="-3.569581761736" Y="0.044225225595" Z="1.9" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.9" />
                  <Point X="-3.546481494795" Y="0.022311442006" Z="1.9" />
                  <Point X="-3.539556741714" Y="0" Z="1.9" />
                  <Point X="-3.54188310634" Y="-0.007495509055" Z="1.9" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.9" />
                  <Point X="-3.555786833388" Y="-0.034650757539" Z="1.9" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.9" />
                  <Point X="-4.687206573719" Y="-0.346665692024" Z="1.9" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.9" />
                  <Point X="-5.040796669856" Y="-0.583197404128" Z="1.9" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.9" />
                  <Point X="-4.94852276388" Y="-1.123323569468" Z="1.9" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.9" />
                  <Point X="-3.628485327756" Y="-1.360752145756" Z="1.9" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.9" />
                  <Point X="-3.212499352862" Y="-1.310782820458" Z="1.9" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.9" />
                  <Point X="-3.194613743864" Y="-1.32993032887" Z="1.9" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.9" />
                  <Point X="-4.175357910045" Y="-2.100323386223" Z="1.9" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.9" />
                  <Point X="-4.429083099941" Y="-2.475436402093" Z="1.9" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.9" />
                  <Point X="-4.122066141794" Y="-2.958566518524" Z="1.9" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.9" />
                  <Point X="-2.897082818471" Y="-2.742692936069" Z="1.9" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.9" />
                  <Point X="-2.568476890356" Y="-2.559853578775" Z="1.9" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.9" />
                  <Point X="-3.1127240771" Y="-3.537995564021" Z="1.9" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.9" />
                  <Point X="-3.196962134688" Y="-3.941517663618" Z="1.9" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.9" />
                  <Point X="-2.779990878498" Y="-4.24591149656" Z="1.9" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.9" />
                  <Point X="-2.282776403379" Y="-4.230154926035" Z="1.9" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.9" />
                  <Point X="-2.161351885092" Y="-4.113107105281" Z="1.9" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.9" />
                  <Point X="-1.890404080408" Y="-3.989187057482" Z="1.9" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.9" />
                  <Point X="-1.600009089756" Y="-4.055817479026" Z="1.9" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.9" />
                  <Point X="-1.410185234075" Y="-4.285460287449" Z="1.9" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.9" />
                  <Point X="-1.400973110234" Y="-4.787397629058" Z="1.9" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.9" />
                  <Point X="-1.338740518269" Y="-4.898635054087" Z="1.9" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.9" />
                  <Point X="-1.042028241973" Y="-4.970213853008" Z="1.9" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.9" />
                  <Point X="-0.517820355852" Y="-3.894715928631" Z="1.9" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.9" />
                  <Point X="-0.37591442196" Y="-3.459451560382" Z="1.9" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.9" />
                  <Point X="-0.189647253562" Y="-3.263098877307" Z="1.9" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.9" />
                  <Point X="0.063711825799" Y="-3.224013167981" Z="1.9" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.9" />
                  <Point X="0.294531437472" Y="-3.342194195386" Z="1.9" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.9" />
                  <Point X="0.716934504549" Y="-4.637820101185" Z="1.9" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.9" />
                  <Point X="0.863018430908" Y="-5.005524447317" Z="1.9" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.9" />
                  <Point X="1.043033785648" Y="-4.971132223944" Z="1.9" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.9" />
                  <Point X="1.012595219865" Y="-3.692574776198" Z="1.9" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.9" />
                  <Point X="0.970878367605" Y="-3.210653193475" Z="1.9" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.9" />
                  <Point X="1.056420254633" Y="-2.987693609357" Z="1.9" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.9" />
                  <Point X="1.249757713572" Y="-2.870281729696" Z="1.9" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.9" />
                  <Point X="1.477824286755" Y="-2.888682470279" Z="1.9" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.9" />
                  <Point X="2.404368723598" Y="-3.990838249063" Z="1.9" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.9" />
                  <Point X="2.711140087136" Y="-4.294873397231" Z="1.9" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.9" />
                  <Point X="2.90486123105" Y="-4.166293252169" Z="1.9" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.9" />
                  <Point X="2.466194310045" Y="-3.059974626358" Z="1.9" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.9" />
                  <Point X="2.261423141552" Y="-2.667958684708" Z="1.9" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.9" />
                  <Point X="2.255969625825" Y="-2.461065064434" Z="1.9" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.9" />
                  <Point X="2.371833313939" Y="-2.302931684142" Z="1.9" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.9" />
                  <Point X="2.560548118804" Y="-2.242024867939" Z="1.9" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.9" />
                  <Point X="3.727438970749" Y="-2.85155519023" Z="1.9" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.9" />
                  <Point X="4.109023203717" Y="-2.984125058715" Z="1.9" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.9" />
                  <Point X="4.279828029799" Y="-2.7335225226" Z="1.9" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.9" />
                  <Point X="3.496130940945" Y="-1.847390949735" Z="1.9" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.9" />
                  <Point X="3.167475190423" Y="-1.575290884728" Z="1.9" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.9" />
                  <Point X="3.096217841959" Y="-1.41531892142" Z="1.9" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.9" />
                  <Point X="3.135588633344" Y="-1.254181395859" Z="1.9" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.9" />
                  <Point X="3.263393237605" Y="-1.145460291072" Z="1.9" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.9" />
                  <Point X="4.527865963876" Y="-1.264498959513" Z="1.9" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.9" />
                  <Point X="4.928238639893" Y="-1.221372705728" Z="1.9" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.9" />
                  <Point X="5.005665653543" Y="-0.85005634132" Z="1.9" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.9" />
                  <Point X="4.074877436331" Y="-0.308409852032" Z="1.9" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.9" />
                  <Point X="3.724689353181" Y="-0.207364005627" Z="1.9" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.9" />
                  <Point X="3.641484706803" Y="-0.149552425249" Z="1.9" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.9" />
                  <Point X="3.595284054413" Y="-0.072316026558" Z="1.9" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.9" />
                  <Point X="3.586087396011" Y="0.024294504634" Z="1.9" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.9" />
                  <Point X="3.613894731597" Y="0.114396313326" Z="1.9" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.9" />
                  <Point X="3.678706061172" Y="0.180784838615" Z="1.9" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.9" />
                  <Point X="4.721090380975" Y="0.481562080737" Z="1.9" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.9" />
                  <Point X="5.031442782416" Y="0.67560269979" Z="1.9" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.9" />
                  <Point X="4.956632231125" Y="1.097107444214" Z="1.9" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.9" />
                  <Point X="3.819619256262" Y="1.268957928878" Z="1.9" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.9" />
                  <Point X="3.439442940941" Y="1.225153481828" Z="1.9" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.9" />
                  <Point X="3.351447833367" Y="1.244326760733" Z="1.9" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.9" />
                  <Point X="3.287233590809" Y="1.292039640836" Z="1.9" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.9" />
                  <Point X="3.246817808503" Y="1.368250355369" Z="1.9" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.9" />
                  <Point X="3.239004629274" Y="1.451703351212" Z="1.9" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.9" />
                  <Point X="3.269646381859" Y="1.528269749167" Z="1.9" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.9" />
                  <Point X="4.162042570142" Y="2.236266501699" Z="1.9" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.9" />
                  <Point X="4.394722799659" Y="2.542065339063" Z="1.9" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.9" />
                  <Point X="4.178856820936" Y="2.883198680372" Z="1.9" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.9" />
                  <Point X="2.885165284464" Y="2.483671016303" Z="1.9" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.9" />
                  <Point X="2.489688454252" Y="2.26159984165" Z="1.9" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.9" />
                  <Point X="2.412133488912" Y="2.247634306363" Z="1.9" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.9" />
                  <Point X="2.344246640759" Y="2.264702996876" Z="1.9" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.9" />
                  <Point X="2.286055603359" Y="2.312778219623" Z="1.9" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.9" />
                  <Point X="2.251795396396" Y="2.37762495492" Z="1.9" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.9" />
                  <Point X="2.250928101007" Y="2.44978112163" Z="1.9" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.9" />
                  <Point X="2.911954336556" Y="3.626973797491" Z="1.9" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.9" />
                  <Point X="3.034293524672" Y="4.069345622758" Z="1.9" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.9" />
                  <Point X="2.653479988138" Y="4.327302632986" Z="1.9" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.9" />
                  <Point X="2.252225225044" Y="4.54917442629" Z="1.9" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.9" />
                  <Point X="1.836580346373" Y="4.732279678552" Z="1.9" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.9" />
                  <Point X="1.352232193981" Y="4.888005684419" Z="1.9" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.9" />
                  <Point X="0.694247254683" Y="5.023854669775" Z="1.9" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.9" />
                  <Point X="0.048594918617" Y="4.536483188094" Z="1.9" />
                  <Point X="0" Y="4.355124473572" Z="1.9" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>