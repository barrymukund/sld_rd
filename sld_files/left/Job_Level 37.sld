<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#189" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2682" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.152619140625" Y="-4.572713378906" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140625" />
                  <Point X="-24.457634765625" Y="-3.467376953125" />
                  <Point X="-24.573744140625" Y="-3.300085449219" />
                  <Point X="-24.62136328125" Y="-3.2314765625" />
                  <Point X="-24.643248046875" Y="-3.209020263672" />
                  <Point X="-24.66950390625" Y="-3.189776611328" />
                  <Point X="-24.69750390625" Y="-3.175669433594" />
                  <Point X="-24.87717578125" Y="-3.119905517578" />
                  <Point X="-24.95086328125" Y="-3.097036132812" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.21649609375" Y="-3.152799560547" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.189777587891" />
                  <Point X="-25.344439453125" Y="-3.209021972656" />
                  <Point X="-25.36632421875" Y="-3.231477294922" />
                  <Point X="-25.48243359375" Y="-3.398768554688" />
                  <Point X="-25.53005078125" Y="-3.467377685547" />
                  <Point X="-25.5381875" Y="-3.481571533203" />
                  <Point X="-25.550990234375" Y="-3.512524658203" />
                  <Point X="-25.667494140625" Y="-3.947325195312" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-25.99644140625" Y="-4.861440917969" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.24641796875" Y="-4.802362304688" />
                  <Point X="-26.2149609375" Y="-4.563437011719" />
                  <Point X="-26.21419921875" Y="-4.547929199219" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.259431640625" Y="-4.300431152344" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.182963867188" />
                  <Point X="-26.30401171875" Y="-4.155126953125" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.4890625" Y="-3.986134765625" />
                  <Point X="-26.556904296875" Y="-3.926639648438" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.862576171875" Y="-3.876576171875" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708740234" />
                  <Point X="-27.014466796875" Y="-3.882028564453" />
                  <Point X="-27.042658203125" Y="-3.894801757812" />
                  <Point X="-27.22559765625" Y="-4.017038330078" />
                  <Point X="-27.300623046875" Y="-4.067169677734" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.335099609375" Y="-4.0994609375" />
                  <Point X="-27.400509765625" Y="-4.184702636719" />
                  <Point X="-27.480146484375" Y="-4.288489257813" />
                  <Point X="-27.680203125" Y="-4.164620605469" />
                  <Point X="-27.80171484375" Y="-4.0893828125" />
                  <Point X="-28.086224609375" Y="-3.870320068359" />
                  <Point X="-28.10472265625" Y="-3.856077636719" />
                  <Point X="-27.951685546875" Y="-3.591011962891" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647655761719" />
                  <Point X="-27.406587890625" Y="-2.616131347656" />
                  <Point X="-27.40557421875" Y="-2.585198730469" />
                  <Point X="-27.414556640625" Y="-2.55558203125" />
                  <Point X="-27.428771484375" Y="-2.526753662109" />
                  <Point X="-27.446798828125" Y="-2.501593505859" />
                  <Point X="-27.464146484375" Y="-2.484244873047" />
                  <Point X="-27.489302734375" Y="-2.466217529297" />
                  <Point X="-27.5181328125" Y="-2.451998291016" />
                  <Point X="-27.54775" Y="-2.443012207031" />
                  <Point X="-27.5786875" Y="-2.444024169922" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.014189453125" Y="-2.677707763672" />
                  <Point X="-28.818021484375" Y="-3.141800537109" />
                  <Point X="-28.98686328125" Y="-2.919979248047" />
                  <Point X="-29.082865234375" Y="-2.793849365234" />
                  <Point X="-29.286833984375" Y="-2.451827392578" />
                  <Point X="-29.306142578125" Y="-2.419449951172" />
                  <Point X="-29.029650390625" Y="-2.207291015625" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.084580078125" Y="-1.475596191406" />
                  <Point X="-28.066615234375" Y="-1.448467163086" />
                  <Point X="-28.053857421875" Y="-1.419839111328" />
                  <Point X="-28.048392578125" Y="-1.398743408203" />
                  <Point X="-28.046150390625" Y="-1.390086914062" />
                  <Point X="-28.043345703125" Y="-1.359652709961" />
                  <Point X="-28.0455546875" Y="-1.327984985352" />
                  <Point X="-28.0525546875" Y="-1.298239624023" />
                  <Point X="-28.068638671875" Y="-1.272255981445" />
                  <Point X="-28.089470703125" Y="-1.248301025391" />
                  <Point X="-28.112970703125" Y="-1.228767578125" />
                  <Point X="-28.131751953125" Y="-1.217714233398" />
                  <Point X="-28.139453125" Y="-1.213180908203" />
                  <Point X="-28.1687109375" Y="-1.201958862305" />
                  <Point X="-28.200599609375" Y="-1.195475585938" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-28.705337890625" Y="-1.256709594727" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.79653125" Y="-1.139643188477" />
                  <Point X="-29.834078125" Y="-0.992650634766" />
                  <Point X="-29.88804296875" Y="-0.615327331543" />
                  <Point X="-29.892423828125" Y="-0.584698547363" />
                  <Point X="-29.585103515625" Y="-0.502352478027" />
                  <Point X="-28.532875" Y="-0.220408432007" />
                  <Point X="-28.517494140625" Y="-0.214828155518" />
                  <Point X="-28.48773046875" Y="-0.199470916748" />
                  <Point X="-28.468048828125" Y="-0.185811157227" />
                  <Point X="-28.4599765625" Y="-0.180208877563" />
                  <Point X="-28.43751953125" Y="-0.158323898315" />
                  <Point X="-28.418275390625" Y="-0.132067810059" />
                  <Point X="-28.40416796875" Y="-0.1040677948" />
                  <Point X="-28.397607421875" Y="-0.082929924011" />
                  <Point X="-28.394916015625" Y="-0.074255256653" />
                  <Point X="-28.390646484375" Y="-0.046089191437" />
                  <Point X="-28.3906484375" Y="-0.016452730179" />
                  <Point X="-28.39491796875" Y="0.011700131416" />
                  <Point X="-28.40147265625" Y="0.032821990967" />
                  <Point X="-28.404162109375" Y="0.041491268158" />
                  <Point X="-28.418271484375" Y="0.069497955322" />
                  <Point X="-28.437517578125" Y="0.095759658813" />
                  <Point X="-28.4599765625" Y="0.117648887634" />
                  <Point X="-28.479658203125" Y="0.131308807373" />
                  <Point X="-28.48853125" Y="0.136771286011" />
                  <Point X="-28.512169921875" Y="0.149592712402" />
                  <Point X="-28.532875" Y="0.157848297119" />
                  <Point X="-28.96441015625" Y="0.273478088379" />
                  <Point X="-29.89181640625" Y="0.521975463867" />
                  <Point X="-29.84868359375" Y="0.813459106445" />
                  <Point X="-29.82448828125" Y="0.976968261719" />
                  <Point X="-29.715853515625" Y="1.377862426758" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-29.5199296875" Y="1.39909375" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228149414" />
                  <Point X="-28.70313671875" Y="1.305263427734" />
                  <Point X="-28.659576171875" Y="1.318998046875" />
                  <Point X="-28.6417109375" Y="1.324630981445" />
                  <Point X="-28.622775390625" Y="1.332962524414" />
                  <Point X="-28.60403125" Y="1.343784790039" />
                  <Point X="-28.587353515625" Y="1.356014526367" />
                  <Point X="-28.573716796875" Y="1.371563842773" />
                  <Point X="-28.56130078125" Y="1.389294311523" />
                  <Point X="-28.55134765625" Y="1.407432983398" />
                  <Point X="-28.533869140625" Y="1.449631103516" />
                  <Point X="-28.526701171875" Y="1.466937011719" />
                  <Point X="-28.520912109375" Y="1.486805908203" />
                  <Point X="-28.51715625" Y="1.508121948242" />
                  <Point X="-28.515806640625" Y="1.528755737305" />
                  <Point X="-28.518951171875" Y="1.549193115234" />
                  <Point X="-28.524552734375" Y="1.570099975586" />
                  <Point X="-28.53205078125" Y="1.58937890625" />
                  <Point X="-28.553140625" Y="1.629893066406" />
                  <Point X="-28.561791015625" Y="1.646508666992" />
                  <Point X="-28.57328515625" Y="1.663711669922" />
                  <Point X="-28.587197265625" Y="1.680289916992" />
                  <Point X="-28.60213671875" Y="1.694590087891" />
                  <Point X="-28.849666015625" Y="1.884526855469" />
                  <Point X="-29.351859375" Y="2.269873291016" />
                  <Point X="-29.17516796875" Y="2.572586669922" />
                  <Point X="-29.0811484375" Y="2.733665039062" />
                  <Point X="-28.793390625" Y="3.103537841797" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.669904296875" Y="3.112127441406" />
                  <Point X="-28.20665625" Y="2.844671142578" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.086125" Y="2.820488525391" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.99901171875" Y="2.826505126953" />
                  <Point X="-27.980458984375" Y="2.835655029297" />
                  <Point X="-27.962205078125" Y="2.847284912109" />
                  <Point X="-27.946078125" Y="2.860228759766" />
                  <Point X="-27.903013671875" Y="2.903291503906" />
                  <Point X="-27.885353515625" Y="2.920952148438" />
                  <Point X="-27.87240625" Y="2.937083984375" />
                  <Point X="-27.86077734375" Y="2.955337158203" />
                  <Point X="-27.85162890625" Y="2.97388671875" />
                  <Point X="-27.8467109375" Y="2.993976318359" />
                  <Point X="-27.843884765625" Y="3.01543359375" />
                  <Point X="-27.84343359375" Y="3.036120117188" />
                  <Point X="-27.8487421875" Y="3.096788574219" />
                  <Point X="-27.85091796875" Y="3.121669677734" />
                  <Point X="-27.854955078125" Y="3.141958251953" />
                  <Point X="-27.86146484375" Y="3.162602539062" />
                  <Point X="-27.869794921875" Y="3.181533447266" />
                  <Point X="-27.979484375" Y="3.371518798828" />
                  <Point X="-28.18333203125" Y="3.724595703125" />
                  <Point X="-27.864365234375" Y="3.969144287109" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.24741015625" Y="4.34648046875" />
                  <Point X="-27.16703515625" Y="4.391134277344" />
                  <Point X="-27.0431953125" Y="4.2297421875" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.99511328125" Y="4.189395019531" />
                  <Point X="-26.92758984375" Y="4.154244140625" />
                  <Point X="-26.899896484375" Y="4.139828125" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.83926953125" Y="4.123583007812" />
                  <Point X="-26.818630859375" Y="4.124935058594" />
                  <Point X="-26.797314453125" Y="4.128692871094" />
                  <Point X="-26.777451171875" Y="4.134481933594" />
                  <Point X="-26.70712109375" Y="4.163614257812" />
                  <Point X="-26.67827734375" Y="4.175561523437" />
                  <Point X="-26.660140625" Y="4.185512207031" />
                  <Point X="-26.642412109375" Y="4.197926757812" />
                  <Point X="-26.62686328125" Y="4.211563476563" />
                  <Point X="-26.6146328125" Y="4.2282421875" />
                  <Point X="-26.603810546875" Y="4.246985839844" />
                  <Point X="-26.595478515625" Y="4.265921386719" />
                  <Point X="-26.572587890625" Y="4.338522949219" />
                  <Point X="-26.56319921875" Y="4.368297851562" />
                  <Point X="-26.5591640625" Y="4.388583496094" />
                  <Point X="-26.55727734375" Y="4.410145996094" />
                  <Point X="-26.557728515625" Y="4.430827148438" />
                  <Point X="-26.57019921875" Y="4.525548828125" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.161607421875" Y="4.750378417969" />
                  <Point X="-25.949630859375" Y="4.80980859375" />
                  <Point X="-25.400203125" Y="4.874110839844" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.2709453125" Y="4.79776171875" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.797580078125" Y="4.496067871094" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.341048828125" Y="4.851123046875" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.70139453125" Y="4.721993652344" />
                  <Point X="-23.5189765625" Y="4.677952148438" />
                  <Point X="-23.22352734375" Y="4.570790039062" />
                  <Point X="-23.105349609375" Y="4.527926269531" />
                  <Point X="-22.81925390625" Y="4.39412890625" />
                  <Point X="-22.705427734375" Y="4.340895996094" />
                  <Point X="-22.429013671875" Y="4.179857421875" />
                  <Point X="-22.319017578125" Y="4.1157734375" />
                  <Point X="-22.0583515625" Y="3.930401611328" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.2413828125" Y="3.609438476562" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857921875" Y="2.53993359375" />
                  <Point X="-22.866921875" Y="2.516058349609" />
                  <Point X="-22.8821484375" Y="2.459122314453" />
                  <Point X="-22.888392578125" Y="2.435771972656" />
                  <Point X="-22.891380859375" Y="2.417936523438" />
                  <Point X="-22.892271484375" Y="2.380954101562" />
                  <Point X="-22.886333984375" Y="2.331720703125" />
                  <Point X="-22.883900390625" Y="2.311529296875" />
                  <Point X="-22.87855859375" Y="2.289606933594" />
                  <Point X="-22.870291015625" Y="2.267514892578" />
                  <Point X="-22.859927734375" Y="2.247469482422" />
                  <Point X="-22.829462890625" Y="2.202573486328" />
                  <Point X="-22.81696875" Y="2.184160888672" />
                  <Point X="-22.80553125" Y="2.170327392578" />
                  <Point X="-22.7783984375" Y="2.145592285156" />
                  <Point X="-22.73350390625" Y="2.115128417969" />
                  <Point X="-22.71508984375" Y="2.102634765625" />
                  <Point X="-22.69504296875" Y="2.092270996094" />
                  <Point X="-22.67295703125" Y="2.084006103516" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.601802734375" Y="2.072726806641" />
                  <Point X="-22.581611328125" Y="2.070291992188" />
                  <Point X="-22.563533203125" Y="2.069845458984" />
                  <Point X="-22.52679296875" Y="2.074170898438" />
                  <Point X="-22.469857421875" Y="2.089396484375" />
                  <Point X="-22.446505859375" Y="2.095640625" />
                  <Point X="-22.434716796875" Y="2.099637939453" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.977421875" Y="2.360739501953" />
                  <Point X="-21.032673828125" Y="2.906190185547" />
                  <Point X="-20.941974609375" Y="2.780138183594" />
                  <Point X="-20.876720703125" Y="2.689451904297" />
                  <Point X="-20.73780078125" Y="2.459883789063" />
                  <Point X="-20.964732421875" Y="2.285752441406" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.77857421875" Y="1.660242553711" />
                  <Point X="-21.79602734375" Y="1.641626831055" />
                  <Point X="-21.83700390625" Y="1.588169311523" />
                  <Point X="-21.85380859375" Y="1.566245483398" />
                  <Point X="-21.863390625" Y="1.550916137695" />
                  <Point X="-21.878369140625" Y="1.517089111328" />
                  <Point X="-21.8936328125" Y="1.462508789062" />
                  <Point X="-21.89989453125" Y="1.440124633789" />
                  <Point X="-21.90334765625" Y="1.417827148438" />
                  <Point X="-21.9041640625" Y="1.39425378418" />
                  <Point X="-21.902259765625" Y="1.371766845703" />
                  <Point X="-21.889728515625" Y="1.311039306641" />
                  <Point X="-21.88458984375" Y="1.286133911133" />
                  <Point X="-21.8793203125" Y="1.26898046875" />
                  <Point X="-21.86371875" Y="1.235741577148" />
                  <Point X="-21.829638671875" Y="1.183940429688" />
                  <Point X="-21.815662109375" Y="1.162695922852" />
                  <Point X="-21.801107421875" Y="1.145450195312" />
                  <Point X="-21.78386328125" Y="1.129360473633" />
                  <Point X="-21.76565234375" Y="1.116034790039" />
                  <Point X="-21.716263671875" Y="1.088233764648" />
                  <Point X="-21.696009765625" Y="1.076832397461" />
                  <Point X="-21.679478515625" Y="1.069501586914" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.57710546875" Y="1.05061340332" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-21.099484375" Y="1.101266357422" />
                  <Point X="-20.22316015625" Y="1.21663659668" />
                  <Point X="-20.1820859375" Y="1.047916870117" />
                  <Point X="-20.154060546875" Y="0.932788513184" />
                  <Point X="-20.1091328125" Y="0.644239074707" />
                  <Point X="-20.36146484375" Y="0.576626647949" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.295208984375" Y="0.325586517334" />
                  <Point X="-21.318453125" Y="0.315068023682" />
                  <Point X="-21.384056640625" Y="0.27714730835" />
                  <Point X="-21.410962890625" Y="0.26159564209" />
                  <Point X="-21.425689453125" Y="0.251094772339" />
                  <Point X="-21.45246875" Y="0.225576385498" />
                  <Point X="-21.49183203125" Y="0.175419082642" />
                  <Point X="-21.507974609375" Y="0.154848754883" />
                  <Point X="-21.51969921875" Y="0.135567443848" />
                  <Point X="-21.52947265625" Y="0.114104133606" />
                  <Point X="-21.536318359375" Y="0.092603782654" />
                  <Point X="-21.549439453125" Y="0.024091234207" />
                  <Point X="-21.5548203125" Y="-0.004006679058" />
                  <Point X="-21.556515625" Y="-0.021875743866" />
                  <Point X="-21.5548203125" Y="-0.058553455353" />
                  <Point X="-21.54169921875" Y="-0.127065856934" />
                  <Point X="-21.536318359375" Y="-0.155163925171" />
                  <Point X="-21.52947265625" Y="-0.176664260864" />
                  <Point X="-21.51969921875" Y="-0.198127578735" />
                  <Point X="-21.507974609375" Y="-0.217408737183" />
                  <Point X="-21.468611328125" Y="-0.267566040039" />
                  <Point X="-21.45246875" Y="-0.288136535645" />
                  <Point X="-21.44000390625" Y="-0.301231323242" />
                  <Point X="-21.41096484375" Y="-0.324155303955" />
                  <Point X="-21.345359375" Y="-0.362075897217" />
                  <Point X="-21.318455078125" Y="-0.377627868652" />
                  <Point X="-21.307291015625" Y="-0.383138763428" />
                  <Point X="-21.283419921875" Y="-0.392149963379" />
                  <Point X="-20.905310546875" Y="-0.493463806152" />
                  <Point X="-20.108525390625" Y="-0.706961914062" />
                  <Point X="-20.129328125" Y="-0.844945922852" />
                  <Point X="-20.144974609375" Y="-0.94872454834" />
                  <Point X="-20.19882421875" Y="-1.18469921875" />
                  <Point X="-20.50641015625" Y="-1.144204833984" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710266113" />
                  <Point X="-21.62533984375" Y="-1.005508666992" />
                  <Point X="-21.75409765625" Y="-1.033494995117" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.836025390625" Y="-1.056597167969" />
                  <Point X="-21.8638515625" Y="-1.073489379883" />
                  <Point X="-21.8876015625" Y="-1.093960083008" />
                  <Point X="-21.965427734375" Y="-1.187560913086" />
                  <Point X="-21.997345703125" Y="-1.225947998047" />
                  <Point X="-22.012064453125" Y="-1.250329589844" />
                  <Point X="-22.023408203125" Y="-1.277715209961" />
                  <Point X="-22.030240234375" Y="-1.305365722656" />
                  <Point X="-22.04139453125" Y="-1.426583007813" />
                  <Point X="-22.04596875" Y="-1.476296142578" />
                  <Point X="-22.043650390625" Y="-1.507566772461" />
                  <Point X="-22.03591796875" Y="-1.539188110352" />
                  <Point X="-22.023546875" Y="-1.567996704102" />
                  <Point X="-21.952291015625" Y="-1.67883203125" />
                  <Point X="-21.92306640625" Y="-1.724287353516" />
                  <Point X="-21.913064453125" Y="-1.737238647461" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.538482421875" Y="-2.030154174805" />
                  <Point X="-20.786876953125" Y="-2.606882324219" />
                  <Point X="-20.831083984375" Y="-2.678415771484" />
                  <Point X="-20.87519921875" Y="-2.749798583984" />
                  <Point X="-20.971017578125" Y="-2.8859453125" />
                  <Point X="-21.246771484375" Y="-2.726738769531" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.2457734375" Y="-2.159825195312" />
                  <Point X="-22.399015625" Y="-2.132149658203" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135177001953" />
                  <Point X="-22.68247265625" Y="-2.202177978516" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.757615234375" Y="-2.246549560547" />
                  <Point X="-22.77857421875" Y="-2.267509033203" />
                  <Point X="-22.795466796875" Y="-2.290438476562" />
                  <Point X="-22.86246875" Y="-2.417745849609" />
                  <Point X="-22.889947265625" Y="-2.469956542969" />
                  <Point X="-22.899771484375" Y="-2.499735595703" />
                  <Point X="-22.904728515625" Y="-2.531909667969" />
                  <Point X="-22.90432421875" Y="-2.563259521484" />
                  <Point X="-22.8766484375" Y="-2.716502441406" />
                  <Point X="-22.865296875" Y="-2.779349853516" />
                  <Point X="-22.86101171875" Y="-2.795142333984" />
                  <Point X="-22.8481796875" Y="-2.826078369141" />
                  <Point X="-22.622701171875" Y="-3.21662109375" />
                  <Point X="-22.13871484375" Y="-4.054907226563" />
                  <Point X="-22.16580859375" Y="-4.074259277344" />
                  <Point X="-22.218134765625" Y="-4.111634277344" />
                  <Point X="-22.298234375" Y="-4.163481445312" />
                  <Point X="-22.514779296875" Y="-3.881273193359" />
                  <Point X="-23.241451171875" Y="-2.934255371094" />
                  <Point X="-23.25249609375" Y="-2.922177490234" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.42921484375" Y="-2.803388671875" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.520009765625" Y="-2.751167236328" />
                  <Point X="-23.551630859375" Y="-2.743435546875" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.7481953125" Y="-2.756327392578" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.843634765625" Y="-2.769396972656" />
                  <Point X="-23.87101953125" Y="-2.780739746094" />
                  <Point X="-23.89540234375" Y="-2.7954609375" />
                  <Point X="-24.0230390625" Y="-2.901587158203" />
                  <Point X="-24.07538671875" Y="-2.945111328125" />
                  <Point X="-24.095857421875" Y="-2.968860595703" />
                  <Point X="-24.11275" Y="-2.996687011719" />
                  <Point X="-24.124375" Y="-3.02580859375" />
                  <Point X="-24.162537109375" Y="-3.201388427734" />
                  <Point X="-24.178189453125" Y="-3.273396484375" />
                  <Point X="-24.180275390625" Y="-3.289627197266" />
                  <Point X="-24.1802578125" Y="-3.323120361328" />
                  <Point X="-24.116357421875" Y="-3.808483642578" />
                  <Point X="-23.977935546875" Y="-4.859915527344" />
                  <Point X="-24.024318359375" Y="-4.870083007812" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.05842578125" Y="-4.752636230469" />
                  <Point X="-26.14124609375" Y="-4.731328125" />
                  <Point X="-26.1207734375" Y="-4.575837890625" />
                  <Point X="-26.120076171875" Y="-4.56809765625" />
                  <Point X="-26.11944921875" Y="-4.541030273438" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497689453125" />
                  <Point X="-26.1662578125" Y="-4.281897460938" />
                  <Point X="-26.18386328125" Y="-4.193397949219" />
                  <Point X="-26.188125" Y="-4.17846875" />
                  <Point X="-26.19902734375" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135462402344" />
                  <Point X="-26.221740234375" Y="-4.107625488281" />
                  <Point X="-26.230576171875" Y="-4.094859863281" />
                  <Point X="-26.250208984375" Y="-4.070937011719" />
                  <Point X="-26.261005859375" Y="-4.059779785156" />
                  <Point X="-26.426423828125" Y="-3.914710449219" />
                  <Point X="-26.494265625" Y="-3.855215332031" />
                  <Point X="-26.506736328125" Y="-3.845965820312" />
                  <Point X="-26.53301953125" Y="-3.829621582031" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.85636328125" Y="-3.781779541016" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166259766" />
                  <Point X="-27.008005859375" Y="-3.781946044922" />
                  <Point X="-27.0390546875" Y="-3.790265869141" />
                  <Point X="-27.053673828125" Y="-3.795496337891" />
                  <Point X="-27.081865234375" Y="-3.80826953125" />
                  <Point X="-27.0954375" Y="-3.815812255859" />
                  <Point X="-27.278376953125" Y="-3.938048828125" />
                  <Point X="-27.35340234375" Y="-3.988180175781" />
                  <Point X="-27.35968359375" Y="-3.992759765625" />
                  <Point X="-27.38044140625" Y="-4.010133056641" />
                  <Point X="-27.402755859375" Y="-4.032770996094" />
                  <Point X="-27.410466796875" Y="-4.041627441406" />
                  <Point X="-27.475876953125" Y="-4.126869140625" />
                  <Point X="-27.503201171875" Y="-4.162479492188" />
                  <Point X="-27.630193359375" Y="-4.083849853516" />
                  <Point X="-27.747587890625" Y="-4.011161621094" />
                  <Point X="-27.98086328125" Y="-3.831547119141" />
                  <Point X="-27.8694140625" Y="-3.638512207031" />
                  <Point X="-27.341490234375" Y="-2.724119873047" />
                  <Point X="-27.3348515625" Y="-2.710086425781" />
                  <Point X="-27.32394921875" Y="-2.681122558594" />
                  <Point X="-27.319685546875" Y="-2.666191894531" />
                  <Point X="-27.3134140625" Y="-2.634667480469" />
                  <Point X="-27.311638671875" Y="-2.619242919922" />
                  <Point X="-27.310625" Y="-2.588310302734" />
                  <Point X="-27.3146640625" Y="-2.557626464844" />
                  <Point X="-27.323646484375" Y="-2.528009765625" />
                  <Point X="-27.3293515625" Y="-2.513568847656" />
                  <Point X="-27.34356640625" Y="-2.484740478516" />
                  <Point X="-27.351548828125" Y="-2.471422607422" />
                  <Point X="-27.369576171875" Y="-2.446262451172" />
                  <Point X="-27.37962109375" Y="-2.434420166016" />
                  <Point X="-27.39696875" Y="-2.417071533203" />
                  <Point X="-27.408810546875" Y="-2.407025390625" />
                  <Point X="-27.433966796875" Y="-2.388998046875" />
                  <Point X="-27.44728125" Y="-2.381016845703" />
                  <Point X="-27.476111328125" Y="-2.366797607422" />
                  <Point X="-27.49055078125" Y="-2.361090576172" />
                  <Point X="-27.52016796875" Y="-2.352104492188" />
                  <Point X="-27.55085546875" Y="-2.348062988281" />
                  <Point X="-27.58179296875" Y="-2.349074951172" />
                  <Point X="-27.597220703125" Y="-2.350849365234" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.061689453125" Y="-2.595435302734" />
                  <Point X="-28.7930859375" Y="-3.017707519531" />
                  <Point X="-28.91126953125" Y="-2.862440673828" />
                  <Point X="-29.00402734375" Y="-2.740573242188" />
                  <Point X="-29.181265625" Y="-2.443373535156" />
                  <Point X="-28.971818359375" Y="-2.282659667969" />
                  <Point X="-28.048123046875" Y="-1.573882080078" />
                  <Point X="-28.036482421875" Y="-1.563310302734" />
                  <Point X="-28.015107421875" Y="-1.540393066406" />
                  <Point X="-28.005373046875" Y="-1.528047363281" />
                  <Point X="-27.987408203125" Y="-1.500918334961" />
                  <Point X="-27.979841796875" Y="-1.487137084961" />
                  <Point X="-27.967083984375" Y="-1.458508789062" />
                  <Point X="-27.961892578125" Y="-1.443662475586" />
                  <Point X="-27.956427734375" Y="-1.422566772461" />
                  <Point X="-27.95155078125" Y="-1.39880480957" />
                  <Point X="-27.94874609375" Y="-1.368370483398" />
                  <Point X="-27.948576171875" Y="-1.353041992188" />
                  <Point X="-27.95078515625" Y="-1.321374389648" />
                  <Point X="-27.953080078125" Y="-1.306223022461" />
                  <Point X="-27.960080078125" Y="-1.276477539062" />
                  <Point X="-27.97177734375" Y="-1.248238525391" />
                  <Point X="-27.987861328125" Y="-1.222254882812" />
                  <Point X="-27.996953125" Y="-1.209916259766" />
                  <Point X="-28.01778515625" Y="-1.185961303711" />
                  <Point X="-28.028744140625" Y="-1.175243896484" />
                  <Point X="-28.052244140625" Y="-1.155710327148" />
                  <Point X="-28.06478515625" Y="-1.14689453125" />
                  <Point X="-28.08356640625" Y="-1.135841064453" />
                  <Point X="-28.105431640625" Y="-1.124481689453" />
                  <Point X="-28.134689453125" Y="-1.113259643555" />
                  <Point X="-28.149783203125" Y="-1.10886340332" />
                  <Point X="-28.181671875" Y="-1.102380126953" />
                  <Point X="-28.197291015625" Y="-1.100533203125" />
                  <Point X="-28.228619140625" Y="-1.09944152832" />
                  <Point X="-28.244328125" Y="-1.100196655273" />
                  <Point X="-28.71773828125" Y="-1.162522338867" />
                  <Point X="-29.66091796875" Y="-1.286694335938" />
                  <Point X="-29.704486328125" Y="-1.116131713867" />
                  <Point X="-29.740763671875" Y="-0.974112609863" />
                  <Point X="-29.786451171875" Y="-0.654654724121" />
                  <Point X="-29.560515625" Y="-0.594115356445" />
                  <Point X="-28.508287109375" Y="-0.312171325684" />
                  <Point X="-28.500474609375" Y="-0.309712524414" />
                  <Point X="-28.47393359375" Y="-0.299252441406" />
                  <Point X="-28.444169921875" Y="-0.283895233154" />
                  <Point X="-28.433564453125" Y="-0.277516052246" />
                  <Point X="-28.4138828125" Y="-0.263856262207" />
                  <Point X="-28.393673828125" Y="-0.248244918823" />
                  <Point X="-28.371216796875" Y="-0.226360046387" />
                  <Point X="-28.360896484375" Y="-0.214483886719" />
                  <Point X="-28.34165234375" Y="-0.188227706909" />
                  <Point X="-28.333435546875" Y="-0.174813308716" />
                  <Point X="-28.319328125" Y="-0.146813201904" />
                  <Point X="-28.3134375" Y="-0.132227798462" />
                  <Point X="-28.306876953125" Y="-0.111089904785" />
                  <Point X="-28.30098828125" Y="-0.088493125916" />
                  <Point X="-28.29671875" Y="-0.060327007294" />
                  <Point X="-28.295646484375" Y="-0.046082988739" />
                  <Point X="-28.2956484375" Y="-0.016446540833" />
                  <Point X="-28.29672265625" Y="-0.002208318472" />
                  <Point X="-28.3009921875" Y="0.025944574356" />
                  <Point X="-28.304185546875" Y="0.039856571198" />
                  <Point X="-28.310740234375" Y="0.060978408813" />
                  <Point X="-28.3193203125" Y="0.084233283997" />
                  <Point X="-28.3334296875" Y="0.112240081787" />
                  <Point X="-28.341646484375" Y="0.125653747559" />
                  <Point X="-28.360892578125" Y="0.151915557861" />
                  <Point X="-28.3712109375" Y="0.163792160034" />
                  <Point X="-28.393669921875" Y="0.185681503296" />
                  <Point X="-28.405810546875" Y="0.195693634033" />
                  <Point X="-28.4254921875" Y="0.209353561401" />
                  <Point X="-28.44323828125" Y="0.220278671265" />
                  <Point X="-28.466876953125" Y="0.233100082397" />
                  <Point X="-28.476984375" Y="0.237836837769" />
                  <Point X="-28.497689453125" Y="0.246092391968" />
                  <Point X="-28.508287109375" Y="0.249611190796" />
                  <Point X="-28.939822265625" Y="0.365240997314" />
                  <Point X="-29.7854453125" Y="0.591824768066" />
                  <Point X="-29.75470703125" Y="0.79955279541" />
                  <Point X="-29.73133203125" Y="0.957522033691" />
                  <Point X="-29.633583984375" Y="1.318236938477" />
                  <Point X="-29.532330078125" Y="1.304906494141" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.204703369141" />
                  <Point X="-28.71514453125" Y="1.20658972168" />
                  <Point X="-28.704890625" Y="1.208053588867" />
                  <Point X="-28.684603515625" Y="1.212088745117" />
                  <Point X="-28.6745703125" Y="1.21466027832" />
                  <Point X="-28.631009765625" Y="1.228394897461" />
                  <Point X="-28.603451171875" Y="1.23767590332" />
                  <Point X="-28.584515625" Y="1.246007568359" />
                  <Point X="-28.5752734375" Y="1.250690795898" />
                  <Point X="-28.556529296875" Y="1.261513061523" />
                  <Point X="-28.547853515625" Y="1.267174926758" />
                  <Point X="-28.53117578125" Y="1.279404907227" />
                  <Point X="-28.5159296875" Y="1.293375854492" />
                  <Point X="-28.50229296875" Y="1.308925170898" />
                  <Point X="-28.495900390625" Y="1.317071044922" />
                  <Point X="-28.483484375" Y="1.334801635742" />
                  <Point X="-28.478015625" Y="1.34359362793" />
                  <Point X="-28.4680625" Y="1.361732299805" />
                  <Point X="-28.463578125" Y="1.371078979492" />
                  <Point X="-28.446099609375" Y="1.413277099609" />
                  <Point X="-28.435494140625" Y="1.440362548828" />
                  <Point X="-28.429705078125" Y="1.460231567383" />
                  <Point X="-28.427353515625" Y="1.470320922852" />
                  <Point X="-28.42359765625" Y="1.491636962891" />
                  <Point X="-28.422359375" Y="1.501921630859" />
                  <Point X="-28.421009765625" Y="1.522555297852" />
                  <Point X="-28.421912109375" Y="1.543202636719" />
                  <Point X="-28.425056640625" Y="1.563640014648" />
                  <Point X="-28.4271875" Y="1.573779174805" />
                  <Point X="-28.4327890625" Y="1.594686035156" />
                  <Point X="-28.436013671875" Y="1.60453515625" />
                  <Point X="-28.44351171875" Y="1.623814086914" />
                  <Point X="-28.44778515625" Y="1.633244384766" />
                  <Point X="-28.468875" Y="1.673758544922" />
                  <Point X="-28.48280078125" Y="1.699286132812" />
                  <Point X="-28.494294921875" Y="1.716489257812" />
                  <Point X="-28.500513671875" Y="1.724779785156" />
                  <Point X="-28.51442578125" Y="1.741358154297" />
                  <Point X="-28.521505859375" Y="1.748917358398" />
                  <Point X="-28.5364453125" Y="1.763217529297" />
                  <Point X="-28.5443046875" Y="1.769958496094" />
                  <Point X="-28.791833984375" Y="1.959895263672" />
                  <Point X="-29.22761328125" Y="2.294280517578" />
                  <Point X="-29.09312109375" Y="2.524697021484" />
                  <Point X="-29.002283203125" Y="2.680322998047" />
                  <Point X="-28.726337890625" Y="3.035013183594" />
                  <Point X="-28.717404296875" Y="3.029855224609" />
                  <Point X="-28.25415625" Y="2.762398925781" />
                  <Point X="-28.24491796875" Y="2.757716796875" />
                  <Point X="-28.225984375" Y="2.749385742188" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.094404296875" Y="2.725850097656" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310791016" />
                  <Point X="-27.976431640625" Y="2.734227783203" />
                  <Point X="-27.9569921875" Y="2.741303466797" />
                  <Point X="-27.938439453125" Y="2.750453369141" />
                  <Point X="-27.929412109375" Y="2.755534667969" />
                  <Point X="-27.911158203125" Y="2.767164550781" />
                  <Point X="-27.902740234375" Y="2.773197265625" />
                  <Point X="-27.88661328125" Y="2.786141113281" />
                  <Point X="-27.878904296875" Y="2.793052246094" />
                  <Point X="-27.83583984375" Y="2.836114990234" />
                  <Point X="-27.8181796875" Y="2.853775634766" />
                  <Point X="-27.811265625" Y="2.861489257812" />
                  <Point X="-27.798318359375" Y="2.87762109375" />
                  <Point X="-27.79228515625" Y="2.886039306641" />
                  <Point X="-27.78065625" Y="2.904292480469" />
                  <Point X="-27.775576171875" Y="2.913316650391" />
                  <Point X="-27.766427734375" Y="2.931866210938" />
                  <Point X="-27.759353515625" Y="2.951297607422" />
                  <Point X="-27.754435546875" Y="2.971387207031" />
                  <Point X="-27.7525234375" Y="2.981570800781" />
                  <Point X="-27.749697265625" Y="3.003028076172" />
                  <Point X="-27.748908203125" Y="3.013362060547" />
                  <Point X="-27.74845703125" Y="3.034048583984" />
                  <Point X="-27.748794921875" Y="3.044401123047" />
                  <Point X="-27.754103515625" Y="3.105069580078" />
                  <Point X="-27.756279296875" Y="3.129950683594" />
                  <Point X="-27.757744140625" Y="3.140209716797" />
                  <Point X="-27.76178125" Y="3.160498291016" />
                  <Point X="-27.764353515625" Y="3.170527832031" />
                  <Point X="-27.77086328125" Y="3.191172119141" />
                  <Point X="-27.774509765625" Y="3.200864501953" />
                  <Point X="-27.78283984375" Y="3.219795410156" />
                  <Point X="-27.7875234375" Y="3.229033935547" />
                  <Point X="-27.897212890625" Y="3.419019287109" />
                  <Point X="-28.05938671875" Y="3.699914550781" />
                  <Point X="-27.8065625" Y="3.893752441406" />
                  <Point X="-27.648365234375" Y="4.015041992188" />
                  <Point X="-27.2012734375" Y="4.263436523438" />
                  <Point X="-27.192525390625" Y="4.268297363281" />
                  <Point X="-27.118564453125" Y="4.17191015625" />
                  <Point X="-27.11182421875" Y="4.164052246094" />
                  <Point X="-27.097521484375" Y="4.149109863281" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.065091796875" Y="4.121897460938" />
                  <Point X="-27.047892578125" Y="4.110405273438" />
                  <Point X="-27.03898046875" Y="4.105129394531" />
                  <Point X="-26.97145703125" Y="4.069978271484" />
                  <Point X="-26.943763671875" Y="4.055562255859" />
                  <Point X="-26.934326171875" Y="4.051286621094" />
                  <Point X="-26.915044921875" Y="4.0437890625" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834716797" />
                  <Point X="-26.853720703125" Y="4.029688720703" />
                  <Point X="-26.83305859375" Y="4.028786132812" />
                  <Point X="-26.812419921875" Y="4.030138183594" />
                  <Point X="-26.802138671875" Y="4.031377685547" />
                  <Point X="-26.780822265625" Y="4.035135498047" />
                  <Point X="-26.770732421875" Y="4.037487548828" />
                  <Point X="-26.750869140625" Y="4.043276611328" />
                  <Point X="-26.741095703125" Y="4.046713623047" />
                  <Point X="-26.670765625" Y="4.075845947266" />
                  <Point X="-26.641921875" Y="4.087793212891" />
                  <Point X="-26.63258203125" Y="4.0922734375" />
                  <Point X="-26.6144453125" Y="4.102224121094" />
                  <Point X="-26.6056484375" Y="4.107694824219" />
                  <Point X="-26.587919921875" Y="4.120109375" />
                  <Point X="-26.579771484375" Y="4.12650390625" />
                  <Point X="-26.56422265625" Y="4.140140625" />
                  <Point X="-26.55025390625" Y="4.155385742188" />
                  <Point X="-26.5380234375" Y="4.172064453125" />
                  <Point X="-26.532361328125" Y="4.180740234375" />
                  <Point X="-26.5215390625" Y="4.199483886719" />
                  <Point X="-26.51685546875" Y="4.208724121094" />
                  <Point X="-26.5085234375" Y="4.227659667969" />
                  <Point X="-26.504875" Y="4.237354980469" />
                  <Point X="-26.481984375" Y="4.309956542969" />
                  <Point X="-26.472595703125" Y="4.339731445312" />
                  <Point X="-26.470025390625" Y="4.349763671875" />
                  <Point X="-26.465990234375" Y="4.370049316406" />
                  <Point X="-26.464525390625" Y="4.380302734375" />
                  <Point X="-26.462638671875" Y="4.401865234375" />
                  <Point X="-26.46230078125" Y="4.412217773438" />
                  <Point X="-26.462751953125" Y="4.432898925781" />
                  <Point X="-26.463541015625" Y="4.443227539062" />
                  <Point X="-26.47601171875" Y="4.53794921875" />
                  <Point X="-26.479263671875" Y="4.562654785156" />
                  <Point X="-26.1359609375" Y="4.658905273438" />
                  <Point X="-25.931169921875" Y="4.716320800781" />
                  <Point X="-25.38916015625" Y="4.779754882812" />
                  <Point X="-25.36522265625" Y="4.782556152344" />
                  <Point X="-25.362708984375" Y="4.773174316406" />
                  <Point X="-25.22566796875" Y="4.261728027344" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.70581640625" Y="4.471479980469" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.350943359375" Y="4.756639648438" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.723689453125" Y="4.629646972656" />
                  <Point X="-23.546408203125" Y="4.586845214844" />
                  <Point X="-23.255919921875" Y="4.481482910156" />
                  <Point X="-23.141734375" Y="4.440066894531" />
                  <Point X="-22.859498046875" Y="4.308074707031" />
                  <Point X="-22.7495546875" Y="4.256657226562" />
                  <Point X="-22.4768359375" Y="4.097771972656" />
                  <Point X="-22.370564453125" Y="4.035858398438" />
                  <Point X="-22.18221875" Y="3.901916503906" />
                  <Point X="-22.32365625" Y="3.656938232422" />
                  <Point X="-22.934689453125" Y="2.59859765625" />
                  <Point X="-22.937619140625" Y="2.593116699219" />
                  <Point X="-22.94681640625" Y="2.573442871094" />
                  <Point X="-22.95581640625" Y="2.549567626953" />
                  <Point X="-22.958697265625" Y="2.540602050781" />
                  <Point X="-22.973923828125" Y="2.483666015625" />
                  <Point X="-22.98016796875" Y="2.460315673828" />
                  <Point X="-22.9820859375" Y="2.451470214844" />
                  <Point X="-22.986353515625" Y="2.420223632812" />
                  <Point X="-22.987244140625" Y="2.383241210938" />
                  <Point X="-22.986587890625" Y="2.369579589844" />
                  <Point X="-22.980650390625" Y="2.320346191406" />
                  <Point X="-22.978216796875" Y="2.300154785156" />
                  <Point X="-22.97619921875" Y="2.289038818359" />
                  <Point X="-22.970857421875" Y="2.267116455078" />
                  <Point X="-22.967533203125" Y="2.256310058594" />
                  <Point X="-22.959265625" Y="2.234218017578" />
                  <Point X="-22.9546796875" Y="2.223886474609" />
                  <Point X="-22.94431640625" Y="2.203841064453" />
                  <Point X="-22.9385390625" Y="2.194127197266" />
                  <Point X="-22.90807421875" Y="2.149231201172" />
                  <Point X="-22.895580078125" Y="2.130818603516" />
                  <Point X="-22.89018359375" Y="2.123626220703" />
                  <Point X="-22.869533203125" Y="2.100121826172" />
                  <Point X="-22.842400390625" Y="2.07538671875" />
                  <Point X="-22.831740234375" Y="2.066981933594" />
                  <Point X="-22.786845703125" Y="2.036518066406" />
                  <Point X="-22.768431640625" Y="2.024024291992" />
                  <Point X="-22.758716796875" Y="2.018244995117" />
                  <Point X="-22.738669921875" Y="2.007881225586" />
                  <Point X="-22.728337890625" Y="2.00329675293" />
                  <Point X="-22.706251953125" Y="1.995031860352" />
                  <Point X="-22.695451171875" Y="1.991707397461" />
                  <Point X="-22.673529296875" Y="1.986365112305" />
                  <Point X="-22.662408203125" Y="1.984346801758" />
                  <Point X="-22.61317578125" Y="1.97841003418" />
                  <Point X="-22.592984375" Y="1.975975219727" />
                  <Point X="-22.58395703125" Y="1.975320922852" />
                  <Point X="-22.55242578125" Y="1.975496948242" />
                  <Point X="-22.515685546875" Y="1.979822509766" />
                  <Point X="-22.50225" Y="1.982395751953" />
                  <Point X="-22.445314453125" Y="1.997621337891" />
                  <Point X="-22.421962890625" Y="2.003865356445" />
                  <Point X="-22.416" Y="2.00567175293" />
                  <Point X="-22.39559765625" Y="2.01306640625" />
                  <Point X="-22.372345703125" Y="2.023573486328" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.929921875" Y="2.278467041016" />
                  <Point X="-21.059595703125" Y="2.780949951172" />
                  <Point X="-21.019087890625" Y="2.724652587891" />
                  <Point X="-20.956037109375" Y="2.637028320312" />
                  <Point X="-20.863115234375" Y="2.483471435547" />
                  <Point X="-21.022564453125" Y="2.361120849609" />
                  <Point X="-21.827046875" Y="1.743819946289" />
                  <Point X="-21.831857421875" Y="1.739872802734" />
                  <Point X="-21.84787890625" Y="1.72521875" />
                  <Point X="-21.86533203125" Y="1.706602905273" />
                  <Point X="-21.87142578125" Y="1.699421142578" />
                  <Point X="-21.91240234375" Y="1.645963623047" />
                  <Point X="-21.92920703125" Y="1.624039794922" />
                  <Point X="-21.934365234375" Y="1.616599853516" />
                  <Point X="-21.950255859375" Y="1.589379760742" />
                  <Point X="-21.965234375" Y="1.555552734375" />
                  <Point X="-21.969859375" Y="1.542674682617" />
                  <Point X="-21.985123046875" Y="1.488094238281" />
                  <Point X="-21.991384765625" Y="1.465710205078" />
                  <Point X="-21.993775390625" Y="1.454663452148" />
                  <Point X="-21.997228515625" Y="1.432366088867" />
                  <Point X="-21.998291015625" Y="1.421115356445" />
                  <Point X="-21.999107421875" Y="1.397541992188" />
                  <Point X="-21.998826171875" Y="1.386237426758" />
                  <Point X="-21.996921875" Y="1.363750488281" />
                  <Point X="-21.995298828125" Y="1.352567871094" />
                  <Point X="-21.982767578125" Y="1.291840332031" />
                  <Point X="-21.97762890625" Y="1.266934936523" />
                  <Point X="-21.97540234375" Y="1.258236694336" />
                  <Point X="-21.965318359375" Y="1.228615112305" />
                  <Point X="-21.949716796875" Y="1.195376342773" />
                  <Point X="-21.943083984375" Y="1.183527587891" />
                  <Point X="-21.90900390625" Y="1.13172644043" />
                  <Point X="-21.89502734375" Y="1.110482055664" />
                  <Point X="-21.88826171875" Y="1.101424316406" />
                  <Point X="-21.87370703125" Y="1.084178588867" />
                  <Point X="-21.86591796875" Y="1.075990234375" />
                  <Point X="-21.848673828125" Y="1.059900512695" />
                  <Point X="-21.839962890625" Y="1.052693847656" />
                  <Point X="-21.821751953125" Y="1.039368164063" />
                  <Point X="-21.812251953125" Y="1.033249389648" />
                  <Point X="-21.76286328125" Y="1.005448425293" />
                  <Point X="-21.742609375" Y="0.994046936035" />
                  <Point X="-21.734521484375" Y="0.989988342285" />
                  <Point X="-21.7053203125" Y="0.978083984375" />
                  <Point X="-21.66972265625" Y="0.968021179199" />
                  <Point X="-21.656328125" Y="0.965257629395" />
                  <Point X="-21.589552734375" Y="0.956432434082" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.555966796875" Y="0.952199829102" />
                  <Point X="-21.53428125" Y="0.951222900391" />
                  <Point X="-21.50878125" Y="0.952032287598" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-21.087083984375" Y="1.007079101562" />
                  <Point X="-20.295296875" Y="1.111319946289" />
                  <Point X="-20.274390625" Y="1.025445800781" />
                  <Point X="-20.2473125" Y="0.914207885742" />
                  <Point X="-20.216126953125" Y="0.713921203613" />
                  <Point X="-20.386052734375" Y="0.668389648438" />
                  <Point X="-21.3080078125" Y="0.42135269165" />
                  <Point X="-21.313966796875" Y="0.419544830322" />
                  <Point X="-21.334375" Y="0.412137176514" />
                  <Point X="-21.357619140625" Y="0.401618682861" />
                  <Point X="-21.365994140625" Y="0.397316192627" />
                  <Point X="-21.43159765625" Y="0.359395507812" />
                  <Point X="-21.45850390625" Y="0.343843902588" />
                  <Point X="-21.4661171875" Y="0.338945281982" />
                  <Point X="-21.4912265625" Y="0.319869537354" />
                  <Point X="-21.518005859375" Y="0.294351135254" />
                  <Point X="-21.527203125" Y="0.284226959229" />
                  <Point X="-21.56656640625" Y="0.23406968689" />
                  <Point X="-21.582708984375" Y="0.213499389648" />
                  <Point X="-21.589146484375" Y="0.204207336426" />
                  <Point X="-21.60087109375" Y="0.184926040649" />
                  <Point X="-21.606158203125" Y="0.174936798096" />
                  <Point X="-21.615931640625" Y="0.153473434448" />
                  <Point X="-21.619994140625" Y="0.14292640686" />
                  <Point X="-21.62683984375" Y="0.121426017761" />
                  <Point X="-21.629623046875" Y="0.110472808838" />
                  <Point X="-21.642744140625" Y="0.041960327148" />
                  <Point X="-21.648125" Y="0.013862423897" />
                  <Point X="-21.649396484375" Y="0.004966006279" />
                  <Point X="-21.6514140625" Y="-0.026262191772" />
                  <Point X="-21.64971875" Y="-0.062939796448" />
                  <Point X="-21.648125" Y="-0.076422569275" />
                  <Point X="-21.63500390625" Y="-0.144934906006" />
                  <Point X="-21.629623046875" Y="-0.173032958984" />
                  <Point X="-21.62683984375" Y="-0.183986160278" />
                  <Point X="-21.619994140625" Y="-0.205486541748" />
                  <Point X="-21.615931640625" Y="-0.216033569336" />
                  <Point X="-21.606158203125" Y="-0.237496948242" />
                  <Point X="-21.600869140625" Y="-0.247486495972" />
                  <Point X="-21.58914453125" Y="-0.26676763916" />
                  <Point X="-21.582708984375" Y="-0.276059387207" />
                  <Point X="-21.543345703125" Y="-0.326216644287" />
                  <Point X="-21.527203125" Y="-0.346787109375" />
                  <Point X="-21.521279296875" Y="-0.353636230469" />
                  <Point X="-21.4988671875" Y="-0.375797241211" />
                  <Point X="-21.469828125" Y="-0.398721130371" />
                  <Point X="-21.458505859375" Y="-0.406404205322" />
                  <Point X="-21.392900390625" Y="-0.444324890137" />
                  <Point X="-21.36599609375" Y="-0.459876800537" />
                  <Point X="-21.360505859375" Y="-0.462814483643" />
                  <Point X="-21.340841796875" Y="-0.472016906738" />
                  <Point X="-21.316970703125" Y="-0.481028198242" />
                  <Point X="-21.3080078125" Y="-0.483912963867" />
                  <Point X="-20.9298984375" Y="-0.58522668457" />
                  <Point X="-20.215119140625" Y="-0.776751220703" />
                  <Point X="-20.223265625" Y="-0.830783508301" />
                  <Point X="-20.238380859375" Y="-0.931035949707" />
                  <Point X="-20.2721953125" Y="-1.079219848633" />
                  <Point X="-20.494009765625" Y="-1.050017578125" />
                  <Point X="-21.563216796875" Y="-0.909253601074" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042480469" />
                  <Point X="-21.63327734375" Y="-0.910840881348" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.774275390625" Y="-0.940662536621" />
                  <Point X="-21.82708203125" Y="-0.952140075684" />
                  <Point X="-21.842125" Y="-0.956742492676" />
                  <Point X="-21.87124609375" Y="-0.968367004395" />
                  <Point X="-21.88532421875" Y="-0.975389465332" />
                  <Point X="-21.913150390625" Y="-0.992281616211" />
                  <Point X="-21.925875" Y="-1.001530273438" />
                  <Point X="-21.949625" Y="-1.022000976562" />
                  <Point X="-21.960650390625" Y="-1.033223022461" />
                  <Point X="-22.0384765625" Y="-1.126823852539" />
                  <Point X="-22.07039453125" Y="-1.1652109375" />
                  <Point X="-22.07867578125" Y="-1.176850952148" />
                  <Point X="-22.09339453125" Y="-1.201232543945" />
                  <Point X="-22.09983203125" Y="-1.213973876953" />
                  <Point X="-22.11117578125" Y="-1.241359619141" />
                  <Point X="-22.115634765625" Y="-1.254927368164" />
                  <Point X="-22.122466796875" Y="-1.282577758789" />
                  <Point X="-22.12483984375" Y="-1.296660522461" />
                  <Point X="-22.135994140625" Y="-1.417877807617" />
                  <Point X="-22.140568359375" Y="-1.467591064453" />
                  <Point X="-22.140708984375" Y="-1.483320068359" />
                  <Point X="-22.138390625" Y="-1.514590698242" />
                  <Point X="-22.135931640625" Y="-1.530132446289" />
                  <Point X="-22.12819921875" Y="-1.56175378418" />
                  <Point X="-22.123208984375" Y="-1.576673461914" />
                  <Point X="-22.110837890625" Y="-1.605481933594" />
                  <Point X="-22.10345703125" Y="-1.61937097168" />
                  <Point X="-22.032201171875" Y="-1.730206420898" />
                  <Point X="-22.0029765625" Y="-1.775661621094" />
                  <Point X="-21.998255859375" Y="-1.782353515625" />
                  <Point X="-21.980205078125" Y="-1.804448974609" />
                  <Point X="-21.956509765625" Y="-1.828119628906" />
                  <Point X="-21.947201171875" Y="-1.836277587891" />
                  <Point X="-21.596314453125" Y="-2.105522705078" />
                  <Point X="-20.912828125" Y="-2.629981201172" />
                  <Point X="-20.9545078125" Y="-2.697422119141" />
                  <Point X="-20.9987265625" Y="-2.760251464844" />
                  <Point X="-21.199271484375" Y="-2.644466308594" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.184970703125" Y="-2.079513916016" />
                  <Point X="-22.216876953125" Y="-2.069326416016" />
                  <Point X="-22.228888671875" Y="-2.066337646484" />
                  <Point X="-22.382130859375" Y="-2.038662109375" />
                  <Point X="-22.444978515625" Y="-2.027311889648" />
                  <Point X="-22.460638671875" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.55515625" Y="-2.03513659668" />
                  <Point X="-22.584931640625" Y="-2.044960571289" />
                  <Point X="-22.59941015625" Y="-2.051109130859" />
                  <Point X="-22.726716796875" Y="-2.118110107422" />
                  <Point X="-22.778927734375" Y="-2.145588134766" />
                  <Point X="-22.791029296875" Y="-2.153170166016" />
                  <Point X="-22.8139609375" Y="-2.170063720703" />
                  <Point X="-22.824791015625" Y="-2.179375244141" />
                  <Point X="-22.84575" Y="-2.200334716797" />
                  <Point X="-22.85505859375" Y="-2.211161132813" />
                  <Point X="-22.871951171875" Y="-2.234090576172" />
                  <Point X="-22.87953515625" Y="-2.246193603516" />
                  <Point X="-22.946537109375" Y="-2.373500976562" />
                  <Point X="-22.974015625" Y="-2.425711669922" />
                  <Point X="-22.9801640625" Y="-2.440193603516" />
                  <Point X="-22.98998828125" Y="-2.46997265625" />
                  <Point X="-22.9936640625" Y="-2.485269775391" />
                  <Point X="-22.99862109375" Y="-2.517443847656" />
                  <Point X="-22.999720703125" Y="-2.533134765625" />
                  <Point X="-22.99931640625" Y="-2.564484619141" />
                  <Point X="-22.9978125" Y="-2.580143554688" />
                  <Point X="-22.97013671875" Y="-2.733386474609" />
                  <Point X="-22.95878515625" Y="-2.796233886719" />
                  <Point X="-22.956982421875" Y="-2.804227783203" />
                  <Point X="-22.94876171875" Y="-2.831540527344" />
                  <Point X="-22.9359296875" Y="-2.8624765625" />
                  <Point X="-22.930453125" Y="-2.873578125" />
                  <Point X="-22.704974609375" Y="-3.264120849609" />
                  <Point X="-22.26410546875" Y="-4.027725097656" />
                  <Point X="-22.276244140625" Y="-4.036083496094" />
                  <Point X="-22.43941015625" Y="-3.823441162109" />
                  <Point X="-23.16608203125" Y="-2.876423339844" />
                  <Point X="-23.171345703125" Y="-2.870145019531" />
                  <Point X="-23.191169921875" Y="-2.849624023438" />
                  <Point X="-23.216748046875" Y="-2.828003662109" />
                  <Point X="-23.22669921875" Y="-2.820646728516" />
                  <Point X="-23.37783984375" Y="-2.723478271484" />
                  <Point X="-23.43982421875" Y="-2.683627929688" />
                  <Point X="-23.453716796875" Y="-2.676245605469" />
                  <Point X="-23.48252734375" Y="-2.663874511719" />
                  <Point X="-23.4974453125" Y="-2.658885742188" />
                  <Point X="-23.52906640625" Y="-2.651154052734" />
                  <Point X="-23.54460546875" Y="-2.648695800781" />
                  <Point X="-23.575875" Y="-2.646376953125" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.756900390625" Y="-2.661727050781" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670339111328" />
                  <Point X="-23.866421875" Y="-2.677170410156" />
                  <Point X="-23.87998828125" Y="-2.681627929688" />
                  <Point X="-23.907373046875" Y="-2.692970703125" />
                  <Point X="-23.92012109375" Y="-2.699412841797" />
                  <Point X="-23.94450390625" Y="-2.714134033203" />
                  <Point X="-23.956138671875" Y="-2.722413085938" />
                  <Point X="-24.083775390625" Y="-2.828539306641" />
                  <Point X="-24.136123046875" Y="-2.872063476562" />
                  <Point X="-24.147345703125" Y="-2.883086914062" />
                  <Point X="-24.16781640625" Y="-2.906836181641" />
                  <Point X="-24.177064453125" Y="-2.919562011719" />
                  <Point X="-24.19395703125" Y="-2.947388427734" />
                  <Point X="-24.20098046875" Y="-2.961466552734" />
                  <Point X="-24.21260546875" Y="-2.990588134766" />
                  <Point X="-24.21720703125" Y="-3.005631591797" />
                  <Point X="-24.255369140625" Y="-3.181211425781" />
                  <Point X="-24.271021484375" Y="-3.253219482422" />
                  <Point X="-24.2724140625" Y="-3.261286865234" />
                  <Point X="-24.275275390625" Y="-3.289677001953" />
                  <Point X="-24.2752578125" Y="-3.323170166016" />
                  <Point X="-24.2744453125" Y="-3.335520507812" />
                  <Point X="-24.210544921875" Y="-3.820883789062" />
                  <Point X="-24.16691015625" Y="-4.152320800781" />
                  <Point X="-24.344931640625" Y="-3.487937255859" />
                  <Point X="-24.347392578125" Y="-3.480121337891" />
                  <Point X="-24.357853515625" Y="-3.453579589844" />
                  <Point X="-24.3732109375" Y="-3.423815917969" />
                  <Point X="-24.37958984375" Y="-3.413209960938" />
                  <Point X="-24.49569921875" Y="-3.245918457031" />
                  <Point X="-24.543318359375" Y="-3.177309570312" />
                  <Point X="-24.553328125" Y="-3.165172851562" />
                  <Point X="-24.575212890625" Y="-3.142716552734" />
                  <Point X="-24.587087890625" Y="-3.132396972656" />
                  <Point X="-24.61334375" Y="-3.113153320312" />
                  <Point X="-24.626759765625" Y="-3.104936279297" />
                  <Point X="-24.654759765625" Y="-3.090829101562" />
                  <Point X="-24.66934375" Y="-3.084938964844" />
                  <Point X="-24.849015625" Y="-3.029175048828" />
                  <Point X="-24.922703125" Y="-3.006305664062" />
                  <Point X="-24.936623046875" Y="-3.003109619141" />
                  <Point X="-24.964783203125" Y="-2.998840087891" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.24465625" Y="-3.062068847656" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.332931640625" Y="-3.090830322266" />
                  <Point X="-25.360931640625" Y="-3.104938720703" />
                  <Point X="-25.37434375" Y="-3.113155273438" />
                  <Point X="-25.400599609375" Y="-3.132399658203" />
                  <Point X="-25.41247265625" Y="-3.142716796875" />
                  <Point X="-25.434357421875" Y="-3.165172119141" />
                  <Point X="-25.444369140625" Y="-3.177310302734" />
                  <Point X="-25.560478515625" Y="-3.3446015625" />
                  <Point X="-25.608095703125" Y="-3.413210693359" />
                  <Point X="-25.61246875" Y="-3.420130859375" />
                  <Point X="-25.625974609375" Y="-3.445261230469" />
                  <Point X="-25.63877734375" Y="-3.476214355469" />
                  <Point X="-25.64275390625" Y="-3.487937011719" />
                  <Point X="-25.7592578125" Y="-3.922737548828" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.638507759442" Y="4.19196156717" />
                  <Point X="-24.388519657367" Y="4.760574901448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.290700536384" Y="3.979063238604" />
                  <Point X="-24.633766588517" Y="4.740371548516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.210410588144" Y="3.853086541708" />
                  <Point X="-24.658388310295" Y="4.648482719575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.258971543504" Y="3.76897604127" />
                  <Point X="-24.683010032073" Y="4.556593890633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.363961301951" Y="4.777848370391" />
                  <Point X="-25.37494774131" Y="4.78141808093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.307532498864" Y="3.684865540832" />
                  <Point X="-24.707631745148" Y="4.464705058863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.334643936444" Y="4.668433669594" />
                  <Point X="-25.600964290267" Y="4.754966398058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.356093602511" Y="3.600755088575" />
                  <Point X="-24.732253348883" Y="4.372816191567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.305326476857" Y="4.559018938227" />
                  <Point X="-25.826980497436" Y="4.728514604133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.404654779869" Y="3.516644660268" />
                  <Point X="-24.756874952618" Y="4.280927324271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.27600901727" Y="4.449604206861" />
                  <Point X="-26.020123465057" Y="4.691381647199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.453215957227" Y="3.432534231961" />
                  <Point X="-24.795436649052" Y="4.193567867659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.246691557682" Y="4.340189475495" />
                  <Point X="-26.185152006751" Y="4.645113759533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.501777134585" Y="3.348423803655" />
                  <Point X="-24.872556741387" Y="4.118736793332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.212286369572" Y="4.229121640923" />
                  <Point X="-26.350179656374" Y="4.598845582016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.058648910645" Y="2.779634108204" />
                  <Point X="-21.06071311993" Y="2.780304810458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.550338311943" Y="3.264313375348" />
                  <Point X="-26.476271992161" Y="4.539926554155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.964842605293" Y="2.649265680662" />
                  <Point X="-21.171421591751" Y="2.71638726219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.598899489301" Y="3.180202947041" />
                  <Point X="-26.462967059383" Y="4.435714608136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.887860388284" Y="2.524363730793" />
                  <Point X="-21.282130063572" Y="2.652469713922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.647460666659" Y="3.096092518734" />
                  <Point X="-26.472817999825" Y="4.339026461409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.924490068079" Y="2.436376523923" />
                  <Point X="-21.392838535394" Y="2.588552165654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.696021844017" Y="3.011982090427" />
                  <Point X="-26.501386311523" Y="4.248419957265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.015942299676" Y="2.36620224393" />
                  <Point X="-21.503547007215" Y="2.524634617386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.744583021374" Y="2.927871662121" />
                  <Point X="-26.544910288155" Y="4.162672843229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.107394984722" Y="2.296028111273" />
                  <Point X="-21.614255479036" Y="2.460717069118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.793144198732" Y="2.843761233814" />
                  <Point X="-26.633828884373" Y="4.091675335198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.198847705165" Y="2.225853990116" />
                  <Point X="-21.724963950858" Y="2.39679952085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.84170537609" Y="2.759650805507" />
                  <Point X="-26.772915939242" Y="4.036978547514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.086002727963" Y="4.1387066118" />
                  <Point X="-27.300394822507" Y="4.208366826034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.290300425609" Y="2.15567986896" />
                  <Point X="-21.835672422679" Y="2.332881972582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.890266553448" Y="2.6755403772" />
                  <Point X="-27.413840740516" Y="4.145338727949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.381753146052" Y="2.085505747803" />
                  <Point X="-21.946380905969" Y="2.26896442804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.938463066105" Y="2.591311462149" />
                  <Point X="-27.527286658525" Y="4.082310629865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.473205866496" Y="2.015331626647" />
                  <Point X="-22.05708945493" Y="2.205046904836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.969180325268" Y="2.501403193363" />
                  <Point X="-27.640732576534" Y="4.019282531781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.564658586939" Y="1.94515750549" />
                  <Point X="-22.167798003891" Y="2.141129381632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.986667249039" Y="2.40719612802" />
                  <Point X="-27.733714123844" Y="3.949605156585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.656111307383" Y="1.874983384334" />
                  <Point X="-22.278506552852" Y="2.077211858428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.978769568617" Y="2.304741104794" />
                  <Point X="-27.825219642085" Y="3.879448190473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.747564027826" Y="1.804809263177" />
                  <Point X="-22.392634539468" Y="2.014405377869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.936496295442" Y="2.191116774413" />
                  <Point X="-27.91672568032" Y="3.809291393317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.837959431397" Y="1.734291598944" />
                  <Point X="-22.579842113537" Y="1.975343894665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.804053249688" Y="2.048194508915" />
                  <Point X="-28.008231718555" Y="3.739134596162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.904653355883" Y="1.656072857326" />
                  <Point X="-28.028083863359" Y="3.645696037718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.274663891218" Y="1.026568264301" />
                  <Point X="-20.466239389414" Y="1.088814916981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.957370301494" Y="1.573312719977" />
                  <Point X="-27.957096834986" Y="3.52274204272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.248259989093" Y="0.918100205141" />
                  <Point X="-20.685019434762" Y="1.060011951555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.986572663656" Y="1.482912231318" />
                  <Point X="-27.886109542193" Y="3.399787961807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.231518413263" Y="0.812771626105" />
                  <Point X="-20.90379948011" Y="1.031208986128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.998845417346" Y="1.387010979416" />
                  <Point X="-27.815120823264" Y="3.276833417514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.226313813532" Y="0.711191637839" />
                  <Point X="-21.122579506978" Y="1.002406014697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.98056880733" Y="1.281183637539" />
                  <Point X="-27.761593005377" Y="3.159552263883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.394797672588" Y="0.666046450841" />
                  <Point X="-21.34135943842" Y="0.973603012261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.931024024003" Y="1.165196650287" />
                  <Point X="-27.749794962563" Y="3.055829936093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.563281704708" Y="0.620901320075" />
                  <Point X="-21.600319863523" Y="0.957855443619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.784800139877" Y="1.017796718973" />
                  <Point X="-27.757596275532" Y="2.958475825031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.731765736829" Y="0.575756189309" />
                  <Point X="-27.802012312235" Y="2.873018558881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.900249768949" Y="0.530611058543" />
                  <Point X="-27.875084236393" Y="2.796872154979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.06873380107" Y="0.485465927777" />
                  <Point X="-27.984006435387" Y="2.732374211487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.482941230653" Y="2.894487953605" />
                  <Point X="-28.764493191614" Y="2.985969731234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.23721783319" Y="0.440320797011" />
                  <Point X="-28.826525061023" Y="2.906236196096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.382653294033" Y="0.387686731467" />
                  <Point X="-28.888556930431" Y="2.826502660959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.488270818393" Y="0.322115034096" />
                  <Point X="-28.950588799839" Y="2.746769125821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.558064172541" Y="0.244903358222" />
                  <Point X="-29.010450549114" Y="2.666330475909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.611828805868" Y="0.162483635248" />
                  <Point X="-29.059460212973" Y="2.5823657697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.637212805368" Y="0.070842485353" />
                  <Point X="-29.108469832967" Y="2.498401049238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.651298259414" Y="-0.0244697845" />
                  <Point X="-29.157479356765" Y="2.41443629752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.638124803524" Y="-0.128639011089" />
                  <Point X="-29.206488880562" Y="2.330471545802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.605251722918" Y="-0.239209033756" />
                  <Point X="-29.099147636805" Y="2.195705350185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.21713540537" Y="-0.790124277293" />
                  <Point X="-20.461355000353" Y="-0.710772520677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.502079930983" Y="-0.372620492354" />
                  <Point X="-28.873362725116" Y="2.022454473962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.231492453369" Y="-0.88534830092" />
                  <Point X="-28.647578914471" Y="1.849203955491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.249419959307" Y="-0.97941221244" />
                  <Point X="-28.480473002683" Y="1.695019042091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.270640479623" Y="-1.072406158727" />
                  <Point X="-28.428372403016" Y="1.578201619771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.751169076821" Y="-1.016161864197" />
                  <Point X="-28.426076797504" Y="1.477566821023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.268013161794" Y="-0.948117952411" />
                  <Point X="-28.456720715111" Y="1.38763472212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.668885814709" Y="-0.9177554431" />
                  <Point X="-28.506641968069" Y="1.303966209164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.848495736598" Y="-0.959285553142" />
                  <Point X="-28.606690419562" Y="1.236585010329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.952838240117" Y="-1.025271529897" />
                  <Point X="-28.846502343317" Y="1.214615716446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.019172566597" Y="-1.10360711199" />
                  <Point X="-29.363345315418" Y="1.282659266639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.082365715864" Y="-1.182963324429" />
                  <Point X="-29.645454141626" Y="1.274433069452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.119549392257" Y="-1.270770526893" />
                  <Point X="-29.6703320109" Y="1.182627467877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.131297886307" Y="-1.366842121078" />
                  <Point X="-29.695209880173" Y="1.090821866302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.140222413679" Y="-1.463831277658" />
                  <Point X="-29.720087749446" Y="0.999016264726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.125995355101" Y="-1.568342840512" />
                  <Point X="-29.739060604667" Y="0.905292007779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.057799000447" Y="-1.690390090653" />
                  <Point X="-29.753163307978" Y="0.809985342552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.963823518478" Y="-1.82081348701" />
                  <Point X="-29.76726615994" Y="0.714678725625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.744162965377" Y="-1.992074438501" />
                  <Point X="-28.368053699251" Y="0.16015812663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.278662606189" Y="0.456032896059" />
                  <Point X="-29.781369030174" Y="0.619372114634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.518378519198" Y="-2.16532516347" />
                  <Point X="-28.304101368173" Y="0.03948984334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.29259430197" Y="-2.338575814049" />
                  <Point X="-28.297075705452" Y="-0.06268184416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.066810084741" Y="-2.511826464627" />
                  <Point X="-21.895220946175" Y="-2.242659459174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.542942495659" Y="-2.032201970072" />
                  <Point X="-28.323020081112" Y="-0.154140916804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.92917707531" Y="-2.656435051546" />
                  <Point X="-21.499513035294" Y="-2.471121664677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.672992322542" Y="-2.089835131129" />
                  <Point X="-28.380459011375" Y="-0.235366788334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.983532628049" Y="-2.738662773164" />
                  <Point X="-21.10380592092" Y="-2.699583611379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.789138860316" Y="-2.15198574466" />
                  <Point X="-28.481765710231" Y="-0.302339157818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.86652803773" Y="-2.226729387945" />
                  <Point X="-28.645652026706" Y="-0.348978176955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.91378203493" Y="-2.311264544832" />
                  <Point X="-28.814135941486" Y="-0.394123345847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.958676442797" Y="-2.396566378768" />
                  <Point X="-28.982619856265" Y="-0.439268514739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.993623351162" Y="-2.48510035122" />
                  <Point X="-29.151103771044" Y="-0.484413683632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.997143953187" Y="-2.583845349582" />
                  <Point X="-29.319587685823" Y="-0.529558852524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.977979351331" Y="-2.689961217498" />
                  <Point X="-29.488071600602" Y="-0.574704021417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.958813398578" Y="-2.796077524347" />
                  <Point X="-28.116376862139" Y="-1.120283570465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.154435690627" Y="-1.107917507474" />
                  <Point X="-29.656555467542" Y="-0.619849205853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.907994041483" Y="-2.91247864572" />
                  <Point X="-23.263561991846" Y="-2.796947615298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.696760505334" Y="-2.656192885887" />
                  <Point X="-27.962722745224" Y="-1.270097730761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.416046688802" Y="-1.122803852718" />
                  <Point X="-29.783019849372" Y="-0.678647348627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.837006960129" Y="-3.035432657933" />
                  <Point X="-23.112840694268" Y="-2.945808844825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.900173850238" Y="-2.689988794954" />
                  <Point X="-27.949297038828" Y="-1.374348918507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.634826736371" Y="-1.151606817423" />
                  <Point X="-29.768037977684" Y="-0.783404165128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.766019878776" Y="-3.158386670145" />
                  <Point X="-23.010736791774" Y="-3.078873325111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.998754503009" Y="-2.757846910504" />
                  <Point X="-27.970957123449" Y="-1.467200041695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.853606806759" Y="-1.180409774713" />
                  <Point X="-29.753056105996" Y="-0.888160981628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.695032663588" Y="-3.281340725843" />
                  <Point X="-22.90863288928" Y="-3.211937805396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.085134438421" Y="-2.829669279431" />
                  <Point X="-28.023947762934" Y="-1.549871250513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.072386891073" Y="-1.209212727479" />
                  <Point X="-29.735768725363" Y="-0.993666903395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.62404462662" Y="-3.404295048554" />
                  <Point X="-22.806528986786" Y="-3.345002285681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.165026661045" Y="-2.903599634028" />
                  <Point X="-28.110401076922" Y="-1.621669777296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.291166975387" Y="-1.238015680244" />
                  <Point X="-29.707943707721" Y="-1.102596710977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.553056589651" Y="-3.527249371264" />
                  <Point X="-22.704425084292" Y="-3.478066765966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.211701271098" Y="-2.98832304521" />
                  <Point X="-28.201853666698" Y="-1.691843940909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.509947059701" Y="-1.26681863301" />
                  <Point X="-29.68011877387" Y="-1.211526491335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.482068552683" Y="-3.650203693975" />
                  <Point X="-22.602321181798" Y="-3.611131246251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.233608672439" Y="-3.081093810324" />
                  <Point X="-28.293306256474" Y="-1.762018104522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.411080515715" Y="-3.773158016685" />
                  <Point X="-22.500217279304" Y="-3.744195726537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.253887332993" Y="-3.174393785399" />
                  <Point X="-28.38475884625" Y="-1.832192268135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.340092478746" Y="-3.896112339396" />
                  <Point X="-22.398113404849" Y="-3.877260197712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.273094864444" Y="-3.268041791417" />
                  <Point X="-24.540691439853" Y="-3.181094393423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.071967265996" Y="-3.008472413376" />
                  <Point X="-28.476211436026" Y="-1.902366431747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.269104441778" Y="-4.019066662106" />
                  <Point X="-22.296009571678" Y="-4.010324655472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.270048020231" Y="-3.368920682416" />
                  <Point X="-24.451175376688" Y="-3.310068836777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.229202404552" Y="-3.057272531222" />
                  <Point X="-28.567664025802" Y="-1.97254059536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.256309471681" Y="-3.47327351874" />
                  <Point X="-24.366098114749" Y="-3.437601026185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.370980579132" Y="-3.111094921108" />
                  <Point X="-27.35514755084" Y="-2.466399991385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.659743850214" Y="-2.367430654319" />
                  <Point X="-28.659116615578" Y="-2.042714758973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.242570923131" Y="-3.577626355065" />
                  <Point X="-24.328369248281" Y="-3.54974878932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.449935198916" Y="-3.185329921334" />
                  <Point X="-27.311663957584" Y="-2.5804175786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.77495037488" Y="-2.429886696623" />
                  <Point X="-28.750569205354" Y="-2.112888922586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.228832374581" Y="-3.681979191389" />
                  <Point X="-24.299051565853" Y="-3.659163593091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.506506174778" Y="-3.266837808344" />
                  <Point X="-27.322692855136" Y="-2.67672298386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.885658945958" Y="-2.49380421264" />
                  <Point X="-28.84202179513" Y="-2.183063086199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.21509382603" Y="-3.786332027713" />
                  <Point X="-24.269733883426" Y="-3.768578396862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.563077091011" Y="-3.348345714729" />
                  <Point X="-27.364040057875" Y="-2.763177374608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.996367517036" Y="-2.557721728658" />
                  <Point X="-28.933474384907" Y="-2.253237249812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.201355386949" Y="-3.890684828469" />
                  <Point X="-24.240416200999" Y="-3.877993200633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.617984547769" Y="-3.430394111861" />
                  <Point X="-27.412601171685" Y="-2.847287823563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.107076021504" Y="-2.621639266319" />
                  <Point X="-29.024927196341" Y="-2.323411341404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.187617002056" Y="-3.995037611618" />
                  <Point X="-24.211098518572" Y="-3.987408004404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.6512079512" Y="-3.519488085012" />
                  <Point X="-27.461162285495" Y="-2.931398272518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.217784430104" Y="-2.685556835129" />
                  <Point X="-29.11638016781" Y="-2.393585380997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.173878617162" Y="-4.099390394767" />
                  <Point X="-24.181780836144" Y="-4.096822808175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.675829397214" Y="-3.611377003555" />
                  <Point X="-27.509723399305" Y="-3.015508721473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.328492838703" Y="-2.749474403939" />
                  <Point X="-29.159801150718" Y="-2.479365959723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.700450843228" Y="-3.703265922098" />
                  <Point X="-27.558284513115" Y="-3.099619170428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.439201247303" Y="-2.813391972749" />
                  <Point X="-29.085914300278" Y="-2.603262164026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.725072289242" Y="-3.795154840641" />
                  <Point X="-27.606845626925" Y="-3.183729619382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.549909655903" Y="-2.877309541559" />
                  <Point X="-29.012027449839" Y="-2.727158368329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.749693735257" Y="-3.887043759184" />
                  <Point X="-27.655406740735" Y="-3.267840068337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.660618064502" Y="-2.941227110369" />
                  <Point X="-28.913954484824" Y="-2.858913117633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.774315394535" Y="-3.978932608433" />
                  <Point X="-27.703967854545" Y="-3.351950517292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.771326473102" Y="-3.005144679179" />
                  <Point X="-28.812940092209" Y="-2.991623594699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.798937189274" Y="-4.070821413669" />
                  <Point X="-26.523987073894" Y="-3.835238425404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.646101796221" Y="-3.79556094692" />
                  <Point X="-27.752528968355" Y="-3.436060966247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.823558984013" Y="-4.162710218904" />
                  <Point X="-26.332020213695" Y="-3.997501150609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.999945169948" Y="-3.780479176717" />
                  <Point X="-27.801090082164" Y="-3.520171415201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.848180778752" Y="-4.254599024139" />
                  <Point X="-26.204001235806" Y="-4.13898594932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.129198941159" Y="-3.838370991941" />
                  <Point X="-27.849651195974" Y="-3.604281864156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.87280257349" Y="-4.346487829375" />
                  <Point X="-26.172799640414" Y="-4.249012873519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.229781856485" Y="-3.90557853295" />
                  <Point X="-27.898212448152" Y="-3.688392268153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.897424368229" Y="-4.43837663461" />
                  <Point X="-26.151556845111" Y="-4.355803987419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.330364181617" Y="-3.972786265724" />
                  <Point X="-27.946773795284" Y="-3.772502641296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.922046162968" Y="-4.530265439846" />
                  <Point X="-26.13031475183" Y="-4.462594873218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.413550634449" Y="-4.045646260042" />
                  <Point X="-27.913975907741" Y="-3.883048232257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.946667957707" Y="-4.622154245081" />
                  <Point X="-26.12002359095" Y="-4.565827585387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.474903419074" Y="-4.125600443201" />
                  <Point X="-27.659780085899" Y="-4.065530372776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.971289752446" Y="-4.714043050316" />
                  <Point X="-26.132091069856" Y="-4.66179553511" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.2443828125" Y="-4.597301269531" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521543945312" />
                  <Point X="-24.6517890625" Y="-3.354252441406" />
                  <Point X="-24.699408203125" Y="-3.285643554688" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.9053359375" Y="-3.210635986328" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.1883359375" Y="-3.243530273438" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285644287109" />
                  <Point X="-25.404388671875" Y="-3.452935546875" />
                  <Point X="-25.452005859375" Y="-3.521544677734" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.57573046875" Y="-3.971912841797" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-26.01454296875" Y="-4.954700195312" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.308482421875" Y="-4.884487304688" />
                  <Point X="-26.351587890625" Y="-4.873396484375" />
                  <Point X="-26.343345703125" Y="-4.810783691406" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.534756835938" />
                  <Point X="-26.35260546875" Y="-4.31896484375" />
                  <Point X="-26.3702109375" Y="-4.230465332031" />
                  <Point X="-26.386283203125" Y="-4.202628417969" />
                  <Point X="-26.551701171875" Y="-4.057559082031" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.8687890625" Y="-3.971372802734" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791259766" />
                  <Point X="-27.172818359375" Y="-4.096027832031" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.325142578125" Y="-4.242536132812" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.73021484375" Y="-4.245391113281" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.144181640625" Y="-3.945592529297" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-28.03395703125" Y="-3.543511962891" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597595214844" />
                  <Point X="-27.5139765625" Y="-2.568766845703" />
                  <Point X="-27.53132421875" Y="-2.551418212891" />
                  <Point X="-27.560154296875" Y="-2.537198974609" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-27.966689453125" Y="-2.759980224609" />
                  <Point X="-28.84295703125" Y="-3.265893554688" />
                  <Point X="-29.06245703125" Y="-2.977517822266" />
                  <Point X="-29.161703125" Y="-2.847126464844" />
                  <Point X="-29.36842578125" Y="-2.500485839844" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-29.087482421875" Y="-2.131922363281" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.39601574707" />
                  <Point X="-28.140357421875" Y="-1.374920043945" />
                  <Point X="-28.138115234375" Y="-1.366263427734" />
                  <Point X="-28.14032421875" Y="-1.334595703125" />
                  <Point X="-28.16115625" Y="-1.31064074707" />
                  <Point X="-28.1799375" Y="-1.299587402344" />
                  <Point X="-28.187638671875" Y="-1.295054199219" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.6929375" Y="-1.350896850586" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.888576171875" Y="-1.163154541016" />
                  <Point X="-29.927392578125" Y="-1.011188476563" />
                  <Point X="-29.9820859375" Y="-0.628777526855" />
                  <Point X="-29.99839453125" Y="-0.514742248535" />
                  <Point X="-29.60969140625" Y="-0.410589538574" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.541896484375" Y="-0.121426116943" />
                  <Point X="-28.52221484375" Y="-0.107766227722" />
                  <Point X="-28.514142578125" Y="-0.102164085388" />
                  <Point X="-28.4948984375" Y="-0.075907905579" />
                  <Point X="-28.488337890625" Y="-0.054769985199" />
                  <Point X="-28.485646484375" Y="-0.046095401764" />
                  <Point X="-28.4856484375" Y="-0.016459047318" />
                  <Point X="-28.49220703125" Y="0.004673182964" />
                  <Point X="-28.494896484375" Y="0.013342202187" />
                  <Point X="-28.514142578125" Y="0.039604011536" />
                  <Point X="-28.53382421875" Y="0.053263896942" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-28.988998046875" Y="0.181715133667" />
                  <Point X="-29.998185546875" Y="0.452126037598" />
                  <Point X="-29.94266015625" Y="0.82736517334" />
                  <Point X="-29.91764453125" Y="0.996415161133" />
                  <Point X="-29.807546875" Y="1.402709472656" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.507529296875" Y="1.493281005859" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.688142578125" Y="1.409601196289" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.651533203125" Y="1.426056518555" />
                  <Point X="-28.6391171875" Y="1.443786987305" />
                  <Point X="-28.621638671875" Y="1.485985107422" />
                  <Point X="-28.614470703125" Y="1.503291137695" />
                  <Point X="-28.61071484375" Y="1.524606933594" />
                  <Point X="-28.61631640625" Y="1.545513671875" />
                  <Point X="-28.63740625" Y="1.586027954102" />
                  <Point X="-28.646056640625" Y="1.602643432617" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-28.907498046875" Y="1.809158447266" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.25721484375" Y="2.620476318359" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-28.86837109375" Y="3.161872070312" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.622404296875" Y="3.194399658203" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.077845703125" Y="2.915126953125" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927405273438" />
                  <Point X="-27.9701875" Y="2.970468017578" />
                  <Point X="-27.95252734375" Y="2.988128662109" />
                  <Point X="-27.9408984375" Y="3.006381835938" />
                  <Point X="-27.938072265625" Y="3.027839111328" />
                  <Point X="-27.943380859375" Y="3.088507568359" />
                  <Point X="-27.945556640625" Y="3.113388671875" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.061755859375" Y="3.324018310547" />
                  <Point X="-28.30727734375" Y="3.749276855469" />
                  <Point X="-27.92216796875" Y="4.044536132813" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.293546875" Y="4.429524414063" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.109640625" Y="4.472389160156" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.88372265625" Y="4.238509765625" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.22225" />
                  <Point X="-26.7434765625" Y="4.251382324219" />
                  <Point X="-26.7146328125" Y="4.263329589844" />
                  <Point X="-26.696904296875" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.294487792969" />
                  <Point X="-26.66319140625" Y="4.367089355469" />
                  <Point X="-26.653802734375" Y="4.396864257812" />
                  <Point X="-26.651916015625" Y="4.418426757812" />
                  <Point X="-26.66438671875" Y="4.5131484375" />
                  <Point X="-26.68913671875" Y="4.701141113281" />
                  <Point X="-26.18725390625" Y="4.8418515625" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.41124609375" Y="4.968466796875" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.179181640625" Y="4.822349609375" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.88934375" Y="4.520655761719" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.331154296875" Y="4.945606445312" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.679099609375" Y="4.814340332031" />
                  <Point X="-23.491544921875" Y="4.76905859375" />
                  <Point X="-23.191134765625" Y="4.660097167969" />
                  <Point X="-23.06896484375" Y="4.615785644531" />
                  <Point X="-22.779009765625" Y="4.480183105469" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.38119140625" Y="4.261942871094" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-22.003294921875" Y="4.007821289062" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.159111328125" Y="3.561938476562" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491514648438" />
                  <Point X="-22.790373046875" Y="2.434578613281" />
                  <Point X="-22.7966171875" Y="2.411228271484" />
                  <Point X="-22.797955078125" Y="2.392328613281" />
                  <Point X="-22.792017578125" Y="2.343095214844" />
                  <Point X="-22.789583984375" Y="2.322903808594" />
                  <Point X="-22.78131640625" Y="2.300811767578" />
                  <Point X="-22.7508515625" Y="2.255915771484" />
                  <Point X="-22.738357421875" Y="2.237503173828" />
                  <Point X="-22.725056640625" Y="2.224202636719" />
                  <Point X="-22.680162109375" Y="2.193738769531" />
                  <Point X="-22.661748046875" Y="2.181245117188" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.5904296875" Y="2.167043457031" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5513359375" Y="2.165946044922" />
                  <Point X="-22.494400390625" Y="2.181171630859" />
                  <Point X="-22.471048828125" Y="2.187415771484" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-22.024921875" Y="2.443011962891" />
                  <Point X="-21.005751953125" Y="3.031430419922" />
                  <Point X="-20.864861328125" Y="2.835623779297" />
                  <Point X="-20.79740234375" Y="2.741873046875" />
                  <Point X="-20.650130859375" Y="2.498504882812" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-20.906900390625" Y="2.210383789062" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583832519531" />
                  <Point X="-21.76160546875" Y="1.530375" />
                  <Point X="-21.77841015625" Y="1.508451171875" />
                  <Point X="-21.78687890625" Y="1.491503540039" />
                  <Point X="-21.802142578125" Y="1.436923217773" />
                  <Point X="-21.808404296875" Y="1.4145390625" />
                  <Point X="-21.809220703125" Y="1.390965698242" />
                  <Point X="-21.796689453125" Y="1.330238037109" />
                  <Point X="-21.79155078125" Y="1.305332763672" />
                  <Point X="-21.784353515625" Y="1.287955566406" />
                  <Point X="-21.7502734375" Y="1.236154541016" />
                  <Point X="-21.736296875" Y="1.21491003418" />
                  <Point X="-21.719052734375" Y="1.19882019043" />
                  <Point X="-21.6696640625" Y="1.171019165039" />
                  <Point X="-21.64941015625" Y="1.159617797852" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.564658203125" Y="1.144794433594" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-21.111884765625" Y="1.195453613281" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.08978125" Y="1.070387817383" />
                  <Point X="-20.06080859375" Y="0.951367797852" />
                  <Point X="-20.0143984375" Y="0.653295471191" />
                  <Point X="-20.002140625" Y="0.57455645752" />
                  <Point X="-20.336876953125" Y="0.484863677979" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819656372" />
                  <Point X="-21.336515625" Y="0.194899047852" />
                  <Point X="-21.363421875" Y="0.179347290039" />
                  <Point X="-21.377734375" Y="0.16692578125" />
                  <Point X="-21.41709765625" Y="0.116768486023" />
                  <Point X="-21.433240234375" Y="0.096198104858" />
                  <Point X="-21.443013671875" Y="0.074734802246" />
                  <Point X="-21.456134765625" Y="0.006222248077" />
                  <Point X="-21.461515625" Y="-0.02187575531" />
                  <Point X="-21.461515625" Y="-0.040684322357" />
                  <Point X="-21.44839453125" Y="-0.109196723938" />
                  <Point X="-21.443013671875" Y="-0.137294876099" />
                  <Point X="-21.433240234375" Y="-0.158758178711" />
                  <Point X="-21.393876953125" Y="-0.208915481567" />
                  <Point X="-21.377734375" Y="-0.229485855103" />
                  <Point X="-21.363423828125" Y="-0.241906448364" />
                  <Point X="-21.297818359375" Y="-0.279827056885" />
                  <Point X="-21.2709140625" Y="-0.295378967285" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.88072265625" Y="-0.401700775146" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.035390625" Y="-0.859108154297" />
                  <Point X="-20.051568359375" Y="-0.966414672852" />
                  <Point X="-20.11102734375" Y="-1.226962646484" />
                  <Point X="-20.125453125" Y="-1.290178588867" />
                  <Point X="-20.518810546875" Y="-1.238392089844" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.733919921875" Y="-1.126327392578" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.814552734375" Y="-1.154697143555" />
                  <Point X="-21.89237890625" Y="-1.248297973633" />
                  <Point X="-21.924296875" Y="-1.286685180664" />
                  <Point X="-21.935640625" Y="-1.314070800781" />
                  <Point X="-21.946794921875" Y="-1.435288085938" />
                  <Point X="-21.951369140625" Y="-1.485001220703" />
                  <Point X="-21.94363671875" Y="-1.516622436523" />
                  <Point X="-21.872380859375" Y="-1.627457763672" />
                  <Point X="-21.84315625" Y="-1.672913085938" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-21.480650390625" Y="-1.954785644531" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.75026953125" Y="-2.728357177734" />
                  <Point X="-20.795865234375" Y="-2.802136962891" />
                  <Point X="-20.918833984375" Y="-2.976860107422" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.294271484375" Y="-2.809011230469" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.415900390625" Y="-2.225637207031" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.638228515625" Y="-2.286245849609" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.334683349609" />
                  <Point X="-22.778400390625" Y="-2.461990722656" />
                  <Point X="-22.80587890625" Y="-2.514201416016" />
                  <Point X="-22.8108359375" Y="-2.546375488281" />
                  <Point X="-22.78316015625" Y="-2.699618408203" />
                  <Point X="-22.77180859375" Y="-2.762465820312" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.540427734375" Y="-3.169121337891" />
                  <Point X="-22.01332421875" Y="-4.082088867188" />
                  <Point X="-22.110591796875" Y="-4.151564453125" />
                  <Point X="-22.164685546875" Y="-4.190202148438" />
                  <Point X="-22.3021875" Y="-4.279204101562" />
                  <Point X="-22.320224609375" Y="-4.29087890625" />
                  <Point X="-22.5901484375" Y="-3.939105224609" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467529297" />
                  <Point X="-23.48058984375" Y="-2.883299072266" />
                  <Point X="-23.54257421875" Y="-2.843448730469" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.739490234375" Y="-2.850927734375" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868508789062" />
                  <Point X="-23.962302734375" Y="-2.974635009766" />
                  <Point X="-24.014650390625" Y="-3.018159179688" />
                  <Point X="-24.03154296875" Y="-3.045985595703" />
                  <Point X="-24.069705078125" Y="-3.221565429688" />
                  <Point X="-24.085357421875" Y="-3.293573486328" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-24.022169921875" Y="-3.796083496094" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.95446484375" Y="-4.952026855469" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.132662109375" Y="-4.986319824219" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#188" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.137041589218" Y="4.866570673383" Z="1.8" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.8" />
                  <Point X="-0.4232162988" Y="5.049408199373" Z="1.8" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.8" />
                  <Point X="-1.206908977239" Y="4.921274931679" Z="1.8" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.8" />
                  <Point X="-1.720116201252" Y="4.53790205109" Z="1.8" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.8" />
                  <Point X="-1.717033944948" Y="4.41340553733" Z="1.8" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.8" />
                  <Point X="-1.76876874257" Y="4.328856590757" Z="1.8" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.8" />
                  <Point X="-1.866791580138" Y="4.314140500414" Z="1.8" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.8" />
                  <Point X="-2.076129432762" Y="4.53410731045" Z="1.8" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.8" />
                  <Point X="-2.323986649474" Y="4.504511880731" Z="1.8" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.8" />
                  <Point X="-2.958747626355" Y="4.11549574825" Z="1.8" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.8" />
                  <Point X="-3.111212876491" Y="3.330298416608" Z="1.8" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.8" />
                  <Point X="-2.999347854783" Y="3.115431857963" Z="1.8" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.8" />
                  <Point X="-3.011700760943" Y="3.037102806943" Z="1.8" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.8" />
                  <Point X="-3.079644607643" Y="2.99621684439" Z="1.8" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.8" />
                  <Point X="-3.603560931689" Y="3.268981072254" Z="1.8" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.8" />
                  <Point X="-3.913991130678" Y="3.223854573885" Z="1.8" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.8" />
                  <Point X="-4.306554375152" Y="2.676961467068" Z="1.8" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.8" />
                  <Point X="-3.944092631926" Y="1.800770811944" Z="1.8" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.8" />
                  <Point X="-3.687912792731" Y="1.594218589494" Z="1.8" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.8" />
                  <Point X="-3.673990869698" Y="1.536398243911" Z="1.8" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.8" />
                  <Point X="-3.709335005212" Y="1.488567272391" Z="1.8" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.8" />
                  <Point X="-4.507160041094" Y="1.574133300366" Z="1.8" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.8" />
                  <Point X="-4.861964107776" Y="1.447066511932" Z="1.8" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.8" />
                  <Point X="-4.998278065728" Y="0.865964161236" Z="1.8" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.8" />
                  <Point X="-4.008097212678" Y="0.164698884362" Z="1.8" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.8" />
                  <Point X="-3.568489128795" Y="0.043466875937" Z="1.8" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.8" />
                  <Point X="-3.546117277557" Y="0.021137939978" Z="1.8" />
                  <Point X="-3.539556741714" Y="0" Z="1.8" />
                  <Point X="-3.542247323578" Y="-0.008669011083" Z="1.8" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.8" />
                  <Point X="-3.556879466328" Y="-0.035409107197" Z="1.8" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.8" />
                  <Point X="-4.628790576095" Y="-0.331013172394" Z="1.8" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.8" />
                  <Point X="-5.037738979693" Y="-0.604576451236" Z="1.8" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.8" />
                  <Point X="-4.943177944904" Y="-1.144248336485" Z="1.8" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.8" />
                  <Point X="-3.692569755343" Y="-1.369189019086" Z="1.8" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.8" />
                  <Point X="-3.211456731504" Y="-1.311396462018" Z="1.8" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.8" />
                  <Point X="-3.194917070497" Y="-1.331101485065" Z="1.8" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.8" />
                  <Point X="-4.124077602256" Y="-2.060974623766" Z="1.8" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.8" />
                  <Point X="-4.417526181541" Y="-2.49481559014" Z="1.8" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.8" />
                  <Point X="-4.108400157592" Y="-2.976520808238" Z="1.8" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.8" />
                  <Point X="-2.947846574567" Y="-2.772001403908" Z="1.8" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.8" />
                  <Point X="-2.567793877436" Y="-2.560536591695" Z="1.8" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.8" />
                  <Point X="-3.08341560926" Y="-3.487231807926" Z="1.8" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.8" />
                  <Point X="-3.1808420348" Y="-3.953929603621" Z="1.8" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.8" />
                  <Point X="-2.762693328728" Y="-4.256621709849" Z="1.8" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.8" />
                  <Point X="-2.291630556161" Y="-4.241693878563" Z="1.8" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.8" />
                  <Point X="-2.151195732184" Y="-4.10632097088" Z="1.8" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.8" />
                  <Point X="-1.878215531108" Y="-3.989985938079" Z="1.8" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.8" />
                  <Point X="-1.5908255908" Y="-4.06387120243" Z="1.8" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.8" />
                  <Point X="-1.407802263808" Y="-4.297440276808" Z="1.8" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.8" />
                  <Point X="-1.399074664723" Y="-4.772977509984" Z="1.8" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.8" />
                  <Point X="-1.327098895566" Y="-4.90163034241" Z="1.8" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.8" />
                  <Point X="-1.030227700143" Y="-4.972504379755" Z="1.8" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.8" />
                  <Point X="-0.533591273422" Y="-3.95357379849" Z="1.8" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.8" />
                  <Point X="-0.369468449394" Y="-3.450164142838" Z="1.8" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.8" />
                  <Point X="-0.179672486508" Y="-3.260003077717" Z="1.8" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.8" />
                  <Point X="0.073686592853" Y="-3.227108967571" Z="1.8" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.8" />
                  <Point X="0.300977410038" Y="-3.351481612929" Z="1.8" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.8" />
                  <Point X="0.701163586979" Y="-4.578962231325" Z="1.8" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.8" />
                  <Point X="0.870118512834" Y="-5.004234621558" Z="1.8" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.8" />
                  <Point X="1.050082715524" Y="-4.96958710257" Z="1.8" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.8" />
                  <Point X="1.021245109404" Y="-3.758277202398" Z="1.8" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.8" />
                  <Point X="0.972997038202" Y="-3.20090564684" Z="1.8" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.8" />
                  <Point X="1.063506228561" Y="-2.981801854397" Z="1.8" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.8" />
                  <Point X="1.258934355647" Y="-2.869437286745" Z="1.8" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.8" />
                  <Point X="1.486214973525" Y="-2.894076906603" Z="1.8" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.8" />
                  <Point X="2.364026487802" Y="-3.938263234504" Z="1.8" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.8" />
                  <Point X="2.718826185981" Y="-4.289898333562" Z="1.8" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.8" />
                  <Point X="2.912311586482" Y="-4.160971636765" Z="1.8" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.8" />
                  <Point X="2.496716976143" Y="-3.112841412697" Z="1.8" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.8" />
                  <Point X="2.259886693358" Y="-2.659451186913" Z="1.8" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.8" />
                  <Point X="2.259689274858" Y="-2.453997427147" Z="1.8" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.8" />
                  <Point X="2.378900951225" Y="-2.299212035109" Z="1.8" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.8" />
                  <Point X="2.569055616599" Y="-2.243561316133" Z="1.8" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.8" />
                  <Point X="3.67457218441" Y="-2.821032524132" Z="1.8" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.8" />
                  <Point X="4.115897506061" Y="-2.974357621963" Z="1.8" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.8" />
                  <Point X="4.286107017838" Y="-2.723362173901" Z="1.8" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.8" />
                  <Point X="3.543629592096" Y="-1.883837951537" Z="1.8" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.8" />
                  <Point X="3.16351927233" Y="-1.569137714118" Z="1.8" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.8" />
                  <Point X="3.096837091988" Y="-1.408589383272" Z="1.8" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.8" />
                  <Point X="3.139909266164" Y="-1.248985015917" Z="1.8" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.8" />
                  <Point X="3.270541439722" Y="-1.14390659977" Z="1.8" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.8" />
                  <Point X="4.468507424379" Y="-1.256684260125" Z="1.8" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.8" />
                  <Point X="4.931562720583" Y="-1.20680613053" Z="1.8" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.8" />
                  <Point X="5.007893083845" Y="-0.835282266863" Z="1.8" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.8" />
                  <Point X="4.126060996863" Y="-0.322124445112" Z="1.8" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.8" />
                  <Point X="3.721047227734" Y="-0.205258791835" Z="1.8" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.8" />
                  <Point X="3.639299431535" Y="-0.146767871028" Z="1.8" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.8" />
                  <Point X="3.594555629324" Y="-0.068512462281" Z="1.8" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.8" />
                  <Point X="3.586815821101" Y="0.028098068913" Z="1.8" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.8" />
                  <Point X="3.616080006866" Y="0.117180867547" Z="1.8" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.8" />
                  <Point X="3.682348186619" Y="0.182890052407" Z="1.8" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.8" />
                  <Point X="4.669906820444" Y="0.467847487656" Z="1.8" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.8" />
                  <Point X="5.02884820627" Y="0.692267238837" Z="1.8" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.8" />
                  <Point X="4.95264296259" Y="1.113494159906" Z="1.8" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.8" />
                  <Point X="3.875432802418" Y="1.276305923891" Z="1.8" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.8" />
                  <Point X="3.435735822964" Y="1.225643422629" Z="1.8" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.8" />
                  <Point X="3.348706019666" Y="1.245870164367" Z="1.8" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.8" />
                  <Point X="3.28534156101" Y="1.294915442964" Z="1.8" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.8" />
                  <Point X="3.246122180048" Y="1.37162172554" Z="1.8" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.8" />
                  <Point X="3.239852028991" Y="1.454733449587" Z="1.8" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.8" />
                  <Point X="3.2719212706" Y="1.531237520794" Z="1.8" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.8" />
                  <Point X="4.117380612023" Y="2.201996177078" Z="1.8" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.8" />
                  <Point X="4.386489417528" Y="2.555671089719" Z="1.8" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.8" />
                  <Point X="4.169568521056" Y="2.89610731011" Z="1.8" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.8" />
                  <Point X="2.943920531081" Y="2.517593371819" Z="1.8" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.8" />
                  <Point X="2.486527577653" Y="2.260754582069" Z="1.8" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.8" />
                  <Point X="2.409400226366" Y="2.247963887201" Z="1.8" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.8" />
                  <Point X="2.341754171595" Y="2.266394238804" Z="1.8" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.8" />
                  <Point X="2.284364361431" Y="2.315270688787" Z="1.8" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.8" />
                  <Point X="2.251465815558" Y="2.380358217466" Z="1.8" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.8" />
                  <Point X="2.251773360589" Y="2.452941998229" Z="1.8" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.8" />
                  <Point X="2.878031981041" Y="3.568218550874" Z="1.8" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.8" />
                  <Point X="3.019524676182" Y="4.07984841188" Z="1.8" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.8" />
                  <Point X="2.637821124955" Y="4.336425522934" Z="1.8" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.8" />
                  <Point X="2.236015323952" Y="4.556755254883" Z="1.8" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.8" />
                  <Point X="1.819757761149" Y="4.738381354503" Z="1.8" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.8" />
                  <Point X="1.326477118106" Y="4.894223753694" Z="1.8" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.8" />
                  <Point X="0.667896305416" Y="5.026614317936" Z="1.8" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.8" />
                  <Point X="0.056202972335" Y="4.564876832568" Z="1.8" />
                  <Point X="0" Y="4.355124473572" Z="1.8" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>