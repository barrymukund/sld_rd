<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#171" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2079" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000475585938" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.223587890625" Y="-4.307853027344" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140136719" />
                  <Point X="-24.457634765625" Y="-3.467376708984" />
                  <Point X="-24.54473828125" Y="-3.341878417969" />
                  <Point X="-24.62136328125" Y="-3.231476318359" />
                  <Point X="-24.643248046875" Y="-3.209020751953" />
                  <Point X="-24.66950390625" Y="-3.189776855469" />
                  <Point X="-24.69750390625" Y="-3.175669433594" />
                  <Point X="-24.8322890625" Y="-3.133836669922" />
                  <Point X="-24.95086328125" Y="-3.097036132812" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097036132812" />
                  <Point X="-25.171609375" Y="-3.138868652344" />
                  <Point X="-25.29018359375" Y="-3.175669433594" />
                  <Point X="-25.31818359375" Y="-3.189776855469" />
                  <Point X="-25.344439453125" Y="-3.209020751953" />
                  <Point X="-25.36632421875" Y="-3.231477050781" />
                  <Point X="-25.45342578125" Y="-3.356975097656" />
                  <Point X="-25.53005078125" Y="-3.467377441406" />
                  <Point X="-25.5381875" Y="-3.481572021484" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.73846484375" Y="-4.212185546875" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-25.94333984375" Y="-4.871748535156" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.232423828125" Y="-4.805962890625" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.240615234375" Y="-4.75829296875" />
                  <Point X="-26.2149609375" Y="-4.563437988281" />
                  <Point X="-26.21419921875" Y="-4.547929199219" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.248708984375" Y="-4.354341308594" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.182964355469" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.44773828125" Y="-4.022376464844" />
                  <Point X="-26.556904296875" Y="-3.926639404297" />
                  <Point X="-26.5831875" Y="-3.910295898438" />
                  <Point X="-26.612884765625" Y="-3.897994628906" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.807728515625" Y="-3.880171142578" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708496094" />
                  <Point X="-27.014466796875" Y="-3.882028076172" />
                  <Point X="-27.042658203125" Y="-3.894801513672" />
                  <Point X="-27.17989453125" Y="-3.986500488281" />
                  <Point X="-27.300623046875" Y="-4.067169677734" />
                  <Point X="-27.31278515625" Y="-4.076821533203" />
                  <Point X="-27.3351015625" Y="-4.099461425781" />
                  <Point X="-27.440353515625" Y="-4.236628417969" />
                  <Point X="-27.4801484375" Y="-4.288489257813" />
                  <Point X="-27.60236328125" Y="-4.21281640625" />
                  <Point X="-27.80171484375" Y="-4.089383544922" />
                  <Point X="-28.01368359375" Y="-3.926173828125" />
                  <Point X="-28.104720703125" Y="-3.856078369141" />
                  <Point X="-27.819796875" Y="-3.362574951172" />
                  <Point X="-27.423759765625" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647654541016" />
                  <Point X="-27.406587890625" Y="-2.616128173828" />
                  <Point X="-27.40557421875" Y="-2.585194091797" />
                  <Point X="-27.41455859375" Y="-2.555575927734" />
                  <Point X="-27.428775390625" Y="-2.526747070312" />
                  <Point X="-27.44680078125" Y="-2.501591064453" />
                  <Point X="-27.4641484375" Y="-2.484242431641" />
                  <Point X="-27.489310546875" Y="-2.466212402344" />
                  <Point X="-27.518140625" Y="-2.451995361328" />
                  <Point X="-27.5477578125" Y="-2.443011230469" />
                  <Point X="-27.578689453125" Y="-2.444024169922" />
                  <Point X="-27.61021484375" Y="-2.450295166016" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.242626953125" Y="-2.809595703125" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-28.9253671875" Y="-3.000773925781" />
                  <Point X="-29.082857421875" Y="-2.793862548828" />
                  <Point X="-29.234828125" Y="-2.539033691406" />
                  <Point X="-29.306142578125" Y="-2.419449951172" />
                  <Point X="-28.798890625" Y="-2.030221557617" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.084576171875" Y="-1.475591918945" />
                  <Point X="-28.066611328125" Y="-1.448459472656" />
                  <Point X="-28.053857421875" Y="-1.419833129883" />
                  <Point X="-28.04615234375" Y="-1.39008581543" />
                  <Point X="-28.04334765625" Y="-1.359655151367" />
                  <Point X="-28.045556640625" Y="-1.327984619141" />
                  <Point X="-28.05255859375" Y="-1.298239990234" />
                  <Point X="-28.068640625" Y="-1.272256713867" />
                  <Point X="-28.08947265625" Y="-1.248300537109" />
                  <Point X="-28.112970703125" Y="-1.228766601562" />
                  <Point X="-28.139453125" Y="-1.213179931641" />
                  <Point X="-28.16871875" Y="-1.201955688477" />
                  <Point X="-28.20060546875" Y="-1.195474609375" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-28.99371875" Y="-1.294675537109" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.772478515625" Y="-1.2338046875" />
                  <Point X="-29.834078125" Y="-0.992650390625" />
                  <Point X="-29.874283203125" Y="-0.711532958984" />
                  <Point X="-29.892423828125" Y="-0.584698425293" />
                  <Point X="-29.322232421875" Y="-0.43191607666" />
                  <Point X="-28.532875" Y="-0.220408309937" />
                  <Point X="-28.5100546875" Y="-0.210971054077" />
                  <Point X="-28.479720703125" Y="-0.193503158569" />
                  <Point X="-28.466552734375" Y="-0.184359313965" />
                  <Point X="-28.441748046875" Y="-0.163827545166" />
                  <Point X="-28.42564453125" Y="-0.146728149414" />
                  <Point X="-28.414228515625" Y="-0.126200218201" />
                  <Point X="-28.40187890625" Y="-0.095600944519" />
                  <Point X="-28.397361328125" Y="-0.081209854126" />
                  <Point X="-28.390787109375" Y="-0.05244071579" />
                  <Point X="-28.388400390625" Y="-0.031043210983" />
                  <Point X="-28.390892578125" Y="-0.009657684326" />
                  <Point X="-28.398060546875" Y="0.021013080597" />
                  <Point X="-28.402658203125" Y="0.035403873444" />
                  <Point X="-28.41441015625" Y="0.064093467712" />
                  <Point X="-28.4259375" Y="0.084569282532" />
                  <Point X="-28.442138671875" Y="0.10158934021" />
                  <Point X="-28.468724609375" Y="0.123356079102" />
                  <Point X="-28.481953125" Y="0.132436187744" />
                  <Point X="-28.510509765625" Y="0.14867137146" />
                  <Point X="-28.532875" Y="0.157848327637" />
                  <Point X="-29.227283203125" Y="0.343914489746" />
                  <Point X="-29.89181640625" Y="0.521975646973" />
                  <Point X="-29.864185546875" Y="0.708702941895" />
                  <Point X="-29.82448828125" Y="0.976967712402" />
                  <Point X="-29.74355078125" Y="1.275651611328" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-29.33149609375" Y="1.374286010742" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228027344" />
                  <Point X="-28.70313671875" Y="1.305263549805" />
                  <Point X="-28.670458984375" Y="1.315567016602" />
                  <Point X="-28.6417109375" Y="1.324631103516" />
                  <Point X="-28.62277734375" Y="1.332961791992" />
                  <Point X="-28.604033203125" Y="1.343783935547" />
                  <Point X="-28.5873515625" Y="1.35601550293" />
                  <Point X="-28.573712890625" Y="1.371567626953" />
                  <Point X="-28.561298828125" Y="1.389297363281" />
                  <Point X="-28.551349609375" Y="1.407433349609" />
                  <Point X="-28.53823828125" Y="1.439089477539" />
                  <Point X="-28.526703125" Y="1.46693762207" />
                  <Point X="-28.520916015625" Y="1.486788818359" />
                  <Point X="-28.51715625" Y="1.508105224609" />
                  <Point X="-28.515802734375" Y="1.528750854492" />
                  <Point X="-28.518951171875" Y="1.549200195312" />
                  <Point X="-28.5245546875" Y="1.570107421875" />
                  <Point X="-28.532048828125" Y="1.589377441406" />
                  <Point X="-28.547869140625" Y="1.619770019531" />
                  <Point X="-28.5617890625" Y="1.646506835938" />
                  <Point X="-28.57328515625" Y="1.663711669922" />
                  <Point X="-28.587197265625" Y="1.680289916992" />
                  <Point X="-28.60213671875" Y="1.694590087891" />
                  <Point X="-29.00044921875" Y="2.000227661133" />
                  <Point X="-29.351859375" Y="2.269873779297" />
                  <Point X="-29.23540234375" Y="2.469394042969" />
                  <Point X="-29.0811484375" Y="2.733665039062" />
                  <Point X="-28.866759765625" Y="3.009231933594" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.554173828125" Y="3.045310546875" />
                  <Point X="-28.20665625" Y="2.844671142578" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.10128125" Y="2.821814697266" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.04056640625" Y="2.818762939453" />
                  <Point X="-28.019107421875" Y="2.821587890625" />
                  <Point X="-27.999015625" Y="2.826504150391" />
                  <Point X="-27.980462890625" Y="2.83565234375" />
                  <Point X="-27.962208984375" Y="2.847281005859" />
                  <Point X="-27.946076171875" Y="2.860228759766" />
                  <Point X="-27.913771484375" Y="2.892533447266" />
                  <Point X="-27.8853515625" Y="2.920952148438" />
                  <Point X="-27.872404296875" Y="2.937084472656" />
                  <Point X="-27.860775390625" Y="2.955338623047" />
                  <Point X="-27.85162890625" Y="2.973887695312" />
                  <Point X="-27.8467109375" Y="2.993976806641" />
                  <Point X="-27.843884765625" Y="3.015435546875" />
                  <Point X="-27.84343359375" Y="3.03612109375" />
                  <Point X="-27.847416015625" Y="3.081633056641" />
                  <Point X="-27.85091796875" Y="3.121670654297" />
                  <Point X="-27.854955078125" Y="3.141960449219" />
                  <Point X="-27.86146484375" Y="3.162603759766" />
                  <Point X="-27.869794921875" Y="3.181533203125" />
                  <Point X="-28.04630078125" Y="3.487249023438" />
                  <Point X="-28.18333203125" Y="3.724595947266" />
                  <Point X="-27.9692734375" Y="3.888713623047" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.36296484375" Y="4.282280761719" />
                  <Point X="-27.167037109375" Y="4.391133789062" />
                  <Point X="-27.14958203125" Y="4.368385253906" />
                  <Point X="-27.0431953125" Y="4.229740722656" />
                  <Point X="-27.028890625" Y="4.214797851563" />
                  <Point X="-27.012310546875" Y="4.200885742188" />
                  <Point X="-26.99511328125" Y="4.18939453125" />
                  <Point X="-26.944458984375" Y="4.163025390625" />
                  <Point X="-26.899896484375" Y="4.139827636719" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.777451171875" Y="4.134482421875" />
                  <Point X="-26.72469140625" Y="4.156336914062" />
                  <Point X="-26.67827734375" Y="4.175562011719" />
                  <Point X="-26.660142578125" Y="4.18551171875" />
                  <Point X="-26.6424140625" Y="4.19792578125" />
                  <Point X="-26.62686328125" Y="4.211563476563" />
                  <Point X="-26.6146328125" Y="4.228243164063" />
                  <Point X="-26.603810546875" Y="4.246987304688" />
                  <Point X="-26.595478515625" Y="4.265921875" />
                  <Point X="-26.578306640625" Y="4.320385742188" />
                  <Point X="-26.56319921875" Y="4.368298339844" />
                  <Point X="-26.5591640625" Y="4.388583496094" />
                  <Point X="-26.55727734375" Y="4.410145507812" />
                  <Point X="-26.557728515625" Y="4.430826660156" />
                  <Point X="-26.577794921875" Y="4.583248046875" />
                  <Point X="-26.58419921875" Y="4.631897949219" />
                  <Point X="-26.297416015625" Y="4.712302246094" />
                  <Point X="-25.949630859375" Y="4.80980859375" />
                  <Point X="-25.54029296875" Y="4.857715820312" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.236708984375" Y="4.669990234375" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.76334375" Y="4.623839355469" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.459626953125" Y="4.863541503906" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.81729296875" Y="4.749974609375" />
                  <Point X="-23.51897265625" Y="4.677950195312" />
                  <Point X="-23.299228515625" Y="4.598248535156" />
                  <Point X="-23.105359375" Y="4.527930664062" />
                  <Point X="-22.892197265625" Y="4.4282421875" />
                  <Point X="-22.705421875" Y="4.340893066406" />
                  <Point X="-22.499478515625" Y="4.220910644531" />
                  <Point X="-22.3190234375" Y="4.115775878906" />
                  <Point X="-22.124810546875" Y="3.9776640625" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.394033203125" Y="3.345039794922" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857919921875" Y="2.539936767578" />
                  <Point X="-22.866921875" Y="2.516058105469" />
                  <Point X="-22.87834375" Y="2.473346191406" />
                  <Point X="-22.888392578125" Y="2.435771728516" />
                  <Point X="-22.891380859375" Y="2.417936279297" />
                  <Point X="-22.892271484375" Y="2.380953857422" />
                  <Point X="-22.887818359375" Y="2.344020263672" />
                  <Point X="-22.883900390625" Y="2.311529052734" />
                  <Point X="-22.878556640625" Y="2.289600830078" />
                  <Point X="-22.8702890625" Y="2.267511230469" />
                  <Point X="-22.859927734375" Y="2.247469726562" />
                  <Point X="-22.83707421875" Y="2.213789794922" />
                  <Point X="-22.81696875" Y="2.184161132813" />
                  <Point X="-22.805533203125" Y="2.170328125" />
                  <Point X="-22.778400390625" Y="2.145592529297" />
                  <Point X="-22.744720703125" Y="2.122739257812" />
                  <Point X="-22.715091796875" Y="2.102635009766" />
                  <Point X="-22.695046875" Y="2.092272216797" />
                  <Point X="-22.672958984375" Y="2.084006347656" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.6141015625" Y="2.074209960938" />
                  <Point X="-22.581611328125" Y="2.070291992188" />
                  <Point X="-22.563533203125" Y="2.069845703125" />
                  <Point X="-22.526794921875" Y="2.074170898438" />
                  <Point X="-22.48408203125" Y="2.085592773438" />
                  <Point X="-22.4465078125" Y="2.095640625" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.7130234375" Y="2.513390136719" />
                  <Point X="-21.032673828125" Y="2.906190429688" />
                  <Point X="-20.983771484375" Y="2.838227050781" />
                  <Point X="-20.87671875" Y="2.68944921875" />
                  <Point X="-20.768458984375" Y="2.510547363281" />
                  <Point X="-20.73780078125" Y="2.459883544922" />
                  <Point X="-21.1657109375" Y="2.131535644531" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.77857421875" Y="1.660241699219" />
                  <Point X="-21.796025390625" Y="1.641627319336" />
                  <Point X="-21.826765625" Y="1.601524780273" />
                  <Point X="-21.853806640625" Y="1.566245849609" />
                  <Point X="-21.863392578125" Y="1.550911743164" />
                  <Point X="-21.878369140625" Y="1.517088378906" />
                  <Point X="-21.8898203125" Y="1.476143554688" />
                  <Point X="-21.89989453125" Y="1.440123779297" />
                  <Point X="-21.90334765625" Y="1.417824951172" />
                  <Point X="-21.9041640625" Y="1.394253173828" />
                  <Point X="-21.902259765625" Y="1.371766235352" />
                  <Point X="-21.892859375" Y="1.326209838867" />
                  <Point X="-21.88458984375" Y="1.286133544922" />
                  <Point X="-21.8793203125" Y="1.268978271484" />
                  <Point X="-21.863716796875" Y="1.235741210938" />
                  <Point X="-21.838150390625" Y="1.196881347656" />
                  <Point X="-21.81566015625" Y="1.162695800781" />
                  <Point X="-21.801107421875" Y="1.145451416016" />
                  <Point X="-21.78386328125" Y="1.129361083984" />
                  <Point X="-21.76565234375" Y="1.116034423828" />
                  <Point X="-21.7286015625" Y="1.095178833008" />
                  <Point X="-21.696009765625" Y="1.07683215332" />
                  <Point X="-21.679478515625" Y="1.069501708984" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.593787109375" Y="1.052818115234" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.84832421875" Y="1.134332397461" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.2000390625" Y="1.121657348633" />
                  <Point X="-20.15405859375" Y="0.932787841797" />
                  <Point X="-20.119943359375" Y="0.713670898438" />
                  <Point X="-20.1091328125" Y="0.644238952637" />
                  <Point X="-20.591791015625" Y="0.514911071777" />
                  <Point X="-21.283419921875" Y="0.329589874268" />
                  <Point X="-21.2952109375" Y="0.325585662842" />
                  <Point X="-21.318453125" Y="0.315067932129" />
                  <Point X="-21.36766796875" Y="0.286620758057" />
                  <Point X="-21.410962890625" Y="0.261595367432" />
                  <Point X="-21.4256875" Y="0.251096496582" />
                  <Point X="-21.45246875" Y="0.22557673645" />
                  <Point X="-21.481998046875" Y="0.187949874878" />
                  <Point X="-21.507974609375" Y="0.154848937988" />
                  <Point X="-21.51969921875" Y="0.135566558838" />
                  <Point X="-21.52947265625" Y="0.114102333069" />
                  <Point X="-21.536318359375" Y="0.092604560852" />
                  <Point X="-21.546162109375" Y="0.041208034515" />
                  <Point X="-21.5548203125" Y="-0.004006072998" />
                  <Point X="-21.556515625" Y="-0.021876203537" />
                  <Point X="-21.5548203125" Y="-0.058554073334" />
                  <Point X="-21.5449765625" Y="-0.109950454712" />
                  <Point X="-21.536318359375" Y="-0.155164550781" />
                  <Point X="-21.52947265625" Y="-0.176662322998" />
                  <Point X="-21.51969921875" Y="-0.198126556396" />
                  <Point X="-21.507974609375" Y="-0.217409088135" />
                  <Point X="-21.4784453125" Y="-0.255035949707" />
                  <Point X="-21.45246875" Y="-0.28813671875" />
                  <Point X="-21.44" Y="-0.301235931396" />
                  <Point X="-21.410962890625" Y="-0.324155517578" />
                  <Point X="-21.361748046875" Y="-0.35260256958" />
                  <Point X="-21.318453125" Y="-0.377627929688" />
                  <Point X="-21.307291015625" Y="-0.383138244629" />
                  <Point X="-21.283419921875" Y="-0.392149871826" />
                  <Point X="-20.674986328125" Y="-0.555179443359" />
                  <Point X="-20.10852734375" Y="-0.706961608887" />
                  <Point X="-20.119306640625" Y="-0.77846295166" />
                  <Point X="-20.14498046875" Y="-0.948748352051" />
                  <Point X="-20.1886875" Y="-1.140276733398" />
                  <Point X="-20.19882421875" Y="-1.18469909668" />
                  <Point X="-20.7735234375" Y="-1.109038574219" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710144043" />
                  <Point X="-21.62533984375" Y="-1.005508728027" />
                  <Point X="-21.721931640625" Y="-1.026503295898" />
                  <Point X="-21.806904296875" Y="-1.044972412109" />
                  <Point X="-21.836025390625" Y="-1.056596923828" />
                  <Point X="-21.8638515625" Y="-1.073489135742" />
                  <Point X="-21.8876015625" Y="-1.093960327148" />
                  <Point X="-21.945984375" Y="-1.164177490234" />
                  <Point X="-21.997345703125" Y="-1.225948242188" />
                  <Point X="-22.012064453125" Y="-1.250329345703" />
                  <Point X="-22.023408203125" Y="-1.27771484375" />
                  <Point X="-22.030240234375" Y="-1.305366088867" />
                  <Point X="-22.038607421875" Y="-1.396300415039" />
                  <Point X="-22.04596875" Y="-1.476296386719" />
                  <Point X="-22.04365234375" Y="-1.507561401367" />
                  <Point X="-22.035921875" Y="-1.539182373047" />
                  <Point X="-22.023548828125" Y="-1.567996337891" />
                  <Point X="-21.97009375" Y="-1.651142822266" />
                  <Point X="-21.923068359375" Y="-1.724287109375" />
                  <Point X="-21.9130625" Y="-1.737242431641" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.32473828125" Y="-2.194165771484" />
                  <Point X="-20.786876953125" Y="-2.606882568359" />
                  <Point X="-20.802828125" Y="-2.632694091797" />
                  <Point X="-20.875201171875" Y="-2.749801513672" />
                  <Point X="-20.965587890625" Y="-2.878230224609" />
                  <Point X="-20.971017578125" Y="-2.8859453125" />
                  <Point X="-21.484671875" Y="-2.58938671875" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.245775390625" Y="-2.159825195312" />
                  <Point X="-22.360734375" Y="-2.139063720703" />
                  <Point X="-22.461865234375" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395507812" />
                  <Point X="-22.525390625" Y="-2.125353515625" />
                  <Point X="-22.555166015625" Y="-2.135177001953" />
                  <Point X="-22.650669921875" Y="-2.185439453125" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.757615234375" Y="-2.246549804688" />
                  <Point X="-22.77857421875" Y="-2.267509277344" />
                  <Point X="-22.795466796875" Y="-2.290438476562" />
                  <Point X="-22.84573046875" Y="-2.38594140625" />
                  <Point X="-22.889947265625" Y="-2.469956542969" />
                  <Point X="-22.899771484375" Y="-2.499735595703" />
                  <Point X="-22.904728515625" Y="-2.531909179688" />
                  <Point X="-22.90432421875" Y="-2.563259277344" />
                  <Point X="-22.8835625" Y="-2.678218505859" />
                  <Point X="-22.865296875" Y="-2.779349609375" />
                  <Point X="-22.86101171875" Y="-2.795141601562" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.485345703125" Y="-3.454521972656" />
                  <Point X="-22.13871484375" Y="-4.054903808594" />
                  <Point X="-22.218166015625" Y="-4.111655273438" />
                  <Point X="-22.298232421875" Y="-4.163480957031" />
                  <Point X="-22.6963203125" Y="-3.644685302734" />
                  <Point X="-23.241451171875" Y="-2.934254882812" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.391455078125" Y="-2.827663574219" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.706900390625" Y="-2.75252734375" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-23.99115234375" Y="-2.875074707031" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968861083984" />
                  <Point X="-24.11275" Y="-2.996687744141" />
                  <Point X="-24.124375" Y="-3.02580859375" />
                  <Point X="-24.15300390625" Y="-3.157524414063" />
                  <Point X="-24.178189453125" Y="-3.273396484375" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323120361328" />
                  <Point X="-24.07743359375" Y="-4.10414453125" />
                  <Point X="-23.97793359375" Y="-4.859915527344" />
                  <Point X="-24.024310546875" Y="-4.870081054688" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.05842578125" Y="-4.752636230469" />
                  <Point X="-26.14124609375" Y="-4.731328125" />
                  <Point X="-26.1207734375" Y="-4.575835449219" />
                  <Point X="-26.120076171875" Y="-4.568098144531" />
                  <Point X="-26.11944921875" Y="-4.541030273438" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497688964844" />
                  <Point X="-26.15553515625" Y="-4.335807128906" />
                  <Point X="-26.18386328125" Y="-4.193397460938" />
                  <Point X="-26.188125" Y="-4.178468261719" />
                  <Point X="-26.19902734375" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135462890625" />
                  <Point X="-26.221740234375" Y="-4.107625976562" />
                  <Point X="-26.23057421875" Y="-4.094861083984" />
                  <Point X="-26.25020703125" Y="-4.070937744141" />
                  <Point X="-26.261005859375" Y="-4.059779296875" />
                  <Point X="-26.385099609375" Y="-3.950951660156" />
                  <Point X="-26.494265625" Y="-3.855214599609" />
                  <Point X="-26.50673828125" Y="-3.845964599609" />
                  <Point X="-26.533021484375" Y="-3.82962109375" />
                  <Point X="-26.54683203125" Y="-3.822527587891" />
                  <Point X="-26.576529296875" Y="-3.810226318359" />
                  <Point X="-26.5913125" Y="-3.805476318359" />
                  <Point X="-26.621455078125" Y="-3.798447998047" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.801515625" Y="-3.785374511719" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166015625" />
                  <Point X="-27.008005859375" Y="-3.781945556641" />
                  <Point X="-27.0390546875" Y="-3.790265136719" />
                  <Point X="-27.053673828125" Y="-3.79549609375" />
                  <Point X="-27.081865234375" Y="-3.80826953125" />
                  <Point X="-27.0954375" Y="-3.815812011719" />
                  <Point X="-27.232673828125" Y="-3.907510986328" />
                  <Point X="-27.35340234375" Y="-3.988180175781" />
                  <Point X="-27.359677734375" Y="-3.992755371094" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032771484375" />
                  <Point X="-27.410470703125" Y="-4.041629150391" />
                  <Point X="-27.503201171875" Y="-4.162478515625" />
                  <Point X="-27.5523515625" Y="-4.132045898438" />
                  <Point X="-27.74759375" Y="-4.011156738281" />
                  <Point X="-27.9557265625" Y="-3.850901367188" />
                  <Point X="-27.98086328125" Y="-3.831547607422" />
                  <Point X="-27.737525390625" Y="-3.410074951172" />
                  <Point X="-27.34148828125" Y="-2.724119628906" />
                  <Point X="-27.33484765625" Y="-2.710079833984" />
                  <Point X="-27.323947265625" Y="-2.681114746094" />
                  <Point X="-27.319685546875" Y="-2.666189453125" />
                  <Point X="-27.3134140625" Y="-2.634663085938" />
                  <Point X="-27.311638671875" Y="-2.619239501953" />
                  <Point X="-27.310625" Y="-2.588305419922" />
                  <Point X="-27.3146640625" Y="-2.557617675781" />
                  <Point X="-27.3236484375" Y="-2.527999511719" />
                  <Point X="-27.32935546875" Y="-2.51355859375" />
                  <Point X="-27.343572265625" Y="-2.484729736328" />
                  <Point X="-27.351552734375" Y="-2.4714140625" />
                  <Point X="-27.369578125" Y="-2.446258056641" />
                  <Point X="-27.379623046875" Y="-2.434417724609" />
                  <Point X="-27.396970703125" Y="-2.417069091797" />
                  <Point X="-27.408814453125" Y="-2.407020751953" />
                  <Point X="-27.4339765625" Y="-2.388990722656" />
                  <Point X="-27.447294921875" Y="-2.381009033203" />
                  <Point X="-27.476125" Y="-2.366791992188" />
                  <Point X="-27.490564453125" Y="-2.3610859375" />
                  <Point X="-27.520181640625" Y="-2.352101806641" />
                  <Point X="-27.5508671875" Y="-2.348062011719" />
                  <Point X="-27.581798828125" Y="-2.349074951172" />
                  <Point X="-27.59722265625" Y="-2.350849609375" />
                  <Point X="-27.628748046875" Y="-2.357120605469" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372285888672" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.290126953125" Y="-2.727323242188" />
                  <Point X="-28.793087890625" Y="-3.017708251953" />
                  <Point X="-28.8497734375" Y="-2.943235595703" />
                  <Point X="-29.00401953125" Y="-2.740587402344" />
                  <Point X="-29.153236328125" Y="-2.490375" />
                  <Point X="-29.181265625" Y="-2.443373535156" />
                  <Point X="-28.74105859375" Y="-2.105590087891" />
                  <Point X="-28.048123046875" Y="-1.573881958008" />
                  <Point X="-28.036482421875" Y="-1.563310180664" />
                  <Point X="-28.015103515625" Y="-1.540388671875" />
                  <Point X="-28.005365234375" Y="-1.528038696289" />
                  <Point X="-27.987400390625" Y="-1.50090625" />
                  <Point X="-27.979833984375" Y="-1.48712121582" />
                  <Point X="-27.967080078125" Y="-1.458494995117" />
                  <Point X="-27.961892578125" Y="-1.443653686523" />
                  <Point X="-27.9541875" Y="-1.41390637207" />
                  <Point X="-27.951552734375" Y="-1.3988046875" />
                  <Point X="-27.948748046875" Y="-1.368373901367" />
                  <Point X="-27.948578125" Y="-1.353045043945" />
                  <Point X="-27.950787109375" Y="-1.321374511719" />
                  <Point X="-27.953083984375" Y="-1.306216430664" />
                  <Point X="-27.9600859375" Y="-1.276471801758" />
                  <Point X="-27.971779296875" Y="-1.248242675781" />
                  <Point X="-27.987861328125" Y="-1.222259399414" />
                  <Point X="-27.996953125" Y="-1.209918701172" />
                  <Point X="-28.01778515625" Y="-1.185962524414" />
                  <Point X="-28.0287421875" Y="-1.175246459961" />
                  <Point X="-28.052240234375" Y="-1.155712768555" />
                  <Point X="-28.064783203125" Y="-1.14689465332" />
                  <Point X="-28.091265625" Y="-1.131307983398" />
                  <Point X="-28.10543359375" Y="-1.124479858398" />
                  <Point X="-28.13469921875" Y="-1.113255615234" />
                  <Point X="-28.149796875" Y="-1.10885925293" />
                  <Point X="-28.18168359375" Y="-1.102378173828" />
                  <Point X="-28.197298828125" Y="-1.100532104492" />
                  <Point X="-28.22862109375" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-29.006119140625" Y="-1.20048828125" />
                  <Point X="-29.66091796875" Y="-1.286694213867" />
                  <Point X="-29.68043359375" Y="-1.210293457031" />
                  <Point X="-29.740763671875" Y="-0.974113098145" />
                  <Point X="-29.780240234375" Y="-0.698083007812" />
                  <Point X="-29.786451171875" Y="-0.65465447998" />
                  <Point X="-29.29764453125" Y="-0.523678955078" />
                  <Point X="-28.508287109375" Y="-0.312171325684" />
                  <Point X="-28.4965703125" Y="-0.308197601318" />
                  <Point X="-28.47375" Y="-0.298760345459" />
                  <Point X="-28.462646484375" Y="-0.293296813965" />
                  <Point X="-28.4323125" Y="-0.275828887939" />
                  <Point X="-28.42553515625" Y="-0.271534851074" />
                  <Point X="-28.4059765625" Y="-0.257541412354" />
                  <Point X="-28.381171875" Y="-0.237009613037" />
                  <Point X="-28.37258984375" Y="-0.228958557129" />
                  <Point X="-28.356486328125" Y="-0.211859207153" />
                  <Point X="-28.342619140625" Y="-0.192900115967" />
                  <Point X="-28.331203125" Y="-0.172372177124" />
                  <Point X="-28.3261328125" Y="-0.161754852295" />
                  <Point X="-28.313783203125" Y="-0.13115562439" />
                  <Point X="-28.311240234375" Y="-0.124053833008" />
                  <Point X="-28.304748046875" Y="-0.102373313904" />
                  <Point X="-28.298173828125" Y="-0.073604232788" />
                  <Point X="-28.296373046875" Y="-0.06297190094" />
                  <Point X="-28.293986328125" Y="-0.041574363708" />
                  <Point X="-28.2940390625" Y="-0.020046632767" />
                  <Point X="-28.29653125" Y="0.001338864207" />
                  <Point X="-28.298384765625" Y="0.011961834908" />
                  <Point X="-28.305552734375" Y="0.042632698059" />
                  <Point X="-28.30756640625" Y="0.049924583435" />
                  <Point X="-28.314748046875" Y="0.071414115906" />
                  <Point X="-28.3265" Y="0.100103683472" />
                  <Point X="-28.331626953125" Y="0.110698120117" />
                  <Point X="-28.343154296875" Y="0.131173904419" />
                  <Point X="-28.357126953125" Y="0.150068618774" />
                  <Point X="-28.373328125" Y="0.167088775635" />
                  <Point X="-28.38195703125" Y="0.175095535278" />
                  <Point X="-28.40854296875" Y="0.196862243652" />
                  <Point X="-28.414962890625" Y="0.201680160522" />
                  <Point X="-28.435" Y="0.215022338867" />
                  <Point X="-28.463556640625" Y="0.231257598877" />
                  <Point X="-28.474447265625" Y="0.236560317993" />
                  <Point X="-28.4968125" Y="0.245737335205" />
                  <Point X="-28.508287109375" Y="0.249611343384" />
                  <Point X="-29.2026953125" Y="0.435677398682" />
                  <Point X="-29.7854453125" Y="0.591825073242" />
                  <Point X="-29.770208984375" Y="0.694796813965" />
                  <Point X="-29.73133203125" Y="0.95752130127" />
                  <Point X="-29.651857421875" Y="1.25080456543" />
                  <Point X="-29.6335859375" Y="1.318237060547" />
                  <Point X="-29.343896484375" Y="1.280098754883" />
                  <Point X="-28.778064453125" Y="1.20560559082" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.20470324707" />
                  <Point X="-28.71514453125" Y="1.206589477539" />
                  <Point X="-28.704888671875" Y="1.208053588867" />
                  <Point X="-28.6846015625" Y="1.212089111328" />
                  <Point X="-28.674568359375" Y="1.214660766602" />
                  <Point X="-28.641890625" Y="1.224964111328" />
                  <Point X="-28.613142578125" Y="1.234028198242" />
                  <Point X="-28.603451171875" Y="1.237676025391" />
                  <Point X="-28.584517578125" Y="1.246006713867" />
                  <Point X="-28.57527734375" Y="1.250689819336" />
                  <Point X="-28.556533203125" Y="1.261511962891" />
                  <Point X="-28.547859375" Y="1.267171875" />
                  <Point X="-28.531177734375" Y="1.279403320312" />
                  <Point X="-28.51592578125" Y="1.293378051758" />
                  <Point X="-28.502287109375" Y="1.308930175781" />
                  <Point X="-28.495892578125" Y="1.317079101562" />
                  <Point X="-28.483478515625" Y="1.334808837891" />
                  <Point X="-28.4780078125" Y="1.343605224609" />
                  <Point X="-28.46805859375" Y="1.361741210938" />
                  <Point X="-28.463580078125" Y="1.371080932617" />
                  <Point X="-28.45046875" Y="1.402737060547" />
                  <Point X="-28.43893359375" Y="1.430585205078" />
                  <Point X="-28.4355" Y="1.440349609375" />
                  <Point X="-28.429712890625" Y="1.460200805664" />
                  <Point X="-28.427359375" Y="1.470287475586" />
                  <Point X="-28.423599609375" Y="1.491603881836" />
                  <Point X="-28.422359375" Y="1.501890380859" />
                  <Point X="-28.421005859375" Y="1.522536010742" />
                  <Point X="-28.421908203125" Y="1.54320703125" />
                  <Point X="-28.425056640625" Y="1.56365637207" />
                  <Point X="-28.427189453125" Y="1.573793945312" />
                  <Point X="-28.43279296875" Y="1.594701171875" />
                  <Point X="-28.436013671875" Y="1.604540771484" />
                  <Point X="-28.4435078125" Y="1.623810791016" />
                  <Point X="-28.44778125" Y="1.633241210938" />
                  <Point X="-28.4636015625" Y="1.663633789062" />
                  <Point X="-28.477521484375" Y="1.690370605469" />
                  <Point X="-28.48280078125" Y="1.699286499023" />
                  <Point X="-28.494296875" Y="1.716491455078" />
                  <Point X="-28.500513671875" Y="1.724779785156" />
                  <Point X="-28.51442578125" Y="1.741358154297" />
                  <Point X="-28.521505859375" Y="1.748917358398" />
                  <Point X="-28.5364453125" Y="1.763217529297" />
                  <Point X="-28.5443046875" Y="1.769958496094" />
                  <Point X="-28.9426171875" Y="2.075596191406" />
                  <Point X="-29.22761328125" Y="2.294281494141" />
                  <Point X="-29.15335546875" Y="2.421504638672" />
                  <Point X="-29.00228515625" Y="2.680322021484" />
                  <Point X="-28.791779296875" Y="2.950897705078" />
                  <Point X="-28.726337890625" Y="3.035013183594" />
                  <Point X="-28.601673828125" Y="2.963038085938" />
                  <Point X="-28.25415625" Y="2.762398681641" />
                  <Point X="-28.24491796875" Y="2.757716796875" />
                  <Point X="-28.225984375" Y="2.749385742188" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.109560546875" Y="2.727176269531" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.038494140625" Y="2.723785644531" />
                  <Point X="-28.02816796875" Y="2.724575683594" />
                  <Point X="-28.006708984375" Y="2.727400634766" />
                  <Point X="-27.99652734375" Y="2.729310302734" />
                  <Point X="-27.976435546875" Y="2.7342265625" />
                  <Point X="-27.957001953125" Y="2.741299316406" />
                  <Point X="-27.93844921875" Y="2.750447509766" />
                  <Point X="-27.929419921875" Y="2.755529541016" />
                  <Point X="-27.911166015625" Y="2.767158203125" />
                  <Point X="-27.90274609375" Y="2.773191650391" />
                  <Point X="-27.88661328125" Y="2.786139404297" />
                  <Point X="-27.878900390625" Y="2.793053710938" />
                  <Point X="-27.846595703125" Y="2.825358398438" />
                  <Point X="-27.81817578125" Y="2.853777099609" />
                  <Point X="-27.81126171875" Y="2.861490234375" />
                  <Point X="-27.798314453125" Y="2.877622558594" />
                  <Point X="-27.79228125" Y="2.886041748047" />
                  <Point X="-27.78065234375" Y="2.904295898438" />
                  <Point X="-27.7755703125" Y="2.913324462891" />
                  <Point X="-27.766423828125" Y="2.931873535156" />
                  <Point X="-27.759353515625" Y="2.951298095703" />
                  <Point X="-27.754435546875" Y="2.971387207031" />
                  <Point X="-27.7525234375" Y="2.981572265625" />
                  <Point X="-27.749697265625" Y="3.003031005859" />
                  <Point X="-27.748908203125" Y="3.013364013672" />
                  <Point X="-27.74845703125" Y="3.034049560547" />
                  <Point X="-27.748794921875" Y="3.044402099609" />
                  <Point X="-27.75277734375" Y="3.0899140625" />
                  <Point X="-27.756279296875" Y="3.129951660156" />
                  <Point X="-27.757744140625" Y="3.140209716797" />
                  <Point X="-27.76178125" Y="3.160499511719" />
                  <Point X="-27.764353515625" Y="3.17053125" />
                  <Point X="-27.77086328125" Y="3.191174560547" />
                  <Point X="-27.77451171875" Y="3.200868164062" />
                  <Point X="-27.782841796875" Y="3.219797607422" />
                  <Point X="-27.7875234375" Y="3.229033447266" />
                  <Point X="-27.964029296875" Y="3.534749267578" />
                  <Point X="-28.05938671875" Y="3.699915039062" />
                  <Point X="-27.911470703125" Y="3.813322021484" />
                  <Point X="-27.6483671875" Y="4.015041015625" />
                  <Point X="-27.316828125" Y="4.199236816406" />
                  <Point X="-27.192525390625" Y="4.268296386719" />
                  <Point X="-27.118564453125" Y="4.171909179688" />
                  <Point X="-27.1118203125" Y="4.164046875" />
                  <Point X="-27.097515625" Y="4.149104003906" />
                  <Point X="-27.089955078125" Y="4.142022949219" />
                  <Point X="-27.073375" Y="4.128110839844" />
                  <Point X="-27.065091796875" Y="4.121896972656" />
                  <Point X="-27.04789453125" Y="4.110405761719" />
                  <Point X="-27.03898046875" Y="4.105128417969" />
                  <Point X="-26.988326171875" Y="4.078759521484" />
                  <Point X="-26.943763671875" Y="4.055561767578" />
                  <Point X="-26.93432421875" Y="4.051285400391" />
                  <Point X="-26.91504296875" Y="4.043788330078" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031378173828" />
                  <Point X="-26.780818359375" Y="4.035136474609" />
                  <Point X="-26.770728515625" Y="4.037488525391" />
                  <Point X="-26.7508671875" Y="4.043277587891" />
                  <Point X="-26.741095703125" Y="4.046714355469" />
                  <Point X="-26.6883359375" Y="4.068568847656" />
                  <Point X="-26.641921875" Y="4.087793945312" />
                  <Point X="-26.63258203125" Y="4.092274169922" />
                  <Point X="-26.614447265625" Y="4.102224121094" />
                  <Point X="-26.60565234375" Y="4.107693359375" />
                  <Point X="-26.587923828125" Y="4.120107421875" />
                  <Point X="-26.579775390625" Y="4.126500976563" />
                  <Point X="-26.564224609375" Y="4.140138671875" />
                  <Point X="-26.550251953125" Y="4.155387695312" />
                  <Point X="-26.538021484375" Y="4.172067382812" />
                  <Point X="-26.532361328125" Y="4.1807421875" />
                  <Point X="-26.5215390625" Y="4.199486328125" />
                  <Point X="-26.516857421875" Y="4.208724121094" />
                  <Point X="-26.508525390625" Y="4.227658691406" />
                  <Point X="-26.504875" Y="4.23735546875" />
                  <Point X="-26.487703125" Y="4.291819335938" />
                  <Point X="-26.472595703125" Y="4.339731933594" />
                  <Point X="-26.470025390625" Y="4.349764160156" />
                  <Point X="-26.465990234375" Y="4.370049316406" />
                  <Point X="-26.464525390625" Y="4.380302246094" />
                  <Point X="-26.462638671875" Y="4.401864257812" />
                  <Point X="-26.46230078125" Y="4.412217285156" />
                  <Point X="-26.462751953125" Y="4.4328984375" />
                  <Point X="-26.463541015625" Y="4.4432265625" />
                  <Point X="-26.479263671875" Y="4.562655273438" />
                  <Point X="-26.27176953125" Y="4.620829589844" />
                  <Point X="-25.931173828125" Y="4.7163203125" />
                  <Point X="-25.52925" Y="4.763359863281" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.32847265625" Y="4.64540234375" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.671580078125" Y="4.599251464844" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.469521484375" Y="4.769058105469" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.839587890625" Y="4.657627929688" />
                  <Point X="-23.54640234375" Y="4.586843261719" />
                  <Point X="-23.33162109375" Y="4.50894140625" />
                  <Point X="-23.14175" Y="4.440073730469" />
                  <Point X="-22.93244140625" Y="4.3421875" />
                  <Point X="-22.749544921875" Y="4.25665234375" />
                  <Point X="-22.54730078125" Y="4.138825195312" />
                  <Point X="-22.370576171875" Y="4.035863525391" />
                  <Point X="-22.18221875" Y="3.901916503906" />
                  <Point X="-22.476306640625" Y="3.392539550781" />
                  <Point X="-22.934689453125" Y="2.59859765625" />
                  <Point X="-22.937619140625" Y="2.593114257812" />
                  <Point X="-22.9468125" Y="2.573448242188" />
                  <Point X="-22.955814453125" Y="2.549569580078" />
                  <Point X="-22.958697265625" Y="2.540600341797" />
                  <Point X="-22.970119140625" Y="2.497888427734" />
                  <Point X="-22.98016796875" Y="2.460313964844" />
                  <Point X="-22.9820859375" Y="2.451469970703" />
                  <Point X="-22.986353515625" Y="2.420223388672" />
                  <Point X="-22.987244140625" Y="2.383240966797" />
                  <Point X="-22.986587890625" Y="2.36958203125" />
                  <Point X="-22.982134765625" Y="2.3326484375" />
                  <Point X="-22.978216796875" Y="2.300157226562" />
                  <Point X="-22.97619921875" Y="2.289036376953" />
                  <Point X="-22.97085546875" Y="2.267108154297" />
                  <Point X="-22.967529296875" Y="2.25630078125" />
                  <Point X="-22.95926171875" Y="2.234211181641" />
                  <Point X="-22.954677734375" Y="2.223882568359" />
                  <Point X="-22.94431640625" Y="2.203841064453" />
                  <Point X="-22.9385390625" Y="2.194128173828" />
                  <Point X="-22.915685546875" Y="2.160448242188" />
                  <Point X="-22.895580078125" Y="2.130819580078" />
                  <Point X="-22.890189453125" Y="2.123631347656" />
                  <Point X="-22.86953515625" Y="2.100123291016" />
                  <Point X="-22.84240234375" Y="2.075387695312" />
                  <Point X="-22.8317421875" Y="2.066981445312" />
                  <Point X="-22.7980625" Y="2.044128295898" />
                  <Point X="-22.76843359375" Y="2.024023925781" />
                  <Point X="-22.75871875" Y="2.018245239258" />
                  <Point X="-22.738673828125" Y="2.007882324219" />
                  <Point X="-22.72834375" Y="2.003298461914" />
                  <Point X="-22.706255859375" Y="1.995032470703" />
                  <Point X="-22.695451171875" Y="1.991707397461" />
                  <Point X="-22.67352734375" Y="1.986364746094" />
                  <Point X="-22.662408203125" Y="1.984346801758" />
                  <Point X="-22.625474609375" Y="1.979893066406" />
                  <Point X="-22.592984375" Y="1.975975219727" />
                  <Point X="-22.583955078125" Y="1.975320922852" />
                  <Point X="-22.55242578125" Y="1.975497314453" />
                  <Point X="-22.5156875" Y="1.979822509766" />
                  <Point X="-22.50225390625" Y="1.982395629883" />
                  <Point X="-22.459541015625" Y="1.993817504883" />
                  <Point X="-22.421966796875" Y="2.003865356445" />
                  <Point X="-22.416005859375" Y="2.005670776367" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572753906" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.6655234375" Y="2.431117675781" />
                  <Point X="-21.059595703125" Y="2.780950195312" />
                  <Point X="-20.95603515625" Y="2.637026123047" />
                  <Point X="-20.863115234375" Y="2.483471435547" />
                  <Point X="-21.22354296875" Y="2.206904052734" />
                  <Point X="-21.827046875" Y="1.743819946289" />
                  <Point X="-21.831859375" Y="1.739869506836" />
                  <Point X="-21.847880859375" Y="1.725216430664" />
                  <Point X="-21.86533203125" Y="1.706602050781" />
                  <Point X="-21.871421875" Y="1.699422363281" />
                  <Point X="-21.902162109375" Y="1.659319824219" />
                  <Point X="-21.929203125" Y="1.624040893555" />
                  <Point X="-21.934361328125" Y="1.616603759766" />
                  <Point X="-21.9502578125" Y="1.589374511719" />
                  <Point X="-21.965234375" Y="1.555551269531" />
                  <Point X="-21.969859375" Y="1.542675415039" />
                  <Point X="-21.981310546875" Y="1.501730712891" />
                  <Point X="-21.991384765625" Y="1.4657109375" />
                  <Point X="-21.993775390625" Y="1.454661865234" />
                  <Point X="-21.997228515625" Y="1.432363037109" />
                  <Point X="-21.998291015625" Y="1.42111328125" />
                  <Point X="-21.999107421875" Y="1.397541503906" />
                  <Point X="-21.998826171875" Y="1.386236816406" />
                  <Point X="-21.996921875" Y="1.36374987793" />
                  <Point X="-21.995298828125" Y="1.352567749023" />
                  <Point X="-21.9858984375" Y="1.307011352539" />
                  <Point X="-21.97762890625" Y="1.266935058594" />
                  <Point X="-21.97540234375" Y="1.258239013672" />
                  <Point X="-21.965314453125" Y="1.228606933594" />
                  <Point X="-21.9497109375" Y="1.195369873047" />
                  <Point X="-21.943080078125" Y="1.183526611328" />
                  <Point X="-21.917513671875" Y="1.144666748047" />
                  <Point X="-21.8950234375" Y="1.110481201172" />
                  <Point X="-21.88826171875" Y="1.101426269531" />
                  <Point X="-21.873708984375" Y="1.084181884766" />
                  <Point X="-21.86591796875" Y="1.075992675781" />
                  <Point X="-21.848673828125" Y="1.05990234375" />
                  <Point X="-21.839966796875" Y="1.052696411133" />
                  <Point X="-21.821755859375" Y="1.039369750977" />
                  <Point X="-21.812251953125" Y="1.033248657227" />
                  <Point X="-21.775201171875" Y="1.012392944336" />
                  <Point X="-21.742609375" Y="0.994046386719" />
                  <Point X="-21.73451953125" Y="0.989987304688" />
                  <Point X="-21.7053203125" Y="0.978084106445" />
                  <Point X="-21.66972265625" Y="0.968021179199" />
                  <Point X="-21.656328125" Y="0.965257629395" />
                  <Point X="-21.606234375" Y="0.958637084961" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.555966796875" Y="0.952199829102" />
                  <Point X="-21.53428125" Y="0.951222900391" />
                  <Point X="-21.50878125" Y="0.952032287598" />
                  <Point X="-21.49939453125" Y="0.952797241211" />
                  <Point X="-20.835923828125" Y="1.040145141602" />
                  <Point X="-20.295296875" Y="1.111319702148" />
                  <Point X="-20.29234375" Y="1.09918737793" />
                  <Point X="-20.247310546875" Y="0.914208190918" />
                  <Point X="-20.216126953125" Y="0.713921203613" />
                  <Point X="-20.61637890625" Y="0.60667401123" />
                  <Point X="-21.3080078125" Y="0.421352844238" />
                  <Point X="-21.31396875" Y="0.419544250488" />
                  <Point X="-21.334376953125" Y="0.412136138916" />
                  <Point X="-21.357619140625" Y="0.401618377686" />
                  <Point X="-21.365994140625" Y="0.397316497803" />
                  <Point X="-21.415208984375" Y="0.36886932373" />
                  <Point X="-21.45850390625" Y="0.343843902588" />
                  <Point X="-21.466115234375" Y="0.338946472168" />
                  <Point X="-21.49122265625" Y="0.319871917725" />
                  <Point X="-21.51800390625" Y="0.294352172852" />
                  <Point X="-21.527203125" Y="0.284227233887" />
                  <Point X="-21.556732421875" Y="0.246600387573" />
                  <Point X="-21.582708984375" Y="0.213499389648" />
                  <Point X="-21.589146484375" Y="0.204205551147" />
                  <Point X="-21.60087109375" Y="0.184923080444" />
                  <Point X="-21.606158203125" Y="0.174934432983" />
                  <Point X="-21.615931640625" Y="0.153470306396" />
                  <Point X="-21.619994140625" Y="0.142927734375" />
                  <Point X="-21.62683984375" Y="0.121429885864" />
                  <Point X="-21.629623046875" Y="0.110474739075" />
                  <Point X="-21.639466796875" Y="0.059078117371" />
                  <Point X="-21.648125" Y="0.013864059448" />
                  <Point X="-21.649396484375" Y="0.004966154575" />
                  <Point X="-21.6514140625" Y="-0.026262638092" />
                  <Point X="-21.64971875" Y="-0.062940391541" />
                  <Point X="-21.648125" Y="-0.076424201965" />
                  <Point X="-21.63828125" Y="-0.127820678711" />
                  <Point X="-21.629623046875" Y="-0.173034744263" />
                  <Point X="-21.62683984375" Y="-0.183989883423" />
                  <Point X="-21.619994140625" Y="-0.205487731934" />
                  <Point X="-21.615931640625" Y="-0.216030303955" />
                  <Point X="-21.606158203125" Y="-0.237494415283" />
                  <Point X="-21.60087109375" Y="-0.247482925415" />
                  <Point X="-21.589146484375" Y="-0.266765411377" />
                  <Point X="-21.582708984375" Y="-0.276059539795" />
                  <Point X="-21.5531796875" Y="-0.313686401367" />
                  <Point X="-21.527203125" Y="-0.346787109375" />
                  <Point X="-21.521279296875" Y="-0.353635498047" />
                  <Point X="-21.498859375" Y="-0.375805419922" />
                  <Point X="-21.469822265625" Y="-0.398725006104" />
                  <Point X="-21.45850390625" Y="-0.406404205322" />
                  <Point X="-21.4092890625" Y="-0.434851226807" />
                  <Point X="-21.365994140625" Y="-0.459876495361" />
                  <Point X="-21.360505859375" Y="-0.462813415527" />
                  <Point X="-21.34084375" Y="-0.472015869141" />
                  <Point X="-21.31697265625" Y="-0.48102746582" />
                  <Point X="-21.3080078125" Y="-0.483912841797" />
                  <Point X="-20.69957421875" Y="-0.646942321777" />
                  <Point X="-20.215123046875" Y="-0.776750549316" />
                  <Point X="-20.238392578125" Y="-0.931085266113" />
                  <Point X="-20.272197265625" Y="-1.079219726563" />
                  <Point X="-20.761123046875" Y="-1.014851318359" />
                  <Point X="-21.563216796875" Y="-0.909253601074" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042297363" />
                  <Point X="-21.63327734375" Y="-0.910840881348" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.742109375" Y="-0.933670837402" />
                  <Point X="-21.82708203125" Y="-0.952139892578" />
                  <Point X="-21.842123046875" Y="-0.9567421875" />
                  <Point X="-21.871244140625" Y="-0.968366699219" />
                  <Point X="-21.88532421875" Y="-0.975389282227" />
                  <Point X="-21.913150390625" Y="-0.992281494141" />
                  <Point X="-21.925875" Y="-1.001530700684" />
                  <Point X="-21.949625" Y="-1.02200189209" />
                  <Point X="-21.960650390625" Y="-1.033223632812" />
                  <Point X="-22.019033203125" Y="-1.103440795898" />
                  <Point X="-22.07039453125" Y="-1.165211547852" />
                  <Point X="-22.078673828125" Y="-1.176850341797" />
                  <Point X="-22.093392578125" Y="-1.201231445312" />
                  <Point X="-22.09983203125" Y="-1.213973632812" />
                  <Point X="-22.11117578125" Y="-1.241359130859" />
                  <Point X="-22.115634765625" Y="-1.254927612305" />
                  <Point X="-22.122466796875" Y="-1.282578857422" />
                  <Point X="-22.12483984375" Y="-1.296661621094" />
                  <Point X="-22.13320703125" Y="-1.387595947266" />
                  <Point X="-22.140568359375" Y="-1.467591918945" />
                  <Point X="-22.140708984375" Y="-1.483315673828" />
                  <Point X="-22.138392578125" Y="-1.514580688477" />
                  <Point X="-22.135935546875" Y="-1.530121948242" />
                  <Point X="-22.128205078125" Y="-1.561742919922" />
                  <Point X="-22.12321484375" Y="-1.576666625977" />
                  <Point X="-22.110841796875" Y="-1.60548059082" />
                  <Point X="-22.103458984375" Y="-1.619370849609" />
                  <Point X="-22.05000390625" Y="-1.702517333984" />
                  <Point X="-22.002978515625" Y="-1.775661621094" />
                  <Point X="-21.99825390625" Y="-1.782356201172" />
                  <Point X="-21.98019921875" Y="-1.804455444336" />
                  <Point X="-21.956505859375" Y="-1.828122070312" />
                  <Point X="-21.947201171875" Y="-1.836277587891" />
                  <Point X="-21.3825703125" Y="-2.269534423828" />
                  <Point X="-20.912830078125" Y="-2.629980224609" />
                  <Point X="-20.9545234375" Y="-2.6974453125" />
                  <Point X="-20.9987265625" Y="-2.760251464844" />
                  <Point X="-21.437171875" Y="-2.507114257812" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.18497265625" Y="-2.079513427734" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228892578125" Y="-2.066337646484" />
                  <Point X="-22.3438515625" Y="-2.045575927734" />
                  <Point X="-22.444982421875" Y="-2.027311889648" />
                  <Point X="-22.460640625" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403442383" />
                  <Point X="-22.507685546875" Y="-2.02650378418" />
                  <Point X="-22.539859375" Y="-2.031461914062" />
                  <Point X="-22.555154296875" Y="-2.03513659668" />
                  <Point X="-22.5849296875" Y="-2.044960083008" />
                  <Point X="-22.59941015625" Y="-2.051108886719" />
                  <Point X="-22.6949140625" Y="-2.101371337891" />
                  <Point X="-22.778927734375" Y="-2.145587890625" />
                  <Point X="-22.79103125" Y="-2.153170654297" />
                  <Point X="-22.813962890625" Y="-2.170064453125" />
                  <Point X="-22.824791015625" Y="-2.179375488281" />
                  <Point X="-22.84575" Y="-2.200334960938" />
                  <Point X="-22.85505859375" Y="-2.211161132813" />
                  <Point X="-22.871951171875" Y="-2.234090332031" />
                  <Point X="-22.87953515625" Y="-2.246193359375" />
                  <Point X="-22.929798828125" Y="-2.341696289062" />
                  <Point X="-22.974015625" Y="-2.425711425781" />
                  <Point X="-22.9801640625" Y="-2.440193603516" />
                  <Point X="-22.98998828125" Y="-2.46997265625" />
                  <Point X="-22.9936640625" Y="-2.48526953125" />
                  <Point X="-22.99862109375" Y="-2.517443115234" />
                  <Point X="-22.999720703125" Y="-2.533134277344" />
                  <Point X="-22.99931640625" Y="-2.564484375" />
                  <Point X="-22.9978125" Y="-2.580143310547" />
                  <Point X="-22.97705078125" Y="-2.695102539062" />
                  <Point X="-22.95878515625" Y="-2.796233642578" />
                  <Point X="-22.95698046875" Y="-2.804228271484" />
                  <Point X="-22.94876171875" Y="-2.831543701172" />
                  <Point X="-22.935927734375" Y="-2.862480712891" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.5676171875" Y="-3.502021972656" />
                  <Point X="-22.264103515625" Y="-4.027721679687" />
                  <Point X="-22.2762421875" Y="-4.036083007813" />
                  <Point X="-22.620951171875" Y="-3.586852783203" />
                  <Point X="-23.16608203125" Y="-2.876422363281" />
                  <Point X="-23.17134765625" Y="-2.870141113281" />
                  <Point X="-23.19116796875" Y="-2.849625732422" />
                  <Point X="-23.21674609375" Y="-2.828004638672" />
                  <Point X="-23.22669921875" Y="-2.820646972656" />
                  <Point X="-23.340080078125" Y="-2.747753417969" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.453716796875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663873535156" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.71560546875" Y="-2.657927001953" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920125" Y="-2.699414794922" />
                  <Point X="-23.944505859375" Y="-2.714135742188" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-24.051888671875" Y="-2.802026855469" />
                  <Point X="-24.136123046875" Y="-2.872063720703" />
                  <Point X="-24.147345703125" Y="-2.883087646484" />
                  <Point X="-24.16781640625" Y="-2.906837158203" />
                  <Point X="-24.177064453125" Y="-2.919562744141" />
                  <Point X="-24.19395703125" Y="-2.947389404297" />
                  <Point X="-24.20098046875" Y="-2.961466552734" />
                  <Point X="-24.21260546875" Y="-2.990587402344" />
                  <Point X="-24.21720703125" Y="-3.005631103516" />
                  <Point X="-24.2458359375" Y="-3.137346923828" />
                  <Point X="-24.271021484375" Y="-3.253218994141" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323170166016" />
                  <Point X="-24.2744453125" Y="-3.335520507812" />
                  <Point X="-24.17162109375" Y="-4.116544433594" />
                  <Point X="-24.16691015625" Y="-4.152319824219" />
                  <Point X="-24.344931640625" Y="-3.487937011719" />
                  <Point X="-24.347392578125" Y="-3.480122314453" />
                  <Point X="-24.357853515625" Y="-3.453578857422" />
                  <Point X="-24.3732109375" Y="-3.423815429688" />
                  <Point X="-24.37958984375" Y="-3.413209228516" />
                  <Point X="-24.466693359375" Y="-3.2877109375" />
                  <Point X="-24.543318359375" Y="-3.177308837891" />
                  <Point X="-24.553328125" Y="-3.165171386719" />
                  <Point X="-24.575212890625" Y="-3.142715820312" />
                  <Point X="-24.587087890625" Y="-3.132397705078" />
                  <Point X="-24.61334375" Y="-3.113153808594" />
                  <Point X="-24.6267578125" Y="-3.104936767578" />
                  <Point X="-24.6547578125" Y="-3.090829345703" />
                  <Point X="-24.66934375" Y="-3.084938964844" />
                  <Point X="-24.80412890625" Y="-3.043106201172" />
                  <Point X="-24.922703125" Y="-3.006305664062" />
                  <Point X="-24.936623046875" Y="-3.003109619141" />
                  <Point X="-24.964783203125" Y="-2.998840087891" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.998840087891" />
                  <Point X="-25.051064453125" Y="-3.003109619141" />
                  <Point X="-25.064984375" Y="-3.006305664062" />
                  <Point X="-25.19976953125" Y="-3.048138183594" />
                  <Point X="-25.31834375" Y="-3.084938964844" />
                  <Point X="-25.3329296875" Y="-3.090829345703" />
                  <Point X="-25.3609296875" Y="-3.104936767578" />
                  <Point X="-25.37434375" Y="-3.113153808594" />
                  <Point X="-25.400599609375" Y="-3.132397705078" />
                  <Point X="-25.412474609375" Y="-3.142717041016" />
                  <Point X="-25.434359375" Y="-3.165173339844" />
                  <Point X="-25.444369140625" Y="-3.177310302734" />
                  <Point X="-25.531470703125" Y="-3.302808349609" />
                  <Point X="-25.608095703125" Y="-3.413210693359" />
                  <Point X="-25.612470703125" Y="-3.420132568359" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213378906" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.830228515625" Y="-4.18759765625" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.698892658725" Y="-0.631193214796" />
                  <Point X="-29.596416466776" Y="-1.278202426934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.606624254397" Y="-0.606469936337" />
                  <Point X="-29.502196918962" Y="-1.265798183613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.744286397233" Y="0.86997768255" />
                  <Point X="-29.696454672812" Y="0.567980060038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.514355850069" Y="-0.581746657878" />
                  <Point X="-29.407977371148" Y="-1.253393940292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.687554784786" Y="1.119071434574" />
                  <Point X="-29.596007606832" Y="0.541065301103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.422087445741" Y="-0.557023379419" />
                  <Point X="-29.313757823335" Y="-1.240989696972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.129598221994" Y="-2.403727658876" />
                  <Point X="-29.102363593236" Y="-2.575680337448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.622688095636" Y="1.31680233374" />
                  <Point X="-29.495560540852" Y="0.514150542168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.329819041413" Y="-0.53230010096" />
                  <Point X="-29.219538275521" Y="-1.228585453651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.043836862743" Y="-2.337920514706" />
                  <Point X="-28.973765220483" Y="-2.780335452159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.5244555952" Y="1.303869791366" />
                  <Point X="-29.395113474872" Y="0.487235783233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.237550633088" Y="-0.507576847732" />
                  <Point X="-29.125318727707" Y="-1.216181210331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.958075503492" Y="-2.272113370537" />
                  <Point X="-28.852307482613" Y="-2.939906372567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.426223094764" Y="1.290937248992" />
                  <Point X="-29.294666408892" Y="0.460321024299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.145282222625" Y="-0.482853608013" />
                  <Point X="-29.031099179893" Y="-1.20377696701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.872314144241" Y="-2.206306226367" />
                  <Point X="-28.747930073672" Y="-2.991636340321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.327990594187" Y="1.278004705727" />
                  <Point X="-29.194219343716" Y="0.433406270447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.053013812161" Y="-0.458130368294" />
                  <Point X="-28.936879632383" Y="-1.191372721768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.78655278499" Y="-2.140499082197" />
                  <Point X="-28.659804399576" Y="-2.940756892585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.229758092879" Y="1.265072157847" />
                  <Point X="-29.093772287277" Y="0.406491571753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.960745401697" Y="-0.433407128574" />
                  <Point X="-28.842660084984" Y="-1.178968475833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.70079141641" Y="-2.074691996927" />
                  <Point X="-28.57167872548" Y="-2.889877444848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.131525591572" Y="1.252139609967" />
                  <Point X="-28.993325230838" Y="0.379576873059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.868476991233" Y="-0.408683888855" />
                  <Point X="-28.748440537584" Y="-1.166564229898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.61503003729" Y="-2.008884978203" />
                  <Point X="-28.483553051384" Y="-2.838997997112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.206208688918" Y="2.330953185" />
                  <Point X="-29.196635670523" Y="2.27051152561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.033293090264" Y="1.239207062087" />
                  <Point X="-28.892878174399" Y="0.352662174365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.776208580769" Y="-0.383960649136" />
                  <Point X="-28.654220990184" Y="-1.154159983963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.52926865817" Y="-1.943077959478" />
                  <Point X="-28.395427377288" Y="-2.788118549375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.130553668945" Y="2.460569244095" />
                  <Point X="-29.087144755283" Y="2.186496149712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.935060588956" Y="1.226274514207" />
                  <Point X="-28.79243111796" Y="0.32574747567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.683940170305" Y="-0.359237409417" />
                  <Point X="-28.560001442784" Y="-1.141755738028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.44350727905" Y="-1.877270940754" />
                  <Point X="-28.307301703192" Y="-2.737239101639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.054898398042" Y="2.590183718884" />
                  <Point X="-28.977653840043" Y="2.102480773813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.836828087648" Y="1.213341966327" />
                  <Point X="-28.691984061521" Y="0.298832776976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.591671759841" Y="-0.334514169697" />
                  <Point X="-28.465781895384" Y="-1.129351492093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.35774589993" Y="-1.81146392203" />
                  <Point X="-28.219176006427" Y="-2.686359797023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.977945753834" Y="2.711606900996" />
                  <Point X="-28.868162870451" Y="2.018465054751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.739262402642" Y="1.204619530881" />
                  <Point X="-28.591537005082" Y="0.271918078282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.499498422335" Y="-0.309190662948" />
                  <Point X="-28.371562347985" Y="-1.116947246158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.271984520811" Y="-1.745656903305" />
                  <Point X="-28.131050304176" Y="-2.635480527051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.950061672517" Y="-3.778197774331" />
                  <Point X="-27.936160452413" Y="-3.865966623815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.898030739639" Y="2.81432641512" />
                  <Point X="-28.758671875282" Y="1.934449174203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.646090732035" Y="1.223639810501" />
                  <Point X="-28.490816522538" Y="0.243277035105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.410933016961" Y="-0.261087569233" />
                  <Point X="-28.277342800585" Y="-1.104543000223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.186223141691" Y="-1.679849884581" />
                  <Point X="-28.042924601925" Y="-2.584601257079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.87458338939" Y="-3.647465842706" />
                  <Point X="-27.826617406382" Y="-3.950311140576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.818115725444" Y="2.917045929244" />
                  <Point X="-28.649180880113" Y="1.850433293654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.555963757113" Y="1.261883542121" />
                  <Point X="-28.384113023254" Y="0.176860710917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.329398492028" Y="-0.168593243485" />
                  <Point X="-28.181495431091" Y="-1.102416418499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.100461762571" Y="-1.614042865857" />
                  <Point X="-27.954798899674" Y="-2.533721987107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.799105106263" Y="-3.516733911082" />
                  <Point X="-27.717882508942" Y="-4.029553207941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.738200684563" Y="3.01976527488" />
                  <Point X="-28.539613452062" Y="1.765934834884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.473906974048" Y="1.351080459798" />
                  <Point X="-28.079652716865" Y="-1.138142953658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.015821355557" Y="-1.541158307801" />
                  <Point X="-27.866673197422" Y="-2.482842717135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.723626825084" Y="-3.386001967154" />
                  <Point X="-27.611240145376" Y="-4.095583536395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.636187884458" Y="2.982964859743" />
                  <Point X="-27.962471552809" Y="-1.270712649667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.948633937058" Y="-1.358079917074" />
                  <Point X="-27.778547495171" Y="-2.431963447163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.648148552541" Y="-3.255269968709" />
                  <Point X="-27.504597798144" Y="-4.161613761712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.530323055318" Y="2.92184369045" />
                  <Point X="-27.69042179292" Y="-2.381084177192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.572670279997" Y="-3.124537970265" />
                  <Point X="-27.424517817196" Y="-4.059935806679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.424458238452" Y="2.860722598658" />
                  <Point X="-27.598971201987" Y="-2.351197428173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.497192007453" Y="-2.99380597182" />
                  <Point X="-27.341010083292" Y="-3.979899832058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.318593421587" Y="2.799601506866" />
                  <Point X="-27.501758649962" Y="-2.357690269723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.421713734909" Y="-2.863073973376" />
                  <Point X="-27.254030932711" Y="-3.92178151974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.21375116802" Y="2.744936625649" />
                  <Point X="-27.396019045221" Y="-2.418020803273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.346235462366" Y="-2.732341974931" />
                  <Point X="-27.167051746809" Y="-3.863663430432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.114826992028" Y="2.72763701568" />
                  <Point X="-27.079790016462" Y="-3.807329256541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.018362387946" Y="2.72586653159" />
                  <Point X="-26.988138089382" Y="-3.778713693922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.927109481086" Y="2.757001408729" />
                  <Point X="-26.891836543301" Y="-3.779454670314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.988835406805" Y="3.754006621774" />
                  <Point X="-27.950358032933" Y="3.511070044205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.842414229592" Y="2.829539692366" />
                  <Point X="-26.794643404079" Y="-3.78582494425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.903066405729" Y="3.819765517358" />
                  <Point X="-27.817812910133" Y="3.281496130405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.763646602266" Y="2.939503522069" />
                  <Point X="-26.697450241059" Y="-3.792195368446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.817297318772" Y="3.885523870713" />
                  <Point X="-26.599463580721" Y="-3.803575737528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.731528231815" Y="3.951282224067" />
                  <Point X="-26.495211732663" Y="-3.854512945069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.645678992821" Y="4.01653451738" />
                  <Point X="-26.383535963625" Y="-3.952322944944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.55727399536" Y="4.065650386797" />
                  <Point X="-26.271836775191" Y="-4.050280809064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.468868997899" Y="4.114766256214" />
                  <Point X="-26.064664647543" Y="-4.751031087757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.380464000437" Y="4.163882125631" />
                  <Point X="-25.97320609568" Y="-4.72119460207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.292058994533" Y="4.21299794174" />
                  <Point X="-25.912754560218" Y="-4.495587519615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.203653966937" Y="4.262113620892" />
                  <Point X="-25.852303024756" Y="-4.26998043716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.088218497868" Y="4.140565809253" />
                  <Point X="-25.791851485966" Y="-4.04437337572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.981698725911" Y="4.075309493759" />
                  <Point X="-25.731399945261" Y="-3.818766326366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.878946584537" Y="4.033841061569" />
                  <Point X="-25.670948404556" Y="-3.593159277013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.782909184972" Y="4.034767840639" />
                  <Point X="-25.604162776779" Y="-3.407544079507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.691848127263" Y="4.067114005641" />
                  <Point X="-25.52584981075" Y="-3.294709631349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.602446612567" Y="4.109938112862" />
                  <Point X="-25.44753694472" Y="-3.181874551819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.520705863754" Y="4.201130392275" />
                  <Point X="-25.36330777977" Y="-3.106393513565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.466865147636" Y="4.468476545377" />
                  <Point X="-25.272762276582" Y="-3.070792265418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.389579870861" Y="4.58779956812" />
                  <Point X="-25.181084635233" Y="-3.042339056306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.297485204911" Y="4.613619787525" />
                  <Point X="-25.08940703475" Y="-3.013885589177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.205390516631" Y="4.639439865948" />
                  <Point X="-24.995775844632" Y="-2.997766601562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.113295819701" Y="4.665259889754" />
                  <Point X="-24.896974486488" Y="-3.014290770153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.021201122771" Y="4.691079913559" />
                  <Point X="-24.795817834744" Y="-3.045685677279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.929053918367" Y="4.716568418225" />
                  <Point X="-24.694661065788" Y="-3.07708132445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.834620218583" Y="4.727620559217" />
                  <Point X="-24.590060729366" Y="-3.130218800926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.740186518798" Y="4.738672700209" />
                  <Point X="-24.469594220744" Y="-3.283531346169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.645752819014" Y="4.749724841201" />
                  <Point X="-24.335403023162" Y="-3.523498167112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.551319119229" Y="4.760776982193" />
                  <Point X="-24.275272142285" Y="-3.295866551284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.456885397199" Y="4.771828982733" />
                  <Point X="-24.221747857558" Y="-3.026522529013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.35832026934" Y="4.756796313457" />
                  <Point X="-24.148136257468" Y="-2.884004824537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.167826662599" Y="4.161350071403" />
                  <Point X="-24.063417839471" Y="-2.811612808439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.061307441899" Y="4.096097236409" />
                  <Point X="-23.978426345585" Y="-2.740944925658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.963765416905" Y="4.087524184403" />
                  <Point X="-23.890920055717" Y="-2.686154839811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.872527312824" Y="4.118752522609" />
                  <Point X="-23.798005744303" Y="-2.665509658192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.789580702776" Y="4.202331293822" />
                  <Point X="-23.703203290717" Y="-2.656785737075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.724802721501" Y="4.400622272472" />
                  <Point X="-23.608400825649" Y="-2.648061888449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.664351225391" Y="4.626229603384" />
                  <Point X="-23.511028279743" Y="-2.65556489161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.592834239085" Y="4.781972178812" />
                  <Point X="-23.407063441437" Y="-2.704689990896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.495027735972" Y="4.771729277681" />
                  <Point X="-23.299974897316" Y="-2.773537392501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.397221217581" Y="4.761486280084" />
                  <Point X="-23.191827846891" Y="-2.849067939888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.299414693799" Y="4.751243248455" />
                  <Point X="-23.071863352749" Y="-2.999210890441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.201608170017" Y="4.741000216826" />
                  <Point X="-22.950661807205" Y="-3.157164276156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.10226358791" Y="4.721046267111" />
                  <Point X="-22.971181329182" Y="-2.420326057154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.887652858481" Y="-2.947704065556" />
                  <Point X="-22.829460261662" Y="-3.31511766187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.002255170762" Y="4.696901027908" />
                  <Point X="-22.897246930998" Y="-2.279846419632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.755107535502" Y="-3.177279243237" />
                  <Point X="-22.708258716118" Y="-3.473071047585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.902246753614" Y="4.672755788704" />
                  <Point X="-22.817912576754" Y="-2.173460762865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.622562212523" Y="-3.406854420918" />
                  <Point X="-22.587057260215" Y="-3.631023867329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.80223833071" Y="4.648610513155" />
                  <Point X="-22.73020449241" Y="-2.119944757196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.490016960143" Y="-3.63642915285" />
                  <Point X="-22.465856035218" Y="-3.788975229192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.702229898149" Y="4.624465176633" />
                  <Point X="-22.641420995463" Y="-2.073218639477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.357471757752" Y="-3.866003569169" />
                  <Point X="-22.344654810221" Y="-3.946926591054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.602221465587" Y="4.60031984011" />
                  <Point X="-22.551410859326" Y="-2.034237216807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.501312544938" Y="4.570489045556" />
                  <Point X="-22.456498782228" Y="-2.026205431302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.399266144352" Y="4.533476485337" />
                  <Point X="-22.357641043308" Y="-2.043085574105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.297219718963" Y="4.496463768509" />
                  <Point X="-22.976749681625" Y="2.473095584861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.930746728648" Y="2.182644370821" />
                  <Point X="-22.25862455537" Y="-2.060968018762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.195173244799" Y="4.459450743733" />
                  <Point X="-22.907815417501" Y="2.645144826377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.814377093238" Y="2.055198465029" />
                  <Point X="-22.157602493769" Y="-2.091513157167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.092253818488" Y="4.416926116011" />
                  <Point X="-22.832337161652" Y="2.775876930224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.708815250445" Y="1.995990276247" />
                  <Point X="-22.051796918993" Y="-2.152260209127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.988375263445" Y="4.36834578781" />
                  <Point X="-22.756858905802" Y="2.90660903407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.609781801415" Y="1.978000743475" />
                  <Point X="-22.103008254302" Y="-1.221641507209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.019274735987" Y="-1.750314135296" />
                  <Point X="-21.945932100668" Y="-2.213381310135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.884496666445" Y="4.319765194702" />
                  <Point X="-22.681380649952" Y="3.037341137917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.513939193227" Y="1.980157386896" />
                  <Point X="-22.024503260818" Y="-1.110019472683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.904256035109" Y="-1.86923057614" />
                  <Point X="-21.840067282343" Y="-2.274502411143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.780618020496" Y="4.271184292545" />
                  <Point X="-22.605902394103" Y="3.168073241763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.421530890505" Y="2.003997381659" />
                  <Point X="-21.943144410508" Y="-1.01641598102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.794765115524" Y="-1.95324597947" />
                  <Point X="-21.734202464018" Y="-2.335623512151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.675279379506" Y="4.213385344481" />
                  <Point X="-22.530424138253" Y="3.29880534561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.332046845581" Y="2.046300413525" />
                  <Point X="-21.855561966427" Y="-0.962106713955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.685274195939" Y="-2.037261382801" />
                  <Point X="-21.628337645693" Y="-2.396744613159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.569317618828" Y="4.151652173545" />
                  <Point X="-22.454945900982" Y="3.429537566758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.243921157928" Y="2.097179775668" />
                  <Point X="-21.763157008336" Y="-0.938245602021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.575783276354" Y="-2.121276786131" />
                  <Point X="-21.522472827368" Y="-2.457865714167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.46335567879" Y="4.089917870172" />
                  <Point X="-22.37946771078" Y="3.56027008509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.155795470276" Y="2.148059137812" />
                  <Point X="-21.670173811874" Y="-0.91803534348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.466292356769" Y="-2.205292189462" />
                  <Point X="-21.416608004084" Y="-2.518986846482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.357091292069" Y="4.0262739936" />
                  <Point X="-22.303989520579" Y="3.691002603422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.067669782623" Y="2.198938499956" />
                  <Point X="-21.965604190689" Y="1.554521714288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.895354875204" Y="1.110984992239" />
                  <Point X="-21.57550555158" Y="-0.908464159257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.356801425718" Y="-2.28930766519" />
                  <Point X="-21.310743160233" Y="-2.580108108659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.248698588889" Y="3.94919245576" />
                  <Point X="-22.228511330377" Y="3.821735121753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.97954409497" Y="2.249817862099" />
                  <Point X="-21.888782847673" Y="1.676773899503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.784372940571" Y="1.017555690388" />
                  <Point X="-21.635689059094" Y="0.078802608507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.578669099549" Y="-0.281207247434" />
                  <Point X="-21.477407048367" Y="-0.920550676464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.247310457412" Y="-2.373323376132" />
                  <Point X="-21.204878316381" Y="-2.641229370835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.891418407318" Y="2.300697224243" />
                  <Point X="-21.805799907626" Y="1.760123292132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.680841061274" Y="0.971164186707" />
                  <Point X="-21.564511440104" Y="0.236687864842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.463155523091" Y="-0.40324820972" />
                  <Point X="-21.379174544624" Y="-0.933483239719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.137819489106" Y="-2.457339087075" />
                  <Point X="-21.099013472529" Y="-2.702350633012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.803292719665" Y="2.351576586387" />
                  <Point X="-21.720038514176" Y="1.825930220376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.582169036625" Y="0.955456597661" />
                  <Point X="-21.482546315029" Y="0.32646348829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.357299447391" Y="-0.464314111968" />
                  <Point X="-21.280942040881" Y="-0.946415802974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.0283285208" Y="-2.541354798018" />
                  <Point X="-20.99458966619" Y="-2.754373542414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.715167032012" Y="2.40245594853" />
                  <Point X="-21.634277120725" Y="1.89173714862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.485846156781" Y="0.95458092519" />
                  <Point X="-21.394934655382" Y="0.380588295531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.25579527711" Y="-0.497903164785" />
                  <Point X="-21.182709537138" Y="-0.959348366229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.918837552494" Y="-2.62537050896" />
                  <Point X="-20.917030847337" Y="-2.636777596381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.627041345899" Y="2.453335320391" />
                  <Point X="-21.548515727275" Y="1.957544076864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.39162661543" Y="0.966985209319" />
                  <Point X="-21.305320965567" Y="0.422072781772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.155348214003" Y="-0.524817905575" />
                  <Point X="-21.084477033395" Y="-0.972280929485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.53891566177" Y="2.504214704788" />
                  <Point X="-21.462754333824" Y="2.023351005107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.29740707408" Y="0.979389493448" />
                  <Point X="-21.213052551206" Y="0.446795996883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.054901150897" Y="-0.551732646365" />
                  <Point X="-20.986244529652" Y="-0.98521349274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.450789977642" Y="2.555094089185" />
                  <Point X="-21.376992940374" Y="2.089157933351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.203187532729" Y="0.991793777576" />
                  <Point X="-21.120784136844" Y="0.471519211994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.95445408779" Y="-0.578647387155" />
                  <Point X="-20.888012025909" Y="-0.998146055995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.362664293514" Y="2.605973473582" />
                  <Point X="-21.291231546923" Y="2.154964861595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.108967991379" Y="1.004198061705" />
                  <Point X="-21.028515722483" Y="0.496242427104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.854007024684" Y="-0.605562127945" />
                  <Point X="-20.789779522166" Y="-1.01107861925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.274538609386" Y="2.656852857979" />
                  <Point X="-21.205470166855" Y="2.220771874335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.014748450028" Y="1.016602345834" />
                  <Point X="-20.936247308121" Y="0.520965642215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.753559961578" Y="-0.632476868735" />
                  <Point X="-20.691547016946" Y="-1.024011191836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.186412925258" Y="2.707732242375" />
                  <Point X="-21.11970883691" Y="2.286579203536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.920528908678" Y="1.029006629963" />
                  <Point X="-20.84397889376" Y="0.545688857326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.653112902024" Y="-0.659391587098" />
                  <Point X="-20.593314511116" Y="-1.036943768265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.09828724113" Y="2.758611626772" />
                  <Point X="-21.033947506965" Y="2.352386532738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.826309365691" Y="1.041410903759" />
                  <Point X="-20.751710479398" Y="0.570412072436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.552665846597" Y="-0.6863062794" />
                  <Point X="-20.495082005287" Y="-1.049876344694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.990412892003" Y="2.684802847622" />
                  <Point X="-20.94818617702" Y="2.418193861939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.732089808303" Y="1.053815086631" />
                  <Point X="-20.659442065036" Y="0.595135287547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.45221879117" Y="-0.713220971702" />
                  <Point X="-20.396849499457" Y="-1.062808921122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.637870250915" Y="1.066219269503" />
                  <Point X="-20.567173653357" Y="0.619858519593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.351771735744" Y="-0.740135664004" />
                  <Point X="-20.298616993628" Y="-1.075741497551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.543650693527" Y="1.078623452375" />
                  <Point X="-20.474905244026" Y="0.644581766461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.251324680317" Y="-0.767050356306" />
                  <Point X="-20.232028963703" Y="-0.888878716307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.449431136139" Y="1.091027635247" />
                  <Point X="-20.382636834694" Y="0.669305013329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.355211578751" Y="1.103431818118" />
                  <Point X="-20.290368425362" Y="0.694028260197" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.3153515625" Y="-4.332440917969" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.622783203125" Y="-3.396045898438" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.86044921875" Y="-3.224567138672" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.14344921875" Y="-3.229599121094" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643798828" />
                  <Point X="-25.375380859375" Y="-3.411141845703" />
                  <Point X="-25.452005859375" Y="-3.521544189453" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.646701171875" Y="-4.2367734375" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.96144140625" Y="-4.9650078125" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.256095703125" Y="-4.897966308594" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.334802734375" Y="-4.745893554688" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.3418828125" Y="-4.372875488281" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.510376953125" Y="-4.093801269531" />
                  <Point X="-26.61954296875" Y="-3.998064208984" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.81394140625" Y="-3.974967773438" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791015625" />
                  <Point X="-27.127115234375" Y="-4.065489990234" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157293945313" />
                  <Point X="-27.364984375" Y="-4.2944609375" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.652375" Y="-4.293586914062" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.071640625" Y="-4.001446289062" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.9020703125" Y="-3.315074951172" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597593261719" />
                  <Point X="-27.513978515625" Y="-2.568764404297" />
                  <Point X="-27.531326171875" Y="-2.551415771484" />
                  <Point X="-27.56015625" Y="-2.537198730469" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.195126953125" Y="-2.891868164062" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-29.0009609375" Y="-3.058312011719" />
                  <Point X="-29.161697265625" Y="-2.847135742188" />
                  <Point X="-29.316419921875" Y="-2.587692382812" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-28.85672265625" Y="-1.954852905273" />
                  <Point X="-28.163787109375" Y="-1.423144897461" />
                  <Point X="-28.145822265625" Y="-1.396012573242" />
                  <Point X="-28.1381171875" Y="-1.366265258789" />
                  <Point X="-28.140326171875" Y="-1.334594726562" />
                  <Point X="-28.161158203125" Y="-1.310638671875" />
                  <Point X="-28.187640625" Y="-1.295052124023" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.981318359375" Y="-1.388862670898" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.8645234375" Y="-1.257315673828" />
                  <Point X="-29.927392578125" Y="-1.011187805176" />
                  <Point X="-29.968326171875" Y="-0.724983154297" />
                  <Point X="-29.99839453125" Y="-0.514742248535" />
                  <Point X="-29.3468203125" Y="-0.340153167725" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.52712890625" Y="-0.111177429199" />
                  <Point X="-28.50232421875" Y="-0.090645599365" />
                  <Point X="-28.489974609375" Y="-0.060046287537" />
                  <Point X="-28.483400390625" Y="-0.03127722168" />
                  <Point X="-28.490568359375" Y="-0.000606403053" />
                  <Point X="-28.5023203125" Y="0.028083265305" />
                  <Point X="-28.52890625" Y="0.049850105286" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.25187109375" Y="0.252151504517" />
                  <Point X="-29.998185546875" Y="0.452126068115" />
                  <Point X="-29.958162109375" Y="0.722608642578" />
                  <Point X="-29.91764453125" Y="0.996414978027" />
                  <Point X="-29.835244140625" Y="1.300498413086" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.319095703125" Y="1.468473266602" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.699025390625" Y="1.406169921875" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.651533203125" Y="1.426056152344" />
                  <Point X="-28.639119140625" Y="1.443785888672" />
                  <Point X="-28.6260078125" Y="1.475441894531" />
                  <Point X="-28.61447265625" Y="1.503290161133" />
                  <Point X="-28.610712890625" Y="1.524606445312" />
                  <Point X="-28.61631640625" Y="1.545513793945" />
                  <Point X="-28.63213671875" Y="1.575906616211" />
                  <Point X="-28.646056640625" Y="1.602643432617" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-29.05828125" Y="1.92485925293" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.31744921875" Y="2.517283447266" />
                  <Point X="-29.16001171875" Y="2.787007568359" />
                  <Point X="-28.941740234375" Y="3.067566162109" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.506673828125" Y="3.127582763672" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.093001953125" Y="2.916453125" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775146484" />
                  <Point X="-28.013251953125" Y="2.927403808594" />
                  <Point X="-27.980947265625" Y="2.959708496094" />
                  <Point X="-27.95252734375" Y="2.988127197266" />
                  <Point X="-27.9408984375" Y="3.006381347656" />
                  <Point X="-27.938072265625" Y="3.027840087891" />
                  <Point X="-27.9420546875" Y="3.073352050781" />
                  <Point X="-27.945556640625" Y="3.113389648438" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.128572265625" Y="3.439748779297" />
                  <Point X="-28.30727734375" Y="3.749276611328" />
                  <Point X="-28.027076171875" Y="3.964105224609" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.4091015625" Y="4.365324707031" />
                  <Point X="-27.141548828125" Y="4.513971679688" />
                  <Point X="-27.074212890625" Y="4.426217285156" />
                  <Point X="-26.967826171875" Y="4.287572753906" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.900591796875" Y="4.247291503906" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.761046875" Y="4.244104980469" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.696904296875" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.29448828125" />
                  <Point X="-26.66891015625" Y="4.348952148438" />
                  <Point X="-26.653802734375" Y="4.396864746094" />
                  <Point X="-26.651916015625" Y="4.418426757812" />
                  <Point X="-26.671982421875" Y="4.570848144531" />
                  <Point X="-26.68913671875" Y="4.701141113281" />
                  <Point X="-26.3230625" Y="4.803775390625" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.5513359375" Y="4.952071777344" />
                  <Point X="-25.22419921875" Y="4.990358398438" />
                  <Point X="-25.1449453125" Y="4.694578125" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.855107421875" Y="4.648427246094" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.449732421875" Y="4.958024902344" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.794998046875" Y="4.842321289062" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.2668359375" Y="4.687555664062" />
                  <Point X="-23.068970703125" Y="4.615788085938" />
                  <Point X="-22.851953125" Y="4.514296875" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.45165625" Y="4.30299609375" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-22.06975390625" Y="4.055083740234" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.31176171875" Y="3.297539794922" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491515869141" />
                  <Point X="-22.786568359375" Y="2.448803955078" />
                  <Point X="-22.7966171875" Y="2.411229492188" />
                  <Point X="-22.797955078125" Y="2.392325683594" />
                  <Point X="-22.793501953125" Y="2.355392089844" />
                  <Point X="-22.789583984375" Y="2.322900878906" />
                  <Point X="-22.78131640625" Y="2.300811279297" />
                  <Point X="-22.758462890625" Y="2.267131347656" />
                  <Point X="-22.738357421875" Y="2.237502685547" />
                  <Point X="-22.72505859375" Y="2.224203613281" />
                  <Point X="-22.69137890625" Y="2.201350341797" />
                  <Point X="-22.66175" Y="2.18124609375" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.602728515625" Y="2.168526611328" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5513359375" Y="2.165946289062" />
                  <Point X="-22.508623046875" Y="2.177368164062" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.7605234375" Y="2.595662597656" />
                  <Point X="-21.005751953125" Y="3.031430419922" />
                  <Point X="-20.906658203125" Y="2.893712890625" />
                  <Point X="-20.79740234375" Y="2.741874267578" />
                  <Point X="-20.687181640625" Y="2.559730712891" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.10787890625" Y="2.056167236328" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583832275391" />
                  <Point X="-21.751369140625" Y="1.543729736328" />
                  <Point X="-21.77841015625" Y="1.508450927734" />
                  <Point X="-21.78687890625" Y="1.491501220703" />
                  <Point X="-21.798330078125" Y="1.450556396484" />
                  <Point X="-21.808404296875" Y="1.414536621094" />
                  <Point X="-21.809220703125" Y="1.39096484375" />
                  <Point X="-21.7998203125" Y="1.345408447266" />
                  <Point X="-21.79155078125" Y="1.30533215332" />
                  <Point X="-21.784353515625" Y="1.287955810547" />
                  <Point X="-21.758787109375" Y="1.249095947266" />
                  <Point X="-21.736296875" Y="1.214910400391" />
                  <Point X="-21.719052734375" Y="1.19882019043" />
                  <Point X="-21.682001953125" Y="1.177964599609" />
                  <Point X="-21.64941015625" Y="1.159617797852" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.58133984375" Y="1.146999145508" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.860724609375" Y="1.22851965332" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.107734375" Y="1.144127441406" />
                  <Point X="-20.060806640625" Y="0.951366516113" />
                  <Point X="-20.02607421875" Y="0.728285766602" />
                  <Point X="-20.002140625" Y="0.57455645752" />
                  <Point X="-20.567203125" Y="0.423148132324" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819351196" />
                  <Point X="-21.320126953125" Y="0.204372238159" />
                  <Point X="-21.363421875" Y="0.179346832275" />
                  <Point X="-21.377734375" Y="0.166926239014" />
                  <Point X="-21.407263671875" Y="0.129299362183" />
                  <Point X="-21.433240234375" Y="0.096198562622" />
                  <Point X="-21.443013671875" Y="0.074734344482" />
                  <Point X="-21.452857421875" Y="0.023337913513" />
                  <Point X="-21.461515625" Y="-0.021876211166" />
                  <Point X="-21.461515625" Y="-0.040683864594" />
                  <Point X="-21.451671875" Y="-0.092080299377" />
                  <Point X="-21.443013671875" Y="-0.137294433594" />
                  <Point X="-21.433240234375" Y="-0.158758636475" />
                  <Point X="-21.4037109375" Y="-0.196385513306" />
                  <Point X="-21.377734375" Y="-0.229486312866" />
                  <Point X="-21.363421875" Y="-0.241906906128" />
                  <Point X="-21.31420703125" Y="-0.270354034424" />
                  <Point X="-21.270912109375" Y="-0.295379425049" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.6503984375" Y="-0.463416473389" />
                  <Point X="-20.001931640625" Y="-0.637172546387" />
                  <Point X="-20.0253671875" Y="-0.792624816895" />
                  <Point X="-20.051568359375" Y="-0.966413574219" />
                  <Point X="-20.096068359375" Y="-1.161412963867" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-20.785923828125" Y="-1.203225830078" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.70175390625" Y="-1.11933581543" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.814552734375" Y="-1.154697021484" />
                  <Point X="-21.872935546875" Y="-1.22491418457" />
                  <Point X="-21.924296875" Y="-1.286685058594" />
                  <Point X="-21.935640625" Y="-1.314070556641" />
                  <Point X="-21.9440078125" Y="-1.405004882813" />
                  <Point X="-21.951369140625" Y="-1.485000854492" />
                  <Point X="-21.943638671875" Y="-1.516621826172" />
                  <Point X="-21.89018359375" Y="-1.599768066406" />
                  <Point X="-21.843158203125" Y="-1.672912475586" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-21.26690625" Y="-2.118797119141" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.722013671875" Y="-2.682635253906" />
                  <Point X="-20.795869140625" Y="-2.802142822266" />
                  <Point X="-20.887900390625" Y="-2.932906982422" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.532171875" Y="-2.671659179688" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.3776171875" Y="-2.232551269531" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219245117188" />
                  <Point X="-22.60642578125" Y="-2.269507568359" />
                  <Point X="-22.690439453125" Y="-2.313724121094" />
                  <Point X="-22.7113984375" Y="-2.33468359375" />
                  <Point X="-22.761662109375" Y="-2.430186523438" />
                  <Point X="-22.80587890625" Y="-2.514201660156" />
                  <Point X="-22.8108359375" Y="-2.546375244141" />
                  <Point X="-22.79007421875" Y="-2.661334472656" />
                  <Point X="-22.77180859375" Y="-2.762465576172" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.40307421875" Y="-3.407021972656" />
                  <Point X="-22.01332421875" Y="-4.082087646484" />
                  <Point X="-22.077068359375" Y="-4.1276171875" />
                  <Point X="-22.16470703125" Y="-4.190215820312" />
                  <Point X="-22.267599609375" Y="-4.25681640625" />
                  <Point X="-22.32022265625" Y="-4.29087890625" />
                  <Point X="-22.771689453125" Y="-3.702517822266" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.442830078125" Y="-2.907573730469" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.6981953125" Y="-2.847127685547" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.930416015625" Y="-2.948122558594" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045986083984" />
                  <Point X="-24.060171875" Y="-3.177701904297" />
                  <Point X="-24.085357421875" Y="-3.293573974609" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-23.98324609375" Y="-4.091744628906" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.92274609375" Y="-4.945073730469" />
                  <Point X="-24.005650390625" Y="-4.96324609375" />
                  <Point X="-24.1007109375" Y="-4.980515625" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#170" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.102805347483" Y="4.73879927325" Z="1.35" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.35" />
                  <Point X="-0.563304405554" Y="5.033012932527" Z="1.35" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.35" />
                  <Point X="-1.342716655857" Y="4.883199221201" Z="1.35" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.35" />
                  <Point X="-1.727712534092" Y="4.595601985722" Z="1.35" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.35" />
                  <Point X="-1.72275271033" Y="4.395267975064" Z="1.35" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.35" />
                  <Point X="-1.786338922581" Y="4.321578722722" Z="1.35" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.35" />
                  <Point X="-1.883660584239" Y="4.322922010621" Z="1.35" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.35" />
                  <Point X="-2.040700872544" Y="4.487935890493" Z="1.35" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.35" />
                  <Point X="-2.439541199834" Y="4.440312298846" Z="1.35" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.35" />
                  <Point X="-3.063654017963" Y="4.035065168983" Z="1.35" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.35" />
                  <Point X="-3.17802982978" Y="3.446028767819" Z="1.35" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.35" />
                  <Point X="-2.998021829762" Y="3.100275476023" Z="1.35" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.35" />
                  <Point X="-3.022458869146" Y="3.02634469874" Z="1.35" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.35" />
                  <Point X="-3.094800989583" Y="2.997542869411" Z="1.35" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.35" />
                  <Point X="-3.487830580478" Y="3.202164118966" Z="1.35" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.35" />
                  <Point X="-3.987360446131" Y="3.129548652619" Z="1.35" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.35" />
                  <Point X="-4.366786822006" Y="2.573768859208" Z="1.35" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.35" />
                  <Point X="-4.094876625441" Y="1.916471426538" Z="1.35" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.35" />
                  <Point X="-3.682643920398" Y="1.584097178585" Z="1.35" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.35" />
                  <Point X="-3.678357548279" Y="1.525856148576" Z="1.35" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.35" />
                  <Point X="-3.720217593258" Y="1.485136008938" Z="1.35" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.35" />
                  <Point X="-4.318726998419" Y="1.549325612174" Z="1.35" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.35" />
                  <Point X="-4.889661232072" Y="1.344855634826" Z="1.35" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.35" />
                  <Point X="-5.013779375529" Y="0.761207788538" Z="1.35" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.35" />
                  <Point X="-4.270969201986" Y="0.235135222693" Z="1.35" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.35" />
                  <Point X="-3.563572280561" Y="0.040054302478" Z="1.35" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.35" />
                  <Point X="-3.544478299986" Y="0.015857180852" Z="1.35" />
                  <Point X="-3.539556741714" Y="0" Z="1.35" />
                  <Point X="-3.543886301149" Y="-0.013949770209" Z="1.35" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.35" />
                  <Point X="-3.561796314562" Y="-0.038821680656" Z="1.35" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.35" />
                  <Point X="-4.365918586786" Y="-0.260576834063" Z="1.35" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.35" />
                  <Point X="-5.023979373959" Y="-0.700782163225" Z="1.35" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.35" />
                  <Point X="-4.919126259509" Y="-1.238409788058" Z="1.35" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.35" />
                  <Point X="-3.980949679484" Y="-1.407154949071" Z="1.35" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.35" />
                  <Point X="-3.206764935393" Y="-1.31415784904" Z="1.35" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.35" />
                  <Point X="-3.19628204035" Y="-1.336371687942" Z="1.35" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.35" />
                  <Point X="-3.893316217205" Y="-1.883905192708" Z="1.35" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.35" />
                  <Point X="-4.365520048739" Y="-2.582021936355" Z="1.35" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.35" />
                  <Point X="-4.046903228686" Y="-3.057315111949" Z="1.35" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.35" />
                  <Point X="-3.176283476995" Y="-2.903889509186" Z="1.35" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.35" />
                  <Point X="-2.564720319296" Y="-2.563610149835" Z="1.35" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.35" />
                  <Point X="-2.951527503982" Y="-3.258794905497" Z="1.35" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.35" />
                  <Point X="-3.108301585304" Y="-4.009783333633" Z="1.35" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.35" />
                  <Point X="-2.684854354761" Y="-4.304817669649" Z="1.35" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.35" />
                  <Point X="-2.331474243682" Y="-4.293619164938" Z="1.35" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.35" />
                  <Point X="-2.105493044099" Y="-4.075783366075" Z="1.35" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.35" />
                  <Point X="-1.823367059259" Y="-3.993580900763" Z="1.35" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.35" />
                  <Point X="-1.549499845498" Y="-4.100112957746" Z="1.35" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.35" />
                  <Point X="-1.397078897604" Y="-4.351350228926" Z="1.35" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.35" />
                  <Point X="-1.390531659925" Y="-4.70808697415" Z="1.35" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.35" />
                  <Point X="-1.274711593401" Y="-4.915109139864" Z="1.35" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.35" />
                  <Point X="-0.97712526191" Y="-4.982811750118" Z="1.35" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.35" />
                  <Point X="-0.604560402486" Y="-4.218434212858" Z="1.35" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.35" />
                  <Point X="-0.340461572849" Y="-3.408370763891" Z="1.35" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.35" />
                  <Point X="-0.134786034766" Y="-3.246071979561" Z="1.35" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.35" />
                  <Point X="0.118573044595" Y="-3.241040065727" Z="1.35" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.35" />
                  <Point X="0.329984286584" Y="-3.393274991876" Z="1.35" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.35" />
                  <Point X="0.630194457915" Y="-4.314101816958" Z="1.35" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.35" />
                  <Point X="0.9020688815" Y="-4.998430405642" Z="1.35" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.35" />
                  <Point X="1.081802899966" Y="-4.962634056388" Z="1.35" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.35" />
                  <Point X="1.060169612329" Y="-4.053938120299" Z="1.35" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.35" />
                  <Point X="0.982531055887" Y="-3.157041686981" Z="1.35" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.35" />
                  <Point X="1.09539311124" Y="-2.955288957078" Z="1.35" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.35" />
                  <Point X="1.300229244985" Y="-2.865637293466" Z="1.35" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.35" />
                  <Point X="1.523973063987" Y="-2.918351870061" Z="1.35" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.35" />
                  <Point X="2.182486426722" Y="-3.701675668991" Z="1.35" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.35" />
                  <Point X="2.753413630783" Y="-4.267510547051" Z="1.35" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.35" />
                  <Point X="2.945838185923" Y="-4.137024367446" Z="1.35" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.35" />
                  <Point X="2.634068973584" Y="-3.350741951222" Z="1.35" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.35" />
                  <Point X="2.252972676485" Y="-2.621167446835" Z="1.35" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.35" />
                  <Point X="2.276427695506" Y="-2.422193059356" Z="1.35" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.35" />
                  <Point X="2.410705319016" Y="-2.282473614461" Z="1.35" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.35" />
                  <Point X="2.607339356677" Y="-2.250475333006" Z="1.35" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.35" />
                  <Point X="3.436671645885" Y="-2.683680526691" Z="1.35" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.35" />
                  <Point X="4.146831866609" Y="-2.930404156577" Z="1.35" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.35" />
                  <Point X="4.314362464013" Y="-2.677640604754" Z="1.35" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.35" />
                  <Point X="3.757373522278" Y="-2.047849459645" Z="1.35" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.35" />
                  <Point X="3.145717640908" Y="-1.541448446371" Z="1.35" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.35" />
                  <Point X="3.099623717119" Y="-1.378306461608" Z="1.35" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.35" />
                  <Point X="3.15935211385" Y="-1.225601306176" Z="1.35" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.35" />
                  <Point X="3.30270834925" Y="-1.136914988913" Z="1.35" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.35" />
                  <Point X="4.201393996645" Y="-1.22151811288" Z="1.35" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.35" />
                  <Point X="4.946521083687" Y="-1.14125654214" Z="1.35" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.35" />
                  <Point X="5.017916520204" Y="-0.768798931806" Z="1.35" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.35" />
                  <Point X="4.356387019256" Y="-0.383840113975" Z="1.35" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.35" />
                  <Point X="3.704657663221" Y="-0.195785329771" Z="1.35" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.35" />
                  <Point X="3.629465692827" Y="-0.134237377034" Z="1.35" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.35" />
                  <Point X="3.591277716421" Y="-0.051396423037" Z="1.35" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.35" />
                  <Point X="3.590093734003" Y="0.045214108171" Z="1.35" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.35" />
                  <Point X="3.625913745573" Y="0.129711361541" Z="1.35" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.35" />
                  <Point X="3.698737751131" Y="0.192363514471" Z="1.35" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.35" />
                  <Point X="4.43958079805" Y="0.406131818794" Z="1.35" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.35" />
                  <Point X="5.017172613612" Y="0.767257664548" Z="1.35" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.35" />
                  <Point X="4.93469125418" Y="1.18723438052" Z="1.35" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.35" />
                  <Point X="4.126593760117" Y="1.309371901446" Z="1.35" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.35" />
                  <Point X="3.419053792065" Y="1.227848156233" Z="1.35" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.35" />
                  <Point X="3.336367858012" Y="1.252815480718" Z="1.35" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.35" />
                  <Point X="3.276827426919" Y="1.307856552542" Z="1.35" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.35" />
                  <Point X="3.242991852003" Y="1.386792891309" Z="1.35" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.35" />
                  <Point X="3.243665327717" Y="1.468368892274" Z="1.35" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.35" />
                  <Point X="3.282158269935" Y="1.544592493117" Z="1.35" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.35" />
                  <Point X="3.916401800489" Y="2.047779716284" Z="1.35" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.35" />
                  <Point X="4.349439197939" Y="2.616896967672" Z="1.35" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.35" />
                  <Point X="4.127771171594" Y="2.954196143936" Z="1.35" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.35" />
                  <Point X="3.208319140857" Y="2.670243971639" Z="1.35" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.35" />
                  <Point X="2.472303632956" Y="2.256950913952" Z="1.35" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.35" />
                  <Point X="2.397100544908" Y="2.24944700097" Z="1.35" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.35" />
                  <Point X="2.330538060356" Y="2.27400482748" Z="1.35" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.35" />
                  <Point X="2.276753772755" Y="2.326486800026" Z="1.35" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.35" />
                  <Point X="2.249982701789" Y="2.392657898924" Z="1.35" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.35" />
                  <Point X="2.255577028706" Y="2.467165942926" Z="1.35" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.35" />
                  <Point X="2.725381381221" Y="3.303819941098" Z="1.35" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.35" />
                  <Point X="2.953064857973" Y="4.127110962929" Z="1.35" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.35" />
                  <Point X="2.567356240633" Y="4.377478527703" Z="1.35" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.35" />
                  <Point X="2.163070769038" Y="4.590868983551" Z="1.35" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.35" />
                  <Point X="1.744056127644" Y="4.765838896284" Z="1.35" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.35" />
                  <Point X="1.210579276667" Y="4.922205065431" Z="1.35" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.35" />
                  <Point X="0.549317033713" Y="5.039032734664" Z="1.35" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.35" />
                  <Point X="0.09043921407" Y="4.692648232701" Z="1.35" />
                  <Point X="0" Y="4.355124473572" Z="1.35" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>